drop table if exists pGlobalCurrency_tbl;
create table pGlobalCurrency_tbl ( pGlobalCurrency varchar(3));
drop table if exists dim_profitcenter_789;
create table dim_profitcenter_789 as select first 0 * from dim_profitcenter order by ValidTo;

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'bi_populate_materialmovement_fact START');

Insert into pGlobalCurrency_tbl values('USD');

Update pGlobalCurrency_tbl
SET pGlobalCurrency =
        ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');
                
INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms');

UPDATE MKPF_MSEG 
SET MSEG_BUKRS = ifnull(MSEG_BUKRS,'Not Set') 
where MSEG_BUKRS <>  ifnull(MSEG_BUKRS,'Not Set');

UPDATE MKPF_MSEG1 
SET MSEG1_BUKRS = ifnull(MSEG1_BUKRS,'Not Set') 
where  MSEG1_BUKRS <>  ifnull(MSEG1_BUKRS,'Not Set');

  UPDATE fact_materialmovement ms
	from MKPF_MSEG m, 
          dim_plant dp, 
          dim_company dc
  SET
          dd_DocumentNo = ifnull(m.MSEG_EBELN,'Not Set') 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND dd_DocumentNo <> ifnull(m.MSEG_EBELN,'Not Set');

  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET
	dd_DocumentItemNo = m.MSEG_EBELP 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
    AND dd_DocumentItemNo <>  m.MSEG_EBELP;


  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET  dd_SalesOrderNo = ifnull(m.MSEG_KDAUF,'Not Set')
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_SalesOrderNo <> ifnull(m.MSEG_KDAUF,'Not Set');

  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET dd_SalesOrderItemNo = m.MSEG_KDPOS 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_SalesOrderItemNo <> m.MSEG_KDPOS;

  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET  
dd_SalesOrderDlvrNo = ifnull(m.MSEG_KDEIN,0) 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  dd_SalesOrderDlvrNo <> ifnull(m.MSEG_KDEIN,0);


  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET dd_GLAccountNo = ifnull(m.MSEG_SAKTO,'Not Set')
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  dd_GLAccountNo <>  ifnull(m.MSEG_SAKTO,'Not Set');

 
  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET  dd_ReferenceDocNo = ifnull(m.MSEG_LFBNR,'Not Set') 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_ReferenceDocNo <>  ifnull(m.MSEG_LFBNR,'Not Set');

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dd_ReferenceDocItem = ifnull(m.MSEG_LFPOS,0) 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_ReferenceDocItem <> ifnull(m.MSEG_LFPOS,0);

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dd_debitcreditid = CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  dd_debitcreditid <> CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dd_productionordernumber = ifnull(m.MSEG_AUFNR,'Not Set') 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_productionordernumber <> ifnull(m.MSEG_AUFNR,'Not Set');

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dd_productionorderitemno = m.MSEG_AUFPS 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  dd_productionorderitemno <>  m.MSEG_AUFPS;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dd_GoodsMoveReason = m.MSEG_GRUND 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  dd_GoodsMoveReason <> m.MSEG_GRUND;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dd_BatchNumber = ifnull(m.MSEG_CHARG, 'Not Set')
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  dd_BatchNumber <>  ifnull(m.MSEG_CHARG, 'Not Set');

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dd_ValuationType = m.MSEG_BWTAR 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_ValuationType <> m.MSEG_BWTAR;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_LocalCurrAmt = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND amt_LocalCurrAmt <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_StdUnitPrice = m.MSEG_DMBTR / (case when m.MSEG_ERFMG=0 then null else m.MSEG_ERFMG end)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND  amt_StdUnitPrice  <> ( m.MSEG_DMBTR / (case when m.MSEG_ERFMG=0 then null else m.MSEG_ERFMG end));


           UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET   amt_DeliveryCost = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND  amt_DeliveryCost <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR ;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_AltPriceControl = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND amt_AltPriceControl <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_OnHand_ValStock = m.MSEG_SALK3 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND amt_OnHand_ValStock <> m.MSEG_SALK3;


 UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_ExchangeRate = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  MSEG_WAERS = dc.Currency;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_ExchangeRate =  (select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = MSEG_WAERS and z.pToCurrency = dc.Currency and z.pDate = MKPF_BUDAT)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND MSEG_WAERS <> dc.Currency;


   UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_ExchangeRate_GBL = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dc.Currency = (select pGlobalCurrency from pGlobalCurrency_tbl);

         UPDATE fact_materialmovement ms
        from pGlobalCurrency_tbl,MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_ExchangeRate_GBL =  (select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.Currency and z.pToCurrency = pGlobalCurrency and z.pDate = MKPF_BUDAT)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dc.Currency <>  pGlobalCurrency ;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     ct_Quantity = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND  ct_Quantity <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     ct_QtyEntryUOM = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND  ct_QtyEntryUOM <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     ct_QtyGROrdUnit = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND ct_QtyGROrdUnit <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     ct_QtyOnHand_ValStock = m.MSEG_LBKUM 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  ct_QtyOnHand_ValStock <> m.MSEG_LBKUM;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     ct_QtyOrdPriceUnit = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND ct_QtyOrdPriceUnit <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG;



UPDATE fact_materialmovement ms


        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
Set dim_DateIDMaterialDocDate = 1
 WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MKPF_BLDAT is null;

UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	 dim_date mdd
SET     dim_DateIDMaterialDocDate =  mdd.Dim_Dateid
WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  mdd.CompanyCode = m.MSEG_BUKRS AND m.MKPF_BLDAT = mdd.DateValue
	AND dim_DateIDMaterialDocDate <>  mdd.Dim_Dateid;


  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_DateIDPostingDate =1
 WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MKPF_BUDAT is null;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_date pd
  SET     dim_DateIDPostingDate = pd.Dim_Dateid 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  pd.DateValue = m.MKPF_BUDAT AND pd.CompanyCode = m.MSEG_BUKRS
	AND dim_DateIDPostingDate <> pd.Dim_Dateid;


 UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_MovementIndicatorid = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_KZBEW is null;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_movementindicator dmi
  SET     ms.dim_MovementIndicatorid = dmi.dim_MovementIndicatorid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND dmi.TypeCode = m.MSEG_KZBEW
	AND ms.dim_MovementIndicatorid <> dmi.dim_MovementIndicatorid;


 UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_specialstockid = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_SOBKZ is null;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_specialstock spt
  SET    ms.dim_specialstockid = spt.dim_specialstockid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  spt.specialstockindicator = m.MSEG_SOBKZ
	AND  ms.dim_specialstockid <> spt.dim_specialstockid;


  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_unitofmeasureid = 1
 WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_MEINS is null;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	 dim_unitofmeasure uom
  SET     dim_unitofmeasureid = uom.Dim_UnitOfMeasureid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND uom.UOM = m.MSEG_MEINS
	AND ms.dim_unitofmeasureid <>  uom.Dim_UnitOfMeasureid;


 UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_controllingareaid =1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_KOKRS is null;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_controllingarea ca
  SET     dim_controllingareaid = ca.dim_controllingareaid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  ca.ControllingAreaCode = m.MSEG_KOKRS
	AND ms.dim_controllingareaid <>  ca.dim_controllingareaid;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_profitcenterid = ifnull((SELECT pc.Dim_ProfitCenterid
                    FROM dim_profitcenter_789 pc
                   WHERE  pc.ControllingArea = MSEG_KOKRS
                      AND pc.ProfitCenterCode = MSEG_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT ),1)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


   UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_consumptiontypeid = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_KZVBR is null;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_consumptiontype ct
  SET     dim_consumptiontypeid = ct.Dim_ConsumptionTypeid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  ct.ConsumptionCode = m.MSEG_KZVBR
	AND ms.dim_consumptiontypeid <> ct.Dim_ConsumptionTypeid;


      UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_stocktypeid = 1
  WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_INSMK is null;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	 dim_stocktype st
  SET     dim_stocktypeid = st.Dim_StockTypeid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND st.TypeCode = m.MSEG_INSMK
	AND ms.dim_stocktypeid <> st.Dim_StockTypeid;


  UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_PurchaseGroupid = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG_MATNR IS NULL;


 UPDATE fact_materialmovement ms
from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_PurchaseGroupid =  ifnull((SELECT pg.Dim_PurchaseGroupid
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG_MATNR
                                      AND dpr.Plant = m.MSEG_WERKS), 1)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG_MATNR IS NOT NULL;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_materialgroupid = ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup
                        AND dpr.PartNumber = m.MSEG_MATNR
                        AND dpr.Plant = m.MSEG_WERKS), 1) 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


     UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_receivingplantid = 1
   WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_UMWRK is null;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	 dim_plant pl
  SET     Dim_receivingplantid = pl.dim_plantid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  pl.PlantCode = m.MSEG_UMWRK
	AND  ms.Dim_receivingplantid <> pl.dim_plantid;


    UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_receivingpartid = 1
   WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_UMMAT is null;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_part prt
  SET     Dim_receivingpartid = prt.Dim_Partid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND prt.Plant = MSEG_UMWRK AND prt.PartNumber = m.MSEG_UMMAT
	AND   Dim_receivingpartid  <> prt.Dim_Partid ;


 UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_RecvIssuStorLocid =1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG_UMLGO IS NULL;

UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	  dim_storagelocation dsl
  SET     Dim_RecvIssuStorLocid = dsl.Dim_StorageLocationid
                          
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND dsl.LocationCode = m.MSEG_UMLGO
	  AND dsl.Plant = m.MSEG_UMWRK
	  AND ifnull(ms.Dim_RecvIssuStorLocid, 1) <> ifnull(dsl.Dim_StorageLocationid, 1)
	  AND m.MSEG_UMLGO IS NOT NULL;

   UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_StorageLocationid = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG_LGORT IS NULL;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_StorageLocationid = (SELECT Dim_StorageLocationid
                          FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG_LGORT
                                AND dsl.Plant = m.MSEG_WERKS)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG_LGORT IS NOT NULL;


     UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_Currencyid = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_WAERS is null;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_currency dcr
  SET     ms.Dim_Currencyid = dcr.Dim_Currencyid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  dcr.CurrencyCode = m.MSEG_WAERS
	AND  ms.Dim_Currencyid <> dcr.Dim_Currencyid ;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_Partid = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_part dpr
  SET     ms.Dim_Partid =dpr.Dim_Partid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND dpr.PartNumber = m.MSEG_MATNR  AND dpr.Plant = m.MSEG_WERKS
	AND  ms.Dim_Partid  <> dpr.Dim_Partid;


   UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_Vendorid =1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_LIFNR is null;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	 dim_vendor dv
  SET     ms.Dim_Vendorid = dv.Dim_Vendorid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  dv.VendorNumber = m.MSEG_LIFNR
	AND ms.Dim_Vendorid <> dv.Dim_Vendorid;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_producthierarchyid = ifnull((SELECT dim_producthierarchyid
                                            FROM dim_producthierarchy dph, dim_part dpr
                                            WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                                  AND dpr.PartNumber = m.MSEG_MATNR
                                                  AND dpr.Plant = m.MSEG_WERKS), 1)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET    Dim_AfsSizeId = 1
 WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_afssize da
  SET    Dim_AfsSizeId = da.dim_AfsSizeId
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  da.Size = MSEG_J_3ASIZ AND da.RowIsCurrent = 1
	AND  ms.Dim_AfsSizeId <>  da.dim_AfsSizeId;

UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m, 
          dim_plant dp, 
          dim_company dc
  SET dd_DocumentNo = ifnull(m.MSEG1_EBELN,'Not Set') 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_DocumentNo <> ifnull(m.MSEG1_EBELN,'Not Set');
     

UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET      dd_DocumentItemNo = m.MSEG1_EBELP 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_DocumentItemNo <> m.MSEG1_EBELP;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_SalesOrderNo = ifnull(m.MSEG1_KDAUF,'Not Set') 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  dd_SalesOrderNo <> ifnull(m.MSEG1_KDAUF,'Not Set');

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_SalesOrderItemNo = m.MSEG1_KDPOS 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  dd_SalesOrderItemNo <> m.MSEG1_KDPOS;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_SalesOrderDlvrNo = ifnull(m.MSEG1_KDEIN,0) 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_SalesOrderDlvrNo<>  ifnull(m.MSEG1_KDEIN,0);

UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_GLAccountNo = ifnull(m.MSEG1_SAKTO,'Not Set') 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_GLAccountNo <> ifnull(m.MSEG1_SAKTO,'Not Set');

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_ReferenceDocNo = ifnull(m.MSEG1_LFBNR,'Not Set') 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_ReferenceDocNo <>  ifnull(m.MSEG1_LFBNR,'Not Set');

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_ReferenceDocItem = ifnull(m.MSEG1_LFPOS,0) 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  dd_ReferenceDocItem <> ifnull(m.MSEG1_LFPOS,0);

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_debitcreditid = CASE WHEN m.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  dd_debitcreditid <>  CASE WHEN m.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_productionordernumber = ifnull(m.MSEG1_AUFNR,'Not Set')  
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_productionordernumber <> ifnull(m.MSEG1_AUFNR,'Not Set');

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_productionorderitemno = m.MSEG1_AUFPS 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_productionorderitemno <> m.MSEG1_AUFPS;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_GoodsMoveReason = m.MSEG1_GRUND 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_GoodsMoveReason <> m.MSEG1_GRUND;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_BatchNumber = ifnull(m.MSEG1_CHARG, 'Not Set')
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dd_BatchNumber <>  ifnull(m.MSEG1_CHARG, 'Not Set');

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dd_ValuationType = m.MSEG1_BWTAR 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  dd_ValuationType <> m.MSEG1_BWTAR;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_LocalCurrAmt = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_StdUnitPrice = m.MSEG1_DMBTR / (case when m.MSEG1_ERFMG=0 then null else m.MSEG1_ERFMG end)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_DeliveryCost = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BNBTR 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_AltPriceControl = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BUALT 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_OnHand_ValStock = m.MSEG1_SALK3
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND amt_OnHand_ValStock <>  m.MSEG1_SALK3;

 UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_ExchangeRate =1.00
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  MSEG1_WAERS = dc.Currency ;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_ExchangeRate = (select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = MSEG1_WAERS and z.pToCurrency = dc.Currency and z.pDate = MKPF_BUDAT)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  MSEG1_WAERS <> dc.Currency ;

  UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_ExchangeRate_GBL = 1.00
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dc.Currency = ( select pGlobalCurrency from pGlobalCurrency_tbl);

      UPDATE fact_materialmovement ms
FROM  pGlobalCurrency_tbl,MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_ExchangeRate_GBL = (select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.Currency and z.pToCurrency = pGlobalCurrency and z.pDate = MKPF_BUDAT)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dc.Currency <>   pGlobalCurrency; 

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     ct_Quantity = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_MENGE 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     ct_QtyEntryUOM = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_ERFMG 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     ct_QtyGROrdUnit = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BSTMG 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     ct_QtyOnHand_ValStock = m.MSEG1_LBKUM 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  ct_QtyOnHand_ValStock <>  m.MSEG1_LBKUM;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     ct_QtyOrdPriceUnit = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BPMNG 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_DateIDMaterialDocDate =1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND m.MKPF_BLDAT is null;


      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_date mdd
  SET     dim_DateIDMaterialDocDate = mdd.Dim_Dateid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND mdd.CompanyCode = m.MSEG1_BUKRS AND m.MKPF_BLDAT = mdd.DateValue
	AND  dim_DateIDMaterialDocDate  <> mdd.Dim_Dateid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_DateIDPostingDate = 1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND m.MKPF_BUDAT is null;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_date pd
  SET     dim_DateIDPostingDate = pd.Dim_Dateid 
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND pd.DateValue = m.MKPF_BUDAT AND pd.CompanyCode = m.MSEG1_BUKRS
	AND  dim_DateIDPostingDate <> pd.Dim_Dateid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_MovementIndicatorid = 1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG1_KZBEW is null;


      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_movementindicator dmi
  SET    ms.dim_MovementIndicatorid = dmi.dim_MovementIndicatorid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND  dmi.TypeCode = m.MSEG1_KZBEW
	AND  ms.dim_MovementIndicatorid <> dmi.dim_MovementIndicatorid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_specialstockid = 1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND MSEG1_SOBKZ is null;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_specialstock spt
  SET     ms.dim_specialstockid = spt.dim_specialstockid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND spt.specialstockindicator = MSEG1_SOBKZ
	AND  ms.dim_specialstockid <> spt.dim_specialstockid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_unitofmeasureid =1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND MSEG1_MEINS is null;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_unitofmeasure uom
  SET     ms.dim_unitofmeasureid = uom.Dim_UnitOfMeasureid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND  uom.UOM = MSEG1_MEINS
	AND  ms.dim_unitofmeasureid <> uom.Dim_UnitOfMeasureid;

   UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_controllingareaid = 1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND MSEG1_KOKRS is null;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_controllingarea ca
  SET     ms.dim_controllingareaid = ca.dim_controllingareaid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND  ca.ControllingAreaCode = MSEG1_KOKRS
	AND ms.dim_controllingareaid <> ca.dim_controllingareaid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_profitcenterid = ifnull((SELECT pc.Dim_ProfitCenterid
                    FROM dim_profitcenter_789 pc
                   WHERE  pc.ControllingArea = MSEG1_KOKRS
                      AND pc.ProfitCenterCode = MSEG1_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT),1)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_consumptiontypeid = 1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND  MSEG1_KZVBR is null;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_consumptiontype ct
  SET     ms.dim_consumptiontypeid = ct.Dim_ConsumptionTypeid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND  ct.ConsumptionCode = MSEG1_KZVBR
	AND  ms.dim_consumptiontypeid <>  ct.Dim_ConsumptionTypeid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_stocktypeid = 1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND MSEG1_INSMK is null;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_stocktype st
  SET     ms.dim_stocktypeid = st.Dim_StockTypeid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND  st.TypeCode = MSEG1_INSMK
	AND ms.dim_stocktypeid <> st.Dim_StockTypeid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_PurchaseGroupid =1
 WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG1_MATNR IS NULL;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_PurchaseGroupid = ifnull((SELECT pg.Dim_PurchaseGroupid
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG1_MATNR
                                      AND dpr.Plant = m.MSEG1_WERKS), 1)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG1_MATNR IS NOT NULL;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_materialgroupid = ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup
                        AND dpr.PartNumber = m.MSEG1_MATNR
                        AND dpr.Plant = m.MSEG1_WERKS), 1)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

     UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_receivingplantid =1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND MSEG1_UMWRK is null;


      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	 dim_plant pl
  SET     Dim_receivingplantid = pl.dim_plantid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND pl.PlantCode = MSEG1_UMWRK
	AND  Dim_receivingplantid <> pl.dim_plantid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_receivingpartid = 1
 WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_part prt
  SET     Dim_receivingpartid = prt.Dim_Partid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND prt.Plant = MSEG1_UMWRK AND prt.PartNumber = MSEG1_UMMAT
	AND  Dim_receivingpartid  <>  prt.Dim_Partid;

 UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_RecvIssuStorLocid = 1
  WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG1_UMLGO IS NULL;

UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_RecvIssuStorLocid =1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG1_UMLGO IS NOT NULL;

UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	dim_storagelocation dsl
  SET     ms.Dim_RecvIssuStorLocid = dsl.Dim_StorageLocationid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG1_UMLGO IS NOT NULL
AND  dsl.LocationCode = m.MSEG1_UMLGO  AND dsl.Plant = m.MSEG1_UMWRK
AND  ms.Dim_RecvIssuStorLocid <> dsl.Dim_StorageLocationid;

  UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_StorageLocationid = 1
   WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG1_LGORT IS NULL;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_StorageLocationid =1
   WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_LGORT IS NOT NULL;


      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	 dim_storagelocation dsl
  SET     ms.Dim_StorageLocationid =dsl.Dim_StorageLocationid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_LGORT IS NOT NULL
	AND  dsl.LocationCode = m.MSEG1_LGORT  AND dsl.Plant = m.MSEG1_WERKS
AND  ms.Dim_StorageLocationid <> dsl.Dim_StorageLocationid;

     UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_Currencyid = 1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG1_WAERS is null;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	 dim_currency dcr
  SET     ms.Dim_Currencyid = dcr.Dim_Currencyid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND dcr.CurrencyCode = m.MSEG1_WAERS
	AND   ms.Dim_Currencyid <> dcr.Dim_Currencyid;

 UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_Partid = 1
  WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	 dim_part dpr
  SET     ms.Dim_Partid = dpr.Dim_Partid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND dpr.PartNumber = m.MSEG1_MATNR AND dpr.Plant = m.MSEG1_WERKS
	AND  ms.Dim_Partid  <> dpr.Dim_Partid;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     Dim_Vendorid = 1
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG1_LIFNR is null;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	 dim_vendor dv
  SET     ms.Dim_Vendorid = dv.Dim_Vendorid
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND dv.VendorNumber = m.MSEG1_LIFNR
	AND  ms.Dim_Vendorid <>  dv.Dim_Vendorid;


      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_producthierarchyid = ifnull((SELECT dim_producthierarchyid
                                            FROM dim_producthierarchy dph, dim_part dpr
                                            WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                                  AND dpr.PartNumber = m.MSEG1_MATNR
                                                  AND dpr.Plant = m.MSEG1_WERKS), 1)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;          

   UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET   Dim_AfsSizeId = 1
 WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
	 dim_afssize da
  SET   ms.Dim_AfsSizeId = da.dim_AfsSizeId
 WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
	AND da.Size = MSEG1_J_3ASIZ AND da.RowIsCurrent = 1
	AND  ms.Dim_AfsSizeId = da.dim_AfsSizeId;


INSERT INTO processinglog (processinglogid, referencename, enddate, description)
 VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');

drop table if exists fact_materialmovement_789;
create table fact_materialmovement_789 as select * from fact_materialmovement where 1=2;
Drop table if exists max_holder_789;
create table max_holder_789(maxid)
as
Select ifnull(max(fact_materialmovementid),0)
from fact_materialmovement;

drop table if exists fact_materialmovement_ppi_789;

create table fact_materialmovement_ppi_789 as
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
from fact_materialmovement;


  INSERT INTO fact_materialmovement_789(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,                   
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			       Dim_AfsSizeId)
   select max_holder_789.maxid + row_number() over(), bb.* from max_holder_789,
   (SELECT DISTINCT
          m.MSEG_MBLNR dd_MaterialDocNo,
          m.MSEG_ZEILE dd_MaterialDocItemNo,
          m.MSEG_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG_AUFNR,'Not Set') dd_productionordernumber, 
          m.MSEG_AUFPS dd_productionorderitemno,
          m.MSEG_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG_CHARG, 'Not Set') dd_BatchNumber,
          m.MSEG_BWTAR dd_ValuationType,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR amt_LocalCurrAmt,
          m.MSEG_DMBTR / (case when m.MSEG_ERFMG =0 then null else  m.MSEG_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT amt_AltPriceControl,
          m.MSEG_SALK3 amt_OnHand_ValStock,
	  (select CASE WHEN MSEG_WAERS = dc.Currency THEN 1.00 ELSE  z.exchangeRate end from tmp_getExchangeRate1 z where z.pFromCurrency  = MSEG_WAERS and z.pToCurrency = dc.Currency and z.pDate = MKPF_BUDAT) amt_ExchangeRate,
	  ( Select CASE WHEN dc.Currency = pGlobalCurrency THEN 1.00 ELSE z.exchangeRate end from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.Currency and z.pToCurrency = pGlobalCurrency and z.pDate = MKPF_BUDAT) amt_ExchangeRate_GBL,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE ct_Quantity,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG ct_QtyGROrdUnit,
          m.MSEG_LBKUM ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG ct_QtyOrdPriceUnit,
          ifnull((SELECT mt.Dim_MovementTypeid
                    FROM dim_movementtype mt
                   WHERE mt.MovementType = m.MSEG_BWART
                        AND mt.ConsumptionIndicator = ifnull(m.MSEG_KZVBR, 'Not Set')
                        AND mt.MovementIndicator = ifnull(m.MSEG_KZBEW, 'Not Set')
                        AND mt.ReceiptIndicator = ifnull(m.MSEG_KZZUG, 'Not Set')
                        AND mt.SpecialStockIndicator = ifnull(m.MSEG_SOBKZ, 'Not Set')),1) Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = m.MSEG_WAERS),1) Dim_Currencyid,
	ifnull(( Select (case when  m.MSEG_MATNR IS NULL then 1 else Dim_Partid end) FROM dim_part dpr  WHERE     dpr.PartNumber = m.MSEG_MATNR  AND dpr.Plant = m.MSEG_WERKS),1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
	  ifnull((select (case  WHEN m.MSEG_LGORT IS NULL then 1 else Dim_StorageLocationid end)
                          FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG_LGORT
                                AND dsl.Plant = m.MSEG_WERKS),1) Dim_StorageLocationid,
          ifnull((SELECT dv.Dim_Vendorid
                    FROM dim_vendor dv
                   WHERE dv.VendorNumber = m.MSEG_LIFNR),1) Dim_Vendorid,
          ifnull((SELECT mdd.Dim_Dateid
                    FROM dim_date mdd
                   WHERE mdd.CompanyCode = m.MSEG_BUKRS AND m.MKPF_BLDAT = mdd.DateValue),1) dim_MaterialDocDate,
          ifnull((SELECT pd.Dim_Dateid 
                    FROM dim_date pd
                   WHERE pd.DateValue = m.MKPF_BUDAT AND pd.CompanyCode = m.MSEG_BUKRS),1) dim_PostingDate,
	  ifnull((select (case WHEN m.MSEG_MATNR IS NULL then 1 else dim_producthierarchyid end)
                          FROM dim_producthierarchy dph, dim_part dpr
                          WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                AND dpr.PartNumber = m.MSEG_MATNR
                                AND dpr.Plant = m.MSEG_WERKS),1) dim_producthierarchyid,
          ifnull((SELECT dmi.dim_MovementIndicatorid
                    FROM dim_movementindicator dmi
                   WHERE dmi.TypeCode = m.MSEG_KZBEW),1) dim_MovementIndicatorid,
          ifnull(( SELECT dcu.dim_Customerid
                     FROM dim_customer dcu
                    WHERE dcu.CustomerNumber = m.MSEG_KUNNR),1) dim_Customerid,
		1 dim_CostCenterid,
          ifnull((SELECT spt.dim_specialstockid
                    FROM dim_specialstock spt
                  WHERE spt.specialstockindicator = MSEG_SOBKZ), 1) dim_specialstockid,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = MSEG_MEINS), 1) dim_unitofmeasureid,
          ifnull((SELECT ca.dim_controllingareaid
                    FROM dim_controllingarea ca
                   WHERE ca.ControllingAreaCode = MSEG_KOKRS),1) dim_controllingareaid,
                   1 Dim_ProfitCenterId,
          ifnull((SELECT ct.Dim_ConsumptionTypeid
                    FROM dim_consumptiontype ct
                   WHERE ct.ConsumptionCode = MSEG_KZVBR), 1) dim_consumptiontypeid,
          ifnull((SELECT st.Dim_StockTypeid
                    FROM dim_stocktype st
                   WHERE st.TypeCode = MSEG_INSMK), 1) dim_stocktypeid,
	ifnull(( Select (case when  m.MSEG_MATNR IS NULL then 1 else  pg.Dim_PurchaseGroupid end )
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG_MATNR
                                      AND dpr.Plant = m.MSEG_WERKS), 1) Dim_PurchaseGroupid,
          ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup
                        AND dpr.PartNumber = m.MSEG_MATNR
                        AND dpr.Plant = m.MSEG_WERKS), 1) Dim_materialgroupid,
          ifnull((SELECT pl.dim_plantid
                    FROM dim_plant pl
                   WHERE pl.PlantCode = MSEG_UMWRK), 1) Dim_ReceivingPlantId,
          ifnull((SELECT prt.Dim_Partid
                    FROM dim_part prt
                   WHERE prt.Plant = MSEG_UMWRK AND prt.PartNumber = MSEG_UMMAT), 1) Dim_ReceivingPartId,
	ifnull((select (case when  m.MSEG_UMLGO IS NULL then 1 else Dim_StorageLocationid end)
		FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG_UMLGO
                                AND dsl.Plant = m.MSEG_UMWRK), 1) Dim_RecvIssuStorLocid,
	ifnull((SELECT dim_AfsSizeId
                        FROM dim_afssize
                        WHERE Size = MSEG_J_3ASIZ AND RowIsCurrent = 1),1) Dim_AfsSizeId
     FROM pGlobalCurrency_tbl, MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)) bb;

drop table if exists max_holder_789;

Update fact_materialmovement_789 fmt
From pGlobalCurrency_tbl, MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
set Dim_ProfitCenterId =  ifnull((SELECT pc.Dim_ProfitCenterid
		    FROM dim_profitcenter_789 pc
		   WHERE  pc.ControllingArea = MSEG_KOKRS
		      AND pc.ProfitCenterCode = MSEG_PRCTR
		      AND pc.ValidTo >= m.MKPF_BUDAT
		   ),1) 
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)
AND fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR;


Update fact_materialmovement_789 fmt
From pGlobalCurrency_tbl, MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
set dim_CostCenterid =   ifnull((select (case  WHEN m.MSEG_KOSTL IS NULL then 1 else dim_CostCenterid end)
                          FROM dim_costcenter dcc
                          WHERE     dcc.Code = m.MSEG_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG_KOKRS = dcc.ControllingArea), 1)
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)
AND fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR;


drop table if exists fact_materialmovement_ppi_789;

  INSERT INTO fact_materialmovement(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			       Dim_AfsSizeId)
Select fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			       Dim_AfsSizeId
from fact_materialmovement_789;

drop table if exists fact_materialmovement_789;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');

drop table if exists fact_materialmovement_789;
create table fact_materialmovement_789 as select * from fact_materialmovement where 1=2;
Drop table if exists max_holder_789;
create table max_holder_789(maxid)
as
Select ifnull(max(fact_materialmovementid),0)
from fact_materialmovement;

create table fact_materialmovement_ppi_789 as
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
From fact_materialmovement;

  INSERT INTO fact_materialmovement_789(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			       Dim_AfsSizeId)
   Select max_holder_789.maxid + row_number() over(), bb.* from max_holder_789,(SELECT DISTINCT
          m.MSEG1_MBLNR dd_MaterialDocNo,
          m.MSEG1_ZEILE dd_MaterialDocItemNo,
          m.MSEG1_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG1_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG1_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG1_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG1_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG1_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG1_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG1_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG1_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG1_AUFNR,'Not Set') dd_productionordernumber, 
          m.MSEG1_AUFPS dd_productionorderitemno,
          m.MSEG1_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG1_CHARG, 'Not Set') dd_BatchNumber,
          m.MSEG1_BWTAR dd_ValuationType,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR amt_LocalCurrAmt,
          m.MSEG1_DMBTR / (case when m.MSEG1_ERFMG =0 then null else  m.MSEG1_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BUALT amt_AltPriceControl,
          m.MSEG1_SALK3 amt_OnHand_ValStock,
	(Select  CASE WHEN MSEG1_WAERS = dc.Currency THEN 1.00 ELSE z.exchangeRate end from tmp_getExchangeRate1 z where z.pFromCurrency  = MSEG1_WAERS and z.pToCurrency = dc.Currency and z.pDate = MKPF_BUDAT ) amt_ExchangeRate,
	(Select  CASE WHEN dc.Currency = pGlobalCurrency THEN 1.00 ELSE z.exchangeRate end from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.Currency and z.pToCurrency = pGlobalCurrency and z.pDate = MKPF_BUDAT) amt_ExchangeRate_GBL,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_MENGE ct_Quantity,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BSTMG ct_QtyGROrdUnit,
          m.MSEG1_LBKUM ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BPMNG ct_QtyOrdPriceUnit,
          ifnull((SELECT mt.Dim_MovementTypeid
                    FROM dim_movementtype mt
                   WHERE mt.MovementType = m.MSEG1_BWART
                        AND mt.ConsumptionIndicator = ifnull(m.MSEG1_KZVBR, 'Not Set')
                        AND mt.MovementIndicator = ifnull(m.MSEG1_KZBEW, 'Not Set')
                        AND mt.ReceiptIndicator = ifnull(m.MSEG1_KZZUG, 'Not Set')
                        AND mt.SpecialStockIndicator = ifnull(m.MSEG1_SOBKZ, 'Not Set')),1) Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = m.MSEG1_WAERS),1) Dim_Currencyid,
          ifnull((SELECT Dim_Partid
                          FROM dim_part dpr
                          WHERE     dpr.PartNumber = m.MSEG1_MATNR
                                AND dpr.Plant = m.MSEG1_WERKS), 1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
          ifnull((SELECT Dim_StorageLocationid
                          FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG1_LGORT
                                AND dsl.Plant = m.MSEG1_WERKS), 1) Dim_StorageLocationid,
          ifnull((SELECT dv.Dim_Vendorid
                    FROM dim_vendor dv
                   WHERE dv.VendorNumber = m.MSEG1_LIFNR),1) Dim_Vendorid,
          ifnull((SELECT mdd.Dim_Dateid
                    FROM dim_date mdd
                   WHERE mdd.CompanyCode = m.MSEG1_BUKRS AND MKPF_BLDAT = mdd.DateValue),1) dim_MaterialDocDate,
          ifnull((SELECT pd.Dim_Dateid 
                    FROM dim_date pd
                   WHERE pd.DateValue = MKPF_BUDAT AND pd.CompanyCode = m.MSEG1_BUKRS),1) dim_PostingDate,
          ifnull((SELECT dim_producthierarchyid
                          FROM dim_producthierarchy dph, dim_part dpr
                          WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                AND dpr.PartNumber = m.MSEG1_MATNR
                                AND dpr.Plant = m.MSEG1_WERKS), 1) dim_producthierarchyid,
          ifnull((SELECT dmi.dim_MovementIndicatorid
                    FROM dim_movementindicator dmi
                   WHERE dmi.TypeCode = m.MSEG1_KZBEW),1) dim_MovementIndicatorid,
          ifnull(( SELECT dcu.dim_Customerid
                     FROM dim_customer dcu
                    WHERE dcu.CustomerNumber = m.MSEG1_KUNNR ),1) dim_Customerid,
                   1 dim_CostCenterid,
          ifnull((SELECT spt.dim_specialstockid
                    FROM dim_specialstock spt
                  WHERE spt.specialstockindicator = MSEG1_SOBKZ), 1) dim_specialstockid,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = MSEG1_MEINS), 1) dim_unitofmeasureid,
          ifnull((SELECT ca.dim_controllingareaid
                    FROM dim_controllingarea ca
                   WHERE ca.ControllingAreaCode = MSEG1_KOKRS),1) dim_controllingareaid,
                   1 dim_profitcenterid,
          ifnull((SELECT ct.Dim_ConsumptionTypeid
                    FROM dim_consumptiontype ct
                   WHERE ct.ConsumptionCode = MSEG1_KZVBR), 1) dim_consumptiontypeid,
          ifnull((SELECT st.Dim_StockTypeid
                    FROM dim_stocktype st
                   WHERE st.TypeCode = MSEG1_INSMK), 1) dim_stocktypeid,
	 ifnull(( Select (case when  m.MSEG1_MATNR IS NULL then 1 else  pg.Dim_PurchaseGroupid end )
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG1_MATNR
                                      AND dpr.Plant = m.MSEG1_WERKS), 1) Dim_PurchaseGroupid,
          ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup
                        AND dpr.PartNumber = m.MSEG1_MATNR
                        AND dpr.Plant = m.MSEG1_WERKS), 1) Dim_materialgroupid,
          ifnull((SELECT pl.dim_plantid
                    FROM dim_plant pl
                   WHERE pl.PlantCode = MSEG1_UMWRK), 1) Dim_ReceivingPlantId,
          ifnull((SELECT prt.Dim_Partid
                    FROM dim_part prt
                   WHERE prt.Plant = MSEG1_UMWRK AND prt.PartNumber = MSEG1_UMMAT), 1) Dim_ReceivingPartId,
	 ifnull((select (case when  m.MSEG1_UMLGO IS NULL then 1 else Dim_StorageLocationid end)
                FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG1_UMLGO
                                AND dsl.Plant = m.MSEG1_UMWRK), 1) Dim_RecvIssuStorLocid,
		ifnull((SELECT dim_AfsSizeId
                        FROM dim_afssize
                        WHERE Size = MSEG1_J_3ASIZ AND RowIsCurrent = 1),1) Dim_AfsSizeId                  
     FROM pGlobalCurrency_tbl, MKPF_MSEG1 m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear)) bb;

drop table if exists max_holder_789;

Update fact_materialmovement_789 fmt
  FROM pGlobalCurrency_tbl, MKPF_MSEG1 m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
set Dim_ProfitCenterId =  ifnull((SELECT pc.Dim_ProfitCenterid
                    FROM dim_profitcenter_789 pc
                   WHERE  pc.ControllingArea = MSEG1_KOKRS
                      AND pc.ProfitCenterCode = MSEG1_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT
                   ),1)
 WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear)
AND  fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear;

Update fact_materialmovement_789 fmt
  FROM pGlobalCurrency_tbl, MKPF_MSEG1 m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
set dim_CostCenterid =  ifnull((select (case  WHEN m.MSEG1_KOSTL IS NULL then 1 else dim_CostCenterid end)
                          FROM dim_costcenter dcc
                          WHERE     dcc.Code = m.MSEG1_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG1_KOKRS = dcc.ControllingArea), 1)
 WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear)
AND  fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear;

drop table if exists fact_materialmovement_ppi_789;

  INSERT INTO fact_materialmovement(fact_materialmovementid,
  				dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
				 dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
				Dim_AfsSizeId)
  Select fact_materialmovementid,
   				dd_MaterialDocNo,
                                dd_MaterialDocItemNo,
                                dd_MaterialDocYear,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dd_SalesOrderNo,
                                dd_SalesOrderItemNo,
                                dd_SalesOrderDlvrNo,
                                dd_GLAccountNo,
                                 dd_ReferenceDocNo,
                                 dd_ReferenceDocItem,
                                dd_debitcreditid,
                                dd_productionordernumber,
                                dd_productionorderitemno,
                                dd_GoodsMoveReason,
                                dd_BatchNumber,
                                dd_ValuationType,
                                amt_LocalCurrAmt,
                                amt_StdUnitPrice,
                                amt_DeliveryCost,
                                amt_AltPriceControl,
                                amt_OnHand_ValStock,
                                amt_ExchangeRate,
                                amt_ExchangeRate_GBL,
                                ct_Quantity,
                                ct_QtyEntryUOM,
                                ct_QtyGROrdUnit,
                                ct_QtyOnHand_ValStock,
                                ct_QtyOrdPriceUnit,
                                Dim_MovementTypeid,
                                dim_Companyid,
                                Dim_Currencyid,
                                Dim_Partid,
                                Dim_Plantid,
                                Dim_StorageLocationid,
                                Dim_Vendorid,
                                dim_DateIDMaterialDocDate,
                                dim_DateIDPostingDate,
                                dim_producthierarchyid,
                                dim_MovementIndicatorid,
                                dim_Customerid,
 				 dim_CostCenterid,
                                dim_specialstockid,
                                dim_unitofmeasureid,
                                dim_controllingareaid,
                                dim_profitcenterid,
                                dim_consumptiontypeid,
                                dim_stocktypeid,
                                Dim_PurchaseGroupid,
                                dim_materialgroupid,
                                Dim_ReceivingPlantId,
                                Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
				Dim_AfsSizeId
from fact_materialmovement_789;

drop table if exists fact_materialmovement_789;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');                         

call vectorwise (combine 'fact_materialmovement');

UPDATE fact_materialmovement mm 
 FROM dim_movementtype mt
SET  mm.dd_VendorSpendFlag = 1
WHERE mm.dim_movementtypeid = mt.dim_movementtypeid
AND ( mt.movementtype IN (101,102,105,106,122,123,161,162,501,502,521,522) OR mm.dd_ConsignmentFlag = 1)
AND mm.dd_VendorSpendFlag <> 1;

UPDATE fact_materialmovement mm
SET mm.amt_VendorSpend = ( CASE WHEN mm.amt_AltPriceControl = 0 THEN mm.amt_LocalCurrAmt ELSE mm.amt_AltPriceControl END) + amt_DeliveryCost
WHERE mm.dd_VendorSpendFlag = 1;

call vectorwise (combine 'fact_materialmovement');

        
     INSERT INTO processinglog (processinglogid, referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms,fact_purchase po');
     
  UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_purchasegroupid = po.dim_purchasegroupid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  po.dim_purchasegroupid <> 1
	AND ms.dim_purchasegroupid <> po.dim_purchasegroupid;

 

  UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET        ms.dim_purchaseorgid = po.dim_purchaseorgid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_purchaseorgid <> po.dim_purchaseorgid;

     UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET      ms.dim_itemcategoryid = po.dim_itemcategoryid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_itemcategoryid <> po.dim_itemcategoryid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_Termid = po.dim_Termid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_Termid <> po.dim_Termid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_documenttypeid = po.dim_documenttypeid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_documenttypeid <> po.dim_documenttypeid;


          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_accountcategoryid = po.dim_accountcategoryid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_accountcategoryid <>  po.dim_accountcategoryid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_itemstatusid = po.dim_itemstatusid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.dim_itemstatusid <> po.dim_itemstatusid;


          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateIDDocCreation = po.Dim_DateidCreate
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDDocCreation <> po.Dim_DateidCreate;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateIDOrdered = po.Dim_DateidOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDOrdered <> po.Dim_DateidOrder;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_MaterialGroupid = po.Dim_MaterialGroupid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_MaterialGroupid <>  po.Dim_MaterialGroupid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.amt_POUnitPrice = po.amt_UnitPrice
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.amt_POUnitPrice <>  po.amt_UnitPrice;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.amt_StdUnitPrice = po.amt_StdUnitPrice
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.amt_StdUnitPrice <> po.amt_StdUnitPrice;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateidCosting = po.Dim_DateidCosting
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateidCosting <> po.Dim_DateidCosting;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_IncoTermid = po.Dim_IncoTerm1id
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_IncoTermid <> po.Dim_IncoTerm1id;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dd_incoterms2 = po.dd_incoterms2
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dd_incoterms2 <>  po.dd_incoterms2;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dd_IntOrder = po.dd_IntOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND ms.dd_IntOrder <>  po.dd_IntOrder;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dd_IntOrder = po.dd_IntOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
        AND ms.dd_IntOrder is null
	AND po.dd_IntOrder is not null;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_POPlantidOrdering = po.Dim_PlantidOrdering
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POPlantidOrdering <> po.Dim_PlantidOrdering;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_POPlantidSupplying = po.Dim_PlantidSupplying
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POPlantidSupplying <> po.Dim_PlantidSupplying;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_POStorageLocid = po.Dim_StorageLocationid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POStorageLocid <>  po.Dim_StorageLocationid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_POIssuStorageLocid = po.Dim_IssuStorageLocid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_POIssuStorageLocid <>  po.Dim_IssuStorageLocid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_ReceivingPlantId = (case when ms.Dim_ReceivingPlantId = 1 and ms.Dim_Plantid <> po.Dim_PlantidOrdering 
                                              and (ms.Dim_Plantid = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId
                                   end)
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND ms.Dim_ReceivingPlantId <> (case when ms.Dim_ReceivingPlantId = 1 and ms.Dim_Plantid <> po.Dim_PlantidOrdering 
                                              and (ms.Dim_Plantid = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId
                                   end);

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_RecvIssuStorLocid = (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid
                                   end)
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND ms.Dim_RecvIssuStorLocid <> (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid
                                   end);
        
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms,fact_purchase po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement m,fact_salesorder so');
  
  UPDATE fact_materialmovement ms
	FROM fact_salesorder so
    SET ms.dim_salesorderheaderstatusid = so.dim_salesorderheaderstatusid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderheaderstatusid <> so.dim_salesorderheaderstatusid;

  UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET         ms.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderitemstatusid <>  so.dim_salesorderitemstatusid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_salesdocumenttypeid = so.dim_salesdocumenttypeid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesdocumenttypeid <> so.dim_salesdocumenttypeid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_salesorderrejectreasonid = so.dim_salesorderrejectreasonid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderrejectreasonid <> so.dim_salesorderrejectreasonid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_salesgroupid = so.dim_salesgroupid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesgroupid <>  so.dim_salesgroupid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_salesorgid = so.dim_salesorgid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorgid <> so.dim_salesorgid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_customergroup1id = so.dim_customergroup1id
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_customergroup1id <>  so.dim_customergroup1id;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_documentcategoryid = so.dim_documentcategoryid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_documentcategoryid <> so.dim_documentcategoryid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.Dim_DateIDDocCreation = so.Dim_DateidSalesOrderCreated
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDDocCreation <>  so.Dim_DateidSalesOrderCreated;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.Dim_DateIDOrdered = so.Dim_DateidSalesOrderCreated
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDOrdered <> so.Dim_DateidSalesOrderCreated;
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement m,fact_salesorder so');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_productionorder po');

  UPDATE fact_materialmovement ms
FROM fact_productionorder po
    SET ms.dim_productionorderstatusid = po.dim_productionorderstatusid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionorderstatusid <>  po.dim_productionorderstatusid;

UPDATE fact_materialmovement ms
FROM fact_productionorder po
    SET  ms.dim_productionordertypeid = po.Dim_ordertypeid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionordertypeid <> po.Dim_ordertypeid;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_productionorder po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

  UPDATE fact_materialmovement ms
FROM fact_inspectionlot fil
    SET ms.dim_inspusagedecisionid = fil.dim_inspusagedecisionid
  WHERE     ms.dd_MaterialDocItemNo = fil.dd_MaterialDocItemNo
        AND ms.dd_MaterialDocNo = fil.dd_MaterialDocNo
        AND ms.dd_MaterialDocYear = fil.dd_MaterialDocYear
AND  ms.dim_inspusagedecisionid <> fil.dim_inspusagedecisionid;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'bi_populate_materialmovement_fact END');

drop table if exists bwtarupd_tmp_100;

create table bwtarupd_tmp_100 as
Select  x.MATNR, x.BWKEY, max((x.LFGJA * 100) + x.LFMON) updcol
from mbew_no_bwtar x
Group by  x.MATNR, x.BWKEY;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement;

UPDATE fact_materialmovement ia
FROM  mbew_no_bwtar m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt
SET amt_PlannedPrice1 = ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0)
 WHERE ia.dim_partid = prt.Dim_partid
  and ia.dim_plantid = pl.dim_plantid
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and amt_PlannedPrice1 <> ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0);

UPDATE fact_materialmovement ia
FROM  mbew_no_bwtar m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt
SET amt_PlannedPrice = ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0)
 WHERE ia.dim_partid = prt.Dim_partid
  and ia.dim_plantid = pl.dim_plantid
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and  amt_PlannedPrice <> ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0);
  
/*call bi_purchase_matmovement_dlvr_link()*/
drop table if exists dim_profitcenter_789;
drop table if exists bwtarupd_tmp_100;
