

/* Start of vw_bi_populate_bom_level script */


/* Q1 : Update dd_rootpart to 'Not Set' and dd_Level to 0 for ALL ROWS in fact_bob */
UPDATE fact_bom
SET dd_rootpart = 'Not Set', dd_Level = 0;

/* Q2 : */
DELETE FROM fact_bom_explosion_tmp1;


/* Q3 */
INSERT INTO fact_bom_explosion_tmp1(dd_ComponentNumber,dd_Alternative,Dim_BomUsageId,dim_bomcategoryid)
SELECT DISTINCT f.dd_ComponentNumber, f.dd_Alternative, f.Dim_BomUsageId, f.dim_bomcategoryid
FROM fact_bom f INNER JOIN dim_billofmaterialmisc m ON f.Dim_BillOfMaterialMiscid = m.Dim_BillOfMaterialMiscid
WHERE f.dd_PartNumber <> 'Not Set' 
AND f.dd_ComponentNumber <> 'Not Set' 
AND f.dd_PartNumber <> f.dd_ComponentNumber 
AND m.BOMIsRecursive = 'Not Set' 
AND m.RecursivenessAllowed = 'Not Set';


/* Q4 */
UPDATE fact_bom f
SET f.dd_rootpart = f.dd_partnumber
WHERE f.dd_ComponentNumber = 'Not Set'
AND f.dd_PartNumber <> 'Not Set'	
AND NOT EXISTS (SELECT 1  
				FROM fact_bom_explosion_tmp1 f1 
				WHERE f1.dd_Alternative = f.dd_Alternative 
				AND f1.Dim_BomUsageId = f.Dim_BomUsageId 
				AND f1.dd_ComponentNumber = f.dd_PartNumber  
				AND f1.dim_bomcategoryid = f.dim_bomcategoryid)
AND IFNULL(f.dd_rootpart,'XX') <> IFNULL(f.dd_partnumber,'YY');

							  
/* Q5							   */
UPDATE fact_bom f
SET f.dd_rootpart = f.dd_partnumber, 
	dd_Level = 1
WHERE f.dd_ComponentNumber <> 'Not Set' 
AND f.dd_PartNumber <> 'Not Set'
AND NOT EXISTS (SELECT 1 
				FROM fact_bom_explosion_tmp1 f1 
				WHERE f1.dd_Alternative = f.dd_Alternative 
				AND f1.Dim_BomUsageId = f.Dim_BomUsageId 
				AND f1.dd_ComponentNumber = f.dd_PartNumber 
				AND f1.dim_bomcategoryid = f.dim_bomcategoryid)
AND ( ifnull(f.dd_rootpart,'xx') <> ifnull(f.dd_partnumber,'yy') 
	 OR 	dd_Level <> 1	);

	 
/* There are cases where same dd_ComponentNumber ( level 1 ) have different dd_rootpart. 
   Use the minimum value in such cases e.g between CB-2201-1A and CB-2201-1B, select  CB-2201-1A */
/* Q6 */
DROP TABLE IF EXISTS   fact_bom_level_tmp;
CREATE TABLE fact_bom_level_tmp
AS
SELECT  dd_Alternative,Dim_BomUsageId,dim_bomcategoryid,dd_ComponentNumber,dd_Level,min(dd_rootpart) as min_dd_rootpart
FROM fact_bom 
GROUP BY dd_Alternative,Dim_BomUsageId,dim_bomcategoryid,dd_ComponentNumber,dd_Level;
	 
/* Update dd_Level 2. Level 1 was already updated in Q5  */
UPDATE    fact_bom f
FROM fact_bom_level_tmp f1 
SET f.dd_rootpart = f1.min_dd_rootpart, f.dd_Level = f1.dd_Level + 1	
WHERE     f.dd_rootpart = 'Not Set'
AND f.dd_Level = 0
AND f1.dd_Level = 1
AND f.dd_PartNumber <> 'Not Set'
AND f1.dd_Alternative = f.dd_Alternative
AND f1.Dim_BomUsageId = f.Dim_BomUsageId
AND f1.dim_bomcategoryid = f.dim_bomcategoryid
AND f.dd_PartNumber = f1.dd_ComponentNumber;

/* Update dd_Level 3 */
UPDATE    fact_bom f
FROM fact_bom f1 
SET f.dd_rootpart = f1.dd_rootpart, f.dd_Level = f1.dd_Level + 1	
WHERE f.dd_rootpart = 'Not Set'
AND f.dd_Level = 0
AND f1.dd_Level = 2
AND f.dd_PartNumber <> 'Not Set'
AND f1.dd_Alternative = f.dd_Alternative
AND f1.Dim_BomUsageId = f.Dim_BomUsageId
AND f1.dim_bomcategoryid = f.dim_bomcategoryid
AND f.dd_PartNumber = f1.dd_ComponentNumber;

/* Update dd_Level 4 */
UPDATE    fact_bom f
FROM fact_bom f1 
SET f.dd_rootpart = f1.dd_rootpart, f.dd_Level = f1.dd_Level + 1	
WHERE f.dd_rootpart = 'Not Set'
AND f.dd_Level = 0
AND f1.dd_Level = 3
AND f.dd_PartNumber <> 'Not Set'
AND f1.dd_Alternative = f.dd_Alternative
AND f1.Dim_BomUsageId = f.Dim_BomUsageId
AND f1.dim_bomcategoryid = f.dim_bomcategoryid
AND f.dd_PartNumber = f1.dd_ComponentNumber;

/* Update dd_Level 5 */
UPDATE    fact_bom f
FROM fact_bom f1 
SET f.dd_rootpart = f1.dd_rootpart, f.dd_Level = f1.dd_Level + 1	
WHERE f.dd_rootpart = 'Not Set'
AND f.dd_Level = 0
AND f1.dd_Level = 4
AND f.dd_PartNumber <> 'Not Set'
AND f1.dd_Alternative = f.dd_Alternative
AND f1.Dim_BomUsageId = f.Dim_BomUsageId
AND f1.dim_bomcategoryid = f.dim_bomcategoryid
AND f.dd_PartNumber = f1.dd_ComponentNumber;

/* Update dd_Level 6 */
UPDATE    fact_bom f
FROM fact_bom f1 
SET f.dd_rootpart = f1.dd_rootpart, f.dd_Level = f1.dd_Level + 1	
WHERE f.dd_rootpart = 'Not Set'
AND f.dd_Level = 0
AND f1.dd_Level = 5
AND f.dd_PartNumber <> 'Not Set'
AND f1.dd_Alternative = f.dd_Alternative
AND f1.Dim_BomUsageId = f.Dim_BomUsageId
AND f1.dim_bomcategoryid = f.dim_bomcategoryid
AND f.dd_PartNumber = f1.dd_ComponentNumber;
											
											
/* Update dd_Level 7 */											
UPDATE    fact_bom f
FROM fact_bom f1 
SET f.dd_rootpart = f1.dd_rootpart, f.dd_Level = f1.dd_Level + 1	
WHERE f.dd_rootpart = 'Not Set'
AND f.dd_Level = 0
AND f1.dd_Level = 6
AND f.dd_PartNumber <> 'Not Set'
AND f1.dd_Alternative = f.dd_Alternative
AND f1.Dim_BomUsageId = f.Dim_BomUsageId
AND f1.dim_bomcategoryid = f.dim_bomcategoryid
AND f.dd_PartNumber = f1.dd_ComponentNumber;	

/* Update dd_Level 8 : There are a max of 7 levels till now. More such blocks can be added if more levels are required */
/* So, for now, this query will update 0 rows in all schemas */
UPDATE    fact_bom f
FROM fact_bom f1 
SET f.dd_rootpart = f1.dd_rootpart, f.dd_Level = f1.dd_Level + 1	
WHERE f.dd_rootpart = 'Not Set'
AND f.dd_Level = 0
AND f1.dd_Level = 7
AND f.dd_PartNumber <> 'Not Set'
AND f1.dd_Alternative = f.dd_Alternative
AND f1.Dim_BomUsageId = f.Dim_BomUsageId
AND f1.dim_bomcategoryid = f.dim_bomcategoryid
AND f.dd_PartNumber = f1.dd_ComponentNumber;											
 
/* Q7  */
UPDATE fact_bom f
SET dim_RootPartId = Dim_PartId
WHERE dd_RootPart = dd_PartNumber
AND  dd_PartNumber <> 'Not Set'
AND  dim_RootPartId <> Dim_PartId; 


/* Q8  */

UPDATE	 fact_bom f
FROM 	MAST m, dim_bomusage bu
SET	 f.Dim_RootPartId = 1
WHERE	 m.MAST_STLNR = f.dd_BomNumber	 AND m.MAST_STLAL = f.dd_Alternative
AND		 bu.BOMUsageCode = m.MAST_STLAN	 AND bu.RowIsCurrent = 1
AND		 dd_RootPart <> dd_PartNumber 
AND		 dd_RootPart <> 'Not Set'
AND		 f.Dim_RootPartId <> 1;

/* Q8B. This query went on for a long time on bigdatae30 on dev */
/*UPDATE	 fact_bom f
FROM 	 MAST m, dim_bomusage bu,dim_part pt
SET	 f.Dim_RootPartId = pt.Dim_PartId
WHERE	 m.MAST_STLNR = f.dd_BomNumber	 AND m.MAST_STLAL = f.dd_Alternative
AND		 bu.BOMUsageCode = m.MAST_STLAN	 AND bu.RowIsCurrent = 1
AND		 dd_RootPart <> dd_PartNumber 
AND		 dd_RootPart <> 'Not Set'
AND		 pt.PartNumber = f.dd_RootPart	 AND pt.Plant = m.MAST_WERKS AND pt.RowIsCurrent = 1
AND		 f.Dim_RootPartId <> pt.Dim_PartId*/

/* Tuned Q8B */
DROP TABLE IF EXISTS TMP_Q8_fact_bom1;
CREATE TABLE TMP_Q8_fact_bom1
AS
SELECT DISTINCT f.dd_RootPart
FROM fact_bom f, MAST m, dim_bomusage bu
WHERE	 m.MAST_STLNR = f.dd_BomNumber	 AND m.MAST_STLAL = f.dd_Alternative
AND		 bu.BOMUsageCode = m.MAST_STLAN	 AND bu.RowIsCurrent = 1
AND		 dd_RootPart <> dd_PartNumber 
AND		 dd_RootPart <> 'Not Set';

DROP TABLE IF EXISTS TMP_Q8_fact_bom2;
CREATE TABLE TMP_Q8_fact_bom2
AS
SELECT DISTINCT pt.PartNumber,pt.Plant,pt.Dim_PartId
FROM dim_part pt 
WHERE EXISTS ( SELECT 1 FROM TMP_Q8_fact_bom1 f WHERE  pt.PartNumber = f.dd_RootPart)
AND pt.RowIsCurrent = 1;

DROP TABLE IF EXISTS TMP_Q8_fact_bom3_mast;
CREATE TABLE TMP_Q8_fact_bom3_mast
AS
SELECT DISTINCT m.MAST_STLNR,m.MAST_STLAL,m.MAST_STLAN,m.MAST_WERKS 
FROM fact_bom f, MAST m, dim_bomusage bu
WHERE	 m.MAST_STLNR = f.dd_BomNumber	 AND m.MAST_STLAL = f.dd_Alternative
AND		 bu.BOMUsageCode = m.MAST_STLAN	 AND bu.RowIsCurrent = 1
AND		 dd_RootPart <> dd_PartNumber 
AND		 dd_RootPart <> 'Not Set';

UPDATE	 fact_bom f
FROM 	 TMP_Q8_fact_bom3_mast m, dim_bomusage bu,TMP_Q8_fact_bom2 pt
SET	 f.Dim_RootPartId = pt.Dim_PartId
WHERE	 m.MAST_STLNR = f.dd_BomNumber	 AND m.MAST_STLAL = f.dd_Alternative
AND		 bu.BOMUsageCode = m.MAST_STLAN	 AND bu.RowIsCurrent = 1
AND		 dd_RootPart <> dd_PartNumber 
AND		 dd_RootPart <> 'Not Set'
AND		 pt.PartNumber = f.dd_RootPart	 AND pt.Plant = m.MAST_WERKS 
AND		 f.Dim_RootPartId <> pt.Dim_PartId;


/* Q9 - This is not used anywyere. Even in the original mysql proc its the same  */
DELETE FROM fact_bom_explosion_tmp1;

/* Q10  */
DELETE FROM fact_bom_explosion_tmp2;

/* Q11 */
INSERT INTO fact_bom_explosion_tmp2(dd_Alternative,Dim_BomUsageId,dd_RootPart,dim_bomcategoryid,CNT)
SELECT distinct bom1.dd_Alternative,bom1.dim_bomusageid,bom1.dd_RootPart,bom1.dim_bomcategoryid,
	   count( DISTINCT bom1.dd_ComponentNumber) AS CNT 
FROM fact_bom bom1
WHERE    bom1.dd_Level <> 0 and bom1.dd_ComponentNumber <> 'Not Set' 
GROUP BY bom1.dd_Alternative,bom1.dim_bomusageid,bom1.dd_RootPart,bom1.dim_bomcategoryid; 


/* Q12 */

UPDATE	fact_bom bom 
FROM	fact_bom_explosion_tmp2 subq
SET bom.ct_TotalComponent = CNT
WHERE bom.dd_Level <> 0
AND	 subq.dd_Alternative = bom.dd_Alternative
AND subq.dim_bomusageid = bom.Dim_BomUsageId
AND subq.dd_RootPart = bom.dd_RootPart
AND subq.dim_bomcategoryid = bom.dim_bomcategoryid
AND ifnull(bom.ct_TotalComponent,-1) <> ifnull(CNT,-2);

/* Q13 and Q14 are repeated in orig mysql proc. Just use once here */
DELETE FROM fact_bom_explosion_tmp2;


/* Q15 */
INSERT INTO fact_bom_explosion_tmp2(dd_Alternative,dim_bomusageid,dd_PartNumber,dd_LEVEl,dim_bomcategoryid,CNT)
SELECT DISTINCT bom1.dd_Alternative,bom1.dim_bomusageid,bom1.dd_PartNumber,bom1.dd_Level,bom1.dim_bomcategoryid,
	  COUNT(DISTINCT bom1.dd_ComponentNumber) AS CNT 
FROM fact_bom bom1
WHERE bom1.dd_Level <> 0 and bom1.dd_ComponentNumber <> 'Not Set' 
GROUP BY bom1.dd_Alternative,bom1.dim_bomusageid,bom1.dd_PartNumber,bom1.dd_Level,bom1.dim_bomcategoryid;

/* Q16 */
UPDATE fact_bom bom 
FROM fact_bom_explosion_tmp2 subq1
SET bom.ct_TotalChildComp = CNT
WHERE bom.dd_Level <> 0
AND  subq1.dd_Alternative = bom.dd_Alternative
AND subq1.Dim_BomUsageId = bom.Dim_BomUsageId
AND subq1.dd_PartNumber = bom.dd_PartNumber
AND subq1.dd_Level = bom.dd_Level
AND subq1.dim_bomcategoryid = bom.dim_bomcategoryid
AND ifnull(bom.ct_TotalChildComp,-1) <> ifnull(CNT,-2);

/* Q17 - Last query */
DELETE FROM fact_bom_explosion_tmp2;

DROP TABLE IF EXISTS TMP_Q8_fact_bom1;
DROP TABLE IF EXISTS TMP_Q8_fact_bom2;
DROP TABLE IF EXISTS TMP_Q8_fact_bom3_mast;
