
/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 10 May 2013 */
/*   Description    : Stored Proc bi_populate_accountsreceivable_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   10 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/

/* Refreshing relevant tables */

/*
mysqldump --default-character-set=utf8 -uoctet -poctet --skip-triggers --routines --single-transaction columbia275 --tables
bsad bsid but000 but050 cvi_cust_link dim_accountreceivablestatus dim_accountsreceivabledocstatus dim_blockingpaymentreason
dim_businessarea dim_chartofaccounts dim_company dim_creditcontrolarea dim_currency dim_customer dim_customerpaymentterms 
dim_date dim_documenttypetext dim_paymentmethod dim_paymentreason dim_postingkey dim_riskclass dim_specialglindicator dim_specialgltransactiontype
fact_accountsreceivable fact_billing fact_disputemanagement fact_salesorder fdm_dcproc scmg_t_case_attr systemproperty ukmbp_cms ukmbp_cms_sgm
| mysql -uoctet -poctet --host=192.168.200.125  -C columbia99 &

*/

/*********************************************START****************************************************************/

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
select 'processinglog',ifnull(max(processinglogid),0)
FROM processinglog;

DROP TABLE IF EXISTS fact_accountsreceivable_temp;

CREATE TABLE fact_accountsreceivable_temp AS 
select * from fact_accountsreceivable where 1 = 2;

delete from NUMBER_FOUNTAIN where table_name = 'fact_accountsreceivable_temp';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_accountsreceivable_temp',ifnull(max(fact_accountsreceivableid),0)
FROM fact_accountsreceivable_temp;


INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'bi_populate_accountsreceivable_fact START',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


UPDATE BSID SET BSID_ZUONR = ifnull(BSID_ZUONR,'Not Set'), BSID_BUKRS = ifnull(BSID_BUKRS,'Not Set'), BSID_KUNNR = ifnull(BSID_KUNNR,'Not Set');
UPDATE BSAD SET BSAD_ZUONR = ifnull(BSAD_ZUONR,'Not Set'), BSAD_BUKRS = ifnull(BSAD_BUKRS,'Not Set'), BSAD_KUNNR = ifnull(BSAD_KUNNR,'Not Set');

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE  fact_accountsreceivable_temp far',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

/* Update 1 - Part 1 
UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET     ct_CashDiscountDays1 = BSID_ZBD1P,
          amt_CashDiscountDocCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_WSKTO,
          amt_CashDiscountLocalCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_SKNTO,
          amt_InLocalCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_DMBTR,
          amt_TaxInDocCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_WMWST,
          amt_TaxInLocalCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_MWSTS,
          amt_InDocCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WRBTR,
          dd_AccountingDocItemNo = BSID_BUZEI,
          dd_AccountingDocNo = BSID_BELNR,
          dd_AssignmentNumber = ifnull(BSID_ZUONR,'Not Set')
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR


UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET     
          Dim_ClearedFlagId = ifnull((SELECT Dim_AccountReceivableStatusId
                                        FROM Dim_AccountReceivableStatus ars
                                       WHERE ars.Status = CASE WHEN BSID_REBZG IS NOT NULL AND BSID_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END), 1),
          Dim_DateIdAccDocDateEntered = ifnull((SELECT dim_dateid
                                                 FROM dim_date dt
                                                WHERE dt.DateValue = BSID_CPUDT AND dt.CompanyCode = arc.BSID_BUKRS), 1) ,
          Dim_DateIdBaseDateForDueDateCalc = ifnull((SELECT dim_dateid
                                                       FROM dim_date dt
                                                      WHERE dt.DateValue = BSID_ZFBDT AND dt.CompanyCode = arc.BSID_BUKRS), 1) ,
          Dim_DateIdCreated = ifnull((SELECT dim_dateid
                                        FROM dim_date dt
                                       WHERE dt.DateValue = BSID_BLDAT AND  dt.CompanyCode = arc.BSID_BUKRS), 1) ,
          Dim_DateIdPosting = ifnull((SELECT dim_dateid
                                        FROM dim_date dt
                                       WHERE dt.DateValue = BSID_BUDAT AND dt.CompanyCode = arc.BSID_BUKRS), 1)
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR


UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET     
          dd_debitcreditid = (CASE WHEN BSID_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END),
          dd_ClearingDocumentNo = ifnull(BSID_AUGBL,'Not Set') ,
          Dim_AccountsReceivableDocStatusId = ifnull((SELECT Dim_AccountsReceivableDocStatusId
                                                        FROM Dim_AccountsReceivableDocStatus ards
                                                       WHERE ards.Status = BSID_BSTAT), 1) ,
          dd_FixedPaymentTerms = BSID_ZBFIX,
          dd_InvoiceNumberTransBelongTo = ifnull(BSID_REBZG,'Not Set'),
          dd_NetPaymentTermsPeriod = BSID_ZBD3T,
          Dim_BlockingPaymentReasonId = ifnull((SELECT Dim_BlockingPaymentReasonId
                                                  FROM Dim_BlockingPaymentReason bpr
                                                 WHERE bpr.BlockingKeyPayment = BSID_ZLSPR), 1),
          Dim_BusinessAreaId = ifnull((SELECT Dim_BusinessAreaId
                                       FROM Dim_BusinessArea ba
                                       WHERE ba.BusinessArea = BSID_GSBER), 1)
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR

UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET     
          Dim_ChartOfAccountsId = ifnull((SELECT Dim_ChartOfAccountsId
                                            FROM Dim_ChartOfAccounts coa INNER JOIN dim_company dc ON dc.ChartOfAccounts = coa.CharOfAccounts
                                           WHERE dc.CompanyCode = BSID_BUKRS AND coa.GLAccountNumber = BSID_SAKNR), 1) ,
          far.dim_companyid = dcm.Dim_CompanyId,
          Dim_CreditControlAreaId = ifnull((SELECT Dim_CreditControlAreaid
                                              FROM dim_creditcontrolarea cca
                                             WHERE cca.CreditControlAreaCode = BSID_KKBER), 1) ,
          Dim_Currencyid = ifnull((SELECT dim_currencyid
                                     FROM dim_currency c
                                    WHERE c.CurrencyCode = dcm.Currency),1)  ,
          far.Dim_CustomerID = dc.Dim_CustomerID,
          Dim_DateIdClearing = ifnull((SELECT dim_dateid
                                         FROM dim_date dt
                                        WHERE dt.DateValue = BSID_AUGDT AND dt.CompanyCode = arc.BSID_BUKRS), 1) ,
          Dim_DocumentTypeId = ifnull((SELECT dim_documenttypetextid
                                         FROM dim_documenttypetext dtt
                                        WHERE dtt.type = BSID_BLART), 1) 
										
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR

UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET     
										
          Dim_PaymentReasonId = ifnull((SELECT Dim_PaymentReasonId
                                         FROM Dim_PaymentReason pr
                                        WHERE pr.PaymentReasonCode = BSID_RSTGR AND pr.CompanyCode = BSID_BUKRS), 1) ,
          Dim_PostingKeyId = ifnull((SELECT Dim_PostingKeyId
                                        FROM Dim_PostingKey pk
                                        WHERE pk.PostingKey= BSID_BSCHL AND pk.SpecialGLIndicator = ifnull(BSID_UMSKZ,'Not Set')), 1) ,
          Dim_SpecialGLIndicatorId = ifnull((SELECT Dim_SpecialGLIndicatorId
                                                FROM Dim_SpecialGLIndicator sgl
                                                WHERE sgl.SpecialGLIndicator = BSID_UMSKZ AND sgl.AccountType =  'D'), 1) ,
          Dim_TargetSpecialGLIndicatorId = ifnull((SELECT Dim_SpecialGLIndicatorId
                                                    FROM Dim_SpecialGLIndicator sgl
                                                    WHERE sgl.SpecialGLIndicator = BSID_ZUMSK AND sgl.AccountType =  'D'), 1) ,
          dd_SalesDocNo = ifnull(BSID_VBEL2, 'Not Set'),
          Dim_SpecialGlTransactionTypeId = ifnull ((SELECT Dim_SpecialGlTransactionTypeId
                                                      FROM Dim_SpecialGlTransactionType sgt
                                                     WHERE sgt.SpecialGlTransactionTypeId = BSID_UMSKS) , 1) 

										
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR

UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET     

          dd_SalesItemNo = BSID_POSN2,
          dd_SalesScheduleNo = BSID_ETEN2,
          dd_CashDiscountPercentage1 = BSID_ZBD1P,
          dd_BillingNo = ifnull(BSID_VBELN,'Not Set'),
          dd_CashDiscountPercentage2 = BSID_ZBD2P,
          dd_ProductionOrderNo = ifnull(BSID_AUFNR,'Not Set'),
          Dim_PaymentTermsId = ifnull ((SELECT Dim_CustomerPaymentTermsid
                                          FROM Dim_CustomerPaymentTerms pt
                                         WHERE pt.PaymentTermCode = BSID_ZTERM) , 1),
          dd_DunningLevel = BSID_MANST,
          Dim_PaymentMethodId = ifnull ((SELECT Dim_PaymentMethodId
                                          FROM Dim_PaymentMethod pm
                                          WHERE pm.PaymentMethod = BSID_ZLSCH AND pm.Country = dc.Country) , 1)										

										
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR


UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET     
          Dim_RiskClassId = ifnull((select dr.Dim_RiskClassId
                                  from UKMBP_CMS a
                                        inner join BUT000 b on a.PARTNER = b.PARTNER and TIMESTAMP(LOCAL_TIMESTAMP) <= ifnull(a.RATING_VAL_DATE,TIMESTAMP(LOCAL_TIMESTAMP))
                                        inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
                                        inner join dim_riskclass dr on dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1
                                  where c.CUSTOMER = dc.CustomerNumber),1)	
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR

 End of Update 1 - Split into 7 parts */


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


/* Insert 1 */

INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency,
                                    amt_CashDiscountLocalCurrency,
                                    amt_InDocCurrency,
                                    amt_InLocalCurrency,
                                    amt_TaxInDocCurrency,
                                    amt_TaxInLocalCurrency,
                                    ct_CashDiscountDays1,
                                    dd_AccountingDocItemNo,
                                    dd_AccountingDocNo,
                                    dd_AssignmentNumber,
                                    Dim_ClearedFlagId,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdCreated,
                                    Dim_DateIdPosting,
                                    dd_debitcreditid,
                                    dd_ClearingDocumentNo,
                                    dd_FiscalPeriod,
                                    dd_FiscalYear,
                                    dd_FixedPaymentTerms,
                                    dd_InvoiceNumberTransBelongTo,
                                    dd_NetPaymentTermsPeriod,
                                    Dim_BlockingPaymentReasonId,
                                    Dim_BusinessAreaId,
                                    Dim_ChartOfAccountsId,
                                    Dim_CompanyId,
                                    Dim_CreditControlAreaId,
                                    Dim_CurrencyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    Dim_DocumentTypeId,
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    Dim_SpecialGLIndicatorId,
                                    Dim_TargetSpecialGLIndicatorId,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    dd_BillingNo,
                                    dd_CashDiscountPercentage1,
                                    dd_CashDiscountPercentage2,
                                    dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    Dim_PaymentTermsId,
                                    dd_DunningLevel,
                                    Dim_PaymentMethodId,
                                    dim_RiskClassId,
                                    dd_CreditRep,
                                    Dim_DateidSONextDate,fact_accountsreceivableid)
   SELECT (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WSKTO amt_CashDiscountDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_SKNTO amt_CashDiscountLocalCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WRBTR amt_InDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_MWSTS amt_TaxInLocalCurrency,
          BSID_ZBD1P ct_CashDiscountDays1,
          BSID_BUZEI dd_AccountingDocItemNo,
          BSID_BELNR dd_AccountingDocNo,
          ifnull(BSID_ZUONR,'Not Set') dd_AssignmentNumber,
          ifnull((SELECT Dim_AccountReceivableStatusId
             FROM Dim_AccountReceivableStatus ars
             WHERE ars.Status = CASE WHEN BSID_REBZG IS NOT NULL AND BSID_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END), 1) Dim_ClearedFlagId,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSID_CPUDT
             AND dt.CompanyCode = arc.BSID_BUKRS
          ), 1) Dim_DateIdAccDocDateEntered,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSID_ZFBDT
              AND dt.CompanyCode = arc.BSID_BUKRS
          ), 1) Dim_DateIdBaseDateForDueDateCalc,
          ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSID_BLDAT
             AND  dt.CompanyCode = arc.BSID_BUKRS
          ), 1) Dim_DateIdCreated,
          ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSID_BUDAT
              AND dt.CompanyCode = arc.BSID_BUKRS)
          , 1) Dim_DateIdPosting,
          (CASE WHEN BSID_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END) dd_debitcreditid,
          ifnull(BSID_AUGBL,'Not Set') dd_ClearingDocumentNo,
          BSID_MONAT dd_FiscalPeriod,
          BSID_GJAHR dd_FiscalYear,
          BSID_ZBFIX dd_FixedPaymentTerms,
          ifnull(BSID_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
          BSID_ZBD3T dd_NetPaymentTermsPeriod,
          ifnull( ( SELECT Dim_BlockingPaymentReasonId
          FROM Dim_BlockingPaymentReason bpr
          WHERE bpr.BlockingKeyPayment = BSID_ZLSPR), 1) Dim_BlockingPaymentReasonId,
          ifnull( ( SELECT Dim_BusinessAreaId
           FROM Dim_BusinessArea ba
           WHERE ba.BusinessArea = BSID_GSBER), 1) Dim_BusinessAreaId,
          ifnull(( SELECT Dim_ChartOfAccountsId
            FROM Dim_ChartOfAccounts coa
            INNER JOIN dim_company dc ON dc.ChartOfAccounts = coa.CharOfAccounts
            WHERE dc.CompanyCode = BSID_BUKRS
            AND coa.GLAccountNumber = BSID_SAKNR), 1) Dim_ChartOfAccountsId,
          dc.Dim_CompanyId,
          ifnull(( SELECT Dim_CreditControlAreaid
                FROM dim_creditcontrolarea cca
               WHERE cca.CreditControlAreaCode = BSID_KKBER), 1) Dim_CreditControlAreaId,
          ifnull((SELECT dim_currencyid
             FROM dim_currency c
            WHERE c.CurrencyCode = dc.Currency),1)  Dim_Currencyid,
          cm.Dim_CustomerId Dim_CustomerID,
          ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSID_AUGDT
                  AND dt.CompanyCode = arc.BSID_BUKRS), 1) Dim_DateIdClearing,
          ifnull(( SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.type = BSID_BLART), 1) Dim_DocumentTypeId,
          ifnull((SELECT Dim_PaymentReasonId
             FROM Dim_PaymentReason pr
            WHERE pr.PaymentReasonCode = BSID_RSTGR
               AND pr.CompanyCode = BSID_BUKRS), 1) Dim_PaymentReasonId,
          ifnull( ( SELECT Dim_PostingKeyId
            FROM Dim_PostingKey pk
            WHERE pk.PostingKey= BSID_BSCHL
             AND pk.SpecialGLIndicator = ifnull(BSID_UMSKZ,'Not Set')), 1) Dim_PostingKeyId,
          ifnull( ( SELECT Dim_SpecialGLIndicatorId
            FROM Dim_SpecialGLIndicator sgl
            WHERE sgl.SpecialGLIndicator = BSID_UMSKZ
              AND sgl.AccountType =  'D'), 1) Dim_SpecialGLIndicatorId,
          ifnull( ( SELECT Dim_SpecialGLIndicatorId
            FROM Dim_SpecialGLIndicator sgl
            WHERE sgl.SpecialGLIndicator = BSID_ZUMSK
              AND sgl.AccountType =  'D'), 1) Dim_TargetSpecialGLIndicatorId,
          ifnull(BSID_VBEL2, 'Not Set') dd_SalesDocNo,
          BSID_POSN2 dd_SalesItemNo,
          BSID_ETEN2 dd_SalesScheduleNo,
          ifnull(BSID_VBELN,'Not Set') dd_BillingNo,
          BSID_ZBD1P dd_CashDiscountPercentage1,
          BSID_ZBD2P dd_CashDiscountPercentage2,
          ifnull(BSID_AUFNR,'Not Set') dd_ProductionOrderNo,
          ifnull ( ( SELECT Dim_SpecialGlTransactionTypeId
            FROM Dim_SpecialGlTransactionType sgt
            WHERE sgt.SpecialGlTransactionTypeId = BSID_UMSKS) , 1) Dim_SpecialGlTransactionTypeId ,
          ifnull( (SELECT Dim_AccountsReceivableDocStatusId
             FROM Dim_AccountsReceivableDocStatus ards
            WHERE ards.Status = BSID_BSTAT), 1) Dim_AccountsReceivableDocStatusId,
          ifnull ( ( SELECT Dim_CustomerPaymentTermsid
            FROM Dim_CustomerPaymentTerms pt
            WHERE pt.PaymentTermCode = BSID_ZTERM) , 1) Dim_PaymentTermsId,
          ifnull ( ( SELECT Dim_PaymentMethodId
            FROM Dim_PaymentMethod pm
            WHERE pm.PaymentMethod = BSID_ZLSCH
              AND pm.Country = cm.Country) , 1) Dim_PaymentMethodId,
          BSID_MANST,
          ifnull((select dr.Dim_RiskClassId
                                  from UKMBP_CMS a
                                        inner join BUT000 b on a.PARTNER = b.PARTNER and TIMESTAMP(LOCAL_TIMESTAMP) <= ifnull(a.RATING_VAL_DATE,TIMESTAMP(LOCAL_TIMESTAMP))
                                        inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
                                        inner join dim_riskclass dr on dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1
                                  where c.CUSTOMER = cm.CustomerNumber),0),
          'Not Set' dd_CreditRep,
          ifnull((select so.dim_dateidnextdate from fact_salesorder so where so.dd_SalesDocNo = BSID_VBEL2), 1) Dim_DateidSONextDate,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_accountsreceivable_temp') + row_number() over ()
    FROM BSID arc
      INNER JOIN dim_company dc ON dc.CompanyCode = arc.BSID_BUKRS
      INNER JOIN dim_customer cm ON cm.CustomerNumber = arc.BSID_KUNNR
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_accountsreceivable_temp ar
                   WHERE     ar.dd_AccountingDocNo = arc.BSID_BELNR
                         AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
                         AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
                         AND ar.dd_fiscalyear = arc.BSID_GJAHR
                         AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
                         AND ar.Dim_CompanyId = dc.Dim_CompanyId
                         AND ar.Dim_CustomerId = cm.Dim_CustomerId);


update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_accountsreceivableid),0) from fact_accountsreceivable_temp)
where table_name = 'fact_accountsreceivable_temp';
					 

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp farINNER JOIN BSAD arc ON far.dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';


/* Start of Update 2 - Split into 7 parts */

/* Update 2 - Part 1
UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSAD arc
SET       amt_CashDiscountDocCurrency =  (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAD_WSKTO,
          amt_CashDiscountLocalCurrency = (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAD_SKNTO,
          amt_InLocalCurrency = (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAD_DMBTR,
          amt_TaxInDocCurrency = (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAD_WMWST,
          amt_TaxInLocalCurrency = (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAD_MWSTS,
          amt_InDocCurrency = (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WRBTR,
          ct_CashDiscountDays1 = BSAD_ZBD1P,
          dd_AccountingDocItemNo =  BSAD_BUZEI,
          dd_AccountingDocNo = BSAD_BELNR,
          dd_AssignmentNumber = ifnull(BSAD_ZUONR, 'Not Set')
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR

UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSAD arc
SET     
          Dim_ClearedFlagId = ifnull((SELECT Dim_AccountReceivableStatusId
             FROM Dim_AccountReceivableStatus ars
             WHERE ars.Status = CASE WHEN BSAD_REBZG IS NOT NULL AND BSAD_REBZJ <> 0 THEN 'F - Partial' ELSE 'C - Cleared' END), 1),
          Dim_DateIdAccDocDateEntered = ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_CPUDT
             AND dt.CompanyCode = arc.BSAD_BUKRS
          ), 1) ,
          Dim_DateIdBaseDateForDueDateCalc = ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_ZFBDT
              AND dt.CompanyCode = arc.BSAD_BUKRS
          ), 1) ,
          Dim_DateIdCreated = ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_BLDAT
             AND  dt.CompanyCode = arc.BSAD_BUKRS
          ), 1) ,
          Dim_DateIdPosting = ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_BUDAT
              AND dt.CompanyCode = arc.BSAD_BUKRS)
          , 1) 	  
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR


UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSAD arc
SET     
          dd_debitcreditid = (CASE WHEN BSAD_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END) ,
          dd_ClearingDocumentNo = ifnull(BSAD_AUGBL,'Not Set') ,
          dd_FixedPaymentTerms = BSAD_ZBFIX,
          dd_InvoiceNumberTransBelongTo = ifnull(BSAD_REBZG,'Not Set'),
          dd_NetPaymentTermsPeriod = BSAD_ZBD3T,
          Dim_BlockingPaymentReasonId = ifnull( ( SELECT Dim_BlockingPaymentReasonId
          FROM Dim_BlockingPaymentReason bpr
          WHERE bpr.BlockingKeyPayment = BSAD_ZLSPR), 1),
          Dim_BusinessAreaId = ifnull( ( SELECT Dim_BusinessAreaId
           FROM Dim_BusinessArea ba
           WHERE ba.BusinessArea = BSAD_GSBER), 1) 
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR

UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSAD arc
SET     
          Dim_ChartOfAccountsId = ifnull(( SELECT Dim_ChartOfAccountsId
            FROM Dim_ChartOfAccounts coa
            INNER JOIN dim_company dc ON dc.ChartOfAccounts = coa.CharOfAccounts
            WHERE dc.CompanyCode = BSAD_BUKRS
            AND coa.GLAccountNumber = BSAD_SAKNR), 1) ,
          far.dim_companyid = dcm.Dim_CompanyId,
          Dim_CreditControlAreaId = ifnull(( SELECT Dim_CreditControlAreaid
                FROM dim_creditcontrolarea cca
               WHERE cca.CreditControlAreaCode = BSAD_KKBER), 1) ,
          Dim_Currencyid = ifnull((SELECT dim_currencyid
             FROM dim_currency c
            WHERE c.CurrencyCode = dcm.Currency),1)  ,
          far.Dim_CustomerID = dc.Dim_CustomerId ,
          Dim_DateIdClearing = ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_AUGDT
                  AND dt.CompanyCode = arc.BSAD_BUKRS), 1) ,
          Dim_DocumentTypeId = ifnull(( SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.type = BSAD_BLART), 1) 
			
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR

UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSAD arc
SET     										
          Dim_PaymentReasonId = ifnull((SELECT Dim_PaymentReasonId
             FROM Dim_PaymentReason pr
            WHERE pr.PaymentReasonCode = BSAD_RSTGR
               AND pr.CompanyCode = BSAD_BUKRS), 1) ,
          Dim_PostingKeyId = ifnull( ( SELECT Dim_PostingKeyId
            FROM Dim_PostingKey pk
            WHERE pk.PostingKey= BSAD_BSCHL
             AND pk.SpecialGLIndicator = ifnull(BSAD_UMSKZ,'Not Set')), 1) ,
          Dim_SpecialGLIndicatorId = ifnull( ( SELECT Dim_SpecialGLIndicatorId
            FROM Dim_SpecialGLIndicator sgl
            WHERE sgl.SpecialGLIndicator = BSAD_UMSKZ
              AND sgl.AccountType = 'D'), 1) ,
          Dim_TargetSpecialGLIndicatorId = ifnull( ( SELECT Dim_SpecialGLIndicatorId
            FROM Dim_SpecialGLIndicator sgl
            WHERE sgl.SpecialGLIndicator = BSAD_ZUMSK
              AND sgl.AccountType =  'D'), 1) ,
          Dim_SpecialGlTransactionTypeId = ifnull ( ( SELECT Dim_SpecialGlTransactionTypeId
            FROM Dim_SpecialGlTransactionType sgt
            WHERE sgt.SpecialGlTransactionTypeId = BSAD_UMSKS) , 1),
          dd_SalesDocNo = ifnull(BSAD_VBEL2, 'Not Set')
										
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR

UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSAD arc
SET     
          dd_SalesItemNo = BSAD_POSN2,
          dd_SalesScheduleNo = BSAD_ETEN2,
          dd_CashDiscountPercentage1 = BSAD_ZBD1P,
          dd_BillingNo = ifnull(BSAD_VBELN,'Not Set'),
          dd_CashDiscountPercentage2 = BSAD_ZBD2P,
          dd_ProductionOrderNo = ifnull(BSAD_AUFNR,'Not Set'),
          Dim_AccountsReceivableDocStatusId = ifnull( (SELECT Dim_AccountsReceivableDocStatusId
             FROM Dim_AccountsReceivableDocStatus ards
            WHERE ards.Status = BSAD_BSTAT), 1),
          Dim_PaymentTermsId = ifnull((SELECT Dim_CustomerPaymentTermsid
            FROM Dim_CustomerPaymentTerms pt
            WHERE pt.PaymentTermCode = BSAD_ZTERM), 1),
          dd_DunningLevel = BSAD_MANST,
          Dim_PaymentMethodId = ifnull ( ( SELECT Dim_PaymentMethodId
            FROM Dim_PaymentMethod pm
            WHERE pm.PaymentMethod = BSAD_ZLSCH
              AND pm.Country = dc.Country) , 1)
			  
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR


UPDATE fact_accountsreceivable_temp far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSAD arc
SET     
          Dim_RiskClassId = ifnull((select dr.Dim_RiskClassId
                                  from UKMBP_CMS a
                                        inner join BUT000 b on a.PARTNER = b.PARTNER and TIMESTAMP(LOCAL_TIMESTAMP) <= ifnull(a.RATING_VAL_DATE,TIMESTAMP(LOCAL_TIMESTAMP))
                                        inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
                                        inner join dim_riskclass dr on dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1
                                  where c.CUSTOMER = dc.CustomerNumber),0)
										
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR


End of Update 2 - Split into 7 parts */


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp farINNER JOIN BSAD arc ON far.dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';


/* Insert 2 */

INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency,
                                    amt_CashDiscountLocalCurrency,
                                    amt_InDocCurrency,
                                    amt_InLocalCurrency,
                                    amt_TaxInDocCurrency,
                                    amt_TaxInLocalCurrency,
                                    ct_CashDiscountDays1,
                                    dd_AccountingDocItemNo,
                                    dd_AccountingDocNo,
                                    dd_AssignmentNumber,
                                    Dim_ClearedFlagId,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdCreated,
                                    Dim_DateIdPosting,
                                    dd_debitcreditid,
                                    dd_ClearingDocumentNo,
                                    dd_FiscalPeriod,
                                    dd_FiscalYear,
                                    dd_FixedPaymentTerms,
                                    dd_InvoiceNumberTransBelongTo,
                                    dd_NetPaymentTermsPeriod,
                                    Dim_BlockingPaymentReasonId,
                                    Dim_BusinessAreaId,
                                    Dim_ChartOfAccountsId,
                                    Dim_CompanyId,
                                    Dim_CreditControlAreaId,
                                    Dim_CurrencyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    Dim_DocumentTypeId,
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    Dim_SpecialGLIndicatorId,
                                    Dim_TargetSpecialGLIndicatorId,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    dd_BillingNo,
                                    dd_CashDiscountPercentage1,
                                    dd_CashDiscountPercentage2,
                                    dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    Dim_PaymentTermsId,
                                    dd_DunningLevel,
                                    Dim_PaymentMethodId,
                                    dim_RiskClassId,
                                    dd_CreditRep,
                                    Dim_DateidSONextDate,fact_accountsreceivableid)
   SELECT (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WSKTO amt_CashDiscountDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_SKNTO amt_CashDiscountLocalCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WRBTR amt_InDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_MWSTS amt_TaxInLocalCurrency,
       
        BSAD_ZBD1P ct_CashDiscountDays1,
          BSAD_BUZEI dd_AccountingDocItemNo,
          BSAD_BELNR dd_AccountingDocNo,
          ifnull(BSAD_ZUONR, 'Not Set') dd_AssignmentNumber,
          ifnull((SELECT Dim_AccountReceivableStatusId
             FROM Dim_AccountReceivableStatus ars
             WHERE ars.Status = CASE WHEN BSAD_REBZG IS NOT NULL AND BSAD_REBZJ <> 0 THEN 'F - Partial' ELSE 'C - Cleared' END), 1) Dim_ClearedFlagId,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_CPUDT
                  AND dt.CompanyCode = BSAD_BUKRS
                  ), 1) Dim_DateIdAccDocDateEntered,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_ZFBDT
                  AND dt.CompanyCode = BSAD_BUKRS
                  ), 1 ) Dim_DateIdBaseDateForDueDateCalc,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_BLDAT
                  AND dt.CompanyCode = BSAD_BUKRS
                  ), 1) Dim_DateIdCreated,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_BUDAT
                  AND dt.CompanyCode = BSAD_BUKRS
                  ), 1) Dim_DateIdPosting,
          (CASE WHEN BSAD_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)  dd_debitcreditid,
          ifnull(BSAD_AUGBL,'Not Set') dd_ClearingDocumentNo,
          BSAD_MONAT dd_FiscalPeriod,
          BSAD_GJAHR dd_FiscalYear,
          BSAD_ZBFIX dd_FixedPaymentTerms,
          ifnull(BSAD_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
          BSAD_ZBD3T dd_NetPaymentTermsPeriod,
          ifnull(( SELECT Dim_BlockingPaymentReasonId
          FROM Dim_BlockingPaymentReason bpr
          WHERE bpr.BlockingKeyPayment = BSAD_ZLSPR), 1 ) Dim_BlockingPaymentReasonId,
          ifnull(( SELECT Dim_BusinessAreaId
           FROM Dim_BusinessArea ba
           WHERE ba.BusinessArea = BSAD_GSBER), 1) Dim_BusinessAreaId,
          ifnull(( SELECT Dim_ChartOfAccountsId
            FROM Dim_ChartOfAccounts coa
            INNER JOIN dim_company dc ON dc.ChartOfAccounts = coa.CharOfAccounts
            WHERE dc.CompanyCode = BSAD_BUKRS
            AND coa.GLAccountNumber = BSAD_SAKNR), 1) Dim_ChartOfAccountsId,
          dc.Dim_CompanyId,
          ifnull(( SELECT Dim_CreditControlAreaid
                FROM dim_creditcontrolarea cca
               WHERE cca.CreditControlAreaCode = BSAD_KKBER), 1) Dim_CreditControlAreaId,
          ifnull((SELECT dim_currencyid
             FROM dim_currency c
            WHERE c.CurrencyCode = dc.Currency)
            , 1) Dim_Currencyid,
          cm.Dim_CustomerId Dim_CustomerID,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAD_AUGDT
                  AND dt.CompanyCode = BSAD_BUKRS
                  ), 1) Dim_DateIdClearing,
          ifnull((SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.type = BSAD_BLART), 1) Dim_DocumentTypeId,
          ifnull((SELECT Dim_PaymentReasonId
             FROM Dim_PaymentReason pr
            WHERE pr.PaymentReasonCode = BSAD_RSTGR
               AND pr.CompanyCode = BSAD_BUKRS), 1) Dim_PaymentReasonId,
          ifnull(( SELECT Dim_PostingKeyId
            FROM Dim_PostingKey pk
            WHERE pk.PostingKey= BSAD_BSCHL
            AND pk.SpecialGLIndicator = ifnull(BSAD_UMSKZ,'Not Set')), 1) Dim_PostingKeyId,
          ifnull(( SELECT Dim_SpecialGLIndicatorId
            FROM Dim_SpecialGLIndicator sgl
            WHERE sgl.SpecialGLIndicator = BSAD_UMSKZ
              AND sgl.AccountType =  'D'), 1) Dim_SpecialGLIndicatorId,
          ifnull(( SELECT Dim_SpecialGLIndicatorId
            FROM Dim_SpecialGLIndicator sgl
            WHERE sgl.SpecialGLIndicator = BSAD_ZUMSK
              AND sgl.AccountType =  'D'), 1) Dim_TargetSpecialGLIndicatorId,
          ifnull(BSAD_VBEL2, 'Not Set') dd_SalesDocNo,
          BSAD_POSN2 dd_SalesItemNo,
          BSAD_ETEN2 dd_SalesScheduleNo,
          ifnull(BSAD_VBELN,'Not Set') dd_BillingNo,
          BSAD_ZBD1P dd_CashDiscountPercentage1,
          BSAD_ZBD2P dd_CashDiscountPercentage2,
          ifnull(BSAD_AUFNR,'Not Set') dd_ProductionOrderNo,
          ifnull ( ( SELECT Dim_SpecialGlTransactionTypeId
            FROM Dim_SpecialGlTransactionType sgt
            WHERE sgt.SpecialGlTransactionTypeId = BSAD_UMSKS) , 1) Dim_SpecialGlTransactionTypeId,
          ifnull( (SELECT Dim_AccountsReceivableDocStatusId
             FROM Dim_AccountsReceivableDocStatus ards
            WHERE ards.Status = BSAD_BSTAT), 1) Dim_AccountsReceivableDocStatusId,
          ifnull((SELECT Dim_CustomerPaymentTermsid
            FROM Dim_CustomerPaymentTerms pt
            WHERE pt.PaymentTermCode = BSAD_ZTERM), 1) Dim_PaymentTermsId,
          BSAD_MANST,
          ifnull ( ( SELECT Dim_PaymentMethodId
            FROM Dim_PaymentMethod pm
            WHERE pm.PaymentMethod = BSAD_ZLSCH
              AND pm.Country = cm.Country ) , 1) Dim_PaymentMethodId,
          ifnull((select dr.Dim_RiskClassId
                                  from UKMBP_CMS a
                                        inner join BUT000 b on a.PARTNER = b.PARTNER and TIMESTAMP(LOCAL_TIMESTAMP) <= ifnull(a.RATING_VAL_DATE,TIMESTAMP(LOCAL_TIMESTAMP))
                                        inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
                                        inner join dim_riskclass dr on dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1
                                  where c.CUSTOMER = cm.CustomerNumber),0),
          'Not Set' dd_CreditRep,
          ifnull((select so.dim_dateidnextdate from fact_salesorder so where so.dd_SalesDocNo = BSAD_VBEL2 ), 1) Dim_DateidSONextDate,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_accountsreceivable_temp') + row_number() over ()
    FROM BSAD arc
      INNER JOIN dim_company dc ON dc.CompanyCode = BSAD_BUKRS
      INNER JOIN dim_customer cm ON cm.CustomerNumber = BSAD_KUNNR
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_accountsreceivable_temp ar
                   WHERE     ar.dd_AccountingDocNo = arc.BSAD_BELNR
                         AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
                         AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
                         AND ar.dd_fiscalyear = arc.BSAD_GJAHR
                         AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
                         AND ar.Dim_CompanyId = dc.Dim_CompanyId
                         AND ar.Dim_CustomerId = cm.Dim_CustomerId);


update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_accountsreceivableid),0) from fact_accountsreceivable_temp)
where table_name = 'fact_accountsreceivable_temp';
					 

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp INNER JOIN BSID arc ON dd_AccountingDocNo = arc.BSID_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 

/* START OF Update 3*/

/* Update 3 could be broken up like this if required. But that will increase the size of code by a lot. But might be required if it causes performance problems */

/*
DROP TABLE IF EXISTS TMP_BSID_accountsreceivable
CREATE TABLE TMP_BSID_accountsreceivable
AS
SELECT b.*,(CASE WHEN BSID_ZFBDT IS NULL THEN BSID_BLDAT ELSE BSID_ZFBDT END ) as datevalue_upd,'N' as update_flag
FROM BSID b

UPDATE TMP_BSID_accountsreceivable
SET 	update_flag = 'Y'
WHERE  BSID_REBZG IS NULL AND BSID_SHKZG = 'H'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD3T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD3T IS NOT NULL AND BSID_ZBD3T <> 0
AND update_flag = 'N'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD2T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD2T IS NOT NULL AND BSID_ZBD2T <> 0
AND update_flag = 'N'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD1T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD1T IS NOT NULL AND BSID_ZBD1T <> 0
AND update_flag = 'N'

*/

/* Update 3 - Part 1*/

UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc
SET 
far.Dim_NetDueDateId = ifnull(( SELECT DIM_DATEID FROM DIM_DATE dt
                          WHERE dt.DateValue = ( CASE WHEN BSID_ZFBDT IS NULL THEN BSID_BLDAT
                                             ELSE BSID_ZFBDT END) + 
												  CAST((
												  CASE WHEN (BSID_REBZG IS NULL AND BSID_SHKZG = 'H') THEN 0 
												  ELSE ( CASE WHEN BSID_ZBD3T IS NOT NULL AND BSID_ZBD3T <> 0 THEN BSID_ZBD3T
														WHEN BSID_ZBD2T IS NOT NULL AND BSID_ZBD2T <> 0 THEN BSID_ZBD2T
														WHEN BSID_ZBD1T IS NOT NULL AND BSID_ZBD1T <> 0 THEN BSID_ZBD1T  ELSE 0 END) END),integer)
                                AND dt.CompanyCode = arc.BSID_BUKRS ),1)
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR;

UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc
SET 
far.Dim_NetDueDateWrtCashDiscountTerms1 = 
				CASE WHEN (BSID_REBZG IS NULL AND (BSID_SHKZG = 'H'))  
						THEN Dim_NetDueDateId
				ELSE Dim_NetDueDateId  END,
far.Dim_NetDueDateWrtCashDiscountTerms2 = 
				CASE WHEN (BSID_REBZG IS NULL AND (BSID_SHKZG = 'H')) 
						THEN Dim_NetDueDateId
                ELSE Dim_NetDueDateId  END			
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR;	 


/* Update 3 - Part 2*/

UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 dim_date dt
	 
SET far.Dim_NetDueDateWrtCashDiscountTerms1 = dt.DIM_DATEID

WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR	
/* Join to update Dim_NetDueDateWrtCashDiscountTerms1 not updated in part 1 */
AND BSID_ZBD1T IS NOT NULL AND dt.DateValue = CAST((BSID_ZFBDT + ifnull(CAST(BSID_ZBD1T,INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSID_BUKRS ;	 
	 
	 
/* Update 3 - Part 3*/	 
	 
UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 dim_date dt	 
SET far.Dim_NetDueDateWrtCashDiscountTerms2 = dt.DIM_DATEID
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR	
/* Join to update Dim_NetDueDateWrtCashDiscountTerms2 not updated in part 1 */
AND BSID_ZBD2T IS NOT NULL AND dt.DateValue = CAST((BSID_ZFBDT + ifnull(CAST(BSID_ZBD2T,INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSID_BUKRS ;		 



INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp INNER JOIN BSID arc ON dd_AccountingDocNo = arc.BSID_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 

/* END OF Update 3 */

/* START OF Update 4 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp INNER JOIN BSAD arc ON dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

/* Update 4 - Part 1*/

UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc
SET 
far.Dim_NetDueDateId = ifnull(( SELECT DIM_DATEID FROM DIM_DATE dt
                          WHERE dt.DateValue = ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
                                             ELSE BSAD_ZFBDT END) + 
												  CAST((
												  CASE WHEN (BSAD_REBZG IS NULL AND BSAD_SHKZG = 'H') THEN 0 
												  ELSE ( CASE WHEN BSAD_ZBD3T IS NOT NULL AND BSAD_ZBD3T <> 0 THEN BSAD_ZBD3T
														WHEN BSAD_ZBD2T IS NOT NULL AND BSAD_ZBD2T <> 0 THEN BSAD_ZBD2T
														WHEN BSAD_ZBD1T IS NOT NULL AND BSAD_ZBD1T <> 0 THEN BSAD_ZBD1T  ELSE 0 END) END),integer)
                                AND dt.CompanyCode = arc.BSAD_BUKRS ),1),
far.Dim_NetDueDateWrtCashDiscountTerms1 = 
				CASE WHEN (BSAD_REBZG IS NULL AND (BSAD_SHKZG = 'H'))  
						THEN Dim_NetDueDateId
				ELSE Dim_NetDueDateId  END,
far.Dim_NetDueDateWrtCashDiscountTerms2 = 
				CASE WHEN (BSAD_REBZG IS NULL AND (BSAD_SHKZG = 'H')) 
						THEN Dim_NetDueDateId
                ELSE Dim_NetDueDateId  END			
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR;	 


/* Update 4 - Part 2*/

UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 dim_date dt	 
SET far.Dim_NetDueDateWrtCashDiscountTerms1 = dt.DIM_DATEID
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
/* Join to update Dim_NetDueDateWrtCashDiscountTerms1 not updated in part 1 */
AND BSAD_ZBD1T IS NOT NULL AND dt.DateValue = CAST((BSAD_ZFBDT + ifnull(CAST(BSAD_ZBD1T,INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSAD_BUKRS ;	 
	 
	 
/* Update 4 - Part 3*/	 
	 
UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 dim_date dt	 
SET far.Dim_NetDueDateWrtCashDiscountTerms2 = dt.DIM_DATEID
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
/* Join to update Dim_NetDueDateWrtCashDiscountTerms2 not updated in part 1 */
AND BSAD_ZBD2T IS NOT NULL AND dt.DateValue = CAST((BSAD_ZFBDT + ifnull(CAST(BSAD_ZBD2T,INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSAD_BUKRS ;		 



INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp INNER JOIN BSAD arc ON dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 

/* END OF Update 4 */	


/* START OF Update 5 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far INNER JOIN  VBRK_VBRP',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE fact_accountsreceivable_temp far
FROM   dim_company dcm,
	   dim_customer dc,
	   VBRK_VBRP vkp,
	   BSID arc 
SET 
	amt_ExchangeRate = 	/* getExchangeRate(BSID_WAERS,dcm.Currency,VBRK_KURRF,BSID_BLDAT), */
	ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
              where z.pFromCurrency  = BSID_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact' and z.pToCurrency = dcm.Currency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = BSID_BLDAT ),1),
	amt_ExchangeRate_GBL = /* getExchangeRate(dcm.Currency,pGlobalCurrency,VBRK_KURRF,BSID_BLDAT), */
	ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
              where z.pFromCurrency  = dcm.Currency and z.fact_script_name = 'bi_populate_accountsreceivable_fact'  and z.pToCurrency = 'USD' AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = BSID_BLDAT ),1),
	dd_CustomerPONumber = ifnull(VBRK_BSTNK_VF,'Not Set')		   
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_billingno = vkp.VBRK_VBELN
AND far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND far.dd_billingno = arc.BSID_VBELN
AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.CustomerNumber = arc.BSID_KUNNR;	  


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far INNER JOIN  VBRK_VBRP',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

/* END OF Update 5 */  
  
/* START OF Update 6 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far INNER JOIN  VBRK_VBRP part2',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	
  
UPDATE fact_accountsreceivable_temp far
FROM   dim_company dcm,
	   dim_customer dc,
	   VBRK_VBRP vkp,
	   BSAD arc 
SET 
	amt_ExchangeRate = 	/* getExchangeRate(BSAD_WAERS,dcm.Currency,VBRK_KURRF,BSAD_BLDAT), */
	ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
              where z.pFromCurrency  = BSAD_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact' and z.pToCurrency = dcm.Currency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = BSAD_BLDAT ),1),
	amt_ExchangeRate_GBL = /* getExchangeRate(dcm.Currency,pGlobalCurrency,VBRK_KURRF,BSAD_BLDAT), */
	ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
              where z.pFromCurrency  = dcm.Currency and z.fact_script_name = 'bi_populate_accountsreceivable_fact' and  z.pToCurrency = 'USD' AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = BSAD_BLDAT ),1),
	dd_CustomerPONumber = ifnull(VBRK_BSTNK_VF,'Not Set')		   
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_billingno = vkp.VBRK_VBELN
AND far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND far.dd_billingno = arc.BSAD_VBELN
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR;	  

/* END OF Update 6 */


/* Update 7 */

UPDATE fact_accountsreceivable_temp far 
FROM fact_billing fb 
SET far.ct_bill_QtySalesUOM = ifnull((SELECT SUM(f_bill.ct_BillingQtySalesUOM) FROM fact_billing f_bill where far.dd_BillingNo = f_bill.dd_billing_no),0),
 far.ct_bill_QtyStockUOM = ifnull((SELECT SUM(f_bill.ct_BillingQtyStockUOM) FROM fact_billing f_bill where far.dd_BillingNo = f_bill.dd_billing_no),0),
 far.Dim_PartId = ifnull(fb.Dim_PartId,1),
 far.Dim_PlantId = ifnull(fb.dim_plantid,1),
 far.dd_CustomerPONumber = fb.dd_CustomerPONumber,
 far.dd_salesdocno = case when far.dd_salesdocno = 'Not Set' then fb.dd_salesdocno else far.dd_salesdocno end
WHERE fb.dd_billing_no = far.dd_billingno;		 
		 
		 
INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far INNER JOIN  VBRK_VBRP part2',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		

/* Update 8 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far INNER JOIN BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		


UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc
SET	 amt_EligibleForDiscount = BSID_SKFBT * amt_ExchangeRate,
     far.Dim_DateidSONextDate = ifnull((select so.dim_dateidnextdate from fact_salesorder so where so.dd_SalesDocNo = far.dd_salesdocno ), 1)
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.CustomerNumber = arc.BSID_KUNNR;
	 

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far INNER JOIN BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		

/* Update 9 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far INNER JOIN BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE fact_accountsreceivable_temp far
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc
SET	 amt_EligibleForDiscount = BSAD_SKFBT * amt_ExchangeRate,
     far.Dim_DateidSONextDate = ifnull((select so.dim_dateidnextdate from fact_salesorder so where so.dd_SalesDocNo = far.dd_salesdocno ), 1)
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR;


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far INNER JOIN BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	


/* Update 10 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far, Dim_ARMiscellaneous armisc, BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE fact_accountsreceivable_temp far
FROM Dim_ARMiscellaneous armisc,
     BSID arc
SET far.Dim_ARMiscellaneousId = armisc.Dim_ARMiscellaneousId
 WHERE     far.dd_AccountingDocNo = arc.BSID_BELNR
       AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
       AND far.dd_AssignmentNumber = arc.BSID_ZUONR
       AND far.dd_fiscalyear = arc.BSID_GJAHR
       AND far.dd_FiscalPeriod  = arc.BSID_MONAT
       AND armisc.CustomerItemsClearingReversed = ifnull(arc.BSID_XRAGL,'Not Set')
       AND armisc.CustomerItemsDocumentPostedYet = ifnull(arc.BSID_XNETB,'Not Set')
       AND armisc.NegativePosting = ifnull(arc.BSID_XNEGP,'Not Set');
	   
INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far, Dim_ARMiscellaneous armisc, BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		   

/* Update 11 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far, Dim_ARMiscellaneous armisc, BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		


UPDATE fact_accountsreceivable_temp far
FROM   Dim_ARMiscellaneous armisc,
       BSAD arc
SET far.Dim_ARMiscellaneousId = armisc.Dim_ARMiscellaneousId
 WHERE       far.dd_AccountingDocNo = arc.BSAD_BELNR
       AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
       AND far.dd_fiscalyear = arc.BSAD_GJAHR
       AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
       AND armisc.CustomerItemsClearingReversed = ifnull(arc.BSAD_XRAGL,'Not Set')
       AND armisc.CustomerItemsDocumentPostedYet = ifnull(arc.BSAD_XNETB,'Not Set')
       AND armisc.NegativePosting = ifnull(arc.BSAD_XNEGP,'Not Set');
	   

/* Update 12 */	   
	   
UPDATE    fact_accountsreceivable_temp far
FROM  dim_date ndt
   SET far.dd_ARAge =    
          (CASE
              WHEN current_date - ndt.DateValue BETWEEN 0 AND 30
              THEN
                 '1 - 30'
              WHEN current_date - ndt.DateValue BETWEEN 31 AND 60
              THEN
                 '31 - 60'
              WHEN current_date - ndt.DateValue BETWEEN 61 AND 90
              THEN
                 '61 - 90'
              WHEN current_date - ndt.DateValue > 90
              THEN
                 ' > 90 '
              WHEN current_date - ndt.DateValue < 0
              THEN
                 'Future'
              ELSE
                 'Not Set'
           END)
 WHERE far.dim_clearedflagid IN (2, 4)
 AND far.Dim_NetDueDateId = ndt.dim_dateid;


UPDATE fact_accountsreceivable_temp ar
FROM fact_billing b, 
fact_salesorderdelivery sod
SET ar.Dim_DateIdActualGIDate = sod.Dim_DateIdActualGI_Original
WHERE ar.dd_accountingdocno = b.dd_billing_no
AND b.dd_Salesdlvrdocno = sod.dd_salesdlvrdocno
AND b.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
AND ar.dd_InvoiceNumberTransBelongTo = 'Not Set';

update fact_accountsreceivable_temp ar
set ar.Dim_DateIdActualGIDate = 1
where ar.Dim_DateIdActualGIDate IS NULL;

DROP TABLE IF EXISTS tmp_CustomerCreditLimit;

CREATE TABLE tmp_CustomerCreditLimit as
select distinct c.CUSTOMER,ifnull(a.CREDIT_LIMIT,0) as Credit_Limit,a.LIMIT_VALID_DATE,RANK() OVER (PARTITION BY c.CUSTOMER ORDER BY a.LIMIT_VALID_DATE ASC) 
  AS Rank from UKMBP_CMS_SGM a 
  inner join BUT000 b on a.PARTNER = b.PARTNER and TIMESTAMP(LOCAL_TIMESTAMP) <= a.LIMIT_VALID_DATE
  inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID;

DELETE FROM tmp_CustomerCreditLimit WHERE Rank <> 1;

UPDATE fact_accountsreceivable_temp far
FROM    dim_customer dc,
        tmp_CustomerCreditLimit  t
SET     dd_CreditLimit = t.Credit_Limit
WHERE	 dc.Dim_CustomerId = far.Dim_CustomerId
AND dc.CustomerNumber = t.customer;

UPDATE fact_accountsreceivable_temp far
SET dd_CreditLimit = 0
WHERE dd_CreditLimit IS NULL;
                                  
DROP TABLE IF EXISTS tmp_CustomerCreditLimit; 

DROP TABLE IF EXISTS tmp_upd_702;

Create table tmp_upd_702 as Select first 0 
(case when b.NAME_FIRST is null then b.NAME_LAST else b.NAME_FIRST+' '+ifnull(b.NAME_LAST,'') end) updcol1,d.BUT050_PARTNER2 updVBAK_KUNNR
from cvi_cust_link c inner join but000 b1 on c.PARTNER_GUID = b1.PARTNER_GUID
	inner join BUT050 d on b1.PARTNER = d.BUT050_PARTNER2 AND d.BUT050_RELTYP = 'UKMSB0'
	inner join but000 b on b.PARTNER = d.BUT050_PARTNER1
where  ANSIDATE(CURRENT_DATE) between ANSIDATE(d.BUT050_DATE_FROM) and ANSIDATE(d.BUT050_DATE_TO)
and (b.NAME_FIRST is not null or b.NAME_LAST is not null and b.BU_GROUP = 'CRED')
order by c.customer ;

update fact_accountsreceivable_temp so
from   tmp_upd_702 t,
       Dim_Customer c
Set dd_CreditRep = t.updcol1
Where so.dim_Customerid = c.dim_customerid
  and t.updVBAK_KUNNR = c.CustomerNumber;

DROP TABLE IF EXISTS tmp_upd_702;

call vectorwise(combine 'fact_accountsreceivable_temp');

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_accountsreceivable_temp far, Dim_ARMiscellaneous armisc, BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		
 

/* Update 13 */

/* Need to break up this query, as the original update with just changes for concat results in Rewrite Error in VW */

/* This is required otherwise the tmp_dim.. creation query fails with error Invalid qualifier 'f' .. */
drop table if exists tmp_fact_accountsreceivable1;
create table tmp_fact_accountsreceivable1
as
select f.*,cast(f.dd_AccountingDocNo as varchar) as char_dd_AccountingDocNo,cast(f.dd_fiscalyear as varchar) as char_dd_fiscalyear,lpad(rtrim(cast(f.dd_AccountingDocItemNo as varchar)),3,'0') as char_dd_AccountingDocItemNo , cast(dcm.CompanyCode as varchar) as char_CompanyCode
FROM fact_accountsreceivable_temp f,dim_company dcm
WHERE   dcm.Dim_CompanyId = f.Dim_CompanyId;




DROP TABLE IF EXISTS tmp_dim_company_fact_accountsreceivable;
CREATE TABLE tmp_dim_company_fact_accountsreceivable
AS
SELECT f.*,
	   (f.char_CompanyCode + f.char_dd_AccountingDocNo  + f.char_dd_fiscalyear + f.char_dd_AccountingDocItemNo) as FDM_DCPROC_OBJ_KEY
FROM 	 tmp_fact_accountsreceivable1 f;

/* Added explicit commits as they were not working earlier with wrapper script */
call vectorwise(combine 'tmp_dim_company_fact_accountsreceivable');

DROP TABLE IF EXISTS tmp_SCMG_T_CASE_ATTR;
CREATE TABLE tmp_SCMG_T_CASE_ATTR
AS
SELECT a.EXT_KEY,b.FDM_DCPROC_OBJ_KEY
from SCMG_T_CASE_ATTR a inner join FDM_DCPROC b on a.CASE_GUID = b.FDM_DCPROC_CASE_GUID_LOC
where b.FDM_DCPROC_OBJ_TYPE = 'BSEG' and b.FDM_DCPROC_NEW_CLASS = 'DISP_RES' ;

UPDATE tmp_dim_company_fact_accountsreceivable f 
  SET dd_DisputeCaseId = ifnull((select a.EXT_KEY
                                from tmp_SCMG_T_CASE_ATTR a
                                where a.FDM_DCPROC_OBJ_KEY = f.FDM_DCPROC_OBJ_KEY),'Not Set');
                                    /* MYSQL Query:
									and b.FDM_DCPROC_OBJ_KEY = concat(dcm.CompanyCode,f.dd_AccountingDocNo,f.dd_fiscalyear,LPAD(CONVERT(f.dd_AccountingDocItemNo,CHAR(3)),3,'0'))),'Not Set') */
/* WHERE dcm.Dim_CompanyId = f.Dim_CompanyId */

/* Update 14 */			

DROP TABLE IF EXISTS tmp_SCMG_T_CASE_ATTR2;
CREATE TABLE tmp_SCMG_T_CASE_ATTR2
AS
SELECT a.EXT_KEY,b.FDM_DCPROC_OBJ_KEY
from SCMG_T_CASE_ATTR a inner join FDM_DCPROC b on a.CASE_GUID = b.FDM_DCPROC_CASE_GUID_LOC
where b.FDM_DCPROC_OBJ_TYPE = 'BSEG' and b.FDM_DCPROC_NEW_CLASS in ('DISP_INV','DISP_PAY');


UPDATE tmp_dim_company_fact_accountsreceivable f 
  SET dd_DisputeCaseId = ifnull((select a.EXT_KEY
                                from tmp_SCMG_T_CASE_ATTR2 a
                                where a.FDM_DCPROC_OBJ_KEY = f.FDM_DCPROC_OBJ_KEY),'Not Set')
WHERE dd_DisputeCaseId = 'Not Set';			

DROP TABLE IF EXISTS tmp_d;
CREATE TABLE tmp_d
AS
select a.EXT_KEY, b.FDM_DCPROC_OBJ_KEY,
                          SUM(FDM_DCPROC_DELTA_CREDITED) SUM_FDM_DCPROC_DELTA_CREDITED,
                          SUM(FDM_DCPROC_DELTA_DISPUTED) SUM_FDM_DCPROC_DELTA_DISPUTED,
                          SUM(FDM_DCPROC_DELTA_NOT_SOLVED) SUM_FDM_DCPROC_DELTA_NOT_SOLVED,
                          SUM(FDM_DCPROC_DELTA_ORIGINAL) SUM_FDM_DCPROC_DELTA_ORIGINAL,
                          SUM(FDM_DCPROC_DELTA_PAID) SUM_FDM_DCPROC_DELTA_PAID,
                          SUM(FDM_DCPROC_DELTA_WRITE_OFF) SUM_FDM_DCPROC_DELTA_WRITE_OFF
                  from SCMG_T_CASE_ATTR a inner join FDM_DCPROC b on a.CASE_GUID = b.FDM_DCPROC_CASE_GUID_LOC
                  where b.FDM_DCPROC_OBJ_TYPE = 'BSEG' and b.FDM_DCPROC_NEW_CLASS in ('DISP_INV','DISP_PAY','DISP_RES')
                  group by a.EXT_KEY, b.FDM_DCPROC_OBJ_KEY;

/*DROP TABLE tmp_dim_company_fact_accountsreceivable*/
DROP TABLE tmp_fact_accountsreceivable1;
DROP TABLE tmp_SCMG_T_CASE_ATTR2;
DROP TABLE tmp_SCMG_T_CASE_ATTR;

 
/* Update 15 - Final Update */	

UPDATE tmp_dim_company_fact_accountsreceivable f 
FROM tmp_d d
SET amt_OriginalAmtDisputed = SUM_FDM_DCPROC_DELTA_ORIGINAL,
    amt_DisputedAmt = SUM_FDM_DCPROC_DELTA_DISPUTED,
    amt_DisputeAmtPaid = SUM_FDM_DCPROC_DELTA_PAID,
    amt_DisputeAmtCredited = SUM_FDM_DCPROC_DELTA_CREDITED,
    amt_DisputeAmtCleared = SUM_FDM_DCPROC_DELTA_WRITE_OFF,
    amt_DisputeAmtWrittenOff = SUM_FDM_DCPROC_DELTA_NOT_SOLVED
WHERE f.FDM_DCPROC_OBJ_KEY = d.FDM_DCPROC_OBJ_KEY
AND EXT_KEY = f.dd_DisputeCaseId;

call vectorwise (combine 'tmp_dim_company_fact_accountsreceivable');

								
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_AccountingDocNo RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_fiscalyear RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_AccountingDocItemNo RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_CompanyCode RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column FDM_DCPROC_OBJ_KEY RESTRICT;

call vectorwise ( combine 'tmp_dim_company_fact_accountsreceivable');

call vectorwise( combine 'fact_accountsreceivable_temp-fact_accountsreceivable_temp+tmp_dim_company_fact_accountsreceivable');
call vectorwise( combine 'fact_accountsreceivable -fact_accountsreceivable');
call vectorwise( combine 'fact_accountsreceivable+fact_accountsreceivable_temp');


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'bi_populate_accountsreceivable_fact END',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		
