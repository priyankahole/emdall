
Drop table if exists mdtb_tmp_q01;
Create table mdtb_tmp_q01 as Select *,(ifnull(MDTB_UMDAT,MDTB_DAT01) -  MDTB_DAT02) leadcolupd from  mdtb Where  ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL ;

UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET m.Dim_ItemCategoryid = ic.Dim_ItemCategoryid
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;



UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET         m.Dim_Vendorid = v.Dim_Vendorid
  WHERE       tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_Vendorid <> v.Dim_Vendorid;



        UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET m.Dim_FixedVendorid = fv.Dim_Vendorid
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_FixedVendorid <> fv.Dim_Vendorid;



        UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET m.Dim_DocumentTypeid = dt.Dim_DocumentTypeid
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_DocumentTypeid <> dt.Dim_DocumentTypeid;



        UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET m.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;

drop table if exists eban_p00;
Create table eban_p00 as Select *, ifnull(EBAN_FLIEF, EBAN_LIFNR) eban_colupd from eban where ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL;
Drop table if exists fact_mrp_del_p00;
Create table fact_mrp_del_p00 as Select fact_mrpid,ct_leadTimeVariance,leadcolupd,k.MDKP_MATNR,k.MDKP_PLWRK,eban_colupd,BANFN,BNFPO,dp.LeadTime
From fact_mrp m, eban_p00 pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
        tmpvariable_00e
  WHERE    tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN;

Update fact_mrp_del_p00 pdo
Set pdo.ct_leadTimeVariance =   ifnull(((pdo.leadcolupd)
                  - (Select v_leadTime
                        from tmp_getLeadTime glt
                        where glt.pPart =pdo.MDKP_MATNR
                              AND glt.pPlant =  pdo.MDKP_PLWRK
                              AND glt.pVendor =   pdo.eban_colupd
                              AND glt.DocumentNumber=  pdo.BANFN
                              AND glt.DocumentLineNumber=  pdo.BNFPO
			      AND glt.fact_script_name = 'bi_populate_mrp_fact'
                              AND glt.DocumentType=  'PR')), -1 * pdo.LeadTime);


Update fact_mrp m
from fact_mrp_del_p00 pdo
Set m.ct_leadTimeVariance = pdo.ct_leadTimeVariance
Where m.fact_mrpid = pdo.fact_mrpid;


	UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET amt_ExtendedPrice = ifnull(pr.MENGE * PREIS / ifnull((case when PEINH = 0 then null else PEINH end), 1), 0)
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN;



        UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET amt_ExtendedPrice_GBL = ifnull(pr.MENGE * PREIS / ifnull((case when PEINH = 0 then null else PEINH end), 1), 0) * 
(SELECT exchangeRate from tmp_getExchangeRate1 
where  tmpTotalCount > 0 and pFromCurrency = pr.EBAN_WAERS 
and pToCurrency = ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD' ) 
and pFromExchangeRate is null
and pDate = pr.BADAT
 and fact_script_name = 'bi_populate_mrp_fact')
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN;



        UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET Dim_DateidReschedule = ifnull((SELECT od.dim_dateid
                                      FROM dim_date od
                                      WHERE od.DateValue = t.MDTB_UMDAT
                                          AND od.CompanyCode = pl.CompanyCode), 1)
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.BANFN
        AND m.dd_DocumentItemNo = pr.BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.BSART, 'Not Set')
        AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN;

drop table if exists mdtb_tmp_q01;
drop table if exists fact_mrp_del_p00;
drop table if exists eban_p00;

