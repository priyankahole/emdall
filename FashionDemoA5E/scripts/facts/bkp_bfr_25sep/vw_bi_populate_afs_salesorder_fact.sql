/* ##################################################################################################################
  
     Script         : bi_populate_afs_salesorder_fact
     Author         : Ashu
     Created On     : 22 Jan 2013
  
  
     Description    : Stored Proc bi_populate_afs_salesorder_fact from MySQL to Vectorwise syntax
  
     Change History
     Date            By        Version           Desc
	 29 Aug 2013     Lokesh    1.2               Changes done for amt_Subtotal3inCustConfig_Billing and amt_Subtotal3_OrderQty.  
     23 May 2013     Lokesh    1.1               Fixed issue with default values getting updated ( Mail sent by Shanthi on 22 May  for columbia prod issue)
     22 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
     30 Aug 2013     Shanthi   1.43              Fixes for null values for sales district   
#################################################################################################################### */

/* Call getExchangerate SQL file VW_getExchangeRate.afs_salesorder_fact.sql*/

Drop table if exists variable_holder_702;

create table variable_holder_702(
        pGlobalCurrency,
        pCustomPartnerFunctionKey,
        pCustomPartnerFunctionKey1,
        pCustomPartnerFunctionKey2,
        pBillToPartyPartnerFunction,
        pPayerPartnerFunction)
AS
Select CAST(ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD'),varchar(3)),
       CAST( ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'custom.partnerfunction.key'),
                'Not Set'),varchar(10)),
       CAST( ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'custom.partnerfunction.key1'),
                'Not Set'),varchar(10)),
       CAST( ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'custom.partnerfunction.key2'),
                'Not Set'),varchar(10)),
       CAST('RE',varchar(5)),
       CAST('RG',varchar(5)) ;


drop table if exists Dim_CostCenter_702;
create table Dim_CostCenter_702 as Select first 0 * from Dim_CostCenter ORDER BY ValidTo DESC;
 
drop table if exists staging_update_702;
drop table if exists VBAK_VBAP_VBEP_702;

create table VBAK_VBAP_VBEP_702 as 
select min(x.VBEP_ETENR) _SalesSchedNo, x.VBAK_VBELN _SaleseDocNo, x.VBAP_POSNR _SalesItemNo
          from VBAK_VBAP_VBEP x group by x.VBAK_VBELN, x.VBAP_POSNR;

call vectorwise(combine 'VBAK_VBAP_VBEP_702');

DELETE FROM fact_salesorder
WHERE exists (select 1 from CDPOS_VBAK a where a.CDPOS_OBJECTID = dd_SalesDocNo AND a.CDPOS_CHNGIND = 'D' AND a.CDPOS_TABNAME = 'VBAK');

create table staging_update_702 as
Select vbep_wmeng ct_ScheduleQtySalesUnit,
	vbep_bmeng ct_ConfirmedQty,
	vbep_cmeng ct_CorrectedQty,
	ifnull(Decimal((vbap_netpr * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4), 0) amt_UnitPrice,
	vbap_kpein ct_PriceUnit,
	Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
		THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0) 
		ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0) 
	   END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
			  where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1),18,4) amt_ScheduleTotal,
	Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
			THEN vbap_wavwr
			ELSE 0
		  END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4) amt_StdCost,
	Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
		    THEN vbap_zwert
		    ELSE 0
		END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4) amt_TargetValue,
        decimal(0.000,18,4) amt_Tax,
	CASE WHEN VBEP_ETENR = y._SalesSchedNo
			  THEN vbap_zmeng
			  ELSE 0
		    END ct_TargetQty,
	ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)  amt_ExchangeRate,
	ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		where z.pFromCurrency  = co.Currency and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = VBAK_AUDAT AND z.pFromExchangeRate = vbak_stwae ),1)  amt_ExchangeRate_GBL,
	vbap_uebto ct_OverDlvrTolerance,
	vbap_untto ct_UnderDlvrTolerance,
	ifnull((SELECT Dim_Dateid
	    FROM Dim_Date dd
	    WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
	ifnull((SELECT Dim_Dateid
	    FROM Dim_Date dd
	    WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_kmein
                        AND uom.RowIsCurrent = 1), 1) Dim_UnitOfMeasureId,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_meins
                        AND uom.RowIsCurrent = 1), 1) Dim_BaseUoMid,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_vrkme
                        AND uom.RowIsCurrent = 1), 1) Dim_SalesUoMid,
	Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				  where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1),18,4) amt_UnitPriceUoM,
	1 Dim_MaterialPriceGroup4Id,
	1 Dim_MaterialPriceGroup5Id
from fact_salesorder, 
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
        AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
        AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

UPDATE staging_update_702
from VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic
set amt_Tax = ifnull(Decimal(((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0) = 0 THEN 1
					WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
					THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							WHEN 0 THEN 1
							ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
						END
					ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
				 END) 
			* (vbep_bmeng * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
						where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1))),18,4), 0)

WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
        AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

call vectorwise(combine 'staging_update_702');

call vectorwise(combine 'fact_salesorder');

update fact_salesorder so
from staging_update_702 sut
set so.ct_ScheduleQtySalesUnit=sut.ct_ScheduleQtySalesUnit
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.ct_ScheduleQtySalesUnit <> sut.ct_ScheduleQtySalesUnit;

update fact_salesorder so
from staging_update_702 sut
set so.ct_ConfirmedQty=sut.ct_ConfirmedQty
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.ct_ConfirmedQty <> sut.ct_ConfirmedQty;

update fact_salesorder so
from staging_update_702 sut
set so.ct_CorrectedQty=sut.ct_CorrectedQty
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.ct_CorrectedQty <> sut.ct_CorrectedQty;

update fact_salesorder so
from staging_update_702 sut
set so.amt_UnitPrice=sut.amt_UnitPrice
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.amt_UnitPrice <> sut.amt_UnitPrice;

update fact_salesorder so
from staging_update_702 sut
set so.ct_PriceUnit=sut.ct_PriceUnit
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and  so.ct_PriceUnit <> sut.ct_PriceUnit;

update fact_salesorder so
from staging_update_702 sut
set so.amt_ScheduleTotal=sut.amt_ScheduleTotal
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.amt_ScheduleTotal <> sut.amt_ScheduleTotal;

update fact_salesorder so
from staging_update_702 sut
set so.amt_StdCost=sut.amt_StdCost
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and  so.amt_StdCost <> sut.amt_StdCost;

update fact_salesorder so
from staging_update_702 sut
set so.amt_TargetValue=sut.amt_TargetValue
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.amt_TargetValue <> sut.amt_TargetValue;

update fact_salesorder so
from staging_update_702 sut
set so.amt_Tax=sut.amt_Tax
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.amt_Tax <> sut.amt_Tax;

update fact_salesorder so
from staging_update_702 sut
set so.ct_TargetQty=sut.ct_TargetQty
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.ct_TargetQty <> sut.ct_TargetQty;

update fact_salesorder so
from staging_update_702 sut
set so.amt_ExchangeRate=sut.amt_ExchangeRate
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.amt_ExchangeRate <> sut.amt_ExchangeRate;

update fact_salesorder so
from staging_update_702 sut
set so.amt_ExchangeRate_GBL =ifnull(sut.amt_ExchangeRate_GBL,1) 
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.amt_ExchangeRate_GBL <> ifnull(sut.amt_ExchangeRate_GBL,1) ;

update fact_salesorder so
from staging_update_702 sut
set so.ct_OverDlvrTolerance=sut.ct_OverDlvrTolerance
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.ct_OverDlvrTolerance <> sut.ct_OverDlvrTolerance;

update fact_salesorder so
from staging_update_702 sut
set so.ct_UnderDlvrTolerance=sut.ct_UnderDlvrTolerance
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.ct_UnderDlvrTolerance <> sut.ct_UnderDlvrTolerance;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_DateidSalesOrderCreated=sut.Dim_DateidSalesOrderCreated
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_DateidSalesOrderCreated <> sut.Dim_DateidSalesOrderCreated;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_DateidFirstDate=sut.Dim_DateidFirstDate
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_DateidFirstDate <> sut.Dim_DateidFirstDate;

update fact_salesorder so
from staging_update_702 sut
set so.amt_UnitPriceUoM=sut.amt_UnitPriceUoM
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.amt_UnitPriceUoM <> sut.amt_UnitPriceUoM;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_UnitOfMeasureId = sut.Dim_UnitOfMeasureId
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_UnitOfMeasureId <> sut.Dim_UnitOfMeasureId;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_BaseUoMid = sut.Dim_BaseUoMid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_BaseUoMid <> sut.Dim_BaseUoMid;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_SalesUoMid = sut.Dim_SalesUoMid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_SalesUoMid <> sut.Dim_SalesUoMid;

call vectorwise(combine 'fact_salesorder');

drop table if exists staging_update_702;

create table staging_update_702(
        dim_dateidscheddeliveryreq integer null,
        dim_dateidscheddelivery integer null,
        dim_dateidgoodsissue integer null,
        dim_dateidmtrlavail integer null,
        dim_dateidloading integer null,
        dim_dateidguaranteedate integer null,
        dim_dateidtransport integer null,
        dim_currencyid smallint null,
        dim_producthierarchyid integer null,
        dim_plantid smallint null,
        dim_companyid smallint null,
        dim_storagelocationid smallint null,
        dim_salesdivisionid integer null,
        dim_shipreceivepointid integer null,
        dim_documentcategoryid integer null,
        dim_salesdocumenttypeid integer null,
        dim_salesorgid integer null,
        dim_customerid integer null,
        dim_schedulelinecategoryid smallint null,
        dim_dateidvalidfrom integer null,
        dim_dateidvalidto integer null,
        dim_salesgroupid integer null,
        dim_costcenterid smallint null,
        dim_controllingareaid integer null,
        dim_billingblockid integer null,
        dim_transactiongroupid integer null,
        dim_salesorderrejectreasonid integer null,
        dim_partid integer null,
        dim_salesorderheaderstatusid integer null,
        dd_salesdocno varchar(10) null default 'Not Set' collate ucs_basic,
        dd_salesitemno integer null default 0,
        dd_scheduleno integer null default 0
);


Insert into staging_update_702 (
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo
)
select
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_702 y
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

call vectorwise(combine 'staging_update_702');

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
Set Dim_DateidSchedDeliveryReq=ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set Dim_DateidSchedDelivery=      ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_edatu AND dd.CompanyCode = pl.CompanyCode),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_DateidGoodsIssue=    ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_wadat AND dd.CompanyCode = pl.CompanyCode),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set   Dim_DateidMtrlAvail=    ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_mbdat AND dd.CompanyCode = pl.CompanyCode),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set   Dim_DateidLoading =      ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_lddat AND dd.CompanyCode = pl.CompanyCode),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
Set Dim_DateidGuaranteedate = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set Dim_DateidTransport =      ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_tddat AND dd.CompanyCode = pl.CompanyCode),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set Dim_Currencyid = ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.Currency),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set Dim_ProductHierarchyid =        ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
Set       Dim_Plantid=     pl.Dim_Plantid 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set      Dim_Companyid =  co.Dim_Companyid 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_StorageLocationid = ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort and sl.plant = vbap_werks),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_SalesDivisionid = ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
Set     Dim_ShipReceivePointid =           ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set   Dim_DocumentCategoryid =   ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory = vbak_vbtyp),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_SalesDocumentTypeid =  ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

call vectorwise(combine 'staging_update_702');

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_SalesOrgid =  ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
Set       Dim_CustomerID=     ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set    Dim_ScheduleLineCategoryId = ifnull((SELECT Dim_ScheduleLineCategoryId
                  FROM Dim_ScheduleLineCategory slc
                  WHERE slc.ScheduleLineCategory = VBEP_ETTYP),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set   Dim_DateidValidFrom = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_DateidValidTo = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
Set      Dim_SalesGroupid =     ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set Dim_CostCenterid =1
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_ControllingAreaid = ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_BillingBlockid = ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
Set       Dim_TransactionGroupid =     ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set  Dim_SalesOrderRejectReasonid = ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set   Dim_Partid = ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_702 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set   Dim_SalesOrderHeaderStatusid = ifnull((select Dim_SalesOrderHeaderStatusid
                    from Dim_SalesOrderHeaderStatus sohs
                    where sohs.SalesDocumentNumber = VBAK_VBELN),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

/*End of temporary code split*/


update staging_update_702 sg
from  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
set Dim_CostCenterid = ifnull((SELECT Dim_CostCenterid
                    FROM Dim_CostCenter_702 cc
                    WHERE cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs AND cc.RowIsCurrent = 1
                     ),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

call vectorwise(combine 'staging_update_702');

drop table if exists staging_update_702;


create table staging_update_702 as
Select  ifnull((SELECT sois.Dim_SalesOrderItemStatusid
                      FROM Dim_SalesOrderItemStatus sois
                      WHERE sois.SalesDocumentNumber = VBAK_VBELN AND sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
            ifnull((SELECT cg1.Dim_CustomerGroup1id
                      FROM Dim_CustomerGroup1 cg1
                      WHERE cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
            ifnull((SELECT cg2.Dim_CustomerGroup2id
                      FROM Dim_CustomerGroup2 cg2
                      WHERE cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
            soic.Dim_SalesOrderItemCategoryid  Dim_salesorderitemcategoryid,
          ifnull(vbep_lfrel,'Not Set') dd_ItemRelForDelv,
	  1 Dim_ProfitCenterId ,
            ifnull((SELECT dc.Dim_DistributionChannelid
                      FROM dim_DistributionChannel dc
                      WHERE   dc.DistributionChannelCode = VBAK_VTWEG
                          AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId ,
            ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
            ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
      ifnull((SELECT r.dim_routeid from dim_route r where
                       r.RouteCode = VBAP_ROUTE and r.RowIsCurrent = 1),1) Dim_RouteId,
          ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where
                       src.SalesRiskCategory = VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER and src.RowIsCurrent = 1),1) Dim_SalesRiskCategoryId,
          'Not Set' dd_CreditRep ,
          decimal(0,18,4) dd_CreditLimit ,
          1 Dim_CustomerRiskCategoryId,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo
from fact_salesorder, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
        AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
        AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

call vectorwise(combine 'staging_update_702');

update staging_update_702 sut
from  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_702 y
set Dim_ProfitCenterId = ifnull((SELECT pc.dim_profitcenterid
                      FROM dim_profitcenter pc
                      WHERE   pc.ProfitCenterCode = VBAP_PRCTR
                          AND pc.ControllingArea = VBAK_KOKRS
                          AND pc.ValidTo >= VBAK_ERDAT
                          AND pc.RowIsCurrent = 1 ),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

drop table if exists tmp_upd_702;

Create table tmp_upd_702 as Select first 0 src.dim_salesriskcategoryid updcol1, c.CUSTOMER  updVBAK_KUNNR,src.CreditControlArea  updVBAK_KKBER
from ukmbp_cms a
      inner join BUT000 b on a.PARTNER = b.PARTNER
      inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
      inner join dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1
order by c.customer ;

update staging_update_702 sut
from  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_702 y
Set Dim_CustomerRiskCategoryId= ifnull((select updcol1 from tmp_upd_702 where updVBAK_KUNNR = VBAK_KUNNR and updVBAK_KKBER=VBAK_KKBER),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;
drop table if exists tmp_upd_702;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_SalesOrderItemStatusid=sut.Dim_SalesOrderItemStatusid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_SalesOrderItemStatusid <> sut.Dim_SalesOrderItemStatusid;

update fact_salesorder so
from staging_update_702 sut
set  so.Dim_CustomerGroup1id=sut.Dim_CustomerGroup1id
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_CustomerGroup1id <> sut.Dim_CustomerGroup1id;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_CustomerGroup2id=sut.Dim_CustomerGroup2id
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_CustomerGroup2id <> sut.Dim_CustomerGroup2id;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_salesorderitemcategoryid=sut.Dim_salesorderitemcategoryid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_salesorderitemcategoryid <> sut.Dim_salesorderitemcategoryid;

update fact_salesorder so
from staging_update_702 sut
set  so.dd_ItemRelForDelv=sut.dd_ItemRelForDelv
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.dd_ItemRelForDelv <> sut.dd_ItemRelForDelv;

update fact_salesorder so
from staging_update_702 sut
set  so.Dim_ProfitCenterId=sut.Dim_ProfitCenterId
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_ProfitCenterId <> sut.Dim_ProfitCenterId;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_DistributionChannelId=sut.Dim_DistributionChannelId
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_DistributionChannelId <> sut.Dim_DistributionChannelId;

update fact_salesorder so
from staging_update_702 sut
set so.dd_BatchNo=sut.dd_BatchNo
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.dd_BatchNo <> sut.dd_BatchNo;

update fact_salesorder so
from staging_update_702 sut
set so.dd_CreatedBy=sut.dd_CreatedBy
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.dd_CreatedBy <> sut.dd_CreatedBy;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_RouteId=sut.Dim_RouteId
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_RouteId <> sut.Dim_RouteId;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_SalesRiskCategoryId=sut.Dim_SalesRiskCategoryId
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and  so.Dim_SalesRiskCategoryId <> sut.Dim_SalesRiskCategoryId;

update fact_salesorder so
from staging_update_702 sut
set so.Dim_CustomerRiskCategoryId=sut.Dim_CustomerRiskCategoryId
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo
and so.Dim_CustomerRiskCategoryId <> sut.Dim_CustomerRiskCategoryId;

call vectorwise(combine 'fact_salesorder');

drop table if exists staging_update_702;

drop table if exists Dim_CostCenter_first1_702;
create table Dim_CostCenter_first1_702 as select  code,ControllingArea,RowIsCurrent,max(Validto) validto , cast(null,integer) dim_costcenterid from Dim_CostCenter_702  group by code,ControllingArea,RowIsCurrent;

Update Dim_CostCenter_first1_702 b
set b.dim_costcenterid=( select a.dim_costcenterid from Dim_CostCenter_702 a where b.code=a.code and b.ControllingArea=a.ControllingArea and b.RowIsCurrent=a.RowIsCurrent and b.validto=a.validto );

Drop table if exists max_holder_702;
create table max_holder_702(maxid)
as
Select ifnull(max(fact_salesorderid),0)
from fact_salesorder;

drop table if exists fact_salesorder_tmptbl;
create table fact_salesorder_tmptbl as select * from fact_salesorder where 1=2;


drop table if exists fact_salesorder_useinsub;
create table fact_salesorder_useinsub as Select dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo from fact_salesorder;

call vectorwise(combine 'fact_salesorder_useinsub');
    
INSERT INTO fact_salesorder_tmptbl(
		fact_salesorderid,
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_CorrectedQty,
              amt_UnitPrice,
              ct_PriceUnit,
              amt_ScheduleTotal,
              amt_StdCost,
              amt_TargetValue,
	      amt_Tax,
              ct_TargetQty,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              ct_OverDlvrTolerance,
              ct_UnderDlvrTolerance,
              Dim_DateidSalesOrderCreated,
              Dim_DateidFirstDate,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidSchedDelivery,
              Dim_DateidGoodsIssue,
              Dim_DateidMtrlAvail,
              Dim_DateidLoading,
              Dim_DateidTransport,
              Dim_DateidGuaranteedate,
              Dim_Currencyid,
              Dim_ProductHierarchyid,
              Dim_Plantid,
              Dim_Companyid,
              Dim_StorageLocationid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_CustomerID,
              Dim_DateidValidFrom,
              Dim_DateidValidTo,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_ControllingAreaid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_SalesOrderRejectReasonid,
              Dim_Partid,
              Dim_SalesOrderHeaderStatusid,
              Dim_SalesOrderItemStatusid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_SalesOrderItemCategoryid,
              Dim_ScheduleLineCategoryId,
              dd_ItemRelForDelv,
              Dim_ProfitCenterId,
              Dim_DistributionChannelId,
              dd_BatchNo,
              dd_CreatedBy,
	      Dim_DateidNextDate,
              Dim_RouteId,
              Dim_SalesRiskCategoryId,
              Dim_CustomerRiskCategoryId,
	      Dim_DateIdFixedValue,
	      Dim_PaymentTermsId,
	      Dim_DateIdNetDueDate
	    )
    SELECT
            max_holder_702.maxid + row_number() over(),
            vbak_vbeln dd_SalesDocNo,
            vbap_posnr dd_SalesItemNo,
            vbep_etenr dd_ScheduleNo,
            vbep_wmeng ct_ScheduleQtySalesUnit,
            vbep_bmeng ct_ConfirmedQty,
            vbep_cmeng ct_CorrectedQty,
            ifnull(Decimal((vbap_netpr * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
						where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4), 0) amt_UnitPrice,
            vbap_kpein ct_PriceUnit,
            Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0) 
			ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0) 
		   END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				  where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1),18,4) amt_ScheduleTotal,
            Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
			THEN vbap_wavwr
			ELSE 0
		  END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4) amt_StdCost,
            Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
			    THEN vbap_zwert
			    ELSE 0
			END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4) amt_TargetValue,
            decimal(0.000,18,4) amt_Tax,
            ifnull(CASE WHEN VBEP_ETENR = y._SalesSchedNo
                  THEN vbap_zmeng
                  ELSE 0
            END,0) ct_TargetQty,
            ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)  amt_ExchangeRate,
	    ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		where z.pFromCurrency  = co.Currency and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = VBAK_AUDAT AND z.pFromExchangeRate = vbak_stwae ),1)  amt_ExchangeRate_GBL,
            vbap_uebto ct_OverDlvrTolerance,
            vbap_untto ct_UnderDlvrTolerance,
            1  Dim_DateidSalesOrderCreated,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_edatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDelivery,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGoodsIssue,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMtrlAvail,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoading,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_tddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidTransport,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
            ifnull((SELECT Dim_Currencyid
                    FROM Dim_Currency cur
                    WHERE cur.CurrencyCode = co.Currency),1) Dim_Currencyid,
            ifnull((SELECT Dim_ProductHierarchyid
                    FROM Dim_ProductHierarchy ph
                    WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
            pl.Dim_Plantid,
            co.Dim_Companyid,
            ifnull((SELECT Dim_StorageLocationid
                    FROM Dim_StorageLocation sl
                    WHERE sl.LocationCode = vbap_lgort AND sl.plant = vbap_werks),1) Dim_StorageLocationid,
            ifnull((SELECT Dim_SalesDivisionid
                    FROM Dim_SalesDivision sd
                    WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
            ifnull((SELECT Dim_ShipReceivePointid
                    FROM Dim_ShipReceivePoint srp
                    WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
            ifnull((SELECT Dim_DocumentCategoryid
                    FROM Dim_DocumentCategory dc
                    WHERE dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
            ifnull((SELECT Dim_SalesDocumentTypeid
                    FROM Dim_SalesDocumentType sdt
                    WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
            ifnull((SELECT Dim_SalesOrgid
                    FROM Dim_SalesOrg so
                    WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
            ifnull((SELECT Dim_CustomerID
                    FROM Dim_Customer cust
                    WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date vf
                    WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date vt
                    WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
            ifnull((SELECT Dim_SalesGroupid
                    FROM Dim_SalesGroup sg
                    WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
            ifnull((SELECT Dim_CostCenterid
                    FROM Dim_CostCenter_first1_702 cc 
                    WHERE cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs AND cc.RowIsCurrent = 1
                    ) ,1) Dim_CostCenterid,
            ifnull((SELECT Dim_ControllingAreaid
                    FROM Dim_ControllingArea ca
                    WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
            ifnull((SELECT Dim_BillingBlockid
                    FROM Dim_BillingBlock bb
                    WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
            ifnull((SELECT Dim_TransactionGroupid
                    FROM Dim_TransactionGroup tg
                    WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
            ifnull((SELECT Dim_SalesOrderRejectReasonid
                    FROM Dim_SalesOrderRejectReason sorr
                    WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
            ifnull((SELECT dim_partid
                      FROM dim_part dp
                      WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
            ifnull((SELECT Dim_SalesOrderHeaderStatusid
                      FROM Dim_SalesOrderHeaderStatus sohs
                      WHERE sohs.SalesDocumentNumber = VBAK_VBELN),1) Dim_SalesOrderHeaderStatusid,
            ifnull((SELECT Dim_SalesOrderItemStatusid
                      FROM Dim_SalesOrderItemStatus sois
                      WHERE sois.SalesDocumentNumber = VBAK_VBELN AND sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
            ifnull((SELECT Dim_CustomerGroup1id
                      FROM Dim_CustomerGroup1 cg1
                      WHERE cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
            ifnull((SELECT Dim_CustomerGroup2id
                      FROM Dim_CustomerGroup2 cg2
                      WHERE cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
            ifnull((SELECT soic.Dim_SalesOrderItemCategoryid
                      FROM dim_salesorderitemcategory soic
                      WHERE soic.SalesOrderItemCategory = VBAP_PSTYV),1) Dim_SalesOrderItemCategoryid,
            ifnull((SELECT slc.Dim_ScheduleLineCategoryId
                    FROM Dim_ScheduleLineCategory slc
                    WHERE slc.ScheduleLineCategory = VBEP_ETTYP),1) Dim_ScheduleLineCategoryId,
            ifnull(vbep_lfrel,'Not Set') dd_ItemRelForDelv,
            1 Dim_ProfitCenterId,
            ifnull((SELECT dc.Dim_DistributionChannelid
                      FROM dim_DistributionChannel dc
                      WHERE   dc.DistributionChannelCode = VBAK_VTWEG
                          AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
            ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
            ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
	ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
	 ifnull((SELECT r.dim_routeid from dim_route r where
                       r.RouteCode = VBAP_ROUTE and r.RowIsCurrent = 1),1),
	  ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where
                       src.SalesRiskCategory = VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER and src.RowIsCurrent = 1),1),
	1 Dim_CustomerRiskCategoryId,
	1 Dim_DateIdFixedValue,
	1 Dim_PaymentTermsId,
	1 Dim_DateIdNetDueDate
    FROM max_holder_702,VBAK_VBAP_VBEP
        INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
        INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
	INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
        INNER JOIN VBAK_VBAP_VBEP_702 y
                ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
    WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder_useinsub f
                      WHERE f.dd_SalesDocNo = VBAK_VBELN AND f.dd_SalesItemNo = VBAP_POSNR AND f.dd_ScheduleNo = VBEP_ETENR)
          AND EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1);

call vectorwise(combine 'fact_salesorder_tmptbl');

UPDATE fact_salesorder_tmptbl fo
FROM VBAK_VBAP_VBEP
        INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
        INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
	INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
SET amt_Tax  = ifnull(Decimal(((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0) = 0 THEN 1
					WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
					THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							WHEN 0 THEN 1
							ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
						END
					ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
				 END) 
			* (vbep_bmeng * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
						where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1))),18,4), 0) 
where   fo.dd_SalesDocNo = VBAK_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = VBEP_ETENR;

UPDATE fact_salesorder_tmptbl fo
 FROM VBAK_VBAP_VBEP
  SET 	amt_unitpriceuom = Decimal((CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END) * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				  where z.pFromCurrency  = VBAP_WAERK and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1),18,4)
where   fo.dd_SalesDocNo = VBAK_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = VBEP_ETENR
    AND (vbap_netpr > 0 OR VBAP_NETWR > 0);

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP_VBEP, Dim_UnitOfMeasure uom
   SET  Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAK_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = VBEP_ETENR
    AND VBAP_KMEIN IS NOT NULL
    AND uom.UOM = vbap_kmein
    AND uom.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP_VBEP
   SET  Dim_UnitOfMeasureId = 1
where   fo.dd_SalesDocNo = VBAK_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = VBEP_ETENR
    AND VBAP_KMEIN IS NULL;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP_VBEP, Dim_UnitOfMeasure uom
   SET  Dim_BaseUoMid =  uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAK_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = VBEP_ETENR
    AND vbap_meins IS NOT NULL
    AND uom.UOM = vbap_meins
    AND uom.RowIsCurrent = 1;

    Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP_VBEP
   SET  Dim_BaseUoMid =  1
where   fo.dd_SalesDocNo = VBAK_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = VBEP_ETENR
    AND vbap_meins IS NULL;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP_VBEP, Dim_UnitOfMeasure uom
   SET  Dim_SalesUoMid = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAK_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = VBEP_ETENR
    AND vbap_vrkme IS NOT NULL
    AND uom.UOM = vbap_vrkme
    AND uom.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP_VBEP
   SET  Dim_SalesUoMid = 1
where   fo.dd_SalesDocNo = VBAK_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = VBEP_ETENR
    AND vbap_vrkme IS NULL;

call vectorwise (combine 'fact_salesorder_tmptbl');

drop table if exists tmp_upd_702;

Create table tmp_upd_702 as 
Select first 0 src.dim_salesriskcategoryid updcol1, c.CUSTOMER  updVBAK_KUNNR,src.CreditControlArea  updVBAK_KKBER
from ukmbp_cms a
      inner join BUT000 b on a.PARTNER = b.PARTNER
      inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
      inner join dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1
order by c.customer  ;

update fact_salesorder_tmptbl fst
from VBAK_VBAP_VBEP
        INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
        INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
        INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
        INNER JOIN VBAK_VBAP_VBEP_702 y ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
set Dim_CustomerRiskCategoryId= ifnull((select updcol1 from tmp_upd_702 where updVBAK_KUNNR = VBAK_KUNNR and updVBAK_KKBER=VBAK_KKBER),1)
    WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder_useinsub f
                      WHERE f.dd_SalesDocNo = VBAK_VBELN AND f.dd_SalesItemNo = VBAP_POSNR AND f.dd_ScheduleNo = VBEP_ETENR)
          AND EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
AND fst.dd_SalesDocNo = VBAK_VBELN AND fst.dd_SalesItemNo = VBAP_POSNR AND fst.dd_ScheduleNo = VBEP_ETENR;

drop table if exists tmp_upd_702;

update fact_salesorder_tmptbl fst
from VBAK_VBAP_VBEP
        INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
        INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
        INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
        INNER JOIN VBAK_VBAP_VBEP_702 y ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
set Dim_DateidSalesOrderCreated = ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1)
    WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder_useinsub f
                      WHERE f.dd_SalesDocNo = VBAK_VBELN AND f.dd_SalesItemNo = VBAP_POSNR AND f.dd_ScheduleNo = VBEP_ETENR)
          AND EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
AND fst.dd_SalesDocNo = VBAK_VBELN AND fst.dd_SalesItemNo = VBAP_POSNR AND fst.dd_ScheduleNo = VBEP_ETENR;


Update fact_salesorder_tmptbl fst
From VBAK_VBAP_VBEP
        INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
        INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
        INNER JOIN VBAK_VBAP_VBEP_702  y ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
set Dim_ProfitCenterId = ifnull((SELECT pc.dim_profitcenterid
                      FROM dim_profitcenter pc
                      WHERE   pc.ProfitCenterCode = VBAP_PRCTR
                          AND pc.ControllingArea = VBAK_KOKRS
                          AND pc.ValidTo >= VBAK_ERDAT
                          AND pc.RowIsCurrent = 1 ),1)
WHERE  NOT EXISTS (SELECT 1 FROM fact_salesorder_useinsub f
                      WHERE f.dd_SalesDocNo = VBAK_VBELN AND f.dd_SalesItemNo = VBAP_POSNR AND f.dd_ScheduleNo = VBEP_ETENR)
          AND EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
AND fst.dd_SalesDocNo = VBAK_VBELN AND fst.dd_SalesItemNo = VBAP_POSNR AND fst.dd_ScheduleNo = VBEP_ETENR;

drop table if exists fact_salesorder_useinsub;

call vectorwise (combine 'fact_salesorder+fact_salesorder_tmptbl');
drop table if exists fact_salesorder_tmptbl;
drop table if exists staging_upd_702;
create table staging_upd_702 as 
select    vbap_kwmeng ct_ScheduleQtySalesUnit,
          vbap_kbmeng ct_ConfirmedQty ,
          0.0000 ct_CorrectedQty ,
          ifnull(Decimal((vbap_netpr * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4), 0) amt_UnitPrice,
          vbap_kpein ct_PriceUnit,
          decimal((vbap_netwr * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4) amt_ScheduleTotal,
          ifnull(decimal((vbap_wavwr * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK  and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4),0) amt_StdCost,
          ifnull(decimal((vbap_zwert * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4),0) amt_TargetValue,
          ifnull(decimal(vbap_mwsbp * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1),18,4), 0) amt_Tax,
          vbap_zmeng ct_TargetQty,
          ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)  amt_ExchangeRate,
	  ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		where z.pFromCurrency  = co.Currency and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = VBAK_AUDAT AND z.pFromExchangeRate = vbak_stwae ),1)  amt_ExchangeRate_GBL,
          vbap_uebto ct_OverDlvrTolerance,
          vbap_untto ct_UnderDlvrTolerance,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          1 Dim_DateidSchedDelivery,
          1 Dim_DateidGoodsIssue,
          1 Dim_DateidMtrlAvail,
          1 Dim_DateidLoading,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
          1 Dim_DateidTransport,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.Currency),1) Dim_Currencyid,
          ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid Dim_Plantid,
         co.Dim_Companyid Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort and sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          1 Dim_ScheduleLineCategoryId,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
	  1 Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((select Dim_SalesOrderHeaderStatusid
                    from Dim_SalesOrderHeaderStatus sohs
                    where sohs.SalesDocumentNumber = VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
          ifnull((select sois.Dim_SalesOrderItemStatusid
                    from Dim_SalesOrderItemStatus sois
                    where sois.SalesDocumentNumber = VBAP_VBELN and sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
          ifnull((select cg1.Dim_CustomerGroup1id
                    from Dim_CustomerGroup1 cg1
                    where cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
          ifnull((select cg2.Dim_CustomerGroup2id
                    from Dim_CustomerGroup2 cg2
                    where cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
          soic.Dim_SalesOrderItemCategoryid Dim_salesorderitemcategoryid,
          'Not Set' dd_ItemRelForDelv,
	  1 Dim_ProfitCenterId,
          ifnull((select dc.Dim_DistributionChannelid
                    from dim_DistributionChannel dc
                    where   dc.DistributionChannelCode = VBAK_VTWEG
                        AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
          ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
          ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
          ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
          ifnull((SELECT r.dim_routeid from dim_route r where
                       r.RouteCode = VBAP_ROUTE and r.RowIsCurrent = 1),1) Dim_RouteId,
          ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where
                       src.SalesRiskCategory = VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER and src.RowIsCurrent = 1),1) Dim_SalesRiskCategoryId,
	   'Not Set' dd_CreditRep,
	     decimal(0,18,4) dd_CreditLimit,
	    1 Dim_CustomerRiskCategoryId,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo,
	Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				  where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1),18,4) amt_UnitPriceUoM,
	1 Dim_DateIdFixedValue,
	1 Dim_PaymentTermsId,
	1 Dim_DateIdNetDueDate
From fact_salesorder so,
          VBAK_VBAP,
          dim_SalesOrderItemCategory soic,
          Dim_Plant pl,
          Dim_Company co
  WHERE pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAP_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo AND
      soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 AND dd_ScheduleNo = 0;

drop table if exists tmp_upd_702;
create table tmp_upd_702 as
SELECT first 0 Dim_CostCenterid,Code,ControllingArea,RowIsCurrent
  FROM Dim_CostCenter cc
ORDER BY cc.ValidTo;

Update staging_upd_702 tu
From VBAK_VBAP,
          dim_SalesOrderItemCategory soic,
          Dim_Plant pl,
          Dim_Company co
set Dim_CostCenterid=ifnull((SELECT Dim_CostCenterid from tmp_upd_702 cc
				WHERE cc.Code = vbak_kostl and cc.ControllingArea = vbak_kokrs and cc.RowIsCurrent = 1),1)
where  pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAP_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo AND
      soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 AND dd_ScheduleNo = 0;

Update staging_upd_702 tu
From VBAK_VBAP,
          dim_SalesOrderItemCategory soic,
          Dim_Plant pl,
          Dim_Company co
set Dim_ProfitCenterId=ifnull((SELECT pc.dim_profitcenterid
                      FROM dim_profitcenter pc
                      WHERE   pc.ProfitCenterCode = VBAP_PRCTR
                          AND pc.ControllingArea = VBAK_KOKRS
                          AND pc.ValidTo >= VBAK_ERDAT
                          AND pc.RowIsCurrent = 1 ),1)
where  pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAP_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo AND
      soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 AND dd_ScheduleNo = 0;

drop table if exists tmp_upd_702;
			   
Create table tmp_upd_702 as Select first 0 src.dim_salesriskcategoryid updcol1, c.CUSTOMER  updVBAK_KUNNR,src.CreditControlArea  updVBAK_KKBER
from ukmbp_cms a
      inner join BUT000 b on a.PARTNER = b.PARTNER
      inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
      inner join dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1
order by c.customer  ;

Update staging_upd_702 tu
From VBAK_VBAP,
          dim_SalesOrderItemCategory soic,
          Dim_Plant pl,
          Dim_Company co
set Dim_CustomerRiskCategoryId= ifnull((select updcol1 from tmp_upd_702 where updVBAK_KUNNR = VBAK_KUNNR and updVBAK_KKBER=VBAK_KKBER),1)
where  pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAP_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo AND
      soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 AND dd_ScheduleNo = 0;

drop table if exists tmp_upd_702;

Update fact_salesorder fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND VBAP_KMEIN IS NOT NULL
    AND uom.UOM = vbap_kmein
    AND uom.RowIsCurrent = 1;

Update fact_salesorder fo
  FROM VBAK_VBAP
   SET  Dim_UnitOfMeasureId = 1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND VBAP_KMEIN IS NULL;

Update fact_salesorder fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_BaseUoMid =  uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_meins IS NOT NULL
    AND uom.UOM = vbap_meins
    AND uom.RowIsCurrent = 1;

    Update fact_salesorder fo
  FROM VBAK_VBAP
   SET  Dim_BaseUoMid =  1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_meins IS NULL;

Update fact_salesorder fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_SalesUoMid = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_vrkme IS NOT NULL
    AND uom.UOM = vbap_vrkme
    AND uom.RowIsCurrent = 1;

Update fact_salesorder fo
  FROM VBAK_VBAP
   SET  Dim_SalesUoMid = 1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_vrkme IS NULL;

call vectorwise (combine 'fact_salesorder');

Update fact_salesorder so 
from staging_upd_702 su 
set so.ct_ScheduleQtySalesUnit=su.ct_ScheduleQtySalesUnit 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.ct_ScheduleQtySalesUnit <> su.ct_ScheduleQtySalesUnit ;

Update fact_salesorder so 
from staging_upd_702 su 
set so.ct_ConfirmedQty =su.ct_ConfirmedQty  
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.ct_ConfirmedQty <> su.ct_ConfirmedQty;

Update fact_salesorder so 
from staging_upd_702 su 
set so.ct_CorrectedQty =su.ct_CorrectedQty  
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.ct_CorrectedQty <> su.ct_CorrectedQty ;

Update fact_salesorder so 
from staging_upd_702 su 
set so.amt_UnitPrice=su.amt_UnitPrice 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.amt_UnitPrice <> su.amt_UnitPrice ;

Update fact_salesorder so 
from staging_upd_702 su 
set so.ct_PriceUnit=su.ct_PriceUnit
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.ct_PriceUnit <> su.ct_PriceUnit;


Update fact_salesorder so 
from staging_upd_702 su 
set so.amt_ScheduleTotal=su.amt_ScheduleTotal 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.amt_ScheduleTotal <> su.amt_ScheduleTotal;

Update fact_salesorder so 
from staging_upd_702 su 
set so.amt_StdCost=su.amt_StdCost 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.amt_StdCost <> su.amt_StdCost;

Update fact_salesorder so 
from staging_upd_702 su 
set so.amt_TargetValue=su.amt_TargetValue 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.amt_TargetValue <> su.amt_TargetValue;

Update fact_salesorder so 
from staging_upd_702 su 
set so.amt_Tax=su.amt_Tax 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.amt_Tax <> su.amt_Tax;

Update fact_salesorder so 
from staging_upd_702 su 
set so.ct_TargetQty=su.ct_TargetQty 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.ct_TargetQty <> su.ct_TargetQty;

Update fact_salesorder so 
from staging_upd_702 su 
set so.amt_ExchangeRate=su.amt_ExchangeRate 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.amt_ExchangeRate <> su.amt_ExchangeRate;

Update fact_salesorder so 
from staging_upd_702 su 
set so.amt_ExchangeRate_GBL=ifnull(su.amt_ExchangeRate_GBL ,1)
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.amt_ExchangeRate_GBL <> ifnull(su.amt_ExchangeRate_GBL ,1);

Update fact_salesorder so 
from staging_upd_702 su 
set so.ct_OverDlvrTolerance=su.ct_OverDlvrTolerance 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.ct_OverDlvrTolerance <> su.ct_OverDlvrTolerance;

Update fact_salesorder so 
from staging_upd_702 su 
set so.ct_UnderDlvrTolerance=su.ct_UnderDlvrTolerance 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.ct_UnderDlvrTolerance <> su.ct_UnderDlvrTolerance;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidSalesOrderCreated=su.Dim_DateidSalesOrderCreated 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidSalesOrderCreated <> su.Dim_DateidSalesOrderCreated;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidFirstDate=su.Dim_DateidFirstDate 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidFirstDate <> su.Dim_DateidFirstDate;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidSchedDeliveryReq=su.Dim_DateidSchedDeliveryReq 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidSchedDeliveryReq <> su.Dim_DateidSchedDeliveryReq ;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidSchedDelivery=su.Dim_DateidSchedDelivery 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidSchedDelivery <> su.Dim_DateidSchedDelivery;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidGoodsIssue=su.Dim_DateidGoodsIssue 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidGoodsIssue <> su.Dim_DateidGoodsIssue;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidMtrlAvail=su.Dim_DateidMtrlAvail 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidMtrlAvail <> su.Dim_DateidMtrlAvail;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidLoading=su.Dim_DateidLoading 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidLoading <> su.Dim_DateidLoading;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidGuaranteedate=su.Dim_DateidGuaranteedate 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidGuaranteedate <> su.Dim_DateidGuaranteedate;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidTransport=su.Dim_DateidTransport 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidTransport <> su.Dim_DateidTransport;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_Currencyid=su.Dim_Currencyid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_Currencyid <> su.Dim_Currencyid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_ProductHierarchyid=su.Dim_ProductHierarchyid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_ProductHierarchyid <> su.Dim_ProductHierarchyid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_Plantid=su.Dim_Plantid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_Plantid <> su.Dim_Plantid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_Companyid=su.Dim_Companyid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_Companyid <> su.Dim_Companyid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_StorageLocationid=su.Dim_StorageLocationid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and  so.Dim_StorageLocationid <> su.Dim_StorageLocationid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesDivisionid=su.Dim_SalesDivisionid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesDivisionid <> su.Dim_SalesDivisionid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_ShipReceivePointid=su.Dim_ShipReceivePointid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesDivisionid <> su.Dim_SalesDivisionid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DocumentCategoryid=su.Dim_DocumentCategoryid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DocumentCategoryid <> su.Dim_DocumentCategoryid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesDocumentTypeid=su.Dim_SalesDocumentTypeid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesDocumentTypeid <> su.Dim_SalesDocumentTypeid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesOrgid=su.Dim_SalesOrgid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesOrgid <> su.Dim_SalesOrgid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_CustomerID=su.Dim_CustomerID 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_CustomerID <> su.Dim_CustomerID;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_ScheduleLineCategoryId=su.Dim_ScheduleLineCategoryId 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_ScheduleLineCategoryId <> su.Dim_ScheduleLineCategoryId;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidValidFrom=su.Dim_DateidValidFrom 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidValidFrom <> su.Dim_DateidValidFrom;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidValidTo=su.Dim_DateidValidTo 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidValidTo <> su.Dim_DateidValidTo;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesGroupid=su.Dim_SalesGroupid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesGroupid <> su.Dim_SalesGroupid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_CostCenterid=su.Dim_CostCenterid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_CostCenterid <> su.Dim_CostCenterid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_ControllingAreaid=su.Dim_ControllingAreaid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_ControllingAreaid <> su.Dim_ControllingAreaid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_BillingBlockid=su.Dim_BillingBlockid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_BillingBlockid <> su.Dim_BillingBlockid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_TransactionGroupid=su.Dim_TransactionGroupid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_TransactionGroupid <> su.Dim_TransactionGroupid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesOrderRejectReasonid=su.Dim_SalesOrderRejectReasonid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesOrderRejectReasonid <> su.Dim_SalesOrderRejectReasonid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_Partid=su.Dim_Partid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_Partid <> su.Dim_Partid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesOrderHeaderStatusid=su.Dim_SalesOrderHeaderStatusid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesOrderHeaderStatusid <> su.Dim_SalesOrderHeaderStatusid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesOrderItemStatusid=su.Dim_SalesOrderItemStatusid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesOrderItemStatusid <> su.Dim_SalesOrderItemStatusid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_CustomerGroup1id=su.Dim_CustomerGroup1id 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_CustomerGroup1id <> su.Dim_CustomerGroup1id;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_CustomerGroup2id=su.Dim_CustomerGroup2id 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_CustomerGroup2id <> su.Dim_CustomerGroup2id;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_salesorderitemcategoryid=su.Dim_salesorderitemcategoryid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_salesorderitemcategoryid <> su.Dim_salesorderitemcategoryid;

Update fact_salesorder so 
from staging_upd_702 su 
set so.dd_ItemRelForDelv=su.dd_ItemRelForDelv 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.dd_ItemRelForDelv <> su.dd_ItemRelForDelv;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_ProfitCenterId=su.Dim_ProfitCenterId 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_ProfitCenterId <> su.Dim_ProfitCenterId;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DistributionChannelId=su.Dim_DistributionChannelId 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DistributionChannelId <> su.Dim_DistributionChannelId;

Update fact_salesorder so 
from staging_upd_702 su 
set so.dd_BatchNo=su.dd_BatchNo 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.dd_BatchNo <> su.dd_BatchNo;

Update fact_salesorder so 
from staging_upd_702 su 
set so.dd_CreatedBy=su.dd_CreatedBy 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.dd_CreatedBy <> su.dd_CreatedBy;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_DateidNextDate=su.Dim_DateidNextDate 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_DateidNextDate <> su.Dim_DateidNextDate;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_RouteId=su.Dim_RouteId 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_RouteId <> su.Dim_RouteId;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesRiskCategoryId=su.Dim_SalesRiskCategoryId 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesRiskCategoryId <> su.Dim_SalesRiskCategoryId;

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_CustomerRiskCategoryId=su.Dim_CustomerRiskCategoryId 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_CustomerRiskCategoryId <> su.Dim_CustomerRiskCategoryId;

/* Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_UnitOfMeasureId=su.Dim_UnitOfMeasureId 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_UnitOfMeasureId <> su.Dim_UnitOfMeasureId

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_BaseUoMid=su.Dim_BaseUoMid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_BaseUoMid <> su.Dim_BaseUoMid

Update fact_salesorder so 
from staging_upd_702 su 
set so.Dim_SalesUoMid=su.Dim_SalesUoMid 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.Dim_SalesUoMid <> su.Dim_SalesUoMid */

Update fact_salesorder so 
from staging_upd_702 su 
set so.amt_UnitPriceUoM=su.amt_UnitPriceUoM 
where so.dd_SalesDocNo=su.dd_SalesDocNo and so.dd_SalesItemNo=su.dd_SalesItemNo and so.dd_ScheduleNo = su.dd_ScheduleNo
and so.amt_UnitPriceUoM <> su.amt_UnitPriceUoM;

call vectorwise(combine 'fact_salesorder');

Drop table if exists max_holder_702;
Create table max_holder_702(maxid)
as
Select ifnull(max(fact_salesorderid),0)
from fact_salesorder;

drop table if exists fact_salesorder_tmptbl;
create table fact_salesorder_tmptbl as select * from fact_salesorder where 1=2;

drop table if exists fact_salesorder_useinsub;
create table fact_salesorder_useinsub as Select dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo from fact_salesorder;

call vectorwise(combine 'fact_salesorder_useinsub');

drop table if exists vbak_vbap_vbep_useinsub;
create table vbak_vbap_vbep_useinsub as SELECT VBAK_VBELN,VBAP_POSNR FROM vbak_vbap_vbep ;

call vectorwise(combine 'vbak_vbap_vbep_useinsub');

INSERT
    INTO fact_salesorder_tmptbl(
	    fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
            Dim_routeid,
            Dim_SalesRiskCategoryId,
            Dim_CustomerRiskCategoryId)
  SELECT
	max_holder_702.maxid + row_number() over(),
          vbap_vbeln dd_SalesDocNo,
          vbap_posnr dd_SalesItemNo,
          0 dd_ScheduleNo,
          vbap_kwmeng ct_ScheduleQtySalesUnit,
          vbap_kbmeng ct_ConfirmedQty,
          0.0000 ct_CorrectedQty,
          ifnull(Decimal((vbap_netpr * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4), 0) amt_UnitPrice,
          vbap_kpein ct_PriceUnit,
          decimal((vbap_netwr * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4) amt_ScheduleTotal,
          ifnull(decimal((vbap_wavwr * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4),0) amt_StdCost,
          ifnull(decimal((vbap_zwert * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4),0) amt_TargetValue,
          ifnull(decimal(vbap_mwsbp * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
					where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1),18,4), 0) amt_Tax,
          vbap_zmeng ct_TargetQty,
          ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)  amt_ExchangeRate,
	  ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		where z.pFromCurrency  = co.Currency and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = VBAK_AUDAT AND z.pFromExchangeRate = vbak_stwae ),1)  amt_ExchangeRate_GBL,
          vbap_uebto ct_OverDlvrTolerance,
          vbap_untto ct_UnderDlvrTolerance,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
          1 Dim_DateidSchedDelivery,
          1 Dim_DateidGoodsIssue,
          1 Dim_DateidMtrlAvail,
          1 Dim_DateidLoading,
          1 Dim_DateidTransport,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.Currency),1) Dim_Currencyid,
          ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid,
          co.Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort and sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
                  1 Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((select Dim_SalesOrderHeaderStatusid
                    from Dim_SalesOrderHeaderStatus sohs
                    where sohs.SalesDocumentNumber = VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
          ifnull((select Dim_SalesOrderItemStatusid
                    from Dim_SalesOrderItemStatus sois
                    where sois.SalesDocumentNumber = VBAP_VBELN and sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
          ifnull((select Dim_CustomerGroup1id
                    from Dim_CustomerGroup1 cg1
                    where cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
          ifnull((select Dim_CustomerGroup2id
                    from Dim_CustomerGroup2 cg2
                    where cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
          soic.Dim_SalesOrderItemCategoryid Dim_SalesOrderItemCategoryid,
          1 Dim_ScheduleLineCategoryId,
          'Not Set' dd_ItemRelForDelv,
	1 Dim_ProfitCenterId,
          ifnull((select dc.Dim_DistributionChannelid
                    from dim_DistributionChannel dc
                    where   dc.DistributionChannelCode = VBAK_VTWEG
                        AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
          ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
          ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
          ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
           ifnull((SELECT r.dim_routeid from dim_route r where
                       r.RouteCode = VBAP_ROUTE and r.RowIsCurrent = 1),1),
          ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where
                       src.SalesRiskCategory = VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER and src.RowIsCurrent = 1),1),
            1 Dim_CustomerRiskCategoryId
  FROM max_holder_702,VBAK_VBAP
      inner join Dim_Plant pl on pl.PlantCode = VBAP_WERKS
      inner join Dim_Company co on co.CompanyCode = pl.CompanyCode
      INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
  WHERE not exists (select 1 from fact_salesorder_useinsub f
                    where f.dd_SalesDocNo = VBAP_VBELN and f.dd_SalesItemNo = VBAP_POSNR and f.dd_ScheduleNo = 0)
        and exists (select 1 from dim_date mdt where mdt.DateValue = vbap_erdat and mdt.Dim_Dateid > 1)
        AND NOT EXISTS ( SELECT 1 FROM vbak_vbap_vbep_useinsub v WHERE vbak_vbap.VBAP_VBELN = v.VBAK_VBELN
                                                         AND vbak_vbap.VBAP_POSNR = v.VBAP_POSNR);


Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  amt_UnitPriceUoM = Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				  where z.pFromCurrency  = VBAP_WAERK and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1),18,4) 
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND (vbap_netpr > 0 or vbap_netwr > 0);

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND VBAP_KMEIN IS NOT NULL
    AND uom.UOM = vbap_kmein
    AND uom.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  Dim_UnitOfMeasureId = 1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND VBAP_KMEIN IS NULL;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_BaseUoMid =  uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_meins IS NOT NULL
    AND uom.UOM = vbap_meins
    AND uom.RowIsCurrent = 1;

    Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  Dim_BaseUoMid =  1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_meins IS NULL;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_SalesUoMid = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_vrkme IS NOT NULL
    AND uom.UOM = vbap_vrkme
    AND uom.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  Dim_SalesUoMid = 1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_vrkme IS NULL;

call vectorwise (combine 'fact_salesorder_tmptbl');


drop table if exists tmp_upd_702;

Create table tmp_upd_702 as Select first 0 src.dim_salesriskcategoryid updcol1, c.CUSTOMER  updVBAK_KUNNR,src.CreditControlArea  updVBAK_KKBER
from ukmbp_cms a
      inner join BUT000 b on a.PARTNER = b.PARTNER
      inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
      inner join dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1
order by c.customer  ;

update fact_salesorder_tmptbl fst
From VBAK_VBAP
      inner join Dim_Plant pl on pl.PlantCode = VBAP_WERKS
      inner join Dim_Company co on co.CompanyCode = pl.CompanyCode
      INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
set Dim_CustomerRiskCategoryId= ifnull((select updcol1 from tmp_upd_702 where updVBAK_KUNNR = VBAK_KUNNR and updVBAK_KKBER=VBAK_KKBER),1)
  WHERE not exists (select 1 from fact_salesorder_useinsub f
                    where f.dd_SalesDocNo = VBAP_VBELN and f.dd_SalesItemNo = VBAP_POSNR and f.dd_ScheduleNo = 0)
        and exists (select 1 from dim_date mdt where mdt.DateValue = vbap_erdat and mdt.Dim_Dateid > 1)
        AND NOT EXISTS ( SELECT 1 FROM vbak_vbap_vbep_useinsub v WHERE vbak_vbap.VBAP_VBELN = v.VBAK_VBELN
                                                         AND vbak_vbap.VBAP_POSNR = v.VBAP_POSNR)
And fst.dd_SalesDocNo = VBAP_VBELN and fst.dd_SalesItemNo = VBAP_POSNR and  fst.dd_ScheduleNo =0;


drop table if exists tmp_upd_702;

create table tmp_upd_702 as
SELECT first 0 Dim_CostCenterid,Code,ControllingArea,RowIsCurrent
  FROM Dim_CostCenter cc
ORDER BY cc.ValidTo;

update fact_salesorder_tmptbl fst
From VBAK_VBAP
      inner join Dim_Plant pl on pl.PlantCode = VBAP_WERKS
      inner join Dim_Company co on co.CompanyCode = pl.CompanyCode
      INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
set  Dim_CostCenterid=ifnull((SELECT Dim_CostCenterid from tmp_upd_702 cc
                                WHERE cc.Code = vbak_kostl and cc.ControllingArea = vbak_kokrs and cc.RowIsCurrent = 1),1)
  WHERE not exists (select 1 from fact_salesorder_useinsub f
                    where f.dd_SalesDocNo = VBAP_VBELN and f.dd_SalesItemNo = VBAP_POSNR and f.dd_ScheduleNo = 0)
        and exists (select 1 from dim_date mdt where mdt.DateValue = vbap_erdat and mdt.Dim_Dateid > 1)
        AND NOT EXISTS ( SELECT 1 FROM vbak_vbap_vbep_useinsub v WHERE vbak_vbap.VBAP_VBELN = v.VBAK_VBELN
                                                         AND vbak_vbap.VBAP_POSNR = v.VBAP_POSNR)
And fst.dd_SalesDocNo = VBAP_VBELN and fst.dd_SalesItemNo = VBAP_POSNR and  fst.dd_ScheduleNo =0;


drop table if exists tmp_upd_702;


update fact_salesorder_tmptbl fst
From VBAK_VBAP
      inner join Dim_Plant pl on pl.PlantCode = VBAP_WERKS
      inner join Dim_Company co on co.CompanyCode = pl.CompanyCode
      INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
set  Dim_ProfitCenterId=ifnull((SELECT pc.dim_profitcenterid
                      FROM dim_profitcenter pc
                      WHERE   pc.ProfitCenterCode = VBAP_PRCTR
                          AND pc.ControllingArea = VBAK_KOKRS
                          AND pc.ValidTo >= VBAK_ERDAT
                          AND pc.RowIsCurrent = 1 ),1)
  WHERE not exists (select 1 from fact_salesorder_useinsub f
                    where f.dd_SalesDocNo = VBAP_VBELN and f.dd_SalesItemNo = VBAP_POSNR and f.dd_ScheduleNo = 0)
        and exists (select 1 from dim_date mdt where mdt.DateValue = vbap_erdat and mdt.Dim_Dateid > 1)
        AND NOT EXISTS ( SELECT 1 FROM vbak_vbap_vbep_useinsub v WHERE vbak_vbap.VBAP_VBELN = v.VBAK_VBELN
                                                         AND vbak_vbap.VBAP_POSNR = v.VBAP_POSNR)
And fst.dd_SalesDocNo = VBAP_VBELN and fst.dd_SalesItemNo = VBAP_POSNR and  fst.dd_ScheduleNo =0;



drop table if exists fact_salesorder_useinsub;

drop table if exists vbak_vbap_vbep_useinsub;

call vectorwise (combine 'fact_salesorder+fact_salesorder_tmptbl');
call vectorwise(combine 'fact_salesorder');
drop table if exists fact_salesorder_tmptbl;

  UPDATE fact_salesorder fso
From Dim_SalesMisc smisc, VBAK_VBAP vkp
     SET fso.Dim_SalesMiscId = smisc.Dim_SalesMiscId
  WHERE     fso.dd_SalesDocNo = vkp.VBAP_VBELN
         AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
         AND fso.dd_ScheduleNo = 0
         AND smisc.DeliveryDateQuantityFixed = ifnull(VBAP_FIXMG,'Not Set')
         AND smisc.FixedQuantity = ifnull(VBAP_FMENG, 'Not Set')
         AND smisc.OverDeliveryAllowed = ifnull(VBAP_UEBTK, 'Not Set')
         AND smisc.CashDiscountIndicator = ifnull(VBAP_SKTOF, 'Not Set')
         AND smisc.ReturnsItem = ifnull(VBAP_SHKZG, 'Not Set')
         AND smisc.PricingOk = ifnull(VBAP_PRSOK, 'Not Set')
         AND smisc.CustomerNotPostedGoodsReceipt = ifnull(VBAP_NACHL, 'Not Set')
         AND smisc.ItemRelevantForDelivery = ifnull(VBAP_LFREL, 'Not Set')
         AND smisc.ScheduleConfirmStatus = 'Not Set'
         AND smisc.InvoiceReceiptIndicator = 'Not Set'
	 AND fso.Dim_SalesMiscId <> smisc.Dim_SalesMiscId;


UPDATE fact_salesorder fso
FROM VBAK_VBAP vp, Dim_PartSales sprt
SET Dim_PartSalesId = sprt.Dim_PartSalesId
where   fso.dd_SalesDocNo = vp.VBAP_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = 0
AND VBAK_VKORG IS NOT NULL
AND sprt.SalesOrgCode = VBAK_VKORG
AND sprt.PartNumber = VBAP_MATNR
AND sprt.RowIsCurrent = 1;

UPDATE fact_salesorder fso
FROM VBAK_VBAP vp
SET Dim_PartSalesId = 1
where   fso.dd_SalesDocNo = vp.VBAP_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = 0
AND VBAK_VKORG IS NULL;

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBEP vp, Dim_PartSales sprt
SET Dim_PartSalesId = sprt.Dim_PartSalesId
where   fso.dd_SalesDocNo = vp.VBAK_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = vp.VBEP_ETENR
AND VBAK_VKORG IS NOT NULL
AND sprt.SalesOrgCode = VBAK_VKORG
AND sprt.PartNumber = VBAP_MATNR
AND sprt.RowIsCurrent = 1;

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBEP vp
SET Dim_PartSalesId = 1
where   fso.dd_SalesDocNo = vp.VBAK_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = vp.VBEP_ETENR
AND VBAK_VKORG IS NULL;

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBKD vp,
     dim_date dt,
     dim_company c
SET Dim_DateIdFixedValue = dt.Dim_DateId
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND vp.VBKD_POSNR = 0
AND fso.dim_companyid = c.dim_companyid
AND VBKD_VALDT IS NOT NULL
AND dt.DateValue = VBKD_VALDT
AND dt.CompanyCode = c.CompanyCode;

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBKD vp,
     dim_date dt,
     dim_company c
SET Dim_DateIdFixedValue = dt.Dim_DateId
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND fso.dd_SalesItemNo = vp.VBKD_POSNR
AND fso.dim_companyid = c.dim_companyid
AND VBKD_VALDT IS NOT NULL
AND dt.DateValue = VBKD_VALDT
AND dt.CompanyCode = c.CompanyCode;

UPDATE fact_salesorder fso
SET Dim_DateIdFixedValue = 1
where Dim_DateIdFixedValue IS NULL;

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBKD vp,
     Dim_CustomerPaymentTerms pt,
     dim_company c
SET Dim_PaymentTermsId = pt.Dim_CustomerPaymentTermsid
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND vp.VBKD_POSNR = 0
AND VBKD_ZTERM IS NOT NULL
AND pt.PaymentTermCode = VBKD_ZTERM;

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBKD vp,
     Dim_CustomerPaymentTerms pt,
     dim_company c
SET Dim_PaymentTermsId = pt.Dim_CustomerPaymentTermsid
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND fso.dd_SalesItemNo = vp.VBKD_POSNR 
AND VBKD_ZTERM IS NOT NULL
AND pt.PaymentTermCode = VBKD_ZTERM;

UPDATE fact_salesorder fso
SET fso.Dim_PaymentTermsId = 1
where fso.Dim_PaymentTermsId  IS NULL;


UPDATE fact_salesorder fso FROM VBAK_VBAP_VBEP vp
   SET Dim_AfsSeasonId =
          ifnull(
             (SELECT sea.Dim_AfsSeasonId
                FROM Dim_AfsSeason sea
               WHERE     sea.SeasonIndicator = VBEP_J_3SEANS
                     AND sea.Theme = VBAP_AFS_THEME
                     AND sea.Collection = VBAP_AFS_COLLECTION
                     AND sea.RowIsCurrent = 1),
             1)
WHERE    fso.dd_SalesDocNo = vp.VBAK_VBELN
     AND fso.dd_SalesItemNo = vp.VBAP_POSNR
     AND fso.dd_ScheduleNo = vp.VBEP_ETENR;

UPDATE fact_salesorder fso
SET Dim_PartSalesId = 1
where   Dim_PartSalesId  IS NULL;

UPDATE fact_salesorder fso
FROM Dim_SalesMisc smisc, VBAK_VBAP_VBEP vkp
      SET fso.Dim_SalesMiscId = smisc.Dim_SalesMiscId
    WHERE     fso.dd_SalesDocNo = vkp.VBAK_VBELN
          AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
          AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
          AND smisc.DeliveryDateQuantityFixed = ifnull(VBAP_FIXMG,'Not Set')
          AND smisc.FixedQuantity = ifnull(VBAP_FMENG, 'Not Set')
          AND smisc.OverDeliveryAllowed = ifnull(VBAP_UEBTK, 'Not Set')
          AND smisc.CashDiscountIndicator = ifnull(VBAP_SKTOF, 'Not Set')
          AND smisc.ReturnsItem = ifnull(VBAP_SHKZG, 'Not Set')
          AND smisc.PricingOk = ifnull(VBAP_PRSOK, 'Not Set')
          AND smisc.CustomerNotPostedGoodsReceipt = ifnull(VBAP_NACHL, 'Not Set')
          AND smisc.ItemRelevantForDelivery = ifnull(VBAP_LFREL, 'Not Set')
          AND smisc.ScheduleConfirmStatus = ifnull(VBEP_WEPOS, 'Not Set')
          AND smisc.InvoiceReceiptIndicator = ifnull(VBEP_REPOS, 'Not Set')
	  AND fso.Dim_SalesMiscId <> smisc.Dim_SalesMiscId;


UPDATE fact_salesorder fso
FROM vbak_vbap_vbep vkp, KNVV k
SET fso.Dim_CustomerGroupId =
            ifnull(
              (SELECT cg.Dim_CustomerGroupId
                  FROM dim_CustomerGroup cg
                WHERE cg.CustomerGroup = k.knvv_kdgrp AND cg.RowIsCurrent = 1),
              1)
WHERE  fso.dd_SalesDocNo = vkp.VBAK_VBELN
              AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
              AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
              AND  k.knvv_VTWEG = vkp.VBAK_VTWEG
              AND k.knvv_SPART = vkp.VBAK_SPART
              AND k.knvv_vkorg = vkp.VBAK_VKORG
              AND k.knvv_KUNNR = vkp.VBAK_KUNNR 
		AND fso.dd_ScheduleNo <> 0 ;


UPDATE fact_salesorder fso
FROM vbak_vbap_vbep vkp, KNVV k
SET  fso.Dim_SalesOfficeId =
            ifnull(
              (SELECT so.Dim_SalesOfficeId
                  FROM dim_SalesOffice so
                WHERE so.SalesOfficeCode = k.knvv_vkbur
                      AND so.RowIsCurrent = 1),
              1)
WHERE  fso.dd_SalesDocNo = vkp.VBAK_VBELN
              AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
              AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
	      AND  k.knvv_VTWEG = vkp.VBAK_VTWEG
              AND k.knvv_SPART = vkp.VBAK_SPART
              AND k.knvv_vkorg = vkp.VBAK_VKORG
              AND k.knvv_KUNNR = vkp.VBAK_KUNNR
	 AND fso.dd_ScheduleNo <> 0 ;



UPDATE fact_salesorder fso
FROM vbak_vbap_vbep vkp, KNVV k
SET   fso.Dim_CustomerPaymentTermsId = ifnull((SELECT cpt.dim_Customerpaymenttermsid
              FROM dim_customerpaymentterms cpt
              WHERE cpt.PaymentTermCode = k.knvv_zterm
              AND cpt.RowIsCurrent = 1), 1)
WHERE  fso.dd_SalesDocNo = vkp.VBAK_VBELN
              AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
              AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
              AND  k.knvv_VTWEG = vkp.VBAK_VTWEG
              AND k.knvv_SPART = vkp.VBAK_SPART
              AND k.knvv_vkorg = vkp.VBAK_VKORG
              AND k.knvv_KUNNR = vkp.VBAK_KUNNR
         AND fso.dd_ScheduleNo <> 0 ;



UPDATE fact_salesorder fso
FROM vbak_vbap vkp,KNVV k
   SET fso.Dim_CustomerGroupId =
          ifnull(
             (SELECT cg.Dim_CustomerGroupId
                FROM dim_CustomerGroup cg
               WHERE cg.CustomerGroup = k.knvv_kdgrp AND cg.RowIsCurrent = 1),
             1)
Where  fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
		AND   k.knvv_VTWEG = vkp.VBAK_VTWEG
          AND k.knvv_SPART = vkp.VBAK_SPART
          AND k.knvv_vkorg = vkp.VBAK_VKORG
          AND k.knvv_KUNNR = vkp.VBAK_KUNNR;


UPDATE fact_salesorder fso
FROM vbak_vbap vkp,KNVV k
Set fso.Dim_SalesOfficeId =
          ifnull(
             (SELECT so.Dim_SalesOfficeId
                FROM dim_SalesOffice so
               WHERE so.SalesOfficeCode = k.knvv_vkbur
                     AND so.RowIsCurrent = 1),
             1)
Where  fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
		AND   k.knvv_VTWEG = vkp.VBAK_VTWEG
          AND k.knvv_SPART = vkp.VBAK_SPART
          AND k.knvv_vkorg = vkp.VBAK_VKORG
          AND k.knvv_KUNNR = vkp.VBAK_KUNNR;


UPDATE fact_salesorder fso
FROM vbak_vbap vkp,KNVV k
Set fso.Dim_CustomerPaymentTermsId = ifnull((SELECT cpt.dim_Customerpaymenttermsid
              FROM dim_customerpaymentterms cpt
              WHERE cpt.PaymentTermCode = k.knvv_zterm
              AND cpt.RowIsCurrent = 1), 1)
Where  fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
		AND   k.knvv_VTWEG = vkp.VBAK_VTWEG
          AND k.knvv_SPART = vkp.VBAK_SPART
          AND k.knvv_vkorg = vkp.VBAK_VKORG
          AND k.knvv_KUNNR = vkp.VBAK_KUNNR;



drop table if exists Dim_CustomerPartnerFunctions_upd;
create table Dim_CustomerPartnerFunctions_upd as 
select first 0 * from Dim_CustomerPartnerFunctions order by PartnerCounter desc;

UPDATE fact_salesorder so
from vbuk v,dim_overallstatusforcreditcheck oscc
SET so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckID
Where so.dd_SalesDocNo = v.VBUK_VBELN
AND  oscc.overallstatusforcreditcheck = v.VBUK_CMGST
AND oscc.RowIsCurrent = 1
AND v.VBUK_CMGST IS NOT NULL
AND so.Dim_OverallStatusCreditCheckId <> oscc.dim_overallstatusforcreditcheckID;

UPDATE fact_salesorder so
from vbuk v
SET so.Dim_OverallStatusCreditCheckId = 1
Where so.dd_SalesDocNo = v.VBUK_VBELN
AND v.VBUK_CMGST IS NULL;

  UPDATE fact_salesorder so
  FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
    SET so.Dim_BillToPartyPartnerFunctionId =
            ifnull(
              (SELECT Dim_CustomerPartnerFunctionsID
                  FROM Dim_CustomerPartnerFunctions_upd cpf
                WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                      AND cpf.SalesOrgCode = shi.vbak_vkorg
                      AND cpf.DivisionCode = shi.vbap_spart
                      AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                      AND cpf.PartnerFunction = sdp.vbpa_parvw
                      AND cpf.RowIsCurrent = 1
                ),
              1)
WHERE sdp.vbpa_parvw = vb.pBillToPartyPartnerFunction
AND so.dd_SalesDocNo = shi.vbak_vbeln
              AND so.dd_SalesItemNo = shi.vbap_posnr
              AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln
 AND so.dd_scheduleno <> 0;


  UPDATE       fact_salesorder so
FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
    SET so.Dim_BillToPartyPartnerFunctionId =
            ifnull(
              (SELECT Dim_CustomerPartnerFunctionsID
                  FROM Dim_CustomerPartnerFunctions_upd cpf
                WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                      AND cpf.SalesOrgCode = shi.vbak_vkorg
                      AND cpf.DivisionCode = shi.vbap_spart
                      AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                      AND cpf.PartnerFunction = sdp.vbpa_parvw
                      AND cpf.RowIsCurrent = 1
                ),
              1)
  WHERE sdp.vbpa_parvw = vb.pBillToPartyPartnerFunction
AND  so.dd_SalesDocNo = shi.vbak_vbeln
              AND so.dd_SalesItemNo = shi.vbap_posnr
              AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
 AND so.dd_scheduleno <> 0;


  UPDATE       fact_salesorder so
FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
    SET so.Dim_PayerPartnerFunctionId =
            ifnull(
              (SELECT Dim_CustomerPartnerFunctionsID
                  FROM Dim_CustomerPartnerFunctions_upd cpf
                WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                      AND cpf.SalesOrgCode = shi.vbak_vkorg
                      AND cpf.DivisionCode = shi.vbap_spart
                      AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                      AND cpf.PartnerFunction = sdp.vbpa_parvw
                      AND cpf.RowIsCurrent = 1
                ),
              1)
  WHERE sdp.vbpa_parvw = vb.pPayerPartnerFunction
AND        so.dd_SalesDocNo = shi.vbak_vbeln
              AND so.dd_SalesItemNo = shi.vbap_posnr
              AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln
 AND so.dd_scheduleno <> 0 ;


  UPDATE       fact_salesorder so
FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
    SET so.Dim_PayerPartnerFunctionId =
            ifnull(
              (SELECT Dim_CustomerPartnerFunctionsID
                  FROM Dim_CustomerPartnerFunctions_upd cpf
                WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                      AND cpf.SalesOrgCode = shi.vbak_vkorg
                      AND cpf.DivisionCode = shi.vbap_spart
                      AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                      AND cpf.PartnerFunction = sdp.vbpa_parvw
                      AND cpf.RowIsCurrent = 1
                ),
              1)
  WHERE sdp.vbpa_parvw = vb.pPayerPartnerFunction
AND  so.dd_SalesDocNo = shi.vbak_vbeln
              AND so.dd_SalesItemNo = shi.vbap_posnr
              AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
 AND so.dd_scheduleno <> 0 ;
  

        UPDATE       fact_salesorder so
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
          SET so.Dim_CustomPartnerFunctionId =
                  ifnull(
                    (SELECT Dim_CustomerPartnerFunctionsID
                        FROM Dim_CustomerPartnerFunctions_upd cpf
                      WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                            AND cpf.SalesOrgCode = shi.vbak_vkorg
                            AND cpf.DivisionCode = shi.vbap_spart
                            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                            AND cpf.PartnerFunction = sdp.vbpa_parvw
                            AND cpf.RowIsCurrent = 1
                      ),
                    1)
        WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey
	AND  so.dd_SalesDocNo = shi.vbak_vbeln
                    AND so.dd_SalesItemNo = shi.vbap_posnr
                    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND vb.pCustomPartnerFunctionKey <> 'Not Set'
	 AND so.dd_scheduleno <> 0;


        UPDATE       fact_salesorder so
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
          SET so.Dim_CustomPartnerFunctionId =
                  ifnull(
                    (SELECT Dim_CustomerPartnerFunctionsID
                        FROM Dim_CustomerPartnerFunctions_upd cpf
                      WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                            AND cpf.SalesOrgCode = shi.vbak_vkorg
                            AND cpf.DivisionCode = shi.vbap_spart
                            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                            AND cpf.PartnerFunction = sdp.vbpa_parvw
                            AND cpf.RowIsCurrent = 1
                      ),
                    1)
        WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey
	AND  so.dd_SalesDocNo = shi.vbak_vbeln
                    AND so.dd_SalesItemNo = shi.vbap_posnr
                    AND so.dd_scheduleno = shi.vbep_etenr
	AND sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
	AND vb.pCustomPartnerFunctionKey <> 'Not Set'
	 AND so.dd_scheduleno <> 0;


        UPDATE       fact_salesorder so
        FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
        SET so.Dim_CustomPartnerFunctionId1 =
              ifnull(
                (SELECT Dim_CustomerPartnerFunctionsID
                    FROM Dim_CustomerPartnerFunctions_upd cpf
                  WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                        AND cpf.SalesOrgCode = shi.vbak_vkorg
                        AND cpf.DivisionCode = shi.vbap_spart
                        AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                        AND cpf.PartnerFunction = sdp.vbpa_parvw
                        AND cpf.RowIsCurrent = 1
                  ),
                1)
        WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey1
AND  so.dd_SalesDocNo = shi.vbak_vbeln
                AND so.dd_SalesItemNo = shi.vbap_posnr
                AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln
AND vb.pCustomPartnerFunctionKey1 <> 'Not Set'
 AND so.dd_scheduleno <> 0;


        UPDATE       fact_salesorder so
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
          SET so.Dim_CustomPartnerFunctionId1 =
                  ifnull(
                    (SELECT Dim_CustomerPartnerFunctionsID
                        FROM Dim_CustomerPartnerFunctions_upd cpf
                      WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                            AND cpf.SalesOrgCode = shi.vbak_vkorg
                            AND cpf.DivisionCode = shi.vbap_spart
                            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                            AND cpf.PartnerFunction = sdp.vbpa_parvw
                            AND cpf.RowIsCurrent = 1
                      ),
                      1)
              WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey1
		AND so.dd_SalesDocNo = shi.vbak_vbeln
                    AND so.dd_SalesItemNo = shi.vbap_posnr
                    AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
AND vb.pCustomPartnerFunctionKey1 <> 'Not Set'
 AND so.dd_scheduleno <> 0;


      UPDATE       fact_salesorder so
      FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
        SET so.Dim_CustomPartnerFunctionId2 =
                ifnull(
                  (SELECT Dim_CustomerPartnerFunctionsID
                      FROM Dim_CustomerPartnerFunctions_upd cpf
                    WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                          AND cpf.SalesOrgCode = shi.vbak_vkorg
                          AND cpf.DivisionCode = shi.vbap_spart
                          AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                          AND cpf.PartnerFunction = sdp.vbpa_parvw
                          AND cpf.RowIsCurrent = 1
                    ),
                  1)
      WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey2
AND so.dd_SalesDocNo = shi.vbak_vbeln
                  AND so.dd_SalesItemNo = shi.vbap_posnr
                  AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln
AND vb.pCustomPartnerFunctionKey2 <> 'Not Set'
 AND so.dd_scheduleno <> 0;


      UPDATE       fact_salesorder so
	From vbak_vbap_vbep shi,vbpa sdp,variable_holder_702 vb
        SET so.Dim_CustomPartnerFunctionId2 =
                ifnull(
                  (SELECT Dim_CustomerPartnerFunctionsID
                      FROM Dim_CustomerPartnerFunctions_upd cpf
                    WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                          AND cpf.SalesOrgCode = shi.vbak_vkorg
                          AND cpf.DivisionCode = shi.vbap_spart
                          AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                          AND cpf.PartnerFunction = sdp.vbpa_parvw
                          AND cpf.RowIsCurrent = 1
                    ),
                  1)
WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey2
AND  so.dd_SalesDocNo = shi.vbak_vbeln
AND so.dd_SalesItemNo = shi.vbap_posnr
AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
AND  vb.pCustomPartnerFunctionKey2 <> 'Not Set'
 AND so.dd_scheduleno <> 0;


/* LK : 23-May-2013 : Fix the columbia prod issue mentioned by Shanthi */
/* This was because sub-queries in update were converted into joins, resulting in default values not being applied when one of the column has nulls or join columsn values do not match */

/* So, update the default values here first */


DROP TABLE IF EXISTS TMP_fact_salesorder_upd_default_del;
CREATE TABLE TMP_fact_salesorder_upd_default_del
AS
SELECT so.*
FROM fact_salesorder so,vbak_vbap_vbep vbk,dim_plant pl
where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <> 0;


DROP TABLE IF EXISTS TMP_fact_salesorder_upd_default_ins;
CREATE TABLE TMP_fact_salesorder_upd_default_ins
AS
SELECT * FROM TMP_fact_salesorder_upd_default_del;


UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.dd_CustomerPONo = 'Not Set'
WHERE ifnull(so.dd_CustomerPONo,'xx') <> 'Not Set';

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_CreditRepresentativeId = 1
WHERE ifnull(so.Dim_CreditRepresentativeId,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_MaterialPriceGroup1Id = 1
WHERE ifnull(so.Dim_MaterialPriceGroup1Id,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_DeliveryBlockId = 1
WHERE ifnull(so.Dim_DeliveryBlockId,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_SalesDocOrderReasonId = 1
WHERE ifnull(so.Dim_SalesDocOrderReasonId,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_MaterialGroupId = 1
WHERE ifnull(so.Dim_MaterialGroupId,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.amt_SubTotal3 = 0
WHERE ifnull(so.amt_SubTotal3,-1) <> 0;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.amt_Subtotal3_OrderQty = 0
WHERE ifnull(so.amt_Subtotal3_OrderQty,-1) <> 0;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.amt_SubTotal4 = 0
WHERE ifnull(so.amt_SubTotal4,-1) <> 0;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.ct_CumConfirmedQty = 0
WHERE ifnull(so.ct_CumConfirmedQty,-1) <> 0;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.ct_CumOrderQty = 0
WHERE ifnull(so.ct_CumOrderQty,-1) <> 0;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_PurchaseOrderTypeId = 1
WHERE ifnull(so.Dim_PurchaseOrderTypeId,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_DateIdPurchaseOrder = 1
WHERE ifnull(so.Dim_DateIdPurchaseOrder,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_DateIdQuotationValidFrom = 1
WHERE ifnull(so.Dim_DateIdQuotationValidFrom,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_DateIdQuotationValidTo = 1
WHERE ifnull(so.Dim_DateIdQuotationValidTo,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_DateIdSOCreated = 1
WHERE ifnull(so.Dim_DateIdSOCreated,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.Dim_DateIdSODocument = 1
WHERE ifnull(so.Dim_DateIdSODocument,-1) <> 1;

UPDATE TMP_fact_salesorder_upd_default_ins so
SET so.dd_ReferenceDocumentNo = 'Not Set'
WHERE ifnull(so.dd_ReferenceDocumentNo,'xx') <> 'Not Set';


call vectorwise(combine 'fact_salesorder - TMP_fact_salesorder_upd_default_del + TMP_fact_salesorder_upd_default_ins');

DROP TABLE IF EXISTS TMP_fact_salesorder_upd_default_del;
DROP TABLE IF EXISTS TMP_fact_salesorder_upd_default_ins;

/* LK : COnverted this update to single column updates and used combine to make chgs to fact_salesorder */
/*
UPDATE fact_salesorder so
FROM  vbak_vbap_vbep vbk,dim_plant pl
SET so.dd_CustomerPONo = 'Not Set',
    so.Dim_CreditRepresentativeId = 1,
    so.Dim_MaterialPriceGroup1Id = 1,
    so.Dim_DeliveryBlockId = 1,
    so.Dim_SalesDocOrderReasonId = 1,
    so.Dim_MaterialGroupId = 1,
    so.amt_SubTotal3 = 0,
    so.amt_SubTotal4 = 0,
    so.ct_CumConfirmedQty = 0,
    so.ct_CumOrderQty = 0,
    so.Dim_PurchaseOrderTypeId = 1,
    so.Dim_DateIdPurchaseOrder = 1,
    so.Dim_DateIdQuotationValidFrom = 1,
    so.Dim_DateIdQuotationValidTo = 1,
    so.Dim_DateIdSOCreated = 1,
    so.Dim_DateIdSODocument = 1,
    so.dd_ReferenceDocumentNo = 'Not Set'

where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <> 0   */


/* LK: End of Fix */


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
   SET so.dd_CustomerPONo = ifnull(vbk.VBAK_BSTNK,'Not Set')
WHERE  vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_scheduleno <> 0 
AND so.dd_CustomerPONo <> ifnull(vbk.VBAK_BSTNK,'Not Set');

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_creditrepresentativegroup cg
SET  so.Dim_CreditRepresentativeId =
ifnull( Dim_CreditRepresentativegroupId, 1)
WHERE  vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_SBGRP IS NOT NULL 
AND vbk.VBAK_KKBER IS NOT NULL
AND cg.CreditRepresentativeGroup = vbk.VBAK_SBGRP
AND cg.CreditControlArea = vbk.VBAK_KKBER
AND cg.RowIsCurrent = 1 
AND so.Dim_CreditRepresentativeId <> ifnull( Dim_CreditRepresentativegroupId, 1);

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk
SET  so.Dim_CreditRepresentativeId = 1
WHERE  vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND ( vbk.VBAK_SBGRP IS NULL OR vbk.VBAK_KKBER IS NULL );

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,Dim_MaterialPriceGroup1 mpg
SET so.Dim_MaterialPriceGroup1Id = mpg.Dim_MaterialPriceGroup1Id
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAP_MVGR1 IS NOT NULL
AND  mpg.MaterialPriceGroup1 = vbk.VBAP_MVGR1
AND mpg.RowIsCurrent = 1
AND so.Dim_MaterialPriceGroup1Id <> ifnull( mpg.Dim_MaterialPriceGroup1Id,1);

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk
SET so.Dim_MaterialPriceGroup1Id = 1
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAP_MVGR1 IS NULL;

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, Dim_DeliveryBlock db
SET so.Dim_DeliveryBlockId =  db.Dim_DeliveryBlockId
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_LIFSK IS NOT NULL
AND db.DeliveryBlock = vbk.VBAK_LIFSK
AND db.RowIsCurrent = 1 
and so.Dim_DeliveryBlockId <> db.Dim_DeliveryBlockId;

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk
SET so.Dim_DeliveryBlockId =  1
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_LIFSK IS NULL;

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_salesdocorderreason sor
SET so.Dim_SalesDocOrderReasonId =
          ifnull( sor.dim_salesdocorderreasonid, 1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND sor.ReasonCode = vbk.VBAK_AUGRU AND sor.RowIsCurrent = 1  AND so.dd_scheduleno <> 0 
and so.Dim_SalesDocOrderReasonId <> ifnull( sor.dim_salesdocorderreasonid, 1);


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_materialgroup mg
SET so.Dim_MaterialGroupId =
          ifnull( mg.dim_materialgroupid,1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND  mg.MaterialGroupCode = vbk.VBAP_MATKL
                     AND mg.RowIsCurrent = 1  AND so.dd_scheduleno <> 0 
		     AND so.Dim_MaterialGroupId <> ifnull( mg.dim_materialgroupid,1)
AND  so.Dim_MaterialGroupId <>  ifnull( mg.dim_materialgroupid,1);


UPDATE fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.amt_SubTotal3 =
          ifnull(
               (  vbk.VBAP_KZWI3
                / CASE
                     WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                          AND VBAP_NETPR > 0
                          AND VBAP_NETPR <>
                                 ROUND(
                                    (  VBAP_KPEIN
                                     * VBAP_NETWR
                                     / CASE VBAP_KWMENG
                                          WHEN 0 THEN 1
                                          ELSE VBAP_KWMENG
                                       END),
                                    4)
                     THEN
                        (CASE ifnull(
                                 Round(
                                    (  VBAP_NETWR
                                     / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) end)
                                        / (CASE
                                              WHEN vbap_kpein = 0 THEN 1
                                              ELSE vbap_kpein
                                           END))),
                                    4),
                                 1)
                            WHEN 0
                            THEN
                               1
                            ELSE
                               ifnull(
                                  Round(
                                     (  VBAP_NETWR
                                      / (  (CASE WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0
                                               THEN
                                                  1
                                               ELSE
                                                  (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END)
                                            END)
                                         / (CASE
                                               WHEN vbap_kpein = 0 THEN 1
                                               ELSE vbap_kpein
                                            END))),
                                     4),
                                  1)
                         END)
                     ELSE
                        Case when ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1) = 0 
			then 1
			Else ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1)
			ENd
                  END)
             * vbep_bmeng
             * ifnull(
                  (SELECT z.exchangeRate
                     FROM tmp_getExchangeRate1 z
                    WHERE     z.pFromCurrency = VBAP_WAERK
                          AND z.fact_script_name =
                                 'bi_populate_afs_salesorder_fact'
                          AND z.pDate = ifnull(PRSDT, VBAK_AUDAT)
                          AND z.pToCurrency = vbak_stwae),
                  1),
             0)
 WHERE     vbk.VBAK_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
       AND vbk.VBEP_ETENR = so.dd_ScheduleNo
       AND pl.plantcode = vbk.VBAP_WERKS
       AND pl.rowiscurrent = 1
       AND so.dd_scheduleno <> 0
       AND so.amt_SubTotal3 <>
              ifnull(
                   (  vbk.VBAP_KZWI3
                    / CASE
                         WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                              AND VBAP_NETPR > 0
                              AND VBAP_NETPR <>
                                     ROUND(
                                        (  VBAP_KPEIN
                                         * VBAP_NETWR
                                         / CASE VBAP_KWMENG
                                              WHEN 0 THEN 1
                                              ELSE VBAP_KWMENG
                                           END),
                                        4)
                         THEN
                            (CASE ifnull(
                                     Round( (  VBAP_NETWR
                                         / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) end)
                                            / (CASE
                                                  WHEN vbap_kpein = 0 THEN 1
                                                  ELSE vbap_kpein
                                               END))),
                                        4),
                                     1)
                                WHEN 0
                                THEN
                                   1
                                ELSE
                                   ifnull(
                                      Round(
                                         (  VBAP_NETWR
                                          / (  (CASE
                                                   WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0
                                                   THEN
                                                      1
                                                   ELSE (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END)
                                                END)
                                             / (CASE
                                                   WHEN vbap_kpein = 0 THEN 1
                                                   ELSE vbap_kpein
                                                END))),
                                         4),
                                      1)
                             END)
                         ELSE
                            case when ifnull(
                                 VBAP_NETWR
                               / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                     THEN
                                        1
                                     ELSE (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                                  END),
                               1) = 0 
				then 1
				else  ifnull(
                                 VBAP_NETWR
                               / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                     THEN
                                        1
                                     ELSE (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                                  END),
                               1)
			   end
                      END)
                 * vbep_bmeng
                 * ifnull(
                      (SELECT z.exchangeRate
                         FROM tmp_getExchangeRate1 z
                        WHERE     z.pFromCurrency = VBAP_WAERK
                              AND z.fact_script_name =
                                     'bi_populate_afs_salesorder_fact'
                              AND z.pDate = ifnull(PRSDT, VBAK_AUDAT)
                              AND z.pToCurrency = vbak_stwae),
                      1),
                 0); 
				 
		
/* LK : 29 Aug change - Update new column amt_Subtotal3_OrderQty */

UPDATE fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so. amt_Subtotal3_OrderQty =
          ifnull(
               (  vbk.VBAP_KZWI3
                / CASE
                     WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                          AND VBAP_NETPR > 0
                          AND VBAP_NETPR <>
                                 ROUND(
                                    (  VBAP_KPEIN
                                     * VBAP_NETWR
                                     / CASE VBAP_KWMENG
                                          WHEN 0 THEN 1
                                          ELSE VBAP_KWMENG
                                       END),
                                    4)
                     THEN
                        (CASE ifnull(
                                 Round(
                                    (  VBAP_NETWR
                                     / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) end)
                                        / (CASE
                                              WHEN vbap_kpein = 0 THEN 1
                                              ELSE vbap_kpein
                                           END))),
                                    4),
                                 1)
                            WHEN 0
                            THEN
                               1
                            ELSE
                               ifnull(
                                  Round(
                                     (  VBAP_NETWR
                                      / (  (CASE WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0
                                               THEN
                                                  1
                                               ELSE
                                                  (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END)
                                            END)
                                         / (CASE
                                               WHEN vbap_kpein = 0 THEN 1
                                               ELSE vbap_kpein
                                            END))),
                                     4),
                                  1)
                         END)
                     ELSE
                        Case when ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1) = 0 
			then 1
			Else ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1)
			ENd
                  END)
             * vbep_wmeng
             * ifnull(
                  (SELECT z.exchangeRate
                     FROM tmp_getExchangeRate1 z
                    WHERE     z.pFromCurrency = VBAP_WAERK
                          AND z.fact_script_name =
                                 'bi_populate_afs_salesorder_fact'
                          AND z.pDate = ifnull(PRSDT, VBAK_AUDAT)
                          AND z.pToCurrency = vbak_stwae),
                  1),
             0)
 WHERE     vbk.VBAK_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
       AND vbk.VBEP_ETENR = so.dd_ScheduleNo
       AND pl.plantcode = vbk.VBAP_WERKS
       AND pl.rowiscurrent = 1
       AND so.dd_scheduleno <> 0 ; 		

/* End of 29 Aug change - Update new column amt_Subtotal3_OrderQty */				 

UPDATE fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.amt_SubTotal4 =
          ifnull(
               (  vbk.VBAP_KZWI4
                / CASE
                     WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                          AND VBAP_NETPR > 0
                          AND VBAP_NETPR <>
                                 ROUND(
                                    (  VBAP_KPEIN
                                     * VBAP_NETWR
                                     / CASE VBAP_KWMENG
                                          WHEN 0 THEN 1
                                          ELSE VBAP_KWMENG
                                       END),
                                    4)
                     THEN
                        (CASE ifnull(
                                 Round(
                                    (  VBAP_NETWR
                                     / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) end)
                                        / (CASE
                                              WHEN vbap_kpein = 0 THEN 1
                                              ELSE vbap_kpein
                                           END))),
                                    4),
                                 1)
                            WHEN 0
                            THEN
                               1
                            ELSE
                               ifnull(
                                  Round(
                                     (  VBAP_NETWR
                                      / (  (CASE WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0
                                               THEN
                                                  1
                                               ELSE
                                                  (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END)
                                            END)
                                         / (CASE
                                               WHEN vbap_kpein = 0 THEN 1
                                               ELSE vbap_kpein
                                            END))),
                                     4),
                                  1)
                         END)
                     ELSE
                        Case when ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1) = 0 
			then 1
			Else ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1)
			ENd
                  END)
             * vbep_bmeng
             * ifnull(
                  (SELECT z.exchangeRate
                     FROM tmp_getExchangeRate1 z
                    WHERE     z.pFromCurrency = VBAP_WAERK
                          AND z.fact_script_name =
                                 'bi_populate_afs_salesorder_fact'
                          AND z.pDate = ifnull(PRSDT, VBAK_AUDAT)
                          AND z.pToCurrency = vbak_stwae),
                  1),
             0)
 WHERE     vbk.VBAK_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
       AND vbk.VBEP_ETENR = so.dd_ScheduleNo
       AND pl.plantcode = vbk.VBAP_WERKS
       AND pl.rowiscurrent = 1
       AND so.dd_scheduleno <> 0
       AND so.amt_SubTotal4 <>
              ifnull(
                   (  vbk.VBAP_KZWI4
                    / CASE
                         WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                              AND VBAP_NETPR > 0
                              AND VBAP_NETPR <>
                                     ROUND(
                                        (  VBAP_KPEIN
                                         * VBAP_NETWR
                                         / CASE VBAP_KWMENG
                                              WHEN 0 THEN 1
                                              ELSE VBAP_KWMENG
                                           END),
                                        4)
                         THEN
                            (CASE ifnull(
                                     Round( (  VBAP_NETWR
                                         / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) end)
                                            / (CASE
                                                  WHEN vbap_kpein = 0 THEN 1
                                                  ELSE vbap_kpein
                                               END))),
                                        4),
                                     1)
                                WHEN 0
                                THEN
                                   1
                                ELSE
                                   ifnull(
                                      Round(
                                         (  VBAP_NETWR
                                          / (  (CASE
                                                   WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0
                                                   THEN
                                                      1
                                                   ELSE (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END)
                                                END)
                                             / (CASE
                                                   WHEN vbap_kpein = 0 THEN 1
                                                   ELSE vbap_kpein
                                                END))),
                                         4),
                                      1)
                             END)
                         ELSE
                            case when ifnull(
                                 VBAP_NETWR
                               / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                     THEN
                                        1
                                     ELSE (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                                  END),
                               1) = 0 
				then 1
				else  ifnull(
                                 VBAP_NETWR
                               / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                     THEN
                                        1
                                     ELSE (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                                  END),
                               1)
			   end
                      END)
                 * vbep_bmeng
                 * ifnull(
                      (SELECT z.exchangeRate
                         FROM tmp_getExchangeRate1 z
                        WHERE     z.pFromCurrency = VBAP_WAERK
                              AND z.fact_script_name =
                                     'bi_populate_afs_salesorder_fact'
                              AND z.pDate = ifnull(PRSDT, VBAK_AUDAT)
                              AND z.pToCurrency = vbak_stwae),
                      1),
                 0); 


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
Set   so.ct_CumConfirmedQty = ifnull(vbk.VBAP_KBMENG, 0)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1  AND so.dd_scheduleno <> 0 
AND  so.ct_CumConfirmedQty <> ifnull(vbk.VBAP_KBMENG, 0);


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
Set  so.ct_CumOrderQty = ifnull(vbk.VBAP_KWMENG, 0)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
	     AND so.ct_CumOrderQty <> ifnull(vbk.VBAP_KWMENG, 0)
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1  AND so.dd_scheduleno <> 0 
AND  so.ct_CumOrderQty <>  ifnull(vbk.VBAP_KWMENG, 0);

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_customerpurchaseordertype cpt
SET so.Dim_PurchaseOrderTypeId =
          ifnull( cpt.dim_customerpurchaseordertypeid, 1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND cpt.CustomerPOType = vbk.VBAK_BSARK
                     AND cpt.RowIsCurrent = 1  AND so.dd_scheduleno <> 0 
AND so.Dim_PurchaseOrderTypeId <>  ifnull( cpt.dim_customerpurchaseordertypeid, 1);


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl, dim_Date dt
SET so.Dim_DateIdPurchaseOrder =
          ifnull( dt.Dim_Dateid,1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND  dt.DateValue = vbk.VBAK_BSTDK
                     AND dt.CompanyCode = pl.CompanyCode  AND so.dd_scheduleno <> 0 
AND  so.Dim_DateIdPurchaseOrder  <>  ifnull( dt.Dim_Dateid,1);



UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
SET so.Dim_DateIdQuotationValidFrom =
          ifnull( dt.Dim_Dateid,1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND  dt.DateValue = vbk.VBAK_ANGDT
                     AND dt.CompanyCode = pl.CompanyCode   AND so.dd_scheduleno <> 0 
AND  so.Dim_DateIdQuotationValidFrom <>  ifnull( dt.Dim_Dateid,1);


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl, dim_Date dt 
SET so.Dim_DateIdQuotationValidTo =
          ifnull( dt.Dim_Dateid,1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND dt.DateValue = vbk.VBAK_BNDDT
                     AND dt.CompanyCode = pl.CompanyCode  AND so.dd_scheduleno <> 0 
AND  so.Dim_DateIdQuotationValidTo <>  ifnull( dt.Dim_Dateid,1);


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
SET so.Dim_DateIdSOCreated =
          ifnull( dt.Dim_Dateid,1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND  dt.DateValue = vbk.VBAK_ERDAT AND dt.CompanyCode = pl.CompanyCode  AND so.dd_scheduleno <> 0 
AND so.Dim_DateIdSOCreated <>  ifnull( dt.Dim_Dateid,1);

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
SET so.Dim_DateIdSOItemChangedOn =
          dt.Dim_Dateid
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND  dt.DateValue = vbk.VBAP_AEDAT AND dt.CompanyCode = pl.CompanyCode  AND so.dd_scheduleno <> 0 
AND vbk.VBAP_AEDAT IS NOT NULL;

UPDATE fact_salesorder so
SET so.Dim_DateIdSOItemChangedOn = 1
WHERE   so.Dim_DateIdSOItemChangedOn IS NULL;

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
SET so.Dim_DateIdSODocument =
          ifnull(  dt.Dim_Dateid,1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND  dt.DateValue = vbk.VBAK_AUDAT
                     AND dt.CompanyCode = pl.CompanyCode  AND so.dd_scheduleno <> 0 
AND so.Dim_DateIdSODocument <>  ifnull(  dt.Dim_Dateid,1) ;


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
SET so.dd_ReferenceDocumentNo = ifnull(vbk.VBAP_VGBEL,'Not Set')
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1  AND so.dd_scheduleno <> 0 
AND  so.dd_ReferenceDocumentNo <> ifnull(vbk.VBAP_VGBEL,'Not Set');

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
SET so.dd_AfsAllocationGroupNo = ifnull(vbk.VBEP_AFS_AGNUM,'Not Set')
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1  AND so.dd_scheduleno <> 0 
AND  vbk.VBEP_AFS_AGNUM IS NOT NULL;

UPDATE          fact_salesorder so
SET so.dd_AfsAllocationGroupNo = 'Not Set'
Where so.dd_AfsAllocationGroupNo IS NULL;

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
SET so.dd_CustomerMaterialNo = ifnull(ifnull(vbk.VBEP_J_3AKDMAT,vbk.VBAP_KDMAT),'Not Set')
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1  AND so.dd_scheduleno <> 0 
AND  (vbk.VBAP_KDMAT IS NOT NULL OR vbk.VBEP_J_3AKDMAT IS NOT NULL);

UPDATE          fact_salesorder so
SET so.dd_CustomerMaterialNo = 'Not Set'
Where so.dd_CustomerMaterialNo IS NULL;

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_ShippingCondition sc
SET so.Dim_ShippingConditionId = sc.Dim_ShippingConditionId
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_VSBED IS NOT NULL
AND sc.ShippingConditionCode = vbk.VBAK_VSBED
AND sc.RowIsCurrent = 1;

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk 
SET so.Dim_ShippingConditionId = 1
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_VSBED IS NULL;

UPDATE fact_salesorder so
from vbak_vbap_vbep, dim_materialpricegroup4 mg4
SET so.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
WHERE so.dd_SalesDocNo = VBAK_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = VBEP_ETENR
AND VBAP_MVGR4 is not null
AND VBAP_MVGR4 = mg4.MaterialPriceGroup4
AND mg4.RowIsCurrent = 1;

UPDATE fact_salesorder so
from vbak_vbap, dim_materialpricegroup4 mg4
SET so.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
WHERE so.dd_SalesDocNo = VBAP_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = 0
AND VBAP_MVGR4 is not null
AND VBAP_MVGR4 = mg4.MaterialPriceGroup4
AND mg4.RowIsCurrent = 1;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id = 1
WHERE so.dim_materialpricegroup4id IS NULL;

UPDATE fact_salesorder so
from vbak_vbap_vbep, dim_materialpricegroup5 mg5
SET so.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
WHERE so.dd_SalesDocNo = VBAK_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = VBEP_ETENR
AND VBAP_MVGR5 is not null
AND VBAP_MVGR5 = mg5.MaterialPriceGroup5
AND mg5.RowIsCurrent = 1;

UPDATE fact_salesorder so
from vbak_vbap, dim_materialpricegroup5 mg5
SET so.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
WHERE so.dd_SalesDocNo = VBAP_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = 0
AND VBAP_MVGR5 is not null
AND VBAP_MVGR5 = mg5.MaterialPriceGroup5
AND mg5.RowIsCurrent = 1;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id = 1
WHERE so.dim_materialpricegroup5id IS NULL;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd,dim_company dc
   SET so.Dim_SalesDistrictId =  ifnull(
             (SELECT sd.Dim_SalesDistrictid
                FROM dim_salesdistrict sd
               WHERE sd.SalesDistrict = vkd.VBKD_BZIRK
                     AND sd.RowIsCurrent = 1),
             1)
Where  so.dd_SalesDocNo = vkd.VBKD_VBELN
and dc.dim_companyid=so.dim_companyid;


UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd,dim_company dc
Set so.Dim_AccountAssignmentGroupId = ifnull(
             (SELECT aag.Dim_AccountAssignmentGroupId
                FROM dim_accountassignmentgroup aag
               WHERE aag.AccountAssignmentGroup = vkd.VBKD_KTGRD
                     AND aag.RowIsCurrent = 1),
             1)
Where  so.dd_SalesDocNo = vkd.VBKD_VBELN
and dc.dim_companyid=so.dim_companyid;


UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd,dim_company dc
Set so.dd_BusinessCustomerPONo = ifnull(vkd.VBKD_BSTKD, 'Not Set')
Where so.dd_SalesDocNo = vkd.VBKD_VBELN
and dc.dim_companyid=so.dim_companyid
AND  vkd.VBKD_BSTKD IS NOT NULL;

UPDATE    fact_salesorder so
Set so.dd_BusinessCustomerPONo = 'Not Set'
Where so.dd_BusinessCustomerPONo IS  NULL;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd,dim_company dc
Set so.Dim_BillingDateId =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date
               WHERE datevalue = VBKD_FKDAT AND companycode = dc.CompanyCode),
             1)
Where so.dd_SalesDocNo = vkd.VBKD_VBELN 
and dc.dim_companyid=so.dim_companyid;



/*    AFS related field population  */
UPDATE       fact_salesorder so
FROM vbak_vbap_vbkd vkd,dim_company dc
    Set so.Dim_SalesDistrictId =
          ifnull(
             (SELECT sd.Dim_SalesDistrictid
                FROM dim_salesdistrict sd
               WHERE sd.SalesDistrict = vkd.VBKD_BZIRK
                     AND sd.RowIsCurrent = 1),
             1)
Where so.dd_SalesDocNo = vkd.VBKD_VBELN
             AND so.dd_SalesItemNo = vkd.VBKD_POSNR
AND  dc.dim_companyid = so.dim_companyid;

UPDATE       fact_salesorder so
    Set so.Dim_SalesDistrictId = 1
Where so.Dim_SalesDistrictId IS NULL;

UPDATE       fact_salesorder so
FROM vbak_vbap_vbkd vkd,dim_company dc
set so.Dim_AccountAssignmentGroupId =
          ifnull(
             (SELECT aag.Dim_AccountAssignmentGroupId
                FROM dim_accountassignmentgroup aag
               WHERE aag.AccountAssignmentGroup = vkd.VBKD_KTGRD
                     AND aag.RowIsCurrent = 1),
             1)
Where so.dd_SalesDocNo = vkd.VBKD_VBELN
             AND so.dd_SalesItemNo = vkd.VBKD_POSNR
AND  dc.dim_companyid = so.dim_companyid;



UPDATE       fact_salesorder so
FROM vbak_vbap_vbkd vkd,dim_company dc
       set so.dd_BusinessCustomerPONo = ifnull(vkd.VBKD_BSTKD, 'Not Set')
Where so.dd_SalesDocNo = vkd.VBKD_VBELN
             AND so.dd_SalesItemNo = vkd.VBKD_POSNR
AND  dc.dim_companyid = so.dim_companyid
AND  so.dd_BusinessCustomerPONo <> ifnull(vkd.VBKD_BSTKD, 'Not Set') ;



UPDATE       fact_salesorder so
FROM vbak_vbap_vbkd vkd,dim_company dc
       set so.Dim_BillingDateId =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date
               WHERE datevalue = VBKD_FKDAT AND companycode = dc.CompanyCode),
             1)
Where so.dd_SalesDocNo = vkd.VBKD_VBELN
             AND so.dd_SalesItemNo = vkd.VBKD_POSNR
AND  dc.dim_companyid = so.dim_companyid;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set so.dd_CustomerPONo = ifnull(vbk.VBAK_BSTNK,'Not Set')
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
	AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
	AND so.dd_ScheduleNo = 0
AND  so.dd_CustomerPONo <> ifnull(vbk.VBAK_BSTNK,'Not Set');

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set so.Dim_CreditRepresentativeId =
          ifnull(
             (SELECT Dim_CreditRepresentativegroupId
                FROM dim_creditrepresentativegroup cg
               WHERE     cg.CreditRepresentativeGroup = vbk.VBAK_SBGRP
                     AND cg.CreditControlArea = vbk.VBAK_KKBER
                     AND cg.RowIsCurrent = 1),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set so.Dim_MaterialPriceGroup1Id =
          ifnull(
             (SELECT Dim_MaterialPriceGroup1Id
                FROM Dim_MaterialPriceGroup1 mpg
               WHERE mpg.MaterialPriceGroup1 = vbk.VBAP_MVGR1
                     AND mpg.RowIsCurrent = 1),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set  so.Dim_DeliveryBlockId =
          ifnull(
             (SELECT Dim_DeliveryBlockId
                FROM Dim_DeliveryBlock db
               WHERE db.DeliveryBlock = vbk.VBAK_LIFSK
                     AND db.RowIsCurrent = 1),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set so.Dim_SalesDocOrderReasonId =
          ifnull(
             (SELECT dim_salesdocorderreasonid
                FROM dim_salesdocorderreason sor
               WHERE sor.ReasonCode = vbk.VBAK_AUGRU AND sor.RowIsCurrent = 1),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set  so.Dim_MaterialGroupId =
          ifnull(
             (SELECT dim_materialgroupid
                FROM dim_materialgroup mg
               WHERE mg.MaterialGroupCode = vbk.VBAP_MATKL
                     AND mg.RowIsCurrent = 1),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set   so.amt_SubTotal3 = ifnull(vbk.VBAP_KZWI3, 0)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0
AND so.amt_SubTotal3 <> ifnull(vbk.VBAP_KZWI3, 0);

/* 29 Aug 2013 change - Update new column amt_Subtotal3_OrderQty */
/* No need to split when there are no schedules */

UPDATE fact_salesorder so
   SET so.amt_Subtotal3_OrderQty = amt_SubTotal3
WHERE so.dd_ScheduleNo = 0;

/* End of 29 Aug 2013 change - Update new column amt_Subtotal3_OrderQty */

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set    so.amt_SubTotal4 = ifnull(vbk.VBAP_KZWI4, 0)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0
AND  so.amt_SubTotal4 <> ifnull(vbk.VBAP_KZWI4, 0);

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set    so.Dim_PurchaseOrderTypeId =
          ifnull(
             (SELECT cpt.dim_customerpurchaseordertypeid
                FROM dim_customerpurchaseordertype cpt
               WHERE cpt.CustomerPOType = vbk.VBAK_BSARK
                     AND cpt.RowIsCurrent = 1),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set    so.Dim_DateIdPurchaseOrder =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_BSTDK
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set     so.Dim_DateIdQuotationValidFrom =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_ANGDT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set   so.Dim_DateIdQuotationValidTo =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_BNDDT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set    so.Dim_DateIdSOCreated =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_ERDAT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set    so.Dim_DateIdSOItemChangedOn =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAP_AEDAT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE          fact_salesorder so
FROM vbak_vbap vbk,dim_ShippingCondition sc
SET so.Dim_ShippingConditionId =
          sc.Dim_ShippingConditionId
Where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
           AND  so.dd_ScheduleNo = 0
AND sc.ShippingConditionCode = ifnull(vbk.VBAK_VSBED ,'Not Set')
AND sc.rowIsCurrent = 1;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set  so.Dim_DateIdSODocument =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_AUDAT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0;

UPDATE fact_salesorder so
from  vbak_vbap vbk,dim_plant pl
Set  so.dd_ReferenceDocumentNo = ifnull(vbk.VBAP_VGBEL,'Not Set')
Where  vbk.VBAP_VBELN = so.dd_SalesDocNo
       AND vbk.VBAP_POSNR = so.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
        AND so.dd_ScheduleNo = 0
AND  so.dd_ReferenceDocumentNo <> ifnull(vbk.VBAP_VGBEL,'Not Set');

call vectorwise(combine 'fact_salesorder');

DROP TABLE IF EXISTS tmp_CustomerCreditLimit;

CREATE TABLE tmp_CustomerCreditLimit as
select distinct c.CUSTOMER,ifnull(a.CREDIT_LIMIT,0) as Credit_Limit,a.LIMIT_VALID_DATE,RANK() OVER (PARTITION BY c.CUSTOMER ORDER BY a.LIMIT_VALID_DATE ASC) 
  AS Rank from UKMBP_CMS_SGM a 
  inner join BUT000 b on a.PARTNER = b.PARTNER and TIMESTAMP(LOCAL_TIMESTAMP) <= a.LIMIT_VALID_DATE
  inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID;

DELETE FROM tmp_CustomerCreditLimit WHERE Rank <> 1;

UPDATE fact_salesorder fso
FROM    dim_customer dc,
        tmp_CustomerCreditLimit  t
SET     dd_CreditLimit = t.Credit_Limit
WHERE	 dc.Dim_CustomerId = fso.Dim_CustomerId
AND dc.CustomerNumber = t.customer;

UPDATE fact_salesorder fso
SET dd_CreditLimit = 0
WHERE dd_CreditLimit IS NULL;
                                  
DROP TABLE IF EXISTS tmp_CustomerCreditLimit;

DROP TABLE IF EXISTS tmp_upd_702;

Create table tmp_upd_702 as Select first 0 
(case when b.NAME_FIRST is null then b.NAME_LAST else b.NAME_FIRST+' '+ifnull(b.NAME_LAST,'') end) updcol1,d.BUT050_PARTNER2 updVBAK_KUNNR
from cvi_cust_link c inner join but000 b1 on c.PARTNER_GUID = b1.PARTNER_GUID
	inner join BUT050 d on b1.PARTNER = d.BUT050_PARTNER2 AND d.BUT050_RELTYP = 'UKMSB0'
	inner join but000 b on b.PARTNER = d.BUT050_PARTNER1
where  ANSIDATE(CURRENT_DATE) between ANSIDATE(d.BUT050_DATE_FROM) and ANSIDATE(d.BUT050_DATE_TO)
and (b.NAME_FIRST is not null or b.NAME_LAST is not null and b.BU_GROUP = 'CRED')
order by c.customer ;

update fact_Salesorder so
from   tmp_upd_702 t,
       Dim_Customer c
Set dd_CreditRep = t.updcol1
Where so.dim_Customerid = c.dim_customerid
  and t.updVBAK_KUNNR = c.CustomerNumber;

DROP TABLE IF EXISTS tmp_upd_702;

call vectorwise(combine 'fact_salesorder');


UPDATE fact_salesorder so SET so.dd_AfsStockType = 'Not Set' WHERE so.dd_AfsStockType <> 'Not Set';
UPDATE fact_salesorder so SET so.amt_AfsOnDeliveryValue = 0 WHERE so.amt_AfsOnDeliveryValue <> 0;
UPDATE fact_salesorder so SET so.amt_AfsUnallocatedValue = 0 WHERE so.amt_AfsUnallocatedValue <> 0;
UPDATE fact_salesorder so SET so.ct_AfsAllocatedQty = 0 WHERE so.ct_AfsAllocatedQty <> 0;
UPDATE fact_salesorder so SET so.ct_AfsOnDeliveryQty = 0 WHERE so.ct_AfsOnDeliveryQty <> 0;
UPDATE fact_salesorder so SET so.ct_AfsOpenQty = 0 WHERE so.ct_AfsOpenQty <> 0;
UPDATE fact_salesorder so SET so.ct_AfsUnallocatedQty = 0 WHERE so.ct_AfsUnallocatedQty <> 0;
UPDATE fact_salesorder so SET so.ct_AfsAllocationFQty = 0 WHERE so.ct_AfsAllocationFQty <> 0;
UPDATE fact_salesorder so SET so.ct_AfsAllocationRQty = 0 WHERE so.ct_AfsAllocationRQty <> 0;
UPDATE fact_salesorder so SET so.amt_CreditHoldValue = 0 WHERE so.amt_CreditHoldValue <> 0;
UPDATE fact_salesorder so SET so.amt_DeliveryBlockValue = 0 WHERE so.amt_DeliveryBlockValue <> 0;

    UPDATE       fact_salesorder so
    FROM j_3abdbs ast
      SET so.dd_AfsStockType = ast.J_3ABDBS_J_3ABSKZ
	Where  so.dd_SalesDocNo = ast.J_3ABDBS_AUFNR
              AND so.dd_SalesItemNo = ast.J_3ABDBS_POSNR
              AND so.dd_ScheduleNo = ast.J_3ABDBS_ETENR 
AND so.dd_AfsStockType <>  ast.J_3ABDBS_J_3ABSKZ ;


drop table if exists ct_group_update;
create table ct_group_update as select J_3ABDBS_AUFNR,J_3ABDBS_POSNR,J_3ABDBS_ETENR,J_3ABDBS_J_3ASTAT,sum(J_3ABDBS_MENGE) J_3ABDBS_MENGE
from j_3abdbs
group by  J_3ABDBS_AUFNR,J_3ABDBS_POSNR,J_3ABDBS_ETENR,J_3ABDBS_J_3ASTAT;

    UPDATE fact_salesorder so
      SET so.ct_AfsAllocationFQty =
              IFNULL(
                (SELECT ast.J_3ABDBS_MENGE
                    FROM ct_group_update ast
                  WHERE     ast.J_3ABDBS_AUFNR = so.dd_SalesDocNo
                        AND ast.J_3ABDBS_POSNR = so.dd_SalesItemNo
                        AND ast.J_3ABDBS_ETENR = so.dd_ScheduleNo
			AND ast.J_3ABDBS_J_3ASTAT = 'F'	
                        ),
                0);



    UPDATE fact_salesorder so
      SET so.ct_AfsAllocationRQty =
              IFNULL(
                (SELECT ast.J_3ABDBS_MENGE
                    FROM  ct_group_update ast
                  WHERE     ast.J_3ABDBS_AUFNR = so.dd_SalesDocNo
                        AND ast.J_3ABDBS_POSNR = so.dd_SalesItemNo
                        AND ast.J_3ABDBS_ETENR = so.dd_ScheduleNo
                        AND ast.J_3ABDBS_J_3ASTAT = 'R'),
                0);


  UPDATE fact_salesorder so
   SET so.ct_AfsAllocatedQty =
          IFNULL(
             (SELECT SUM(ast.J_3ABDBS_MENGE)
                FROM ct_group_update ast
               WHERE     ast.J_3ABDBS_AUFNR = so.dd_SalesDocNo
                     AND ast.J_3ABDBS_POSNR = so.dd_SalesItemNo
                     AND ast.J_3ABDBS_ETENR = so.dd_ScheduleNo),
             0);

drop table if exists ct_group_update;

    UPDATE       fact_salesorder so
    FROM j_3abdsi afs
      SET so.ct_AfsOpenQty = ifnull(J_3ABDSI_MENGE, 0)
  Where  so.dd_SalesDocNo = afs.J_3ABDSI_AUFNR
              AND so.dd_SalesItemNo = afs.J_3ABDSI_POSNR
              AND so.dd_ScheduleNo = afs.J_3ABDSI_ETENR 
AND  so.ct_AfsOpenQty <> ifnull(J_3ABDSI_MENGE, 0);


 UPDATE       fact_salesorder so
    FROM j_3abdsi afs
      SET  so.dd_RequirementType = ifnull(afs.J_3ABDSI_BDART, 'Not Set')
     Where  so.dd_SalesDocNo = afs.J_3ABDSI_AUFNR
              AND so.dd_SalesItemNo = afs.J_3ABDSI_POSNR
              AND so.dd_ScheduleNo = afs.J_3ABDSI_ETENR 
AND so.dd_RequirementType <> ifnull(afs.J_3ABDSI_BDART, 'Not Set');

 UPDATE       fact_salesorder so
    FROM j_3abdsi afs,
	 Dim_Plant pl
      SET  so.Dim_DateidSchedDeliveryReq=ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = afs.J_3ABDSI_vdatu AND dd.CompanyCode = pl.CompanyCode),1) 
     Where  so.Dim_Plantid = pl.Dim_Plantid
           AND  so.dd_SalesDocNo = afs.J_3ABDSI_AUFNR
              AND so.dd_SalesItemNo = afs.J_3ABDSI_POSNR
              AND so.dd_ScheduleNo = afs.J_3ABDSI_ETENR;

    UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk,dim_plant pl
      SET so.dd_AfsStockCategory = ifnull(vbk.VBEP_J_4KRCAT,'Not Set')
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
                AND vbk.VBAP_POSNR = so.dd_SalesItemNo
                AND vbk.VBEP_ETENR = so.dd_ScheduleNo
	AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
AND so.dd_AfsStockCategory <>  ifnull(vbk.VBEP_J_4KRCAT,'Not Set');


    UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk,dim_plant pl
SET so.dd_AfsDepartment = ifnull(vbk.VBAP_J_3ADEPM,'Not Set')
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
                AND vbk.VBAP_POSNR = so.dd_SalesItemNo
                AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
AND so.dd_AfsDepartment <>  ifnull(vbk.VBAP_J_3ADEPM,'Not Set');


  UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk,dim_plant pl,dim_date dd
SET so.Dim_DateIdAfsCancelDate = dim_dateid
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo 
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAP_J_3ACADA IS NOT NULL
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
AND dd.DateValue = vbk.VBAP_J_3ACADA AND dd.CompanyCode = pl.CompanyCode;


  UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk
SET so.Dim_DateIdAfsCancelDate = 1
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo 
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAP_J_3ACADA IS NULL;

  UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk,dim_plant pl,dim_date dd
set so.Dim_DateIdAfsReqDelivery = dim_dateid
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
 AND  vbk.VBAP_POSNR = so.dd_SalesItemNo
 AND  vbk.VBEP_ETENR = so.dd_ScheduleNo
 AND vbk.VBAP_J_3ARQDA IS NOT NULL
 AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
 AND dd.DateValue = vbk.VBAP_J_3ARQDA
 AND dd.CompanyCode = pl.CompanyCode;

  UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk
set so.Dim_DateIdAfsReqDelivery = 1
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
 AND  vbk.VBAP_POSNR = so.dd_SalesItemNo
 AND  vbk.VBEP_ETENR = so.dd_ScheduleNo
 AND vbk.VBAP_J_3ARQDA IS NULL;

UPDATE  fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
set so.amt_AfsNetValue = ifnull(VBEP_J_3ANETW,0)
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
                AND vbk.VBAP_POSNR = so.dd_SalesItemNo
                AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
AND  so.amt_AfsNetValue <> ifnull(VBEP_J_3ANETW,0);


UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk, Dim_SalesOrderRejectReason sorr
SET so.Dim_AfsRejectionReasonId =  sorr.Dim_SalesOrderRejectReasonid
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBEP_J_3AABGRU IS NOT NULL
AND sorr.RejectReasonCode = vbk.VBEP_J_3AABGRU
AND so.Dim_AfsRejectionReasonId  <> sorr.Dim_SalesOrderRejectReasonid;

UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk
SET so.Dim_AfsRejectionReasonId =  1
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBEP_J_3AABGRU IS NULL;


  UPDATE  fact_salesorder so
    FROM vbak_vbap_vbep vbk,dim_plant pl,dim_afssize dz
SET so.Dim_AfsSizeId = ifnull( dz.dim_AfsSizeId,1)
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
                AND vbk.VBAP_POSNR = so.dd_SalesItemNo
                AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
AND dz.Size = vbk.VBEP_J_3ASIZE AND dz.RowIsCurrent = 1
AND so.Dim_AfsSizeId <>  ifnull( dz.dim_AfsSizeId,1);


UPDATE  fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
SET so.amt_AfsNetPrice =
              ifnull(
                Decimal((vbk.VBEP_J_3ANETP
                  * ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
				where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_afs_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAK_AUDAT) AND z.pToCurrency = vbak_stwae ),1)),18,4),
                0)
Where  vbk.VBAK_VBELN = so.dd_SalesDocNo
                AND vbk.VBAP_POSNR = so.dd_SalesItemNo
                AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 ;

DROP TABLE IF EXISTS tmp_NetDueDateCalc;
DROP TABLE IF EXISTS tmp_SalesOrder_ForNetDueDateCalc;

CREATE TABLE tmp_SalesOrder_ForNetDueDateCalc AS
SELECT distinct dd_SalesDocNo,dd_SalesItemNo,dd_scheduleNo,
(CASE WHEN fso.Dim_DateIdFixedValue <> 1  THEN fso.Dim_DateIdFixedValue WHEN fso.Dim_DateIdFixedValue = 1 THEN fso.Dim_DateIdAfsReqDelivery ELSE 1 END) as Dim_DateIdDeliv,
(CASE WHEN fso.Dim_PaymentTermsId <> 1  THEN fso.Dim_PaymentTermsId WHEN fso.Dim_PaymentTermsId = 1 THEN fso.Dim_CustomerPaymentTermsId ELSE 1 END) as Dim_PayTermsId
FROM fact_salesorder fso;


CREATE TABLE tmp_NetDueDateCalc AS
SELECT distinct dd_SalesDocNo,dd_SalesItemNo,dd_scheduleNo,dt.CompanyCode, (dt.datevalue + (interval '1' DAY) *(CASE WHEN T052_ZTAG3 IS NULL THEN (CASE WHEN T052_ZTAG2 IS NULL THEN T052_ZTAG1 ELSE T052_ZTAG2 END) ELSE T052_ZTAG3 END)) AS DueDateValue
FROM tmp_SalesOrder_ForNetDueDateCalc fso,
     T052 t,
     dim_date dt,
     Dim_CustomerPaymentTerms pt
WHERE fso.Dim_DateIdDeliv = dt.Dim_DateId
AND fso.Dim_PayTermsId = pt.Dim_CustomerPaymentTermsId
AND t.T052_ZTERM = pt.PaymentTermCode
AND fso.Dim_DateIdDeliv > 1;

UPDATE fact_salesorder fso
    FROM
       tmp_NetDueDateCalc t,
       dim_Date dt
  SET fso.Dim_DateIdNetDueDate = dt.Dim_DateId
WHERE fso.dd_SalesDocNo = t.dd_SalesDocNo
  AND fso.dd_SalesItemNo = t.dd_SalesItemNo
  AND fso.dd_ScheduleNo = t.dd_ScheduleNo
  AND dt.DateValue = t.DueDateValue
  AND dt.companyCode = t.CompanyCode;

DROP TABLE IF EXISTS tmp_SalesOrder_ForNetDueDateCalc;
DROP TABLE IF EXISTS tmp_NetDueDateCalc;

UPDATE fact_salesorder
SET Dim_DateIdNetDueDate = 1
WHERE Dim_DateIdNetDueDate IS NULL;

UPDATE fact_salesorder fso
SET fso.ct_ShippedOrBilledQty = 0
WHERE fso.ct_ShippedOrBilledQty <> 0;

UPDATE fact_salesorder fso
SET fso.amt_ShippedOrBilled = 0
WHERE fso.amt_ShippedOrBilled <> 0;
	  
DROP TABLE IF EXISTS tmp_UpdateShippedOrBilledQty;

CREATE TABLE tmp_UpdateShippedOrBilledQty AS 
SELECT dd_SalesDocNo, dd_SalesItemNo, dd_AfsScheduleNo, sum(fb.ct_BilledQtyAtSchedule) as BilledQty
FROM fact_billing fb, Dim_BillingMisc bmisc, dim_billingdocumenttype bdt
    WHERE     fb.dim_documenttypeid = bdt.dim_billingdocumenttypeid
          AND fb.Dim_BillingMiscId = bmisc.Dim_BillingMiscId
          AND bmisc.CancelFlag = 'Not Set'
          AND bdt.type in ('ZF2','F1','F2','ZF2C')
          group by dd_SalesDocNo, dd_SalesItemNo, dd_AfsScheduleNo;
          
    UPDATE fact_salesorder fso
	FROM tmp_UpdateShippedOrBilledQty t 
      SET fso.ct_ShippedOrBilledQty = t.BilledQty
    WHERE     t.dd_salesdocno = fso.dd_SalesDocNo
          AND t.dd_salesitemno = fso.dd_SalesItemNo
          AND t.dd_AfsScheduleNo = fso.dd_ScheduleNo
          AND t.BilledQty > 0;


    UPDATE fact_salesorder fso
        FROM tmp_UpdateShippedOrBilledQty t 
      SET fso.amt_ShippedOrBilled =
              Decimal(t.BilledQty
              * (CASE
                    WHEN (fso.amt_AfsNetPrice <> 0) THEN fso.amt_AfsNetPrice
                    ELSE fso.amt_UnitPrice
                END),18,4)
    WHERE     t.dd_salesdocno = fso.dd_SalesDocNo
          AND t.dd_salesitemno = fso.dd_SalesItemNo
          AND t.dd_AfsScheduleNo = fso.dd_ScheduleNo;
          
DROP TABLE IF EXISTS tmp_UpdateShippedOrBilledQty;          


call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder so FROM
       dim_plant pl, 
       cdpos_rejdate cpr,
       cdhdr_vbap_vbep_rejdate chr
   SET so.Dim_DateIdRejection =  ifnull((SELECT dd.Dim_Dateid
                  FROM Dim_Date dd 
                 WHERE dd.DateValue = chr.CDHDR_UDATE
                 AND dd.CompanyCode = pl.CompanyCode), 1)
    where   cpr.CDPOS_CHANGENR = chr.CDHDR_CHANGENR
        AND chr.CDHDR_OBJECTID = so.dd_SalesDocNo
	AND cpr.CDPOS_FNAME = 'J_3AABGRU'
	AND cpr.CDPOS_TABNAME = 'VBEP'
        AND substr(cpr.CDPOS_TABKEY, 4, 10) = so.dd_SalesDocNo
	AND substr(cpr.CDPOS_TABKEY, 14, 6) = so.dd_SalesItemNo
	AND substr(cpr.CDPOS_TABKEY, 20,4 ) = so.dd_ScheduleNo
	AND so.Dim_Plantid = pl.dim_plantid;

 UPDATE fact_salesorder so
  SET so.Dim_DateIdRejection = 1
 WHERE so.Dim_DateIdRejection IS NULL;


drop table if exists ct_group_update;
create table ct_group_update as
Select dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo, SUM(ct_QtyDelivered) ct_QtyDelivered
From fact_salesorderdelivery
where dd_ScheduleNo <> 0 AND Dim_DateidActualGI_Original = 1
Group by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo;

call vectorwise(combine 'ct_group_update');


    UPDATE fact_salesorder so
      SET so.ct_AfsOnDeliveryQty =
              IFNULL(
                (SELECT l.ct_QtyDelivered
                    FROM ct_group_update l
                  WHERE     so.dd_SalesDocNo = l.dd_SalesDocNo
                        AND so.dd_SalesItemNo = l.dd_SalesItemNo
                        AND so.dd_Scheduleno = l.dd_ScheduleNo
                        ),
                0)
     WHERE so.Dim_DateidActualGI = 1;  
             
drop table if exists ct_group_update;


UPDATE    fact_salesorder so
FROM vbfa_vbak_vbap f
SET so.ct_AfsTotalDrawn = 0
Where  f.VBFA_VBELV = so.dd_SalesDocNo
              AND f.VBFA_POSNV = so.dd_SalesItemNo
              AND f.VBFA_J_3AETENR = so.dd_ScheduleNo
                AND f.VBFA_VBTYP_N = 'J'
AND  so.ct_AfsTotalDrawn <> 0;


UPDATE    fact_salesorder so
FROM vbfa_vbak_vbap f
SET so.dd_SubsequentDocNo = ifnull(f.VBFA_VBELN, 'Not Set')
Where  f.VBFA_VBELV = so.dd_SalesDocNo
              AND f.VBFA_POSNV = so.dd_SalesItemNo
              AND f.VBFA_J_3AETENR = so.dd_ScheduleNo
                AND f.VBFA_VBTYP_N = 'J'
AND so.dd_SubsequentDocNo <> ifnull(f.VBFA_VBELN, 'Not Set');


UPDATE    fact_salesorder so
FROM vbfa_vbak_vbap f
SET  so.dd_SubsDocItemNo = f.VBFA_POSNN
Where  f.VBFA_VBELV = so.dd_SalesDocNo
              AND f.VBFA_POSNV = so.dd_SalesItemNo
              AND f.VBFA_J_3AETENR = so.dd_ScheduleNo
                AND f.VBFA_VBTYP_N = 'J'
AND  so.dd_SubsDocItemNo <> f.VBFA_POSNN;



UPDATE    fact_salesorder so
FROM vbfa_vbak_vbap f
      SET so.Dim_SubsDocCategoryId =
              ifnull((SELECT Dim_DocumentCategoryid
                        FROM Dim_DocumentCategory dc
                      WHERE  dc.DocumentCategory = f.VBFA_VBTYP_N),
                    1)
Where  f.VBFA_VBELV = so.dd_SalesDocNo
              AND f.VBFA_POSNV = so.dd_SalesItemNo
              AND f.VBFA_J_3AETENR = so.dd_ScheduleNo 
		AND f.VBFA_VBTYP_N = 'J';


UPDATE       fact_salesorder so
FROM j_3avbfae afsv,Dim_plant pl
SET so.dd_SubsScheduleNo = afsv.J_3AVBFAE_ETENN
Where  afsv.J_3AVBFAE_VBELV = so.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = so.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = so.dd_ScheduleNo
AND  pl.dim_plantid = so.dim_plantid
AND afsv.J_3AVBFAE_VBTYP_N = 'J'
AND  so.dd_SubsScheduleNo <>  afsv.J_3AVBFAE_ETENN;


UPDATE       fact_salesorder so
FROM j_3avbfae afsv,Dim_plant pl
SET so.ct_AfsTotalDrawn = ifnull(afsv.J_3AVBFAE_RFMNG, 0)
Where  afsv.J_3AVBFAE_VBELV = so.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = so.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = so.dd_ScheduleNo
AND  pl.dim_plantid = so.dim_plantid
AND afsv.J_3AVBFAE_VBTYP_N = 'J'
AND so.ct_AfsTotalDrawn <>  ifnull(afsv.J_3AVBFAE_RFMNG, 0);


UPDATE       fact_salesorder so
FROM j_3avbfae afsv,Dim_plant pl
SET so.Dim_DateIdAsLastDate =
              ifnull(
                (SELECT dim_dateid
                    FROM dim_date
                  WHERE DateValue = J_3AVBFAE_ERDAT
                        AND CompanyCode = pl.CompanyCode),
                1)
Where  afsv.J_3AVBFAE_VBELV = so.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = so.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = so.dd_ScheduleNo
AND  pl.dim_plantid = so.dim_plantid
AND afsv.J_3AVBFAE_VBTYP_N = 'J';


UPDATE       fact_salesorder so
FROM j_3avbfae afsv,Dim_plant pl
SET  so.dd_SubsequentDocNo = ifnull(afsv.J_3AVBFAE_VBELN, 'Not Set')
Where  afsv.J_3AVBFAE_VBELV = so.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = so.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = so.dd_ScheduleNo
AND  pl.dim_plantid = so.dim_plantid
AND afsv.J_3AVBFAE_VBTYP_N = 'J'
AND  so.dd_SubsequentDocNo  <>  ifnull(afsv.J_3AVBFAE_VBELN, 'Not Set');



UPDATE       fact_salesorder so
FROM j_3avbfae afsv,Dim_plant pl
      SET so.dd_SubsDocItemNo = afsv.J_3AVBFAE_POSNN
Where  afsv.J_3AVBFAE_VBELV = so.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = so.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = so.dd_ScheduleNo
AND  pl.dim_plantid = so.dim_plantid
AND afsv.J_3AVBFAE_VBTYP_N = 'J'
AND  so.dd_SubsDocItemNo <> afsv.J_3AVBFAE_POSNN;


UPDATE       fact_salesorder so
FROM j_3avbfae afsv,Dim_plant pl
SET  so.Dim_SubsDocCategoryId =
              ifnull(
                (SELECT Dim_DocumentCategoryid
                    FROM Dim_DocumentCategory dc
                  WHERE dc.DocumentCategory = afsv.J_3AVBFAE_VBTYP_N),
                1)
Where  afsv.J_3AVBFAE_VBELV = so.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = so.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = so.dd_ScheduleNo
AND  pl.dim_plantid = so.dim_plantid 
AND afsv.J_3AVBFAE_VBTYP_N = 'J';
 DROP TABLE IF EXISTS tmp_QuotationsDrawnQty;
    
CREATE TABLE tmp_QuotationsDrawnQty
AS
SELECT J_3AVBFAE_VBELV,
      J_3AVBFAE_POSNV,
      J_3AVBFAE_ETENV,
      J_3AVBFAE_VBTYP_V,
      ifnull(sum(afsv.J_3AVBFAE_RFMNG), 0) AS TotalDrawnQty
FROM j_3avbfae afsv
WHERE afsv.J_3AVBFAE_VBTYP_V IN ('B','b','G','g')
GROUP BY J_3AVBFAE_VBELV, J_3AVBFAE_POSNV, J_3AVBFAE_ETENV,J_3AVBFAE_VBTYP_V;
      
UPDATE fact_salesorder so
	 FROM
       dim_documentcategory dc,
       tmp_QuotationsDrawnQty afsv
   SET so.ct_AfsTotalDrawn = TotalDrawnQty
 WHERE     dc.dim_documentcategoryid = so.Dim_DocumentCategoryid
       AND afsv.J_3AVBFAE_VBELV = so.dd_SalesDocNo
       AND afsv.J_3AVBFAE_POSNV = so.dd_SalesItemNo
       AND afsv.J_3AVBFAE_ETENV = so.dd_ScheduleNo
       AND afsv.J_3AVBFAE_VBTYP_V = dc.DocumentCategory
       AND dc.DocumentCategory IN ('B','b','G','g');

DROP TABLE IF EXISTS tmp_QuotationsDrawnQty;
   

UPDATE fact_salesorder so
SET so.ct_AfsUnallocatedQty =
      (ifnull(so.ct_AfsOpenQty, 0) - ifnull(so.ct_AfsAllocatedQty, 0));

UPDATE fact_salesorder so
      SET  so.amt_AfsUnallocatedValue =
              ((ifnull(so.ct_AfsOpenQty, 0) - ifnull(so.ct_AfsAllocatedQty, 0))
              * amt_AfsNetPrice);

UPDATE fact_salesorder so
      SET  so.amt_AfsOnDeliveryValue =
              (CASE
                  WHEN so.ct_AfsOnDeliveryQty > 0
                  THEN
                    (so.ct_AfsOnDeliveryQty
                      * (CASE
                            WHEN (so.amt_AfsNetPrice <> 0)
                            THEN
                              so.amt_AfsNetPrice
                            ELSE
                              so.amt_UnitPrice
                        END))
                  ELSE
                    0
              END)
WHERE so.Dim_DateidActualGI = 1;

              

    UPDATE    fact_salesorder so
	FROM dim_overallstatusforcreditcheck ocs
      SET so.amt_CreditHoldValue =
              (CASE
                  WHEN ocs.OverallStatusforCreditCheck = 'B'
                  THEN
                    so.ct_ConfirmedQty * so.amt_AfsNetPrice
                  ELSE
                    0
              END)
Where  ocs.dim_overallstatusforcreditcheckID =
                so.Dim_OverallStatusCreditCheckId ;


    UPDATE    fact_salesorder so
    FROM dim_DeliveryBlock db
      SET so.amt_DeliveryBlockValue =
              (CASE
                  WHEN db.DeliveryBlock <> 'Not Set'
                  THEN
                    so.ct_ConfirmedQty * so.amt_AfsNetPrice
                  ELSE
                    0
              END)
Where  db.Dim_DeliveryBlockId = so.Dim_DeliveryblockId ;
                
call vectorwise(combine 'fact_salesorder');
    DROP TABLE IF EXISTS tmp_CustomerPOsCancelDate;

    CREATE TABLE tmp_CustomerPOsCancelDate as 
    SELECT so.dd_CustomerPONo, min(dt.DateValue) AS EarliestCancelDate FROM fact_Salesorder so
    INNER JOIN dim_Date dt ON dt.dim_dateid = so.Dim_DateIdAfsCancelDate
    WHERE so.dd_CustomerPONo <> 'Not Set' AND so.Dim_DateIdAfsCancelDate <> 1 GROUP BY so.dd_CustomerPONo;


    UPDATE fact_Salesorder so
	FROM dim_Date edt,
          dim_plant pl,
          tmp_CustomerPOsCancelDate po
      SET so.Dim_DateIdEarliestSOCancelDate = edt.Dim_Dateid
    WHERE     pl.Dim_Plantid = so.Dim_Plantid
          AND edt.companycode = pl.companycode
          AND edt.DateValue = po.EarliestCancelDate
          AND so.dd_CustomerPONo = po.dd_CustomerPONo
	  AND so.dd_CustomerPONo <> 'Not Set'
AND so.Dim_DateIdEarliestSOCancelDate <> edt.Dim_Dateid;

/* LK : 29 Aug changes - amt_Subtotal3inCustConfig_Billing */

/* For AFS, dd_afsscheduleno is present. So, direct 1-1 mapping between billing and sales  */ 

UPDATE fact_salesorder so
SET amt_Subtotal3inCustConfig_Billing = 0;

UPDATE fact_salesorder so 
FROM fact_billing fb
SET so.amt_Subtotal3inCustConfig_Billing = fb.amt_customerconfigsubtotal3
WHERE so.dd_salesdocno = fb.dd_salesdlvrdocno
AND so.dd_salesitemno = fb.dd_salesdlvritemno
AND so.dd_scheduleno = fb.dd_afsscheduleno;

/* End of 29 Aug changes - amt_Subtotal3inCustConfig_Billing */

DROP TABLE IF EXISTS tmp_CustomerPOsCancelDate;

UPDATE fact_salesorder so
   FROM dim_overallstatusforcreditcheck os
SET so.dd_SalesOrderBlocked = 'X'
WHERE so.Dim_OverallStatusCreditCheckId = os.dim_overallstatusforcreditcheckid
AND ( so.Dim_DeliveryBlockId <> 1 OR so.Dim_BillingBlockid <> 1 
OR os.overallstatusforcreditcheck = 'B');

UPDATE fact_salesorder so
SET so.dd_SalesOrderBlocked = 'Not Set'
WHERE so.dd_SalesOrderBlocked IS NULL;


Drop table if exists deletepart_702;

drop table if exists VBAK_VBAP_VBEP_useinsub;
drop table if exists VBAK_VBAP_useinsub;

Create table VBAK_VBAP_VBEP_useinsub as Select VBAK_VBELN,VBAP_POSNR,VBEP_ETENR from VBAK_VBAP_VBEP;
Create table VBAK_VBAP_useinsub as select VBAP_VBELN,VBAP_POSNR from VBAK_VBAP;

call vectorwise(combine 'VBAK_VBAP_VBEP_useinsub');
call vectorwise(combine 'VBAK_VBAP_useinsub');

Create table deletepart_702
As  
 Select * from fact_salesorder 
  WHERE exists (select 1 from VBAK_VBAP_VBEP_useinsub where VBAK_VBELN = dd_SalesDocNo)
      and not exists (select 1 from VBAK_VBAP_VBEP_useinsub where VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo)
AND dd_scheduleno<>0;


call vectorwise ( combine 'fact_salesorder-deletepart_702');
call vectorwise(combine 'fact_salesorder');

drop table if exists deletepart_702;

      
Create table deletepart_702
AS
Select * from fact_salesorder 
  WHERE exists (select 1 from VBAK_VBAP_VBEP_useinsub where VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo)
      and not exists (select 1 from VBAK_VBAP_VBEP_useinsub where VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo)
AND dd_scheduleno<>0;

call vectorwise(combine 'deletepart_702');
call vectorwise ( combine 'fact_salesorder-deletepart_702');
call vectorwise(combine 'fact_salesorder');


drop table if exists deletepart_702;
Create table deletepart_702
AS
Select *  FROM fact_salesorder
  WHERE dd_ScheduleNo = 0
        AND EXISTS
                (SELECT 1 FROM VBAK_VBAP_useinsub WHERE VBAP_VBELN = dd_SalesDocNo)
        AND NOT EXISTS
                    (SELECT 1 FROM VBAK_VBAP_useinsub WHERE VBAP_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo);

call vectorwise(combine 'deletepart_702');
call vectorwise ( combine 'fact_salesorder-deletepart_702');
call vectorwise(combine 'fact_salesorder');

/* Need to check below condition with Shanthi */
DROP Table if exists tmp_MinSalesSchedules_0;
CREATE table tmp_MinSalesSchedules_0
AS 
SELECT dd_SalesDocNo,dd_SalesItemNo,min(dd_ScheduleNo) as MinScheduleNo
from facT_salesorder
where dd_ScheduleNo <> 0
group by dd_SalesDocNo,dd_SalesItemNo;

drop table if exists deletepart_702;
Create table deletepart_702
AS
Select *  FROM fact_salesorder f
  WHERE dd_ScheduleNo = 0
        AND EXISTS
                (SELECT 1 FROM tmp_MinSalesSchedules_0 t WHERE t.dd_SalesDocNo = f.dd_SalesDocNo
                  AND t.dd_SalesItemNo = f.dd_SalesItemNo);

call vectorwise(combine 'deletepart_702');
call vectorwise ( combine 'fact_salesorder-deletepart_702');
call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder so
  FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm
   SET so.dim_Customermastersalesid = cms.dim_Customermastersalesid
 WHERE     so.dim_salesorgid = sorg.dim_salesorgid
       AND so.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = so.dim_salesdivisionid
       AND cm.dim_customerid = so.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber;

call vectorwise(combine 'fact_salesorder');

drop table if exists deletepart_702;
drop table if exists VBAK_VBAP_VBEP_useinsub;
drop table if exists VBAK_VBAP_useinsub;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.amt_Cost =
            Decimal((sd.amt_Cost_DocCurr
            * Decimal((CASE
                  WHEN f.amt_ExchangeRate < 0
                  THEN
                      (1 / (case when (-1 * f.amt_ExchangeRate)=0 then null else (-1 * f.amt_ExchangeRate) end))
                  ELSE
                      f.amt_ExchangeRate
                END),18,4)),18,4)
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET  sd.ct_PriceUnit = f.ct_PriceUnit
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.ct_PriceUnit<>  f.ct_PriceUnit;


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET 	sd.amt_UnitPrice = f.amt_UnitPrice, sd.amt_UnitPriceUoM = f.amt_UnitPriceUoM
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.amt_UnitPrice <> f.amt_UnitPrice;


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET   sd.amt_ExchangeRate = f.amt_ExchangeRate
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.amt_ExchangeRate <> f.amt_ExchangeRate;
        

  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET 	sd.amt_ExchangeRate_GBL = ifnull(f.amt_ExchangeRate_GBL,1)
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.amt_ExchangeRate_GBL <>  ifnull(f.amt_ExchangeRate_GBL,1);


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET  sd.Dim_DateidSalesOrderCreated = f.Dim_DateidSalesOrderCreated
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_DateidSalesOrderCreated <>  f.Dim_DateidSalesOrderCreated ;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET         sd.Dim_DateidSchedDeliveryReq = f.Dim_DateidSchedDeliveryReq
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_DateidSchedDeliveryReq <> f.Dim_DateidSchedDeliveryReq;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_DateidSchedDlvrReqPrev = f.Dim_DateidSchedDlvrReqPrev
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_DateidSchedDlvrReqPrev <> f.Dim_DateidSchedDlvrReqPrev;


          UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_DateidMatlAvailOriginal = f.Dim_DateidMtrlAvail
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_DateidMatlAvailOriginal <> f.Dim_DateidMtrlAvail;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_DateidFirstDate = f.Dim_DateidFirstDate
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_DateidFirstDate <> f.Dim_DateidFirstDate;


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_Currencyid = f.Dim_Currencyid
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_Currencyid <> f.Dim_Currencyid;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_Companyid = f.Dim_Companyid
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_Companyid <> f.Dim_Companyid;


          UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_SalesDivisionid = f.Dim_SalesDivisionid
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_SalesDivisionid <> f.Dim_SalesDivisionid;


          UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_ShipReceivePointid = f.Dim_ShipReceivePointid
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_ShipReceivePointid <> f.Dim_ShipReceivePointid;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_DocumentCategoryid <> f.Dim_DocumentCategoryid;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET  sd.Dim_SalesDocumentTypeid = f.Dim_SalesDocumentTypeid
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND     sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_SalesDocumentTypeid <> f.Dim_SalesDocumentTypeid ;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_SalesOrgid = f.Dim_SalesOrgid
  WHERE     f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND  sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_SalesOrgid <> f.Dim_SalesOrgid ;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_SalesGroupid = f.Dim_SalesGroupid
  WHERE     f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND  sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_SalesGroupid <> f.Dim_SalesGroupid;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_CostCenterid = f.Dim_CostCenterid
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_CostCenterid <> f.Dim_CostCenterid;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_BillingBlockid = f.Dim_BillingBlockid
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_BillingBlockid <>  f.Dim_BillingBlockid;


          UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_TransactionGroupid = f.Dim_TransactionGroupid
 WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND    sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_TransactionGroupid <> f.Dim_TransactionGroupid;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET  sd.Dim_CustomeridSoldTo =
            CASE sd.Dim_CustomeridSoldTo
              WHEN 1 THEN f.Dim_CustomerID
              ELSE sd.Dim_CustomeridSoldTo
            END
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND    sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_CustomerGroup1id = f.Dim_CustomerGroup1id
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_CustomerGroup1id <>  f.Dim_CustomerGroup1id;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET  sd.Dim_CustomerGroup2id = f.Dim_CustomerGroup2id
  WHERE  f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_CustomerGroup2id <> f.Dim_CustomerGroup2id;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.dim_salesorderitemcategoryid = f.dim_salesorderitemcategoryid
  WHERE     f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.dim_salesorderitemcategoryid  <> f.dim_salesorderitemcategoryid;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_ScheduleLineCategoryId = f.Dim_ScheduleLineCategoryId
  WHERE       f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR  AND  sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_ScheduleLineCategoryId <> f.Dim_ScheduleLineCategoryId;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.ct_AfsTotalDrawn = f.ct_AfsTotalDrawn
  WHERE       f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR  AND  sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.ct_AfsTotalDrawn <> f.ct_AfsTotalDrawn;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.dd_ReferenceDocNo = f.dd_ReferenceDocumentNo
  WHERE       f.dd_SalesDocNo = v.VBAK_VBELN AND f.dd_SalesItemNo = v.VBAP_POSNR AND f.dd_ScheduleNo = v.VBEP_ETENR  AND  sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.dd_ReferenceDocNo <> f.dd_ReferenceDocumentNo;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_AfsAllocationGroupNo = so.dd_AfsAllocationGroupNo
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.dd_AfsAllocationGroupNo <> so.dd_AfsAllocationGroupNo;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_ShippingConditionId = so.Dim_ShippingConditionId
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo;

call vectorwise  (combine 'fact_salesorder');
call vectorwise  (combine 'fact_salesorderdelivery');

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_DateIdSOItemChangedOn = ifnull(so.Dim_DateIdSOItemChangedOn,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.Dim_DateIdSOItemChangedOn <> ifnull(so.Dim_DateIdSOItemChangedOn,1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_SalesOrderItemStatusId = ifnull(so.Dim_SalesOrderItemStatusId,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_SalesOrderItemStatusId,1) <> ifnull(so.Dim_SalesOrderItemStatusId,1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_SalesOrderHeaderStatusId = ifnull(so.Dim_SalesOrderHeaderStatusId,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_SalesOrderHeaderStatusId,1) <> ifnull(so.Dim_SalesOrderHeaderStatusId,1);
 
update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_CustomerMaterial = ifnull(so.dd_CustomerMaterialNo,'Not Set')
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.dd_CustomerMaterial <> ifnull(so.dd_CustomerMaterialNo,'Not Set');

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_DeliveryBlockId = so.Dim_DeliveryBlockId
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.Dim_DeliveryBlockId <> so.Dim_DeliveryBlockId;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_OverallStatusCreditCheckId = so.Dim_OverallStatusCreditCheckId
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.Dim_OverallStatusCreditCheckId <> so.Dim_OverallStatusCreditCheckId;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_MaterialPriceGroup4Id = so.Dim_MaterialPriceGroup4Id
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup4Id,1) <> so.Dim_MaterialPriceGroup4Id;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_MaterialPriceGroup5Id = so.Dim_MaterialPriceGroup5Id
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup5Id,1) <> so.Dim_MaterialPriceGroup5Id;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_SalesDistrictId = so.Dim_SalesDistrictId
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_SalesDistrictId,1) <> so.Dim_SalesDistrictId;
      
/* call bi_populate_customer_hierarchy */

Drop table if exists variable_holder_702;
Drop table if exists Dim_CostCenter_702;
Drop table if exists  deletepart_702;
drop table if exists VBAK_VBAP_VBEP_702;
drop table if exists staging_upd_702;
