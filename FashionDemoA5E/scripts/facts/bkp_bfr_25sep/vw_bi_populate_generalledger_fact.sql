

/* Update 1 */

UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET amt_InLocalCurrency = (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_DMBTR
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND amt_InLocalCurrency <> (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_DMBTR;



UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET amt_TaxInDocCurrency = (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_WMWST
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND  amt_TaxInDocCurrency <> (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_WMWST;


       UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET amt_TaxInLocalCurrency = (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_MWSTS
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND amt_TaxInLocalCurrency <> (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_MWSTS;


       UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_AccountingDocItemNo = BSIS_BUZEI
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AccountingDocItemNo  <> BSIS_BUZEI;


       UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_AccountingDocNo = BSIS_BELNR
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AccountingDocNo <>  BSIS_BELNR;



       UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_AssignmentNumber = ifnull(BSIS_ZUONR, 'Not Set')
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AssignmentNumber <>  ifnull(BSIS_ZUONR, 'Not Set');

UPDATE fact_generalledger fgl 
from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_ClearedFlagId =
          ifnull((SELECT Dim_GeneralLedgerStatusId
                    FROM Dim_GeneralLedgerStatus gls
                   WHERE gls.Status = 'O - Open' AND gls.RowIsCurrent = 1),
                 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


      UPDATE fact_generalledger fgl
from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET  Dim_DateIdBaseDateForDueDateCalc =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSIS_ZFBDT AND dt.CompanyCode = glc.BSIS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;

       UPDATE fact_generalledger fgl
from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DateIdDocument =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSIS_BLDAT AND dt.CompanyCode = glc.BSIS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


       UPDATE fact_generalledger fgl
from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DateIdPosting =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSIS_BUDAT AND dt.CompanyCode = glc.BSIS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;



UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DateIdReferenceDate =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSIS_DABRZ AND dt.CompanyCode = glc.BSIS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;
 
UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_DebitCreditInd = (CASE WHEN BSIS_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_DebitCreditInd <> (CASE WHEN BSIS_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END);

      UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET  dd_ClearingDocumentNo = ifnull(BSIS_AUGBL, 'Not Set')
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_ClearingDocumentNo  <> ifnull(BSIS_AUGBL, 'Not Set');


       UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_BusinessAreaId = ifnull(
             (SELECT Dim_BusinessAreaId
                FROM Dim_BusinessArea ba
               WHERE ba.BusinessArea = BSIS_GSBER AND ba.RowIsCurrent = 1),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;

       UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET fgl.dim_companyid = ifnull(dcm.Dim_CompanyId, 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND  fgl.dim_companyid  <>  ifnull(dcm.Dim_CompanyId, 1);

UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_Currencyid =
          ifnull((SELECT dim_currencyid
                    FROM dim_currency c
                   WHERE c.CurrencyCode = BSIS_WAERS),
                 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_GLCurrencyid =
          ifnull((SELECT dim_currencyid
                    FROM dim_currency c
                   WHERE c.CurrencyCode = BSIS_PSWSL),
                 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DateIdClearing =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSIS_AUGDT AND dt.CompanyCode = glc.BSIS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DocumentTypeId =
          ifnull((SELECT dim_documenttypetextid
                    FROM dim_documenttypetext dtt
                   WHERE dtt.type = BSIS_BLART AND dtt.RowIsCurrent = 1),
                 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_PostingKeyId =
          ifnull(
             (SELECT Dim_PostingKeyId
                FROM Dim_PostingKey pk
               WHERE     pk.PostingKey = BSIS_BSCHL
                     AND pk.SpecialGLIndicator = 'Not Set'
                     AND pk.RowIsCurrent = 1),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;




UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET fgl.Dim_ChartOfAccountsId = ifnull(coa.Dim_ChartOfAccountsId, 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
       AND  fgl.Dim_ChartOfAccountsId <>  ifnull(coa.Dim_ChartOfAccountsId, 1);



UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET fgl.Dim_FunctionalAreaId = ifnull(
             (SELECT fa.Dim_FunctionalAreaId
                FROM Dim_FunctionalArea fa
               WHERE     fa.FunctionalArea = BSIS_FKBER
                     AND fa.RowIsCurrent = 1),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;

UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       fgl.Dim_PlantId = ifnull((SELECT p.Dim_PlantId
                FROM Dim_Plant p
               WHERE     p.PlantCode = BSIS_WERKS
                     AND p.RowIsCurrent = 1),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET fgl.Dim_ProfitCenterId = ifnull((SELECT Dim_ProfitCenterId from Dim_ProfitCenter pc
       WHERE pc.ProfitCenterCode = BSIS_PRCTR
       AND pc.ControllingArea = ('Not Set')
       AND pc.RowIsCurrent = 1) , 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


/* End of Update 1 */

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_generalledger';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_generalledger', ifnull(max(fact_generalledgerid), 0)
     FROM fact_generalledger;


INSERT INTO fact_generalledger(fact_generalledgerid,
                               amt_InDocCurrency,
                               amt_InLocalCurrency,
                               amt_TaxInDocCurrency,
                               amt_TaxInLocalCurrency,
                               dd_AccountingDocItemNo,
                               dd_AccountingDocNo,
                               dd_AssignmentNumber,
                               Dim_ClearedFlagId,
                               Dim_DateIdBaseDateForDueDateCalc,
                               Dim_DateIdDocument,
                               Dim_DateIdPosting,
                               Dim_DateIdReferenceDate,
                               dd_DebitCreditInd,
                               dd_ClearingDocumentNo,
                               dd_FiscalPeriod,
                               dd_FiscalYear,
                               Dim_BusinessAreaId,
                               Dim_CompanyId,
                               Dim_CurrencyId,
                               Dim_GLCurrencyId,
                               Dim_DateIdClearing,
                               Dim_DocumentTypeId,
                               Dim_PostingKeyId,
                               Dim_ChartOfAccountsId,
                               Dim_FunctionalAreaId,
                               Dim_PlantId,
                               Dim_ProfitCenterId)
   SELECT (SELECT max_id
             FROM NUMBER_FOUNTAIN
            WHERE table_name = 'fact_generalledger')
          + row_number()
             over
 (),
       (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSIS_WRBTR amt_InDocCurrency,
          (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSIS_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSIS_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSIS_MWSTS amt_TaxInLocalCurrency,
          BSIS_BUZEI dd_AccountingDocItemNo,
          BSIS_BELNR dd_AccountingDocNo,
          ifnull(BSIS_ZUONR,'Not Set') dd_AssignmentNumber,
          ifnull((SELECT Dim_GeneralLedgerStatusId
             FROM Dim_GeneralLedgerStatus gls
             WHERE gls.Status = 'O - Open'
             AND gls.RowIsCurrent = 1), 1) Dim_ClearedFlagId,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSIS_ZFBDT
              AND dt.CompanyCode = glc.BSIS_BUKRS
          ), 1) Dim_DateIdBaseDateForDueDateCalc,
          ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSIS_BLDAT
             AND  dt.CompanyCode = glc.BSIS_BUKRS
          ), 1) Dim_DateIdDocument,
          ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSIS_BUDAT
              AND dt.CompanyCode = glc.BSIS_BUKRS)
          , 1) Dim_DateIdPosting,
          ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSIS_DABRZ
              AND dt.CompanyCode = glc.BSIS_BUKRS)
          , 1) Dim_DateIdReferenceDate,
          (CASE WHEN BSIS_SHKZG ='H' THEN 'Credit' ELSE 'Debit' END) dd_DebitCreditInd,
          ifnull(BSIS_AUGBL,'Not Set') dd_ClearingDocumentNo,
          BSIS_MONAT dd_FiscalPeriod,
          BSIS_GJAHR dd_FiscalYear,
          ifnull( ( SELECT Dim_BusinessAreaId
           FROM Dim_BusinessArea ba
           WHERE ba.BusinessArea = BSIS_GSBER
           AND ba.RowIsCurrent = 1), 1) Dim_BusinessAreaId,
          ifnull(dc.Dim_CompanyId, 1),
          ifnull((SELECT dim_currencyid
             FROM dim_currency c
            WHERE c.CurrencyCode = BSIS_WAERS ),1)  Dim_Currencyid,
          ifnull((SELECT dim_currencyid
             FROM dim_currency c
            WHERE c.CurrencyCode = BSIS_PSWSL ),1)  Dim_GLCurrencyid,
          ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSIS_AUGDT
                  AND dt.CompanyCode = glc.BSIS_BUKRS), 1) Dim_DateIdClearing,
          ifnull(( SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.type = BSIS_BLART
              AND dtt.RowIsCurrent = 1 ), 1) Dim_DocumentTypeId,
          ifnull( ( SELECT Dim_PostingKeyId
            FROM Dim_PostingKey pk
            WHERE pk.PostingKey= BSIS_BSCHL
             AND pk.SpecialGLIndicator = 'Not Set'
            AND pk.RowIsCurrent = 1), 1) Dim_PostingKeyId,
            ifnull(coa.Dim_ChartOfAccountsId, 1),
          ifnull(
             (SELECT fa.Dim_FunctionalAreaId
                FROM Dim_FunctionalArea fa
               WHERE     fa.FunctionalArea = BSIS_FKBER
                     AND fa.RowIsCurrent = 1),
             1),
          ifnull((SELECT p.Dim_PlantId
                FROM Dim_Plant p
               WHERE     p.PlantCode = BSIS_WERKS
                     AND p.RowIsCurrent = 1),
             1),
          ifnull((SELECT Dim_ProfitCenterId from Dim_ProfitCenter pc
              WHERE pc.ProfitCenterCode = BSIS_PRCTR
                AND pc.ControllingArea = ('Not Set')
            AND pc.RowIsCurrent = 1) , 1)
     FROM BSIS glc
      INNER JOIN dim_company dc ON dc.CompanyCode = glc.BSIS_BUKRS AND dc.RowIsCurrent = 1
      INNER JOIN dim_chartofaccounts coa ON coa.GLAccountNumber = glc.BSIS_HKONT AND coa.CharOfAccounts = dc.ChartOfAccounts AND coa.RowIsCurrent = 1
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_generalledger ar
                   WHERE     ar.dd_AccountingDocNo = glc.BSIS_BELNR
                         AND ar.dd_AccountingDocItemNo = glc.BSIS_BUZEI
                         AND ar.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR,'Not Set')
                         AND ar.dd_fiscalyear = glc.BSIS_GJAHR
                         AND ar.dd_FiscalPeriod  = glc.BSIS_MONAT
                         AND ar.dim_companyid = dc.Dim_CompanyId
                         AND ar.Dim_ChartOfAccountsId = coa.Dim_ChartOfAccountsId);


/* Update 2 */

UPDATE fact_generalledger fgl
from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET amt_InLocalCurrency = (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_DMBTR
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND  amt_InLocalCurrency <> (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_DMBTR;


UPDATE fact_generalledger fgl
from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET        amt_TaxInDocCurrency =
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_WMWST
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND  amt_TaxInDocCurrency  <>  (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_WMWST;


       UPDATE fact_generalledger fgl
from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET amt_TaxInLocalCurrency =
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_MWSTS
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND amt_TaxInLocalCurrency <> (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_MWSTS;


       UPDATE fact_generalledger fgl
from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_AccountingDocItemNo = BSAS_BUZEI
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AccountingDocItemNo <>  BSAS_BUZEI;


UPDATE fact_generalledger fgl
from BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_AccountingDocNo = BSAS_BELNR
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AccountingDocNo <> BSAS_BELNR;


UPDATE fact_generalledger fgl
from BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_AssignmentNumber = ifnull(BSAS_ZUONR, 'Not Set')
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AssignmentNumber <> ifnull(BSAS_ZUONR, 'Not Set');

UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       Dim_ClearedFlagId =
          ifnull(
             (SELECT Dim_GeneralLedgerStatusId
                FROM Dim_GeneralLedgerStatus gls
               WHERE gls.Status = 'C - Cleared' AND gls.RowIsCurrent = 1),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;




UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET       Dim_DateIdBaseDateForDueDateCalc =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSAS_ZFBDT AND dt.CompanyCode = glc.BSAS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


      
UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DateIdDocument =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSAS_BLDAT AND dt.CompanyCode = glc.BSAS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


      
UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DateIdPosting =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSAS_BUDAT AND dt.CompanyCode = glc.BSAS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;

UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DateIdReferenceDate =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSAS_DABRZ AND dt.CompanyCode = glc.BSAS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_DebitCreditInd =
          (CASE BSAS_SHKZG WHEN 'H' THEN 1 WHEN 'S' THEN -1 END)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_DebitCreditInd <> (CASE BSAS_SHKZG WHEN 'H' THEN 1 WHEN 'S' THEN -1 END);


UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET dd_ClearingDocumentNo = ifnull(BSAS_AUGBL, 'Not Set')
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       Dim_BusinessAreaId =
          ifnull(
             (SELECT Dim_BusinessAreaId
                FROM Dim_BusinessArea ba
               WHERE ba.BusinessArea = BSAS_GSBER AND ba.RowIsCurrent = 1),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       fgl.dim_companyid = ifnull(dcm.Dim_CompanyId, 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;

UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       Dim_Currencyid =
          ifnull((SELECT dim_currencyid
                    FROM dim_currency c
                   WHERE c.CurrencyCode = BSAS_WAERS),
                 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;




UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET       Dim_GLCurrencyid =
          ifnull((SELECT dim_currencyid
                    FROM dim_currency c
                   WHERE c.CurrencyCode = BSAS_PSWSL),
                 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;



      UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DateIdClearing =
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = BSAS_AUGDT AND dt.CompanyCode = glc.BSAS_BUKRS),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;



      UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET Dim_DocumentTypeId =
          ifnull((SELECT dim_documenttypetextid
                    FROM dim_documenttypetext dtt
                   WHERE dtt.type = BSAS_BLART AND dtt.RowIsCurrent = 1),
                 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       Dim_PostingKeyId =
          ifnull(
             (SELECT Dim_PostingKeyId
                FROM Dim_PostingKey pk
               WHERE     pk.PostingKey = BSAS_BSCHL
                     AND pk.SpecialGLIndicator = 'Not Set'
                     AND pk.RowIsCurrent = 1),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;




UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       fgl.Dim_ChartOfAccountsId = ifnull(coa.Dim_ChartOfAccountsId, 1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND fgl.Dim_ChartOfAccountsId  <> ifnull(coa.Dim_ChartOfAccountsId, 1) ;




UPDATE fact_generalledger fgl
                              from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       fgl.Dim_FunctionalAreaId =  ifnull(
             (SELECT fa.Dim_FunctionalAreaId
                FROM Dim_FunctionalArea fa
               WHERE     fa.FunctionalArea = BSAS_FKBER
                     AND fa.RowIsCurrent = 1),
             1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


UPDATE fact_generalledger fgl
from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       fgl.Dim_PlantId = ifnull((SELECT p.Dim_PlantId
                FROM Dim_Plant p
               WHERE     p.PlantCode = BSAS_WERKS
                     AND p.RowIsCurrent = 1),1)
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;



UPDATE fact_generalledger fgl
from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa
   SET
       fgl.Dim_ProfitCenterId = ifnull((SELECT Dim_ProfitCenterId from Dim_ProfitCenter pc
       WHERE pc.ProfitCenterCode = BSAS_PRCTR
       AND pc.ControllingArea = ('Not Set')
       AND pc.RowIsCurrent = 1) , 1)

 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


/* End of Update 2 */

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_generalledger';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_generalledger', ifnull(max(fact_generalledgerid), 0)
     FROM fact_generalledger;

INSERT INTO fact_generalledger(fact_generalledgerid,
                               amt_InDocCurrency,
                               amt_InLocalCurrency,
                               amt_TaxInDocCurrency,
                               amt_TaxInLocalCurrency,
                               dd_AccountingDocItemNo,
                               dd_AccountingDocNo,
                               dd_AssignmentNumber,
                               Dim_ClearedFlagId,
                               Dim_DateIdBaseDateForDueDateCalc,
                               Dim_DateIdDocument,
                               Dim_DateIdPosting,
                               dd_DebitCreditInd,
                               dd_ClearingDocumentNo,
                               dd_FiscalPeriod,
                               dd_FiscalYear,
                               Dim_BusinessAreaId,
                               Dim_CompanyId,
                               Dim_CurrencyId,
                               Dim_GLCurrencyId,
                               Dim_DateIdClearing,
                               Dim_DateIdReferenceDate,
                               Dim_DocumentTypeId,
                               Dim_PostingKeyId,
                               Dim_ChartOfAccountsId,
                               Dim_FunctionalAreaId,
                               Dim_PlantId,
                               Dim_ProfitCenterId)
   SELECT (SELECT max_id
             FROM NUMBER_FOUNTAIN
            WHERE table_name = 'fact_generalledger')
          + row_number()
             over
 (),
       (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAS_WRBTR amt_InDocCurrency,
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAS_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAS_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAS_MWSTS amt_TaxInLocalCurrency,
          BSAS_BUZEI dd_AccountingDocItemNo,
          BSAS_BELNR dd_AccountingDocNo,
          ifnull(BSAS_ZUONR, 'Not Set') dd_AssignmentNumber,
          ifnull((SELECT Dim_GeneralLedgerStatusId
             FROM Dim_GeneralLedgerStatus gls
             WHERE gls.Status = 'C - Cleared'
             AND gls.RowIsCurrent = 1), 1) Dim_ClearedFlagId,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAS_ZFBDT
                  AND dt.CompanyCode = BSAS_BUKRS
                  ), 1 ) Dim_DateIdBaseDateForDueDateCalc,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAS_BLDAT
                  AND dt.CompanyCode = BSAS_BUKRS
                  ), 1) Dim_DateIdDocument,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAS_BUDAT
                  AND dt.CompanyCode = BSAS_BUKRS
                  ), 1) Dim_DateIdPosting,
          BSAS_SHKZG  dd_DebitCreditInd,
          ifnull(BSAS_AUGBL,'Not Set') dd_ClearingDocumentNo,
          BSAS_MONAT dd_FiscalPeriod,
          BSAS_GJAHR dd_FiscalYear,
          ifnull(( SELECT Dim_BusinessAreaId
           FROM Dim_BusinessArea ba
           WHERE ba.BusinessArea = BSAS_GSBER
           AND ba.RowIsCurrent = 1
           ), 1) Dim_BusinessAreaId,
          ifnull(dc.Dim_CompanyId,1),
          ifnull((SELECT dim_currencyid
             FROM dim_currency c
            WHERE c.CurrencyCode = BSAS_WAERS)
            , 1) Dim_Currencyid,
          ifnull((SELECT dim_currencyid
             FROM dim_currency c
            WHERE c.CurrencyCode = BSAS_PSWSL)
            , 1) Dim_GLCurrencyid,
          ifnull((SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAS_AUGDT
                  AND dt.CompanyCode = BSAS_BUKRS
                  ), 1) Dim_DateIdClearing,
          ifnull( (SELECT dim_dateid
             FROM dim_date dt
            WHERE dt.DateValue = BSAS_DABRZ
              AND dt.CompanyCode = glc.BSAS_BUKRS)
          , 1) Dim_DateIdReferenceDate,
          ifnull((SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.type = BSAS_BLART
              AND dtt.RowIsCurrent = 1
              ), 1) Dim_DocumentTypeId,
          ifnull(( SELECT Dim_PostingKeyId
            FROM Dim_PostingKey pk
            WHERE pk.PostingKey= BSAS_BSCHL
            AND pk.SpecialGLIndicator = 'Not Set'
            AND pk.RowIsCurrent = 1
            ), 1) Dim_PostingKeyId,
            ifnull(coa.Dim_ChartOfAccountsId, 1),
            ifnull(
             (SELECT fa.Dim_FunctionalAreaId
                FROM Dim_FunctionalArea fa
               WHERE     fa.FunctionalArea = BSAS_FKBER
                     AND fa.RowIsCurrent = 1),
             1),
             ifnull((SELECT p.Dim_PlantId
                FROM Dim_Plant p
               WHERE     p.PlantCode = BSAS_WERKS
                     AND p.RowIsCurrent = 1),
                   1),
      ifnull((SELECT Dim_ProfitCenterId FROM Dim_ProfitCenter pc
            WHERE pc.ProfitCenterCode = BSAS_PRCTR
            AND pc.ControllingArea = ('Not Set')
            AND pc.RowIsCurrent = 1) , 1)
            FROM BSAS glc
      INNER JOIN dim_company dc ON dc.CompanyCode = BSAS_BUKRS AND dc.RowIsCurrent = 1
      INNER JOIN dim_chartofaccounts coa ON coa.GLAccountNumber = glc.BSAS_HKONT AND coa.CharOfAccounts = dc.ChartOfAccounts AND coa.RowIsCurrent = 1
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_generalledger ar
                   WHERE     ar.dd_AccountingDocNo = glc.BSAS_BELNR
                         AND ar.dd_AccountingDocItemNo = glc.BSAS_BUZEI
                         AND ar.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
                         AND ar.dd_fiscalyear = glc.BSAS_GJAHR
                         AND ar.dd_FiscalPeriod  = glc.BSAS_MONAT
                         AND ar.dim_companyid = dc.Dim_CompanyId
                         AND ar.Dim_ChartOfAccountsId = coa.Dim_ChartOfAccountsId);



/* Update 3 */

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET
 gl.dd_RefDocNo = mm.dd_MaterialDocNo
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND  ifnull(gl.dd_RefDocNo,'xx') <> ifnull(mm.dd_MaterialDocNo,'yy');

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.Dim_VendorId = mm.Dim_Vendorid
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.Dim_VendorId <> mm.Dim_Vendorid;

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.Dim_MovementTypeId = mm.Dim_MovementTypeid
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.Dim_MovementTypeId,-1) <> ifnull(mm.Dim_MovementTypeid,-2);

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.dd_DocumentNo = mm.dd_DocumentNo
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.dd_DocumentNo,'xx') <> ifnull(mm.dd_DocumentNo,'yy');

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.dd_DocumentItemNo = mm.dd_DocumentItemNo
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.dd_DocumentItemNo,-1) <> ifnull(mm.dd_DocumentItemNo,-2);

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.Dim_PartId = mm.Dim_PartId
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.Dim_PartId <> mm.Dim_PartId;

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.Dim_PurchasePlantId = mm.Dim_Plantid
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.Dim_PurchasePlantId <> mm.Dim_Plantid;

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.ct_Quantity = mm.ct_Quantity
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.ct_Quantity,-2) <> ifnull(mm.ct_Quantity,-2);

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.amt_StdPrice = w.STPRS
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.amt_StdPrice,-1) <> ifnull(w.STPRS,-2);

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl
SET gl.amt_UnitPrice = w.PEINH
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.amt_UnitPrice,-1) <> ifnull(w.PEINH,-2);



/* Update 4 */

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, Dim_Part pt, Dim_Plant pl
SET
 gl.Dim_ReceivingPartId = mm.Dim_ReceivingPartId
WHERE     gl.dd_AccountingDocNo = mm.dd_MaterialDocNo
 AND mm.Dim_ReceivingPartid = pt.Dim_Partid
 AND mm.Dim_ReceivingPlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND  gl.Dim_ReceivingPartId <> mm.Dim_ReceivingPartId;

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, Dim_Part pt, Dim_Plant pl
SET gl.Dim_ReceivingPlantId = mm.Dim_ReceivingPlantid
WHERE     gl.dd_AccountingDocNo = mm.dd_MaterialDocNo
 AND mm.Dim_ReceivingPartid = pt.Dim_Partid
 AND mm.Dim_ReceivingPlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.Dim_ReceivingPlantId <> mm.Dim_ReceivingPlantid;

UPDATE fact_generalledger gl
from fact_materialmovement mm, MBEW w, Dim_Part pt, Dim_Plant pl
SET gl.amt_StdPriceatRecPlant = w.STPRS
WHERE     gl.dd_AccountingDocNo = mm.dd_MaterialDocNo
 AND mm.Dim_ReceivingPartid = pt.Dim_Partid
 AND mm.Dim_ReceivingPlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.amt_StdPriceatRecPlant <> w.STPRS;


DROP TABLE IF EXISTS TMP_amt_UnitPrice_GL;
CREATE TABLE TMP_amt_UnitPrice_GL
AS
SELECT p.dd_DocumentNo,p.dd_DocumentItemNo, AVG(p.amt_UnitPrice) as avg_amt_UnitPrice
FROM fact_purchase p,fact_generalledger gl
WHERE p.dd_DocumentNo = gl.dd_DocumentNo
AND p.dd_DocumentItemNo = gl.dd_DocumentItemNo
GROUP BY p.dd_DocumentNo,p.dd_DocumentItemNo;


UPDATE fact_generalledger gl FROM TMP_amt_UnitPrice_GL p
   SET gl.amt_POUnitPrice = p.avg_amt_UnitPrice         
            WHERE p.dd_DocumentNo = gl.dd_DocumentNo
                  AND p.dd_DocumentItemNo = gl.dd_DocumentItemNo
		  AND ifnull(gl.amt_POUnitPrice, 0) <> ifnull(p.avg_amt_UnitPrice, 0);

DROP TABLE IF EXISTS TMP_amt_UnitPrice_GL;

call vectorwise(combine 'fact_generalledger');
