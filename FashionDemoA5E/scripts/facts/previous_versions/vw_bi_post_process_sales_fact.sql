update fact_salesorder fo from vbak_vbap 
set dd_prodhierarchy1 = VBAP_ZZPRODH1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    and dd_prodhierarchy1 <> ifnull(VBAP_ZZPRODH1, 'Not Set');
    
update fact_salesorder fo from vbak_vbap 
set dd_prodhierarchy2 = VBAP_ZZPRODH2
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    and dd_prodhierarchy2 <> ifnull(VBAP_ZZPRODH2, 'Not Set');
    
update fact_salesorder fo from vbak_vbap 
set 
dd_prodhierarchy3 = VBAP_ZZPRODH3
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    and dd_prodhierarchy3 <> ifnull(VBAP_ZZPRODH3, 'Not Set');
    
update fact_salesorder fo from vbak_vbap 
set
dd_prodhierarchy4 = VBAP_ZZPRODH4
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    and dd_prodhierarchy4 <> ifnull(VBAP_ZZPRODH4, 'Not Set');

UPDATE fact_salesorder so
FROM dim_therapeuticclass t, dim_part dp
SET  so.dim_therapeuticclassid_hdsmith = t.dim_therapeuticclassid
where so.dim_partid = dp.dim_partid and 
dp.TherapeuticClass_HdSmith = t.TherapeuticClass;

UPDATE fact_salesorder so
FROM dim_FineClass f, dim_part dp
SET  so.dim_FineClassId_hdsmith = f.dim_FineClassId
where so.dim_partid = dp.dim_partid and 
dp.FineLineClass_HdSmith = f.FineClass;

UPDATE fact_salesorder so
set so.dim_therapeuticclassid_hdsmith = 1
WHERE  so.dim_therapeuticclassid_hdsmith IS NULL;

UPDATE fact_salesorder so
set so.dim_FineClassId_hdsmith = 1
WHERE  so.dim_FineClassId_hdsmith IS NULL;

/* Updates for Custom Measures */
drop table if exists temp_konv_hds_so;

create table temp_konv_hds_so as 
select 
KONV_KNUMV,KONV_KPOSN,KONV_KSCHL,
AVG(KONV_KBETR) as AVG_KBETR,
AVG(KONV_KWERT) as AVG_KWERT,
SUM(KONV_KWERT) as SUM_KWERT
from KONV_HDS
group by KONV_KNUMV,KONV_KPOSN,KONV_KSCHL;

/*Already Extended measures=multiplied by qty already in source have to be distributed between schedules,another temp table is needed*/
/*this will be used to add and compute distributed measures*/

drop table if exists tmp_fact_salesorder_upd_distributions;

create table tmp_fact_salesorder_upd_distributions as (
select 
dd_SalesDocNo,
dd_SalesItemNo,
dd_scheduleno,
dd_ConditionNo,
ct_ConfirmedQty,
ifnull(sum(ct_ConfirmedQty) over (partition by dd_SalesDocNo,dd_SalesItemNo),0) as sum_qty,
cast(0 as decimal(18,4)) as distribution_factor
from fact_salesorder 
);

update tmp_fact_salesorder_upd_distributions
set distribution_factor=ifnull(ct_ConfirmedQty/case when sum_qty=0 then nullif(1,1) else sum_qty end,0);

call vectorwise (combine 'tmp_fact_salesorder_upd_distributions');


/*cannot join fact_salesorder,tmp_fact_salesorder_upd_distributions and temp_konv_hds_so at the same time as join will fail due to very high no of rows and memory consumption*/


/*combination KONV_KNUMV,KONV_KPOSN,KONV_KSCHL is unique in temp_konv_hds_so table*/
/*unique key in Fact table will get duplicated only if it points to different KONV_KSCHL*/
/*rank over fact_salesorder natural key and create hardcoded hierarchy*/
/* Retail Margin field is stored times 10 on HdSmith side*/

drop table if exists temp_konv_hds_so_hierarchy;

create table temp_konv_hds_so_hierarchy as (
select 
tso.dd_SalesDocNo,
tso.dd_SalesItemNo,
tso.dd_scheduleno,
((k.AVG_KBETR/10) * tso.distribution_factor) as amt_RetailMargin_Distributed,
rank() over (
partition by tso.dd_SalesDocNo,tso.dd_SalesItemNo,tso.dd_scheduleno
order by case when KONV_KSCHL='ZIVE' then 1 
			  when KONV_KSCHL='ZRMP' then 2
			  when KONV_KSCHL='ZFLC' then 3
			  when KONV_KSCHL='ZDEF' then 4
		 end ) as rank
from tmp_fact_salesorder_upd_distributions tso
inner join temp_konv_hds_so k
	on tso.dd_ConditionNo=k.KONV_KNUMV
	and tso.dd_SalesItemNo=k.KONV_KPOSN
where	
	k.KONV_KSCHL in ('ZIVE','ZRMP','ZFLC','ZDEF')
	);
	
delete from temp_konv_hds_so_hierarchy 
where rank <>1;

update fact_salesorder so
from temp_konv_hds_so_hierarchy tsoh
set 
so.amt_RetailMargin_hds=tsoh.amt_RetailMargin_Distributed
where
so.dd_SalesDocNo=tsoh.dd_SalesDocNo
and so.dd_SalesItemNo=tsoh.dd_SalesItemNo
and so.dd_scheduleno=tsoh.dd_scheduleno
and so.amt_RetailMargin_hds<>tsoh.amt_RetailMargin_Distributed;



/*
update fact_salesorder so
from temp_konv_hds_so k
set amt_RetailPrice_hds=k.AVG_KWERT
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZSRP'
*/

call vectorwise (combine 'tmp_fact_salesorder_upd_distributions');

alter table tmp_fact_salesorder_upd_distributions 
add column amt_RetailPrice_Distributed decimal(18,4) NOT NULL WITH DEFAULT;


update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_RetailPrice_Distributed=(k.AVG_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZSRP';


update fact_salesorder so
from tmp_fact_salesorder_upd_distributions tso
set 
so.amt_RetailPrice_hds=tso.amt_RetailPrice_Distributed
where
so.dd_SalesDocNo=tso.dd_SalesDocNo
and so.dd_SalesItemNo=tso.dd_SalesItemNo
and so.dd_scheduleno=tso.dd_scheduleno
and so.amt_RetailPrice_hds<>tso.amt_RetailPrice_Distributed;



update fact_salesorder so
from temp_konv_hds_so k
set amt_AWP_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZAWP';


update fact_salesorder so
from temp_konv_hds_so k
set amt_AcquisitionPrice_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZLOW';


update fact_salesorder so
from temp_konv_hds_so k
set amt_OverridePrice_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZOVR';


update fact_salesorder so
from temp_konv_hds_so k
set amt_NetPrice_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZINP';



update fact_salesorder so
from temp_konv_hds_so k
set amt_ContractPrice_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZVCC';


update fact_salesorder so
from temp_konv_hds_so k
set amt_ListPrice_WAC_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZILP';


/*
update fact_salesorder so
from temp_konv_hds_so k
set amt_Revenue_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZCAP'
*/

call vectorwise (combine 'tmp_fact_salesorder_upd_distributions');

alter table tmp_fact_salesorder_upd_distributions 
add column amt_Revenue_Distributed decimal(18,4) NOT NULL WITH DEFAULT;

 
update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_Revenue_Distributed=(k.AVG_KBETR * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZCAP';


update fact_salesorder so
from tmp_fact_salesorder_upd_distributions tso
set 
so.amt_Revenue_hds=tso.amt_Revenue_Distributed
where
so.dd_SalesDocNo=tso.dd_SalesDocNo
and so.dd_SalesItemNo=tso.dd_SalesItemNo
and so.dd_scheduleno=tso.dd_scheduleno
and so.amt_Revenue_hds<>tso.amt_Revenue_Distributed;

call vectorwise (combine 'tmp_fact_salesorder_upd_distributions');

alter table tmp_fact_salesorder_upd_distributions 
add column amt_GPO_AdminFees_Distributed decimal(18,4) NOT NULL WITH DEFAULT;

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_GPO_AdminFees_Distributed=(k.AVG_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZAGA';

update fact_salesorder so
from tmp_fact_salesorder_upd_distributions tso
set 
so.amt_GPO_AdminFees_hds=tso.amt_GPO_AdminFees_Distributed
where
so.dd_SalesDocNo=tso.dd_SalesDocNo
and so.dd_SalesItemNo=tso.dd_SalesItemNo
and so.dd_scheduleno=tso.dd_scheduleno
and so.amt_GPO_AdminFees_hds<>tso.amt_GPO_AdminFees_Distributed;

update fact_salesorder so
from temp_konv_hds_so k
set amt_BuyCost_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL = 'ZBCT';


drop table if exists temp_konv_hds_so_in3;

create table temp_konv_hds_so_in3 as 
select 
KONV_KNUMV,KONV_KPOSN,AVG(AVG_KWERT) as AVG_KWERT
from temp_konv_hds_so
where KONV_KSCHL in ('ZOI%', 'ZOI$')
group by KONV_KNUMV,KONV_KPOSN;

update fact_salesorder so
from temp_konv_hds_so_in3 k
set amt_OffInvoiceDiscount_hds=(k.AVG_KWERT/10)
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN;

/*distribution logic also needs applied to the Sales/Purchasing rebate measures*/

call vectorwise (combine 'tmp_fact_salesorder_upd_distributions');

alter table tmp_fact_salesorder_upd_distributions 
add column amt_s_Operational_Distributed decimal(18,4) NOT NULL WITH DEFAULT;
alter table tmp_fact_salesorder_upd_distributions 
add column amt_p_Operational_Distributed decimal(18,4) NOT NULL WITH DEFAULT;
alter table tmp_fact_salesorder_upd_distributions 
add column amt_s_Standard_Distributed decimal(18,4) NOT NULL WITH DEFAULT;
alter table tmp_fact_salesorder_upd_distributions 
add column amt_p_Standard_Distributed decimal(18,4) NOT NULL WITH DEFAULT;
alter table tmp_fact_salesorder_upd_distributions 
add column amt_s_Promotional_Distributed decimal(18,4) NOT NULL WITH DEFAULT;
alter table tmp_fact_salesorder_upd_distributions 
add column amt_p_Promotional_Distributed decimal(18,4) NOT NULL WITH DEFAULT;
alter table tmp_fact_salesorder_upd_distributions 
add column amt_s_Incentive_Distributed decimal(18,4) NOT NULL WITH DEFAULT;
alter table tmp_fact_salesorder_upd_distributions 
add column amt_p_Incentive_Distributed decimal(18,4) NOT NULL WITH DEFAULT;


update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_s_Operational_Distributed=(k.SUM_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZASO';

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_p_Operational_Distributed=(k.SUM_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZAPO';

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_s_Standard_Distributed=(k.SUM_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZASS';

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_p_Standard_Distributed=(k.SUM_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZAPS';

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_s_Promotional_Distributed=(k.SUM_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZASP';

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_p_Promotional_Distributed=(k.SUM_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZAPP';

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_s_Incentive_Distributed=(k.SUM_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZASI';

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_p_Incentive_Distributed=(k.SUM_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZAPI';

update fact_salesorder so
from tmp_fact_salesorder_upd_distributions tso
set 
so.amt_s_Operational_hds=tso.amt_s_Operational_Distributed,
so.amt_p_Operational_hds=tso.amt_p_Operational_Distributed,
so.amt_s_Standard_hds=tso.amt_s_Standard_Distributed,
so.amt_p_Standard_hds=tso.amt_p_Standard_Distributed,
so.amt_s_Promotional_hds=tso.amt_s_Promotional_Distributed,
so.amt_p_Promotional_hds=tso.amt_p_Promotional_Distributed,
so.amt_s_Incentive_hds=tso.amt_s_Incentive_Distributed,
so.amt_p_Incentive_hds=tso.amt_p_Incentive_Distributed
where
so.dd_SalesDocNo=tso.dd_SalesDocNo
and so.dd_SalesItemNo=tso.dd_SalesItemNo
and so.dd_scheduleno=tso.dd_scheduleno;


/*update fact_salesorder so
from temp_konv_hds_so k
set amt_Chargeback_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZASO';*/

/*change the logic for the measure Chargeback to the following: Chargeback = BuyCost - Contract Price*/

/*update fact_salesorder so
set so.amt_Chargeback_hds = (so.amt_BuyCost_hds - so.amt_ContractPrice_hds)
where
so.amt_Chargeback_hds <> (so.amt_BuyCost_hds - so.amt_ContractPrice_hds)*/

update fact_salesorder so
set so.amt_Chargeback_hds = (case 
							when so.amt_ContractPrice_hds > 0 then (so.amt_BuyCost_hds - so.amt_ContractPrice_hds)
							when so.amt_ContractPrice_hds <= 0 then 0
							end)
where
so.amt_Chargeback_hds <>   (case 
							when so.amt_ContractPrice_hds > 0 then (so.amt_BuyCost_hds - so.amt_ContractPrice_hds)
							when so.amt_ContractPrice_hds <= 0 then 0
							end);


/*
update fact_salesorder so
from temp_konv_hds_so k,dim_CustomerConditionGroups cg1
set amt_ContractCost_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZVCC'
and so.dim_CustomerConditionGroups1id=cg1.dim_CustomerConditionGroupsid
and cg1.CustomerCondGrp = 'CS'*/


update fact_salesorder so
from temp_konv_hds_so k
set amt_CustomerRebates_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZACR';



update fact_salesorder so
from temp_konv_hds_so k
set amt_GrossInvoice_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZBCT';

/*use same logic as Buy Cost for now*/

update fact_salesorder
set amt_pocost_hds = amt_BuyCost_hds;

/*update fact_salesorder so
from temp_konv_hds_so k
set amt_pocost_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL = 'PB00'*/


/*for the Cash Discount the amounts are percentages stored times 10*/

/*update fact_salesorder so
from temp_konv_hds_so k
set amt_cashdiscount_hds=(k.AVG_KBETR/10)
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZCDT';*/


/*change the logic for the measure Cash Discount to the following:  Cash Discount = Buy Cost * (2 - (select KBETR from KONV where KSCHL = ZCDT)/1000)*/

update fact_salesorder so
from temp_konv_hds_so k
set so.amt_cashdiscount_hds=so.amt_BuyCost_hds * (0.0204 - k.AVG_KBETR/1000)
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZCDT';


update fact_salesorder so
from temp_konv_hds_so k,dim_CustomerConditionGroups cg1,dim_CustomerConditionGroups cg2
set amt_ContractNetPrice_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZMI3'
and so.dim_CustomerConditionGroups1id=cg1.dim_CustomerConditionGroupsid
and so.dim_CustomerConditionGroups2id=cg2.dim_CustomerConditionGroupsid
and cg1.CustomerCondGrp = 'CS'
and cg2.CustomerCondGrp = 'NS';


/*update fact_salesorder so
from temp_konv_hds_so k
set amt_IMAFees_hds=k.AVG_KBETR
where
so.dd_ConditionNo=k.KONV_KNUMV
and so.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL = 'ZAIM';*/

call vectorwise (combine 'tmp_fact_salesorder_upd_distributions');

alter table tmp_fact_salesorder_upd_distributions 
add column amt_IMAFees_Distributed decimal(18,4) NOT NULL WITH DEFAULT;

update tmp_fact_salesorder_upd_distributions tso
from temp_konv_hds_so k
set tso.amt_IMAFees_Distributed=(k.AVG_KWERT * tso.distribution_factor)
where
tso.dd_ConditionNo=k.KONV_KNUMV
and tso.dd_SalesItemNo=k.KONV_KPOSN
and k.KONV_KSCHL ='ZAIM';

update fact_salesorder so
from tmp_fact_salesorder_upd_distributions tso
set 
so.amt_IMAFees_hds=tso.amt_IMAFees_Distributed
where
so.dd_SalesDocNo=tso.dd_SalesDocNo
and so.dd_SalesItemNo=tso.dd_SalesItemNo
and so.dd_scheduleno=tso.dd_scheduleno
and so.amt_IMAFees_hds<>tso.amt_IMAFees_Distributed;


call vectorwise (combine 'fact_salesorder');
drop table if exists temp_konv_hds_so;
drop table if exists temp_konv_hds_so_hierarchy;
drop table if exists temp_konv_hds_so_in3;
drop table if exists tmp_fact_salesorder_upd_distributions;