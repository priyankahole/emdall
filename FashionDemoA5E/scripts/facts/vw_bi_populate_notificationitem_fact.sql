DELETE FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_Qualitynotificationitem';

INSERT INTO NUMBER_FOUNTAIN(table_name,max_id)
SELECT 'fact_Qualitynotificationitem', ifnull(MAX(fact_Qualitynotificationitemid),1) FROM fact_Qualitynotificationitem; 

Drop table if exists tmp_far_variable_holder;

Declare global temporary table tmp_far_variable_holder(
	pGlobalCurrency)
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD'),varchar(3))
ON COMMIT PRESERVE ROWS;
DROP TABLE IF EXISTS fact_accountsreceivable_temp;


UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.ct_ExternalDefectiveQty = IFNULL(q1.QMFE_FMGFRD,0)
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.ct_InternalDefectiveQty = IFNULL(q1.QMFE_FMGEIG,0)
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_CreatedBy = IFNULL(q1.QMFE_ERNAM,'Not Set')
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_ChangedBy = IFNULL(q1.QMFE_AENAM,'Not Set')
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_date dt, dim_Company dc, dim_plant pl
SET fi.Dim_DateIdCreatedOn = ifnull(dt.dim_dateid, 'Not Set')
WHERE dt.DateValue = q1.QMFE_ERDAT
AND pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = IFNULL(pl.CompanyCode,'Not Set')
AND dt.CompanyCode =IFNULL(dc.CompanyCode,'Not Set')             
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_date dt, dim_Company dc, dim_plant pl
SET fi.Dim_DateIdChangedOn = ifnull(dt.dim_dateid, 'Not Set')
WHERE dt.DateValue = q1.QMFE_AEDAT
AND pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = IFNULL(pl.CompanyCode,'Not Set')
AND dt.CompanyCode =IFNULL(dc.CompanyCode,'Not Set')            
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

/*
UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_CreatedOn = q1.QMFE_ERDAT
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_ChangedOn = q1.QMFE_AEDAT
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
*/

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_ItemShortText = IFNULL(q1.QMFE_FETXT,'Not Set')
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_NotificationItemNo = IFNULL(q1.QMFE_FENUM,0)
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_NotificationNo = IFNULL(q1.QMFE_QMNUM,0)
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_plant pl, dim_Company dc 
SET fi.dim_companyid = dc.dim_Companyid
WHERE pl.plantcode = q1.QMFE_WERKS
AND dc.CompanyCode = pl.CompanyCode
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;


UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_part p 
SET fi.dim_partid = p.dim_partid
WHERE p.PartNumber = IFNULL(Q1.QMFE_MATNR,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_plant pl
SET fi.dim_plantid =pl.dim_plantid
WHERE pl.plantcode = q1.QMFE_WERKS
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_UnitOfMeasure u
SET fi.Dim_UnitOfMeasureid =u.dim_unitofmeasureid
WHERE u.uom = IFNULL(Q1.QMFE_FMGEIN,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_inspectioncatalogtype d
SET fi.Dim_InspectionCatalogTypeid = d.Dim_InspectionCatalogTypeid
WHERE d.InspectionCatalogTypeCode = q1.QMFE_FEKAT
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_notificationitemmisc dn
SET fi.dim_notificationitemmiscid = dn.dim_notificationitemmiscid
WHERE dn.originaldefect = IFNULL(q1.QMFE_KZORG,'Not Set') 
AND dn.repetitivedefect = IFNULL(q1.QMFE_WDFEH,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_codetext c
SET fi.Dim_codetextid = c.Dim_codetextid
WHERE c.codegroup = IFNULL(q1.QMFE_OTGRP,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_inspectionpartcatalogtype i
SET fi.dim_inspectionpartcatalogtypeid = i.dim_inspectionpartcatalogtypeid
WHERE i.inspectionpartcatalogtypecode = IFNULL(Q1.QMFE_OTKAT,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_ItemOrderNumber =  IFNULL(q1.QMFE_FCOAUFNR,'Not Set')
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_EquipmentNumber =  IFNULL(q1.QMFE_EQUNR,'Not Set')
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_CostCenter cc
SET fi.dim_CostCenterId = cc.dim_CostCenterId
WHERE cc.code = IFNULL(Q1.QMFE_KOSTL,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.ct_ItemDefectiveQtyInt =  IFNULL(q1.QMFE_MENGE,0)
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_NumberOfDefects =  IFNULL(q1.QMFE_ANZFEHLER,0)
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_codetext ct
SET fi.Dim_objectpartid = ct.Dim_codetextid
WHERE ct.codegroup = IFNULL(q1.QMFE_OTGRP,'Not Set')
  AND ct.code = IFNULL(q1.QMFE_OTEIL,'Not Set')
  AND ct.version = IFNULL(q1.QMFE_OTVER,'Not Set')
  AND ct.catalog = IFNULL(q1.QMFE_OTKAT,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_codetext ctt
SET fi.dim_damagecodeid = ctt.dim_codetextid
WHERE ctt.codegroup = IFNULL(q1.QMFE_FEGRP,'Not Set')
	AND ctt.catalog = IFNULL(q1.QMFE_FEKAT,'Not Set')
    AND ctt.code = IFNULL(q1.QMFE_FECOD,'Not Set')
    AND ctt.version = IFNULL(q1.QMFE_FEVER,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

/*
UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_TimeOfChange =  IFNULL(q1.QMFE_AEZEIT,'Not Set')
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_FunctionalLocation =  IFNULL(q1.QMFE_TPLNR,'Not Set')
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM


*/

INSERT INTO fact_Qualitynotificationitem(amt_ExchangeRate,
                                         amt_ExchangeRate_GBL,
                                         ct_ExternalDefectiveQty,
                                         ct_InternalDefectiveQty,
                                         dd_CreatedBy,
										 dd_ChangedBy,
					 					 dim_DateIdCreatedOn,
					 					 dim_DateIdChangedOn,
                                         dd_ItemShortText,
                                         dd_NotificationItemNo,
                                         dd_NotificationNo,
					 					 dim_companyid,
                                         dim_defectreporttypeid,
                                         dim_partid,
                                         dim_plantid,
                                         dim_qualitynotificationmiscid, /*unused*/
                                         Dim_UnitOfMeasureid,
                                         dim_inspectioncatalogtypeid,
                                         dim_notificationitemmiscid,
					 dim_codetextid,
					 dim_inspectionpartcatalogtypeid,
					 dd_ItemOrderNumber,
					 dd_EquipmentNumber,
					 dim_CostCenterId,
					 ct_ItemDefectiveQtyInt,
					 dd_NumberOfDefects,
					 /*dd_TimeOfChange,
					 dd_FunctionalLocation,
					 */
					 dim_ObjectPartId,
					 dim_DamageCodeId,
					 fact_Qualitynotificationitemid)
   SELECT 
	  1 amt_ExchangeRate,	
	  1 amt_exchangerate_GBL, 
	  IFNULL(QMFE_FMGFRD,0),
	  IFNULL(QMFE_FMGEIG,0),
          IFNULL(QMFE_ERNAM, 'Not Set'),
	  IFNULL(QMFE_AENAM, 'Not Set'),
	1,
	1,
	  QMFE_FETXT,
          QMFE_FENUM,
          QMFE_QMNUM,
	  ifnull((SELECT dc.dim_Companyid FROM  dim_Company dc, dim_plant pl
	  WHERE pl.plantcode = QMFE_WERKS
	  AND pl.rowiscurrent = 1
	  AND pl.companycode = dc.companycode
	  AND dc.rowiscurrent = 1),1) dim_companyid,
	  1,
          IFNULL((SELECT p.dim_partid FROM dim_part p
                WHERE p.PartNumber = IFNULL(QMFE.QMFE_MATNR,'Not Set')
                 and p.rowiscurrent = 1 ),1),
           IFNULL((SELECT pl.dim_plantid FROM dim_plant pl
                WHERE pl.plantcode = qmfe_werks
		  AND pl.rowiscurrent = 1 ),1) dim_plantid,
          1, 
        IFNULL((SELECT u.dim_unitofmeasureid FROM dim_UnitOfMeasure u
                WHERE u.uom = IFNULL(QMFE.QMFE_FMGEIN,'Not Set')
                 and u.rowiscurrent = 1 ),1),
	IFNULL((SELECT d.Dim_InspectionCatalogTypeid FROM dim_inspectioncatalogtype d
                WHERE d.InspectionCatalogTypeCode = qmfe.QMFE_FEKAT
                 and d.rowiscurrent = 1 ),1),     
	IFNULL((SELECT dn.Dim_NotificationItemMiscId FROM dim_NotificationItemMisc dn
	WHERE dn.originaldefect = IFNULL(qmfe.QMFE_KZORG,'Not Set') 
             AND dn.repetitivedefect = IFNULL(qmfe.QMFE_WDFEH,'Not Set')),1),
	
	IFNULL((SELECT c.Dim_codetextid FROM dim_codetext c
                WHERE c.codegroup = IFNULL(QMFE.QMFE_OTGRP,'Not Set')
                 and c.rowiscurrent = 1 ),1),   
	IFNULL((SELECT i.dim_inspectionpartcatalogtypeid FROM dim_inspectionpartcatalogtype i
                WHERE i.inspectionpartcatalogtypecode = IFNULL(QMFE.QMFE_OTKAT,'Not Set')
                 and i.rowiscurrent = 1 ),1),
	IFNULL(QMFE_FCOAUFNR,'Not Set'),
	IFNULL(QMFE_EQUNR,'Not Set'),
	IFNULL((SELECT cc.dim_CostCenterId FROM dim_CostCenter cc
                WHERE cc.code = IFNULL(QMFE.QMFE_KOSTL,'Not Set')
                 and cc.rowiscurrent = 1 ),1),
	IFNULL(QMFE_MENGE,0),
	IFNULL(QMFE_ANZFEHLER,0),
	/*IFNULL(QMFE_AEZEIT,'Not Set'), 
	IFNULL(QMFE_TPLNR,'Not Set'),
	IFNULL(QMFE_FECOD,'Not Set'), */
	IFNULL((SELECT ct.dim_codetextId FROM dim_codetext ct
                WHERE ct.codegroup = IFNULL(QMFE.QMFE_OTGRP,'Not Set')
                 and ct.rowiscurrent = 1 ),1),
    IFNULL((SELECT ctt.dim_codetextId FROM dim_codetext ctt
                WHERE ctt.codegroup = IFNULL(QMFE.QMFE_FEGRP,'Not Set')
                 and ctt.rowiscurrent = 1 ),1),             
                 
        /* hash(concat(QMFE_QMNUM,QMFE_FENUM))*/
	(  (SELECT max_id
                FROM NUMBER_FOUNTAIN
               WHERE table_name = 'fact_Qualitynotificationitem')
           + row_number() OVER(ORDER BY QMFE_QMNUM,QMFE_FENUM))

 FROM tmp_far_variable_holder t, QMFE
WHERE  NOT EXISTS /*( SELECT 1 FROM fact_Qualitynotificationitem where hash(concat(qmfe_qmnum, qmfe_fenum)) = fact_Qualitynotificationitemid)*/
( SELECT 1 FROM fact_Qualitynotificationitem WHERE qmfe_qmnum = dd_notificationno
                    AND qmfe_fenum = dd_notificationitemno);
		    
UPDATE fact_Qualitynotificationitem fi
FROM tmp_getExchangeRate1 z,dim_company dc, tmp_far_variable_holder t
SET fi.amt_Exchangerate_GBL = z.exchangeRate
WHERE dc.dim_companyid = fi.dim_companyid
AND z.pFromCurrency  = dc.currency 
AND z.fact_script_name = 'bi_populate_qualitynotificationitem_fact' 
AND z.pToCurrency = t.pGlobalCurrency 
AND z.pFromExchangeRate = 0 
AND z.pDate = ANSIDATE(LOCAL_TIMESTAMP);		    
		    
UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_plant pl, dim_Company dc , dim_currency cur
SET fi.dim_currencyid = cur.dim_currencyid
WHERE pl.plantcode = q1.QMFE_WERKS
AND dc.CompanyCode = pl.CompanyCode
AND dc.currency = cur.currencycode
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
AND fi.dim_currencyid <> cur.dim_currencyid;

UPDATE fact_Qualitynotificationitem fi  
FROM QMFE q1, dim_plant pl, dim_Company dc , dim_currency c,tmp_far_variable_holder t
SET fi.dim_currencyid_GBL = c.dim_currencyid
WHERE pl.plantcode = q1.QMFE_WERKS
AND dc.CompanyCode = pl.CompanyCode
AND c.CurrencyCode = t.pGlobalCurrency
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
AND fi.dim_currencyid_GBL <> c.dim_currencyid;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_plant pl, dim_Company dc , dim_currency cur
SET fi.dim_currencyid_TRA = cur.dim_currencyid
WHERE pl.plantcode = q1.QMFE_WERKS
AND dc.CompanyCode = pl.CompanyCode
AND dc.currency = cur.currencycode
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
AND fi.dim_currencyid_TRA <> cur.dim_currencyid;

/* update dim_objectpartid and dim_damagecodeid*/

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_codetext ct
SET fi.Dim_objectpartid = ct.Dim_codetextid
WHERE ct.codegroup = IFNULL(q1.QMFE_OTGRP,'Not Set')
  AND ct.code = IFNULL(q1.QMFE_OTEIL,'Not Set')
  AND ct.version = IFNULL(q1.QMFE_OTVER,'Not Set')
  AND ct.catalog = IFNULL(q1.QMFE_OTKAT,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_codetext ctt
SET fi.dim_damagecodeid = ctt.dim_codetextid
WHERE ctt.codegroup = IFNULL(q1.QMFE_FEGRP,'Not Set')
	AND ctt.catalog = IFNULL(q1.QMFE_FEKAT,'Not Set')
    AND ctt.code = IFNULL(q1.QMFE_FECOD,'Not Set')
    AND ctt.version = IFNULL(q1.QMFE_FEVER,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_date dt, dim_Company dc, dim_plant pl
SET fi.Dim_DateIdCreatedOn = ifnull(dt.dim_dateid, 'Not Set')
WHERE dt.DateValue = q1.QMFE_ERDAT
AND pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = IFNULL(pl.CompanyCode,'Not Set')
AND dt.CompanyCode =IFNULL(dc.CompanyCode,'Not Set')             
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1, dim_date dt, dim_Company dc, dim_plant pl
SET fi.Dim_DateIdChangedOn = ifnull(dt.dim_dateid, 'Not Set')
WHERE dt.DateValue = q1.QMFE_AEDAT
AND pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = IFNULL(pl.CompanyCode,'Not Set')
AND dt.CompanyCode =IFNULL(dc.CompanyCode,'Not Set')             
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

/*  New Fields 31 Jan 2014 */
UPDATE fact_Qualitynotificationitem fi
FROM fact_Qualitynotification fq
SET fi.dd_documentno =  IFNULL(fq.dd_documentno,'Not Set'),
    fi.dd_documentitemno =  IFNULL(fq.dd_documentitemno,0),
	fi.dim_vendorid = IFNULL(fq.dim_vendorid,1),
	fi.Dim_dateidrecordcreated = IFNULL(fq.Dim_dateidrecordcreated,1),
	fi.dim_dateidnotificationcompletion = ifnull(fq.dim_dateidnotificationcompletion,1),
	fi.dd_VendorMaterialNo = ifnull(fq.dd_VendorMaterialNo,'Not Set'),
	fi.dim_notificationtypeid = ifnull(fq.dim_notificationtypeid,1)
WHERE fi.dd_NotificationNo = fq.dd_notificationo;

UPDATE fact_Qualitynotificationitem fi
FROM QMUR QM, Dim_codetext dc
SET fi.Dim_codetextCausesid = dc.Dim_codetextid
WHERE QM.QMUR_QMNUM = fi.dd_NotificationNo
  AND QM.QMUR_FENUM = fi.dd_NotificationItemNo
  AND QM.QMUR_URKAT = dc.Catalog
  AND QM.QMUR_URGRP = dc.codegroup
  AND QM.QMUR_URCOD = dc.code;
  
UPDATE fact_Qualitynotificationitem fi
FROM QMUR QM
SET fi.dd_DefectNumber = ifnull(QM.QMUR_URNUM,0)
WHERE QM.QMUR_QMNUM = fi.dd_NotificationNo
  AND QM.QMUR_FENUM = fi.dd_NotificationItemNo;
  
UPDATE fact_Qualitynotificationitem fi
FROM QMSM QM, dim_company dc
SET fi.dim_dateidqntaskcreated =  ifnull((SELECT dim_dateid FROM dim_date dnc
                      				WHERE dnc.DateValue = QM.QMSM_ERDAT AND dnc.CompanyCode = dc.CompanyCode
							        AND QMSM_ERDAT IS NOT NULL),1)
WHERE fi.dd_NotificationNo = QM.QMSM_QMNUM
AND fi.dd_NotificationItemNo = QM.QMSM_FENUM; 

UPDATE fact_Qualitynotificationitem fi
FROM QMSM QM, dim_company dc
SET fi.dim_dateidqntaskcompleted =  ifnull((SELECT dim_dateid FROM dim_date dnc
                      				WHERE dnc.DateValue = QM.QMSM_ERLDAT AND dnc.CompanyCode = dc.CompanyCode
							        AND QMSM_ERLDAT IS NOT NULL),1)
WHERE fi.dd_NotificationNo = QM.QMSM_QMNUM
AND fi.dd_NotificationItemNo = QM.QMSM_FENUM;

update fact_Qualitynotificationitem set dd_documentno = 'Not Set' where dd_documentno is NULL;
update fact_Qualitynotificationitem set dd_documentitemno = 0 where dd_documentitemno is NULL;
update fact_Qualitynotificationitem set dim_vendorid = 1 where dim_vendorid is NULL;
update fact_Qualitynotificationitem set Dim_dateidrecordcreated = 1 where Dim_dateidrecordcreated is NULL;
update fact_Qualitynotificationitem set dim_dateidnotificationcompletion = 1 where dim_dateidnotificationcompletion is NULL;
update fact_Qualitynotificationitem set dd_VendorMaterialNo = 'Not Set' where dd_VendorMaterialNo is NULL;
update fact_Qualitynotificationitem set dd_DefectNumber = 0 where dd_DefectNumber is NULL;
update fact_Qualitynotificationitem set dim_notificationtypeid = 1 where dim_notificationtypeid is NULL;
update fact_Qualitynotificationitem set Dim_codetextCausesid = 1 where Dim_codetextCausesid is NULL;
update fact_Qualitynotificationitem set dim_dateidqntaskcreated = 1 where dim_dateidqntaskcreated is NULL;
update fact_Qualitynotificationitem set dim_dateidqntaskcompleted = 1 where dim_dateidqntaskcompleted is NULL;