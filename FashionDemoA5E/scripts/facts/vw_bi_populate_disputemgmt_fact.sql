/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 17 May 2013 */
/*   Description    : Stored Proc bi_populate_disputemgmt_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   25 Feb 2014      Nicoleta  1.3 		  Added dim_CaseCategoriesId								      */	
/*   5 Nov 2013       George I. 1.2		  Added new fields												  */
/*   18 Sep 2013      Lokesh    1.1               Currency and exchange rate changes							  */
/*   17 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/*   01 Aug 2014      Alexandra    1.0            Exclude FDM_DCPROC table                            */
/******************************************************************************************************************/

DELETE FROM fact_disputemanagement;

delete from NUMBER_FOUNTAIN where table_name = 'fact_disputemanagement';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_disputemanagement',ifnull(max(fact_disputemanagementid),0)
FROM fact_disputemanagement;

DROP TABLE IF EXISTS tmp_FDM_DCPROC_fact_disputemanagement;
CREATE TABLE tmp_FDM_DCPROC_fact_disputemanagement
AS
SELECT *,left(FDM_DCPROC_TIMESTAMP,4) + '-' + left(shift(FDM_DCPROC_TIMESTAMP,-4),2) + '-' + left(shift(FDM_DCPROC_TIMESTAMP,-6),2) as fdm_datevalue
FROM FDM_DCPROC;

update tmp_FDM_DCPROC_fact_disputemanagement
set fdm_datevalue = '1900-01-01' where fdm_datevalue like '%-00-%';

DROP TABLE IF EXISTS tmp_SCMG_T_CASE_ATTR_fact_disputemanagement;
CREATE TABLE tmp_SCMG_T_CASE_ATTR_fact_disputemanagement
AS
SELECT *,left(CREATE_TIME,4) + '-' + left(shift(CREATE_TIME,-4),2) + '-' + left(shift(CREATE_TIME,-6),2) as CREATE_TIME_datevalue,
	left(CHANGE_TIME,4) + '-' + left(shift(CHANGE_TIME,-4),2) + '-' + left(shift(CHANGE_TIME,-6),2) as CHANGE_TIME_datevalue,
	left(CLOSING_TIME,4) + '-' + left(shift(CLOSING_TIME,-4),2) + '-' + left(shift(CLOSING_TIME,-6),2) as CLOSING_TIME_datevalue
FROM SCMG_T_CASE_ATTR;


update tmp_SCMG_T_CASE_ATTR_fact_disputemanagement
set closing_time_datevalue = '1900-01-01' where closing_time_datevalue like '%-00-%';
update tmp_SCMG_T_CASE_ATTR_fact_disputemanagement
set CHANGE_TIME_datevalue = '1900-01-01' where CHANGE_TIME_datevalue like '%00%';
update tmp_SCMG_T_CASE_ATTR_fact_disputemanagement
set CREATE_TIME_datevalue = '1900-01-01' where CREATE_TIME_datevalue like '%00%';

drop table if exists fact_disputemanagement_ot10;
create table fact_disputemanagement_ot10 as select a1.*,
varchar(null,20) CompanyCode_upd,
amt_DisputeAmtPaid amt_DisputeAmtPaid_upd,
amt_DisputeAmtCredited amt_DisputeAmtCredited_upd,
amt_DisputeAmtCleared amt_DisputeAmtCleared_upd,
amt_DisputeAmtWrittenOff amt_DisputeAmtWrittenOff_upd,
CREATE_TIME_datevalue CREATE_TIME_datevalue_upd,
CHANGE_TIME_datevalue CHANGE_TIME_datevalue_upd,
CLOSING_TIME_datevalue CLOSING_TIME_datevalue_upd,
PLAN_END_DATE PLAN_END_DATE_upd,
CREATE_TIME CREATE_TIME_upd,
CHANGE_TIME CHANGE_TIME_upd,
CLOSING_TIME CLOSING_TIME_upd,
UDMCASEATTR00_FIN_DUE_DATE FIN_DUE_DATE_upd

from fact_disputemanagement a1,
      tmp_FDM_DCPROC_fact_disputemanagement  a2,
      tmp_SCMG_T_CASE_ATTR_fact_disputemanagement  a3,
	  UDMCASEATTR00 a4
where 1=2;

  INSERT INTO fact_disputemanagement_ot10 (Dim_CompanyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    dd_ClearingDocumentNo,
                                    dd_AssignmentNumber,
                                    dd_FiscalYear,
                                    dd_AccountingDocNo,
                                    dd_AccountingDocItemNo,
                                    Dim_DateIdPosting,
                                    Dim_DateIdCreated,
                                    Dim_BusinessAreaId,
                                    dd_debitcreditid,
                                    Dim_CreditControlAreaId,
                                    Dim_DocumentTypeId,
                                    dd_FiscalPeriod,
                                    Dim_ChartOfAccountsId,
                                    Dim_CurrencyId,
									Dim_CurrencyId_TRA,
									Dim_CurrencyId_GBL,									
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    dd_InvoiceNumberTransBelongTo,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_ClearedFlagId,
                                    amt_ExchangeRate_GBL,
                                    amt_ExchangeRate,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    Dim_NetDueDateId,
                                    dd_BillingNo,
                                    dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    dd_RelevantInvoiceFiscalYear,
                                    Dim_PartId,
                                    Dim_PlantId,
                                    Dim_PaymentTermsId,
                                    ct_CashDiscountDays1,
                                    dd_CustomerPONumber,
                                    dd_CreditRep,
                                    dd_CreditLimit,
                                    Dim_RiskClassId,
                                    dd_DisputeCaseID,
                                    amt_OriginalAmtDisputed,
                                    amt_DisputedAmt,
                                    amt_DisputeAmtPaid,
                                    amt_DisputeAmtCredited,
                                    amt_DisputeAmtCleared,
                                    amt_DisputeAmtWrittenOff,
                                    Dim_SalesOrgid,
                                    Dim_DateidAmountPaid,
                                    Dim_DateidAmountCredited,
                                    Dim_DateidAmountCleared,
                                    Dim_DateidAmtWrittenOff,
                                    dd_DisputeStatus,
                                    Dim_DateidDisputeCaseCreated,
                                    Dim_DateidDisputeCaseLastChanged,
                                    Dim_DateidDisputeClosed,
                                    Dim_DateidDisputePlannedClosed,
                                    dd_Processor,
                                    dd_PersonResponsible,
                                    dd_DisputeCaseTitle,
                                    dd_DisputeEscalationReason,
                                    dd_DisputeCategory,
                                    dd_DisputePriority,
				    fact_disputemanagementid,
				    dd_DeductionAnalyst,
				    dd_ExternalReference,
				    Dim_SpecialGLIndicatorId,
				    dim_DisputeCaseStatusId,
				    dd_RootCauseCode,
				    dd_ReasonForCase,
				    dd_CoordDisputeCase,
				    dd_CreatedBy,
CompanyCode_upd,
amt_DisputeAmtPaid_upd,
amt_DisputeAmtCredited_upd,
amt_DisputeAmtCleared_upd,
amt_DisputeAmtWrittenOff_upd,
CREATE_TIME_datevalue_upd,
CHANGE_TIME_datevalue_upd,
CLOSING_TIME_datevalue_upd,
PLAN_END_DATE_upd,
CREATE_TIME_upd,
CHANGE_TIME_upd,
CLOSING_TIME_upd,
dim_CaseTypeid,
dim_ProcessingDeadlineDateId,
FIN_DUE_DATE_upd,
dd_DeductionAnalystName,
dim_InvoiceDateid,
dim_escalationreasonid,
dim_rootcausecodeid,
dim_casereasonsid,
dd_ClearingDocument,
amt_clearedmanually,
dim_casecategoriesid)
									
  SELECT f.Dim_CompanyId,
          f.Dim_CustomerId,
          f.Dim_DateIdClearing,
          f.dd_ClearingDocumentNo,
          f.dd_AssignmentNumber,
          f.dd_FiscalYear,
          f.dd_AccountingDocNo,
          f.dd_AccountingDocItemNo,
          f.Dim_DateIdPosting,
          f.Dim_DateIdCreated,
          f.Dim_BusinessAreaId,
          f.dd_debitcreditid,
          f.Dim_CreditControlAreaId,
          f.Dim_DocumentTypeId,
          f.dd_FiscalPeriod,
          f.Dim_ChartOfAccountsId,
          f.Dim_CurrencyId,
		  f.Dim_CurrencyId_TRA,
		  f.Dim_CurrencyId_GBL,
          f.Dim_PaymentReasonId,
          f.Dim_PostingKeyId,
          f.dd_InvoiceNumberTransBelongTo,
          f.Dim_DateIdBaseDateForDueDateCalc,
          f.Dim_DateIdAccDocDateEntered,
          f.Dim_ClearedFlagId,
          f.amt_ExchangeRate_GBL,
          f.amt_ExchangeRate,
          f.dd_SalesDocNo,
          f.dd_SalesItemNo,
          f.dd_SalesScheduleNo,
          f.Dim_NetDueDateId,
          f.dd_BillingNo,
          f.dd_ProductionOrderNo,
          f.Dim_SpecialGlTransactionTypeId,
          f.Dim_AccountsReceivableDocStatusId,
          f.dd_RelevantInvoiceFiscalYear,
          f.Dim_PartId,
          f.Dim_PlantId,
          f.Dim_PaymentTermsId,
          f.ct_CashDiscountDays1,
          f.dd_CustomerPONumber,
          f.dd_CreditRep,
          f.dd_CreditLimit,
          f.Dim_RiskClassId,
          f.dd_DisputeCaseID,
          e.UDMCASEATTR00_FIN_ORIGINAL_AMT amt_OriginalAmtDisputed,
          0 amt_DisputedAmt,
          0 amt_DisputeAmtPaid,
          z.ZDISPCASE_CREDIT_AMT amt_DisputeAmtCredited,
          e.UDMCASEATTR00_FIN_WRT_OFF_AMT amt_DisputeAmtCleared,
          e.UDMCASEATTR00_FIN_NOT_SOLV_AMT amt_DisputeAmtWrittenOff,
          Dim_SalesOrgid,
		  1 Dim_DateidAmountPaid,
          1 Dim_DateidAmountCredited,
          1 Dim_DateidAmountCleared,
          1 Dim_DateidAmtWrittenOff,
          a.STAT_ORDERNO dd_DisputeStatus,
          1 Dim_DateidDisputeCaseCreated,
          1 Dim_DateidDisputeCaseLastChanged,
          1 Dim_DateidDisputeClosed,
          1 Dim_DateidDisputePlannedClosed,
	      ifnull(a.PROCESSOR,'Not Set') dd_Processor,
          ifnull(a.RESPONSIBLE,'Not Set') dd_PersonResponsible,
          ifnull(a.CASE_TITLE,'Not Set') dd_DisputeCaseTitle,
          ifnull(a.ESCAL_REASON,'Not Set') dd_DisputeEscalationReason,
          ifnull(a.CATEGORY,'Not Set') dd_DisputeCategory,
          ifnull(a.PRIORITY,0) dd_DisputePriority,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_disputemanagement') + row_number() over(),
		  'Not Set' dd_DeductionAnalyst,
	      ifnull(a.EXT_REF,'Not Set') dd_ExternalReference,	  
   	      ifnull(f.Dim_SpecialGLIndicatorId,1),
          ifnull(d.dim_DisputeCaseStatusId,1),
	      ifnull(e.UDMCASEATTR00_FIN_ROOT_CCODE,'Not Set') dd_RootCauseCode,
	      ifnull(a.REASON_CODE,'Not Set') dd_ReasonForCase,
	      ifnull(e.UDMCASEATTR00_FIN_COORDINATOR,'Not Set') dd_CoordDisputeCase,
  	      ifnull(a.CREATED_BY,'Not Set') dd_CreatedBy,
	      dcm.CompanyCode CompanyCode_upd,
	   	  amt_DisputeAmtPaid amt_DisputeAmtPaid_upd,
		  amt_DisputeAmtCredited amt_DisputeAmtCredited_upd,
		  amt_DisputeAmtCleared amt_DisputeAmtCleared_upd,
		  amt_DisputeAmtWrittenOff amt_DisputeAmtWrittenOff_upd,
		  CREATE_TIME_datevalue CREATE_TIME_datevalue_upd,
		  CHANGE_TIME_datevalue CHANGE_TIME_datevalue_upd,
		  CLOSING_TIME_datevalue CLOSING_TIME_datevalue_upd,
		  PLAN_END_DATE PLAN_END_DATE_upd,
		  CREATE_TIME CREATE_TIME_upd,
		  CHANGE_TIME CHANGE_TIME_upd,
		  CLOSING_TIME CLOSING_TIME_upd,
		  1 dim_CaseTypeid,
		  1 dim_ProcessingDeadlineDateId,
		  e.UDMCASEATTR00_FIN_DUE_DATE FIN_DUE_DATE_upd,
		  'Not Set'  dd_DeductionAnalystName,
		  1 dim_InvoiceDateid,
		  1 dim_escalationreasonid,
		  1 dim_rootcausecodeid,
		  1 dim_casereasonsid,
		  'Not Set' dd_ClearingDocument,
		  e.UDMCASEATTR00_FIN_WRT_OFF_AMT,
		  1 dim_casecategoriesid
		
  FROM fact_accountsreceivable f
      INNER JOIN dim_company dcm ON dcm.Dim_CompanyId = f.Dim_CompanyId
      inner join tmp_SCMG_T_CASE_ATTR_fact_disputemanagement a ON EXT_KEY = f.dd_DisputeCaseId 
	  INNER JOIN FDM_DCOBJ c on c.FDM_DCOBJ_CASE_GUID_LOC = a.CASE_GUID
			AND f.dd_AccountingDocNo =  substr(c.FDM_DCOBJ_OBJ_KEY,5,10)
			AND f.dd_AccountingDocItemNo = substr(c.FDM_DCOBJ_OBJ_KEY,19,3)
	  INNER JOIN dim_DisputeCaseStatus d on d.DisputeCaseStatus = a.STAT_ORDERNO AND d.DisputeCaseProfileId = a.PROFILE_ID
	  INNER JOIN UDMCASEATTR00 e on varchar(e.UDMCASEATTR00_CASE_GUID,32) = varchar(a.CASE_GUID,32)
      INNER JOIN dim_customer cm on cm.Dim_CustomerId = f.Dim_CustomerId
      INNER JOIN ZDISPCASE z on e.UDMCASEATTR00_CASE_GUID = z.ZDISPCASE_CASE_GUID;
 
Update fact_disputemanagement_ot10
Set Dim_DateidDisputeCaseCreated = ifnull((select dt.Dim_Dateid from dim_date dt
                 where dt.DateValue = CREATE_TIME_datevalue_upd
                 and dt.CompanyCode = CompanyCode_upd and CREATE_TIME_upd > 0),1) ;

Update fact_disputemanagement_ot10
Set Dim_DateidDisputeCaseLastChanged = ifnull((select dt.Dim_Dateid from dim_date dt
                 where (dt.DateValue) = (CHANGE_TIME_datevalue_upd)
                 and dt.CompanyCode = CompanyCode_upd and CHANGE_TIME_upd > 0),1);

Update fact_disputemanagement_ot10
Set Dim_DateidDisputeClosed = ifnull((select dt.Dim_Dateid from dim_date dt
                 where dt.DateValue = CLOSING_TIME_datevalue_upd
                 and dt.CompanyCode = CompanyCode_upd and CLOSING_TIME_upd > 0),1);


Update fact_disputemanagement_ot10
Set Dim_DateidDisputePlannedClosed = ifnull((select dt.Dim_Dateid from dim_date dt
                 where dt.DateValue = PLAN_END_DATE_upd
                 and dt.CompanyCode = CompanyCode_upd ),1) ;

Update fact_disputemanagement_ot10
Set dim_ProcessingDeadlineDateId = ifnull((select dt.Dim_Dateid from dim_date dt
                 where dt.DateValue = FIN_DUE_DATE_upd
                 and dt.CompanyCode = CompanyCode_upd ),1) ;


  INSERT INTO fact_disputemanagement (Dim_CompanyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    dd_ClearingDocumentNo,
                                    dd_AssignmentNumber,
                                    dd_FiscalYear,
                                    dd_AccountingDocNo,
                                    dd_AccountingDocItemNo,
                                    Dim_DateIdPosting,
                                    Dim_DateIdCreated,
                                    Dim_BusinessAreaId,
                                    dd_debitcreditid,
                                    Dim_CreditControlAreaId,
                                    Dim_DocumentTypeId,
                                    dd_FiscalPeriod,
                                    Dim_ChartOfAccountsId,
                                    Dim_CurrencyId,
                                    Dim_CurrencyId_TRA,
                                    Dim_CurrencyId_GBL,
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    dd_InvoiceNumberTransBelongTo,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_ClearedFlagId,
                                    amt_ExchangeRate_GBL,
                                    amt_ExchangeRate,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    Dim_NetDueDateId,
                                    dd_BillingNo,
				    				dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    dd_RelevantInvoiceFiscalYear,
                                    Dim_PartId,
                                    Dim_PlantId,
                                    Dim_PaymentTermsId,
                                    ct_CashDiscountDays1,
                                    dd_CustomerPONumber,
                                    dd_CreditRep,
                                    dd_CreditLimit,
                                    Dim_RiskClassId,
                                    dd_DisputeCaseID,
                                    amt_OriginalAmtDisputed,
                                    amt_DisputedAmt,
                                    amt_DisputeAmtPaid,
                                    amt_DisputeAmtCredited,
                                    amt_DisputeAmtCleared,
                                    amt_DisputeAmtWrittenOff,
                                    Dim_SalesOrgid,
                                    Dim_DateidAmountPaid,
                                    Dim_DateidAmountCredited,
                                    Dim_DateidAmountCleared,
                                    Dim_DateidAmtWrittenOff,
                                    dd_DisputeStatus,
                                    Dim_DateidDisputeCaseCreated,
                                    Dim_DateidDisputeCaseLastChanged,
                                    Dim_DateidDisputeClosed,
                                    Dim_DateidDisputePlannedClosed,
                                    dd_Processor,
                                    dd_PersonResponsible,
                                    dd_DisputeCaseTitle,
                                    dd_DisputeEscalationReason,
                                    dd_DisputeCategory,
                                    dd_DisputePriority,
                                    fact_disputemanagementid,
                                    dd_DeductionAnalyst,
                                    dd_ExternalReference,
                                    Dim_SpecialGLIndicatorId,
                                    dim_DisputeCaseStatusId,
				    				dd_RootCauseCode,
                                    dd_ReasonForCase,
                                    dd_CoordDisputeCase,
                                    dd_CreatedBy,
									dim_CaseTypeid,
									dim_ProcessingDeadlineDateId,
									dd_DeductionAnalystName,
									dim_InvoiceDateid,
									dim_escalationreasonid,
									dim_rootcausecodeid,
									dim_casereasonsid,
									dd_ClearingDocument,
									amt_clearedmanually)
Select Dim_CompanyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    dd_ClearingDocumentNo,
                                    dd_AssignmentNumber,
                                    dd_FiscalYear,
                                    dd_AccountingDocNo,
                                    dd_AccountingDocItemNo,
                                    Dim_DateIdPosting,
                                    Dim_DateIdCreated,
                                    Dim_BusinessAreaId,
                                    dd_debitcreditid,
                                    Dim_CreditControlAreaId,
                                    Dim_DocumentTypeId,
                                    dd_FiscalPeriod,
                                    Dim_ChartOfAccountsId,
                                    Dim_CurrencyId,
                                    Dim_CurrencyId_TRA,
                                    Dim_CurrencyId_GBL,
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    dd_InvoiceNumberTransBelongTo,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_ClearedFlagId,
                                    amt_ExchangeRate_GBL,
                                    amt_ExchangeRate,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    Dim_NetDueDateId,
                                    dd_BillingNo,
				   					dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    dd_RelevantInvoiceFiscalYear,
                                    Dim_PartId,
                                    Dim_PlantId,
                                    Dim_PaymentTermsId,
                                    ct_CashDiscountDays1,
                                    dd_CustomerPONumber,
                                    dd_CreditRep,
                                    dd_CreditLimit,
                                    Dim_RiskClassId,
                                    dd_DisputeCaseID,
                                    amt_OriginalAmtDisputed,
                                    amt_DisputedAmt,
                                    amt_DisputeAmtPaid,
                                    amt_DisputeAmtCredited,
                                    amt_DisputeAmtCleared,
                                    amt_DisputeAmtWrittenOff,
                                    Dim_SalesOrgid,
                                    Dim_DateidAmountPaid,
                                    Dim_DateidAmountCredited,
                                    Dim_DateidAmountCleared,
                                    Dim_DateidAmtWrittenOff,
                                    dd_DisputeStatus,
                                    Dim_DateidDisputeCaseCreated,
                                    Dim_DateidDisputeCaseLastChanged,
                                    Dim_DateidDisputeClosed,
                                    Dim_DateidDisputePlannedClosed,
                                    dd_Processor,
                                    dd_PersonResponsible,
                                    dd_DisputeCaseTitle,
                                    dd_DisputeEscalationReason,
                                    dd_DisputeCategory,
                                    dd_DisputePriority,
                                    fact_disputemanagementid,
                                    dd_DeductionAnalyst,
                                    dd_ExternalReference,
                                    Dim_SpecialGLIndicatorId,
                                    dim_DisputeCaseStatusId,
				    				dd_RootCauseCode,
                                    dd_ReasonForCase,
                                    dd_CoordDisputeCase,
                                    dd_CreatedBy,
									dim_CaseTypeid,
									dim_ProcessingDeadlineDateId,
									dd_DeductionAnalystName,
									dim_InvoiceDateid,
									dim_escalationreasonid,
									dim_rootcausecodeid,
									dim_casereasonsid,
									dd_ClearingDocument,
									amt_clearedmanually
from fact_disputemanagement_ot10;
                                    
drop table if exists fact_disputemanagement_ot10;


UPDATE NUMBER_FOUNTAIN
SET
  max_id = (SELECT ifnull(max(fact_disputemanagementid), 0)
            FROM
              fact_disputemanagement)
WHERE
  table_name = 'fact_disputemanagement';	  

DROP TABLE IF EXISTS tmp_dd_factdisputemanagement;
Create table tmp_dd_factdisputemanagement as 
Select d.BUT050_PARTNER1,d.BUT050_PARTNER2 updVBAK_KUNNR
from cvi_cust_link c inner join but000 b1 on c.PARTNER_GUID = b1.PARTNER_GUID
	inner join BUT050 d on b1.PARTNER = d.BUT050_PARTNER2 AND d.BUT050_RELTYP = 'BUR013';

update fact_disputemanagement f
from   tmp_dd_factdisputemanagement t,
       Dim_Customer c
Set dd_DeductionAnalyst = t.BUT050_PARTNER1
Where f.dim_Customerid = c.dim_customerid
  and t.updVBAK_KUNNR = c.CustomerNumber;


drop table if exists FDM_DCOBJ_sub01;
create table FDM_DCOBJ_sub01 as 
select *,substr(FDM_DCOBJ_OBJ_KEY,5,10) FDM_DCOBJ_OBJ_KEY_str1 ,substr(FDM_DCOBJ_OBJ_KEY,19,3) FDM_DCOBJ_OBJ_KEY_str2 
from FDM_DCOBJ;

DROP TABLE IF EXISTS tmp_FDM_DCOBJ_fact_disputemanagement;
CREATE TABLE tmp_FDM_DCOBJ_fact_disputemanagement
AS
SELECT f.dd_DisputeCaseId,f.dd_AccountingDocNo,f.dd_AccountingDocItemNo,f.Dim_ClearedFlagId,sum(f.amt_InDocCurrency) as InDocCurrency_Amt,
	   c.FDM_DCOBJ_CLASSIFICATION
FROM FDM_DCOBJ_sub01 c,  fact_accountsreceivable f, tmp_SCMG_T_CASE_ATTR_fact_disputemanagement b
	    WHERE b.CASE_GUID = c.FDM_DCOBJ_CASE_GUID_LOC  
	  AND f.dd_disputecaseid = b.EXT_KEY
      AND f.dd_AccountingDocNo =  FDM_DCOBJ_OBJ_KEY_str1
      AND f.dd_AccountingDocItemNo = FDM_DCOBJ_OBJ_KEY_str2
	  AND c.FDM_DCOBJ_RELATION = 'F1'
	  AND c.FDM_DCOBJ_IS_VOIDED is null
      group by f.dd_DisputeCaseId,f.dd_AccountingDocNo,f.dd_AccountingDocItemNo,f.Dim_ClearedFlagId,c.FDM_DCOBJ_CLASSIFICATION; 

drop table if exists FDM_DCOBJ_sub01;

UPDATE fact_disputemanagement f
 FROM tmp_FDM_DCOBJ_fact_disputemanagement t
 SET f.dd_OriginalInvoiceNo = ifnull(t.dd_AccountingDocNo,'Not Set')
WHERE f.dd_DisputeCaseId = t.dd_DisputeCaseId
      AND f.dd_AccountingDocNo =  t.dd_AccountingDocNo
      AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
      AND t.FDM_DCOBJ_CLASSIFICATION = 'DISP_INV';

UPDATE fact_disputemanagement f
 FROM tmp_FDM_DCOBJ_fact_disputemanagement t
 SET f.dd_OriginalInvoiceNo = ifnull(f.dd_InvoiceNumberTransBelongTo,'Not Set')
WHERE f.dd_DisputeCaseId = t.dd_DisputeCaseId
      AND f.dd_AccountingDocNo =  t.dd_AccountingDocNo
      AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
      AND t.FDM_DCOBJ_CLASSIFICATION = 'DISP_RES';
	  
UPDATE fact_disputemanagement f
 FROM tmp_FDM_DCOBJ_fact_disputemanagement t
 SET f.amt_OriginalInvoiceAmount = ifnull(t.InDocCurrency_Amt,0)
WHERE f.dd_DisputeCaseId = t.dd_DisputeCaseId
      AND f.dd_AccountingDocNo =  t.dd_AccountingDocNo
      AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
      AND t.Dim_ClearedFlagId IN (3,5);
      
UPDATE fact_disputemanagement f
 FROM tmp_FDM_DCOBJ_fact_disputemanagement t
 SET f.amt_ResidualAmount = ifnull(t.InDocCurrency_Amt,0)
WHERE f.dd_DisputeCaseId = t.dd_DisputeCaseId
      AND f.dd_AccountingDocNo =  t.dd_AccountingDocNo
      AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
      AND t.Dim_ClearedFlagId IN (2,4);

UPDATE fact_disputemanagement set amt_originalinvoiceamount = 0 where amt_originalinvoiceamount IS NULL;
UPDATE fact_disputemanagement set amt_residualamount = 0 where amt_residualamount IS NULL;
UPDATE fact_disputemanagement set dd_originalinvoiceno = 'Not Set' where dd_originalinvoiceno IS NULL;

UPDATE fact_disputemanagement f
 FROM tmp_SCMG_T_CASE_ATTR_fact_disputemanagement a,
      dim_CaseType t
 SET f.dim_CaseTypeid = ifnull(t.dim_CaseTypeid,1)
WHERE a.EXT_KEY = f.dd_DisputeCaseId
      AND t.CASETYPE=a.CASE_TYPE;
UPDATE fact_disputemanagement set dim_CaseTypeid = 1 where dim_CaseTypeid IS NULL;	  

DROP TABLE IF EXISTS tmp_dd_factdisputemanagement2;
Create table tmp_dd_factdisputemanagement2 as 
Select b.BUT050_PARTNER1,b.BUT050_PARTNER2 updVBAK_KUNNR, z.NAME_FIRST, z.NAME_LAST
from CVI_CUST_LINK C, BUT000 y, BUT050 b, BUT000 z
where z.PARTNER = B.BUT050_PARTNER1
	and b.BUT050_PARTNER2 = y.PARTNER
	and y.PARTNER_GUID = c.PARTNER_GUID;

update fact_disputemanagement f
from   tmp_dd_factdisputemanagement2 t,
       Dim_Customer c
Set dd_DeductionAnalystName = ifnull(ifnull(t.NAME_FIRST,'')+' '+ifnull(t.NAME_LAST,''),'Not Set')
Where f.dim_Customerid = c.dim_customerid
  and t.updVBAK_KUNNR = c.CustomerNumber;
UPDATE fact_disputemanagement set dd_DeductionAnalystName = 'Not Set' where dd_DeductionAnalystName IS NULL;  
	  	 
UPDATE fact_disputemanagement f
 FROM BKPF a, dim_company dcm
 SET f.dim_InvoiceDateid = ifnull((select dt.Dim_Dateid from dim_date dt
                                   where dt.DateValue = a.BKPF_BUDAT
                                   and dt.CompanyCode = dcm.CompanyCode ),1)               
WHERE f.dd_OriginalInvoiceNo = a.BKPF_BELNR 
      AND dcm.CompanyCode =  a.BKPF_BUKRS
	  AND dcm.Dim_CompanyId = f.Dim_CompanyId;
UPDATE fact_disputemanagement set dim_InvoiceDateid = 1 where dim_InvoiceDateid IS NULL; 

UPDATE fact_disputemanagement f
 FROM tmp_SCMG_T_CASE_ATTR_fact_disputemanagement a,
      dim_escalationreason t
 SET f.dim_escalationreasonid = ifnull(t.dim_escalationreasonid,1)
WHERE a.EXT_KEY = f.dd_DisputeCaseId
      AND t.ESCALREASON=a.ESCAL_REASON;
UPDATE fact_disputemanagement set dim_escalationreasonid = 1 where dim_escalationreasonid IS NULL;

UPDATE fact_disputemanagement f
 FROM tmp_SCMG_T_CASE_ATTR_fact_disputemanagement a,
      dim_rootcausecode t
 SET f.dim_rootcausecodeid = ifnull(t.dim_rootcausecodeid,1)
WHERE a.EXT_KEY = f.dd_DisputeCaseId
      AND t.CASETYPE=a.CASE_TYPE;
UPDATE fact_disputemanagement set dim_rootcausecodeid = 1 where dim_rootcausecodeid IS NULL;

UPDATE fact_disputemanagement f
 FROM tmp_SCMG_T_CASE_ATTR_fact_disputemanagement a,
      dim_casereasons t
 SET f.dim_casereasonsid = ifnull(t.dim_casereasonsid,1)
WHERE a.EXT_KEY = f.dd_DisputeCaseId
      AND t.CASETYPE=a.CASE_TYPE
      and t.reasoncode = a.reason_code;
UPDATE fact_disputemanagement set dim_casereasonsid = 1 where dim_casereasonsid IS NULL;

UPDATE fact_disputemanagement f
 FROM tmp_FDM_DCOBJ_fact_disputemanagement t
 SET f.dd_ClearingDocument = ifnull(t.dd_AccountingDocItemNo,'Not Set')
WHERE f.dd_DisputeCaseId = t.dd_DisputeCaseId
      AND f.dd_AccountingDocNo =  t.dd_AccountingDocNo
      AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
      AND t.FDM_DCOBJ_CLASSIFICATION = 'DISP_RES';
UPDATE fact_disputemanagement set dd_ClearingDocument = 'Not Set' where dd_ClearingDocument IS NULL;
UPDATE fact_disputemanagement set dd_Offsetglaccount = 'Not Set' where dd_Offsetglaccount IS NULL;

UPDATE fact_disputemanagement f
FROM dim_CaseType t,
	dim_casecategories c
SET f.dim_casecategoriesid = ifnull(c.dim_casecategoriesid,1)
WHERE f.dim_CaseTypeid = t.dim_CaseTypeid
	and t.CASETYPE=c.casetype
	and f.dd_disputecategory = c.category
	and c.category <> 'Not Set';

UPDATE fact_disputemanagement set dim_casecategoriesid = 1 where dim_casecategoriesid IS NULL;
  
DROP TABLE IF EXISTS tmp_FDM_DCOBJ_fact_disputemanagement;
DROP TABLE IF EXISTS tmp_FDM_DCPROC_fact_disputemanagement;
DROP TABLE IF EXISTS tmp_SCMG_T_CASE_ATTR_fact_disputemanagement;
DROP TABLE IF EXISTS tmp_dd_factdisputemanagement;
DROP TABLE IF EXISTS tmp_dd_factdisputemanagement2;