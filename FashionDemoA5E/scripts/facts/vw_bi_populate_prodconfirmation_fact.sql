﻿set session authorization merckhumanhealth;


/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_prodconfirmation_fact.sql */
/*   Author         : Lokesh */
/*   Created On     : 111 May 2015 */
/* */
/* */
/*   Description    : Script for Production Operation  Confirmation */
/* */
/*   Change History */
/* ####################################################################################################################   */

Drop table if exists pGlobalCurrency_po_77;

Create table pGlobalCurrency_po_77(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_77(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_77
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

Drop table if exists fact_prodop_confirmation_tmp;
Drop table if exists max_holder_prop;

Create table fact_prodop_confirmation_tmp as
SELECT dd_ordernumber,
dd_operationnumber, dd_RoutingOperationNo,dd_GeneralOrderCounter, 
dd_confirmation, dd_confirmationcounter
FROM fact_prodop_confirmation ;

Create table max_holder_prop
as
Select ifnull(max(fact_prodop_confirmationid),0) maxid
from fact_prodop_confirmation;

/* Temporarily do this as we have full data. Once measures etc are tested, remove this and add not exist in insert */
DELETE FROM fact_prodop_confirmation;


/* PHFLG should not be 'X'. If it is 'X', then sumnr should be blank */
DROP TABLE IF EXISTS tmp_afvc_afvv;
CREATE TABLE tmp_afvc_afvv
AS
SELECT *
FROM AFVC_AFVV
WHERE (ifnull(afvc_phflg,'yy')) <> 'X';

INSERT INTO tmp_afvc_afvv
SELECT *
FROM afvc_afvv
WHERE ifnull(AFVC_PHFLG,'yy') = 'X' AND (rtrim(afvc_sumnr) = '' or afvc_sumnr is null);

DROP TABLE IF EXISTS tmp_afru;
CREATE TABLE tmp_afru
AS
SELECT *
FROM AFRU
WHERE AFRU_SATZA in ( 'B10','B30','B40');

INSERT INTO fact_prodop_confirmation(fact_prodop_confirmationid,fact_productionoperationid,
amt_Meteriel_PrimaryCost,
amt_ActivityCostTtl,
amt_Price,
amt_PriceUnit,
ct_ActivityDuration,
ct_ActualWork,
ct_ActivityWorkInvolved,
dim_businessareaid,
dim_companyid,
dim_controllingareaid,
dim_materialgroupid,
dim_currencyid,
dim_plantid,
dim_profitcenterid,
dim_purchasegroupid,
dim_purchaseorgid,
dim_tasklisttypeid,
dim_vendorid,
dim_costcenterid,
dd_PODocumentNo,
dim_functionalareaid,
dim_objecttypeid,
dim_unitofmeasureid,
dd_RoutingOperationNo,
dd_GeneralOrderCounter,
dd_POItemNo,
Dim_DateIdActualStartExec,
Dim_DateIdActualFinishExec,
dd_hourActualStartExec,
dd_hourActualFinishExe,
Dim_DateIdSchedStartExec,
Dim_DateIdSchedFinishExec,
dd_hourSchedStartExec,
dd_hourSchedFinishExec,
dd_RoutingRefSequence,
dd_OperationNumber,
dd_WorkCenter,
dd_bomexplosionno,
dim_productionorderstatusid,
dim_currencyid_TRA,
dim_currencyid_GBL,
dd_phaseindicator,
dd_supopnode,
dd_confirmation,
dd_confirmationcounter,
ct_operationquantity,
ct_basequantity,
ct_processingtime,
dim_uomprocessingtime,
ct_fixedruntime_theoretical,
dim_uomfixedruntime,
ct_variableruntime_theoretical,
dim_uomvariableruntime_theoretical,
dd_confirmation_recordtype,
dd_confirmationenteredby,
dd_lastchangeenteredby,
dd_confobjectid,
dim_plantidconfirmation,
dd_confirmationtext,
dd_conf_languagekey,
ct_confirmedbreaktime,
dd_activitytobeconfirmed,
dd_reversed,
dd_cancelconfirmation,
ct_confoperationquantity,
AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL) 
/*,
dd_confirmedstart_datetime,
dd_confirmedend_datetime,
dim_dateidconfirmationentry,
dim_dateidlastchange,
dim_dateidposting_confirmation,
dim_uombreaktime,
dd_activitytobeconfirmed,
dim_uom_activitytobeconfirmed,
dd_reversed,
dd_cancelconfirmation,
ct_confoperationquantity)*/
/*SELECT 1,1,*/
SELECT maxid + row_number() over(),1,
tp.*
From (SELECT DISTINCT
ifnull(AA.AFVC_MAT_PRKST, 0) amt_Meteriel_PrimaryCost,
ifnull(AA.AFVC_PRKST, 0) amt_ActivityCostTtl,
ifnull(AA.AFVC_PREIS, 0) amt_Price,
ifnull(AA.AFVC_PEINH, 0) amt_PriceUnit,
ifnull(AA.AFVV_DAUNO, 0) ct_ActivityDuration,
ifnull(AA.AFVV_ISMNW, 0) ct_ActualWork,
ifnull(AA.AFVV_ARBEI, 0) ct_ActivityWorkInvolved,
1	dim_businessareaid,
1	dim_companyid,
1	dim_controllingareaid,	
1	dim_materialgroupid,
1	dim_currencyid,	
1	dim_plantid,	
1	dim_profitcenterid,	
1	dim_purchasegroupid,	
1	dim_purchaseorgid,	
1	dim_tasklisttypeid,	
1	dim_vendorid,	
1	dim_costcenterid,	
ifnull(AA.AFVC_EBELN, 'Not Set') dd_PODocumentNo,
1	dim_functionalareaid,	
1	dim_objecttypeid,	
1	dim_unitofmeasureid,	
ifnull(AA.AFVC_AUFPL, 0) dd_RoutingOperationNo,
ifnull(AA.AFVC_APLZL, 0) dd_GeneralOrderCounter,
ifnull(AA.AFVC_EBELP, 0) dd_POItemNo,
1	Dim_DateIdActualStartExec,	
1	Dim_DateIdActualFinishExec,	
ifnull(AA.AFVV_ISDZ, 0) dd_hourActualStartExec,
ifnull(AA.AFVV_IEDZ, 0) dd_hourActualFinishExe,
1	Dim_DateIdSchedStartExec,	
1	Dim_DateIdSchedFinishExec,	
ifnull(AA.AFVV_FSAVZ, 0) dd_hourSchedStartExec,
ifnull(AA.AFVV_FSEDZ, 0) dd_hourSchedFinishExec,
ifnull(AA.AFVC_VPLFL, 'Not Set') dd_RoutingRefSequence,
AA.AFVC_VORNR dd_OperationNumber,
'Not Set'	dd_WorkCenter,
'Not Set'	dd_bomexplosionno,
1	dim_productionorderstatusid,
ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode = AFVC_WAERS),1) Dim_Currencyid_TRA,
ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode = 'USD'),1) dim_Currencyid_GBL,
IFNULL(AFVC_PHFLG,'Not Set') dd_phaseindicator,
ifnull(AFVC_SUMNR,0) dd_supopnode,
ifnull(AFRU_RUECK,0) dd_confirmation,
ifnull(AFRU_RMZHL,0) dd_confirmationcounter,
ifnull(AFVV_MGVRG,0) ct_operationquantity,
ifnull(AFVV_BMSCH,0) ct_basequantity,
ifnull(AFVV_BEARZ,0) ct_processingtime,
1 dim_uomprocessingtime,
0 ct_fixedruntime_theoretical,
1 dim_uomfixedruntime,
0 ct_variableruntime_theoretical,
1 dim_uomvariableruntime_theoretical,
ifnull(AFRU_SATZA,'Not Set') dd_confirmation_recordtype,
ifnull(AFRU_ERNAM,'Not Set') dd_confirmationenteredby,
ifnull(AFRU_AENAM,'Not Set') dd_lastchangeenteredby,
ifnull(AFRU_ARBID,0) dd_confobjectid,
1 dim_plantidconfirmation,
ifnull(AFRU_LTXA1,'Not Set') dd_confirmationtext,
IFNULL(AFRU_TXTSP,'Not Set') dd_conf_languagekey,
IFNULL(AFRU_ISERH,0) ct_confirmedbreaktime,
IFNULL(AFRU_ISM01,0) dd_activitytobeconfirmed,
IFNULL(AFRU_STOKZ,'Not Set') dd_reversed,
IFNULL(AFRU_STZHL,0) dd_cancelconfirmation,
IFNULL(AFRU_SMENG,0) ct_confoperationquantity,
ifnull(AFRU_AUFNR,'Not Set') /*production order*/,
ifnull(AFRU_AUFPL,0) /*operationtasklistno*/,
ifnull(AFRU_APLZL,0) /*counter*/
FROM  pGlobalCurrency_po_77,AFVC_AFVV AA left outer join tmp_AFRU on AA.AFVC_RUECK = AFRU_RUECK
) tp, max_holder_prop m;

UPDATE fact_prodop_confirmation f
FROM tmp_AFRU t, dim_plant pl
SET f.dim_plantidconfirmation = pl.dim_plantid
WHERE f.dd_confirmation = t.AFRU_RUECK
AND f.dd_confirmationcounter = t.AFRU_RMZHL
AND t.AFRU_WERKS = pl.plantcode;

DROP TABLE IF EXISTS tmp_afru_calculated_1;
CREATE TABLE tmp_afru_calculated_1
AS
SELECT DISTINCT
AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL,AFRU_SATZA,AFRU_ISBD,AFRU_ISBZ,AFRU_IEBD,AFRU_IEBZ,
AFRU_RUECK,AFRU_RMZHL,AFRU_ARBID,afru_ltxa1, row_number() over( PARTITION BY AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL,AFRU_RUECK order by cast(AFRU_RMZHL as int)) sr_no
FROM tmp_AFRU
WHERE AFRU_SATZA in ( 'B10','B30','B40');

UPDATE tmp_afru_calculated_1 a
FROM tmp_afru_calculated_1 b
SET a.AFRU_IEBD = b.AFRU_IEBD
WHERE a.AFRU_AUFNR = b.AFRU_AUFNR
AND a.AFRU_AUFPL = b.AFRU_AUFPL
AND a.AFRU_APLZL = B.AFRU_APLZL
AND a.AFRU_RUECK = b.AFRU_RUECK
AND a.AFRU_SATZA = 'B10'
AND a.sr_no = b.sr_no - 1;

UPDATE tmp_afru_calculated_1 a
FROM tmp_afru_calculated_1 b
SET a.AFRU_IEBZ = b.AFRU_IEBZ
WHERE a.AFRU_AUFNR = b.AFRU_AUFNR
AND a.AFRU_AUFPL = b.AFRU_AUFPL
AND a.AFRU_APLZL = b.AFRU_APLZL
AND a.AFRU_RUECK = b.AFRU_RUECK
AND a.AFRU_SATZA = 'B10'
AND a.sr_no = b.sr_no - 1;

DELETE FROM tmp_afru_calculated_1
WHERE AFRU_SATZA NOT IN ('B10') ;

DROP TABLE IF EXISTS tmp_afru_calculated;
CREATE TABLE tmp_afru_calculated
AS
SELECT AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL,AFRU_SATZA,AFRU_ISBD,AFRU_ISBZ,AFRU_IEBD,AFRU_IEBZ,
AFRU_RUECK,AFRU_RMZHL,AFRU_ARBID,afru_ltxa1, (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2))) date_hhmmss_beg,
cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)) date_hhmmss_end,
cast(0 as int) ts_diff_secs
FROM tmp_afru_calculated_1;

UPDATE tmp_afru_calculated
SET AFRU_ISBD = AFRU_ISBD + 1
WHERE AFRU_ISBZ = 240000;

UPDATE tmp_afru_calculated
SET AFRU_ISBZ = '000000'
WHERE AFRU_ISBZ = 240000;

UPDATE tmp_afru_calculated
SET afrU_IEBD = AFRU_IEBD + 1
where AFRU_IEBZ = 240000;

UPDATE tmp_afru_calculated
set AFRU_IEBZ = '000000'
WHERE AFRU_IEBZ = '240000';

UPDATE tmp_afru_calculated
SET date_hhmmss_beg = (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)))
where date_hhmmss_beg <> (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)));

UPDATE tmp_afru_calculated
SET date_hhmmss_end = cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2))
WHERE date_hhmmss_end <> cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2));

UPDATE tmp_afru_calculated
SET ts_diff_secs = TIMESTAMPDIFF(SECOND,date_hhmmss_beg,date_hhmmss_end)
WHERE date_hhmmss_beg is not null AND date_hhmmss_end is not null;

/*UPDATE fact_prodop_confirmation f
FROM tmp_afru_calculated t
SET f.ct_duration_seconds = t.ts_diff_secs
WHERE f.dd_confirmation = t.AFRU_RUECK
AND f.dd_confirmationcounter = t.AFRU_RMZHL*/

UPDATE fact_prodop_confirmation f
FROM tmp_afru_calculated t
SET f.ct_duration_seconds = t.ts_diff_secs
WHERE f.AFRU_AUFNR = t.AFRU_AUFNR
AND f.AFRU_AUFPL = t.AFRU_AUFPL
AND f.AFRU_APLZL = t.AFRU_APLZL
AND f.dd_confirmation = t.AFRU_RUECK
AND f.dd_confirmationcounter = t.AFRU_RMZHL;

UPDATE fact_prodop_confirmation f
FROM fact_prodop_confirmation f2
SET f.ct_duration_seconds = f2.ct_duration_seconds
WHERE f.dd_confirmation = f2.dd_confirmation
AND f.dd_confirmation_recordtype <> 'B10'
AND f2.dd_confirmation_recordtype = 'B10';
		
Drop table if exists fact_prodop_confirmation_tmp;
Drop table if exists max_holder_prop;

DELETE FROM fact_prodop_confirmation
WHERE EXISTS
          (SELECT 1
             FROM AFVC_AFVV
            WHERE     AFVC_AUFPL = dd_RoutingOperationNo
                  AND AFVC_APLZL = dd_GeneralOrderCounter
				  AND AUFK_LOEKZ = 'X');                
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET amt_Meteriel_PrimaryCost = ifnull(AFVC_MAT_PRKST ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_Meteriel_PrimaryCost <> ifnull(AA.AFVC_MAT_PRKST ,0);
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET amt_ActivityCostTtl = ifnull (AFVC_PRKST ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_ActivityCostTtl <> ifnull (AFVC_PRKST ,0);
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET amt_Price = ifnull(AFVC_PREIS ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_Price <> ifnull(AA.AFVC_PREIS ,0);
			
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET amt_PriceUnit = ifnull(AFVC_PEINH ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_PriceUnit <> ifnull(AA.AFVC_PEINH ,0);
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET ct_ActivityDuration = ifnull(AFVV_DAUNO ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActivityDuration <> ifnull(AA.AFVV_DAUNO ,0);
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET ct_ActualWork = ifnull(AFVV_ISMNW ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActualWork <> ifnull(AA.AFVV_ISMNW ,0);	
				
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET ct_ActivityWorkInvolved = ifnull(AFVV_ARBEI ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActivityWorkInvolved <> ifnull(AA.AFVV_ARBEI ,0);
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_businessarea bsar	
SET dim_businessareaid = bsar.dim_businessareaid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_GSBER = bsar.Businessarea
		AND pop.dim_businessareaid <> bsar.dim_businessareaid;
									
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_company comp	
SET dim_companyid = comp.dim_companyid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_BUKRS = comp.companycode
		AND pop.dim_companyid <> comp.dim_companyid;					
												
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_controllingarea ctrar	
SET dim_controllingareaid = ctrar.dim_controllingareaid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_ANFKOKRS = ctrar.controllingareacode
		AND pop.dim_controllingareaid <> ctrar.dim_controllingareaid;	
						
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_materialgroup matgp	
SET dim_materialgroupid = matgp.dim_materialgroupid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_MATKL = matgp.materialgroupcode
		AND pop.dim_materialgroupid <> matgp.dim_materialgroupid;

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_currency cur	
SET dim_currencyid = cur.dim_currencyid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_WAERS = cur.currencycode
		AND pop.dim_currencyid <> cur.dim_currencyid;

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_plant pla	
SET dim_plantid = pla.dim_plantid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_WERKS = pla.plantcode
		AND pop.dim_plantid <> pla.dim_plantid;
						
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_profitcenter proctr	
SET dim_profitcenterid = proctr.dim_profitcenterid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_PRCTR = proctr.profitcentercode
		AND pop.dim_profitcenterid <> proctr.dim_profitcenterid;
						
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_purchasegroup porgp	
SET dim_purchasegroupid = porgp.dim_purchasegroupid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_EKGRP = porgp.purchasegroup
		AND pop.dim_purchasegroupid <> porgp.dim_purchasegroupid;
						
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_purchaseorg pororg	
SET dim_purchaseorgid = pororg.dim_purchaseorgid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_EKORG = pororg.purchaseorgcode
		AND pop.dim_purchaseorgid <> pororg.dim_purchaseorgid;
						
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_tasklisttype tsklt	
SET dim_tasklisttypeid = tsklt.dim_tasklisttypeid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_PLNTY = tsklt.tasklisttypecode
		AND pop.dim_tasklisttypeid <> tsklt.dim_tasklisttypeid;

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_vendor ven	
SET dim_vendorid = ven.dim_vendorid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_LIFNR = ven.vendornumber
		AND pop.dim_vendorid <> ven.dim_vendorid;						
						
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_costcenter coctr	
SET dim_costcenterid = coctr.dim_costcenterid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_ANFKO = coctr.code
		AND pop.dim_costcenterid <> coctr.dim_costcenterid;	
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_PODocumentNo = ifnull(AFVC_EBELN, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_PODocumentNo <> ifnull(AA.AFVC_EBELN, 'Not Set');

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_functionalarea fctar	
SET dim_functionalareaid = fctar.dim_functionalareaid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_FUNC_AREA = fctar.functionalarea
		AND pop.dim_functionalareaid <> fctar.dim_functionalareaid;

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_objecttype objt	
SET dim_objecttypeid = objt.dim_objecttypeid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_OTYPE = objt.objecttype
		AND pop.dim_objecttypeid <> objt.dim_objecttypeid;

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_unitofmeasure uniom	
SET dim_unitofmeasureid = uniom.dim_unitofmeasureid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_MEINH = uniom.uom
		AND pop.dim_unitofmeasureid <> uniom.dim_unitofmeasureid;

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_POItemNo = ifnull(AFVC_EBELP, 0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_POItemNo <> ifnull(AA.AFVC_EBELP, 0);
		
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_date asedt	
SET Dim_DateIdActualStartExec = asedt.Dim_DateId
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_ISDD = asedt.datevalue 
		AND AA.AFVC_BUKRS = asedt.companycode
		AND pop.Dim_DateIdActualStartExec <> asedt.Dim_DateId;
						
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_date afedt	
SET Dim_DateIdActualFinishExec = afedt.Dim_DateId
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_IEDD = afedt.datevalue 
		AND AA.AFVC_BUKRS = afedt.companycode
		AND pop.Dim_DateIdActualFinishExec <> afedt.Dim_DateId;						
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_hourActualStartExec = ifnull(AFVV_ISDZ, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourActualStartExec <> ifnull(AA.AFVV_ISDZ, 'Not Set');						
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_hourActualFinishExe = ifnull(AFVV_IEDZ, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourActualFinishExe <> ifnull(AA.AFVV_IEDZ, 'Not Set');				

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_date ssedt	
SET Dim_DateIdSchedStartExec = ssedt.Dim_DateId
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_FSAVD = ssedt.datevalue 
		AND AA.AFVC_BUKRS = ssedt.companycode
		AND pop.Dim_DateIdSchedStartExec <> ssedt.Dim_DateId;	

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA,dim_date sfedt	
SET Dim_DateIdSchedFinishExec = sfedt.Dim_DateId
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_FSEDD = sfedt.datevalue 
		AND AA.AFVC_BUKRS = sfedt.companycode
		AND pop.Dim_DateIdSchedFinishExec <> sfedt.Dim_DateId;	
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_hourSchedStartExec = ifnull(AFVV_FSAVZ, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourSchedStartExec <> ifnull(AA.AFVV_FSAVZ, 'Not Set');					
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_hourSchedFinishExec = ifnull(AFVV_FSEDZ, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourSchedFinishExec <> ifnull(AA.AFVV_FSEDZ, 'Not Set');				
				
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_RoutingRefSequence = ifnull(AFVC_VPLFL, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_RoutingRefSequence <> ifnull(AA.AFVC_VPLFL, 'Not Set');

UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_Operationshorttext = ifnull(AFVC_LTXA1, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_Operationshorttext <> ifnull(AA.AFVC_LTXA1, 'Not Set');
		
UPDATE fact_prodop_confirmation pop
from AFVC_AFVV AA
SET dd_descriptionline2 = ifnull(AFVC_LTXA2, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_descriptionline2 <> ifnull(AA.AFVC_LTXA2, 'Not Set');		

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET pop.dd_OrderNumber = po.dd_OrderNumber 
	WHERE	pop.dd_RoutingOperationNo = po.dd_RoutingOperationNo;	
			
/*UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dd_OperationNumber = po.dd_OperationNumber,
dd_WorkCenter = po.dd_WorkCenter,
dd_bomexplosionno = po.dd_bomexplosionno,
dim_productionorderstatusid = po.dim_productionorderstatusid
	WHERE	pop.dd_RoutingOperationNo = po.dd_ordernumber*/											

UPDATE fact_prodop_confirmation pop
SET dd_WorkCenter = 'Not Set'
	WHERE dd_WorkCenter is NULL;

UPDATE fact_prodop_confirmation pop
SET dd_bomexplosionno = 'Not Set'
	WHERE dd_bomexplosionno is NULL;

UPDATE fact_prodop_confirmation pop
SET dim_productionorderstatusid = 1
	WHERE dim_productionorderstatusid is NULL;
	
UPDATE fact_prodop_confirmation pop
FROM AFVC_AFVV AA,
     dim_company dc
SET amt_ExchangeRate = 1
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND ifnull(amt_ExchangeRate,-1) <> 1; 

UPDATE fact_prodop_confirmation pop
FROM AFVC_AFVV AA,
     dim_company dc,
	 tmp_getExchangeRate1 ex
SET amt_ExchangeRate = ex.exchangeRate 
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND  pFromCurrency = AA.AFVC_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = ANSIDATE(LOCAL_TIMESTAMP)
and fact_script_name = 'bi_populate_productionorder_fact' 
AND amt_ExchangeRate <> ex.exchangeRate ;


UPDATE fact_prodop_confirmation pop
FROM AFVC_AFVV AA,
     dim_company dc
SET amt_ExchangeRate_GBL = 1
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;  

UPDATE fact_prodop_confirmation pop
FROM AFVC_AFVV AA,
     dim_company dc,
	 tmp_getExchangeRate1 ex,
	 pGlobalCurrency_po_77
SET amt_ExchangeRate_GBL = ex.exchangeRate 
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND  pFromCurrency = AA.AFVC_WAERS
and pToCurrency = pGlobalCurrency
and pFromExchangeRate = 0
and pDate =  ANSIDATE(LOCAL_TIMESTAMP)   /*current date to be used for global rate*/
and fact_script_name = 'bi_populate_productionorder_fact'
AND amt_ExchangeRate <> ex.exchangeRate ;	


UPDATE fact_prodop_confirmation f
FROM afvc_afvv a
SET dd_objectid = a.afvc_arbid
WHERE f.dd_RoutingOperationNo = a.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = a.AFVC_APLZL;


UPDATE fact_prodop_confirmation f
FROM dim_workcenter wc
SET f.dim_workcenterid = wc.dim_workcenterid
WHERE wc.objectid = f.dd_objectid;

UPDATE fact_prodop_confirmation f
FROM dim_costcenter dc, CRCO c
SET f.dim_costcenterid = dc.dim_costcenterid
WHERE f.dd_objectid = c.crco_objid
AND c.crco_KOSTL = dc.code
AND f.dim_costcenterid <> dc.dim_costcenterid;

/* Update fields from AFVU */
UPDATE fact_prodop_confirmation f
from AFVU AA
SET f.ct_fixedruntime_theoretical = ifnull(AA.AFVU_USR04,0)
WHERE   f.dd_RoutingOperationNo = AA.AFVU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVU_APLZL
AND f.ct_fixedruntime_theoretical <> ifnull(AA.AFVU_USR04,0);

UPDATE fact_prodop_confirmation f
from AFVU AA
SET f.ct_variableruntime_theoretical = ifnull(AA.AFVU_USR05,0)
WHERE   f.dd_RoutingOperationNo = AA.AFVU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVU_APLZL
AND f.ct_variableruntime_theoretical <> ifnull(AA.AFVU_USR05,0);

/* Update dates other missing fields */
UPDATE fact_prodop_confirmation f
FROM AFRU , dim_date dd, dim_company dc
SET f.dim_dateidconfirmationentry = dd.dim_dateid
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_ERSDA = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidconfirmationentry <> dd.dim_dateid;

UPDATE fact_prodop_confirmation f
FROM AFRU , dim_date dd, dim_company dc
SET f.dim_dateidlastchange = dd.dim_dateid
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_LAEDA = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidlastchange <> dd.dim_dateid;

UPDATE fact_prodop_confirmation f
FROM AFRU , dim_date dd, dim_company dc
SET f.dim_dateidposting_confirmation = dd.dim_dateid
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_BUDAT = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidposting_confirmation <> dd.dim_dateid;


UPDATE fact_prodop_confirmation f
FROM AFRU , dim_date dd, dim_company dc
SET f.dim_dateidconfirmedstart = dd.dim_dateid
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_ISBD = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidconfirmedstart <> dd.dim_dateid;

UPDATE fact_prodop_confirmation f
FROM AFRU , dim_date dd, dim_company dc
SET f.dim_dateidconfirmedfinish = dd.dim_dateid
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_IEBD = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidconfirmedfinish <> dd.dim_dateid;

UPDATE fact_prodop_confirmation f
FROM AFRU
SET f.dd_confirmedstart_datetime = (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)))
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND AFRU_ISBD IS NOT NULL AND AFRU_ISBZ <> 240000
AND f.dd_confirmedstart_datetime <> (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)));


UPDATE fact_prodop_confirmation f
FROM AFRU
SET f.dd_confirmedstart_datetime = (cast((AFRU_ISBD + INTERVAL '1' DAY) as varchar(10)) || ' ' || (substring('00',1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)))
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND AFRU_ISBD IS NOT NULL AND AFRU_ISBZ = 240000
AND f.dd_confirmedstart_datetime <> (cast((AFRU_ISBD + INTERVAL '1' DAY) as varchar(10)) || ' ' || (substring('00',1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)));

UPDATE fact_prodop_confirmation f
FROM AFRU
SET f.dd_confirmedend_datetime = (cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)))
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND AFRU_IEBD IS NOT NULL AND AFRU_IEBZ <> 240000
AND f.dd_confirmedend_datetime <> (cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)));

UPDATE fact_prodop_confirmation f
FROM AFRU
SET f.dd_confirmedend_datetime = (cast((AFRU_IEBD + INTERVAL '1' DAY) as varchar(10)) || ' ' || (substring('00',1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)))
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND AFRU_IEBD IS NOT NULL AND AFRU_IEBZ = 240000
AND f.dd_confirmedend_datetime <> (cast((AFRU_IEBD + INTERVAL '1' DAY) as varchar(10)) || ' ' || (substring('00',1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)));

/* Update all UOMs */

UPDATE fact_prodop_confirmation f
from AFVC_AFVV AA,dim_unitofmeasure uniom
SET f.dim_uomprocessingtime = uniom.dim_unitofmeasureid
WHERE   f.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND AA.AFVV_BEAZE = uniom.uom
AND f.dim_uomprocessingtime <> uniom.dim_unitofmeasureid;


UPDATE fact_prodop_confirmation f
from AFVU AA,dim_unitofmeasure uniom
SET f.dim_uomfixedruntime = uniom.dim_unitofmeasureid
WHERE   f.dd_RoutingOperationNo = AA.AFVU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVU_APLZL
AND AA.AFVU_USE04 = uniom.uom
AND f.dim_uomfixedruntime <> uniom.dim_unitofmeasureid;

UPDATE fact_prodop_confirmation f
from AFVU AA,dim_unitofmeasure uniom
SET f.dim_uomvariableruntime_theoretical = uniom.dim_unitofmeasureid
WHERE   f.dd_RoutingOperationNo = AA.AFVU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVU_APLZL
AND AA.AFVU_USE05 = uniom.uom
AND f.dim_uomvariableruntime_theoretical <> uniom.dim_unitofmeasureid;

UPDATE fact_prodop_confirmation f
from AFRU AA,dim_unitofmeasure uniom
SET f.dim_uombreaktime = uniom.dim_unitofmeasureid
WHERE   f.dd_RoutingOperationNo = AA.AFRU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFRU_APLZL
AND f.dd_confirmation = AA.AFRU_RUECK
AND f.dd_confirmationcounter = AA.AFRU_RMZHL
AND AA.AFRU_ZEIER = uniom.uom
AND f.dim_uombreaktime <> uniom.dim_unitofmeasureid;

UPDATE fact_prodop_confirmation f
from AFRU AA,dim_unitofmeasure uniom
SET f.dim_uom_activitytobeconfirmed = uniom.dim_unitofmeasureid
WHERE   f.dd_RoutingOperationNo = AA.AFRU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFRU_APLZL
AND f.dd_confirmation = AA.AFRU_RUECK
AND f.dd_confirmationcounter = AA.AFRU_RMZHL
AND AA.AFRU_ILE01 = uniom.uom
AND f.dim_uom_activitytobeconfirmed <> uniom.dim_unitofmeasureid;


UPDATE fact_prodop_confirmation pop
from fact_productionorder po
SET dd_OperationNumber = po.dd_OperationNumber,
dd_WorkCenter = po.dd_WorkCenter,
dd_bomexplosionno = po.dd_bomexplosionno,
dim_productionorderstatusid = po.dim_productionorderstatusid
        WHERE   pop.dd_RoutingOperationNo = po.dd_RoutingOperationNo;


		
		/* Update Production Order Columns */
UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidactualitemfinish_prodorder = po.dim_dateidactualitemfinish
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidactualitemfinish_prodorder <> po.dim_dateidactualitemfinish;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidactualheaderfinishdate_merck_prodorder = po.dim_dateidactualheaderfinishdate_merck
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidactualheaderfinishdate_merck_prodorder <> po.dim_dateidactualheaderfinishdate_merck;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidactualrelease_prodorder = po.dim_dateidactualrelease
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidactualrelease_prodorder <> po.dim_dateidactualrelease;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidactualstart_prodorder = po.dim_dateidactualstart
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidactualstart_prodorder <> po.dim_dateidactualstart;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidbasicfinish_prodorder = po.dim_dateidbasicfinish
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidbasicfinish_prodorder <> po.dim_dateidbasicfinish;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidbasicstart_prodorder = po.dim_dateidbasicstart
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidbasicstart_prodorder <> po.dim_dateidbasicstart;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidconfirmedorderfinish_prodorder = po.dim_dateidconfirmedorderfinish
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidconfirmedorderfinish_prodorder <> po.dim_dateidconfirmedorderfinish;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidlastscheduling_prodorder = po.dim_dateidlastscheduling
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidlastscheduling_prodorder <> po.dim_dateidlastscheduling;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_partidheader_prodorder = po.dim_partidheader
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_partidheader_prodorder <> po.dim_partidheader;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_partiditem_prodorder = po.dim_partiditem
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_partiditem_prodorder <> po.dim_partiditem;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_mrpcontrollerid_prodorder = po.dim_mrpcontrollerid
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_mrpcontrollerid_prodorder <> po.dim_mrpcontrollerid;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_ordertypeid_prodorder = po.dim_ordertypeid
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_ordertypeid_prodorder <> po.dim_ordertypeid;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidplannedorderdelivery_prodorder = po.dim_dateidplannedorderdelivery
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidplannedorderdelivery_prodorder <> po.dim_dateidplannedorderdelivery;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_productionschedulerid_prodorder = po.dim_productionschedulerid
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_productionschedulerid_prodorder <> po.dim_productionschedulerid;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidrelease_prodorder = po.dim_dateidrelease
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidrelease_prodorder <> po.dim_dateidrelease;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidroutingtransfer_prodorder = po.dim_dateidroutingtransfer
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidroutingtransfer_prodorder <> po.dim_dateidroutingtransfer;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidscheduledfinishheader_prodorder = po.dim_dateidscheduledfinishheader
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidscheduledfinishheader_prodorder <> po.dim_dateidscheduledfinishheader;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidscheduledfinish_prodorder = po.dim_dateidscheduledfinish
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidscheduledfinish_prodorder <> po.dim_dateidscheduledfinish;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidscheduledrelease_prodorder = po.dim_dateidscheduledrelease
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidscheduledrelease_prodorder <> po.dim_dateidscheduledrelease;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidscheduledstart_prodorder = po.dim_dateidscheduledstart
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidscheduledstart_prodorder <> po.dim_dateidscheduledstart;

UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dim_dateidtechnicalcompletion_prodorder = po.dim_dateidtechnicalcompletion
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidtechnicalcompletion_prodorder <> po.dim_dateidtechnicalcompletion;

/* For some reason operation number is not getting set, so we are running the update statement here */
update fact_prodop_confirmation f
FROM  AFVC_AFVV AA
set f.dd_OperationNumber = AA.AFVC_VORNR
WHERE f.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND f.dd_OperationNumber <> AA.AFVC_VORNR;