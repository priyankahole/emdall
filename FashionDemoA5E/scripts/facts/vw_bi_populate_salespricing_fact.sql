/* ##################################################################################################################
  
     Script         : vw_bi_populate_salespricing_fact.sql
     Author         : Issam
     Created On     : 29 Dec 2013
  
  
     Description    : Script for sales pricing processing
  
     Change History
     Date            By        Version           Desc

#################################################################################################################### */

drop table if exists fact_salespricing_tmp;
create table fact_salespricing_tmp as
select DISTINCT row_number() over () fact_salespricingid,
		so.dim_salesorgid,
		so.dim_distributionchannelid,
		so.dim_salesdivisionid,
		so.dim_CustomerID,
		so.dd_salesdocno,
		so.dd_salesitemno,
		so.Dim_DocumentCategoryid,
		so.Dim_SalesDocumentTypeid,
		so.dd_CustomerPONo,
		so.Dim_DateIdSODocument,
		so.Dim_DateidSalesOrderCreated,
		so.Dim_DateIdAfsReqDelivery,
		so.dim_salesorderitemcategoryid,
		so.Dim_Partid,
		so.Dim_CurrencyId,
		so.dim_salesofficeid,
		so.Dim_SalesOrderHeaderStatusid,
		so.amt_ExchangeRate,		
		0 ct_CumConfirmedQty,
		0 ct_CumOrderQty,
		0 amt_SubTotal2,
		0 amt_SubTotal3,
		0 amt_SubTotal4,
		0 amt_SubTotal5,
		tpc.dim_transactionpriceconditionid
FROM fact_salesorder so
JOIN dim_transactionpricecondition tpc
ON so.dd_DocumentConditionNo = tpc.DocumentConditionNumber
AND so.dd_SalesItemNo = tpc.ConditionItemNumber
UNION 
select DISTINCT row_number() over () fact_salespricingid,
		so.dim_salesorgid,
		so.dim_distributionchannelid,
		so.dim_salesdivisionid,
		so.dim_CustomerID,
		so.dd_salesdocno,
		so.dd_salesitemno,
		so.Dim_DocumentCategoryid,
		so.Dim_SalesDocumentTypeid,
		so.dd_CustomerPONo,
		so.Dim_DateIdSODocument,
		so.Dim_DateidSalesOrderCreated,
		so.Dim_DateIdAfsReqDelivery,
		so.dim_salesorderitemcategoryid,
		so.Dim_Partid,
		so.Dim_CurrencyId,
		so.dim_salesofficeid,
		so.Dim_SalesOrderHeaderStatusid,
		so.amt_ExchangeRate,		
		so.ct_CumConfirmedQty,
		so.ct_CumOrderQty,
		so.amt_SubTotal2,
		so.amt_SubTotal3,
		so.amt_SubTotal4,
		so.amt_SubTotal5,
		1 dim_transactionpriceconditionid
FROM fact_salesorder so;	


drop table if exists fact_salespricing;
rename table fact_salespricing_tmp to fact_salespricing;

CALL VECTORWISE( COMBINE 'fact_salespricing');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salespricing;