		/* START TIME */
			select 'START OF PROC vw_bi_populate_purchase_requisition_fact',TIMESTAMP(LOCAL_TIMESTAMP);

		/* Table to monitor number of records in FACT, based on fact_purchase_requisitionID */
			delete from NUMBER_FOUNTAIN where table_name = 'fact_purchase_requisition';
			
			INSERT INTO NUMBER_FOUNTAIN
			   SELECT 'fact_purchase_requisition', ifnull(max(fact_purchase_requisitionid), 0)
				 FROM fact_purchase_requisition;
		
		/* **********  START MAIN UPDATE PROCESS   ********** */
		
		/* 1. Dim_DocumentTypeid -> EBAN_BSART, EBAN_BSTYP */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_DocumentType dtp
		SET F.Dim_DocumentTypeid = dtp.Dim_DocumentTypeid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND dtp.Type     = EBAN_BSART
			  AND dtp.Category = EBAN_BSTYP
			  AND F.Dim_DocumentTypeid <> dtp.Dim_DocumentTypeid;

		/* 2. dd_PurchaseReqDocCat -> EBAN_BSTYP */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_PurchaseReqDocCat = ifnull(C.EBAN_BSTYP,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_PurchaseReqDocCat <> ifnull(C.EBAN_BSTYP,'Not Set');

		/* 3. dd_deletionindicator -> EBAN_LOEKZ */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_deletionindicator = ifnull(C.EBAN_LOEKZ,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_deletionindicator <> ifnull(C.EBAN_LOEKZ,'Not Set');			  			  

		/* 4. dim_prstatusid -> EBAN_STATU */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_ProcessingStatus = ifnull(C.EBAN_STATU,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_ProcessingStatus <> ifnull(C.EBAN_STATU,'Not Set');	
		
		/* 5. dd_CreationIndicator -> EBAN_ESTKZ */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_CreationIndicator = ifnull(C.EBAN_ESTKZ,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_CreationIndicator <> ifnull(C.EBAN_ESTKZ,'Not Set');	

		/* 6. Dim_PurchaseGroupid -> EBAN_EKGRP */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_PurchaseGroup its
		SET F.Dim_PurchaseGroupid = its.Dim_PurchaseGroupid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND its.PurchaseGroup = C.EBAN_EKGRP
			  AND F.Dim_PurchaseGroupid <> its.Dim_PurchaseGroupid;	
		
		/* 7. dd_ReleaseIndicator -> EBAN_FRGKZ */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_ReleaseIndicator = ifnull(C.EBAN_FRGKZ,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_ReleaseIndicator <> ifnull(C.EBAN_FRGKZ,'Not Set');	

    	/* 8. dd_CreatedObjectByPerson -> EBAN_ERNAM */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_CreatedObjectByPerson = ifnull(C.EBAN_ERNAM,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_CreatedObjectByPerson <> ifnull(C.EBAN_ERNAM,'Not Set');	
  
		/* 9. dim_datechangeonid -> EBAN_ERDAT */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_date pd
		SET F.dim_datechangeonid = pd.dim_dateid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND pd.datevalue   = C.EBAN_ERDAT
			  AND pd.companycode = 'Not Set'
			  AND F.dim_datechangeonid <> pd.dim_dateid;	

		/* 10. DIM_MATERIALID -> EBAN_MATNR, EBAN_WERKS  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_part pt
		SET F.DIM_MATERIALID = pt.dim_partid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND pt.PartNumber = C.EBAN_MATNR
			  AND pt.Plant      = C.EBAN_WERKS
			  AND F.DIM_MATERIALID <> pt.dim_partid;	

		/* 11. Dim_Plantid -> EBAN_WERKS  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_plant pl
		SET F.Dim_Plantid = pl.dim_plantid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND pl.PlantCode = EBAN_WERKS
			  AND F.Dim_Plantid <> pl.dim_plantid;	

		/* 12. dim_storagelocationid -> EBAN_LGORT, EBAN_WERKS  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_StorageLocation sl
		SET F.dim_storagelocationid = sl.Dim_StorageLocationid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND sl.LocationCode = C.EBAN_LGORT 
			  AND sl.Plant        = C.EBAN_WERKS
			  AND F.dim_storagelocationid <> sl.Dim_StorageLocationid;

		/* 13. Dim_MaterialGroupid -> EBAN_MATKL  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_MaterialGroup mg
		SET F.Dim_MaterialGroupid = mg.Dim_MaterialGroupid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND mg.MaterialGroupCode = C.EBAN_MATKL
			  AND F.Dim_MaterialGroupid <> mg.Dim_MaterialGroupid;			  

    	/* 14. dd_RequestTrackNo -> EBAN_BEDNR */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_RequestTrackNo = ifnull(C.EBAN_BEDNR,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_RequestTrackNo <> ifnull(C.EBAN_BEDNR,'Not Set');				  
		
		/* 15. DIM_SupplPlantStockTranspOrdId -> EBAN_RESWK  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_Plant dps
		SET F.DIM_SupplPlantStockTranspOrdId = dps.dim_plantid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND dps.PlantCode = C.EBAN_RESWK
			  AND F.DIM_SupplPlantStockTranspOrdId <> dps.dim_plantid;	
		
    	/* 16. ct_PurchaseReq -> EBAN_MENGE */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET ct_PurchaseReq = ifnull(C.EBAN_MENGE, 0)
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND ct_PurchaseReq <> ifnull(C.EBAN_MENGE, 0);		

        /* 17. DIM_SupplPlantStockTranspOrdId -> EBAN_MEINS  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_UnitOfMeasure uom
		SET F.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND uom.UOM = C.EBAN_MEINS
			  AND F.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;

		/* 18. dim_dateRequsiotionid -> EBAN_BADAT */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_date pd
		SET F.dim_dateRequsiotionid = pd.dim_dateid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND pd.datevalue   = C.EBAN_BADAT
			  AND pd.companycode = 'Not Set'
			  AND F.dim_dateRequsiotionid <> pd.dim_dateid;

		/* 19. dim_datePurchaseReqReleaseid -> EBAN_FRGDT */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_date pd
		SET F.dim_datePurchaseReqReleaseid = pd.dim_dateid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND pd.datevalue   = C.EBAN_FRGDT
			  AND pd.companycode = 'Not Set'
			  AND F.dim_datePurchaseReqReleaseid <> pd.dim_dateid;

		/* 20. dim_dateItemDeliveryid -> EBAN_LFDAT */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_date pd
		SET F.dim_dateItemDeliveryid = pd.dim_dateid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND pd.datevalue   = C.EBAN_LFDAT
			  AND pd.companycode = 'Not Set'
			  AND F.dim_dateItemDeliveryid <> pd.dim_dateid;

    	/* 21. amt_PurchaseReqPrice -> EBAN_PREIS */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET amt_PurchaseReqPrice = ifnull(C.EBAN_PREIS, 0)
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND amt_PurchaseReqPrice <> ifnull(C.EBAN_PREIS, 0);	

    	/* 22. ct_PriceUnit -> EBAN_PEINH */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET ct_PriceUnit = ifnull(C.EBAN_PEINH, 0)
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND ct_PriceUnit <> ifnull(C.EBAN_PEINH, 0);

        /* 23. Dim_ItemCategoryid -> EBAN_PSTYP  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_ItemCategory ic
		SET F.Dim_ItemCategoryid = ic.Dim_ItemCategoryid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND ic.CategoryCode = C.EBAN_PSTYP
			  AND F.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;

	    /* 24. dim_accountcategoryid -> EBAN_KNTTP  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_AccountCategory ac
		SET F.dim_accountcategoryid = ac.Dim_AccountCategoryid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND ac.Category = C.EBAN_KNTTP
			  AND F.dim_accountcategoryid <> ac.Dim_AccountCategoryid;

	    /* 25. dim_consumptiontypeid -> EBAN_KZVBR  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_ConsumptionType ct
		SET F.dim_consumptiontypeid = ct.Dim_ConsumptionTypeid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND ct.ConsumptionCode = C.EBAN_KZVBR
			  AND F.dim_consumptiontypeid <> ct.Dim_ConsumptionTypeid;

	    /* 26. Dim_Vendorid -> EBAN_LIFNR  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_vendor v
		SET F.Dim_Vendorid = v.Dim_Vendorid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND v.VendorNumber = C.EBAN_LIFNR
			  AND F.Dim_Vendorid <> v.Dim_Vendorid;
		  
		/* 27. Dim_PurchaseOrgid -> EBAN_EKORG  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_PurchaseOrg po
		SET F.Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND po.PurchaseOrgCode = C.EBAN_EKORG
			  AND F.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;	
		
		/* 28. Dim_FixedVendorid -> EBAN_FLIEF  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_vendor v
		SET F.Dim_FixedVendorid = v.Dim_Vendorid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND v.VendorNumber = EBAN_FLIEF
			  AND F.Dim_FixedVendorid <> v.Dim_Vendorid;	
			  
    	/* 29. dd_PurchaseInforRecord -> EBAN_INFNR */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_PurchaseInforRecord = ifnull(C.EBAN_INFNR,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_PurchaseInforRecord <> ifnull(C.EBAN_INFNR,'Not Set');
    	
		/* 30. dd_PurchaseOrderNo -> EBAN_EBELN */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_PurchaseOrderNo = ifnull(C.EBAN_EBELN,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_PurchaseOrderNo <> ifnull(C.EBAN_EBELN,'Not Set');
			  
		/* 31. dd_PurchaseOrderItemNo -> EBAN_EBELP */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_PurchaseOrderItemNo = ifnull(C.EBAN_EBELP,0)
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_PurchaseOrderItemNo <> ifnull(C.EBAN_EBELP,0);

		/* 32. dim_datePurchaseOrderId -> EBAN_BEDAT */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, dim_date pd
		SET F.dim_datePurchaseOrderId = pd.dim_dateid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND pd.datevalue   = C.EBAN_BEDAT
			  AND pd.companycode = 'Not Set'
			  AND F.dim_datePurchaseOrderId <> pd.dim_dateid;

		/* 33. ct_OrderedAgainstPurchase -> EBAN_BSMNG */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET ct_OrderedAgainstPurchase = ifnull(C.EBAN_BSMNG,0)
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.ct_OrderedAgainstPurchase <> ifnull(C.EBAN_BSMNG,0);
			  
		/* 34. dd_PurchaseReqClosed -> EBAN_EBAKZ */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_PurchaseReqClosed = ifnull(C.EBAN_EBAKZ,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_PurchaseReqClosed <> ifnull(C.EBAN_EBAKZ,'Not Set');

		/* 35. Dim_Currencyid -> EBAN_WAERS  */
		UPDATE fact_purchase_requisition f
		FROM EBAN c, Dim_Currency dcr
		SET F.Dim_Currencyid = dcr.Dim_Currencyid
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND dcr.CurrencyCode = C.EBAN_WAERS
			  AND F.Dim_Currencyid <> dcr.Dim_Currencyid;			  
	  
		/* 36. dd_ManufacturePartNo -> EBAN_MFRPN */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_ManufacturePartNo = ifnull(C.EBAN_MFRPN,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_ManufacturePartNo <> ifnull(C.EBAN_MFRPN,'Not Set');
			  
		/* 37. dd_PlannedDeliveryDays -> EBAN_PLIFZ */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_PlannedDeliveryDays = ifnull(C.EBAN_PLIFZ,0)
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_PlannedDeliveryDays <> ifnull(C.EBAN_PLIFZ,0);

		/* 38. dd_PurchaseReqBlocked -> EBAN_BLCKD */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET dd_PurchaseReqBlocked = ifnull(C.EBAN_BLCKD,'Not Set')
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND F.dd_PurchaseReqBlocked <> ifnull(C.EBAN_BLCKD,'Not Set');
		
		/* 39. amt_ExchangeRate -> JUST FOR FACT */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET amt_ExchangeRate = 1
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND amt_ExchangeRate is null;			  

		/* 40. amt_ExchangeRate_GBL -> JUST FOR FACT */
		UPDATE fact_purchase_requisition f
		FROM EBAN c
		SET amt_ExchangeRate_GBL = 1
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND amt_ExchangeRate_GBL is null;	
		
		/* **********  FINISH MAIN UPDATE PROCESS   ********** */
		
		/* **********  START GET NEW ROWS TO INSERT   ********** */	
		
		/* CLOSE TRANSACTIONS ON FACT */
		CALL vectorwise(combine 'fact_purchase_requisition');
		
		DROP TABLE IF EXISTS tmp_insert_fct_prchs_rqstn;
		DROP TABLE IF EXISTS tmp_delete_fct_prchs_rqstn;
		
		/* GET ALL NEW DATA based on fact grain */
		CREATE TABLE tmp_insert_fct_prchs_rqstn AS
		SELECT EBAN_BANFN AS dd_purchasereqno,
			   EBAN_BNFPO AS dd_purchasereqitemno
		FROM EBAN;
		
		CREATE TABLE tmp_delete_fct_prchs_rqstn AS
	    SELECT *
		FROM tmp_insert_fct_prchs_rqstn
		WHERE 1=2;
		
		/* INSERT EXISTING DATA FROM FACT based on fact grain */
		INSERT INTO tmp_delete_fct_prchs_rqstn
	    SELECT DISTINCT dd_purchasereqno, dd_purchasereqitemno
		FROM fact_purchase_requisition;

		/* REMOVE EXITING FACT GRAIN KEY from new data data stage arrival*/
		CALL VECTORWISE(combine 'tmp_insert_fct_prchs_rqstn-tmp_delete_fct_prchs_rqstn');		
		
		/* **********  FINISH GET NEW ROWS TO INSERT   ********** */	

		/* **********  START INSERT NEW ROWS IN FACT   ********** */
		insert into fact_purchase_requisition (
							fact_purchase_requisitionid   , 
							dd_purchasereqno 			  , /* EBAN_BANFN */
							dd_purchasereqitemno		  , /* EBAN_BNFPO */
							Dim_DocumentTypeid            , /* EBAN_BSART */
							dd_PurchaseReqDocCat          , /* EBAN_BSTYP */
							dd_DeletionIndicator          , /* EBAN_LOEKZ */
							dd_ProcessingStatus           , /* EBAN_STATU */
							dd_CreationIndicator          , /* EBAN_ESTKZ */
							Dim_PurchaseGroupid           , /* EBAN_EKGRP */ 
							dd_ReleaseIndicator           , /* EBAN_FRGKZ */
							dd_CreatedObjectByPerson      , /* EBAN_ERNAM */
							dim_datechangeonid            , /* EBAN_ERDAT */
							DIM_MATERIALID                , /* EBAN_MATNR */
							Dim_Plantid                   , /* EBAN_WERKS */
							dim_storagelocationid         , /* EBAN_LGORT */
							Dim_MaterialGroupid           , /* EBAN_MATKL */
							dd_RequestTrackNo             , /* EBAN_BEDNR */
							DIM_SupplPlantStockTranspOrdId, /* EBAN_RESWK */
							ct_PurchaseReq                , /* EBAN_MENGE */
							Dim_UnitOfMeasureid           , /* EBAN_MEINS */
							dim_dateRequsiotionid         , /* EBAN_BADAT */
							dim_datePurchaseReqReleaseid  , /* EBAN_FRGDT */
							dim_dateItemDeliveryid        , /* EBAN_LFDAT */
							amt_PurchaseReqPrice          , /* EBAN_PREIS */
							ct_PriceUnit                  , /* EBAN_PEINH */
							Dim_ItemCategoryid            , /* EBAN_PSTYP */
							dim_accountcategoryid         , /* EBAN_KNTTP */
							dim_consumptiontypeid         , /* EBAN_KZVBR */
							Dim_Vendorid                  , /* EBAN_LIFNR */
							Dim_PurchaseOrgid             , /* EBAN_EKORG */
							Dim_FixedVendorid             , /* EBAN_FLIEF */
							dd_PurchaseInforRecord        , /* EBAN_INFNR */
							dd_PurchaseOrderNo            , /* EBAN_EBELN */
							dd_PurchaseOrderItemNo        , /* EBAN_EBELP */
							dim_datePurchaseOrderId       , /* EBAN_BEDAT */
							ct_OrderedAgainstPurchase     , /* EBAN_BSMNG */
							dd_PurchaseReqClosed          , /* EBAN_EBAKZ */
							Dim_Currencyid                , /* EBAN_WAERS */
							dd_ManufacturePartNo          , /* EBAN_MFRPN */
							dd_PlannedDeliveryDays        , /* EBAN_PLIFZ */
							dd_PurchaseReqBlocked		  , /* EBAN_BLCKD */
							amt_ExchangeRate,
							amt_ExchangeRate_GBL
							 )
		select (SELECT max_id 
				FROM NUMBER_FOUNTAIN
				WHERE table_name = 'fact_purchase_requisition'
				) + row_number() over() 							AS fact_purchase_requisitionid,
				EBAN_BANFN 											AS dd_purchasereqno,
				EBAN_BNFPO 											AS dd_purchasereqitemno,
				ifnull((SELECT Dim_DocumentTypeid
						FROM Dim_DocumentType dtp
						WHERE     dtp.Type     = EBAN_BSART
							  AND dtp.Category = EBAN_BSTYP),1)   AS Dim_DocumentTypeid,       /* 1. */
				ifnull(cast(EBAN_BSTYP as varchar(7)),'Not Set')  AS dd_PurchaseReqDocCat,     /* 2. */
				ifnull(cast(EBAN_LOEKZ as varchar(7)),'Not Set')  AS dd_deletionindicator,     /* 3. */
				ifnull(cast(EBAN_STATU as varchar(7)),'Not Set')  AS dd_ProcessingStatus,      /* 4. */
				ifnull(cast(EBAN_ESTKZ as varchar(7)),'Not Set')  AS dd_CreationIndicator,     /* 5. */
				ifnull((SELECT Dim_PurchaseGroupid
						FROM Dim_PurchaseGroup pg
						WHERE pg.PurchaseGroup = EBAN_EKGRP),1)   AS Dim_PurchaseGroupid,      /* 6. */
				ifnull(cast(EBAN_FRGKZ as varchar(7)),'Not Set')  AS dd_ReleaseIndicator,      /* 7. */
				ifnull(cast(EBAN_ERNAM as varchar(12)),'Not Set') AS dd_CreatedObjectByPerson, /* 8. */
				ifnull((SELECT pd.dim_dateid
						FROM dim_date pd 
						WHERE     pd.datevalue   = EBAN_ERDAT
							  AND pd.companycode = 'Not Set'),1) AS dim_datechangeonid, 	   /* 9. */ /* check this line, solved */
				ifnull((SELECT dim_partid
						FROM dim_Part pt 
						WHERE     pt.PartNumber = EBAN_MATNR
							  AND pt.Plant      = EBAN_WERKS),1)   AS DIM_MATERIALID, 			/* 10. */ /* check this line, solved */
				ifnull((SELECT pl.dim_plantid 
						FROM dim_plant pl 
						WHERE pl.PlantCode = EBAN_WERKS),1) 		 AS Dim_Plantid,            /* 11. */
				ifnull((SELECT Dim_StorageLocationid
						FROM dim_StorageLocation sl
						WHERE     sl.LocationCode = EBAN_LGORT 
							  AND sl.Plant        = EBAN_WERKS ),1) AS dim_storagelocationid,   /* 12. */
				ifnull((SELECT Dim_MaterialGroupid
						FROM Dim_MaterialGroup mg
						WHERE mg.MaterialGroupCode = EBAN_MATKL), 1) AS Dim_MaterialGroupid,    /* 13. */
				ifnull(cast(EBAN_BEDNR as varchar(10)),'Not Set')    AS dd_RequestTrackNo,      /* 14. */
				ifnull((SELECT Dim_Plantid
						FROM Dim_Plant dps
						WHERE dps.PlantCode = EBAN_RESWK),1)         AS DIM_SupplPlantStockTranspOrdId, /* 15. */
				ifnull(EBAN_MENGE,0) 								 AS ct_PurchaseReq,         /* 16. */
				ifnull((SELECT Dim_UnitOfMeasureid
						FROM Dim_UnitOfMeasure uom
						WHERE uom.UOM = EBAN_MEINS), 1) AS Dim_UnitOfMeasureid,                 /* 17. */
				ifnull((SELECT pd.dim_dateid
						FROM dim_date pd 
						WHERE     pd.datevalue   = EBAN_BADAT
							  AND pd.companycode = 'Not Set'),1) AS dim_dateRequsiotionid,      /* 18. */ /* check this line, solved */
				ifnull((SELECT pd.dim_dateid
						FROM dim_date pd 
						WHERE     pd.datevalue   = EBAN_FRGDT
							  AND pd.companycode = 'Not Set'),1) AS dim_datePurchaseReqReleaseid, /* 19. */ /* check this line, solved */
				ifnull((SELECT pd.dim_dateid
						FROM dim_date pd 
						WHERE     pd.datevalue   = EBAN_LFDAT
							  AND pd.companycode = 'Not Set'),1) AS dim_dateItemDeliveryid,      /* 20. */ /* check this line, solved */
				ifnull(EBAN_PREIS,0) 								 AS amt_PurchaseReqPrice,    /* 21. */
				ifnull(EBAN_PEINH,0) 								 AS ct_PriceUnit,            /* 22. */
				ifnull((SELECT Dim_ItemCategoryid
						FROM Dim_ItemCategory ic
						WHERE ic.CategoryCode = EBAN_PSTYP),1) 	 AS Dim_ItemCategoryid,          /* 23. */
				ifnull((SELECT Dim_AccountCategoryid
						FROM Dim_AccountCategory ac
						WHERE ac.Category = EBAN_KNTTP), 1) 		 AS dim_accountcategoryid,   /* 24. */
				ifnull((SELECT Dim_ConsumptionTypeid
						FROM Dim_ConsumptionType ct
						WHERE ct.ConsumptionCode = EBAN_KZVBR),1)  AS dim_consumptiontypeid,     /* 25. */
				ifnull((SELECT v.Dim_Vendorid 
						FROM dim_vendor v
						WHERE v.VendorNumber = EBAN_LIFNR),1) 	 AS Dim_Vendorid,                /* 26. */
				ifnull((SELECT Dim_PurchaseOrgid
						FROM Dim_PurchaseOrg po
						WHERE po.PurchaseOrgCode = EBAN_EKORG),1)  AS Dim_PurchaseOrgid,         /* 27. */
				ifnull((SELECT v.Dim_Vendorid 
						FROM dim_vendor v
						WHERE v.VendorNumber = EBAN_FLIEF),1) 	 AS Dim_FixedVendorid,           /* 28. */
				ifnull(EBAN_INFNR,'Not Set') 								 AS dd_PurchaseInforRecord,  /* 29. */
				ifnull(EBAN_EBELN,'Not Set')								 AS dd_PurchaseOrderNo,      /* 30. */
				ifnull(EBAN_EBELP,0)								 AS dd_PurchaseOrderItemNo,  /* 31. */
				ifnull((SELECT pd.dim_dateid
						FROM dim_date pd 
						WHERE     pd.datevalue   = EBAN_BEDAT
							  AND pd.companycode = 'Not Set'),1) AS dim_datePurchaseOrderId,     /* 32. */ /* check this line, solved */
				ifnull(EBAN_BSMNG,0)                                 AS ct_OrderedAgainstPurchase, /* 33. */
				ifnull(cast(EBAN_EBAKZ as varchar(7)),'Not Set')     AS dd_PurchaseReqClosed,      /* 34. */
				ifnull((SELECT Dim_Currencyid
						FROM Dim_Currency dcr
						WHERE dcr.CurrencyCode = EBAN_WAERS),1) 	 AS Dim_Currencyid,            /* 35. */
				ifnull(cast(EBAN_MFRPN as varchar(40)),'Not Set')    AS dd_ManufacturePartNo,      /* 36. */
				ifnull(EBAN_PLIFZ,0) 								 AS dd_PlannedDeliveryDays,    /* 37. */
				ifnull(cast(EBAN_BLCKD as varchar(7)),'Not Set')     AS dd_PurchaseReqBlocked,     /* 38. */
				1 													 AS amt_ExchangeRate,
				1 													 AS amt_ExchangeRate_GBL
		FROM eban F, 
			 tmp_insert_fct_prchs_rqstn i
		WHERE     F.EBAN_BANFN = I.dd_purchasereqno
			  AND F.EBAN_BNFPO = I.dd_purchasereqitemno;		
		
		/* **********  FINISH INSERT NEW ROWS IN FACT   ********** */		

		/* **********  START CLEAN UP  ********** */				
		CALL vectorwise(combine 'fact_purchase_requisition');					 
		
		DROP TABLE IF EXISTS tmp_insert_fct_prchs_rqstn;
		DROP TABLE IF EXISTS tmp_delete_fct_prchs_rqstn;	