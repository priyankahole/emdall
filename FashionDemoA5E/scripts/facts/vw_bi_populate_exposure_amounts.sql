
call vectorwise(combine 'fact_exposure');

update fact_exposure f
set amt_DailyCustExpBeyond = 0
WHERE amt_DailyCustExpBeyond IS NULL
OR amt_DailyCustExpBeyond <> 0;

update fact_exposure f
set amt_DailyCustExpCurrMth = 0
WHERE amt_DailyCustExpCurrMth IS NULL
OR amt_DailyCustExpCurrMth <> 0;

update fact_exposure f
set amt_DailyCustExpNextMth = 0
WHERE amt_DailyCustExpNextMth IS NULL
OR amt_DailyCustExpNextMth <> 0;

update fact_exposure f
set amt_DailyCustExposureAmt = 0
WHERE amt_DailyCustExposureAmt IS NULL
OR amt_DailyCustExposureAmt <> 0;

update fact_exposure f
set amt_DailyCustExpPrevMth = 0
WHERE amt_DailyCustExpPrevMth IS NULL
OR amt_DailyCustExpPrevMth <> 0;

update fact_exposure f
set amt_DailyCustExpWeek9 = 0
WHERE amt_DailyCustExpWeek9 IS NULL
OR amt_DailyCustExpWeek9 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek8 = 0
WHERE amt_DailyCustExpWeek8 IS NULL
OR amt_DailyCustExpWeek8 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek7 = 0
WHERE amt_DailyCustExpWeek7 IS NULL
OR amt_DailyCustExpWeek7 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek6 = 0
WHERE amt_DailyCustExpWeek6 IS NULL
OR amt_DailyCustExpWeek6 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek5 = 0
WHERE amt_DailyCustExpWeek5 IS NULL
OR amt_DailyCustExpWeek5 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek4 = 0
WHERE amt_DailyCustExpWeek4 IS NULL
OR amt_DailyCustExpWeek4 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek3 = 0
WHERE amt_DailyCustExpWeek3 IS NULL
OR amt_DailyCustExpWeek3 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek2 = 0
WHERE amt_DailyCustExpWeek2 IS NULL
OR amt_DailyCustExpWeek2 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek12 = 0
WHERE amt_DailyCustExpWeek12 IS NULL
OR amt_DailyCustExpWeek12 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek11 = 0
WHERE amt_DailyCustExpWeek11 IS NULL
OR amt_DailyCustExpWeek11 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek10 = 0
WHERE amt_DailyCustExpWeek10 IS NULL
OR amt_DailyCustExpWeek10 <> 0;

update fact_exposure f
set amt_DailyCustExpWeek1 = 0
WHERE amt_DailyCustExpWeek1 IS NULL
OR amt_DailyCustExpWeek1 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth9 = 0
WHERE amt_DailyCustExpMonth9 IS NULL
OR amt_DailyCustExpMonth9 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth8 = 0
WHERE amt_DailyCustExpMonth8 IS NULL
OR amt_DailyCustExpMonth8 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth7 = 0
WHERE amt_DailyCustExpMonth7 IS NULL
OR amt_DailyCustExpMonth7 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth6 = 0
WHERE amt_DailyCustExpMonth6 IS NULL
OR amt_DailyCustExpMonth6 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth5 = 0
WHERE amt_DailyCustExpMonth5 IS NULL
OR amt_DailyCustExpMonth5 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth4 = 0
WHERE amt_DailyCustExpMonth4 IS NULL
OR amt_DailyCustExpMonth4 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth12 = 0
WHERE amt_DailyCustExpMonth12 IS NULL
OR amt_DailyCustExpMonth12 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth11 = 0
WHERE amt_DailyCustExpMonth11 IS NULL
OR amt_DailyCustExpMonth11 <> 0;

update fact_exposure f
set amt_DailyCustExpMonth10 = 0
WHERE amt_DailyCustExpMonth10 IS NULL
OR amt_DailyCustExpMonth10 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek1 = 0
where f.amt_DailyCustExpWeek1 is null
 OR f.amt_DailyCustExpWeek1 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek2 = 0
where f.amt_DailyCustExpWeek2 is null
 OR f.amt_DailyCustExpWeek2 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek3 = 0
where f.amt_DailyCustExpWeek3 is null
 OR f.amt_DailyCustExpWeek3 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek4 = 0
where f.amt_DailyCustExpWeek4 is null
 OR f.amt_DailyCustExpWeek4 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek5 = 0
where f.amt_DailyCustExpWeek5 is null
 OR f.amt_DailyCustExpWeek5 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek6 = 0
where f.amt_DailyCustExpWeek6 is null
 OR f.amt_DailyCustExpWeek6 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek7 = 0
where f.amt_DailyCustExpWeek7 is null
 OR f.amt_DailyCustExpWeek7 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek8 = 0
where f.amt_DailyCustExpWeek8 is null
 OR f.amt_DailyCustExpWeek8 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek9 = 0
where f.amt_DailyCustExpWeek9 is null
 OR f.amt_DailyCustExpWeek9 <> 0;

update fact_exposure f
set f.amt_DailyCustExpWeek10 = 0
where f.amt_DailyCustExpWeek10 is null
 OR f.amt_DailyCustExpWeek10 <> 0;

 update fact_exposure f
set f.amt_DailyCustExpWeek11 = 0
where f.amt_DailyCustExpWeek11 is null
 OR f.amt_DailyCustExpWeek11 <> 0;

 update fact_exposure f
set f.amt_DailyCustExpWeek12 = 0
where f.amt_DailyCustExpWeek12 is null
 OR f.amt_DailyCustExpWeek12 <> 0;

call vectorwise(combine 'fact_exposure');

/* Max exposure overall	*/

  DROP TABLE IF EXISTS  fact_ar_exposure_amt;
  CREATE TABLE fact_ar_exposure_amt (
      Dim_CustomerId int,
      dim_DateIdNetDueDate int,
      dim_dateidactualgidate int,
      ExpAmt decimal(18,4)
    );

  insert into fact_ar_exposure_amt
  select f_ar0.dim_customerid, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0
  inner join dim_Date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where ndd.DateValue  = current_date
  and agi.datevalue <= ndd.Datevalue
  group by f_ar0.dim_customerid, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
   FROM fact_ar_exposure_amt t 
   set f.amt_DailyCustExposureAmt = t.ExpAmt
   WHERE f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
   AND f.dim_dateidactualgidate = t.dim_dateidactualgidate;
  

  DROP TABLE fact_ar_exposure_amt;

  /* # Max exposure previous month	*/

  CREATE TABLE fact_ar_exposure_amt
  AS
  select f_ar0.dim_customerid, f_ar0.dim_dateidactualgidate,f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt 
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
    inner join dim_date agi on agi.dim_dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_dateidactualgidate <> 1 AND f_ar0.dim_DateIdNetDueDate <> 1
  and agi.datevalue <= ndd.datevalue
  and (current_date - ndd.datevalue) between 0 and 27
  group by f_ar0.dim_customerid, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpPrevMth = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate;

call vectorwise(combine 'fact_exposure');
  
  DROP TABLE fact_ar_exposure_amt;

  /* # Max exposure current month	*/

   CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' AND f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '27' DAY)) )
  and ndd.DateValue >= ( current_date + INTERVAL '27' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '27' DAY)))
  and ndd.DateValue between (current_date + INTERVAL '0' DAY) and (current_date + INTERVAL '27' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpCurrMth = t.ExpAmt
  WHERE f.Dim_CustomerId = t.Dim_CustomerId 
  and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;
  
  DROP TABLE fact_ar_exposure_amt;

   CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND  agi.DateValue <= (current_date + INTERVAL '55' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '55' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
   and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '55' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '28' DAY) and (current_date + INTERVAL '55' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

call vectorwise(combine 'fact_exposure');

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpNextMth = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
    AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber
    AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '83' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '83' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '83' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '56' DAY) and (current_date + INTERVAL '83' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
    set f.amt_DailyCustExpBeyond = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
    AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;

/*  # Max exposure beyond next month	*/
/* LK: Incorrect comment. This is 'max exposure within next week' */

CREATE TABLE fact_ar_exposure_amt
  AS	
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '6' DAY) ))
  and ndd.DateValue >= (current_date + INTERVAL '6' DAY)
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '6' DAY) ))
  and ndd.DateValue between current_date and (current_date + INTERVAL '6' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek1 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
  AND f.amt_DailyCustExpWeek1 <> t.ExpAmt
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;


  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '13' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '13' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '13' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '7' DAY) and (current_date + INTERVAL '13' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek2 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');
 
  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '20' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '20' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '20' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '14' DAY) and (current_date + INTERVAL '20' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set  f.amt_DailyCustExpWeek3 = t.ExpAmt 
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;


  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '27' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '27' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo, f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
    and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '27' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '21' DAY) and (current_date + INTERVAL '27' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM  fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek4 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;
  
  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '34' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '34' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '34' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '28' DAY) and (current_date + INTERVAL '34' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek5 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;
  
 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '41' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '41' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '41' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '35' DAY) and (current_date + INTERVAL '41' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM  fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek6 = t.ExpAmt
  WHERE f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

  DROP TABLE fact_ar_exposure_amt;
  
 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '48' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '48' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '48' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '42' DAY) and (current_date + INTERVAL '48' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek7 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;

 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '55' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '55' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '55' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '49' DAY) and (current_date + INTERVAL '55' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM   fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek8 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

  DROP TABLE fact_ar_exposure_amt;

 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '62' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '62' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '62' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '56' DAY) and (current_date + INTERVAL '62' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM  fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek9 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;

 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '69' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '69' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '69' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '63' DAY) and (current_date + INTERVAL '69' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek10 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

  DROP TABLE fact_ar_exposure_amt;
  
 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '76' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '76' DAY)
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '76' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '70' DAY) and (current_date + INTERVAL '76' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM  fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek11 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;
 
 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '83' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '83' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '83' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '77' DAY) and (current_date + INTERVAL '83' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpWeek12 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '111' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '111' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '111' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '84' DAY) and (current_date + INTERVAL '111' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth4 = t.ExpAmt
  where  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');
 
  DROP TABLE fact_ar_exposure_amt;

/*   # Max exposure beyond next month	*/

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '139' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '139' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '139' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '112' DAY) and (current_date + INTERVAL '139' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth5 = t.ExpAmt
  where  f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
     AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;
  
  DROP TABLE fact_ar_exposure_amt;
  
/*   # Max exposure beyond next month */
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '167' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '167' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '167' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '140' DAY) and (current_date + INTERVAL '167' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth6 = t.ExpAmt
  where f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
   AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');
  
  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '195' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '195' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '195' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '168' DAY) and (current_date + INTERVAL '195' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth7 = t.ExpAmt
  where f.Dim_CustomerId = t.Dim_CustomerId 
  and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
   AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND  agi.DateValue <= (current_date + INTERVAL '223' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '223' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '223' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '196' DAY) and (current_date + INTERVAL '223' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth8 = t.ExpAmt
  where  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
   AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;


  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '251' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '251' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '251' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '224' DAY) and (current_date + INTERVAL '251' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth9 = t.ExpAmt
  where f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
     AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '279' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '279' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '279' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '252' DAY) and (current_date + INTERVAL '279' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth10 = t.ExpAmt
  where f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
     AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

  DROP TABLE fact_ar_exposure_amt;

CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '307' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '307' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '307' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '280' DAY) and (current_date + INTERVAL '307' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth11 = t.ExpAmt
  WHERE f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
     AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

  DROP TABLE fact_ar_exposure_amt;

CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where  f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '335' DAY) ))
  and ndd.DateValue >= ( current_date + INTERVAL '335' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate
  UNION
  select f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_dateidactualgidate
  where f_ar0.dim_DateIdNetDueDate <> 1
  and (f_ar0.dd_DueType = 'AR' OR (f_ar0.dd_DueType = 'Sales' and f_ar0.dim_dateidactualgidate <> 1 AND agi.DateValue <= (current_date + INTERVAL '335' DAY) ))
  and ndd.DateValue between (current_date + INTERVAL '308' DAY) and (current_date + INTERVAL '335' DAY) 
  group by f_ar0.dd_SalesDocNo,f_ar0.dd_SalesItemNo,f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dim_companyid,f_ar0.dim_customerid,
  f_ar0.dd_AssignmentNumber, f_ar0.dim_dateidactualgidate, f_ar0.dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCustExpMonth12 = t.ExpAmt
  WHERE f.Dim_CustomerId = t.Dim_CustomerId and f.dim_DateIdNetDueDate = t.dim_DateIdNetDueDate
  AND f.dim_dateidactualgidate = t.dim_dateidactualgidate
    AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
   AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
   AND f.dim_companyid = t.dim_companyid
   AND f.dim_customerid = t.dim_customerid
   AND f.dd_AssignmentNumber = t.dd_AssignmentNumber  
   AND f.dd_SalesDocNo = t.dd_SalesDocNo
   AND f.dd_SalesItemNo = t.dd_SalesItemNo;

call vectorwise(combine 'fact_exposure');

DROP TABLE IF EXISTS fact_ar_exposure_amt;

CREATE TABLE fact_ar_exposure_amt
(
   Dim_CustomerId   int,
   WeekNo           int,
   DateValueMax	    ansidate,
   MaxExpAmt        decimal(18, 4)
);

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 1, current_Date as DateValueMax,SUM(f_ar0.amt_DailyCustExpWeek1) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 2, (current_Date  + INTERVAL '7' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek2) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 3, (current_Date  + INTERVAL '14' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek3) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 4, (current_Date  + INTERVAL '21' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek4) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 5, (current_Date  + INTERVAL '28' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek5) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 6, (current_Date  + INTERVAL '35' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek6) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 7, (current_Date  + INTERVAL '42' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek7) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 8, (current_Date  + INTERVAL '49' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek8) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 9, (current_Date  + INTERVAL '56' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek9) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 10, (current_Date  + INTERVAL '63' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek10) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 11, (current_Date  + INTERVAL '70' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek11) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

INSERT INTO fact_ar_exposure_amt
   SELECT f_ar0.dim_customerid, 12, (current_Date  + INTERVAL '77' DAY) as DateValueMax, SUM(f_ar0.amt_DailyCustExpWeek12) ExpAmt
     FROM fact_exposure f_ar0
   GROUP BY f_ar0.dim_customerid;

call vectorwise (combine 'fact_ar_exposure_amt');


DROP TABLE IF EXISTS tmp_Rank_MaxExposure;

CREATE TABLE tmp_rank_maxexposure
AS
   SELECT DISTINCT dim_Customerid,
                   WeekNo,
		   DateValueMax,
                   MaxExpAmt,
                   RANK() OVER
 (PARTITION BY dim_Customerid ORDER BY MaxExpAmt DESC, WeekNo ASC) AS Rank1
 FROM fact_ar_exposure_amt;

DELETE FROM tmp_rank_maxexposure
 WHERE RANK1 <> 1;

  UPDATE fact_exposure f
    FROM tmp_rank_maxexposure t, Dim_Company  c1,Dim_Date dt
     set amt_MaxExposure = MaxExpAmt,
         dd_MaxExposureWeek = weekNo,
	 f.dim_DatIdMaxExposure = dt.dim_DateId
  WHERE t.dim_CustomerId = f.dim_CustomerId
    AND c1.Dim_CompanyId = f.Dim_CompanyId
    AND c1.CompanyCode = dt.CompanyCode
    AND dt.datevalue = t.DateValueMax;

DROP TABLE IF EXISTS tmp_Rank_MaxExposure;

DROP TABLE IF EXISTS fact_ar_exposure_amt;

call vectorwise(combine 'fact_exposure');

DROP TABLE IF EXISTS tmp_Rank_MaxExposure;

CREATE TABLE tmp_rank_maxexposure
(
Dim_customerid integer,
ExposureAmt decimal(18,4) not null with default);

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpCurrMth) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpNextMth) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpBeyond) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth4) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth5) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth6) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth7) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth8) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth9) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth10) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth11) from fact_exposure
group by dim_customerid;

INSERT INTO tmp_rank_maxexposure
SELECT dim_customerid, sum(amt_DailyCustExpMonth12) from fact_exposure
group by dim_customerid;

UPDATE fact_exposure f
   SET amt_MaxMonthExposure =
          (SELECT max(exposureamt)
             FROM tmp_rank_maxexposure t
            WHERE f.dim_Customerid = t.dim_CustomerId
           GROUP BY t.dim_CustomerId);
	   
DROP TABLE IF EXISTS tmp_rank_maxexposure;


UPDATE fact_exposure f
SET dim_DatIdMaxExposure = 1
WHERE dim_DatIdMaxExposure  IS NULL;

UPDATE fact_exposure f
SET dd_MaxExposureWeek = 0
WHERE dd_MaxExposureWeek  IS NULL;

UPDATE fact_exposure f
SET amt_MaxExposure = 0
WHERE amt_MaxExposure  IS NULL;
call vectorwise (combine 'fact_exposure');