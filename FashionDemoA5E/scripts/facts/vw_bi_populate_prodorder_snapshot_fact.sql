/**********************************************************************************/
/*  17 Dec 2014   1.2	      Lokesh    Do not capture data with schedule finish date in last week. Added query to delete data for rerun of current snapshot */
/*  29 Oct 2014   1.0	      Lokesh    New script for new Merck Subj Area - Prod Order Snapshot */
/**********************************************************************************/

/* Store current date in beginning - to handle the scenario of date changing during script run */
DROP TABLE IF EXISTS tmp_current_date_fprsnapshot;
CREATE TABLE tmp_current_date_fprsnapshot
AS
SELECT current_date currdate;

/* If today's snapshot is being rerun, delete existing data */
DELETE FROM fact_productionorder_snapshot WHERE dd_snapshotdate = current_date;


/* Part 1 - Update  existing rows - only non-orig columns */
/* For all existing ord-items in snapshot table, update the values(of non-orig cols) to current values */
/* Note that orig columns won't change */

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET dim_dateidscheduledfinish = fpr.dim_dateidscheduledfinish
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber 
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dim_dateidscheduledfinish <> fpr.dim_dateidscheduledfinish;


UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET ct_totalorderqty = fpr.ct_totalorderqty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_totalorderqty <> fpr.ct_totalorderqty;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET dd_cancelledorder = fpr.dd_cancelledorder
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dd_cancelledorder <> fpr.dd_cancelledorder;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET ct_underdeliverytol_merck = fpr.ct_underdeliverytol_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_underdeliverytol_merck <> fpr.ct_underdeliverytol_merck;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET ct_overdeliverytol_merck = fpr.ct_overdeliverytol_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_overdeliverytol_merck <> fpr.ct_overdeliverytol_merck;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET dd_unimitedoverdelivery_merck = fpr.dd_unimitedoverdelivery_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dd_unimitedoverdelivery_merck <> fpr.dd_unimitedoverdelivery_merck;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET ct_GRQty = fpr.ct_GRQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_GRQty <> fpr.ct_GRQty;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET ct_OrderItemQty = fpr.ct_OrderItemQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_OrderItemQty <> fpr.ct_OrderItemQty;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET s.dim_dateidactualitemfinish = fpr.dim_dateidactualitemfinish
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dim_dateidactualitemfinish <> fpr.dim_dateidactualitemfinish;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET s.dim_dateidactualheaderfinishdate_merck = fpr.dim_dateidactualheaderfinishdate_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dim_dateidactualheaderfinishdate_merck <> fpr.dim_dateidactualheaderfinishdate_merck;

UPDATE fact_productionorder_snapshot s
FROM fact_productionorder fpr
SET s.ct_GRQty = fpr.ct_GRQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_GRQty <> fpr.ct_GRQty;

/* Part 2 - Insert new rows */
/* For a given ord-item with scheduled finish date in next week, there are 2 scenarios possible :
A. The given ord-item is already present in fact_productionorder_snapshot. So, update the original fields from this table ( latest row for that ord-item )
B. The doc-item is new. So, in this case the original values will be the same as current values */

/* First capture the latest row from snapshot subj area for each order/item */
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_maxdate_1;
CREATE TABLE tmp_fact_productionorder_snapshot_maxdate_1
AS
SELECT dd_ordernumber, dd_orderitemno, max(dd_snapshotdate) max_dd_snapshotdate
FROM fact_productionorder_snapshot
GROUP BY dd_ordernumber, dd_orderitemno;

DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_maxdate;
CREATE TABLE tmp_fact_productionorder_snapshot_maxdate
AS
SELECT f.*
FROM fact_productionorder_snapshot f, tmp_fact_productionorder_snapshot_maxdate_1 m
WHERE f.dd_ordernumber = m.dd_ordernumber AND f.dd_orderitemno = m.dd_orderitemno AND f.dd_snapshotdate = m.max_dd_snapshotdate;


/* Insert new order-item */
/* Only open production orders with due dates in the next 7 days */
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_ins;
CREATE TABLE tmp_fact_productionorder_snapshot_ins
AS
SELECT DISTINCT dd_ordernumber, dd_orderitemno
FROM fact_productionorder pr, dim_date d, tmp_current_date_fprsnapshot
WHERE pr.dim_dateidscheduledfinish = d.dim_dateid
AND d.datevalue > currdate
AND d.datevalue <= currdate + INTERVAL '7' DAY
AND dim_dateidactualitemfinish = 1;      	/* Ensure that its not closed */

/* ALL production orders with due dates in the past 7 days (cancelled, closed or open) */
/*INSERT INTO tmp_fact_productionorder_snapshot_ins
SELECT DISTINCT dd_ordernumber, dd_orderitemno
FROM fact_productionorder pr, dim_date d, tmp_current_date_fprsnapshot
WHERE pr.dim_dateidscheduledfinish = d.dim_dateid
AND d.datevalue > currdate - INTERVAL '7' DAY
AND  d.datevalue <= currdate*/

DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_del;
CREATE TABLE tmp_fact_productionorder_snapshot_del
AS
SELECT DISTINCT dd_ordernumber, dd_orderitemno
FROM fact_productionorder_snapshot;

DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_upd;
CREATE TABLE tmp_fact_productionorder_snapshot_upd
AS
SELECT distinct i.*
FROM tmp_fact_productionorder_snapshot_ins i, tmp_fact_productionorder_snapshot_del d
WHERE i.dd_ordernumber = d.dd_ordernumber 
AND i.dd_orderitemno = d.dd_orderitemno;

/* So, wherever there's existing row and there's an update, mark dd_latestsnapshotflag as N */
UPDATE fact_productionorder_snapshot s
FROM tmp_fact_productionorder_snapshot_upd u
SET dd_latestsnapshotflag = 'N'
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_ordernumber = u.dd_ordernumber AND s.dd_orderitemno = u.dd_orderitemno
AND s.dd_latestsnapshotflag <> 'N';

CALL VECTORWISE(COMBINE 'tmp_fact_productionorder_snapshot_ins-tmp_fact_productionorder_snapshot_del');

/* For new inserts, pick up directly from production order */
INSERT INTO fact_productionorder_snapshot(
	fact_productionorderid,
	Dim_DateidRelease,
	Dim_DateidBOMExplosion,
	Dim_DateidLastScheduling,
	Dim_DateidTechnicalCompletion,
	Dim_DateidBasicStart,
	Dim_DateidScheduledRelease,
	Dim_DateidScheduledFinish,
	Dim_DateidScheduledStart,
	Dim_DateidActualStart,
	Dim_DateidConfirmedOrderFinish,
	Dim_DateidActualHeaderFinish,
	Dim_DateidActualHeaderFinishDate_merck,
	Dim_DateidActualRelease,
	Dim_DateidActualItemFinish,
	Dim_DateidPlannedOrderDelivery,
	Dim_DateidRoutingTransfer,
	Dim_DateidBasicFinish,
	Dim_ControllingAreaid,
	Dim_ProfitCenterId,
	Dim_tasklisttypeid,
	Dim_PartidHeader,
	Dim_bomstatusid,
	Dim_bomusageid,
	dim_productionschedulerid,
	Dim_ProcurementID,
	Dim_SpecialProcurementID,
	Dim_UnitOfMeasureid,
	Dim_PartidItem,
	Dim_AccountCategoryid,
	Dim_StockTypeid,
	Dim_StorageLocationid,
	Dim_Plantid,
	dim_specialstockid,
	Dim_ConsumptionTypeid,
	Dim_SalesOrgid,
	Dim_PurchaseOrgid,
	Dim_Currencyid,
	Dim_Companyid,
	Dim_ordertypeid,
	dim_productionorderstatusid,
	dim_productionordermiscid,
	Dim_BuildTypeid,
	dd_ordernumber,
	dd_orderitemno,
	dd_bomexplosionno,
	dd_plannedorderno,
	dd_SalesOrderNo,
	dd_SalesOrderItemNo,
	dd_SalesOrderDeliveryScheduleNo,
	ct_ConfirmedReworkQty,
	ct_ScrapQty,
	ct_OrderItemQty,
	ct_TotalOrderQty,
	ct_GRQty,
	ct_GRProcessingTime,
	amt_estimatedTotalCost,
	amt_ValueGR,
	dd_BOMLevel,
	dd_sequenceno,
	dd_ObjectNumber,
	ct_ConfirmedScrapQty,
	dd_OrderDescription,
	Dim_MrpControllerId,
	dim_currencyid_TRA,
	dim_currencyid_GBL,
	dd_OperationNumber,
	dd_WorkCenter,
	dim_customerid,
	dd_RoutingOperationNo,
	dd_snapshotdate,
	dim_dateidscheduledfinish_orig_merck,
	ct_totalorderqty_orig_merck,
	dd_cancelledorder_orig_merck,
	ct_underdeliverytol_orig_merck,
	ct_overdeliverytol_orig_merck,
	dd_unimitedoverdelivery_orig_merck,
	ct_GRQty_orig_merck,
	ct_OrderItemQty_orig_merck,
	/* For new inserts orig and non-orig values are same */
	ct_underdeliverytol_merck,
	ct_overdeliverytol_merck,
	dd_unimitedoverdelivery_merck,
	ct_snapshotcount,
	dd_latestsnapshotflag)

SELECT
	f.fact_productionorderid,
	f.Dim_DateidRelease,
	f.Dim_DateidBOMExplosion,
	f.Dim_DateidLastScheduling,
	f.Dim_DateidTechnicalCompletion,
	f.Dim_DateidBasicStart,
	f.Dim_DateidScheduledRelease,
	f.Dim_DateidScheduledFinish,
	f.Dim_DateidScheduledStart,
	f.Dim_DateidActualStart,
	f.Dim_DateidConfirmedOrderFinish,
	f.Dim_DateidActualHeaderFinish,
	f.Dim_DateidActualHeaderFinishDate_merck,
	f.Dim_DateidActualRelease,
	f.Dim_DateidActualItemFinish,
	f.Dim_DateidPlannedOrderDelivery,
	f.Dim_DateidRoutingTransfer,
	f.Dim_DateidBasicFinish,
	f.Dim_ControllingAreaid,
	f.Dim_ProfitCenterId,
	f.Dim_tasklisttypeid,
	f.Dim_PartidHeader,
	f.Dim_bomstatusid,
	f.Dim_bomusageid,
	f.dim_productionschedulerid,
	f.Dim_ProcurementID,
	f.Dim_SpecialProcurementID,
	f.Dim_UnitOfMeasureid,
	f.Dim_PartidItem,
	f.Dim_AccountCategoryid,
	f.Dim_StockTypeid,
	f.Dim_StorageLocationid,
	f.Dim_Plantid,
	f.dim_specialstockid,
	f.Dim_ConsumptionTypeid,
	f.Dim_SalesOrgid,
	f.Dim_PurchaseOrgid,
	f.Dim_Currencyid,
	f.Dim_Companyid,
	f.Dim_ordertypeid,
	f.dim_productionorderstatusid,
	f.dim_productionordermiscid,
	f.Dim_BuildTypeid,
	f.dd_ordernumber,
	f.dd_orderitemno,
	f.dd_bomexplosionno,
	f.dd_plannedorderno,
	f.dd_SalesOrderNo,
	f.dd_SalesOrderItemNo,
	f.dd_SalesOrderDeliveryScheduleNo,
	f.ct_ConfirmedReworkQty,
	f.ct_ScrapQty,
	f.ct_OrderItemQty,
	f.ct_TotalOrderQty,
	f.ct_GRQty,
	f.ct_GRProcessingTime,
	f.amt_estimatedTotalCost,
	f.amt_ValueGR,
	f.dd_BOMLevel,
	f.dd_sequenceno,
	f.dd_ObjectNumber,
	f.ct_ConfirmedScrapQty,
	f.dd_OrderDescription,
	f.Dim_MrpControllerId,
	f.dim_currencyid_TRA,
	f.dim_currencyid_GBL,
	f.dd_OperationNumber,
	f.dd_WorkCenter,
	dim_customerid,
	dd_RoutingOperationNo,
	currdate, f.dim_dateidscheduledfinish,
	f.ct_totalorderqty,f.dd_cancelledorder,f.ct_underdeliverytol_merck,f.ct_overdeliverytol_merck,
	f.dd_unimitedoverdelivery_merck,
	f.ct_GRQty,f.ct_OrderItemQty,
	f.ct_underdeliverytol_merck,f.ct_overdeliverytol_merck,
	f.dd_unimitedoverdelivery_merck,
	1,
	'Y'
FROM tmp_fact_productionorder_snapshot_ins i,fact_productionorder f, tmp_current_date_fprsnapshot
WHERE i.dd_ordernumber = f.dd_ordernumber AND i.dd_orderitemno = f.dd_orderitemno;

/* For existing ord-item, pick up orig fields from latest prod-order snapshot for that ord-item*/
/* Ensure that only those are picked up that have the latest schedule finish date in next week */
INSERT INTO fact_productionorder_snapshot(
	fact_productionorderid,
	Dim_DateidRelease,
	Dim_DateidBOMExplosion,
	Dim_DateidLastScheduling,
	Dim_DateidTechnicalCompletion,
	Dim_DateidBasicStart,
	Dim_DateidScheduledRelease,
	Dim_DateidScheduledFinish,
	Dim_DateidScheduledStart,
	Dim_DateidActualStart,
	Dim_DateidConfirmedOrderFinish,
	Dim_DateidActualHeaderFinish,
	Dim_DateidActualHeaderFinishDate_merck,
	Dim_DateidActualRelease,
	Dim_DateidActualItemFinish,
	Dim_DateidPlannedOrderDelivery,
	Dim_DateidRoutingTransfer,
	Dim_DateidBasicFinish,
	Dim_ControllingAreaid,
	Dim_ProfitCenterId,
	Dim_tasklisttypeid,
	Dim_PartidHeader,
	Dim_bomstatusid,
	Dim_bomusageid,
	dim_productionschedulerid,
	Dim_ProcurementID,
	Dim_SpecialProcurementID,
	Dim_UnitOfMeasureid,
	Dim_PartidItem,
	Dim_AccountCategoryid,
	Dim_StockTypeid,
	Dim_StorageLocationid,
	Dim_Plantid,
	dim_specialstockid,
	Dim_ConsumptionTypeid,
	Dim_SalesOrgid,
	Dim_PurchaseOrgid,
	Dim_Currencyid,
	Dim_Companyid,
	Dim_ordertypeid,
	dim_productionorderstatusid,
	dim_productionordermiscid,
	Dim_BuildTypeid,
	dd_ordernumber,
	dd_orderitemno,
	dd_bomexplosionno,
	dd_plannedorderno,
	dd_SalesOrderNo,
	dd_SalesOrderItemNo,
	dd_SalesOrderDeliveryScheduleNo,
	ct_ConfirmedReworkQty,
	ct_ScrapQty,
	ct_OrderItemQty,
	ct_TotalOrderQty,
	ct_GRQty,
	ct_GRProcessingTime,
	amt_estimatedTotalCost,
	amt_ValueGR,
	dd_BOMLevel,
	dd_sequenceno,
	dd_ObjectNumber,
	ct_ConfirmedScrapQty,
	dd_OrderDescription,
	Dim_MrpControllerId,
	dim_currencyid_TRA,
	dim_currencyid_GBL,
	dd_OperationNumber,
	dd_WorkCenter,
	dim_customerid,
	dd_RoutingOperationNo,
	dd_snapshotdate,
	dim_dateidscheduledfinish_orig_merck,
	ct_totalorderqty_orig_merck,
	dd_cancelledorder_orig_merck,
	ct_underdeliverytol_orig_merck,
	ct_overdeliverytol_orig_merck,
	dd_unimitedoverdelivery_orig_merck,
	ct_GRQty_orig_merck,
	ct_OrderItemQty_orig_merck,
	dd_cancelledorder,
	ct_underdeliverytol_merck,
	ct_overdeliverytol_merck,
	dd_unimitedoverdelivery_merck,
	ct_snapshotcount,
	dd_latestsnapshotflag)
SELECT 
	fpr.fact_productionorderid,
	fpr.Dim_DateidRelease,
	fpr.Dim_DateidBOMExplosion,
	fpr.Dim_DateidLastScheduling,
	fpr.Dim_DateidTechnicalCompletion,
	fpr.Dim_DateidBasicStart,
	fpr.Dim_DateidScheduledRelease,
	fpr.Dim_DateidScheduledFinish,
	fpr.Dim_DateidScheduledStart,
	fpr.Dim_DateidActualStart,
	fpr.Dim_DateidConfirmedOrderFinish,
	fpr.Dim_DateidActualHeaderFinish,
	fpr.Dim_DateidActualHeaderFinishDate_merck,
	fpr.Dim_DateidActualRelease,
	fpr.Dim_DateidActualItemFinish,
	fpr.Dim_DateidPlannedOrderDelivery,
	fpr.Dim_DateidRoutingTransfer,
	fpr.Dim_DateidBasicFinish,
	fpr.Dim_ControllingAreaid,
	fpr.Dim_ProfitCenterId,
	fpr.Dim_tasklisttypeid,
	fpr.Dim_PartidHeader,
	fpr.Dim_bomstatusid,
	fpr.Dim_bomusageid,
	fpr.dim_productionschedulerid,
	fpr.Dim_ProcurementID,
	fpr.Dim_SpecialProcurementID,
	fpr.Dim_UnitOfMeasureid,
	fpr.Dim_PartidItem,
	fpr.Dim_AccountCategoryid,
	fpr.Dim_StockTypeid,
	fpr.Dim_StorageLocationid,
	fpr.Dim_Plantid,
	fpr.dim_specialstockid,
	fpr.Dim_ConsumptionTypeid,
	fpr.Dim_SalesOrgid,
	fpr.Dim_PurchaseOrgid,
	fpr.Dim_Currencyid,
	fpr.Dim_Companyid,
	fpr.Dim_ordertypeid,
	fpr.dim_productionorderstatusid,
	fpr.dim_productionordermiscid,
	fpr.Dim_BuildTypeid,
	fpr.dd_ordernumber,
	fpr.dd_orderitemno,
	fpr.dd_bomexplosionno,
	fpr.dd_plannedorderno,
	fpr.dd_SalesOrderNo,
	fpr.dd_SalesOrderItemNo,
	fpr.dd_SalesOrderDeliveryScheduleNo,
	fpr.ct_ConfirmedReworkQty,
	fpr.ct_ScrapQty,
	fpr.ct_OrderItemQty,
	fpr.ct_TotalOrderQty,
	fpr.ct_GRQty,
	fpr.ct_GRProcessingTime,
	fpr.amt_estimatedTotalCost,
	fpr.amt_ValueGR,
	fpr.dd_BOMLevel,
	fpr.dd_sequenceno,
	fpr.dd_ObjectNumber,
	fpr.ct_ConfirmedScrapQty,
	fpr.dd_OrderDescription,
	fpr.Dim_MrpControllerId,
	fpr.dim_currencyid_TRA,
	fpr.dim_currencyid_GBL,
	fpr.dd_OperationNumber,
	fpr.dd_WorkCenter,
	fpr.dim_customerid,
	fpr.dd_RoutingOperationNo,
	currdate,
	s.dim_dateidscheduledfinish,
	s.ct_totalorderqty,
	s.dd_cancelledorder,
	s.ct_underdeliverytol_merck,
	s.ct_overdeliverytol_merck,
	s.dd_unimitedoverdelivery_merck,
	s.ct_GRQty,
	s.ct_OrderItemQty,
	fpr.dd_cancelledorder,
	fpr.ct_underdeliverytol_merck,
	fpr.ct_overdeliverytol_merck,
	fpr.dd_unimitedoverdelivery_merck,
	s.ct_snapshotcount + 1,
	'Y'	
FROM 	fact_productionorder fpr, tmp_current_date_fprsnapshot, tmp_fact_productionorder_snapshot_maxdate s,
	tmp_fact_productionorder_snapshot_upd u
WHERE s.dd_ordernumber = fpr.dd_ordernumber AND s.dd_orderitemno = fpr.dd_orderitemno
AND u.dd_ordernumber = s.dd_ordernumber AND u.dd_orderitemno = s.dd_orderitemno;

/* Update dim_dateidsnapshot */
UPDATE fact_productionorder_snapshot s
FROM dim_date dt,dim_company dc
SET s.dim_dateidsnapshot = dt.dim_dateid
WHERE s.dd_snapshotdate = dt.datevalue
AND s.dim_companyid = dc.dim_companyid AND dc.companycode = dt.CompanyCode
AND s.dim_dateidsnapshot <> dt.dim_dateid;


DROP TABLE IF EXISTS tmp_current_date_fprsnapshot;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_maxdate_1;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_maxdate;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_ins;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_del;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_upd;

