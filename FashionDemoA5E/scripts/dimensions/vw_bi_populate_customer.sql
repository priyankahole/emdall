/* ##################################################################################################################

   Script         : bi_vw_populate_customer 
   Author         : AUngureanu
   Created On     : 22 09 2014


   Description    : Combine 3 script from Application Manager for populating DIM_CUSTOMER

   Change History
   Date            By        	Version           Desc
   22 09 2014     AUngureanu    1.0               Combine 3 script in one file
   Dec 2014	  SVinayan	1.1		  Added additional fields as a part of Columbia Report 856
   
#################################################################################################################### */

insert into dim_customer
(Dim_CustomerID,RowChangeReason,RowEndDate,RowIsCurrent,RowStartDate,Industry,CustomerNumber,Country,Name,
City,PostalCode,Region,Plant,OneTime,IndustryCode,Classification,ClassificationDescription,CreationDate,CreditRep,CreditLimit,
RiskClass,CustAccountGroup,RegionDescription,CustAccountGroupDescription,Name2,HighCustomerNumber,HigherCustomerNumber,
TopLevelCustomerNumber,HierarchyType,HighCustomerDesc,HigherCustomerDesc,TopLevelCustomerDesc,CountryName,Channel,SubChannel,
ChannelDescription,SubChannelDescription,TradingPartner,Attribute3,Attribute4,Attribute5,Attribute6,Attribute7,Attribute8,ConditionGroup1,
ConditionGroup2,IndustryCode1,ConditionGroupDescription2,IndustryCode1Description
,custBillingBlock,dataLine,deliveryBlock,deletionFlag,faxNumber,languageKey  /* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
,orderBlock,searchTerm,telephone1,transportationZone,brandDescription,transportationZoneDesc,addressnumber /* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
)
SELECT
1,null,null,1,current_timestamp,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set',null,'Not Set',0.00,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set'
,'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', /* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
'Not Set', 'Not Set', 'Not Set', 'Not Set','Not Set','Not Set','Not Set' /* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
FROM (SELECT 1) a
where not exists(select 1 from dim_customer where dim_customerid = 1);

/*  UPDATE PART  */
/* Start Of Update Customer Transfer Interface*/
					   
UPDATE dim_customer dc FROM kna1 kn, t005t      
   SET dc.IndustryCode = ifnull(KNA1_BRSCH, 'Not Set'),
       dc.Industry =
          ifnull((SELECT T5UNT_NTEXT
                    FROM T5UNT
                   WHERE T5UNT_NAICS = KNA1_BRSCH),
                 'Not Set'),
       dc.Country = ifnull(KNA1_LAND1, 'Not Set'),
       dc.Name = ifnull(KNA1_NAME1, 'Not Set'),
	   dc.Name2 = ifnull(KNA1_NAME2, 'Not Set'),	   
       dc.City = ifnull(KNA1_ORT01, 'Not Set'),
       dc.PostalCode = ifnull(KNA1_PSTLZ, 'Not Set'),
       dc.Region = ifnull(KNA1_REGIO, 'Not Set'),
       dc.OneTime =(CASE WHEN ifnull(KNA1_XCPDK, '') = 'X' THEN 'Yes' ELSE 'No' END),
       dc.Classification = ifnull(KNA1_KUKLA,'Not Set'),
       dc.ClassificationDescription = ifnull((SELECT t.TKUKT_VTEXT FROM TKUKT t
                  WHERE t.TKUKT_KUKLA = KNA1_KUKLA), 'Not Set'),
       dc.CreationDate = KNA1_ERDAT,
	dc.CustAccountGroup = ifnull(KNA1_KTOKD,'Not Set'),
       dc.CustAccountGroupDescription = ifnull((SELECT t.T077X_TXT30 FROM T077X t
                  WHERE t.T077X_KTOKD = KNA1_KTOKD AND T.T077X_SPRAS = 'E'), 'Not Set'),
	dc.RegionDescription = 	 ifnull((SELECT T005U_BEZEI
		 FROM T005U
		 WHERE T005U_LAND1 = KNA1_LAND1
                  AND T005U_BLAND = KNA1_REGIO),'Not Set'),
       dc.CountryName = ifnull(t005t_landx, 'Not Set'),
       dc.Channel = ifnull(KNA1_KATR1,'Not Set'),
       dc.SubChannel = ifnull(KNA1_KATR2,'Not Set'),
       dc.ChannelDescription = ifnull((SELECT t.TVK1T_VTEXT FROM TVK1T t
                WHERE t.TVK1T_KATR1 = KNA1_KATR1),'Not Set'),
       dc.SubChannelDescription = ifnull((SELECT t.TVK2T_VTEXT FROM TVK2T t
                WHERE t.TVK2T_KATR2 = KNA1_KATR2),'Not Set'),
	dc.TradingPartner = ifnull(KNA1_VBUND,'Not Set'),
        dc.Attribute3 = ifnull(KNA1_KATR3,'Not Set'),
	dc.Attribute4 = ifnull(KNA1_KATR4,'Not Set'),
	dc.Attribute5 = ifnull(KNA1_KATR5,'Not Set'),
        dc.Attribute6 = ifnull(KNA1_KATR6,'Not Set'),
	dc.Attribute7 = ifnull(KNA1_KATR7,'Not Set'),
	dc.Attribute8 = ifnull(KNA1_KATR8,'Not Set'),
	dc.ConditionGroup1 = ifnull(KNA1_KDKG1,'Not Set'),
	dc.ConditionGroup2 = ifnull(KNA1_KDKG2,'Not Set'),
	dc.IndustryCode1 = ifnull(KNA1_BRAN1,'Not Set'),
	dc.ConditionGroupDescription2 = ifnull ((SELECT TVKGGT_VTEXT FROM TVKGGT where TVKGGT_KDKGR = kn.KNA1_KDKG2),'Not Set'),
	dc.IndustryCode1Description = ifnull ((SELECT TBRCT_VTEXT FROM TBRCT where TBRCT_BRACO = kn.KNA1_BRAN1),'Not Set'),
        dc.Address = ifnull(KNA1_STRAS,'Not Set'),
	dc.paymethod = ifnull((select KNB1_ZWELS from kna1_knb1 t1 where t1.KNA1_KUNNR = kn.kna1_kunnr) ,'Not Set'),
    	dc.paymethoddescription = ifnull((select T042Z_TEXT1 
					from 	kna1_knb1 t1, T042Z t2
					where	t1.KNB1_ZWELS = t2.T042Z_ZLSCH
					and t1.KNA1_KUNNR = kn.kna1_kunnr),'Not Set') 
	,dc.custBillingBlock = ifnull(KNA1_FAKSD, 'Not Set') 	/* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
	,dc.dataLine = ifnull(KNA1_DATLT, 'Not Set') 
	,dc.deliveryBlock = ifnull(KNA1_LIFSD, 'Not Set') 
	,dc.deletionFlag = ifnull(KNA1_LOEVM, 'Not Set') 
	,dc.faxNumber = ifnull(KNA1_TELFX, 'Not Set') 
	,dc.languageKey = ifnull(KNA1_SPRAS, 'Not Set') 
	,dc.orderBlock = ifnull(KNA1_AUFSD, 'Not Set') 
	,dc.searchTerm = ifnull(KNA1_SORTL, 'Not Set') 
	,dc.telephone1 = ifnull(KNA1_TELF1, 'Not Set') 
	,dc.transportationZone = ifnull(KNA1_LZONE, 'Not Set') 
	,dc.brandDescription = ifnull((SELECT t.TVK6T_VTEXT FROM TVK6T t WHERE kn.KNA1_KATR6 = t.TVK6T_KATR6 AND t.TVK6T_SPRAS='E'), 'Not Set')
	,dc.transportationZoneDesc = ifnull((SELECT t.TZONT_VTEXT FROM TZONE_TZONT t WHERE kn.KNA1_LZONE = t.TZONE_ZONE1 AND t.TZONT_SPRAS='E'),'Not Set')
	,dc.addressnumber = ifnull(KNA1_ADRNR,'Not Set') 	/* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
	,dc.dw_update_date = current_timestamp
	WHERE  dc.CustomerNumber = kn.kna1_kunnr 
	AND dc.RowIsCurrent = 1 
	AND KNA1_LAND1 = T005T_LAND1;

CALL vectorwise(combine 'dim_customer');	  


/* Begin of Customer Transfer Interface - Insert*/

delete from number_fountain m where m.table_name = 'dim_customer';

insert into number_fountain
select 	'dim_customer',
	ifnull(max(d.Dim_CustomerID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customer d
where d.Dim_CustomerID <> 1;

INSERT INTO dim_customer(Dim_CustomerID,
                         CustomerNumber,
                         IndustryCode,
                         Industry,
                         Country,
                         Name,
			 Name2,
                         City,
                         PostalCode,
                         Region,
                         OneTime,
			 Classification,
			 ClassificationDescription,
                         RowIsCurrent,
                         RowStartDate,
			 CreationDate,
			CustAccountGroup,
			CustAccountGroupDescription,
                        RegionDescription,
			CountryName,
			Channel,
			SubChannel,
 			ChannelDescription,
			SubChannelDescription,
			TradingPartner,
			Attribute3,
			Attribute4,
			Attribute5,
			Attribute6,
			Attribute7,
			Attribute8,
			ConditionGroup1,
			ConditionGroup2,
			IndustryCode1,
			ConditionGroupDescription2,
			IndustryCode1Description,
			Address,
			PayMethod,
			PayMethodDescription
			,custBillingBlock /* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
			,dataLine
			,deliveryBlock
			,deletionFlag
			,faxNumber
			,languageKey
			,orderBlock
			,searchTerm
			,telephone1
			,transportationZone
			,brandDescription
			,transportationZoneDesc
			,addressnumber /* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
		)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customer') + row_number() over(),
	a.* 
	from (
	SELECT 
		distinct KNA1_KUNNR, 
		ifnull(KNA1_BRSCH, 'Not Set'),
		ifnull((SELECT T5UNT_NTEXT
		  FROM T5UNT
		  WHERE T5UNT_NAICS = KNA1_BRSCH ),'Not Set'),
		ifnull(KNA1_LAND1, 'Not Set'),
		ifnull(KNA1_NAME1, 'Not Set'),
		ifnull(KNA1_NAME2, 'Not Set'),
		ifnull(KNA1_ORT01, 'Not Set'),
		ifnull(KNA1_PSTLZ, 'Not Set'),
		ifnull(KNA1_REGIO, 'Not Set'),
		CASE WHEN ifnull(KNA1_XCPDK, '') = 'X' THEN 'Yes' ELSE 'No' END,
		ifnull(KNA1_KUKLA,'Not Set'),
		ifnull((SELECT t.TKUKT_VTEXT FROM TKUKT t
		  WHERE t.TKUKT_KUKLA = KNA1_KUKLA), 'Not Set'),
		1,
		current_timestamp,
		KNA1_ERDAT,
		ifnull(KNA1_KTOKD,'Not Set'),
		ifnull((SELECT t.T077X_TXT30 FROM T077X t
		  WHERE t.T077X_KTOKD = KNA1_KTOKD AND T.T077X_SPRAS = 'E'), 'Not Set'),
		ifnull((SELECT T005U_BEZEI FROM T005U
			WHERE T005U_LAND1 = KNA1_LAND1 AND T005U_BLAND = KNA1_REGIO),'Not Set'),
		ifnull(t005t_landx, 'Not Set'),
		ifnull(KNA1_KATR1, 'Not Set'),
		ifnull(KNA1_KATR2, 'Not Set'),
		ifnull((SELECT t.TVK1T_VTEXT FROM TVK1T t
			WHERE t.TVK1T_KATR1 = KNA1_KATR1),'Not Set') ChannelDescription,
		ifnull((SELECT t.TVK2T_VTEXT FROM TVK2T t
			WHERE t.TVK2T_KATR2 = KNA1_KATR2),'Not Set') SubChannelDescription,
		ifnull(KNA1_VBUND,'Not Set'),
		ifnull(KNA1_KATR3,'Not Set'),
		ifnull(KNA1_KATR4,'Not Set'),
		ifnull(KNA1_KATR5,'Not Set'),
		ifnull(KNA1_KATR6,'Not Set'),
		ifnull(KNA1_KATR7,'Not Set'),
		ifnull(KNA1_KATR8,'Not Set'),
		ifnull(KNA1_KDKG1,'Not Set'),
		ifnull(KNA1_KDKG2,'Not Set'),
		ifnull(KNA1_BRAN1,'Not Set'),
		ifnull((SELECT TVKGGT_VTEXT FROM TVKGGT where TVKGGT_KDKGR = KNA1_KDKG2),'Not Set'),
		ifnull((SELECT TBRCT_VTEXT FROM TBRCT where TBRCT_BRACO = KNA1_BRAN1),'Not Set'),
		ifnull(KNA1_STRAS,'Not Set'),
		ifnull((select KNB1_ZWELS from kna1_knb1 t1 where t1.KNA1_KUNNR = kna1_kunnr) ,'Not Set') as PayMethod,
		ifnull((select T042Z_TEXT1 from kna1_knb1 t1, T042Z t2
			where t1.KNB1_ZWELS = t2.T042Z_ZLSCH
			and t1.KNA1_KUNNR = kna1_kunnr),'Not Set') as PayMethodDescription
		,ifnull(KNA1_FAKSD, 'Not Set') custBillingBlock /* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
		,ifnull(KNA1_DATLT, 'Not Set') dataLine
		,ifnull(KNA1_LIFSD, 'Not Set') deliveryBlock
		,ifnull(KNA1_LOEVM, 'Not Set') deletionFlag
		,ifnull(KNA1_TELFX, 'Not Set') faxNumber
		,ifnull(KNA1_SPRAS, 'Not Set') languageKey
		,ifnull(KNA1_AUFSD, 'Not Set') orderBlock
		,ifnull(KNA1_SORTL, 'Not Set') searchTerm
		,ifnull(KNA1_TELF1, 'Not Set') telephone1
		,ifnull(KNA1_LZONE, 'Not Set') transportationZone
		,ifnull((SELECT t.TVK6T_VTEXT FROM TVK6T t	WHERE KNA1_KATR6 = t.TVK6T_KATR6 AND t.TVK6T_SPRAS='E'), 'Not Set') brandDescription
		,ifnull((SELECT TZONT_VTEXT FROM TZONE_TZONT t WHERE KNA1_LZONE = t.TZONE_ZONE1 AND t.TZONT_SPRAS='E'),'Not Set') transportationZoneDesc
		,ifnull(KNA1_ADRNR,'Not Set') addressnumber /* End New Fields added as a part of Columbia 856 Report, Dec 2014*/	
	FROM kna1 LEFT JOIN t005t on KNA1_LAND1 = T005T_LAND1
	WHERE NOT EXISTS (SELECT 1
	                FROM dim_customer dc
	               WHERE dc.CustomerNumber = kna1_kunnr)) a;

delete from number_fountain m where m.table_name = 'dim_customer';


/* Begin of Populate Customer Hierarchy Interface*/

/* ##################################################################################################################
   Script         : bi_populate_customer_hierarchy 
#################################################################################################################### */

Drop table if exists pCustHierType_701;

create table pCustHierType_701 ( pCustomerHierarchyType ) As
Select CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.hierarchy.type'),
              'A'),varchar(5));

Drop table if exists custhierarchy_tmp;

Create table custhierarchy_tmp(dd_HierarchyType,
                              CustomerNumber,
                              HighCustomerNumber,
                              HigherCustomerNumber,
                              TopLevelCustomerNumber,
                              HighCustomerDesc,
                              HigherCustomerDesc,
                              TopLevelCustomerDesc)
as Select HierarchyType,
                              CustomerNumber,
                              HighCustomerNumber,
                              HigherCustomerNumber,
                              TopLevelCustomerNumber,
                              HighCustomerDesc,
                              HigherCustomerDesc,
                              TopLevelCustomerDesc
from dim_customer where 1=2;

INSERT INTO custhierarchy_tmp(dd_HierarchyType,
                              CustomerNumber,
                              HighCustomerNumber,
                              HigherCustomerNumber,
                              TopLevelCustomerNumber,
                              HighCustomerDesc,
                              HigherCustomerDesc,
                              TopLevelCustomerDesc)
			      	/* EndDate, StartDate) Not in USE */
   SELECT ifnull(KNVH_HITYP,'Not Set'),
          ifnull(KNVH_KUNNR,'Not Set'),
          ifnull(KNVH_HKUNNR,'Not Set'),
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set'
           	/* KNVH_DATBI, KNVH_DATAB Not in USE */
     FROM KNVH
 WHERE  current_date BETWEEN KNVH_DATAB AND KNVH_DATBI;

      
 UPDATE    custhierarchy_tmp ct FROM
          custhierarchy_tmp ctPar
   SET ct.HigherCustomerNumber = ctPar.HighCustomerNumber
 WHERE     ctPar.CustomerNumber = ct.HighCustomerNumber
          AND ctPar.dd_HierarchyType = ct.dd_HierarchyType AND  ct.HigherCustomerNumber = 'Not Set';


UPDATE    custhierarchy_tmp ct  FROM
          custhierarchy_tmp ctPar
   SET ct.TopLevelCustomerNumber = ctPar.HighCustomerNumber
 WHERE ctPar.CustomerNumber = ct.HigherCustomerNumber
          AND ctPar.dd_HierarchyType = ct.dd_HierarchyType 
          AND ct.TopLevelCustomerNumber = 'Not Set';
          
UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.HigherCustomerNumber,
       ct.HigherCustomerNumber = ct.HighCustomerNumber,
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber <> 'Not Set'
    and ct.HighCustomerNumber <> 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.HighCustomerNumber,
       ct.HigherCustomerNumber = 'Not Set',
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber = 'Not Set'
    and ct.HighCustomerNumber <> 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.CustomerNumber,
       ct.HigherCustomerNumber = 'Not Set',
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber = 'Not Set'
    and ct.HighCustomerNumber = 'Not Set';

UPDATE    custhierarchy_tmp ct FROM
          dim_customer c       
   SET ct.TopLevelCustomerDesc = c.Name
 WHERE ct.TopLevelCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

UPDATE    custhierarchy_tmp ct FROM
          dim_customer c       
   SET ct.HigherCustomerDesc = c.Name
 WHERE ct.HigherCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;
 
 UPDATE    custhierarchy_tmp ct FROM
          dim_customer c       
   SET ct.HighCustomerDesc = c.Name
 WHERE ct.HighCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

UPDATE    custhierarchy_tmp ct 
   SET ct.TopLevelCustomerDesc = 'Not Set'
 WHERE ct.TopLevelCustomerDesc IS NULL;

UPDATE    custhierarchy_tmp ct 
   SET ct.HigherCustomerDesc = 'Not Set'
 WHERE ct.HigherCustomerDesc IS NULL;
 
 UPDATE    custhierarchy_tmp ct 
   SET ct.HighCustomerDesc = 'Not Set'
 WHERE ct.HighCustomerDesc IS NULL;
 
 UPDATE dim_customer c FROM
         custhierarchy_tmp ct, pCustHierType_701 p
  SET c.HighCustomerNumber = ct.HighCustomerNumber,
      c.HierarchyType = ct.dd_HierarchyType,
      c.HigherCustomerNumber = ct.HigherCustomerNumber,
      c.TopLevelCustomerNumber = ct.TopLevelCustomerNumber,
      c.HighCustomerDesc = ct.HighCustomerDesc,
      c.HigherCustomerDesc = ct.HigherCustomerDesc,
      c.TopLevelCustomerDesc = ct.TopLevelCustomerDesc,
			c.dw_update_date = current_timestamp
 WHERE ct.CustomerNumber = c.CustomerNumber
       AND ct.dd_HierarchyType = p.pCustomerHierarchyType;

call vectorwise ( combine 'custhierarchy_tmp-custhierarchy_tmp');
