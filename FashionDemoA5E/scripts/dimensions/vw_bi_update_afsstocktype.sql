UPDATE    dim_afsstocktype ast
       FROM
          DD07T dt
   SET ast.StockTypeDescription = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE ast.RowIsCurrent = 1
 AND  dt.DD07T_DOMNAME = 'J_3ABSKZ'
          AND dt.DD07T_DOMVALUE IS NOT NULL
          AND ast.StockType = dt.DD07T_DOMVALUE;