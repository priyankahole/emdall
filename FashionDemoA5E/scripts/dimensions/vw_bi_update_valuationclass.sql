UPDATE    dim_ValuationClass vc
       FROM
          t025t t
   SET DescriptionOfValuationClass = ifnull(T025T_BKBEZ, 'Not Set')
 WHERE vc.RowIsCurrent = 1
 AND t.T025T_BKLAS = vc.ValuationClass;
