UPDATE    dim_storagelocation s
       FROM
          T001L l
   SET s.Description = ifnull(T001L_LGOBE, 'Not Set'),
       s.Division = ifnull(T001L_SPART, 'Not Set'),
       s.FreezingBookInventory = ifnull(T001L_XBUFX, 'Not Set'),
       s.MRPExclude = ifnull(T001L_DISKZ, 'Not Set'),
       s.StorageResource = ifnull(T001L_XRESS, 'Not Set'),
       s.PartnerStorageLocation = ifnull(T001L_PARLG, 'Not Set'),
       s.StorageSalesOrg = ifnull(T001L_VKORG, 'Not Set'),
       s.DistributionChannel = ifnull(T001L_VTWEG, 'Not Set'),
       s.ShipReceivePoint = ifnull(T001L_VSTEL, 'Not Set'),
       s.InTransit = ifnull(T001L_OIG_ITRFL, 'Not Set')
 WHERE s.RowIsCurrent = 1
 AND s.LocationCode = l.T001L_LGORT AND s.Plant = l.T001L_WERKS
;
