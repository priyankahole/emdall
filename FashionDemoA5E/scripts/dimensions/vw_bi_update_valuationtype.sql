UPDATE    dim_ValuationType vt
       FROM
          T149D t
   SET AccountCategory = ifnull(T149D_KKREF, 'Not Set')
 WHERE vt.RowIsCurrent = 1
 AND t.T149D_BWTAR = vt.ValuationType;
