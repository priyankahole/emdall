UPDATE    dim_reasoncode drc
       FROM
          ReasonCode rc
   SET drc.ReasonCode = ifnull(rc.Reason_mnemonic,'Not Set'),
       drc.Description = ifnull(rc.Description,'Not Set'),
       drc.CodeType = ifnull(rc.Code_type,'Not Set'),
			drc.dw_update_date = current_timestamp
	WHERE drc.ReasonCodeId = rc.ID
	  AND drc.RowIsCurrent = 1
;

INSERT INTO dim_reasoncode(dim_reasoncodeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_reasoncode
               WHERE dim_reasoncodeId = 1);

delete from number_fountain m where m.table_name = 'dim_reasoncode';
   
insert into number_fountain
select 	'dim_reasoncode',
	ifnull(max(d.dim_reasoncodeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_reasoncode d
where d.dim_reasoncodeid <> 1; 

INSERT INTO dim_reasoncode(dim_reasoncodeid,
                           CodeType,
                           Description,
                           ReasonCode,
			   ReasonCodeId,
                           RowIsCurrent,
                           RowStartDate)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_reasoncode')
         + row_number() over(),
			  ifnull(Code_type,'Not Set'),
          ifnull(Description,'Not Set'),
          ifnull(Reason_mnemonic,'Not Set'),
	  ID,
          1,
          current_timestamp
     FROM reasoncode rc
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_reasoncode
               WHERE rc.ID = ReasonCodeId
		 AND RowIsCurrent = 1)
;

