/* ##################################################################################################################
  
     Script         : bi_populate_so_headerstatus_dim 
     Author         : Ashu
     Created On     : 17 Jan 2013
  
  
     Description    : Stored Proc bi_populate_so_headerstatus_dim from MySQL to Vectorwise syntax
  
     Change History
     Date            By        Version           Desc
     17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
	 7 July 2014     George    1.1               Added GeneralIncompleteStatusItemCode,OverallCreditStatus,OverallCreditStatusCode,OverallBlkdStatusCode,
														OverallDlvrBlkStatusCode,TotalIncompleteStatusAllItemsCode,RejectionStatusCode
	 11 July 2013    George    1.2               Added TVBST source fields: GeneralIncompletionProcessingstatusOfHeader,OverallProcessingstatusofcreditchecks,
												 OverallBlockedProcessingStatus,DeliveryBlockProcessingStatus,TotalIncompletionProcessingStatusAllItems,OverallRejectionProcessingStatus
	07 Aug 2015 	Cristina    1.3 			 Added the population script of dimension from Shipments process
	 #################################################################################################################### */

select current_time;

DELETE FROM VBUK
  WHERE NOT EXISTS
            (SELECT 1
               FROM VBAK_VBAP_VBEP
              WHERE VBUK_VBELN = VBAK_VBELN) 
        AND NOT EXISTS
               (SELECT 1
                  FROM fact_salesorderlife
                 WHERE dd_SalesDocNo = VBUK_VBELN);

 
DELETE FROM VBUK
  WHERE NOT EXISTS
               (SELECT 1
                  FROM VBAK_VBAP
                 WHERE VBUK_VBELN = VBAP_VBELN)
        AND NOT EXISTS
               (SELECT 1
                  FROM fact_salesorderlife
                 WHERE dd_SalesDocNo = VBUK_VBELN);


UPDATE dim_salesorderheaderstatus
from VBUK
SET RefStatusAllItems = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
    ConfirmationStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
    DeliveryStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
    OverallDlvrStatusOfItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
    BillingStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
    BillingStatusOfOrdRelBillDoc = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
    RejectionStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
    OverallProcessStatusItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
    TotalIncompleteStatusAllItems = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
    GeneralIncompleteStatusItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
    OverallPickingStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
    HeaderIncompletionStautusConcernDlvry = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set'),
    OverallBlkdStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
    OverallDlvrBlkStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	GeneralIncompleteStatusItemCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
	OverallCreditStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	OverallCreditStatusCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	OverallBlkdStatusCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	OverallDlvrBlkStatusCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	TotalIncompleteStatusAllItemsCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
	RejectionStatusCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
	GeneralIncompletionProcessingstatusOfHeader = ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_UVALL,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'UVALL'),'Not Set'),
	OverallProcessingStatusOfCreditChecks = ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_CMGST,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'CMGST'),'Not Set'),
	OverallBlockedProcessingStatus = ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_SPSTG,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'SPSTG'),'Not Set'),
	DeliveryBlockProcessingStatus = ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_LSSTK,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'LSSTK'),'Not Set'),
	TotalIncompletionProcessingStatusAllItems = ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_UVALS,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'UVALS'),'Not Set'),
	OverallRejectionProcessingStatus = ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_ABSTK,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'ABSTK'),'Not Set'),
			dw_update_date = current_timestamp
WHERE dim_salesorderheaderstatus.SalesDocumentNumber = VBUK_VBELN;

delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 
 
INSERT INTO dim_salesorderheaderstatus(
	    dim_salesorderheaderstatusid,
            SalesDocumentNumber,
            RefStatusAllItems,
            ConfirmationStatus,
            DeliveryStatus,
            OverallDlvrStatusOfItem,
            BillingStatus,
            BillingStatusOfOrdRelBillDoc,
            RejectionStatus,
            OverallProcessStatusItem,
            TotalIncompleteStatusAllItems,
            GeneralIncompleteStatusItem,
            OverallPickingStatus,
	    HeaderIncompletionStautusConcernDlvry,
	    OverallBlkdStatus,
	    OverallDlvrBlkStatus,
		GeneralIncompleteStatusItemCode,
		OverallCreditStatus,
		OverallCreditStatusCode,
		OverallBlkdStatusCode,
		OverallDlvrBlkStatusCode,
		TotalIncompleteStatusAllItemsCode,
		RejectionStatusCode,
		GeneralIncompletionProcessingstatusOfHeader,
		OverallProcessingstatusofcreditchecks,
		OverallBlockedProcessingStatus,
		DeliveryBlockProcessingStatus,
		TotalIncompletionProcessingStatusAllItems,
		OverallRejectionProcessingStatus
		)
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_UVALL,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'UVALL'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_CMGST,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'CMGST'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_SPSTG,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'SPSTG'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_LSSTK,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'LSSTK'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_UVALS,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'UVALS'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_ABSTK,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'ABSTK'),'Not Set')
FROM VBUK inner join VBAK_VBAP_VBEP on VBAK_VBELN = VBUK_VBELN
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = VBUK_VBELN)) dtbl1;
   
delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 

INSERT
INTO dim_salesorderheaderstatus(
	dim_salesorderheaderstatusid,
   SalesDocumentNumber,
   RefStatusAllItems,
   ConfirmationStatus,
   DeliveryStatus,
   OverallDlvrStatusOfItem,
   BillingStatus,
   BillingStatusOfOrdRelBillDoc,
   RejectionStatus,
   OverallProcessStatusItem,
   TotalIncompleteStatusAllItems,
   GeneralIncompleteStatusItem,
   OverallPickingStatus,
   HeaderIncompletionStautusConcernDlvry,
   OverallBlkdStatus,
   OverallDlvrBlkStatus,
   GeneralIncompleteStatusItemCode,
   OverallCreditStatus,
   OverallCreditStatusCode,
   OverallBlkdStatusCode,
   OverallDlvrBlkStatusCode,
   TotalIncompleteStatusAllItemsCode,
   RejectionStatusCode,
   GeneralIncompletionProcessingstatusOfHeader,
   OverallProcessingstatusofcreditchecks,
   OverallBlockedProcessingStatus,
   DeliveryBlockProcessingStatus,
   TotalIncompletionProcessingStatusAllItems,
   OverallRejectionProcessingStatus   
   )
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_UVALL,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'UVALL'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_CMGST,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'CMGST'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_SPSTG,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'SPSTG'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_LSSTK,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'LSSTK'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_UVALS,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'UVALS'),'Not Set'),
	ifnull((select t.TVBST_BEZEI from TVBST t where varchar(t.TVBST_STATU,1) = varchar(VBUK_ABSTK,1) and t.TVBST_TBNAM = 'VBUK' and t.TVBST_FDNAM = 'ABSTK'),'Not Set')
FROM VBUK inner join VBAK_VBAP on VBAP_VBELN = VBUK_VBELN
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = VBUK_VBELN)) dtbl1;

select 'Process Complete : ';

drop table if exists tmp_del_LIKP_LIPS_VBUK;
create table tmp_del_LIKP_LIPS_VBUK
as
select distinct LIKP_VBELN from LIKP_LIPS
union
select distinct dd_SalesDlvrDocNo from fact_salesorderlife;

delete from LIKP_LIPS_VBUK
  where not exists (select 1 from tmp_del_LIKP_LIPS_VBUK where VBUK_VBELN = LIKP_VBELN);

  UPDATE dim_salesorderheaderstatus
	FROM LIKP_LIPS_VBUK
   SET RefStatusAllItems = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
       ConfirmationStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
       DeliveryStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
       OverallDlvrStatusOfItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
       BillingStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
       BillingStatusOfOrdRelBillDoc = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
       RejectionStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
       OverallProcessStatusItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
       TotalIncompleteStatusAllItems = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
       GeneralIncompleteStatusItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
       OverallPickingStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
       HeaderIncompletionStautusConcernDlvry = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set'),
			dw_update_date = current_timestamp
  WHERE dim_salesorderheaderstatus.SalesDocumentNumber = VBUK_VBELN;

delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 

  INSERT
    INTO dim_salesorderheaderstatus(
	  dim_salesorderheaderstatusid,
          SalesDocumentNumber,
          RefStatusAllItems,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDlvrStatusOfItem,
          BillingStatus,
          BillingStatusOfOrdRelBillDoc,
          RejectionStatus,
          OverallProcessStatusItem,
          TotalIncompleteStatusAllItems,
          GeneralIncompleteStatusItem,
          OverallPickingStatus,
	  HeaderIncompletionStautusConcernDlvry
          )
 SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set')
FROM LIKP_LIPS_VBUK inner join LIKP_LIPS on LIKP_VBELN = VBUK_VBELN
  WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = VBUK_VBELN)) dtbl1;

drop table if exists tmp_del_LIKP_LIPS_VBUK;
