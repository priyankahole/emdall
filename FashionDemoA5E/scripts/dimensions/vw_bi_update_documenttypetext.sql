
UPDATE    dim_documenttypetext dt
       FROM
          T003T t
   SET dt.Description = t.T003T_LTEXT
   WHERE dt.Type = t.T003T_BLART AND dt.RowIsCurrent = 1;
