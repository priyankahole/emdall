UPDATE    dim_storagelocation s
       FROM
          T001L l
   SET s.Description = ifnull(T001L_LGOBE, 'Not Set'),
       s.Division = ifnull(T001L_SPART, 'Not Set'),
       s.FreezingBookInventory = ifnull(T001L_XBUFX, 'Not Set'),
       s.MRPExclude = ifnull(T001L_DISKZ, 'Not Set'),
       s.StorageResource = ifnull(T001L_XRESS, 'Not Set'),
       s.PartnerStorageLocation = ifnull(T001L_PARLG, 'Not Set'),
       s.StorageSalesOrg = ifnull(T001L_VKORG, 'Not Set'),
       s.DistributionChannel = ifnull(T001L_VTWEG, 'Not Set'),
       s.ShipReceivePoint = ifnull(T001L_VSTEL, 'Not Set'),
       s.InTransit = ifnull(T001L_OIG_ITRFL, 'Not Set'),
			s.dw_update_date = current_timestamp
 WHERE s.RowIsCurrent = 1
 AND s.LocationCode = l.T001L_LGORT AND s.Plant = l.T001L_WERKS
;

INSERT INTO dim_storagelocation(dim_storagelocationId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_storagelocation
               WHERE dim_storagelocationId = 1);

delete from number_fountain m where m.table_name = 'dim_storagelocation';

insert into number_fountain
select 	'dim_storagelocation',
	ifnull(max(d.Dim_StorageLocationid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_storagelocation d
where d.Dim_StorageLocationid <> 1;

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
                                Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_storagelocation') 
          + row_number() over(),
			 ifnull(T001L_LGOBE, 'Not Set'),
          T001L_LGORT,
          ifnull(T001L_SPART, 'Not Set'),
          ifnull(T001L_XBUFX, 'Not Set'),
          ifnull(T001L_DISKZ, 'Not Set'),
          ifnull(T001L_XRESS, 'Not Set'),
          ifnull(T001L_PARLG, 'Not Set'),
          ifnull(T001L_VKORG, 'Not Set'),
          ifnull(T001L_VTWEG, 'Not Set'),
          ifnull(T001L_VSTEL, 'Not Set'),
          ifnull(T001L_WERKS, 'Not Set'),
          ifnull(T001L_OIG_ITRFL, 'Not Set'),
          current_timestamp,
          1
     FROM T001L
    WHERE T001L_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE LocationCode = T001L_LGORT AND Plant = T001L_WERKS)
;

