
UPDATE    dim_materialgroup s
       FROM
          T023T t
   SET s.MaterialGroupName = t.T023T_WGBEZ
 WHERE s.RowIsCurrent = 1
 AND s.MaterialGroupCode = t.T023T_MATKL;
