UPDATE    dim_salesriskcategory src
       FROM
          T691T t
   SET src.SalesRiskCategoryDescription = ifnull(t.T691T_RTEXT, 'Not Set'),
			src.dw_update_date = current_timestamp
 WHERE src.RowIsCurrent = 1
 AND src.SalesRiskCategory = t.T691T_CTLPC
 AND src.CreditControlArea = t.T691T_KKBER
;

INSERT INTO dim_salesriskcategory(dim_salesriskcategoryId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesriskcategory
               WHERE dim_salesriskcategoryId = 1);

delete from number_fountain m where m.table_name = 'dim_salesriskcategory';

insert into number_fountain
select 	'dim_salesriskcategory',
	ifnull(max(d.dim_SalesRiskCategoryId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesriskcategory d
where d.dim_SalesRiskCategoryId <> 1; 

INSERT INTO dim_salesriskcategory(dim_SalesRiskCategoryId,
                                SalesRiskCategory,
                                CreditControlArea,
				SalesRiskCategoryDescription,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesriskcategory') 
          + row_number() over(),
		  ifnull(T691T_CTLPC, 'Not Set'),
          ifnull(T691T_KKBER, 'Not Set'),
          ifnull(T691T_RTEXT, 'Not Set'),
          current_timestamp,
          1
     FROM T691T
    WHERE T691T_CTLPC IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_salesriskcategory
                  WHERE SalesRiskCategory = T691T_CTLPC AND CreditControlArea = T691T_KKBER)
;

