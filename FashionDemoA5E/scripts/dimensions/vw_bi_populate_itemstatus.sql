UPDATE    dim_itemstatus s
       FROM
          DD07T t
   SET s.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			s.dw_update_date = current_timestamp
 WHERE s.RowIsCurrent = 1
       AND     t.DD07T_DOMNAME = 'EPSTATU'
          AND DD07T_DOMVALUE IS NOT NULL
          AND s.status = t.DD07T_DOMVALUE;

INSERT INTO dim_itemstatus(dim_itemstatusid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_itemstatus
               WHERE dim_itemstatusid = 1);

delete from number_fountain m where m.table_name = 'dim_itemstatus';
   
insert into number_fountain
select 	'dim_itemstatus',
	ifnull(max(d.Dim_ItemStatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_itemstatus d
where d.Dim_ItemStatusid <> 1; 

INSERT INTO dim_itemstatus(Dim_ItemStatusid,
                           Description,
                           Status,
                           RowStartDate,
                           RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_itemstatus')
          + row_number() over() ,
                         ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'EPSTATU' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_itemstatus
                    WHERE Status = DD07T_DOMVALUE AND DD07T_DOMNAME = 'EPSTATU')
   ORDER BY 2 OFFSET 0;
