UPDATE    dim_notificationphase s
       FROM
          DD07T t
   SET s.NotificationPhaseName = t.DD07T_DDTEXT
 WHERE s.RowIsCurrent = 1
    AND   t.DD07T_DOMNAME = 'QM_PHASE'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND s.NotificationPhaseCode = t.DD07T_DOMVALUE
;