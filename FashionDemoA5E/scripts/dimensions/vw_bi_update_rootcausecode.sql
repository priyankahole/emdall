UPDATE    dim_rootcausecode ds
       FROM
          UDMATTR_RCCODET dt
   SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set')
 WHERE ds.RowIsCurrent = 1
          AND ds.RootCCode = dt.ROOT_CCODE
          AND dt.ROOT_CCODE IS NOT NULL
;
