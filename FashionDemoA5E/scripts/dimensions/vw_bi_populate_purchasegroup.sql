UPDATE    dim_purchasegroup pg
       FROM
          T024 t
   SET pg.Description = ifnull(T024_EKNAM, 'Not Set'),
			pg.dw_update_date = current_timestamp
   WHERE t.T024_EKGRP = pg.PurchaseGroup AND pg.RowIsCurrent = 1
;

INSERT INTO dim_purchasegroup(dim_purchasegroupId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_purchasegroup
               WHERE dim_purchasegroupId = 1);

delete from number_fountain m where m.table_name = 'dim_purchasegroup';
   
insert into number_fountain
select 	'dim_purchasegroup',
	ifnull(max(d.Dim_PurchaseGroupid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_purchasegroup d
where d.Dim_PurchaseGroupid <> 1; 

INSERT INTO dim_purchasegroup(Dim_PurchaseGroupid,
                              PurchaseGroup,
                              Description,
                              RowStartDate,
                              RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_purchasegroup')
         + row_number() over(),
			  t.T024_EKGRP,
          ifnull(T024_EKNAM, 'Not Set'),
          current_timestamp,
          1
     FROM t024 t
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_purchasegroup
                       WHERE PurchaseGroup = T024_EKGRP and rowiscurrent = 1)
;

