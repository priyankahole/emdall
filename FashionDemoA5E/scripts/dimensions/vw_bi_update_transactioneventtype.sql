UPDATE    dim_transactioneventtype et
       FROM
          t158w w
   SET et.TransactionEventTypeDescription = w.T158W_LTEXT
 WHERE et.RowIsCurrent = 1
 AND et.TransactionEventTypeCode = w.T158W_VGART
;