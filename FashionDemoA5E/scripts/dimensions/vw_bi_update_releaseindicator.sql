UPDATE    dim_ReleaseIndicator ri
       FROM
          T16FE t
   SET ri.ReleaseIndicator = ifnull(t.T16FE_FRGET, 'Not Set')
 WHERE ri.RowIsCurrent = 1
 AND t.T16FE_FRGKE = ri.ReleaseIndicatorCode;
