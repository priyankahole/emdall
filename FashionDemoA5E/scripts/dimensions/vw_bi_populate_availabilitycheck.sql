insert into Dim_AvailabilityCheck (Dim_AvailabilityCheckid,AvailabilityCheck,AvailabilityCheckDescription,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason)
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from Dim_AvailabilityCheck where Dim_AvailabilityCheckid = 1);
               
UPDATE dim_AvailabilityCheck a FROM TMVFT 
   SET a.AvailabilityCheckDescription = ifnull(TMVFT_BEZEI, 'Not Set'),
		a.dw_update_date = current_timestamp
 WHERE a.AvailabilityCheck = TMVFT_MTVFP AND RowIsCurrent = 1;               

delete from number_fountain m where m.table_name = 'Dim_AvailabilityCheck';

insert into number_fountain
select 	'Dim_AvailabilityCheck',
	ifnull(max(d.Dim_AvailabilityCheckid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_AvailabilityCheck d
where d.Dim_AvailabilityCheckid <> 1; 
 
INSERT INTO dim_availabilitycheck(Dim_availabilitycheckid,
                                    availabilitycheck,
                                    availabilitycheckDescription,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_AvailabilityCheck') 
          + row_number() over() ,
			 t.TMVFT_MTVFP,
          ifnull(t.TMVFT_BEZEI, 'Not Set'),
          current_timestamp,
          1
     FROM TMVFT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_availabilitycheck a
               WHERE a.availabilitycheck = t.TMVFT_MTVFP);
			   
delete from number_fountain m where m.table_name = 'Dim_AvailabilityCheck';			   