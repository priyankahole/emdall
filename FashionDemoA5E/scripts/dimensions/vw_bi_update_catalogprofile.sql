UPDATE    dim_catalogprofile cp
       FROM
          T352B_T t      
   SET cp.CatalogProfileName = ifnull(t.T352B_T_RBNRX, 'Not Set')
 WHERE cp.RowIsCurrent = 1
 AND t.T352B_T_RBNR = cp.CatalogProfileCode
;