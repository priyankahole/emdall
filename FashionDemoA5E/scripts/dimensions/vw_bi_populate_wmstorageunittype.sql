/* 	Server: QA
	Process Name: WM Storage Unit Type Transfer
	Interface No: 2
*/
INSERT INTO dim_wmstorageunittype
(dim_wmstorageunittypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmstorageunittype where dim_wmstorageunittypeid = 1);

UPDATE    dim_wmstorageunittype wmsut
FROM T307T t
SET  wmsut.StorageUnitTypeDescription = t.T307T_LETYT,
			wmsut.dw_update_date = current_timestamp
WHERE 		wmsut.RowIsCurrent = 1
		AND wmsut.StorageUnitType = t.T307T_LETYP
		AND wmsut.WarehouseNumber = t.T307T_LGNUM;
 
delete from number_fountain m where m.table_name = 'dim_wmstorageunittype';

insert into number_fountain				   
select 	'dim_wmstorageunittype',
	ifnull(max(d.dim_wmstorageunittypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmstorageunittype d
where d.dim_wmstorageunittypeid <> 1;

INSERT INTO dim_wmstorageunittype(dim_wmstorageunittypeid,
							WarehouseNumber,
							StorageUnitType,
                            StorageUnitTypeDescription,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmstorageunittype') + row_number() over(),
		  t.T307T_LGNUM,
          t.T307T_LETYP,
		  t.T307T_LETYT,
          current_timestamp,
          1
     FROM T307T t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_wmstorageunittype s
                  WHERE    s.StorageUnitType = t.T307T_LETYP
					    AND s.WarehouseNumber = t.T307T_LGNUM
                        AND s.RowIsCurrent = 1) ;
