UPDATE dim_MaterialPricingGroup a FROM T178T 
   SET a.MaterialPricingGroupName = ifnull(T178T_VTEXT, 'Not Set'),
			a.dw_update_date = current_timestamp
 WHERE a.MaterialPricingGroupCode = T178T_KONDM AND RowIsCurrent = 1;

INSERT INTO dim_MaterialPricingGroup(dim_MaterialPricingGroupId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPricingGroup
               WHERE dim_MaterialPricingGroupId = 1);

delete from number_fountain m where m.table_name = 'dim_MaterialPricingGroup';
   
insert into number_fountain
select 	'dim_MaterialPricingGroup',
	ifnull(max(d.dim_materialpricinggroupId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MaterialPricingGroup d
where d.dim_materialpricinggroupId <> 1; 

INSERT INTO dim_MaterialPricingGroup(dim_materialpricinggroupId,
                                    MaterialPricingGroupCode,
                                    MaterialPricingGroupName,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MaterialPricingGroup') 
          + row_number() over() ,
	  t.T178T_KONDM,
          ifnull(T178T_VTEXT, 'Not Set'),
          current_timestamp,
          1
     FROM T178T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPricingGroup a
               WHERE a.MaterialPricingGroupCode = T178T_VTEXT);
