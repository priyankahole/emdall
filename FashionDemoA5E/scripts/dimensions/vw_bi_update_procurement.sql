UPDATE    Dim_Procurement p
       FROM
          DD07T t
   SET p.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE p.RowIsCurrent = 1
       AND    t.DD07T_DOMNAME = 'BESKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND p.Procurement = t.DD07T_DOMVALUE 
;
