
UPDATE dim_CustomerConditionGroups a FROM TVKGGT
   SET a.Description = ifnull(TVKGGT_VTEXT, 'Not Set')
 WHERE a.CustomerCondGrp = TVKGGT_KDKGR AND RowIsCurrent = 1;
