UPDATE    dim_notificationtype nt
       FROM
          TQ80_T t
   SET nt.NotificationTypeName = t.TQ80_T_QMARTX,
			nt.dw_update_date = current_timestamp
 WHERE nt.RowIsCurrent = 1
 AND nt.NotificationTypeCode = t.TQ80_T_QMART
;

INSERT INTO dim_notificationtype(dim_notificationtypeId, RowIsCurrent,notificationtypecode,notificationtypename,
rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_notificationtype
               WHERE dim_notificationtypeId = 1);

delete from number_fountain m where m.table_name = 'dim_notificationtype';
   
insert into number_fountain
select 	'dim_notificationtype',
	ifnull(max(d.dim_notificationtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_notificationtype d
where d.dim_notificationtypeid <> 1; 

INSERT INTO dim_notificationtype(dim_notificationtypeid,
                                          NotificationTypeCode,
                                          NotificationTypeName,           
                                          RowStartDate,
                                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_notificationtype') 
          + row_number() over() ,
			 t.TQ80_T_QMART,
          t.TQ80_T_QMARTX,
          CURRENT_TIMESTAMP,
          1
     FROM TQ80_T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_notificationtype s
               WHERE s.NotificationTypeCode = t.TQ80_T_QMART
                     AND s.RowIsCurrent = 1)
;
