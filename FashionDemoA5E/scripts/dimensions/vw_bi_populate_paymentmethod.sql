UPDATE    dim_paymentmethod pm
       FROM
          T042ZT t, T005T
   SET pm.Description = t.T042ZT_TEXT2,
       pm.CountryName = ifnull(t005t_landx, 'Not Set'),
			pm.dw_update_date = current_timestamp
 WHERE pm.RowIsCurrent = 1
 AND  pm.PaymentMethod = t.T042ZT_ZLSCH AND pm.Country = t.T042ZT_LAND1 AND t.T042ZT_LAND1 = T005T_LAND1;

INSERT INTO dim_paymentmethod(dim_paymentmethodid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_paymentmethod
               WHERE dim_paymentmethodid = 1);

delete from number_fountain m where m.table_name = 'dim_paymentmethod';
   
insert into number_fountain
select 	'dim_paymentmethod',
	ifnull(max(d.Dim_PaymentMethodId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_paymentmethod d
where d.Dim_PaymentMethodId <> 1; 
			   
INSERT INTO dim_paymentmethod(Dim_PaymentMethodId,
                        Country,
                        PaymentMethod,
              		Description,
                        RowStartDate,
                        RowIsCurrent,
           		CountryName)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_paymentmethod')
         + row_number() over(),
        t.T042ZT_LAND1,
          t.T042ZT_ZLSCH,
   t.T042ZT_TEXT2,
          current_timestamp,
          1,
   ifnull(t005t_landx, 'Not Set')
     FROM T042ZT t INNER JOIN T005T ON t.T042ZT_LAND1 = T005T_LAND1
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_paymentmethod pm
               WHERE pm.PaymentMethod = t.T042ZT_ZLSCH AND pm.Country = t.T042ZT_LAND1 AND pm.RowIsCurrent = 1);
