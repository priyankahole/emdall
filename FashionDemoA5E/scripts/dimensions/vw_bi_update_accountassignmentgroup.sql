
UPDATE dim_accountassignmentgroup aag from TVKTT t
   SET aag.Description = ifnull(t.TVKTT_VTEXT, 'Not Set')
 WHERE aag.AccountAssignmentGroup = t.TVKTT_KTGRD AND aag.RowIsCurrent = 1;
