UPDATE    dim_materialgroup s
       FROM
          T023T t
   SET s.MaterialGroupName = t.T023T_WGBEZ,
			s.dw_update_date = current_timestamp
 WHERE s.RowIsCurrent = 1
 AND s.MaterialGroupCode = t.T023T_MATKL;

INSERT INTO dim_materialgroup(dim_materialgroupid, RowIsCurrent,materialgroupcode,materialgroupname,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_materialgroup
               WHERE dim_materialgroupid = 1);

delete from number_fountain m where m.table_name = 'dim_materialgroup';
   
insert into number_fountain
select 	'dim_materialgroup',
	ifnull(max(d.dim_materialgroupid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_materialgroup d
where d.dim_materialgroupid <> 1; 

INSERT INTO dim_materialgroup(dim_materialgroupid,
                              MaterialGroupCode,
                              MaterialGroupName,
                              RowStartDate,
                              RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_materialgroup')
          + row_number()
             over(), tt.* from (SELECT DISTINCT
    t.T023T_MATKL,
          t.T023T_WGBEZ,
       current_timestamp,
          1
     FROM T023T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_materialgroup s
               WHERE s.MaterialGroupCode = t.T023T_MATKL
                     AND s.RowIsCurrent = 1)) tt;
