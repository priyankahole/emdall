UPDATE    dim_purchasegroup pg
       FROM
          T024 t
   SET pg.Description = ifnull(T024_EKNAM, 'Not Set')
   WHERE t.T024_EKGRP = pg.PurchaseGroup AND pg.RowIsCurrent = 1
;
