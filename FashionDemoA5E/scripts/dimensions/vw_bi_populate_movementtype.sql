UPDATE    dim_movementtype mt
       FROM
          t156t t
   SET mt.Description = t156t_btext,
			mt.dw_update_date = current_timestamp
 WHERE mt.RowIsCurrent = 1
      AND    t.t156t_bwart = mt.MovementType
          AND ifnull(t.t156t_sobkz, 'Not Set') = mt.SpecialStockIndicator
          AND ifnull(t.t156t_kzbew, 'Not Set') = mt.MovementIndicator
          AND ifnull(t.t156t_kzzug, 'Not Set') = mt.ReceiptIndicator
          AND ifnull(t.t156t_kzvbr, 'Not Set') = mt.ConsumptionIndicator;

INSERT INTO dim_movementtype(dim_movementtypeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_movementtype
               WHERE dim_movementtypeId = 1);

delete from number_fountain m where m.table_name = 'dim_movementtype';
   
insert into number_fountain
select 	'dim_movementtype',
	ifnull(max(d.Dim_MovementTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_movementtype d
where d.Dim_MovementTypeid <> 1; 

INSERT INTO dim_movementtype(Dim_MovementTypeid,
                             MovementType,
                             Description,
                             SpecialStockIndicator,
                             MovementIndicator,
                             ReceiptIndicator,
                             ConsumptionIndicator,
                             RowStartDate,
                             RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_movementtype') 
          + row_number() over() ,
			 t156t_bwart,
          t156t_btext,
          ifnull(t156t_sobkz, 'Not Set'),
          ifnull(t156t_kzbew, 'Not Set'),
          ifnull(t156t_kzzug, 'Not Set'),
          ifnull(t156t_kzvbr, 'Not Set'),
          current_timestamp,
          1
     FROM t156t t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_movementtype mt
               WHERE mt.MovementType = t.t156t_bwart
                     AND ifnull(t156t_sobkz, 'Not Set') = mt.SpecialStockIndicator
                     AND ifnull(t156t_kzbew, 'Not Set') = mt.MovementIndicator
                     AND ifnull(t156t_kzzug, 'Not Set') = mt.ReceiptIndicator
                     AND ifnull(t156t_kzvbr, 'Not Set') = mt.ConsumptionIndicator)
;
