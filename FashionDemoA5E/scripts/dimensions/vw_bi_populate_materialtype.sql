insert into dim_materialtype
(dim_materialtypeid, materialtype, materialtypedescription, RowIsCurrent)
select 	1, 'Not Set', 'Not Set', 1
from ( select 1 )  AS t
where not exists
             (select 1
                from dim_materialtype
               where dim_materialtypeid = 1);

update    dim_materialtype d
from ( select distinct parttype, parttypedescription from dim_part p where p.parttype <> 'Not Set' ) mf
set d.materialtypedescription	= mf.parttypedescription,
	d.dw_update_date 			= current_timestamp
where d.materialtype = mf.parttype AND d.RowIsCurrent = 1 and d.materialtypedescription <>	mf.parttypedescription;
   
delete from number_fountain m where m.table_name = 'dim_materialtype';

insert into number_fountain
select 	'dim_materialtype',
	ifnull(max(d.dim_materialtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_materialtype d
where d.dim_materialtypeid <> 1;
	
insert into dim_materialtype
(dim_materialtypeid, materialtype, materialtypedescription, RowStartDate, RowIsCurrent)	
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_materialtype')
          + row_number() over(),
mf.*
from 
(select distinct parttype, parttypedescription, current_timestamp, 1
from dim_part p
where p.parttype <> 'Not Set' 
) mf
where not exists (select 1
                        from dim_materialtype
                       where materialtype = mf.parttype);

delete from number_fountain m where m.table_name = 'dim_materialtype';					