UPDATE    dim_fiscalyearvariant fp
       FROM
          T009T t
   SET fp.FiscalYearVariantDescription = t.T009T_LTEXT
   WHERE fp.FiscalYearVariantCode = t.T009T_PERIV AND fp.RowIsCurrent = 1;
