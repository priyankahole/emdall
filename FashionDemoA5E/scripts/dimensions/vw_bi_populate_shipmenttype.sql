INSERT INTO dim_shipmenttype(dim_shipmenttypeid,
                             ShipmentType,
                             Description,
                             RowStartDate,
                             RowEndDate,
                             RowIsCurrent,
                             RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          NULL,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_shipmenttype
               WHERE dim_shipmenttypeid = 1);

UPDATE dim_shipmenttype s
    FROM TVTKT t
   SET s.Description = t.TVTKT_BEZEI,
			s.dw_update_date = current_timestamp
 WHERE s.ShipmentType = t.TVTKT_SHTYP AND s.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_shipmenttype';

insert into number_fountain
select 	'dim_shipmenttype',
	ifnull(max(d.dim_shipmenttypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_shipmenttype d
where d.dim_shipmenttypeid <> 1;  
 
 INSERT INTO dim_shipmenttype(dim_shipmenttypeid,
                                shipmenttype,
                                Description,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_shipmenttype') 
          + row_number() over(),
	  t.TVTKT_SHTYP,
          t.TVTKT_BEZEI,
          current_timestamp,
          1
     FROM TVTKT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_shipmenttype r
               WHERE r.shipmenttype = t.TVTKT_SHTYP AND r.RowIsCurrent = 1);