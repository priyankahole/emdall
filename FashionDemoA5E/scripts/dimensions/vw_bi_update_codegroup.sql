UPDATE    dim_codegroup  FROM QPGT  
   SET SHORTDESCRIPTION = QPGT_KURZTEXT
   WHERE CATALOG = QPGT_KATALOGART
	and CODEGROUP = QPGT_CODEGRUPPE
	and RowIsCurrent = 1
;