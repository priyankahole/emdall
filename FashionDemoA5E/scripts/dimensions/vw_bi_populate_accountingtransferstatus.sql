
INSERT INTO dim_accountingtransferstatus(dim_accountingtransferstatusid, RowIsCurrent,rowstartdate)
SELECT 1, 1,current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_accountingtransferstatus
               WHERE dim_accountingtransferstatusid = 1);

UPDATE  dim_accountingtransferstatus ats
FROM 	DD07T d
SET 	ats.AccountingTransferStatusName = ifnull(DD07T_DDTEXT, 'Not Set'),
		ats.dw_update_date = current_timestamp
WHERE 	ats.AccountingTransferStatusCode = d.DD07T_DOMVALUE
		AND d.DD07T_DOMNAME = 'RFBSK'
		AND d.DD07T_DOMVALUE IS NOT NULL
		AND ats.RowIsCurrent = 1;
				
delete from number_fountain m where m.table_name = 'dim_accountingtransferstatus';

insert into number_fountain		 
select 	'dim_accountingtransferstatus',
		ifnull(max(d.dim_accountingtransferstatusid), 
		ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_accountingtransferstatus d
where d.dim_accountingtransferstatusid <> 1;
			 
INSERT INTO dim_accountingtransferstatus(Dim_AccountingTransferStatusid,
                                                          AccountingTransferStatusCode,
                                                          AccountingTransferStatusName,
                                                          RowStartDate,
                                                          RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_accountingtransferstatus')
          + row_number() over() ,
                  DD07T_DOMVALUE,
        ifnull(DD07T_DDTEXT, 'Not Set'),
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'RFBSK' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_accountingtransferstatus
                    WHERE AccountingTransferStatusCode = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'RFBSK');

delete from number_fountain m where m.table_name = 'dim_accountingtransferstatus';						  