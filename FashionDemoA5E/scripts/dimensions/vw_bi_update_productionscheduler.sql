UPDATE    dim_productionscheduler ps
       FROM
          T024F t
   SET ps.Description = ifnull(T024F_TXT, 'Not Set')
 WHERE ps.RowIsCurrent = 1
      AND ps.ProductionScheduler = t.T024F_FEVOR AND ps.plant = t.T024F_WERKS 
;
