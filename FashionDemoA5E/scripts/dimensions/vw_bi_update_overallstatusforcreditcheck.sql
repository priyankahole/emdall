UPDATE    dim_OverallStatusforCreditCheck oscc
       FROM
          DD07T t
   SET oscc.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE oscc.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'CMGST'
          AND oscc.OverallStatusforCreditCheck = ifnull(t.DD07T_DOMVALUE,'Not Set')
;
