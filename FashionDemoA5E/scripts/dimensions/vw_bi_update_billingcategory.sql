UPDATE    dim_billingcategory bc
       FROM
          DD07T dt
	SET bc.Description = ifnull(dt.DD07T_DDTEXT, 'Not Set')
 WHERE bc.Category = dt.DD07T_DOMVALUE AND bc.RowIsCurrent = 1
 AND dt.DD07T_DOMNAME = 'FKTYP' AND dt.DD07T_DOMNAME IS NOT NULL