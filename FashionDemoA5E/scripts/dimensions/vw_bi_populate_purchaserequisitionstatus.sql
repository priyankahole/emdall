/* ##################################################################################################################*/
/*	Script         : vw_bi_populate_purchaserequisitionstatus																		*/
/*     Created By     : Amar															*/
/*     Created On     : 12 March 2015													*/
/*     Change History																	*/
/*     Date            By        Version           Desc									*/
/*	 3/12/2015		 Amar	   1				 Multi-Statement changed to Script		*/
/*#################################################################################################################### */

UPDATE dim_prstatus s
FROM
DD07T t
SET s.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			s.dw_update_date = current_timestamp
WHERE s.RowIsCurrent = 1
AND t.DD07T_DOMNAME = 'BANST'
AND DD07T_DOMVALUE IS NOT NULL
AND s.prstatus = t.DD07T_DOMVALUE;


INSERT INTO dim_prstatus(Dim_prStatusid,
Prstatus,
Description,
RowStartDate,
RowIsCurrent)
SELECT 			1,
				'Not Set',
				'Not Set',
				current_timestamp,
				1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_prstatus WHERE dim_prstatusId = 1);
   
DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_prstatus';

INSERT INTO NUMBER_FOUNTAIN
select 	'dim_prstatus',
		ifnull(max(d.dim_prstatusId), 
		ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_prstatus d
where d.dim_prstatusId <> 1;

INSERT INTO dim_prstatus(Dim_prStatusid,
Prstatus,
Description,
RowStartDate,
RowIsCurrent)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_prstatus') 
+ row_number() over() ,
ifnull(DD07T_DDTEXT, 'Not Set'),
DD07T_DOMVALUE,
current_timestamp,
1
FROM DD07T
WHERE DD07T_DOMNAME = 'BANST' AND DD07T_DOMVALUE IS NOT NULL
AND NOT EXISTS
(SELECT 1
FROM dim_prstatus
WHERE prstatus = DD07T_DOMVALUE AND DD07T_DOMNAME = 'BANST')
ORDER BY 2 OFFSET 0;

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_prstatus';


