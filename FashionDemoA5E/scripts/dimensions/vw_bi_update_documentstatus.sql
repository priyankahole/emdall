
UPDATE    dim_documentstatus ds
       FROM
          DD07T dt
   SET ds.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE ds.RowIsCurrent = 1
 AND    dt.DD07T_DOMNAME = 'ESTAK'
          AND dt.DD07T_DOMVALUE IS NOT NULL
          AND ds.Status = dt.DD07T_DOMVALUE;

