UPDATE    dim_notificationtype nt
       FROM
          TQ80_T t
   SET nt.NotificationTypeName = t.TQ80_T_QMARTX
 WHERE nt.RowIsCurrent = 1
 AND nt.NotificationTypeCode = t.TQ80_T_QMART
;