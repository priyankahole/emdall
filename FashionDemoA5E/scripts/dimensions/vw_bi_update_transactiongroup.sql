UPDATE    dim_transactiongroup tg
       FROM
          DD07T t
   SET tg.Description = ifnull(DD07T_DDTEXT, 'Not Set')
   WHERE     t.DD07T_DOMNAME = 'TRVOG'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND tg.TransactionGroup = t.DD07T_DOMVALUE
;