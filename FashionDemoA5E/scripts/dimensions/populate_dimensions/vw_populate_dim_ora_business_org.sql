/*insert default row*/
INSERT INTO dim_ora_business_org
(
dim_ora_business_orgid,creation_date,last_update_date,date_from,name,date_to,last_updated_by,created_by,org_information_context,country,
address_line_1,address_line_2,town_or_city,postal_code,region_1,region_2,type,location_update_dt,orginfo_update_date,internal_external_flag,
org_type,organization_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT 
T.dim_ora_business_orgid,
T.creation_date,
T.last_update_date,
T.date_from,
T.name,
T.date_to,
T.last_updated_by,
T.created_by,
T.org_information_context,
T.country,
T.address_line_1,
T.address_line_2,
T.town_or_city,
T.postal_code,
T.region_1,
T.region_2,
T.type,
T.location_update_dt,
T.orginfo_update_date,
T.internal_external_flag,
T.org_type,
T.organization_id,
T.dw_insert_date,
T.dw_update_date,
T.rowstartdate,T.rowenddate,T.rowiscurrent,T.rowchangereason,
T.key_id,
T.source_id
 FROM
(SELECT
1 dim_ora_business_orgid,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
timestamp '0001-01-01 00:00:00.000000' DATE_FROM,
'Not Set' NAME,
timestamp '0001-01-01 00:00:00.000000' DATE_TO,
0 LAST_UPDATED_BY,
0 CREATED_BY,
'Not Set' ORG_INFORMATION_CONTEXT,
'Not Set' COUNTRY,
'Not Set' ADDRESS_LINE_1,
'Not Set' ADDRESS_LINE_2,
'Not Set' TOWN_OR_CITY,
'Not Set' POSTAL_CODE,
'Not Set' REGION_1,
'Not Set' REGION_2,
'Not Set' TYPE,
timestamp '0001-01-01 00:00:00.000000' LOCATION_UPDATE_DT,
timestamp '0001-01-01 00:00:00.000000' ORGINFO_UPDATE_DATE,
'Not Set' INTERNAL_EXTERNAL_FLAG,
'Not Set' org_type,
0 organization_id,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_business_org D
ON T.dim_ora_business_orgid = D.dim_ora_business_orgid
WHERE D.dim_ora_business_orgid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_business_org';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_business_org',IFNULL(MAX(dim_ora_business_orgid),0)
FROM dim_ora_business_org;

/*update dimension rows*/
UPDATE dim_ora_business_org T
FROM dim_ora_business_org D JOIN ORA_BUSINESS_ORG S
ON D.KEY_ID = VARCHAR(S.ORG_TYPE, 200) +'~' +VARCHAR(S.ORGANIZATION_ID, 200) 
SET 
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.DATE_FROM = ifnull(S.DATE_FROM, timestamp '0001-01-01 00:00:00.000000'),
T.NAME = ifnull(S.NAME, 'Not Set'),
T.DATE_TO = ifnull(S.DATE_TO, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.ORG_INFORMATION_CONTEXT = ifnull(S.ORG_INFORMATION_CONTEXT, 'Not Set'),
T.COUNTRY = ifnull(S.COUNTRY, 'Not Set'),
T.ADDRESS_LINE_1 = ifnull(S.ADDRESS_LINE_1, 'Not Set'),
T.ADDRESS_LINE_2 = ifnull(S.ADDRESS_LINE_2, 'Not Set'),
T.TOWN_OR_CITY = ifnull(S.TOWN_OR_CITY, 'Not Set'),
T.POSTAL_CODE = ifnull(S.POSTAL_CODE, 'Not Set'),
T.REGION_1 = ifnull(S.REGION_1, 'Not Set'),
T.REGION_2 = ifnull(S.REGION_2, 'Not Set'),
T.TYPE = ifnull(S.TYPE, 'Not Set'),
T.LOCATION_UPDATE_DT = ifnull(S.LOCATION_UPDATE_DT, timestamp '0001-01-01 00:00:00.000000'),
T.ORGINFO_UPDATE_DATE = ifnull(S.ORGINFO_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.INTERNAL_EXTERNAL_FLAG = ifnull(S.INTERNAL_EXTERNAL_FLAG, 'Not Set'),
T.org_type = ifnull(S.org_type, 'Not Set'),
T.organization_id = S.organization_id,
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
VARCHAR(S.ORG_TYPE, 200) +'~' +VARCHAR(S.ORGANIZATION_ID, 200) = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_business_org
(
dim_ora_business_orgid,creation_date,last_update_date,date_from,name,date_to,last_updated_by,created_by,org_information_context,country,
address_line_1,address_line_2,town_or_city,postal_code,region_1,region_2,type,location_update_dt,orginfo_update_date,internal_external_flag,
org_type,organization_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_business_org' ),0) + ROW_NUMBER() OVER() dim_ora_business_orgid,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.DATE_FROM, timestamp '0001-01-01 00:00:00.000000') DATE_FROM,
ifnull(S.NAME, 'Not Set') NAME,
ifnull(S.DATE_TO, timestamp '0001-01-01 00:00:00.000000') DATE_TO,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.ORG_INFORMATION_CONTEXT, 'Not Set') ORG_INFORMATION_CONTEXT,
ifnull(S.COUNTRY, 'Not Set') COUNTRY,
ifnull(S.ADDRESS_LINE_1, 'Not Set') ADDRESS_LINE_1,
ifnull(S.ADDRESS_LINE_2, 'Not Set') ADDRESS_LINE_2,
ifnull(S.TOWN_OR_CITY, 'Not Set') TOWN_OR_CITY,
ifnull(S.POSTAL_CODE, 'Not Set') POSTAL_CODE,
ifnull(S.REGION_1, 'Not Set') REGION_1,
ifnull(S.REGION_2, 'Not Set') REGION_2,
ifnull(S.TYPE, 'Not Set') TYPE,
ifnull(S.LOCATION_UPDATE_DT, timestamp '0001-01-01 00:00:00.000000') LOCATION_UPDATE_DT,
ifnull(S.ORGINFO_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') ORGINFO_UPDATE_DATE,
ifnull(S.INTERNAL_EXTERNAL_FLAG, 'Not Set') INTERNAL_EXTERNAL_FLAG,
ifnull(S.org_type, 'Not Set') org_type,
S.organization_id,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
VARCHAR(S.ORG_TYPE, 200) +'~' +VARCHAR(S.ORGANIZATION_ID, 200) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_BUSINESS_ORG S LEFT JOIN dim_ora_business_org D ON
D.KEY_ID = VARCHAR(S.ORG_TYPE, 200) +'~' +VARCHAR(S.ORGANIZATION_ID, 200)
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_business_org');