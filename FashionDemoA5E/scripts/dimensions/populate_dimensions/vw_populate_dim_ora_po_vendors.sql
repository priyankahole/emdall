/*insert default row*/
INSERT INTO dim_ora_po_vendors
(
dim_ora_po_vendorsid,
withholding_start_date,withholding_status_lookup_code,type_1099,individual_1099,num_1099,hold_reason,hold_future_payments_flag,hold_all_payments_flag,
invoice_amount_limit,payment_currency_code,invoice_currency_code,payment_priority,pay_group_lookup_code,pay_date_basis_lookup_code,
always_take_disc_flag,set_of_books_id,terms_id,min_order_amount,parent_vendor_id,one_time_flag,customer_num,vendor_type_lookup_code,
employee_id,created_by,creation_date,last_update_login,segment5,segment4,segment3,segment2,enabled_flag,summary_flag,segment1,
vendor_name_alt,vendor_name,last_updated_by,last_update_date,vendor_id,pay_awt_group_id,party_number,ni_number,parent_party_id,
party_id,create_debit_memo_flag,match_option,bank_charge_bearer,global_attribute_category,awt_group_id,allow_awt_flag,check_digits,
tax_reporting_name,exclude_freight_from_discount,validation_number,auto_calculate_interest_flag,vat_registration_num,program_update_date,
attribute15,attribute14,attribute13,attribute12,attribute11,attribute10,attribute9,attribute8,attribute7,attribute6,attribute5,attribute4,
attribute3,attribute2,attribute1,attribute_category,federal_reportable_flag,state_reportable_flag,name_control,tax_verification_date,
hold_unmatched_invoices_flag,allow_unordered_receipts_flag,allow_substitute_receipts_flag,receiving_routing_id,receipt_days_exception_code,
days_late_receipt_allowed,days_early_receipt_allowed,enforce_ship_to_location_code,qty_rcv_exception_code,qty_rcv_tolerance,
receipt_required_flag,inspection_required_flag,terms_date_basis,hold_date,hold_by,purchasing_hold_reason,hold_flag,standard_industry_class,
small_business_flag,women_owned_flag,minority_group_lookup_code,end_date_active,start_date_active,organization_type_lookup_code,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_po_vendorsid,
timestamp '0001-01-01 00:00:00.000000' WITHHOLDING_START_DATE,
'Not Set' WITHHOLDING_STATUS_LOOKUP_CODE,
'Not Set' TYPE_1099,
'Not Set' INDIVIDUAL_1099,
'Not Set' NUM_1099,
'Not Set' HOLD_REASON,
'Not Set' HOLD_FUTURE_PAYMENTS_FLAG,
'Not Set' HOLD_ALL_PAYMENTS_FLAG,
0 INVOICE_AMOUNT_LIMIT,
'Not Set' PAYMENT_CURRENCY_CODE,
'Not Set' INVOICE_CURRENCY_CODE,
0 PAYMENT_PRIORITY,
'Not Set' PAY_GROUP_LOOKUP_CODE,
'Not Set' PAY_DATE_BASIS_LOOKUP_CODE,
'Not Set' ALWAYS_TAKE_DISC_FLAG,
0 SET_OF_BOOKS_ID,
0 TERMS_ID,
0 MIN_ORDER_AMOUNT,
0 PARENT_VENDOR_ID,
'Not Set' ONE_TIME_FLAG,
'Not Set' CUSTOMER_NUM,
'Not Set' VENDOR_TYPE_LOOKUP_CODE,
0 EMPLOYEE_ID,
0 CREATED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 LAST_UPDATE_LOGIN,
'Not Set' SEGMENT5,
'Not Set' SEGMENT4,
'Not Set' SEGMENT3,
'Not Set' SEGMENT2,
'Not Set' ENABLED_FLAG,
'Not Set' SUMMARY_FLAG,
'Not Set' SEGMENT1,
'Not Set' VENDOR_NAME_ALT,
'Not Set' VENDOR_NAME,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 VENDOR_ID,
0 PAY_AWT_GROUP_ID,
'Not Set' PARTY_NUMBER,
'Not Set' NI_NUMBER,
0 PARENT_PARTY_ID,
0 PARTY_ID,
'Not Set' CREATE_DEBIT_MEMO_FLAG,
'Not Set' MATCH_OPTION,
'Not Set' BANK_CHARGE_BEARER,
'Not Set' GLOBAL_ATTRIBUTE_CATEGORY,
0 AWT_GROUP_ID,
'Not Set' ALLOW_AWT_FLAG,
'Not Set' CHECK_DIGITS,
'Not Set' TAX_REPORTING_NAME,
'Not Set' EXCLUDE_FREIGHT_FROM_DISCOUNT,
'Not Set' VALIDATION_NUMBER,
'Not Set' AUTO_CALCULATE_INTEREST_FLAG,
'Not Set' VAT_REGISTRATION_NUM,
timestamp '0001-01-01 00:00:00.000000' PROGRAM_UPDATE_DATE,
'Not Set' ATTRIBUTE15,
'Not Set' ATTRIBUTE14,
'Not Set' ATTRIBUTE13,
'Not Set' ATTRIBUTE12,
'Not Set' ATTRIBUTE11,
'Not Set' ATTRIBUTE10,
'Not Set' ATTRIBUTE9,
'Not Set' ATTRIBUTE8,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE4,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE1,
'Not Set' ATTRIBUTE_CATEGORY,
'Not Set' FEDERAL_REPORTABLE_FLAG,
'Not Set' STATE_REPORTABLE_FLAG,
'Not Set' NAME_CONTROL,
timestamp '0001-01-01 00:00:00.000000' TAX_VERIFICATION_DATE,
'Not Set' HOLD_UNMATCHED_INVOICES_FLAG,
'Not Set' ALLOW_UNORDERED_RECEIPTS_FLAG,
'Not Set' ALLOW_SUBSTITUTE_RECEIPTS_FLAG,
0 RECEIVING_ROUTING_ID,
'Not Set' RECEIPT_DAYS_EXCEPTION_CODE,
0 DAYS_LATE_RECEIPT_ALLOWED,
0 DAYS_EARLY_RECEIPT_ALLOWED,
'Not Set' ENFORCE_SHIP_TO_LOCATION_CODE,
'Not Set' QTY_RCV_EXCEPTION_CODE,
0 QTY_RCV_TOLERANCE,
'Not Set' RECEIPT_REQUIRED_FLAG,
'Not Set' INSPECTION_REQUIRED_FLAG,
'Not Set' TERMS_DATE_BASIS,
timestamp '0001-01-01 00:00:00.000000' HOLD_DATE,
0 HOLD_BY,
'Not Set' PURCHASING_HOLD_REASON,
'Not Set' HOLD_FLAG,
'Not Set' STANDARD_INDUSTRY_CLASS,
'Not Set' SMALL_BUSINESS_FLAG,
'Not Set' WOMEN_OWNED_FLAG,
'Not Set' MINORITY_GROUP_LOOKUP_CODE,
timestamp '0001-01-01 00:00:00.000000' END_DATE_ACTIVE,
timestamp '0001-01-01 00:00:00.000000' START_DATE_ACTIVE,
'Not Set' ORGANIZATION_TYPE_LOOKUP_CODE,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_po_vendors D
ON T.dim_ora_po_vendorsid = D.dim_ora_po_vendorsid 
WHERE D.dim_ora_po_vendorsid IS NULL;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_po_vendors';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_po_vendors',IFNULL(MAX(dim_ora_po_vendorsid),0)
FROM dim_ora_po_vendors;

/*update dimension rows*/
UPDATE dim_ora_po_vendors T	FROM ORA_PURCH_VENDOR S
SET 
T.WITHHOLDING_START_DATE = ifnull(S.WITHHOLDING_START_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.WITHHOLDING_STATUS_LOOKUP_CODE = ifnull(S.WITHHOLDING_STATUS_LOOKUP_CODE, 'Not Set'),
T.TYPE_1099 = ifnull(S.TYPE_1099, 'Not Set'),
T.INDIVIDUAL_1099 = ifnull(S.INDIVIDUAL_1099, 'Not Set'),
T.NUM_1099 = ifnull(S.NUM_1099, 'Not Set'),
T.HOLD_REASON = ifnull(S.HOLD_REASON, 'Not Set'),
T.HOLD_FUTURE_PAYMENTS_FLAG = ifnull(S.HOLD_FUTURE_PAYMENTS_FLAG, 'Not Set'),
T.HOLD_ALL_PAYMENTS_FLAG = ifnull(S.HOLD_ALL_PAYMENTS_FLAG, 'Not Set'),
T.INVOICE_AMOUNT_LIMIT = ifnull(S.INVOICE_AMOUNT_LIMIT, 0),
T.PAYMENT_CURRENCY_CODE = ifnull(S.PAYMENT_CURRENCY_CODE, 'Not Set'),
T.INVOICE_CURRENCY_CODE = ifnull(S.INVOICE_CURRENCY_CODE, 'Not Set'),
T.PAYMENT_PRIORITY = ifnull(S.PAYMENT_PRIORITY, 0),
T.PAY_GROUP_LOOKUP_CODE = ifnull(S.PAY_GROUP_LOOKUP_CODE, 'Not Set'),
T.PAY_DATE_BASIS_LOOKUP_CODE = ifnull(S.PAY_DATE_BASIS_LOOKUP_CODE, 'Not Set'),
T.ALWAYS_TAKE_DISC_FLAG = ifnull(S.ALWAYS_TAKE_DISC_FLAG, 'Not Set'),
T.SET_OF_BOOKS_ID = ifnull(S.SET_OF_BOOKS_ID, 0),
T.TERMS_ID = ifnull(S.TERMS_ID, 0),
T.MIN_ORDER_AMOUNT = ifnull(S.MIN_ORDER_AMOUNT, 0),
T.PARENT_VENDOR_ID = ifnull(S.PARENT_VENDOR_ID, 0),
T.ONE_TIME_FLAG = ifnull(S.ONE_TIME_FLAG, 'Not Set'),
T.CUSTOMER_NUM = ifnull(S.CUSTOMER_NUM, 'Not Set'),
T.VENDOR_TYPE_LOOKUP_CODE = ifnull(S.VENDOR_TYPE_LOOKUP_CODE, 'Not Set'),
T.EMPLOYEE_ID = ifnull(S.EMPLOYEE_ID, 0),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.SEGMENT5 = ifnull(S.SEGMENT5, 'Not Set'),
T.SEGMENT4 = ifnull(S.SEGMENT4, 'Not Set'),
T.SEGMENT3 = ifnull(S.SEGMENT3, 'Not Set'),
T.SEGMENT2 = ifnull(S.SEGMENT2, 'Not Set'),
T.ENABLED_FLAG = ifnull(S.ENABLED_FLAG, 'Not Set'),
T.SUMMARY_FLAG = ifnull(S.SUMMARY_FLAG, 'Not Set'),
T.SEGMENT1 = ifnull(S.SEGMENT1, 'Not Set'),
T.VENDOR_NAME_ALT = ifnull(S.VENDOR_NAME_ALT, 'Not Set'),
T.VENDOR_NAME = ifnull(S.VENDOR_NAME, 'Not Set'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.VENDOR_ID = S.VENDOR_ID,
T.PAY_AWT_GROUP_ID = ifnull(S.PAY_AWT_GROUP_ID, 0),
T.PARTY_NUMBER = ifnull(S.PARTY_NUMBER, 'Not Set'),
T.NI_NUMBER = ifnull(S.NI_NUMBER, 'Not Set'),
T.PARENT_PARTY_ID = ifnull(S.PARENT_PARTY_ID, 0),
T.PARTY_ID = ifnull(S.PARTY_ID, 0),
T.CREATE_DEBIT_MEMO_FLAG = ifnull(S.CREATE_DEBIT_MEMO_FLAG, 'Not Set'),
T.MATCH_OPTION = ifnull(S.MATCH_OPTION, 'Not Set'),
T.BANK_CHARGE_BEARER = ifnull(S.BANK_CHARGE_BEARER, 'Not Set'),
T.GLOBAL_ATTRIBUTE_CATEGORY = ifnull(S.GLOBAL_ATTRIBUTE_CATEGORY, 'Not Set'),
T.AWT_GROUP_ID = ifnull(S.AWT_GROUP_ID, 0),
T.ALLOW_AWT_FLAG = ifnull(S.ALLOW_AWT_FLAG, 'Not Set'),
T.CHECK_DIGITS = ifnull(S.CHECK_DIGITS, 'Not Set'),
T.TAX_REPORTING_NAME = ifnull(S.TAX_REPORTING_NAME, 'Not Set'),
T.EXCLUDE_FREIGHT_FROM_DISCOUNT = ifnull(S.EXCLUDE_FREIGHT_FROM_DISCOUNT, 'Not Set'),
T.VALIDATION_NUMBER = ifnull(S.VALIDATION_NUMBER, 'Not Set'),
T.AUTO_CALCULATE_INTEREST_FLAG = ifnull(S.AUTO_CALCULATE_INTEREST_FLAG, 'Not Set'),
T.VAT_REGISTRATION_NUM = ifnull(S.VAT_REGISTRATION_NUM, 'Not Set'),
T.PROGRAM_UPDATE_DATE = ifnull(S.PROGRAM_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.ATTRIBUTE15 = ifnull(S.ATTRIBUTE15, 'Not Set'),
T.ATTRIBUTE14 = ifnull(S.ATTRIBUTE14, 'Not Set'),
T.ATTRIBUTE13 = ifnull(S.ATTRIBUTE13, 'Not Set'),
T.ATTRIBUTE12 = ifnull(S.ATTRIBUTE12, 'Not Set'),
T.ATTRIBUTE11 = ifnull(S.ATTRIBUTE11, 'Not Set'),
T.ATTRIBUTE10 = ifnull(S.ATTRIBUTE10, 'Not Set'),
T.ATTRIBUTE9 = ifnull(S.ATTRIBUTE9, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set'),
T.FEDERAL_REPORTABLE_FLAG = ifnull(S.FEDERAL_REPORTABLE_FLAG, 'Not Set'),
T.STATE_REPORTABLE_FLAG = ifnull(S.STATE_REPORTABLE_FLAG, 'Not Set'),
T.NAME_CONTROL = ifnull(S.NAME_CONTROL, 'Not Set'),
T.TAX_VERIFICATION_DATE = ifnull(S.TAX_VERIFICATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.HOLD_UNMATCHED_INVOICES_FLAG = ifnull(S.HOLD_UNMATCHED_INVOICES_FLAG, 'Not Set'),
T.ALLOW_UNORDERED_RECEIPTS_FLAG = ifnull(S.ALLOW_UNORDERED_RECEIPTS_FLAG, 'Not Set'),
T.ALLOW_SUBSTITUTE_RECEIPTS_FLAG = ifnull(S.ALLOW_SUBSTITUTE_RECEIPTS_FLAG, 'Not Set'),
T.RECEIVING_ROUTING_ID = ifnull(S.RECEIVING_ROUTING_ID, 0),
T.RECEIPT_DAYS_EXCEPTION_CODE = ifnull(S.RECEIPT_DAYS_EXCEPTION_CODE, 'Not Set'),
T.DAYS_LATE_RECEIPT_ALLOWED = ifnull(S.DAYS_LATE_RECEIPT_ALLOWED, 0),
T.DAYS_EARLY_RECEIPT_ALLOWED = ifnull(S.DAYS_EARLY_RECEIPT_ALLOWED, 0),
T.ENFORCE_SHIP_TO_LOCATION_CODE = ifnull(S.ENFORCE_SHIP_TO_LOCATION_CODE, 'Not Set'),
T.QTY_RCV_EXCEPTION_CODE = ifnull(S.QTY_RCV_EXCEPTION_CODE, 'Not Set'),
T.QTY_RCV_TOLERANCE = ifnull(S.QTY_RCV_TOLERANCE, 0),
T.RECEIPT_REQUIRED_FLAG = ifnull(S.RECEIPT_REQUIRED_FLAG, 'Not Set'),
T.INSPECTION_REQUIRED_FLAG = ifnull(S.INSPECTION_REQUIRED_FLAG, 'Not Set'),
T.TERMS_DATE_BASIS = ifnull(S.TERMS_DATE_BASIS, 'Not Set'),
T.HOLD_DATE = ifnull(S.HOLD_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.HOLD_BY = ifnull(S.HOLD_BY, 0),
T.PURCHASING_HOLD_REASON = ifnull(S.PURCHASING_HOLD_REASON, 'Not Set'),
T.HOLD_FLAG = ifnull(S.HOLD_FLAG, 'Not Set'),
T.STANDARD_INDUSTRY_CLASS = ifnull(S.STANDARD_INDUSTRY_CLASS, 'Not Set'),
T.SMALL_BUSINESS_FLAG = ifnull(S.SMALL_BUSINESS_FLAG, 'Not Set'),
T.WOMEN_OWNED_FLAG = ifnull(S.WOMEN_OWNED_FLAG, 'Not Set'),
T.MINORITY_GROUP_LOOKUP_CODE = ifnull(S.MINORITY_GROUP_LOOKUP_CODE, 'Not Set'),
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.ORGANIZATION_TYPE_LOOKUP_CODE = ifnull(S.ORGANIZATION_TYPE_LOOKUP_CODE, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE T.KEY_ID = S.VENDOR_ID;

/*insert new rows*/
INSERT INTO dim_ora_po_vendors
(
dim_ora_po_vendorsid,
withholding_start_date,withholding_status_lookup_code,type_1099,individual_1099,num_1099,hold_reason,hold_future_payments_flag,hold_all_payments_flag,
invoice_amount_limit,payment_currency_code,invoice_currency_code,payment_priority,pay_group_lookup_code,pay_date_basis_lookup_code,
always_take_disc_flag,set_of_books_id,terms_id,min_order_amount,parent_vendor_id,one_time_flag,customer_num,vendor_type_lookup_code,
employee_id,created_by,creation_date,last_update_login,segment5,segment4,segment3,segment2,enabled_flag,summary_flag,segment1,
vendor_name_alt,vendor_name,last_updated_by,last_update_date,vendor_id,pay_awt_group_id,party_number,ni_number,parent_party_id,
party_id,create_debit_memo_flag,match_option,bank_charge_bearer,global_attribute_category,awt_group_id,allow_awt_flag,check_digits,
tax_reporting_name,exclude_freight_from_discount,validation_number,auto_calculate_interest_flag,vat_registration_num,program_update_date,
attribute15,attribute14,attribute13,attribute12,attribute11,attribute10,attribute9,attribute8,attribute7,attribute6,attribute5,attribute4,
attribute3,attribute2,attribute1,attribute_category,federal_reportable_flag,state_reportable_flag,name_control,tax_verification_date,
hold_unmatched_invoices_flag,allow_unordered_receipts_flag,allow_substitute_receipts_flag,receiving_routing_id,receipt_days_exception_code,
days_late_receipt_allowed,days_early_receipt_allowed,enforce_ship_to_location_code,qty_rcv_exception_code,qty_rcv_tolerance,
receipt_required_flag,inspection_required_flag,terms_date_basis,hold_date,hold_by,purchasing_hold_reason,hold_flag,standard_industry_class,
small_business_flag,women_owned_flag,minority_group_lookup_code,end_date_active,start_date_active,organization_type_lookup_code,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_po_vendors' ),0) + ROW_NUMBER() OVER() dim_ora_po_vendorsid,
ifnull(S.WITHHOLDING_START_DATE, timestamp '0001-01-01 00:00:00.000000') WITHHOLDING_START_DATE,
ifnull(S.WITHHOLDING_STATUS_LOOKUP_CODE, 'Not Set') WITHHOLDING_STATUS_LOOKUP_CODE,
ifnull(S.TYPE_1099, 'Not Set') TYPE_1099,
ifnull(S.INDIVIDUAL_1099, 'Not Set') INDIVIDUAL_1099,
ifnull(S.NUM_1099, 'Not Set') NUM_1099,
ifnull(S.HOLD_REASON, 'Not Set') HOLD_REASON,
ifnull(S.HOLD_FUTURE_PAYMENTS_FLAG, 'Not Set') HOLD_FUTURE_PAYMENTS_FLAG,
ifnull(S.HOLD_ALL_PAYMENTS_FLAG, 'Not Set') HOLD_ALL_PAYMENTS_FLAG,
ifnull(S.INVOICE_AMOUNT_LIMIT, 0) INVOICE_AMOUNT_LIMIT,
ifnull(S.PAYMENT_CURRENCY_CODE, 'Not Set') PAYMENT_CURRENCY_CODE,
ifnull(S.INVOICE_CURRENCY_CODE, 'Not Set') INVOICE_CURRENCY_CODE,
ifnull(S.PAYMENT_PRIORITY, 0) PAYMENT_PRIORITY,
ifnull(S.PAY_GROUP_LOOKUP_CODE, 'Not Set') PAY_GROUP_LOOKUP_CODE,
ifnull(S.PAY_DATE_BASIS_LOOKUP_CODE, 'Not Set') PAY_DATE_BASIS_LOOKUP_CODE,
ifnull(S.ALWAYS_TAKE_DISC_FLAG, 'Not Set') ALWAYS_TAKE_DISC_FLAG,
ifnull(S.SET_OF_BOOKS_ID, 0) SET_OF_BOOKS_ID,
ifnull(S.TERMS_ID, 0) TERMS_ID,
ifnull(S.MIN_ORDER_AMOUNT, 0) MIN_ORDER_AMOUNT,
ifnull(S.PARENT_VENDOR_ID, 0) PARENT_VENDOR_ID,
ifnull(S.ONE_TIME_FLAG, 'Not Set') ONE_TIME_FLAG,
ifnull(S.CUSTOMER_NUM, 'Not Set') CUSTOMER_NUM,
ifnull(S.VENDOR_TYPE_LOOKUP_CODE, 'Not Set') VENDOR_TYPE_LOOKUP_CODE,
ifnull(S.EMPLOYEE_ID, 0) EMPLOYEE_ID,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.SEGMENT5, 'Not Set') SEGMENT5,
ifnull(S.SEGMENT4, 'Not Set') SEGMENT4,
ifnull(S.SEGMENT3, 'Not Set') SEGMENT3,
ifnull(S.SEGMENT2, 'Not Set') SEGMENT2,
ifnull(S.ENABLED_FLAG, 'Not Set') ENABLED_FLAG,
ifnull(S.SUMMARY_FLAG, 'Not Set') SUMMARY_FLAG,
ifnull(S.SEGMENT1, 'Not Set') SEGMENT1,
ifnull(S.VENDOR_NAME_ALT, 'Not Set') VENDOR_NAME_ALT,
ifnull(S.VENDOR_NAME, 'Not Set') VENDOR_NAME,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
S.VENDOR_ID,
ifnull(S.PAY_AWT_GROUP_ID, 0) PAY_AWT_GROUP_ID,
ifnull(S.PARTY_NUMBER, 'Not Set') PARTY_NUMBER,
ifnull(S.NI_NUMBER, 'Not Set') NI_NUMBER,
ifnull(S.PARENT_PARTY_ID, 0) PARENT_PARTY_ID,
ifnull(S.PARTY_ID, 0) PARTY_ID,
ifnull(S.CREATE_DEBIT_MEMO_FLAG, 'Not Set') CREATE_DEBIT_MEMO_FLAG,
ifnull(S.MATCH_OPTION, 'Not Set') MATCH_OPTION,
ifnull(S.BANK_CHARGE_BEARER, 'Not Set') BANK_CHARGE_BEARER,
ifnull(S.GLOBAL_ATTRIBUTE_CATEGORY, 'Not Set') GLOBAL_ATTRIBUTE_CATEGORY,
ifnull(S.AWT_GROUP_ID, 0) AWT_GROUP_ID,
ifnull(S.ALLOW_AWT_FLAG, 'Not Set') ALLOW_AWT_FLAG,
ifnull(S.CHECK_DIGITS, 'Not Set') CHECK_DIGITS,
ifnull(S.TAX_REPORTING_NAME, 'Not Set') TAX_REPORTING_NAME,
ifnull(S.EXCLUDE_FREIGHT_FROM_DISCOUNT, 'Not Set') EXCLUDE_FREIGHT_FROM_DISCOUNT,
ifnull(S.VALIDATION_NUMBER, 'Not Set') VALIDATION_NUMBER,
ifnull(S.AUTO_CALCULATE_INTEREST_FLAG, 'Not Set') AUTO_CALCULATE_INTEREST_FLAG,
ifnull(S.VAT_REGISTRATION_NUM, 'Not Set') VAT_REGISTRATION_NUM,
ifnull(S.PROGRAM_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') PROGRAM_UPDATE_DATE,
ifnull(S.ATTRIBUTE15, 'Not Set') ATTRIBUTE15,
ifnull(S.ATTRIBUTE14, 'Not Set') ATTRIBUTE14,
ifnull(S.ATTRIBUTE13, 'Not Set') ATTRIBUTE13,
ifnull(S.ATTRIBUTE12, 'Not Set') ATTRIBUTE12,
ifnull(S.ATTRIBUTE11, 'Not Set') ATTRIBUTE11,
ifnull(S.ATTRIBUTE10, 'Not Set') ATTRIBUTE10,
ifnull(S.ATTRIBUTE9, 'Not Set') ATTRIBUTE9,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set') ATTRIBUTE_CATEGORY,
ifnull(S.FEDERAL_REPORTABLE_FLAG, 'Not Set') FEDERAL_REPORTABLE_FLAG,
ifnull(S.STATE_REPORTABLE_FLAG, 'Not Set') STATE_REPORTABLE_FLAG,
ifnull(S.NAME_CONTROL, 'Not Set') NAME_CONTROL,
ifnull(S.TAX_VERIFICATION_DATE, timestamp '0001-01-01 00:00:00.000000') TAX_VERIFICATION_DATE,
ifnull(S.HOLD_UNMATCHED_INVOICES_FLAG, 'Not Set') HOLD_UNMATCHED_INVOICES_FLAG,
ifnull(S.ALLOW_UNORDERED_RECEIPTS_FLAG, 'Not Set') ALLOW_UNORDERED_RECEIPTS_FLAG,
ifnull(S.ALLOW_SUBSTITUTE_RECEIPTS_FLAG, 'Not Set') ALLOW_SUBSTITUTE_RECEIPTS_FLAG,
ifnull(S.RECEIVING_ROUTING_ID, 0) RECEIVING_ROUTING_ID,
ifnull(S.RECEIPT_DAYS_EXCEPTION_CODE, 'Not Set') RECEIPT_DAYS_EXCEPTION_CODE,
ifnull(S.DAYS_LATE_RECEIPT_ALLOWED, 0) DAYS_LATE_RECEIPT_ALLOWED,
ifnull(S.DAYS_EARLY_RECEIPT_ALLOWED, 0) DAYS_EARLY_RECEIPT_ALLOWED,
ifnull(S.ENFORCE_SHIP_TO_LOCATION_CODE, 'Not Set') ENFORCE_SHIP_TO_LOCATION_CODE,
ifnull(S.QTY_RCV_EXCEPTION_CODE, 'Not Set') QTY_RCV_EXCEPTION_CODE,
ifnull(S.QTY_RCV_TOLERANCE, 0) QTY_RCV_TOLERANCE,
ifnull(S.RECEIPT_REQUIRED_FLAG, 'Not Set') RECEIPT_REQUIRED_FLAG,
ifnull(S.INSPECTION_REQUIRED_FLAG, 'Not Set') INSPECTION_REQUIRED_FLAG,
ifnull(S.TERMS_DATE_BASIS, 'Not Set') TERMS_DATE_BASIS,
ifnull(S.HOLD_DATE, timestamp '0001-01-01 00:00:00.000000') HOLD_DATE,
ifnull(S.HOLD_BY, 0) HOLD_BY,
ifnull(S.PURCHASING_HOLD_REASON, 'Not Set') PURCHASING_HOLD_REASON,
ifnull(S.HOLD_FLAG, 'Not Set') HOLD_FLAG,
ifnull(S.STANDARD_INDUSTRY_CLASS, 'Not Set') STANDARD_INDUSTRY_CLASS,
ifnull(S.SMALL_BUSINESS_FLAG, 'Not Set') SMALL_BUSINESS_FLAG,
ifnull(S.WOMEN_OWNED_FLAG, 'Not Set') WOMEN_OWNED_FLAG,
ifnull(S.MINORITY_GROUP_LOOKUP_CODE, 'Not Set') MINORITY_GROUP_LOOKUP_CODE,
ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.ORGANIZATION_TYPE_LOOKUP_CODE, 'Not Set') ORGANIZATION_TYPE_LOOKUP_CODE,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.VENDOR_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_PURCH_VENDOR S LEFT JOIN dim_ora_po_vendors D ON
D.KEY_ID = S.VENDOR_ID
WHERE 
D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_po_vendors');