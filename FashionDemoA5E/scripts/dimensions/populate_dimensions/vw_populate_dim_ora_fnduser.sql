/*insert default row*/
INSERT INTO dim_ora_fnduser
(
dim_ora_fnduserid,
gcn_code_combination_id,user_guid,
security_group_id,web_password,supplier_id,customer_id,fax,email_address,employee_id,last_logon_date,description,end_date,start_date,
session_number,last_update_login,created_by,creation_date,last_updated_by,last_update_date,user_name,person_party_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
select T.* from (SELECT
1 dim_ora_fnduserid,
0 GCN_CODE_COMBINATION_ID,
'Not Set' USER_GUID,
0 SECURITY_GROUP_ID,
'Not Set' WEB_PASSWORD,
0 SUPPLIER_ID,
0 CUSTOMER_ID,
'Not Set' FAX,
'Not Set' EMAIL_ADDRESS,
0 EMPLOYEE_ID,
timestamp '0001-01-01 00:00:00.000000' LAST_LOGON_DATE,
'Not Set' DESCRIPTION,
timestamp '0001-01-01 00:00:00.000000' END_DATE,
timestamp '0001-01-01 00:00:00.000000' START_DATE,
0 SESSION_NUMBER,
0 LAST_UPDATE_LOGIN,
0 CREATED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
'Not Set' USER_NAME,
0 PERSON_PARTY_ID,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_fnduser D
on T.dim_ora_fnduserid = D.dim_ora_fnduserid
where D.dim_ora_fnduserid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_fnduser';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_fnduser',IFNULL(MAX(dim_ora_fnduserid),0)
FROM dim_ora_fnduser;

/*update dimension rows*/
UPDATE dim_ora_fnduser T
FROM dim_ora_fnduser D JOIN ORA_FND_USERS S
ON D.KEY_ID = S.USER_ID
SET 
T.GCN_CODE_COMBINATION_ID = ifnull(S.GCN_CODE_COMBINATION_ID, 0),
T.USER_GUID = ifnull(S.USER_GUID, 'Not Set'),
T.SECURITY_GROUP_ID = ifnull(S.SECURITY_GROUP_ID, 0),
T.WEB_PASSWORD = ifnull(S.WEB_PASSWORD, 'Not Set'),
T.SUPPLIER_ID = ifnull(S.SUPPLIER_ID, 0),
T.CUSTOMER_ID = ifnull(S.CUSTOMER_ID, 0),
T.FAX = ifnull(S.FAX, 'Not Set'),
T.EMAIL_ADDRESS = ifnull(S.EMAIL_ADDRESS, 'Not Set'),
T.EMPLOYEE_ID = ifnull(S.EMPLOYEE_ID, 0),
T.LAST_LOGON_DATE = ifnull(S.LAST_LOGON_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.END_DATE = ifnull(S.END_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.START_DATE = ifnull(S.START_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.SESSION_NUMBER = ifnull(S.SESSION_NUMBER, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.USER_NAME = ifnull(S.USER_NAME, 'Not Set'), 
T.PERSON_PARTY_ID = ifnull(S.PERSON_PARTY_ID, 0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE S.USER_ID  = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_fnduser
(
dim_ora_fnduserid,
gcn_code_combination_id,user_guid,
security_group_id,web_password,supplier_id,customer_id,fax,email_address,employee_id,last_logon_date,description,end_date,start_date,
session_number,last_update_login,created_by,creation_date,last_updated_by,last_update_date,user_name,person_party_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_fnduser' ),0) + ROW_NUMBER() OVER() dim_ora_fnduserid,
ifnull(S.GCN_CODE_COMBINATION_ID, 0) GCN_CODE_COMBINATION_ID,
ifnull(S.USER_GUID, 'Not Set') USER_GUID,
ifnull(S.SECURITY_GROUP_ID, 0) SECURITY_GROUP_ID,
ifnull(S.WEB_PASSWORD, 'Not Set') WEB_PASSWORD,
ifnull(S.SUPPLIER_ID, 0) SUPPLIER_ID,
ifnull(S.CUSTOMER_ID, 0) CUSTOMER_ID,
ifnull(S.FAX, 'Not Set') FAX,
ifnull(S.EMAIL_ADDRESS, 'Not Set') EMAIL_ADDRESS,
ifnull(S.EMPLOYEE_ID, 0) EMPLOYEE_ID,
ifnull(S.LAST_LOGON_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_LOGON_DATE,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.END_DATE, timestamp '0001-01-01 00:00:00.000000') END_DATE,
ifnull(S.START_DATE, timestamp '0001-01-01 00:00:00.000000') START_DATE,
ifnull(S.SESSION_NUMBER, 0) SESSION_NUMBER,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.USER_NAME, 'Not Set') USER_NAME,
ifnull(S.PERSON_PARTY_ID, 0) PERSON_PARTY_ID,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.USER_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_FND_USERS S LEFT JOIN dim_ora_fnduser D ON
D.KEY_ID = S.USER_ID
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_fnduser');