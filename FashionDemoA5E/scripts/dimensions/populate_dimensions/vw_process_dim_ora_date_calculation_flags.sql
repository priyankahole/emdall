/*set session authorization oracle_data*/

/*select * from dim_date*/
/*ALTER table dim_date*/
/*add column yearflag  varchar(30) NULL*/
/*ALTER table dim_date*/
/*add column quarterflag varchar(30) NULL*/
/*ALTER table dim_date*/
/*add column monthflag  varchar(30) NULL*/
/*ALTER table dim_date*/
/*add column weekflag  varchar(30) NULL*/
/*ALTER table dim_date*/
/*add column dayflag  varchar(30) NULL*/

/*select * from dim_date*/

/*dim_date yearflag dayflag quarterflag monthflag weekflag*/
update dim_date
set 
yearflag    = 'Not Set',
quarterflag = 'Not Set',
monthflag   = 'Not Set',
weekflag    = 'Not Set',
dayflag     = 'Not Set';

/*yearflag*/
update dim_date
set 
yearflag = 'Current'
where year(current_timestamp)= calendaryear;

update dim_date
set 
yearflag = 'Previous'
where year(current_timestamp)-1= calendaryear;

update dim_date
set 
yearflag = 'Next'
where year(current_timestamp)+1= calendaryear;

/*dayflag*/
update dim_date
set 
dayflag = 'Current'
where ansidate(current_timestamp)= datevalue;

update dim_date
set 
dayflag = 'Previous'
where ansidate(current_timestamp)-1= datevalue;

update dim_date
set 
dayflag = 'Next'
where ansidate(current_timestamp)+1= datevalue;

/*create temp table for date calculations*/
DROP IF EXISTS dim_date_calculations_tmp;


CREATE TABLE dim_date_calculations_tmp as
select 
dim_dateid, datevalue,
DENSE_RANK() over(order by calendaryear asc, calendarquarter asc) RANK_quarter,
DENSE_RANK() over(order by calendaryear asc, calendarmonthnumber asc) RANK_month,
DENSE_RANK() over(order by calendaryear asc, calendarweek asc) RANK_week
from dim_date
order by datevalue;

/*quarterflag*/
update dim_date d
from dim_date_calculations_tmp t1
set 
d.quarterflag = 'Current'
where d.datevalue =t1.datevalue and 
rank_quarter = (select distinct rank_quarter from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);

update dim_date d
from dim_date_calculations_tmp t1
set 
d.quarterflag = 'Previous'
where d.datevalue =t1.datevalue and 
rank_quarter+1 = (select distinct rank_quarter from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);

update dim_date d
from dim_date_calculations_tmp t1
set 
d.quarterflag = 'Next'
where d.datevalue =t1.datevalue and 
rank_quarter-1 = (select distinct rank_quarter from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);


/*monthflag*/
update dim_date d
from dim_date_calculations_tmp t1
set 
d.monthflag = 'Current'
where d.datevalue =t1.datevalue and 
rank_month = (select distinct rank_month from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);

update dim_date d
from dim_date_calculations_tmp t1
set 
d.monthflag = 'Previous'
where d.datevalue =t1.datevalue and 
rank_month+1 = (select distinct rank_month from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);

update dim_date d
from dim_date_calculations_tmp t1
set 
d.monthflag = 'Next'
where d.datevalue =t1.datevalue and 
rank_month-1 = (select distinct rank_month from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);

/*weekflag*/
update dim_date d
from dim_date_calculations_tmp t1
set 
d.weekflag = 'Current'
where d.datevalue =t1.datevalue and 
RANK_week = (select distinct RANK_week from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);

update dim_date d
from dim_date_calculations_tmp t1
set 
d.weekflag = 'Previous'
where d.datevalue =t1.datevalue and 
RANK_week+1 = (select distinct RANK_week from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);

update dim_date d
from dim_date_calculations_tmp t1
set 
d.weekflag = 'Next'
where d.datevalue =t1.datevalue and 
RANK_week-1 = (select distinct RANK_week from dim_date_calculations_tmp where ansidate(current_timestamp) = datevalue);

/*drop temp table*/
DROP IF EXISTS dim_date_calculations_tmp;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_date');