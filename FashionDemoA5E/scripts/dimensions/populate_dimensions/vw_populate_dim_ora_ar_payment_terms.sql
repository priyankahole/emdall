/*insert default row*/
INSERT INTO dim_ora_ar_payment_terms
(
dim_ora_ar_payment_termsid,
last_update_date,last_updated_by,creation_date,created_by,last_update_login,credit_check_flag,due_cutoff_day,printing_lead_days,
start_date_active,end_date_active,attribute_category,base_amount,calc_discount_on_lines_flag,first_installment_code,in_use,
partial_discount_flag,attribute1,attribute2,attribute3,attribute4,attribute5,attribute6,attribute7,attribute8,attribute9,
attribute10,attribute11,attribute12,attribute13,attribute14,attribute15,name,description,prepayment_flg,billing_cycle_id,cycle_name,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_ar_payment_termsid,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' CREDIT_CHECK_FLAG,
0 DUE_CUTOFF_DAY,
0 PRINTING_LEAD_DAYS,
timestamp '0001-01-01 00:00:00.000000' START_DATE_ACTIVE,
timestamp '0001-01-01 00:00:00.000000' END_DATE_ACTIVE,
'Not Set' ATTRIBUTE_CATEGORY,
0 BASE_AMOUNT,
'Not Set' CALC_DISCOUNT_ON_LINES_FLAG,
'Not Set' FIRST_INSTALLMENT_CODE,
'Not Set' IN_USE,
'Not Set' PARTIAL_DISCOUNT_FLAG,
'Not Set' ATTRIBUTE1,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE4,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE8,
'Not Set' ATTRIBUTE9,
'Not Set' ATTRIBUTE10,
'Not Set' ATTRIBUTE11,
'Not Set' ATTRIBUTE12,
'Not Set' ATTRIBUTE13,
'Not Set' ATTRIBUTE14,
'Not Set' ATTRIBUTE15,
'Not Set' NAME,
'Not Set' DESCRIPTION,
'Not Set' PREPAYMENT_FLG,
0 BILLING_CYCLE_ID,
'Not Set' CYCLE_NAME,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_ar_payment_terms D
ON T.dim_ora_ar_payment_termsid = D.dim_ora_ar_payment_termsid
WHERE D.dim_ora_ar_payment_termsid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_ar_payment_terms';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_ar_payment_terms',IFNULL(MAX(dim_ora_ar_payment_termsid),0)
FROM dim_ora_ar_payment_terms;

/*update dimension rows*/
UPDATE dim_ora_ar_payment_terms T
FROM dim_ora_ar_payment_terms D JOIN ORA_AR_PAYMENT_TERMS S
ON D.KEY_ID = S.TERM_ID
SET 
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.CREDIT_CHECK_FLAG = ifnull(S.CREDIT_CHECK_FLAG, 'Not Set'),
T.DUE_CUTOFF_DAY = ifnull(S.DUE_CUTOFF_DAY, 0),
T.PRINTING_LEAD_DAYS = ifnull(S.PRINTING_LEAD_DAYS, 0),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set'),
T.BASE_AMOUNT = ifnull(S.BASE_AMOUNT, 0),
T.CALC_DISCOUNT_ON_LINES_FLAG = ifnull(S.CALC_DISCOUNT_ON_LINES_FLAG, 'Not Set'),
T.FIRST_INSTALLMENT_CODE = ifnull(S.FIRST_INSTALLMENT_CODE, 'Not Set'),
T.IN_USE = ifnull(S.IN_USE, 'Not Set'),
T.PARTIAL_DISCOUNT_FLAG = ifnull(S.PARTIAL_DISCOUNT_FLAG, 'Not Set'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.ATTRIBUTE9 = ifnull(S.ATTRIBUTE9, 'Not Set'),
T.ATTRIBUTE10 = ifnull(S.ATTRIBUTE10, 'Not Set'),
T.ATTRIBUTE11 = ifnull(S.ATTRIBUTE11, 'Not Set'),
T.ATTRIBUTE12 = ifnull(S.ATTRIBUTE12, 'Not Set'),
T.ATTRIBUTE13 = ifnull(S.ATTRIBUTE13, 'Not Set'),
T.ATTRIBUTE14 = ifnull(S.ATTRIBUTE14, 'Not Set'),
T.ATTRIBUTE15 = ifnull(S.ATTRIBUTE15, 'Not Set'),
T.NAME = ifnull(S.NAME, 'Not Set'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.PREPAYMENT_FLG = ifnull(S.PREPAYMENT_FLG, 'Not Set'),
T.BILLING_CYCLE_ID = ifnull(S.BILLING_CYCLE_ID, 0),
T.CYCLE_NAME = ifnull(S.CYCLE_NAME, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
S.TERM_ID = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_ar_payment_terms
(
dim_ora_ar_payment_termsid,
last_update_date,last_updated_by,creation_date,created_by,last_update_login,credit_check_flag,due_cutoff_day,printing_lead_days,
start_date_active,end_date_active,attribute_category,base_amount,calc_discount_on_lines_flag,first_installment_code,in_use,
partial_discount_flag,attribute1,attribute2,attribute3,attribute4,attribute5,attribute6,attribute7,attribute8,attribute9,
attribute10,attribute11,attribute12,attribute13,attribute14,attribute15,name,description,prepayment_flg,billing_cycle_id,cycle_name,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_ar_payment_terms' ),0) + ROW_NUMBER() OVER() dim_ora_ar_payment_termsid,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.CREDIT_CHECK_FLAG, 'Not Set') CREDIT_CHECK_FLAG,
ifnull(S.DUE_CUTOFF_DAY, 0) DUE_CUTOFF_DAY,
ifnull(S.PRINTING_LEAD_DAYS, 0) PRINTING_LEAD_DAYS,
ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set') ATTRIBUTE_CATEGORY,
ifnull(S.BASE_AMOUNT, 0) BASE_AMOUNT,
ifnull(S.CALC_DISCOUNT_ON_LINES_FLAG, 'Not Set') CALC_DISCOUNT_ON_LINES_FLAG,
ifnull(S.FIRST_INSTALLMENT_CODE, 'Not Set') FIRST_INSTALLMENT_CODE,
ifnull(S.IN_USE, 'Not Set') IN_USE,
ifnull(S.PARTIAL_DISCOUNT_FLAG, 'Not Set') PARTIAL_DISCOUNT_FLAG,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.ATTRIBUTE9, 'Not Set') ATTRIBUTE9,
ifnull(S.ATTRIBUTE10, 'Not Set') ATTRIBUTE10,
ifnull(S.ATTRIBUTE11, 'Not Set') ATTRIBUTE11,
ifnull(S.ATTRIBUTE12, 'Not Set') ATTRIBUTE12,
ifnull(S.ATTRIBUTE13, 'Not Set') ATTRIBUTE13,
ifnull(S.ATTRIBUTE14, 'Not Set') ATTRIBUTE14,
ifnull(S.ATTRIBUTE15, 'Not Set') ATTRIBUTE15,
ifnull(S.NAME, 'Not Set') NAME,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.PREPAYMENT_FLG, 'Not Set') PREPAYMENT_FLG,
ifnull(S.BILLING_CYCLE_ID, 0) BILLING_CYCLE_ID,
ifnull(S.CYCLE_NAME, 'Not Set') CYCLE_NAME,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.TERM_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_AR_PAYMENT_TERMS S LEFT JOIN dim_ora_ar_payment_terms D ON
D.KEY_ID = S.TERM_ID
WHERE 
D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_ar_payment_terms');