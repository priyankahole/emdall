/*insert default row*/
INSERT INTO dim_ora_mtl_item_rev
(
dim_ora_mtl_item_revid,
revision_id,revision_label,revision_reason,
lifecycle_id,current_phase_id,object_version_number,last_update_date,last_updated_by,creation_date,created_by,last_update_login,
change_notice,ecn_initiation_date,implementation_date,implemented_serial_number,effectivity_date,revised_item_sequence_id,description,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_mtl_item_revid,
0 REVISION_ID,
'Not Set' REVISION_LABEL,
'Not Set' REVISION_REASON,
0 LIFECYCLE_ID,
0 CURRENT_PHASE_ID,
0 OBJECT_VERSION_NUMBER,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' CHANGE_NOTICE,
timestamp '0001-01-01 00:00:00.000000' ECN_INITIATION_DATE,
timestamp '0001-01-01 00:00:00.000000' IMPLEMENTATION_DATE,
'Not Set' IMPLEMENTED_SERIAL_NUMBER,
timestamp '0001-01-01 00:00:00.000000' EFFECTIVITY_DATE,
0 REVISED_ITEM_SEQUENCE_ID,
'Not Set' DESCRIPTION,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_mtl_item_rev D
ON T.dim_ora_mtl_item_revid = D.dim_ora_mtl_item_revid
WHERE D.dim_ora_mtl_item_revid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_mtl_item_rev';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_mtl_item_rev',IFNULL(MAX(dim_ora_mtl_item_revid),0)
FROM dim_ora_mtl_item_rev;

/*update dimension rows*/
UPDATE dim_ora_mtl_item_rev T
FROM dim_ora_mtl_item_rev D JOIN ORA_MTL_ITEM_REV S
ON D.KEY_ID = VARCHAR(S.INVENTORY_ITEM_ID, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.REVISION, 200) 
SET 
T.REVISION_ID = ifnull(S.REVISION_ID, 0),
T.REVISION_LABEL = ifnull(S.REVISION_LABEL, 'Not Set'),
T.REVISION_REASON = ifnull(S.REVISION_REASON, 'Not Set'),
T.LIFECYCLE_ID = ifnull(S.LIFECYCLE_ID, 0),
T.CURRENT_PHASE_ID = ifnull(S.CURRENT_PHASE_ID, 0),
T.OBJECT_VERSION_NUMBER = ifnull(S.OBJECT_VERSION_NUMBER, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.CHANGE_NOTICE = ifnull(S.CHANGE_NOTICE, 'Not Set'),
T.ECN_INITIATION_DATE = ifnull(S.ECN_INITIATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.IMPLEMENTATION_DATE = ifnull(S.IMPLEMENTATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.IMPLEMENTED_SERIAL_NUMBER = ifnull(S.IMPLEMENTED_SERIAL_NUMBER, 'Not Set'),
T.EFFECTIVITY_DATE = ifnull(S.EFFECTIVITY_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.REVISED_ITEM_SEQUENCE_ID = ifnull(S.REVISED_ITEM_SEQUENCE_ID, 0),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
VARCHAR(S.INVENTORY_ITEM_ID, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.REVISION, 200) = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_mtl_item_rev
(
dim_ora_mtl_item_revid,
revision_id,revision_label,revision_reason,
lifecycle_id,current_phase_id,object_version_number,last_update_date,last_updated_by,creation_date,created_by,last_update_login,
change_notice,ecn_initiation_date,implementation_date,implemented_serial_number,effectivity_date,revised_item_sequence_id,description,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_mtl_item_rev' ),0) + ROW_NUMBER() OVER() dim_ora_mtl_item_revid,
ifnull(S.REVISION_ID, 0) REVISION_ID,
ifnull(S.REVISION_LABEL, 'Not Set') REVISION_LABEL,
ifnull(S.REVISION_REASON, 'Not Set') REVISION_REASON,
ifnull(S.LIFECYCLE_ID, 0) LIFECYCLE_ID,
ifnull(S.CURRENT_PHASE_ID, 0) CURRENT_PHASE_ID,
ifnull(S.OBJECT_VERSION_NUMBER, 0) OBJECT_VERSION_NUMBER,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.CHANGE_NOTICE, 'Not Set') CHANGE_NOTICE,
ifnull(S.ECN_INITIATION_DATE, timestamp '0001-01-01 00:00:00.000000') ECN_INITIATION_DATE,
ifnull(S.IMPLEMENTATION_DATE, timestamp '0001-01-01 00:00:00.000000') IMPLEMENTATION_DATE,
ifnull(S.IMPLEMENTED_SERIAL_NUMBER, 'Not Set') IMPLEMENTED_SERIAL_NUMBER,
ifnull(S.EFFECTIVITY_DATE, timestamp '0001-01-01 00:00:00.000000') EFFECTIVITY_DATE,
ifnull(S.REVISED_ITEM_SEQUENCE_ID, 0) REVISED_ITEM_SEQUENCE_ID,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
VARCHAR(S.INVENTORY_ITEM_ID, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.REVISION, 200) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_MTL_ITEM_REV S LEFT JOIN dim_ora_mtl_item_rev D 
ON D.KEY_ID = VARCHAR(S.INVENTORY_ITEM_ID, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.REVISION, 200)
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_mtl_item_rev');