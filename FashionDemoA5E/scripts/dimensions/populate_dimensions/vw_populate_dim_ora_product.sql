/*insert default row*/
INSERT INTO dim_ora_product
(
dim_ora_productid,
organization_id,last_update_date,last_updated_by,creation_date,created_by,carrying_cost,description,list_price_per_unit,
segment1,segment2,segment3,segment4,segment5,segment6,segment7,segment8,segment9,segment10,segment11,segment12,segment13,
segment14,segment15,segment16,segment17,segment18,segment19,segment20,minimum_order_quantity,maximum_order_quantity,
std_lot_size,planning_time_fence_days,full_lead_time,
postprocessing_lead_time,preprocessing_lead_time,order_cost,
shelf_life_days,primary_uom_code,unit_of_issue,source_subinventory,inventory_item_id,
safety_stock_quantity,planner_code,planner_description,planner_disable_date,
cum_manufacturing_lead_time,mrp_planning_code,employee_number,full_name,enabled_flag,start_date_active,
end_date_active,invoiceable_item_flag,invoice_enabled_flag,prod_cat_segment1,prod_cat_segment2,prod_cat_segment3,prod_cat_segment4,item_type,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_productid,
0 ORGANIZATION_ID,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 CARRYING_COST, 
'Not Set' DESCRIPTION,
0 LIST_PRICE_PER_UNIT,
'Not Set' SEGMENT1,
'Not Set' SEGMENT2,
'Not Set' SEGMENT3,
'Not Set' SEGMENT4,
'Not Set' SEGMENT5,
'Not Set' SEGMENT6,
'Not Set' SEGMENT7,
'Not Set' SEGMENT8,
'Not Set' SEGMENT9,
'Not Set' SEGMENT10,
'Not Set' SEGMENT11,
'Not Set' SEGMENT12,
'Not Set' SEGMENT13,
'Not Set' SEGMENT14,
'Not Set' SEGMENT15,
'Not Set' SEGMENT16,
'Not Set' SEGMENT17,
'Not Set' SEGMENT18,
'Not Set' SEGMENT19,
'Not Set' SEGMENT20,
0 MINIMUM_ORDER_QUANTITY,
0 MAXIMUM_ORDER_QUANTITY,
0 STD_LOT_SIZE,
0 PLANNING_TIME_FENCE_DAYS,
0 FULL_LEAD_TIME,
0 POSTPROCESSING_LEAD_TIME,
0 PREPROCESSING_LEAD_TIME,
0 ORDER_COST,
0 SHELF_LIFE_DAYS,
'Not Set' PRIMARY_UOM_CODE,
'Not Set' UNIT_OF_ISSUE,
'Not Set' SOURCE_SUBINVENTORY,
0 INVENTORY_ITEM_ID,
0 SAFETY_STOCK_QUANTITY,
'Not Set' PLANNER_CODE,
'Not Set' planner_description,
timestamp '0001-01-01 00:00:00.000000' planner_disable_date,
0 CUM_MANUFACTURING_LEAD_TIME,
0 MRP_PLANNING_CODE,
'Not Set' EMPLOYEE_NUMBER,
'Not Set' FULL_NAME,
'Not Set' ENABLED_FLAG,
timestamp '0001-01-01 00:00:00.000000' START_DATE_ACTIVE,
timestamp '0001-01-01 00:00:00.000000' END_DATE_ACTIVE,
'Not Set' INVOICEABLE_ITEM_FLAG,
'Not Set' INVOICE_ENABLED_FLAG,
'Not Set' PROD_CAT_SEGMENT1,
'Not Set' PROD_CAT_SEGMENT2,
'Not Set' PROD_CAT_SEGMENT3,
'Not Set' PROD_CAT_SEGMENT4,
'Not Set' item_type,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_product D
ON T.dim_ora_productid = D.dim_ora_productid
WHERE D.dim_ora_productid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_product';
	
INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_product',IFNULL(MAX(dim_ora_productid),0)
FROM dim_ora_product;

/*update dimension rows*/
UPDATE dim_ora_product T
FROM dim_ora_product D JOIN ORA_MTL_INVENTORY_PROD S
ON 
D.KEY_ID = S.INVENTORY_ITEM_ID
SET 
T.ORGANIZATION_ID = ifnull(S.ORGANIZATION_ID, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.SEGMENT1 = ifnull(S.SEGMENT1, 'Not Set'),
T.SEGMENT2 = ifnull(S.SEGMENT2, 'Not Set'),
T.SEGMENT3 = ifnull(S.SEGMENT3, 'Not Set'),
T.SEGMENT4 = ifnull(S.SEGMENT4, 'Not Set'),
T.SEGMENT5 = ifnull(S.SEGMENT5, 'Not Set'),
T.SEGMENT6 = ifnull(S.SEGMENT6, 'Not Set'),
T.SEGMENT7 = ifnull(S.SEGMENT7, 'Not Set'),
T.SEGMENT8 = ifnull(S.SEGMENT8, 'Not Set'),
T.SEGMENT9 = ifnull(S.SEGMENT9, 'Not Set'),
T.SEGMENT10 = ifnull(S.SEGMENT10, 'Not Set'),
T.SEGMENT11 = ifnull(S.SEGMENT11, 'Not Set'),
T.SEGMENT12 = ifnull(S.SEGMENT12, 'Not Set'),
T.SEGMENT13 = ifnull(S.SEGMENT13, 'Not Set'),
T.SEGMENT14 = ifnull(S.SEGMENT14, 'Not Set'),
T.SEGMENT15 = ifnull(S.SEGMENT15, 'Not Set'),
T.SEGMENT16 = ifnull(S.SEGMENT16, 'Not Set'),
T.SEGMENT17 = ifnull(S.SEGMENT17, 'Not Set'),
T.SEGMENT18 = ifnull(S.SEGMENT18, 'Not Set'),
T.SEGMENT19 = ifnull(S.SEGMENT19, 'Not Set'),
T.SEGMENT20 = ifnull(S.SEGMENT20, 'Not Set'),
T.MINIMUM_ORDER_QUANTITY = ifnull(S.MINIMUM_ORDER_QUANTITY, 0),
T.MAXIMUM_ORDER_QUANTITY = ifnull(S.MAXIMUM_ORDER_QUANTITY, 0),
T.STD_LOT_SIZE = ifnull(S.STD_LOT_SIZE, 0),
T.PLANNING_TIME_FENCE_DAYS = ifnull(S.PLANNING_TIME_FENCE_DAYS, 0),
T.FULL_LEAD_TIME = ifnull(S.FULL_LEAD_TIME, 0),
T.POSTPROCESSING_LEAD_TIME = ifnull(S.POSTPROCESSING_LEAD_TIME, 0),
T.PREPROCESSING_LEAD_TIME = ifnull(S.PREPROCESSING_LEAD_TIME, 0),
T.ORDER_COST = ifnull(S.ORDER_COST, 0),
T.SHELF_LIFE_DAYS = ifnull(S.SHELF_LIFE_DAYS, 0),
T.PRIMARY_UOM_CODE = ifnull(S.PRIMARY_UOM_CODE, 'Not Set'),
T.UNIT_OF_ISSUE = ifnull(S.UNIT_OF_ISSUE, 'Not Set'),
T.SOURCE_SUBINVENTORY = ifnull(S.SOURCE_SUBINVENTORY, 'Not Set'),
T.SAFETY_STOCK_QUANTITY = ifnull(S.SAFETY_STOCK_QUANTITY, 0),
T.PLANNER_CODE = ifnull(S.PLANNER_CODE, 'Not Set'),
T.CUM_MANUFACTURING_LEAD_TIME = ifnull(S.CUM_MANUFACTURING_LEAD_TIME, 0),
T.MRP_PLANNING_CODE = ifnull(S.MRP_PLANNING_CODE, 0),
T.EMPLOYEE_NUMBER = ifnull(S.EMPLOYEE_NUMBER, 'Not Set'),
T.FULL_NAME = ifnull(S.FULL_NAME, 'Not Set'),
T.ENABLED_FLAG = ifnull(S.ENABLED_FLAG, 'Not Set'),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.INVOICEABLE_ITEM_FLAG = ifnull(S.INVOICEABLE_ITEM_FLAG, 'Not Set'),
T.INVOICE_ENABLED_FLAG = ifnull(S.INVOICE_ENABLED_FLAG, 'Not Set'),
T.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID,
T.CARRYING_COST = ifnull(S.CARRYING_COST,0),
T.LIST_PRICE_PER_UNIT = ifnull(S.LIST_PRICE_PER_UNIT,0),
T.item_type= ifnull(S.item_type, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
S.ORGANIZATION_ID = (select property_value from systemproperty where property = 'PROD.MASTER_ORGANIZATION_ID') AND 
S.INVENTORY_ITEM_ID  = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_product
(
dim_ora_productid,
organization_id,last_update_date,last_updated_by,creation_date,created_by,carrying_cost,description,list_price_per_unit,
segment1,segment2,segment3,segment4,segment5,segment6,segment7,segment8,segment9,segment10,segment11,segment12,segment13,
segment14,segment15,segment16,segment17,segment18,segment19,segment20,minimum_order_quantity,maximum_order_quantity,
std_lot_size,planning_time_fence_days,full_lead_time,
postprocessing_lead_time,preprocessing_lead_time,order_cost,
shelf_life_days,primary_uom_code,unit_of_issue,source_subinventory,inventory_item_id,
safety_stock_quantity,planner_code,planner_description,planner_disable_date,
cum_manufacturing_lead_time,mrp_planning_code,employee_number,full_name,enabled_flag,start_date_active,
end_date_active,invoiceable_item_flag,invoice_enabled_flag,prod_cat_segment1,prod_cat_segment2,prod_cat_segment3,prod_cat_segment4,item_type,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_product' ),0) + ROW_NUMBER() OVER() dim_ora_productid,
ifnull(S.ORGANIZATION_ID, 0) ORGANIZATION_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.CARRYING_COST, 0) CARRYING_COST,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.LIST_PRICE_PER_UNIT, 0) LIST_PRICE_PER_UNIT,
ifnull(S.SEGMENT1, 'Not Set') SEGMENT1,
ifnull(S.SEGMENT2, 'Not Set') SEGMENT2,
ifnull(S.SEGMENT3, 'Not Set') SEGMENT3,
ifnull(S.SEGMENT4, 'Not Set') SEGMENT4,
ifnull(S.SEGMENT5, 'Not Set') SEGMENT5,
ifnull(S.SEGMENT6, 'Not Set') SEGMENT6,
ifnull(S.SEGMENT7, 'Not Set') SEGMENT7,
ifnull(S.SEGMENT8, 'Not Set') SEGMENT8,
ifnull(S.SEGMENT9, 'Not Set') SEGMENT9,
ifnull(S.SEGMENT10, 'Not Set') SEGMENT10,
ifnull(S.SEGMENT11, 'Not Set') SEGMENT11,
ifnull(S.SEGMENT12, 'Not Set') SEGMENT12,
ifnull(S.SEGMENT13, 'Not Set') SEGMENT13,
ifnull(S.SEGMENT14, 'Not Set') SEGMENT14,
ifnull(S.SEGMENT15, 'Not Set') SEGMENT15,
ifnull(S.SEGMENT16, 'Not Set') SEGMENT16,
ifnull(S.SEGMENT17, 'Not Set') SEGMENT17,
ifnull(S.SEGMENT18, 'Not Set') SEGMENT18,
ifnull(S.SEGMENT19, 'Not Set') SEGMENT19,
ifnull(S.SEGMENT20, 'Not Set') SEGMENT20,
ifnull(S.MINIMUM_ORDER_QUANTITY, 0) MINIMUM_ORDER_QUANTITY,
ifnull(S.MAXIMUM_ORDER_QUANTITY, 0) MAXIMUM_ORDER_QUANTITY,
ifnull(S.STD_LOT_SIZE, 0) STD_LOT_SIZE,
ifnull(S.PLANNING_TIME_FENCE_DAYS, 0) PLANNING_TIME_FENCE_DAYS,
ifnull(S.FULL_LEAD_TIME, 0) FULL_LEAD_TIME,
ifnull(S.POSTPROCESSING_LEAD_TIME, 0) POSTPROCESSING_LEAD_TIME,
ifnull(S.PREPROCESSING_LEAD_TIME, 0) PREPROCESSING_LEAD_TIME,
ifnull(S.ORDER_COST, 0) ORDER_COST,
ifnull(S.SHELF_LIFE_DAYS, 0) SHELF_LIFE_DAYS,
ifnull(S.PRIMARY_UOM_CODE, 'Not Set') PRIMARY_UOM_CODE,
ifnull(S.UNIT_OF_ISSUE, 'Not Set') UNIT_OF_ISSUE,
ifnull(S.SOURCE_SUBINVENTORY, 'Not Set') SOURCE_SUBINVENTORY,
S.INVENTORY_ITEM_ID INVENTORY_ITEM_ID,
ifnull(S.SAFETY_STOCK_QUANTITY, 0) SAFETY_STOCK_QUANTITY,
ifnull(S.PLANNER_CODE, 'Not Set') PLANNER_CODE,
'Not Set' planner_description,
timestamp '0001-01-01 00:00:00.000000' planner_disable_date,
ifnull(S.CUM_MANUFACTURING_LEAD_TIME, 0) CUM_MANUFACTURING_LEAD_TIME,
ifnull(S.MRP_PLANNING_CODE, 0) MRP_PLANNING_CODE,
ifnull(S.EMPLOYEE_NUMBER, 'Not Set') EMPLOYEE_NUMBER,
ifnull(S.FULL_NAME, 'Not Set') FULL_NAME,
ifnull(S.ENABLED_FLAG, 'Not Set') ENABLED_FLAG,
ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.INVOICEABLE_ITEM_FLAG, 'Not Set') INVOICEABLE_ITEM_FLAG,
ifnull(S.INVOICE_ENABLED_FLAG, 'Not Set') INVOICE_ENABLED_FLAG,
ifnull(S.SEGMENT1, 'Not Set') PROD_CAT_SEGMENT1,
ifnull(S.SEGMENT2, 'Not Set') PROD_CAT_SEGMENT2,
ifnull(S.SEGMENT3, 'Not Set') PROD_CAT_SEGMENT3,
ifnull(S.SEGMENT4, 'Not Set') PROD_CAT_SEGMENT4,
ifnull(S.item_type, 'Not Set') item_type,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.INVENTORY_ITEM_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_MTL_INVENTORY_PROD S 
JOIN (
select
ORGANIZATION_ID,
INVENTORY_ITEM_ID,
ifnull(SAFETY_STOCK_QUANTITY, 0) SAFETY_STOCK_QUANTITY,
row_number() over (partition by ORGANIZATION_ID, INVENTORY_ITEM_ID order by ifnull(SAFETY_STOCK_QUANTITY, 0) desc) row_number
from ORA_MTL_INVENTORY_PROD
) t on t.ORGANIZATION_ID = S.ORGANIZATION_ID and t.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID and t.SAFETY_STOCK_QUANTITY = ifnull(S.SAFETY_STOCK_QUANTITY, 0)
and t.row_number = 1
LEFT JOIN dim_ora_product D ON
D.KEY_ID = S.INVENTORY_ITEM_ID
WHERE 
S.ORGANIZATION_ID = (select integer(property_value) from systemproperty where property = 'PROD.MASTER_ORGANIZATION_ID') and
D.KEY_ID is null;

/*update product category attributes*/
UPDATE dim_ora_product T
FROM dim_ora_product D JOIN ORA_MTL_PRODUCT_CATEGORY S
ON 
D.KEY_ID = S.INVENTORY_ITEM_ID
JOIN (select property_value from systemproperty where property = 'PROD.MASTER_ORGANIZATION_ID') morg
on S.ORGANIZATION_ID = morg.property_value
JOIN (select property_value from systemproperty where property = 'PROD.CATEGORY_SET_ID') catset
ON S.CATEGORY_SET_ID = catset.property_value
SET 
T.PROD_CAT_SEGMENT1 = ifnull(S.SEGMENT1, 'Not Set'),
T.PROD_CAT_SEGMENT2 = ifnull(S.SEGMENT2, 'Not Set'),
T.PROD_CAT_SEGMENT3 = ifnull(S.SEGMENT3, 'Not Set'),
T.PROD_CAT_SEGMENT4 = ifnull(S.SEGMENT4, 'Not Set')
WHERE
S.ORGANIZATION_ID =  morg.property_value
AND S.CATEGORY_SET_ID = catset.property_value
AND S.INVENTORY_ITEM_ID  = T.KEY_ID;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_product');