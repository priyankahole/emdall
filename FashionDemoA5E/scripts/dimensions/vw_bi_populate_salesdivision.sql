INSERT INTO dim_Salesdivision(dim_Salesdivisionid,
                             DivisionCode,
                             Name)
   SELECT 1,
          'Not Set',
          'Not Set'
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_Salesdivision
               WHERE dim_Salesdivisionid = 1);

UPDATE    dim_Salesdivision a
       FROM
          TSPAT t
   SET a.Name = ifnull(TSPAT_VTEXT, 'Not Set'),
			a.dw_update_date = current_timestamp
WHERE t.TSPAT_SPART IS NOT NULL AND a.DivisionCode = t.TSPAT_SPART;

delete from number_fountain m where m.table_name = 'dim_salesdivision';

insert into number_fountain
select 	'dim_salesdivision',
	ifnull(max(d.Dim_SalesDivisionid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesdivision d
where d.Dim_SalesDivisionid <> 1; 
 
insert into dim_salesdivision
(	Dim_SalesDivisionid,
	DivisionCode,
	Name
)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesdivision') 
          + row_number() over(),
			  TSPAT_SPART,
	ifnull(TSPAT_VTEXT,'Not Set')
from TSPAT
where TSPAT_SPART is not null
	and not exists (select 1 from dim_salesdivision a where a.DivisionCode = TSPAT_SPART) ;