UPDATE    dim_profitcenter pc
       FROM
          CEPCT c
   SET pc.ProfitCenterName = ifnull(CEPCT_KTEXT, 'Not Set'),
       pc.ControllingArea = ifnull(CEPCT_KOKRS, 'Not Set'),
       pc.ValidTo = CEPCT_DATBI,
			pc.dw_update_date = current_timestamp
 WHERE pc.RowIsCurrent = 1
 AND c.CEPCT_PRCTR = pc.ProfitCenterCode
;

INSERT INTO dim_profitcenter(dim_profitcenterId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_profitcenter
               WHERE dim_profitcenterId = 1);

delete from number_fountain m where m.table_name = 'dim_profitcenter';
   
insert into number_fountain
select 	'dim_profitcenter',
	ifnull(max(d.Dim_ProfitCenterid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_profitcenter d
where d.Dim_ProfitCenterid <> 1; 

INSERT INTO dim_profitcenter(Dim_ProfitCenterid,
			  ProfitCenterCode,
			  ValidTo,
			  ControllingArea,
			  ProfitCenterName,
                          RowStartDate,
                          RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_profitcenter')
         + row_number() over(),
			  CEPCT_PRCTR,
	    CEPCT_DATBI,
	    ifnull(CEPCT_KOKRS, 'Not Set'),
	    ifnull(CEPCT_KTEXT, 'Not Set'),
            current_timestamp,
            1
       FROM CEPCT
      WHERE CEPCT_PRCTR IS NOT NULL
	    AND NOT EXISTS
                  (SELECT 1
                     FROM dim_profitcenter
                    WHERE ProfitCenterCode = CEPCT_PRCTR)
;

