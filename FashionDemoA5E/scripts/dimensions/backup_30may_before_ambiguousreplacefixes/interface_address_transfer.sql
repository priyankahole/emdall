--Update (Interface 1)
$MySQL{
UPDATE    dim_Address da
       INNER JOIN
          adrc a
       ON a.adrc_addrnumber = da.AddressNumber
          AND a.adrc_addrnumber IS NOT NULL
	INNER JOIN 
	  t005t
	ON adrc_country = t005t_land1
   SET da.Address1 = ifnull(adrc_name1, 'Not Set'),
       da.Address2 = ifnull(adrc_name2, 'Not Set'),
       da.City = ifnull(adrc_city1, 'Not Set'),
       da.State = ifnull(adrc_region, 'Not Set'),
       da.Zip = ifnull(adrc_post_Code1, 'Not Set'),
       da.Country = ifnull(adrc_country, 'Not Set'),
       da.CountryName = ifnull(t005t_landx, 'Not Set')
}

$Vectorwise{
UPDATE	dim_Address da
FROM	adrc a, t005t
SET		da.Address1 = ifnull(adrc_name1, 'Not Set'),
		da.Address2 = ifnull(adrc_name2, 'Not Set'),
		da.City = ifnull(adrc_city1, 'Not Set'),
		da.State = ifnull(adrc_region, 'Not Set'),
		da.Zip = ifnull(adrc_post_Code1, 'Not Set'),
		da.Country = ifnull(adrc_country, 'Not Set'),
		da.CountryName = ifnull(t005t_landx, 'Not Set'),
		da.dw_update_date = current_timestamp,
		da.street = ifnull(adrc_street,'Not Set'),
		da.str_suppl1 = ifnull(adrc_str_suppl1,'Not Set'), 
		da.str_suppl2 = ifnull(adrc_str_suppl2,'Not Set'), 
		da.str_suppl3 = ifnull(adrc_str_suppl3,'Not Set'), 
		da.location = ifnull(adrc_location,'Not Set') 
WHERE   a.adrc_addrnumber = da.AddressNumber
        AND a.adrc_addrnumber IS NOT NULL
        AND a.adrc_country = t005t_land1
}

--Insert (Interface 2)

$MySQL{
INSERT INTO dim_address(Address1,
                        Address2,
                        City,
                        State,
                        Zip,
                        Country,
                        AddressNumber,
			CountryName)
   SELECT ifnull(adrc_name1, 'Not Set'),
          ifnull(adrc_name2, 'Not Set'),
          ifnull(adrc_city1, 'Not Set'),          
          ifnull(adrc_region, 'Not Set'),          
          ifnull(adrc_post_Code1, 'Not Set'),          
          ifnull(adrc_country, 'Not Set'),          
          adrc_addrnumber,
	  ifnull(t005t_landx, 'Not Set')
     FROM adrc a inner join t005t on adrc_country = t005t_land1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_address da
                       WHERE a.adrc_addrnumber = da.AddressNumber)
	and adrc_addrnumber is not null
}

$Vectorwise{
INSERT INTO dim_address(Dim_Addressid,
                                                Address1,
                        Address2,
                        City,
                        State,
                        Zip,
                        Country,
                        AddressNumber,
                        CountryName,
						street,
						str_suppl1,
						str_suppl2,
						str_suppl3,
						location)
   SELECT (SELECT ifnull(max(dad.Dim_Addressid)+1, 1) id FROM dim_address dad)
          + row_number() over(),
                  ifnull(adrc_name1, 'Not Set'),
          ifnull(adrc_name2, 'Not Set'),
          ifnull(adrc_city1, 'Not Set'),
          ifnull(adrc_region, 'Not Set'),
          ifnull(adrc_post_Code1, 'Not Set'),
          ifnull(adrc_country, 'Not Set'),
          adrc_addrnumber,
          ifnull(t005t_landx, 'Not Set'),
		  ifnull(adrc_street,'Not Set'),
		  ifnull(adrc_str_suppl1,'Not Set'),
		  ifnull(adrc_str_suppl2,'Not Set'),
		  ifnull(adrc_str_suppl3,'Not Set'),
		  ifnull(adrc_location,'Not Set')
     FROM adrc a inner join t005t on adrc_country = t005t_land1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_address da
                       WHERE a.adrc_addrnumber = da.AddressNumber)
        and adrc_addrnumber is not null
}
dimensions/vw_bi_populate_address.sql
