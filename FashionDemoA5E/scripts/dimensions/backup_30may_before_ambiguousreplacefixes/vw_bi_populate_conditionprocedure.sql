
INSERT INTO dim_conditionprocedure(dim_conditionprocedureId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_conditionprocedure
               WHERE dim_conditionprocedureId = 1);
			   
UPDATE    dim_conditionprocedure cp
       FROM
          T683U t
   SET cp.ProcedureDescription = t.T683U_VTEXT,
       cp.ProcedureApplication = t.T683U_KAPPL,
       cp.UsageOfConditionTable = t.T683U_KVEWE,
			cp.dw_update_date = current_timestamp
 WHERE cp.RowIsCurrent = 1
 AND cp.ProcedureCode = t.T683U_KALSM;
 
delete from number_fountain m where m.table_name = 'dim_conditionprocedure';

insert into number_fountain
select 	'dim_conditionprocedure',
	ifnull(max(d.dim_conditionprocedureId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_conditionprocedure d
where d.dim_conditionprocedureId <> 1;

INSERT INTO dim_conditionprocedure(Dim_ConditionProcedureid,
							ProcedureCode,
                            ProcedureDescription,
			    ProcedureApplication,
                            UsageOfConditionTable,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_conditionprocedure') 
          + row_number() over() ,
			t.T683U_KALSM,
          t.T683U_VTEXT,
          t.T683U_KAPPL,
          t.T683U_KVEWE,
          current_timestamp,
          1
     FROM T683U t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_conditionprocedure cp
               WHERE cp.ProcedureCode = t.T683U_KALSM );

delete from number_fountain m where m.table_name = 'dim_conditionprocedure';
			   