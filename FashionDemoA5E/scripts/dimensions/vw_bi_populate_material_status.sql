INSERT INTO dim_materialstatus(Dim_MaterialStatusid,
                               MaterialStatus,
                               MaterialStatusDescription,
                               RowStartDate,
                               RowEndDate,
                               RowIsCurrent,
                               RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          '2013-05-06 04:56:33',
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_materialstatus
               WHERE dim_materialstatusid = 1);

UPDATE dim_materialstatus a FROM T141T 
   SET a.MaterialStatusDescription = ifnull(T141T_MTSTB, 'Not Set'),
			a.dw_update_date = current_timestamp
 WHERE a.MaterialStatus = T141T_MMSTA AND RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_materialstatus';
   
insert into number_fountain
select 	'dim_materialstatus',
	ifnull(max(d.Dim_MaterialStatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_materialstatus d
where d.Dim_MaterialStatusid <> 1; 
 
 INSERT INTO dim_materialstatus(Dim_MaterialStatusid,
                                    MaterialStatus,
                                    MaterialStatusDescription,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_materialstatus') 
          + row_number() over() ,
			 t.T141T_MMSTA,
          ifnull(t.T141T_MTSTB, 'Not Set'),
          current_timestamp,
          1
     FROM T141T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_materialstatus a
               WHERE a.MaterialStatus = t.T141T_MMSTA);