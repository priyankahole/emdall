UPDATE    dim_objecttype ot
       FROM
          DD07T t
   SET ot.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE ot.RowIsCurrent = 1
        AND   t.DD07T_DOMNAME = 'OBART'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND ot.ObjectType = t.DD07T_DOMVALUE
;