UPDATE    dim_effectiveformatplanning emp
       FROM
          DD07T t
   SET emp.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE emp.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'NO_DISP_PLUS'
          AND emp.EffectiveForMatPlanning = ifnull(t.DD07T_DOMVALUE, 'Not Set');

