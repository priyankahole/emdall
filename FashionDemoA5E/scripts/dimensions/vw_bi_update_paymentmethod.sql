
UPDATE    dim_paymentmethod pm
       FROM
          T042ZT t, T005T
   SET pm.Description = t.T042ZT_TEXT2,
       pm.CountryName = ifnull(t005t_landx, 'Not Set')
 WHERE pm.RowIsCurrent = 1
 AND  pm.PaymentMethod = t.T042ZT_ZLSCH AND pm.Country = t.T042ZT_LAND1 AND t.T042ZT_LAND1 = T005T_LAND1;
