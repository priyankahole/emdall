insert into dim_damagecode(dim_damagecodeid,itemshorttext,catalogtype_problems,damagecode,versionnumber,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason) 
SELECT 1,'Not Set','Not Set','Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_damagecode where dim_damagecodeid = 1);

UPDATE dim_damagecode dc from QPCT 
   SET dc.itemshorttext = QPCT_KURZTEXT,
			dc.dw_update_date = current_timestamp
    WHERE dc.catalogtype_problems = QPCT_KATALOGART
      AND dc.damagecode = QPCT_CODE
      AND dc.versionnumber = QPCT_VERSION
      AND dc.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_damagecode';

insert into number_fountain
select 	'dim_damagecode',
	ifnull(max(d.dim_damagecodeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_damagecode d
where d.dim_damagecodeid <> 1;
	  
INSERT INTO dim_damagecode(dim_damagecodeid,
                                  itemshorttext,
                                  catalogtype_problems,
                                  damagecode,
                                  versionnumber,
                                  RowStartDate,
                                  RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_damagecode') 
          + row_number() over(),
		  QPCT_KURZTEXT,
		  QPCT_KATALOGART,
          QPCT_CODE,
          QPCT_VERSION,
          current_timestamp,
          1
     FROM QPCT
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM dim_damagecode a
                   WHERE     a.itemshorttext = QPCT_KURZTEXT
                         AND a.catalogtype_problems = QPCT_KATALOGART
                         AND a.damagecode = QPCT_CODE
                         AND a.versionnumber = QPCT_VERSION
			 AND a.RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_damagecode';			 