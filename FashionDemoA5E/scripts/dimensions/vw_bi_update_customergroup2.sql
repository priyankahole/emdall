
UPDATE    dim_customergroup2 cg2
       FROM
          TVV2T t
   SET cg2.Description = ifnull(TVV2T_BEZEI, 'Not Set')
   WHERE t.TVV2T_KVGR2 = cg2.CustomerGroup AND t.TVV2T_SPRAS = 'E';
