
UPDATE    dim_escalationreason ds
       FROM
          SCMGATTR_SESCALT dt
   SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set')
 WHERE ds.RowIsCurrent = 1
          AND ds.EscalReason = dt.ESCAL_REASON
          AND dt.ESCAL_REASON IS NOT NULL;
