UPDATE    dim_inspectionlotorigin o
       FROM
          TQ31T t
   SET o.InspectionLotOriginName = t.TQ31T_HERKTXT
 WHERE o.RowIsCurrent = 1
 AND o.InspectionLotOriginCode = t.TQ31T_HERKUNFT;

