
UPDATE dim_ActionType dat from actiontype aty
   SET dat.ActionMnemonic = ifnull(aty.ACTION_MNEMONIC,'Not Set'),
       dat.Description = ifnull(aty.DESCRIPTION,'Not Set'),
       dat.DueByPeriod = aty.DUE_BY_PERIOD,
       dat.MatchingEnabled = aty.Matching_enabled
 WHERE dat.ActionTypeId = aty.ACTION_TYPE AND dat.RowIsCurrent = 1;
