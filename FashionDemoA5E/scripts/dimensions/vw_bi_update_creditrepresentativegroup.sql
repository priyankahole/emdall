UPDATE dim_CreditRepresentativeGroup FROM T024B t
   SET CreditRepGroupName = ifnull(T024B_STEXT, 'Not Set'),
       RMailUserName = ifnull(T024B_SMAIL, 'Not Set')
 WHERE     CreditRepresentativeGroup = t.T024B_SBGRP
       AND CreditControlArea = t.T024B_KKBER
       AND RowIsCurrent = 1;
