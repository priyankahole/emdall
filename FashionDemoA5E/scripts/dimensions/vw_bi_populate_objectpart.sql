insert into dim_objectpart(dim_objectpartid,codegroup,partofobject,versionnumber,catalogtype_objectpart,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason) 
SELECT 1,'Not Set','Not Set','Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_objectpart where dim_objectpartid = 1);

UPDATE dim_objectpart from QPCT 
   SET codegroup = QPCT_CODEGRUPPE,
			dw_update_date = current_timestamp
    WHERE partofobject = QPCT_CODE
      AND versionnumber = QPCT_VERSION
      AND catalogtype_objectpart = QPCT_KATALOGART
      AND RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_objectpart';
   
insert into number_fountain
select 	'dim_objectpart',
	ifnull(max(d.dim_objectpartid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_objectpart d
where d.dim_objectpartid <> 1; 

INSERT INTO dim_objectpart(dim_objectpartid,
                                  codegroup,
                                  partofobject,
                                  versionnumber,
                                  catalogtype_objectpart,
                                  RowStartDate,
                                  RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_objectpart') 
          + row_number() over(),
		  QPCT_CODEGRUPPE,
		  QPCT_CODE,
          QPCT_VERSION,
          QPCT_KATALOGART,
          current_timestamp,
          1
     FROM QPCT
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM dim_objectpart a
                   WHERE     a.codegroup = QPCT_CODEGRUPPE
                         AND a.partofobject = QPCT_CODE
                         AND a.versionnumber = QPCT_VERSION
                         AND a.catalogtype_objectpart = QPCT_KATALOGART
			 AND a.RowIsCurrent = 1);
