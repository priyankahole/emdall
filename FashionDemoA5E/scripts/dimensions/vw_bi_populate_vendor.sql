/* Start Update dim_vendor Interface */
INSERT INTO dim_vendor
(dim_vendorid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_vendor where dim_vendorid = 1);

UPDATE dim_vendor v
          FROM
             LFA1 l
       INNER JOIN
          t077y vg
       ON l.LFA1_KTOKK = vg.T077Y_KTOKK
	INNER JOIN T005T ON l.LAND1 = T005T_LAND1
   SET v.VendorName = ifnull(l.NAME1, 'Not Set'),
       v.City = ifnull(l.LFA1_ORT01, 'Not Set'),
       v.State = ifnull(l.LFA1_REGIO, 'Not Set'),
       v.PostalCode = ifnull(l.LFA1_PSTLZ, 'Not Set'),
       v.Country = ifnull(l.LAND1, 'Not Set'),
       v.IndustryCode = ifnull(l.LFA1_BRSCH, 'Not Set'),
       v.Industry =
          ifnull((SELECT T5UNT_NTEXT
                    FROM T5UNT
                   WHERE T5UNT_NAICS = LFA1_BRSCH
                   ),
                 'Not Set'),
       v.AccountGroup = ifnull(vg.T077Y_TXT30, 'Not Set'),
       v.DeleteIndicator = ifnull(l.LFA1_LOEVM, 'Not Set'),
       v.PurchaseBlock = ifnull(l.LFA1_SPERM, 'Not Set'),
       v.PostingBlock = ifnull(l.LFA1_SPERR, 'Not Set'),
       v.OneTimeAccount = ifnull(l.LFA1_XCPDK, 'Not Set'),
       v.QualitySystem = ifnull(l.LFA1_QSSYS, 'Not Set'),
       v.CompanyCode = ifnull(l.LFA1_VBUND, 'Not Set'),
       v.VendorGroup = ifnull(l.LFA1_KTOKK, 'Not Set'),
       v.ACHFlag = 'N',
       v.PreviousVendorNumber =  ifnull((SELECT lb.LFB1_ALTKN
                    FROM LFB1 lb
                   WHERE lb.LFB1_LIFNR = l.LIFNR
						AND lb.LFB1_ALTKN is not null),
                 'Not Set'),
	v.CountryName = ifnull(t005t_landx, 'Not Set'),
	v.RegionDescription = 	 ifnull((SELECT T005U_BEZEI FROM T005U
		 WHERE T005U_LAND1 = l.LAND1
                  AND T005U_BLAND = l.LFA1_REGIO),'Not Set'),
        v.VendorMfgCode = IFNULL(l.LFA1_EMNFR,'Not Set'),
			v.dw_update_date = current_timestamp
	   WHERE   v.vendornumber = l.LIFNR;

delete from number_fountain m where m.table_name = 'dim_vendor';

insert into number_fountain
select 	'dim_vendor',
	ifnull(max(d.Dim_Vendorid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_vendor d
where d.Dim_Vendorid <> 1;	   
	   
INSERT INTO dim_vendor(Dim_Vendorid,
                       VendorNumber,
                       VendorName,
                       City,
                       State,
                       PostalCode,
                       Country,
                       IndustryCode,
                       Industry,
                       AccountGroup,
                       DeleteIndicator,
                       PurchaseBlock,
                       PostingBlock,
                       OneTimeAccount,
                       QualitySystem,
                       CompanyCode,
                       VendorGroup,
			ACHFlag,
			PreviousVendorNumber,
                       RowStartDate,
                       RowIsCurrent,
			CountryName,
			RegionDescription,
                        vendormfgcode)
        SELECT DISTINCT(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_vendor')
          + row_number() over(),
              l.LIFNR,
          ifnull(NAME1, 'Not Set'),
          ifnull(LFA1_ORT01, 'Not Set'),
          ifnull(LFA1_REGIO, 'Not Set'),
          ifnull(LFA1_PSTLZ, 'Not Set'),
          ifnull(LAND1, 'Not Set'),
          ifnull(LFA1_BRSCH, 'Not Set'),
      ifnull((SELECT T5UNT_NTEXT
                  FROM T5UNT
                  WHERE T5UNT_NAICS = LFA1_BRSCH
                  ),'Not Set'),
          ifnull(vg.T077Y_TXT30, 'Not Set'),
          ifnull(LFA1_LOEVM, 'Not Set'),
          ifnull(LFA1_SPERM, 'Not Set'),
          ifnull(LFA1_SPERR, 'Not Set'),
          ifnull(LFA1_XCPDK, 'Not Set'),
          ifnull(LFA1_QSSYS, 'Not Set'),
          ifnull(LFA1_VBUND, 'Not Set'),
          ifnull(LFA1_KTOKK, 'Not Set'),
          ifnull(
             (SELECT 'Y'
                FROM LFA1 l2
               WHERE EXISTS
                        (SELECT 1
                           FROM LFBK
                          WHERE     LFBK_LIFNR = l2.LIFNR
                                AND LFBK_BANKL IS NOT NULL
                                AND LFBK_BANKN IS NOT NULL
                                AND LFBK_BANKS IS NOT NULL)
               ),
             'N'),
		ifnull((SELECT lb.LFB1_ALTKN
                    FROM LFB1 lb
                   WHERE lb.LFB1_LIFNR = l.LIFNR
		       AND lb.LFB1_ALTKN is not null),
                 'Not Set'),
          current_timestamp,
          1,
	ifnull(t005t_landx, 'Not Set'),
	ifnull((SELECT T005U_BEZEI FROM T005U
		 WHERE T005U_LAND1 = l.LAND1
                  AND T005U_BLAND = l.LFA1_REGIO),'Not Set'),
        ifnull(LFA1_EMNFR,'Not Set')
     FROM LFA1 l INNER JOIN t077y vg
             ON l.LFA1_KTOKK = vg.T077Y_KTOKK
		 INNER JOIN T005T ON l.LAND1 = T005T_LAND1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_vendor
                       WHERE vendornumber = l.LIFNR);

UPDATE dim_vendor v
FROM LFA1 l, LFBK
   SET v.ACHFlag = 'Y',
			v.dw_update_date = current_timestamp
WHERE v.vendornumber = l.LIFNR 
  AND LFBK_LIFNR = l.LIFNR
  AND LFBK_BANKL IS NOT NULL
  AND LFBK_BANKN IS NOT NULL
  AND LFBK_BANKS IS NOT NULL;					   

UPDATE dim_vendor v
FROM TZONE_TZONT tt, LFA1 l
   SET v.departurezone = ifnull(tt.TZONT_VTEXT,'Not Set'),
			v.dw_update_date = current_timestamp
   WHERE tt.TZONE_LAND1 = l.LAND1
   AND tt.TZONE_ZONE1=l.LFA1_LZONE
   AND v.vendornumber = l.LIFNR
   AND v.departurezone <> ifnull(tt.TZONT_VTEXT, 'Not Set');
 

SELECT 'END of Vencor dimension Processing';
