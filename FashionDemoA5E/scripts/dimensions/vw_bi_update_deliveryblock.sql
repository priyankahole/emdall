UPDATE dim_deliveryblock a FROM TVLST
   SET Description = ifnull(TVLST_VTEXT, 'Not Set')
 WHERE a.DeliveryBlock = TVLST_LIFSP AND RowIsCurrent = 1;

