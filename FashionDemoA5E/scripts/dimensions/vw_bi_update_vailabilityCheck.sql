UPDATE dim_AvailabilityCheck a FROM TMVFT 
   SET a.AvailabilityCheckDescription = ifnull(TMVFT_BEZEI, 'Not Set')
 WHERE a.AvailabilityCheck = TMVFT_MTVFP AND RowIsCurrent = 1;