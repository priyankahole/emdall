UPDATE dim_codeText from QPCT 
   SET Description = QPCT_KURZTEXT
    WHERE Catalog = QPCT_KATALOGART
                         AND CodeGroup = QPCT_CODEGRUPPE
                         AND Code = QPCT_CODE
                         AND 'Version' = QPCT_VERSION
                         AND RowIsCurrent = 1
;