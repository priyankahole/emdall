UPDATE    dim_supplyarea a
       FROM
          PVKT p
   SET a.Description = ifnull(PVKT_PVBTX, 'Not Set')
 WHERE a.RowIsCurrent = 1
       AND a.SupplyArea = p.PVKT_PRVBE AND a.Plant = p.PVKT_WERKS 
;