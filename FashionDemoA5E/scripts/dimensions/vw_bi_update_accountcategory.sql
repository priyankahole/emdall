UPDATE    dim_accountcategory ac
       from
          t163i t
   SET ac.Description = ifnull(t163i_knttx, 'Not Set')
   WHERE ac.Category = t.t163i_knttp AND ac.RowIsCurrent = 1;
