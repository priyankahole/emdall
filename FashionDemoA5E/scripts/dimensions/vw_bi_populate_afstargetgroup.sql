insert into Dim_AfsTargetGroup (Dim_AfsTargetGroupId,AfsTargetGroup,AfsTargetGroupDescription,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason)
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from Dim_AfsTargetGroup where Dim_AfsTargetGroupid = 1);
               
UPDATE dim_AfsTargetGroup a FROM J_3AGENDRT 
   SET a.AFSTargetGroupDescription = ifnull(J_3AGENDRT_TEXT, 'Not Set'),
		a.dw_update_date = current_timestamp
 WHERE a.AFSTargetGroup = J_3AGENDRT_J_3AGEND AND RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'Dim_AfsTargetGroup';

insert into number_fountain
select 	'Dim_AfsTargetGroup',
	ifnull(max(d.Dim_AfsTargetGroupId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_AfsTargetGroup d
where d.Dim_AfsTargetGroupId <> 1;
 
INSERT INTO dim_AfsTargetGroup(dim_AfsTargetGroupId,
                                    AfsTargetGroup,
                                    AFSTargetGroupDescription,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_AfsTargetGroup') 
          + row_number() over() ,
			 t.J_3AGENDRT_J_3AGEND,
          ifnull(t.J_3AGENDRT_TEXT, 'Not Set'),
          current_timestamp,
          1
     FROM J_3AGENDRT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_AfsTargetGroup a
               WHERE a.dim_AfsTargetGroupId = t.J_3AGENDRT_J_3AGEND);
			   
delete from number_fountain m where m.table_name = 'Dim_AfsTargetGroup';			   