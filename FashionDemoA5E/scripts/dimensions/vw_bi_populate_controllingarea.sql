
INSERT INTO dim_controllingarea(dim_controllingareaId )
SELECT 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_controllingarea
               WHERE dim_controllingareaId = 1);
			   
UPDATE    dim_controllingarea ca
       FROM
          TKA01 t    
   SET ca.ControllingAreaName = ifnull(TKA01_BEZEI, 'Not Set'),
       ca.CurrencyKey = ifnull(TKA01_WAERS, 'Not Set'),
       ca.FiscalYrVariant = ifnull(TKA01_LMONA, 'Not Set'),
       ca.AllocationIndicator = ifnull(TKA01_KOKFI, 'Not Set'),
			ca.dw_update_date = current_timestamp
	WHERE t.TKA01_KOKRS IS NOT NULL
          AND ca.ControllingAreaCode = t.TKA01_KOKRS;
		  
delete from number_fountain m where m.table_name = 'dim_controllingarea';

insert into number_fountain
select 	'dim_controllingarea',
	ifnull(max(d.dim_controllingareaId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_controllingarea d
where d.dim_controllingareaId <> 1;

insert into dim_controllingarea
(	Dim_ControllingAreaid,
	ControllingAreaCode, 
	ControllingAreaName, 
	CurrencyKey, 
	FiscalYrVariant, 
	AllocationIndicator
)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_controllingarea') 
          + row_number() over(),
			TKA01_KOKRS,
	ifnull(TKA01_BEZEI,'Not Set'),
	ifnull(TKA01_WAERS,'Not Set'),
	ifnull(TKA01_LMONA,'Not Set'),
	ifnull(TKA01_KOKFI,'Not Set')
from TKA01
where TKA01_KOKRS is not null
	and not exists (select 1 from dim_controllingarea a where a.ControllingAreaCode = TKA01_KOKRS);

delete from number_fountain m where m.table_name = 'dim_controllingarea';
	
