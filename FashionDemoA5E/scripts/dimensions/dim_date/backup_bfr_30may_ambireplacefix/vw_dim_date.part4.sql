Update companymaster_d00 set MAXDT_EXISTS = 0;

Update companymaster_d00
set MAXDT_EXISTS = IFNULL((SELECT 1 FROM dim_date
                               WHERE DateValue = MAXDT AND companycode = pCompanyCode ),0);
                               
Drop table if exists mh_i01;
Create table mh_i01(maxid)
as

select 	ifnull(max(d.dim_dateid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_date d
where d.dim_dateid <> 1;
 
      INSERT INTO dim_date(dim_dateid,DateValue,
                          DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          select mh_i01.maxid + row_number() over(), MAXDT,
                  MAXDT_NAME,
                  0,
                  YEAR(MAXDT),
                  0,
                  QUARTER(MAXDT),
                  0,
                  concat(YEAR(MAXDT), ' Q ',QUARTER(MAXDT)),
                  0,
                  0,
                  'Not Set',
                  'Not Set',
                  0,
                  MONTH(MAXDT),
                  date_format(MAXDT,'%M'),
                  LEFT(date_format(MAXDT,'%M'),3),
                  0,
                  0,
                  0,
                  WEEK(MAXDT,4),
                  0,
                  0,
                  DAY(MAXDT),
                  0,
                  DAYOFMONTH(MAXDT),
                  DAYOFWEEK(MAXDT),
                  date_format(MAXDT,'%W'),
                  LEFT(date_format(MAXDT,'%W'),3),
                  (CASE WHEN DAYOFWEEK(MAXDT) IN (1,7) THEN 1 ELSE 0 END),
                  (CASE WHEN ((mod(YEAR(MAXDT),4) = 0)  AND (mod(YEAR(MAXDT) , 100) != 0 OR mod(YEAR(MAXDT) ,400) = 0)) THEN 1 ELSE 0 END),
                  DAYOFYEAR(CAST(concat('9999-01-01 00:00:00') AS timestamp(0))),
                  DAYOFYEAR(CAST(concat('9999-01-01 00:00:00') AS timestamp(0))),
                  DAYOFMONTH(MAXDT) - 1,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  DAYOFMONTH(MAXDT),
                  0,
                  0,
                  pCompanyCode,
                  concat(YEAR(MAXDT),'-',lpad(cast(WEEK(MAXDT,4) as char(2)),2,'0'))
		  from companymaster_d00,mh_i01 where MAXDT_EXISTS = 0;
                 
drop table if exists mh_i01;
 
Update companymaster_d00 
SET MAXDT = STR_TO_DATE('12/31/9999','%m/%d/%Y'), MAXDT_NAME = '31 Dec 9999';
 
Update companymaster_d00 
set MAXDT_EXISTS = IFNULL((SELECT 1 FROM dim_date
                               WHERE DateValue = MAXDT AND companycode = pCompanyCode ),0);
                               

drop table if exists mh_i01;
Create table mh_i01(maxid)
as

select 	ifnull(max(d.dim_dateid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_date d
where d.dim_dateid <> 1;

 
      INSERT INTO dim_date(dim_dateid,DateValue,
                          DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          select mh_i01.maxid + row_number() over(),MAXDT,
                  MAXDT_NAME,
                  0,
                  YEAR(MAXDT),
                  0,
                  QUARTER(MAXDT),
                  0,
                  concat(YEAR(MAXDT), ' Q ',QUARTER(MAXDT)),
                  0,
                  0,
                  'Not Set',
                  'Not Set',
                  0,
                  MONTH(MAXDT),
                  date_format(MAXDT,'%M'),
                  LEFT(date_format(MAXDT,'%M'),3),
                  0,
                  0,
                  0,
                  WEEK(MAXDT,4),
                  0,
                  0,
                  DAY(MAXDT),
                  0,
                  DAYOFMONTH(MAXDT),
                  DAYOFWEEK(MAXDT),
                  date_format(MAXDT,'%W'),
                  LEFT(date_format(MAXDT,'%W'),3),
                  (CASE WHEN DAYOFWEEK(MAXDT) IN (1,7) THEN 1 ELSE 0 END),
                  (CASE WHEN ((mod(YEAR(MAXDT) , 4) = 0)  AND (mod(YEAR(MAXDT) , 100) != 0 OR mod(YEAR(MAXDT) , 400) = 0)) THEN 1 ELSE 0 END),
                  ifnull(DAYOFYEAR(CAST(concat('9999-12-31 00:00:00') AS timestamp(0))),365),
                  ifnull(DAYOFYEAR(CAST(concat('9999-12-31 00:00:00') AS timestamp(0))),365),
                  DAYOFMONTH(MAXDT) - 1,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  DAYOFMONTH(MAXDT),
                  0,
                  0,
                  pCompanyCode,
                  concat(YEAR(MAXDT),'-',lpad(cast(WEEK(MAXDT,4) as char(2)),2,'0'))
		from companymaster_d00,mh_i01
		where MAXDT_EXISTS=0;
                 
drop table if exists mh_i01; 
  
  insert into dim_monthyear
  (Dim_MonthYearid, MonthYear, MonthYearAlt, MonthStartDate, MonthEndDate, DaysInMonth, CalYear, WorkdaysInMonth)
  select distinct case Dim_Dateid when 1 then 1 else CalendarMonthID end CalendarMonthID, 
          case Dim_Dateid when 1 then 'Jan 0001' else MonthYear end MonthYear, 
          case Dim_Dateid when 1 then '0001-01' else concat(CalendarYear,'-',lpad(CalendarMonthnumber,2,0)) end MonthYearAlt, 
          MonthStartDate, 
          MonthEndDate, 
          DaysInMonth, 
          CalendarYear CalYear, 
          case Dim_Dateid when 1 then 0 else WorkdaysInMonth end WorkdaysInMonth
  from dim_date dt
  where (Dim_Dateid = 1 or CalendarMonthID > 0) and companycode = 'Not Set'
      and not exists (select 1 from dim_monthyear my 
                      where my.Dim_MonthYearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarMonthID end);

\i /db/schema_migration/bin/wrapper_optimizedb.sh dim_year;
\i /db/schema_migration/bin/wrapper_optimizedb.sh dim_date;

DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins1;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_del;

CREATE TABLE tmp_dimdate_dim_year_ins 
AS
select distinct case Dim_Dateid when 1 then 1 else CalendarYear end Dim_Yearid,
CalendarYear CalYear,
case CalendarYear when 9999 then 365 else DaysInCalendarYear end DaysInCalendarYear
from dim_date dt
where (Dim_Dateid = 1 or CalendarYear > 0) and companycode = 'Not Set';

CREATE TABLE tmp_dimdate_dim_year_ins1
AS
SELECT DISTINCT Dim_Yearid
FROM tmp_dimdate_dim_year_ins;

CREATE TABLE tmp_dimdate_dim_year_del
AS
SELECT * FROM tmp_dimdate_dim_year_ins1 where 1=2;

INSERT INTO tmp_dimdate_dim_year_del
SELECT DISTINCT y.Dim_Yearid
FROM dim_year y, dim_date dt
where y.Dim_Yearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarYear end;

call vectorwise(combine 'tmp_dimdate_dim_year_ins1-tmp_dimdate_dim_year_del');





insert into dim_year
(
dim_yearid,
calyear,
daysincalendaryear
)
select DISTINCT dt.Dim_Yearid, 
dt.CalYear,
dt.DaysInCalendarYear
from tmp_dimdate_dim_year_ins dt, tmp_dimdate_dim_year_ins1 i
WHERE i.Dim_Yearid = dt.Dim_Yearid;
/*where (Dim_Dateid = 1 or CalendarYear > 0) and companycode = 'Not Set'
and not exists (select 1 from dim_year y 
      where y.Dim_Yearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarYear end)*/

DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins1;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_del;

/* business days sequence calculation */
  
drop table if exists dimdateseqtemp;
create table dimdateseqtemp (
companycode varchar(10) null,

dim_dateid bigint null,

datevalue timestamp(0) null,
dateseqno integer null);

Insert into dimdateseqtemp(companycode,dim_dateid,datevalue,dateseqno)
select companycode,dim_dateid, datevalue, row_number() over(partition by companycode order by DateValue)  DtSeqNo
from dim_date dt
where dt.IsaPublicHoliday = 0 and dt.IsaWeekendday = 0 ;


/* Update BusinessDaysSeqNo for business days */
update dim_date dt
from dimdateseqtemp s

  set dt.BusinessDaysSeqNo = s.dateseqno,
			dt.dw_update_date = current_timestamp

where  dt.dim_dateid = s.dim_dateid
and dt.companycode = s.companycode 
and dt.BusinessDaysSeqNo <> s.dateseqno;

/* Update BusinessDaysSeqNo for holidays/weekends where previous day is a business day */
/*update dim_date dt 
from dimdateseqtemp s 
  set dt.BusinessDaysSeqNo = s.dateseqno
where  dt.datevalue - 1 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1 */

/* Update BusinessDaysSeqNo for holidays/weekends where previous day is also a non-business day */

/*update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 2 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 2 days are non-business days */
/*update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 3 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 3 days are non-business days */
/*update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 4 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 4 days are non-business days */
/*update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 5 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */


/*
update dim_date dt
set dt.BusinessDaysSeqNo = (select max(s.dateseqno) from dimdateseqtemp s where s.DateValue <= dt.DateValue and s.companycode=dt.companycode)
where (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1)*/

DROP TABLE IF EXISTS tmp_testing_dimdateseqtemp;
CREATE TABLE tmp_testing_dimdateseqtemp
AS
SELECT dt.companycode,dt.DateValue,max(s.dateseqno) max_dateseqno
FROM dim_date dt, dimdateseqtemp s
where s.DateValue <= dt.DateValue and s.companycode=dt.companycode
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1)
GROUP BY dt.companycode,dt.DateValue ;

UPDATE 	dim_date dt
FROM tmp_testing_dimdateseqtemp s
SET dt.BusinessDaysSeqNo = s.max_dateseqno,
			dw_update_date = current_timestamp
WHERE s.DateValue = dt.datevalue AND s.companycode=dt.companycode
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1)
AND ifnull(dt.BusinessDaysSeqNo,-1) <> ifnull(s.max_dateseqno,-2);


/* Handle the case where the first date or first few days were non-business days */
UPDATE dim_date

SET BusinessDaysSeqNo = 0,
			dw_update_date = current_timestamp

WHERE BusinessDaysSeqNo IS NULL;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycount;
CREATE TABLE tmp_dim_date_weekdaycount
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,CalendarYear,
row_number() over( PARTITION BY CompanyCode,CalendarYear order by d2.datevalue) WeekdaysInCalendarYearSofar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycountfinyr;
CREATE TABLE tmp_dim_date_weekdaycountfinyr
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,FinancialYear,
row_number() over( PARTITION BY CompanyCode,FinancialYear order by d2.datevalue) WeekdaysInFinancialYearSofar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycountmonth;
CREATE TABLE tmp_dim_date_weekdaycountmonth
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,CalendarYear,MonthName,
row_number() over( PARTITION BY CompanyCode,CalendarYear,MonthName order by d2.datevalue) WeekdaysInMonthSoFar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


UPDATE tmp_dim_date_weekdaycount
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycount
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountfinyr
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountfinyr
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountmonth
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountmonth
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';


DROP TABLE IF EXISTS tmp_weekenddays;
CREATE TABLE tmp_weekenddays
AS
SELECT datevalue
FROM dim_date 
WHERE IsaWeekendday = 1;

/* Update WeekdaysInCalendarYearSofar*/
UPDATE dim_date d
FROM tmp_dim_date_weekdaycount w

SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.datevalue
AND d.CompanyCode = w.CompanyCode
AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar;

UPDATE dim_date d
FROM tmp_dim_date_weekdaycount w, tmp_weekenddays e

SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.date_value_next
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue
AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar;

UPDATE dim_date d
FROM tmp_dim_date_weekdaycount w,tmp_weekenddays e

SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.date_value_next_2
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar; /* Check that the previous day was also a weekend day */

/* Update weekdaysinfinancialyearsofar*/

UPDATE dim_date d
FROM tmp_dim_date_weekdaycountfinyr w

SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.datevalue
AND d.CompanyCode = w.CompanyCode 
AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar;


UPDATE dim_date d
FROM tmp_dim_date_weekdaycountfinyr w, tmp_weekenddays e

SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.date_value_next
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue
AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar;

UPDATE dim_date d
FROM tmp_dim_date_weekdaycountfinyr w,tmp_weekenddays e

SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.date_value_next_2
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar; /* Check that the previous day was also a weekend day */

/* Reset to 0 where its the 1st month of a financial year and 1st is Saturday */

UPDATE dim_date d
FROM tmp_weekenddays e

SET d.weekdaysinfinancialyearsofar = 0,
			dw_update_date = current_timestamp

WHERE d.datevalue = e.datevalue
AND substring(FinancialMonthID,5,2) = '01'
AND date_format(d.datevalue,'%e') = 1
AND d.weekdaysinfinancialyearsofar <> 0;

/* Reset to 0 where its the 1st month of a financial year and 1st or 2nd is Sunday */

UPDATE dim_date d
FROM tmp_weekenddays e

SET d.weekdaysinfinancialyearsofar = 0,
			dw_update_date = current_timestamp

WHERE d.datevalue = e.datevalue
AND substring(FinancialMonthID,5,2) = '01'
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
AND date_format(d.datevalue,'%e') IN ( 1,2)
AND d.weekdaysinfinancialyearsofar <> 0;



/* Update weekdaysinmonthsofar */

UPDATE dim_date d
FROM tmp_dim_date_weekdaycountmonth w

SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.datevalue 
AND d.CompanyCode = w.CompanyCode
AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar;

/* Update for Saturdays */
UPDATE dim_date d
FROM tmp_dim_date_weekdaycountmonth w, tmp_weekenddays e

SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.date_value_next
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue
AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar;


/* Update for Sundays */
UPDATE dim_date d
FROM tmp_dim_date_weekdaycountmonth w,tmp_weekenddays e

SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar,
			dw_update_date = current_timestamp

WHERE d.datevalue = w.date_value_next_2
AND d.datevalue = e.datevalue
AND d.CompanyCode = w.CompanyCode
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar; /* Check that the previous day was also a weekend day */

/* Reset to 0 where its a Sat and the 1st of a month */
UPDATE dim_date d
FROM tmp_weekenddays e

SET d.weekdaysinmonthsofar = 0,
			dw_update_date = current_timestamp

WHERE d.datevalue = e.datevalue
AND date_format(d.datevalue,'%e') = 1
AND d.weekdaysinmonthsofar <> 0;

/* Reset to 0 where its a Sunday and 1st or 2nd of a month */
UPDATE dim_date d
FROM tmp_weekenddays e

SET d.weekdaysinmonthsofar = 0,
			dw_update_date = current_timestamp

WHERE d.datevalue = e.datevalue
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
AND date_format(d.datevalue,'%e') IN ( 1,2)
AND d.weekdaysinmonthsofar <> 0;


/* workdaysincalendaryearsofar eqivalent to weekdaysincalendaryearsofar 
workdaysinfinancialyearsofar equivalent to weekdaysinfinancialyearsofar */

UPDATE dim_date

SET workdaysincalendaryearsofar = WeekdaysInCalendarYearSofar,
			dw_update_date = current_timestamp;

UPDATE dim_date

SET workdaysinfinancialyearsofar = weekdaysinfinancialyearsofar,
			dw_update_date = current_timestamp;


UPDATE dim_date

SET workdaysinmonthsofar = weekdaysinmonthsofar,
			dw_update_date = current_timestamp;

/* Update values for default dates */
UPDATE dim_date

SET weekdaysincalendaryearsofar = 0,
			dw_update_date = current_timestamp

WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date

SET weekdaysinfinancialyearsofar = 0,
			dw_update_date = current_timestamp

WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date

SET workdaysincalendaryearsofar = 0,
			dw_update_date = current_timestamp

WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date

SET workdaysinfinancialyearsofar = 0,
			dw_update_date = current_timestamp

WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date

SET weekdaysinmonthsofar = 0,
			dw_update_date = current_timestamp

WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date

SET workdaysinmonthsofar = 0,
			dw_update_date = current_timestamp

WHERE datevalue IN ( '9999-01-01', '9999-12-31' );


UPDATE dim_date
SET WeekStartDate = '0001-01-01' ,
			dw_update_date = current_timestamp
WHERE dim_dateid = 1;

UPDATE dim_date
SET WeekEndDate = '0001-01-06' ,
			dw_update_date = current_timestamp
WHERE dim_dateid = 1;

UPDATE dim_date
SET MonthStartDate = '0001-01-01',
			dw_update_date = current_timestamp 
WHERE dim_dateid = 1;


UPDATE dim_date
SET MonthEndDate = '9999-01-01' ,
			dw_update_date = current_timestamp
WHERE dim_dateid = 1;

UPDATE dim_date
SET FinancialMonthStartDate = '0001-01-01',
			dw_update_date = current_timestamp 
WHERE dim_dateid = 1;

UPDATE dim_date
SET FinancialMonthEndDate = '9999-01-01',
			dw_update_date = current_timestamp 
WHERE dim_dateid = 1;

UPDATE dim_date
SET WeekStartDate = '9998-12-27',
			dw_update_date = current_timestamp 
WHERE datevalue IN ( '9999-01-01','9999-12-31');

UPDATE dim_date
SET WeekEndDate = '9999-01-02',
			dw_update_date = current_timestamp 
WHERE datevalue IN ( '9999-01-01','9999-12-31');

UPDATE dim_date
SET MonthStartDate = '9999-01-01' ,
			dw_update_date = current_timestamp
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET MonthEndDate = '9999-12-31',
			dw_update_date = current_timestamp 
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET FinancialMonthStartDate = '9999-01-01' ,
			dw_update_date = current_timestamp
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET FinancialMonthEndDate = '9999-12-31',
			dw_update_date = current_timestamp 
WHERE datevalue IN ( '9999-01-01','9999-12-31');


UPDATE dim_date
SET DaysInFinancialYear = financialyearenddate - financialyearstartdate + 1,
			dw_update_date = current_timestamp
where DaysInFinancialYear <> financialyearenddate - financialyearstartdate + 1;

drop table if exists tmp_finworkdays_001;
create table tmp_finworkdays_001 as
select companycode, financialyear, count(*) workdaysfinyr 
from dim_date 
where isaweekendday = 0 and isapublicholiday = 0
group by companycode, financialyear;

update dim_date dt
from tmp_finworkdays_001 a
set dt.weekdaysinfinancialyear = a.workdaysfinyr,
			dt.dw_update_date = current_timestamp
where dt.companycode = a.companycode and dt.financialyear = a.financialyear
and dt.weekdaysinfinancialyear <> a.workdaysfinyr;

update dim_date dt
from tmp_finworkdays_001 a
set dt.workdaysinfinancialyear = a.workdaysfinyr,
			dt.dw_update_date = current_timestamp
where dt.companycode = a.companycode and dt.financialyear = a.financialyear
and dt.workdaysinfinancialyear <> a.workdaysfinyr;

/* Fix daysinfinancialyearsofar */

UPDATE dim_date
SET daysinfinancialyearsofar = current_date - financialyearstartdate + 1,
			dw_update_date = current_timestamp
WHERE daysinfinancialyearsofar <> current_date - financialyearstartdate + 1;

/* financialweek should be the weekno from the start of 0th weekstart of that financial year. Week starts from 0 */
/* For example, if the first week starts on 30th March 2014,1 to 5 Apr will be finweek 0 and 6th April 2014 will be in financialweek 1 */
UPDATE dim_date a
FROM dim_date b
SET financialweek = FLOOR((a.datevalue - b.weekstartdate)/7),
			a.dw_update_date = current_timestamp
WHERE b.companycode = a.companycode AND a.financialyear = b.financialyear AND b.datevalue = b.financialyearstartdate
AND a.financialweek <> FLOOR((a.datevalue - b.weekstartdate)/7);

UPDATE dim_date
SET financialweekid = financialyear * 100 + financialweek,
			dw_update_date = current_timestamp
WHERE financialweekid <> financialyear * 100 + financialweek;


UPDATE dim_date
SET DayofFinancialYear = daysinfinancialyearsofar,
			dw_update_date = current_timestamp
WHERE DayofFinancialYear <> daysinfinancialyearsofar;


/* Update Financial Half Data */

/* By default financialhalf = 1. Set it to 2 where financialmonthstartdate is atleast 6 months more than financialyearstartdate */
UPDATE dim_date dt
SET financialhalf = 2 ,
			dt.dw_update_date = current_timestamp
where months_between(financialmonthstartdate,financialyearstartdate) >= 6
AND financialhalf <> 2;


Update dim_date dt
SET financialhalfid = (financialyear * 10) + financialhalf,
			dt.dw_update_date = current_timestamp
Where  financialhalfid <> (financialyear * 10) + financialhalf;

Update dim_date dt
SET financialhalfname = concat(financialyear,' H ',CAST(financialhalf AS CHAR (1))),
			dt.dw_update_date = current_timestamp
WHERE financialhalfname <> concat(financialyear,' H ',CAST(financialhalf AS CHAR (1)));

UPDATE dim_date dt
SET financialhalfstartdate = financialyearstartdate,
			dt.dw_update_date = current_timestamp
WHERE financialhalf = 1
AND ifnull(financialhalfstartdate,'1 Jan 1902') <> ifnull(financialyearstartdate,'1 Jan 1903' );

UPDATE dim_date dt
SET financialhalfstartdate = financialyearstartdate + interval '6' MONTH,
			dt.dw_update_date = current_timestamp
WHERE financialhalf = 2
AND ifnull(financialhalfstartdate,'1 Jan 1902') <> ifnull(financialyearstartdate + interval '6' MONTH,'1 Jan 1903')
AND dt.datevalue <> '31 Dec 9999';


UPDATE dim_date dt
SET financialhalfenddate = financialhalfstartdate + interval '6' MONTH - interval '1' day,
			dt.dw_update_date = current_timestamp
WHERE ifnull(financialhalfenddate,'1 Jan 1902') <>  ifnull(financialhalfstartdate + interval '6' MONTH - interval '1' day,'1 Jan 1903')
AND dt.datevalue <> '31 Dec 9999';


UPDATE dim_date dt
SET financialhalfenddate = '31 Dec 9999',
			dt.dw_update_date = current_timestamp
WHERE dt.datevalue = '31 Dec 9999'
AND financialhalfenddate <> '31 Dec 9999';

UPDATE dim_date dt
SET financialhalfstartdate = '31 Dec 9999',
			dt.dw_update_date = current_timestamp
WHERE dt.datevalue = '31 Dec 9999'
AND financialhalfstartdate <> '31 Dec 9999';


drop table if exists tmp_finworkdays_001;
drop TABLE if exists dimdateseqtemp ;
drop TABLE if exists tmp_dimdate_dim_year_del;
drop TABLE if exists tmp_dimdate_dim_year_ins;
drop TABLE if exists tmp_dimdate_dim_year_ins1;
drop TABLE if exists tmp_dim_date_weekdaycount;
drop TABLE if exists tmp_dim_date_weekdaycountfinyr;
drop TABLE if exists tmp_dim_date_weekdaycountmonth;
drop TABLE if exists tmp_weekenddays;
drop TABLE if exists tmp_ffyr_minbutag;
drop TABLE if exists TMP_FFY_T009B_1;
drop TABLE if exists TMP_FFY_T009B_2;
drop TABLE if exists TMP_T009B_1;
drop TABLE if exists TMP_T009B_2;
drop TABLE if exists TMP_T009B_2a;
drop TABLE if exists TMP_T009B_3A;
drop TABLE if exists TMP_T009B_3A_TO;
drop TABLE if exists TMP_T009B_3B;
drop TABLE if exists TMP_T009B_3B_TO;
drop TABLE if exists TMP_T009B_4A;
drop TABLE if exists TMP_T009B_4A_TO;
drop TABLE if exists TMP_T009B_4B;
drop TABLE if exists TMP_T009B_4B_TO;
drop TABLE if exists TMP_TO_3;
drop TABLE if exists TMP_TO_3B;
drop TABLE if exists TMP_TO_4;
drop TABLE if exists TMP_TO_4B;
DROP TABLE IF EXISTS tmp_testing_dimdateseqtemp;
