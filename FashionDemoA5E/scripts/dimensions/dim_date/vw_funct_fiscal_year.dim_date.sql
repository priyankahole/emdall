/*********************************************Change History*******************************************************/
/*Date             By        Version           Desc                                                            */
/*   5 Feb 2015      Liviu Ionescu              Add Combine for tmp_funct_fiscal_year */
/******************************************************************************************************************/

DELETE FROM tmp_funct_fiscal_year
where fact_script_name = 'dim_date';

INSERT INTO tmp_funct_fiscal_year
(pCompanyCode,FiscalYear,Period,fact_script_name)
SELECT distinct pCompanyCode, FINYEAR, 1,'dim_date'
FROM companydetailed_d00;

INSERT INTO tmp_funct_fiscal_year
(pCompanyCode,FiscalYear,Period,fact_script_name)
SELECT distinct pCompanyCode, FINYEAR, 12,'dim_date'
FROM companydetailed_d00;

/* LI Changes 5 Feb 2015 */
call vectorwise(combine 'tmp_funct_fiscal_year');


