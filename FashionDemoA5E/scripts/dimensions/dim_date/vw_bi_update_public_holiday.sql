
/*  Georgiana Changes 14 May 2015  */
/*UPDATE dim_date a
From TFACS t ,dim_plant pl
SET a.IsaPublicHoliday = CASE SUBSTRING(case a.CalendarMonthNumber 
                                              when 1 then t.TFACS_MON01
                                              when 2 then t.TFACS_MON02
                                              when 3 then t.TFACS_MON03
                                              when 4 then t.TFACS_MON04
                                              when 5 then t.TFACS_MON05
                                              when 6 then t.TFACS_MON06
                                              when 7 then t.TFACS_MON07
                                              when 8 then t.TFACS_MON08
                                              when 9 then t.TFACS_MON09
                                              when 10 then t.TFACS_MON10
                                              when 11 then t.TFACS_MON11
                                              when 12 then t.TFACS_MON12
                                          end, a.DayofMonth, 1) WHEN '0' then 1 else 0 END,
	a.dw_update_date = current_timestamp
Where pl.FactoryCalendarKey = t.TFACS_IDENT AND a.CompanyCode = pl.CompanyCode and t.TFACS_JAHR = a.CalendarYear*/

DROP TABLE IF EXISTS tmp_dim_date1;
CREATE TABLE tmp_dim_date1                                                                                                                                                                                                       
AS                                                                                                                                                                                                        
SELECT DISTINCT 
pl.FactoryCalendarKey,
a.CompanyCode,
t.TFACS_JAHR,
a.CalendarMonthNumber,
a.DayofMonth,
 CASE SUBSTRING(case a.CalendarMonthNumber
when 1 then t.TFACS_MON01
when 2 then t.TFACS_MON02
when 3 then t.TFACS_MON03
when 4 then t.TFACS_MON04
when 5 then t.TFACS_MON05
when 6 then t.TFACS_MON06
when 7 then t.TFACS_MON07
when 8 then t.TFACS_MON08
when 9 then t.TFACS_MON09
when 10 then t.TFACS_MON10
when 11 then t.TFACS_MON11
when 12 then t.TFACS_MON12
end, a.DayofMonth, 1) WHEN '0' then 1 else 0 END IsaPublicHoliday,
row_number() over (partition by a.CompanyCode,t.TFACS_JAHR, IsaPublicHoliday) as rownum 
FROM dim_date a,TFACS t ,dim_plant pl
WHERE pl.FactoryCalendarKey = t.TFACS_IDENT AND a.CompanyCode = pl.CompanyCode and t.TFACS_JAHR = a.CalendarYear;

UPDATE dim_date a
From  tmp_dim_date1 tmp
SET a.IsaPublicHoliday = tmp.IsaPublicHoliday,
    a.dw_update_date = current_timestamp
where 
tmp.rownum=1
and a.CompanyCode=tmp.CompanyCode 
and tmp.TFACS_JAHR=a.CalendarYear
and a.CalendarMonthNumber=tmp.CalendarMonthNumber
and a.DayofMonth=tmp.DayofMonth;

DROP TABLE IF EXISTS tmp_dim_date1;



