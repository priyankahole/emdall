Update fiscalprd_i00
set pCalYear =  YEAR(CalDate),
		pCalMth = MONTH(CalDate),
		pCalMthDay = DAYOFMONTH(CalDate),
		Period = 0,
		FiscalYear = 0;

Update fiscalprd_i00
From T001
set pPeriv = PERIV
WHERE BUKRS = pCompanyCode;

Update fiscalprd_i00
From T009
set pVariant1 = XJABH ,
    pVariant2 = XKALE
Where PERIV = pPeriv;

Update fiscalprd_i00 
set pflag=1 
where pVariant1 = 'X';

Update fiscalprd_i00 
set pflag=2 
Where pVariant1 is null and pVariant2 is null;
 
drop table if exists T009B_i00;

Create table T009B_i00 as
Select POPER, RELJR,PERIV, STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y') sdt,row_number () over (order by BDATJ, BUMON, BUTAG) rid
from T009B ;

Update fiscalprd_i00
set minid = ifnull(( select min(rid) from  T009B_i00  where PERIV = pPeriv and ansidate(CalDate) <= ansidate(sdt)),0)
where pflag=1;

Update fiscalprd_i00
Set Period = ifnull((Select POPER from T009B_i00 where rid=minid),0)
where pflag=1;


Update fiscalprd_i00
set pYearShift = ifnull((SELECT RELJR from  T009B_i00  where rid=minid),'0')
Where pflag=1;

drop table if exists T009B_i00;

Update fiscalprd_i00
set FiscalYear = case when pYearShift = '+1' then  (pCalYear + 1)
	              when pYearShift = '-1' then  (pCalYear - 1)
		      else pCalYear
		 end
where pflag=1;
   
drop table if exists T009B_sub009;

create table T009B_sub009 as 
SELECT PERIV,BDATJ,POPER,ANSIDATE(STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y')) subdt 
FROM T009B 
group by PERIV,BDATJ,POPER,ANSIDATE(STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y')) 
order by subdt desc;

Update fiscalprd_i00
set pFromDate = (SELECT subdt + (INTERVAL '1' DAY) tDT
                    FROM T009B_sub009
                    WHERE ansidate(subdt) < ansidate(CalDate) and PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear - 1)
                        and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12)))
where pflag=1;

drop table if exists T009B_sub009;

create table T009B_sub009 as 
SELECT *,ansidate(STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y'))  sdt, row_number() over(order by ansidate(STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y'))) rid
FROM T009B;

Update fiscalprd_i00 set minid=0;

Update fiscalprd_i00 set minid= (SELECT min(rid)
                    FROM T009B_sub009
                    WHERE ansidate(sdt)  > ansidate(CalDate) and PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear + 1)
                        and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1)))
where pflag=1 ;


Update fiscalprd_i00 set pToDate = ( select sdt from T009B_sub009 where rid=minid)
where pflag=1  ;


drop table if exists T009B_sub009;

create table T009B_sub009 as
SELECT PERIV,POPER,ANSIDATE(STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y')) subdt
FROM T009B group by PERIV,POPER,ANSIDATE(STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y')) 
order by subdt desc;

Update fiscalprd_i00
set pToDate = (SELECT subdt
                    FROM T009B_sub009
                    WHERE PERIV = pPeriv and POPER = Period
                        and ansidate(subdt)  < ansidate(pToDate))
where pflag=1;
    
drop table if exists T009B_sub009;
drop table if exists T009B_i00;

Create table T009B_i00 as
Select *,row_number() over(ORDER BY BUMON, BUTAG) rid
FROM T009B;

Update fiscalprd_i00 set minid=0;

Update fiscalprd_i00 set minid =ifnull((SELECT min(rid) FROM T009B_i00
                        WHERE PERIV = pPeriv and BDATJ = 0
                            and ansidate(CalDate) <= ansidate((case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                                              then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
                                              else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
                                            end))),0)
where pflag =2;;


Update fiscalprd_i00
set Period = ifnull((SELECT POPER from T009B_i00 where rid=minid),0)
where pflag=2;

update fiscalprd_i00
set pYearShift =  ifnull((SELECT RELJR from T009B_i00 where rid=minid),'0')
where pflag=2;
   
update fiscalprd_i00
set FiscalYear = case when pYearShift = '+1' then (pCalYear + 1)
		      when pYearShift = '-1' then (pCalYear - 1)
		      else pCalYear
		 end
where pflag=2;


drop table if exists T009B_i00;

create table T009B_i00 as SELECT iid,case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                              then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y') + ( INTERVAL '1' DAY)
                              else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y') + ( INTERVAL '1' DAY)
                            end tdt
                    FROM T009B,fiscalprd_i00
                    WHERE PERIV = pPeriv  and BDATJ = 0
                        and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
                        and ansidate(CalDate) > ansidate(case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                                          then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
                                          else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
                                        end)
			and pflag=2;

drop table if exists tb_max_o23;

create table tb_max_o23 as 
Select iid,max(tdt) maxtdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00
from tb_max_o23 t
set pFromDate = maxtdt 
Where pflag=2 and t.iid=fiscalprd_i00.iid;
   
drop table if exists T009B_i00;
drop table if exists tb_max_o23;
 
create table T009B_i00 as SELECT iid,case when MOD(pCalYear-1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                              then STR_TO_DATE(concat('28/2/', pCalYear-1), '%d/%m/%Y') + ( INTERVAL '1' DAY)
                              else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear-1),  '%d/%m/%Y') + ( INTERVAL '1' DAY)
                            end tDT
                    FROM T009B,fiscalprd_i00
                    WHERE PERIV = pPeriv  and BDATJ = 0
                        and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
                        and ansidate(CalDate) > ansidate(case when MOD(pCalYear-1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                                          then STR_TO_DATE(concat('28/2/', pCalYear-1), '%d/%m/%Y')
                                          else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear-1), '%d/%m/%Y')
                                        end)
			and pflag=2 and pFromDate is null;

drop table if exists tb_max_o23;
create table tb_max_o23 as 
Select iid,max(tdt) maxtdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00
from tb_max_o23 t
set pFromDate = maxtdt 
Where  pflag=2 and pFromDate is null and t.iid=fiscalprd_i00.iid;

drop table if exists tb_max_o23;
drop table if exists T009B_i00;

create table T009B_i00 as SELECT iid,case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                              then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
                              else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
                            end tDT
                    FROM T009B,fiscalprd_i00
                    WHERE PERIV = pPeriv and BDATJ = 0
                        and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
                        and ansidate(CalDate) < ansidate(case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                                          then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
                                          else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
                                        end)
			and  pflag=2 ;

drop table if exists tb_max_o23;

create table tb_max_o23 as 
Select iid,min(tdt) mintdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00
from tb_max_o23 t
set pToDate1 = mintdt
Where pflag=2  and t.iid=fiscalprd_i00.iid;

drop table if exists tb_max_o23;
drop table if exists T009B_i00;

create table T009B_i00 as SELECT iid,case when MOD(pCalYear+1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                              then STR_TO_DATE(concat('28/2/', pCalYear+1), '%d/%m/%Y')
                              else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear+1), '%d/%m/%Y')
                            end tDT
                    FROM T009B,fiscalprd_i00
                    WHERE PERIV = pPeriv and BDATJ = 0
                        and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
                        and ansidate(CalDate) < ansidate(case when MOD(pCalYear+1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                                          then STR_TO_DATE(concat('28/2/', pCalYear+1), '%d/%m/%Y')
                                          else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear+1), '%d/%m/%Y')
                                        end)
			and pflag=2  and pToDate1 is null;

drop table if exists tb_max_o23;

create table tb_max_o23 as 
Select iid,min(tdt) mintdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00
from tb_max_o23 t
set pToDate1 = mintdt
Where pflag=2 and pToDate1 is null and t.iid=fiscalprd_i00.iid;

drop table if exists tb_max_o23;
drop table if exists T009B_i00;

create table T009B_i00 as  SELECT iid,case when MOD(pCalYear+1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                              then STR_TO_DATE(concat('28/2/', pCalYear+1), '%d/%m/%Y')
                              else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear+1), '%d/%m/%Y')
                            end tDT
                    FROM T009B,fiscalprd_i00
                    WHERE PERIV = pPeriv and BDATJ = 0 and POPER = Period
                        and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
                        and ansidate(pToDate1) > ansidate(case when MOD(pCalYear+1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                                          then STR_TO_DATE(concat('28/2/', pCalYear+1), '%d/%m/%Y')
                                          else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear+1), '%d/%m/%Y')
                                        end)
		and pflag=2;

drop table if exists tb_max_o23;

create table tb_max_o23 as  
Select iid,max(tdt) maxtdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00
from tb_max_o23 t
Set pToDate = maxtdt
where pflag=2 and t.iid=fiscalprd_i00.iid;

drop table if exists tb_max_o23;
    
drop table if exists T009B_i00;

create table T009B_i00 as  SELECT iid,case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                              then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
                              else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
                            end tDT
                    FROM T009B,fiscalprd_i00
                    WHERE PERIV = pPeriv and BDATJ = 0 and POPER = Period
                        and ansidate(pToDate1) > ansidate(case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
                                          then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
                                          else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
                                        end)
		and  pflag=2 and pToDate is null;

drop table if exists tb_max_o23;

create  table tb_max_o23 as 
Select iid,max(tdt) maxtdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00 
from tb_max_o23 t
Set pToDate = maxtdt
where pflag=2 and pToDate is null and t.iid=fiscalprd_i00.iid;
   
drop table if exists tb_max_o23; 
  
Update fiscalprd_i00 set Period = pCalMth where pflag=0;  
Update fiscalprd_i00 set FiscalYear = pCalYear where pflag=0;
Update fiscalprd_i00 set pFromDate = ( STR_TO_DATE(concat('01/', pCalMth, '/', pCalYear), '%d/%m/%Y')) where pflag=0;    
Update fiscalprd_i00 set pToDate = (Last_Day(pFromDate)) where pflag=0;   
    

Update fiscalprd_i00 set pReturn = (concat(FiscalYear,'|',RIGHT(concat('00',Period),2),'|',cast(pFromDate as char(20)), '|', cast(pToDate as char(20))));

