Drop table if exists fiscalprd_i00;
Create table fiscalprd_i00(
iid bigint null,
pCompanyCode char(18) null,
CalDate timestamp(0) null,
pFromDate timestamp(0) null,
pToDate timestamp(0) null,
pToDate1 date null,
pMon Integer null,
pVariant1 Char(4) null,
pVariant2 Char(4) null,
pReturn Char(53) null,
pYearShift Char(4) null,
pYearShiftCnt Integer null,
pPrevFIYEAR Integer null,
pPrevPeriod Integer null,
pCalYear  Integer null,
pCalMth   Integer null,
pCalMthDay  Integer null,
Period    Integer null,
FiscalYear  Integer null,
pPeriv Char(2) null,
pflag integer default 0,
minid integer default 0);

Insert into fiscalprd_i00 (iid,pCompanyCode,CalDate)
Select row_number() over(),pCompanyCode,dt from
companydetailed_d00;

