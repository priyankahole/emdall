/* Standard proc funct_fiscal_year */


UPDATE tmp_funct_fiscal_year
SET  processed_flag = 'Y'
where processed_flag IS NULL;

UPDATE tmp_funct_fiscal_year
SET upd_flag = 'N'
WHERE  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
 set pPeriv = (SELECT PERIV FROM T001 WHERE BUKRS = pCompanyCode)
WHERE  processed_flag = 'Y';

 UPDATE tmp_funct_fiscal_year
 SET pPrevPeriod = 12,pPrevFIYEAR = FiscalYear - 1,upd_flag = 'Q1A'
 WHERE Period = 1
AND  processed_flag = 'Y';


 UPDATE tmp_funct_fiscal_year
 SET pPrevPeriod = Period - 1,pPrevFIYEAR = FiscalYear,upd_flag = 'Q1B'
 WHERE upd_flag = 'N'
AND  processed_flag = 'Y';


UPDATE tmp_funct_fiscal_year
  set pVariant1 = (select XJABH FROM T009 WHERE PERIV = pPeriv)
WHERE  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
  set pVariant2 = (select XKALE FROM T009 WHERE PERIV = pPeriv)
WHERE  processed_flag = 'Y';


/**************************************************************************************MAIN IF SECTION ***************************/

 DROP TABLE IF EXISTS TMP_FFY_T009B_1;
 CREATE TABLE TMP_FFY_T009B_1
 AS
 SELECT PERIV,POPER,BDATJ,RELJR,MIN(BUMON) min_BUMON
 FROM T009B T,tmp_funct_fiscal_year R
WHERE PERIV = pPeriv and POPER = Period
and ((BDATJ = FiscalYear and RELJR = '0')
or (BDATJ = FiscalYear-1 and RELJR = '+1')
or (BDATJ = FiscalYear+1 and RELJR = '-1'))
AND  processed_flag = 'Y'
group BY PERIV,POPER,BDATJ,RELJR;

 DROP TABLE IF EXISTS TMP_FFY_T009B_2;
 CREATE TABLE TMP_FFY_T009B_2
 AS
 SELECT T.*
 FROM T009B T,TMP_FFY_T009B_1 x
 WHERE T.PERIV = x.PERIV
 AND T.POPER = x.POPER
 AND T.BDATJ = x.BDATJ
 AND T.RELJR = x.RELJR
 AND T.BUMON = x.min_BUMON;


UPDATE tmp_funct_fiscal_year
FROM TMP_FFY_T009B_2
SET pYearShift = RELJR,upd_flag = 'Q2A'
WHERE PERIV = pPeriv and POPER = Period
and ((BDATJ = FiscalYear and RELJR = '0')
or (BDATJ = FiscalYear-1 and RELJR = '+1')
or (BDATJ = FiscalYear+1 and RELJR = '-1'))
AND pVariant1 = 'X'
AND  processed_flag = 'Y';


UPDATE tmp_funct_fiscal_year
FROM TMP_FFY_T009B_1
SET pCalMth = min_BUMON,upd_flag = 'Q2B'
WHERE PERIV = pPeriv and POPER = Period
and ((BDATJ = FiscalYear and RELJR = '0')
or (BDATJ = FiscalYear-1 and RELJR = '+1')
or (BDATJ = FiscalYear+1 and RELJR = '-1'))
AND pVariant1 = 'X'
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear - 1, upd_flag = 'Q2C'
WHERE pVariant1 = 'X'
AND pYearShift = '+1'
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear + 1, upd_flag = 'Q2D'
WHERE pVariant1 = 'X'
AND pYearShift = '-1'
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear , upd_flag = 'Q2E'
WHERE pVariant1 = 'X'
AND upd_flag NOT IN ( 'Q2C','Q2D' )
AND  processed_flag = 'Y';


UPDATE tmp_funct_fiscal_year
SET pcalyear_minus_1 = pCalYear - 1
WHERE  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pcalyear_plus_1 = pCalYear + 1
WHERE  processed_flag = 'Y';

DROP TABLE IF EXISTS tmp_ffyr_minbutag;
CREATE TABLE tmp_ffyr_minbutag
AS
SELECT PERIV,POPER,BDATJ,BUMON,min(BUTAG) as min_BUTAG
FROM T009B
GROUP BY PERIV,POPER,BDATJ,BUMON;

UPDATE tmp_funct_fiscal_year
FROM tmp_ffyr_minbutag
SET   pCalMthDay = min_BUTAG
WHERE PERIV = pPeriv and POPER = Period and BDATJ = pCalYear and BUMON = pCalMth
AND pVariant1 = 'X'
AND  processed_flag = 'Y';


UPDATE tmp_funct_fiscal_year
/* SET CalDate = pCalMthDay || '/' || pCalMth || '/' || pCalYear */
SET CalDate = cast(pCalMth || '/' || pCalMthDay || '/' || pCalYear as date)
WHERE pVariant1 = 'X'
AND  processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_1;
CREATE TABLE TMP_T009B_1
as
/* SELECT PERIV,BDATJ,POPER,BUTAG||BUMON||BDATJ as tDT */
/* cast expects string in mm/dd/yyyy format */
SELECT PERIV,BDATJ,POPER,cast(BUMON || '/' || BUTAG || '/' || BDATJ as date) as tDT
FROM T009B, tmp_funct_fiscal_year
WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear - 1 or BDATJ = pCalYear + 1)
AND  processed_flag = 'Y';


 /* if (pVariant1 = 'X') */

/*Update pFromDate */

UPDATE tmp_funct_fiscal_year
SET pFromDate = ( SELECT MAX(tDT) + INTERVAL '1' DAY
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear - 1)
                        and POPER = 12
                        and tDT < CalDate )
WHERE pVariant1 = 'X'
AND Period = 1
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pFromDate = ( SELECT MAX(tDT) + INTERVAL '1' DAY
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv
                                            and (BDATJ = pCalYear or BDATJ = pCalYear - 1)
                        and POPER < Period
                        and tDT < CalDate )
WHERE pVariant1 = 'X'
AND Period > 1
AND  processed_flag = 'Y';

/*Update pToDate */

UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MIN(tDT)
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear + 1)
                        and POPER = 1
                        and tDT > CalDate )
WHERE pVariant1 = 'X'
AND Period = 12
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MIN(tDT)
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear + 1)
                        and POPER > Period
                        and tDT > CalDate )
WHERE pVariant1 = 'X'
AND Period < 12
AND  processed_flag = 'Y';


UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MAX(tDT)
                                from TMP_T009B_1
                                WHERE PERIV = pPeriv and POPER = Period
                                and tDT < pToDate )

WHERE pVariant1 = 'X'
AND  processed_flag = 'Y';


/*********************************************       ELSEIF SECTION      *********************************************************/
/********************************************(pVariant1 is null and pVariant2 is null)  ******************************************/

DROP TABLE IF EXISTS TMP_T009B_2a;
CREATE TABLE TMP_T009B_2a
as
SELECT PERIV,POPER,BDATJ,min(BUMON) min_BUMON
FROM T009B
GROUP BY PERIV,POPER,BDATJ;

DROP TABLE IF EXISTS TMP_T009B_2;
CREATE TABLE TMP_T009B_2
as
SELECT B.*
FROM T009B B,TMP_T009B_2a T
where B.PERIV = T.PERIV
AND B.POPER = T.POPER
AND B.BDATJ = T.BDATJ
AND B.BUMON = T.min_BUMON;

        UPDATE tmp_funct_fiscal_year
    set pYearShift = (SELECT RELJR FROM TMP_T009B_2
                      WHERE PERIV = pPeriv and POPER = Period and BDATJ = 0)
        WHERE pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

        UPDATE tmp_funct_fiscal_year
    set pCalMth = (SELECT min_BUMON FROM TMP_T009B_2a
                      WHERE PERIV = pPeriv and POPER = Period and BDATJ = 0)
        WHERE pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

        UPDATE tmp_funct_fiscal_year
        SET pCalYear = FiscalYear - 1
        WHERE pYearShift = '+1'
        AND  pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

        UPDATE tmp_funct_fiscal_year
        SET pCalYear = FiscalYear + 1
        WHERE pYearShift = '-1'
        AND  pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

        UPDATE tmp_funct_fiscal_year
        SET pCalYear = FiscalYear
        WHERE pYearShift NOT IN ( '+1','-1')
        AND  pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';


                UPDATE tmp_funct_fiscal_year
                SET  pcalyear_minus_1 = pCalYear - 1
                WHERE pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

                UPDATE tmp_funct_fiscal_year
                SET  pcalyear_plus_1 = pCalYear + 1
                WHERE pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';


        UPDATE tmp_funct_fiscal_year
        set pCalMthDay = (SELECT MIN(BUTAG) FROM T009B
                                  WHERE PERIV = pPeriv and POPER = Period and BDATJ = 0 and BUMON = pCalMth )
        WHERE pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

        UPDATE tmp_funct_fiscal_year
        set CalDate =  cast(case when MOD(pCalYear,4) <> 0 AND pCalMthDay || '/' || pCalMth = '29/2'
                                        then '2/28/' || pCalYear
                                        /* else pCalMthDay || '/' || pCalMth || '/' || pCalYear */
                                        else pCalMth || '/' || pCalMthDay || '/' || pCalYear
                                        END as date)
        WHERE pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_3A;
CREATE TABLE TMP_T009B_3A
AS
SELECT t.pPeriv,t.Period,t.CalDate,t.pCalYear,
cast(case when MOD(pCalYear,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
THEN ('2/28/' || pCalYear)  + INTERVAL '1' DAY
ELSE (BUMON||'/'||BUTAG||'/'||pCalYear) + INTERVAL '1' DAY
END as date)  tDT
FROM T009B,tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0
and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
AND CalDate > cast(case when MOD(pCalYear,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
                                        THEN '2/28/' || pCalYear
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear */
                                        ELSE BUMON||'/'||BUTAG||'/'||pCalYear
                                END as date)
AND t.pVariant1 is null and t.pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_3B;
CREATE TABLE TMP_T009B_3B
AS
SELECT  t.pPeriv,t.Period,t.CalDate,t.pCalYear,max(     tDT ) max_tDT
FROM TMP_T009B_3A t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
FROM TMP_T009B_3B y
SET x.pFromDate = y.max_tDT,upd_flag = upd_flag || 'E1'
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.CalDate = y.CalDate
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null and x.pVariant2 is null
AND  processed_flag = 'Y';



DROP TABLE IF EXISTS TMP_T009B_4A;
CREATE TABLE TMP_T009B_4A
AS
SELECT t.pPeriv,t.Period,t.CalDate,t.pCalYear,
cast(case when MOD(pCalYear_minus_1,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
THEN ('2/28/' || pCalYear_minus_1 ) + INTERVAL '1' DAY
ELSE (BUMON||'/'||BUTAG||'/'||pCalYear_minus_1) + INTERVAL '1' DAY
END as date)  tDT
FROM T009B,  tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0
and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
AND CalDate >  cast(case when MOD(pCalYear_minus_1,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
                                        THEN '2/28/' || pCalYear_minus_1
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear-1 */
                                        ELSE BUMON||'/'||BUTAG||'/'||pCalYear_minus_1
                                END as date)
AND t.pVariant1 is null and t.pVariant2 is null
AND pFromDate is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_4B;
CREATE TABLE TMP_T009B_4B
AS
SELECT  t.pPeriv,t.Period,t.CalDate,t.pCalYear,max(     tDT ) max_tDT
FROM TMP_T009B_4A t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
FROM TMP_T009B_4B y
SET x.pFromDate = y.max_tDT,upd_flag = upd_flag || 'E2'
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.CalDate = y.CalDate
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null and x.pVariant2 is null
AND x.pFromDate is null
AND  processed_flag = 'Y';


/* To date queries now. */

/* To Date Q1 */

DROP TABLE IF EXISTS TMP_T009B_3A_TO;
CREATE TABLE TMP_T009B_3A_TO
AS
SELECT t.pPeriv,t.Period,t.CalDate,t.pCalYear,
cast(case when MOD(pCalYear,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
THEN ('2/28/' || pCalYear)
ELSE (BUMON||'/'||BUTAG||'/'||pCalYear)
END as date)  tDT
FROM T009B,tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0
and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
AND CalDate < cast(case when MOD(pCalYear,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
                                        THEN '2/28/' || pCalYear
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear */
                                        ELSE BUMON||'/'||BUTAG||'/'||pCalYear
                                END as date)
AND t.pVariant1 is null and t.pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_3B_TO;
CREATE TABLE TMP_T009B_3B_TO
AS
SELECT  t.pPeriv,t.Period,t.CalDate,t.pCalYear,min(     tDT ) min_tDT
FROM TMP_T009B_3A_TO t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
FROM TMP_T009B_3B_TO y
SET x.pToDate1 = y.min_tDT,upd_flag = upd_flag || 'F1'
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.CalDate = y.CalDate
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null and x.pVariant2 is null
AND  processed_flag = 'Y';


/* Q2 */

DROP TABLE IF EXISTS TMP_T009B_4A_TO;
CREATE TABLE TMP_T009B_4A_TO
AS
SELECT t.pPeriv,t.Period,t.CalDate,t.pCalYear,
cast(case when MOD(pCalYear_plus_1,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
THEN ('2/28/' || pCalYear_plus_1 )
ELSE (BUMON||'/'||BUTAG||'/'||pCalYear_plus_1)
END as date)  tDT
FROM T009B, tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0
and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
AND CalDate <  cast(case when MOD(pCalYear_plus_1,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
                                        THEN '2/28/' || pCalYear_plus_1
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear+1 */
                                        ELSE BUMON||'/'||BUTAG||'/'||pCalYear_plus_1
                                END as date)
AND t.pVariant1 is null and t.pVariant2 is null
AND pToDate1 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_4B_TO;
CREATE TABLE TMP_T009B_4B_TO
AS
SELECT  t.pPeriv,t.Period,t.CalDate,t.pCalYear,min(     tDT ) min_tDT
FROM TMP_T009B_4A_TO t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
FROM TMP_T009B_4B_TO y
SET x.pToDate1 = y.min_tDT,upd_flag = upd_flag || 'F2'
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.CalDate = y.CalDate
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null and x.pVariant2 is null
AND x.pToDate1 is null
AND  processed_flag = 'Y';


/* To Date Q3 */

DROP TABLE IF EXISTS TMP_TO_3;
CREATE TABLE TMP_TO_3
AS
SELECT t.pPeriv,t.Period,t.pToDate1,t.pCalYear,
cast(case when MOD(pCalYear_plus_1,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
THEN ('2/28/' || pCalYear_plus_1 )
ELSE (BUMON||'/'||BUTAG||'/'||pCalYear_plus_1)
END as date)  tDT
FROM T009B, tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0 and POPER = Period
and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
AND pToDate1 >  cast(case when MOD(pCalYear_plus_1,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
                                        THEN '2/28/' || pCalYear_plus_1
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear+1 */
                                        ELSE BUMON||'/'||BUTAG||'/'||pCalYear_plus_1
                                END as date)
AND t.pVariant1 is null and t.pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_TO_3B;
CREATE TABLE TMP_TO_3B
AS
SELECT  t.pPeriv,t.Period,t.pToDate1,t.pCalYear,max(tDT ) max_tDT
FROM TMP_TO_3 t
GROUP by t.pPeriv,t.Period,t.pToDate1,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
FROM TMP_TO_3B y
SET x.pToDate = y.max_tDT,upd_flag = upd_flag || 'G1'
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.pToDate1 = y.pToDate1
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null and x.pVariant2 is null
AND  processed_flag = 'Y';


/* To Date Q4 */

DROP TABLE IF EXISTS TMP_TO_4;
CREATE TABLE TMP_TO_4
AS
SELECT t.pPeriv,t.Period,t.pToDate1,t.pCalYear,
cast(case when MOD(pCalYear,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
THEN ('2/28/' || pCalYear )
ELSE (BUMON||'/'||BUTAG||'/'||pCalYear)
END as date)  tDT
FROM T009B, tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0 and POPER = Period
AND pToDate1 >  cast(case when MOD(pCalYear,4) <> 0 AND BUTAG||'/'||BUMON = '29/2'
                                        THEN '2/28/' || pCalYear
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear */
                                        ELSE BUMON||'/'||BUTAG||'/'||pCalYear
                                END as date)
AND t.pVariant1 is null and t.pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_TO_4B;
CREATE TABLE TMP_TO_4B
AS
SELECT  t.pPeriv,t.Period,t.pToDate1,t.pCalYear,max(tDT ) max_tDT
FROM TMP_TO_4 t
GROUP by t.pPeriv,t.Period,t.pToDate1,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
FROM TMP_TO_4B y
SET x.pToDate = y.max_tDT,upd_flag = upd_flag || 'G2'
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.pToDate1 = y.pToDate1
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null and x.pVariant2 is null
AND x.pToDate is null
AND  processed_flag = 'Y';


/************ELSE SECTION ***************************/

UPDATE tmp_funct_fiscal_year
/*set pFromDate = '01/' || Period ||  '/' || FiscalYear*/
set pFromDate = Period ||  '/' ||'01/' || FiscalYear
WHERE   ifnull(pVariant1,'Y') <> 'X' AND ( pVariant1 IS NOT NULL or pVariant2 IS NOT NULL )
AND Period >=1 AND Period <= 12
AND FiscalYear >= 1900
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
set pToDate = DATE(pFromDate)  + INTERVAL '1' MONTH - INTERVAL '1' DAY
WHERE ifnull(pVariant1,'Y') <> 'X' AND ( pVariant1 IS NOT NULL or pVariant2 IS NOT NULL )
AND  processed_flag = 'Y';

/************End of if-elseif-else ***************************/

UPDATE tmp_funct_fiscal_year
SET pReturn = cast(pFromDate as char(20)) || '|' || cast(pToDate as char(20))
WHERE  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_FFY_T009B_1;
DROP TABLE IF EXISTS TMP_FFY_T009B_2;
DROP TABLE IF EXISTS TMP_T009B_1;
DROP TABLE IF EXISTS TMP_T009B_2a;
DROP TABLE IF EXISTS TMP_T009B_2;
DROP TABLE IF EXISTS TMP_T009B_3A;
DROP TABLE IF EXISTS TMP_T009B_3B;
DROP TABLE IF EXISTS TMP_T009B_4A;
DROP TABLE IF EXISTS TMP_T009B_4B;
DROP TABLE IF EXISTS TMP_T009B_3A_TO;
DROP TABLE IF EXISTS TMP_T009B_3B_TO;
DROP TABLE IF EXISTS TMP_T009B_4A_TO;
DROP TABLE IF EXISTS TMP_T009B_4B_TO;
DROP TABLE IF EXISTS TMP_TO_3;
DROP TABLE IF EXISTS TMP_TO_3B;
DROP TABLE IF EXISTS TMP_TO_4;
DROP TABLE IF EXISTS TMP_TO_4B;
DROP TABLE IF EXISTS tmp_ffyr_minbutag;

UPDATE tmp_funct_fiscal_year
SET processed_flag = 'D'
WHERE processed_flag = 'Y';

