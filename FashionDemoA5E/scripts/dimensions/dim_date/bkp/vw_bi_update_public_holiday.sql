UPDATE dim_date a
From TFACS t ,dim_plant pl
SET a.IsaPublicHoliday = CASE SUBSTRING(case a.CalendarMonthNumber 
                                              when 1 then t.TFACS_MON01
                                              when 2 then t.TFACS_MON02
                                              when 3 then t.TFACS_MON03
                                              when 4 then t.TFACS_MON04
                                              when 5 then t.TFACS_MON05
                                              when 6 then t.TFACS_MON06
                                              when 7 then t.TFACS_MON07
                                              when 8 then t.TFACS_MON08
                                              when 9 then t.TFACS_MON09
                                              when 10 then t.TFACS_MON10
                                              when 11 then t.TFACS_MON11
                                              when 12 then t.TFACS_MON12
                                          end, a.DayofMonth, 1) WHEN '0' then 1 else 0 END
Where pl.FactoryCalendarKey = t.TFACS_IDENT AND a.CompanyCode = pl.CompanyCode and t.TFACS_JAHR = a.CalendarYear ;
