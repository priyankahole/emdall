UPDATE    dim_notificationorigin no
       FROM
          DD07T t
   SET no.NotificationOriginName = t.DD07T_DDTEXT,
			no.dw_update_date = current_timestamp
 WHERE no.RowIsCurrent = 1
       AND t.DD07T_DOMNAME = 'HERKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND no.NotificationOriginCode = t.DD07T_DOMVALUE 
;

INSERT INTO dim_notificationorigin(dim_notificationoriginId, RowIsCurrent,rowstartdate)
SELECT 1, 1,current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_notificationorigin
               WHERE dim_notificationoriginId = 1);


delete from number_fountain m where m.table_name = 'dim_notificationorigin';
   
insert into number_fountain
select 	'dim_notificationorigin',
	ifnull(max(d.Dim_NotificationOriginid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_notificationorigin d
where d.Dim_NotificationOriginid <> 1; 

INSERT INTO dim_notificationorigin(Dim_NotificationOriginid,
                                       NotificationOriginCode,
                                       NotificationOriginName,
                                       RowStartDate,
                                       RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_notificationorigin') 
          + row_number() over() ,
			 t.DD07T_DOMVALUE,
          t.DD07T_DDTEXT,
          current_timestamp,
          1
     FROM DD07T t
    WHERE t.DD07T_DOMNAME = 'HERKZ' AND t.DD07T_DOMVALUE IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_notificationorigin s
                  WHERE     s.NotificationOriginCode = t.DD07T_DOMVALUE
                        AND t.DD07T_DOMNAME = 'HERKZ'
                        AND s.RowIsCurrent = 1)
;
