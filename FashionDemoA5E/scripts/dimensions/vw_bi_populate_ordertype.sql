UPDATE    dim_OrderType o
       FROM
          DD07T t
   SET o.Description = ifnull(t.DD07T_DDTEXT, 'Not Set'),
			o.dw_update_date = current_timestamp
 WHERE o.RowIsCurrent = 1
  AND t.DD07T_DOMNAME = 'PAART'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND o.OrderTypeCode = t.DD07T_DOMVALUE;

INSERT INTO dim_OrderType(dim_OrderTypeid, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_OrderType
               WHERE dim_OrderTypeid = 1);

delete from number_fountain m where m.table_name = 'dim_OrderType';
   
insert into number_fountain
select 	'dim_OrderType',
	ifnull(max(d.Dim_OrderTypeId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_OrderType d
where d.Dim_OrderTypeId <> 1; 

INSERT INTO dim_OrderType(Dim_OrderTypeId,
                                                        Description,
                           OrderTypeCode,
                           RowStartDate,
                           RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_OrderType')
         + row_number() over(),
                          ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'PAART' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_OrderType
                    WHERE OrderTypeCode = DD07T_DOMVALUE AND DD07T_DOMNAME = 'PAART')
   ORDER BY 2 OFFSET 0;
