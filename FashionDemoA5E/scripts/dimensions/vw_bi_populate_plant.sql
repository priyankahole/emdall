\i /db/schema_migration/bin/wrapper_optimizedb.sh t001w;

\i /db/schema_migration/bin/wrapper_optimizedb.sh dim_plant;

INSERT INTO dim_plant 
(dim_plantid
,RowIsCurrent)
SELECT 1, 1
FROM (SELECT 1) a
WHERE NOT EXISTS ( SELECT 'x' FROM dim_plant WHERE dim_plantid = 1);

UPDATE dim_plant FROM T001W t, t001k k, T005T
   SET CompanyCode = k.BUKRS,
       Name = t.NAME1,
       PostalCode = ifnull(PSTLZ, 'Not Set'),
       City = ifnull(ORT01, 'Not Set'),
       State = ifnull(REGIO, 'Not Set'),
       Country = ifnull(LAND1, 'Not Set'),
       SalesOrg = case when SalesOrg = 'Not Set' then SalesOrg
 else ifnull(T001W_VKORG, 'Not Set') end,
       PurchOrg =
          CASE
             WHEN PurchOrg = 'Not Set' THEN PurchOrg
             ELSE ifnull(EKORG, WERKS)
          END,
       PlanningPlant = ifnull(T001W_IWERK, 'Not Set'),
       ValuationArea = t.BWKEY,
       FactoryCalendarKey = ifnull(t.FABKL, 'Not Set'),
       LanguageKey = ifnull(t.SPRAS, 'E'),
       CountryName = ifnull(t005t_landx, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE t.bwkey = k.bwkey AND PlantCode = t.werks AND t.LAND1 = T005T_LAND1;

\i /db/schema_migration/bin/wrapper_optimizedb.sh t001w;

\i /db/schema_migration/bin/wrapper_optimizedb.sh dim_plant;

DROP TABLE IF EXISTS tmp_dim_plant_insert;
CREATE TABLE tmp_dim_plant_insert
AS
SELECT WERKS PlantCode
FROM t001w;

DROP TABLE IF EXISTS tmp_dim_plant_delete;
CREATE TABLE tmp_dim_plant_delete
AS
SELECT *
FROM tmp_dim_plant_insert
WHERE 1=2;

INSERT INTO tmp_dim_plant_delete
SELECT PlantCode 
FROM dim_plant;


CALL VECTORWISE(combine 'tmp_dim_plant_insert-tmp_dim_plant_delete');

DROP TABLE IF EXISTS tmp_dim_plant_insert2;
CREATE TABLE tmp_dim_plant_insert2
AS
SELECT t.* FROM t001w t,tmp_dim_plant_insert i
WHERE t.WERKS = i.PlantCode;

delete from number_fountain m where m.table_name = 'dim_plant';
   
insert into number_fountain
select 	'dim_plant',
	ifnull(max(d.dim_plantid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_plant d
where d.dim_plantid <> 1; 

INSERT INTO dim_plant(dim_plantid,
		      PlantCode,
                      Name,
                      PostalCode,
                      City,
                      State,
                      Country,
                      SalesOrg,
                      PurchOrg,
                      PlanningPlant,
                      ValuationArea,
                      LanguageKey,
                      RowStartDate,
                      RowIsCurrent)
   SELECT 
   (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_plant')
          + row_number()
             over(),
   	      t.WERKS,
          t.NAME1,
          ifnull(t.PSTLZ, 'Not Set'),
          ifnull(t.ORT01, 'Not Set'),
          ifnull(t.REGIO, 'Not Set'),
          ifnull(t.LAND1, 'Not Set'),
          ifnull(t.T001W_VKORG, 'Not Set'),
          ifnull(t.EKORG, t.WERKS),
          ifnull(t.T001W_IWERK, 'Not Set'),
          ifnull(t.BWKEY, 'Not Set'),
          ifnull(t.SPRAS, 'E'),
          current_date,
          1
     FROM t001w t,tmp_dim_plant_insert2 p2
    WHERE p2.WERKS = t.WERKS;

DROP TABLE IF EXISTS tmp_dim_plant_insert;
DROP TABLE IF EXISTS tmp_dim_plant_delete;
DROP TABLE IF EXISTS tmp_dim_plant_insert2;