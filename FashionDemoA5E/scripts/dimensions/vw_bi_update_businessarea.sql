
UPDATE    dim_businessarea ba
       FROM
          TGSBT t
       SET ba.Description = t.TGSBT_GTEXT
 WHERE ba.RowIsCurrent = 1
 AND ba.BusinessArea = t.TGSBT_GSBER;
