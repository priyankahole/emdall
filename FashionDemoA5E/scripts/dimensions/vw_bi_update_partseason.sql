
UPDATE dim_partseason pse
FROM J_3AMSEA t,J_3ASEANT
SET pse.Description = ifnull(J_3ASEANT_TEXT,'Not Set')
 WHERE pse.RowIsCurrent = 1
 AND pse.SeasonIndicator = ifnull(J_3AMSEA_J_3ASEAN, 'Not Set')
 AND pse.Collection = ifnull(J_3AMSEA_AFSCOLLECTION, 'Not Set')
 AND pse.Theme = ifnull(J_3AMSEA_AFSTHEME, 'Not Set')
 AND pse.GridValue = ifnull(J_3AMSEA_J_3ASIZE,'Not Set')
 AND pse.CategoryValue = ifnull(J_3AMSEA_J_4KRCAT, 'Not Set')
 AND pse.PartNumber = ifnull(J_3AMSEA_MATNR,'Not Set')
AND J_3ASEANT_J_3ASEAN = J_3AMSEA_J_3ASEAN
AND J_3ASEANT_AFS_COLLECTION = J_3AMSEA_AFSCOLLECTION
AND J_3ASEANT_AFS_THEME = J_3AMSEA_AFSTHEME
AND pse.Description <> ifnull(J_3ASEANT_TEXT,'Not Set');

UPDATE    dim_partseason pse
from J_3AMSEA t, MEAN
SET pse.InternationalPartNumber = ifnull(MEAN_EAN11,'Not Set')
 WHERE pse.RowIsCurrent = 1
 AND pse.SeasonIndicator = ifnull(J_3AMSEA_J_3ASEAN, 'Not Set')
 AND pse.Collection = ifnull(J_3AMSEA_AFSCOLLECTION, 'Not Set')
 AND pse.Theme = ifnull(J_3AMSEA_AFSTHEME, 'Not Set')
 AND pse.GridValue = ifnull(J_3AMSEA_J_3ASIZE,'Not Set')
 AND pse.CategoryValue = ifnull(J_3AMSEA_J_4KRCAT, 'Not Set')
 AND pse.PartNumber = ifnull(J_3AMSEA_MATNR,'Not Set')
AND MEAN_J_3AKORDX = J_3AMSEA_J_3ASIZE
AND MEAN_MATNR = J_3AMSEA_MATNR
AND pse.InternationalPartNumber <> ifnull(MEAN_EAN11,'Not Set');

