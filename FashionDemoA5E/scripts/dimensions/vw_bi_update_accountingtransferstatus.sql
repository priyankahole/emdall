
UPDATE    dim_accountingtransferstatus ats
       FROM
          DD07T d
        SET ats.AccountingTransferStatusName = ifnull(DD07T_DDTEXT, 'Not Set')
       WHERE     ats.AccountingTransferStatusCode = d.DD07T_DOMVALUE
          AND d.DD07T_DOMNAME = 'RFBSK'
          AND d.DD07T_DOMVALUE IS NOT NULL
                AND ats.RowIsCurrent = 1;
