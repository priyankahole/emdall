insert into Dim_BlockingPaymentReason(Dim_BlockingPaymentReasonId,BlockingKeyPayment,ExplainReasonForPaymentBlock,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason)
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from Dim_BlockingPaymentReason where Dim_BlockingPaymentReasonid = 1);

UPDATE    dim_blockingpaymentreason bpr
       FROM
          T008T t
   SET bpr.ExplainReasonForPaymentBlock = ifnull(T008T_TEXTL, 'Not Set'),
		bpr.dw_update_date = current_timestamp
       WHERE t.T008T_ZAHLS = bpr.BlockingKeyPayment AND bpr.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'Dim_BlockingPaymentReason';

insert into number_fountain
select 	'Dim_BlockingPaymentReason',
	ifnull(max(d.Dim_BlockingPaymentReasonId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_BlockingPaymentReason d
where d.Dim_BlockingPaymentReasonId <> 1;
	   
INSERT INTO dim_blockingpaymentreason(Dim_BlockingPaymentReasonId,
									  BlockingKeyPayment,
                                      ExplainReasonForPaymentBlock,
                                      RowStartDate,
                                      RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_BlockingPaymentReason') 
          + row_number() over() ,
			T008T_ZAHLS,
          T008T_TEXTL,
          current_timestamp,
          1
     FROM T008T t
    WHERE t.T008T_SPRAS = 'E'
          AND NOT EXISTS
                     (SELECT 1
                        FROM dim_blockingpaymentreason bpr
                       WHERE bpr.BlockingKeyPayment = t.T008T_ZAHLS
                             AND bpr.RowIsCurrent = 1);
							 
delete from number_fountain m where m.table_name = 'Dim_BlockingPaymentReason';
							 