UPDATE       dim_notificationpriority s
          FROM
             T356_T t1
       INNER JOIN
          T356A_T t2
       ON t1.T356_T_ARTPR = t2.T356A_T_ARTPR
   SET s.NotificationPriorityName = T356_T_PRIOKX
 WHERE s.RowIsCurrent = 1
 AND  s.NotificationPriorityType = t1.T356_T_ARTPR
             AND s.NotificationPriorityCode = t1.T356_T_PRIOK
;