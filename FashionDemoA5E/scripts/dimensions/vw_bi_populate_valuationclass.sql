UPDATE    dim_ValuationClass vc
       FROM
          t025t t
   SET DescriptionOfValuationClass = ifnull(T025T_BKBEZ, 'Not Set'),
			vc.dw_update_date = current_timestamp
 WHERE vc.RowIsCurrent = 1
 AND t.T025T_BKLAS = vc.ValuationClass;
 
INSERT INTO dim_ValuationClass(dim_ValuationClassid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_ValuationClass
               WHERE dim_ValuationClassid = 1);


drop table if exists tmp_dim_ValuationClass_t025t_ins;
create table tmp_dim_ValuationClass_t025t_ins as
select T025T_BKLAS from t025t where 1 <> 1;

insert into tmp_dim_ValuationClass_t025t_ins
select  T025T_BKLAS
FROM    t025t ;

drop table if exists tmp_dim_ValuationClass_t025t_del;
create table tmp_dim_ValuationClass_t025t_del as
select T025T_BKLAS from t025t where 1 <> 1;

insert into tmp_dim_ValuationClass_t025t_del
select vc.ValuationClass
from dim_ValuationClass vc;

call vectorwise (combine 'tmp_dim_ValuationClass_t025t_ins-tmp_dim_ValuationClass_t025t_del');

delete from number_fountain m where m.table_name = 'dim_ValuationClass';

insert into number_fountain
select 	'dim_ValuationClass',
	ifnull(max(d.dim_valuationclassid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ValuationClass d
where d.dim_valuationclassid <> 1;	

INSERT INTO dim_ValuationClass(dim_valuationclassid,
                                                                DescriptionOfValuationClass,
                                                                ValuationClass,
                                                                RowStartDate,
                                                                RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_ValuationClass')
          + row_number() over(),
                 ifnull(t025t.T025T_BKBEZ, 'Not Set'),
                 ifnull(t025t.T025T_BKLAS, 'Not Set'),
        current_timestamp,
         1
     FROM t025t, tmp_dim_ValuationClass_t025t_ins vc
    WHERE T025T_BKBEZ IS NOT NULL
    AND t025t.T025T_BKLAS = vc.T025T_BKLAS;

call vectorwise (combine 'tmp_dim_ValuationClass_t025t_ins-tmp_dim_ValuationClass_t025t_ins');
call vectorwise (combine 'tmp_dim_ValuationClass_t025t_del-tmp_dim_ValuationClass_t025t_del');

