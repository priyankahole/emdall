UPDATE    dim_purchaseorg po
       FROM
          t024e t
   SET po.CompanyCode = ifnull(t.T024E_BUKRS, 'Not Set'),
       po.Name = ifnull(t.T024E_EKOTX, 'Not Set'),
			po.dw_update_date = current_timestamp
 WHERE po.RowIsCurrent = 1
 AND po.PurchaseOrgCode = t.T024E_EKORG
;

INSERT INTO dim_purchaseorg(dim_purchaseorgId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_purchaseorg
               WHERE dim_purchaseorgId = 1);

delete from number_fountain m where m.table_name = 'dim_purchaseorg';
   
insert into number_fountain
select 	'dim_purchaseorg',
	ifnull(max(d.Dim_PurchaseOrgid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_purchaseorg d
where d.Dim_PurchaseOrgid <> 1; 

INSERT INTO dim_purchaseorg(Dim_PurchaseOrgid,
                            CompanyCode,
                            Name,
                            PurchaseOrgCode,
                            RowStartDate,
                            RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_purchaseorg')
         + row_number() over(),
			 ifnull(t.T024E_BUKRS, 'Not Set'),
          ifnull(t.T024E_EKOTX, 'Not Set'),
          t.T024E_EKORG,
          current_timestamp,
          1
     FROM t024e t
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_purchaseorg
                       WHERE PurchaseOrgCode = T024E_EKORG and rowiscurrent = 1)
;

