UPDATE    Dim_MaterialStagingZone ms
       FROM
          T30CT t
   SET ms.MSAreaTxt = ifnull(t.T30CT_LBZOT, 'Not Set')
 WHERE ms.RowIsCurrent = 1
 AND ms.WarehouseCode = t.T30CT_LGNUM AND ms.StagingArea = t.T30CT_LGBZO
;