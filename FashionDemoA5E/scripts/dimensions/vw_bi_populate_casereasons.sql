insert into dim_casereasons
(dim_casereasonsid, casetype, reasoncode, description, rowstartdate, rowenddate, rowiscurrent, rowchangereason) 
SELECT 1,'Not Set','Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_casereasons where dim_casereasonsid = 1);


UPDATE    dim_casereasons ds
       FROM
          SCMGATTR_REASONT dt
   SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set'),
		ds.dw_update_date = current_timestamp
 WHERE ds.RowIsCurrent = 1
          AND ds.ReasonCode = dt.REASON_CODE
          AND dt.REASON_CODE IS NOT NULL;
		  
 
delete from number_fountain m where m.table_name = 'dim_casereasons';

insert into number_fountain
select 	'dim_casereasons',
	ifnull(max(d.dim_casereasonsid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_casereasons d
where d.dim_casereasonsid <> 1; 
 
INSERT INTO dim_casereasons(dim_casereasonsid,
			       CaseType,
			       ReasonCode,
                               Description,
                               RowStartDate,
                               RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_casereasons') 
          + row_number() over() ,
			 ifnull(CASE_TYPE, 'Not Set'),
			 ifnull(REASON_CODE, 'Not Set'),
            ifnull(DESCRIPTION,'Not Set'),
            current_timestamp,
            1
       FROM SCMGATTR_REASONT
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_casereasons
                    WHERE ReasonCode = REASON_CODE)
   ORDER BY 2 OFFSET 0;
					 
delete from number_fountain m where m.table_name = 'dim_casereasons';	

					 