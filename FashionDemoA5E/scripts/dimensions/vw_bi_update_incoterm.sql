UPDATE    dim_incoterm it
       FROM
          tinct t
   SET it.Name = t.TINCT_BEZEI
WHERE it.RowIsCurrent = 1
AND it.IncoTermCode = t.TINCT_INCO1;

