
UPDATE dim_producthierarchy ph
   SET ph.Level1Desc = ifnull
                   ((SELECT ph1.VTEXT
                      FROM t179t ph1
                     WHERE length(ph.ProductHierarchy) >= 2
					 AND ph1.PRODH = substring(ph.ProductHierarchy, 1, 2)),
                   'Not Set'),
		ph.Level1Code = (CASE WHEN ProductHierarchy <> 'Not Set' AND length(ph.ProductHierarchy) >= 2 THEN substring(ph.ProductHierarchy, 1, 2) ELSE 'Not Set' END),
		ph.Level2Desc = ifnull
                   ((SELECT ph1.VTEXT
                      FROM t179t ph1
                     WHERE length(ph.ProductHierarchy) >= 4
					 AND ph1.PRODH = substring(ph.ProductHierarchy, 1, 4)),
                   'Not Set'),
		ph.Level2Code = (CASE WHEN ProductHierarchy <> 'Not Set' AND length(ph.ProductHierarchy) >= 4 THEN substring(ph.ProductHierarchy, 1, 4) ELSE 'Not Set' END),
		ph.Level3Desc = ifnull
                   ((SELECT ph1.VTEXT
                      FROM t179t ph1
                     WHERE length(ph.ProductHierarchy) >= 6
					 AND ph1.PRODH = substring(ph.ProductHierarchy, 1, 6)),
                   'Not Set'),
		ph.Level3Code = (CASE WHEN ProductHierarchy <> 'Not Set' AND length(ph.ProductHierarchy) >= 6 THEN substring(ph.ProductHierarchy, 1, 6) ELSE 'Not Set' END),
		ph.Level4Desc = ifnull
                   ((SELECT ph1.VTEXT
                      FROM t179t ph1
                     WHERE length(ph.ProductHierarchy) >= 9
					 AND ph1.PRODH = substring(ph.ProductHierarchy, 1, 9)),
                   'Not Set'),
		ph.Level4Code = (CASE WHEN ProductHierarchy <> 'Not Set' AND length(ph.ProductHierarchy) >= 9 THEN substring(ph.ProductHierarchy, 1, 9) ELSE 'Not Set' END),
		ph.Level5Desc = ifnull
                   ((SELECT ph1.VTEXT
                      FROM t179t ph1
                     WHERE length(ph.ProductHierarchy) >= 13
					 AND ph1.PRODH = substring(ph.ProductHierarchy, 1, 13)),
                   'Not Set'),
		ph.Level5Code = (CASE WHEN ProductHierarchy <> 'Not Set' AND length(ph.ProductHierarchy) >= 13 THEN substring(ph.ProductHierarchy, 1, 13) ELSE 'Not Set' END),					   
		ph.Level6Desc = ifnull
                   ((SELECT ph1.VTEXT
                      FROM t179t ph1
                     WHERE length(ph.ProductHierarchy) >= 18
					 AND ph1.PRODH = substring(ph.ProductHierarchy, 1, 18)),
                   'Not Set'),
		ph.Level6Code = (CASE WHEN ProductHierarchy <> 'Not Set' AND length(ph.ProductHierarchy) >= 18 THEN substring(ph.ProductHierarchy, 1, 18) ELSE 'Not Set' END),
		ph.dw_update_date = current_timestamp;