UPDATE    dim_incoterm it
       FROM
          tinct t
   SET it.Name = t.TINCT_BEZEI,
			it.dw_update_date = current_timestamp
WHERE it.RowIsCurrent = 1
AND it.IncoTermCode = t.TINCT_INCO1;

INSERT INTO dim_incoterm(dim_incotermid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_incoterm
               WHERE dim_incotermid = 1);

delete from number_fountain m where m.table_name = 'dim_incoterm';
   
insert into number_fountain
select 	'dim_incoterm',
	ifnull(max(d.Dim_IncoTermid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_incoterm d
where d.Dim_IncoTermid <> 1; 

INSERT INTO dim_incoterm(Dim_IncoTermid,
                                                Name,
                         IncoTermCode,
                         RowStartDate,
                         RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_incoterm')
          + row_number() over(),
          TINCT_BEZEI,
          TINCT_INCO1,
          current_timestamp,
          1
     FROM tinct
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_incoterm
                       WHERE IncoTermCode = TINCT_INCO1);

