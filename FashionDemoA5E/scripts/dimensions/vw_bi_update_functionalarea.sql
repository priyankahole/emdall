UPDATE    dim_FunctionalArea fa
       FROM
          TFKBT t
   SET fa.Description = t.TFKBT_FKBTX
 WHERE fa.RowIsCurrent = 1
 AND fa.FunctionalArea = t.TFKBT_FKBER;

