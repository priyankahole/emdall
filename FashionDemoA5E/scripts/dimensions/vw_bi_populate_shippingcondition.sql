INSERT INTO dim_shippingcondition(dim_ShippingConditionid,
                                  ShippingConditionCode,
                                  ShippingConditionName,
                                  RowStartDate,
                                  RowEndDate,
                                  RowIsCurrent,
                                  RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          NULL,
          NULL,
          NULL,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_shippingcondition
               WHERE dim_shippingconditionid = 1);

UPDATE    dim_shippingcondition s
       FROM
          TVSBT t
 SET s.ShippingConditionName = t.TVSBT_VTEXT,
			s.dw_update_date = current_timestamp
 WHERE s.RowIsCurrent = 1
 AND s.ShippingConditionCode = t.TVSBT_VSBED;

delete from number_fountain m where m.table_name = 'dim_shippingcondition';

insert into number_fountain
select 	'dim_shippingcondition',
	ifnull(max(d.dim_ShippingConditionid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_shippingcondition d
where d.dim_ShippingConditionid <> 1; 

 INSERT INTO dim_shippingcondition(dim_ShippingConditionid,
                                ShippingConditionCode,
                                ShippingConditionName,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_shippingcondition') 
          + row_number() over(),
	  t.TVSBT_VSBED,
          t.TVSBT_VTEXT,
          current_timestamp,
          1
     FROM TVSBT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_shippingcondition r
               WHERE r.ShippingConditionCode = t.TVSBT_VSBED AND r.RowIsCurrent = 1);