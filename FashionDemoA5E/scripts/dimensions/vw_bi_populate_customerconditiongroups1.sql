
INSERT INTO dim_CustomerConditionGroups1(dim_CustomerConditionGroups1id, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_CustomerConditionGroups1
               WHERE dim_CustomerConditionGroups1id = 1);

UPDATE dim_CustomerConditionGroups1 a FROM TVKGGT
   SET a.Description = ifnull(TVKGGT_VTEXT, 'Not Set'),
			a.dw_update_date = current_timestamp
 WHERE a.CustomerCondGrp1 = TVKGGT_KDKGR AND RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_CustomerConditionGroups1';

insert into number_fountain
select 	'dim_CustomerConditionGroups1',
	ifnull(max(d.dim_CustomerConditionGroups1id), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_CustomerConditionGroups1 d
where d.dim_CustomerConditionGroups1id <> 1;

INSERT INTO dim_CustomerConditionGroups1(dim_CustomerConditionGroups1Id,
                                    CustomerCondGrp1,
                                    Description,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_CustomerConditionGroups1')
			+ row_number() over() ,
          t.TVKGGT_KDKGR,
          ifnull(TVKGGT_VTEXT, 'Not Set'),
          current_timestamp,
          1
     FROM TVKGGT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM  dim_CustomerConditionGroups1 a
               WHERE a.CustomerCondGrp1 = TVKGGT_KDKGR);

delete from number_fountain m where m.table_name = 'dim_CustomerConditionGroups1';