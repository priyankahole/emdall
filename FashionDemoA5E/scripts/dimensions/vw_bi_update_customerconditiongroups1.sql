
UPDATE dim_CustomerConditionGroups1 a FROM TVKGGT
   SET a.Description = ifnull(TVKGGT_VTEXT, 'Not Set')
 WHERE a.CustomerCondGrp1 = TVKGGT_KDKGR AND RowIsCurrent = 1;
