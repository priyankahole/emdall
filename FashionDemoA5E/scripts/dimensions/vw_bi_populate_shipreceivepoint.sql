INSERT INTO dim_shipreceivepoint(Dim_ShipReceivePointid,
                                 ShipReceivePointCode,
                                 Description,
                                 FactoryCalendarKey,
                                 RoundupPeriodDays,
                                 AddressKey,
                                 Country,
                                 CountryName)
   SELECT 1,
          'Not Set',
          'Not Set',
          'Not Set',
          0.00,
          'Not Set',
          'Not Set',
          'Not Set'
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_shipreceivepoint
               WHERE dim_shipreceivepointid = 1);

UPDATE       dim_shipreceivepoint a
          FROM
             TVST s
       INNER JOIN
          TVSTT t
       ON s.TVST_VSTEL = t.TVSTT_VSTEL AND t.TVSTT_SPRAS = 'E'
	INNER JOIN T005T ON s.TVST_ALAND = T005T_LAND1
   SET a.Description = ifnull(TVSTT_VTEXT, 'Not Set'),
       a.FactoryCalendarKey = ifnull(TVST_FABKL, 'Not Set'),
       a.RoundupPeriodDays = ifnull(TVST_VTRZT, 'Not Set'),
       a.AddressKey = ifnull(TVST_ADRNR, 'Not Set'),
       a.Country = ifnull(TVST_ALAND, 'Not Set'),
	a.CountryName = ifnull(t005t_landx, 'Not Set'),
			a.dw_update_date = current_timestamp
	WHERE  a.ShipReceivePointCode = s.TVST_VSTEL;

delete from number_fountain m where m.table_name = 'dim_shipreceivepoint';

insert into number_fountain
select 	'dim_shipreceivepoint',
	ifnull(max(d.Dim_ShipReceivePointid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_shipreceivepoint d
where d.Dim_ShipReceivePointid <> 1;
	
INSERT
  INTO dim_shipreceivepoint(Dim_ShipReceivePointid,
          ShipReceivePointCode,
          Description,
          FactoryCalendarKey,
          RoundupPeriodDays,
          AddressKey,
          Country,
	  CountryName)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_shipreceivepoint') 
          + row_number() over(),
			 TVST_VSTEL,
	ifnull(TVSTT_VTEXT,'Not Set'),
	ifnull(TVST_FABKL,'Not Set'),
	ifnull(TVST_VTRZT,'Not Set'),
	ifnull(TVST_ADRNR,'Not Set'),
	ifnull(TVST_ALAND,'Not Set'),
	ifnull(t005t_landx, 'Not Set')
FROM TVST inner join TVSTT on TVST_VSTEL = TVSTT_VSTEL and TVSTT_SPRAS = 'E'
	  inner join T005T on TVST_ALAND = T005T_LAND1
WHERE not exists (select 1 from dim_shipreceivepoint a where a.ShipReceivePointCode = TVST_VSTEL);