
UPDATE    dim_bomcategory bc
       FROM
          DD07T t
        SET bc.Description = t.DD07T_DDTEXT
 WHERE bc.RowIsCurrent = 1
 AND  t.DD07T_DOMNAME = 'STLTY'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND bc.category = t.DD07T_DOMVALUE;
