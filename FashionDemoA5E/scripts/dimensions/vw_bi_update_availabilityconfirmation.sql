UPDATE    dim_availabilityconfirmation ac
       FROM
          DD07T dt
   SET ac.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE ac.AvailabilityConfirmationCode = dt.DD07T_DOMVALUE
 AND dt.DD07T_DOMNAME = 'MDPBV' AND dt.DD07T_DOMVALUE IS NOT NULL;