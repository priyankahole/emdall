UPDATE    dim_statusprofile p
       FROM
          TJ20T t
   SET p.StatusProfileName = t.TJ20T_TXT
 WHERE p.RowIsCurrent = 1
 AND p.StatusProfileCode = t.TJ20T_STSMA
;
