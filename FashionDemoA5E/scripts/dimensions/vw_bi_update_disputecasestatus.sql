
UPDATE dim_DisputeCaseStatus s
FROM
SCMGSTATPROFST t
SET s.Description = ifnull(t.SCMGSTATPROFST_STAT_ORDNO_DESCR, 'Not Set')
WHERE s.RowIsCurrent = 1
AND s.DisputeCaseStatus=t.SCMGSTATPROFST_STAT_ORDERNO;