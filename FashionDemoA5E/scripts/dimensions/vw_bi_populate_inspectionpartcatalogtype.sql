UPDATE    dim_inspectionpartcatalogtype
       FROM
          TQ15T  t
   SET INSPECTIONPARTCATALOGTYPENAME = t.TQ15T_KATALOGTXT,
			dw_update_date = current_timestamp
    WHERE   INSPECTIONPARTCATALOGTYPEKEY = t.TQ15T_SCHLAGWORT
 AND INSPECTIONPARTCATALOGTYPECODE = t.TQ15T_KATALOGART
 AND RowIsCurrent = 1;
 
INSERT INTO dim_inspectionpartcatalogtype(Dim_inspectionpartcatalogtypeid,RowIsCurrent,rowstartdate)
SELECT 1,1,current_timestamp
FROM (SELECT 1) D
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionpartcatalogtype
               WHERE Dim_inspectionpartcatalogtypeid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectionpartcatalogtype';
   
insert into number_fountain
select 	'dim_inspectionpartcatalogtype',
	ifnull(max(d.Dim_inspectionpartcatalogtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectionpartcatalogtype d
where d.Dim_inspectionpartcatalogtypeid <> 1; 

INSERT INTO dim_inspectionpartcatalogtype(Dim_inspectionpartcatalogtypeid,
                                          inspectionpartcatalogtypecode,
                                          inspectionpartcatalogtypename,
                                          inspectionpartcatalogtypekey,
                                          RowStartDate,
                                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectionpartcatalogtype')
          + row_number() over() ,
                        t.TQ15T_katalogart,
          t.TQ15T_katalogtxt,
                t.TQ15T_schlagwort,
          current_timestamp,
          1
     FROM TQ15T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionpartcatalogtype c
               WHERE c.inspectionpartcatalogtypename = t.TQ15T_KATALOGTXT
                     AND c.RowIsCurrent = 1);
