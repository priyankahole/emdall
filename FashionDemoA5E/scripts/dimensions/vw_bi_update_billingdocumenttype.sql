UPDATE    dim_billingdocumenttype bdt
       FROM
          tvfkt t
		SET bdt.Description = t.TVFKT_VTEXT
    WHERE t.TVFKT_FKART = bdt.Type
   AND bdt.RowIsCurrent = 1;