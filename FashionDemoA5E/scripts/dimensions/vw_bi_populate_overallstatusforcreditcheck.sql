UPDATE    dim_OverallStatusforCreditCheck oscc
       FROM
          DD07T t
   SET oscc.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			oscc.dw_update_date = current_timestamp
 WHERE oscc.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'CMGST'
          AND oscc.OverallStatusforCreditCheck = ifnull(t.DD07T_DOMVALUE,'Not Set')
;

INSERT INTO dim_OverallStatusforCreditCheck(dim_OverallStatusforCreditCheckId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_OverallStatusforCreditCheck
               WHERE dim_OverallStatusforCreditCheckId = 1);

delete from number_fountain m where m.table_name = 'dim_OverallStatusforCreditCheck';
   
insert into number_fountain
select 	'dim_OverallStatusforCreditCheck',
	ifnull(max(d.dim_overallstatusforcreditcheckID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_OverallStatusforCreditCheck d
where d.dim_overallstatusforcreditcheckID <> 1; 

INSERT INTO dim_OverallStatusforCreditCheck(dim_overallstatusforcreditcheckID,
                                Description,
                                OverallStatusforCreditCheck,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_OverallStatusforCreditCheck')
         + row_number() over(),
			  ifnull(DD07T_DDTEXT, 'Not Set'),
            ifnull(DD07T_DOMVALUE,'Not Set'),
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'CMGST'  
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_OverallStatusforCreditCheck
                    WHERE OverallStatusforCreditCheck = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'CMGST')
   ORDER BY 2 OFFSET 0;

