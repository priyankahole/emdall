UPDATE    dim_postingkey pk
       FROM
          TBSLT t
   SET pk.Name = t.TBSLT_LTEXT 
 WHERE pk.RowIsCurrent = 1
   AND t.TBSLT_SPRAS = 'E'
          AND pk.PostingKey = t.TBSLT_BSCHL
          AND pk.SpecialGLIndicator = ifnull(t.TBSLT_UMSKZ, 'Not Set')   
;
