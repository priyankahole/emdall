UPDATE dim_streamlineusers su FROM users u
   SET su.LoginId = u.Loginid,
       UserName = ifnull(User_name,'Not Set'),
       UserRole = User_role,
       BuyerFlag = Buyer_flag,
       PlannerFlag = Planner_Flag,
       AdminFlag = Admin_flag,
       su.Email = u.Email,
       VendorId = ifnull(Vendor_Id,'Not Set'),
			su.dw_update_date = current_timestamp
 WHERE u.Id = UserId AND su.RowIsCurrent = 1
;

INSERT INTO dim_streamlineusers(dim_streamlineusersId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_streamlineusers
               WHERE dim_streamlineusersId = 1);

delete from number_fountain m where m.table_name = 'dim_streamlineusers';

insert into number_fountain
select 	'dim_streamlineusers',
	ifnull(max(d.dim_streamlineusersid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_streamlineusers d
where d.dim_streamlineusersid <> 1;

INSERT INTO dim_streamlineusers(dim_streamlineusersid,
                                UserId,
                                LoginId,
                                UserName,
                                UserRole,
                                BuyerFlag,
                                PlannerFlag,
                                AdminFlag,
                                Email,
                                VendorId,
                                RowIsCurrent,
                                RowStartDate)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_streamlineusers') 
          + row_number() over(),
			 Id,
          LoginId,
          ifnull('User_Name','Not Set'),
          User_Role,
          Buyer_Flag,
          Planner_Flag,
          Admin_Flag,
          Email,
          ifnull(Vendor_Id,'Not Set'),
          1,
          current_timestamp
     FROM users
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_streamlineusers
               WHERE Id = UserId)
;
