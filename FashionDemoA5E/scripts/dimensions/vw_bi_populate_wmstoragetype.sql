/* 	Server: QA
	Process Name: WM Storage Type Transfer
	Interface No: 2
*/
INSERT INTO dim_wmstoragetype
(dim_wmstoragetypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmstoragetype where dim_wmstoragetypeid = 1);

UPDATE    dim_wmstoragetype st
       FROM
          T301T t
   SET st.StorageTypeDescription = ifnull(t.T301T_LTYPT,'Not Set'),
       st.MixedStorage = ifnull((SELECT dd1.DD07T_DDTEXT
                FROM DD07T dd1
               WHERE dd1.DD07T_DOMNAME = 'T331_MISCH'
                     AND dd1.DD07T_DOMVALUE = t.T331_MISCH), 'Not Set'),
       st.NegativeStockQty = ifnull((SELECT dd2.DD07T_DDTEXT
                FROM DD07T dd2
               WHERE dd2.DD07T_DOMNAME = 'XFELD'
                     AND dd2.DD07T_DOMVALUE = t.T331_NEGAT), 'No'),
       st.PutawayStrategy = ifnull((SELECT dd3.DD07T_DDTEXT
                FROM DD07T dd3
               WHERE dd3.DD07T_DOMNAME = 'T331_STEIN'
                     AND dd3.DD07T_DOMVALUE = t.T331_STEIN), 'Not Set'),
       st.StkRmvlStrategy = ifnull((SELECT dd4.DD07T_DDTEXT
                FROM DD07T dd4
               WHERE dd4.DD07T_DOMNAME = 'T331_STAUS'
                     AND dd4.DD07T_DOMVALUE = t.T331_STAUS), 'Not Set'),
			st.dw_update_date = current_timestamp   
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM;
 
delete from number_fountain m where m.table_name = 'dim_wmstoragetype';

insert into number_fountain				   
select 	'dim_wmstoragetype',
	ifnull(max(d.dim_wmstoragetypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmstoragetype d
where d.dim_wmstoragetypeid <> 1;

INSERT INTO dim_wmstoragetype(dim_wmstoragetypeid,
                            WarehouseNumber,
							              StorageType,
                            StorageTypeDescription,
                            MixedStorage,
                            NegativeStockQty,
                            PutawayStrategy,
                            StkRmvlStrategy,                            
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmstoragetype') 
          + row_number() over() ,
		      t.T301T_LGNUM,
          t.T301T_LGTYP,
          ifnull(t.T301T_LTYPT,'Not Set'),
          ifnull((SELECT dd1.DD07T_DDTEXT
                  FROM DD07T dd1
                 WHERE dd1.DD07T_DOMNAME = 'T331_MISCH'
                       AND dd1.DD07T_DOMVALUE = t.T331_MISCH), 'Not Set'),
          ifnull((SELECT dd2.DD07T_DDTEXT
                  FROM DD07T dd2
                 WHERE dd2.DD07T_DOMNAME = 'XFELD'
                       AND dd2.DD07T_DOMVALUE = t.T331_NEGAT), 'No'),
          ifnull((SELECT dd3.DD07T_DDTEXT
                  FROM DD07T dd3
                 WHERE dd3.DD07T_DOMNAME = 'T331_STEIN'
                       AND dd3.DD07T_DOMVALUE = t.T331_STEIN), 'Not Set'),
          ifnull((SELECT dd4.DD07T_DDTEXT
                  FROM DD07T dd4
                 WHERE dd4.DD07T_DOMNAME = 'T331_STAUS'
                       AND dd4.DD07T_DOMVALUE = t.T331_STAUS), 'Not Set'),
          current_timestamp,
          1
     FROM T301T t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_wmstoragetype s
                  WHERE s.StorageType = t.T301T_LGTYP
					    AND s.WarehouseNumber = t.T301T_LGNUM
                        AND s.RowIsCurrent = 1) ;
