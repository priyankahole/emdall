UPDATE    dim_statusprofile p
       FROM
          TJ20T t
   SET p.StatusProfileName = t.TJ20T_TXT,
			p.dw_update_date = current_timestamp
 WHERE p.RowIsCurrent = 1
 AND p.StatusProfileCode = t.TJ20T_STSMA
;

INSERT INTO dim_statusprofile(dim_statusprofileId, RowIsCurrent,statusprofilecode,statusprofilename,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_statusprofile
               WHERE dim_statusprofileId = 1);
delete from number_fountain m where m.table_name = 'dim_statusprofile';

insert into number_fountain
select 	'dim_statusprofile',
	ifnull(max(d.dim_StatusProfileid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_statusprofile d
where d.dim_StatusProfileid <> 1;

INSERT INTO dim_statusprofile(dim_StatusProfileid,
                              StatusProfileCode,
                              StatusProfileName,
                              RowStartDate,
                              RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_statusprofile') 
          + row_number() over(),
			 t.TJ20T_STSMA,
          t.TJ20T_TXT,
          current_timestamp,
          1
     FROM TJ20T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_statusprofile sp
               WHERE sp.StatusProfileCode = t.TJ20T_STSMA
                     AND sp.RowIsCurrent = 1)
;

