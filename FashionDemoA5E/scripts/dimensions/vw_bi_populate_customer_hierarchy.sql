/* ##################################################################################################################

   Script         : bi_populate_customer_hierarchy 
   Author         : Ashu
   Created On     : 17 Jan 2013


   Description    : Stored Proc bi_populate_customer_hierarchy from MySQL to Vectorwise syntax

   Change History
   Date            By        Version           Desc
   17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
#################################################################################################################### */

Drop table if exists pCustHierType_701;

create table pCustHierType_701 ( pCustomerHierarchyType ) As
Select CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.hierarchy.type'),
              'A'),varchar(5));

call vectorwise ( combine 'custhierarchy_tmp-custhierarchy_tmp');

INSERT INTO custhierarchy_tmp(dd_HierarchyType,
                              CustomerNumber,
                              HighCustomerNumber,
                              HigherCustomerNumber,
                              TopLevelCustomerNumber,
                              HighCustomerDesc,
                              HigherCustomerDesc,
                              TopLevelCustomerDesc,
			      EndDate,
                              StartDate)
   SELECT ifnull(KNVH_HITYP,'Not Set'),
          ifnull(KNVH_KUNNR,'Not Set'),
          ifnull(KNVH_HKUNNR,'Not Set'),
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          KNVH_DATBI,
          KNVH_DATAB
     FROM KNVH
 WHERE  current_date BETWEEN KNVH_DATAB AND KNVH_DATBI;

      
 UPDATE    custhierarchy_tmp ct FROM
          custhierarchy_tmp ctPar
   SET ct.HigherCustomerNumber = ctPar.HighCustomerNumber
 WHERE     ctPar.CustomerNumber = ct.HighCustomerNumber
          AND ctPar.dd_HierarchyType = ct.dd_HierarchyType AND  ct.HigherCustomerNumber = 'Not Set';


UPDATE    custhierarchy_tmp ct  FROM
          custhierarchy_tmp ctPar
   SET ct.TopLevelCustomerNumber = ctPar.HighCustomerNumber
 WHERE ctPar.CustomerNumber = ct.HigherCustomerNumber
          AND ctPar.dd_HierarchyType = ct.dd_HierarchyType 
          AND ct.TopLevelCustomerNumber = 'Not Set';
          
UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.HigherCustomerNumber,
       ct.HigherCustomerNumber = ct.HighCustomerNumber,
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber <> 'Not Set'
    and ct.HighCustomerNumber <> 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.HighCustomerNumber,
       ct.HigherCustomerNumber = 'Not Set',
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber = 'Not Set'
    and ct.HighCustomerNumber <> 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.CustomerNumber,
       ct.HigherCustomerNumber = 'Not Set',
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber = 'Not Set'
    and ct.HighCustomerNumber = 'Not Set';

UPDATE    custhierarchy_tmp ct FROM
          dim_customer c       
   SET ct.TopLevelCustomerDesc = c.Name
 WHERE ct.TopLevelCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

UPDATE    custhierarchy_tmp ct FROM
          dim_customer c       
   SET ct.HigherCustomerDesc = c.Name
 WHERE ct.HigherCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;
 
 UPDATE    custhierarchy_tmp ct FROM
          dim_customer c       
   SET ct.HighCustomerDesc = c.Name
 WHERE ct.HighCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

UPDATE    custhierarchy_tmp ct 
   SET ct.TopLevelCustomerDesc = 'Not Set'
 WHERE ct.TopLevelCustomerDesc IS NULL;

UPDATE    custhierarchy_tmp ct 
   SET ct.HigherCustomerDesc = 'Not Set'
 WHERE ct.HigherCustomerDesc IS NULL;
 
 UPDATE    custhierarchy_tmp ct 
   SET ct.HighCustomerDesc = 'Not Set'
 WHERE ct.HighCustomerDesc IS NULL;
 
 UPDATE dim_customer c FROM
         custhierarchy_tmp ct, pCustHierType_701 p
  SET c.HighCustomerNumber = ct.HighCustomerNumber,
      c.HierarchyType = ct.dd_HierarchyType,
      c.HigherCustomerNumber = ct.HigherCustomerNumber,
      c.TopLevelCustomerNumber = ct.TopLevelCustomerNumber,
      c.HighCustomerDesc = ct.HighCustomerDesc,
      c.HigherCustomerDesc = ct.HigherCustomerDesc,
      c.TopLevelCustomerDesc = ct.TopLevelCustomerDesc,
	  c.dw_update_date = current_timestamp
 WHERE ct.CustomerNumber = c.CustomerNumber
       AND ct.dd_HierarchyType = p.pCustomerHierarchyType;

call vectorwise ( combine 'custhierarchy_tmp-custhierarchy_tmp');