UPDATE    dim_stocktype s
       FROM
          DD07T t
   SET s.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			s.dw_update_date = current_timestamp
 WHERE s.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'INSMK'
          AND s.TypeCode = ifnull(t.DD07T_DOMVALUE, 'Not Set')
;

INSERT INTO dim_stocktype(dim_stocktypeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_stocktype
               WHERE dim_stocktypeId = 1);

delete from number_fountain m where m.table_name = 'dim_stocktype';

insert into number_fountain
select 	'dim_stocktype',
	ifnull(max(d.Dim_StockTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_stocktype d
where d.Dim_StockTypeid <> 1;

INSERT INTO dim_stocktype(Dim_StockTypeid,
                          Description,
                          TypeCode,
                          RowStartDate,
                          RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_stocktype') 
          + row_number() over(),
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'INSMK'
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_stocktype
                    WHERE TypeCode = ifnull(DD07T_DOMVALUE, 'Not Set')
                          AND DD07T_DOMNAME = 'INSMK')
   ORDER BY 2 OFFSET 0
;

