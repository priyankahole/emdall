/*Default row*/
insert into dim_afssize(Dim_AfsSizeId,Size,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason) values (1,'Not Set','2012-10-15 00:00:00',null,1,null) 
where not exists(select 1 from dim_afssize where dim_afssizeId = 1);

/*Update AFS Season Transfer */				 
UPDATE    dim_afsseason ase
       FROM
          J_3ASEANT t
   SET ase.Description = ifnull(t.J_3ASEANT_TEXT, 'Not Set')
 WHERE ase.RowIsCurrent = 1
 AND ase.SeasonIndicator = t.J_3ASEANT_J_3ASEAN
 AND ase.Collection = t.J_3ASEANT_AFS_COLLECTION
 AND ase.Theme = t.J_3ASEANT_AFS_THEME;
