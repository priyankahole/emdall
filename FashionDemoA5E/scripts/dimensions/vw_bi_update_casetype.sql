UPDATE    dim_casetype ds
       FROM
          SCMGCASETYPET dt
   SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set')
 WHERE ds.RowIsCurrent = 1
          AND ds.CaseType = dt.CASE_TYPE
          AND dt.CASE_TYPE IS NOT NULL
;