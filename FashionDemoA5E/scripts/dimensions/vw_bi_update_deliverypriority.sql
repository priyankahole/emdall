UPDATE dim_DeliveryPriority dp
  FROM tprit t
SET dp.Description = ifnull(t.BEZEI,'Not Set')
WHERE dp.DeliveryPriority = t.LPRIO
 AND dp.RowIsCurrent = 1;
