UPDATE    dim_unitofmeasure uom
       FROM
          t006a a 
   SET uom.Description = ifnull(a.T006A_MSEHT,'Not Set'),
			uom.dw_update_date = current_timestamp
 WHERE uom.RowIsCurrent = 1
 AND uom.UOM = a.T006A_MSEHI
;

INSERT INTO dim_unitofmeasure(dim_unitofmeasureId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_unitofmeasure
               WHERE dim_unitofmeasureId = 1);

delete from number_fountain m where m.table_name = 'dim_unitofmeasure';

insert into number_fountain
select 	'dim_unitofmeasure',
	ifnull(max(d.Dim_UnitOfMeasureid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_unitofmeasure d
where d.Dim_UnitOfMeasureid <> 1;	

INSERT INTO dim_unitofmeasure(Dim_UnitOfMeasureid,
                              Description,
                              UOM,
                              RowStartDate,
                              RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_unitofmeasure') 
          + row_number() over(),
			  ifnull(T006A_MSEHT,'Not Set'),
          ifnull(T006A_MSEHI,'Not Set'),
          current_timestamp,
          1
     FROM t006a
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_unitofmeasure
                       WHERE UOM = T006A_MSEHI)
;
