
UPDATE dim_MaterialPriceGroup5 a FROM TVM5T
   SET a.Description = ifnull(TVM5T_BEZEI, 'Not Set')
 WHERE a.MaterialPriceGroup5 = TVM5T_MVGR5 AND RowIsCurrent = 1;
