/* ##################################################################################################################
  
     Script         : bi_populate_so_headerstatus_dim 
     Author         : Ashu
     Created On     : 17 Jan 2013
  
  
     Description    : Stored Proc bi_populate_so_headerstatus_dim from MySQL to Vectorwise syntax
  
     Change History
     Date            By        Version           Desc
     17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
	 7 July 2014     George    1.1               Added GeneralIncompleteStatusItemCode,OverallCreditStatus,OverallCreditStatusCode,OverallBlkdStatusCode,
														OverallDlvrBlkStatusCode,TotalIncompleteStatusAllItemsCode,RejectionStatusCode
	 11 July 2013    George    1.2               Added TVBST source fields: GeneralIncompletionProcessingstatusOfHeader,OverallProcessingstatusofcreditchecks,
												 OverallBlockedProcessingStatus,DeliveryBlockProcessingStatus,TotalIncompletionProcessingStatusAllItems,OverallRejectionProcessingStatus
	  5 Feb 2015     Liviu Ionescu   1.3     Add combine for table dim_salesorderheaderstatus, VBUK
	 #################################################################################################################### */
insert into dim_salesorderheaderstatus
 (dim_salesorderheaderstatusid
           )
select 1
from (select 1) a
where not exists ( select 'x' from dim_salesorderheaderstatus where dim_salesorderheaderstatusid = 1);
	 
select current_time;

DELETE FROM VBUK
  WHERE NOT EXISTS
            (SELECT 1
               FROM VBAK_VBAP_VBEP
              WHERE VBUK_VBELN = VBAK_VBELN) 
        AND NOT EXISTS
               (SELECT 1
                  FROM fact_salesorder
                 WHERE dd_SalesDocNo = VBUK_VBELN);

 
DELETE FROM VBUK
  WHERE NOT EXISTS
               (SELECT 1
                  FROM VBAK_VBAP
                 WHERE VBUK_VBELN = VBAP_VBELN)
        AND NOT EXISTS
               (SELECT 1
                  FROM fact_salesorder
                 WHERE dd_SalesDocNo = VBUK_VBELN);

/* LI Start Changes 5 Feb 2015 */
call vectorwise(combine 'VBUK');
/* End Changes 5 Feb 2015 */

UPDATE dim_salesorderheaderstatus
from VBUK
SET RefStatusAllItems = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
    ConfirmationStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
    DeliveryStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
    OverallDlvrStatusOfItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
    BillingStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
    BillingStatusOfOrdRelBillDoc = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
    RejectionStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
    OverallProcessStatusItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
    TotalIncompleteStatusAllItems = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
    GeneralIncompleteStatusItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
    OverallPickingStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
    HeaderIncompletionStautusConcernDlvry = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set'),
    OverallBlkdStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
    OverallDlvrBlkStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	GeneralIncompleteStatusItemCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
	OverallCreditStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	OverallCreditStatusCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	OverallBlkdStatusCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	OverallDlvrBlkStatusCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	TotalIncompleteStatusAllItemsCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
	RejectionStatusCode = ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set')
WHERE dim_salesorderheaderstatus.SalesDocumentNumber = VBUK_VBELN;

delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 
 
INSERT INTO dim_salesorderheaderstatus(
	    dim_salesorderheaderstatusid,
            SalesDocumentNumber,
            RefStatusAllItems,
            ConfirmationStatus,
            DeliveryStatus,
            OverallDlvrStatusOfItem,
            BillingStatus,
            BillingStatusOfOrdRelBillDoc,
            RejectionStatus,
            OverallProcessStatusItem,
            TotalIncompleteStatusAllItems,
            GeneralIncompleteStatusItem,
            OverallPickingStatus,
	    HeaderIncompletionStautusConcernDlvry,
	    OverallBlkdStatus,
	    OverallDlvrBlkStatus,
		GeneralIncompleteStatusItemCode,
		OverallCreditStatus,
		OverallCreditStatusCode,
		OverallBlkdStatusCode,
		OverallDlvrBlkStatusCode,
		TotalIncompleteStatusAllItemsCode,
		RejectionStatusCode		
		)
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set')
FROM VBUK inner join VBAK_VBAP_VBEP on VBAK_VBELN = VBUK_VBELN
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = VBUK_VBELN)) dtbl1;
   
delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 

INSERT
INTO dim_salesorderheaderstatus(
	dim_salesorderheaderstatusid,
   SalesDocumentNumber,
   RefStatusAllItems,
   ConfirmationStatus,
   DeliveryStatus,
   OverallDlvrStatusOfItem,
   BillingStatus,
   BillingStatusOfOrdRelBillDoc,
   RejectionStatus,
   OverallProcessStatusItem,
   TotalIncompleteStatusAllItems,
   GeneralIncompleteStatusItem,
   OverallPickingStatus,
   HeaderIncompletionStautusConcernDlvry,
   OverallBlkdStatus,
   OverallDlvrBlkStatus,
   GeneralIncompleteStatusItemCode,
   OverallCreditStatus,
   OverallCreditStatusCode,
   OverallBlkdStatusCode,
   OverallDlvrBlkStatusCode,
   TotalIncompleteStatusAllItemsCode,
   RejectionStatusCode		
   )
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_CMGST),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_SPSTG),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LSSTK),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
	ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set')
FROM VBUK inner join VBAK_VBAP on VBAP_VBELN = VBUK_VBELN
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = VBUK_VBELN)) dtbl1;

/* LI Start Changes 5 Feb 2015 */
call vectorwise(combine 'dim_salesorderheaderstatus');
/* End Changes 5 Feb 2015 */

select 'Process Complete : ';
