/* 	Server: QA
	Process Name: RF Queue Transfer
	Interface No: 2
*/

INSERT INTO dim_rfqueue
(dim_rfqueueid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_rfqueue where dim_rfqueueid = 1);

UPDATE    dim_rfqueue rfq
       FROM
          T346T t
   SET  rfq.QueueDescription = t.T346T_QUTXT,
			rfq.dw_update_date = current_timestamp
 WHERE rfq.RowIsCurrent = 1
     AND rfq.Queue = t.T346T_QUEUE
	   AND rfq.WarehouseNumber = t.T346T_LGNUM;
 
delete from number_fountain m where m.table_name = 'dim_rfqueue';

insert into number_fountain				   
select 	'dim_rfqueue',
	ifnull(max(d.dim_rfqueueid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_rfqueue d
where d.dim_rfqueueid <> 1;

INSERT INTO dim_rfqueue(dim_rfqueueid,
							WarehouseNumber,
							Queue,
                            QueueDescription,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_rfqueue') + row_number() over(),
		  t.T346T_LGNUM,
          t.T346T_QUEUE,
		  t.T346T_QUTXT,
          current_timestamp,
          1
     FROM T346T t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_rfqueue s
                  WHERE    s.Queue = t.T346T_QUEUE
					    AND s.WarehouseNumber = t.T346T_LGNUM
                        AND s.RowIsCurrent = 1);
