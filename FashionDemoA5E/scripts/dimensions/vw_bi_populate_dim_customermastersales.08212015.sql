INSERT INTO dim_customermastersales(dim_customermastersalesid,RowIsCurrent)
   SELECT 1,1
     FROM ( SELECT 1 )  AS t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customermastersales
               WHERE dim_customermastersalesid = 1);

/*Update Process */
UPDATE dim_customermastersales FROM KNVV
   SET ABCClassification = ifnull(KNVV_KLABC, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND ABCClassification <> ifnull(KNVV_KLABC, 'Not Set');

UPDATE dim_customermastersales FROM KNVV
   SET AccountAssignmentGroup = ifnull(KNVV_KTGRD, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND AccountAssignmentGroup <> ifnull(KNVV_KTGRD, 'Not Set');

UPDATE dim_customermastersales FROM KNVV, TVKTT
   SET AccountAssignmentGroupDesciption = ifnull(TVKTT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND tvktt.TVKTT_KTGRD = KNVV_KTGRD
       AND AccountAssignmentGroupDesciption <> ifnull(TVKTT_VTEXT, 'Not Set');

UPDATE dim_customermastersales FROM KNVV
   SET SalesOffice = ifnull(knvv_VKBUR, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND SalesOffice <> ifnull(knvv_VKBUR, 'Not Set');

UPDATE dim_customermastersales FROM KNVV, TVKBT
   SET SalesOfficeDescription = ifnull(TVKBT_BEZEI, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_VKBUR = TVKBT_VKBUR
       AND SalesOfficeDescription <> ifnull(TVKBT_BEZEI, 'Not Set');

UPDATE dim_customermastersales FROM KNVV
   SET SalesDistrict = ifnull(KNVV_BZIRK, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND SalesDistrict <> ifnull(KNVV_BZIRK, 'Not Set');

UPDATE dim_customermastersales FROM KNVV, T171T
   SET SalesDistrictDescription = ifnull(T171T_BZTXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_BZIRK = T171T_BZIRK
       AND SalesDistrictDescription <> ifnull(T171T_BZTXT, 'Not Set');

UPDATE dim_customermastersales FROM KNVV
   SET CustomerGroup = ifnull(KNVV_KDGRP, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND CustomerGroup <> ifnull(KNVV_KDGRP, 'Not Set');

UPDATE dim_customermastersales FROM KNVV, T151T
   SET CustomerGroupDescription = ifnull(T151T_KTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND knvv.knvv_KDGRP = T151T_KDGRP
       AND CustomerGroupDescription <> ifnull(T151T_KTEXT, 'Not Set');

UPDATE dim_customermastersales FROM KNVV
   SET PaymentTerms = ifnull(KNVV_ZTERM, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND PaymentTerms <> ifnull(KNVV_ZTERM, 'Not Set');

UPDATE dim_customermastersales FROM KNVV, KNA1 k
   SET CustomerName = ifnull(k.KNA1_NAME1, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNA1_KUNNR = knvv.knvv_KUNNR
       AND CustomerName <> ifnull(k.KNA1_NAME1, 'Not Set');

UPDATE dim_customermastersales FROM KNVV, TVTWT
   SET DistributionChannelDescription = ifnull(TVTWT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVTWT_VTWEG = knvv.KNVV_VTWEG
       AND DistributionChannelDescription <> ifnull(TVTWT_VTEXT, 'Not Set');

UPDATE dim_customermastersales FROM KNVV, TSPAT
   SET Division = ifnull(TSPAT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TSPAT_SPART = knvv.KNVV_SPART
       AND Division <> ifnull(TSPAT_VTEXT, 'Not Set');

UPDATE dim_customermastersales FROM KNVV, TVKOT
   SET SalesOrgDescription = ifnull(TVKOT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVKOT_VKORG = knvv.KNVV_VKORG
       AND SalesOrgDescription <> ifnull(TVKOT_VTEXT, 'Not Set');


UPDATE dim_customermastersales FROM KNVV
   SET Customergroup3 = ifnull(KNVV_KVGR3, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND ifnull(Customergroup3,'X') <> ifnull(KNVV_KVGR3, 'Not Set')
       AND Customergroup3 <> ifnull(KNVV_KVGR3, 'Not Set');
       
 UPDATE dim_customermastersales FROM KNVV
 SET deletionFlag = ifnull(KNVV_LOEVM, 'Not Set'),
 			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND deletionFlag <> ifnull(KNVV_LOEVM, 'Not Set');  
 
 
  /* Start New fields added as a part of Columbia Report 856 */ 
UPDATE dim_customermastersales FROM KNVV
   SET custpricingProcedure = ifnull(KNVV_KALKS, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND custpricingProcedure <> ifnull(KNVV_KALKS, 'Not Set'); 
	   
UPDATE dim_customermastersales FROM KNVV
   SET deliveryPriority = ifnull(KNVV_LPRIO,1.0000),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND deliveryPriority <> ifnull(KNVV_LPRIO,1.0000);
       
UPDATE dim_customermastersales FROM KNVV
   SET incoterm1 = ifnull(KNVV_INCO1, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND incoterm1 <> ifnull(KNVV_INCO1, 'Not Set');	   

UPDATE dim_customermastersales FROM KNVV
   SET incoTerm2 = ifnull(KNVV_INCO2, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND incoTerm2 <> ifnull(KNVV_INCO2, 'Not Set');   
	   
UPDATE dim_customermastersales FROM KNVV
   SET maxPartialDelivery = ifnull(KNVV_ANTLF,1.0000),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND maxPartialDelivery <> ifnull(KNVV_ANTLF,1.0000);
	   
UPDATE dim_customermastersales FROM KNVV
   SET storeLocator = ifnull(KNVV_AGREL, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND storeLocator <> ifnull(KNVV_AGREL, 'Not Set');


       	
UPDATE dim_customermastersales FROM KNVV
   SET customergroup5 = ifnull(KNVV_KVGR5, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup5 <> ifnull(KNVV_KVGR5, 'Not Set');	
	   
UPDATE dim_customermastersales FROM KNVV
   SET customergroup2 = ifnull(KNVV_KVGR2, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup2 <> ifnull(KNVV_KVGR2, 'Not Set');	
	   
UPDATE dim_customermastersales FROM KNVV
   SET customergroup1 = ifnull(KNVV_KVGR1, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup1 <> ifnull(KNVV_KVGR1, 'Not Set');	
	   
 /* AFS Specific fields for Columbia 856 report */
UPDATE dim_customermastersales FROM KNVV
   SET assortmentPreference = ifnull(KNVV_J_3AKVGR6, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND assortmentPreference <> ifnull(KNVV_J_3AKVGR6, 'Not Set');		   
	
	   
UPDATE dim_customermastersales FROM KNVV
   SET groupStrategy = ifnull(KNVV_J_3AGRSGY,'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND groupStrategy <> ifnull(KNVV_J_3AGRSGY,'Not Set');	


UPDATE dim_customermastersales FROM KNVV
   SET groupStrategyDescription = ifnull((SELECT t.J_3AGRSG_T_BEZEI FROM J_3AGRSG_T t 
   					WHERE knvv.KNVV_J_3AGRSGY = t.J_3AGRSG_T_J_3AGRSGY 
					AND J_3AGRSG_T_SPRAS = 'E'),'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND groupStrategyDescription <> ifnull((SELECT t.J_3AGRSG_T_BEZEI FROM J_3AGRSG_T t 
   					WHERE knvv.KNVV_J_3AGRSGY = t.J_3AGRSG_T_J_3AGRSGY 
					AND J_3AGRSG_T_SPRAS = 'E'),'Not Set');	

UPDATE dim_customermastersales FROM KNVV
   SET relstrategy = ifnull(KNVV_J_3ARESGY,'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND relstrategy <> ifnull(KNVV_J_3ARESGY,'Not Set')
       AND relstrategy <> ifnull(KNVV_J_3ARESGY,'Not Set');
	   
UPDATE dim_customermastersales FROM KNVV
   SET relStrategyDescription = ifnull((SELECT t.J_3ARESG_T_BEZEI FROM J_3ARESG_T t
					WHERE knvv.KNVV_J_3ARESGY = t.J_3ARESG_T_J_3ARESGY
					AND t.J_3ARESG_T_SPRAS = 'E'),'Not Set'),
			dw_update_date = current_timestamp	
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND relStrategyDescription <> ifnull((SELECT t.J_3ARESG_T_BEZEI FROM J_3ARESG_T t
					WHERE knvv.KNVV_J_3ARESGY = t.J_3ARESG_T_J_3ARESGY
					AND t.J_3ARESG_T_SPRAS = 'E'),'Not Set');		   
	   
UPDATE dim_customermastersales FROM KNVV
   SET packSlip = ifnull(KNVV_J_3AKVGR8,'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND packSlip <> ifnull(KNVV_J_3AKVGR8,'Not Set');										
	   
UPDATE dim_customermastersales FROM KNVV
   SET freightPayer = ifnull(KNVV_J_3AKVGR7,'Not Set') ,
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND freightPayer <> ifnull(KNVV_J_3AKVGR7,'Not Set');
       
 /* End of AFS Fields for Columbia 856 Report*/
/* End New fields added as a part of Columbia Report 856 Nov 2014*/
			   
/* Insert Process */			  
/* Modifed on 12222014 to fix issue while processing */ 
drop table if exists tmp_dim_customermastersales_knvv_ins;
create table tmp_dim_customermastersales_knvv_ins as
select knvv_KUNNR,KNVV_VTWEG,KNVV_VKORG,KNVV_SPART
FROM    KNVV;

drop table if exists tmp_dim_customermastersales_knvv_del;
create table tmp_dim_customermastersales_knvv_del as
select * from tmp_dim_customermastersales_knvv_ins where 1=2;
/* end of modification on 12222014 */ 

insert into tmp_dim_customermastersales_knvv_del
select CustomerNumber,DistributionChannel,SalesOrg,DivisionCode
from dim_customermastersales;

call vectorwise (combine 'tmp_dim_customermastersales_knvv_ins-tmp_dim_customermastersales_knvv_del');

delete from number_fountain m where m.table_name = 'dim_customermastersales';

insert into number_fountain
select 	'dim_customermastersales',
	ifnull(max(d.dim_customermastersalesid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customermastersales d
where d.dim_customermastersalesid <> 1;

INSERT INTO dim_customermastersales(Dim_CustomerMasterSalesId,ABCClassification,
                                    AccountAssignmentGroup,
                                    AccountAssignmentGroupDesciption,
                                    CustomerName,
                                    CustomerNumber,
                                    DistributionChannel,
                                    DistributionChannelDescription,
                                    Division,
                                    DivisionCode,
                                    RowIsCurrent,
                                    RowStartDate,
                                    SalesOrg,
                                    SalesOrgDescription,
                                    SalesOffice,
                                    SalesOfficeDescription,
                                    SalesDistrict,
                                    SalesDistrictDescription,
                                    CustomerGroup,
                                    CustomerGroupDescription,
                                    PaymentTerms,
                                    PaymentTermsDescription,
				    CustomerGroup3,

						SalesGroup
						/* Start New fields added as a part of Columbia 856 report Nov 2014*/
						,custpricingProcedure
						,deliveryPriority
						,incoterm1
						,incoTerm2
						,maxPartialDelivery
						,storeLocator

				    	,customergroup5
					,customergroup2
					,customergroup1
						,assortmentPreference  /*AFS Specific Fields*/
						,relStrategy
						,relStrategyDescription
						,groupStrategy
						,groupStrategyDescription
						,packSlip
						,freightPayer     
						/*,closeoutallowed /* Columbia Custom Fields. Should be custom script
						,contractallowed
						,nondcallowed
						,replenishindicator	*/
						/* End New fields added as a part of Columbia 856 report*/
					, deletionFlag )
    SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customermastersales')
    		+ row_number() over(),
			a.* 
	FROM (SELECT distinct ifnull(KNVV_KLABC,'Not Set'),
          ifnull(KNVV_KTGRD,'Not Set'),
          ifnull((SELECT TVKTT_VTEXT
                    FROM TVKTT
                   WHERE tvktt.TVKTT_KTGRD = knvv.KNVV_KTGRD),
                 'Not Set'),
          ifnull((SELECT k1.KNA1_NAME1
                    FROM KNA1 k1
                   WHERE k1.KNA1_KUNNR = knvv.KNVV_KUNNR),
                 'Not Set'),
          ifnull(knvv.knvv_KUNNR,'Not Set'),
          ifnull(knvv.knvv_VTWEG,'Not Set'),
          ifnull((SELECT t.TVTWT_VTEXT
                    FROM TVTWT t
                   WHERE t.TVTWT_VTWEG = knvv.KNVV_VTWEG),
                 'Not Set'),
          ifnull((SELECT t.TSPAT_VTEXT
                    FROM TSPAT t
                   WHERE t.TSPAT_SPART = knvv.KNVV_SPART),
                 'Not Set'),
          ifnull(knvv.knvv_SPART,'Not Set'),
          1,
          current_Date,
          ifnull(knvv.knvv_VKORG,'Not Set'),
          ifnull((SELECT t.TVKOT_VTEXT
                    FROM TVKOT t
                   WHERE t.TVKOT_VKORG = knvv.KNVV_VKORG),
                 'Not Set'),
          ifnull(knvv_VKBUR,'Not Set'),
          ifnull((SELECT t.TVKBT_BEZEI
                    FROM TVKBT t
                   WHERE t.TVKBT_VKBUR = knvv.KNVV_VKBUR),
                 'Not Set'),
          ifnull(KNVV_BZIRK,'Not Set'),
          ifnull((SELECT t.T171T_BZTXT
                    FROM T171T t
                   WHERE t.T171T_BZIRK = knvv.KNVV_BZIRK),
                 'Not Set'),
          ifnull(knvv_KDGRP,'Not Set'),
          ifnull((SELECT t.T151T_KTEXT
                    FROM T151T t
                   WHERE t.T151T_KDGRP = knvv.KNVV_KDGRP),
                 'Not Set'),
          ifnull(knvv_zterm,'Not Set'),
          'Not Set',
	  ifnull(KNVV_KVGR3,'Not Set'),
	    ifnull(KNVV_VKGRP,'Not Set')	 
			/* Start New fields added as a part of Columbia 856 report Nov 2014*/
			,ifnull(KNVV_KALKS,'Not Set') custpricingProcedure
			,ifnull(KNVV_LPRIO,0.0000) deliveryPriority
			,ifnull(KNVV_INCO1, 'Not Set') incoterm1
			,ifnull(KNVV_INCO2,'Not Set') incoTerm2
			,ifnull(KNVV_ANTLF,0.0000) maxPartialDelivery
			,ifnull(KNVV_AGREL,'Not Set') storeLocator

			,ifnull(KNVV_KVGR5,'Not Set') customergroup5
			,ifnull(KNVV_KVGR2,'Not Set') customergroup2
			,ifnull(KNVV_KVGR1,'Not Set') customergroup1
			/*ifnull(KNVV_J_3AKVGR6,'Not Set') assortmentPreference /*AFS Specific Fields. Moved to Custom Post Process 
			,ifnull(KNVV_J_3ARESGY,'Not Set') relStrategy
			,ifnull((SELECT t.J_3ARESG_T_BEZEI
						FROM J_3ARESG_T t
						WHERE knvv.KNVV_J_3ARESGY = t.J_3ARESG_T_J_3ARESGY
						AND t.J_3ARESG_T_SPRAS = 'E'),
					'Not Set')	relStrategyDescription
			,ifnull(KNVV_J_3AGRSGY,'Not Set') groupStrategy
			,ifnull((SELECT t.J_3AGRSG_T_BEZEI 
						FROM J_3AGRSG_T t 
						WHERE knvv.KNVV_J_3AGRSGY = t.J_3AGRSG_T_J_3AGRSGY 
						AND J_3AGRSG_T_SPRAS = 'E'),
					'Not Set')  groupStrategyDescription
			,ifnull(KNVV_J_3AKVGR8,'Not Set') packSlip
			,ifnull(KNVV_J_3AKVGR7,'Not Set') freightPayer*/
			/*,ifnull(KNVV_ZZCLOSEOUT,'Not Set')  /*Columbia Custom Fields. Must be in a custom script
			,ifnull(KNVV_ZZCONTRACT,'Not Set')
			,ifnull(KNVV_ZZNONDC,'Not Set')
			,ifnull(KNVV_ZZREPLEN,'Not Set')*/
			/* End New fields added as a part of Columbia 856 report Nov 2014*/
	  ,  ifnull(KNVV_LOEVM,'Not Set')
	  
     FROM KNVV knvv, tmp_dim_customermastersales_knvv_ins c
    WHERE knvv.knvv_KUNNR = c.knvv_KUNNR
    AND knvv.KNVV_VTWEG = c.KNVV_VTWEG
    AND knvv.KNVV_VKORG = c.KNVV_VKORG
    AND knvv.KNVV_SPART = c.KNVV_SPART ) a;

delete from number_fountain m where m.table_name = 'dim_customermastersales';	

call vectorwise (combine 'tmp_dim_customermastersales_knvv_ins-tmp_dim_customermastersales_knvv_ins');
call vectorwise (combine 'tmp_dim_customermastersales_knvv_del-tmp_dim_customermastersales_knvv_del');

