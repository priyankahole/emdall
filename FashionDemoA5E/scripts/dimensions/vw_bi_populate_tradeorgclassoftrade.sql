
INSERT INTO dim_tradeorgclassoftrade(dim_tradeorgclassoftradeid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_tradeorgclassoftrade
               WHERE dim_tradeorgclassoftradeid = 1);
			   
UPDATE dim_tradeorgclassoftrade from A702
   SET processingstatus = A702_KBSTAT,
   	customernumber =A702_HIENR,
    priority = A702_IRM_IPAPRIO,
    membergroup1 = A702_MBGRP1,
    membergroup2 = A702_MBGRP2,
    membergroup3 = A702_MBGRP3,
    membergroup4 = A702_MBGRP4,
    membergroup5 = A702_MBGRP5,
			dw_update_date = current_timestamp
   		WHERE application = A702_KAPPL
                         AND conditiontype = A702_KSCHL
                         AND customer = A702_HIENR
                         AND condreccno = A702_KUNNR
                         AND releasestatus = A702_KFRST
                         AND validityenddate = A702_DATBI
                         AND validitystartdate = A702_DATAB
                         AND RowIsCurrent = 1
;

delete from number_fountain m where m.table_name = 'dim_tradeorgclassoftrade';

insert into number_fountain
select 	'dim_tradeorgclassoftrade',
	ifnull(max(d.dim_tradeorgclassoftradeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_tradeorgclassoftrade d
where d.dim_tradeorgclassoftradeid <> 1;	
			   
INSERT INTO dim_tradeorgclassoftrade(dim_tradeorgclassoftradeid,
				application,
                                  conditiontype,
                                  releasestatus,
                                  validityenddate,
                                  validitystartdate,
                                  processingstatus,
                                  condreccno,
                                  customer,
                                  customernumber,
                                  priority,
                                  membergroup1,
                                  membergroup2,
                                  membergroup3,
                                  membergroup4,
                                  membergroup5,
                                  RowStartDate,
                                  RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_tradeorgclassoftrade') 
          + row_number() over(),
		  A702_KAPPL,
		  A702_KSCHL,
		  A702_KFRST,
		  A702_DATBI,
		  A702_DATAB,
		  A702_KBSTAT,
		  A702_KNUMH,
		  A702_HIENR,
		  A702_KUNNR,
		  A702_IRM_IPAPRIO,
		  A702_MBGRP1,
		  A702_MBGRP2,
		  A702_MBGRP3,
		  A702_MBGRP4,
		  A702_MBGRP5,
          current_timestamp,
          1
     FROM A702
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM dim_tradeorgclassoftrade
                   WHERE     application = A702_KAPPL
                         AND conditiontype = A702_KSCHL
                         AND customer = A702_HIENR
                         AND condreccno = A702_KUNNR
                         AND releasestatus = A702_KFRST
                         AND validityenddate = A702_DATBI
                         AND validitystartdate = A702_DATAB
			 AND RowIsCurrent = 1)
;
