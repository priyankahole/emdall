UPDATE    dim_term dt
       FROM
          t052u u
   SET Name = ifnull(T052U_TEXT1, 'Not Set')
 WHERE dt.RowIsCurrent = 1
 AND u.T052U_ZTERM = dt.Termcode
;