

insert into dim_afssize(Dim_AfsSizeId,Size,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason) 
SELECT 1,'Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_afssize where dim_afssizeId = 1);

/*This Script will be called from Fact Sales Order */
/*Populate AFS Size Dimension Sales*/
delete from number_fountain m where m.table_name = 'dim_afssize';

insert into number_fountain
select 	'dim_afssize',
	ifnull(max(d.dim_afssizeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_afssize d
where d.dim_afssizeid <> 1;

INSERT INTO dim_AfsSize(dim_AfsSizeId,Size, RowStartDate, RowIsCurrent)
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_afssize') + row_number() over ()),
a.*
FROM
(SELECT DISTINCT vkp.VBEP_J_3ASIZE as AfsSize,
current_date as Date1,
1 as CurrentRow   
FROM VBAK_VBAP_VBEP vkp
    WHERE vkp.VBEP_J_3ASIZE IS NOT NULL
          AND NOT EXISTS
                 (SELECT 1
                    FROM dim_AfsSize
                   WHERE Size = vkp.VBEP_J_3ASIZE)
    ) a
order by a.AfsSize offset 0;


/*Populate AFS Size Dimension Purchasing*/
delete from number_fountain m where m.table_name = 'dim_afssize';

insert into number_fountain
select 	'dim_afssize',
	ifnull(max(d.dim_afssizeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_afssize d
where d.dim_afssizeid <> 1;

INSERT INTO dim_AfsSize(dim_AfsSizeid,
                        Size,
                        RowStartDate,
                        RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_afssize')
          + row_number()
             over(),tty.*
From(
   SELECT DISTINCT ekp.EKET_J_3ASIZE, current_timestamp, 1
     FROM ekko_ekpo_eket ekp
    WHERE ekp.EKET_J_3ASIZE IS NOT NULL
          AND NOT EXISTS
                 (SELECT 1
                    FROM dim_AfsSize
                   WHERE Size = ekp.EKET_J_3ASIZE)) tty;

CALL vectorwise(combine 'dim_AfsSize');

delete from number_fountain m where m.table_name = 'dim_afssize';
