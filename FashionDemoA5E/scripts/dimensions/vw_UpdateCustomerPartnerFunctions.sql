/*Insert Not Set record */
insert into dim_customerpartnerfunctions 
(dim_customerpartnerfunctionsid, rowchangereason, rowenddate, rowiscurrent, rowstartdate, 
customernumber1, customername1, salesorgcode, salesorgdesc, distributionchannelcode, 
distributionchannelname, divisioncode, divisionname, partnerfunction, partnerfunctiondesc, 
partnercounter, customernumberbusinesspartner, customernamebusinesspartner, vendoraccnumber, 
vendorname, personalnumber, contactpersonnumber, customerdescriptionofpartner, defaultpartner
) 
SELECT 1,null,null,1,current_timestamp,
'Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set',
'Not Set',0,0,'Not Set','Not Set'
FROM (SELECT 1) a
where not exists(select 1 from dim_customerpartnerfunctions where dim_customerpartnerfunctionsid = 1);


/*Update Query from Integration Process Update Step */
UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp
   SET    CustomerName1 = ifnull((SELECT KNA1_NAME1
                    FROM KNA1 k1
                   WHERE k1.KNA1_KUNNR = kp.KNVP_KUNNR),
                 'Not Set'),
          DistributionChannelName = ifnull((SELECT TVTWT_VTEXT
                    FROM TVTWT
                   WHERE TVTWT_VTWEG = kp.KNVP_VTWEG),
                 'Not Set'),
          DivisionName = ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = kp.KNVP_SPART),
                 'Not Set'),
          PartnerFunctionDesc = ifnull((SELECT TPART_VTEXT
                    FROM TPART
                   WHERE TPART_PARVW = kp.KNVP_PARVW),
                 'Not Set'),
          SalesOrgDesc = ifnull((SELECT TVKOT_VTEXT
                    FROM TVKOT
                   WHERE TVKOT_VKORG = kp.KNVP_VKORG),
                 'Not Set'),
          CustomerNumberBusinessPartner = ifnull(kp.KNVP_KUNN2,'Not Set'),
          CustomerNameBusinessPartner = ifnull((SELECT KNA1_NAME1
                    FROM KNA1 k1
                   WHERE k1.KNA1_KUNNR = kp.KNVP_KUNN2),
                 'Not Set'),
          VendorAccNumber= ifnull(kp.KNVP_LIFNR,'Not Set') ,
          VendorName = ifnull((SELECT NAME1
                    FROM LFA1 l1
                   WHERE l1.LIFNR = kp.KNVP_LIFNR),
                 'Not Set'),
          PersonalNumber = ifnull(kp.KNVP_PERNR, 0),
          ContactPersonNumber = ifnull(kp.KNVP_PARNR, 0),
          CustomerDescriptionofPartner = ifnull(kp.KNVP_KNREF, 'Not Set'),
          DefaultPartner = ifnull(kp.KNVP_DEFPA, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
                            AND cpf.SalesOrgCode = kp.KNVP_VKORG
                            AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
                            AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
                            AND cpf.PartnerFunction = kp.KNVP_PARVW
                            AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
                            AND cpf.RowIsCurrent = 1;
							
/* Start of Populate Script */
UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp, LFA1 l1
   SET   VendorName = ifnull(NAME1, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode <> ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND l1.LIFNR = kp.KNVP_LIFNR
        AND VendorName <> ifnull(NAME1, 'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp, KNA1 k1
   SET   CustomerNameBusinessPartner = ifnull(KNA1_NAME1, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode <> ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND k1.KNA1_KUNNR = kp.KNVP_KUNN2
        AND CustomerNameBusinessPartner <> ifnull(KNA1_NAME1, 'Not Set')
        AND cpf.RowIsCurrent = 1;        
 
UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp, TVKOT
   SET   SalesOrgDesc = ifnull(TVKOT_VTEXT, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode <> ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND TVKOT_VKORG = kp.KNVP_VKORG
        AND SalesOrgDesc <> ifnull(TVKOT_VTEXT, 'Not Set')
        AND cpf.RowIsCurrent = 1;
 
 UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp,TPART
   SET   PartnerFunctionDesc = ifnull( TPART_VTEXT, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode <> ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND TPART_PARVW = kp.KNVP_PARVW
        AND PartnerFunctionDesc <> ifnull( TPART_VTEXT, 'Not Set')
        AND cpf.RowIsCurrent = 1;
 
 UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp, TSPAT
   SET   DivisionName = ifnull(TSPAT_VTEXT, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND TSPAT_SPART = kp.KNVP_SPART
        AND DivisionName <> ifnull(TSPAT_VTEXT, 'Not Set')
        AND cpf.RowIsCurrent = 1;
        
        UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp,KNA1 k1
   SET    CustomerName1 = ifnull(KNA1_NAME1, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND k1.KNA1_KUNNR = kp.KNVP_KUNNR
        AND CustomerName1 <> ifnull( KNA1_NAME1,'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp, TVTWT
   SET   DistributionChannelName = ifnull(TVTWT_VTEXT, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND TVTWT_VTWEG = kp.KNVP_VTWEG
        AND DistributionChannelName <> ifnull(TVTWT_VTEXT, 'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp
   SET    VendorAccNumber= ifnull(kp.KNVP_LIFNR,'Not Set'),
		  dw_update_date = current_timestamp
 WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
			    AND VendorAccNumber <> ifnull(kp.KNVP_LIFNR,'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp
   SET    
     CustomerNumberBusinessPartner = ifnull(kp.KNVP_KUNN2,'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
			    AND CustomerNumberBusinessPartner <> ifnull(kp.KNVP_KUNN2,'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp
   SET PersonalNumber = ifnull(kp.KNVP_PERNR, 0),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND cpf.RowIsCurrent = 1
  AND PersonalNumber <> ifnull(kp.KNVP_PERNR, 0);

UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp
   SET ContactPersonNumber = ifnull(kp.KNVP_PARNR, 0),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND cpf.RowIsCurrent = 1
  AND ContactPersonNumber <> ifnull(kp.KNVP_PARNR, 0);

UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp
   SET CustomerDescriptionofPartner = ifnull(kp.KNVP_KNREF, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
       AND CustomerDescriptionofPartner <> ifnull(kp.KNVP_KNREF, 'Not Set')
 AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf FROM KNVP kp
   SET DefaultPartner = ifnull(kp.KNVP_DEFPA, 'Not Set'),
		  dw_update_date = current_timestamp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set')
        AND cpf.RowIsCurrent = 1
     AND DefaultPartner <> ifnull(kp.KNVP_DEFPA, 'Not Set');
     

delete from NUMBER_FOUNTAIN where table_name = 'dim_customerpartnerfunctions';
     
INSERT INTO NUMBER_FOUNTAIN
select 	'dim_customerpartnerfunctions',
	ifnull(max(d.dim_customerpartnerfunctionsid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customerpartnerfunctions d
where d.dim_customerpartnerfunctionsid <> 1;

     INSERT INTO dim_customerpartnerfunctions(dim_customerpartnerfunctionsid,
                                         CustomerNumber1,
                                         CustomerName1,
                                         DistributionChannelCode,
                                         DistributionChannelName,
                                         DivisionCode,
                                         DivisionName,
                                         PartnerFunction,
                                         PartnerFunctionDesc,
                                         SalesOrgCode,
                                         SalesOrgDesc,
                                         PartnerCounter,
                                         CustomerNumberBusinessPartner,
                                         CustomerNameBusinessPartner,
                                         VendorAccNumber,
                                         VendorName,
                                         PersonalNumber,
                                         ContactPersonNumber,
                                         CustomerDescriptionofPartner,
                                         DefaultPartner,
                                         RowStartDate,
                                         RowIsCurrent)
   SELECT DISTINCT ((SELECT ifnull(m.max_id, 1) from NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_customerpartnerfunctions') + row_number() over ()),
          kp.KNVP_KUNNR CustomerNumber1,
          ifnull((SELECT KNA1_NAME1
                    FROM KNA1 k1
                   WHERE k1.KNA1_KUNNR = kp.KNVP_KUNNR),
                 'Not Set')
             CustomerName1,
          ifnull((SELECT TVTWT_VTWEG
                    FROM TVTWT
                   WHERE TVTWT_VTWEG = kp.KNVP_VTWEG),
                 'Not Set')
             DistChannelCode,
          ifnull((SELECT TVTWT_VTEXT
                    FROM TVTWT
                   WHERE TVTWT_VTWEG = kp.KNVP_VTWEG),
                 'Not Set')
             DistChannelName,
          ifnull((SELECT TSPAT_SPART
                    FROM TSPAT
                   WHERE TSPAT_SPART = kp.KNVP_SPART),
                 'Not Set')
             DivisionCode,
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = kp.KNVP_SPART),
                 'Not Set')
             DivisionName,
          ifnull(tp.TPART_PARVW,'Not Set') PartnerFunction,
          ifnull(tp.TPART_VTEXT,'Not Set') PartnerFunctionDesc,
          ifnull((SELECT TVKOT_VKORG
                    FROM TVKOT
                   WHERE TVKOT_VKORG = kp.KNVP_VKORG),
                 'Not Set')
             SalesOrgCode,
          ifnull((SELECT TVKOT_VTEXT
                    FROM TVKOT
                   WHERE TVKOT_VKORG = kp.KNVP_VKORG),
                 'Not Set')
             SalesOrgDesc,
          ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set') PartnerCounter,
          kp.KNVP_KUNN2 CustomerNumberBusinessPartner,
          ifnull((SELECT KNA1_NAME1
                    FROM KNA1 k1
                   WHERE k1.KNA1_KUNNR = kp.KNVP_KUNN2),
                 'Not Set')
             CustomerNameBusinessPartner,
          kp.KNVP_LIFNR VendorAccNumber,
          ifnull((SELECT NAME1
                    FROM LFA1 l1
                   WHERE l1.LIFNR = kp.KNVP_LIFNR),
                 'Not Set')
             VendorName,
          ifnull(kp.KNVP_PERNR, 0) PersonalNumber,
          ifnull(kp.KNVP_PARNR, 0) ContactPersonNumber,
          ifnull(kp.KNVP_KNREF, 'Not Set') CustomerDescriptionofPartner,
          ifnull(kp.KNVP_DEFPA, 'Not Set') DefaultPartner,
          current_date,
          1
          FROM KNVP kp, tpart tp
    WHERE kp.KNVP_PARVW = tp.TPART_PARVW AND NOT EXISTS
                     (SELECT 1
                        FROM dim_customerpartnerfunctions cpf
                       WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
                            AND cpf.SalesOrgCode = kp.KNVP_VKORG
                            AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
                            AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
                            AND cpf.PartnerFunction = kp.KNVP_PARVW
                            AND cpf.PartnerCounter = ifnull(cast(kp.KNVP_PARZA,varchar(10)), 'Not Set'));

delete from NUMBER_FOUNTAIN where table_name = 'dim_customerpartnerfunctions';
							
call vectorwise(combine 'dim_customerpartnerfunctions');