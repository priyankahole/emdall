/* Pre Process For DeltaShipmentHeaderStatus */
INSERT INTO LIKP_LIPS_VBUK(VBUK_WBSTK,
                           VBUK_VBELN,
                           VBUK_UVALS,
                           VBUK_UVALL,
                           VBUK_TRSTA,
                           VBUK_RFSTK,
                           VBUK_RFGSK,
                           VBUK_RELIK,
                           VBUK_LFSTK,
                           VBUK_LFGSK,
                           VBUK_KOSTK,
                           VBUK_KOQUK,
                           VBUK_GBSTK,
                           VBUK_FKSTK,
                           VBUK_FKSAK,
                           VBUK_FKIVK,
                           VBUK_CMGST,
                           VBUK_BUCHK,
                           VBUK_BESTK,
                           VBUK_AEDAT,
                           VBUK_ABSTK,VBUK_UVVLK,VBUK_SPSTG,VBUK_LSSTK)
   SELECT DISTINCT
	  VBUK_WBSTK,
          VBUK_VBELN,
          VBUK_UVALS,
          VBUK_UVALL,
          VBUK_TRSTA,
          VBUK_RFSTK,
          VBUK_RFGSK,
          VBUK_RELIK,
          VBUK_LFSTK,
          VBUK_LFGSK,
          VBUK_KOSTK,
          VBUK_KOQUK,
          VBUK_GBSTK,
          VBUK_FKSTK,
          VBUK_FKSAK,
          VBUK_FKIVK,
          VBUK_CMGST,
          VBUK_BUCHK,
          VBUK_BESTK,
          VBUK_AEDAT,
          VBUK_ABSTK,VBUK_UVVLK,VBUK_SPSTG,VBUK_LSSTK
     FROM DELTA_LIKP_LIPS_VBUK dl
    WHERE NOT EXISTS
             (SELECT l.VBUK_VBELN
                FROM LIKP_LIPS_VBUK l
               WHERE l.VBUK_VBELN = dl.VBUK_VBELN);
CALL VECTORWISE(COMBINE 'LIKP_LIPS_VBUK');