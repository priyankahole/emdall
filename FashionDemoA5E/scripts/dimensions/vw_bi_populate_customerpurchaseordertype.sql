
INSERT INTO dim_customerpurchaseordertype(dim_customerpurchaseordertypeid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customerpurchaseordertype
               WHERE dim_customerpurchaseordertypeid = 1);

UPDATE dim_customerpurchaseordertype cpo FROM T176T t
   SET cpo.Description = ifnull(t.T176T_VTEXT, 'Not Set'),
			cpo.dw_update_date = current_timestamp
 WHERE cpo.CustomerPOType = t.T176T_BSARK AND cpo.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_customerpurchaseordertype';

insert into number_fountain
select 	'dim_customerpurchaseordertype',
	ifnull(max(d.dim_customerpurchaseordertypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customerpurchaseordertype d
where d.dim_customerpurchaseordertypeid <> 1;
			   
INSERT INTO dim_customerpurchaseordertype(dim_customerpurchaseordertypeid,
                                          CustomerPOType,
                                          Description,
                                          RowStartDate,
                                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customerpurchaseordertype')
          + row_number() over() , a.* FROM (SELECT DISTINCT
                        t.T176T_BSARK,
          t.T176T_VTEXT,
          current_timestamp,
          1
     FROM T176T t
    WHERE t.T176T_SPRAS = 'E'
          AND NOT EXISTS
                 (SELECT 1
                    FROM dim_customerpurchaseordertype cpo
                   WHERE cpo.CustomerPOType = t.T176T_BSARK)) a;

delete from number_fountain m where m.table_name = 'dim_customerpurchaseordertype';				   