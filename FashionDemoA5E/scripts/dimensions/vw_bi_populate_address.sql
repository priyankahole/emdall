
INSERT INTO dim_address(dim_addressid,AddressNumber )
SELECT 1,'ADDRESS_NULL'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_address
               WHERE dim_addressid = 1);

/*UPDATE	dim_Address da
FROM	adrc a, t005t
SET		da.Address1 = ifnull(adrc_name1, 'Not Set'),
		da.Address2 = ifnull(adrc_name2, 'Not Set'),
		da.City = ifnull(adrc_city1, 'Not Set'),
		da.State = ifnull(adrc_region, 'Not Set'),
		da.Zip = ifnull(adrc_post_Code1, 'Not Set'),
		da.Country = ifnull(adrc_country, 'Not Set'),
		da.CountryName = ifnull(t005t_landx, 'Not Set'),
		da.dw_update_date = current_timestamp,
		da.street = ifnull(adrc_street,'Not Set'),  /* Marius 16 Dec 2014 + str_suppl1/2/3 + location */
/*		da.str_suppl1 = ifnull(adrc_str_suppl1,'Not Set'),
		da.str_suppl2 = ifnull(adrc_str_suppl2,'Not Set'),
		da.str_suppl3 = ifnull(adrc_str_suppl3,'Not Set'),
		da.location = ifnull(adrc_location,'Not Set')
WHERE   a.adrc_addrnumber = da.AddressNumber
        AND a.adrc_addrnumber IS NOT NULL
        AND a.adrc_country = t005t_land1
*/
    drop table if exists upd_adrc;
        create table upd_adrc as
        select  adrc_addrnumber, adrc_country,
        max(adrc_name1) as adrc_name1,
		max(adrc_name2) as adrc_name2,
		max(adrc_city1) as adrc_city1,
		max(adrc_region) as adrc_region,
		max(adrc_post_Code1) as adrc_post_Code1,
		max(adrc_street) as adrc_street,  /* Marius 16 Dec 2014 + str_suppl1/2/3 + location */
		max(adrc_str_suppl1) as adrc_str_suppl1,
		max(adrc_str_suppl2) as adrc_str_suppl2,
		max(adrc_str_suppl3) as adrc_str_suppl3,
		max(adrc_location) as adrc_location
		from adrc  group by  adrc_addrnumber, adrc_country;

		UPDATE	dim_Address da
FROM	upd_adrc a, t005t
SET		da.Address1 = ifnull(adrc_name1, 'Not Set'),
		da.Address2 = ifnull(adrc_name2, 'Not Set'),
		da.City = ifnull(adrc_city1, 'Not Set'),
		da.State = ifnull(adrc_region, 'Not Set'),
		da.Zip = ifnull(adrc_post_Code1, 'Not Set'),
		da.Country = ifnull(adrc_country, 'Not Set'),
		da.CountryName = ifnull(t005t_landx, 'Not Set'),
		da.dw_update_date = current_timestamp,
		da.street = ifnull(adrc_street,'Not Set'),  /* Marius 16 Dec 2014 + str_suppl1/2/3 + location */
		da.str_suppl1 = ifnull(adrc_str_suppl1,'Not Set'),
		da.str_suppl2 = ifnull(adrc_str_suppl2,'Not Set'),
		da.str_suppl3 = ifnull(adrc_str_suppl3,'Not Set'),
		da.location = ifnull(adrc_location,'Not Set')
WHERE   a.adrc_addrnumber = da.AddressNumber
        AND a.adrc_addrnumber IS NOT NULL
        AND a.adrc_country = t005t_land1;

delete from number_fountain m where m.table_name = 'dim_address';

insert into number_fountain
select 	'dim_address',
	ifnull(max(d.dim_addressid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_address d
where d.dim_addressid <> 1;

INSERT INTO dim_address(Dim_Addressid,
                        Address1,
                        Address2,
                        City,
                        State,
                        Zip,
                        Country,
                        AddressNumber,
                        CountryName,
						street,
						str_suppl1,
						str_suppl2,
						str_suppl3,
						location)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_address')
          + row_number() over(),
                  ifnull(adrc_name1, 'Not Set'),
          ifnull(adrc_name2, 'Not Set'),
          ifnull(adrc_city1, 'Not Set'),
          ifnull(adrc_region, 'Not Set'),
          ifnull(adrc_post_Code1, 'Not Set'),
          ifnull(adrc_country, 'Not Set'),
          adrc_addrnumber,
          ifnull(t005t_landx, 'Not Set'),
		  ifnull(adrc_street,'Not Set'),
		  ifnull(adrc_str_suppl1,'Not Set'),
		  ifnull(adrc_str_suppl2,'Not Set'),
		  ifnull(adrc_str_suppl3,'Not Set'),
		  ifnull(adrc_location,'Not Set')
     FROM adrc a inner join t005t on adrc_country = t005t_land1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_address da
                       WHERE a.adrc_addrnumber = da.AddressNumber)
        and adrc_addrnumber is not null;

delete from number_fountain m where m.table_name = 'dim_address';
