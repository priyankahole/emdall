
UPDATE    dim_itemcategory ic
       FROM
          T163Y t
   SET ic.Description = t.T163Y_PTEXT,
       ic.Category = ifnull(T163Y_EPSTP, 'Not Set')
 WHERE ic.RowIsCurrent = 1
 AND t.T163Y_PSTYP = ic.categorycode;
