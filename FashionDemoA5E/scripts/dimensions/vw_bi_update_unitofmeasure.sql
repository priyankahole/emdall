UPDATE    dim_unitofmeasure uom
       FROM
          t006a a 
   SET uom.Description = a.T006A_MSEHT
 WHERE uom.RowIsCurrent = 1
 AND uom.UOM = a.T006A_MSEHI
;