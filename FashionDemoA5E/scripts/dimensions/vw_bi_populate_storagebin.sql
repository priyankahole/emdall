/* 	Server: QA
	Process Name: Storage Bin Transfer
	Interface No: 2
*/

INSERT INTO dim_storagebin
(dim_storagebinid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_storagebin where dim_storagebinid = 1);

UPDATE    dim_storagebin sb
       FROM
          LAGP l
   SET sb.Zone = ifnull(l.LAGP_LZONE,'Not Set'),
	sb.StorageSection = ifnull(l.LAGP_LGBER,'Not Set'),
	sb.StorageSectionDescription = ifnull((SELECT t.T302T_LBERT from T302T t where 
						t.T302T_LGNUM = l.LAGP_LGNUM
						AND t.T302T_LGTYP = l.LAGP_LGTYP
						AND t.T302T_LGBER = l.LAGP_LGBER) ,'Not Set'),
			sb.dw_update_date = current_timestamp
 WHERE sb.RowIsCurrent = 1
     AND sb.StorageType = l.LAGP_LGTYP
	   AND sb.WarehouseNumber = l.LAGP_LGNUM
	   AND sb.StorageBin = l.LAGP_LGPLA;
 
delete from number_fountain m where m.table_name = 'dim_storagebin';

insert into number_fountain				   
select 	'dim_storagebin',
	ifnull(max(d.dim_storagebinid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_storagebin d
where d.dim_storagebinid <> 1;

INSERT INTO dim_storagebin(dim_storagebinid,
                            WarehouseNumber,
			StorageType,
                            StorageBin,
				Zone,
			    StorageSection,
			    StorageSectionDescription,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_storagebin') 
          + row_number() over() ,
		  l.LAGP_LGNUM,
          l.LAGP_LGTYP,
		  l.LAGP_LGPLA,
		  ifnull(l.LAGP_LZONE,'Not Set'),
          ifnull(l.LAGP_LGBER,'Not Set'),
	  ifnull((SELECT t.T302T_LBERT from T302T t where 
						t.T302T_LGNUM = l.LAGP_LGNUM
						AND t.T302T_LGTYP = l.LAGP_LGTYP
						AND t.T302T_LGBER = l.LAGP_LGBER) ,'Not Set'),
          current_timestamp,
          1
     FROM LAGP l
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_storagebin s
                  WHERE s.StorageType = l.LAGP_LGTYP
					    AND s.WarehouseNumber = l.LAGP_LGNUM
						AND s.StorageBin = l.LAGP_LGPLA
                        AND s.RowIsCurrent = 1) ;
