UPDATE    dim_revenuerecognitioncategory r
       FROM
          DD07T t
   SET r.Description = ifnull(t.DD07T_DDTEXT, 'Not Set')
 WHERE r.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'RR_RELTYP'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND r.Category = ifnull(t.DD07T_DOMVALUE, 'Not Set')
;
