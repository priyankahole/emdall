insert into dim_company(Dim_Companyid,CompanyCode,CompanyName,PostalCode,City,State,Country,Currency,Language,FiscalYearKey,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason,ChartOfAccounts,Region,CountryName) 
SELECT 1,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',current_timestamp,null,1,null,'Not Set','Not Set','Not Set'
FROM (SELECT 1) a
where not exists(select 1 from dim_company where dim_companyid = 1);

/* Commenting mysql code for harmonization
-- Interface Populate Company Code MySQL 

UPDATE dim_company dc
  INNER JOIN t001 t ON t.bukrs = dc.CompanyCode and dc.RowIsCurrent = 1
  LEFT JOIN adrc ON t.t001_adrnr = adrc_addrnumber
  INNER JOIN t005t on t.land1 = t005t_land1
SET dc.CompanyName = butxt,
    dc.PostalCode =  ifnull(adrc_post_code1, 'Not Set'),
    dc.City = ifnull(adrc_city1, 'Not Set'),
    dc.State = ifnull(adrc_region, 'Not Set'),
    dc.Country = ifnull(land1, 'Not Set'),
    dc.CountryName = ifnull(t005t_landx, 'Not Set'),
    dc.Currency = waers,
    dc.ChartOfAccounts =  ktopl,
    dc.Language = spras,
    dc.FiscalYearKey = ifnull(periv, 'Not Set'),
	dc.dw_update_date = current_timestamp
	
INSERT INTO dim_company(CompanyCode,
                        CompanyName,
                        PostalCode,
                        City,
                        State,
                        Country,
                        Currency,
			ChartOfAccounts,
                        Language,
                        FiscalYearKey,
                        RowStartDate,
                        RowIsCurrent,
			CountryName)
   SELECT distinct bukrs,
          butxt,
          ifnull(adrc_post_code1, 'Not Set'),
          ifnull(adrc_city1, 'Not Set'),
          ifnull(adrc_region, 'Not Set'),
          ifnull(land1, 'Not Set'),
          waers,
          ktopl,
          spras,
          ifnull(periv, 'Not Set'),
          current_timestamp,
          1,
	  ifnull(t005t_landx, 'Not Set')
     FROM t001 t LEFT JOIN adrc
             ON t.t001_adrnr = adrc_addrnumber and t.SPRAS = ADRC_LANGU
	  INNER JOIN t005t on t.land1 = t005t_land1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_company
                       WHERE CompanyCode = t.bukrs)

UPDATE dim_company d, country_region_mapping s
   SET d.region = s.region,
		d.dw_update_date = current_timestamp
 WHERE s.countrycode = d.country
       AND ifnull(s.region, 'Not Set') <> ifnull(d.region, 'Not Set')
	   
UPDATE dim_company t
   SET t.region = 'Not Set',
		t.dw_update_date = current_timestamp
 WHERE t.region IS NULL
 
*/

 
/* Interface Populate Company Code VW */
 
 UPDATE dim_company dc
  FROM t001 t 
  LEFT JOIN adrc ON t.t001_adrnr = adrc_addrnumber
  INNER JOIN t005t on t.land1 = t005t_land1
SET dc.CompanyName = butxt,
    dc.PostalCode =  ifnull(adrc_post_code1, 'Not Set'),
    dc.City = ifnull(adrc_city1, 'Not Set'),
    dc.State = ifnull(adrc_region, 'Not Set'),
    dc.Country = ifnull(land1, 'Not Set'),
    dc.CountryName = ifnull(t005t_landx, 'Not Set'),
    dc.Currency = waers,
    dc.ChartOfAccounts =  ktopl,
    dc.Language = spras,
    dc.FiscalYearKey = ifnull(periv, 'Not Set'),
	dc.dw_update_date = current_timestamp
	WHERE t.bukrs = dc.CompanyCode and dc.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_company';

insert into number_fountain
select 	'dim_company',
	ifnull(max(d.Dim_Companyid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_company d
where d.Dim_Companyid <> 1;

	
INSERT INTO dim_company(Dim_Companyid,
			CompanyCode,
                        CompanyName,
                        PostalCode,
                        City,
                        State,
                        Country,
                        Currency,
			ChartOfAccounts,
                        Language,
                        FiscalYearKey,
                        RowStartDate,
                        RowIsCurrent,
			CountryName)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_company') 
          + row_number() over() ,
			bukrs,
          butxt,
          ifnull(adrc_post_code1, 'Not Set'),
          ifnull(adrc_city1, 'Not Set'),
          ifnull(adrc_region, 'Not Set'),
          ifnull(land1, 'Not Set'),
          waers,
          ktopl,
          spras,
          ifnull(periv, 'Not Set'),
          current_timestamp,
          1,
	  ifnull(t005t_landx, 'Not Set')
     FROM t001 t LEFT JOIN adrc
             ON t.t001_adrnr = adrc_addrnumber and t.SPRAS = ADRC_LANGU
	  INNER JOIN t005t on t.land1 = t005t_land1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_company
                       WHERE CompanyCode = t.bukrs);

delete from number_fountain m where m.table_name = 'dim_company';
					   
UPDATE dim_company d from country_region_mapping s
   SET d.region = s.region,
		d.dw_update_date = current_timestamp
 WHERE s.countrycode = d.country
       AND ifnull(s.region, 'Not Set') <> ifnull(d.region, 'Not Set');

UPDATE dim_company t
   SET t.region = 'Not Set',
		t.dw_update_date = current_timestamp
 WHERE t.region IS NULL;
 
