/* 	Server: QA
	Process Name: WM Movement Type Transfer
	Interface No: 2
*/

INSERT INTO dim_wmmovementtype
(dim_wmmovementtypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmmovementtype where dim_wmmovementtypeid = 1);

UPDATE    dim_wmmovementtype wmmt
       FROM
          T333T t
   SET  wmmt.MovementTypeDescription = t.T333T_LBWAT,
			wmmt.dw_update_date = current_timestamp
 WHERE wmmt.RowIsCurrent = 1
     AND wmmt.MovementType = t.T333T_BWLVS
	   AND wmmt.WarehouseNumber = t.T333T_LGNUM;
 
delete from number_fountain m where m.table_name = 'dim_wmmovementtype';

insert into number_fountain				   
select 	'dim_wmmovementtype',
	ifnull(max(d.dim_wmmovementtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmmovementtype d
where d.dim_wmmovementtypeid <> 1;

INSERT INTO dim_wmmovementtype(dim_wmmovementtypeid,
							WarehouseNumber,
							MovementType,
                            MovementTypeDescription,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmmovementtype') + row_number() over(),
		  t.T333T_LGNUM,
          t.T333T_BWLVS,
		  t.T333T_LBWAT,
          current_timestamp,
          1
     FROM T333T t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_wmmovementtype s
                  WHERE    s.MovementType = t.T333T_BWLVS
					    AND s.WarehouseNumber = t.T333T_LGNUM
                        AND s.RowIsCurrent = 1) ;
