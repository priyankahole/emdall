/* INSERT Not Set Record */
insert into dim_casecategories 
(dim_casecategoriesid, casetype, category, description, rowstartdate, rowenddate, rowiscurrent, rowchangereason) 
SELECT 1,'Not Set','Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_casecategories where dim_casecategoriesid = 1);


/* UPDATE    dim_casecategories	*/
UPDATE    dim_casecategories ds
       FROM
          SCMGATTR_CATEGOT dt
   SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set'),
			ds.dw_update_date = current_timestamp
 WHERE ds.RowIsCurrent = 1
       AND ds.CATEGORY = dt.CATEGORY
 	   AND ds.casetype = dt.case_type
	   AND dt.CATEGORY IS NOT NULL;

/* INSERT INTO    dim_casecategories	*/
delete from number_fountain m where m.table_name = 'dim_casecategories';

insert into number_fountain
select 	'dim_casecategories',
	ifnull(max(d.dim_casecategoriesid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_casecategories d
where d.dim_casecategoriesid <> 1;

INSERT INTO dim_casecategories(dim_casecategoriesid,
			       CaseType,
			       Category,
                               Description,
                               RowStartDate,
                               RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_casecategories') 
          + row_number() over() ,
			 ifnull(CASE_TYPE, 'Not Set'),
			 ifnull(CATEGORY, 'Not Set'),
            ifnull(DESCRIPTION,'Not Set'),
            current_timestamp,
            1
       FROM SCMGATTR_CATEGOT s
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_casecategories d
                    WHERE d.category = s.CATEGORY
					AND d.casetype = s.case_type);

delete from number_fountain m where m.table_name = 'dim_casecategories';					