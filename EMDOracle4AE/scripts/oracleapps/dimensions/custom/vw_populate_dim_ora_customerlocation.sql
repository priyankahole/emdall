/*update dimension rows*/
UPDATE dim_ora_customerlocation T
SET 
T.PARTY_NAME = ifnull(S.PARTY_NAME, 'Not Set'),
T.DW_UPDATE_DATE = current_timestamp,
T.rowiscurrent = 1,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_customerlocation T, ORA_HZ_CUSTLOCATION S
WHERE S.SITE_USE_ID  = T.KEY_ID;
