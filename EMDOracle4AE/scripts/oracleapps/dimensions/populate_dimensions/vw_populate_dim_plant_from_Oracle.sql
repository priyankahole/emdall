
DELETE FROM emd586.dim_plant WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_plant
(
	 dim_plantid
	,name
	,country
	,city
	,postalcode

	,"state"
	/*,'INV' */
	,plantcode
	,plant_emd
	,projectsourceid
	,dw_insert_date
	,dw_update_date
	,rowstartdate
	,rowenddate
	,rowiscurrent
	,rowchangereason
	,countryname /*@Catalin M Jira 4133 - add countryname from import*/
	,region
	,manufacturingSite
	,Executive_Scope_flag

)
SELECT
		 d.dim_ora_business_orgid + ifnull(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid -- THE IMPORTANT STEP TO TAKE THE EXISTING KEY AND TO MULTIPLY
		,SUBSTR(d.name,1,30) name
		,d.country
		,d.town_or_city
		,d.postal_code
		,d.region_2
		/*d.org_type */
	    -- ,cmg_number--
        ,d.organization_code--d.organization_id
        ,CASE WHEN d.organization_code <> 'Not Set' THEN d.organization_code || ' - ' || SUBSTR(d.name,1,30) ELSE SUBSTR(d.name,1,30) END
		,p.dim_projectsourceid
		,d.dw_insert_date
		,d.dw_update_date
		,d.rowstartdate
		,d.rowenddate
		,d.rowiscurrent
		,d.rowchangereason
		,org_country_name
		,region
		,'Not Set' manufacturingSite
		,Executive_Scope_flag
FROM emdoracle4ae.dim_ora_business_org d, emdoracle4ae.dim_projectsource p;
