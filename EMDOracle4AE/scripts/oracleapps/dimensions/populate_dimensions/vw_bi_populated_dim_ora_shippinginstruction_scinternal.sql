
TRUNCATE TABLE ora_s_curve_final_temp2;
INSERT INTO ora_s_curve_final_temp2
SELECT DISTINCT FREIGHT_CODE,SHIPMENT_PRIORITY_CODE FROM ora_s_curve;


DELETE FROM NUMBER_FOUNTAIN_dim_ora_shippinginstruction WHERE table_name = 'dim_ora_shippinginstruction';
INSERT INTO NUMBER_FOUNTAIN_dim_ora_shippinginstruction
SELECT 'dim_ora_shippinginstruction',IFNULL(MAX(dim_ora_shippinginstructionid),0)
FROM dim_ora_shippinginstruction;

DELETE FROM dim_ora_shippinginstruction WHERE dim_ora_shippinginstructionid <> 1 ;

INSERT INTO dim_ora_shippinginstruction
(
dim_ora_shippinginstructionid,
shipping_instruction_code,
shipping_instruction_desc,
dim_projectsourceid
)
SELECT (SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_shippinginstruction WHERE TABLE_NAME = 'dim_ora_shippinginstruction')
+ ROW_NUMBER() OVER (order by '') dim_ora_shippinginstructionid
,ifnull(FREIGHT_CODE,'Not Set')
,SHIPMENT_PRIORITY_CODE
,7

from ora_s_curve_final_temp2;
