/**********************************************************************************/
/*  06 Nov 2015   1.00		  LiviuT     First version */
/**********************************************************************************/

/* Get data from SAP EU Project */


TRUNCATE TABLE MDG_MARA;
INSERT INTO MDG_MARA
select * from EMDTempoCC4.MDG_MARA;

TRUNCATE TABLE MDG_T9D241T;
INSERT INTO MDG_T9D241T
select * from EMDTempoCC4.MDG_T9D241T;

TRUNCATE TABLE MDG_T141T;
INSERT INTO MDG_T141T
select * from EMDTempoCC4.MDG_T141T;

TRUNCATE TABLE MDG_T134T;
INSERT INTO MDG_T134T
select * from EMDTempoCC4.MDG_T134T;


TRUNCATE TABLE MDG_TPTMT;
INSERT INTO MDG_TPTMT
select * from EMDTempoCC4.MDG_TPTMT;

TRUNCATE TABLE MDG_T179T;
INSERT INTO MDG_T179T
select * from EMDTempoCC4.MDG_T179T;

TRUNCATE TABLE MDG_T9D520T;
INSERT INTO MDG_T9D520T
select * from EMDTempoCC4.MDG_T9D520T;

TRUNCATE TABLE MDG_T9D335T;
INSERT INTO MDG_T9D335T
select * from EMDTempoCC4.MDG_T9D335T;

TRUNCATE TABLE MDG_YYD_V_MAPPING;
INSERT INTO MDG_YYD_V_MAPPING
select * from EMDTempoCC4.MDG_YYD_V_MAPPING;

TRUNCATE TABLE MDG_YYD_V_YORG;
INSERT INTO MDG_YYD_V_YORG
select * from EMDTempoCC4.MDG_YYD_V_YORG;

INSERT INTO dim_ora_mdg_product
(
  dim_mdg_partid,
  partnumber,
  product_hierarchy,
  global_operational_hierarchy,
  planned_product_hierarchy,
  product_hierarchy_phoenix,
  price_hierarchy,
  product_hierarchy_description,
  MatTypeforGlbRep,
  GlbProductGroup,
  GlbABCIndicator,
  Oldmatnumber,
  BaseUnitofMeasure,
  TotalShelfLife,
  MinRemainingShelfLife,
  CrossPlantMatStatus,
  MaterialType,
  Generalitemcatgroup,
  Producthierarchy,
  Batchmanreqind,
  APORelevance,
  MatTypeforGlbRepDescr,
  CrossPlantMatStatusDescr,
  MaterialTypeDescr,
  GeneralitemcatgroupDescr,
  ProducthierarchyDescr,
  APORelevanceDesc
)
SELECT 1,
       'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   0,
	   0,
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set'
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 'x' FROM dim_ora_mdg_product x WHERE x.dim_mdg_partid = 1);

UPDATE dim_ora_mdg_product
SET MatTypeforGlbRep = IFNULL(YYD_GLMTY,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(MatTypeforGlbRep,'a') <> IFNULL(YYD_GLMTY,'Not Set');

UPDATE dim_ora_mdg_product
SET GlbProductGroup = IFNULL(YYD_YSBU,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(GlbProductGroup,'a') <> IFNULL(YYD_YSBU,'Not Set');

UPDATE dim_ora_mdg_product
SET GlbABCIndicator = IFNULL(YYD_ABCCL,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(GlbABCIndicator,'a') <> IFNULL(YYD_ABCCL,'Not Set');

UPDATE dim_ora_mdg_product
SET Oldmatnumber = IFNULL(BISMT,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(Oldmatnumber,'a') <> IFNULL(BISMT,'Not Set');

UPDATE dim_ora_mdg_product
SET BaseUnitofMeasure = IFNULL(MEINS,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(BaseUnitofMeasure,'a') <> IFNULL(MEINS,'Not Set');

UPDATE dim_ora_mdg_product
SET TotalShelfLife = IFNULL(MHDHB,0)
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(TotalShelfLife,-1) <> IFNULL(MHDHB,0);

UPDATE dim_ora_mdg_product
SET MinRemainingShelfLife = IFNULL(MHDRZ,0)
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(MinRemainingShelfLife,-1) <> IFNULL(MHDRZ,0);

UPDATE dim_ora_mdg_product
SET CrossPlantMatStatus = IFNULL(MSTAE,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(CrossPlantMatStatus,'a') <> IFNULL(MSTAE,'Not Set');

UPDATE dim_ora_mdg_product
SET MaterialType = IFNULL(MTART,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(MaterialType,'a') <> IFNULL(MTART,'Not Set');

UPDATE dim_ora_mdg_product
SET Generalitemcatgroup = IFNULL(MTPOS_MARA,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(Generalitemcatgroup,'a') <> IFNULL(MTPOS_MARA,'Not Set');

UPDATE dim_ora_mdg_product
SET Producthierarchy = IFNULL(PRDHA,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(Producthierarchy,'a') <> IFNULL(PRDHA,'Not Set');

UPDATE dim_ora_mdg_product
SET Batchmanreqind = IFNULL(XCHPF,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(Batchmanreqind,'a') <> IFNULL(XCHPF,'Not Set');

UPDATE dim_ora_mdg_product
SET APORelevance = IFNULL(YYD_APORL,'Not Set')
FROM dim_ora_mdg_product, MDG_MARA
WHERE partnumber = MATNR
	AND IFNULL(APORelevance,'a') <> IFNULL(YYD_APORL,'Not Set');

UPDATE dim_ora_mdg_product d
SET d.global_operational_hierarchy = IFNULL(m.YYD_GLOPH, 'Not Set'),
    d.dw_update_date = current_timestamp
FROM dim_ora_mdg_product d, MDG_MARA m
WHERE d.partnumber = m.MATNR
      AND d.global_operational_hierarchy <> IFNULL(m.YYD_GLOPH, 'Not Set');

UPDATE dim_ora_mdg_product
SET MatTypeforGlbRepDescr = IFNULL(yyd_text,'Not Set')
FROM dim_ora_mdg_product,MDG_T9D241T
WHERE MatTypeforGlbRep = yyd_glmty
	AND IFNULL(MatTypeforGlbRepDescr,'a') <> IFNULL(yyd_text,'Not Set');

UPDATE dim_ora_mdg_product
SET CrossPlantMatStatusDescr = IFNULL(mtstb,'Not Set')
FROM dim_ora_mdg_product, MDG_T141T
WHERE CrossPlantMatStatus = mmsta
	AND IFNULL(CrossPlantMatStatusDescr,'a') <> IFNULL(mtstb,'Not Set');

UPDATE dim_ora_mdg_product
SET MaterialTypeDescr = IFNULL(t134t_mtbez,'Not Set')
FROM dim_ora_mdg_product, MDG_T134T
WHERE MaterialType = t134t_mtart
	AND IFNULL(MaterialTypeDescr,'a') <> IFNULL(t134t_mtbez,'Not Set');

UPDATE dim_ora_mdg_product
SET GeneralitemcatgroupDescr = IFNULL(bezei,'Not Set')
FROM dim_ora_mdg_product, MDG_TPTMT
WHERE Generalitemcatgroup = mtpos
	AND IFNULL(GeneralitemcatgroupDescr,'a') <> IFNULL(bezei,'Not Set');

UPDATE dim_ora_mdg_product
SET ProducthierarchyDescr = IFNULL(mdg_t179t_vtext,'Not Set')
FROM dim_ora_mdg_product, MDG_T179T
WHERE Producthierarchy = mdg_t179t_prodh
	AND IFNULL(ProducthierarchyDescr,'a') <> IFNULL(mdg_t179t_vtext,'Not Set');

UPDATE dim_ora_mdg_product
SET APORelevanceDesc = IFNULL(ltext,'Not Set')
FROM dim_ora_mdg_product, MDG_T9D520T
WHERE APORelevance = yyd_aporl
	AND IFNULL(APORelevanceDesc,'a') <> IFNULL(ltext,'Not Set');

UPDATE dim_ora_mdg_product d
SET global_operational_hierarchy_description = IFNULL(mdg_t9d335t_ltext,'Not Set'),
	d.dw_update_date = CURRENT_TIMESTAMP
FROM dim_ora_mdg_product, MDG_T9D335T
WHERE mdg_t9d335t_yyd_gloph = global_operational_hierarchy AND global_operational_hierarchy_description <> IFNULL(mdg_t9d335t_ltext,'Not Set');

/* INSERT NEW ROWS */
DELETE FROM NUMBER_FOUNTAIN_dim_ora_mdg_product m
WHERE m.table_name = 'dim_ora_mdg_product';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_mdg_product
SELECT 'dim_ora_mdg_product',
       IFNULL(MAX(d.dim_mdg_partid ), IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_ora_mdg_product d
WHERE d.dim_mdg_partid <> 1;

INSERT INTO dim_ora_mdg_product
(
  dim_mdg_partid,
  partnumber,
  MatTypeforGlbRep,
  GlbProductGroup,
  GlbABCIndicator,
  Oldmatnumber,
  BaseUnitofMeasure,
  TotalShelfLife,
  MinRemainingShelfLife,
  CrossPlantMatStatus,
  MaterialType,
  Generalitemcatgroup,
  Producthierarchy,
  Batchmanreqind,
  APORelevance,
  global_operational_hierarchy,
  MatTypeforGlbRepDescr,
  CrossPlantMatStatusDescr,
  MaterialTypeDescr,
  GeneralitemcatgroupDescr,
  ProducthierarchyDescr,
  APORelevanceDesc,
  global_operational_hierarchy_description
)
SELECT (SELECT IFNULL(m.max_id, 1)
        FROM NUMBER_FOUNTAIN_dim_ora_mdg_product m
        WHERE m.table_name = 'dim_ora_mdg_product') + ROW_NUMBER() OVER( ORDER BY '') AS dim_mdg_partid,
       m.MATNR AS partnumber,
	   IFNULL(YYD_GLMTY,'Not Set'),
	   IFNULL(YYD_YSBU,'Not Set'),
	   IFNULL(YYD_ABCCL,'Not Set'),
	   IFNULL(BISMT,'Not Set'),
	   IFNULL(MEINS,'Not Set'),
	   IFNULL(MHDHB,0),
	   IFNULL(MHDRZ,0),
	   IFNULL(MSTAE,'Not Set'),
	   IFNULL(MTART,'Not Set'),
	   IFNULL(MTPOS_MARA,'Not Set'),
	   IFNULL(PRDHA,'Not Set'),
	   IFNULL(XCHPF,'Not Set'),
	   IFNULL(YYD_APORL,'Not Set'),
	   IFNULL(YYD_GLOPH, 'Not Set'),
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set'
FROM MDG_MARA m
WHERE NOT EXISTS (SELECT 'x'
                  FROM dim_ora_mdg_product dmp
                  WHERE dmp.partnumber = m.MATNR);

UPDATE dim_ora_mdg_product
SET MatTypeforGlbRepDescr = IFNULL(yyd_text,'Not Set')
FROM dim_ora_mdg_product, MDG_T9D241T
WHERE MatTypeforGlbRep = yyd_glmty
	AND IFNULL(MatTypeforGlbRepDescr,'a') <> IFNULL(yyd_text,'Not Set');

UPDATE dim_ora_mdg_product
SET CrossPlantMatStatusDescr = IFNULL(mtstb,'Not Set')
FROM dim_ora_mdg_product, MDG_T141T
WHERE CrossPlantMatStatus = mmsta
	AND IFNULL(CrossPlantMatStatusDescr,'a') <> IFNULL(mtstb,'Not Set');

UPDATE dim_ora_mdg_product
SET MaterialTypeDescr = IFNULL(t134t_mtbez,'Not Set')
FROM dim_ora_mdg_product, MDG_T134T
WHERE MaterialType = t134t_mtart
	AND IFNULL(MaterialTypeDescr,'a') <> IFNULL(t134t_mtbez,'Not Set');

UPDATE dim_ora_mdg_product
SET GeneralitemcatgroupDescr = IFNULL(bezei,'Not Set')
FROM dim_ora_mdg_product, MDG_TPTMT
WHERE Generalitemcatgroup = mtpos
	AND IFNULL(GeneralitemcatgroupDescr,'a') <> IFNULL(bezei,'Not Set');

UPDATE dim_ora_mdg_product
SET ProducthierarchyDescr = IFNULL(mdg_t179t_vtext,'Not Set')
FROM dim_ora_mdg_product, MDG_T179T
WHERE Producthierarchy = mdg_t179t_prodh
	AND IFNULL(ProducthierarchyDescr,'a') <> IFNULL(mdg_t179t_vtext,'Not Set');

UPDATE dim_ora_mdg_product
SET APORelevanceDesc = IFNULL(ltext,'Not Set')
FROM dim_ora_mdg_product, MDG_T9D520T
WHERE APORelevance = yyd_aporl
	AND IFNULL(APORelevanceDesc,'a') <> IFNULL(ltext,'Not Set');

UPDATE dim_ora_mdg_product d
SET global_operational_hierarchy_description = IFNULL(mdg_t9d335t_ltext,'Not Set'),
	d.dw_update_date = current_timestamp
FROM dim_ora_mdg_product d, MDG_T9D335T
WHERE mdg_t9d335t_yyd_gloph = global_operational_hierarchy AND global_operational_hierarchy_description <> IFNULL(mdg_t9d335t_ltext,'Not Set');

/* aditional atributes */

drop table if exists tmp_aditionam_mdm_atributes;
create table tmp_aditionam_mdm_atributes
as
select a.md_mmmaterial,a.usmdkmmmaterial,b.usmd_active,b.usmd_o_yorg,b.md_mmyyd_orga,b.md_mmyyd_yorga
FROM MDG_YYD_V_MAPPING a
	inner join MDG_YYD_V_YORG b on a.usmdkmmmaterial = b.usmdkmmmaterial AND usmd_active = 1 AND IFNULL(usmd_o_yorg,'Y') <> 'X';

UPDATE dim_ora_mdg_product d
SET d.primary_production_location = IFNULL(t.md_mmyyd_yorga,'Not Set')
FROM dim_ora_mdg_product d, tmp_aditionam_mdm_atributes t
WHERE d.partnumber = t.md_mmmaterial AND t.md_mmyyd_orga = 'PPL'
	AND IFNULL(d.primary_production_location,'aaa') <> IFNULL(t.md_mmyyd_yorga,'Not Set');

UPDATE dim_ora_mdg_product d
SET d.global_demand_planner = IFNULL(t.md_mmyyd_yorga,'Not Set')
FROM dim_ora_mdg_product d, tmp_aditionam_mdm_atributes t
WHERE d.partnumber = t.md_mmmaterial AND t.md_mmyyd_orga = 'GDP'
	AND IFNULL(d.global_demand_planner,'aaa') <> IFNULL(t.md_mmyyd_yorga,'Not Set');

UPDATE dim_ora_mdg_product d
SET d.global_supply_planner = IFNULL(t.md_mmyyd_yorga,'Not Set')
FROM dim_ora_mdg_product d, tmp_aditionam_mdm_atributes t
WHERE d.partnumber = t.md_mmmaterial AND t.md_mmyyd_orga = 'GSP'
	AND IFNULL(d.global_supply_planner,'aaa') <> IFNULL(t.md_mmyyd_yorga,'Not Set');

update dim_ora_mdg_product  d
set d.global_demand_planner_desc = ifnull(t.LTEXT,'Not Set')
from dim_ora_mdg_product  d, MDG_T9D519T t
where d.global_demand_planner = t.YYD_YORGA and d.global_demand_planner_desc <> ifnull(t.LTEXT,'Not Set');

update dim_ora_mdg_product  d
set d.global_supply_planner_desc = ifnull(t.LTEXT,'Not Set')
from dim_ora_mdg_product  d, MDG_T9D519T t
where d.global_supply_planner = t.YYD_YORGA and d.global_supply_planner_desc <> ifnull(t.LTEXT,'Not Set');

/* 21 Mar 2017 Cornelia add PRM and PRP */
update dim_ora_mdg_product  d
set d.PRM_Product_manager_code = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_ora_mdg_product  d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PRM'
	and ifnull(d.PRM_Product_manager_code,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_ora_mdg_product  d
set d.PRM_Product_manager_desc = ifnull(t.LTEXT,'Not Set')
from dim_ora_mdg_product  d, MDG_T9D519T t
where d.PRM_Product_manager_code = t.YYD_YORGA and d.PRM_Product_manager_desc <> ifnull(t.LTEXT,'Not Set');


update dim_ora_mdg_product  d
set d.PRP_Price_responsible_code = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_ora_mdg_product  d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PRP'
	and ifnull(d.PRP_Price_responsible_code,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_ora_mdg_product  d
set d.PRP_Price_responsible_desc = ifnull(t.LTEXT,'Not Set')
from dim_ora_mdg_product  d, MDG_T9D519T t
where d.PRP_Price_responsible_code = t.YYD_YORGA and d.PRP_Price_responsible_desc <> ifnull(t.LTEXT,'Not Set');
/* END 21 Mar 2017 */
drop table if exists tmp_aditionam_mdm_atributes;

TRUNCATE TABLE mdg_t9d519t;
INSERT INTO mdg_t9d519t
SELECT * FROM emdtempocc4.mdg_t9d519t;


UPDATE dim_ora_mdg_product d
SET d.primary_production_location_name = IFNULL(t.ltext,'Not Set')
FROM dim_ora_mdg_product d, mdg_t9d519t t
WHERE d.primary_production_location = t.yyd_yorga
  AND d.primary_production_location_name <> IFNULL(t.ltext,'Not Set');

TRUNCATE TABLE emdoracle4ae.mdg_makt;
INSERT INTO mdg_makt
SELECT * FROM emdtempocc4.mdg_makt;

UPDATE dim_ora_mdg_product d
   SET d.partdescription = ifnull(t.makt_maktx,'Not Set')
FROM dim_ora_mdg_product d
INNER JOIN mdg_makt t ON d.partnumber = t.makt_matnr
WHERE d.partdescription <> ifnull(t.makt_maktx,'Not Set');

UPDATE dim_ora_mdg_product
SET supplychainplaningppi = IFNULL(YYD_SCPPI,'Not Set')
FROM dim_ora_mdg_product, mdg_mara
WHERE partnumber = MATNR
AND IFNULL(supplychainplaningppi,'a') <> IFNULL(YYD_SCPPI,'Not Set');


/*BI-4717 - add maingroup fields to MDG - Alina*/


UPDATE dim_ora_mdg_product c
SET maingroup = IFNULL(substring(c.producthierarchy,1,3),'Not Set')
   ,uppergroup = IFNULL(substring(c.producthierarchy,1,6),'Not Set')
   ,articlegroup = IFNULL(substring(c.producthierarchy,1,9),'Not Set')
   ,internationalarticle = IFNULL(substring(c.producthierarchy,1,12),'Not Set')
   ,dw_update_date = CURRENT_DATE
WHERE producthierarchy <>'Not Set';

UPDATE dim_ora_mdg_product d
SET maingroupdesc = ifnull(mdg_t179t_vtext,'Not Set')
FROM dim_ora_mdg_product d,MDG_T179T c
WHERE d.maingroup = c.mdg_t179t_prodh
AND maingroupdesc <> ifnull(mdg_t179t_vtext,'Not Set');

UPDATE dim_ora_mdg_product d
SET uppergroupdesc = ifnull(mdg_t179t_vtext,'Not Set')
FROM dim_ora_mdg_product d,MDG_T179T c
WHERE d.uppergroup = c.mdg_t179t_prodh
AND uppergroupdesc <> ifnull(mdg_t179t_vtext,'Not Set');

UPDATE dim_ora_mdg_product d
SET articlegroupdesc = ifnull(mdg_t179t_vtext,'Not Set')
FROM dim_ora_mdg_product d,MDG_T179T c
WHERE d.articlegroup = c.mdg_t179t_prodh
AND articlegroupdesc <> ifnull(mdg_t179t_vtext,'Not Set');

UPDATE dim_ora_mdg_product d
SET internationalarticledesc = ifnull(mdg_t179t_vtext,'Not Set')
FROM dim_ora_mdg_product d,MDG_T179T c
WHERE d.internationalarticle = c.mdg_t179t_prodh
AND internationalarticledesc <> ifnull(mdg_t179t_vtext,'Not Set');

update dim_ora_mdg_product
set IndicatorSalesCatalogue = ifnull(YYD_YKZLP,'Not Set')
from dim_ora_mdg_product, MDG_MARA
where partnumber = MATNR
	and ifnull(IndicatorSalesCatalogue,'a') <> ifnull(YYD_YKZLP,'Not Set');

update dim_ora_mdg_product dmp
set dmp.IndicatorSalesCatalogueDesc = ifnull(cd.description,'Not Set')
from dim_ora_mdg_product dmp,  catalogue_desc cd
where dmp.IndicatorSalesCatalogue = cd.code
	and ifnull(IndicatorSalesCatalogueDesc,'a') <> ifnull(cd.description,'Not Set');

UPDATE dim_ora_mdg_product SET projectsourceid = 7;

/* 7 august 2017 - Cornelia add new attribute MaterialCategory */
update dim_ora_mdg_product
set materialcategory ='Finished goods'
where mattypeforglbrep in (
'MER',
'FERT',
'FIN',
'ZCSM',
'ZFRT',
'ZVFT',
'ZVAR',
'KMAT',
'LEER',
'NLAG',
'UNBW',
'ZLIT',
'ZRCH',
'ZCTO',
'Not Set'
);


update dim_ora_mdg_product
set materialcategory ='Raw Material'
where mattypeforglbrep in (
'DIEN',
'DUM',
'HIBE',
'RAW',
'ROH',
'VERP',
'ZUNB',
'ZVRP',
'PROC',
'ZHLB',
'ZIMU',
'ABF'
);

update dim_ora_mdg_product
set materialcategory ='Unfinished Goods'
where mattypeforglbrep in ('HALB','UFG');