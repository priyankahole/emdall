
DELETE FROM emd586.dim_producthierarchy WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );



INSERT INTO emd586.dim_producthierarchy(dim_producthierarchyid
                                          ,ProductHierarchy
										  ,level1code
										  ,level2code
										  ,level3code
										  ,level4code
                                          ,RowStartDate
                                          ,RowIsCurrent
										  ,dw_insert_date
										  ,dw_update_date
										  ,projectsourceid)
SELECT  
		 dim_producthierarchyid
		,producthierarchy
		,level1code
		,level2code
		,level3code
		,level4code
		,RowStartDate
		,RowIsCurrent
		,dw_insert_date
		,dw_update_date
		,projectsourceid
FROM emdoracle4ae.dim_ora_producthierarchy
WHERE dim_producthierarchyid <> 1;
