/*insert default row*/
/*INSERT INTO dim_ora_mtl_salesorders
(
dim_ora_mtl_salesordersid,
ORDER_TYPE,SOURCE_CODE,END_DATE_ACTIVE,START_DATE_ACTIVE,ENABLED_FLAG,SUMMARY_FLAG,
SEGMENT20,SEGMENT19,SEGMENT18,SEGMENT17,SEGMENT16,SEGMENT15,SEGMENT14,SEGMENT13,
SEGMENT12,SEGMENT11,SEGMENT10,SEGMENT9,SEGMENT8,SEGMENT7,SEGMENT6,SEGMENT5,
SEGMENT4,SEGMENT3,SEGMENT2,SEGMENT1,LAST_UPDATE_LOGIN,CREATED_BY,CREATION_DATE,
LAST_UPDATED_BY,LAST_UPDATE_DATE,SALES_ORDER_ID,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT T.* FROM
(SELECT
1 dim_ora_mtl_salesordersid,
'Not Set' ORDER_TYPE,
'Not Set' SOURCE_CODE,
timestamp '1001-01-01 00:00:00.000000' END_DATE_ACTIVE,
timestamp '1001-01-01 00:00:00.000000' START_DATE_ACTIVE,
'Not Set' ENABLED_FLAG,
'Not Set' SUMMARY_FLAG,
'Not Set' SEGMENT20,
'Not Set' SEGMENT19,
'Not Set' SEGMENT18,
'Not Set' SEGMENT17,
'Not Set' SEGMENT16,
'Not Set' SEGMENT15,
'Not Set' SEGMENT14,
'Not Set' SEGMENT13,
'Not Set' SEGMENT12,
'Not Set' SEGMENT11,
'Not Set' SEGMENT10,
'Not Set' SEGMENT9,
'Not Set' SEGMENT8,
'Not Set' SEGMENT7,
'Not Set' SEGMENT6,
'Not Set' SEGMENT5,
'Not Set' SEGMENT4,
'Not Set' SEGMENT3,
'Not Set' SEGMENT2,
'Not Set' SEGMENT1,
0 LAST_UPDATE_LOGIN,
0 CREATED_BY,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 SALES_ORDER_ID,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_mtl_salesorders D
ON T.dim_ora_mtl_salesordersid = D.dim_ora_mtl_salesordersid WHERE D.dim_ora_mtl_salesordersid IS NULL*/

TRUNCATE TABLE tmp_dim_ora_mtl;
INSERT INTO tmp_dim_ora_mtl
SELECT
	1 dim_ora_mtl_salesordersid,
	'Not Set' ORDER_TYPE,
	'Not Set' SOURCE_CODE,
	timestamp '1001-01-01 00:00:00.000000' END_DATE_ACTIVE,
	timestamp '1001-01-01 00:00:00.000000' START_DATE_ACTIVE,
	'Not Set' ENABLED_FLAG,
	'Not Set' SUMMARY_FLAG,
	'Not Set' SEGMENT20,
	'Not Set' SEGMENT19,
	'Not Set' SEGMENT18,
	'Not Set' SEGMENT17,
	'Not Set' SEGMENT16,
	'Not Set' SEGMENT15,
	'Not Set' SEGMENT14,
	'Not Set' SEGMENT13,
	'Not Set' SEGMENT12,
	'Not Set' SEGMENT11,
	'Not Set' SEGMENT10,
	'Not Set' SEGMENT9,
	'Not Set' SEGMENT8,
	'Not Set' SEGMENT7,
	'Not Set' SEGMENT6,
	'Not Set' SEGMENT5,
	'Not Set' SEGMENT4,
	'Not Set' SEGMENT3,
	'Not Set' SEGMENT2,
	'Not Set' SEGMENT1,
	0 LAST_UPDATE_LOGIN,
	0 CREATED_BY,
	timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
	0 LAST_UPDATED_BY,
	timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
	0 SALES_ORDER_ID,
	timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
	timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
	current_timestamp AS rowstartdate,
	timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
	1 AS rowiscurrent, 'Insert' AS rowchangereason,
	0 KEY_ID, 'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A;

INSERT INTO dim_ora_mtl_salesorders
( dim_ora_mtl_salesordersid, ORDER_TYPE,SOURCE_CODE,END_DATE_ACTIVE,START_DATE_ACTIVE,ENABLED_FLAG,SUMMARY_FLAG, SEGMENT20,SEGMENT19,SEGMENT18,SEGMENT17,SEGMENT16,SEGMENT15,SEGMENT14,SEGMENT13, SEGMENT12,SEGMENT11,SEGMENT10,SEGMENT9,SEGMENT8,SEGMENT7,SEGMENT6,SEGMENT5, SEGMENT4,SEGMENT3,SEGMENT2,SEGMENT1,LAST_UPDATE_LOGIN,CREATED_BY,CREATION_DATE, LAST_UPDATED_BY,LAST_UPDATE_DATE,SALES_ORDER_ID, DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason, KEY_ID,SOURCE_ID )
SELECT T.*
FROM tmp_dim_ora_mtl T
	LEFT JOIN dim_ora_mtl_salesorders D ON T.dim_ora_mtl_salesordersid = D.dim_ora_mtl_salesordersid WHERE D.dim_ora_mtl_salesordersid IS NULL;

/*initialize NUMBER_FOUNTAIN_dim_ora_mtl_salesorders*/
delete from NUMBER_FOUNTAIN_dim_ora_mtl_salesorders where table_name = 'dim_ora_mtl_salesorders';	

INSERT INTO NUMBER_FOUNTAIN_dim_ora_mtl_salesorders
SELECT 'dim_ora_mtl_salesorders',IFNULL(MAX(dim_ora_mtl_salesordersid),0)
FROM dim_ora_mtl_salesorders;

/*update dimension rows*/
UPDATE dim_ora_mtl_salesorders T
SET
T.ORDER_TYPE = ifnull(S.ORDER_TYPE, 'Not Set'),
T.SOURCE_CODE = ifnull(S.SOURCE_CODE, 'Not Set'),
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000'),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000'),
T.ENABLED_FLAG = ifnull(S.ENABLED_FLAG, 'Not Set'),
T.SUMMARY_FLAG = ifnull(S.SUMMARY_FLAG, 'Not Set'),
T.SEGMENT20 = ifnull(S.SEGMENT20, 'Not Set'),
T.SEGMENT19 = ifnull(S.SEGMENT19, 'Not Set'),
T.SEGMENT18 = ifnull(S.SEGMENT18, 'Not Set'),
T.SEGMENT17 = ifnull(S.SEGMENT17, 'Not Set'),
T.SEGMENT16 = ifnull(S.SEGMENT16, 'Not Set'),
T.SEGMENT15 = ifnull(S.SEGMENT15, 'Not Set'),
T.SEGMENT14 = ifnull(S.SEGMENT14, 'Not Set'),
T.SEGMENT13 = ifnull(S.SEGMENT13, 'Not Set'),
T.SEGMENT12 = ifnull(S.SEGMENT12, 'Not Set'),
T.SEGMENT11 = ifnull(S.SEGMENT11, 'Not Set'),
T.SEGMENT10 = ifnull(S.SEGMENT10, 'Not Set'),
T.SEGMENT9 = ifnull(S.SEGMENT9, 'Not Set'),
T.SEGMENT8 = ifnull(S.SEGMENT8, 'Not Set'),
T.SEGMENT7 = ifnull(S.SEGMENT7, 'Not Set'),
T.SEGMENT6 = ifnull(S.SEGMENT6, 'Not Set'),
T.SEGMENT5 = ifnull(S.SEGMENT5, 'Not Set'),
T.SEGMENT4 = ifnull(S.SEGMENT4, 'Not Set'),
T.SEGMENT3 = ifnull(S.SEGMENT3, 'Not Set'),
T.SEGMENT2 = ifnull(S.SEGMENT2, 'Not Set'),
T.SEGMENT1 = ifnull(S.SEGMENT1, 'Not Set'),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_mtl_salesorders T, ora_mtl_salesorders S
WHERE
T.KEY_ID = S.SALES_ORDER_ID;

/*insert new rows*/
INSERT INTO dim_ora_mtl_salesorders
(
dim_ora_mtl_salesordersid,
ORDER_TYPE,SOURCE_CODE,END_DATE_ACTIVE,START_DATE_ACTIVE,ENABLED_FLAG,SUMMARY_FLAG,
SEGMENT20,SEGMENT19,SEGMENT18,SEGMENT17,SEGMENT16,SEGMENT15,SEGMENT14,SEGMENT13,
SEGMENT12,SEGMENT11,SEGMENT10,SEGMENT9,SEGMENT8,SEGMENT7,SEGMENT6,SEGMENT5,
SEGMENT4,SEGMENT3,SEGMENT2,SEGMENT1,LAST_UPDATE_LOGIN,CREATED_BY,CREATION_DATE,
LAST_UPDATED_BY,LAST_UPDATE_DATE,SALES_ORDER_ID,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_mtl_salesorders WHERE TABLE_NAME = 'dim_ora_mtl_salesorders' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_mtl_salesordersid,
ifnull(S.ORDER_TYPE, 'Not Set') ORDER_TYPE,
ifnull(S.SOURCE_CODE, 'Not Set') SOURCE_CODE,
ifnull(S.END_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.START_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.ENABLED_FLAG, 'Not Set') ENABLED_FLAG,
ifnull(S.SUMMARY_FLAG, 'Not Set') SUMMARY_FLAG,
ifnull(S.SEGMENT20, 'Not Set') SEGMENT20,
ifnull(S.SEGMENT19, 'Not Set') SEGMENT19,
ifnull(S.SEGMENT18, 'Not Set') SEGMENT18,
ifnull(S.SEGMENT17, 'Not Set') SEGMENT17,
ifnull(S.SEGMENT16, 'Not Set') SEGMENT16,
ifnull(S.SEGMENT15, 'Not Set') SEGMENT15,
ifnull(S.SEGMENT14, 'Not Set') SEGMENT14,
ifnull(S.SEGMENT13, 'Not Set') SEGMENT13,
ifnull(S.SEGMENT12, 'Not Set') SEGMENT12,
ifnull(S.SEGMENT11, 'Not Set') SEGMENT11,
ifnull(S.SEGMENT10, 'Not Set') SEGMENT10,
ifnull(S.SEGMENT9, 'Not Set') SEGMENT9,
ifnull(S.SEGMENT8, 'Not Set') SEGMENT8,
ifnull(S.SEGMENT7, 'Not Set') SEGMENT7,
ifnull(S.SEGMENT6, 'Not Set') SEGMENT6,
ifnull(S.SEGMENT5, 'Not Set') SEGMENT5,
ifnull(S.SEGMENT4, 'Not Set') SEGMENT4,
ifnull(S.SEGMENT3, 'Not Set') SEGMENT3,
ifnull(S.SEGMENT2, 'Not Set') SEGMENT2,
ifnull(S.SEGMENT1, 'Not Set') SEGMENT1,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
S.SALES_ORDER_ID,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.SALES_ORDER_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ora_mtl_salesorders S
LEFT JOIN dim_ora_mtl_salesorders D
ON D.KEY_ID = S.SALES_ORDER_ID
WHERE D.KEY_ID is null;
