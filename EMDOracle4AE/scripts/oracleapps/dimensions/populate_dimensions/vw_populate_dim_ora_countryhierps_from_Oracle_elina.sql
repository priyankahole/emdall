
DELETE FROM emd586.dim_countryhierps WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


INSERT INTO emd586.dim_countryhierps
(
 DIM_COUNTRYHIERPSID	
,COUNTRY	
,COUNTRYNAME
,TOTALPS	
,REGIONPS1	
,REGIONPS2	
,REGIONPS3	
,REGIONPS4
,DW_INSERT_DATE	
,DW_UPDATE_DATE	
,PROJECTSOURCEID
)

SELECT 
 DIM_COUNTRYHIERPSID + IFNULL(s.dim_projectsourceid * s.multiplier ,0) DIM_COUNTRYHIERARID
,COUNTRY	
,COUNTRYNAME
,TOTALPS	
,REGIONPS1	
,REGIONPS2	
,REGIONPS3	
,REGIONPS4	
,DW_INSERT_DATE	
,DW_UPDATE_DATE	
,s.dim_projectsourceid 

from emdoracle4ae.dim_ora_countryhierps, emdoracle4ae.dim_projectsource s

