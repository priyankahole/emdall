
DELETE FROM emd586.dim_mdg_part WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

INSERT INTO emd586.DIM_MDG_PART
(
	 dim_mdg_partid
	,dw_insert_date
	,dw_update_date
	,partnumber
	,product_hierarchy
	,global_operational_hierarchy
	,planned_product_hierarchy
	,product_hierarchy_phoenix
	,price_hierarchy
	,product_hierarchy_description
	,planned_product_hierarchy_description
	,projectsourceid
	,global_operational_hierarchy_description
	,product_hierarchy_phoenix_description
	,price_hierarchy_description
	,zbw_description
	,zbw_division
	,zbw_businessunit
	,zbw_businessfield
	,zbw_businessline
	,zbw_productgroup
	,zbw_maingroup
	,zbw_uppergroup
	,zbw_articlegroup
	,zbw_interntaionalarticle
	,zbw_valuationclass
	,zbw_producttype
	,zbw_lsku
	,zbw_hier_description_lvl10
	,zbw_hier_parent_lvl10
	,zbw_hier_description_lvl1
	,zbw_hier_parent_lvl1
	,zbw_hier_description_lvl2
	,zbw_hier_parent_lvl2
	,zbw_hier_description_lvl3
	,zbw_hier_parent_lvl3
	,zbw_hier_description_lvl4
	,zbw_hier_parent_lvl4
	,zbw_hier_description_lvl5
	,zbw_hier_parent_lvl5
	,zbw_hier_description_lvl6
	,zbw_hier_parent_lvl6
	,zbw_hier_description_lvl7
	,zbw_hier_parent_lvl7
	,zbw_hier_description_lvl8
	,zbw_hier_parent_lvl8
	,zbw_hier_description_lvl9
	,zbw_hier_parent_lvl9
	,mattypeforglbrep
	,glbproductgroup
	,glbabcindicator
	,oldmatnumber
	,baseunitofmeasure
	,totalshelflife
	,minremainingshelflife
	,crossplantmatstatus
	,materialtype
	,generalitemcatgroup
	,producthierarchy
	,batchmanreqind
	,aporelevance
	,mattypeforglbrepdescr
	,crossplantmatstatusdescr
	,materialtypedescr
	,generalitemcatgroupdescr
	,producthierarchydescr
	,aporelevancedesc
	,primary_production_location
	,global_demand_planner
	,global_supply_planner
	,primary_production_location_name
	,ppu
	,act_conv
	,articlenumber
	,partdescription
	,supplychainplaningppi
	,maingroup
	,uppergroup
	,articlegroup
	,internationalarticle
	,maingroupdesc
	,uppergroupdesc
	,articlegroupdesc
	,internationalarticledesc
	,global_demand_planner_desc
	,global_supply_planner_desc
        ,IndicatorSalesCatalogue
	,IndicatorSalesCatalogueDesc
,PRM_Product_manager_code
,PRM_Product_manager_desc
,PRP_Price_responsible_code
,PRP_Price_responsible_desc
,materialcategory
)
SELECT
		 dim_mdg_partid
		,dw_insert_date
		,dw_update_date
		,partnumber
		,product_hierarchy
		,global_operational_hierarchy
		,planned_product_hierarchy
		,product_hierarchy_phoenix
		,price_hierarchy
		,product_hierarchy_description
		,planned_product_hierarchy_description
		,projectsourceid
		,global_operational_hierarchy_description
		,product_hierarchy_phoenix_description
		,price_hierarchy_description
		,zbw_description
		,zbw_division
		,zbw_businessunit
		,zbw_businessfield
		,zbw_businessline
		,zbw_productgroup
		,zbw_maingroup
		,zbw_uppergroup
		,zbw_articlegroup
		,zbw_interntaionalarticle
		,zbw_valuationclass
		,zbw_producttype
		,zbw_lsku
		,zbw_hier_description_lvl10
		,zbw_hier_parent_lvl10
		,zbw_hier_description_lvl1
		,zbw_hier_parent_lvl1
		,zbw_hier_description_lvl2
		,zbw_hier_parent_lvl2
		,zbw_hier_description_lvl3
		,zbw_hier_parent_lvl3
		,zbw_hier_description_lvl4
		,zbw_hier_parent_lvl4
		,zbw_hier_description_lvl5
		,zbw_hier_parent_lvl5
		,zbw_hier_description_lvl6
		,zbw_hier_parent_lvl6
		,zbw_hier_description_lvl7
		,zbw_hier_parent_lvl7
		,zbw_hier_description_lvl8
		,zbw_hier_parent_lvl8
		,zbw_hier_description_lvl9
		,zbw_hier_parent_lvl9
		,mattypeforglbrep
		,glbproductgroup
		,glbabcindicator
		,oldmatnumber
		,baseunitofmeasure
		,totalshelflife
		,minremainingshelflife
		,crossplantmatstatus
		,materialtype
		,generalitemcatgroup
		,producthierarchy
		,batchmanreqind
		,aporelevance
		,mattypeforglbrepdescr
		,crossplantmatstatusdescr
		,materialtypedescr
		,generalitemcatgroupdescr
		,producthierarchydescr
		,aporelevancedesc
		,primary_production_location
		,global_demand_planner
		,global_supply_planner
		,primary_production_location_name
		,0
		,0
		,'Not Set'
		,partdescription
		,supplychainplaningppi
		,maingroup
	    ,uppergroup
	    ,articlegroup
	    ,internationalarticle
	    ,maingroupdesc
	    ,uppergroupdesc
	    ,articlegroupdesc
	    ,internationalarticledesc
	    ,global_demand_planner_desc
	    ,global_supply_planner_desc
	    ,IndicatorSalesCatalogue
	,IndicatorSalesCatalogueDesc
,PRM_Product_manager_code
,PRM_Product_manager_desc
,PRP_Price_responsible_code
,PRP_Price_responsible_desc
,materialcategory
    FROM emdoracle4ae.dim_ora_mdg_product
    WHERE dim_mdg_partid <> 1;


update emd586.dim_mdg_part
set PRODUCT_HIERARCHY_DESCRIPTION2 = decode(producthierarchy,'Not Set',producthierarchy,left(producthierarchy,3))
where PRODUCT_HIERARCHY_DESCRIPTION2 <> decode(producthierarchy,'Not Set',producthierarchy,left(producthierarchy,3))
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.dim_mdg_part
set PrimaryProductionLocation = primary_production_location || ' - ' || primary_production_location_name
where PrimaryProductionLocation <> primary_production_location || ' - ' || primary_production_location_name
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

UPDATE emd586.dim_mdg_part dp
SET   dp.MDGPartNumber_NoLeadZero = ifnull(case when length(dp.partnumber) = 18 and dp.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from dp.partnumber) else dp.partnumber end,'Not Set')
where   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );
