
update dim_ora_cluster
set clust= case when clust= 'APAC Region' then 'Asia' 
            when clust  in ('Not Set','Not Assigned','Lab Supply') then 'Others' else clust  end
from dim_ora_cluster;


DELETE FROM emd586.dim_cluster WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_cluster
(
	 dim_clusterid 
	,primary_manufacturing_site 
	,clust
	,projectsourceid 
	,dw_insert_date 
	,dw_update_date 
)
SELECT 
		 dim_ora_clusterid 
		,primary_manufacturing_site 
		,clust
		,projectsourceid 
		,dw_insert_date 
		,dw_update_date 
FROM emdoracle4ae.dim_ora_cluster d
WHERE dim_ora_clusterid <> 1;



