DELETE FROM emd586.dim_documentcategory WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_documentcategory
(
     documentcategory
     ,description
    ,dw_insert_date
    ,dw_update_date
    ,projectsourceid
	,dim_documentcategoryid

)
SELECT 
         SUBSTR(description,1,20)
         ,name
		 ,d.dw_insert_date
		,d.dw_update_date
		,p.dim_projectsourceid
	    ,dim_ora_xacttypeid + ifnull(p.dim_projectsourceid * p.multiplier ,0) dim_ora_order_header_typeid 

	
FROM emdoracle4ae.dim_ora_xacttype d, emdoracle4ae.dim_projectsource p;