
DELETE FROM emd586.dim_storagelocation WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_storagelocation
(
	 dim_storagelocationid
	,plant
	,description
	,projectsourceid
	,dw_insert_date
	,dw_update_date
	,rowstartdate
	,rowenddate
	,rowiscurrent
	,rowchangereason
	,locationcode

)
SELECT
		 d.dim_ora_mtl_sec_inventoryid + ifnull( s.dim_projectsourceid * s.multiplier,1) dim_ora_mtl_sec_inventoryid -- THE IMPORTANT STEP TO TAKE THE EXISTING KEY AND TO MULTIPLY	
		,d.source_organization_id
		,SUBSTR(d.secondary_inventory_name,1,16) description
		,s.dim_projectsourceid
		,d.dw_insert_date
		,d.dw_update_date
		,d.rowstartdate
		,d.rowenddate
		,d.rowiscurrent
		,d.rowchangereason
		,SUBSTR(d.secondary_inventory_name,1,16)
FROM emdoracle4ae.dim_ora_mtl_sec_inventory d,emdoracle4ae.dim_projectsource s;