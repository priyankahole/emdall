/*
set session authorization emd586
set session authorization emdoracle4ae

GRANT INSERT,DELETE ON emd586.dim_bwhierarchycountry TO emdoracle4ae
GRANT SELECT ON emdoracle4ae.dim_ora_bwhierarchycountry TO emd586 */


DELETE FROM emd586.dim_bwhierarchycountry WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

INSERT INTO emd586.dim_bwhierarchycountry (
	 dim_bwhierarchycountryid
	,internalhierarchyname
	,internalhierarchyid
	,internalidno
	,nodehierarchynamelvl7
	,nodehierarchynamelvl6
	,nodehierarchynamelvl5
	,nodehierarchynamelvl4
	,nodehierarchynamelvl3
	,nodehierarchynamelvl2
	,nodehierarchynamelvl1
	,nodehierarchydesclvl7
	,nodehierarchydesclvl6
	,nodehierarchydesclvl5
	,nodehierarchydesclvl4
	,nodehierarchydesclvl3
	,nodehierarchydesclvl2
	,nodehierarchydesclvl1
	,dw_insert_date
	,dw_update_date
	,projectsourceid
)
SELECT 
     s.dim_ora_bwhierarchycountryid
	,s.internalhierarchyname
	,s.internalhierarchyid
	,s.internalidno
	,s.nodehierarchynamelvl7
	,s.nodehierarchynamelvl6
	,s.nodehierarchynamelvl5
	,s.nodehierarchynamelvl4
	,s.nodehierarchynamelvl3
	,s.nodehierarchynamelvl2
	,s.nodehierarchynamelvl1
	,s.nodehierarchydesclvl7
	,s.nodehierarchydesclvl6
	,s.nodehierarchydesclvl5
	,s.nodehierarchydesclvl4
	,s.nodehierarchydesclvl3
	,s.nodehierarchydesclvl2
	,s.nodehierarchydesclvl1
	,s.dw_insert_date
	,s.dw_update_date
	,d.dim_projectsourceid
FROM emdoracle4ae.dim_ora_bwhierarchycountry s, emdoracle4ae.dim_projectsource d
WHERE S.dim_ora_bwhierarchycountryid <> 1;