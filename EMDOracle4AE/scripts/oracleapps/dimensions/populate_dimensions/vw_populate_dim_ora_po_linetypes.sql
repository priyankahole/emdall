/*insert default row*/
INSERT INTO dim_ora_po_linetypes
(
dim_ora_po_linetypesid,
receive_close_tolerance,program_update_date,outside_operation_flag,attribute15,attribute14,attribute13,
attribute12,attribute11,attribute10,attribute9,attribute8,attribute7,attribute6,attribute5,attribute4,attribute3,attribute2,
attribute1,attribute_category,inactive_date,receiving_flag,unit_price,unit_of_measure,category_id,description,created_by,
creation_date,last_update_login,order_type_lookup_code,line_type,last_updated_by,last_update_date,line_type_id,matching_basis,purchase_basis,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_po_linetypesid,
0 RECEIVE_CLOSE_TOLERANCE,
timestamp '1001-01-01 00:00:00.000000' PROGRAM_UPDATE_DATE,
'Not Set' OUTSIDE_OPERATION_FLAG,
'Not Set' ATTRIBUTE15,
'Not Set' ATTRIBUTE14,
'Not Set' ATTRIBUTE13,
'Not Set' ATTRIBUTE12,
'Not Set' ATTRIBUTE11,
'Not Set' ATTRIBUTE10,
'Not Set' ATTRIBUTE9,
'Not Set' ATTRIBUTE8,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE4,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE1,
'Not Set' ATTRIBUTE_CATEGORY,
timestamp '1001-01-01 00:00:00.000000' INACTIVE_DATE,
'Not Set' RECEIVING_FLAG,
0 UNIT_PRICE,
'Not Set' UNIT_OF_MEASURE,
0 CATEGORY_ID,
'Not Set' DESCRIPTION,
0 CREATED_BY,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
0 LAST_UPDATE_LOGIN,
'Not Set' ORDER_TYPE_LOOKUP_CODE,
'Not Set' LINE_TYPE,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LINE_TYPE_ID,
'Not Set' MATCHING_BASIS,
'Not Set' PURCHASE_BASIS,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_po_linetypes D
ON T.dim_ora_po_linetypesid = D.dim_ora_po_linetypesid
WHERE D.dim_ora_po_linetypesid IS NULL;

/*initialize NUMBER_FOUNTAIN_dim_ora_po_linetypes*/
delete from NUMBER_FOUNTAIN_dim_ora_po_linetypes where table_name = 'dim_ora_po_linetypes';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_po_linetypes
SELECT 'dim_ora_po_linetypes',IFNULL(MAX(dim_ora_po_linetypesid),0)
FROM dim_ora_po_linetypes;

/*update dimension rows*/
UPDATE dim_ora_po_linetypes T
SET
T.RECEIVE_CLOSE_TOLERANCE = ifnull(S.RECEIVE_CLOSE_TOLERANCE, 0),
T.PROGRAM_UPDATE_DATE = ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.OUTSIDE_OPERATION_FLAG = ifnull(S.OUTSIDE_OPERATION_FLAG, 'Not Set'),
T.ATTRIBUTE15 = ifnull(S.ATTRIBUTE15, 'Not Set'),
T.ATTRIBUTE14 = ifnull(S.ATTRIBUTE14, 'Not Set'),
T.ATTRIBUTE13 = ifnull(S.ATTRIBUTE13, 'Not Set'),
T.ATTRIBUTE12 = ifnull(S.ATTRIBUTE12, 'Not Set'),
T.ATTRIBUTE11 = ifnull(S.ATTRIBUTE11, 'Not Set'),
T.ATTRIBUTE10 = ifnull(S.ATTRIBUTE10, 'Not Set'),
T.ATTRIBUTE9 = ifnull(S.ATTRIBUTE9, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set'),
T.INACTIVE_DATE = ifnull(S.INACTIVE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.RECEIVING_FLAG = ifnull(S.RECEIVING_FLAG, 'Not Set'),
T.UNIT_PRICE = ifnull(S.UNIT_PRICE, 0),
T.UNIT_OF_MEASURE = ifnull(S.UNIT_OF_MEASURE, 'Not Set'),
T.CATEGORY_ID = ifnull(S.CATEGORY_ID, 0),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.ORDER_TYPE_LOOKUP_CODE = ifnull(S.ORDER_TYPE_LOOKUP_CODE, 'Not Set'),
T.LINE_TYPE = ifnull(S.LINE_TYPE, 'Not Set'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LINE_TYPE_ID = S.LINE_TYPE_ID,
T.MATCHING_BASIS = ifnull(S.MATCHING_BASIS, 'Not Set'),
T.PURCHASE_BASIS = ifnull(S.PURCHASE_BASIS, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
 FROM dim_ora_po_linetypes T, ORA_PURCH_LTYPE S
WHERE T.KEY_ID = S.LINE_TYPE_ID;

/*insert new rows*/
INSERT INTO dim_ora_po_linetypes
(
dim_ora_po_linetypesid,
receive_close_tolerance,program_update_date,outside_operation_flag,attribute15,attribute14,attribute13,
attribute12,attribute11,attribute10,attribute9,attribute8,attribute7,attribute6,attribute5,attribute4,attribute3,attribute2,
attribute1,attribute_category,inactive_date,receiving_flag,unit_price,unit_of_measure,category_id,description,created_by,
creation_date,last_update_login,order_type_lookup_code,line_type,last_updated_by,last_update_date,line_type_id,matching_basis,purchase_basis,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_po_linetypes WHERE TABLE_NAME = 'dim_ora_po_linetypes' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_po_linetypesid,
ifnull(S.RECEIVE_CLOSE_TOLERANCE, 0) RECEIVE_CLOSE_TOLERANCE,
ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') PROGRAM_UPDATE_DATE,
ifnull(S.OUTSIDE_OPERATION_FLAG, 'Not Set') OUTSIDE_OPERATION_FLAG,
ifnull(S.ATTRIBUTE15, 'Not Set') ATTRIBUTE15,
ifnull(S.ATTRIBUTE14, 'Not Set') ATTRIBUTE14,
ifnull(S.ATTRIBUTE13, 'Not Set') ATTRIBUTE13,
ifnull(S.ATTRIBUTE12, 'Not Set') ATTRIBUTE12,
ifnull(S.ATTRIBUTE11, 'Not Set') ATTRIBUTE11,
ifnull(S.ATTRIBUTE10, 'Not Set') ATTRIBUTE10,
ifnull(S.ATTRIBUTE9, 'Not Set') ATTRIBUTE9,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set') ATTRIBUTE_CATEGORY,
ifnull(S.INACTIVE_DATE, timestamp '1001-01-01 00:00:00.000000') INACTIVE_DATE,
ifnull(S.RECEIVING_FLAG, 'Not Set') RECEIVING_FLAG,
ifnull(S.UNIT_PRICE, 0) UNIT_PRICE,
ifnull(S.UNIT_OF_MEASURE, 'Not Set') UNIT_OF_MEASURE,
ifnull(S.CATEGORY_ID, 0) CATEGORY_ID,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.ORDER_TYPE_LOOKUP_CODE, 'Not Set') ORDER_TYPE_LOOKUP_CODE,
ifnull(S.LINE_TYPE, 'Not Set') LINE_TYPE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
S.LINE_TYPE_ID,
ifnull(S.MATCHING_BASIS, 'Not Set') MATCHING_BASIS,
ifnull(S.PURCHASE_BASIS, 'Not Set') PURCHASE_BASIS,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.LINE_TYPE_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_PURCH_LTYPE S LEFT JOIN dim_ora_po_linetypes D ON
D.KEY_ID = S.LINE_TYPE_ID
WHERE
D.KEY_ID is null;
