/* Remove duplicates from ORA_MTL_INVENTORY_PROD, by FPOPESCU, on 22-12-2015. */


TRUNCATE TABLE TMP_DIST_ORA_MTL_INVENTORY_PROD;
INSERT INTO TMP_DIST_ORA_MTL_INVENTORY_PROD
SELECT DISTINCT * FROM ORA_MTL_INVENTORY_PROD;

TRUNCATE TABLE ORA_MTL_INVENTORY_PROD;
INSERT INTO ORA_MTL_INVENTORY_PROD SELECT * FROM TMP_DIST_ORA_MTL_INVENTORY_PROD;


/* END Remove duplicates from ORA_MTL_INVENTORY_PROD, by FPOPESCU, on 22-12-2015.*/

/*insert default row*/
INSERT INTO dim_ora_invproduct
(
dim_ora_invproductid,
organization_id,last_update_date,last_updated_by,
creation_date,created_by,carrying_cost,description,item_status,make_buy,list_price_per_unit,segment1,segment2,segment3,segment4,segment5,segment6,
segment7,segment8,segment9,segment10,segment11,segment12,segment13,segment14,segment15,segment16,segment17,segment18,segment19,segment20,
minimum_order_quantity,maximum_order_quantity,std_lot_size,planning_time_fence_days,full_lead_time,
postprocessing_lead_time,preprocessing_lead_time,order_cost,shelf_life_days,
primary_uom_code,unit_of_issue,source_subinventory,inventory_item_id,safety_stock_quantity,
planner_code,planner_description,planner_disable_date,
cum_manufacturing_lead_time,mrp_planning_code,employee_number,full_name,enabled_flag,start_date_active,end_date_active,invoiceable_item_flag,invoice_enabled_flag,item_type,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_invproductid,
0 ORGANIZATION_ID,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 CARRYING_COST,
'Not Set' DESCRIPTION,
'Not Set' ITEM_STATUS,
0 MAKE_BUY,
0 LIST_PRICE_PER_UNIT,
'Not Set' SEGMENT1,
'Not Set' SEGMENT2,
'Not Set' SEGMENT3,
'Not Set' SEGMENT4,
'Not Set' SEGMENT5,
'Not Set' SEGMENT6,
'Not Set' SEGMENT7,
'Not Set' SEGMENT8,
'Not Set' SEGMENT9,
'Not Set' SEGMENT10,
'Not Set' SEGMENT11,
'Not Set' SEGMENT12,
'Not Set' SEGMENT13,
'Not Set' SEGMENT14,
'Not Set' SEGMENT15,
'Not Set' SEGMENT16,
'Not Set' SEGMENT17,
'Not Set' SEGMENT18,
'Not Set' SEGMENT19,
'Not Set' SEGMENT20,
0 MINIMUM_ORDER_QUANTITY,
0 MAXIMUM_ORDER_QUANTITY,
0 STD_LOT_SIZE,
0 PLANNING_TIME_FENCE_DAYS,
0 FULL_LEAD_TIME,
0 POSTPROCESSING_LEAD_TIME,
0 PREPROCESSING_LEAD_TIME,
0 ORDER_COST,
0 SHELF_LIFE_DAYS,
'Not Set' PRIMARY_UOM_CODE,
'Not Set' UNIT_OF_ISSUE,
'Not Set' SOURCE_SUBINVENTORY,
0 INVENTORY_ITEM_ID,
0 SAFETY_STOCK_QUANTITY,
'Not Set' PLANNER_CODE,
'Not Set' planner_description,
timestamp '1001-01-01 00:00:00.000000' planner_disable_date,
0 CUM_MANUFACTURING_LEAD_TIME,
0 MRP_PLANNING_CODE,
'Not Set' EMPLOYEE_NUMBER,
'Not Set' FULL_NAME,
'Not Set' ENABLED_FLAG,
timestamp '1001-01-01 00:00:00.000000' START_DATE_ACTIVE,
timestamp '1001-01-01 00:00:00.000000' END_DATE_ACTIVE,
'Not Set' INVOICEABLE_ITEM_FLAG,
'Not Set' INVOICE_ENABLED_FLAG,
'Not Set' item_type,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_invproduct D
ON T.dim_ora_invproductid = D.dim_ora_invproductid
WHERE D.dim_ora_invproductid is null;

/*initialize NUMBER_FOUNTAIN_dim_ora_invproduct*/
delete from NUMBER_FOUNTAIN_dim_ora_invproduct where table_name = 'dim_ora_invproduct';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_invproduct
SELECT 'dim_ora_invproduct',IFNULL(MAX(dim_ora_invproductid),0)
FROM dim_ora_invproduct;

/*update dimension rows*/
UPDATE dim_ora_invproduct T
SET
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.ITEM_STATUS = ifnull(S.ITEM_STATUS, 'Not Set'),
T.MAKE_BUY = ifnull(S.MAKE_BUY, 0),
T.SEGMENT1 = ifnull(S.SEGMENT1, 'Not Set'),
T.SEGMENT2 = ifnull(S.SEGMENT2, 'Not Set'),
T.SEGMENT3 = ifnull(S.SEGMENT3, 'Not Set'),
T.SEGMENT4 = ifnull(S.SEGMENT4, 'Not Set'),
T.SEGMENT5 = ifnull(S.SEGMENT5, 'Not Set'),
T.SEGMENT6 = ifnull(S.SEGMENT6, 'Not Set'),
T.SEGMENT7 = ifnull(S.SEGMENT7, 'Not Set'),
T.SEGMENT8 = ifnull(S.SEGMENT8, 'Not Set'),
T.SEGMENT9 = ifnull(S.SEGMENT9, 'Not Set'),
T.SEGMENT10 = ifnull(S.SEGMENT10, 'Not Set'),
T.SEGMENT11 = ifnull(S.SEGMENT11, 'Not Set'),
T.SEGMENT12 = ifnull(S.SEGMENT12, 'Not Set'),
T.SEGMENT13 = ifnull(S.SEGMENT13, 'Not Set'),
T.SEGMENT14 = ifnull(S.SEGMENT14, 'Not Set'),
T.SEGMENT15 = ifnull(S.SEGMENT15, 'Not Set'),
T.SEGMENT16 = ifnull(S.SEGMENT16, 'Not Set'),
T.SEGMENT17 = ifnull(S.SEGMENT17, 'Not Set'),
T.SEGMENT18 = ifnull(S.SEGMENT18, 'Not Set'),
T.SEGMENT19 = ifnull(S.SEGMENT19, 'Not Set'),
T.SEGMENT20 = ifnull(S.SEGMENT20, 'Not Set'),
T.MINIMUM_ORDER_QUANTITY = ifnull(S.MINIMUM_ORDER_QUANTITY, 0),
T.MAXIMUM_ORDER_QUANTITY = ifnull(S.MAXIMUM_ORDER_QUANTITY, 0),
T.STD_LOT_SIZE = ifnull(S.STD_LOT_SIZE, 0),
T.PLANNING_TIME_FENCE_DAYS = ifnull(S.PLANNING_TIME_FENCE_DAYS, 0),
T.FULL_LEAD_TIME = ifnull(S.FULL_LEAD_TIME, 0),
T.POSTPROCESSING_LEAD_TIME = ifnull(S.POSTPROCESSING_LEAD_TIME, 0),
T.PREPROCESSING_LEAD_TIME = ifnull(S.PREPROCESSING_LEAD_TIME, 0),
T.ORDER_COST = ifnull(S.ORDER_COST, 0),
T.SHELF_LIFE_DAYS = ifnull(S.SHELF_LIFE_DAYS, 0),
T.PRIMARY_UOM_CODE = ifnull(S.PRIMARY_UOM_CODE, 'Not Set'),
T.UNIT_OF_ISSUE = ifnull(S.UNIT_OF_ISSUE, 'Not Set'),
T.SOURCE_SUBINVENTORY = ifnull(S.SOURCE_SUBINVENTORY, 'Not Set'),
T.SAFETY_STOCK_QUANTITY = ifnull(S.SAFETY_STOCK_QUANTITY, 0),
T.PLANNER_CODE = ifnull(S.PLANNER_CODE, 'Not Set'),
T.CUM_MANUFACTURING_LEAD_TIME = ifnull(S.CUM_MANUFACTURING_LEAD_TIME, 0),
T.MRP_PLANNING_CODE = ifnull(S.MRP_PLANNING_CODE, 0),
T.EMPLOYEE_NUMBER = ifnull(S.EMPLOYEE_NUMBER, 'Not Set'),
T.FULL_NAME = ifnull(S.FULL_NAME, 'Not Set'),
T.ENABLED_FLAG = ifnull(S.ENABLED_FLAG, 'Not Set'),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000'),
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000'),
T.INVOICEABLE_ITEM_FLAG = ifnull(S.INVOICEABLE_ITEM_FLAG, 'Not Set'),
T.INVOICE_ENABLED_FLAG = ifnull(S.INVOICE_ENABLED_FLAG, 'Not Set'),
T.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID,
T.ORGANIZATION_ID = S.ORGANIZATION_ID,
T.CARRYING_COST = ifnull(S.CARRYING_COST,0),
T.LIST_PRICE_PER_UNIT = ifnull(S.LIST_PRICE_PER_UNIT,0),
T.item_type = ifnull(S.item_type, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_invproduct T
	INNER JOIN ORA_MTL_INVENTORY_PROD S ON CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'|| CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = T.KEY_ID
;


/*insert new rows*/
INSERT INTO dim_ora_invproduct
(
dim_ora_invproductid,
organization_id,last_update_date,last_updated_by,
creation_date,created_by,carrying_cost,description,item_status,make_buy,list_price_per_unit,segment1,segment2,segment3,segment4,segment5,segment6,
segment7,segment8,segment9,segment10,segment11,segment12,segment13,segment14,segment15,segment16,segment17,segment18,segment19,segment20,
minimum_order_quantity,maximum_order_quantity,std_lot_size,planning_time_fence_days,full_lead_time,
postprocessing_lead_time,preprocessing_lead_time,order_cost,shelf_life_days,
primary_uom_code,unit_of_issue,source_subinventory,inventory_item_id,safety_stock_quantity,
planner_code,planner_description,planner_disable_date,
cum_manufacturing_lead_time,mrp_planning_code,employee_number,full_name,enabled_flag,start_date_active,end_date_active,invoiceable_item_flag,invoice_enabled_flag,item_type,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_invproduct WHERE TABLE_NAME = 'dim_ora_invproduct' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_invproductid,
S.ORGANIZATION_ID ORGANIZATION_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.CARRYING_COST, 0) CARRYING_COST,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.ITEM_STATUS, 'Not Set') ITEM_STATUS,
ifnull(S.MAKE_BUY, 0) MAKE_BUY,
ifnull(S.LIST_PRICE_PER_UNIT, 0) LIST_PRICE_PER_UNIT,
ifnull(S.SEGMENT1, 'Not Set') SEGMENT1,
ifnull(S.SEGMENT2, 'Not Set') SEGMENT2,
ifnull(S.SEGMENT3, 'Not Set') SEGMENT3,
ifnull(S.SEGMENT4, 'Not Set') SEGMENT4,
ifnull(S.SEGMENT5, 'Not Set') SEGMENT5,
ifnull(S.SEGMENT6, 'Not Set') SEGMENT6,
ifnull(S.SEGMENT7, 'Not Set') SEGMENT7,
ifnull(S.SEGMENT8, 'Not Set') SEGMENT8,
ifnull(S.SEGMENT9, 'Not Set') SEGMENT9,
ifnull(S.SEGMENT10, 'Not Set') SEGMENT10,
ifnull(S.SEGMENT11, 'Not Set') SEGMENT11,
ifnull(S.SEGMENT12, 'Not Set') SEGMENT12,
ifnull(S.SEGMENT13, 'Not Set') SEGMENT13,
ifnull(S.SEGMENT14, 'Not Set') SEGMENT14,
ifnull(S.SEGMENT15, 'Not Set') SEGMENT15,
ifnull(S.SEGMENT16, 'Not Set') SEGMENT16,
ifnull(S.SEGMENT17, 'Not Set') SEGMENT17,
ifnull(S.SEGMENT18, 'Not Set') SEGMENT18,
ifnull(S.SEGMENT19, 'Not Set') SEGMENT19,
ifnull(S.SEGMENT20, 'Not Set') SEGMENT20,
ifnull(S.MINIMUM_ORDER_QUANTITY, 0) MINIMUM_ORDER_QUANTITY,
ifnull(S.MAXIMUM_ORDER_QUANTITY, 0) MAXIMUM_ORDER_QUANTITY,
ifnull(S.STD_LOT_SIZE, 0) STD_LOT_SIZE,
ifnull(S.PLANNING_TIME_FENCE_DAYS, 0) PLANNING_TIME_FENCE_DAYS,
ifnull(S.FULL_LEAD_TIME, 0) FULL_LEAD_TIME,
ifnull(S.POSTPROCESSING_LEAD_TIME, 0) POSTPROCESSING_LEAD_TIME,
ifnull(S.PREPROCESSING_LEAD_TIME, 0) PREPROCESSING_LEAD_TIME,
ifnull(S.ORDER_COST, 0) ORDER_COST,
ifnull(S.SHELF_LIFE_DAYS, 0) SHELF_LIFE_DAYS,
ifnull(S.PRIMARY_UOM_CODE, 'Not Set') PRIMARY_UOM_CODE,
ifnull(S.UNIT_OF_ISSUE, 'Not Set') UNIT_OF_ISSUE,
ifnull(S.SOURCE_SUBINVENTORY, 'Not Set') SOURCE_SUBINVENTORY,
S.INVENTORY_ITEM_ID INVENTORY_ITEM_ID,
ifnull(S.SAFETY_STOCK_QUANTITY, 0) SAFETY_STOCK_QUANTITY,
ifnull(S.PLANNER_CODE, 'Not Set') PLANNER_CODE,
'Not Set' planner_description,
timestamp '1001-01-01 00:00:00.000000' planner_disable_date,
ifnull(S.CUM_MANUFACTURING_LEAD_TIME, 0) CUM_MANUFACTURING_LEAD_TIME,
ifnull(S.MRP_PLANNING_CODE, 0) MRP_PLANNING_CODE,
ifnull(S.EMPLOYEE_NUMBER, 'Not Set') EMPLOYEE_NUMBER,
ifnull(S.FULL_NAME, 'Not Set') FULL_NAME,
ifnull(S.ENABLED_FLAG, 'Not Set') ENABLED_FLAG,
ifnull(S.START_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.END_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.INVOICEABLE_ITEM_FLAG, 'Not Set') INVOICEABLE_ITEM_FLAG,
ifnull(S.INVOICE_ENABLED_FLAG, 'Not Set') INVOICE_ENABLED_FLAG,
ifnull(S.item_type, 'Not Set') item_type,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'|| CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_MTL_INVENTORY_PROD S
	LEFT JOIN dim_ora_invproduct D ON D.KEY_ID = CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'|| CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID)
WHERE D.KEY_ID is null;

TRUNCATE TABLE TMP_PROP;
INSERT INTO TMP_PROP
SELECT property_value,property from systemproperty where property = 'PROD.MASTER_ORGANIZATION_ID' or property = 'PROD.CATEGORY_SET_ID';


/*UPDATE safety_stock_quantity*/

TRUNCATE TABLE ora_mtl_safety_stocks_tmp;

INSERT INTO ora_mtl_safety_stocks_tmp
   SELECT organization_id,
          inventory_item_id,
          max(EFFECTIVITY_DATE) EFFECTIVITY_DATE
     FROM ora_mtl_safety_stocks
   GROUP BY organization_id, inventory_item_id;

UPDATE dim_ora_invproduct t
SET t.safety_stock_quantity = IFNULL(ss.safety_stock_quantity, 0)
FROM dim_ora_invproduct t
	INNER JOIN ora_mtl_safety_stocks_tmp s ON ( s.organization_id = t.organization_id
  						AND s.inventory_item_id  = t.inventory_item_id )
	INNER JOIN ora_mtl_safety_stocks ss ON (ss.organization_id = s.organization_id
										AND ss.inventory_item_id  = s.inventory_item_id
										AND ss.EFFECTIVITY_DATE = s.EFFECTIVITY_DATE)
WHERE
t.safety_stock_quantity <> IFNULL(ss.safety_stock_quantity, 0);

/*update product category attributes*/
UPDATE dim_ora_invproduct T
SET
T.SEGMENT16 = ifnull(S.SEGMENT1, 'Not Set'),
T.SEGMENT17 = ifnull(S.SEGMENT2, 'Not Set'),
T.SEGMENT18 = ifnull(S.SEGMENT3, 'Not Set'),
T.SEGMENT19 = ifnull(S.SEGMENT4, 'Not Set'),
t.org_description = ifnull(S.org_description, 'Not Set')
FROM dim_ora_invproduct T JOIN ORA_MTL_PRODUCT_CATEGORY S
ON
T.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID
JOIN TMP_PROP morg
on S.ORGANIZATION_ID = morg.property_value
and morg.property = 'PROD.MASTER_ORGANIZATION_ID'
JOIN TMP_PROP catset
ON S.CATEGORY_SET_ID = catset.property_value
and catset.property = 'PROD.CATEGORY_SET_ID'
WHERE
S.ORGANIZATION_ID =  morg.property_value
AND S.CATEGORY_SET_ID = catset.property_value
AND S.INVENTORY_ITEM_ID  = T.INVENTORY_ITEM_ID;

UPDATE dim_ora_invproduct T
SET
T.SEGMENT16 = ifnull(S.SEGMENT1, 'Not Set'),
T.SEGMENT17 = ifnull(S.SEGMENT2, 'Not Set'),
T.SEGMENT18 = ifnull(S.SEGMENT3, 'Not Set'),
T.SEGMENT19 = ifnull(S.SEGMENT4, 'Not Set'),
t.org_description = ifnull(S.org_description, 'Not Set')
FROM dim_ora_invproduct T JOIN ORA_MTL_PRODUCT_CATEGORY S
ON
T.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND
T.ORGANIZATION_ID = S.ORGANIZATION_ID
JOIN TMP_PROP morg
on S.ORGANIZATION_ID = morg.property_value
and morg.property = 'PROD.MASTER_ORGANIZATION_ID'
JOIN TMP_PROP catset
ON S.CATEGORY_SET_ID = catset.property_value
and catset.property = 'PROD.CATEGORY_SET_ID'
WHERE
S.ORGANIZATION_ID =  morg.property_value
AND S.CATEGORY_SET_ID = catset.property_value
AND S.INVENTORY_ITEM_ID  = T.INVENTORY_ITEM_ID
AND S.ORGANIZATION_ID  = T.ORGANIZATION_ID;

UPDATE dim_ora_invproduct d_invp
SET d_invp.mdm_globalid = IFNULL(d_stg.mdm_globalid,'Not Set')
FROM ora_mtl_product_mdmglobalid d_stg,dim_ora_invproduct d_invp
  WHERE d_invp.inventory_item_id = d_stg.inventory_item_id
    AND d_invp.organization_id = d_stg.organization_id
    AND d_invp.mdm_globalid <> IFNULL(d_stg.mdm_globalid,'Not Set');

/*Adding the lowerhierarchy and upperhierachy codes */
/*
UPDATE dim_ora_invproduct d_invp
SET d_invp.producthierarchy = IFNULL(d_stg.gph_code,'Not Set')
   ,dw_update_date = CURRENT_DATE
FROM dim_ora_invproduct d_invp, ora_mtl_product_lowerhierarchy d_stg
WHERE d_invp.inventory_item_id = d_stg.inventory_item_id
  AND d_invp.producthierarchy <> IFNULL(d_stg.gph_code,'Not Set')
  */

UPDATE dim_ora_invproduct d_invp
SET d_invp.productgroupsbu = IFNULL(d_stg.product_group,'Not Set')
    ,dw_update_date = CURRENT_DATE
FROM dim_ora_invproduct d_invp, ora_mtl_product_lowerhierarchy d_stg
WHERE d_invp.inventory_item_id = d_stg.inventory_item_id
  AND d_invp.productgroupsbu <> IFNULL(d_stg.product_group,'Not Set');

/* 17 may 2016 Cornelia add dd_PlannerName */
update dim_ora_invproduct i
set i.PlannerName = m.description
from dim_ora_invproduct i, dim_ora_mtl_planners m
where i.organization_id = m.organization_id
AND i.planner_code = m.planner_code
and i.PlannerName <> m.description;


/*Added the segment1 from category_set_id = 1 for tradesalesflag from Sales by Victor on 27thMay2016*/
UPDATE dim_ora_invproduct d_invp
SET  d_invp.segment1_catset1 = IFNULL(d_stg.segment1,'Not Set')
    ,dw_update_date = CURRENT_DATE
FROM dim_ora_invproduct d_invp, ora_mtl_prd_item_category d_stg
WHERE d_invp.organization_id =  d_stg.organization_id
  AND d_invp.inventory_item_id  = d_stg.inventory_item_id
  AND d_invp.segment1_catset1 <> IFNULL(d_stg.segment1,'Not Set');

/*Start calculate ABC_INDICATOR @CatalinM 02.06.2016*/

DROP TABLE IF EXISTS tmp_ora_scm_code;
CREATE TABLE tmp_ora_scm_code AS
SELECT
     b.organization_id,c.segment1,a.geography,b.inventory_item_id,C.ATTRIBUTE1,C.ATTRIBUTE2,
	CASE
	WHEN LENGTH(c.segment1) != 2 and COALESCE(a.geography,'XX') = 'JAPAN' THEN
			CASE WHEN INSTR(c.segment1,'J')!= 0 THEN 'JA' ELSE 'BU'
			END
	WHEN length(c.segment1) != 2 and COALESCE(a.geography,'XX') IN ('ASIA','EU') THEN
			CASE WHEN ((INSTR(c.segment1,'F') != 0)  OR
					   (INSTR(c.segment1,'EI') != 0)  OR
					   (INSTR(c.segment1,'BP') != 0)  OR
					   (INSTR(c.segment1,'UK') != 0) )	THEN 'FR' ELSE 'BU'
			END
	WHEN LENGTH(c.segment1) != 2 AND COALESCE(a.geography,'XX') IN ('SA','NA') THEN
			 CASE WHEN ( (INSTR(c.segment1,'BU') != 0)  OR
						 (INSTR(c.segment1,'RQ') != 0)  OR
						 (LENGTH(c.segment1) = 3 AND (INSTR(c.segment1,'B') != 0) ) ) THEN 'BU' ELSE 'FR'
			 END
	ELSE CASE WHEN LENGTH(c.segment1) != 2 THEN 'BU' ELSE IFNULL(c.segment1,'BU') END
	END scm_code
FROM dim_ora_invproduct b
LEFT OUTER JOIN ora_xx_le_sob_operating_unit a ON a.org_id = b.organization_id
LEFT OUTER JOIN ora_mtl_product_category c ON c.organization_id = b.organization_id AND c.inventory_item_id = b.inventory_item_id;

/*update attribute1*/

UPDATE dim_ora_invproduct a
SET a.attribute1_mtl_prcat = IFNULL(mtl.attribute1,'Not Set')
FROM dim_ora_invproduct a , ora_mtl_product_category mtl
WHERE a.organization_id =  mtl.organization_id
AND a.inventory_item_id  = mtl.inventory_item_id
AND a.attribute1_mtl_prcat <> IFNULL(mtl.attribute1,'Not Set');

/*Update attribute2*/

UPDATE dim_ora_invproduct a
SET a.attribute2_mtl_prcat = IFNULL(mtl.attribute2,'Not Set')
FROM dim_ora_invproduct a , ora_mtl_product_category mtl
WHERE a.organization_id =  mtl.organization_id
AND a.inventory_item_id  = mtl.inventory_item_id
AND a.attribute2_mtl_prcat <> IFNULL(mtl.attribute2,'Not Set');

/*update abc indicator for JA*/

UPDATE dim_ora_invproduct a
SET a.abc_indicator = 'Yonezawa'
FROM dim_ora_invproduct a , TMP_ORA_SCM_CODE mtl
WHERE a.organization_id =  mtl.organization_id
AND a.inventory_item_id  = mtl.inventory_item_id
AND mtl.segment1 = 'JA'
AND a.abc_indicator <> 'Yonezawa';

/*update abc indicator for  ('FR','EI','BP','UK')*/

UPDATE dim_ora_invproduct a
SET a.abc_indicator = IFNULL(mtl.attribute2,'Not Set')
FROM dim_ora_invproduct a , TMP_ORA_SCM_CODE mtl
WHERE a.organization_id =  mtl.organization_id
AND a.inventory_item_id  = mtl.inventory_item_id
AND mtl.segment1 IN ('FR','EI','BP','UK')
AND mtl.organization_id = DECODE(mtl.segment1,'FR',76,'EI',195,'BP',700,'UK',380,76)
AND a.abc_indicator <> IFNULL(mtl.attribute2,'Not Set');

/*update abc indicator for segment IN ('FR','EI','BP','UK') but organization_id not IN 76,195,700,380*/

UPDATE dim_ora_invproduct a
SET a.abc_indicator = IFNULL(mtl.attribute2,'Not Set')
FROM dim_ora_invproduct a , TMP_ORA_SCM_CODE mtl
WHERE a.organization_id =  mtl.organization_id
AND a.inventory_item_id  = mtl.inventory_item_id
AND mtl.organization_id = 76
AND a.abc_indicator <> IFNULL(mtl.attribute2,'Not Set');

/*update abc indicator when others*/

UPDATE dim_ora_invproduct a
SET a.abc_indicator = mtl.attribute2
FROM dim_ora_invproduct a , TMP_ORA_SCM_CODE mtl
WHERE a.organization_id =  mtl.organization_id
AND a.inventory_item_id  = mtl.inventory_item_id
AND mtl.organization_id = 190
AND (a.abc_indicator <> IFNULL(mtl.attribute2,'Not Set') OR a.abc_indicator = 'UNK');

UPDATE dim_ora_invproduct a
SET a.abcgroup = ds.abcgroup, a.abcsegments = ds.abcsegments
FROM dim_ora_invproduct a , TMP_ORA_SCM_CODE mtl,ora_csv_abc ds
WHERE a.organization_id =  mtl.organization_id
AND a.inventory_item_id  = mtl.inventory_item_id
AND ds.abcindicator = mtl.segment1
AND a.abcgroup <> ds.abcgroup
AND a.abcsegments <> ds.abcsegments;

/* update ABCGroup and ABCSegments*/

UPDATE dim_ora_invproduct dt
SET dt.abcgroup = IFNULL(ds.abcgroup,'Not Set'), dt.abcsegments = IFNULL(ds.abcsegments,'Not Set')
FROM dim_ora_invproduct dt , TMP_ORA_SCM_CODE mtl JOIN ora_csv_abc ds ON ds.abcindicatordesc = mtl.segment1
WHERE dt.organization_id = mtl.organization_id
AND dt.inventory_item_id = mtl.inventory_item_id
AND (dt.abcgroup <> IFNULL(ds.abcgroup,'Not Set') OR dt.abcsegments <> IFNULL(ds.abcsegments,'Not Set'));
/* end update ABCGroup and ABCSegments*/

/*end calculate ABC_INDICATOR @CatalinM 02.06.2016*/
