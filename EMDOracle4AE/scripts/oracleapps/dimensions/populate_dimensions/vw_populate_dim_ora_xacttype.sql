/*insert default row*/
INSERT INTO dim_ora_xacttype
(
dim_ora_xacttypeid,
created_by,creation_date,description,language,last_updated_by,last_update_date,last_update_login,name,
program_application_id,program_id,request_id,source_lang,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_xacttypeid,
0 CREATED_BY,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
'Not Set' DESCRIPTION,
'Not Set' LANGUAGE,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATE_LOGIN,
'Not Set' NAME,
0 PROGRAM_APPLICATION_ID,
0 PROGRAM_ID,
0 REQUEST_ID,
'Not Set' SOURCE_LANG,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_xacttype D
ON T.dim_ora_xacttypeid = D.dim_ora_xacttypeid
WHERE D.dim_ora_xacttypeid is null;

/*initialize NUMBER_FOUNTAIN_dim_ora_xacttype*/
delete from NUMBER_FOUNTAIN_dim_ora_xacttype where table_name = 'dim_ora_xacttype';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_xacttype
SELECT 'dim_ora_xacttype',IFNULL(MAX(dim_ora_xacttypeid),0)
FROM dim_ora_xacttype;

/*update dimension rows*/
UPDATE dim_ora_xacttype T
SET
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.LANGUAGE = ifnull(S.LANGUAGE, 'Not Set'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.NAME = ifnull(S.NAME, 'Not Set'),
T.PROGRAM_APPLICATION_ID = ifnull(S.PROGRAM_APPLICATION_ID, 0),
T.PROGRAM_ID = ifnull(S.PROGRAM_ID, 0),
T.REQUEST_ID = ifnull(S.REQUEST_ID, 0),
T.SOURCE_LANG = ifnull(S.SOURCE_LANG, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID = 'ORA 12.1'
FROM dim_ora_xacttype T, ORA_OE_TRANSACTION_TYPES S
WHERE
S.TRANSACTION_TYPE_ID = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_xacttype
(
dim_ora_xacttypeid,
created_by,creation_date,description,language,last_updated_by,last_update_date,last_update_login,name,
program_application_id,program_id,request_id,source_lang,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_xacttype WHERE TABLE_NAME = 'dim_ora_xacttype' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_xacttypeid,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.LANGUAGE, 'Not Set') LANGUAGE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.NAME, 'Not Set') NAME,
ifnull(S.PROGRAM_APPLICATION_ID, 0) PROGRAM_APPLICATION_ID,
ifnull(S.PROGRAM_ID, 0) PROGRAM_ID,
ifnull(S.REQUEST_ID, 0) REQUEST_ID,
ifnull(S.SOURCE_LANG, 'Not Set') SOURCE_LANG,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.TRANSACTION_TYPE_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_OE_TRANSACTION_TYPES S LEFT JOIN dim_ora_xacttype D
ON D.KEY_ID = S.TRANSACTION_TYPE_ID
WHERE D.KEY_ID is null;
