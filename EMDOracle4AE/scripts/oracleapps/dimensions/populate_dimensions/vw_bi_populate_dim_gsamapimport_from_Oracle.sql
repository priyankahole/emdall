
DELETE FROM emd586.dim_gsamapimport WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_gsamapimport
(
	 dim_gsamapimportid
	,platform 
	,business_name
	,gsa_code
	,customer_number
	,gsa_name
	,projectsourceid

)
SELECT 
		 dim_ora_gsa_codeid + ifnull(p.dim_projectsourceid * p.multiplier ,0) dim_ora_gsa_codeid 
		,platform
		,business_name
	    ,gsa_code
		,customer_number
		,gsa_name
		,p.dim_projectsourceid
		
FROM emdoracle4ae.dim_ora_gsa_code d, emdoracle4ae.dim_projectsource p;

