/*insert default row*/
INSERT INTO dim_ora_cst_cost_elements
(
dim_ora_cst_cost_elementsid,
COST_ELEMENT_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,COST_ELEMENT,
DESCRIPTION,REQUEST_ID,PROGRAM_APPLICATION_ID,PROGRAM_ID,PROGRAM_UPDATE_DATE,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT T.* FROM
(SELECT
1 dim_ora_cst_cost_elementsid,
0 COST_ELEMENT_ID,
timestamp '1970-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '1970-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' COST_ELEMENT,
'Not Set' DESCRIPTION,
0 REQUEST_ID,
0 PROGRAM_APPLICATION_ID,
0 PROGRAM_ID,
timestamp '1970-01-01 00:00:00.000000' PROGRAM_UPDATE_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_cst_cost_elements D
ON T.dim_ora_cst_cost_elementsid = D.dim_ora_cst_cost_elementsid
--WHERE D.dim_ora_cst_cost_elementsid IS NULL;

/*initialize NUMBER_FOUNTAIN_dim_ora_cst_cost_elements*/
delete from NUMBER_FOUNTAIN_dim_ora_cst_cost_elements where table_name = 'dim_ora_cst_cost_elements';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_cst_cost_elements
SELECT 'dim_ora_cst_cost_elements',IFNULL(MAX(dim_ora_cst_cost_elementsid),0)
FROM dim_ora_cst_cost_elements;

/*update dimension rows*/
UPDATE dim_ora_cst_cost_elements T
SET
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.COST_ELEMENT = ifnull(S.COST_ELEMENT, 'Not Set'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.REQUEST_ID = ifnull(S.REQUEST_ID, 0),
T.PROGRAM_APPLICATION_ID = ifnull(S.PROGRAM_APPLICATION_ID, 0),
T.PROGRAM_ID = ifnull(S.PROGRAM_ID, 0),
T.PROGRAM_UPDATE_DATE = ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_cst_cost_elements T, ora_cst_cost_elements S
WHERE
T.KEY_ID = S.COST_ELEMENT_ID;

/*insert new rows*/
INSERT INTO dim_ora_cst_cost_elements
(
dim_ora_cst_cost_elementsid,
COST_ELEMENT_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,COST_ELEMENT,
DESCRIPTION,REQUEST_ID,PROGRAM_APPLICATION_ID,PROGRAM_ID,PROGRAM_UPDATE_DATE,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_cst_cost_elements WHERE TABLE_NAME = 'dim_ora_cst_cost_elements' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_cst_cost_elementsid,
S.COST_ELEMENT_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.COST_ELEMENT, 'Not Set') COST_ELEMENT,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.REQUEST_ID, 0) REQUEST_ID,
ifnull(S.PROGRAM_APPLICATION_ID, 0) PROGRAM_APPLICATION_ID,
ifnull(S.PROGRAM_ID, 0) PROGRAM_ID,
ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') PROGRAM_UPDATE_DATE,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.COST_ELEMENT_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ora_cst_cost_elements S LEFT JOIN dim_ora_cst_cost_elements D
ON D.KEY_ID = S.COST_ELEMENT_ID
WHERE D.KEY_ID is null;
