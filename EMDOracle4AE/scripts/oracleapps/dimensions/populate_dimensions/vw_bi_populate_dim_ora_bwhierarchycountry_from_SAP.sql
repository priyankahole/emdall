/******************************************************************************************************************/
/*   Script         : bi_populate_bwhierarchycountry                                                              */
/*   Author         : Cristian C                                                                                  */
/*   Created On     : 21 Apr 2016                                                                                 */
/*   Description    : Populating script of dim_ora_bwhierarchycountry.                                            */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   21 Apr 2016         Cristian C      1.0               Creating the script.                                   */
/******************************************************************************************************************/


DROP TABLE IF EXISTS bi0_hcountryflat;
DROP TABLE IF EXISTS country_mapping;

/*
CREATE TABLE bi0_hcountryflat AS SELECT * FROM emdtempocc4.bi0_hcountryflat
CREATE TABLE country_mapping AS SELECT * FROM emdtempocc4.country_mapping
*/

DROP TABLE IF EXISTS dim_ora_bwhierarchycountry;

CREATE TABLE dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid BIGINT DEFAULT 1 NOT NULL ENABLE
,internalhierarchyname VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,internalhierarchyid VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,internalidno BIGINT DEFAULT 0 NOT NULL ENABLE
,nodehierarchynamelvl7 VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchynamelvl6 VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchynamelvl5 VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchynamelvl4 VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchynamelvl3 VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchynamelvl2 VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchynamelvl1 VARCHAR(100) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchydesclvl7 VARCHAR(200) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchydesclvl6 VARCHAR(200) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchydesclvl5 VARCHAR(200) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchydesclvl4 VARCHAR(200) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchydesclvl3 VARCHAR(200) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchydesclvl2 VARCHAR(200) DEFAULT 'Not Set' NOT NULL ENABLE
,nodehierarchydesclvl1 VARCHAR(200) DEFAULT 'Not Set' NOT NULL ENABLE
,dw_insert_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
,dw_update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
,projectsourceid INT DEFAULT 1 NOT NULL ENABLE);

insert into dim_ora_bwhierarchycountry(dim_ora_bwhierarchycountryid)
select 1 from (select 1) a where not exists (select 1 from dim_ora_bwhierarchycountry where dim_ora_bwhierarchycountryid = 1);


DROP TABLE IF EXISTS TMP_COUNTRY04_CONS_HLTH;
CREATE TABLE TMP_COUNTRY04_CONS_HLTH
AS
SELECT IFNULL(a.hieid,'Not Set') as internalhierarchyID,
	   IFNULL(a.nodeid, 0) as internalidno,
       CASE
         WHEN a.tlevel = 5 THEN a.nodename
         WHEN b.tlevel = 5 THEN b.nodename
         WHEN c.tlevel = 5 THEN c.nodename
         WHEN d.tlevel = 5 THEN d.nodename
         WHEN e.tlevel = 5 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl5,
       CASE
         WHEN a.tlevel = 4 THEN a.nodename
         WHEN b.tlevel = 4 THEN b.nodename
         WHEN c.tlevel = 4 THEN c.nodename
         WHEN d.tlevel = 4 THEN d.nodename
         WHEN e.tlevel = 4 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl4,
       CASE
         WHEN a.tlevel = 3 THEN a.nodename
         WHEN b.tlevel = 3 THEN b.nodename
         WHEN c.tlevel = 3 THEN c.nodename
         WHEN d.tlevel = 3 THEN d.nodename
         WHEN e.tlevel = 3 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl3,
       CASE
         WHEN a.tlevel = 2 THEN a.nodename
         WHEN b.tlevel = 2 THEN b.nodename
         WHEN c.tlevel = 2 THEN c.nodename
         WHEN d.tlevel = 2 THEN d.nodename
         WHEN e.tlevel = 2 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl2,
       CASE
         WHEN a.tlevel = 1 THEN a.nodename
         WHEN b.tlevel = 1 THEN b.nodename
         WHEN c.tlevel = 1 THEN c.nodename
         WHEN d.tlevel = 1 THEN d.nodename
         WHEN e.tlevel = 1 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl1,
	   CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl5,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl4,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl3,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl2,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl1
FROM   bi0_hcountryflat a
       LEFT JOIN bi0_hcountryflat b
              ON a.hieid = b.hieid
                 AND a.parentid = b.nodeid
       LEFT JOIN bi0_hcountryflat c
              ON b.hieid = c.hieid
                 AND b.parentid = c.nodeid
       LEFT JOIN bi0_hcountryflat d
              ON c.hieid = d.hieid
                 AND c.parentid = d.nodeid
       LEFT JOIN bi0_hcountryflat e
              ON d.hieid = e.hieid
                 AND d.parentid = e.nodeid
where a.hieid = '548WP23VF6R8ICTMIER1FW7R0'
AND  a.childid = 0;

UPDATE TMP_COUNTRY04_CONS_HLTH dim
SET shortdescriptionlvl5 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY04_CONS_HLTH dim, country_mapping m
WHERE  dim.nodehierarchynamelvl5 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY04_CONS_HLTH dim
SET shortdescriptionlvl4 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY04_CONS_HLTH dim, country_mapping m
WHERE  dim.nodehierarchynamelvl4 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY04_CONS_HLTH dim
SET shortdescriptionlvl3 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY04_CONS_HLTH dim, country_mapping m
WHERE  dim.nodehierarchynamelvl3 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY04_CONS_HLTH dim
SET shortdescriptionlvl2 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY04_CONS_HLTH dim, country_mapping m
WHERE  dim.nodehierarchynamelvl2 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY04_CONS_HLTH dim
SET shortdescriptionlvl1 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY04_CONS_HLTH dim, country_mapping m
WHERE  dim.nodehierarchynamelvl1 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY04_CONS_HLTH'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl5
,'Not Set'
,'Not Set'
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl5
,'Not Set'
,'Not Set'
,shortdescriptionlvl4
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY04_CONS_HLTH
WHERE nodehierarchynamelvl5 <> 'Not Set';

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY04_CONS_HLTH'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl4
,'Not Set'
,'Not Set'
,'Not Set'
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl4
,'Not Set'
,'Not Set'
,'Not Set'
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY04_CONS_HLTH
WHERE nodehierarchynamelvl5 = 'Not Set' AND nodehierarchynamelvl4 <> 'Not Set';

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY04_CONS_HLTH'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl3
,'Not Set'
,'Not Set'
,'Not Set'
,'Not Set'
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl3
,'Not Set'
,'Not Set'
,'Not Set'
,'Not Set'
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY04_CONS_HLTH
WHERE nodehierarchynamelvl5 = 'Not Set' AND nodehierarchynamelvl4 = 'Not Set' AND nodehierarchynamelvl3 <> 'Not Set';
-------------------------------------------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS TMP_COUNTRY05_BIO_PHRMA;
CREATE TABLE TMP_COUNTRY05_BIO_PHRMA
AS
SELECT IFNULL(a.hieid,'Not Set') as internalhierarchyID,
	   IFNULL(a.nodeid, 0) as internalidno,
	   	   CASE
         WHEN a.tlevel = 7 THEN a.nodename
         WHEN b.tlevel = 7 THEN b.nodename
         WHEN c.tlevel = 7 THEN c.nodename
         WHEN d.tlevel = 7 THEN d.nodename
         WHEN e.tlevel = 7 THEN e.nodename
		 WHEN f.tlevel = 7 THEN f.nodename
		 WHEN g.tlevel = 7 THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl7,
	   CASE
         WHEN a.tlevel = 6 THEN a.nodename
         WHEN b.tlevel = 6 THEN b.nodename
         WHEN c.tlevel = 6 THEN c.nodename
         WHEN d.tlevel = 6 THEN d.nodename
         WHEN e.tlevel = 6 THEN e.nodename
		 WHEN f.tlevel = 6 THEN f.nodename
		 WHEN g.tlevel = 6 THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl6,
       CASE
         WHEN a.tlevel = 5 THEN a.nodename
         WHEN b.tlevel = 5 THEN b.nodename
         WHEN c.tlevel = 5 THEN c.nodename
         WHEN d.tlevel = 5 THEN d.nodename
         WHEN e.tlevel = 5 THEN e.nodename
		 WHEN f.tlevel = 5 THEN f.nodename
		 WHEN g.tlevel = 5 THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl5,
       CASE
         WHEN a.tlevel = 4 THEN a.nodename
         WHEN b.tlevel = 4 THEN b.nodename
         WHEN c.tlevel = 4 THEN c.nodename
         WHEN d.tlevel = 4 THEN d.nodename
         WHEN e.tlevel = 4 THEN e.nodename
		 WHEN f.tlevel = 4 THEN f.nodename
		 WHEN g.tlevel = 4 THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl4,
       CASE
         WHEN a.tlevel = 3 THEN a.nodename
         WHEN b.tlevel = 3 THEN b.nodename
         WHEN c.tlevel = 3 THEN c.nodename
         WHEN d.tlevel = 3 THEN d.nodename
         WHEN e.tlevel = 3 THEN e.nodename
		 WHEN f.tlevel = 3 THEN f.nodename
		 WHEN g.tlevel = 3 THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl3,
       CASE
         WHEN a.tlevel = 2 THEN a.nodename
         WHEN b.tlevel = 2 THEN b.nodename
         WHEN c.tlevel = 2 THEN c.nodename
         WHEN d.tlevel = 2 THEN d.nodename
         WHEN e.tlevel = 2 THEN e.nodename
		 WHEN f.tlevel = 2 THEN f.nodename
         WHEN g.tlevel = 2 THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl2,
       CASE
         WHEN a.tlevel = 1 THEN a.nodename
         WHEN b.tlevel = 1 THEN b.nodename
         WHEN c.tlevel = 1 THEN c.nodename
         WHEN d.tlevel = 1 THEN d.nodename
         WHEN e.tlevel = 1 THEN e.nodename
		 WHEN f.tlevel = 1 THEN f.nodename
		 WHEN g.tlevel = 1 THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl1,
	   IFNULL(a.tlevel, 0) as nodehierarchylevel,
	   IFNULL(a.link,'Not Set') as nodehierarchylinkind,
	   IFNULL(a.parentid, 0) as nodehierarchyparentid,
	   IFNULL(a.childid, 0) as nodehierarchychildid,
	   IFNULL(a.nextid, 0) as nodehierarchynextid,
	   CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl7,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl6,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl5,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl4,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl3,
	   CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl2,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl1
FROM   bi0_hcountryflat a
       LEFT JOIN bi0_hcountryflat b
              ON a.hieid = b.hieid
                 AND a.parentid = b.nodeid
       LEFT JOIN bi0_hcountryflat c
              ON b.hieid = c.hieid
                 AND b.parentid = c.nodeid
       LEFT JOIN bi0_hcountryflat d
              ON c.hieid = d.hieid
                 AND c.parentid = d.nodeid
       LEFT JOIN bi0_hcountryflat e
              ON d.hieid = e.hieid
                 AND d.parentid = e.nodeid
		LEFT JOIN bi0_hcountryflat f
              ON a.hieid = f.hieid
                 AND e.parentid = f.nodeid
	    LEFT JOIN bi0_hcountryflat g
              ON a.hieid = g.hieid
                 AND f.parentid = g.nodeid
where a.hieid = '548YXNWXHSJ2Y19OSUCHY2CL7'
AND  a.childid = 0;

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET shortdescriptionlvl7 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY05_BIO_PHRMA dim, country_mapping m
WHERE  dim.nodehierarchynamelvl7 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET shortdescriptionlvl6 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY05_BIO_PHRMA dim, country_mapping m
WHERE  dim.nodehierarchynamelvl6 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET shortdescriptionlvl5 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY05_BIO_PHRMA dim, country_mapping m
WHERE  dim.nodehierarchynamelvl5 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET shortdescriptionlvl4 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY05_BIO_PHRMA dim, country_mapping m
WHERE  dim.nodehierarchynamelvl4 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET shortdescriptionlvl3 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY05_BIO_PHRMA dim, country_mapping m
WHERE  dim.nodehierarchynamelvl3 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET shortdescriptionlvl2 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY05_BIO_PHRMA dim, country_mapping m
WHERE  dim.nodehierarchynamelvl2 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET shortdescriptionlvl1 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY05_BIO_PHRMA dim, country_mapping m
WHERE  dim.nodehierarchynamelvl1 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET dim.shortdescriptionlvl7 = IFNULL(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY05_BIO_PHRMA dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl7 = t.COUNTRY;

DROP TABLE IF EXISTS TMP6;
CREATE TABLE TMP6 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl6
FROM TMP_COUNTRY05_BIO_PHRMA dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl6 = t.COUNTRY
	AND dim.nodehierarchynamelvl7 = 'Not Set';

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET dim.shortdescriptionlvl6 = ifnull(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY05_BIO_PHRMA dim, TMP6 T
WHERE  dim.nodehierarchynamelvl6 = t.COUNTRY
	AND dim.nodehierarchynamelvl7 = 'Not Set';

DROP TABLE IF EXISTS TMP5;
CREATE TABLE TMP5 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl5
FROM TMP_COUNTRY05_BIO_PHRMA dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl5 = t.COUNTRY
	AND dim.nodehierarchynamelvl6 = 'Not Set'
	AND dim.nodehierarchynamelvl7 = 'Not Set';

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET dim.shortdescriptionlvl5 = ifnull(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY05_BIO_PHRMA dim, TMP5 T
WHERE  dim.nodehierarchynamelvl5 = t.COUNTRY
	AND dim.nodehierarchynamelvl6 = 'Not Set'
	AND dim.nodehierarchynamelvl7 = 'Not Set';

DROP TABLE IF EXISTS TMP4;
CREATE TABLE TMP4 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl4
FROM TMP_COUNTRY05_BIO_PHRMA dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl4 = t.COUNTRY
	AND dim.nodehierarchynamelvl5 = 'Not Set'
	AND dim.nodehierarchynamelvl6 = 'Not Set'
	AND dim.nodehierarchynamelvl7 = 'Not Set';

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET dim.shortdescriptionlvl4 = ifnull(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY05_BIO_PHRMA dim, TMP4 T
WHERE  dim.nodehierarchynamelvl4 = t.COUNTRY
	AND dim.nodehierarchynamelvl5 = 'Not Set'
	AND dim.nodehierarchynamelvl6 = 'Not Set'
	AND dim.nodehierarchynamelvl7 = 'Not Set';

DROP TABLE IF EXISTS TMP3;
CREATE TABLE TMP3 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl3
FROM TMP_COUNTRY05_BIO_PHRMA dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl3 = t.COUNTRY
	AND dim.nodehierarchynamelvl4 = 'Not Set'
	AND dim.nodehierarchynamelvl5 = 'Not Set'
	AND dim.nodehierarchynamelvl6 = 'Not Set'
	AND dim.nodehierarchynamelvl7 = 'Not Set';

UPDATE TMP_COUNTRY05_BIO_PHRMA dim
SET dim.shortdescriptionlvl3 = ifnull(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY05_BIO_PHRMA dim, TMP3 T
WHERE  dim.nodehierarchynamelvl3 = t.COUNTRY
	AND dim.nodehierarchynamelvl4 = 'Not Set'
	AND dim.nodehierarchynamelvl5 = 'Not Set'
	AND dim.nodehierarchynamelvl6 = 'Not Set'
	AND dim.nodehierarchynamelvl7 = 'Not Set';

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY05_BIO_PHRMA'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl7
,shortdescriptionlvl6
,shortdescriptionlvl5
,shortdescriptionlvl4
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY05_BIO_PHRMA
WHERE nodehierarchynamelvl7 <> 'Not Set';

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY05_BIO_PHRMA'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl6
,'Not Set'
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl6
,'Not Set'
,shortdescriptionlvl5
,shortdescriptionlvl4
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY05_BIO_PHRMA
WHERE nodehierarchynamelvl7 = 'Not Set'
	AND nodehierarchynamelvl6 <> 'Not Set';

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY05_BIO_PHRMA'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl5
,'Not Set'
,'Not Set'
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl5
,'Not Set'
,'Not Set'
,shortdescriptionlvl4
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY05_BIO_PHRMA
WHERE nodehierarchynamelvl7 = 'Not Set'
	AND nodehierarchynamelvl6 = 'Not Set'
	AND nodehierarchynamelvl5 <> 'Not Set';

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY05_BIO_PHRMA'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl4
,'Not Set'
,'Not Set'
,'Not Set'
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl4
,'Not Set'
,'Not Set'
,'Not Set'
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY05_BIO_PHRMA
WHERE nodehierarchynamelvl7 = 'Not Set'
	AND nodehierarchynamelvl6 = 'Not Set'
	AND nodehierarchynamelvl5 = 'Not Set'
	AND nodehierarchynamelvl4 <> 'Not Set';

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY05_BIO_PHRMA'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl3
,'Not Set'
,'Not Set'
,'Not Set'
,'Not Set'
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl3
,'Not Set'
,'Not Set'
,'Not Set'
,'Not Set'
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY05_BIO_PHRMA
WHERE nodehierarchynamelvl7 = 'Not Set'
	AND nodehierarchynamelvl6 = 'Not Set'
	AND nodehierarchynamelvl5 = 'Not Set'
	AND nodehierarchynamelvl4 = 'Not Set'
	AND nodehierarchynamelvl3 <> 'Not Set';
---------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS TMP_COUNTRY07_LIFE_SCIENCE;
CREATE TABLE TMP_COUNTRY07_LIFE_SCIENCE
AS
SELECT IFNULL(a.hieid,'Not Set') as internalhierarchyID,
	   IFNULL(a.nodeid, 0) as internalidno,
     CASE
         WHEN a.tlevel = 6 THEN a.nodename
         WHEN b.tlevel = 6 THEN b.nodename
         WHEN c.tlevel = 6 THEN c.nodename
         WHEN d.tlevel = 6 THEN d.nodename
         WHEN e.tlevel = 6 THEN e.nodename
		 WHEN f.tlevel = 6 THEN f.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl6,
       CASE
         WHEN a.tlevel = 5 THEN a.nodename
         WHEN b.tlevel = 5 THEN b.nodename
         WHEN c.tlevel = 5 THEN c.nodename
         WHEN d.tlevel = 5 THEN d.nodename
         WHEN e.tlevel = 5 THEN e.nodename
		 WHEN f.tlevel = 5 THEN f.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl5,
       CASE
         WHEN a.tlevel = 4 THEN a.nodename
         WHEN b.tlevel = 4 THEN b.nodename
         WHEN c.tlevel = 4 THEN c.nodename
         WHEN d.tlevel = 4 THEN d.nodename
         WHEN e.tlevel = 4 THEN e.nodename
		 WHEN f.tlevel = 4 THEN f.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl4,
       CASE
         WHEN a.tlevel = 3 THEN a.nodename
         WHEN b.tlevel = 3 THEN b.nodename
         WHEN c.tlevel = 3 THEN c.nodename
         WHEN d.tlevel = 3 THEN d.nodename
         WHEN e.tlevel = 3 THEN e.nodename
		 WHEN f.tlevel = 3 THEN f.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl3,
       CASE
         WHEN a.tlevel = 2 THEN a.nodename
         WHEN b.tlevel = 2 THEN b.nodename
         WHEN c.tlevel = 2 THEN c.nodename
         WHEN d.tlevel = 2 THEN d.nodename
         WHEN e.tlevel = 2 THEN e.nodename
		 WHEN f.tlevel = 2 THEN f.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl2,
       CASE
         WHEN a.tlevel = 1 THEN a.nodename
         WHEN b.tlevel = 1 THEN b.nodename
         WHEN c.tlevel = 1 THEN c.nodename
         WHEN d.tlevel = 1 THEN d.nodename
         WHEN e.tlevel = 1 THEN e.nodename
		 WHEN f.tlevel = 1 THEN f.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl1,
	   CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl6,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl5,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl4,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl3,
       CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl2,
	   CONVERT(VARCHAR(200), 'Not Set') as shortdescriptionlvl1
FROM   bi0_hcountryflat a
       LEFT JOIN bi0_hcountryflat b
              ON a.hieid = b.hieid
                 AND a.parentid = b.nodeid
       LEFT JOIN bi0_hcountryflat c
              ON b.hieid = c.hieid
                 AND b.parentid = c.nodeid
       LEFT JOIN bi0_hcountryflat d
              ON c.hieid = d.hieid
                 AND c.parentid = d.nodeid
       LEFT JOIN bi0_hcountryflat e
              ON d.hieid = e.hieid
                 AND d.parentid = e.nodeid
		LEFT JOIN bi0_hcountryflat f
              ON a.hieid = f.hieid
                 AND e.parentid = f.nodeid
where a.hieid = '549HWG7F0J8VZ1P8YIYH1CPQ3'
AND  a.childid = 0;

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET shortdescriptionlvl6 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, country_mapping m
WHERE  dim.nodehierarchynamelvl6 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET shortdescriptionlvl5 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, country_mapping m
WHERE  dim.nodehierarchynamelvl5 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET shortdescriptionlvl4 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, country_mapping m
WHERE  dim.nodehierarchynamelvl4 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET shortdescriptionlvl3 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, country_mapping m
WHERE  dim.nodehierarchynamelvl3 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET shortdescriptionlvl2 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, country_mapping m
WHERE  dim.nodehierarchynamelvl2 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET shortdescriptionlvl1 = CASE WHEN m.longdescription is not NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, country_mapping m
WHERE  dim.nodehierarchynamelvl1 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

DROP TABLE IF EXISTS TMP6;
CREATE TABLE TMP6 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl6
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl6 = t.COUNTRY;

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET dim.shortdescriptionlvl6 = ifnull(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, TMP6 T
WHERE  dim.nodehierarchynamelvl6 = t.COUNTRY;

DROP TABLE IF EXISTS TMP5;
CREATE TABLE TMP5 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl5
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl5 = t.COUNTRY
	AND dim.nodehierarchynamelvl6 = 'Not Set';

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET dim.shortdescriptionlvl5 = ifnull(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, TMP5 T
WHERE  dim.nodehierarchynamelvl5 = t.COUNTRY
	AND dim.nodehierarchynamelvl6 = 'Not Set';

DROP TABLE IF EXISTS TMP4;
CREATE TABLE TMP4 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl4
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl4 = t.COUNTRY
	AND dim.nodehierarchynamelvl5 = 'Not Set'
	AND dim.nodehierarchynamelvl6 = 'Not Set';

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET dim.shortdescriptionlvl4 = ifnull(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, TMP4 T
WHERE  dim.nodehierarchynamelvl4 = t.COUNTRY
	AND dim.nodehierarchynamelvl5 = 'Not Set'
	AND dim.nodehierarchynamelvl6 = 'Not Set';

DROP TABLE IF EXISTS TMP3;
CREATE TABLE TMP3 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl3
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl3 = t.COUNTRY
	AND dim.nodehierarchynamelvl4 = 'Not Set'
	AND dim.nodehierarchynamelvl5 = 'Not Set'
	AND dim.nodehierarchynamelvl6 = 'Not Set';

UPDATE TMP_COUNTRY07_LIFE_SCIENCE dim
SET dim.shortdescriptionlvl3 = ifnull(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY07_LIFE_SCIENCE dim, TMP3 T
WHERE  dim.nodehierarchynamelvl3 = t.COUNTRY
	AND dim.nodehierarchynamelvl4 = 'Not Set'

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY07_LIFE_SCIENCE'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl6
,'Not Set'
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl6
,'Not Set'
,shortdescriptionlvl5
,shortdescriptionlvl4
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY07_LIFE_SCIENCE
WHERE nodehierarchynamelvl6 <> 'Not Set';

delete from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m where m.table_name = 'dim_ora_bwhierarchycountry';
insert into NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
select 'dim_ora_bwhierarchycountry',
 ifnull(max(d.dim_ora_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ora_bwhierarchycountry d
where d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        from NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        where m.table_name = 'dim_ora_bwhierarchycountry') + row_number() over(order by '') AS dim_ora_bwhierarchycountryid
,'COUNTRY07_LIFE_SCIENCE'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl5
,'Not Set'
,'Not Set'
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl5
,'Not Set'
,'Not Set'
,shortdescriptionlvl4
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY07_LIFE_SCIENCE
WHERE nodehierarchynamelvl6 = 'Not Set'
	AND nodehierarchynamelvl5 <> 'Not Set';

DELETE FROM NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m WHERE m.table_name = 'dim_ora_bwhierarchycountry';
INSERT INTO NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
SELECT 'dim_ora_bwhierarchycountry',
 IFNULL(max(d.dim_ora_bwhierarchycountryid ),
              IFNULL((select s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_ora_bwhierarchycountry d
WHERE d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT IFNULL(m.max_id, 1)
        FROM NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        WHERE m.table_name = 'dim_ora_bwhierarchycountry') + ROW_NUMBER() over(ORDER BY '') AS dim_ora_bwhierarchycountryid
,'COUNTRY07_LIFE_SCIENCE'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl4
,'Not Set'
,'Not Set'
,'Not Set'
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl4
,'Not Set'
,'Not Set'
,'Not Set'
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY07_LIFE_SCIENCE
WHERE nodehierarchynamelvl6 = 'Not Set'
	AND nodehierarchynamelvl5 = 'Not Set'
	AND nodehierarchynamelvl4 <> 'Not Set';

DELETE FROM NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m WHERE m.table_name = 'dim_ora_bwhierarchycountry';
INSERT INTO NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
SELECT 'dim_ora_bwhierarchycountry',
 IFNULL(MAX(d.dim_ora_bwhierarchycountryid ),
              IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_ora_bwhierarchycountry d
WHERE d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT IFNULL(m.max_id, 1)
        FROM NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        WHERE m.table_name = 'dim_ora_bwhierarchycountry') + ROW_NUMBER() over(ORDER BY '') AS dim_ora_bwhierarchycountryid
,'COUNTRY07_LIFE_SCIENCE'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl3
,'Not Set'
,'Not Set'
,'Not Set'
,'Not Set'
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl3
,'Not Set'
,'Not Set'
,'Not Set'
,'Not Set'
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY07_LIFE_SCIENCE
WHERE nodehierarchynamelvl6 = 'Not Set'
	AND nodehierarchynamelvl5 = 'Not Set'
	AND nodehierarchynamelvl4 = 'Not Set'
	AND nodehierarchynamelvl3 <> 'Not Set';
---------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS TMP_COUNTRY08_PERF_MAT;
CREATE TABLE TMP_COUNTRY08_PERF_MAT
AS
SELECT IFNULL(a.hieid,'Not Set') AS internalhierarchyID,
	   IFNULL(a.nodeid, 0) AS internalidno,
       CASE
         WHEN a.tlevel = 5 THEN a.nodename
         WHEN b.tlevel = 5 THEN b.nodename
         WHEN c.tlevel = 5 THEN c.nodename
         WHEN d.tlevel = 5 THEN d.nodename
         WHEN e.tlevel = 5 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl5,
       CASE
         WHEN a.tlevel = 4 THEN a.nodename
         WHEN b.tlevel = 4 THEN b.nodename
         WHEN c.tlevel = 4 THEN c.nodename
         WHEN d.tlevel = 4 THEN d.nodename
         WHEN e.tlevel = 4 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl4,
       CASE
         WHEN a.tlevel = 3 THEN a.nodename
         WHEN b.tlevel = 3 THEN b.nodename
         WHEN c.tlevel = 3 THEN c.nodename
         WHEN d.tlevel = 3 THEN d.nodename
         WHEN e.tlevel = 3 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl3,
       CASE
         WHEN a.tlevel = 2 THEN a.nodename
         WHEN b.tlevel = 2 THEN b.nodename
         WHEN c.tlevel = 2 THEN c.nodename
         WHEN d.tlevel = 2 THEN d.nodename
         WHEN e.tlevel = 2 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl2,
       CASE
         WHEN a.tlevel = 1 THEN a.nodename
         WHEN b.tlevel = 1 THEN b.nodename
         WHEN c.tlevel = 1 THEN c.nodename
         WHEN d.tlevel = 1 THEN d.nodename
         WHEN e.tlevel = 1 THEN e.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl1,
	   CONVERT(VARCHAR(200), 'Not Set') AS shortdescriptionlvl5,
       CONVERT(VARCHAR(200), 'Not Set') AS shortdescriptionlvl4,
       CONVERT(VARCHAR(200), 'Not Set') AS shortdescriptionlvl3,
       CONVERT(VARCHAR(200), 'Not Set') AS shortdescriptionlvl2,
       CONVERT(VARCHAR(200), 'Not Set') AS shortdescriptionlvl1
FROM   bi0_hcountryflat a
       LEFT JOIN bi0_hcountryflat b
              ON a.hieid = b.hieid
                 AND a.parentid = b.nodeid
       LEFT JOIN bi0_hcountryflat c
              ON b.hieid = c.hieid
                 AND b.parentid = c.nodeid
       LEFT JOIN bi0_hcountryflat d
              ON c.hieid = d.hieid
                 AND c.parentid = d.nodeid
       LEFT JOIN bi0_hcountryflat e
              ON d.hieid = e.hieid
                 AND d.parentid = e.nodeid
WHERE a.hieid = '54G8L55S3E80Q2SXZYJAELHKR'
AND  a.childid = 0;

UPDATE TMP_COUNTRY08_PERF_MAT dim
SET shortdescriptionlvl5 = CASE WHEN m.longdescription IS NOT NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY08_PERF_MAT dim, country_mapping m
WHERE  dim.nodehierarchynamelvl5 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY08_PERF_MAT dim
SET shortdescriptionlvl4 = CASE WHEN m.longdescription IS NOT NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY08_PERF_MAT dim, country_mapping m
WHERE  dim.nodehierarchynamelvl4 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY08_PERF_MAT dim
SET shortdescriptionlvl3 = CASE WHEN m.longdescription IS NOT NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY08_PERF_MAT dim, country_mapping m
WHERE  dim.nodehierarchynamelvl3 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY08_PERF_MAT dim
SET shortdescriptionlvl2 = CASE WHEN m.longdescription IS NOT NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY08_PERF_MAT dim, country_mapping m
WHERE  dim.nodehierarchynamelvl2 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

UPDATE TMP_COUNTRY08_PERF_MAT dim
SET shortdescriptionlvl1 = CASE WHEN m.longdescription IS NOT NULL THEN m.longdescription ELSE m.shortdescription END
FROM TMP_COUNTRY08_PERF_MAT dim, country_mapping m
WHERE  dim.nodehierarchynamelvl1 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;

DROP TABLE IF EXISTS TMP5;
CREATE TABLE TMP5 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl5
FROM TMP_COUNTRY08_PERF_MAT dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl5 = t.COUNTRY;

UPDATE TMP_COUNTRY08_PERF_MAT dim
SET dim.shortdescriptionlvl5 = IFNULL(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY08_PERF_MAT dim, TMP5 t
WHERE  dim.nodehierarchynamelvl5 = t.COUNTRY;

DROP TABLE IF EXISTS TMP4;
CREATE TABLE TMP4 AS
SELECT  distinct t.COUNTRY , dim.nodehierarchynamelvl4
FROM TMP_COUNTRY08_PERF_MAT dim, dim_ora_business_org t
WHERE  dim.nodehierarchynamelvl4 = t.COUNTRY
	AND dim.nodehierarchynamelvl5 = 'Not Set';

UPDATE TMP_COUNTRY08_PERF_MAT dim
SET dim.shortdescriptionlvl4 = IFNULL(T.COUNTRY,'Not Set')
FROM TMP_COUNTRY08_PERF_MAT dim, TMP4 t
WHERE  dim.nodehierarchynamelvl4 = t.COUNTRY
	AND dim.nodehierarchynamelvl5 = 'Not Set';

DELETE FROM NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m WHERE m.table_name = 'dim_ora_bwhierarchycountry';
INSERT INTO NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
SELECT 'dim_ora_bwhierarchycountry',
 IFNULL(MAX(d.dim_ora_bwhierarchycountryid ),
              IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_ora_bwhierarchycountry d
WHERE d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT IFNULL(m.max_id, 1)
        FROM NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        WHERE m.table_name = 'dim_ora_bwhierarchycountry') + ROW_NUMBER() over(ORDER BY '') AS dim_ora_bwhierarchycountryid
,'COUNTRY08_PERF_MAT'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl5
,'Not Set'
,'Not Set'
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl5
,'Not Set'
,'Not Set'
,shortdescriptionlvl4
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY08_PERF_MAT
WHERE nodehierarchynamelvl5 <> 'Not Set';

DELETE FROM NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m WHERE m.table_name = 'dim_ora_bwhierarchycountry';
INSERT INTO NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry
SELECT 'dim_ora_bwhierarchycountry',
 IFNULL(MAX(d.dim_ora_bwhierarchycountryid ),
              IFNULL((SELECT s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM dim_ora_bwhierarchycountry d
WHERE d.dim_ora_bwhierarchycountryid <> 1;

INSERT INTO dim_ora_bwhierarchycountry
(dim_ora_bwhierarchycountryid
,internalhierarchyname
,internalhierarchyid
,internalidno
,nodehierarchynamelvl7
,nodehierarchynamelvl6
,nodehierarchynamelvl5
,nodehierarchynamelvl4
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,nodehierarchydesclvl7
,nodehierarchydesclvl6
,nodehierarchydesclvl5
,nodehierarchydesclvl4
,nodehierarchydesclvl3
,nodehierarchydesclvl2
,nodehierarchydesclvl1)
SELECT (SELECT ifnull(m.max_id, 1)
        FROM NUMBER_FOUNTAIN_dim_ora_bwhierarchycountry m
        WHERE m.table_name = 'dim_ora_bwhierarchycountry') + ROW_NUMBER() over(ORDER BY '') AS dim_ora_bwhierarchycountryid
,'COUNTRY08_PERF_MAT'
,internalhierarchyID
,internalidno
,nodehierarchynamelvl4
,'Not Set'
,'Not Set'
,'Not Set'
,nodehierarchynamelvl3
,nodehierarchynamelvl2
,nodehierarchynamelvl1
,shortdescriptionlvl4
,'Not Set'
,'Not Set'
,'Not Set'
,shortdescriptionlvl3
,shortdescriptionlvl2
,shortdescriptionlvl1
FROM TMP_COUNTRY08_PERF_MAT
WHERE nodehierarchynamelvl5 = 'Not Set'
	AND nodehierarchynamelvl4 <> 'Not Set';
