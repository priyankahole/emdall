
DELETE FROM EMD586.DIM_SHIPPINGINSTRUCTION WHERE PROJECTOURCEID=7;

INSERT INTO EMD586.DIM_SHIPPINGINSTRUCTION
(
DIM_SHIPPINGINSTRUCTIONID
,SHIPPINGINSTRUCTIONCODE
,SHIPPINGINSTRUCTIONDESC
,DW_INSERT_DATE
,DW_UPDATE_DATE
,PROJECTOURCEID
)

SELECT
dim_ora_shippinginstructionid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
,IFNULL(shipping_instruction_code,'Not Set')
,IFNULL(shipping_instruction_desc,'Not Set')
,CURRENT_TIMESTAMP DW_INSERT_DATE
,CURRENT_TIMESTAMP DW_UPDATE_DATE
,p.dim_projectsourceid


FROM dim_ora_shippinginstruction,DIM_projectsource p;


