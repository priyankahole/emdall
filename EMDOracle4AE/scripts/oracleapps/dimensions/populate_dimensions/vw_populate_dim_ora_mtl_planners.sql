/*insert default row*/
INSERT INTO dim_ora_mtl_planners
(
dim_ora_mtl_plannersid,
PLANNER_CODE,ORGANIZATION_ID,
LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,DESCRIPTION,DISABLE_DATE,ATTRIBUTE_CATEGORY,
ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3,ATTRIBUTE4,ATTRIBUTE5,ATTRIBUTE6,ATTRIBUTE7,
ATTRIBUTE8,ATTRIBUTE9,ATTRIBUTE10,ATTRIBUTE11,ATTRIBUTE12,ATTRIBUTE13,ATTRIBUTE14,ATTRIBUTE15,
REQUEST_ID,PROGRAM_APPLICATION_ID,PROGRAM_ID,PROGRAM_UPDATE_DATE,ELECTRONIC_MAIL_ADDRESS,EMPLOYEE_ID,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_mtl_plannersid,
'Not Set' PLANNER_CODE,
0 ORGANIZATION_ID,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' DESCRIPTION,
timestamp '1001-01-01 00:00:00.000000' DISABLE_DATE,
'Not Set' ATTRIBUTE_CATEGORY,
'Not Set' ATTRIBUTE1,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE4,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE8,
'Not Set' ATTRIBUTE9,
'Not Set' ATTRIBUTE10,
'Not Set' ATTRIBUTE11,
'Not Set' ATTRIBUTE12,
'Not Set' ATTRIBUTE13,
'Not Set' ATTRIBUTE14,
'Not Set' ATTRIBUTE15,
0 REQUEST_ID,
0 PROGRAM_APPLICATION_ID,
0 PROGRAM_ID,
timestamp '1001-01-01 00:00:00.000000' PROGRAM_UPDATE_DATE,
'Not Set' ELECTRONIC_MAIL_ADDRESS,
0 EMPLOYEE_ID,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_mtl_planners D
ON T.dim_ora_mtl_plannersid = D.dim_ora_mtl_plannersid
WHERE D.dim_ora_mtl_plannersid is null;

/*initialize NUMBER_FOUNTAIN_dim_ora_mtl_planners*/
delete from NUMBER_FOUNTAIN_dim_ora_mtl_planners where table_name = 'dim_ora_mtl_planners';	

INSERT INTO NUMBER_FOUNTAIN_dim_ora_mtl_planners
SELECT 'dim_ora_mtl_planners',IFNULL(MAX(dim_ora_mtl_plannersid),0)
FROM dim_ora_mtl_planners;

/*update dimension rows*/
UPDATE dim_ora_mtl_planners T
SET
T.PLANNER_CODE    = S.PLANNER_CODE,
T.ORGANIZATION_ID = S.ORGANIZATION_ID,
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.DISABLE_DATE = ifnull(S.DISABLE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.ATTRIBUTE9 = ifnull(S.ATTRIBUTE9, 'Not Set'),
T.ATTRIBUTE10 = ifnull(S.ATTRIBUTE10, 'Not Set'),
T.ATTRIBUTE11 = ifnull(S.ATTRIBUTE11, 'Not Set'),
T.ATTRIBUTE12 = ifnull(S.ATTRIBUTE12, 'Not Set'),
T.ATTRIBUTE13 = ifnull(S.ATTRIBUTE13, 'Not Set'),
T.ATTRIBUTE14 = ifnull(S.ATTRIBUTE14, 'Not Set'),
T.ATTRIBUTE15 = ifnull(S.ATTRIBUTE15, 'Not Set'),
T.REQUEST_ID = ifnull(S.REQUEST_ID, 0),
T.PROGRAM_APPLICATION_ID = ifnull(S.PROGRAM_APPLICATION_ID, 0),
T.PROGRAM_ID = ifnull(S.PROGRAM_ID, 0),
T.PROGRAM_UPDATE_DATE = ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.ELECTRONIC_MAIL_ADDRESS = ifnull(S.ELECTRONIC_MAIL_ADDRESS, 'Not Set'),
T.EMPLOYEE_ID = ifnull(S.EMPLOYEE_ID, 0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_mtl_planners T, ora_mtl_planners S
WHERE
CONVERT(VARCHAR(200),S.PLANNER_CODE) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_mtl_planners
(
dim_ora_mtl_plannersid,
PLANNER_CODE,ORGANIZATION_ID,
LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,DESCRIPTION,DISABLE_DATE,ATTRIBUTE_CATEGORY,
ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3,ATTRIBUTE4,ATTRIBUTE5,ATTRIBUTE6,ATTRIBUTE7,
ATTRIBUTE8,ATTRIBUTE9,ATTRIBUTE10,ATTRIBUTE11,ATTRIBUTE12,ATTRIBUTE13,ATTRIBUTE14,ATTRIBUTE15,
REQUEST_ID,PROGRAM_APPLICATION_ID,PROGRAM_ID,PROGRAM_UPDATE_DATE,ELECTRONIC_MAIL_ADDRESS,EMPLOYEE_ID,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_mtl_planners WHERE TABLE_NAME = 'dim_ora_mtl_planners' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_mtl_plannersid,
S.PLANNER_CODE,
S.ORGANIZATION_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.DISABLE_DATE, timestamp '1001-01-01 00:00:00.000000') DISABLE_DATE,
ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set') ATTRIBUTE_CATEGORY,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.ATTRIBUTE9, 'Not Set') ATTRIBUTE9,
ifnull(S.ATTRIBUTE10, 'Not Set') ATTRIBUTE10,
ifnull(S.ATTRIBUTE11, 'Not Set') ATTRIBUTE11,
ifnull(S.ATTRIBUTE12, 'Not Set') ATTRIBUTE12,
ifnull(S.ATTRIBUTE13, 'Not Set') ATTRIBUTE13,
ifnull(S.ATTRIBUTE14, 'Not Set') ATTRIBUTE14,
ifnull(S.ATTRIBUTE15, 'Not Set') ATTRIBUTE15,
ifnull(S.REQUEST_ID, 0) REQUEST_ID,
ifnull(S.PROGRAM_APPLICATION_ID, 0) PROGRAM_APPLICATION_ID,
ifnull(S.PROGRAM_ID, 0) PROGRAM_ID,
ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') PROGRAM_UPDATE_DATE,
ifnull(S.ELECTRONIC_MAIL_ADDRESS, 'Not Set') ELECTRONIC_MAIL_ADDRESS,
ifnull(S.EMPLOYEE_ID, 0) EMPLOYEE_ID,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
CONVERT(VARCHAR(200),S.PLANNER_CODE) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ora_mtl_planners S LEFT JOIN dim_ora_mtl_planners D ON
D.KEY_ID = CONVERT(VARCHAR(200),S.PLANNER_CODE) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID)
WHERE
D.KEY_ID is null;

/*dim_ora_product.planner_code*/
update dim_ora_product p
set
p.planner_description  = pln.description,
p.planner_disable_date = pln.disable_date
from dim_ora_product p, dim_ora_mtl_planners pln
where p.planner_code = pln.planner_code
and p.organization_id=pln.organization_id;

/*dim_ora_invproduct.planner_code*/
update dim_ora_invproduct p
set
p.planner_description  = pln.description,
p.planner_disable_date = pln.disable_date
from dim_ora_invproduct p, dim_ora_mtl_planners pln
where p.planner_code = pln.planner_code
and p.organization_id=pln.organization_id;
