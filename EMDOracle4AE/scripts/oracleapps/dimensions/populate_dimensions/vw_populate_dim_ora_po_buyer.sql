/*insert default row*/
INSERT INTO dim_ora_po_buyer
(
dim_ora_po_buyerid,
end_date_active,start_date_active,authorization_limit,category_id,location_code,location_id,created_by,creation_date,
last_update_login,last_updated_by,last_update_date,agent_name,agent_id,row_id,attribute15,attribute14,attribute13,attribute12,attribute11,
attribute10,attribute9,attribute8,attribute7,attribute6,attribute5,attribute4,attribute3,attribute2,attribute1,attribute_category,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_po_buyerid,
timestamp '1001-01-01 00:00:00.000000' END_DATE_ACTIVE,
timestamp '1001-01-01 00:00:00.000000' START_DATE_ACTIVE,
0 AUTHORIZATION_LIMIT,
0 CATEGORY_ID,
'Not Set' LOCATION_CODE,
0 LOCATION_ID,
0 CREATED_BY,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
0 LAST_UPDATE_LOGIN,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
'Not Set' AGENT_NAME,
0 AGENT_ID,
'Not Set' ROW_ID,
'Not Set' ATTRIBUTE15,
'Not Set' ATTRIBUTE14,
'Not Set' ATTRIBUTE13,
'Not Set' ATTRIBUTE12,
'Not Set' ATTRIBUTE11,
'Not Set' ATTRIBUTE10,
'Not Set' ATTRIBUTE9,
'Not Set' ATTRIBUTE8,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE4,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE1,
'Not Set' ATTRIBUTE_CATEGORY,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_po_buyer D
ON T.dim_ora_po_buyerid = D.dim_ora_po_buyerid
WHERE D.dim_ora_po_buyerid is null;

/*initialize NUMBER_FOUNTAIN_dim_ora_po_buyer*/
delete from NUMBER_FOUNTAIN_dim_ora_po_buyer where table_name = 'dim_ora_po_buyer';	

INSERT INTO NUMBER_FOUNTAIN_dim_ora_po_buyer
SELECT 'dim_ora_po_buyer',IFNULL(MAX(dim_ora_po_buyerid),0)
FROM dim_ora_po_buyer;

/*update dimension rows*/
UPDATE dim_ora_po_buyer T
SET
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000'),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000'),
T.AUTHORIZATION_LIMIT = ifnull(S.AUTHORIZATION_LIMIT, 0),
T.CATEGORY_ID = ifnull(S.CATEGORY_ID, 0),
T.LOCATION_CODE = ifnull(S.LOCATION_CODE, 'Not Set'),
T.LOCATION_ID = ifnull(S.LOCATION_ID, 0),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.AGENT_NAME = ifnull(S.AGENT_NAME, 'Not Set'),
T.AGENT_ID = S.AGENT_ID,
T.ROW_ID = ifnull(S.ROW_ID, 'Not Set'),
T.ATTRIBUTE15 = ifnull(S.ATTRIBUTE15, 'Not Set'),
T.ATTRIBUTE14 = ifnull(S.ATTRIBUTE14, 'Not Set'),
T.ATTRIBUTE13 = ifnull(S.ATTRIBUTE13, 'Not Set'),
T.ATTRIBUTE12 = ifnull(S.ATTRIBUTE12, 'Not Set'),
T.ATTRIBUTE11 = ifnull(S.ATTRIBUTE11, 'Not Set'),
T.ATTRIBUTE10 = ifnull(S.ATTRIBUTE10, 'Not Set'),
T.ATTRIBUTE9 = ifnull(S.ATTRIBUTE9, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_po_buyer T , ORA_PURCH_AGENT S
WHERE T.KEY_ID = S.AGENT_ID;

/*insert new rows*/
INSERT INTO dim_ora_po_buyer
(
dim_ora_po_buyerid,
end_date_active,start_date_active,authorization_limit,category_id,location_code,location_id,created_by,creation_date,
last_update_login,last_updated_by,last_update_date,agent_name,agent_id,row_id,attribute15,attribute14,attribute13,attribute12,attribute11,
attribute10,attribute9,attribute8,attribute7,attribute6,attribute5,attribute4,attribute3,attribute2,attribute1,attribute_category,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_po_buyer WHERE TABLE_NAME = 'dim_ora_po_buyer' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_po_buyerid,
ifnull(S.END_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.START_DATE_ACTIVE, timestamp '1001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.AUTHORIZATION_LIMIT, 0) AUTHORIZATION_LIMIT,
ifnull(S.CATEGORY_ID, 0) CATEGORY_ID,
ifnull(S.LOCATION_CODE, 'Not Set') LOCATION_CODE,
ifnull(S.LOCATION_ID, 0) LOCATION_ID,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.AGENT_NAME, 'Not Set') AGENT_NAME,
S.AGENT_ID,
ifnull(S.ROW_ID, 'Not Set') ROW_ID,
ifnull(S.ATTRIBUTE15, 'Not Set') ATTRIBUTE15,
ifnull(S.ATTRIBUTE14, 'Not Set') ATTRIBUTE14,
ifnull(S.ATTRIBUTE13, 'Not Set') ATTRIBUTE13,
ifnull(S.ATTRIBUTE12, 'Not Set') ATTRIBUTE12,
ifnull(S.ATTRIBUTE11, 'Not Set') ATTRIBUTE11,
ifnull(S.ATTRIBUTE10, 'Not Set') ATTRIBUTE10,
ifnull(S.ATTRIBUTE9, 'Not Set') ATTRIBUTE9,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set') ATTRIBUTE_CATEGORY,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.AGENT_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_PURCH_AGENT S LEFT JOIN dim_ora_po_buyer D ON
D.KEY_ID = S.AGENT_ID
WHERE
D.KEY_ID is null;
