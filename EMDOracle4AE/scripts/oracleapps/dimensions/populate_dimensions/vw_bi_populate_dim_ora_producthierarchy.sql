
INSERT INTO dim_ora_producthierarchy
(dim_producthierarchyid
,RowIsCurrent)
SELECT 1, 1
FROM (SELECT 1) a
WHERE NOT EXISTS ( SELECT 'x' FROM dim_ora_producthierarchy WHERE dim_producthierarchyid = 1);


DELETE FROM NUMBER_FOUNTAIN_dim_ora_producthierarchy m WHERE m.table_name = 'dim_ora_producthierarchy';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_producthierarchy
SELECT 	'dim_ora_producthierarchy',
	IFNULL(max(d.dim_producthierarchyid),
	IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_ora_producthierarchy d
WHERE d.dim_producthierarchyid <> 1;

INSERT INTO dim_ora_producthierarchy(dim_producthierarchyid
                                          ,ProductHierarchy
										  ,level1code
										  ,level2code
										  ,level3code
										  ,level4code
                                          ,RowStartDate
                                          ,RowIsCurrent
										  ,projectsourceid)
SELECT
		 (SELECT IFNULL(m.max_id, 1) FROM NUMBER_FOUNTAIN_dim_ora_producthierarchy m WHERE m.table_name = 'dim_ora_producthierarchy') + ROW_NUMBER() OVER(ORDER BY '')
		,t.producthierarchy
		,t.level1code
		,t.level2code
		,t.level3code
		,t.level4code
		,current_timestamp
		,1
		,(SELECT s.dim_projectsourceid FROM dim_projectsource s)
FROM (SELECT
             DISTINCT
			 c.gph_code AS producthierarchy
			,IFNULL(substring(c.gph_code,1,3),'Not Set') level1code
			,IFNULL(substring(c.gph_code,1,6),'Not Set') level2code
			,IFNULL(substring(c.gph_code,1,9),'Not Set') level3code
			,IFNULL(c.gph_code,'Not Set') level4code
         FROM ora_mtl_product_lowerhierarchy c ) t
	 LEFT JOIN dim_ora_producthierarchy d_ph ON t.producthierarchy = d_ph.producthierarchy
WHERE d_ph.producthierarchy IS NULL;
