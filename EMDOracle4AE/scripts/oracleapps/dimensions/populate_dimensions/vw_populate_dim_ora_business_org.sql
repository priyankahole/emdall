/*insert default row*/
INSERT INTO dim_ora_business_org
(
dim_ora_business_orgid,creation_date,last_update_date,date_from,name,date_to,last_updated_by,created_by,org_information_context,country,
address_line_1,address_line_2,town_or_city,postal_code,region_1,region_2,type,location_update_dt,orginfo_update_date,internal_external_flag,
org_type,organization_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
T.dim_ora_business_orgid,
T.creation_date,
T.last_update_date,
T.date_from,
T.name,
T.date_to,
T.last_updated_by,
T.created_by,
T.org_information_context,
T.country,
T.address_line_1,
T.address_line_2,
T.town_or_city,
T.postal_code,
T.region_1,
T.region_2,
T.type,
T.location_update_dt,
T.orginfo_update_date,
T.internal_external_flag,
T.org_type,
T.organization_id,
T.dw_insert_date,
T.dw_update_date,
T.rowstartdate,T.rowenddate,T.rowiscurrent,T.rowchangereason,
T.key_id,
T.source_id
 FROM
(SELECT
1 dim_ora_business_orgid,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
timestamp '1001-01-01 00:00:00.000000' DATE_FROM,
'Not Set' NAME,
timestamp '1001-01-01 00:00:00.000000' DATE_TO,
0 LAST_UPDATED_BY,
0 CREATED_BY,
'Not Set' ORG_INFORMATION_CONTEXT,
'Not Set' COUNTRY,
'Not Set' ADDRESS_LINE_1,
'Not Set' ADDRESS_LINE_2,
'Not Set' TOWN_OR_CITY,
'Not Set' POSTAL_CODE,
'Not Set' REGION_1,
'Not Set' REGION_2,
'Not Set' TYPE,
timestamp '1001-01-01 00:00:00.000000' LOCATION_UPDATE_DT,
timestamp '1001-01-01 00:00:00.000000' ORGINFO_UPDATE_DATE,
'Not Set' INTERNAL_EXTERNAL_FLAG,
'Not Set' org_type,
0 organization_id,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_business_org D
ON T.dim_ora_business_orgid = D.dim_ora_business_orgid
WHERE D.dim_ora_business_orgid is null;

/*initialize NUMBER_FOUNTAIN_dim_ora_business_org*/
delete from NUMBER_FOUNTAIN_dim_ora_business_org where table_name = 'dim_ora_business_org';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_business_org
SELECT 'dim_ora_business_org',IFNULL(MAX(dim_ora_business_orgid),0)
FROM dim_ora_business_org;

/*update dimension rows*/
UPDATE dim_ora_business_org T
SET
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.DATE_FROM = ifnull(S.DATE_FROM, timestamp '1001-01-01 00:00:00.000000'),
T.NAME = ifnull(S.NAME, 'Not Set'),
T.DATE_TO = ifnull(S.DATE_TO, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.ORG_INFORMATION_CONTEXT = ifnull(S.ORG_INFORMATION_CONTEXT, 'Not Set'),
T.COUNTRY = ifnull(S.COUNTRY, 'Not Set'),
T.ADDRESS_LINE_1 = ifnull(S.ADDRESS_LINE_1, 'Not Set'),
T.ADDRESS_LINE_2 = ifnull(S.ADDRESS_LINE_2, 'Not Set'),
T.TOWN_OR_CITY = ifnull(S.TOWN_OR_CITY, 'Not Set'),
T.POSTAL_CODE = ifnull(S.POSTAL_CODE, 'Not Set'),
T.REGION_1 = ifnull(S.REGION_1, 'Not Set'),
T.REGION_2 = ifnull(S.REGION_2, 'Not Set'),
T.TYPE = ifnull(S.TYPE, 'Not Set'),
T.LOCATION_UPDATE_DT = ifnull(S.LOCATION_UPDATE_DT, timestamp '1001-01-01 00:00:00.000000'),
T.ORGINFO_UPDATE_DATE = ifnull(S.ORGINFO_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.INTERNAL_EXTERNAL_FLAG = ifnull(S.INTERNAL_EXTERNAL_FLAG, 'Not Set'),
T.org_type = ifnull(S.org_type, 'Not Set'),
T.organization_id = S.organization_id,
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_business_org T, ORA_BUSINESS_ORG S
WHERE
CONVERT(VARCHAR(200),S.ORG_TYPE) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_business_org
(
dim_ora_business_orgid,creation_date,last_update_date,date_from,name,date_to,last_updated_by,created_by,org_information_context,country,
address_line_1,address_line_2,town_or_city,postal_code,region_1,region_2,type,location_update_dt,orginfo_update_date,internal_external_flag,
org_type,organization_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_business_org WHERE TABLE_NAME = 'dim_ora_business_org' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_business_orgid,
ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.DATE_FROM, timestamp '1001-01-01 00:00:00.000000') DATE_FROM,
ifnull(S.NAME, 'Not Set') NAME,
ifnull(S.DATE_TO, timestamp '1001-01-01 00:00:00.000000') DATE_TO,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.ORG_INFORMATION_CONTEXT, 'Not Set') ORG_INFORMATION_CONTEXT,
ifnull(S.COUNTRY, 'Not Set') COUNTRY,
ifnull(S.ADDRESS_LINE_1, 'Not Set') ADDRESS_LINE_1,
ifnull(S.ADDRESS_LINE_2, 'Not Set') ADDRESS_LINE_2,
ifnull(S.TOWN_OR_CITY, 'Not Set') TOWN_OR_CITY,
ifnull(S.POSTAL_CODE, 'Not Set') POSTAL_CODE,
ifnull(S.REGION_1, 'Not Set') REGION_1,
ifnull(S.REGION_2, 'Not Set') REGION_2,
ifnull(S.TYPE, 'Not Set') TYPE,
ifnull(S.LOCATION_UPDATE_DT, timestamp '1001-01-01 00:00:00.000000') LOCATION_UPDATE_DT,
ifnull(S.ORGINFO_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') ORGINFO_UPDATE_DATE,
ifnull(S.INTERNAL_EXTERNAL_FLAG, 'Not Set') INTERNAL_EXTERNAL_FLAG,
ifnull(S.org_type, 'Not Set') org_type,
S.organization_id,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
CONVERT(VARCHAR(200),S.ORG_TYPE) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_BUSINESS_ORG S LEFT JOIN dim_ora_business_org D ON
D.KEY_ID = CONVERT(VARCHAR(200),S.ORG_TYPE) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID)
WHERE D.KEY_ID is null;

/*Operatin Unit Organization LegelEntity*/
UPDATE dim_ora_business_org b
SET b.legal_entity = IFNULL(c.legal_entity,'Not Set')
FROM dim_ora_business_org b,ora_company_code c
WHERE b.organization_id = c.org_id
  AND b.legal_entity <> IFNULL(c.legal_entity,'Not Set');

UPDATE dim_ora_business_org b
SET b.cmg_number = IFNULL(c.cmg_number,0)
FROM dim_ora_business_org b,ora_company_code c
WHERE b.organization_id = c.org_id
  AND b.cmg_number <> IFNULL(c.cmg_number,0);

/*HR_Lega Organization LegelEntity*/
UPDATE dim_ora_business_org b
SET b.legal_entity = IFNULL(c.legal_entity,'Not Set')
FROM dim_ora_business_org b,ora_company_code c
WHERE b.organization_id = c.le_org_id
  AND b.legal_entity <> IFNULL(c.legal_entity,'Not Set');

UPDATE dim_ora_business_org b
SET b.cmg_number = IFNULL(c.cmg_number,0)
FROM dim_ora_business_org b,ora_company_code c
WHERE b.organization_id = c.le_org_id
  AND b.cmg_number <> IFNULL(c.cmg_number,0);

/*Inventory Organization LegalEntity*/
TRUNCATE TABLE orgid_invorgid_tmp;
INSERT INTO orgid_invorgid_tmp
SELECT
        DISTINCT
        s.ship_from_org_id --INV
       ,b.legal_entity
       ,t.org_id          --OPERATING_UNIT
       ,b.cmg_number
   FROM  (SELECT ship_from_org_id,MAX(last_update_date) m_date FROM ORA_OE_ORDER_HEADER_LINE GROUP BY ship_from_org_id ) s
        ,(SELECT org_id, ship_from_org_id,last_update_date m_date FROM ORA_OE_ORDER_HEADER_LINE ) t
        ,dim_ora_business_org b
   WHERE s.ship_from_org_id = t.ship_from_org_id
     AND s.m_date = t.m_date
     AND t.org_id = b.organization_id;

UPDATE dim_ora_business_org b
 SET b.legal_entity = IFNULL(s.legal_entity,'Not Set')
 FROM dim_ora_business_org b, orgid_invorgid_tmp s
 WHERE b.organization_id = s.ship_from_org_id
   AND b.legal_entity = 'Not Set'
   AND b.legal_entity <> IFNULL(s.legal_entity,'Not Set');

UPDATE dim_ora_business_org b
 SET b.cmg_number = IFNULL(s.cmg_number,0)
 FROM dim_ora_business_org b, orgid_invorgid_tmp s
 WHERE b.organization_id = s.ship_from_org_id
   AND b.cmg_number = 0
   AND b.cmg_number <> IFNULL(s.cmg_number,0);

/*Organization Code (PlantCode) from MTL_parameters*/
UPDATE dim_ora_business_org b
SET b.organization_code = d.organization_code
FROM dim_ora_business_org b, dim_ora_mtl_parameters d
WHERE b.organization_id = d.organization_id
  AND b.organization_code <> d.organization_code;

/*@Catalin M  - Jira 4133 - Country code from imported file*/

 update dim_ora_business_org a
 set a.org_country_name = ifnull(b.country,'Not Set')
from
dim_ora_business_org a,ora_add_countryname b
where b.plant_name = a.name
and a.org_country_name <> ifnull(b.country,'Not Set');

/* 14 April - Alina - Update Region */


update dim_ora_business_org b
set b.region=m.region
from dim_ora_business_org b,EMDTempoCC4.plant_mapping_country m
where
case when SUBSTR(b.country,1,10)='USA' then 'US'
else  SUBSTR(b.country,1,10) end=plant_country
and b.region<>m.region;


update dim_ora_business_org d
set
d.country=m.country ,
d.org_country_name=m.plant_country_name,
d.region=m.plant_region
 from dim_ora_business_org d,plant_mapping_oracle m
 where SUBSTR(d.name,1,30)=m.plant_name
 and (d.region='Not Set' or d.org_country_name='Not Set' or d.region='Not Set');


update dim_ora_business_org dp
set dp.Executive_Scope_flag=ifnull(p.executive, 'Not Set')
from dim_ora_business_org dp,EMDTempoCC4.plant_new_mapping p
where CASE WHEN dp.organization_code <> 'Not Set' THEN
dp.organization_code || ' - ' || SUBSTR(dp.name,1,30) ELSE SUBSTR(dp.name,1,30) END=p.plant_emd
and dp.organization_code=p.plantcode
and p.platform like '%Oracle%'
and dp.Executive_Scope_flag<>ifnull(p.executive, 'Not Set');
