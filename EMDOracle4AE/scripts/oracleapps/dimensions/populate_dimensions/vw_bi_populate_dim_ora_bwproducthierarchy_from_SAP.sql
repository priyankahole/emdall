/******************************************************************************************************************/
/*   Script         : bi_populate_bwproducthierarchy                                                              */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 25 Nov 2015                                                                                 */
/*   Description    : Populating script of dim_ora_bwproducthierarchy.                                                */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   25 Nov 2015         Cristian T      1.0               Creating the script.                                   */
/*   16 Feb 2016         Cristian T      1.1               Add logic for mrkhc_brand and mrkhc_strength           */
/******************************************************************************************************************/

/*set session authorization emdoracle4ae  -- stg v2
OPEN SCHEMA emdoracle4ae */
/*summary SELECT COUNT(*) FROM dim_ora_bwproducthierarchy;--9259 had the combination of lower + upper hierarchy + 1972 only has lowerhierarchy = 11231*/

/* Get data from SAP EU Project */
/*set session authorization emdtempocc4 -- stg v2
GRANT SELECT ON emdtempocc4.RSHIEDIR,emdtempocc4.HZ_UPHIER,emdtempocc4.BIC_TZ_UPHIER,emdtempocc4.dim_bwproducthierarchy,emdtempocc4.dim_producthierarchy TO emdoracle4ae
*/


TRUNCATE TABLE bic_tz_uphier;
INSERT INTO BIC_TZ_UPHIER SELECT * FROM emdtempocc4.BIC_TZ_UPHIER;


TRUNCATE TABLE rshiedir;
INSERT INTO RSHIEDIR SELECT * FROM emdtempocc4.RSHIEDIR;

TRUNCATE TABLE hz_uphier;
INSERT INTO HZ_UPHIER SELECT * FROM emdtempocc4.HZ_UPHIER;

/*
CREATE TABLE dim_ora_bwproducthierarchy AS SELECT * FROM emdtempocc4.dim_bwproducthierarchy WHERE dim_bwproducthierarchyID = 1
CREATE TABLE dim_ora_producthierarchy AS SELECT * FROM emdtempocc4.dim_producthierarchy WHERE dim_producthierarchyid = 1
*/

--MODIFY dim_ora_bwproducthierarchy TO TRUNCATED;



/* Part 0 - Inserting the default row if not exists */
INSERT INTO dim_ora_bwproducthierarchy
(
  dim_bwproducthierarchyid
)
SELECT 1
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 'x' FROM dim_ora_bwproducthierarchy x WHERE x.dim_bwproducthierarchyid = 1);


/* Create tmp Upper Hierarchy table */

DROP TABLE IF EXISTS tmp_bw_upperhierarchy1;
CREATE TABLE tmp_bw_upperhierarchy1
AS
SELECT
	h1.hz_uphier_nodename h1_hz_uphier_nodename
	,IFNULL(t1.txtlg,'Not Set') t1_txtlg
	,h2.hz_uphier_nodename h2_hz_uphier_nodename
	,IFNULL(t2.txtlg,'Not Set') t2_txtlg
	,h3.hz_uphier_nodename h3_hz_uphier_nodename
	,IFNULL(t3.txtlg,'Not Set') t3_txtlg
	,h4.hz_uphier_nodename h4_hz_uphier_nodename
	,IFNULL(t4.txtlg,'Not Set') t4_txtlg
	,h5.hz_uphier_nodename h5_hz_uphier_nodename
	,IFNULL(t5.txtlg,'Not Set') t5_txtlg
	,h6.hz_uphier_nodename h6_hz_uphier_nodename
	,IFNULL(t6.txtlg,'Not Set') t6_txtlg
	,h7.hz_uphier_nodename h7_hz_uphier_nodename
	,IFNULL(t7.txtlg,'Upper Product Hierarchy ' || LEFT(rshiedir_datefrom,4)) t7_txtlg
	,RIGHT(h1.hz_uphier_nodename,3) upperhiercode
	,TO_DATE(r.rshiedir_datefrom,'YYYYMMDD') upperhierstartdate
	,TO_DATE(r.rshiedir_dateto,'YYYYMMDD') upperhierenddate
	,r.rshiedir_hieid upperhierid
FROM RSHIEDIR r
	INNER JOIN HZ_UPHIER h1 ON h1.hz_uphier_hieid = r.rshiedir_hieid AND h1.hz_uphier_tlevel = 7
	INNER JOIN HZ_UPHIER h2 ON h2.hz_uphier_nodeid = h1.hz_uphier_parentid AND h2.hz_uphier_hieid = r.rshiedir_hieid
	INNER JOIN HZ_UPHIER h3 ON h3.hz_uphier_nodeid = h2.hz_uphier_parentid AND h3.hz_uphier_hieid = r.rshiedir_hieid
	INNER JOIN HZ_UPHIER h4 ON h4.hz_uphier_nodeid = h3.hz_uphier_parentid AND h4.hz_uphier_hieid = r.rshiedir_hieid
	INNER JOIN HZ_UPHIER h5 ON h5.hz_uphier_nodeid = h4.hz_uphier_parentid AND h5.hz_uphier_hieid = r.rshiedir_hieid
	INNER JOIN HZ_UPHIER h6 ON h6.hz_uphier_nodeid = h5.hz_uphier_parentid AND h6.hz_uphier_hieid = r.rshiedir_hieid
	INNER JOIN HZ_UPHIER h7 ON h7.hz_uphier_nodeid = h6.hz_uphier_parentid AND h7.hz_uphier_hieid = r.rshiedir_hieid
	LEFT join BIC_TZ_UPHIER t1 ON h1.hz_uphier_nodename = t1.bic_z_uphier AND TO_DATE(r.rshiedir_datefrom,'YYYYMMDD') BETWEEN t1.datefrom AND t1.dateto
	LEFT join BIC_TZ_UPHIER t2 ON h2.hz_uphier_nodename = t2.bic_z_uphier AND TO_DATE(r.rshiedir_datefrom,'YYYYMMDD') BETWEEN t2.datefrom AND t2.dateto
	LEFT join BIC_TZ_UPHIER t3 ON h3.hz_uphier_nodename = t3.bic_z_uphier AND TO_DATE(r.rshiedir_datefrom,'YYYYMMDD') BETWEEN t3.datefrom AND t3.dateto
	LEFT join BIC_TZ_UPHIER t4 ON h4.hz_uphier_nodename = t4.bic_z_uphier AND TO_DATE(r.rshiedir_datefrom,'YYYYMMDD') BETWEEN t4.datefrom AND t4.dateto
	LEFT join BIC_TZ_UPHIER t5 ON h5.hz_uphier_nodename = t5.bic_z_uphier AND TO_DATE(r.rshiedir_datefrom,'YYYYMMDD') BETWEEN t5.datefrom AND t5.dateto
	LEFT join BIC_TZ_UPHIER t6 ON h6.hz_uphier_nodename = t6.bic_z_uphier AND TO_DATE(r.rshiedir_datefrom,'YYYYMMDD') BETWEEN t6.datefrom AND t6.dateto
	LEFT join BIC_TZ_UPHIER t7 ON h7.hz_uphier_nodename = t7.bic_z_uphier AND TO_DATE(r.rshiedir_datefrom,'YYYYMMDD') BETWEEN t7.datefrom AND t7.dateto
where r.rshiedir_iobjnm = 'Z_UPHIER' AND r.rshiedir_hienm = 'ORGUPH01' AND r.rshiedir_version = 'A';


/* Part 1 - Update Codes AND descriptions */

UPDATE dim_ora_bwproducthierarchy d
SET version = IFNULL(b.h7_hz_uphier_nodename,'Not Set')
	,versiondesc = IFNULL(b.t7_txtlg,'Not Set')
	,businesssector = IFNULL(b.h6_hz_uphier_nodename,'Not Set')
	,businesssectordesc = IFNULL(b.t6_txtlg,'Not Set')
	,business = IFNULL(b.h5_hz_uphier_nodename,'Not Set')
	,businessdesc = IFNULL(b.t5_txtlg,'Not Set')
	,businessunit = IFNULL(b.h4_hz_uphier_nodename,'Not Set')
	,businessunitdesc = IFNULL(b.t4_txtlg,'Not Set')
	,businessfield = IFNULL(b.h3_hz_uphier_nodename,'Not Set')
	,businessfielddesc = IFNULL(b.t3_txtlg,'Not Set')
	,businessline = IFNULL(b.h2_hz_uphier_nodename,'Not Set')
	,businesslinedesc = IFNULL(b.t2_txtlg,'Not Set')
	,productgroup = IFNULL(b.h1_hz_uphier_nodename,'Not Set')
	,productgroupdesc = IFNULL(b.t1_txtlg,'Not Set')
	,dw_update_date = CURRENT_DATE
FROM dim_ora_bwproducthierarchy d, tmp_bw_upperhierarchy1 b
where d.upperhierarchycode = b.upperhiercode AND d.upperhierid = b.upperhierid;



/*Specifically for Oracle*/


UPDATE dim_ora_bwproducthierarchy d
SET maingroup = IFNULL(c.level1code,'Not Set')
	,uppergroup = IFNULL(c.level2code,'Not Set')
	,articlegroup = IFNULL(c.level3code,'Not Set')
	,internationalarticle = IFNULL(c.level4code,'Not Set')
	,dw_update_date = CURRENT_DATE
FROM dim_ora_bwproducthierarchy d, dim_ora_producthierarchy c
WHERE d.lowerhierarchycode = c.producthierarchy;


/* Part 2 - Inserting new rows */

TRUNCATE TABLE tmp_part_hiercodes;
INSERT INTO tmp_part_hiercodes(productgroupsbu,producthierarchy)
SELECT DISTINCT GlbProductGroup,producthierarchy FROM dim_ora_mdg_product WHERE GlbProductGroup <> 'Not Set' or producthierarchy <> 'Not Set';

DELETE FROM NUMBER_FOUNTAIN_dim_ora_bwproducthierarchy m
WHERE m.table_name = 'dim_ora_bwproducthierarchy';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_bwproducthierarchy
SELECT 'dim_ora_bwproducthierarchy',
       IFNULL(MAX(bw.dim_bwproducthierarchyid), IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_ora_bwproducthierarchy bw
WHERE bw.dim_bwproducthierarchyid <> 1;


TRUNCATE TABLE tmp_insert_dim;
INSERT INTO tmp_insert_dim
SELECT
	1 dim_bwproducthierarchyid
	,IFNULL(b.h7_hz_uphier_nodename,'Not Set') version
	,IFNULL(b.t7_txtlg,'Not Set') versiondesc
	,IFNULL(b.h6_hz_uphier_nodename,'Not Set')  businesssector
	,IFNULL(b.t6_txtlg,'Not Set') businesssectordesc
	,IFNULL(b.h5_hz_uphier_nodename,'Not Set') business
	,IFNULL(b.t5_txtlg,'Not Set') businessdesc
	,IFNULL(b.h4_hz_uphier_nodename,'Not Set') businessunit
	,IFNULL(b.t4_txtlg,'Not Set') businessunitdesc
	,IFNULL(b.h3_hz_uphier_nodename,'Not Set') businessfield
	,IFNULL(b.t3_txtlg,'Not Set') businessfielddesc
	,IFNULL(b.h2_hz_uphier_nodename,'Not Set') businessline
	,IFNULL(b.t2_txtlg,'Not Set') businesslinedesc
	,IFNULL(b.h1_hz_uphier_nodename,'Not Set') productgroup
	,IFNULL(b.t1_txtlg,'Not Set') productgroupdesc
	,IFNULL(c.level1code,'Not Set') maingroup
	,IFNULL(c.level1desc,'Not Set') maingroupdesc
	,IFNULL(c.level2code,'Not Set') uppergroup
	,IFNULL(c.level2desc,'Not Set') uppergroupdesc
	,IFNULL(c.level3code,'Not Set') articlegroup
	,IFNULL(c.level3desc,'Not Set') articlegroupdesc
	,IFNULL(c.level4code,'Not Set') internationalarticle
	,IFNULL(c.level4desc,'Not Set') internationalarticledesc
	,IFNULL(b.upperhiercode,'Not Set') upperhierarchycode
	,IFNULL(b.upperhierstartdate,'0001-01-01') upperhierstartdate
	,IFNULL(b.upperhierenddate,'9999-01-01') upperhierenddate
	,IFNULL(b.upperhierid,'Not Set') upperhierid
	,IFNULL(c.producthierarchy,'Not Set') lowerhierarchycode
FROM tmp_part_hiercodes a
     LEFT JOIN tmp_bw_upperhierarchy1 b ON h1_hz_uphier_nodename = case when substring(a.productgroupsbu,1,3)='SBU' then a.productgroupsbu
 else concat('SBU-',a.productgroupsbu) end
     LEFT JOIN dim_ora_producthierarchy c ON c.producthierarchy = a.producthierarchy;


INSERT INTO dim_ora_bwproducthierarchy
(
	dim_bwproducthierarchyid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,productgroup
	,productgroupdesc
	,maingroup
	,maingroupdesc
	,uppergroup
	,uppergroupdesc
	,articlegroup
	,articlegroupdesc
	,internationalarticle
	,internationalarticledesc
	,upperhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
	,lowerhierarchycode
)
SELECT
	(SELECT IFNULL(m.max_id, 1)
	FROM NUMBER_FOUNTAIN_dim_ora_bwproducthierarchy m
	WHERE m.table_name = 'dim_ora_bwproducthierarchy') + ROW_NUMBER() OVER(ORDER BY '') AS dim_bwproducthierarchyid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,productgroup
	,productgroupdesc
	,maingroup
	,maingroupdesc
	,uppergroup
	,uppergroupdesc
	,articlegroup
	,articlegroupdesc
	,internationalarticle
	,internationalarticledesc
	,upperhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
	,lowerhierarchycode
FROM tmp_insert_dim t
WHERE  NOT EXISTS (SELECT 1
                   FROM dim_ora_bwproducthierarchy bw
                   WHERE IFNULL(t.upperhierarchycode,'Not Set') = bw.upperhierarchycode AND IFNULL(t.upperhierid,'Not Set') = bw.upperhierid AND IFNULL(t.lowerhierarchycode,'Not Set') = bw.lowerhierarchycode);

DROP TABLE IF EXISTS tmp_bw_upperhierarchy1;


TRUNCATE TABLE tmp_part_hiercodes;
INSERT INTO tmp_part_hiercodes(producthierarchy)
SELECT DISTINCT producthierarchy from dim_ora_mdg_product WHERE producthierarchy <> 'Not Set';

DELETE FROM NUMBER_FOUNTAIN_dim_ora_bwproducthierarchy m
WHERE m.table_name = 'dim_ora_bwproducthierarchy';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_bwproducthierarchy
SELECT 'dim_ora_bwproducthierarchy',
       IFNULL(MAX(bw.dim_bwproducthierarchyid), IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_ora_bwproducthierarchy bw
WHERE bw.dim_bwproducthierarchyid <> 1;

INSERT INTO dim_ora_bwproducthierarchy
(
	dim_bwproducthierarchyid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,productgroup
	,productgroupdesc
	,maingroup
	,maingroupdesc
	,uppergroup
	,uppergroupdesc
	,articlegroup
	,articlegroupdesc
	,internationalarticle
	,internationalarticledesc
	,upperhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
	,lowerhierarchycode
)
SELECT (SELECT IFNULL(m.max_id, 1)
        FROM NUMBER_FOUNTAIN_dim_ora_bwproducthierarchy m
        WHERE m.table_name = 'dim_ora_bwproducthierarchy') + ROW_NUMBER() OVER(ORDER BY '') AS dim_bwproducthierarchyid
        ,'Not Set' version
        ,'Not Set' versiondesc
        ,'Not Set'  businesssector
        ,'Not Set' businesssectordesc
        ,'Not Set' business
        ,'Not Set' businessdesc
        ,'Not Set' businessunit
        ,'Not Set' businessunitdesc
        ,'Not Set' businessfield
        ,'Not Set' businessfielddesc
        ,'Not Set' businessline
        ,'Not Set' businesslinedesc
        ,'Not Set' productgroup
        ,'Not Set' productgroupdesc
        ,'Not Set' maingroup
        ,'Not Set' maingroupdesc
        ,'Not Set' uppergroup
        ,'Not Set' uppergroupdesc
        ,'Not Set' articlegroup
        ,'Not Set' articlegroupdesc
        ,'Not Set' internationalarticle
        ,'Not Set' internationalarticledesc
        ,'Not Set' upperhierarchycode
        ,'0001-01-01' upperhierstartdate
        ,'9999-01-01' upperhierenddate
        ,'Not Set' upperhierid
        ,IFNULL(a.producthierarchy,'Not Set')lowerhierarchycode
FROM tmp_part_hiercodes a
    -- INNER JOIN dim_ora_producthierarchy c ON c.producthierarchy = a.producthierarchy
WHERE NOT EXISTS (SELECT 1
                   FROM dim_ora_bwproducthierarchy bw
                   WHERE bw.upperhierarchycode = 'Not Set' AND bw.upperhierid ='Not Set' AND IFNULL(a.producthierarchy,'Not Set') = bw.lowerhierarchycode);



/* 16 Feb 2016 CristianT Start: Add logic for mrkhc_brand AND mrkhc_strength */

UPDATE dim_ora_bwproducthierarchy
SET mrkhc_brand = CASE
                    WHEN internationalarticledesc = 'Not Set' THEN 'Not Set'
                    ELSE LEFT(internationalarticledesc, LOCATE(internationalarticledesc, '_')-1)
                  END
WHERE businesssectordesc in ('HEALTHCARE','PHARMACEUTICALS');

DROP TABLE IF EXISTS tmp_mrkhc_strength;
CREATE TABLE tmp_mrkhc_strength
AS
SELECT businesssectordesc,
       internationalarticledesc,
       mrkhc_brand,
       IFNULL(CASE
                WHEN LOCATE(mrkhc_strength, '_') > LENGTH(mrkhc_strength) THEN mrkhc_strength
                ELSE mrkhc_brand || RIGHT(mrkhc_strength, LENGTH (mrkhc_strength) - LOCATE(mrkhc_strength, '_'))
              END, 'Not Set') mrkhc_strength
FROM (
SELECT distinct businesssectordesc,
       internationalarticledesc,
       mrkhc_brand,
       CASE
          WHEN internationalarticledesc = 'Not Set' THEN 'Not Set'
          WHEN LOCATE(internationalarticledesc, '_') > length(internationalarticledesc) THEN internationalarticledesc
          ELSE RIGHT(internationalarticledesc, (length(internationalarticledesc) - LOCATE(internationalarticledesc, '_')))
       END mrkhc_strength
FROM dim_ora_bwproducthierarchy
WHERE 1 = 1
      AND businesssectordesc in ('HEALTHCARE','PHARMACEUTICALS')
      AND internationalarticledesc <> 'Not Set'
) t;

UPDATE dim_ora_bwproducthierarchy dim
SET dim.mrkhc_strength = tmp.mrkhc_strength
FROM dim_ora_bwproducthierarchy dim, tmp_mrkhc_strength tmp
WHERE dim.businesssectordesc = tmp.businesssectordesc
      AND dim.internationalarticledesc = tmp.internationalarticledesc
      AND IFNULL(dim.mrkhc_strength, 'Not Set') <> tmp.mrkhc_strength;

DROP TABLE IF EXISTS tmp_mrkhc_strength;

UPDATE dim_ora_bwproducthierarchy dim SET dim.projectsourceid = 7;

UPDATE dim_ora_bwproducthierarchy SET bussinessnamesort =
CASE WHEN businessdesc = 'Applied Solutions' THEN 1
WHEN businessdesc = 'Research Solutions' THEN 2
WHEN businessdesc = 'Process Solutions' THEN 3
ELSE 0 END
where bussinessnamesort <> CASE WHEN businessdesc = 'Applied Solutions' THEN 1
WHEN businessdesc = 'Research Solutions' THEN 2
WHEN businessdesc = 'Process Solutions' THEN 3
ELSE 0 END;

UPDATE dim_ora_bwproducthierarchy
SET businesssectordesc='LIFE SCIENCE'
WHERE businesssectordesc='Life Science';

UPDATE dim_ora_bwproducthierarchy
set upperhierenddate=replace(upperhierenddate,'9999',year(upperhierstartdate))
from dim_ora_bwproducthierarchy
where upperhierenddate='9999-12-31';

/* 16 Feb 2016 CristianT End */
/* Cornelia add business details*/
update dim_ora_bwproducthierarchy
set business_ps = 'R'||'&'||'A'
where business in ('DIV-61','DIV-62')
and business_ps <>'R'||'&'||'A';

update dim_ora_bwproducthierarchy
set business_ps = 'PS'
where business in ('DIV-63')
and business_ps <>'PS';

update dim_ora_bwproducthierarchy
set businessname_ps = 'Research and Applied Solutions'
where business in ('DIV-61','DIV-62')
and businessname_ps <>'Research and Applied Solutions';

update dim_ora_bwproducthierarchy
set businessname_ps = 'Process Solutions'
where business in ('DIV-63')
and businessname_ps <>'Process Solutions';
