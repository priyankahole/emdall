/*insert default row*/
INSERT INTO dim_ora_org_gl_batches
(
dim_ora_org_gl_batchesid,
ACCT_PERIOD_ID,GL_BATCH_DATE,DESCRIPTION,LAST_UPDATE_LOGIN,ORGANIZATION_ID,GL_BATCH_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT T.* FROM
(SELECT
1 dim_ora_org_gl_batchesid,
0 ACCT_PERIOD_ID,
timestamp '1970-01-01 00:00:00.000000' GL_BATCH_DATE,
'Not Set' DESCRIPTION,
0 LAST_UPDATE_LOGIN,
0 ORGANIZATION_ID,
0 GL_BATCH_ID,
timestamp '1970-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '1970-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'Not Set' KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_org_gl_batches D
ON T.dim_ora_org_gl_batchesid = D.dim_ora_org_gl_batchesid
WHERE D.dim_ora_org_gl_batchesid IS NULL;

/*initialize NUMBER_FOUNTAIN_dim_ora_org_gl_batches*/
delete from NUMBER_FOUNTAIN_dim_ora_org_gl_batches where table_name = 'dim_ora_org_gl_batches';	

INSERT INTO NUMBER_FOUNTAIN_dim_ora_org_gl_batches
SELECT 'dim_ora_org_gl_batches',IFNULL(MAX(dim_ora_org_gl_batchesid),0)
FROM dim_ora_org_gl_batches;

/*update dimension rows*/
UPDATE dim_ora_org_gl_batches T
SET
T.ACCT_PERIOD_ID = ifnull(S.ACCT_PERIOD_ID, 0),
T.GL_BATCH_DATE = ifnull(S.GL_BATCH_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_org_gl_batches T, ora_org_gl_batches S
WHERE
T.KEY_ID = CONVERT(VARCHAR(200),ifnull(S.ORGANIZATION_ID,'Not Set')) ||'~'|| CONVERT(VARCHAR(200),ifnull(S.GL_BATCH_ID,'Not Set'));

/*insert new rows*/
INSERT INTO dim_ora_org_gl_batches
(
dim_ora_org_gl_batchesid,
ACCT_PERIOD_ID,GL_BATCH_DATE,DESCRIPTION,LAST_UPDATE_LOGIN,ORGANIZATION_ID,GL_BATCH_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_org_gl_batches WHERE TABLE_NAME = 'dim_ora_org_gl_batches' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_org_gl_batchesid,
ifnull(S.ACCT_PERIOD_ID, 0) ACCT_PERIOD_ID,
ifnull(S.GL_BATCH_DATE, timestamp '1970-01-01 00:00:00.000000') GL_BATCH_DATE,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
S.ORGANIZATION_ID,
S.GL_BATCH_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,

current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
CONVERT(VARCHAR(200),ifnull(S.ORGANIZATION_ID,'Not Set')) ||'~'|| CONVERT(VARCHAR(200),ifnull(S.GL_BATCH_ID,'Not Set')) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ora_org_gl_batches S LEFT JOIN dim_ora_org_gl_batches D
ON D.KEY_ID = CONVERT(VARCHAR(200),ifnull(S.ORGANIZATION_ID,'Not Set')) ||'~'|| CONVERT(VARCHAR(200),ifnull(S.GL_BATCH_ID,'Not Set'))
WHERE D.KEY_ID is null;
