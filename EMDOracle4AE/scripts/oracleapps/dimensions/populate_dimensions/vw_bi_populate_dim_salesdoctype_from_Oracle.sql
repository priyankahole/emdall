DELETE FROM emd586.dim_salesdocumenttype WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_salesdocumenttype
(
	 description
	,dim_salesdocumenttypeid
	,documenttype
	,dw_insert_date
	,dw_update_date
	,projectsourceid

)
SELECT 
         SUBSTR(description,1,20)
	    ,dim_ora_xacttypeid + ifnull(p.dim_projectsourceid * p.multiplier ,0) dim_ora_order_header_typeid 
	    ,SUBSTR(name,-2,2)
		,d.dw_insert_date
		,d.dw_update_date
		,p.dim_projectsourceid
FROM emdoracle4ae.dim_ora_xacttype d, emdoracle4ae.dim_projectsource p;


