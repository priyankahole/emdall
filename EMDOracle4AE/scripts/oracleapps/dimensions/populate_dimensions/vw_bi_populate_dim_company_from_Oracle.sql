DELETE FROM emd586.dim_company WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_company
(
	     dim_companyid
	    ,company
	    ,companycode
	    ,CompanyName
	    ,country		
		,projectsourceid
		,dw_insert_date
		,dw_update_date
		,rowstartdate
		,rowenddate
		,rowiscurrent
		,rowchangereason
)
SELECT 
         dim_ora_business_orgid + ifnull(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid 
	    ,cmg_number
        ,cmg_number
	    ,name
	    ,legal_entity		
		,p.dim_projectsourceid
		,d.dw_insert_date
		,d.dw_update_date
		,d.rowstartdate
		,d.rowenddate
		,d.rowiscurrent
		,d.rowchangereason
FROM emdoracle4ae.dim_ora_business_org d, emdoracle4ae.dim_projectsource p;

update emd586.Dim_Company
set Name2 = case when company != 'Not Set' then company || ' - ' || companyname else companyname end
where Name2 <> case when company != 'Not Set' then company || ' - ' || companyname else companyname end
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );