
DELETE FROM emd586.dim_unitofmeasure WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_unitofmeasure
(
	 dim_unitofmeasureid
    ,uom
	,rowstartdate
	,rowenddate
	,rowiscurrent
	,rowchangereason
	,dw_insert_date
	,dw_update_date
	,projectsourceid
)
SELECT
		 ifnull(s.dim_projectsourceid * s.multiplier,1)+  + ROW_NUMBER() OVER(order by '') dim_unitofmeasureid 
		,f_inv.uom       
		,current_timestamp rowstartdate
		,current_timestamp rowenddate
		,1 rowiscurrent
		,NULL rowchangereason
		,current_timestamp dw_insert_date
		,current_timestamp dw_update_date		
		,ifnull(s.dim_projectsourceid,1)
FROM 
    (SELECT DISTINCT uom FROM ( SELECT f.dd_transaction_uom_code uom FROM emdoracle4ae.fact_ora_invonhand f GROUP BY f.dd_transaction_uom_code UNION ALL SELECT dd_order_quantity_uom uom FROM fact_ora_sales_order_line GROUP BY dd_order_quantity_uom ) T) f_inv
    ,emdoracle4ae.dim_projectsource s;
