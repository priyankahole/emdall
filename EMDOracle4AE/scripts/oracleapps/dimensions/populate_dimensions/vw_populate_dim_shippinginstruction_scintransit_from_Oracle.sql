
INSERT INTO EMD586.DIM_SHIPPINGINSTRUCTION
(
 DIM_SHIPPINGINSTRUCTIONID
,SHIPPINGINSTRUCTIONCODE
,SHIPPINGINSTRUCTIONDESC
,DW_INSERT_DATE
,DW_UPDATE_DATE
,PROJECTOURCEID
)

SELECT
 dim_ora_shippinginstruction_instrid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
,ifnull(shipping_instruction_code,'Not Set')
,'Not set'
,CURRENT_TIMESTAMP DW_INSERT_DATE
,CURRENT_TIMESTAMP DW_UPDATE_DATE
,p.dim_projectsourceid

FROM dim_ora_shippinginstruction_instr,DIM_projectsource p;

