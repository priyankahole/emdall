/*insert default row*/
INSERT INTO dim_ora_wip_entities
(
dim_ora_wip_entitiesid,
WIP_ENTITY_ID,ORGANIZATION,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,REQUEST_ID,PROGRAM_APPLICATION_ID,PROGRAM_ID,PROGRAM_UPDATE_DATE,WIP_ENTITY_NAME,ENTITY_TYPE,DESCRIPTION,PRIMARY_ITEM,GEN_OBJECT_ID,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT T.* FROM
(SELECT
1 dim_ora_wip_entitiesid,
0 WIP_ENTITY_ID,
'Not Set' ORGANIZATION,
timestamp '1970-01-01 00:00:00.000000' LAST_UPDATE_DATE,
'Not Set' LAST_UPDATED_BY,
timestamp '1970-01-01 00:00:00.000000' CREATION_DATE,
'Not Set' CREATED_BY,
0 LAST_UPDATE_LOGIN,
0 REQUEST_ID,
0 PROGRAM_APPLICATION_ID,
0 PROGRAM_ID,
timestamp '1970-01-01 00:00:00.000000' PROGRAM_UPDATE_DATE,
'Not Set' WIP_ENTITY_NAME,
'Not Set' ENTITY_TYPE,
'Not Set' DESCRIPTION,
'Not Set' PRIMARY_ITEM,
0 GEN_OBJECT_ID,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_wip_entities D
ON T.dim_ora_wip_entitiesid = D.dim_ora_wip_entitiesid
WHERE D.dim_ora_wip_entitiesid IS NULL;

/*initialize NUMBER_FOUNTAIN_dim_ora_wip_entities*/
delete from NUMBER_FOUNTAIN_dim_ora_wip_entities where table_name = 'dim_ora_wip_entities';	

INSERT INTO NUMBER_FOUNTAIN_dim_ora_wip_entities
SELECT 'dim_ora_wip_entities',IFNULL(MAX(dim_ora_wip_entitiesid),0)
FROM dim_ora_wip_entities;

/*update dimension rows*/
UPDATE dim_ora_wip_entities T
SET
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.REQUEST_ID = ifnull(S.REQUEST_ID, 0),
T.PROGRAM_APPLICATION_ID = ifnull(S.PROGRAM_APPLICATION_ID, 0),
T.PROGRAM_ID = ifnull(S.PROGRAM_ID, 0),
T.PROGRAM_UPDATE_DATE = ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.WIP_ENTITY_NAME = ifnull(S.WIP_ENTITY_NAME, 'Not Set'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.GEN_OBJECT_ID = ifnull(S.GEN_OBJECT_ID, 0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_wip_entities T	, ora_wip_entities S
WHERE
T.KEY_ID = S.WIP_ENTITY_ID;

/*insert new rows*/
INSERT INTO dim_ora_wip_entities
(
dim_ora_wip_entitiesid,
WIP_ENTITY_ID,ORGANIZATION,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,REQUEST_ID,PROGRAM_APPLICATION_ID,PROGRAM_ID,PROGRAM_UPDATE_DATE,WIP_ENTITY_NAME,ENTITY_TYPE,DESCRIPTION,PRIMARY_ITEM,GEN_OBJECT_ID,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_wip_entities WHERE TABLE_NAME = 'dim_ora_wip_entities' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_wip_entitiesid,
S.WIP_ENTITY_ID,
'Not Set' ORGANIZATION,
ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') LAST_UPDATE_DATE,
'Not Set' LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000') CREATION_DATE,
'Not Set' CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.REQUEST_ID, 0) REQUEST_ID,
ifnull(S.PROGRAM_APPLICATION_ID, 0) PROGRAM_APPLICATION_ID,
ifnull(S.PROGRAM_ID, 0) PROGRAM_ID,
ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') PROGRAM_UPDATE_DATE,
ifnull(S.WIP_ENTITY_NAME, 'Not Set') WIP_ENTITY_NAME,
'Not Set' ENTITY_TYPE,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
'Not Set' PRIMARY_ITEM,
ifnull(S.GEN_OBJECT_ID, 0) GEN_OBJECT_ID,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.WIP_ENTITY_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ora_wip_entities S LEFT JOIN dim_ora_wip_entities D
ON D.KEY_ID = S.WIP_ENTITY_ID
WHERE D.KEY_ID is null;

/*ORGANIZATION*/
UPDATE dim_ora_wip_entities F
SET
F.ORGANIZATION = D.NAME,
DW_UPDATE_DATE = current_timestamp
FROM dim_ora_wip_entities F, ora_wip_entities S , DIM_ORA_BUSINESS_ORG D
WHERE F.KEY_ID = S.WIP_ENTITY_ID
and 'INV' ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID
AND F.ORGANIZATION <> D.NAME
and d.rowiscurrent = 1;

/*PRIMARY_ITEM*/
UPDATE dim_ora_wip_entities F
SET
F.PRIMARY_ITEM = D.DESCRIPTION,
DW_UPDATE_DATE = current_timestamp
FROM dim_ora_wip_entities F, ora_wip_entities S , DIM_ORA_INVPRODUCT D
WHERE F.KEY_ID = S.WIP_ENTITY_ID
and CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'|| CONVERT(VARCHAR(200),S.PRIMARY_ITEM_ID) = D.KEY_ID
AND F.PRIMARY_ITEM <> D.DESCRIPTION
and d.rowiscurrent = 1;

/*LAST_UPDATED_BY*/
UPDATE dim_ora_wip_entities F
SET
F.LAST_UPDATED_BY = D.USER_NAME,
DW_UPDATE_DATE = current_timestamp
FROM dim_ora_wip_entities F, ora_wip_entities S,DIM_ORA_FNDUSER D
WHERE F.KEY_ID = S.WIP_ENTITY_ID
and S.LAST_UPDATED_BY = D.KEY_ID
and d.rowiscurrent = 1
and F.LAST_UPDATED_BY <> D.USER_NAME;

/*CREATED_BY*/
UPDATE dim_ora_wip_entities F
SET
F.CREATED_BY = D.USER_NAME,
DW_UPDATE_DATE = current_timestamp
FROM dim_ora_wip_entities F, ora_wip_entities S, DIM_ORA_FNDUSER D
WHERE F.KEY_ID = S.WIP_ENTITY_ID
and S.CREATED_BY = D.KEY_ID
and d.rowiscurrent = 1
and F.CREATED_BY <> D.USER_NAME;

TRUNCATE TABLE LKP_TMP;
INSERT INTO LKP_TMP
select D.LOOKUP_CODE ,D.LOOKUP_TYPE,d.rowiscurrent,D.MEANING
FROM DIM_ORA_FND_LOOKUP D
where  D.LOOKUP_TYPE='WIP_ENTITIES';

/*ENTITY_TYPE*/
UPDATE dim_ora_wip_entities F
SET
F.ENTITY_TYPE = D.MEANING,
DW_UPDATE_DATE = current_timestamp
FROM dim_ora_wip_entities F, ora_wip_entities S , LKP_TMP D
WHERE F.KEY_ID = S.WIP_ENTITY_ID
and S.ENTITY_TYPE = D.LOOKUP_CODE
AND D.LOOKUP_TYPE='WIP_ENTITIES'
and d.rowiscurrent = 1
and F.ENTITY_TYPE <> D.MEANING;
