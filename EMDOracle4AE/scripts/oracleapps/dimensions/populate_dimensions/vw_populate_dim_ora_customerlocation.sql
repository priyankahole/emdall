/*insert default row*/
INSERT INTO dim_ora_customerlocation
(
dim_ora_customerlocationid,
party_name,party_id,cust_account_id,account_number,account_name,site_use_code,address1,address2,
address3,address4,city,"state",postal_code,county,territory_short_name,acctschannel,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_customerlocationid,
'Not Set' PARTY_NAME,
0 PARTY_ID,
0 CUST_ACCOUNT_ID,
'Not Set' ACCOUNT_NUMBER,
'Not Set' ACCOUNT_NAME,
'Not Set' SITE_USE_CODE,
'Not Set' ADDRESS1,
'Not Set' ADDRESS2,
'Not Set' ADDRESS3,
'Not Set' ADDRESS4,
'Not Set' CITY,
'Not Set' "state",
'Not Set' POSTAL_CODE,
'Not Set' COUNTY,
'Not Set' TERRITORY_SHORT_NAME,
'Not Set' ACCTSCHANNEL,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_customerlocation D
ON T.dim_ora_customerlocationid = D.dim_ora_customerlocationid
WHERE T.dim_ora_customerlocationid is null;

/*initialize NUMBER_FOUNTAIN_dim_ora_customerlocation*/
delete from NUMBER_FOUNTAIN_dim_ora_customerlocation where table_name = 'dim_ora_customerlocation';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_customerlocation
SELECT 'dim_ora_customerlocation',IFNULL(MAX(dim_ora_customerlocationid),0)
FROM dim_ora_customerlocation;

/*update dimension rows*/
UPDATE dim_ora_customerlocation T
SET
T.PARTY_NAME = ifnull(S.PARTY_NAME, 'Not Set'),
T.PARTY_ID = ifnull(S.PARTY_ID, 0),
T.CUST_ACCOUNT_ID = ifnull(S.CUST_ACCOUNT_ID, 0),
T.ACCOUNT_NUMBER = ifnull(S.ACCOUNT_NUMBER, 'Not Set'),
T.ACCOUNT_NAME = ifnull(S.ACCOUNT_NAME, 'Not Set'),
T.SITE_USE_CODE = ifnull(S.SITE_USE_CODE, 'Not Set'),
T.ADDRESS1 = ifnull(S.ADDRESS1, 'Not Set'),
T.ADDRESS2 = ifnull(S.ADDRESS2, 'Not Set'),
T.ADDRESS3 = ifnull(S.ADDRESS3, 'Not Set'),
T.ADDRESS4 = ifnull(S.ADDRESS4, 'Not Set'),
T.CITY = ifnull(S.CITY, 'Not Set'),
T."state" = ifnull(S."state", 'Not Set'),
T.POSTAL_CODE = ifnull(S.POSTAL_CODE, 'Not Set'),
T.COUNTY = ifnull(S.COUNTY, 'Not Set'),
T.TERRITORY_SHORT_NAME = ifnull(S.TERRITORY_SHORT_NAME, 'Not Set'),
T.ACCTSCHANNEL = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.DW_UPDATE_DATE = current_timestamp,
T.rowiscurrent = 1,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_customerlocation T, ORA_HZ_CUSTLOCATION S
WHERE S.SITE_USE_ID  = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_customerlocation
(
dim_ora_customerlocationid,
party_name,party_id,cust_account_id,account_number,account_name,site_use_code,address1,address2,
address3,address4,city,"state",postal_code,county,territory_short_name,acctschannel,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_customerlocation WHERE TABLE_NAME = 'dim_ora_customerlocation' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_customerlocationid,
ifnull(S.PARTY_NAME, 'Not Set') PARTY_NAME,
ifnull(S.PARTY_ID, 0) PARTY_ID,
ifnull(S.CUST_ACCOUNT_ID, 0) CUST_ACCOUNT_ID,
ifnull(S.ACCOUNT_NUMBER, 'Not Set') ACCOUNT_NUMBER,
ifnull(S.ACCOUNT_NAME, 'Not Set') ACCOUNT_NAME,
ifnull(S.SITE_USE_CODE, 'Not Set') SITE_USE_CODE,
ifnull(S.ADDRESS1, 'Not Set') ADDRESS1,
ifnull(S.ADDRESS2, 'Not Set') ADDRESS2,
ifnull(S.ADDRESS3, 'Not Set') ADDRESS3,
ifnull(S.ADDRESS4, 'Not Set') ADDRESS4,
ifnull(S.CITY, 'Not Set') CITY,
ifnull(S."state", 'Not Set') "state",
ifnull(S.POSTAL_CODE, 'Not Set') POSTAL_CODE,
ifnull(S.COUNTY, 'Not Set') COUNTY,
ifnull(S.TERRITORY_SHORT_NAME, 'Not Set') TERRITORY_SHORT_NAME,
ifnull(S.ATTRIBUTE1, 'Not Set') ACCTSCHANNEL,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.SITE_USE_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_HZ_CUSTLOCATION S LEFT JOIN dim_ora_customerlocation D ON
D.KEY_ID = S.SITE_USE_ID
WHERE D.KEY_ID is null;

/*10 may 2016 Cornelia add LOCATION */
UPDATE dim_ora_customerlocation T
SET
T.LOCATION = ifnull(S.LOCATION, 'Not Set'),
T.DW_UPDATE_DATE = current_timestamp,
T.rowiscurrent = 1,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_customerlocation T, ORA_HZ_CUSTLOCATION S
WHERE S.SITE_USE_ID  = T.KEY_ID
and T.LOCATION <> ifnull(S.LOCATION, 'Not Set');

/*13 may 2016 Cornelia add dd_attribute3 */
UPDATE dim_ora_customerlocation T
SET
T.dd_attribute3 = ifnull(S.attribute3, 'Not Set'),
T.DW_UPDATE_DATE = current_timestamp,
T.rowiscurrent = 1,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_customerlocation T, ORA_HZ_CUSTLOCATION S
WHERE S.SITE_USE_ID  = T.KEY_ID
and T.dd_attribute3 <> ifnull(S.attribute3, 'Not Set');


UPDATE dim_ora_customerlocation T
SET
T.country = ifnull(S.country, 'Not Set'),
T.DW_UPDATE_DATE = current_timestamp,
T.rowiscurrent = 1,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_customerlocation T, ORA_HZ_CUSTLOCATION S
WHERE S.SITE_USE_ID  = T.KEY_ID
and T.country <> ifnull(S.country, 'Not Set');


UPDATE dim_ora_customerlocation T
SET
T.city = upper(T.city);

/*
update dim_ora_customerlocation
set gsa=sold_to_gsa
from dim_ora_customerlocation,EMDTempoCC4.customer_gsa_mapping
where key_id=sold_to_party
and platform='ORACLE'
and gsa<>sold_to_gsa */


MERGE INTO dim_ora_customerlocation D
USING
(
SELECT key_id,MAX(sold_to_gsa) AS GSA
FROM dim_ora_customerlocation,EMDTempoCC4.customer_gsa_mapping
where key_id=sold_to_party
and platform='ORACLE'
and gsa<>sold_to_gsa
GROUP BY key_id
) X
ON
(X.key_id  = D.key_id)
WHEN MATCHED THEN
UPDATE SET D.GSA = X.GSA
WHERE D.GSA <> X.GSA;

update dim_ora_customerlocation c
set c.gsa_all_products=cc.gsa_all_products
from dim_ora_customerlocation c,EMDTempoCC4.GSA_MAPPING_NEW_FILED cc
where to_char(c.key_id)=to_char(cc.customer_number)
and c.gsa_all_products<>cc.gsa_all_products;

update dim_ora_customerlocation c
set c.country_region=m.region
from dim_ora_customerlocation c,EMDTempoCC4.plant_mapping_country m
where SUBSTR(c.country,1,10)=m.plant_country
and c.country_region<>m.region;


update dim_ora_customerlocation c
set c.country_region=m.region
from dim_ora_customerlocation c,EMDTempoCC4.countr_mapping_20171212 m
where trim(SUBSTR(c.country,1,2))=trim(m.country)
and c.country_region<>m.region;
