/*set session authorization oracle_data*/
/*select * from dim_ora_mtl_trx_reasons*/
/*insert default row*/
INSERT INTO dim_ora_mtl_trx_reasons
(
dim_ora_mtl_trx_reasonsid,
reason_context_code,attribute5,attribute6,attribute7,attribute8,attribute9,attribute10,attribute11,attribute12,attribute13,attribute14,
attribute15,attribute_category,workflow_name,workflow_display_name,workflow_process,workflow_display_process,reason_type,
reason_type_display,reason_id,last_update_date,last_updated_by,creation_date,created_by,last_update_login,reason_name,description,
disable_date,attribute1,attribute2,attribute3,attribute4,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_mtl_trx_reasonsid,
'Not Set' REASON_CONTEXT_CODE,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE8,
'Not Set' ATTRIBUTE9,
'Not Set' ATTRIBUTE10,
'Not Set' ATTRIBUTE11,
'Not Set' ATTRIBUTE12,
'Not Set' ATTRIBUTE13,
'Not Set' ATTRIBUTE14,
'Not Set' ATTRIBUTE15,
'Not Set' ATTRIBUTE_CATEGORY,
'Not Set' WORKFLOW_NAME,
'Not Set' WORKFLOW_DISPLAY_NAME,
'Not Set' WORKFLOW_PROCESS,
'Not Set' WORKFLOW_DISPLAY_PROCESS,
0 REASON_TYPE,
'Not Set' REASON_TYPE_DISPLAY,
0 REASON_ID,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' REASON_NAME,
'Not Set' DESCRIPTION,
timestamp '1001-01-01 00:00:00.000000' DISABLE_DATE,
'Not Set' ATTRIBUTE1,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE4,
timestamp '1001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_mtl_trx_reasons D
ON T.dim_ora_mtl_trx_reasonsid = D.dim_ora_mtl_trx_reasonsid
WHERE D.dim_ora_mtl_trx_reasonsid is null;

/*initialize NUMBER_FOUNTAIN_dim_ora_mtl_trx_reasons*/
delete from NUMBER_FOUNTAIN_dim_ora_mtl_trx_reasons where table_name = 'dim_ora_mtl_trx_reasons';

INSERT INTO NUMBER_FOUNTAIN_dim_ora_mtl_trx_reasons
SELECT 'dim_ora_mtl_trx_reasons',IFNULL(MAX(dim_ora_mtl_trx_reasonsid),0)
FROM dim_ora_mtl_trx_reasons;

/*update dimension rows*/
UPDATE dim_ora_mtl_trx_reasons T
SET
T.REASON_CONTEXT_CODE = ifnull(S.REASON_CONTEXT_CODE, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.ATTRIBUTE9 = ifnull(S.ATTRIBUTE9, 'Not Set'),
T.ATTRIBUTE10 = ifnull(S.ATTRIBUTE10, 'Not Set'),
T.ATTRIBUTE11 = ifnull(S.ATTRIBUTE11, 'Not Set'),
T.ATTRIBUTE12 = ifnull(S.ATTRIBUTE12, 'Not Set'),
T.ATTRIBUTE13 = ifnull(S.ATTRIBUTE13, 'Not Set'),
T.ATTRIBUTE14 = ifnull(S.ATTRIBUTE14, 'Not Set'),
T.ATTRIBUTE15 = ifnull(S.ATTRIBUTE15, 'Not Set'),
T.ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set'),
T.WORKFLOW_NAME = ifnull(S.WORKFLOW_NAME, 'Not Set'),
T.WORKFLOW_DISPLAY_NAME = ifnull(S.WORKFLOW_DISPLAY_NAME, 'Not Set'),
T.WORKFLOW_PROCESS = ifnull(S.WORKFLOW_PROCESS, 'Not Set'),
T.WORKFLOW_DISPLAY_PROCESS = ifnull(S.WORKFLOW_DISPLAY_PROCESS, 'Not Set'),
T.REASON_TYPE = ifnull(S.REASON_TYPE, 0),
T.REASON_TYPE_DISPLAY = ifnull(S.REASON_TYPE_DISPLAY, 'Not Set'),
T.REASON_ID = ifnull(S.REASON_ID, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.REASON_NAME = ifnull(S.REASON_NAME, 'Not Set'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.DISABLE_DATE = ifnull(S.DISABLE_DATE, timestamp '1001-01-01 00:00:00.000000'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_mtl_trx_reasons T, ORA_MTL_TRX_REASONS S
WHERE T.KEY_ID = S.REASON_ID;

/*insert new rows*/
INSERT INTO dim_ora_mtl_trx_reasons
(
dim_ora_mtl_trx_reasonsid,
reason_context_code,attribute5,attribute6,attribute7,attribute8,attribute9,attribute10,attribute11,attribute12,attribute13,attribute14,
attribute15,attribute_category,workflow_name,workflow_display_name,workflow_process,workflow_display_process,reason_type,
reason_type_display,reason_id,last_update_date,last_updated_by,creation_date,created_by,last_update_login,reason_name,description,
disable_date,attribute1,attribute2,attribute3,attribute4,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_mtl_trx_reasons WHERE TABLE_NAME = 'dim_ora_mtl_trx_reasons' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_mtl_trx_reasonsid,
ifnull(S.REASON_CONTEXT_CODE, 'Not Set') REASON_CONTEXT_CODE,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.ATTRIBUTE9, 'Not Set') ATTRIBUTE9,
ifnull(S.ATTRIBUTE10, 'Not Set') ATTRIBUTE10,
ifnull(S.ATTRIBUTE11, 'Not Set') ATTRIBUTE11,
ifnull(S.ATTRIBUTE12, 'Not Set') ATTRIBUTE12,
ifnull(S.ATTRIBUTE13, 'Not Set') ATTRIBUTE13,
ifnull(S.ATTRIBUTE14, 'Not Set') ATTRIBUTE14,
ifnull(S.ATTRIBUTE15, 'Not Set') ATTRIBUTE15,
ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set') ATTRIBUTE_CATEGORY,
ifnull(S.WORKFLOW_NAME, 'Not Set') WORKFLOW_NAME,
ifnull(S.WORKFLOW_DISPLAY_NAME, 'Not Set') WORKFLOW_DISPLAY_NAME,
ifnull(S.WORKFLOW_PROCESS, 'Not Set') WORKFLOW_PROCESS,
ifnull(S.WORKFLOW_DISPLAY_PROCESS, 'Not Set') WORKFLOW_DISPLAY_PROCESS,
ifnull(S.REASON_TYPE, 0) REASON_TYPE,
ifnull(S.REASON_TYPE_DISPLAY, 'Not Set') REASON_TYPE_DISPLAY,
ifnull(S.REASON_ID, 0) REASON_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '1001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '1001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.REASON_NAME, 'Not Set') REASON_NAME,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.DISABLE_DATE, timestamp '1001-01-01 00:00:00.000000') DISABLE_DATE,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.REASON_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_MTL_TRX_REASONS S LEFT JOIN dim_ora_mtl_trx_reasons D ON
D.KEY_ID = S.REASON_ID
WHERE D.KEY_ID is null;
