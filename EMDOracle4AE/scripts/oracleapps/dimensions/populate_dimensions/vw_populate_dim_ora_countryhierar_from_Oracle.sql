
DELETE FROM emd586.dim_countryhierar WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


INSERT INTO emd586.dim_countryhierar
(
 DIM_COUNTRYHIERARID
,COUNTRY	
,COUNTRYNAME
,TOTALAR	
,REGIONAR1	
,REGIONAR2	
,REGIONAR3	
,REGIONAR4
,DW_INSERT_DATE	
,DW_UPDATE_DATE	
,PROJECTSOURCEID 
)

SELECT 

DIM_COUNTRYHIERARID + IFNULL(s.dim_projectsourceid * s.multiplier ,0) DIM_COUNTRYHIERARID
,COUNTRY	
,COUNTRYNAME
,TOTALAR	
,REGIONAR1	
,REGIONAR2	
,REGIONAR3	
,REGIONAR4
,ch.DW_INSERT_DATE	
,ch.DW_UPDATE_DATE	
,s.dim_projectsourceid 

from emdoracle4ae.dim_ora_countryhierar ch, emdoracle4ae.dim_projectsource s;
