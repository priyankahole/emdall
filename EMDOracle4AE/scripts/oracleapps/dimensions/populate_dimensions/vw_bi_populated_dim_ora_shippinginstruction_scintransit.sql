
TRUNCATE TABLE intransit_temp;
INSERT INTO intransit_temp
SELECT DISTINCT FREIGHT_CARRIER_CODE FROM ORA_MTL_INTRANSIT_SCURVE;

DELETE FROM NUMBER_FOUNTAIN_dim_ora_shippinginstruction WHERE table_name = 'dim_ora_shippinginstruction';
INSERT INTO NUMBER_FOUNTAIN_dim_ora_shippinginstruction
SELECT 'dim_ora_shippinginstruction',IFNULL(MAX(dim_ora_shippinginstructionid),0)
FROM dim_ora_shippinginstruction;


DELETE FROM dim_ora_shippinginstruction_instr;

INSERT INTO dim_ora_shippinginstruction_instr
(
dim_ora_shippinginstruction_instrid ,
shipping_instruction_code
)
SELECT (SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_shippinginstruction WHERE TABLE_NAME = 'dim_ora_shippinginstruction')
+ ROW_NUMBER() OVER (order by '') dim_ora_shippinginstruction_instrid
,IFNULL(FREIGHT_CARRIER_CODE,'Not Set')


FROM intransit_temp;
