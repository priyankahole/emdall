TRUNCATE TABLE ora_s_curve_final_temp;
INSERT INTO ora_s_curve_final_temp
SELECT DISTINCT ORGANIZATION_CODE FROM ora_s_curve;

DELETE FROM NUMBER_FOUNTAIN_dim_ora_vendor_purchasing WHERE table_name = 'dim_ora_vendor_purchasing';
INSERT INTO NUMBER_FOUNTAIN_dim_ora_vendor_purchasing
SELECT 'dim_ora_vendor_purchasing',IFNULL(MAX(dim_ora_vendor_purchasingid),0)
FROM dim_ora_vendor_purchasing;

DELETE FROM dim_ora_vendor_purchasing WHERE dim_ora_vendor_purchasingid<>1;

INSERT INTO dim_ora_vendor_purchasing
(
dim_ora_vendor_purchasingid,
vendor_code
)
SELECT (SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_vendor_purchasing WHERE TABLE_NAME = 'dim_ora_vendor_purchasing')+ ROW_NUMBER() OVER (order by '') dim_ora_vendor_purchasingid
,from_org

FROM ora_s_curve_final_temp;
