TRUNCATE TABLE temp_vendor_intransit;
INSERT INTO temp_vendor_intransit
SELECT DISTINCT MP_FROM_ORG_CODE from ORA_MTL_INTRANSIT_SCURVE;

DELETE FROM NUMBER_FOUNTAIN_dim_ora_vendor_purchasing WHERE table_name = 'dim_ora_vendor_purchasing';
INSERT INTO NUMBER_FOUNTAIN_dim_ora_vendor_purchasing
SELECT 'dim_ora_vendor_purchasing',IFNULL(MAX(dim_ora_vendor_purchasingid),0)
FROM dim_ora_vendor_purchasing;


DELETE FROM dim_ora_vendor_purchasing_intr;

INSERT INTO dim_ora_vendor_purchasing_intr
(
dim_ora_vendor_purchasing_intrid,
vendor_code
)
SELECT (SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_vendor_purchasing WHERE TABLE_NAME = 'dim_ora_vendor_purchasing')+ ROW_NUMBER() OVER (order by '') dim_ora_vendor_purchasing_intrid
,MP_FROM_ORG_CODE

FROM temp_vendor_intransit;
