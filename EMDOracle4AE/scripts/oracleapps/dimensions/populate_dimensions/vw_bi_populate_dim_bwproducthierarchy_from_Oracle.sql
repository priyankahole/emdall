
DELETE FROM emd586.dim_bwproducthierarchy WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

INSERT INTO emd586.dim_bwproducthierarchy (
	 dim_bwproducthierarchyid
	,dw_insert_date
	,dw_update_date
	,projectsourceid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,productgroup
	,productgroupdesc
	,maingroup
	,maingroupdesc
	,uppergroup
	,uppergroupdesc
	,articlegroup
	,articlegroupdesc
	,internationalarticle
	,internationalarticledesc
	,upperhierarchycode
	,lowerhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
	,mrkhc_brand
	,mrkhc_strength
	,bussinessnamesort
	,businessname_ps
        ,business_ps
)
SELECT 
     dim_bwproducthierarchyid
	,dw_insert_date
	,dw_update_date
	,projectsourceid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,productgroup
	,productgroupdesc
	,maingroup
	,maingroupdesc
	,uppergroup
	,uppergroupdesc
	,articlegroup
	,articlegroupdesc
	,internationalarticle
	,internationalarticledesc
	,upperhierarchycode
	,lowerhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
	,mrkhc_brand
	,mrkhc_strength
	,bussinessnamesort
	,businessname_ps
        ,business_ps
FROM emdoracle4ae.dim_ora_bwproducthierarchy
WHERE dim_bwproducthierarchyid <> 1;

update EMD586.dim_bwproducthierarchy
set businesslinedescription = case when businessline != 'Not Set' then businessline ||  ' - ' || businesslinedesc else businesslinedesc end
where businesslinedescription <> case when businessline != 'Not Set' then businessline ||  ' - ' || businesslinedesc else businesslinedesc end
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update EMD586.dim_bwproducthierarchy
set businessfielddescription = case when businessfield != 'Not Set' then businessfield ||  ' - ' || businessfielddesc else businessfielddesc end
where businessfielddescription <> case when businessfield != 'Not Set' then businessfield ||  ' - ' || businessfielddesc else businessfielddesc end
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update EMD586.dim_bwproducthierarchy
set mrkhc_brand = upper(mrkhc_brand)
where mrkhc_brand <> upper(mrkhc_brand)
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update EMD586.dim_bwproducthierarchy
set mrkhc_strength = upper(mrkhc_strength)
where mrkhc_strength <> upper(mrkhc_strength)
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update EMD586.dim_bwproducthierarchy
set businessunitdescription = case when businessunit != 'Not Set' then businessunit ||  ' - ' || businessunitdesc else businessunitdesc end
where businessunitdescription <> case when businessunit != 'Not Set' then businessunit ||  ' - ' || businessunitdesc else businessunitdesc end
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update EMD586.dim_bwproducthierarchy
set productgroupdescription = case when productgroup != 'Not Set' then productgroup || ' - ' ||productgroupdesc else productgroupdesc end
where productgroupdescription <> case when productgroup != 'Not Set' then productgroup|| ' - ' ||productgroupdesc else productgroupdesc end
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );
