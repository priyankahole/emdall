DELETE FROM emd586.dim_customer WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_customer
(
	     dim_customerid
		,customernumber
		,customernumber2
		,name
		,address
		,addressnumber
		,city
		,postalcode
		,country
		,countryname
		,channel

		,projectsourceid
		,dw_insert_date
		,dw_update_date
		,rowstartdate
		,rowenddate
		,rowiscurrent
		,rowchangereason
		,gsa
		,NielsenID
		,gsa_all_products
		,country_region
)
SELECT
         dim_ora_customerlocationid + ifnull(p.dim_projectsourceid * p.multiplier ,0) dim_ora_customerlocationid
	    ,key_id --account_number
			,key_id
		,case when account_name = 'Not Set' then
         (case when left(party_name,locate('-',party_name)-1) is not null then left(party_name,locate('-',party_name)-1)
         else party_name end) else account_name end
		,address1
		,address2
		,SUBSTR(city,1,35) city
		,SUBSTR(postal_code,1,10) postal_code
		,SUBSTR(country,1,10) country
		,case when SUBSTR(country,1,10)='AE' then 'Utd.Arab Emir.'
              when SUBSTR(country,1,10)='CF' then 'CAR'
              when SUBSTR(country,1,10)='CI' then 'Ivory Coast'
              when SUBSTR(country,1,10)='KR' then 'South Korea'
              when SUBSTR(country,1,10)='RU' then 'Russian Fed.'
              when SUBSTR(country,1,10)='US' then 'USA'
                       else TERRITORY_SHORT_NAME end
		,SUBSTR(acctschannel,1,7) acctschannel

		,p.dim_projectsourceid
		,d.dw_insert_date
		,d.dw_update_date
		,d.rowstartdate
		,d.rowenddate
		,d.rowiscurrent
		,d.rowchangereason
		,gsa
		,'Not Set'
		,gsa_all_products
		,ifnull(country_region,'Not Set')
FROM emdoracle4ae.dim_ora_customerlocation d, emdoracle4ae.dim_projectsource p;


update emd586.dim_Customer
set ADDRESS2 = TRIM(LEADING '0' FROM CustomerNumber)||' - '||Name
where ADDRESS2 <> TRIM(LEADING '0' FROM CustomerNumber)||' - '||Name
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.dim_Customer
set CREDITLIMITINDIVIDUAL2 = to_char(CREDITLIMITINDIVIDUAL,'9,999,999,999,990.00')
where CREDITLIMITINDIVIDUAL2 <> to_char(CREDITLIMITINDIVIDUAL,'9,999,999,999,990.00')
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.dim_Customer
set HighCustomerNumber2 = TRIM(LEADING '0' FROM HighCustomerNumber)
where HighCustomerNumber2 <> TRIM(LEADING '0' FROM HighCustomerNumber)
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.dim_Customer
set CREDITLIMITTOTAL2 = to_char(CREDITLIMITTOTAL,'9,999,999,999,990.00')
where CREDITLIMITTOTAL2 <> to_char(CREDITLIMITTOTAL,'9,999,999,999,990.00')
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.dim_Customer
set HigherCustomerNumber2 = TRIM(LEADING '0' FROM HigherCustomerNumber)
where HigherCustomerNumber2 <> TRIM(LEADING '0' FROM HigherCustomerNumber)
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.dim_Customer
set TopLevelCustomerNumber2 = TRIM(LEADING '0' FROM TopLevelCustomerNumber)
where TopLevelCustomerNumber2 <> TRIM(LEADING '0' FROM TopLevelCustomerNumber)
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.dim_Customer
set CREDITLIMITTOTAL_EUR2 = to_char(CREDITLIMITTOTAL_EUR,'9,999,999,999,990.00')
where CREDITLIMITTOTAL_EUR2 <> to_char(CREDITLIMITTOTAL_EUR,'9,999,999,999,990.00')
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );
