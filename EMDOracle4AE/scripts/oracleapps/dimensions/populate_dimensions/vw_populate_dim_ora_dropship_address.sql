/*insert default row*/
INSERT INTO dim_ora_dropship_address
(DIM_ORA_DROPSHIP_ADDRESSID,
    ORDER_NUMBER,
    HEADER_ID,
    BILL_TO_CONTACT,
    SHIP_TO_CONTACT,
    COMPANY_NAME,
    ADDRESS1,
    ADDRESS2,
    ADDRESS3,
    ADDRESS4,
    CITY,
    PROVINCE,
    "state",
    POSTAL_CODE,
    COUNTRY,
    LAST_UPDATE_DATE,
    LAST_UPDATED_BY,
    CREATION_DATE,
    CREATED_BY,
    LAST_UPDATE_LOGIN,
    DW_INSERT_DATE,
    DW_UPDATE_DATE,
    ROWSTARTDATE,
    ROWENDDATE,
    ROWISCURRENT,
    ROWCHANGEREASON,
    SOURCE_ID )
SELECT T.* FROM
(SELECT
1 DIM_ORA_DROPSHIP_ADDRESSID,
0 ORDER_NUMBER,
0 HEADER_ID,
'Not Set' BILL_TO_CONTACT,
'Not Set' SHIP_TO_CONTACT,
'Not Set' COMPANY_NAME,
'Not Set' ADDRESS1,
'Not Set' ADDRESS2,
'Not Set' ADDRESS3,
'Not Set' ADDRESS4,
'Not Set' CITY,
'Not Set' PROVINCE,
'Not Set' "state",
'Not Set' POSTAL_CODE,
'Not Set' COUNTRY,
timestamp '1001-01-01 00:00:00.000000' as LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' as CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
timestamp '1001-01-01 00:00:00.000000' as DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000' as DW_UPDATE_DATE,
current_timestamp AS ROWSTARTDATE,
timestamp '9999-12-31 00:00:00.000000' AS ROWENDDATE,
1 AS ROWISCURRENT,
'Insert' AS ROWCHANGEREASON,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_dropship_address D
ON T.dim_ora_dropship_addressid = D.dim_ora_dropship_addressid
WHERE D.dim_ora_dropship_addressid is null;

/*initialize NUMBER_FOUNTAIN_dim_ora_dropship_address*/
delete from NUMBER_FOUNTAIN_dim_ora_dropship_address where table_name = 'dim_ora_dropship_address';	

INSERT INTO NUMBER_FOUNTAIN_dim_ora_dropship_address
SELECT 'dim_ora_dropship_address',IFNULL(MAX(dim_ora_dropship_addressid),0)
FROM dim_ora_dropship_address;

/*update dimension rows*/
UPDATE dim_ora_dropship_address T
SET
T.ORDER_NUMBER = ifnull(S.ORDER_NUMBER,0),
T.HEADER_ID = ifnull(S.HEADER_ID,0),
T.BILL_TO_CONTACT = ifnull(S.BILL_TO_CONTACT,'Not Set'),
T.SHIP_TO_CONTACT = ifnull(S.SHIP_TO_CONTACT,'Not Set'),
T.COMPANY_NAME = ifnull(S.COMPANY_NAME,'Not Set'),
T.ADDRESS1 = ifnull(S.ADDRESS1,'Not Set' ),
T.ADDRESS2 = ifnull(S.ADDRESS2,'Not Set'),
T.ADDRESS3 = ifnull(S.ADDRESS3,'Not Set'),
T.ADDRESS4 = ifnull(S.ADDRESS4,'Not Set'),
T.CITY = ifnull(S.CITY,'Not Set'),
T.PROVINCE = ifnull(S.PROVINCE,'Not Set'),
T.state = ifnull(S.state,'Not Set'),
T.POSTAL_CODE = ifnull(S.POSTAL_CODE,'Not Set'),
T.COUNTRY = ifnull(S.COUNTRY,'Not Set'),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE,timestamp '1001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY,0),
T.CREATION_DATE = ifnull(S.CREATION_DATE,timestamp '1001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY,0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN,0),
T.DW_UPDATE_DATE = current_timestamp,
T.rowiscurrent = 1,
T.SOURCE_ID ='ORA 12.1'
FROM dim_ora_dropship_address T,ORA_XX_DROPSHIP_ADDRESS S
WHERE
S.HEADER_ID  = T.HEADER_ID ;

/*insert new rows*/
INSERT INTO dim_ora_dropship_address
(DIM_ORA_DROPSHIP_ADDRESSID,
    ORDER_NUMBER,
    HEADER_ID,
    BILL_TO_CONTACT,
    SHIP_TO_CONTACT,
    COMPANY_NAME,
    ADDRESS1,
    ADDRESS2,
    ADDRESS3,
    ADDRESS4,
    CITY,
    PROVINCE,
    "state",
    POSTAL_CODE,
    COUNTRY,
    LAST_UPDATE_DATE,
    LAST_UPDATED_BY,
    CREATION_DATE,
    CREATED_BY,
    LAST_UPDATE_LOGIN,
    DW_INSERT_DATE,
    DW_UPDATE_DATE,
    ROWSTARTDATE,
    ROWENDDATE,
    ROWISCURRENT,
    ROWCHANGEREASON,
    SOURCE_ID)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_dim_ora_dropship_address WHERE TABLE_NAME = 'dim_ora_dropship_address' ),0) + ROW_NUMBER() OVER(order by '') dim_ora_dropship_addressid,
0 ORDER_NUMBER,
0 HEADER_ID,
'Not Set' BILL_TO_CONTACT,
'Not Set' SHIP_TO_CONTACT,
'Not Set' COMPANY_NAME,
'Not Set' ADDRESS1,
'Not Set' ADDRESS2,
'Not Set' ADDRESS3,
'Not Set' ADDRESS4,
'Not Set' CITY,
'Not Set' PROVINCE,
'Not Set' "state",
'Not Set' POSTAL_CODE,
'Not Set' COUNTRY,
timestamp '1001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '1001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
timestamp '1001-01-01 00:00:00.000000'  DW_INSERT_DATE,
timestamp '1001-01-01 00:00:00.000000'  DW_UPDATE_DATE,
current_timestamp AS ROWSTARTDATE,
timestamp '9999-12-31 00:00:00.000000' AS ROWENDDATE,
1 AS ROWISCURRENT,
'Insert' AS ROWCHANGEREASON,
'ORA 12.1' SOURCE_ID
FROM ORA_XX_DROPSHIP_ADDRESS S
LEFT JOIN dim_ora_dropship_address D ON
D.HEADER_ID  = S.HEADER_ID
WHERE
D.HEADER_ID is null;
