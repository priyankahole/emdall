
DELETE FROM emd586.dim_date WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_date
(
	 dim_dateid
	,datevalue
	,juliandate
	,calendaryear
	,financialyear
	,calendarquarter
	,calendarquarterid
	,calendarquartername
	,financialquarter
	,financialquarterid
	,financialquartername
	,season
	,calendarmonthid
	,calendarmonthnumber
	,monthname
	,monthabbreviation
	,financialmonthid
	,financialmonthnumber
	,calendarweekid
	,calendarweek
	,financialweekid
	,financialweek
	,dayofcalendaryear
	,dayoffinancialyear
	,dayofmonth
	,weekdaynumber
	,weekdayname
	,weekdayabbreviation
	,isaweekendday
	,isapublicholiday
	,isaleapyear
	,isaspecialday
	,daysincalendaryear
	,daysinfinancialyear
	,weekdaysincalendaryear
	,weekdaysinfinancialyear
	,workdaysincalendaryear
	,workdaysinfinancialyear
	,daysincalendaryearsofar
	,daysinfinancialyearsofar
	,weekdaysincalendaryearsofar
	,weekdaysinfinancialyearsofar
	,workdaysincalendaryearsofar
	,workdaysinfinancialyearsofar
	,daysinmonth
	,weekdaysinmonth
	,workdaysinmonth
	,daysinmonthsofar
	,weekdaysinmonthsofar
	,workdaysinmonthsofar
	,companycode
	,datename
	,monthyear
	,weekstartdate
	,weekenddate
	,monthstartdate
	,monthenddate
	,financialmonthstartdate
	,financialmonthenddate
	,calendarweekyr
	,financialmonthyear
	,businessdaysseqno
	,financialyearstartdate
	,financialyearenddate
	,financialhalf
	,financialhalfid
	,financialhalfname
	,financialhalfstartdate
	,financialhalfenddate
	,yearflag
	,quarterflag
	,monthflag
	,weekflag
	,dayflag
	,currentperiodname
	,fiscalyearstartflag
	,fiscalyearendflag
	,weekstartflag
	,weekendflag
	,dw_insert_date
	,dw_update_date
	,projectsourceid

)
SELECT
		 dim_dateid + ifnull(p.dim_projectsourceid * p.multiplier ,0) dim_dateid -- THE IMPORTANT STEP TO TAKE THE EXISTING KEY AND TO MULTIPLY
		,datevalue
		,juliandate
		,calendaryear
		,financialyear
		,calendarquarter
		,calendarquarterid
		,calendarquartername
		,financialquarter
		,financialquarterid
		,financialquartername
		,season
		,calendarmonthid
		,calendarmonthnumber
		,monthname
		,monthabbreviation
		,financialmonthid
		,financialmonthnumber
		,calendarweekid
		,calendarweek
		,financialweekid
		,financialweek
		,dayofcalendaryear
		,dayoffinancialyear
		,dayofmonth
		,weekdaynumber
		,weekdayname
		,weekdayabbreviation
		,isaweekendday
		,isapublicholiday
		,isaleapyear
		,isaspecialday
		,daysincalendaryear
		,daysinfinancialyear
		,weekdaysincalendaryear
		,weekdaysinfinancialyear
		,workdaysincalendaryear
		,workdaysinfinancialyear
		,daysincalendaryearsofar
		,daysinfinancialyearsofar
		,weekdaysincalendaryearsofar
		,weekdaysinfinancialyearsofar
		,workdaysincalendaryearsofar
		,workdaysinfinancialyearsofar
		,daysinmonth
		,weekdaysinmonth
		,workdaysinmonth
		,daysinmonthsofar
		,weekdaysinmonthsofar
		,workdaysinmonthsofar
		,companycode
		,datename
		,monthyear
		,weekstartdate
		,weekenddate
		,monthstartdate
		,monthenddate
		,financialmonthstartdate
		,financialmonthenddate
		,calendarweekyr
		,financialmonthyear
		,businessdaysseqno
		,financialyearstartdate
		,financialyearenddate
		,financialhalf
		,financialhalfid
		,financialhalfname
		,financialhalfstartdate
		,financialhalfenddate
		,yearflag
		,quarterflag
		,monthflag
		,weekflag
		,dayflag
		,currentperiodname
		,fiscalyearstartflag
		,fiscalyearendflag
		,weekstartflag
		,weekendflag
		,current_date dw_insert_date
		,current_date dw_update_date
		,7 projectsourceid
FROM emdoracle4ae.dim_date d, dim_projectsource p;

update emd586.Dim_Date
set IsaWeekendday2 = case when IsaWeekendday= 1 then 'Yes' else 'No' end
where ifnull(IsaWeekendday2,0) <> case when IsaWeekendday= 1 then 'Yes' else 'No' end
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );   

update emd586.Dim_Date
set calendarmonthid2 = concat (left(calendarmonthid,4),'-',right(calendarmonthid,2))
where ifnull(calendarmonthid2,0) <> concat (left(calendarmonthid,4),'-',right(calendarmonthid,2))
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s ); 

update emd586.Dim_Date
set CalendarWeekYr2 = to_char(CalendarWeekYr)
where ifnull(CalendarWeekYr2,0) <> to_char(CalendarWeekYr)
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );   

update emd586.dim_date
set IsaPublicHoliday2 = case when IsaPublicHoliday= 1 then 'Yes' else 'No' end
where ifnull(IsaPublicHoliday2,0) <> case when IsaPublicHoliday= 1 then 'Yes' else 'No' end
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.dim_date
set IsaLeapYear2 = case when IsaLeapYear= 1 then 'Yes' else 'No' end
where ifnull(IsaLeapYear2,0) <> case when IsaLeapYear= 1 then 'Yes' else 'No' end
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s ); 
