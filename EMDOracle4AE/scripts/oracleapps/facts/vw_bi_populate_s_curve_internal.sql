DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'ora_s_curve_final';
INSERT INTO NUMBER_FOUNTAIN
SELECT 'ora_s_curve_final',IFNULL(MAX(ora_s_curve_finalid),0)
FROM ora_s_curve_final;

delete from ora_s_curve_final;

INSERT INTO  ora_s_curve_final

(
ora_s_curve_finalid,
from_org,
part,
global_id,
transaction_date,
shipment_priority_code,
to_org,
shipment_number,
freight_code,
receip_date,
to_qty,
req,
organization_id,
dim_projectsourceid,
creation_date,
booked_date,
order_number
)

select
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'ora_s_curve_final' ) + ROW_NUMBER() OVER(order by '') ora_s_curve_finalid,
ORGANIZATION_CODE from_org,
part,
attribute9,
transaction_date,
SHIPMENT_PRIORITY_CODE,
TO_ORG,
shipment_number,
freight_code,
max(RECEIP_DATE) as RECEIP_DATE,
sum(PRIMARY_QUANTITY) as to_qty,
REQ,
organization_id,
7 AS dim_projectsourceid,
creation_date,
booked_date,
order_number

from ora_s_curve
where organization_code in ('FRW','BUR','EIW','KAW','TEW','LRW','SEW','HAW','UKW','BPW')
group by
ORGANIZATION_CODE,
part,
attribute9,
transaction_date,
SHIPMENT_PRIORITY_CODE,
TO_ORG,
shipment_number,
freight_code,
req,
organization_id,
creation_date,
booked_date,
order_number
order by 1,2,3,4;


UPDATE ora_s_curve_final f
SET  f.DIM_ORA_INV_PRODUCTID = D.DIM_ORA_INVPRODUCTID
FROM ora_s_curve_final f,ora_mtl_system_items_2 m,dim_ora_invproduct d
WHERE f.part=m.segment1 and f.global_id=m.attribute9 and f.organization_id=m.organization_id
AND CONVERT(VARCHAR(200),m.ORGANIZATION_ID) ||'~'||  CONVERT(VARCHAR(200),m.INVENTORY_ITEM_ID) = D.KEY_ID
AND f.DIM_ORA_INV_PRODUCTID <> D.DIM_ORA_INVPRODUCTID;

UPDATE ora_s_curve_final c
SET c.dim_ora_mdg_productid = mdg.dim_mdg_partid
FROM ora_s_curve_final c,dim_ora_mdg_product mdg
WHERE c.global_id = mdg.partnumber;


UPDATE ora_s_curve_final f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM ora_s_curve_final f,dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND CASE WHEN substring(dp.GlbProductGroup,1,3)='SBU' THEN dp.GlbProductGroup
ELSE concat('SBU-',dp.GlbProductGroup) END = bw.productgroup
AND '2017-12-28' BETWEEN bw.upperhierstartdate AND bw.upperhierenddate
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE ora_s_curve_final f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM ora_s_curve_final f,dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE ora_s_curve_final f
SET F.dim_ora_orgid = D.DIM_ORA_BUSINESS_ORGID
FROM ora_s_curve_final f,DIM_ORA_BUSINESS_ORG d
WHERE f.to_org=d.organization_code
AND F.dim_ora_orgid <> D.DIM_ORA_BUSINESS_ORGID;


UPDATE ora_s_curve_final c
SET c.dim_ora_transaction_dateid=mdg.dim_dateid
FROM ora_s_curve_final c,dim_date mdg
WHERE transaction_date=datevalue
AND c.dim_ora_transaction_dateid <> mdg.dim_dateid;

UPDATE ora_s_curve_final c
SET c.dim_ora_RECEIP_DATE_dateid=mdg.dim_dateid
FROM ora_s_curve_final c,dim_date mdg
WHERE RECEIP_DATE=DATEVALUE
AND c.dim_ora_RECEIP_DATE_dateid <> mdg.dim_dateid;


UPDATE ora_s_curve_final f
SET f.dim_ora_vendor_purchasingid=c.dim_ora_vendor_purchasingid
FROM ora_s_curve_final f,dim_ora_vendor_purchasing c
WHERE f.from_org=c.vendor_code;

UPDATE ora_s_curve_final c
SET c.dim_ora_shippinginstructionid=mdg.dim_ora_shippinginstructionid
FROM ora_s_curve_final c,dim_ora_shippinginstruction mdg
WHERE shipping_instruction_code=IFNULL(FREIGHT_CODE,'Not Set')
AND shipping_instruction_desc=SHIPMENT_PRIORITY_CODE;


UPDATE ora_s_curve_final c
SET c.dim_ora_creation_dateid=mdg.dim_dateid
FROM ora_s_curve_final c,dim_date mdg
WHERE creation_date=datevalue;

UPDATE ora_s_curve_final c
SET c.dim_ora_booked_dateid=mdg.dim_dateid
FROM ora_s_curve_final c,dim_date mdg
WHERE booked_date=datevalue;
