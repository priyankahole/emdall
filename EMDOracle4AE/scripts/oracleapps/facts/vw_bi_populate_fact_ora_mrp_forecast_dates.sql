/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mrp_forecast_dates*/
/*SELECT * FROM fact_ora_mrp_forecast_dates*/

/*initialize NUMBER_FOUNTAIN_fact_ora_mrp_forecast_dates*/
delete from NUMBER_FOUNTAIN_fact_ora_mrp_forecast_dates where table_name = 'fact_ora_mrp_forecast_dates';

INSERT INTO NUMBER_FOUNTAIN_fact_ora_mrp_forecast_dates
SELECT 'fact_ora_mrp_forecast_dates',IFNULL(MAX(fact_ora_mrp_forecast_datesid),0)
FROM fact_ora_mrp_forecast_dates;

/*update fact columns*/
UPDATE fact_ora_mrp_forecast_dates T
SET
DD_ATTRIBUTE5 = ifnull(S.ATTRIBUTE5,'Not Set'),
DD_TO_UPDATE = ifnull(S.TO_UPDATE,0),
TRANSACTION_ID = S.TRANSACTION_ID,
CT_ORIGINAL_FORECAST_QUANTITY = S.ORIGINAL_FORECAST_QUANTITY,
CT_CURRENT_FORECAST_QUANTITY = S.CURRENT_FORECAST_QUANTITY,
CT_CONFIDENCE_PERCENTAGE = S.CONFIDENCE_PERCENTAGE,
DD_COMMENTS = ifnull(S.COMMENTS,'Not Set'),
DD_SOURCE_CODE = ifnull(S.SOURCE_CODE,'Not Set'),
DD_SOURCE_LINE_ID = ifnull(S.SOURCE_LINE_ID,0),
CT_END_PLANNING_BOM_PERCENT = ifnull(S.END_PLANNING_BOM_PERCENT,0),
CT_FORECAST_TREND = ifnull(S.FORECAST_TREND,0),
DD_FORECAST_MAD = ifnull(S.FORECAST_MAD,0),
DD_DEMAND_CLASS = ifnull(S.DEMAND_CLASS,'Not Set'),
DD_DDF_CONTEXT = ifnull(S.DDF_CONTEXT,'Not Set'),
DD_ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY,'Not Set'),
DD_ATTRIBUTE1 = ifnull(S.ATTRIBUTE1,'Not Set'),
DD_ATTRIBUTE2 = ifnull(S.ATTRIBUTE2,'Not Set'),
DD_ATTRIBUTE3 = ifnull(S.ATTRIBUTE3,'Not Set'),
DD_ATTRIBUTE4 = ifnull(S.ATTRIBUTE4,'Not Set'),
DD_ATTRIBUTE6 = ifnull(S.ATTRIBUTE6,'Not Set'),
DD_ATTRIBUTE7 = ifnull(S.ATTRIBUTE7,'Not Set'),
DD_ATTRIBUTE8 = ifnull(S.ATTRIBUTE8,'Not Set'),
DD_ATTRIBUTE9 = ifnull(S.ATTRIBUTE9,'Not Set'),
DD_ATTRIBUTE10 = ifnull(S.ATTRIBUTE10,'Not Set'),
DD_ATTRIBUTE11 = ifnull(S.ATTRIBUTE11,'Not Set'),
DD_ATTRIBUTE12 = ifnull(S.ATTRIBUTE12,'Not Set'),
DD_ATTRIBUTE13 = ifnull(S.ATTRIBUTE13,'Not Set'),
DD_ATTRIBUTE14 = ifnull(S.ATTRIBUTE14,'Not Set'),
DD_ATTRIBUTE15 = ifnull(S.ATTRIBUTE15,'Not Set'),
DD_OLD_TRANSACTION_ID = ifnull(S.OLD_TRANSACTION_ID,0),
amt_exchangerate = 1,
amt_exchangerate_gbl = 1,
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
FROM fact_ora_mrp_forecast_dates T	,ora_mrp_forecast_dates S
WHERE
T.TRANSACTION_ID= S.TRANSACTION_ID;

/*insert new rows*/
INSERT INTO fact_ora_mrp_forecast_dates
(
fact_ora_mrp_forecast_datesid,
DD_ATTRIBUTE5,DD_TO_UPDATE,TRANSACTION_ID,DIM_ORA_DATE_LAST_UPDATEid,
DIM_ORA_LAST_UPDATED_BYid,DIM_ORA_DATE_CREATIONid,DIM_ORA_CREATED_BYid,DIM_ORA_INVENTORY_ITEMID,DIM_ORA_ORGANIZATIONID,DIM_ORA_FORECAST_DESIGNATORid,
DIM_ORA_DATE_FORECASTid,CT_ORIGINAL_FORECAST_QUANTITY,CT_CURRENT_FORECAST_QUANTITY,CT_CONFIDENCE_PERCENTAGE,DIM_ORA_BUCKET_TYPEid,
DIM_ORA_DATE_RATE_ENDid,DIM_ORA_ORIGINATION_TYPEid,DIM_ORA_CUSTOMERID,DIM_ORA_SHIPID,DIM_ORA_BILLID,DD_COMMENTS,DIM_ORA_SOURCE_ORGANIZATIONID,
DIM_ORA_SOURCE_FORECAST_DESIGNATORid,DD_SOURCE_CODE,DD_SOURCE_LINE_ID,DIM_ORA_END_ITEMID,CT_END_PLANNING_BOM_PERCENT,DIM_ORA_FORECAST_RULEID,
DIM_ORA_DATE_DEMAND_USAGE_STARTID,CT_FORECAST_TREND,DIM_ORA_FOCUS_TYPEid,DD_FORECAST_MAD,DD_DEMAND_CLASS,DD_DDF_CONTEXT,
DD_ATTRIBUTE_CATEGORY,DD_ATTRIBUTE1,DD_ATTRIBUTE2,DD_ATTRIBUTE3,DD_ATTRIBUTE4,DD_ATTRIBUTE6,DD_ATTRIBUTE7,DD_ATTRIBUTE8,DD_ATTRIBUTE9,
DD_ATTRIBUTE10,DD_ATTRIBUTE11,DD_ATTRIBUTE12,DD_ATTRIBUTE13,DD_ATTRIBUTE14,DD_ATTRIBUTE15,DD_OLD_TRANSACTION_ID,DIM_ORA_LINEid,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_fact_ora_mrp_forecast_dates WHERE TABLE_NAME = 'fact_ora_mrp_forecast_dates' ),0) + ROW_NUMBER() OVER(order by '') fact_ora_mrp_forecast_datesid,
ifnull(S.ATTRIBUTE5,'Not Set') AS DD_ATTRIBUTE5,
ifnull(S.TO_UPDATE,0) AS DD_TO_UPDATE,
S.TRANSACTION_ID AS TRANSACTION_ID,
1 AS DIM_ORA_DATE_LAST_UPDATEid,
1 AS DIM_ORA_LAST_UPDATED_BYid,
1 AS DIM_ORA_DATE_CREATIONid,
1 AS DIM_ORA_CREATED_BYid,
1 AS DIM_ORA_INVENTORY_ITEMID,
1 AS DIM_ORA_ORGANIZATIONID,
1 AS DIM_ORA_FORECAST_DESIGNATORid,
1 AS DIM_ORA_DATE_FORECASTid,
S.ORIGINAL_FORECAST_QUANTITY AS CT_ORIGINAL_FORECAST_QUANTITY,
S.CURRENT_FORECAST_QUANTITY AS CT_CURRENT_FORECAST_QUANTITY,
S.CONFIDENCE_PERCENTAGE AS CT_CONFIDENCE_PERCENTAGE,
1 AS DIM_ORA_BUCKET_TYPEid,
1 AS DIM_ORA_DATE_RATE_ENDid,
1 AS DIM_ORA_ORIGINATION_TYPEid,
1 AS DIM_ORA_CUSTOMERID,
1 AS DIM_ORA_SHIPID,
1 AS DIM_ORA_BILLID,
ifnull(S.COMMENTS,'Not Set') AS DD_COMMENTS,
1 AS DIM_ORA_SOURCE_ORGANIZATIONID,
1 AS DIM_ORA_SOURCE_FORECAST_DESIGNATORid,
ifnull(S.SOURCE_CODE,'Not Set') AS DD_SOURCE_CODE,
ifnull(S.SOURCE_LINE_ID,0) AS DD_SOURCE_LINE_ID,
1 AS DIM_ORA_END_ITEMID,
ifnull(S.END_PLANNING_BOM_PERCENT,0) AS CT_END_PLANNING_BOM_PERCENT,
1 AS DIM_ORA_FORECAST_RULEID,
1 AS DIM_ORA_DATE_DEMAND_USAGE_STARTID,
ifnull(S.FORECAST_TREND,0) AS CT_FORECAST_TREND,
1 AS DIM_ORA_FOCUS_TYPEid,
ifnull(S.FORECAST_MAD,0) AS DD_FORECAST_MAD,
ifnull(S.DEMAND_CLASS,'Not Set') AS DD_DEMAND_CLASS,
ifnull(S.DDF_CONTEXT,'Not Set') AS DD_DDF_CONTEXT,
ifnull(S.ATTRIBUTE_CATEGORY,'Not Set') AS DD_ATTRIBUTE_CATEGORY,
ifnull(S.ATTRIBUTE1,'Not Set') AS DD_ATTRIBUTE1,
ifnull(S.ATTRIBUTE2,'Not Set') AS DD_ATTRIBUTE2,
ifnull(S.ATTRIBUTE3,'Not Set') AS DD_ATTRIBUTE3,
ifnull(S.ATTRIBUTE4,'Not Set') AS DD_ATTRIBUTE4,
ifnull(S.ATTRIBUTE6,'Not Set') AS DD_ATTRIBUTE6,
ifnull(S.ATTRIBUTE7,'Not Set') AS DD_ATTRIBUTE7,
ifnull(S.ATTRIBUTE8,'Not Set') AS DD_ATTRIBUTE8,
ifnull(S.ATTRIBUTE9,'Not Set') AS DD_ATTRIBUTE9,
ifnull(S.ATTRIBUTE10,'Not Set') AS DD_ATTRIBUTE10,
ifnull(S.ATTRIBUTE11,'Not Set') AS DD_ATTRIBUTE11,
ifnull(S.ATTRIBUTE12,'Not Set') AS DD_ATTRIBUTE12,
ifnull(S.ATTRIBUTE13,'Not Set') AS DD_ATTRIBUTE13,
ifnull(S.ATTRIBUTE14,'Not Set') AS DD_ATTRIBUTE14,
ifnull(S.ATTRIBUTE15,'Not Set') AS DD_ATTRIBUTE15,
ifnull(S.OLD_TRANSACTION_ID,0) AS DD_OLD_TRANSACTION_ID,
1 AS DIM_ORA_LINEid,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'INSERT' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mrp_forecast_dates S LEFT JOIN fact_ora_mrp_forecast_dates F
ON F.TRANSACTION_ID = S.TRANSACTION_ID
WHERE
F.TRANSACTION_ID is null;

/*UPDATE DIM_ORA_DATE_LAST_UPDATEid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_DATE_LAST_UPDATEid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_DATE D ON to_date(S.LAST_UPDATE_DATE) = D.DATEVALUE
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_DATE_LAST_UPDATEid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_LAST_UPDATED_BYid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_LAST_UPDATED_BYid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_LAST_UPDATED_BYid <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_DATE_CREATIONid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_DATE_CREATIONid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_DATE D ON to_date(S.CREATION_DATE) = D.DATEVALUE
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_DATE_CREATIONid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_CREATED_BYid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_CREATED_BYid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_CREATED_BYid <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_INVENTORY_ITEMID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_INVENTORY_ITEMID = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S
JOIN DIM_ORA_INVPRODUCT D ON CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'|| CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_INVENTORY_ITEMID <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE DIM_ORA_ORGANIZATIONID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_ORGANIZATIONID = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_MTL_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_ORGANIZATIONID <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE DIM_ORA_FORECAST_DESIGNATORid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_FORECAST_DESIGNATORid = D.DIM_ORA_MRP_FORECAST_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_MRP_FORECAST_DESIGNATORS D
ON CONVERT(VARCHAR(200),S.FORECAST_DESIGNATOR) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_FORECAST_DESIGNATORid <> D.DIM_ORA_MRP_FORECAST_DESIGNATORSID;

/*UPDATE DIM_ORA_DATE_FORECASTid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_DATE_FORECASTid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_DATE D ON to_date(S.FORECAST_DATE) = D.DATEVALUE
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_DATE_FORECASTid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_BUCKET_TYPEid*/
TRUNCATE TABLE mrp_for_dt;
INSERT INTO MRP_FOR_DT
SELECT D.LOOKUP_CODE,D.DIM_ORA_FND_LOOKUPID
from DIM_ORA_FND_LOOKUP D
WHERE D.LOOKUP_TYPE = 'MRP_BUCKET_TYPE'
AND d.rowiscurrent = 1;


UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_BUCKET_TYPEid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN mrp_for_dt D ON S.BUCKET_TYPE = D.LOOKUP_CODE
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_BUCKET_TYPEid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_DATE_RATE_ENDid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_DATE_RATE_ENDid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_DATE D ON to_date(S.RATE_END_DATE) = D.DATEVALUE
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_DATE_RATE_ENDid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_ORIGINATION_TYPEid*/

TRUNCATE TABLE mrp_org_tp;
INSERT INTO mrp_org_tp
SELECT D.LOOKUP_CODE,D.DIM_ORA_FND_LOOKUPID
from DIM_ORA_FND_LOOKUP D
WHERE D.LOOKUP_TYPE = 'MRP_FORECAST_ORIG'
AND d.rowiscurrent = 1;

UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_ORIGINATION_TYPEid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN mrp_org_tp D ON S.ORIGINATION_TYPE = D.LOOKUP_CODE
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_ORIGINATION_TYPEid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_CUSTOMERID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_CUSTOMERID = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_HZ_CUSTACCOUNTS D ON S.CUSTOMER_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_CUSTOMERID <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE DIM_ORA_SHIPID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_SHIPID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.SHIP_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_SHIPID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_ORA_BILLID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_BILLID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.BILL_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_BILLID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_ORA_SOURCE_ORGANIZATIONID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_SOURCE_ORGANIZATIONID = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_MTL_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_SOURCE_ORGANIZATIONID <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE DIM_ORA_SOURCE_FORECAST_DESIGNATORid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_SOURCE_FORECAST_DESIGNATORid = D.DIM_ORA_MRP_FORECAST_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_MRP_FORECAST_DESIGNATORS D
ON CONVERT(VARCHAR(200),S.SOURCE_FORECAST_DESIGNATOR) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_SOURCE_FORECAST_DESIGNATORid <> D.DIM_ORA_MRP_FORECAST_DESIGNATORSID;

/*UPDATE DIM_ORA_END_ITEMID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_END_ITEMID = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_INVPRODUCT D
ON CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'|| CONVERT(VARCHAR(200),S.END_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_END_ITEMID <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE DIM_ORA_FORECAST_RULEID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_FORECAST_RULEID = D.DIM_ORA_MRP_FORECAST_RULESID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_MRP_FORECAST_RULES D ON S.FORECAST_RULE_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_FORECAST_RULEID <> D.DIM_ORA_MRP_FORECAST_RULESID;

/*UPDATE DIM_ORA_DATE_DEMAND_USAGE_STARTID*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_DATE_DEMAND_USAGE_STARTID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_DATE D ON to_date(S.DEMAND_USAGE_START_DATE) = D.DATEVALUE
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_DATE_DEMAND_USAGE_STARTID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_FOCUS_TYPEid*/

TRUNCATE TABLE mrp_focus_tmp;
INSERT INTO mrp_focus_tmp
SELECT D.LOOKUP_CODE,D.DIM_ORA_FND_LOOKUPID
from DIM_ORA_FND_LOOKUP D
WHERE D.LOOKUP_TYPE = 'MTL_FOCUS_CALENDAR'
AND d.rowiscurrent = 1;

UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_FOCUS_TYPEid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN mrp_focus_tmp D
ON S.FOCUS_TYPE = D.LOOKUP_CODE
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_FOCUS_TYPEid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_LINEid*/
UPDATE fact_ora_mrp_forecast_dates F
SET
F.DIM_ORA_LINEid = D.DIM_ORA_WIP_LINESID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_dates F,ora_mrp_forecast_dates S JOIN DIM_ORA_WIP_LINES D
ON CONVERT(VARCHAR(200),S.LINE_ID) ||'~'||  CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.TRANSACTION_ID= S.TRANSACTION_ID AND
F.DIM_ORA_LINEid <> D.DIM_ORA_WIP_LINESID;
/*update dim_ora_mdg_productid*/
UPDATE fact_ora_mrp_forecast_dates f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM fact_ora_mrp_forecast_dates f_inv,dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.dim_ora_inventory_itemid = d_invp.dim_ora_invproductid
 AND  d_invp.mdm_globalid = d_mp.partnumber
 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;

 /*update dim_ora_bwproducthierarchyid*/
 UPDATE fact_ora_mrp_forecast_dates f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_mrp_forecast_dates f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inventory_itemid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND dp.productgroupsbu = bw.productgroup
AND current_date BETWEEN bw.upperhierstartdate AND bw.upperhierenddate
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE fact_ora_mrp_forecast_dates f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_mrp_forecast_dates f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inventory_itemid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;
