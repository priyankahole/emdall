/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_ar_sales_invoices*/
/*SELECT * FROM fact_ora_ar_sales_invoices*/

/*initialize NUMBER_FOUNTAIN_fact_ora_ar_sales_invoices*/
delete from NUMBER_FOUNTAIN_fact_ora_ar_sales_invoices where table_name = 'fact_ora_ar_sales_invoices';

INSERT INTO NUMBER_FOUNTAIN_fact_ora_ar_sales_invoices
SELECT 'fact_ora_ar_sales_invoices',IFNULL(MAX(fact_ora_ar_sales_invoicesid),0)
FROM fact_ora_ar_sales_invoices;

/*update fact columns*/
UPDATE fact_ora_ar_sales_invoices T
SET
DD_TYPE = ifnull(S.TYPE,'Not Set'),
CUSTOMER_TRX_ID = S.CUSTOMER_TRX_ID,
CUSTOMER_TRX_LINE_ID = S.CUSTOMER_TRX_LINE_ID,
CUST_TRX_LINE_GL_DIST_ID = S.CUST_TRX_LINE_GL_DIST_ID,
DD_TRX_NUMBER = S.TRX_NUMBER,
DD_SOLD_TO_CONTACT_ID = ifnull(S.SOLD_TO_CONTACT_ID,0),
dd_PURCHASE_ORDER = ifnull(S.PURCHASE_ORDER,'Not Set'),
ct_EXCHANGE_RATE = ifnull(S.EXCHANGE_RATE,0),
TERRITORY_ID = ifnull(S.TERRITORY_ID,0),
DD_INVOICE_CURRENCY_CODE = ifnull(S.INVOICE_CURRENCY_CODE,'Not Set'),
DD_COMPLETE_FLAG = S.COMPLETE_FLAG,
PAYING_SITE_USE_ID = ifnull(S.PAYING_SITE_USE_ID,0),
dd_LINE_NUMBER = S.LINE_NUMBER,
ct_QUANTITY_INVOICED = ifnull(S.QUANTITY_INVOICED,0),
ct_QUANTITY_CREDITED = ifnull(S.QUANTITY_CREDITED,0),
ct_UNIT_STANDARD_PRICE = ifnull(S.UNIT_STANDARD_PRICE,0),
ct_UNIT_SELLING_PRICE = ifnull(S.UNIT_SELLING_PRICE,0),
dd_SALES_ORDER = ifnull(S.SALES_ORDER,'Not Set'),
dd_SALES_ORDER_LINE = ifnull(S.SALES_ORDER_LINE,'Not Set'),
dd_LINE_TYPE = S.LINE_TYPE,
amt_EXTENDED_AMOUNT = S.EXTENDED_AMOUNT,
amt_TAX_RATE = ifnull(S.TAX_RATE,0),
dd_UOM_CODE = ifnull(S.UOM_CODE,'Not Set'),
dd_INTERFACE_LINE_CONTEXT = ifnull(S.INTERFACE_LINE_CONTEXT,'Not Set'),
dd_INTERFACE_LINE_ATTRIBUTE6 = ifnull(S.INTERFACE_LINE_ATTRIBUTE6,'Not Set'),
dd_INTERFACE_LINE_ATTRIBUTE7 = ifnull(S.INTERFACE_LINE_ATTRIBUTE7,'Not Set'),
dd_INTERFACE_LINE_ATTRIBUTE11 = ifnull(S.INTERFACE_LINE_ATTRIBUTE11,'Not Set'),
dd_PARENT_SALES_ORDER = ifnull(S.PARENT_SALES_ORDER,'Not Set'),
dd_PARENT_SALES_ORDER_LINE = ifnull(S.PARENT_SALES_ORDER_LINE,'Not Set'),
dd_PARENT_LINE_CONTEXT = ifnull(S.PARENT_LINE_CONTEXT,'Not Set'),
dd_PARENT_ATTRIBUTE7 = ifnull(S.PARENT_ATTRIBUTE7,'Not Set'),
dd_STATUS_TRX = ifnull(S.STATUS_TRX,'Not Set'),
dd_EXCHANGE_RATE_TYPE = ifnull(S.EXCHANGE_RATE_TYPE,'Not Set'),
dd_LINE_CATEGORY_CODE = ifnull(S.LINE_CATEGORY_CODE,'Not Set'),
dd_ITEM_TYPE_CODE = ifnull(S.ITEM_TYPE_CODE,'Not Set'),
dd_SHIPMENT_NUMBER = ifnull(S.SHIPMENT_NUMBER,0),
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
FROM fact_ora_ar_sales_invoices T, ora_ar_sales_invoices S
WHERE
T.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID;

/*insert new rows*/
INSERT INTO fact_ora_ar_sales_invoices
(
fact_ora_ar_sales_invoicesid,
DD_TYPE,CUSTOMER_TRX_ID,CUSTOMER_TRX_LINE_ID,CUST_TRX_LINE_GL_DIST_ID,DD_TRX_NUMBER,
dim_ora_cust_trx_typeid,dim_ora_date_trxid,dim_ora_sold_to_customerid,DD_SOLD_TO_CONTACT_ID,
dim_ora_sold_to_site_useid,dim_ora_bill_to_customerid,dim_ora_bill_to_site_useid,
dim_ora_ship_to_customerid,dim_ora_ship_to_site_useid,
dim_ora_termid,dim_ora_date_due_termid,dim_ora_primary_salesrepid,
dim_ora_date_printing_originalid,dd_PURCHASE_ORDER,
dim_ora_date_exchangeid,ct_EXCHANGE_RATE,TERRITORY_ID,
DD_INVOICE_CURRENCY_CODE,DD_COMPLETE_FLAG,PAYING_SITE_USE_ID,dim_ora_date_last_updateid,
dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_created_byid,
dd_LINE_NUMBER,dim_ora_set_of_booksid,dim_ora_inventory_itemid,ct_QUANTITY_INVOICED,
ct_QUANTITY_CREDITED,ct_UNIT_STANDARD_PRICE,ct_UNIT_SELLING_PRICE,dd_SALES_ORDER,
dd_SALES_ORDER_LINE,dd_LINE_TYPE,amt_EXTENDED_AMOUNT,amt_TAX_RATE,dd_UOM_CODE,
dim_ora_orgid,dd_INTERFACE_LINE_CONTEXT,dd_INTERFACE_LINE_ATTRIBUTE6,dd_INTERFACE_LINE_ATTRIBUTE7,
dd_INTERFACE_LINE_ATTRIBUTE11,dim_ora_warehouseid,fact_ora_parent_lineid,
dim_ora_parent_inventory_itemid,dd_PARENT_SALES_ORDER,dd_PARENT_SALES_ORDER_LINE,
dd_PARENT_LINE_CONTEXT,dd_PARENT_ATTRIBUTE7,dim_ora_parent_warehouseid,
dd_STATUS_TRX,dd_EXCHANGE_RATE_TYPE,dim_ora_date_invoice_last_updateid,
dim_ora_date_trx_type_last_updateid,dim_ora_date_so_header_updatedid,
dim_ora_sales_channel_codeid,dim_ora_order_typeid,dim_ora_payment_type_codeid,
dim_ora_freight_termsd_codeid,dd_LINE_CATEGORY_CODE,dd_ITEM_TYPE_CODE,dd_SHIPMENT_NUMBER,
dim_ora_ship_from_orgid,dim_ora_code_combinationid,DD_FUNCTIONAL_CURR_CODE,AMT_EXCHANGERATE,AMT_EXCHANGERATE_GBL,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_fact_ora_ar_sales_invoices WHERE TABLE_NAME = 'fact_ora_ar_sales_invoices' ),0) + ROW_NUMBER() OVER(order by '') fact_ora_ar_sales_invoicesid,
ifnull(S.TYPE,'Not Set') AS DD_TYPE,
S.CUSTOMER_TRX_ID AS CUSTOMER_TRX_ID,
S.CUSTOMER_TRX_LINE_ID AS CUSTOMER_TRX_LINE_ID,
S.CUST_TRX_LINE_GL_DIST_ID AS CUST_TRX_LINE_GL_DIST_ID,
S.TRX_NUMBER AS DD_TRX_NUMBER,
1 AS dim_ora_cust_trx_typeid,
1 AS dim_ora_date_trxid,
1 AS dim_ora_sold_to_customerid,
ifnull(S.SOLD_TO_CONTACT_ID,0) AS DD_SOLD_TO_CONTACT_ID,
1 AS dim_ora_sold_to_site_useid,
1 AS dim_ora_bill_to_customerid,
1 AS dim_ora_bill_to_site_useid,
1 AS dim_ora_ship_to_customerid,
1 AS dim_ora_ship_to_site_useid,
1 AS dim_ora_termid,
1 AS dim_ora_date_due_termid,
1 AS dim_ora_primary_salesrepid,
1 AS dim_ora_date_printing_originalid,
ifnull(S.PURCHASE_ORDER,'Not Set') AS dd_PURCHASE_ORDER,
1 AS dim_ora_date_exchangeid,
ifnull(S.EXCHANGE_RATE,0) AS ct_EXCHANGE_RATE,
ifnull(S.TERRITORY_ID,0) AS TERRITORY_ID,
ifnull(S.INVOICE_CURRENCY_CODE,'Not Set') AS DD_INVOICE_CURRENCY_CODE,
S.COMPLETE_FLAG AS DD_COMPLETE_FLAG,
ifnull(S.PAYING_SITE_USE_ID,0) AS PAYING_SITE_USE_ID,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_last_updated_byid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_created_byid,
S.LINE_NUMBER AS dd_LINE_NUMBER,
1 AS dim_ora_set_of_booksid,
1 AS dim_ora_inventory_itemid,
ifnull(S.QUANTITY_INVOICED,0) AS ct_QUANTITY_INVOICED,
ifnull(S.QUANTITY_CREDITED,0) AS ct_QUANTITY_CREDITED,
ifnull(S.UNIT_STANDARD_PRICE,0) AS ct_UNIT_STANDARD_PRICE,
ifnull(S.UNIT_SELLING_PRICE,0) AS ct_UNIT_SELLING_PRICE,
ifnull(S.SALES_ORDER,'Not Set') AS dd_SALES_ORDER,
ifnull(S.SALES_ORDER_LINE,'Not Set') AS dd_SALES_ORDER_LINE,
S.LINE_TYPE AS dd_LINE_TYPE,
S.EXTENDED_AMOUNT AS amt_EXTENDED_AMOUNT,
ifnull(S.TAX_RATE,0) AS amt_TAX_RATE,
ifnull(S.UOM_CODE,'Not Set') AS dd_UOM_CODE,
1 AS dim_ora_orgid,
ifnull(S.INTERFACE_LINE_CONTEXT,'Not Set') AS dd_INTERFACE_LINE_CONTEXT,
ifnull(S.INTERFACE_LINE_ATTRIBUTE6,'Not Set') AS dd_INTERFACE_LINE_ATTRIBUTE6,
ifnull(S.INTERFACE_LINE_ATTRIBUTE7,'Not Set') AS dd_INTERFACE_LINE_ATTRIBUTE7,
ifnull(S.INTERFACE_LINE_ATTRIBUTE11,'Not Set') AS dd_INTERFACE_LINE_ATTRIBUTE11,
1 AS dim_ora_warehouseid,
0 AS fact_ora_parent_lineid,
1 AS dim_ora_parent_inventory_itemid,
ifnull(S.PARENT_SALES_ORDER,'Not Set') AS dd_PARENT_SALES_ORDER,
ifnull(S.PARENT_SALES_ORDER_LINE,'Not Set') AS dd_PARENT_SALES_ORDER_LINE,
ifnull(S.PARENT_LINE_CONTEXT,'Not Set') AS dd_PARENT_LINE_CONTEXT,
ifnull(S.PARENT_ATTRIBUTE7,'Not Set') AS dd_PARENT_ATTRIBUTE7,
1 AS dim_ora_parent_warehouseid,
ifnull(S.STATUS_TRX,'Not Set') AS dd_STATUS_TRX,
ifnull(S.EXCHANGE_RATE_TYPE,'Not Set') AS dd_EXCHANGE_RATE_TYPE,
1 AS dim_ora_date_invoice_last_updateid,
1 AS dim_ora_date_trx_type_last_updateid,
1 AS dim_ora_date_so_header_updatedid,
1 AS dim_ora_sales_channel_codeid,
1 AS dim_ora_order_typeid,
1 AS dim_ora_payment_type_codeid,
1 AS dim_ora_freight_termsd_codeid,
ifnull(S.LINE_CATEGORY_CODE,'Not Set') AS dd_LINE_CATEGORY_CODE,
ifnull(S.ITEM_TYPE_CODE,'Not Set') AS dd_ITEM_TYPE_CODE,
ifnull(S.SHIPMENT_NUMBER,0) AS dd_SHIPMENT_NUMBER,
1 AS dim_ora_ship_from_orgid,
1 AS dim_ora_code_combinationid,
'Not Set' AS DD_FUNCTIONAL_CURR_CODE,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_ar_sales_invoices S LEFT JOIN fact_ora_ar_sales_invoices F
ON F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
WHERE F.CUST_TRX_LINE_GL_DIST_ID is null;

/*UPDATE dim_ora_cust_trx_typeid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_cust_trx_typeid = D.DIM_ORA_AR_INVOICE_TYPESID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F, ora_ar_sales_invoices S, DIM_ORA_AR_INVOICE_TYPES D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),ifnull(S.CUST_TRX_TYPE_ID,0)) ||'~'|| CONVERT(VARCHAR(200),ifnull(S.ORG_ID,0)) = D.KEY_ID
and d.rowiscurrent = 1
AND F.dim_ora_cust_trx_typeid <> D.DIM_ORA_AR_INVOICE_TYPESID;

/*UPDATE dim_ora_date_trxid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_trxid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F, ora_ar_sales_invoices S, DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.TRX_DATE) = D.datevalue
AND F.dim_ora_date_trxid <> D.DIM_DATEID;

/*UPDATE dim_ora_sold_to_customerid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_sold_to_customerid = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F, ora_ar_sales_invoices S, DIM_ORA_HZ_CUSTACCOUNTS D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.SOLD_TO_CUSTOMER_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_sold_to_customerid <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE dim_ora_sold_to_site_useid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_sold_to_site_useid = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S,DIM_ORA_CUSTOMERLOCATION D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.SOLD_TO_SITE_USE_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_sold_to_site_useid <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE dim_ora_bill_to_customerid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_bill_to_customerid = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S,DIM_ORA_HZ_CUSTACCOUNTS D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.BILL_TO_CUSTOMER_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_bill_to_customerid <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE dim_ora_bill_to_site_useid*/
UPDATE fact_ora_ar_sales_invoices F

SET
F.dim_ora_bill_to_site_useid = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S,DIM_ORA_CUSTOMERLOCATION D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.BILL_TO_SITE_USE_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_bill_to_site_useid <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE dim_ora_ship_to_customerid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_ship_to_customerid = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_HZ_CUSTACCOUNTS D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.SHIP_TO_CUSTOMER_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_ship_to_customerid <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE dim_ora_ship_to_site_useid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_ship_to_site_useid = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S,DIM_ORA_CUSTOMERLOCATION D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.SHIP_TO_SITE_USE_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_ship_to_site_useid <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE dim_ora_termid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_termid = D.DIM_ORA_AR_PAYMENT_TERMSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_AR_PAYMENT_TERMS D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.TERM_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_termid <> D.DIM_ORA_AR_PAYMENT_TERMSID;

/*UPDATE dim_ora_date_due_termid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_due_termid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S,DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.TERM_DUE_DATE) = D.DATEVALUE
AND F.dim_ora_date_due_termid <> D.DIM_DATEID;

/*UPDATE dim_ora_primary_salesrepid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_primary_salesrepid = D.DIM_ORA_SALESREPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_SALESREP D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),ifnull(S.PRIMARY_SALESREP_ID,0)) ||'~'|| CONVERT(VARCHAR(200),ifnull(S.ORG_ID,0)) = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_primary_salesrepid <> D.DIM_ORA_SALESREPID;

/*UPDATE dim_ora_date_printing_originalid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_printing_originalid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.PRINTING_ORIGINAL_DATE) = D.DATEVALUE
AND F.dim_ora_date_printing_originalid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_exchangeid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_exchangeid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F, ora_ar_sales_invoices S,DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.EXCHANGE_DATE) = D.DATEVALUE
AND F.dim_ora_date_exchangeid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.LAST_UPDATE_DATE) = D.DATEVALUE
AND F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_last_updated_byid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_last_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_FNDUSER D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_last_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.CREATION_DATE) = D.DATEVALUE
AND F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_FNDUSER D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_set_of_booksid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_set_of_booksid = D.DIM_ORA_GL_SETOFBOOKSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_GL_SETOFBOOKS D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.SET_OF_BOOKS_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_set_of_booksid <> D.DIM_ORA_GL_SETOFBOOKSID;

/*UPDATE dim_ora_inventory_itemid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_inventory_itemid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_INVPRODUCT D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),S.ORG_ID) ||'~'|| CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = D.KEY_ID
and d.rowiscurrent = 1
AND F.dim_ora_inventory_itemid <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE dim_ora_orgid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_orgid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_BUSINESS_ORG D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),'HR_BG') ||'~'|| CONVERT(VARCHAR(200),S.ORG_ID) = D.KEY_ID
and d.rowiscurrent = 1
AND F.dim_ora_orgid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_warehouseid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_warehouseid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_BUSINESS_ORG D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),'INV') ||'~'|| CONVERT(VARCHAR(200),S.WAREHOUSE_ID) = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_warehouseid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE fact_ora_parent_lineid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.fact_ora_parent_lineid = D.FACT_ORA_AR_SALES_INVOICESID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, FACT_ORA_AR_SALES_INVOICES D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.PARENT_LINE_ID = D.CUSTOMER_TRX_LINE_ID and d.rowiscurrent = 1
AND F.fact_ora_parent_lineid <> D.FACT_ORA_AR_SALES_INVOICESID;

/*UPDATE dim_ora_parent_inventory_itemid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_parent_inventory_itemid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_INVPRODUCT D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),S.ORG_ID) ||'~'|| CONVERT(VARCHAR(200),S.PARENT_INVENTORY_ID) = D.KEY_ID
and d.rowiscurrent = 1
AND F.dim_ora_parent_inventory_itemid <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE dim_ora_parent_warehouseid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_parent_warehouseid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_BUSINESS_ORG D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),'INV') ||'~'|| CONVERT(VARCHAR(200),S.PARENT_WAREHOUSE_ID) = D.KEY_ID
and d.rowiscurrent = 1
AND F.dim_ora_parent_warehouseid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_date_invoice_last_updateid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_invoice_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.INVOICE_LAST_UPDATE) = D.DATEVALUE
AND F.dim_ora_date_invoice_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_trx_type_last_updateid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_trx_type_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.TRX_TYPE_LAST_UPDATE) = D.DATEVALUE
AND F.dim_ora_date_trx_type_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_so_header_updatedid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_so_header_updatedid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.SO_HEADER_UPDATE) = D.DATEVALUE
AND F.dim_ora_date_so_header_updatedid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_so_header_creationid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_date_so_header_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_DATE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND to_date(S.SO_CREATION_DATE) = D.DATEVALUE
AND F.dim_ora_date_so_header_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_sales_channel_codeid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_sales_channel_codeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_FND_LOOKUP D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),S.SALES_CHANNEL_CODE) = D.lookup_type
AND D.lookup_type like '%SALES_CHANNEL%'
and d.rowiscurrent = 1
AND F.dim_ora_sales_channel_codeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_order_typeid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_order_typeid = D.DIM_ORA_XACTTYPEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_XACTTYPE D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.ORDER_TYPE_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_order_typeid <> D.DIM_ORA_XACTTYPEID;

/*UPDATE dim_ora_payment_type_codeid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_payment_type_codeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_FND_LOOKUP D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),S.PAYMENT_TYPE_CODE) = D.KEY_ID
AND D.lookup_type like '%OE_PAYMENT_TYPE%'
and d.rowiscurrent = 1
AND F.dim_ora_payment_type_codeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_freight_termsd_codeid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_freight_termsd_codeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_FND_LOOKUP D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),S.FREIGHT_TERMS_CODE) = D.KEY_ID
AND D.lookup_type like '%FREIGHT_TERMS%'
and d.rowiscurrent = 1
AND F.dim_ora_freight_termsd_codeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_ship_from_orgid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_ship_from_orgid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_BUSINESS_ORG D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND CONVERT(VARCHAR(200),'INV') ||'~'|| CONVERT(VARCHAR(200),S.SHIP_FROM_ORG_ID) = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_ship_from_orgid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_code_combinationid*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.dim_ora_code_combinationid = D.DIM_ORA_GL_CODE_COMBID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_GL_CODE_COMB D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
AND S.CODE_COMBINATION_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.dim_ora_code_combinationid <> D.DIM_ORA_GL_CODE_COMBID;

/*UPDATE DD_FUNCTIONAL_CURR_CODE*/
UPDATE fact_ora_ar_sales_invoices F

SET
F.DD_FUNCTIONAL_CURR_CODE = D.CURRENCY_CODE,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S, DIM_ORA_GL_SETOFBOOKS D
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID
 AND S.SET_OF_BOOKS_ID = D.KEY_ID and d.rowiscurrent = 1
AND F.DD_FUNCTIONAL_CURR_CODE <> D.CURRENCY_CODE;

/*UPDATE AMT_EXCHANGERATE*/
UPDATE fact_ora_ar_sales_invoices F
SET
F.AMT_EXCHANGERATE = ifnull(s.EXCHANGE_RATE,1),
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,ora_ar_sales_invoices S
WHERE F.CUST_TRX_LINE_GL_DIST_ID =  S.CUST_TRX_LINE_GL_DIST_ID AND
F.AMT_EXCHANGERATE <> ifnull(s.EXCHANGE_RATE,1);

/*UPDATE AMT_EXCHANGERATE_GBL*/
UPDATE fact_ora_ar_sales_invoices F
SET F.AMT_EXCHANGERATE_GBL = ifnull(GLR.CONVERSION_RATE,1),
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_ar_sales_invoices F,hlp_ORA_GL_DAILY_RATES GLR,DIM_DATE DD
WHERE
 to_date(GLR.CONVERSION_DATE) = DD.DATEVALUE
AND GLR.FROM_CURRENCY   = F.DD_INVOICE_CURRENCY_CODE AND
GLR.TO_CURRENCY     = (select property_value from systemproperty WHERE property = 'customer.global.currency') AND
F.dim_ora_date_trxid = dd.DIM_DATEID AND
glr.rowiscurrent = 1 and
GLR.CONVERSION_TYPE = (select property_value from systemproperty WHERE property = 'GL.conversion_type') AND
F.AMT_EXCHANGERATE_GBL <> ifnull(GLR.CONVERSION_RATE,1);

/*fact_ora_sales_order_line.dd_shipped_not_invoiced_flag*/
DROP TABLE IF EXISTS fact_ora_sales_order_line_tmp;

CREATE TABLE fact_ora_sales_order_line_tmp AS
select fact_ora_sales_order_lineid, dd_line_id
from fact_ora_sales_order_line
where dd_fulfilled_flag            = 'Y'
  and dd_open_flag                 = 'N'
  and dd_line_flow_status_code     <> 'CANCELLED'
  and dd_shipping_interfaced_flag  = 'Y'
  and rowiscurrent = 1;
DROP TABLE IF EXISTS fact_ora_ar_sales_invoices_tmp;

CREATE TABLE fact_ora_ar_sales_invoices_tmp AS
select distinct dd_interface_line_attribute6 line_id
from fact_ora_ar_sales_invoices si
join fact_ora_sales_order_line_tmp so
on so.dd_line_id = (CASE WHEN si.dd_interface_line_attribute6='Not Set' then 0 ELSE si.dd_interface_line_attribute6 end)
and si.rowiscurrent = 1;


UPDATE fact_ora_sales_order_line fsol
SET dd_shipped_not_invoiced_flag = 'Y'
from fact_ora_sales_order_line fsol, fact_ora_sales_order_line_tmp tmp
where fsol.fact_ora_sales_order_lineid = tmp.fact_ora_sales_order_lineid;

UPDATE fact_ora_sales_order_line fsol
SET dd_shipped_not_invoiced_flag = 'N'
from fact_ora_sales_order_line fsol, fact_ora_ar_sales_invoices_tmp tmp
where fsol.dd_line_id = tmp.line_id;

/*DROP TABLE IF EXISTS fact_ora_sales_order_line_tmp*/
DROP TABLE IF EXISTS fact_ora_ar_sales_invoices_tmp;

/*update dim_ora_mdg_productid*/
UPDATE fact_ora_ar_sales_invoices f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM fact_ora_ar_sales_invoices f_inv,dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.dim_ora_inventory_itemid = d_invp.dim_ora_invproductid
 AND  d_invp.mdm_globalid = d_mp.partnumber
 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;

 /*update dim_ora_bwproducthierarchyid*/
 UPDATE fact_ora_ar_sales_invoices f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_ar_sales_invoices f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inventory_itemid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND dp.productgroupsbu = bw.productgroup
AND current_date BETWEEN bw.upperhierstartdate AND bw.upperhierenddate
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE fact_ora_ar_sales_invoices f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_ar_sales_invoices f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inventory_itemid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

/* Add fields from Sales - Roxana D - 2017-11-28 */

update fact_ora_ar_sales_invoices si
set si.dim_ora_otd_promise_dateid = so.dim_ora_otd_promise_dateid
from fact_ora_ar_sales_invoices si    
join fact_ora_sales_order_line so
on so.dd_line_id = (CASE WHEN si.dd_interface_line_attribute6='Not Set' then 0 ELSE si.dd_interface_line_attribute6 end)
and si.rowiscurrent = 1
and si.dim_ora_otd_promise_dateid <> so.dim_ora_otd_promise_dateid;

update fact_ora_ar_sales_invoices si
set si.dim_ora_date_customer_requestedid = so.dim_ora_date_customer_requestedid
from fact_ora_ar_sales_invoices si    
join fact_ora_sales_order_line so
on so.dd_line_id = (CASE WHEN si.dd_interface_line_attribute6='Not Set' then 0 ELSE si.dd_interface_line_attribute6 end)
and si.rowiscurrent = 1
and si.dim_ora_date_customer_requestedid <> so.dim_ora_date_customer_requestedid; 



update fact_ora_ar_sales_invoices si
set si.DD_INTERCOMPFLAG = so.DD_INTERCOMPFLAG
from fact_ora_ar_sales_invoices si    
join fact_ora_sales_order_line so
on so.dd_line_id = (CASE WHEN si.dd_interface_line_attribute6='Not Set' then 0 ELSE si.dd_interface_line_attribute6 end)
and si.rowiscurrent = 1
and si.DD_INTERCOMPFLAG <> so.DD_INTERCOMPFLAG;


UPDATE fact_ora_ar_sales_invoices si
SET SI.AMT_OPENORDERVALUE = SO.AMT_OPENORDERVALUE
FROM fact_ora_ar_sales_invoices si,
(select CASE WHEN dd_interface_line_attribute6='Not Set' then 0 ELSE dd_interface_line_attribute6 end as Interface6, max(cust_trx_line_gl_dist_id) as cust_trx_line_gl_dist_id
from  fact_ora_ar_sales_invoices 
group by CASE WHEN dd_interface_line_attribute6='Not Set' then 0 ELSE dd_interface_line_attribute6 end
 ) selrow,
(SELECT dd_line_id,
sum( (case when f_osol.dd_cancelled_l_flag<>'N' then 0 else ((f_osol.ct_ordered_quantity-f_osol.ct_total_shipped_quantity)-f_osol.ct_cancelled_quantity) end)*f_osol.ct_product_unit_selling_price) as AMT_OPENORDERVALUE 
from fact_ora_sales_order_line f_osol group by dd_line_id)  so
where
so.dd_line_id = 
(CASE WHEN si.dd_interface_line_attribute6='Not Set' then 0 ELSE si.dd_interface_line_attribute6 end)
and si.rowiscurrent = 1
and si.cust_trx_line_gl_dist_id = selrow.cust_trx_line_gl_dist_id
and selrow.interface6 = 
(CASE WHEN si.dd_interface_line_attribute6='Not Set' then 0 ELSE si.dd_interface_line_attribute6 end)
and SI.AMT_OPENORDERVALUE <> SO.AMT_OPENORDERVALUE;


/* End - Add fields from Sales - Roxana D - 2017-11-28 */

/*Add dd_line_type_id - Roxana D 2017-12-10 */

UPDATE fact_ora_ar_sales_invoices T
SET t.DD_LINEORDERTYPE = IFNULL(TL.NAME,'Not Set')
FROM fact_ora_ar_sales_invoices T,ORA_OE_TRANSACTION_TYPES_TL TL,ORA_OE_ORDER_HEADER_LINES_ALL s
WHERE CASE WHEN T.dd_INTERFACE_LINE_ATTRIBUTE6 =  'Not Set' then 0 else dd_INTERFACE_LINE_ATTRIBUTE6 end = s.LINE_ID
AND s.line_type_id = TL.TRANSACTION_TYPE_ID
AND t.DD_LINEORDERTYPE <> IFNULL(TL.NAME,'Not Set');

/*Update  dd_ITEM_TYPE_CODE from OE_ORDER_LINES_ALL*/

UPDATE fact_ora_ar_sales_invoices T
SET t.dd_ITEM_TYPE_CODE = ifnull(S.ITEM_TYPE_CODE,'Not Set')
FROM fact_ora_ar_sales_invoices T,ORA_OE_ORDER_HEADER_LINES_ALL s
WHERE CASE WHEN T.dd_INTERFACE_LINE_ATTRIBUTE6 =  'Not Set' then 0 else t.dd_INTERFACE_LINE_ATTRIBUTE6 end = s.LINE_ID
and t.dd_ITEM_TYPE_CODE <> ifnull(S.ITEM_TYPE_CODE,'Not Set');


 