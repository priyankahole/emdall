/*set session authorization oracle_data*/

/*DELETE FROM fact_ORA_SALES_ORDER_LINE*/
/*SELECT * FROM fact_ORA_SALES_ORDER_LINE*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_sales_order_line';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_sales_order_line',IFNULL(MAX(fact_ora_sales_order_lineID),0)
FROM fact_ora_sales_order_line;


/*update fact columns*/
UPDATE fact_ora_sales_order_line F
SET
CT_AGING_DAYS = ifnull(days_between(REQUEST_DATE,ACTUAL_SHIPMENT_DATE), -100000),
CT_PRODUCT_UNIT_COST = ifnull(S.UNIT_COST,0),
CT_PRODUCT_UNIT_LIST_PRICE = ifnull(S.UNIT_LIST_PRICE,0) ,
CT_PRODUCT_UNIT_SELLING_PRICE = ifnull(S.UNIT_SELLING_PRICE,0),
CT_MANUFACTURING_LEAD_TIME = ifnull(S.MFG_LEAD_TIME,0),
CT_SHIP_TOLERANCE_ABOVE = ifnull(S.SHIP_TOLERANCE_ABOVE,0),
CT_SHIP_TOLERANCE_BELOW = ifnull(S.SHIP_TOLERANCE_BELOW,0),
CT_DELIVERY_LEAD_TIME = ifnull(S.DELIVERY_LEAD_TIME,0),
CT_ORDERED_QUANTITY = ifnull(S.ORDERED_QUANTITY,0),
CT_CANCELLED_QUANTITY = ifnull(S.CANCELLED_QUANTITY,0),
CT_SALES_QUANTITY = ifnull(S.ORDERED_QUANTITY,0)-ifnull(CANCELLED_QUANTITY,0),
CT_FULFILLED_QUANTITY = ifnull(S.FULFILLED_QUANTITY,0),
CT_TOTAL_SHIPPED_QUANTITY = ifnull(S.SHIPPED_QUANTITY,0),
CT_TOTAL_INVOICED_QUANTITY = ifnull(S.INVOICED_QUANTITY,0),
AMT_NET_ORDERED = ifnull(S.ORDERED_QUANTITY,0)*ifnull(UNIT_SELLING_PRICE,0),
AMT_TAX = ifnull(S.TAX_VALUE,0),
AMT_COST = ifnull(S.ORDERED_QUANTITY,0)*ifnull(UNIT_COST,0),
AMT_LIST = ifnull(S.ORDERED_QUANTITY,0)*ifnull(UNIT_LIST_PRICE,0),
DD_OTIF_FLAG = CASE WHEN S.REQUEST_DATE>=S.ACTUAL_SHIPMENT_DATE and ifnull(S.ORDERED_QUANTITY,0) = ifnull(S.SHIPPED_QUANTITY,0) THEN 1 ELSE 0 END,
DD_LINE_CATEGORY = S.LINE_CATEGORY_CODE,
DD_ORDER_QUANTITY_UOM = ifnull(S.ORDER_QUANTITY_UOM,'Not Set'),
DD_ORDER_NUMBER = S.ORDER_NUMBER,
DD_LINE_NUMBER = S.LINE_NUMBER,
DD_SHIPMENT_NUMBER = S.SHIPMENT_NUMBER,
DD_CUST_PO_NUMBER = ifnull(S.CUST_PO_NUMBER,'Not Set'),
DD_CANCELLED_H_FLAG = ifnull(S.HEADER_CANCELLED_FLAG,'N'),
DD_CANCELLED_L_FLAG = ifnull(S.LINE_CANCELLED_FLAG,'N'),
DD_OPEN_FLAG = ifnull(S.OPEN_FLAG,'N'),
DD_FULFILLED_FLAG = ifnull(S.FULFILLED_FLAG,'N'),
DD_SHIPPABLE_FLAG = ifnull(S.SHIPPABLE_FLAG,'N'),
DD_TAX_EXEMPT_FLAG = ifnull(S.TAX_EXEMPT_FLAG,'N'),
DD_BOOKED_FLAG = S.BOOKED_FLAG,
DD_MODEL_REMNANT_FLAG = ifnull(S.MODEL_REMNANT_FLAG, 'N'),
DD_ON_HOLD_FLAG = case when S.HOLD_NAME is null then 'N' else 'Y' end ,
DD_OPERATION_BACKLOG_FLAG = case when S.HOLD_NAME is not null or (S.line_cancelled_flag ='N' and ifnull(S.SHIPPED_QUANTITY,0) <= (ifnull(S.ORDERED_QUANTITY,0)- ifnull(S.cancelled_quantity,0))  and S.REQUEST_DATE > current_timestamp) then 'Y' else 'N' end,
DD_TRANSACTIONAL_CURR_CODE = ifnull(S.TRANSACTIONAL_CURR_CODE,'Not Set'),
dd_line_flow_status_code    = ifnull(S.line_flow_status_code,'Not Set'),
dd_shipping_interfaced_flag = ifnull(S.shipping_interfaced_flag,'N'),
dd_subinventory = ifnull(S.subinventory,'Not Set'),
DW_UPDATE_DATE = current_timestamp, 
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1',
dd_lineno_compno = s.LINE_NUMBER||'.'||s.SHIPMENT_NUMBER||decode(s.COMPONENT_NUMBER,NULL,NULL,'.')||s.COMPONENT_NUMBER /* 25 may 2016  Cornelia add dd_lineno_compno */
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S 
WHERE 
f.dd_header_id = s.header_id AND f.dd_line_id   = s.line_id; 


/*insert new rows*/
INSERT INTO fact_ora_sales_order_line
(
fact_ora_sales_order_lineid,
dim_ora_account_repid,dim_ora_business_orgid,dim_ora_channel_typeid,dim_ora_company_orgid,dim_ora_cost_centerid,
dim_ora_customer_bill_to_locationid,dim_ora_customer_ship_to_locationid,dim_ora_customer_sold_to_locationid,dim_ora_customer_bill_toid,
dim_ora_customer_ship_toid,dim_ora_customer_sold_toid,dim_ora_customer_account_bill_toid,dim_ora_customer_account_ship_toid,
dim_ora_customer_account_sold_toid,dim_ora_inventory_orgid,dim_ora_inv_productid,dim_ora_order_line_statusid,dim_ora_payment_methodid,
dim_ora_payment_termid,dim_ora_productid,dim_ora_sales_groupid,dim_ora_sales_office_locationid,dim_ora_salesrepid,dim_ora_servicerepid,
dim_ora_order_line_typeid,dim_ora_order_header_typeid,dim_ora_order_header_statusid,dim_ora_freight_termid,dim_ora_shipment_methodid,
dim_ora_date_order_bookedid,dim_ora_date_customer_requestedid,dim_ora_date_enteredid,dim_ora_date_orderedid,dim_ora_date_promise_deliveryid,
dim_ora_date_scheduled_shipid,dim_ora_date_actual_shipid,dim_ora_date_earliest_shipmentid,dim_ora_date_order_confirmedid,
dim_ora_date_actual_fulfillmentid,dim_ora_date_fulfillmentid,dim_ora_date_actual_arrival_dateid,dim_ora_date_schedule_arrivalid,
dim_ora_date_taxid,dim_ora_date_pricingid,dim_ora_gl_set_of_booksid,dim_ora_hr_operatingunitid,dim_ora_aging_days_bucketid,
ct_aging_days,ct_product_unit_cost,ct_product_unit_list_price,ct_product_unit_selling_price,ct_manufacturing_lead_time,
ct_ship_tolerance_above,ct_ship_tolerance_below,ct_delivery_lead_time,ct_ordered_quantity,ct_cancelled_quantity,ct_sales_quantity,
ct_fulfilled_quantity,ct_confirmed_quantity,ct_fill_quantity,ct_fill_quantity_crd,ct_total_shipped_quantity,ct_total_invoiced_quantity,
amt_net_ordered,amt_tax,amt_cost,amt_list,amt_discount,dd_line_category,dd_order_quantity_uom,dd_order_number,dd_line_number,
dd_shipment_number,dd_orig_ref_doc_number,dd_cust_po_number,dd_otif_flag,dd_cancelled_h_flag,dd_cancelled_l_flag,dd_open_flag,
dd_fulfilled_flag,dd_shippable_flag,dd_tax_exempt_flag,dd_booked_flag,dd_model_remnant_flag,dd_on_hold_flag,dd_financial_backlog_flag,
dd_operation_backlog_flag,dd_header_id,dd_line_id,dd_transactional_curr_code,dd_functional_curr_code,
dd_shipped_not_invoiced_flag,ct_so_touch_count,dd_line_flow_status_code,dd_shipping_interfaced_flag,dd_subinventory,
amt_exchangerate,amt_exchangerate_gbl,dim_ora_created_byid,dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_date_last_updateid,dim_ora_mtl_plannersid,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id,dd_lineno_compno /* 25 may 2016  Cornelia add dd_lineno_compno */
,ct_countsalesdocitem
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_sales_order_line' ) + ROW_NUMBER() OVER(order by '') fact_ora_sales_order_lineID,
1 AS DIM_ACCOUNT_REPID,
1 AS DIM_BUSINESS_ORGID,
1 AS DIM_CHANNEL_TYPEID,
1 AS DIM_ORA_COMPANY_ORGID,
1 AS DIM_COST_CENTERID,
1 AS DIM_CUSTOMER_BILL_TO_LOCATIONID,
1 AS DIM_CUSTOMER_SHIP_TO_LOCATIONID,
1 AS DIM_CUSTOMER_SOLD_TO_LOCATIONID,
1 AS DIM_ORA_CUSTOMER_BILL_TOID ,
1 AS DIM_ORA_CUSTOMER_SHIP_TOID ,
1 AS DIM_ORA_CUSTOMER_SOLD_TOID ,
1 AS DIM_ORA_CUSTOMER_ACCOUNT_BILL_TOID ,
1 AS DIM_ORA_CUSTOMER_ACCOUNT_SHIP_TOID,
1 AS DIM_ORA_CUSTOMER_ACCOUNT_SOLD_TOID,
1 AS DIM_INVENTORY_ORGID,
1 AS DIM_INV_PRODUCTID,
1 AS DIM_ORDER_LINE_STATUSID,
1 AS DIM_PAYMENT_METHODID,
1 AS DIM_PAYMENT_TERMID,
1 AS DIM_PRODUCTID,
1 AS DIM_SALES_GROUPID,
1 AS DIM_SALES_OFFICE_LOCATIONID,
1 AS DIM_SALESREPID,
1 AS DIM_ORA_SERVICEREPID,
1 AS DIM_ORDER_LINE_TYPEID,
1 AS DIM_ORDER_HEADER_TYPEID,
1 AS DIM_ORA_ORDER_HEADER_STATUSID,
1 AS DIM_FREIGHT_TERMID,
1 AS DIM_SHIPMENT_METHODID,
1 AS DIM_DATE_ORDER_BOOKEDID,
1 AS DIM_DATE_CUSTOMER_REQUESTEDID,
1 AS DIM_DATE_ENTEREDID,
1 AS DIM_DATE_ORDEREDID,
1 AS DIM_DATE_PROMISE_DELIVERYID,
1 AS DIM_DATE_SCHEDULED_SHIPID,
1 AS DIM_DATE_ACTUAL_SHIPID,
1 AS DIM_DATE_EARLIEST_SHIPMENTID,
1 AS DIM_DATE_ORDER_CONFIRMEDID,
1 AS DIM_DATE_ACTUAL_FULFILLMENTID,
1 AS DIM_DATE_FULFILLMENTID,
1 AS DIM_DATE_ACTUAL_ARRIVAL_DATEID,
1 AS DIM_DATE_SCHEDULE_ARRIVALID,
1 AS DIM_DATE_TAXID,
1 AS DIM_DATE_PRICINGID,
1 AS DIM_GL_SET_OF_BOOKSID,
1 AS DIM_HR_OPERATINGUNITID,
1 AS DIM_ORA_AGING_DAYS_BUCKETID,
ifnull((to_date(REQUEST_DATE)-to_date(ACTUAL_SHIPMENT_DATE)), -100000) CT_AGING_DAYS,
ifnull(S.UNIT_COST,0) CT_PRODUCT_UNIT_COST ,
ifnull(S.UNIT_LIST_PRICE,0)    CT_PRODUCT_UNIT_LIST_PRICE ,
ifnull(S.UNIT_SELLING_PRICE,0)    CT_PRODUCT_UNIT_SELLING_PRICE,
ifnull(S.MFG_LEAD_TIME,0) AS CT_MANUFACTURING_LEAD_TIME,
ifnull(S.SHIP_TOLERANCE_ABOVE,0) AS CT_SHIP_TOLERANCE_ABOVE,
ifnull(S.SHIP_TOLERANCE_BELOW,0) AS CT_SHIP_TOLERANCE_BELOW,
ifnull(S.DELIVERY_LEAD_TIME,0) AS CT_DELIVERY_LEAD_TIME,
ifnull(S.ORDERED_QUANTITY,0) AS CT_ORDERED_QUANTITY,
ifnull(S.CANCELLED_QUANTITY,0) AS CT_CANCELLED_QUANTITY,
ifnull(S.ORDERED_QUANTITY,0)-ifnull(S.CANCELLED_QUANTITY,0) AS CT_SALES_QUANTITY,
ifnull(S.FULFILLED_QUANTITY,0) AS CT_FULFILLED_QUANTITY,
0 AS CT_CONFIRMED_QUANTITY,
0 AS CT_FILL_QUANTITY,
0 AS CT_FILL_QUANTITY_CRD,
ifnull(S.SHIPPED_QUANTITY,0) AS CT_TOTAL_SHIPPED_QUANTITY,
ifnull(S.INVOICED_QUANTITY,0) AS CT_TOTAL_INVOICED_QUANTITY,
ifnull(S.ORDERED_QUANTITY,0)*ifnull(S.UNIT_SELLING_PRICE,0) AS AMT_NET_ORDERED,
ifnull(S.TAX_VALUE,0) AS AMT_TAX,
ifnull(S.ORDERED_QUANTITY,0)*ifnull(S.UNIT_COST,0) AS AMT_COST,
ifnull(S.ORDERED_QUANTITY,0)*ifnull(S.UNIT_LIST_PRICE,0) AS AMT_LIST,
0 AS AMT_DISCOUNT,
S.LINE_CATEGORY_CODE AS DD_LINE_CATEGORY,
ifnull(S.ORDER_QUANTITY_UOM,'Not Set') AS DD_ORDER_QUANTITY_UOM,
S.ORDER_NUMBER AS DD_ORDER_NUMBER,
S.LINE_NUMBER AS DD_LINE_NUMBER,
S.SHIPMENT_NUMBER AS DD_SHIPMENT_NUMBER,
'Not Set' AS DD_ORIG_REF_DOC_NUMBER,
ifnull(S.CUST_PO_NUMBER,'Not Set') AS DD_CUST_PO_NUMBER,
CASE WHEN S.REQUEST_DATE>=S.ACTUAL_SHIPMENT_DATE and ifnull(S.ORDERED_QUANTITY,0) = ifnull(S.SHIPPED_QUANTITY,0) THEN 1 ELSE 0 END AS DD_OTIF_FLAG,
ifnull(S.HEADER_CANCELLED_FLAG,'N') AS DD_CANCELLED_H_FLAG,
ifnull(S.LINE_CANCELLED_FLAG,'N') AS DD_CANCELLED_L_FLAG,
ifnull(S.OPEN_FLAG,'N') AS DD_OPEN_FLAG,
ifnull(S.FULFILLED_FLAG,'N') AS DD_FULFILLED_FLAG,
ifnull(S.SHIPPABLE_FLAG,'N') AS DD_SHIPPABLE_FLAG,
ifnull(S.TAX_EXEMPT_FLAG,'N') AS DD_TAX_EXEMPT_FLAG,
S.BOOKED_FLAG AS DD_BOOKED_FLAG,
ifnull(S.MODEL_REMNANT_FLAG, 'N') AS DD_MODEL_REMNANT_FLAG,
case when S.HOLD_NAME is null then 'N' else 'Y' end AS DD_ON_HOLD_FLAG,
'N' DD_FINANCIAL_BACKLOG_FLAG,
'N' DD_OPERATION_BACKLOG_FLAG,
ifnull(S.HEADER_ID,0) AS DD_HEADER_ID,
ifnull(S.LINE_ID,0) AS DD_LINE_ID,
ifnull(S.TRANSACTIONAL_CURR_CODE,'Not Set') DD_TRANSACTIONAL_CURR_CODE,
'Not Set' DD_FUNCTIONAL_CURR_CODE,
'Not Set' AS dd_shipped_not_invoiced_flag,
1 AS ct_so_touch_count,
ifnull(S.line_flow_status_code,'Not Set') dd_line_flow_status_code,
ifnull(S.shipping_interfaced_flag,'N') dd_shipping_interfaced_flag,
ifnull(S.subinventory,'Not Set') dd_subinventory,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
1 AS DIM_CREATED_BYID,
1 AS DIM_LAST_UPDATED_BYID,
1 AS DIM_DATE_CREATIONID,
1 AS DIM_DATE_LAST_UPDATEID,
1 as dim_ora_mtl_plannersid,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID,
(s.LINE_NUMBER||'.'||s.SHIPMENT_NUMBER||decode(s.COMPONENT_NUMBER,NULL,NULL,'.')||s.COMPONENT_NUMBER) as dd_lineno_compno /* 25 may 2016  Cornelia add dd_lineno_compno */
,1 ct_countsalesdocitem
FROM ORA_OE_ORDER_HEADER_LINE S 
LEFT JOIN fact_ora_sales_order_line F ON
F.DD_HEADER_ID = S.HEADER_ID AND
F.DD_LINE_ID   = S.LINE_ID 
WHERE F.DD_HEADER_ID is null and F.DD_LINE_ID is null;


/*UPDATE DIM_BUSINESS_ORG_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_BUSINESS_ORGID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_BUSINESS_ORG D
ON  'HR_BG' ||'~'|| CONVERT(VARCHAR(200),S.ORG_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_BUSINESS_ORGID <> D.DIM_ORA_BUSINESS_ORGID;

TRUNCATE TABLE prop_tmp;
INSERT INTO prop_tmp 
select property_value,property from systemproperty WHERE property = 'OM.security_group_id' or property = 'OM.view_appplication_id';

/*UPDATE DIM_CHANNEL_TYPE_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_CHANNEL_TYPEID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_FND_LOOKUP D 
ON S.SALES_CHANNEL_CODE  = D.LOOKUP_CODE
and D.LOOKUP_TYPE like '%CHANNEL%' and d.rowiscurrent = 1
JOIN  prop_tmp sg
on sg.property_value =  d.security_group_id and sg.property='OM.security_group_id'
JOIN prop_tmp app
on app.property_value = d.view_application_id and app.property = 'OM.view_appplication_id'
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CHANNEL_TYPEID <> D.DIM_ORA_FND_LOOKUPID;


/*UPDATE DIM_ORA_COMPANY_ORGID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_COMPANY_ORGID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_BUSINESS_ORG D 
ON  'HR_ORG' ||'~'||CONVERT(VARCHAR(200),S.ORG_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_COMPANY_ORGID <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE DIM_CUSTOMER_BILL_TO_LOCATION_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_CUSTOMER_BILL_TO_LOCATIONID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.INVOICE_TO_ORG_ID = D.KEY_ID and d.rowiscurrent = 1 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_BILL_TO_LOCATIONID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_CUSTOMER_SHIP_TO_LOCATION_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_CUSTOMER_SHIP_TO_LOCATIONID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.SHIP_TO_ORG_ID = D.KEY_ID and d.rowiscurrent = 1  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_SHIP_TO_LOCATIONID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_ORA_CUSTOMER_BILL_TOID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_CUSTOMER_BILL_TOID = P.DIM_ORA_HZ_PARTYID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.INVOICE_TO_ORG_ID = D.KEY_ID and d.rowiscurrent = 1 
JOIN DIM_ORA_HZ_PARTY P ON D.PARTY_ID =P.KEY_ID  and P.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_BILL_TOID <> P.DIM_ORA_HZ_PARTYID;

/*UPDATE DIM_ORA_CUSTOMER_SHIP_TOID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_CUSTOMER_SHIP_TOID = P.DIM_ORA_HZ_PARTYID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.SHIP_TO_ORG_ID = D.KEY_ID and d.rowiscurrent = 1  
JOIN DIM_ORA_HZ_PARTY P ON D.PARTY_ID =P.KEY_ID  and P.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_SHIP_TOID <> P.DIM_ORA_HZ_PARTYID;

/*UPDATE DIM_ORA_CUSTOMER_ACCOUNT_SOLD_TOID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_CUSTOMER_ACCOUNT_SOLD_TOID = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S 
JOIN DIM_ORA_HZ_CUSTACCOUNTS D ON S.SOLD_TO_ORG_ID  = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_ACCOUNT_SOLD_TOID <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE DIM_ORA_CUSTOMER_SOLD_TOID*/
UPDATE fact_ora_sales_order_line F
SET 
f.dim_ora_customer_sold_toid = p.dim_ora_hz_partyid,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S 
JOIN DIM_ORA_HZ_CUSTACCOUNTS D ON S.SOLD_TO_ORG_ID  = D.KEY_ID and d.rowiscurrent = 1
JOIN dim_ora_hz_party P ON D.PARTY_ID =P.KEY_ID and P.rowiscurrent = 1 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_SOLD_TOID <> P.DIM_ORA_HZ_PARTYID;

/*UPDATE DIM_CUSTOMER_SOLD_TO_LOCATION_ID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_CUSTOMER_SOLD_TO_LOCATIONID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.SOLD_TO_SITE_USE_ID = D.KEY_ID and d.rowiscurrent = 1 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_SOLD_TO_LOCATIONID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_ORA_CUSTOMER_ACCOUNT_SHIP_TOID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_CUSTOMER_ACCOUNT_SHIP_TOID = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_CUSTOMERLOCATION cl ON S.SHIP_TO_ORG_ID = CL.KEY_ID and cl.rowiscurrent = 1  
JOIN DIM_ORA_HZ_CUSTACCOUNTS D ON CL.CUST_ACCOUNT_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_ACCOUNT_SHIP_TOID <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE DIM_ORA_CUSTOMER_ACCOUNT_BILL_TOID */
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_CUSTOMER_ACCOUNT_BILL_TOID = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_CUSTOMERLOCATION cl ON S.INVOICE_TO_ORG_ID = CL.KEY_ID and cl.rowiscurrent = 1  
JOIN DIM_ORA_HZ_CUSTACCOUNTS D ON CL.CUST_ACCOUNT_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CUSTOMER_ACCOUNT_BILL_TOID <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE DIM_INVENTORY_ORG_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_INVENTORY_ORGID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_BUSINESS_ORG D ON 'INV~'|| CONVERT(VARCHAR(200),s.ship_from_org_id) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_INVENTORY_ORGID <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE DIM_INV_PRODUCT_ID*/
UPDATE fact_ora_sales_order_line F
SET 
f.dim_ora_inv_productid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_INVPRODUCT D ON CONVERT(VARCHAR(200),S.ship_from_org_id) ||'~'|| CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_INV_PRODUCTID <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE DIM_ORDER_LINE_STATUS_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_ORDER_LINE_STATUSID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_FND_LOOKUP D  
ON S.LINE_FLOW_STATUS_CODE = D.LOOKUP_CODE
and D.LOOKUP_TYPE like 'FLOW_STATUS' and d.rowiscurrent = 1
JOIN  prop_tmp sg
on sg.property_value =  d.security_group_id and sg.property='OM.security_group_id'
JOIN prop_tmp app
on app.property_value = d.view_application_id and app.property = 'OM.view_appplication_id'
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_ORDER_LINE_STATUSID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_ORDER_HEADER_STATUSID,*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_ORDER_HEADER_STATUSID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_FND_LOOKUP D  
ON S.LINE_FLOW_STATUS_CODE = D.LOOKUP_CODE
and D.LOOKUP_TYPE like 'FLOW_STATUS' and d.rowiscurrent = 1 
JOIN  prop_tmp sg
on sg.property_value =  d.security_group_id and sg.property='OM.security_group_id'
JOIN prop_tmp app
on app.property_value = d.view_application_id and app.property = 'OM.view_appplication_id'
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_ORDER_HEADER_STATUSID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_PAYMENT_METHOD_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_PAYMENT_METHODID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_FND_LOOKUP D 
ON S.PAYMENT_TYPE_CODE = D.LOOKUP_CODE
and D.LOOKUP_TYPE like '%OE_PAYMENT_TYPE%' and d.rowiscurrent = 1
JOIN  prop_tmp sg
on sg.property_value =  d.security_group_id and sg.property='OM.security_group_id'
JOIN prop_tmp app
on app.property_value = d.view_application_id and app.property = 'OM.view_appplication_id'
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_PAYMENT_METHODID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_PAYMENT_TERM_ID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_PAYMENT_TERMID = D.DIM_ORA_AR_PAYMENT_TERMSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_AR_PAYMENT_TERMS D ON S.PAYMENT_TERM_ID = D.KEY_ID and d.rowiscurrent = 1 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_PAYMENT_TERMID <> D.DIM_ORA_AR_PAYMENT_TERMSID;

/*UPDATE DIM_PRODUCT_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_PRODUCTID = D.DIM_ORA_PRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_PRODUCT D ON S.INVENTORY_ITEM_ID = D.KEY_ID and d.rowiscurrent = 1  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_PRODUCTID <> D.DIM_ORA_PRODUCTID;

/*UPDATE DIM_SALESREP_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_SALESREPID = D.DIM_ORA_SALESREPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_SALESREP D ON CONVERT(VARCHAR(200),ifnull(S.SALESREP_ID,0)) ||'~'|| CONVERT(VARCHAR(200),ifnull(S.ORG_ID,0)) = D.KEY_ID and d.rowiscurrent = 1  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_SALESREPID <> D.DIM_ORA_SALESREPID;

/*UPDATE DIM_ORDER_LINE_TYPE_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_ORDER_LINE_TYPEID = D.DIM_ORA_XACTTYPEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_XACTTYPE D ON S.LINE_TYPE_ID = D.KEY_ID and d.rowiscurrent = 1  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_ORDER_LINE_TYPEID <> D.DIM_ORA_XACTTYPEID;

/*UPDATE DIM_ORDER_HEADER_TYPE_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_ORDER_HEADER_TYPEID = D.DIM_ORA_XACTTYPEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_XACTTYPE D ON S.ORDER_TYPE_ID = D.KEY_ID and d.rowiscurrent = 1  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_ORDER_HEADER_TYPEID <> D.DIM_ORA_XACTTYPEID;

/*UPDATE DIM_FREIGHT_TERM_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_FREIGHT_TERMID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_FND_LOOKUP D 
ON S.FREIGHT_TERMS_CODE = D.LOOKUP_CODE
and D.LOOKUP_TYPE like '%FREIGHT_TERMS%' and d.rowiscurrent = 1
JOIN  prop_tmp sg
on sg.property_value =  d.security_group_id and sg.property='OM.security_group_id'
JOIN prop_tmp app
on app.property_value = d.view_application_id and app.property = 'OM.view_appplication_id'
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_FREIGHT_TERMID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_SHIPMENT_METHOD_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_SHIPMENT_METHODID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_FND_LOOKUP D 
ON S.SHIPPING_METHOD_CODE = D.LOOKUP_CODE
and D.LOOKUP_TYPE like '%SHIP_METHOD%' and d.rowiscurrent = 1 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_SHIPMENT_METHODID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_DATE_ORDER_BOOKED_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_DATE_ORDER_BOOKEDID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.BOOKED_DATE) = D.DATEVALUE  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_ORDER_BOOKEDID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_CUSTOMER_REQUESTED_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.dim_ora_date_customer_requestedid_dimD = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.REQUEST_DATE) = D.DATEVALUE  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.dim_ora_date_customer_requestedid_dimD <> D.DIM_DATEID;

/*UPDATE DIM_DATE_CUSTOMER_REQUESTED_ID*/
 UPDATE  fact_ora_sales_order_line f_so
  SET f_so.dim_ora_date_customer_requestedid = dfc.dim_dateid
 FROM fact_ora_sales_order_line f_so,ORA_OE_ORDER_HEADER_LINE S, dim_date_factory_calendar dfc, dim_ora_business_org dbo
 WHERE  f_so.DD_HEADER_ID = S.HEADER_ID AND f_so.DD_LINE_ID = S.LINE_ID
   AND to_date(S.REQUEST_DATE) = dfc.datevalue
   AND f_so.dim_ora_inventory_orgid = dbo.dim_ora_business_orgid
   AND CASE WHEN dbo.organization_code IN ('BAW','BUR','HAW','KAP','KAW','LRW','SEW','TEW') THEN 'BAW' 
            WHEN dbo.organization_code IN ('LKP','UKW') THEN 'LKP'
            ELSE dbo.organization_code 
       END = dfc.plantcode_factory;


/*UPDATE DIM_DATE_ENTERED_ID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_DATE_ENTEREDID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.BOOKED_DATE) = D.DATEVALUE 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_ENTEREDID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_ORDERED_ID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_DATE_ORDEREDID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.ORDERED_DATE) = D.DATEVALUE 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_ORDEREDID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_PROMISE_DELIVERY_ID*/
UPDATE fact_ora_sales_order_line F
SET 
f.dim_ora_date_promise_deliveryid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.PROMISE_DATE) = D.DATEVALUE  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_PROMISE_DELIVERYID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_SCHEDULED_SHIP_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_DATE_SCHEDULED_SHIPID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.SCHEDULE_SHIP_DATE) = D.DATEVALUE  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_SCHEDULED_SHIPID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_ACTUAL_SHIP_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_DATE_ACTUAL_SHIPID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.ACTUAL_SHIPMENT_DATE) = D.DATEVALUE  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_ACTUAL_SHIPID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_EARLIEST_SHIPMENT_ID*/
UPDATE fact_ora_sales_order_line F  
SET 
F.DIM_ORA_DATE_EARLIEST_SHIPMENTID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.EARLIEST_SHIP_DATE) = D.DATEVALUE
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_EARLIEST_SHIPMENTID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_ORDER_CONFIRMED_ID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_DATE_ORDER_CONFIRMEDID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.ORDER_FIRMED_DATE) = D.DATEVALUE 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_ORDER_CONFIRMEDID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_ACTUAL_FULFILLMENT_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_DATE_ACTUAL_FULFILLMENTID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.ACTUAL_FULFILLMENT_DATE) = D.DATEVALUE
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_ACTUAL_FULFILLMENTID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_FULFILLMENT_ID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_DATE_FULFILLMENTID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.FULFILLMENT_DATE) = D.DATEVALUE 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_FULFILLMENTID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_ACTUAL_ARRIVAL_DATE_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_DATE_ACTUAL_ARRIVAL_DATEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.ACTUAL_ARRIVAL_DATE) = D.DATEVALUE
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_ACTUAL_ARRIVAL_DATEID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_SCHEDULE_ARRIVAL_ID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_DATE_SCHEDULE_ARRIVALID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.SCHEDULE_ARRIVAL_DATE) = D.DATEVALUE 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_SCHEDULE_ARRIVALID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_TAX_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_DATE_TAXID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.TAX_DATE) = D.DATEVALUE  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_TAXID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_PRICING_ID*/
UPDATE fact_ora_sales_order_line F 
SET 
F.DIM_ORA_DATE_PRICINGID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.PRICING_DATE) = D.DATEVALUE 
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_PRICINGID <> D.DIM_DATEID;

/*UPDATE DIM_HR_OPERATINGUNIT_ID*/
UPDATE fact_ora_sales_order_line F  
SET 
F.DIM_ORA_HR_OPERATINGUNITID = D.DIM_ORA_HROPERATINGUNITID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN dim_ora_hroperatingunit D ON S.ORG_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_HR_OPERATINGUNITID <> D.DIM_ORA_HROPERATINGUNITID;

/*UPDATE DIM_GL_SET_OF_BOOKS_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_GL_SET_OF_BOOKSID = DSB.DIM_ORA_GL_SETOFBOOKSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN dim_ora_hroperatingunit D ON S.ORG_ID = D.KEY_ID and d.rowiscurrent = 1  
JOIN dim_ora_gl_setofbooks DSB ON DSB.KEY_ID = D.SET_OF_BOOKS_ID and dsb.rowiscurrent = 1
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_GL_SET_OF_BOOKSID <> DSB.DIM_ORA_GL_SETOFBOOKSID;

/*DIM_ORA_AGING_DAYS_BUCKETID*/
UPDATE fact_ora_sales_order_line FF
SET FF.DIM_ORA_AGING_DAYS_BUCKETID = ifnull(DIM_ORA_BUCKETID,1),
FF.DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line FF,fact_ora_sales_order_line F
LEFT JOIN DIM_ORA_BUCKET dob 
ON F.CT_AGING_DAYS BETWEEN dob.BUCKET_MIN_VALUE AND dob.BUCKET_MAX_VALUE and f.rowiscurrent = 1
WHERE 
F.DD_HEADER_ID = FF.DD_HEADER_ID AND F.DD_LINE_ID   = FF.DD_LINE_ID AND 
F.DIM_ORA_AGING_DAYS_BUCKETID <> dob.DIM_ORA_BUCKETID AND
ff.rowiscurrent = 1 and
dob.BUCKET_GROUP_CODE in ( 'AGING_DAYS_SO','ALL');

/*UPDATE DIM_CREATED_BY_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_CREATED_BYID = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_CREATED_BYID <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_LAST_UPDATED_BY_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_LAST_UPDATED_BYID = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_LAST_UPDATED_BYID <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_DATE_CREATION_ID*/
UPDATE fact_ora_sales_order_line F
SET 
F.DIM_ORA_DATE_CREATIONID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.CREATION_DATE) = D.DATEVALUE  
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_CREATIONID <> D.DIM_DATEID;

/*UPDATE DIM_DATE_LAST_UPDATE_ID*/
UPDATE fact_ora_sales_order_line F  
SET 
F.DIM_ORA_DATE_LAST_UPDATEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_sales_order_line F,ORA_OE_ORDER_HEADER_LINE S JOIN DIM_DATE D ON to_date(S.LAST_UPDATE_DATE) = D.DATEVALUE
WHERE F.DD_HEADER_ID = S.HEADER_ID AND F.DD_LINE_ID   = S.LINE_ID AND 
F.DIM_ORA_DATE_LAST_UPDATEID <> D.DIM_DATEID;

/*DD_FINANCIAL_BACKLOG_FLAG*/


/*DD_FUNCTIONAL_CURR_CODE*/
UPDATE fact_ora_sales_order_line F
SET F.DD_FUNCTIONAL_CURR_CODE =  D.CURRENCY_CODE
FROM fact_ora_sales_order_line F,dim_ora_gl_setofbooks d 
WHERE F.DIM_ORA_GL_SET_OF_BOOKSID = D.DIM_ORA_GL_SETOFBOOKSID and d.rowiscurrent = 1 and
F.DD_FUNCTIONAL_CURR_CODE <> D.CURRENCY_CODE;

/*UPDATE AMT_EXCHANGERATE*/ 
UPDATE fact_ora_sales_order_line F
SET F.AMT_EXCHANGERATE = ifnull(GLR.CONVERSION_RATE,1)
FROM fact_ora_sales_order_line F,hlp_ORA_GL_DAILY_RATES GLR
JOIN DIM_DATE DD ON  to_date(GLR.CONVERSION_DATE) = DD.DATEVALUE
WHERE 
GLR.FROM_CURRENCY   = F.DD_TRANSACTIONAL_CURR_CODE AND
GLR.TO_CURRENCY     = F.DD_FUNCTIONAL_CURR_CODE AND
F.DIM_ORA_DATE_ORDER_BOOKEDID = dd.DIM_DATEID AND
glr.rowiscurrent = 1 and
GLR.CONVERSION_TYPE = (select property_value from systemproperty WHERE property = 'GL.conversion_type')
and F.AMT_EXCHANGERATE <> ifnull(GLR.CONVERSION_RATE,1);


/*UPDATE AMT_EXCHANGERATE_GBL*/
/* we will use directly the gauss system
UPDATE fact_ora_sales_order_line F
SET F.AMT_EXCHANGERATE_GBL = ifnull(GLR.CONVERSION_RATE,1)
FROM fact_ora_sales_order_line F,hlp_ORA_GL_DAILY_RATES GLR
JOIN DIM_DATE DD ON  to_date(GLR.CONVERSION_DATE) = DD.DATEVALUE
WHERE 
GLR.FROM_CURRENCY   = F.DD_TRANSACTIONAL_CURR_CODE AND
GLR.TO_CURRENCY     = (select property_value from systemproperty WHERE property = 'customer.global.currency') AND
F.DIM_ORA_DATE_ORDER_BOOKEDID = dd.DIM_DATEID AND
glr.rowiscurrent = 1 and
GLR.CONVERSION_TYPE = (select property_value from systemproperty WHERE property = 'GL.conversion_type')
and F.AMT_EXCHANGERATE_GBL <> ifnull(GLR.CONVERSION_RATE,1)
*/
UPDATE fact_ora_sales_order_line f_osol SET  f_osol.amt_exchangerate_gbl = 1;

UPDATE fact_ora_sales_order_line f_osol 
SET f_osol.amt_exchangerate_gbl = f_osol.amt_exchangerate_gbl * IFNULL(dr.exrate,1)
   ,f_osol.dw_update_date = CURRENT_DATE
FROM  fact_ora_sales_order_line f_osol , csv_oprate dr
WHERE  DECODE(f_osol.dd_transactional_curr_code,'PLZ','PLN','CSK','CZK',f_osol.dd_transactional_curr_code) = IFNULL(dr.fromc,'USD')
  AND f_osol.amt_exchangerate_gbl <> f_osol.amt_exchangerate_gbl * IFNULL(dr.exrate,1);

/*UPDATE DELETE_FL*/
/*UPDATE fact_ora_sales_order_line */
/*SET DELETE_FL ='Y',*/
/*DW_UPDATE_DATE = current_timestamp*/
/*WHERE NOT EXISTS */
/*(select LINE_ID FROM ORA_OE_ORDER_HEADER_LINE WHERE LINE_ID = DD_LINE_ID)*/

/*Discount Amount*/
/* to be analazed and to find the unique key for linking the hlp_ORA_OE_PRICE_ADJUSTMENTS with Sales 
UPDATE fact_ora_sales_order_line F
FROM hlp_ORA_OE_PRICE_ADJUSTMENTS opa
SET F.AMT_DISCOUNT = ifnull(opa.ADJUSTED_AMOUNT,0)
WHERE 
F.DD_HEADER_ID = opa.HEADER_ID
AND F.DD_LINE_ID = opa.LINE_ID
and opa.rowiscurrent = 1
AND opa.APPLIED_FLAG = 'Y'
*/

/*Discount Amount*/

TRUNCATE TABLE ORA_OE_PRICE_TMP;
INSERT INTO ORA_OE_PRICE_TMP 
SELECT  opa.HEADER_ID,opa.LINE_ID,MAX(price_adjustment_id) price_adjustment_id
FROM  hlp_ORA_OE_PRICE_ADJUSTMENTS opa
where opa.rowiscurrent = 1
and opa.APPLIED_FLAG = 'Y'
GROUP BY opa.HEADER_ID,opa.LINE_ID;  

TRUNCATE TABLE ORA_OE_PRICE_TMP2;
INSERT INTO ORA_OE_PRICE_TMP2 
SELECT opa.ADJUSTED_AMOUNT,t.HEADER_ID,t.LINE_ID,t.price_adjustment_id
FROM hlp_ORA_OE_PRICE_ADJUSTMENTS opa,ORA_OE_PRICE_TMP t
where opa.HEADER_ID=t.HEADER_ID
AND opa.LINE_ID=t.LINE_ID
and opa.price_adjustment_id=t.price_adjustment_id
AND opa.rowiscurrent = 1
AND opa.APPLIED_FLAG = 'Y';

UPDATE fact_ora_sales_order_line F
SET F.AMT_DISCOUNT = ifnull(opa.ADJUSTED_AMOUNT,0)
FROM fact_ora_sales_order_line F,ORA_OE_PRICE_TMP2 opa
WHERE 
F.DD_HEADER_ID = opa.HEADER_ID
AND F.DD_LINE_ID = opa.LINE_ID;

/*dim_ora_mtl_plannersid*/
UPDATE fact_ora_sales_order_line F
set f.dim_ora_mtl_plannersid = dp.dim_ora_mtl_plannersid
FROM fact_ora_sales_order_line F,dim_ora_invproduct D
JOIN dim_ora_mtl_planners dp on d.organization_id = dp.organization_id and d.planner_code = dp.planner_code
WHERE F.DIM_ORA_INV_PRODUCTID = D.DIM_ORA_INVPRODUCTID 
AND f.dim_ora_mtl_plannersid <> dp.dim_ora_mtl_plannersid;

/* UPDATE dd_crdflag */
UPDATE fact_ora_sales_order_line SO 
SET SO.dd_crdflag='N' ;

UPDATE fact_ora_sales_order_line SO 
SET dd_crdflag='Y' 
FROM fact_ora_sales_order_line SO,dim_date DT 
where 
/* SO.dim_ora_date_customer_requestedid=DT.dim_dateid */
SO.dim_ora_date_customer_requestedid_dimD = DT.dim_dateid 
AND DT.datevalue <=
(SELECT distinct (quarter_enddate)+7 FROM dim_date WHERE quarterflag='Current' )
AND SO.dd_line_flow_status_code NOT IN('CANCELLED','CLOSED','INVOICED');


/* UPDATE dd_csdflag */
UPDATE fact_ora_sales_order_line SO 
set SO.dd_csdflag='N';

UPDATE fact_ora_sales_order_line SO 
set dd_csdflag='Y' 
FROM fact_ora_sales_order_line SO,dim_date DT 
where 
SO.dim_ora_date_scheduled_shipid=DT.dim_dateid 
and DT.datevalue <=
(SELECT distinct (quarter_enddate) FROM dim_date WHERE quarterflag='Current' )
AND SO.dd_line_flow_status_code NOT IN('CANCELLED','CLOSED','INVOICED');

 /*update dim_ora_mdg_productid*/
UPDATE fact_ora_sales_order_line f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM fact_ora_sales_order_line f_inv,dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.DIM_ORA_INV_PRODUCTID = d_invp.dim_ora_invproductid
 AND  d_invp.mdm_globalid = d_mp.partnumber
 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;
 
 /*update dim_ora_bwproducthierarchyid*/
 UPDATE fact_ora_sales_order_line f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_sales_order_line f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.DIM_ORA_INV_PRODUCTID = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND dp.productgroupsbu = bw.productgroup
AND current_date BETWEEN bw.upperhierstartdate AND bw.upperhierenddate
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE fact_ora_sales_order_line f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_sales_order_line f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.DIM_ORA_INV_PRODUCTID = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

/* 11 may 2016 Cornelia add dim_ora_dropship_addressid */
UPDATE fact_ora_sales_order_line f_inv
SET f_inv.dim_ora_dropship_addressid = d_invp.dim_ora_dropship_addressid
FROM fact_ora_sales_order_line f_inv,dim_ora_dropship_address d_invp
WHERE f_inv.DD_HEADER_ID = d_invp.HEADER_ID
 AND f_inv.dim_ora_dropship_addressid <> d_invp.dim_ora_dropship_addressid;
 
 /* 13 may 2016 Cornelia add dd_attribute11 and dd_context */
UPDATE fact_ora_sales_order_line f_inv
SET f_inv.dd_attribute11 = s.attribute11
FROM fact_ora_sales_order_line f_inv,ORA_OE_ORDER_HEADER_LINE S
WHERE f_inv.DD_HEADER_ID = s.HEADER_ID
and f_inv.DD_line_id = s.line_id
AND f_inv.dd_attribute11 <> s.attribute11;

UPDATE fact_ora_sales_order_line f_inv
SET  f_inv.dd_context = s.context
FROM fact_ora_sales_order_line f_inv,ORA_OE_ORDER_HEADER_LINE S
WHERE f_inv.DD_HEADER_ID = s.HEADER_ID
and f_inv.DD_line_id = s.line_id
AND  f_inv.dd_context <> s.context;

/*Added by Victor on 12 May 2016 - BackOrders Dates */

 UPDATE fact_ora_sales_order_line f
SET f.dim_ora_otd_promise_dateid_dimD = d.dim_dateid 
FROM fact_ora_sales_order_line f, ora_source_tableids s, dim_date d
WHERE f.dd_line_id = s.integer1
  AND s.table_name='ora_xx_otd_line_detail'
  AND s.DATE1 = d.datevalue
  AND f.dim_ora_otd_promise_dateid_dimD <> d.dim_dateid; 
  
  
  /*Added by Victor on 12 May 2016 - BackOrders Dates */

	 UPDATE fact_ora_sales_order_line f_so 
  SET f_so.dim_ora_otd_promise_dateid = dfc.dim_dateid
 FROM fact_ora_sales_order_line f_so,dim_date_factory_calendar dfc, dim_ora_business_org dbo,ora_source_tableids s
 WHERE s.DATE1 = dfc.datevalue
   AND f_so.dd_line_id = s.integer1
   AND s.table_name='ora_xx_otd_line_detail'
   AND f_so.dim_ora_inventory_orgid = dbo.dim_ora_business_orgid
   AND CASE WHEN dbo.organization_code IN ('BAW','BUR','HAW','KAP','KAW','LRW','SEW','TEW') THEN 'BAW' 
            WHEN dbo.organization_code IN ('LKP','UKW') THEN 'LKP'
            ELSE dbo.organization_code 
       END = dfc.plantcode_factory
     AND f_so.dim_ora_otd_promise_dateid <> dfc.dim_dateid  ;


  /*Added by Alina on 29 June 2016 - OTD Date */
  
   /*OTD Date DimDate*/

  UPDATE fact_ora_sales_order_line f
SET f.dim_otd_dateid_dimD = CASE WHEN d.dim_dateid = 1 THEN dim_ora_date_customer_requestedid_dimD ELSE d.dim_dateid END 
FROM fact_ora_sales_order_line f, ora_source_tableids s, dim_date d
WHERE f.dd_line_id = s.integer1
  AND s.DATE2 = d.datevalue
  AND s.table_name='ora_xx_otd_line_detail'
  AND f.dim_otd_dateid_dimD <> d.dim_dateid;
  
  
  /*OTD Date DimDateFactoryCalendar*/
      UPDATE fact_ora_sales_order_line f_so
  SET f_so.dim_ora_otd_dateid = CASE WHEN dfc.dim_dateid = 1 THEN dim_ora_date_customer_requestedid ELSE dfc.dim_dateid END 
 FROM fact_ora_sales_order_line f_so,dim_date_factory_calendar dfc, dim_ora_business_org dbo,ora_source_tableids s
 WHERE s.DATE2  = dfc.datevalue
 AND f_so.dd_line_id = s.integer1
 AND  s.table_name='ora_xx_otd_line_detail'
   AND f_so.dim_ora_inventory_orgid = dbo.dim_ora_business_orgid
   AND CASE WHEN dbo.organization_code IN ('BAW','BUR','HAW','KAP','KAW','LRW','SEW','TEW') THEN 'BAW' 
            WHEN dbo.organization_code IN ('LKP','UKW') THEN 'LKP'
            ELSE dbo.organization_code 
       END = dfc.plantcode_factory
       AND f_so.dim_ora_otd_dateid <> dfc.dim_dateid;
  

 UPDATE fact_ora_sales_order_line f_so
 SET f_so.dim_ora_otd_dateid = dim_ora_date_customer_requestedid 
 FROM fact_ora_sales_order_line f_so
 WHERE f_so.dim_ora_otd_dateid  = 1
 AND f_so.dim_ora_otd_dateid <> dim_ora_date_customer_requestedid ;

 /*Plant for SAP Harmonization*/
TRUNCATE TABLE hlp_ora_plant;

INSERT INTO hlp_ora_plant
(hlp_ora_plantid
)
SELECT 1
FROM (SELECT 1) a
WHERE NOT EXISTS ( SELECT 1 FROM hlp_ora_plant WHERE hlp_ora_plantid = 1);


INSERT INTO hlp_ora_plant
SELECT
      1 + ROW_NUMBER() OVER(ORDER BY '') AS hlp_ora_plantid 
     ,plant_code
     ,plant_description
     ,country 
     ,dw_insert_date
     ,dw_update_date
     ,projectsourceid
  FROM (
SELECT      
	     DISTINCT d.segment16 plant_code
	    ,d.org_description plant_description
	    ,dbo.country 
	    ,current_timestamp dw_insert_date
	    ,current_timestamp dw_update_date
	    ,dpr.dim_projectsourceid projectsourceid
   FROM fact_ora_sales_order_line f, dim_ora_invproduct d, dim_ora_business_org dbo, emdoracle4ae.dim_projectsource dpr
  WHERE f.dim_ora_inv_productid = d.dim_ora_invproductid
    AND f.dim_ora_inventory_orgid = dbo.dim_ora_business_orgid
    );

UPDATE fact_ora_sales_order_line f SET dim_ora_plantid = 1;


UPDATE fact_ora_sales_order_line f
SET dim_ora_plantid = hlp_ora_plantid
FROM fact_ora_sales_order_line f, dim_ora_invproduct d, dim_ora_business_org dbo, hlp_ora_plant hlp 
WHERE f.dim_ora_inv_productid = d.dim_ora_invproductid
	AND f.dim_ora_inventory_orgid = dbo.dim_ora_business_orgid
	AND d.segment16 = hlp.plantcode
	AND dbo.country = hlp.country
        AND dbo.dim_projectsourceid = hlp.projectsourceid
	AND f.dim_ora_inventory_orgid <> hlp_ora_plantid;    
	
/*END Plant for SAP Harmonization*/

/* 17 may 2016 Cornelia */
UPDATE fact_ora_sales_order_line f
SET f.dim_backorder_dateid = d.dim_dateid 
FROM fact_ora_sales_order_line f, ORA_XX_BACKORDER_LINES s, dim_date d
WHERE f.dd_line_id = s.line_id
  AND s.BACKORDER_DATE = d.datevalue
  AND f.dim_ora_otd_promise_dateid <> d.dim_dateid;

UPDATE fact_ora_sales_order_line f
SET f.dd_BACKORDER_RESPONSIBILITY  = s.BACKORDER_RESPONSIBILITY  
FROM fact_ora_sales_order_line f, ORA_XX_BACKORDER_LINES s
WHERE f.dd_line_id = s.line_id
  AND f.dd_BACKORDER_RESPONSIBILITY  <> s.BACKORDER_RESPONSIBILITY;

UPDATE fact_ora_sales_order_line f
SET f.dd_BACKORDER_RESP_GEOGRAPHY  = s.BACKORDER_RESP_GEOGRAPHY 
FROM fact_ora_sales_order_line f, ORA_XX_BACKORDER_LINES s
WHERE f.dd_line_id = s.line_id
  AND f.dd_BACKORDER_RESP_GEOGRAPHY  <> s.BACKORDER_RESP_GEOGRAPHY;

UPDATE fact_ora_sales_order_line f
SET f.dd_BACKORDER_STATUS  = s.BACKORDER_STATUS 
FROM fact_ora_sales_order_line f, ORA_XX_BACKORDER_LINES s
WHERE f.dd_line_id = s.line_id
  AND f.dd_BACKORDER_STATUS  <> s.BACKORDER_STATUS;

UPDATE fact_ora_sales_order_line f
SET 
	f.dd_backorderreason = NVL(lk.meaning, l.backorder_reason)
   ,dw_update_date = CURRENT_TIMESTAMP
FROM fact_ora_sales_order_line f INNER JOIN ora_xx_backorder_lines l ON f.dd_line_id = l.line_id 
LEFT JOIN (SELECT DISTINCT t.* FROM ora_mco_lookups t WHERE t.lookup_type = 'XX_BACKORDER_REASONS') lk ON lk.lookup_code = l.backorder_reason
WHERE l.backorder_status   = 'BACKORDERED'
  AND f.dd_BackorderReason <> NVL(lk.meaning, l.backorder_reason);	
 
 
 
/*Added the tradesales flag by Victor on 19th May 2016*/
UPDATE fact_ora_sales_order_line f_osol		
	SET  f_osol.dd_tradsales_flag = IFNULL(
		CASE 
			WHEN shploc.DD_ATTRIBUTE3 NOT IN ('5','B','I','X','H')
				AND f_osol.DD_CONTEXT 
						IN ('Adjustment','Consignment', 
							'Regular Sale/Return',
							'Repair',
							'Warranty Credit',
							'Prepaid Bill Only',
							'Prepaid Credit',
							'Miscellaneous Charges',
							'MFG Warranty Replacement',
							'Warranty Replacement',
							'Short Shipped')
				AND oltyp.name NOT LIKE 'Inquiry Line%'
				AND oltyp.name NOT LIKE 'Blanket Line%'
				AND invprod.segment1_catset1 NOT IN ('180','181','182') 
			THEN 'Y'
			ELSE 'N'
		END -- trade_sales
        , 'Not Set')
FROM fact_ora_sales_order_line f_osol INNER JOIN dim_ora_customerlocation shploc ON f_osol.dim_ora_customer_ship_to_locationid = shploc.dim_ora_customerlocationid
    INNER JOIN dim_ora_xacttype oltyp ON f_osol.dim_ora_order_line_typeid = oltyp.dim_ora_xacttypeID
	INNER JOIN dim_ora_invproduct invprod ON f_osol.dim_ora_inv_productid  = invprod.dim_ora_invproductID;
	


/*Addeed the Dropshipment/Intercompany flag on 19thMay2016 by Victor */
MERGE INTO fact_ora_sales_order_line f_os
USING (SELECT 
              fact_ora_sales_order_lineid
			  ,CASE 
					WHEN dd_tradsales_flag = 'N' AND ace1.INTERCOMPANY IS NOT NULL AND ace2.INTERCOMPANY IS NOT NULL  --trade_salesZL
					     THEN '3rd Party' 
				    WHEN dd_tradsales_flag = 'Y' 
					     THEN '3rd Party' 
					     ELSE 'Intercompany' 
			  END  val_flag  
             ,CASE 
					WHEN dd_tradsales_flag = 'N' AND ace1.INTERCOMPANY IS NOT NULL AND ace2.INTERCOMPANY IS NOT NULL  --trade_salesZL
					     THEN 'Yes'
					     ELSE 'No' 
			  END  flag
			  ,dd_tradsales_flag
			  ,ace1.INTERCOMPANY 
			  ,ace2.INTERCOMPANY		
			  ,oltyp.name	  
       		FROM fact_ora_sales_order_line f_osol INNER JOIN  dim_ora_xacttype oltyp ON f_osol.dim_ora_order_header_typeid = oltyp.dim_ora_xacttypeID --  f_osol.dim_ora_order_line_typeid = oltyp.dim_ora_xacttypeID
			     LEFT JOIN ACE_3RD_PARTY_FILTERS ace1 ON ( UPPER(oltyp.name) = UPPER(ace1.INTERCOMPANY) AND ace1.filter_value = 'HEADTYP' AND ace1.DESCRIPTION = 'EMD Oracle' )
				 LEFT JOIN ACE_3RD_PARTY_FILTERS ace2 ON ( UPPER(f_osol.dd_attribute11) = UPPER(ace2.INTERCOMPANY) AND ace2.filter_value = 'ULTDES' AND ace2.DESCRIPTION = 'EMD Oracle')
      ) f_osol ON f_os.fact_ora_sales_order_lineID = f_osol.fact_ora_sales_order_lineid      
WHEN MATCHED THEN UPDATE	  			  
		SET f_os.DD_INTERCOMPFLAG = f_osol.val_flag
		   ,f_os.DD_LSINTERCOMPFLAG = f_osol.flag;	
		   
/*Added the lookup_type as BackOrderReason by Victor on 20thMay2016*/		   
UPDATE fact_ora_sales_order_line f
SET dd_backord_reason_lookupt = IFNULL(l.lookup_type,'Not Set')
  FROM fact_ora_sales_order_line f
      ,(SELECT DISTINCT t.* FROM ora_mco_lookups t) l
      ,ora_xx_backorder_lines lh
 WHERE f.dd_line_id = lh.line_id
   AND l.lookup_code = lh.backorder_reason 
   AND l.lookup_type = 'XX_BACKORDER_REASONS'
   AND dd_backord_reason_lookupt <> IFNULL(l.lookup_type,'Not Set');		   

/* 20 may 2016 Cornelia added dd_backorder based on filter */
MERGE INTO fact_ora_sales_order_line f_os
USING (
SELECT 
      fact_ora_sales_order_lineid,
		CASE 
			WHEN
				hl.[varchar1] NOT IN (DECODE('Component Lines','Kit Lines','INCLUDED','KIT'))
		--AND f.dd_backord_reason_lookupt = 'XX_BACKORDER_REASONS'
			 AND sou.legal_entity in ('AN','AS','AU','BE','BP','BR','CA','CD','CN','CZ','DA','EI','FI','FP','FR','GB','GE','HK','HU','IN','IT','KO','KR','LR','MH','MS','MX','MY','NL','NO','NP','PL','PR','RQ','SC','SG','SN','SP','SW','SZ','UK','US','WN','XX')
				AND 
				(
				dd_tradsales_flag IN ('Y','N')
				OR
				(tl.name  IN  ('Internal Drop Ship US','Internal Drop Ship UK','Internal Drop Ship LR','Internal Drop Ship FR','Internal Drop Ship EI','Internal Drop Ship BP')
				AND f.dd_attribute11  IN  ('NL','BE','IT','DE','SE','AT','ES','CH','DK','FI','NO') )
				)
				AND f.dd_backorder_status  =  'BACKORDERED'
				AND f.dd_open_flag  =  'Y' 
				AND t2.name  NOT LIKE  'Collateral%'
		   THEN 'X' ELSE 'Not Set'
		END  AS backorder 
FROM fact_ora_sales_order_line f
INNER JOIN ora_source_tableids hl ON (f.dd_header_id = hl.integer1 AND f.dd_line_id = hl.integer2 and hl.table_name='ora_oe_order_header_line')
INNER JOIN dim_ora_invproduct pc ON  f.dim_ora_inv_productid = pc.dim_ora_invproductid
INNER JOIN dim_ora_business_org sou ON hl.integer3 = sou.organization_id
INNER JOIN dim_ora_xacttype tl ON f.dim_ora_order_header_typeid = tl.dim_ora_xacttypeid
INNER JOIN dim_ora_xacttype t2 ON f.dim_ora_order_line_typeid = t2.dim_ora_xacttypeid
--INNER JOIN dim_date d ON f.dim_backorder_dateid = d.dim_dateid AND d.datevalue < CURRENT_DATE
WHERE UPPER(dd_line_category) <> 'RETURN'
  AND dd_line_id IN (SELECT integer1 FROM ora_source_tableids s where s.table_name='ora_xx_otd_line_detail') 
  AND ct_ordered_quantity - ct_total_shipped_quantity > 0
      ) f_osol ON f_os.fact_ora_sales_order_lineID = f_osol.fact_ora_sales_order_lineid
WHEN MATCHED THEN UPDATE	  			  
		SET f_os.dd_backorder = f_osol.BACKORDER;	

/* Update backorder when OpenQty = 0 */

  UPDATE fact_ora_sales_order_line f
  SET dd_backorder = 'Not Set'
  FROM fact_ora_sales_order_line f
  WHERE f.ct_ordered_quantity - f.ct_total_shipped_quantity = 0
  AND dd_backorder = 'X';
		
/*Add Global Filters Condition for Ace BackOrder filter on 24thMay2016*/		

/*
UPDATE fact_ora_sales_order_line f
 SET dd_backorder = 'X'
FROM fact_ora_sales_order_line f, emdtempocc4.csv_ace_all_platform_filters s 
WHERE UPPER(f.dd_backorder_resp_geography) = UPPER(s.filtervalue) 
  AND s.technical = 'XX_BACKORDER_LINES.BACKORDER_RESP_GEOGRAPHY'
  AND dd_backorder <> 'X'*/
  
UPDATE fact_ora_sales_order_line f
 SET dd_backorder = 'Not Set'
FROM fact_ora_sales_order_line f, emdtempocc4.csv_ace_all_platform_filters s 
WHERE UPPER(f.dd_backorderreason) = UPPER(s.filtervalue) 
  AND s.technical = 'XX_BACKORDER_LINES.BACKORDER_REASON'
  AND dd_backorder <> 'Not Set';  

  
/* 27thMay2016  Added the ACE dd_ace_openorders  */
MERGE INTO fact_ora_sales_order_line f_os
USING (
SELECT fact_ora_sales_order_lineid,
case when
hl.[varchar1] not in (decode('Component Lines','Kit Lines','INCLUDED','KIT'))
AND sou.LEGAL_ENTITY in ('AN','AS','AU','BE','BP','BR','CA','CD','CN','CZ','DA','EI','FI','FP','FR','GB','GE','HK','HU','IN','IT','KO','KR','LR','MH','MS','MX','MY','NL','NO','NP','PL','PR','RQ','SC','SG','SN','SP','SW','SZ','UK','US','WN','XX')
AND 
(
dd_tradsales_flag IN ('Y','N')
OR
(tl.NAME  IN  ('Internal Drop Ship US','Internal Drop Ship UK','Internal Drop Ship LR','Internal Drop Ship FR','Internal Drop Ship EI','Internal Drop Ship BP')
AND f.dd_ATTRIBUTE11  IN  ('NL','BE','IT','DE','SE','AT','ES','CH','DK','FI','NO') )
)
AND t2.NAME  NOT LIKE  'Collateral%'
then 'X' else 'Not Set'
end  AS dd_ace_openorders 
FROM fact_ora_sales_order_line f
INNER JOIN ora_source_tableids hl ON (f.dd_header_id = hl.integer1 AND f.dd_line_id = hl.integer2 and hl.table_name='ora_oe_order_header_line')
INNER JOIN dim_ora_invproduct pc on  f.dim_ora_inv_productid = pc.dim_ora_invproductid
INNER JOIN dim_ora_business_org sou ON hl.integer3 = sou.organization_id
INNER JOIN dim_ora_xacttype tl on f.dim_ora_order_header_typeid = tl.dim_ora_xacttypeid
INNER JOIN dim_ora_xacttype t2 on f.dim_ora_order_line_typeid = t2.dim_ora_xacttypeid
WHERE UPPER(dd_line_category) <> 'RETURN'
AND dd_line_id in (SELECT integer1 FROM ora_source_tableids s where s.table_name='ora_xx_otd_line_detail') 
      ) f_osol ON f_os.fact_ora_sales_order_lineID = f_osol.fact_ora_sales_order_lineid
WHEN MATCHED THEN UPDATE	  			  
		SET f_os.dd_ace_openorders = f_osol.dd_ace_openorders;	   
		
/* 27thMay2016  Added the ACE dd_ace_openorders  */	  

/*Added Cluster Dimension to Sales by Victor on 24thMay2016*/		
UPDATE fact_ora_sales_order_line f
SET f.dim_ora_clusterid = dc.dim_ora_clusterid
FROM fact_ora_sales_order_line f, dim_ora_mdg_product mdg, dim_ora_bwproducthierarchy ph, dim_ora_cluster dc
WHERE f.dim_ora_mdg_productid = mdg.dim_mdg_partid
	AND f.dim_ora_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	AND ph.businesssector = 'BS-02'
	AND mdg.primary_production_location = dc.primary_manufacturing_site
	AND f.dim_ora_clusterid <> dc.dim_ora_clusterid;		
	
	
/* Added the Commercial View on 25thMay2016 by Victor*/
	
UPDATE fact_ora_sales_order_line f_so
SET f_so.dim_ora_commercialviewid = ds.dim_commercialviewid
FROM fact_ora_sales_order_line f_so, dim_commercialview ds, dim_ora_customerlocation dcl, dim_ora_bwproducthierarchy b
WHERE ds.soldtocountry = dcl.country
  AND ds.upperprodhier = b.business
  AND f_so.dim_ora_bwproducthierarchyid = b.dim_bwproducthierarchyid
  AND f_so.dim_ora_customer_ship_to_locationid = dcl.dim_ora_customerlocationid
  AND f_so.dim_ora_commercialviewid <> ds.dim_commercialviewid;	
  
/*Added the LineNo as it is in ACE on 26thMay2016 by Victor*/  
UPDATE fact_ora_sales_order_line f_so
SET dd_lineno_compno = IFNULL(line_number||'.'||shipment_number||decode(component_number,NULL,NULL,'.')||component_number,'0')
FROM fact_ora_sales_order_line f_so, ora_oe_order_header_line s
WHERE f_so.dd_header_id = s.header_id
  AND f_so.dd_line_id = s.line_id
  AND dd_lineno_compno <> IFNULL(line_number||'.'||shipment_number||decode(component_number,NULL,NULL,'.')||component_number,'0');
  

/*Added for OTIF dd_infull on 30thMay2016 by Victor*/   
UPDATE 
    fact_ora_sales_order_line f_so 
 SET f_so.dd_deliveryisfull = 
 CASE 
    WHEN (CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTO'
       AND ct_total_shipped_quantity >= ct_ordered_quantity 
    THEN 'X'
    WHEN (CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTS'      
       AND ct_total_shipped_quantity >= ct_ordered_quantity 
    THEN 'X'--'OTIF'
    ELSE 'Not Set'
 END
FROM fact_ora_sales_order_line f_so
	,dim_ora_invproduct d_inv
WHERE f_so.dim_ora_inv_productid = d_inv.dim_ora_invproductid	
 AND f_so.dd_ace_openorders = 'X';  

/*Added for OTIF dd_ontime on 30thMay2016 by Victor 
UPDATE 
    fact_ora_sales_order_line f_so 
 SET f_so.dd_deliveryontime = 
 CASE 
    WHEN (CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTO'
       AND d_as.datevalue <=  d_rd.datevalue + 1 
    THEN 'X'
    WHEN (CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTS'      
       AND d_as.datevalue <=  d_pd.datevalue + 1
    THEN 'X'
    ELSE 'Not Set'
 END
FROM fact_ora_sales_order_line f_so
	,dim_ora_invproduct d_inv
	,dim_date d_rd
	,dim_date d_pd
	,dim_date d_as
WHERE f_so.dim_ora_inv_productid = d_inv.dim_ora_invproductid
  AND f_so.dim_ora_date_actual_shipid = d_as.dim_dateid 
  AND f_so.dim_ora_otd_dateid = d_rd.dim_dateid 
  AND f_so.dim_ora_otd_promise_dateid = d_pd.dim_dateid  	-- f_so.dim_ora_date_promise_deliveryid
  AND f_so.dd_ace_openorders = 'X' */ 

  
  UPDATE fact_ora_sales_order_line f_so 
 SET f_so.dd_deliveryontime  = 
 CASE 
    WHEN (CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTO'
       AND d_as.datevalue <= case when d_pd.nextworkingdate='0001-01-01' then add_days(d_pd.datevalue,1)
                                  when d_pd.nextworkingdate=d_pd.datevalue then add_days(d_pd.nextworkingdate,1) 
     	   		                  else d_pd.nextworkingdate end 
    THEN 'X'
    WHEN (CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTS'      
       AND d_as.datevalue <= case when d_rd.nextworkingdate='0001-01-01' then add_days(d_rd.datevalue,1)
                                   when d_rd.nextworkingdate=d_rd.datevalue then add_days(d_rd.nextworkingdate,1) 
                                   else d_rd.nextworkingdate end
    THEN 'X'
    ELSE 'Not Set'
 END
FROM fact_ora_sales_order_line f_so
	,dim_ora_invproduct d_inv
	,dim_date_factory_calendar d_rd
	,dim_date_factory_calendar d_pd
	,dim_date d_as
WHERE f_so.dim_ora_inv_productid = d_inv.dim_ora_invproductid
  AND f_so.dim_ora_date_actual_shipid = d_as.dim_dateid 
  AND f_so.DIM_ORA_DATE_CUSTOMER_REQUESTEDID = d_rd.dim_dateid 
  AND f_so.dim_ora_otd_promise_dateid = d_pd.dim_dateid  	-- f_so.dim_ora_date_promise_deliveryid
  AND f_so.dd_ace_openorders = 'X';
  
  
 /*Added the qty for OTIF UI calculations by Victor on 31thMay2016*/  
UPDATE fact_ora_sales_order_line f
SET f.ct_deliveryisfull = (CASE WHEN f.dd_deliveryisfull = 'X'  THEN 1 ELSE 0 END )
   ,f.ct_deliveryontime = (CASE WHEN f.dd_deliveryontime = 'X'  THEN 1 ELSE 0 END )
from fact_ora_sales_order_line f;  

/*Added the Hold Line Info on 3thJune by Alina*/
UPDATE fact_ora_sales_order_line f_os SET f_os.dd_line_on_hold = 'N' WHERE f_os.dd_line_on_hold = 'Not Set';

MERGE INTO fact_ora_sales_order_line f_os
USING (
SELECT
      fact_ora_sales_order_lineid
     ,f.dd_line_id
     ,ohi.line_id
FROM fact_ora_sales_order_line f
INNER JOIN (SELECT header_id,line_id,ROW_NUMBER() OVER(PARTITION BY header_id,line_id ORDER BY hold_name) rn ,hold_name, hold_until_date, hold_comment,activity_name FROM ora_hold_info )ohi 
ON f.dd_line_id = ohi.line_id
WHERE ohi.rn = 1
      ) f_osol ON f_os.fact_ora_sales_order_lineID = f_osol.fact_ora_sales_order_lineid
WHEN MATCHED THEN UPDATE	  			  
		SET f_os.dd_line_on_hold = 'Y';  


 /*Added the Active Hold Info on 3thJune by Alina*/
UPDATE fact_ora_sales_order_line so
SET dd_active_hold_info = (CASE 
                            WHEN hold_name ||DECODE(activity_name,'BOOK_ORDER','Booking', 'PICK_LINE','Shipping','INVOICE_INTERFACE','Invoicing','INVENTORY_INTERFACE','Inventory Interface','LINE_SCHEDULING','Scheduling','CLOSE_LINE','Close Line',activity_name) ||decode(hold_until_date,null,null,(','||to_char(hold_until_date,'DD-MON-YYYY'))) 
                                 IS NULL 
                            THEN 'No Holds' 
                            ELSE 'Holds' 
                       END)
FROM fact_ora_sales_order_line so, (SELECT header_id,line_id,ROW_NUMBER() OVER(PARTITION BY header_id,line_id ORDER BY hold_name asc) rn,hold_name, hold_until_date, hold_comment,activity_name FROM ora_hold_info ) th                       
WHERE so.dd_header_id = th.header_id
  AND so.dd_line_id = th.line_id
  AND th.rn = 1;


UPDATE fact_ora_sales_order_line so
SET dd_active_hold_info = (CASE 
                            WHEN hold_name ||DECODE(activity_name,'BOOK_ORDER','Booking', 'PICK_LINE','Shipping','INVOICE_INTERFACE','Invoicing','INVENTORY_INTERFACE','Inventory Interface','LINE_SCHEDULING','Scheduling','CLOSE_LINE','Close Line',activity_name) ||decode(hold_until_date,null,null,(','||to_char(hold_until_date,'DD-MON-YYYY'))) 
                                 IS NULL 
                            THEN 'No Holds' 
                            ELSE 'Holds' 
                       END)
FROM fact_ora_sales_order_line so, (SELECT header_id,line_id,ROW_NUMBER() OVER(PARTITION BY hold_name ORDER BY hold_name) rn ,hold_name, hold_until_date, hold_comment,activity_name FROM ora_hold_info ) th                       
WHERE so.dd_header_id = th.header_id
  AND th.rn = 1
  AND dd_active_hold_info = 'Not Set';
  
/*Added deliveryontimecasual on 9th June 2016 by Alina

MERGE INTO fact_ora_sales_order_line f_os
USING (SELECT
fact_ora_sales_order_lineid,
 CASE
	WHEN 
	    (CASE WHEN d.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTS' 
	    AND (CASE 
		         WHEN dt1.weekdayname = 'Friday' THEN add_days(dt1.datevalue,3) 
		         WHEN dt1.weekdayname NOT IN('Friday','Saturday','Sunday') THEN add_days(dt1.datevalue,1) 
				 ELSE dt1.datevalue
		     END ) >= dt.datevalue  
	    AND dt.datevalue  <> '0001-01-01' 
   THEN 'X' 	   													
   WHEN 
	    (CASE WHEN d.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTO' 
	    AND (CASE 
		         WHEN dt2.weekdayname = 'Friday' THEN add_days(dt2.datevalue,3) 
		         WHEN dt2.weekdayname NOT IN('Friday','Saturday','Sunday') THEN add_days(dt2.datevalue,1) 
				 ELSE dt2.datevalue
		     END ) >= dt.datevalue  
	   AND dt.datevalue  <> '0001-01-01' 
	THEN 'X' 
	ELSE 'Not Set' 
 END deliveryontimecasual
FROM fact_ora_sales_order_line f 
INNER JOIN dim_ora_invproduct d ON f.dim_ora_inv_productid = d.dim_ora_invproductid
INNER JOIN dim_date dt1 ON f.dim_ora_otd_dateid = dt1.dim_dateid
INNER JOIN dim_date dt2 ON f.dim_ora_otd_promise_dateid  = dt2.dim_dateid
INNER JOIN dim_date dt ON f.dim_ora_date_actual_shipid  = dt.dim_dateid
) f_osol ON f_os.fact_ora_sales_order_lineID = f_osol.fact_ora_sales_order_lineid    
 WHEN MATCHED THEN UPDATE	  			  
		SET f_os.dd_deliveryontimecasual = f_osol.deliveryontimecasual */  
		
		
/*Added deliveryontimecasual on 9th June 2016 by Alina */		
MERGE INTO fact_ora_sales_order_line f_os
USING (SELECT
fact_ora_sales_order_lineid,
 CASE
	WHEN 
	  ( CASE WHEN d.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END) = 'MTS' 
	    AND ( CASE WHEN dt1.nextworkingdate = '0001-01-01' THEN add_days(dt1.datevalue,1)
		WHEN dt1.nextworkingdate=dt1.datevalue then add_days(dt1.nextworkingdate,1) 
	    ELSE dt1.nextworkingdate END ) >= dt.datevalue  
	    AND dt.datevalue  <> '0001-01-01' 
   THEN 'X' 	   													
   WHEN 
	  ( CASE WHEN d.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END ) = 'MTO' 
	    AND ( CASE WHEN dt2.nextworkingdate = '0001-01-01' THEN dt2.datevalue 
		  WHEN dt2.nextworkingdate=dt2.datevalue then add_days(dt2.nextworkingdate,1) 
		ELSE dt2.nextworkingdate END ) >= dt.datevalue  
	    AND dt.datevalue  <> '0001-01-01' 
   THEN 'X' 
   ELSE 'Not Set' 
 END deliveryontimecasual
FROM fact_ora_sales_order_line f 
INNER JOIN dim_ora_invproduct d ON f.dim_ora_inv_productid = d.dim_ora_invproductid
INNER JOIN dim_date_factory_calendar dt1 ON f.DIM_ORA_DATE_CUSTOMER_REQUESTEDID = dt1.dim_dateid
INNER JOIN dim_date_factory_calendar dt2 ON f.dim_ora_otd_promise_dateid  = dt2.dim_dateid
INNER JOIN dim_date dt ON f.dim_ora_date_actual_shipid  = dt.dim_dateid
) f_osol ON f_os.fact_ora_sales_order_lineID = f_osol.fact_ora_sales_order_lineid    
 WHEN MATCHED THEN UPDATE	  			  
		SET f_os.dd_deliveryontimecasual = f_osol.deliveryontimecasual; 
			
  
/*Ended deliveryontimecasual on 9th June 2016 by Alina*/  
  
/*Added ct_deliveryontimecasual on 9th June 2016 by Alina*/ 
UPDATE fact_ora_sales_order_line f
SET f.ct_deliveryontimecasual = (CASE WHEN f.dd_deliveryontimecasual = 'X'  THEN 1 ELSE 0 END );
 /*Ended ct_deliveryontimecasual on 9th June 2016 by Alina*/  
 
 
/*Added the covered/not covered flag on 5th July 2016 by Victor*/ 
UPDATE fact_ora_sales_order_line f
SET dd_coveredbyplant = CASE WHEN f.dd_BACKORDER_RESPONSIBILITY = 'PLANT' THEN 'Not Set' ELSE 'X' END   
   ,dd_coveredbymaterial = CASE WHEN f.dd_BACKORDER_RESPONSIBILITY = 'PLANT' THEN 'Not Set' ELSE 'X' END;
/*End the covered/not covered flag on 5th July 2016 by Victor*/   


/*Added the covered/not covered flag for open orders on 20th July 2016 by Alina*/ 

UPDATE fact_ora_sales_order_line f
SET dd_coveredbyplant = CASE WHEN f.ct_ordered_quantity = f.ct_total_shipped_quantity THEN 'X' ELSE 'Not Set' END   
   ,dd_coveredbymaterial = CASE WHEN f.ct_ordered_quantity = f.ct_total_shipped_quantity THEN 'X' ELSE 'Not Set' END 
   FROM fact_ora_sales_order_line f
   WHERE dd_ace_openorders = 'X'
   AND (dd_coveredbyplant = 'Not Set' or dd_coveredbymaterial = 'Not Set') ;
   
/*End the covered/not covered flag for open orders on 20th July 2016 by Alina*/ 

/*Added Country hierarchy on 12th August 2016 by Alina*/ 
UPDATE fact_ora_sales_order_line f
SET f.dim_countryhierpsid = dch.dim_countryhierpsid
FROM fact_ora_sales_order_line f, dim_ora_business_org dc, dim_ora_countryhierps dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_BUSINESS_ORGID = dc.dim_ora_business_orgid
AND f.dim_countryhierpsid <> dch.dim_countryhierpsid;

UPDATE fact_ora_sales_order_line f
SET f.dim_countryhierarid  = dch.dim_countryhierarid
FROM fact_ora_sales_order_line f, dim_ora_business_org dc, dim_ora_countryhierar dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_BUSINESS_ORGID = dc.dim_ora_business_orgid
AND f.dim_countryhierarid <> dch.dim_countryhierarid;

    