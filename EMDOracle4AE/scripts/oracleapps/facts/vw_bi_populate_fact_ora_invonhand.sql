/*set session authorization oracle_data*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_invonhand';
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_invonhand',IFNULL(MAX(fact_ora_invonhandid),0)
FROM fact_ora_invonhand;

/*DELETE FROM fact_ora_invonhand*/
/*select count(*) from fact_ora_invonhand*/
/*select count(*) from ORA_MTL_ONHAND_RESERVE*/

/*delete rows*/
/*DELETE FROM fact_ora_invonhand*/
TRUNCATE TABLE fact_ora_invonhand;

TRUNCATE TABLE tmp_ORA_MTL_ONHAND_RESERVE;
INSERT INTO tmp_ORA_MTL_ONHAND_RESERVE
SELECT
	 INVENTORY_ITEM_ID
	,ORGANIZATION_ID
	,STATUS_ID
	,REVISION
	,LOCATOR_ID
	,SUBINVENTORY_CODE
	,TRANSACTION_UOM_CODE
	,SUM(ON_HAND) ON_HAND
	,SUM(PRIMARY_TRANSACTION_QUANTITY) PRIMARY_TRANSACTION_QUANTITY
	,DATE_RECEIVED
	,CREATE_TRANSACTION_ID
	,UPDATE_TRANSACTION_ID
	,MAX(ORIG_DATE_RECEIVED) ORIG_DATE_RECEIVED
	,CONTAINERIZED_FLAG
	,MAX(ONHAND_QUANTITIES_ID) ONHAND_QUANTITIES_ID
	,ORGANIZATION_TYPE
	,OWNING_ORGANIZATION_ID OWNING_ORGANIZATION_ID
	,OWNING_TP_TYPE OWNING_TP_TYPE
	,PLANNING_ORGANIZATION_ID PLANNING_ORGANIZATION_ID
	,PLANNING_TP_TYPE PLANNING_TP_TYPE
	,SECONDARY_UOM_CODE
	,SECONDARY_TRANSACTION_QUANTITY
	,LOT_NUMBER
	,RESERVATION_ID
	,REQUIREMENT_DATE
	,RESERVATION_QUANTITY
	,SHIP_READY_FLAG
	,DETAILED_QUANTITY
	,DEMAND_SHIP_DATE
	,IS_CONSIGNED
FROM ORA_MTL_ONHAND_RESERVE
GROUP BY
	 INVENTORY_ITEM_ID
	,ORGANIZATION_ID
	,STATUS_ID
	,REVISION
	,LOCATOR_ID
	,SUBINVENTORY_CODE
	,TRANSACTION_UOM_CODE
	,DATE_RECEIVED
	,CREATE_TRANSACTION_ID
	,UPDATE_TRANSACTION_ID
	,CONTAINERIZED_FLAG
	,ORGANIZATION_TYPE
	,SECONDARY_UOM_CODE
	,SECONDARY_TRANSACTION_QUANTITY
	,LOT_NUMBER
	,RESERVATION_ID
	,REQUIREMENT_DATE
	,RESERVATION_QUANTITY
	,SHIP_READY_FLAG
	,DETAILED_QUANTITY
	,DEMAND_SHIP_DATE
	,PLANNING_TP_TYPE
	,PLANNING_ORGANIZATION_ID
	,OWNING_TP_TYPE
	,OWNING_ORGANIZATION_ID
	,IS_CONSIGNED;
TRUNCATE TABLE ORA_MTL_ONHAND_RESERVE;
INSERT INTO ORA_MTL_ONHAND_RESERVE SELECT * FROM tmp_ORA_MTL_ONHAND_RESERVE;

/*insert new rows*/
INSERT INTO fact_ora_invonhand
(
fact_ora_invonhandid,DIM_ORA_INV_PRODUCTID,DIM_ORA_PRODUCTID,DIM_ORA_INV_ORGID,DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,DIM_ORA_MTL_SEC_INVENTORYID,CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,CT_PRIMARY_TRANSACTION_QUANTITY,DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,fact_ORA_INVMTL_UPDATE_TRXID,DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,DIM_ORA_ORGANIZATION_TYPEID,DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,DIM_ORA_PLANNING_ORGANIZATIONID,DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,CT_SECONDARY_TRANSACTION_QUANTITY,DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,CT_RESERVATION_QUANTITY,DD_SHIP_READY_FLAG,CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,CT_DEMAND_QUANTITY,dd_aging_flag,CREATE_TRANSACTION_ID,UPDATE_TRANSACTION_ID,RESERVATION_ID,INVENTORY_ITEM_ID,
ORGANIZATION_ID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID, DD_OWNING_ORGANIZATION_ID,DD_PLANNING_ORGANIZATION_ID,DD_DATE_RECEIVED,dd_isconsigned
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_invonhand' ) + ROW_NUMBER() OVER(order by '') fact_ora_invonhandid,
1 AS DIM_ORA_INV_PRODUCTID,
1 AS DIM_ORA_PRODUCTID,
1 AS DIM_ORA_INV_ORGID,
1 AS DIM_ORA_MTL_ITEM_REVID,
1 AS DIM_ORA_MTL_ITEM_LOCATORID,
1 AS DIM_ORA_MTL_SEC_INVENTORYID,
S.ON_HAND AS CT_TRANSACTION_QUANTITY,
S.TRANSACTION_UOM_CODE AS DD_TRANSACTION_UOM_CODE,
S.PRIMARY_TRANSACTION_QUANTITY AS CT_PRIMARY_TRANSACTION_QUANTITY,
1 AS DIM_ORA_DATE_RECEIVEDID,
0 AS fact_ORA_INVMTL_CREATE_TRXID,
0 AS fact_ORA_INVMTL_UPDATE_TRXID,
1 AS DIM_ORA_DATE_ORIG_RECEIVEDID,
ifnull(S.CONTAINERIZED_FLAG,0) AS DD_CONTAINERIZED_FLAG,
1 as DIM_ORA_ORGANIZATION_TYPEID,
1 AS DIM_ORA_OWNING_ORGANIZATIONID,
1 AS DIM_ORA_OWNINING_TP_TYPEID,
1 AS DIM_ORA_PLANNING_ORGANIZATIONID,
1 AS DIM_ORA_PLANNING_TP_TYPEID,
ifnull(S.SECONDARY_UOM_CODE,'Not Set') AS DD_SECONDARY_UOM_CODE,
ifnull(S.SECONDARY_TRANSACTION_QUANTITY,0) AS CT_SECONDARY_TRANSACTION_QUANTITY,
ifnull(S.LOT_NUMBER,'Not Set') AS DD_LOT_NUMBER,
1 AS DIM_ORA_DATE_REQUIREMENTID,
ifnull(S.RESERVATION_QUANTITY,0) AS CT_RESERVATION_QUANTITY,
ifnull(S.SHIP_READY_FLAG,0) AS DD_SHIP_READY_FLAG,
ifnull(S.DETAILED_QUANTITY,0) AS CT_DETAILED_QUANTITY,
1 AS DIM_ORA_DATE_DEMAND_SHIPID,
0 AS CT_DEMAND_QUANTITY,
'Not Set' dd_aging_flag,
ifnull(S.CREATE_TRANSACTION_ID,0) AS CREATE_TRANSACTION_ID,
ifnull(S.UPDATE_TRANSACTION_ID,0) AS UPDATE_TRANSACTION_ID,
ifnull(S.RESERVATION_ID,0) AS RESERVATION_ID,
S.INVENTORY_ITEM_ID AS INVENTORY_ITEM_ID,
S.ORGANIZATION_ID AS ORGANIZATION_ID,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID,
ifnull(S.OWNING_ORGANIZATION_ID,0) DD_OWNING_ORGANIZATION_ID,
ifnull(S.PLANNING_ORGANIZATION_ID,0) DD_PLANNING_ORGANIZATION_ID,
IFNULL(S.DATE_RECEIVED,'9999-12-31 00:00:00.000000') DD_DATE_RECEIVED,
ifnull(s.is_consigned,0) dd_isconsigned
FROM ORA_MTL_ONHAND_RESERVE S LEFT JOIN fact_ora_invonhand F ON
F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND
F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND
F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND
ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND
ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set') AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
WHERE F.INVENTORY_ITEM_ID IS NULL
AND F.ORGANIZATION_ID IS NULL
AND F.dd_owning_organization_id is null
AND F.DD_PLANNING_ORGANIZATION_ID is null
AND F.DD_DATE_RECEIVED is null;

/*UPDATE DIM_ORA_INV_PRODUCTID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_INV_PRODUCTID = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F,ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_INVPRODUCT D
ON CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'||  CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_INV_PRODUCTID <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE DIM_ORA_PRODUCTID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_PRODUCTID = D.DIM_ORA_PRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_PRODUCT D ON S.INVENTORY_ITEM_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_PRODUCTID <> D.DIM_ORA_PRODUCTID;

/*UPDATE DIM_ORA_INV_ORGID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_INV_ORGID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_BUSINESS_ORG D
ON  'INV' ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_INV_ORGID <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE DIM_ORA_MTL_ITEM_REVID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_MTL_ITEM_REVID = D.DIM_ORA_MTL_ITEM_REVID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_MTL_ITEM_REV D
ON CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) ||'~'||  CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'||  CONVERT(VARCHAR(200),S.REVISION)  = D.KEY_ID and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_MTL_ITEM_REVID <> D.DIM_ORA_MTL_ITEM_REVID;

/*UPDATE DIM_ORA_MTL_ITEM_LOCATORID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_MTL_ITEM_LOCATORID = D.DIM_ORA_MTL_ITEM_LOCATIONSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_MTL_ITEM_LOCATIONS D
ON S.LOCATOR_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_MTL_ITEM_LOCATORID <> D.DIM_ORA_MTL_ITEM_LOCATIONSID;

/*UPDATE DIM_ORA_MTL_SEC_INVENTORYID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_MTL_SEC_INVENTORYID = D.DIM_ORA_MTL_SEC_INVENTORYID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_MTL_SEC_INVENTORY D
ON D.KEY_ID = CONVERT(VARCHAR(200),S.SUBINVENTORY_CODE) ||'~'||  CONVERT(VARCHAR(200),S.ORGANIZATION_ID) and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_MTL_SEC_INVENTORYID <> D.DIM_ORA_MTL_SEC_INVENTORYID;

/*UPDATE DIM_ORA_DATE_RECEIVEDID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_DATE_RECEIVEDID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_DATE D ON to_date(S.DATE_RECEIVED) = D.DATEVALUE
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_DATE_RECEIVEDID <> D.DIM_DATEID;

/*UPDATE fact_ORA_INVMTL_CREATE_TRXID*/
UPDATE fact_ora_invonhand F
SET
F.fact_ORA_INVMTL_CREATE_TRXID = D.FACT_ORA_INVMTLTRXID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN FACT_ORA_INVMTLTRX D ON S.CREATE_TRANSACTION_ID = D.TRANSACTION_ID and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.fact_ORA_INVMTL_CREATE_TRXID <> D.FACT_ORA_INVMTLTRXID;

/*UPDATE fact_ORA_INVMTL_UPDATE_TRXID*/
UPDATE fact_ora_invonhand F
SET
F.fact_ORA_INVMTL_UPDATE_TRXID = D.FACT_ORA_INVMTLTRXID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN FACT_ORA_INVMTLTRX D ON S.UPDATE_TRANSACTION_ID = D.TRANSACTION_ID and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.fact_ORA_INVMTL_UPDATE_TRXID <> D.FACT_ORA_INVMTLTRXID;

/*UPDATE DIM_ORA_DATE_ORIG_RECEIVEDID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_DATE_ORIG_RECEIVEDID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_DATE D ON to_date(S.ORIG_DATE_RECEIVED) = D.DATEVALUE
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_DATE_ORIG_RECEIVEDID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_OWNING_ORGANIZATIONID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_OWNING_ORGANIZATIONID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_BUSINESS_ORG D
ON  'HR_BG' ||'~'|| CONVERT(VARCHAR(200),S.OWNING_ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_OWNING_ORGANIZATIONID <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE DIM_ORA_PLANNING_ORGANIZATIONID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_PLANNING_ORGANIZATIONID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_BUSINESS_ORG D
ON  'HR_BG' ||'~'|| CONVERT(VARCHAR(200),S.PLANNING_ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_PLANNING_ORGANIZATIONID <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE DIM_ORA_DATE_REQUIREMENTID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_DATE_REQUIREMENTID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_DATE D ON to_date(S.REQUIREMENT_DATE) = D.DATEVALUE
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_DATE_REQUIREMENTID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE_DEMAND_SHIPID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_DATE_DEMAND_SHIPID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_DATE D ON to_date(S.DEMAND_SHIP_DATE) = D.DATEVALUE
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
AND F.DIM_ORA_DATE_DEMAND_SHIPID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_ORGANIZATION_TYPEID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_ORGANIZATION_TYPEID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_FND_LOOKUP D ON S.ORGANIZATION_TYPE = D.LOOKUP_CODE AND D.lookup_type ='MTL_TP_TYPES' and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
and F.DIM_ORA_ORGANIZATION_TYPEID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_OWNINING_TP_TYPEID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_OWNINING_TP_TYPEID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_FND_LOOKUP D ON S.OWNING_TP_TYPE = D.LOOKUP_CODE AND D.lookup_type ='MTL_TP_TYPES' and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
and F.DIM_ORA_OWNINING_TP_TYPEID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_PLANNING_TP_TYPEID*/
UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_PLANNING_TP_TYPEID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_FND_LOOKUP D ON S.PLANNING_TP_TYPE = D.LOOKUP_CODE AND D.lookup_type ='MTL_TP_TYPES' and d.rowiscurrent = 1
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND
F.dd_owning_organization_id = ifnull(S.owning_organization_id,0) AND
F.dd_planning_organization_id = ifnull(S.planning_organization_id,0) AND
F.dd_date_received = IFNULL(S.date_received,'9999-12-31 00:00:00.000000')
and F.DIM_ORA_PLANNING_TP_TYPEID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE CT_DEMAND_QUANTITY*/
DROP TABLE IF EXISTS fact_ora_sales_order_line_quantity_open_orders;

CREATE TABLE fact_ora_sales_order_line_quantity_open_orders as
select
sol.fact_ora_sales_order_lineid,
sol.dim_ora_inv_productid,
sol.ct_ordered_quantity quantity
from fact_ora_sales_order_line sol
join dim_ora_xacttype cttype
on cttype.dim_ora_xacttypeid = sol.dim_ora_order_line_typeid and sol.rowiscurrent = 1 and cttype.rowiscurrent = 1
join dim_ora_fnd_lookup lfsc
on lfsc.dim_ora_fnd_lookupid = sol.dim_ora_order_line_statusid and lfsc.rowiscurrent = 1
where sol.ct_ordered_quantity<>0
and not(cttype.NAME like '%Return%')
and not (lfsc.lookup_code like '%CANCELLED%' or lfsc.lookup_code like '%CLOSED%' or lfsc.lookup_code like '%REJECTED%')
order by sol.fact_ora_sales_order_lineid;

DROP TABLE IF EXISTS fact_ora_sales_order_line_quantity_last3months;

CREATE TABLE fact_ora_sales_order_line_quantity_last3months as
select
sol.fact_ora_sales_order_lineid,
sol.dim_ora_inv_productid,
sol.ct_ordered_quantity quantity
from fact_ora_sales_order_line sol
join dim_ora_xacttype cttype
on cttype.dim_ora_xacttypeid = sol.dim_ora_order_line_typeid and sol.rowiscurrent = 1 and cttype.rowiscurrent = 1
join dim_ora_fnd_lookup lfsc
on lfsc.dim_ora_fnd_lookupid = sol.dim_ora_order_line_statusid and lfsc.rowiscurrent = 1
join dim_date d
on d.dim_dateid = sol.dim_ora_date_orderedid
where sol.ct_ordered_quantity<>0
and not(cttype.NAME like '%Return%')
and lfsc.lookup_code like '%CLOSED%'
and d.datevalue >= to_date(current_timestamp)-90
order by sol.fact_ora_sales_order_lineid;

DROP TABLE IF EXISTS fact_ora_sales_order_line_quantity;

CREATE TABLE fact_ora_sales_order_line_quantity
as
select dim_ora_inv_productid, sum(quantity) quantity
from
(
select fact_ora_sales_order_lineid,dim_ora_inv_productid, quantity from fact_ora_sales_order_line_quantity_open_orders
UNION
select fact_ora_sales_order_lineid,dim_ora_inv_productid, quantity from fact_ora_sales_order_line_quantity_last3months
) t
group by dim_ora_inv_productid
order by dim_ora_inv_productid;

DROP TABLE IF EXISTS fact_ora_invonhand_byinvprod_tmp;

CREATE TABLE fact_ora_invonhand_byinvprod_tmp as
select dim_ora_inv_productid,dim_ora_inv_orgid,sum(ct_transaction_quantity) ct_transaction_quantity
from fact_ora_invonhand
where rowiscurrent = 1
group by dim_ora_inv_productid,dim_ora_inv_orgid;

UPDATE fact_ora_invonhand F
SET F.CT_DEMAND_QUANTITY =
case when tmpprod.ct_transaction_quantity = 0 then 0
else
round(tmp.QUANTITY *( f.ct_transaction_quantity/ tmpprod.ct_transaction_quantity),2)
end
FROM fact_ora_invonhand F, fact_ora_invonhand_byinvprod_tmp tmpprod
JOIN fact_ora_sales_order_line_quantity tmp
on tmpprod.dim_ora_inv_productid =  tmp.dim_ora_inv_productid
WHERE
f.dim_ora_inv_productid = tmpprod.dim_ora_inv_productid
and f.dim_ora_inv_orgid = tmpprod.dim_ora_inv_orgid;

DROP TABLE fact_ora_invonhand_byinvprod_tmp;
DROP TABLE fact_ora_sales_order_line_quantity_open_orders;
DROP TABLE fact_ora_sales_order_line_quantity_last3months;
DROP TABLE fact_ora_sales_order_line_quantity;

/*dd_aging_flag*/
DROP TABLE IF EXISTS fact_ora_invonhand_items_tmp;

create table fact_ora_invonhand_items_tmp as
select distinct dim_ora_inv_productid
from fact_ora_invonhand
where rowiscurrent = 1
order by dim_ora_inv_productid;

DROP TABLE IF EXISTS fact_ora_invmtltrx_tmp;

CREATE table fact_ora_invmtltrx_tmp as
select trx.dim_ora_inv_productid, max(d.datevalue) transaction_date,
case when (max(d.datevalue)- to_date(current_timestamp))<100 then 1 else 0 end before_100_days
from fact_ora_invmtltrx trx
join fact_ora_invonhand_items_tmp tmp
on trx.dim_ora_inv_productid =  tmp.dim_ora_inv_productid and trx.rowiscurrent = 1
join dim_date d
on d.dim_Dateid = trx.DIM_ORA_DATE_TRANSACTIONID
group by trx.dim_ora_inv_productid;

UPDATE fact_ora_invonhand inv
SET dd_aging_flag = case when tmp.before_100_days = 1 then 'N' else 'Y' end
FROM fact_ora_invonhand inv, fact_ora_invmtltrx_tmp tmp
WHERE inv.dim_ora_inv_productid = tmp.dim_ora_inv_productid;

DROP TABLE IF EXISTS fact_ora_invonhand_items_tmp;
DROP TABLE IF EXISTS fact_ora_invmtltrx_tmp;

/*update dim_ora_mdg_productid*/
UPDATE fact_ora_invonhand f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM fact_ora_invonhand f_inv, dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.dim_ora_inv_productid = d_invp.dim_ora_invproductid
 AND DECODE(d_invp.mdm_globalid,'Not Set',REPLACE(d_invp.segment1,'.',''),d_invp.mdm_globalid) = d_mp.partnumber
 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;

 /*update dim_ora_bwproducthierarchyid*/
 UPDATE fact_ora_invonhand f
 SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND CASE WHEN substring(dp.GlbProductGroup,1,3)='SBU' THEN dp.GlbProductGroup
ELSE concat('SBU-',dp.GlbProductGroup) END = bw.productgroup
AND to_date('2017-12-28') BETWEEN bw.upperhierstartdate AND bw.upperhierenddate -- TO BE REPLACED AFTER 01.01.2017: '2016-12-20' WITH CURRENT_DATE
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;


/*Standard Cost*/
TRUNCATE TABLE hlp_ora_cst_standards_costs_tmp;
INSERT INTO hlp_ora_cst_standards_costs_tmp
SELECT
      inventory_item_id
     ,organization_id
     ,MAX(standard_cost_revision_date) standard_cost_revision_date
  FROM hlp_ora_cst_standards_costs
 GROUP BY inventory_item_id
         ,organization_id;



UPDATE fact_ora_invonhand  f_inv
SET f_inv.dd_standart_cost = IFNULL(hlp.standard_cost,0)
FROM fact_ora_invonhand  f_inv, hlp_ora_cst_standards_costs hlp, hlp_ora_cst_standards_costs_tmp hlp_t
WHERE f_inv.inventory_item_id = hlp.inventory_item_id
  AND f_inv.organization_id = hlp.organization_id
  AND hlp_t.inventory_item_id = hlp.inventory_item_id
  AND hlp_t.organization_id = hlp.organization_id
  AND hlp.standard_cost_revision_date = hlp_t.standard_cost_revision_date
  AND  f_inv.dd_standart_cost <> IFNULL(hlp.standard_cost,0);

/*Standard Cost*/

/*From Currency not mandatory*/

UPDATE fact_ora_invonhand f_inv
SET dd_from_currency_code = IFNULL(dd_currency_code,'USD')
FROM fact_ora_invonhand f_inv, fact_ora_invmtltrx f_invx
WHERE f_inv.fact_ora_invmtl_create_trxid = f_invx.fact_ora_invmtltrxid
 AND dd_from_currency_code <> IFNULL(dd_currency_code,'USD');

/*From Currency not mandatory*/


/*Exchange Rate Calculations */

UPDATE fact_ora_invonhand f_inv
SET amt_exchangerate_gbl = IFNULL(dr.exrate,1)
FROM fact_ora_invonhand f_inv, csv_oprate dr,dim_ora_business_org dc,ora_xx_legal_entities l
WHERE f_inv.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND dc.organization_id=l.inv_org_id
AND dr.toc = 'EUR'
AND dr.fromc=l.currency_code
AND amt_exchangerate_gbl <> IFNULL(dr.exrate,1);

/*Exchange Rate Calculations */


/* COGS FROM SAP */
TRUNCATE TABLE csv_cogs;
INSERT INTO csv_cogs
SELECT * FROM EMDTempoCC4.csv_cogs;

UPDATE fact_ora_invonhand f
SET f.amt_cogsactualrate_emd = IFNULL(c.Z_GCACTF,0)
	,f.amt_cogsfixedrate_emd = IFNULL(c.Z_GCFIXF,0)
	,f.amt_cogsfixedplanrate_emd = IFNULL(c.Z_GCPLFF,0)
	,f.amt_cogsplanrate_emd = IFNULL(c.Z_GCPLRF,0)
	,f.amt_cogsprevyearfixedrate_emd = IFNULL(c.Z_GCPYFF,0)
	,f.amt_cogsprevyearrate_emd = IFNULL(c.Z_GCPYRF,0)
	,f.amt_cogsprevyearto1_emd = IFNULL(c.Z_GCPYTF1,0)
	,f.amt_cogsprevyearto2_emd = IFNULL(c.Z_GCPYTF2,0)
	,f.amt_cogsprevyearto3_emd = IFNULL(c.Z_GCPYTF3,0)
	,f.amt_cogsturnoverrate1_emd = IFNULL(c.Z_GCTOF1,0)
	,f.amt_cogsturnoverrate2_emd = IFNULL(c.Z_GCTOF2,0)
	,f.amt_cogsturnoverrate3_emd = IFNULL(c.Z_GCTOF3,0)
FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_business_org dc, csv_cogs c
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
	AND f.dim_ora_inv_orgid = dc.dim_ora_business_orgid
	AND TRIM(LEADING 0 FROM c.PRODUCT)= dp.partnumber
	AND c.Z_REPUNIT = dc.cmg_number;

/* COGS FROM SAP */

/*Plant for SAP Harmonization*/

/* 17 may 2016 Cornelia */
/*for inventory on hand */

INSERT INTO hlp_ora_plant
SELECT
      1 + ROW_NUMBER() OVER(ORDER BY '') AS hlp_ora_plantid
     ,plant_code
     ,t.plant_description
     ,t.country
     ,t.dw_insert_date
     ,t.dw_update_date
     ,t.projectsourceid
  FROM (
SELECT
	     DISTINCT d.segment16 plant_code
	    ,d.org_description plant_description
	    ,dbo.country
	    ,current_timestamp dw_insert_date
	    ,current_timestamp dw_update_date
	    ,dpr.dim_projectsourceid projectsourceid
   FROM fact_ora_invonhand f, dim_ora_invproduct d, dim_ora_business_org dbo, emdoracle4ae.dim_projectsource dpr
  WHERE f.dim_ora_inv_productid = d.dim_ora_invproductid
    AND f.dim_ora_inv_orgid = dbo.dim_ora_business_orgid
    ) t LEFT JOIN hlp_ora_plant h ON t.plant_code = h.plantcode
WHERE h.plantcode IS NULL;



UPDATE fact_ora_invonhand f SET dim_ora_plantid = 1;

UPDATE fact_ora_invonhand f
SET dim_ora_plantid = hlp_ora_plantid
FROM fact_ora_invonhand f, dim_ora_invproduct d, dim_ora_business_org dbo, hlp_ora_plant hlp
WHERE f.dim_ora_inv_productid = d.dim_ora_invproductid
	AND f.dim_ora_inv_orgid = dbo.dim_ora_business_orgid
	AND d.segment16 = hlp.plantcode
	AND dbo.country = hlp.country
    AND dbo.dim_projectsourceid = hlp.projectsourceid
	AND dim_ora_plantid <> hlp_ora_plantid;

/*Plant for SAP Harmonization*/


/* update dim_ora_bwhierarchycountryid*/

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business IN ('DIV.32') AND dim_con.internalhierarchyid = '548WP23VF6R8ICTMIER1FW7R0'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV.31','DIV.35','DIV.34') AND dim_con.internalhierarchyid = '548YXNWXHSJ2Y19OSUCHY2CL7'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88') AND dim_con.internalhierarchyid = '549HWG7F0J8VZ1P8YIYH1CPQ3'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV-87') AND dim_con.internalhierarchyid = '54G8L55S3E80Q2SXZYJAELHKR'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;


 UPDATE fact_ora_invonhand f
SET f.dim_ora_clusterid = dc.dim_ora_clusterid
FROM fact_ora_invonhand f, dim_ora_mdg_product mdg, dim_ora_bwproducthierarchy ph, dim_ora_cluster dc
WHERE f.dim_ora_mdg_productid = mdg.dim_mdg_partid
	AND f.dim_ora_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	AND businesssector = 'BS-02'
	AND mdg.primary_production_location = dc.primary_manufacturing_site
	AND f.dim_ora_clusterid <> dc.dim_ora_clusterid;


/*UPDATE DIM_EXPIRATION_DATE_ID*/

UPDATE fact_ora_invonhand f
SET
f.dim_ora_expiration_dateid = d.dim_dateid,
dw_update_date = current_timestamp
FROM fact_ora_invonhand f,ora_mtl_lot_numbers l,dim_date d
WHERE f.organization_id = l.organization_id
AND f.inventory_item_id =l.inventory_item_id
AND f.DD_LOT_NUMBER = l.LOT_NUMBER
AND l.expiration_date = d.datevalue
AND f.dim_ora_expiration_dateid <> d.dim_dateid;


UPDATE fact_ora_invonhand f
SET f.dim_countryhierpsid = dch.dim_countryhierpsid
FROM fact_ora_invonhand f, dim_ora_business_org dc, dim_ora_countryhierps dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND f.dim_countryhierpsid <> dch.dim_countryhierpsid;

UPDATE fact_ora_invonhand f
SET f.dim_countryhierarid  = dch.dim_countryhierarid
FROM fact_ora_invonhand f, dim_ora_business_org dc, dim_ora_countryhierar dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND f.dim_countryhierarid <> dch.dim_countryhierarid;

/* 20 July 2016 Cornelia add dd_isconsigned */
UPDATE fact_ora_invonhand F
SET
f.dd_isconsigned = ifnull(s.IS_CONSIGNED,0)
FROM  ORA_MTL_ONHAND_RESERVE S LEFT JOIN fact_ora_invonhand F ON
ifnull(F.INVENTORY_ITEM_ID,0) = ifnull(S.INVENTORY_ITEM_ID,0) AND
ifnull(F.ORGANIZATION_ID,0) = ifnull(S.ORGANIZATION_ID,0) AND
ifnull(F.CREATE_TRANSACTION_ID,0) = ifnull(S.CREATE_TRANSACTION_ID,0) AND
ifnull(F.UPDATE_TRANSACTION_ID,0) = ifnull(S.UPDATE_TRANSACTION_ID,0) AND
IFNULL(TRIM(f.RESERVATION_ID),0) =  IFNULL(TRIM(s.RESERVATION_ID),0) AND
ifnull(F.DD_LOT_NUMBER,'Not Set') = ifnull(S.LOT_NUMBER,'Not Set')
WHERE F.dd_isconsigned <> s.is_consigned;

/* Added the Commercial View on 27thJuly2016 by Alina

UPDATE fact_ora_invonhand f_so
SET f_so.dim_ora_commercialviewid = ds.dim_commercialviewid
FROM fact_ora_invonhand f_so, dim_commercialview ds, dim_ora_business_org dcl, dim_ora_bwproducthierarchy b
WHERE ds.soldtocountry = dcl.country
  AND ds.upperprodhier = b.business
  AND f_so.dim_ora_bwproducthierarchyid = b.dim_bwproducthierarchyid
  AND f_so.DIM_ORA_INV_ORGID = dcl.dim_ora_business_orgid
  AND f_so.dim_ora_commercialviewid <> ds.dim_commercialviewid


  Added the Safety Stock on 30th July 2016 by Alina
	commented on customer request by Victor on 22th of Sept 2016*/

TRUNCATE TABLE tmp_inv_part;
INSERT INTO tmp_inv_part
select f.DIM_ORA_INV_PRODUCTID
	,CASE WHEN SUM(CASE WHEN d_sinv.availability_type = 1 THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END)
	 < MAX( (prt.safety_stock_quantity) ) THEN 1 ELSE 0 END flag
	,max(fact_ora_invonhandid) id
from fact_ora_invonhand f
	inner join dim_ora_invproduct prt on f.DIM_ORA_INV_PRODUCTID = prt.dim_ora_invproductid
	join dim_ora_mtl_sec_inventory d_sinv
	on f.dim_ora_mtl_sec_inventoryid = d_sinv.dim_ora_mtl_sec_inventoryid
group by f.DIM_ORA_INV_PRODUCTID;

update fact_ora_invonhand f
set f.ct_countmaterialsafetystock = flag
from fact_ora_invonhand f
	inner join tmp_inv_part t on f.fact_ora_invonhandid = t.id
where f.ct_countmaterialsafetystock <> t.flag;

 /* Added the master data from product table to Inventory SA on 30th July 2016 by Alina*/


delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_invonhand';
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_invonhand',IFNULL(MAX(fact_ora_invonhandid),0)
FROM fact_ora_invonhand;

INSERT INTO Fact_ora_invonhand
(
fact_ora_invonhandid,DIM_ORA_INV_PRODUCTID,DIM_ORA_PRODUCTID,DIM_ORA_INV_ORGID,DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,DIM_ORA_MTL_SEC_INVENTORYID,CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,CT_PRIMARY_TRANSACTION_QUANTITY,DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,fact_ORA_INVMTL_UPDATE_TRXID,DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,DIM_ORA_ORGANIZATION_TYPEID,DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,DIM_ORA_PLANNING_ORGANIZATIONID,DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,CT_SECONDARY_TRANSACTION_QUANTITY,DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,CT_RESERVATION_QUANTITY,DD_SHIP_READY_FLAG,CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,CT_DEMAND_QUANTITY,dd_aging_flag,CREATE_TRANSACTION_ID,UPDATE_TRANSACTION_ID,RESERVATION_ID,INVENTORY_ITEM_ID,
ORGANIZATION_ID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID, DD_OWNING_ORGANIZATION_ID,
DD_PLANNING_ORGANIZATION_ID,DD_DATE_RECEIVED--,dd_isconsigned

)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_invonhand' ) + ROW_NUMBER() OVER(order by '') fact_ora_invonhandid,
 dinv.dim_ora_invproductid AS DIM_ORA_INV_PRODUCTID,
1 AS DIM_ORA_PRODUCTID,
1 AS DIM_ORA_INV_ORGID,
1 AS DIM_ORA_MTL_ITEM_REVID,
1 AS DIM_ORA_MTL_ITEM_LOCATORID,
1 AS DIM_ORA_MTL_SEC_INVENTORYID,
0 AS CT_TRANSACTION_QUANTITY,
'Not Set' AS DD_TRANSACTION_UOM_CODE,
0 AS CT_PRIMARY_TRANSACTION_QUANTITY,
1 AS DIM_ORA_DATE_RECEIVEDID,
0 AS fact_ORA_INVMTL_CREATE_TRXID,
0 AS fact_ORA_INVMTL_UPDATE_TRXID,
1 AS DIM_ORA_DATE_ORIG_RECEIVEDID,
0 AS DD_CONTAINERIZED_FLAG,
1 as DIM_ORA_ORGANIZATION_TYPEID,
1 AS DIM_ORA_OWNING_ORGANIZATIONID,
1 AS DIM_ORA_OWNINING_TP_TYPEID,
1 AS DIM_ORA_PLANNING_ORGANIZATIONID,
1 AS DIM_ORA_PLANNING_TP_TYPEID,
'Not Set' AS DD_SECONDARY_UOM_CODE,
0 AS CT_SECONDARY_TRANSACTION_QUANTITY,
'Not Set' AS DD_LOT_NUMBER,
1 AS DIM_ORA_DATE_REQUIREMENTID,
0 AS CT_RESERVATION_QUANTITY,
0 AS DD_SHIP_READY_FLAG,
0 AS CT_DETAILED_QUANTITY,
1 AS DIM_ORA_DATE_DEMAND_SHIPID,
0 AS CT_DEMAND_QUANTITY,
'Not Set' dd_aging_flag,
0 AS CREATE_TRANSACTION_ID,
0 AS UPDATE_TRANSACTION_ID,
0 AS RESERVATION_ID,
0 AS INVENTORY_ITEM_ID,
0 AS ORGANIZATION_ID,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID,
0 DD_OWNING_ORGANIZATION_ID,
0 DD_PLANNING_ORGANIZATION_ID,
'9999-12-31 00:00:00.000000' DD_DATE_RECEIVED
--0 dd_isconsigned

FROM dim_ora_invproduct dinv
WHERE dinv.dim_ora_invproductid NOT IN (SELECT f.DIM_ORA_INV_PRODUCTID FROM fact_ora_invonhand f)
AND dinv.safety_stock_quantity > 0;

/*Added OnHandQty from Inventory on 1st September 2016 by Victor
  Added new measures from Inventory on the 29th March 2017 by Veronica P */

TRUNCATE TABLE TMP_UPDATE_ONHANDQTY;

INSERT INTO TMP_UPDATE_ONHANDQTY
SELECT SUM (CASE WHEN d_sinv.availability_type = 1 AND d_sinv.asset_inventory = 1
				  THEN CONVERT (DECIMAL (18,5), ct_primary_transaction_quantity)
				  ELSE 0 END
		    )AS unrestricted_stock
	,SUM(CASE WHEN d_sinv.availability_type = 2 AND d_sinv.asset_inventory = 1
			  THEN (CONVERT (DECIMAL (18,5), ct_primary_transaction_quantity) )
			  ELSE 0 END
		    ) AS total_restricted_stock
	,SUM(
		CASE WHEN asset_inventory = 1 AND d_sinv.availability_type in (1,2) THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END
		+ CASE WHEN asset_inventory = 1 THEN CONVERT (DECIMAL (18,5),ct_reservation_quantity ) ELSE 0 END
		+ ct_intransit_quantity
		) AS ON_HAND_QTY
	,SUM(
		CASE WHEN asset_inventory = 1 AND d_sinv.availability_type in (1,2) THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END
		+ CASE WHEN asset_inventory = 1 THEN CONVERT (DECIMAL (18,5),ct_reservation_quantity ) ELSE 0 END
		+ CONVERT (DECIMAL (18,5),CT_WIP_QUANTITY )
		+ ct_intransit_quantity
		) AS Qty_On_Hand_WIP
	,SUM(CONVERT (DECIMAL (18,5),CT_WIP_QUANTITY)) AS CT_WIP_QUANTITY
	,SUM(ct_intransit_quantity) AS CT_intransit_qty
	,f.inventory_item_id
    ,f.organization_id
FROM fact_ora_invonhand f
	,dim_ora_mtl_sec_inventory d_sinv
WHERE f.dim_ora_mtl_sec_inventoryid = d_sinv.dim_ora_mtl_sec_inventoryid
GROUP BY
        f.inventory_item_id
       ,f.organization_id;

UPDATE fact_ora_sales_order_line f_so
SET f_so.ct_onhandqtyinv_unrestricted = t.unrestricted_stock
FROM fact_ora_sales_order_line f_so
    ,TMP_UPDATE_ONHANDQTY t
    ,dim_ora_invproduct dpr
WHERE f_so.dim_ora_inv_productid = dpr.dim_ora_invproductid
  AND dpr.inventory_item_id = t.inventory_item_id
  AND dpr.organization_id = t.organization_id
  AND f_so.ct_onhandqtyinv_unrestricted <> t.unrestricted_stock;

UPDATE fact_ora_sales_order_line f_so
SET f_so.ct_onhandqtyinv_total_restricted = t.total_restricted_stock
FROM fact_ora_sales_order_line f_so
    ,TMP_UPDATE_ONHANDQTY t
    ,dim_ora_invproduct dpr
WHERE f_so.dim_ora_inv_productid = dpr.dim_ora_invproductid
  AND dpr.inventory_item_id = t.inventory_item_id
  AND dpr.organization_id = t.organization_id
  AND f_so.ct_onhandqtyinv_total_restricted <> t.total_restricted_stock;

UPDATE fact_ora_sales_order_line f_so
SET f_so.ct_onhandqtyinv = t.on_hand_qty
FROM fact_ora_sales_order_line f_so
    ,TMP_UPDATE_ONHANDQTY t
    ,dim_ora_invproduct dpr
WHERE f_so.dim_ora_inv_productid = dpr.dim_ora_invproductid
  AND dpr.inventory_item_id = t.inventory_item_id
  AND dpr.organization_id = t.organization_id
  AND f_so.ct_onhandqtyinv <> t.on_hand_qty;

UPDATE fact_ora_sales_order_line f_so
SET f_so.ct_onhandqty_WIP = t.Qty_On_Hand_WIP
FROM fact_ora_sales_order_line f_so
    ,TMP_UPDATE_ONHANDQTY t
    ,dim_ora_invproduct dpr
WHERE f_so.dim_ora_inv_productid = dpr.dim_ora_invproductid
  AND dpr.inventory_item_id = t.inventory_item_id
  AND dpr.organization_id = t.organization_id
  AND f_so.ct_onhandqty_WIP <> t.Qty_On_Hand_WIP;

UPDATE fact_ora_sales_order_line f_so
SET f_so.ct_qty_WIP = t.CT_WIP_QUANTITY
FROM fact_ora_sales_order_line f_so
    ,TMP_UPDATE_ONHANDQTY t
    ,dim_ora_invproduct dpr
WHERE f_so.dim_ora_inv_productid = dpr.dim_ora_invproductid
  AND dpr.inventory_item_id = t.inventory_item_id
  AND dpr.organization_id = t.organization_id
  AND f_so.ct_qty_WIP <> t.CT_WIP_QUANTITY;

UPDATE fact_ora_sales_order_line f_so
SET f_so.ct_intransit_qty = t.CT_intransit_qty
FROM fact_ora_sales_order_line f_so
    ,TMP_UPDATE_ONHANDQTY t
    ,dim_ora_invproduct dpr
WHERE f_so.dim_ora_inv_productid = dpr.dim_ora_invproductid
  AND dpr.inventory_item_id = t.inventory_item_id
  AND dpr.organization_id = t.organization_id
  AND f_so.ct_intransit_qty <> t.CT_intransit_qty;


/*Added WIP measures on the 9th September 2016 by Alina*/

TRUNCATE TABLE temp_WipDiscrete;

INSERT INTO temp_WipDiscrete
SELECT
organization_code org
,INVENTORY_ITEM_ID
,ORGANIZATION_ID
,wip_entity_name
,wip_entity_id
,(nvl(START_QUANTITY,0) - ( nvl(quantity_completed,0) + nvl(QUANTITY_SCRAPPED,0) )) open_qty,

SUM
(ROUND(

nvl(pl_material_in,0)

+ nvl(pl_material_overhead_in,0)

+ nvl(tl_resource_in,0)+ nvl(pl_resource_in,0)

+ nvl(tl_outside_processing_in,0)+ nvl(pl_outside_processing_in,0)

+ nvl(tl_overhead_in,0)+ nvl(pl_overhead_in,0)

+ nvl(tl_scrap_in,0), 2)

) -

SUM
(ROUND(

nvl(tl_material_out,0) + nvl(pl_material_out,0)

+ nvl(pl_material_overhead_out,0) + nvl(tl_material_overhead_out,0)

+ nvl(tl_resource_out,0) + nvl(pl_resource_out,0)

+ nvl(tl_outside_processing_out,0) + nvl(pl_outside_processing_out,0)

+ nvl(tl_overhead_out,0) + nvl(pl_overhead_out,0)

+ nvl(tl_scrap_out,0), 2)

) -

SUM
(ROUND(

nvl(tl_material_var,0) + nvl(pl_material_var,0)

+ nvl(pl_material_overhead_var,0) + nvl(tl_material_overhead_var,0)

+ nvl(tl_resource_var,0) + nvl(pl_resource_var,0)

+ nvl(tl_outside_processing_var,0) + nvl(pl_outside_processing_var,0)

+ nvl(tl_overhead_var,0) + nvl(pl_overhead_var,0)

+ nvl(tl_scrap_var,0), 2)) wip_value
FROM ora_wip_summary

GROUP BY organization_code
        ,INVENTORY_ITEM_ID
		,ORGANIZATION_ID
		,wip_entity_name
		,wip_entity_id,
		nvl(start_quantity,0) - ( nvl(quantity_completed,0) + nvl(quantity_scrapped,0) )

HAVING

SUM
(ROUND(

nvl(pl_material_in,0)

+ nvl(pl_material_overhead_in,0)

+ nvl(tl_resource_in,0)+ nvl(pl_resource_in,0)

+ nvl(tl_outside_processing_in,0)+ nvl(pl_outside_processing_in,0)

+ nvl(tl_overhead_in,0)+ nvl(pl_overhead_in,0)

+ nvl(tl_scrap_in,0), 2)

) -

SUM
(ROUND(

nvl(tl_material_out,0) + nvl(pl_material_out,0)

+ nvl(pl_material_overhead_out,0) + nvl(tl_material_overhead_out,0)

+ nvl(tl_resource_out,0) + nvl(pl_resource_out,0)

+ nvl(tl_outside_processing_out,0) + nvl(pl_outside_processing_out,0)

+ nvl(tl_overhead_out,0) + nvl(pl_overhead_out,0)

+ nvl(tl_scrap_out,0), 2)

) -

SUM
(ROUND(

nvl(tl_material_var,0) + nvl(pl_material_var,0)

+ nvl(pl_material_overhead_var,0) + nvl(tl_material_overhead_var,0)

+ nvl(tl_resource_var,0) + nvl(pl_resource_var,0)

+ nvl(tl_outside_processing_var,0) + nvl(pl_outside_processing_var,0)

+ nvl(tl_overhead_var,0) + nvl(pl_overhead_var,0)

+ nvl(tl_scrap_var,0), 2))  <> 0

ORDER BY 1,3,4;



delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_invonhand';
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_invonhand',IFNULL(MAX(fact_ora_invonhandid),0)
FROM fact_ora_invonhand;


INSERT INTO Fact_ora_invonhand
(
fact_ora_invonhandid,DIM_ORA_INV_PRODUCTID,DIM_ORA_PRODUCTID,DIM_ORA_INV_ORGID,DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,DIM_ORA_MTL_SEC_INVENTORYID,CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,CT_PRIMARY_TRANSACTION_QUANTITY,DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,fact_ORA_INVMTL_UPDATE_TRXID,DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,DIM_ORA_ORGANIZATION_TYPEID,DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,DIM_ORA_PLANNING_ORGANIZATIONID,DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,CT_SECONDARY_TRANSACTION_QUANTITY,DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,CT_RESERVATION_QUANTITY,DD_SHIP_READY_FLAG,CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,CT_DEMAND_QUANTITY,dd_aging_flag,CREATE_TRANSACTION_ID,UPDATE_TRANSACTION_ID,RESERVATION_ID,INVENTORY_ITEM_ID,
ORGANIZATION_ID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID, DD_OWNING_ORGANIZATION_ID,
DD_PLANNING_ORGANIZATION_ID,DD_DATE_RECEIVED,dd_isconsigned
,amt_wip,CT_WIP_QUANTITY
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_invonhand' ) + ROW_NUMBER() OVER(order by '') fact_ora_invonhandid,
1 AS DIM_ORA_INV_PRODUCTID,
1 AS DIM_ORA_PRODUCTID,
1 AS DIM_ORA_INV_ORGID,
1 AS DIM_ORA_MTL_ITEM_REVID,
1 AS DIM_ORA_MTL_ITEM_LOCATORID,
1 AS DIM_ORA_MTL_SEC_INVENTORYID,
0 AS CT_TRANSACTION_QUANTITY,
0 AS DD_TRANSACTION_UOM_CODE,
0 AS CT_PRIMARY_TRANSACTION_QUANTITY,
1 AS DIM_ORA_DATE_RECEIVEDID,
0 AS fact_ORA_INVMTL_CREATE_TRXID,
0 AS fact_ORA_INVMTL_UPDATE_TRXID,
1 AS DIM_ORA_DATE_ORIG_RECEIVEDID,
1 AS DD_CONTAINERIZED_FLAG,
1 as DIM_ORA_ORGANIZATION_TYPEID,
1 AS DIM_ORA_OWNING_ORGANIZATIONID,
1 AS DIM_ORA_OWNINING_TP_TYPEID,
1 AS DIM_ORA_PLANNING_ORGANIZATIONID,
1 AS DIM_ORA_PLANNING_TP_TYPEID,
'Not Set' AS DD_SECONDARY_UOM_CODE,
0 AS CT_SECONDARY_TRANSACTION_QUANTITY,
s.wip_entity_name AS DD_LOT_NUMBER,
1 AS DIM_ORA_DATE_REQUIREMENTID,
0 AS CT_RESERVATION_QUANTITY,
0 AS DD_SHIP_READY_FLAG,
0 AS CT_DETAILED_QUANTITY,
1 AS DIM_ORA_DATE_DEMAND_SHIPID,
0 AS CT_DEMAND_QUANTITY,
'Not Set' dd_aging_flag,
ifnull(s.wip_entity_id,0) AS CREATE_TRANSACTION_ID,
ifnull(s.wip_entity_id,0) AS UPDATE_TRANSACTION_ID,
0 AS RESERVATION_ID,
s.INVENTORY_ITEM_ID AS INVENTORY_ITEM_ID,
s.ORGANIZATION_ID AS ORGANIZATION_ID,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'WIP' AS SOURCE_ID,
0 DD_OWNING_ORGANIZATION_ID,
0 DD_PLANNING_ORGANIZATION_ID,
'9999-12-31 00:00:00.000000' DD_DATE_RECEIVED,
0 dd_isconsigned,
wip_value as amt_wip,
s.open_qty

FROM temp_WipDiscrete s;


UPDATE FACT_ORA_INVONHAND F
SET
F.DIM_ORA_INV_PRODUCTID = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, temp_WipDiscrete S JOIN DIM_ORA_INVPRODUCT D
ON CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'||  CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID
and f.DD_LOT_NUMBER=s.wip_entity_name and f.CREATE_TRANSACTION_ID=s.wip_entity_id
AND f.source_id='WIP';


UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_INV_ORGID= D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, temp_WipDiscrete S JOIN DIM_ORA_BUSINESS_ORG D
ON 'INV' ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID
and f.DD_LOT_NUMBER=s.wip_entity_name and f.CREATE_TRANSACTION_ID=s.wip_entity_id
AND f.source_id='WIP';


 /* Update for mdg_part: Global Material Number */

UPDATE fact_ora_invonhand f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM fact_ora_invonhand f_inv, dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.dim_ora_inv_productid = d_invp.dim_ora_invproductid
 AND DECODE(d_invp.mdm_globalid,'Not Set',REPLACE(d_invp.segment1,'.',''),d_invp.mdm_globalid) = d_mp.partnumber
 AND f_inv.source_id='WIP'
 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;


 /*update dim_ora_bwproducthierarchyid*/

/* UPDATE fact_ora_invonhand f
 SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_invonhand f, dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inv_productid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND dp.productgroupsbu = bw.productgroup
AND '2016-12-20' BETWEEN bw.upperhierstartdate AND bw.upperhierenddate -- TO BE REPLACED AFTER 01.01.2017: '2016-12-20' WITH CURRENT_DATE
and f.source_id='WIP'
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid*/



 UPDATE fact_ora_invonhand f
 SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND CASE WHEN substring(dp.GlbProductGroup,1,3)='SBU' THEN dp.GlbProductGroup
ELSE concat('SBU-',dp.GlbProductGroup) END = bw.productgroup
AND '2017-12-20' BETWEEN bw.upperhierstartdate AND bw.upperhierenddate -- TO BE REPLACED AFTER 01.01.2017: '2016-12-20' WITH CURRENT_DATE
and f.source_id='WIP'
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
and f.source_id='WIP'
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;


UPDATE fact_ora_invonhand  f_inv
SET f_inv.dd_standart_cost = IFNULL(hlp.standard_cost,0)
FROM fact_ora_invonhand  f_inv, hlp_ora_cst_standards_costs hlp, hlp_ora_cst_standards_costs_tmp hlp_t
WHERE f_inv.inventory_item_id = hlp.inventory_item_id
  AND f_inv.organization_id = hlp.organization_id
  AND hlp_t.inventory_item_id = hlp.inventory_item_id
  AND hlp_t.organization_id = hlp.organization_id
  AND hlp.standard_cost_revision_date = hlp_t.standard_cost_revision_date
  AND f_inv.source_id='WIP'
  AND f_inv.dd_standart_cost <> IFNULL(hlp.standard_cost,0);

  UPDATE fact_ora_invonhand f
SET f.dim_ora_clusterid = dc.dim_ora_clusterid
FROM fact_ora_invonhand f, dim_ora_mdg_product mdg, dim_ora_bwproducthierarchy ph, dim_ora_cluster dc
WHERE f.dim_ora_mdg_productid = mdg.dim_mdg_partid
	AND f.dim_ora_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	AND businesssector = 'BS-02'
	AND f.source_id='WIP'
	AND mdg.primary_production_location = dc.primary_manufacturing_site
	AND f.dim_ora_clusterid <> dc.dim_ora_clusterid;



/* COGS FROM SAP */
TRUNCATE TABLE csv_cogs;
INSERT INTO csv_cogs
SELECT * FROM EMDTempoCC4.csv_cogs;

UPDATE fact_ora_invonhand f
SET f.amt_cogsactualrate_emd = IFNULL(c.Z_GCACTF,0)
	,f.amt_cogsfixedrate_emd = IFNULL(c.Z_GCFIXF,0)
	,f.amt_cogsfixedplanrate_emd = IFNULL(c.Z_GCPLFF,0)
	,f.amt_cogsplanrate_emd = IFNULL(c.Z_GCPLRF,0)
	,f.amt_cogsprevyearfixedrate_emd = IFNULL(c.Z_GCPYFF,0)
	,f.amt_cogsprevyearrate_emd = IFNULL(c.Z_GCPYRF,0)
	,f.amt_cogsprevyearto1_emd = IFNULL(c.Z_GCPYTF1,0)
	,f.amt_cogsprevyearto2_emd = IFNULL(c.Z_GCPYTF2,0)
	,f.amt_cogsprevyearto3_emd = IFNULL(c.Z_GCPYTF3,0)
	,f.amt_cogsturnoverrate1_emd = IFNULL(c.Z_GCTOF1,0)
	,f.amt_cogsturnoverrate2_emd = IFNULL(c.Z_GCTOF2,0)
	,f.amt_cogsturnoverrate3_emd = IFNULL(c.Z_GCTOF3,0)
FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_business_org dc, csv_cogs c
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
	AND f.dim_ora_inv_orgid = dc.dim_ora_business_orgid
	AND TRIM(LEADING 0 FROM c.PRODUCT)= dp.partnumber
	and f.source_id='WIP'
	AND c.Z_REPUNIT = dc.cmg_number;



UPDATE fact_ora_invonhand f
SET f.dim_countryhierpsid = dch.dim_countryhierpsid
FROM fact_ora_invonhand f, dim_ora_business_org dc, dim_ora_countryhierps dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND f.source_id='WIP'
AND f.dim_countryhierpsid <> dch.dim_countryhierpsid;

UPDATE fact_ora_invonhand f
SET f.dim_countryhierarid  = dch.dim_countryhierarid
FROM fact_ora_invonhand f, dim_ora_business_org dc, dim_ora_countryhierar dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND f.source_id='WIP'
AND f.dim_countryhierarid <> dch.dim_countryhierarid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business IN ('DIV.32') AND dim_con.internalhierarchyid = '548WP23VF6R8ICTMIER1FW7R0'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='WIP'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV.31','DIV.35','DIV.34') AND dim_con.internalhierarchyid = '548YXNWXHSJ2Y19OSUCHY2CL7'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='WIP'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88') AND dim_con.internalhierarchyid = '549HWG7F0J8VZ1P8YIYH1CPQ3'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='WIP'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV-87') AND dim_con.internalhierarchyid = '54G8L55S3E80Q2SXZYJAELHKR'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='WIP'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f_inv
SET amt_exchangerate_gbl = IFNULL(dr.exrate,1)
FROM fact_ora_invonhand f_inv, csv_oprate dr,dim_ora_business_org dc,ora_xx_legal_entities l
WHERE f_inv.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND dc.organization_id=l.inv_org_id
AND dr.toc = 'EUR'
AND dr.fromc=l.currency_code
AND amt_exchangerate_gbl <> IFNULL(dr.exrate,1)
and f_inv.source_id='WIP'
and dc.organization_code in ('BUR','TEW','HAW','LRW');


/*End WIP measures on the 9th September 2016 by Alina*/
