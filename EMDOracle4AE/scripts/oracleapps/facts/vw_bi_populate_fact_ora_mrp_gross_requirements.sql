/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mrp_gross_requirements*/
/*SELECT * FROM fact_ora_mrp_gross_requirements*/


/*initialize NUMBER_FOUNTAIN_fact_ora_mrp_gross_requirements*/
delete from NUMBER_FOUNTAIN_fact_ora_mrp_gross_requirements where table_name = 'fact_ora_mrp_gross_requirements';	

INSERT INTO NUMBER_FOUNTAIN_fact_ora_mrp_gross_requirements
SELECT 'fact_ora_mrp_gross_requirements',IFNULL(MAX(fact_ora_mrp_gross_requirementsid),0)
FROM fact_ora_mrp_gross_requirements;

/*update fact columns*/
UPDATE fact_ora_mrp_gross_requirements T
SET
DEMAND_ID = S.DEMAND_ID,
CT_USING_REQUIREMENTS_QUANTITY = S.USING_REQUIREMENTS_QUANTITY,
DD_DEMAND_TYPE = S.DEMAND_TYPE,
CT_DAILY_DEMAND_RATE = ifnull(S.DAILY_DEMAND_RATE,0),
CT_RESERVE_QUANTITY = ifnull(S.RESERVE_QUANTITY,0),
DD_UPDATED = ifnull(S.UPDATED,0),
DD_STATUS = ifnull(S.STATUS,0),
DD_APPLIED = ifnull(S.APPLIED,0),
DD_DEMAND_CLASS = ifnull(S.DEMAND_CLASS,'Not Set'),
CT_FIRM_QUANTITY = ifnull(S.FIRM_QUANTITY,0),
CT_OLD_DEMAND_QUANTITY = ifnull(S.OLD_DEMAND_QUANTITY,0),
DD_DEMAND_SCHEDULE_NAME = ifnull(S.DEMAND_SCHEDULE_NAME,'Not Set'),
DD_PLANNING_GROUP = ifnull(S.PLANNING_GROUP,'Not Set'),
DD_END_ITEM_UNIT_NUMBER = ifnull(S.END_ITEM_UNIT_NUMBER,'Not Set'),
amt_exchangerate = 1,
amt_exchangerate_gbl = 1,
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
FROM fact_ora_mrp_gross_requirements T,ora_mrp_gross_requirements S
WHERE
T.DEMAND_ID = S.DEMAND_ID;

/*insert new rows*/
INSERT INTO fact_ora_mrp_gross_requirements
(
fact_ora_mrp_gross_requirementsid,DEMAND_ID,
DIM_ORA_DATE_LAST_UPDATEid,DIM_ORA_LAST_UPDATED_Byid,DIM_ORA_DATE_CREATIONid,DIM_ORA_CREATED_Byid,
DIM_ORA_INVENTORY_ITEMid,DIM_ORA_ORGANIZATIONid,
DIM_ORA_COMPILE_DESIGNATORid,DIM_ORA_USING_ASSEMBLY_ITEMid,DIM_ORA_DATE_USING_ASSEMBLY_DEMANDid,
CT_USING_REQUIREMENTS_QUANTITY,DIM_ORA_DATE_ASSEMBLY_DEMAND_COMPid,
DD_DEMAND_TYPE,DIM_ORA_ORIGINATION_TYPEid,fact_ora_mrp_recommendationsid,CT_DAILY_DEMAND_RATE,
CT_RESERVE_QUANTITY,DIM_ORA_DATE_DATE_PROGRAM_UPDATEid,DIM_ORA_SOURCE_ORGANIZATIONid,DD_UPDATED,
DD_STATUS,DD_APPLIED,DD_DEMAND_CLASS,CT_FIRM_QUANTITY,DIM_ORA_DATE_FIRMid,CT_OLD_DEMAND_QUANTITY,
DD_DEMAND_SCHEDULE_NAME,DIM_ORA_DATE_OLD_DEMANDid,DD_PLANNING_GROUP,DD_END_ITEM_UNIT_NUMBER,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_fact_ora_mrp_gross_requirements WHERE TABLE_NAME = 'fact_ora_mrp_gross_requirements' ),0) + ROW_NUMBER() OVER(order by '') fact_ora_mrp_gross_requirementsid,
S.DEMAND_ID AS DEMAND_ID,
1 AS DIM_ORA_DATE_LAST_UPDATEid,
1 AS DIM_ORA_LAST_UPDATED_Byid,
1 AS DIM_ORA_DATE_CREATIONid,
1 AS DIM_ORA_CREATED_Byid,
1 AS DIM_ORA_INVENTORY_ITEMid,
1 AS DIM_ORA_ORGANIZATIONid,
1 AS DIM_ORA_COMPILE_DESIGNATORid,
1 AS DIM_ORA_USING_ASSEMBLY_ITEMid,
1 AS DIM_ORA_DATE_USING_ASSEMBLY_DEMANDid,
S.USING_REQUIREMENTS_QUANTITY AS CT_USING_REQUIREMENTS_QUANTITY,
1 AS DIM_ORA_DATE_ASSEMBLY_DEMAND_COMPid,
S.DEMAND_TYPE AS DD_DEMAND_TYPE,
1 AS DIM_ORA_ORIGINATION_TYPEid,
0 AS fact_ora_mrp_recommendationsid,
ifnull(S.DAILY_DEMAND_RATE,0) AS CT_DAILY_DEMAND_RATE,
ifnull(S.RESERVE_QUANTITY,0) AS CT_RESERVE_QUANTITY,
1 AS DIM_ORA_DATE_DATE_PROGRAM_UPDATEid,
1 AS DIM_ORA_SOURCE_ORGANIZATIONid,
ifnull(S.UPDATED,0) AS DD_UPDATED,
ifnull(S.STATUS,0) AS DD_STATUS,
ifnull(S.APPLIED,0) AS DD_APPLIED,
ifnull(S.DEMAND_CLASS,'Not Set') AS DD_DEMAND_CLASS,
ifnull(S.FIRM_QUANTITY,0) AS CT_FIRM_QUANTITY,
1 AS DIM_ORA_DATE_FIRMid,
ifnull(S.OLD_DEMAND_QUANTITY,0) AS CT_OLD_DEMAND_QUANTITY,
ifnull(S.DEMAND_SCHEDULE_NAME,'Not Set') AS DD_DEMAND_SCHEDULE_NAME,
1 AS DIM_ORA_DATE_OLD_DEMANDid,
ifnull(S.PLANNING_GROUP,'Not Set') AS DD_PLANNING_GROUP,
ifnull(S.END_ITEM_UNIT_NUMBER,'Not Set') AS DD_END_ITEM_UNIT_NUMBER,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'INSERT' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mrp_gross_requirements S LEFT JOIN fact_ora_mrp_gross_requirements F
ON F.DEMAND_ID = S.DEMAND_ID
WHERE F.DEMAND_ID is null;

/*UPDATE DIM_ORA_DATE_LAST_UPDATEid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_DATE_LAST_UPDATEid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_DATE D ON to_date(S.LAST_UPDATE_DATE) = D.datevalue
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_DATE_LAST_UPDATEid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_LAST_UPDATED_Byid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_LAST_UPDATED_Byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_LAST_UPDATED_Byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_DATE_CREATIONid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_DATE_CREATIONid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_DATE D ON to_date(S.CREATION_DATE) = D.datevalue
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_DATE_CREATIONid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_CREATED_Byid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_CREATED_Byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_CREATED_Byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_INVENTORY_ITEMid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_INVENTORY_ITEMid = D.DIM_ORA_MRP_SYSTEM_ITEMSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_ORA_MRP_SYSTEM_ITEMS D
ON CONVERT(VARCHAR(200),S.COMPILE_DESIGNATOR)||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID)||'~'|| CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_INVENTORY_ITEMid <> D.DIM_ORA_MRP_SYSTEM_ITEMSID;

/*UPDATE DIM_ORA_ORGANIZATIONid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_ORGANIZATIONid = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_ORA_MTL_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_ORGANIZATIONid <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE DIM_ORA_COMPILE_DESIGNATORid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_COMPILE_DESIGNATORid = D.DIM_ORA_MRP_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_ORA_MRP_DESIGNATORS D
ON CONVERT(VARCHAR(200),S.COMPILE_DESIGNATOR) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_COMPILE_DESIGNATORid <> D.DIM_ORA_MRP_DESIGNATORSID;

/*UPDATE DIM_ORA_USING_ASSEMBLY_ITEMid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_USING_ASSEMBLY_ITEMid = D.DIM_ORA_MRP_SYSTEM_ITEMSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_ORA_MRP_SYSTEM_ITEMS D
ON CONVERT(VARCHAR(200),S.COMPILE_DESIGNATOR) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'|| CONVERT(VARCHAR(200),S.USING_ASSEMBLY_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_USING_ASSEMBLY_ITEMid <> D.DIM_ORA_MRP_SYSTEM_ITEMSID;

/*UPDATE DIM_ORA_DATE_USING_ASSEMBLY_DEMANDid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_DATE_USING_ASSEMBLY_DEMANDid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_DATE D ON to_date(S.USING_ASSEMBLY_DEMAND_DATE) = D.datevalue
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_DATE_USING_ASSEMBLY_DEMANDid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE_ASSEMBLY_DEMAND_COMPid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_DATE_ASSEMBLY_DEMAND_COMPid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_DATE D ON to_date(S.ASSEMBLY_DEMAND_COMP_DATE) = D.datevalue
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_DATE_ASSEMBLY_DEMAND_COMPid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_ORIGINATION_TYPEid*/

TRUNCATE TABLE ORG_TPY_TMP;
INSERT INTO ORG_TPY_TMP
SELECT D.LOOKUP_CODE,D.DIM_ORA_FND_LOOKUPID
from DIM_ORA_FND_LOOKUP D
WHERE D.LOOKUP_TYPE = 'MRP_SCHEDULE_ORIG'
AND d.rowiscurrent = 1;


UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_ORIGINATION_TYPEid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN ORG_TPY_TMP D
ON S.ORIGINATION_TYPE = D.LOOKUP_CODE
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_ORIGINATION_TYPEid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE fact_ora_mrp_recommendationsid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.fact_ora_mrp_recommendationsid = D.FACT_ORA_MRP_RECOMMENDATIONSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S
JOIN fact_ora_mrp_recommendations D
ON S.DISPOSITION_ID = D.TRANSACTION_ID  and d.rowiscurrent = 1
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.fact_ora_mrp_recommendationsid <> D.FACT_ORA_MRP_RECOMMENDATIONSID;

/*UPDATE DIM_ORA_DATE_DATE_PROGRAM_UPDATEid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_DATE_DATE_PROGRAM_UPDATEid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_DATE D ON to_date(S.PROGRAM_UPDATE_DATE) = D.datevalue
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_DATE_DATE_PROGRAM_UPDATEid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_SOURCE_ORGANIZATIONid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_SOURCE_ORGANIZATIONid = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_ORA_MTL_PARAMETERS D ON S.SOURCE_ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_SOURCE_ORGANIZATIONid <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE DIM_ORA_DATE_FIRMid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_DATE_FIRMid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_DATE D ON to_date(S.FIRM_DATE) = D.datevalue
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_DATE_FIRMid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE_OLD_DEMANDid*/
UPDATE fact_ora_mrp_gross_requirements F
SET
F.DIM_ORA_DATE_OLD_DEMANDid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_gross_requirements F,ora_mrp_gross_requirements S JOIN DIM_DATE D ON to_date(S.OLD_DEMAND_DATE) = D.datevalue
WHERE F.DEMAND_ID= S.DEMAND_ID AND
F.DIM_ORA_DATE_OLD_DEMANDid <> D.DIM_DATEID;
