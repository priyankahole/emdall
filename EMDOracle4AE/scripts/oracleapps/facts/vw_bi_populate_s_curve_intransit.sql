
drop table if exists temp_intransit_scurve;
create table temp_intransit_scurve
as
select INVENTORY_ITEM_ID,sum(QUANTITY) qty,
shipment_num,PART,SEGMENT1,SUPPLY_TYPE_CODE,
FREIGHT_CARRIER_CODE,RECEIPT_DATE,MP_FROM_ORG_CODE,MP_TO_ORG_CODE,NEED_BY_DATE,SHIPPED_DATE,APPROVED_DATE,
decode(supply_type_code,'SHIPMENT','In Transit',supply_type_code) status,creation_date
from
ORA_MTL_INTRANSIT_SCURVE
group by INVENTORY_ITEM_ID,
shipment_num,PART,SEGMENT1,SUPPLY_TYPE_CODE,
FREIGHT_CARRIER_CODE,RECEIPT_DATE,MP_FROM_ORG_CODE,MP_TO_ORG_CODE,NEED_BY_DATE,SHIPPED_DATE,APPROVED_DATE,
supply_type_code,creation_date ;

DELETE FROM NUMBER_FOUNTAIN where table_name = 'ora_s_curve_final';
INSERT INTO NUMBER_FOUNTAIN
SELECT 'ora_s_curve_final',IFNULL(MAX(ora_s_curve_finalid),0)
FROM ora_s_curve_final;

delete from ora_intransit_scurve;

INSERT INTO ora_intransit_scurve

(
ORA_INTRANSIT_SCURVEID,
INVENTORY_ITEM_ID,
QTY,
SHIPMENT_NUMBER,
PO_NUMBER,
PART,
SUPPLY_TYPE_CODE,
FREIGHT_CARRIER_CODE,
RECEIPT_DATE,
MP_FROM_ORG_CODE,
MP_TO_ORG_CODE,
NEED_BY_DATE,
SHIPPED_DATE,
APPROVED_DATE,
STATUS,
DIM_PROJECTSOURCEID,
creation_date)


select
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'ora_s_curve_final' ) + ROW_NUMBER() OVER(order by '') ora_intransit_scurveid,
INVENTORY_ITEM_ID,
QTY,
SHIPMENT_NUM,
SEGMENT1,
PART,
SUPPLY_TYPE_CODE,
FREIGHT_CARRIER_CODE,
RECEIPT_DATE,
MP_FROM_ORG_CODE,
MP_TO_ORG_CODE,
NEED_BY_DATE,
SHIPPED_DATE,
APPROVED_DATE,
STATUS,
7 DIM_PROJECTSOURCEID,
creation_date

from temp_intransit_scurve;


UPDATE ora_intransit_scurve f
SET  f.DIM_ORA_INV_PRODUCTID = D.DIM_ORA_INVPRODUCTID
FROM ora_intransit_scurve  f,dim_ora_invproduct d
WHERE 190 ||'~'||  CONVERT(VARCHAR(200),f.INVENTORY_ITEM_ID) = D.KEY_ID;

UPDATE ora_intransit_scurve f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM ora_intransit_scurve f_inv,dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.DIM_ORA_INV_PRODUCTID = d_invp.dim_ora_invproductid
AND DECODE(d_invp.mdm_globalid,'Not Set',REPLACE(d_invp.segment1,'.',''),d_invp.mdm_globalid) = d_mp.partnumber;

UPDATE ora_intransit_scurve f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM ora_intransit_scurve f,dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND CASE WHEN substring(dp.GlbProductGroup,1,3)='SBU' THEN dp.GlbProductGroup
ELSE concat('SBU-',dp.GlbProductGroup) END = bw.productgroup
AND current_date BETWEEN bw.upperhierstartdate AND bw.upperhierenddate
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE ora_intransit_scurve f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM ora_intransit_scurve f,dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;


UPDATE ora_intransit_scurve f
SET F.DIM_ORA_BUSINESS_ORGID = D.DIM_ORA_BUSINESS_ORGID
FROM ora_intransit_scurve f,DIM_ORA_BUSINESS_ORG d
WHERE f.MP_FROM_ORG_CODE=d.organization_code;

UPDATE ora_intransit_scurve c
SET c.dim_ora_RECEIP_DATE_dateid=mdg.dim_dateid
FROM ora_intransit_scurve c,dim_date mdg
WHERE receipt_date=datevalue
AND c.dim_ora_RECEIP_DATE_dateid <> mdg.dim_dateid;

UPDATE ora_intransit_scurve c
SET c.dim_ora_APPROVED_dateid=mdg.dim_dateid
FROM ora_intransit_scurve c,dim_date mdg
WHERE approved_date=datevalue
AND c.dim_ora_APPROVED_dateid <> mdg.dim_dateid ;

UPDATE ora_intransit_scurve c
SET c.dim_ora_SHIPPED_DATEid=mdg.dim_dateid
FROM ora_intransit_scurve c,dim_date mdg
WHERE SHIPPED_DATE=datevalue
AND c.dim_ora_SHIPPED_DATEid <> mdg.dim_dateid;

UPDATE ora_intransit_scurve c
SET c.dim_ora_NEED_BY_DATEid=mdg.dim_dateid
FROM ora_intransit_scurve c,dim_date mdg
WHERE NEED_BY_DATE=datevalue
AND c.dim_ora_NEED_BY_DATEid <> mdg.dim_dateid;

UPDATE ora_intransit_scurve c
SET c.dim_ora_vendor_purchasing_intrid=d.dim_ora_vendor_purchasing_intrid
FROM ora_intransit_scurve c,dim_ora_vendor_purchasing_intr d
WHERE c.MP_FROM_ORG_CODE=d.vendor_code;

UPDATE ora_intransit_scurve c
SET c.dim_ora_shippinginstruction_instrid=d.dim_ora_shippinginstruction_instrid
FROM ora_intransit_scurve c,dim_ora_shippinginstruction_instr d
WHERE c.FREIGHT_CARRIER_CODE =d.shipping_instruction_code;

UPDATE ora_intransit_scurve c
SET c.dim_ora_creation_DATEid=mdg.dim_dateid
FROM ora_intransit_scurve c,dim_date mdg
WHERE creation_date=datevalue;
