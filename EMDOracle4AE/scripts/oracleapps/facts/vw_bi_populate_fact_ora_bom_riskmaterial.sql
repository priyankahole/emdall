/*set session authorization oracle_data*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_bom_riskmaterial';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_bom_riskmaterial',IFNULL(MAX(fact_ora_bom_riskmaterialid),0)
FROM fact_ora_bom_riskmaterial;

/*dim_ora_supplierid*/ 
DROP table IF EXISTS dim_ora_supplier_tmp;
create table dim_ora_supplier_tmp as
select distinct po.dim_ora_vendorid, p.key_id item_id from fact_ora_purchaseorder po
inner join dim_ora_product p on po.dim_ora_productid = p.dim_ora_productid
group by dim_ora_vendorid, p.key_id
order by dim_ora_vendorid, p.key_id;

DROP TABLE IF EXISTS ora_bom_riskmaterial_tmp;

create table ora_bom_riskmaterial_tmp as
select
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_bom_riskmaterial' ) + ROW_NUMBER() OVER(order by '') fact_ora_bom_riskmaterialid,
toplevelparent,
	"path",
parentdesc,
childpart,
childdesc,
childrevision,
purchaseflag,
level1,
organizationid,
parentid,
childid,
makebuy,
quantity,
ifnull(s.dim_ora_vendorid,1) dim_ora_vendorid
from ora_bom_riskmaterial b
left join dim_ora_supplier_tmp s on b.childid = s.item_id;

TRUNCATE TABLE ora_bom_riskmaterial;

insert into ora_bom_riskmaterial
select
fact_ora_bom_riskmaterialid,
toplevelparent,
"path",
parentdesc,
childpart,
childdesc,
childrevision,
purchaseflag,
level1,
organizationid,
parentid,
childid,
makebuy,
quantity,
dim_ora_vendorid
from ora_bom_riskmaterial_tmp;

DROP table IF EXISTS ora_bom_riskmaterial_tmp;
DROP table IF EXISTS dim_ora_supplier_tmp;

DELETE FROM fact_ora_bom_riskmaterial
WHERE snapshot_date = to_timestamp(to_date(current_timestamp));

INSERT INTO fact_ora_bom_riskmaterial
(
fact_ora_bom_riskmaterialid,
dim_ora_parent_productid,dim_ora_component_productid,
dd_toplevelparent,dd_path,dd_component_level,dim_ora_supplierid,dd_revision_number,dd_current_flag,
ct_quantity,dd_abc_classification,amt_std_unit_cost,amt_unit_price_paid,
ct_lead_time,ct_lead_time_variance,ct_po_count,amt_po_amount,ct_po_quantity,
amt_open_po_amount,ct_open_po_quantity,
ct_valid_info_records,ct_open_so_count,ct_pr_count,amt_requisition_amount,ct_requisition_quantity,
amt_open_requisition_amount,ct_open_requisition_quantity,ct_rejected_quantity,
ct_approved_PO_count_without_pr,dim_ora_source_list_vendorid,
ct_cancel_msg_count,ct_cancel_msg_quantity,
ct_pull_in_msg_count,ct_pull_in_msg_quantity,ct_push_out_msg_count,ct_push_out_msg_quantity,
ct_plan_order_count,ct_plan_order_quantity,ct_production_order_count,
ct_production_order_quantity,ct_open_production_order_count,ct_open_production_order_quantity,
ct_past_due_count,ct_past_due_quantity,ct_avg_delivery_performance_current,
ct_avg_delivery_performance_last12months,ct_onhand_current_inventory_quantity,
ct_avg_daily_onhand_inventory_quantity_last12months,ct_avg_ppv_component_last12months,
snapshot_date,dim_ora_date_snapshotid,amt_exchangerate,amt_exchangerate_gbl,dim_projectsourceid,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
select 
fact_ora_bom_riskmaterialid,
1 as dim_ora_parent_productid,
1 as dim_ora_component_productid,
toplevelparent dd_toplevelparent,
"path" dd_path,
level1 dd_component_level,
dim_ora_vendorid dim_ora_supplierid,
'Not Set' dd_revision_number,
'Y' as dd_current_flag,
quantity as ct_quantity,
'Not Set' dd_abc_classification,
0 as amt_std_unit_cost,
0 as amt_unit_price_paid,
0 as ct_lead_time,
0 as ct_lead_time_variance,
0 as ct_po_count,
0 as amt_po_amount,
0 as ct_po_quantity,
0 as amt_open_po_amount,
0 as ct_open_po_quantity,
0 as ct_valid_info_records,
0 as ct_open_so_count,
0 as ct_pr_count,
0 as amt_requisition_amount,
0 as ct_requisition_quantity,
0 as amt_open_requisition_amount,
0 as ct_open_requisition_quantity,
0 as ct_rejected_quantity,
0 as ct_approved_PO_count_without_pr,
1 as dim_ora_source_list_vendorid,
0 as ct_cancel_msg_count,
0 as ct_cancel_msg_quantity,
0 as ct_pull_in_msg_count,
0 as ct_pull_in_msg_quantity,
0 as ct_push_out_msg_count,
0 as ct_push_out_msg_quantity,
0 as ct_plan_order_count,
0 as ct_plan_order_quantity,
0 as ct_production_order_count,
0 as ct_production_order_quantity,
0 as ct_open_production_order_count,
0 as ct_open_production_order_quantity,
0 as ct_past_due_count,
0 as ct_past_due_quantity,
0 as ct_avg_delivery_performance_current,
0 as ct_avg_delivery_performance_last12months,
0 as ct_onhand_current_inventory_quantity,
0 as ct_avg_daily_onhand_inventory_quantity_last12months,
0 as ct_avg_ppv_component_last12months,
to_date(current_timestamp) snapshot_date,
1 as dim_ora_date_snapshotid,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
1 as dim_projectsourceid,
current_timestamp AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
from ora_bom_riskmaterial;

/*dim_ora_parent_productid*/
UPDATE fact_ora_bom_riskmaterial F
SET 
F.dim_ora_parent_productid = D.DIM_ORA_PRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_bom_riskmaterial F,ora_bom_riskmaterial S,DIM_ORA_PRODUCT D
WHERE F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid AND 
S.parentid = D.KEY_ID and d.rowiscurrent = 1 and
F.dim_ora_parent_productid <> D.DIM_ORA_PRODUCTID;

/*dim_ora_component_productid*/
UPDATE fact_ora_bom_riskmaterial F
SET 
F.dim_ora_component_productid = D.DIM_ORA_PRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_bom_riskmaterial F,ora_bom_riskmaterial S,DIM_ORA_PRODUCT D
WHERE F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid AND 
S.childid = D.KEY_ID and d.rowiscurrent = 1 and
F.dim_ora_component_productid <> D.DIM_ORA_PRODUCTID;
 
/*dim_ora_date_snapshotid*/
UPDATE fact_ora_bom_riskmaterial F
SET 
F.dim_ora_date_snapshotid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
from fact_ora_bom_riskmaterial F,ora_bom_riskmaterial s,DIM_DATE D
WHERE F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
AND to_date(current_timestamp) = D.DATEVALUE
AND to_date(F.SNAPSHOT_DATE) = D.DATEVALUE
AND F.DIM_ORA_DATE_SNAPSHOTID =1;
 
 	
TRUNCATE TABLE TMP_PROP;
INSERT INTO TMP_PROP 
SELECT property_value,property from systemproperty where property ='PROD.MASTER_ORGANIZATION_ID' or property ='BOM.CST_ITEM_COSTS.COST_TYPE_ID';


/*amt_std_unit_cost*/
update fact_ora_bom_riskmaterial f
set f.amt_std_unit_cost = bic.item_cost
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join DIM_ORA_PRODUCT d on s.childid = d.key_id and d.rowiscurrent = 1
join dim_ora_bom_item_costs bic on bic.inventory_item_id = d.key_id and bic.rowiscurrent = 1
join  TMP_PROP org on bic.organization_id = org.property_value and org.property ='PROD.MASTER_ORGANIZATION_ID'
join  TMP_PROP csttyp on bic.cost_type_id = csttyp.property_value and csttyp.property ='BOM.CST_ITEM_COSTS.COST_TYPE_ID'
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid;
 


/*amt_unit_price_paid*/
DROP table IF EXISTS amt_unit_price_paid_tmp;

create table amt_unit_price_paid_tmp as
select 
key_id item_id, po.dim_ora_vendorid, avg(ct_list_price_per_unit) ct_list_price_per_unit
from fact_ora_purchaseorder po 
join dim_ora_product p on po.dim_ora_productid = p.dim_ora_productid  and p.rowiscurrent = 1
join dim_date dlup on po.dim_ora_date_last_updateid = dlup.dim_dateid
where dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by po.dim_ora_vendorid, key_id;

update fact_ora_bom_riskmaterial f
set f.amt_unit_price_paid =  tmp.ct_list_price_per_unit
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join amt_unit_price_paid_tmp tmp on s.childid = tmp.item_id and s.dim_ora_vendorid = tmp.dim_ora_vendorid
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid; 

DROP table IF EXISTS amt_unit_price_paid_tmp;

/*ct_lead_time*/
DROP table IF EXISTS ct_lead_time_tmp;

create table ct_lead_time_tmp as
select pr.dim_ora_productid, key_id item_id,avg(dpol.datevalue-dnb.datevalue) ct_lead_time
from fact_ora_purchasereceipt pr
join dim_date dpol on pr.dim_ora_date_po_lines_last_updateid = dpol.dim_dateid
join dim_date dnb  on pr.dim_ora_date_need_byid 			 = dnb.dim_dateid
join DIM_ORA_PRODUCT d on pr.dim_ora_productid = d.dim_ora_productid
join dim_date dlup on pr.dim_ora_date_last_updateid = dlup.dim_dateid
where dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by pr.dim_ora_productid, key_id,dpol.datevalue,dnb.datevalue;

update fact_ora_bom_riskmaterial f
set f.ct_lead_time = tmp.ct_lead_time
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join ct_lead_time_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_lead_time_tmp;

/*ct_po_count*/
DROP table IF EXISTS ct_po_count_tmp;

create table ct_po_count_tmp as
select 
po.dim_ora_productid, key_id item_id, count(distinct dd_segment1) ct_po_count
from fact_ora_purchaseorder po 
join dim_ora_product p on po.dim_ora_productid = p.dim_ora_productid
join dim_date dlup on po.dim_ora_date_last_updateid = dlup.dim_dateid
where dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by po.dim_ora_productid, key_id;

update fact_ora_bom_riskmaterial f
set f.ct_po_count =  tmp.ct_po_count
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join ct_po_count_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_po_count_tmp;

/*amt_po_amount*/
DROP table IF EXISTS amt_po_amount_tmp;

create table amt_po_amount_tmp as
select 
po.dim_ora_productid, key_id item_id, sum(amt_amount) amt_amount
from fact_ora_purchaseorder po 
join dim_ora_product p on po.dim_ora_productid = p.dim_ora_productid
join dim_date dlup on po.dim_ora_date_last_updateid = dlup.dim_dateid
where dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by po.dim_ora_productid, key_id;

update fact_ora_bom_riskmaterial f
set f.amt_po_amount =  tmp.amt_amount
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join amt_po_amount_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS amt_po_amount_tmp;

/*ct_po_quantity*/
DROP table IF EXISTS ct_po_quantity_tmp;

create table ct_po_quantity_tmp as
select 
po.dim_ora_productid, key_id item_id, sum(ct_quantity) ct_quantity
from fact_ora_purchaseorder po 
join dim_ora_product p on po.dim_ora_productid = p.dim_ora_productid
join dim_date dlup on po.dim_ora_date_last_updateid = dlup.dim_dateid
where dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by po.dim_ora_productid, key_id;

update fact_ora_bom_riskmaterial f
set f.ct_po_quantity =  tmp.ct_quantity
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join ct_po_quantity_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_po_quantity_tmp;

/*amt_open_po_amount*/
DROP table IF EXISTS amt_open_po_amount_tmp;

create table amt_open_po_amount_tmp as
select 
po.dim_ora_productid, p.key_id item_id, sum(amt_amount) amt_amount
from fact_ora_purchaseorder po 
join dim_ora_product p on po.dim_ora_productid = p.dim_ora_productid
join dim_ora_fnd_lookup fas on fas.dim_ora_fnd_lookupid = po.dim_ora_authorization_statusid
join dim_date dlup on po.dim_ora_date_last_updateid = dlup.dim_dateid
where po.dd_line_close_code ='OPEN' and fas.lookup_code ='APPROVED'
and dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by po.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.amt_open_po_amount =  tmp.amt_amount
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join amt_open_po_amount_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS amt_open_po_amount_tmp;

/*ct_open_po_quantity*/
DROP table IF EXISTS ct_open_po_quantity_tmp;

create table ct_open_po_quantity_tmp as
select 
po.dim_ora_productid, p.key_id item_id, sum(ct_quantity) ct_quantity
from fact_ora_purchaseorder po 
join dim_ora_product p on po.dim_ora_productid = p.dim_ora_productid
--join dim_date dlup on po.dim_ora_date_last_updateid = dlup.dim_dateid
join dim_ora_fnd_lookup fas on fas.dim_ora_fnd_lookupid = po.dim_ora_authorization_statusid
join dim_date dlup on po.dim_ora_date_last_updateid = dlup.dim_dateid
and dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
where po.dd_line_close_code ='OPEN' and fas.lookup_code ='APPROVED'
group by po.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_open_po_quantity =  tmp.ct_quantity
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join ct_open_po_quantity_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_open_po_quantity_tmp;

/*ct_open_so_count*/

DROP table IF EXISTS ct_open_so_count_tmp;

CREATE TABLE ct_open_so_count_tmp as
select 
sol.dim_ora_productid, p.key_id item_id, count(distinct dd_order_number) ct_open_so_count
from fact_ora_sales_order_line sol
join dim_ora_product p on sol.dim_ora_productid = p.dim_ora_productid
join dim_ora_fnd_lookup fhs 
on fhs.dim_ora_fnd_lookupid = sol.dim_ora_order_header_statusid
join dim_date dbk on 
sol.dim_ora_date_order_bookedid = dbk.dim_dateid
where not (fhs.lookup_code in ('CLOSED','CANCELLED'))
--linecategorycode  not equal to return

and dbk.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by sol.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_open_so_count =  tmp.ct_open_so_count
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join ct_open_so_count_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_open_so_count_tmp;


/*ct_pr_count*/
DROP table IF EXISTS ct_pr_count_tmp;

create table ct_pr_count_tmp as 
select p.dim_ora_productid, p.key_id item_id, count(distinct dd_segment1) ct_pr_count
from fact_ora_purchaserequisition pr
join dim_ora_invproduct ip on 
ip.dim_ora_invproductid = pr.dim_ora_inv_productid
join dim_ora_product p on
p.key_id = ip.inventory_item_id
join dim_date dlup on pr.dim_ora_date_pr_updateid = dlup.dim_dateid
where dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by p.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_pr_count =  tmp.ct_pr_count
from fact_ora_bom_riskmaterial f,ora_bom_riskmaterial s
join ct_pr_count_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_pr_count_tmp;

/*amt_requisition_amount*/
DROP table IF EXISTS amt_requisition_amount_tmp;

create table amt_requisition_amount_tmp as 
select p.dim_ora_productid, p.key_id item_id, sum(amt_requisition_line) amt_requisition_line
from fact_ora_purchaserequisition pr
join dim_ora_invproduct ip on 
ip.dim_ora_invproductid = pr.dim_ora_inv_productid
join dim_ora_product p on
p.key_id = ip.inventory_item_id
join dim_date dlup on pr.dim_ora_date_pr_updateid = dlup.dim_dateid
where dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by p.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.amt_requisition_amount =  tmp.amt_requisition_line
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join amt_requisition_amount_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS amt_requisition_amount_tmp;

/*ct_requisition_quantity*/
DROP table IF EXISTS ct_requisition_quantity_tmp;

create table ct_requisition_quantity_tmp as 
select p.dim_ora_productid, p.key_id item_id, sum(ct_quantity) ct_quantity
from fact_ora_purchaserequisition pr
join dim_ora_invproduct ip on 
ip.dim_ora_invproductid = pr.dim_ora_inv_productid
join dim_ora_product p on
p.key_id = ip.inventory_item_id
join dim_date dlup on pr.dim_ora_date_pr_updateid = dlup.dim_dateid
where dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by p.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_requisition_quantity =  tmp.ct_quantity
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join ct_requisition_quantity_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_requisition_quantity_tmp;

/*amt_open_requisition_amount*/
DROP table IF EXISTS amt_open_requisition_amount_tmp;

create table amt_open_requisition_amount_tmp as 
select p.dim_ora_productid, p.key_id item_id, sum(amt_requisition_line) amt_requisition_line
from fact_ora_purchaserequisition pr
join dim_ora_invproduct ip on 
ip.dim_ora_invproductid = pr.dim_ora_inv_productid
join dim_ora_product p on
p.key_id = ip.inventory_item_id
join dim_date dlup on pr.dim_ora_date_pr_updateid = dlup.dim_dateid
where pr.dd_authorization_status = 'APPROVED' and not (pr.dd_closed_code like '%CLOSE%')
and dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by p.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.amt_open_requisition_amount =  tmp.amt_requisition_line
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join amt_open_requisition_amount_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS amt_open_requisition_amount_tmp;

/*ct_open_requisition_quantity*/
DROP table IF EXISTS ct_open_requisition_quantity_tmp;

create table ct_open_requisition_quantity_tmp as 
select p.dim_ora_productid, p.key_id item_id, sum(ct_quantity) ct_quantity
from fact_ora_purchaserequisition pr
join dim_ora_invproduct ip on 
ip.dim_ora_invproductid = pr.dim_ora_inv_productid
join dim_ora_product p on
p.key_id = ip.inventory_item_id
--join dim_date dlup on pr.dim_ora_date_pr_updateid = dlup.dim_dateid
where pr.dd_authorization_status = 'APPROVED' and not (pr.dd_closed_code like '%CLOSE%')
--and dlup.datevalue between to_date(current_timestamp)- 365 and to_date(current_timestamp)
group by p.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_open_requisition_quantity =  tmp.ct_quantity
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join ct_open_requisition_quantity_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_open_requisition_quantity_tmp;

/*ct_rejected_quantity*/
DROP table IF EXISTS ct_rejected_quantity_tmp;

create table ct_rejected_quantity_tmp as
select pr.dim_ora_productid, key_id item_id, sum(ct_quantity) ct_rejected_quantity
from fact_ora_purchasereceipt pr
join DIM_ORA_PRODUCT d on pr.dim_ora_productid = d.dim_ora_productid
where pr.dd_transaction_type in ('RETURN TO VENDOR','REJECT')
group by pr.dim_ora_productid, key_id;

update fact_ora_bom_riskmaterial f
set f.ct_rejected_quantity =  tmp.ct_rejected_quantity
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join ct_rejected_quantity_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_rejected_quantity_tmp;
 
/*ct_approved_PO_count_without_pr*/

DROP table IF EXISTS ct_approved_PO_count_without_pr_tmp;

create table ct_approved_PO_count_without_pr_tmp as 
select 
p.dim_ora_productid, p.key_id item_id, SUM(case when poc.dim_ora_line_locationid = 1 then 1 else 0 end) ct_approved_PO_count_without_pr
from fact_ora_purchaseordercost poc 
join dim_ora_invproduct ip on 
ip.dim_ora_invproductid = poc.dim_ora_inv_productid
join dim_ora_product p on ip.inventory_item_id = p.key_id
join dim_ora_fnd_lookup fas on fas.dim_ora_fnd_lookupid = poc.dim_ora_authorization_statusid
where poc.dd_line_close_code ='OPEN' and fas.lookup_code ='APPROVED'
group by p.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_approved_PO_count_without_pr =  tmp.ct_approved_PO_count_without_pr
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join ct_approved_PO_count_without_pr_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_approved_PO_count_without_pr_tmp;

/*ct_past_due_count*/

DROP table IF EXISTS ct_past_due_count_tmp;

create table ct_past_due_count_tmp as
select p.dim_ora_productid, p.key_id item_id, count(distinct poc.dd_segment1) ct_past_due_count
from fact_ora_purchaseordercost poc
join dim_ora_invproduct ip on 
ip.dim_ora_invproductid = poc.dim_ora_inv_productid
join dim_ora_product p on
p.key_id = ip.inventory_item_id
join dim_date nbd 
on poc.dim_ora_date_need_bydateid = nbd.dim_dateid
where nbd.datevalue> to_date(current_timestamp)
group by p.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_past_due_count =  tmp.ct_past_due_count
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join ct_past_due_count_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_past_due_count_tmp;

/*ct_past_due_quantity*/

DROP table IF EXISTS ct_past_due_quantity_tmp;

create table ct_past_due_quantity_tmp as
select p.dim_ora_productid, p.key_id item_id, sum(poc.ct_quantity_ordered) ct_quantity_ordered
from fact_ora_purchaseordercost poc
join dim_ora_invproduct ip on 
ip.dim_ora_invproductid = poc.dim_ora_inv_productid
join dim_ora_product p on
p.key_id = ip.inventory_item_id
join dim_date nbd 
on poc.dim_ora_date_need_bydateid = nbd.dim_dateid
where nbd.datevalue> to_date(current_timestamp)
group by p.dim_ora_productid, p.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_past_due_quantity =  tmp.ct_quantity_ordered
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join ct_past_due_quantity_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_past_due_quantity_tmp;

/*ct_onhand_current_inventory_quantity*/

DROP table IF EXISTS ct_onhand_current_inventory_quantity_tmp;

create table ct_onhand_current_inventory_quantity_tmp as
select ioh.dim_ora_productid, d.key_id item_id,sum(ct_transaction_quantity) ct_transaction_quantity 
from fact_ora_invonhand ioh
join DIM_ORA_PRODUCT d on ioh.dim_ora_productid = d.dim_ora_productid
group by ioh.dim_ora_productid, d.key_id;

update fact_ora_bom_riskmaterial f
set f.ct_onhand_current_inventory_quantity =  tmp.ct_transaction_quantity
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join ct_onhand_current_inventory_quantity_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_onhand_current_inventory_quantity_tmp;

/*ct_avg_daily_onhand_inventory_quantity_last12months*/

DROP table IF EXISTS ct_avg_daily_onhand_inventory_quantity_last12months_tmp2;
DROP table IF EXISTS ct_avg_daily_onhand_inventory_quantity_last12months_tmp;


create table ct_avg_daily_onhand_inventory_quantity_last12months_tmp2 as 
select ioh.dim_ora_productid, d.key_id item_id, to_date(snapshot_date) snapshot_date,  sum(ct_transaction_quantity) ct_transaction_quantity 
from fact_ora_invonhand_daily ioh
join DIM_ORA_PRODUCT d on ioh.dim_ora_productid = d.dim_ora_productid
where to_date(snapshot_date) between to_date(current_timestamp -365) and to_date(current_timestamp)
group by ioh.dim_ora_productid, d.key_id, to_date(snapshot_date);

create table ct_avg_daily_onhand_inventory_quantity_last12months_tmp as
select dim_ora_productid, item_id, avg(ct_transaction_quantity) ct_transaction_quantity 
from ct_avg_daily_onhand_inventory_quantity_last12months_tmp2
group by dim_ora_productid, item_id;

update fact_ora_bom_riskmaterial f
set f.ct_avg_daily_onhand_inventory_quantity_last12months =  tmp.ct_transaction_quantity
from fact_ora_bom_riskmaterial f, ora_bom_riskmaterial s
join ct_avg_daily_onhand_inventory_quantity_last12months_tmp tmp on s.childid = tmp.item_id
where F.fact_ora_bom_riskmaterialid = S.fact_ora_bom_riskmaterialid
and tmp.dim_ora_productid = f.dim_ora_component_productid;

DROP table IF EXISTS ct_avg_daily_onhand_inventory_quantity_last12months_tmp2;
DROP table IF EXISTS ct_avg_daily_onhand_inventory_quantity_last12months_tmp;

/*ct_avg_ppv_component_last12months*/
update fact_ora_bom_riskmaterial
set ct_avg_ppv_component_last12months = ifnull(amt_unit_price_paid,0) - ifnull(amt_std_unit_cost,0);
