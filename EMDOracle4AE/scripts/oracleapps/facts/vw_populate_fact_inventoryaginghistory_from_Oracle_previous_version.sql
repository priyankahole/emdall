DELETE FROM emd586.fact_inventoryhistory WHERE dim_projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


INSERT INTO emd586.fact_inventoryhistory
(
    amt_exchangerate
	,amt_exchangerate_gbl
	,ct_blockedstock
	,ct_stockqty		--,amt_OnHand
    ,amt_StockValueAmt
    ,amt_StockValueAmt_1WeekChange
	,dd_batchno
	,dim_unitofmeasureid
	,dim_storagelocentrydateid
	,dim_plantid
	,dim_partid
	,dim_storagelocationid
	,dim_mdg_partid
	,dim_bwproducthierarchyid
	,dim_companyid
	,dw_insert_date
	,dw_update_date
	,fact_inventoryhistoryid
    ,dim_projectsourceid
    ,ct_StockInQInsp
    ,ct_StockInTransit
    ,ct_StockInTransfer
    ,ct_TotalRestrictedStock
    ,dim_itemcategoryid
    ,dim_stockcategoryid
    ,amt_StdUnitPrice
    ,amt_WIPBalance
    ,ct_WIPQty
    ,dim_dateidsnapshot
    /* from On Hand Amt formula need to have default value*/
	--,amt_StockValueAmt
	,amt_StockInQInspAmt
	,amt_BlockedStockAmt
	,amt_StockInTransitAmt
	,amt_StockInTransferAmt

	,amt_StockInQInspAmt_1WeekChange
	,amt_BlockedStockAmt_1WeekChange
	,amt_StockInTransferAmt_1WeekChange
	,amt_StockInTransitAmt_1WeekChange
	,ct_TotalRestrictedStock_1WeekChange

	,amt_cogsactualrate_emd
	,amt_cogsfixedrate_emd
	,amt_cogsfixedplanrate_emd
	,amt_cogsplanrate_emd
	,amt_cogsprevyearfixedrate_emd
	,amt_cogsprevyearrate_emd
	,amt_cogsprevyearto1_emd
	,amt_cogsprevyearto2_emd
	,amt_cogsprevyearto3_emd
	,amt_cogsturnoverrate1_emd
	,amt_cogsturnoverrate2_emd
	,amt_cogsturnoverrate3_emd

	,dim_bwhierarchycountryid
	,dim_clusterid
	,ct_countmaterialsafetystock
	,amt_cogs_1daychange
        ,amt_cogs_1weekchange
        ,amt_cogs_1monthchange
	,dim_specialstockid
		,dd_isconsigned
		,dd_batch_status_qkz
		,dim_productionschedulerid
		,ct_baseuomratioPCcustom
		,dim_countryhierpsid
		,dim_countryhierarid
      		,amt_OnHand
		,dim_dateidexpirydate
    ,amt_cogsfixedrate_emd_new
    ,amt_cogsfixedplanrate_emd_new
    ,amt_cogsplanrate_emd_new
    ,amt_exchangerate_gbl_bis
    ,amt_cogsfixedplanrate_emd_bis
    ,amt_StdUnitPrice_bis

		)
SELECT
         CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
		,CASE WHEN d_sinv.availability_type = 1 AND SUBSTR(d_sinv.secondary_inventory_name,1,16) not in ('STERISHLD','7DANB','7DAND')
    THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
    WHEN d_sinv.availability_type = 1 AND to_date(snapshot_date)>='2017-05-07' AND SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
    THEN 0
    WHEN d_sinv.availability_type = 1 AND to_date(snapshot_date)<'2017-05-07' AND SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
   THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
    ELSE 0 END ct_primary_transaction_quantity

		,CASE WHEN d_sinv.availability_type = 1 and SUBSTR(d_sinv.secondary_inventory_name,1,16) not in ('STERISHLD','7DANB','7DAND')
then CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		 END
     WHEN d_sinv.availability_type = 1 and SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
     AND to_date(snapshot_date)>='2017-05-07' THEN 0
   WHEN d_sinv.availability_type = 1 and SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
     AND to_date(snapshot_date)<'2017-05-07' THEN
CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		 END
     else 0 end amt_primary_transaction_quantity  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost

        ,amt_primary_transact_qty_1weekchange

		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
		,d_uom.dim_unitofmeasureid
		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid4
		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
		,f_inv.dw_insert_date
		,f_inv.dw_update_date
		,f_inv.fact_ora_invonhand_dailyid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhand_dailyid
		,p.dim_projectsourceid
		,0
		,0
		,0
		,CASE WHEN d_sinv.availability_type = 2 THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
     WHEN to_date(snapshot_date)>='2017-05-07' and SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
     THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
    ELSE 0 END restricted_qty
		,1
		,1
		,dd_standart_cost
		,0
		,0
		,DIM_ORA_DATE_SNAPSHOTID + IFNULL(p.dim_projectsourceid * p.multiplier ,0) DIM_ORA_DATE_SNAPSHOTID
		--,0
		,0
		,0
		,0
		,0

		,0
		,0
		,0
		,0
		,0

		,amt_cogsactualrate_emd
		,amt_cogsfixedrate_emd
		,amt_cogsfixedplanrate_emd
		,amt_cogsplanrate_emd
		,amt_cogsprevyearfixedrate_emd
		,amt_cogsprevyearrate_emd
		,amt_cogsprevyearto1_emd
		,amt_cogsprevyearto2_emd
		,amt_cogsprevyearto3_emd
		,amt_cogsturnoverrate1_emd
		,amt_cogsturnoverrate2_emd
		,amt_cogsturnoverrate3_emd

	    ,dim_ora_bwhierarchycountryid
		,dim_ora_clusterid
		,ct_countmaterialsafetystock
		,amt_cogs_1daychange
                ,amt_cogs_1weekchange
                ,amt_cogs_1monthchange
		,1
				,ifnull(dd_isconsigned,0)
				,'Not Set' dd_batch_status_qkz
				,1 dim_productionschedulerid
				,1 ct_baseuomratioPCcustom
	,dim_countryhierpsid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierpsid
        ,dim_countryhierarid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierarid
		,CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		 END amt_primary_transaction_quantity
	,dim_ora_expiration_dateid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_dateidexpirydate
  ,amt_cogsfixedrate_emd_new
  ,amt_cogsfixedplanrate_emd_new
  ,amt_cogsplanrate_emd_new
  ,amt_exchangerate_gbl_bis
  ,amt_cogsfixedplanrate_emd_bis
  ,amt_StdUnitPrice_bis

 FROM emdoracle4ae.fact_ora_invonhand_daily f_inv
     ,emdoracle4ae.dim_projectsource p
     ,emd586.dim_unitofmeasure d_uom
     ,emdoracle4ae.dim_ora_mtl_sec_inventory d_sinv
WHERE IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = d_uom.uom
 AND d_uom.projectsourceid = p.dim_projectsourceid
 AND f_inv.dim_ora_mtl_sec_inventoryid = d_sinv.dim_ora_mtl_sec_inventoryid
 AND d_sinv.asset_inventory = 1
 AND ct_primary_transaction_quantity <> 999999999999999868928;

 update emd586.fact_inventoryhistory f
set f.amt_exchangerate_gbl= ff.amt_exchangerate_gbl_bis
FROM emd586.fact_inventoryhistory f
      ,emdoracle4ae.fact_ora_invonhand_daily ff
     ,emdoracle4ae.dim_projectsource p

WHERE f.fact_inventoryhistoryid=ff.fact_ora_invonhand_dailyid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
and f.dim_projectsourceid=7
and to_date(ff.snapshot_date)='2016-12-31'
and f.amt_exchangerate_gbl<>ff.amt_exchangerate_gbl_bis;
