/*set session authorization oracle_data*/

/*DELETE FROM hlp_ora_cst_standards_costs*/
/*SELECT * FROM hlp_ora_cst_standards_costs*/

delete from NUMBER_FOUNTAIN_hlp_ora_cst_standards_costs where table_name = 'hlp_ora_cst_standards_costs';

INSERT INTO NUMBER_FOUNTAIN_hlp_ora_cst_standards_costs
SELECT 'hlp_ora_cst_standards_costs',IFNULL(MAX(hlp_ora_cst_standards_costsid),0)
FROM hlp_ora_cst_standards_costs;

/*update rows*/
UPDATE hlp_ora_cst_standards_costs T
SET
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.STANDARD_COST_REVISION_DATE = ifnull(S.STANDARD_COST_REVISION_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.STANDARD_COST = ifnull(S.STANDARD_COST, 0),
T.INVENTORY_ADJUSTMENT_QUANTITY = ifnull(S.INVENTORY_ADJUSTMENT_QUANTITY, 0),
T.INVENTORY_ADJUSTMENT_VALUE = ifnull(S.INVENTORY_ADJUSTMENT_VALUE, 0),
T.INTRANSIT_ADJUSTMENT_QUANTITY = ifnull(S.INTRANSIT_ADJUSTMENT_QUANTITY, 0),
T.INTRANSIT_ADJUSTMENT_VALUE = ifnull(S.INTRANSIT_ADJUSTMENT_VALUE, 0),
T.WIP_ADJUSTMENT_QUANTITY = ifnull(S.WIP_ADJUSTMENT_QUANTITY, 0),
T.WIP_ADJUSTMENT_VALUE = ifnull(S.WIP_ADJUSTMENT_VALUE, 0),
T.LAST_COST_UPDATE_ID = ifnull(S.LAST_COST_UPDATE_ID, 0),
T.REQUEST_ID = ifnull(S.REQUEST_ID, 0),
T.PROGRAM_APPLICATION_ID = ifnull(S.PROGRAM_APPLICATION_ID, 0),
T.PROGRAM_ID = ifnull(S.PROGRAM_ID, 0),
T.PROGRAM_UPDATE_DATE = ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp

FROM hlp_ora_cst_standards_costs T,ora_cst_standard_costs S
WHERE
T.COST_UPDATE_ID = S.COST_UPDATE_ID AND T.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND T.ORGANIZATION_ID = S.ORGANIZATION_ID;

/*insert new rows*/
INSERT INTO hlp_ora_cst_standards_costs
(hlp_ora_cst_standards_costsid,
COST_UPDATE_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,
STANDARD_COST_REVISION_DATE,STANDARD_COST,INVENTORY_ADJUSTMENT_QUANTITY,INVENTORY_ADJUSTMENT_VALUE,INTRANSIT_ADJUSTMENT_QUANTITY,INTRANSIT_ADJUSTMENT_VALUE,
WIP_ADJUSTMENT_QUANTITY,WIP_ADJUSTMENT_VALUE,LAST_COST_UPDATE_ID,REQUEST_ID,PROGRAM_APPLICATION_ID,PROGRAM_ID,PROGRAM_UPDATE_DATE,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_hlp_ora_cst_standards_costs WHERE TABLE_NAME = 'hlp_ora_cst_standards_costs' ),0) + ROW_NUMBER() OVER(order by '') hlp_ora_cst_standards_costsid,
S.COST_UPDATE_ID,
S.INVENTORY_ITEM_ID,
S.ORGANIZATION_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.STANDARD_COST_REVISION_DATE, timestamp '1970-01-01 00:00:00.000000') STANDARD_COST_REVISION_DATE,
ifnull(S.STANDARD_COST, 0) STANDARD_COST,
ifnull(S.INVENTORY_ADJUSTMENT_QUANTITY, 0) INVENTORY_ADJUSTMENT_QUANTITY,
ifnull(S.INVENTORY_ADJUSTMENT_VALUE, 0) INVENTORY_ADJUSTMENT_VALUE,
ifnull(S.INTRANSIT_ADJUSTMENT_QUANTITY, 0) INTRANSIT_ADJUSTMENT_QUANTITY,
ifnull(S.INTRANSIT_ADJUSTMENT_VALUE, 0) INTRANSIT_ADJUSTMENT_VALUE,
ifnull(S.WIP_ADJUSTMENT_QUANTITY, 0) WIP_ADJUSTMENT_QUANTITY,
ifnull(S.WIP_ADJUSTMENT_VALUE, 0) WIP_ADJUSTMENT_VALUE,
ifnull(S.LAST_COST_UPDATE_ID, 0) LAST_COST_UPDATE_ID,
ifnull(S.REQUEST_ID, 0) REQUEST_ID,
ifnull(S.PROGRAM_APPLICATION_ID, 0) PROGRAM_APPLICATION_ID,
ifnull(S.PROGRAM_ID, 0) PROGRAM_ID,
ifnull(S.PROGRAM_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') PROGRAM_UPDATE_DATE,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' SOURCE_ID
FROM ora_cst_standard_costs S LEFT JOIN hlp_ora_cst_standards_costs F
ON F.COST_UPDATE_ID = S.COST_UPDATE_ID AND F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID
WHERE
F.COST_UPDATE_ID is null;
