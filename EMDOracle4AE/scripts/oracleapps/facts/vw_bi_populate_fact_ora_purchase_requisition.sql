/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_purchaserequisition*/
/*SELECT * FROM fact_ora_purchaserequisition*/

delete from NUMBER_FOUNTAIN_fact_ora_purchaserequisition where table_name = 'fact_ora_purchaserequisition';

INSERT INTO NUMBER_FOUNTAIN_fact_ora_purchaserequisition
SELECT 'fact_ora_purchaserequisition',IFNULL(MAX(fact_ora_purchaserequisitionid),0)
FROM fact_ora_purchaserequisition;

/*update fact columns*/
UPDATE fact_ora_purchaserequisition F
SET
dd_authorization_status = ifnull(S.AUTHORIZATION_STATUS,'Not Set'),
dd_closed_code = ifnull(S.CLOSED_CODE,'Not Set'),
dd_type_lookup_code = ifnull(S.TYPE_LOOKUP_CODE,'Not Set'),
ct_quantity = ifnull(S.QUANTITY,0),
ct_unit_price = ifnull(S.UNIT_PRICE,0),
dd_unit_measure_lookup_code = ifnull(S.UNIT_MEAS_LOOKUP_CODE,'Not Set'),
dd_currency_code = ifnull(S.PR_DOC_CURR,'Not Set'),
ct_rate = ifnull(S.RATE,1),
dd_segment1 = S.SEGMENT1,
dd_line_num = S.LINE_NUM,
dd_item_description = S.ITEM_DESCRIPTION,
requisition_line_id = S.INTEGRATION_ID,
dd_cancel_flag = ifnull(S.CANCEL_FLAG,'Not Set'),
dd_modified_by_agent_flag = ifnull(S.MODIFIED_BY_AGENT_FLAG,'Not Set'),
dd_matching_basis = S.MATCHING_BASIS,
amt_requisition_line = ifnull(S.AMOUNT,0),
dd_on_rfq_flag = ifnull(S.ON_RFQ_FLAG,'Not Set'),
dd_auction_title = ifnull(S.AUCTION_TITLE,'Not Set'),
dd_reqs_in_pool_flag = ifnull(S.REQS_IN_POOL_FLAG,'Not Set'),
dd_at_sourcing_flag = ifnull(S.AT_SOURCING_FLAG,'Not Set'),
dd_urgent_flag = ifnull(S.URGENT_FLAG,'Not Set'),
dd_approved_flag = ifnull(S.APPROVED_FLAG,'Not Set'),
dd_receipt_required_flag = ifnull(S.RECEIPT_REQUIRED_FLAG,'Not Set'),
dd_po_line_matching_basis = ifnull(S.MATCHING_BASIS_PO,'Not Set'),
dd_inspection_required_flag = ifnull(S.INSPECTION_REQUIRED_FLAG,'Not Set'),
dd_consigned_flag = ifnull(S.CONSIGNED_FLAG,'Not Set'),
dd_vmi_flag = ifnull(S.VMI_FLAG,'Not Set'),
dd_progress_payment_flag = ifnull(S.PROGRESS_PAYMENT_FLAG,'Not Set'),
ct_cancelled_quantity = ifnull(S.QUANTITY_CANCELLED,0),
dd_po_creation_method_id = ifnull(S.PO_CREATION_METHOD_ID,'Not Set'),
dd_po_revisions = ifnull(S.PO_REVISIONS,0),
dd_rate_type = ifnull(S.RATE_TYPE,'Not Set'),
dd_line_location_id    = ifnull(LINE_LOCATION_ID, 0),
dd_line_location_id_po = ifnull(LINE_LOCATION_ID_PO, 0),
dd_document_type_code = ifnull(S.DOCUMENT_TYPE_CODE,'Not Set'),
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S
WHERE
F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID;

/*insert new rows*/
INSERT INTO fact_ora_purchaserequisition
(
fact_ora_purchaserequisitionid,dim_ora_to_personid,dim_ora_vendorsiteid,dim_ora_inv_productid,
dd_authorization_status,dd_closed_code,dd_type_lookup_code,dim_ora_deliver_to_locationid,dim_ora_orgid,
dim_ora_created_byid,dim_ora_updated_byid,dim_ora_date_line_location_createdid,dim_ora_date_need_byid,dim_ora_date_pr_line_creationid,
dim_ora_date_pr_line_updateid,ct_quantity,ct_unit_price,dd_unit_measure_lookup_code,dd_currency_code,ct_rate,dd_segment1,
dd_line_num,dd_item_description,requisition_line_id,dim_ora_destination_orgid,dim_ora_date_pr_approvalid,dim_ora_vendorid,dd_cancel_flag,
dim_ora_date_pr_updateid,dim_ora_date_line_location_updateid,dim_ora_line_typeid,dim_ora_categoryid,dd_modified_by_agent_flag,dd_matching_basis,
amt_requisition_line,dd_on_rfq_flag,dd_auction_title,dd_reqs_in_pool_flag,dd_at_sourcing_flag,dd_urgent_flag,
dd_line_location_id,dd_line_location_id_po,
dd_approved_flag,dd_receipt_required_flag,dd_po_line_matching_basis,dd_inspection_required_flag,
dim_ora_date_shipment_closedid,dim_ora_date_closed_for_receivingid,dim_ora_date_closed_for_invoiceid,dd_consigned_flag,
dd_vmi_flag,dd_progress_payment_flag,ct_cancelled_quantity,dim_ora_date_po_approved_onid,dim_ora_date_po_submit_onid,dd_po_creation_method_id,
dd_po_revisions,dd_rate_type,dd_document_type_code,
dd_functional_curr_code,amt_exchangerate,amt_exchangerate_gbl,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_fact_ora_purchaserequisition WHERE TABLE_NAME = 'fact_ora_purchaserequisition' ),0) + ROW_NUMBER() OVER(order by '') fact_ora_purchaserequisitionid,
1 AS dim_ora_to_personid,
1 AS dim_ora_vendorsiteid,
1 AS dim_ora_inv_productid,
ifnull(S.AUTHORIZATION_STATUS,'Not Set') AS dd_authorization_status,
ifnull(S.CLOSED_CODE,'Not Set') AS dd_closed_code,
ifnull(S.TYPE_LOOKUP_CODE,'Not Set') AS dd_type_lookup_code,
1 AS dim_ora_deliver_to_locationid,
1 AS dim_ora_orgid,
1 AS dim_ora_created_byid,
1 AS dim_ora_updated_byid,
1 AS dim_ora_date_line_location_createdid,
1 AS dim_ora_date_need_byid,
1 AS dim_ora_date_pr_line_creationid,
1 AS dim_ora_date_pr_line_updateid,
ifnull(S.QUANTITY,0) AS ct_quantity,
ifnull(S.UNIT_PRICE,0) AS ct_unit_price,
ifnull(S.UNIT_MEAS_LOOKUP_CODE,'Not Set') AS dd_unit_measure_lookup_code,
ifnull(S.PR_DOC_CURR,'Not Set') AS dd_currency_code,
ifnull(S.RATE,1) AS ct_rate,
S.SEGMENT1 AS dd_segment1,
S.LINE_NUM AS dd_line_num,
S.ITEM_DESCRIPTION AS dd_item_description,
S.INTEGRATION_ID AS requisition_line_id,
1 AS dim_ora_destination_orgid,
1 AS dim_ora_date_pr_approvalid,
1 AS dim_ora_vendorid,
ifnull(S.CANCEL_FLAG,'Not Set') AS dd_cancel_flag,
1 AS dim_ora_date_pr_updateid,
1 AS dim_ora_date_line_location_updateid,
1 AS dim_ora_line_typeid,
1 AS dim_ora_categoryid,
ifnull(S.MODIFIED_BY_AGENT_FLAG,'Not Set') AS dd_modified_by_agent_flag,
S.MATCHING_BASIS AS dd_matching_basis,
ifnull(S.AMOUNT,0) AS amt_requisition_line,
ifnull(S.ON_RFQ_FLAG,'Not Set') AS dd_on_rfq_flag,
ifnull(S.AUCTION_TITLE,'Not Set') AS dd_auction_title,
ifnull(S.REQS_IN_POOL_FLAG,'Not Set') AS dd_reqs_in_pool_flag,
ifnull(S.AT_SOURCING_FLAG,'Not Set') AS dd_at_sourcing_flag,
ifnull(S.URGENT_FLAG,'Not Set') AS dd_urgent_flag,
ifnull(S.line_location_id, 0) as dd_line_location_id,
ifnull(S.line_location_id_po, 0) as dd_line_location_id_po,
ifnull(S.APPROVED_FLAG,'Not Set') AS dd_approved_flag,
ifnull(S.RECEIPT_REQUIRED_FLAG,'Not Set') AS dd_receipt_required_flag,
ifnull(S.MATCHING_BASIS_PO,'Not Set') AS dd_po_line_matching_basis,
ifnull(S.INSPECTION_REQUIRED_FLAG,'Not Set') AS dd_inspection_required_flag,
1 AS dim_ora_date_shipment_closedid,
1 AS dim_ora_date_closed_for_receivingid,
1 AS dim_ora_date_closed_for_invoiceid,
ifnull(S.CONSIGNED_FLAG,'Not Set') AS dd_consigned_flag,
ifnull(S.VMI_FLAG,'Not Set') AS dd_vmi_flag,
ifnull(S.PROGRESS_PAYMENT_FLAG,'Not Set') AS dd_progress_payment_flag,
ifnull(S.QUANTITY_CANCELLED,0) AS ct_cancelled_quantity,
1 AS dim_ora_date_po_approved_onid,
1 AS dim_ora_date_po_submit_onid,
ifnull(S.PO_CREATION_METHOD_ID,'Not Set') AS dd_po_creation_method_id,
ifnull(S.PO_REVISIONS,0) AS dd_po_revisions,
ifnull(S.RATE_TYPE,'Not Set') AS dd_rate_type,
ifnull(S.DOCUMENT_TYPE_CODE,'Not Set') AS dd_document_type_code,
'Not Set' AS DD_FUNCTIONAL_CURR_CODE,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ORA_PURCH_REQ_LINE S LEFT JOIN fact_ora_purchaserequisition F
ON F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID
WHERE F.REQUISITION_LINE_ID is null;

/*UPDATE dim_ora_to_personid*/
TRUNCATE TABLE tmp_fact_ora_purchaserequisition_to_personid;
INSERT INTO tmp_fact_ora_purchaserequisition_to_personid
SELECT
f.fact_ora_purchaserequisitionid,
D.DIM_ORA_HR_EMPLOYEESID,
ROW_NUMBER() OVER (partition by f.fact_ora_purchaserequisitionid ORDER BY f.rowstartdate) AS rn
FROM fact_ora_purchaserequisition F,
	 ORA_PURCH_REQ_LINE S JOIN DIM_ORA_HR_EMPLOYEES D ON S.TO_PERSON_ID = D.PERSON_ID and S.CREATION_DATE BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID
  AND D.global_name<>'Not Set';

UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_to_personid = tmp.DIM_ORA_HR_EMPLOYEESID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition  F
INNER JOIN tmp_fact_ora_purchaserequisition_to_personid tmp ON (f.fact_ora_purchaserequisitionid = tmp.fact_ora_purchaserequisitionid and tmp.rn=1)
WHERE
F.dim_ora_to_personid <> tmp.DIM_ORA_HR_EMPLOYEESID;

/*UPDATE dim_ora_vendorsiteid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_vendorsiteid = D.DIM_ORA_PO_VENDORSITESID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_PO_VENDORSITES D ON S.VENDOR_SITE_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_vendorsiteid <> D.DIM_ORA_PO_VENDORSITESID;

/*UPDATE dim_ora_inv_productid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_inv_productid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_INVPRODUCT D ON S.ITEM_ID = D.INVENTORY_ITEM_ID  AND S.ORG_ID = D.ORGANIZATION_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_inv_productid <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE dim_ora_deliver_to_locationid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_deliver_to_locationid = D.DIM_ORA_HR_LOCATIONSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_HR_LOCATIONS D ON S.DELIVER_TO_LOCATION_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_deliver_to_locationid <> D.DIM_ORA_HR_LOCATIONSID;

/*UPDATE dim_ora_orgid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_orgid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_BUSINESS_ORG D ON 'HR_BG' ||'~'|| CONVERT(VARCHAR(200),S.ORG_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_orgid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_updated_byid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_line_location_createdid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_line_location_createdid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.CREATION_DATE1) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_line_location_createdid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_need_byid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_need_byid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.NEED_BY_DATE) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_need_byid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_pr_line_creationid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_pr_line_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.CREATION_DATE) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_pr_line_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_pr_line_updateid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_pr_line_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.LAST_UPDATE_DATE) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_pr_line_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_destination_orgid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_destination_orgid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_BUSINESS_ORG D ON 'INV' ||'~'|| CONVERT(VARCHAR(200),S.DESTINATION_ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_destination_orgid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_date_pr_approvalid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_pr_approvalid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.PR_APPROVAL_DATE) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_pr_approvalid <> D.DIM_DATEID;

/*UPDATE dim_ora_vendorid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_vendorid = D.DIM_ORA_PO_VENDORSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_PO_VENDORS D ON S.VENDOR_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_vendorid <> D.DIM_ORA_PO_VENDORSID;

/*UPDATE dim_ora_date_pr_updateid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_pr_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.AUX1_CHANGED_ON_DT) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_pr_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_line_location_updateid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_line_location_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.AUX2_CHANGED_ON_DT) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_line_location_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_line_typeid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_line_typeid = D.DIM_ORA_PO_LINETYPESID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_PO_LINETYPES D ON S.LINE_TYPE_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_line_typeid <> D.DIM_ORA_PO_LINETYPESID;

/*UPDATE dim_ora_categoryid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_categoryid = D.DIM_ORA_PO_CATEGORIESID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_ORA_PO_CATEGORIES D ON S.CATEGORY_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_categoryid <> D.DIM_ORA_PO_CATEGORIESID;

/*UPDATE dim_ora_date_shipment_closedid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_shipment_closedid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.SHIPMENT_CLOSED_DATE) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_shipment_closedid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_closed_for_receivingid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_closed_for_receivingid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.CLOSED_FOR_RECEIVING_DATE) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_closed_for_receivingid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_closed_for_invoiceid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_closed_for_invoiceid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.CLOSED_FOR_INVOICE_DATE) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_closed_for_invoiceid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_po_approved_onid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_po_approved_onid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.PO_APPROVED_ON_DT) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_po_approved_onid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_po_submit_onid*/
UPDATE fact_ora_purchaserequisition F
SET
F.dim_ora_date_po_submit_onid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN DIM_DATE D ON to_date(S.PO_SUBMIT_ON_DT) = D.DATEVALUE
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dim_ora_date_po_submit_onid <> D.DIM_DATEID;

/*UPDATE dd_functional_curr_code*/
UPDATE fact_ora_purchaserequisition F
SET F.DD_FUNCTIONAL_CURR_CODE = DSB.CURRENCY_CODE,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S JOIN dim_ora_hroperatingunit D ON S.ORG_ID = D.KEY_ID and d.rowiscurrent = 1
JOIN dim_ora_gl_setofbooks DSB ON DSB.KEY_ID = D.SET_OF_BOOKS_ID and dsb.rowiscurrent = 1
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.dd_functional_curr_code <> DSB.CURRENCY_CODE;

/*UPDATE amt_exchangerate*/
UPDATE fact_ora_purchaserequisition F
SET
F.amt_exchangerate =  ifnull(S.RATE,1),
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.amt_exchangerate <> ifnull(S.RATE,1);

/*UPDATE amt_exchangerate_gbl*/
UPDATE fact_ora_purchaserequisition F
SET F.AMT_EXCHANGERATE_GBL = ifnull(GLR.CONVERSION_RATE,1),
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_purchaserequisition F,ORA_PURCH_REQ_LINE S
JOIN hlp_ORA_GL_DAILY_RATES GLR ON  to_date(GLR.CONVERSION_DATE) = to_date(S.CREATION_DATE)  AND GLR.FROM_CURRENCY  = S.pr_doc_curr and glr.rowiscurrent = 1
JOIN systemproperty glcurr ON GLR.TO_CURRENCY = glcurr.property_value AND glcurr.property = 'customer.global.currency'
JOIN systemproperty contyp ON GLR.CONVERSION_TYPE = contyp.property_value AND contyp.property  = 'GL.conversion_type'
WHERE F.REQUISITION_LINE_ID = S.REQUISITION_LINE_ID AND
F.AMT_EXCHANGERATE_GBL <>  ifnull(GLR.CONVERSION_RATE,1);
  /*update dim_ora_mdg_productid*/
UPDATE fact_ora_purchaserequisition f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM fact_ora_purchaserequisition f_inv,dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.DIM_ORA_INV_PRODUCTID = d_invp.dim_ora_invproductid
 AND  d_invp.mdm_globalid = d_mp.partnumber
 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;

 /*update dim_ora_bwproducthierarchyid*/
 UPDATE fact_ora_purchaserequisition f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_purchaserequisition f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.DIM_ORA_INV_PRODUCTID = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND dp.productgroupsbu = bw.productgroup
AND current_date BETWEEN bw.upperhierstartdate AND bw.upperhierenddate
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE fact_ora_purchaserequisition f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_purchaserequisition f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.DIM_ORA_INV_PRODUCTID = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;
