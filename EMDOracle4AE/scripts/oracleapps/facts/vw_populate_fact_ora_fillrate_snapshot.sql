DELETE FROM fact_ora_fillrate_snapshot WHERE TO_DATE(snapshot_date) = TO_DATE(CURRENT_DATE) - 1 and fact_ora_sales_order_lineid not in (select distinct fact_ora_sales_order_lineid
from fact_ora_sales_order_line);


DELETE FROM NUMBER_FOUNTAIN_fact_ora_fillrate_snapshot WHERE table_name = 'fact_ora_fillrate_snapshot';

INSERT INTO NUMBER_FOUNTAIN_fact_ora_fillrate_snapshot
SELECT
    'fact_ora_fillrate_snapshot'
	 ,IFNULL(MAX(fact_ora_fillrate_snapshotid),0)
  FROM fact_ora_fillrate_snapshot;

  INSERT INTO fact_ora_fillrate_snapshot
(
	 fact_ora_fillrate_snapshotid
	,fact_ora_sales_order_lineid
	,dim_ora_account_repid
	,dim_ora_business_orgid
	,dim_ora_channel_typeid
	,dim_ora_company_orgid
	,dim_ora_cost_centerid
	,dim_ora_customer_bill_to_locationid
	,dim_ora_customer_ship_to_locationid
	,dim_ora_customer_sold_to_locationid
	,dim_ora_customer_bill_toid
	,dim_ora_customer_ship_toid
	,dim_ora_customer_sold_toid
	,dim_ora_customer_account_bill_toid
	,dim_ora_customer_account_ship_toid
	,dim_ora_customer_account_sold_toid
	,dim_ora_inventory_orgid
	,dim_ora_inv_productid
	,dim_ora_order_line_statusid
	,dim_ora_payment_methodid
	,dim_ora_payment_termid
	,dim_ora_productid
	,dim_ora_sales_groupid
	,dim_ora_sales_office_locationid
	,dim_ora_salesrepid
	,dim_ora_servicerepid
	,dim_ora_order_line_typeid
	,dim_ora_order_header_typeid
	,dim_ora_order_header_statusid
	,dim_ora_freight_termid
	,dim_ora_shipment_methodid
	,dim_ora_date_order_bookedid
	,dim_ora_otd_dateid
	,dim_ora_date_enteredid
	,dim_ora_date_orderedid
	,dim_ora_date_promise_deliveryid
	,dim_ora_date_scheduled_shipid
	,dim_ora_date_actual_shipid
	,dim_ora_date_earliest_shipmentid
	,dim_ora_date_order_confirmedid
	,dim_ora_date_actual_fulfillmentid
	,dim_ora_date_fulfillmentid
	,dim_ora_date_actual_arrival_dateid
	,dim_ora_date_schedule_arrivalid
	,dim_ora_date_taxid
	,dim_ora_date_pricingid
	,dim_ora_gl_set_of_booksid
	,dim_ora_hr_operatingunitid
	,dim_ora_aging_days_bucketid
	,ct_aging_days
	,ct_product_unit_cost
	,ct_product_unit_list_price
	,ct_product_unit_selling_price
	,ct_manufacturing_lead_time
	,ct_ship_tolerance_above
	,ct_ship_tolerance_below
	,ct_delivery_lead_time
	,ct_ordered_quantity
	,ct_cancelled_quantity
	,ct_sales_quantity
	,ct_fulfilled_quantity
	,ct_confirmed_quantity
	,ct_fill_quantity
	,ct_fill_quantity_crd
	,ct_total_shipped_quantity
	,ct_total_invoiced_quantity
	,amt_net_ordered
	,amt_tax
	,amt_cost
	,amt_list
	,amt_discount
	,dd_line_category
	,dd_order_quantity_uom
	,dd_order_number
	,dd_line_number
	,dd_shipment_number
	,dd_orig_ref_doc_number
	,dd_cust_po_number
	,dd_otif_flag
	,dd_cancelled_h_flag
	,dd_cancelled_l_flag
	,dd_open_flag
	,dd_fulfilled_flag
	,dd_shippable_flag
	,dd_tax_exempt_flag
	,dd_booked_flag
	,dd_model_remnant_flag
	,dd_on_hold_flag
	,dd_financial_backlog_flag
	,dd_operation_backlog_flag
	,dd_hold_name
	,dd_header_id
	,dd_line_id
	,dd_transactional_curr_code
	,dd_functional_curr_code
	,dd_shipped_not_invoiced_flag
	,ct_so_touch_count
	,dd_line_flow_status_code
	,dd_shipping_interfaced_flag
	,amt_exchangerate
	,amt_exchangerate_gbl
	,dim_ora_created_byid
	,dim_ora_last_updated_byid
	,dim_ora_date_creationid
	,dim_ora_date_last_updateid
	,dim_ora_mtl_plannersid
	,dim_projectsourceid
	,dw_insert_date
	,dw_update_date
	,rowstartdate
	,rowenddate
	,rowiscurrent
	,rowchangereason
	,source_id
	,ct_pushout_pullin
	,ct_order_change
	,amt_goal
	,amt_revenuedollarsbilling
	,ct_quantitybilling
	,dd_crdflag
	,dd_csdflag
	,dd_hold_date
	,dd_rowiscurrent
	,dd_subinventory
	,dim_ora_mdg_productid
	,dim_ora_bwproducthierarchyid
	,dim_ora_dropship_addressid
	,dim_ora_otd_promise_dateid
	,dd_headertype
	,dd_attribute11
	,dd_context
	,dim_ora_plantid
	,dim_backorder_dateid
	,dd_backorder_responsibility
	,dd_backorder_resp_geography
	,dd_backorder_status
	,dd_backorderreason
	,dd_backorder
	,dd_lsintercompflag
	,dd_intercompflag
	,dd_tradsales_flag
	,dd_backord_reason_lookupt
	,dim_ora_clusterid
	,dim_ora_commercialviewid
	,dd_coveredbymaterial
	,dd_coveredbyplant
	,dd_lineno_compno
	,dd_ace_openorders
	,dd_deliveryontime
	,dd_deliveryisfull
	,ct_deliveryisfull
	,ct_deliveryontime
	,snapshot_date
	,dim_ora_date_snapshotid
	,ct_deliveryontimecasual
    ,dd_deliveryontimecasual
	,dim_otd_dateid_dimD
	,dim_ora_otd_promise_dateid_dimD
	,ct_countsalesdocitem
	,dim_ora_date_customer_requestedid_dimD
	,dim_ora_date_customer_requestedid
	,dim_countryhierpsid
	,dim_countryhierarid
	,dd_gsa_code
	,dim_ora_date_enddateloadingid
	,dim_ora_gsa_codeid
	,dd_inco1
	,dd_inco2
	,dd_mtomts
	,ct_LineReserved_qty
	,dd_delivery_group
  ,dd_single_lot
)

SELECT
	(SELECT max_id FROM NUMBER_FOUNTAIN_fact_ora_fillrate_snapshot WHERE table_name = 'fact_ora_fillrate_snapshot' ) + ROW_NUMBER() OVER (order by '') fact_ora_fillrate_snapshotid
	,f.fact_ora_sales_order_lineid
	,f.dim_ora_account_repid
	,f.dim_ora_business_orgid
	,f.dim_ora_channel_typeid
	,f.dim_ora_company_orgid
	,f.dim_ora_cost_centerid
	,f.dim_ora_customer_bill_to_locationid
	,f.dim_ora_customer_ship_to_locationid
	,f.dim_ora_customer_sold_to_locationid
	,f.dim_ora_customer_bill_toid
	,f.dim_ora_customer_ship_toid
	,f.dim_ora_customer_sold_toid
	,f.dim_ora_customer_account_bill_toid
	,f.dim_ora_customer_account_ship_toid
	,f.dim_ora_customer_account_sold_toid
	,f.dim_ora_inventory_orgid
	,f.dim_ora_inv_productid
	,f.dim_ora_order_line_statusid
	,f.dim_ora_payment_methodid
	,f.dim_ora_payment_termid
	,f.dim_ora_productid
	,f.dim_ora_sales_groupid
	,f.dim_ora_sales_office_locationid
	,f.dim_ora_salesrepid
	,f.dim_ora_servicerepid
	,f.dim_ora_order_line_typeid
	,f.dim_ora_order_header_typeid
	,f.dim_ora_order_header_statusid
	,f.dim_ora_freight_termid
	,f.dim_ora_shipment_methodid
	,f.dim_ora_date_order_bookedid
	,f.dim_ora_otd_dateid
	,f.dim_ora_date_enteredid
	,f.dim_ora_date_orderedid
	,f.dim_ora_date_promise_deliveryid
	,f.dim_ora_date_scheduled_shipid
	,f.dim_ora_date_actual_shipid
	,f.dim_ora_date_earliest_shipmentid
	,f.dim_ora_date_order_confirmedid
	,f.dim_ora_date_actual_fulfillmentid
	,f.dim_ora_date_fulfillmentid
	,f.dim_ora_date_actual_arrival_dateid
	,f.dim_ora_date_schedule_arrivalid
	,f.dim_ora_date_taxid
	,f.dim_ora_date_pricingid
	,f.dim_ora_gl_set_of_booksid
	,f.dim_ora_hr_operatingunitid
	,f.dim_ora_aging_days_bucketid
	,f.ct_aging_days
	,f.ct_product_unit_cost
	,f.ct_product_unit_list_price
	,f.ct_product_unit_selling_price
	,f.ct_manufacturing_lead_time
	,f.ct_ship_tolerance_above
	,f.ct_ship_tolerance_below
	,f.ct_delivery_lead_time
	,f.ct_ordered_quantity
	,f.ct_cancelled_quantity
	,f.ct_sales_quantity
	,f.ct_fulfilled_quantity
	,f.ct_confirmed_quantity
	,f.ct_fill_quantity
	,f.ct_fill_quantity_crd
	,f.ct_total_shipped_quantity
	,f.ct_total_invoiced_quantity
	,f.amt_net_ordered
	,f.amt_tax
	,f.amt_cost
	,f.amt_list
	,f.amt_discount
	,f.dd_line_category
	,f.dd_order_quantity_uom
	,f.dd_order_number
	,f.dd_line_number
	,f.dd_shipment_number
	,f.dd_orig_ref_doc_number
	,f.dd_cust_po_number
	,f.dd_otif_flag
	,f.dd_cancelled_h_flag
	,f.dd_cancelled_l_flag
	,f.dd_open_flag
	,f.dd_fulfilled_flag
	,f.dd_shippable_flag
	,f.dd_tax_exempt_flag
	,f.dd_booked_flag
	,f.dd_model_remnant_flag
	,f.dd_on_hold_flag
	,f.dd_financial_backlog_flag
	,f.dd_operation_backlog_flag
	,f.dd_hold_name
	,f.dd_header_id
	,f.dd_line_id
	,f.dd_transactional_curr_code
	,f.dd_functional_curr_code
	,f.dd_shipped_not_invoiced_flag
	,f.ct_so_touch_count
	,f.dd_line_flow_status_code
	,f.dd_shipping_interfaced_flag
	,f.amt_exchangerate
	,f.amt_exchangerate_gbl
	,f.dim_ora_created_byid
	,f.dim_ora_last_updated_byid
	,f.dim_ora_date_creationid
	,f.dim_ora_date_last_updateid
	,f.dim_ora_mtl_plannersid
	,f.dim_projectsourceid
	,f.dw_insert_date
	,f.dw_update_date
	,f.rowstartdate
	,f.rowenddate
	,f.rowiscurrent
	,f.rowchangereason
	,f.source_id
	,f.ct_pushout_pullin
	,f.ct_order_change
	,f.amt_goal
	,f.amt_revenuedollarsbilling
	,f.ct_quantitybilling
	,f.dd_crdflag
	,f.dd_csdflag
	,f.dd_hold_date
	,f.dd_rowiscurrent
	,f.dd_subinventory
	,f.dim_ora_mdg_productid
	,f.dim_ora_bwproducthierarchyid
	,f.dim_ora_dropship_addressid
	,f.dim_ora_otd_promise_dateid
	,f.dd_headertype
	,f.dd_attribute11
	,f.dd_context
	,f.dim_ora_plantid
	,f.dim_backorder_dateid
	,f.dd_backorder_responsibility
	,f.dd_backorder_resp_geography
	,f.dd_backorder_status
	,f.dd_backorderreason
	,f.dd_backorder
	,f.dd_lsintercompflag
	,f.dd_intercompflag
	,f.dd_tradsales_flag
	,f.dd_backord_reason_lookupt
	,f.dim_ora_clusterid
	,f.dim_ora_commercialviewid
	,f.dd_coveredbymaterial
	,f.dd_coveredbyplant
	,f.dd_lineno_compno
	,f.dd_ace_openorders
	,f.dd_deliveryontime
	,f.dd_deliveryisfull
	,f.ct_deliveryisfull
	,f.ct_deliveryontime
	,CURRENT_DATE - 1 snapshot_date
	,1 dim_ora_date_snapshotid
   ,ct_deliveryontimecasual
    ,dd_deliveryontimecasual
	,dim_otd_dateid_dimD
	,dim_ora_otd_promise_dateid_dimD
	,ct_countsalesdocitem
	,dim_ora_date_customer_requestedid_dimD
	,dim_ora_date_customer_requestedid
	,dim_countryhierpsid
	,dim_countryhierarid
	,dd_gsa_code
	,dim_ora_date_enddateloadingid
	,dim_ora_gsa_codeid
	,dd_inco1
	,dd_inco2
	,CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END as dd_mtomts
	,ct_LineReserved_qty
	,dd_delivery_group
  ,dd_single_lot
FROM fact_ora_sales_order_line f
	INNER JOIN dim_ora_invproduct d_inv ON f.dim_ora_inv_productid = d_inv.dim_ora_invproductid
	INNER JOIN dim_ora_bwproducthierarchy uph ON f.dim_ora_bwproducthierarchyid = uph.dim_bwproducthierarchyid
	INNER JOIN dim_date_factory_calendar mts ON f.dim_ora_date_customer_requestedid = mts.dim_dateid
	INNER JOIN dim_date_factory_calendar mto ON (case when uph.businessdesc like '%Research Solutions%' then f.dim_ora_date_customer_requestedid else
f.dim_ora_otd_promise_dateid end) = mto.dim_dateid

WHERE
CASE
  WHEN mts.datevalue = CURRENT_DATE - 1
	  AND mts.datevalue  <> '0001-01-01'
	  AND CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END  = 'MTS'
  THEN 'Yes'
  WHEN mto.datevalue = CURRENT_DATE - 1
	  AND mto.datevalue <> '0001-01-01'
	  AND CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END = 'MTO'
  THEN  'Yes'
  ELSE 'No'
END = 'Yes'
AND f.dd_fillratesnapshot_flag = 'Not Set'
AND concat(f.dd_order_number,f.dd_lineno_compno) not in (select distinct concat(f.dd_order_number,f.dd_lineno_compno) from fact_ora_fillrate_snapshot f)
AND f.DD_ACE_OPENORDERS = 'X'
 AND uph.businesssector = 'BS-02';


 /*UPDATE DIM_ORA_DATE_SNAPSHOTID*/
UPDATE fact_ora_fillrate_snapshot f
 SET f.dim_ora_date_snapshotid = d.dim_dateid
    ,dw_update_date = CURRENT_DATE
FROM fact_ora_fillrate_snapshot f, dim_date d
WHERE TO_DATE(f.snapshot_date) = d.datevalue
  AND f.dim_ora_date_snapshotid = 1;


UPDATE fact_ora_sales_order_line F
SET
F.dd_fillratesnapshot_flag ='X'
FROM fact_ora_sales_order_line F, fact_ora_fillrate_snapshot fls
WHERE f.fact_ora_sales_order_lineid = fls.fact_ora_sales_order_lineid
AND fls.snapshot_date = current_date - 1;

/* Update Customer ShipTo according to CTT logic - 28th October 2016 by Alina*/

truncate table temp_CCT;
insert into temp_CCT
select distinct fo.dd_salesdocno,fo.dim_customerid
from EMDTempoCC4.fact_salesorder fo;


UPDATE fact_ora_fillrate_snapshot f
SET dim_ora_customer_ship_to_locationid =  case when SUBSTR(d.description,1,20) like '%Internal Drop Ship%' then fo.dim_customerid
                                           else f.dim_ora_customer_ship_to_locationid end
FROM dim_ora_xacttype d,fact_ora_fillrate_snapshot f,temp_CCT fo
WHERE f.DIM_ORA_ORDER_HEADER_TYPEID=d.DIM_ORA_XACTTYPEID
AND SUBSTR(d.description,1,20) like '%Internal Drop Ship%'
AND SUBSTR(f.dd_cust_po_number,1,40)=fo.dd_salesdocno;

UPDATE fact_ora_fillrate_snapshot f_so
SET f_so.dim_ora_commercialviewid = ds.dim_commercialviewid
FROM fact_ora_fillrate_snapshot f_so, dim_commercialview ds, EMDTempoCC4.dim_customer dc, dim_ora_bwproducthierarchy b
WHERE ds.soldtocountry = substring(dc.country,1,2)
  AND ds.upperprodhier = b.business
  AND f_so.dim_ora_bwproducthierarchyid = b.dim_bwproducthierarchyid
  AND f_so.dim_ora_customer_ship_to_locationid = dc.dim_customerid
  AND f_so.dim_ora_commercialviewid <> ds.dim_commercialviewid;


 /* Covered by Plant*/

drop table if exists fact_fillrate_snapshot_history;
create table fact_fillrate_snapshot_history as select a.* from fact_ora_fillrate_snapshot a
where TO_DATE(snapshot_date) = TO_DATE(CURRENT_DATE) - 1;

update fact_fillrate_snapshot_history f
 set dd_coveredbyplant = 'Not Set'
 where ifnull(dd_coveredbyplant,'Not Set') <> 'Not Set';

drop table if exists tmp_diff;
create table tmp_diff as select dd_order_number,dd_lineno_compno,dim_ora_date_snapshotid,sum(ct_ordered_quantity)-sum(ct_total_shipped_quantity) diff
from fact_fillrate_snapshot_history
group by 1,2,3;



update fact_fillrate_snapshot_history a
set a.dd_coveredbyplant = 'X'
from fact_fillrate_snapshot_history a, tmp_diff b
where a.dd_order_number = b.dd_order_number
and a.dd_lineno_compno = b.dd_lineno_compno
and a.dim_ora_date_snapshotid = b.dim_ora_date_snapshotid
and b.diff = 0;

drop table if exists tmp_sales_openqty_1;
create table tmp_sales_openqty_1
as
select
	 to_date(snapshot_date) datevalue
	,dd_order_number dd_salesdocno
	,dd_lineno_compno dd_salesitemno
	,dp.segment1 partnumber
	,pl.organization_code PLANTCODE
	,1 deliverypriority
	, mlrsd.datevalue dim_dateidrequested_emd
	,sum(case
				  when (f_so.CT_ORDERED_QUANTITY  - f_so.ct_total_shipped_quantity ) < 0 then 0.0000 --- f_so.ct_CmlQtyReceived
			      else (f_so.CT_ORDERED_QUANTITY  - f_so.ct_total_shipped_quantity )
			      end) openqty
from fact_fillrate_snapshot_history f_so
	inner join dim_ora_invproduct dp on f_so.dim_ora_inv_productid= dp.DIM_ORA_INVPRODUCTID
INNER JOIN dim_ora_invproduct d_inv ON f_so.dim_ora_inv_productid = d_inv.dim_ora_invproductid
inner join dim_ora_bwproducthierarchy bw on f_so.dim_ora_bwproducthierarchyid=bw.dim_bwproducthierarchyid
	 INNER JOIN dim_date_factory_calendar mlrsd on
	CASE WHEN(CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END ='MTO') then (case when bw.businessdesc like '%Research Solutions%'
then f_so.dim_ora_date_customer_requestedid else f_so.dim_ora_otd_promise_dateid end)
ELSE dim_ora_date_customer_requestedid END = mlrsd.dim_dateid
	inner join dim_ora_business_org pl on f_so.DIM_ORA_INVENTORY_ORGID=pl.DIM_ORA_BUSINESS_ORGID
where dd_coveredbyplant <>'X'
group by snapshot_date
	,dd_order_number
	,dd_lineno_compno
	,dp.segment1
	,pl.organization_code
	,1
	, mlrsd.datevalue
having sum(case
				  when (f_so.CT_ORDERED_QUANTITY  - f_so.ct_total_shipped_quantity ) < 0 then 0.0000 --- f_so.ct_CmlQtyReceived
			      else (f_so.CT_ORDERED_QUANTITY  - f_so.ct_total_shipped_quantity )
			      end) > 0;


drop table if exists tmp_sales_openqty;
create table tmp_sales_openqty
as
select
	row_number() over(order by dim_dateidrequested_emd asc, dd_salesdocno asc,datevalue asc) proc_order
	,dd_salesdocno
	,dd_salesitemno
	,datevalue
	,partnumber
	,PLANTCODE
	,openqty
from tmp_sales_openqty_1;

drop table if exists tmp_sales_openqty_cum;
create table tmp_sales_openqty_cum
as
select
	 a.datevalue
	,a.proc_order
	,a.dd_salesdocno
	,a.dd_salesitemno
	,a.partnumber
	,a.PLANTCODE
	,a.openqty
	,'Not Set' cvrd_by_plant
	,sum(a.openqty) over(partition by partnumber,PLANTCODE,datevalue order by a.proc_order rows between unbounded preceding and current row) cum_openqty
from tmp_sales_openqty a
group by a.proc_order, a.dd_salesdocno, a.dd_salesitemno, a.partnumber, a.PLANTCODE, a.openqty,a.datevalue;

drop table if exists tmp_uprestrictedstock;
create table tmp_uprestrictedstock
as
select
	dp.segment1 partnumber
	,pl.organization_code PLANTCODE
	,sum(CASE WHEN f.source_id='ORA 12.1' and
      d_sinv.availability_type = 1 THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) when
f.source_id='OPM' and (f.dd_restricted_unrestricted_flag = 'N' or f.dd_restricted_unrestricted_flag = 'Not Set') then
CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
else
0 END) ct_StockQty
	,to_date(f.snapshot_date) datevalue
from fact_ora_invonhand_daily f
	inner join dim_ora_invproduct dp on dp.DIM_ORA_INVPRODUCTID = f.DIM_ORA_INV_PRODUCTID
	inner join dim_ora_business_org pl on f.DIM_ORA_INV_ORGID = pl.DIM_ORA_BUSINESS_ORGID
    inner join dim_ora_mtl_sec_inventory d_sinv on f.dim_ora_mtl_sec_inventoryid = d_sinv.dim_ora_mtl_sec_inventoryid
group by dp.segment1, pl.organization_code,f.snapshot_date;

update tmp_sales_openqty_cum a
set a.cvrd_by_plant = 'X'
from tmp_sales_openqty_cum a, tmp_uprestrictedstock b
where a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and a.cum_openqty <= b.ct_StockQty and to_date(a.datevalue) = to_date(b.datevalue);

/* check if the remaining stock can be allocated */

drop table if exists tmp_unalocatedstock;
create table tmp_unalocatedstock
as
select a.partnumber,a.PLANTCODE,max(b.ct_StockQty) - max(a.cum_openqty) remaining_stock ,a.datevalue
from tmp_sales_openqty_cum a
	inner join tmp_uprestrictedstock b on a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and to_date(a.datevalue) = to_date(b.datevalue)
where a.cvrd_by_plant = 'X'
group by a.partnumber,a.PLANTCODE,a.datevalue
having max(b.ct_StockQty) - max(a.cum_openqty) > 0;

/* add materials that did not manage to cover any order but have stock */

drop table if exists tmp_materialsnotcovered;
create table tmp_materialsnotcovered
as
select distinct a.partnumber,a.PLANTCODE,a.datevalue from tmp_sales_openqty_cum a where not exists
 (select 1 from tmp_sales_openqty_cum b where a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and b.cvrd_by_plant = 'X' and to_date(a.datevalue) = to_date(b.datevalue));

insert into tmp_unalocatedstock(partnumber, PLANTCODE, remaining_stock,datevalue)
select partnumber, PLANTCODE, ct_StockQty,datevalue from tmp_uprestrictedstock where (partnumber,PLANTCODE,to_date(datevalue)) in (select partnumber,PLANTCODE,to_date(datevalue) from tmp_materialsnotcovered) and ct_StockQty > 0;

drop table if exists tmp_sowithunalocatedstock;
create table tmp_sowithunalocatedstock
as
select a.*
from tmp_sales_openqty_cum a
	inner join tmp_unalocatedstock b on a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and to_date(a.datevalue) = to_date(b.datevalue)
where a.openqty <= b.remaining_stock
	and a.cvrd_by_plant = 'Not Set';


 execute script emd586.emd_covered_by_plant_history('tmp_sowithunalocatedstock','tmp_unalocatedstock','tmp_sales_openqty_cum');
/* end check if the remaining stock can be allocated */


update fact_fillrate_snapshot_history f
set f.dd_coveredbyplant = trim(t.cvrd_by_plant)
from fact_fillrate_snapshot_history f inner join dim_date dd on
f.dim_ora_date_snapshotid = dd.dim_dateid, tmp_sales_openqty_cum t,dim_ora_invproduct dp
where f.dd_order_number = t.dd_salesdocno and
f.dd_lineno_compno=t.dd_salesitemno and to_date(dd.datevalue) = to_date(t.datevalue)
AND f.dim_ora_inv_productid= dp.DIM_ORA_INVPRODUCTID
AND dp.segment1=T.PARTNUMBER
	and f.dd_coveredbyplant <> trim(t.cvrd_by_plant) ;


 drop table if exists tmp_sales_openqty_1;
 drop table if exists tmp_sales_openqty;
 drop table if exists tmp_sales_openqty_cum;
 drop table if exists tmp_unalocatedstock;
 drop table if exists tmp_sowithunalocatedstock;
 drop table if exists tmp_materialsnotcovered;


update fact_fillrate_snapshot_history ffs
set ffs.dd_coveredbyplant= 'X'
 from fact_fillrate_snapshot_history ffs
 inner join dim_date d1 on d1.dim_dateid = ffs.dim_ora_date_actual_shipid
 inner join dim_date d2 on d2.dim_dateid = ffs.dim_ora_date_snapshotid

where
to_date(d1.datevalue) <= to_date(d2.datevalue)
AND (case when (CT_ORDERED_QUANTITY  - ct_total_shipped_quantity ) < 0 then 0.0000 --- f_so.ct_CmlQtyReceived
			      else (CT_ORDERED_QUANTITY  - ct_total_shipped_quantity )
			      end) = 0
and ffs.CT_ORDERED_QUANTITY<>0
and ffs.dd_coveredbyplant <> 'X';

drop table if exists tmp_distinct;
create table tmp_distinct as select distinct a.fact_ora_fillrate_snapshotid aa,a.dd_coveredbyplant
from  fact_fillrate_snapshot_history a inner join fact_fillrate_snapshot_history b
on a.dd_order_number = b.dd_order_number
and a.dd_lineno_compno = b.dd_lineno_compno
and a.dim_ora_inv_productid = b.dim_ora_inv_productid
and a.fact_ora_fillrate_snapshotid <> b.fact_ora_fillrate_snapshotid
and a.dim_ora_date_snapshotid = b.dim_ora_date_snapshotid and a.dd_coveredbyplant<>b.dd_coveredbyplant
where a.CT_ORDERED_QUANTITY = 0;

update fact_fillrate_snapshot_history a
set a.dd_coveredbyplant = b.dd_coveredbyplant
from fact_fillrate_snapshot_history a
    ,tmp_distinct b
where a.fact_ora_fillrate_snapshotid = b.aa
and a.dd_coveredbyplant<> 'X';

drop table if exists tmp_tbp;
create table tmp_tbp as
select distinct a.fact_ora_fillrate_snapshotid , b.dd_coveredbyplant,a.dim_ora_date_snapshotid
from fact_fillrate_snapshot_history a,fact_fillrate_snapshot_history b
where a.dd_order_number = b.dd_order_number
and a.dd_lineno_compno = b.dd_lineno_compno
and a.dim_ora_date_snapshotid = b.dim_ora_date_snapshotid
and a.CT_ORDERED_QUANTITY = 0
and b.CT_ORDERED_QUANTITY <>0
and a.dd_coveredbyplant <> b.dd_coveredbyplant;

update fact_fillrate_snapshot_history a
set a.dd_coveredbyplant = b.dd_coveredbyplant
from fact_fillrate_snapshot_history a,tmp_tbp b
where a.fact_ora_fillrate_snapshotid = b.fact_ora_fillrate_snapshotid
and a.dim_ora_date_snapshotid = b.dim_ora_date_snapshotid
and a.CT_ORDERED_QUANTITY= 0
and b.dd_coveredbyplant ='X'
and a.dd_coveredbyplant <> b.dd_coveredbyplant;

update fact_ora_fillrate_snapshot a
set a.dd_coveredbyplant = b.dd_coveredbyplant
from fact_ora_fillrate_snapshot a, fact_fillrate_snapshot_history b--,dim_ora_invproduct d
where a.fact_ora_fillrate_snapshotid = b.fact_ora_fillrate_snapshotid
and a.dd_coveredbyplant <> b.dd_coveredbyplant;

drop table if exists fact_fillrate_snapshot_history;
drop table if exists tmp_diff;
drop table if exists tmp_distinct;
drop table if exists tmp_tbp;
drop table if exists tmp_upd_sales;
drop table if exists tmp_dayofprocessing_var;
drop table if exists tmp_loaded_datevalues;


UPDATE fact_ora_fillrate_snapshot f
SET f.dim_ora_clusterid = dc.dim_ora_clusterid
FROM fact_ora_fillrate_snapshot f, dim_ora_mdg_product mdg, dim_ora_bwproducthierarchy ph, dim_ora_cluster dc
WHERE f.dim_ora_mdg_productid = mdg.dim_mdg_partid
    AND f.dim_ora_bwproducthierarchyid = ph.dim_bwproducthierarchyid
    AND ph.businesssector = 'BS-02'
    AND mdg.primary_production_location = dc.primary_manufacturing_site
    AND f.dim_ora_clusterid <> dc.dim_ora_clusterid
    AND TO_DATE(F.snapshot_date) = TO_DATE(CURRENT_DATE) - 1 ;




update fact_ora_fillrate_snapshot
set ct_countsalesdocitem_NEW=ct_countsalesdocitem
from fact_ora_fillrate_snapshot
where ct_countsalesdocitem_NEW<>ct_countsalesdocitem;

update fact_ora_fillrate_snapshot f_so
set dim_mercklsconforreqdateid = mlsrd.dim_dateid
from fact_ora_fillrate_snapshot f_so,dim_date_factory_calendar mlsrd, dim_ora_invproduct prt,dim_ora_bwproducthierarchy bw
where f_so.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
and f_so.dim_ora_inv_productid = prt.dim_ora_invproductid
and CASE WHEN(CASE WHEN prt.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END ='MTO') then (case when bw.businessdesc like '%Research Solutions%'
then f_so.dim_ora_date_customer_requestedid else f_so.dim_ora_otd_promise_dateid end)
ELSE dim_ora_date_customer_requestedid END = mlsrd.dim_dateid
and dim_mercklsconforreqdateid <> mlsrd.dim_dateid;

UPDATE fact_ora_fillrate_snapshot f
SET f.dim_ora_customer_ship_to_locationid = dc.dim_ora_customer_ship_to_locationid
FROM fact_ora_fillrate_snapshot f, fact_ora_sales_order_line dc
WHERE f.fact_ora_sales_order_lineid = dc.fact_ora_sales_order_lineid
    AND f.dim_ora_customer_ship_to_locationid<> dc.dim_ora_customer_ship_to_locationid
and f.dim_ora_customer_ship_to_locationid = 1;


update EMDORACLE4AE.FACT_ORA_FILLRATE_SNAPSHOT f
set f.DD_PRIMARYPRODUCTIONLOCATION = mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
FROM  EMDORACLE4AE.FACT_ORA_FILLRATE_SNAPSHOT f
inner join dim_ora_mdg_product  mdg on f.dim_ora_mdg_productid=mdg.dim_mdg_partid
where f.DD_PRIMARYPRODUCTIONLOCATION <> mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
and TO_DATE(snapshot_date) = TO_DATE(CURRENT_DATE) - 1;

