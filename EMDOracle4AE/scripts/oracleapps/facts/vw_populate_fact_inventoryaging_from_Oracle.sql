
truncate table tmp_fact_inventoryaging_Oracle;

INSERT INTO tmp_fact_inventoryaging_Oracle
(
     amt_exchangerate
	,amt_exchangerate_gbl
	,ct_blockedstock
	,ct_stockqty
	,amt_OnHand
	,dd_batchno
	,dim_unitofmeasureid
	,dim_storagelocentrydateid
	,dim_plantid
	,dim_partid
	,dim_storagelocationid
	,dim_mdg_partid
	,dim_bwproducthierarchyid
	,dim_companyid
	,dw_insert_date
	,dw_update_date
	,fact_inventoryagingid
    ,dim_projectsourceid
    ,ct_StockInQInsp
    ,ct_StockInTransit
    ,ct_StockInTransfer
    ,ct_TotalRestrictedStock
    ,dim_itemcategoryid
    ,dim_stockcategoryid
    ,amt_StdUnitPrice
    ,amt_WIPBalance
    ,ct_WIPQty

	,amt_cogsactualrate_emd
	,amt_cogsfixedrate_emd
	,amt_cogsfixedplanrate_emd
	,amt_cogsplanrate_emd
	,amt_cogsprevyearfixedrate_emd
	,amt_cogsprevyearrate_emd
	,amt_cogsprevyearto1_emd
	,amt_cogsprevyearto2_emd
	,amt_cogsprevyearto3_emd
	,amt_cogsturnoverrate1_emd
	,amt_cogsturnoverrate2_emd
	,amt_cogsturnoverrate3_emd

    ,dim_bwhierarchycountryid
	,dim_clusterid
	,dim_DateidExpiryDate
	,dim_countryhierarid
	,dim_countryhierpsid
	,dim_commercialviewid
	,dd_isconsigned
	,ct_baseuomratioPCcustom
	,ct_countmaterialsafetystock
	,dim_specialstockid
	,dd_batch_status_qkz
	,amt_StockValueAmt
	,dim_productionschedulerid
	,dim_batchstatusid
     )
SELECT
         CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
		,CASE WHEN d_sinv.availability_type = 1 and SUBSTR(d_sinv.secondary_inventory_name,1,16) not in ('STERISHLD','7DANB','7DAND')
     THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END ct_primary_transaction_quantity

		,CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		 END amt_primary_transaction_quantity  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost

		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
		,d_uom.dim_unitofmeasureid
		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
        ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
		,f_inv.dw_insert_date
		,f_inv.dw_update_date
		,f_inv.fact_ora_invonhandid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhandid
		,p.dim_projectsourceid
		,0
		,0
		,0
		,CASE WHEN d_sinv.availability_type = 2 THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
    WHEN SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
    THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
    ELSE 0 END restricted_qty
		,1
		,1
		,dd_standart_cost
		,0
		,0

		,amt_cogsactualrate_emd
		,amt_cogsfixedrate_emd
		,amt_cogsfixedplanrate_emd
		,amt_cogsplanrate_emd
		,amt_cogsprevyearfixedrate_emd
		,amt_cogsprevyearrate_emd
		,amt_cogsprevyearto1_emd
		,amt_cogsprevyearto2_emd
		,amt_cogsprevyearto3_emd
		,amt_cogsturnoverrate1_emd
		,amt_cogsturnoverrate2_emd
		,amt_cogsturnoverrate3_emd

        ,dim_ora_bwhierarchycountryid
		,dim_ora_clusterid
		,dim_ora_expiration_dateid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_expiration_dateid
		,dim_countryhierarid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierarid
	    ,dim_countryhierpsid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierpsid
		,dim_ora_commercialviewid
		,ifnull(dd_isconsigned,0)
		,1
		,ct_countmaterialsafetystock
		,1
		,'Not Set' dd_batch_status_qkz
        ,CASE WHEN d_sinv.availability_type = 1 and SUBSTR(d_sinv.secondary_inventory_name,1,16) not in ('STERISHLD','7DANB','7DAND')
        and amt_cogsfixedplanrate_emd =0 THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END * dd_standart_cost
		,1 dim_productionschedulerid
		,1 dim_batchstatusid
 FROM emdoracle4ae.fact_ora_invonhand f_inv
     ,emdoracle4ae.dim_projectsource p
     ,emd586.dim_unitofmeasure d_uom
     ,emdoracle4ae.dim_ora_mtl_sec_inventory d_sinv
WHERE IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = d_uom.uom
 AND d_uom.projectsourceid = p.dim_projectsourceid
 AND f_inv.dim_ora_mtl_sec_inventoryid = d_sinv.dim_ora_mtl_sec_inventoryid
 AND d_sinv.asset_inventory = 1
 AND ct_primary_transaction_quantity <> 999999999999999868928;

 INSERT INTO tmp_fact_inventoryaging_Oracle
(
  amt_exchangerate
 ,amt_exchangerate_gbl
 ,ct_blockedstock
 ,ct_stockqty
 ,amt_OnHand
 ,dd_batchno
 ,dim_unitofmeasureid
 ,dim_storagelocentrydateid
 ,dim_plantid
 ,dim_partid
 ,dim_storagelocationid
 ,dim_mdg_partid
 ,dim_bwproducthierarchyid
 ,dim_companyid
 ,dw_insert_date
 ,dw_update_date
 ,fact_inventoryagingid
    ,dim_projectsourceid
    ,ct_StockInQInsp
    ,ct_StockInTransit
    ,ct_StockInTransfer
    ,ct_TotalRestrictedStock
    ,dim_itemcategoryid
    ,dim_stockcategoryid
    ,amt_StdUnitPrice

 ,amt_cogsactualrate_emd
 ,amt_cogsfixedrate_emd
 ,amt_cogsfixedplanrate_emd
 ,amt_cogsplanrate_emd
 ,amt_cogsprevyearfixedrate_emd
 ,amt_cogsprevyearrate_emd
 ,amt_cogsprevyearto1_emd
 ,amt_cogsprevyearto2_emd
 ,amt_cogsprevyearto3_emd
 ,amt_cogsturnoverrate1_emd
 ,amt_cogsturnoverrate2_emd
 ,amt_cogsturnoverrate3_emd

    ,dim_bwhierarchycountryid
 ,dim_clusterid
 ,dim_DateidExpiryDate
 ,dim_countryhierarid
 ,dim_countryhierpsid
 ,dim_commercialviewid
 ,dd_isconsigned
 ,ct_baseuomratioPCcustom
 ,ct_countmaterialsafetystock
 ,CT_WIPQTY
 ,AMT_WIPBALANCE
 ,amt_StockInTransitAmt
 ,dim_specialstockid
 ,dd_batch_status_qkz
 ,amt_StockValueAmt
 ,dim_productionschedulerid
 ,DIM_ITEMSTATUSID
 ,dim_batchstatusid

     )
SELECT
         CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
   ,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
   ,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
   ,ct_primary_transaction_quantity

   ,CONVERT (DECIMAL (18,5),ct_intransit_quantity) * dd_standart_cost  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost

   ,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
   ,1 -- d_uom.dim_unitofmeasureid
   ,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
   ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
   ,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
   ,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
   ,f_inv.dim_ora_mdg_productid -- contains the multiplier already
   ,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
        ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
   ,f_inv.dw_insert_date
   ,f_inv.dw_update_date
   ,f_inv.fact_ora_invonhandid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhandid
   ,7
   ,0
   ,ct_intransit_quantity
   ,0
   ,ct_primary_transaction_quantity restricted_qty
   ,1
   ,1
   ,dd_standart_cost
   ,amt_cogsactualrate_emd
   ,amt_cogsfixedrate_emd
   ,amt_cogsfixedplanrate_emd
   ,amt_cogsplanrate_emd
   ,amt_cogsprevyearfixedrate_emd
   ,amt_cogsprevyearrate_emd
   ,amt_cogsprevyearto1_emd
   ,amt_cogsprevyearto2_emd
   ,amt_cogsprevyearto3_emd
   ,amt_cogsturnoverrate1_emd
   ,amt_cogsturnoverrate2_emd
   ,amt_cogsturnoverrate3_emd

        ,dim_ora_bwhierarchycountryid
   ,dim_ora_clusterid
   ,dim_ora_expiration_dateid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_expiration_dateid
   ,dim_countryhierarid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierarid
     ,dim_countryhierpsid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierpsid
   ,dim_ora_commercialviewid
   ,ifnull(dd_isconsigned,0)
   ,1
   ,ct_countmaterialsafetystock
   ,0
   ,0
   ,CONVERT (DECIMAL (18,5),ct_intransit_quantity) * dd_standart_cost as amt_primary_transaction_quantity
   ,1
   ,'Not Set' dd_batch_status_qkz
   ,0
   ,1 dim_productionschedulerid
   ,1 DIM_ITEMSTATUSID
   ,1 dim_batchstatusid
 FROM emdoracle4ae.fact_ora_invonhand f_inv
      ,emdoracle4ae.dim_projectsource p

WHERE ct_primary_transaction_quantity <> 999999999999999868928
 and f_inv.source_id ='Intransit';

 INSERT INTO tmp_fact_inventoryaging_Oracle
 (
      amt_exchangerate
 	,amt_exchangerate_gbl
 	,ct_blockedstock
 	,ct_stockqty
 	,amt_OnHand
 	,dd_batchno
 	,dim_unitofmeasureid
 	,dim_storagelocentrydateid
 	,dim_plantid
 	,dim_partid
 	,dim_storagelocationid
 	,dim_mdg_partid
 	,dim_bwproducthierarchyid
 	,dim_companyid
 	,dw_insert_date
 	,dw_update_date
 	,fact_inventoryagingid
     ,dim_projectsourceid
     ,ct_StockInQInsp
     ,ct_StockInTransit
     ,ct_StockInTransfer
     ,ct_TotalRestrictedStock
     ,dim_itemcategoryid
     ,dim_stockcategoryid
     ,amt_StdUnitPrice
     ,amt_WIPBalance
     ,ct_WIPQty

 	,amt_cogsactualrate_emd
 	,amt_cogsfixedrate_emd
 	,amt_cogsfixedplanrate_emd
 	,amt_cogsplanrate_emd
 	,amt_cogsprevyearfixedrate_emd
 	,amt_cogsprevyearrate_emd
 	,amt_cogsprevyearto1_emd
 	,amt_cogsprevyearto2_emd
 	,amt_cogsprevyearto3_emd
 	,amt_cogsturnoverrate1_emd
 	,amt_cogsturnoverrate2_emd
 	,amt_cogsturnoverrate3_emd

     ,dim_bwhierarchycountryid
 	,dim_clusterid
 	,dim_DateidExpiryDate
 	,dim_countryhierarid
 	,dim_countryhierpsid
 	,dim_commercialviewid
 	,dd_isconsigned
 	,ct_baseuomratioPCcustom
 	,ct_countmaterialsafetystock
 	,dim_specialstockid
 	,dd_batch_status_qkz
 	,amt_StockValueAmt
 	,dim_productionschedulerid
 	,DIM_ITEMSTATUSID
 	,dim_batchstatusid
      )
 SELECT
          CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
 		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
 		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
 		,CASE WHEN f_inv.dd_restricted_unrestricted_flag = 'N' or f_inv.dd_restricted_unrestricted_flag = 'Not Set' THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
            ELSE 0
 		 END ct_primary_transaction_quantity

 		,CASE WHEN f_inv.source_id = 'OPM' THEN amt_opm_onhand_value-- contains already unit price in the amt
 		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
 		 END amt_primary_transaction_quantity  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost

 		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
 		,1 dim_unitofmeasureid
 		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
 		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
 		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
 		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
 		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
 		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
         ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
 		,f_inv.dw_insert_date
 		,f_inv.dw_update_date
 		,f_inv.fact_ora_invonhandid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhandid
 		,p.dim_projectsourceid
 		,0
 		,0
 		,0
 		,CASE WHEN f_inv.dd_restricted_unrestricted_flag = 'Y' THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END restricted_qty
 		,1
 		,1
 		,dd_standart_cost
 		,0
 		,0

 		,amt_cogsactualrate_emd
 		,amt_cogsfixedrate_emd
 		,amt_cogsfixedplanrate_emd
 		,amt_cogsplanrate_emd
 		,amt_cogsprevyearfixedrate_emd
 		,amt_cogsprevyearrate_emd
 		,amt_cogsprevyearto1_emd
 		,amt_cogsprevyearto2_emd
 		,amt_cogsprevyearto3_emd
 		,amt_cogsturnoverrate1_emd
 		,amt_cogsturnoverrate2_emd
 		,amt_cogsturnoverrate3_emd

         ,dim_ora_bwhierarchycountryid
 		,dim_ora_clusterid
 		,dim_ora_expiration_dateid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_expiration_dateid
 		,dim_countryhierarid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierarid
 	    ,dim_countryhierpsid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierpsid
 		,dim_ora_commercialviewid
 		,ifnull(dd_isconsigned,0)
 		,1
 		,ct_countmaterialsafetystock
 		,1
 		,'Not Set' dd_batch_status_qkz
 		,CASE WHEN f_inv.source_id = 'OPM' and (f_inv.dd_restricted_unrestricted_flag = 'N' or f_inv.dd_restricted_unrestricted_flag = 'Not Set') THEN CONVERT (DECIMAL (18,5),amt_opm_onhand_value)
            ELSE 0
 		 END amt_StockValueAmt
         ,1 dim_productionschedulerid
 		,1 DIM_ITEMSTATUSID
 		,1 dim_batchstatusid
  FROM emdoracle4ae.fact_ora_invonhand f_inv
      ,emdoracle4ae.dim_projectsource p

 WHERE f_inv.source_id='OPM'
  AND ct_primary_transaction_quantity <> 999999999999999868928;

  INSERT INTO tmp_fact_inventoryaging_Oracle
 (
      amt_exchangerate
   ,amt_exchangerate_gbl
   ,ct_blockedstock
   ,ct_stockqty
   ,amt_OnHand
   ,dd_batchno
   ,dim_unitofmeasureid
   ,dim_storagelocentrydateid
   ,dim_plantid
   ,dim_partid
   ,dim_storagelocationid
   ,dim_mdg_partid
   ,dim_bwproducthierarchyid
   ,dim_companyid
   ,dw_insert_date
   ,dw_update_date
   ,fact_inventoryagingid
     ,dim_projectsourceid
      ,ct_StockInTransit
     ,ct_StockInTransfer
     ,ct_TotalRestrictedStock
     ,dim_itemcategoryid
     ,dim_stockcategoryid
     ,amt_StdUnitPrice

   ,amt_cogsactualrate_emd
   ,amt_cogsfixedrate_emd
   ,amt_cogsfixedplanrate_emd
   ,amt_cogsplanrate_emd
   ,amt_cogsprevyearfixedrate_emd
   ,amt_cogsprevyearrate_emd
   ,amt_cogsprevyearto1_emd
   ,amt_cogsprevyearto2_emd
   ,amt_cogsprevyearto3_emd
   ,amt_cogsturnoverrate1_emd
   ,amt_cogsturnoverrate2_emd
   ,amt_cogsturnoverrate3_emd

     ,dim_bwhierarchycountryid
   ,dim_clusterid
   ,dim_DateidExpiryDate
   ,dim_countryhierarid
   ,dim_countryhierpsid
   ,dim_commercialviewid
   ,dd_isconsigned
   ,ct_baseuomratioPCcustom
   ,ct_countmaterialsafetystock
   ,CT_WIPQTY
   ,AMT_WIPBALANCE
   ,amt_StockInTransitAmt
   ,dim_specialstockid
     ,ct_StockInQInsp
     ,amt_StockInQInspAmt
   ,dim_productionschedulerid
   ,DIM_ITEMSTATUSID
   ,dim_batchstatusid
      )
 SELECT
          CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
     ,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
     ,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
     ,ct_primary_transaction_quantity

     ,CONVERT (DECIMAL (18,5),ct_qinspection_quantity) * dd_standart_cost  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost

     ,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
     ,1 -- d_uom.dim_unitofmeasureid
     ,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
     ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
     ,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
     ,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
     ,f_inv.dim_ora_mdg_productid -- contains the multiplier already
     ,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
         ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
     ,f_inv.dw_insert_date
     ,f_inv.dw_update_date
     ,f_inv.fact_ora_invonhandid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhandid
     ,7
     ,0
     ,0
     ,0
     ,1
     ,1
     ,dd_standart_cost
     ,amt_cogsactualrate_emd
     ,amt_cogsfixedrate_emd
     ,amt_cogsfixedplanrate_emd
     ,amt_cogsplanrate_emd
     ,amt_cogsprevyearfixedrate_emd
     ,amt_cogsprevyearrate_emd
     ,amt_cogsprevyearto1_emd
     ,amt_cogsprevyearto2_emd
     ,amt_cogsprevyearto3_emd
     ,amt_cogsturnoverrate1_emd
     ,amt_cogsturnoverrate2_emd
     ,amt_cogsturnoverrate3_emd

         ,dim_ora_bwhierarchycountryid
     ,dim_ora_clusterid
     ,dim_ora_expiration_dateid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_expiration_dateid
     ,dim_countryhierarid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierarid
       ,dim_countryhierpsid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierpsid
     ,dim_ora_commercialviewid
     ,ifnull(dd_isconsigned,0)
     ,1
     ,ct_countmaterialsafetystock
     ,0
     ,0
     ,0
     ,1
     ,ct_qinspection_quantity
         ,CONVERT (DECIMAL (18,5),ct_qinspection_quantity) * dd_standart_cost
     ,1 dim_productionschedulerid
     ,1 DIM_ITEMSTATUSID
     ,1 dim_batchstatusid
  FROM emdoracle4ae.fact_ora_invonhand f_inv
       ,emdoracle4ae.dim_projectsource p

 WHERE ct_primary_transaction_quantity <> 999999999999999868928
  and f_inv.source_id ='QInspection';


  INSERT INTO tmp_fact_inventoryaging_Oracle
  (
       amt_exchangerate
  	,amt_exchangerate_gbl
  	,ct_blockedstock
  	,ct_stockqty
  	,amt_OnHand
  	,dd_batchno
  	,dim_unitofmeasureid
  	,dim_storagelocentrydateid
  	,dim_plantid
  	,dim_partid
  	,dim_storagelocationid
  	,dim_mdg_partid
  	,dim_bwproducthierarchyid
  	,dim_companyid
  	,dw_insert_date
  	,dw_update_date
  	,fact_inventoryagingid
      ,dim_projectsourceid
      ,ct_StockInQInsp
      ,ct_StockInTransit
      ,ct_StockInTransfer
      ,ct_TotalRestrictedStock
      ,dim_itemcategoryid
      ,dim_stockcategoryid
      ,amt_StdUnitPrice

  	,amt_cogsactualrate_emd
  	,amt_cogsfixedrate_emd
  	,amt_cogsfixedplanrate_emd
  	,amt_cogsplanrate_emd
  	,amt_cogsprevyearfixedrate_emd
  	,amt_cogsprevyearrate_emd
  	,amt_cogsprevyearto1_emd
  	,amt_cogsprevyearto2_emd
  	,amt_cogsprevyearto3_emd
  	,amt_cogsturnoverrate1_emd
  	,amt_cogsturnoverrate2_emd
  	,amt_cogsturnoverrate3_emd

      ,dim_bwhierarchycountryid
  	,dim_clusterid
  	,dim_DateidExpiryDate
  	,dim_countryhierarid
  	,dim_countryhierpsid
  	,dim_commercialviewid
  	,dd_isconsigned
  	,ct_baseuomratioPCcustom
  	,ct_countmaterialsafetystock
  	,CT_WIPQTY
  	,AMT_WIPBALANCE
  	,dim_specialstockid
  	,dd_batch_status_qkz
  	,amt_StockValueAmt
  	,dim_productionschedulerid
  	,DIM_ITEMSTATUSID
  	,dim_batchstatusid
       )
  SELECT
           CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
  		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
  		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
  		,ct_primary_transaction_quantity

  		,CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
  		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
  		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
  		 END amt_primary_transaction_quantity  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost

  		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
  		,1 -- d_uom.dim_unitofmeasureid
  		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
  		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
  		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
  		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
  		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
  		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
          ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
  		,f_inv.dw_insert_date
  		,f_inv.dw_update_date
  		,f_inv.fact_ora_invonhandid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhandid
  		,7
  		,0
  		,0
  		,0
  		,ct_primary_transaction_quantity restricted_qty
  		,1
  		,1
  		,dd_standart_cost
  		,amt_cogsactualrate_emd
  		,amt_cogsfixedrate_emd
  		,amt_cogsfixedplanrate_emd
  		,amt_cogsplanrate_emd
  		,amt_cogsprevyearfixedrate_emd
  		,amt_cogsprevyearrate_emd
  		,amt_cogsprevyearto1_emd
  		,amt_cogsprevyearto2_emd
  		,amt_cogsprevyearto3_emd
  		,amt_cogsturnoverrate1_emd
  		,amt_cogsturnoverrate2_emd
  		,amt_cogsturnoverrate3_emd

          ,dim_ora_bwhierarchycountryid
  		,dim_ora_clusterid
  		,dim_ora_expiration_dateid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_expiration_dateid
  		,dim_countryhierarid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierarid
  	    ,dim_countryhierpsid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierpsid
  		,dim_ora_commercialviewid
  		,ifnull(dd_isconsigned,0)
  		,1
  		,ct_countmaterialsafetystock
  		,CONVERT (DECIMAL (18,5),f_inv.CT_WIP_QUANTITY ) CT_WIP_QUANTITY
  		,CONVERT (DECIMAL (18,5),f_inv.amt_wip ) amt_wip
  		,1
  		,'Not Set' dd_batch_status_qkz
  		,CASE WHEN amt_cogsfixedplanrate_emd =0 THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END * dd_standart_cost
  		,1 dim_productionschedulerid
  		,1 DIM_ITEMSTATUSID
  		,1 dim_batchstatusid
   FROM emdoracle4ae.fact_ora_invonhand f_inv
        ,emdoracle4ae.dim_projectsource p

  WHERE ct_primary_transaction_quantity <> 999999999999999868928
   and f_inv.source_id ='WIP';

   DELETE FROM emd586.fact_inventoryaging WHERE dim_projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


   insert into emd586.fact_inventoryaging
   (
   FACT_INVENTORYAGINGID
   ,CT_STOCKQTY
   ,AMT_STOCKVALUEAMT
   ,CT_LASTRECEIVEDQTY
   ,DD_BATCHNO
   ,DD_VALUATIONTYPE
   ,DD_DOCUMENTNO
   ,DD_DOCUMENTITEMNO
   ,DD_MOVEMENTTYPE
   ,DIM_STORAGELOCENTRYDATEID
   ,DIM_LASTRECEIVEDDATEID
   ,DIM_PARTID
   ,DIM_PLANTID
   ,DIM_STORAGELOCATIONID
   ,DIM_COMPANYID
   ,DIM_VENDORID
   ,DIM_CURRENCYID
   ,DIM_STOCKTYPEID
   ,DIM_SPECIALSTOCKID
   ,DIM_PURCHASEORGID
   ,DIM_PURCHASEGROUPID
   ,DIM_PRODUCTHIERARCHYID
   ,DIM_UNITOFMEASUREID
   ,DIM_MOVEMENTINDICATORID
   ,DIM_CONSUMPTIONTYPEID
   ,DIM_COSTCENTERID
   ,DIM_DOCUMENTSTATUSID
   ,DIM_DOCUMENTTYPEID
   ,DIM_INCOTERMID
   ,DIM_ITEMCATEGORYID
   ,DIM_ITEMSTATUSID
   ,DIM_TERMID
   ,DIM_PURCHASEMISCID
   ,AMT_STOCKVALUEAMT_GBL
   ,CT_TOTALRESTRICTEDSTOCK
   ,CT_STOCKINQINSP
   ,CT_BLOCKEDSTOCK
   ,CT_STOCKINTRANSFER
   ,CT_UNRESTRICTEDCONSGNSTOCK
   ,CT_RESTRICTEDCONSGNSTOCK
   ,CT_BLOCKEDCONSGNSTOCK
   ,CT_CONSGNSTOCKINQINSP
   ,CT_BLOCKEDSTOCKRETURNS
   ,AMT_BLOCKEDSTOCKAMT
   ,AMT_BLOCKEDSTOCKAMT_GBL
   ,AMT_STOCKINQINSPAMT
   ,AMT_STOCKINQINSPAMT_GBL
   ,AMT_STOCKINTRANSFERAMT
   ,AMT_STOCKINTRANSFERAMT_GBL
   ,AMT_UNRESTRICTEDCONSGNSTOCKAMT
   ,AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL
   ,AMT_STDUNITPRICE
   ,AMT_STDUNITPRICE_GBL
   ,DIM_STOCKCATEGORYID
   ,CT_STOCKINTRANSIT
   ,AMT_STOCKINTRANSITAMT
   ,AMT_STOCKINTRANSITAMT_GBL
   ,DIM_SUPPLYINGPLANTID
   ,AMT_MTLDLVRCOST
   ,AMT_OVERHEADCOST
   ,AMT_OTHERCOST
   ,AMT_WIPBALANCE
   ,CT_WIPAGING
   ,AMT_MOVINGAVGPRICE
   ,AMT_PREVIOUSPRICE
   ,AMT_COMMERICALPRICE1
   ,AMT_PLANNEDPRICE1
   ,AMT_PREVPLANNEDPRICE
   ,AMT_CURPLANNEDPRICE
   ,DIM_DATEIDPLANNEDPRICE2
   ,DIM_DATEIDLASTCHANGEDPRICE
   ,DIM_CUSTOMERID
   ,AMT_ONHAND
   ,AMT_ONHAND_GBL
   ,CT_POOPENQTY
   ,CT_SOOPENQTY
   ,AMT_EXCHANGERATE_GBL
   ,AMT_EXCHANGERATE
   ,DIM_PROFITCENTERID
   ,CT_WIPQTY
   ,DIM_PARTSALESID
   ,DIM_DATEIDTRANSACTION
   ,DIM_DATEIDACTUALRELEASE
   ,DIM_DATEIDACTUALSTART
   ,DD_PRODORDERNUMBER
   ,DD_PRODORDERITEMNO
   ,DD_PLANNEDORDERNO
   ,DIM_PRODUCTIONORDERSTATUSID
   ,DIM_PRODUCTIONORDERTYPEID
   ,DIM_CURRENCYID_TRA
   ,DIM_CURRENCYID_GBL
   ,CT_GRQTY_LATE30
   ,CT_GRQTY_31_60
   ,CT_GRQTY_61_90
   ,CT_GRQTY_91_180
   ,CT_GIQTY_LATE30
   ,CT_GIQTY_31_60
   ,CT_GIQTY_61_90
   ,CT_GIQTY_91_180
   ,DIM_DATEIDEXPIRYDATE
   ,CT_GLOBALONHANDAMT_MERCK
   ,CT_GLOBALEXTTOTALCOST_MERCK
   ,CT_LOCALONHANDAMT_MERCK
   ,CT_LOCALEXTTOTALCOST_MERCK
   ,AMT_GBLSTDPRICE_MERCK
   ,AMT_STDPRICEPMRA_MERCK
   ,DW_INSERT_DATE
   ,DW_UPDATE_DATE
   ,DIM_PROJECTSOURCEID
   ,DIM_MDG_PARTID
   ,DIM_BWPARTID
   ,DIM_BWPRODUCTHIERARCHYID
   ,CT_TOTVALSTKQTY
   ,AMT_PRICEUNIT
   ,AMT_COGSACTUALRATE_EMD
   ,AMT_COGSFIXEDRATE_EMD
   ,AMT_COGSFIXEDPLANRATE_EMD
   ,AMT_COGSPLANRATE_EMD
   ,AMT_COGSPREVYEARFIXEDRATE_EMD
   ,AMT_COGSPREVYEARRATE_EMD
   ,AMT_COGSPREVYEARTO1_EMD
   ,AMT_COGSPREVYEARTO2_EMD
   ,AMT_COGSPREVYEARTO3_EMD
   ,AMT_COGSTURNOVERRATE1_EMD
   ,AMT_COGSTURNOVERRATE2_EMD
   ,AMT_COGSTURNOVERRATE3_EMD
   ,DD_INVCOVFLAG_EMD
   ,CT_AVGFCST_EMD
   ,DIM_DATEIDLASTPROCESSED
   ,DIM_BWHIERARCHYCOUNTRYID
   ,DIM_CLUSTERID
   ,AMT_EXCHANGERATE_CUSTOM
   ,DIM_DATEOFMANUFACTURE
   ,DIM_DATEOFLASTGOODSRECEIPT
   ,CT_BASEUOMRATIOKG
   ,DD_JDACONTAINER
   ,DD_MATERIALSTATUS
   ,CT_BASEUOMRATIOPC
   ,DIM_COUNTRYHIERPSID
   ,DIM_COUNTRYHIERARID
   ,DIM_COMMERCIALVIEWID
   ,DD_ISCONSIGNED
   ,CT_COUNTMATERIALSAFETYSTOCK
   ,DD_AGECLUSTER
   ,DD_AGECLUSTERSORT
   ,DD_BATCH_STATUS_QKZ
   ,CT_ORDERITEMQTY
   ,CT_GRQTY
   ,DIM_VALUATIONCLASSID
   ,DIM_GSAMAPIMPORTID
   ,DIM_CUSTOMERIDSHIPTO
   ,DIM_CUSTOMERIDSOLDTO
   ,CT_DEPENDENTDEMANDKG
   ,CT_INTRANSITKG
   ,CT_INVENTORYQKZ01
   ,DD_DATEOFEXPIRATION
   ,DD_DATEOFEXPIRATIONSORT
   ,DIM_PRODUCTIONSCHEDULERID
   ,DD_QKZDESCRIPTION
   ,DD_BATCHSTATUS
   ,CT_BASEUOMRATIOPCCUSTOM
   ,DD_VALUATIONCLASS
   ,DD_FREEAVAILABLESTOCK
   ,DIM_DATEMES_EXPIRYDATEID
   ,DIM_DATEMES_MANUFACTUREID
   ,DIM_BATCHSTATUSID
   ,CT_AVGPRICE
   ,CT_MERCKHC_SAPFORECAST
   ,DD_SAPMESMANAGECLUSTER
   ,DD_SAPMESMANAGECLUSTERSORT
   ,DD_SAPMESEXPAGECLUSTER
   ,DD_SAPMESEXPAGECLUSTERSORT
   ,CT_OPENORDERQTYUOM
   ,AMT_OPENORDER
   ,CT_SALESORDERLINECOUNT
   ,AMT_STOCKOUTPREVIOUSDAY
   ,DD_STOCKOUTDELTA
   ,CT_NUMBEROFPALLETS
   ,DD_FREETEXTATTRIB1
   ,DD_FREETEXTATTRIB2
   ,DD_FREETEXTATTRIB3
   ,DD_FREETEXTATTRIB4
   ,AMT_WIP_NEW
   ,CT_STOCKINTRANSITPM
   ,CT_TOTALDEMANDKG
   ,CT_DEPENDENTDEMANDKGPM3
   ,CT_AVGTOTALDEMANDKG
   )
   select
   FACT_INVENTORYAGINGID
   ,ifnull(CT_STOCKQTY,0)
   ,ifnull(AMT_STOCKVALUEAMT,0)
   ,ifnull(CT_LASTRECEIVEDQTY,0)
   ,ifnull(DD_BATCHNO,'Not Set')
   ,ifnull(DD_VALUATIONTYPE,'Not Set')
   ,ifnull(DD_DOCUMENTNO,'Not Set')
   ,ifnull(DD_DOCUMENTITEMNO,0)
   ,ifnull(DD_MOVEMENTTYPE,'Not Set')
   ,ifnull(DIM_STORAGELOCENTRYDATEID,1)
   ,ifnull(DIM_LASTRECEIVEDDATEID,1)
   ,ifnull(DIM_PARTID,1)
   ,ifnull(DIM_PLANTID,1)
   ,ifnull(DIM_STORAGELOCATIONID,1)
   ,ifnull(DIM_COMPANYID,1)
   ,ifnull(DIM_VENDORID,1)
   ,ifnull(DIM_CURRENCYID,1)
   ,ifnull(DIM_STOCKTYPEID,1)
   ,ifnull(DIM_SPECIALSTOCKID,1)
   ,ifnull(DIM_PURCHASEORGID,1)
   ,ifnull(DIM_PURCHASEGROUPID,1)
   ,ifnull(DIM_PRODUCTHIERARCHYID,1)
   ,ifnull(DIM_UNITOFMEASUREID,1)
   ,ifnull(DIM_MOVEMENTINDICATORID,1)
   ,ifnull(DIM_CONSUMPTIONTYPEID,1)
   ,ifnull(DIM_COSTCENTERID,1)
   ,ifnull(DIM_DOCUMENTSTATUSID,1)
   ,ifnull(DIM_DOCUMENTTYPEID,1)
   ,ifnull(DIM_INCOTERMID,1)
   ,ifnull(DIM_ITEMCATEGORYID,1)
   ,ifnull(DIM_ITEMSTATUSID,1)
   ,ifnull(DIM_TERMID,1)
   ,ifnull(DIM_PURCHASEMISCID,1)
   ,ifnull(AMT_STOCKVALUEAMT_GBL,0)
   ,ifnull(CT_TOTALRESTRICTEDSTOCK,0)
   ,ifnull(CT_STOCKINQINSP,0)
   ,ifnull(CT_BLOCKEDSTOCK,0)
   ,ifnull(CT_STOCKINTRANSFER,0)
   ,ifnull(CT_UNRESTRICTEDCONSGNSTOCK,0)
   ,ifnull(CT_RESTRICTEDCONSGNSTOCK,0)
   ,ifnull(CT_BLOCKEDCONSGNSTOCK,0)
   ,ifnull(CT_CONSGNSTOCKINQINSP,0)
   ,ifnull(CT_BLOCKEDSTOCKRETURNS,0)
   ,ifnull(AMT_BLOCKEDSTOCKAMT,0)
   ,ifnull(AMT_BLOCKEDSTOCKAMT_GBL,0)
   ,ifnull(AMT_STOCKINQINSPAMT,0)
   ,ifnull(AMT_STOCKINQINSPAMT_GBL,0)
   ,ifnull(AMT_STOCKINTRANSFERAMT,0)
   ,ifnull(AMT_STOCKINTRANSFERAMT_GBL,0)
   ,ifnull(AMT_UNRESTRICTEDCONSGNSTOCKAMT,0)
   ,ifnull(AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,0)
   ,ifnull(AMT_STDUNITPRICE,0)
   ,ifnull(AMT_STDUNITPRICE_GBL,0)
   ,ifnull(DIM_STOCKCATEGORYID,1)
   ,ifnull(CT_STOCKINTRANSIT,0)
   ,ifnull(AMT_STOCKINTRANSITAMT,0)
   ,ifnull(AMT_STOCKINTRANSITAMT_GBL,0)
   ,ifnull(DIM_SUPPLYINGPLANTID,1)
   ,ifnull(AMT_MTLDLVRCOST,0)
   ,ifnull(AMT_OVERHEADCOST,0)
   ,ifnull(AMT_OTHERCOST,0)
   ,ifnull(AMT_WIPBALANCE,0)
   ,ifnull(CT_WIPAGING,0)
   ,ifnull(AMT_MOVINGAVGPRICE,0)
   ,ifnull(AMT_PREVIOUSPRICE,0)
   ,ifnull(AMT_COMMERICALPRICE1,0)
   ,ifnull(AMT_PLANNEDPRICE1,0)
   ,ifnull(AMT_PREVPLANNEDPRICE,0)
   ,ifnull(AMT_CURPLANNEDPRICE,0)
   ,ifnull(DIM_DATEIDPLANNEDPRICE2,1)
   ,ifnull(DIM_DATEIDLASTCHANGEDPRICE,1)
   ,ifnull(DIM_CUSTOMERID,1)
   ,ifnull(AMT_ONHAND,0)
   ,ifnull(AMT_ONHAND_GBL,0)
   ,ifnull(CT_POOPENQTY,0)
   ,ifnull(CT_SOOPENQTY,0)
   ,ifnull(AMT_EXCHANGERATE_GBL,1)
   ,ifnull(AMT_EXCHANGERATE,1)
   ,ifnull(DIM_PROFITCENTERID,1)
   ,ifnull(CT_WIPQTY,0)
   ,ifnull(DIM_PARTSALESID,1)
   ,ifnull(DIM_DATEIDTRANSACTION,1)
   ,ifnull(DIM_DATEIDACTUALRELEASE,1)
   ,ifnull(DIM_DATEIDACTUALSTART,1)
   ,ifnull(DD_PRODORDERNUMBER,'Not Set')
   ,ifnull(DD_PRODORDERITEMNO,NULL)
   ,ifnull(DD_PLANNEDORDERNO,'Not Set')
   ,ifnull(DIM_PRODUCTIONORDERSTATUSID,1)
   ,ifnull(DIM_PRODUCTIONORDERTYPEID,1)
   ,ifnull(DIM_CURRENCYID_TRA,1)
   ,ifnull(DIM_CURRENCYID_GBL,1)
   ,ifnull(CT_GRQTY_LATE30,0)
   ,ifnull(CT_GRQTY_31_60,0)
   ,ifnull(CT_GRQTY_61_90,0)
   ,ifnull(CT_GRQTY_91_180,0)
   ,ifnull(CT_GIQTY_LATE30,0)
   ,ifnull(CT_GIQTY_31_60,0)
   ,ifnull(CT_GIQTY_61_90,0)
   ,ifnull(CT_GIQTY_91_180,0)
   ,ifnull(DIM_DATEIDEXPIRYDATE,1)
   ,ifnull(CT_GLOBALONHANDAMT_MERCK,0)
   ,ifnull(CT_GLOBALEXTTOTALCOST_MERCK,0)
   ,ifnull(CT_LOCALONHANDAMT_MERCK,0)
   ,ifnull(CT_LOCALEXTTOTALCOST_MERCK,0)
   ,ifnull(AMT_GBLSTDPRICE_MERCK,0)
   ,ifnull(AMT_STDPRICEPMRA_MERCK,0)
   ,ifnull(DW_INSERT_DATE,CURRENT_TIMESTAMP)
   ,ifnull(DW_UPDATE_DATE,CURRENT_TIMESTAMP)
   ,ifnull(DIM_PROJECTSOURCEID,1)
   ,ifnull(DIM_MDG_PARTID,1)
   ,ifnull(DIM_BWPARTID,1)
   ,ifnull(DIM_BWPRODUCTHIERARCHYID,1)
   ,ifnull(CT_TOTVALSTKQTY,0)
   ,ifnull(AMT_PRICEUNIT,0)
   ,ifnull(AMT_COGSACTUALRATE_EMD,0)
   ,ifnull(AMT_COGSFIXEDRATE_EMD,0)
   ,ifnull(AMT_COGSFIXEDPLANRATE_EMD,0)
   ,ifnull(AMT_COGSPLANRATE_EMD,0)
   ,ifnull(AMT_COGSPREVYEARFIXEDRATE_EMD,0)
   ,ifnull(AMT_COGSPREVYEARRATE_EMD,0)
   ,ifnull(AMT_COGSPREVYEARTO1_EMD,0)
   ,ifnull(AMT_COGSPREVYEARTO2_EMD,0)
   ,ifnull(AMT_COGSPREVYEARTO3_EMD,0)
   ,ifnull(AMT_COGSTURNOVERRATE1_EMD,0)
   ,ifnull(AMT_COGSTURNOVERRATE2_EMD,0)
   ,ifnull(AMT_COGSTURNOVERRATE3_EMD,0)
   ,ifnull(DD_INVCOVFLAG_EMD,'Not Set')
   ,ifnull(CT_AVGFCST_EMD,0)
   ,ifnull(DIM_DATEIDLASTPROCESSED,1)
   ,ifnull(DIM_BWHIERARCHYCOUNTRYID,1)
   ,ifnull(DIM_CLUSTERID,1)
   ,ifnull(AMT_EXCHANGERATE_CUSTOM,1)
   ,ifnull(DIM_DATEOFMANUFACTURE,1)
   ,ifnull(DIM_DATEOFLASTGOODSRECEIPT,1)
   ,ifnull(CT_BASEUOMRATIOKG,0)
   ,ifnull(DD_JDACONTAINER,'Not Set')
   ,ifnull(DD_MATERIALSTATUS,'Not Set')
   ,ifnull(CT_BASEUOMRATIOPC,1)
   ,ifnull(DIM_COUNTRYHIERPSID,1)
   ,ifnull(DIM_COUNTRYHIERARID,1)
   ,ifnull(DIM_COMMERCIALVIEWID,1)
   ,ifnull(DD_ISCONSIGNED,'Not Set')
   ,ifnull(CT_COUNTMATERIALSAFETYSTOCK,0)
   ,ifnull(DD_AGECLUSTER,'Not Set')
   ,ifnull(DD_AGECLUSTERSORT,'Not Set')
   ,ifnull(DD_BATCH_STATUS_QKZ,'Not Set')
   ,ifnull(CT_ORDERITEMQTY,0)
   ,ifnull(CT_GRQTY,0)
   ,ifnull(DIM_VALUATIONCLASSID,1)
   ,ifnull(DIM_GSAMAPIMPORTID,1)
   ,ifnull(DIM_CUSTOMERIDSHIPTO,1)
   ,ifnull(DIM_CUSTOMERIDSOLDTO,1)
   ,ifnull(CT_DEPENDENTDEMANDKG,0)
   ,ifnull(CT_INTRANSITKG,0)
   ,ifnull(CT_INVENTORYQKZ01,0)
   ,ifnull(DD_DATEOFEXPIRATION,'Not Set')
   ,ifnull(DD_DATEOFEXPIRATIONSORT,'Not Set')
   ,ifnull(DIM_PRODUCTIONSCHEDULERID,1)
   ,ifnull(DD_QKZDESCRIPTION,'Not Set')
   ,ifnull(DD_BATCHSTATUS,'Not Set')
   ,ifnull(CT_BASEUOMRATIOPCCUSTOM,1)
   ,ifnull(DD_VALUATIONCLASS,'Not Set')
   ,ifnull(DD_FREEAVAILABLESTOCK,'Not Set')
   ,ifnull(DIM_DATEMES_EXPIRYDATEID,1)
   ,ifnull(DIM_DATEMES_MANUFACTUREID,1)
   ,ifnull(DIM_BATCHSTATUSID,1)
   ,ifnull(CT_AVGPRICE,0)
   ,ifnull(CT_MERCKHC_SAPFORECAST,0)
   ,ifnull(DD_SAPMESMANAGECLUSTER,'Not Set')
   ,ifnull(DD_SAPMESMANAGECLUSTERSORT,'Not Set')
   ,ifnull(DD_SAPMESEXPAGECLUSTER,'Not Set')
   ,ifnull(DD_SAPMESEXPAGECLUSTERSORT,'Not Set')
   ,ifnull(CT_OPENORDERQTYUOM,0)
   ,ifnull(AMT_OPENORDER,0)
   ,ifnull(CT_SALESORDERLINECOUNT,0)
   ,ifnull(AMT_STOCKOUTPREVIOUSDAY,0)
   ,ifnull(DD_STOCKOUTDELTA,'Not Set')
   ,ifnull(CT_NUMBEROFPALLETS,0)
   ,ifnull(DD_FREETEXTATTRIB1,'Not Set')
   ,ifnull(DD_FREETEXTATTRIB2,'Not Set')
   ,ifnull(DD_FREETEXTATTRIB3,'Not Set')
   ,ifnull(DD_FREETEXTATTRIB4,'Not Set')
   ,ifnull(AMT_WIP_NEW,0)
   ,ifnull(CT_STOCKINTRANSITPM,0)
   ,ifnull(CT_TOTALDEMANDKG,0)
   ,ifnull(CT_DEPENDENTDEMANDKGPM3,0)
   ,ifnull(CT_AVGTOTALDEMANDKG,0)
   from tmp_fact_inventoryaging_Oracle;
