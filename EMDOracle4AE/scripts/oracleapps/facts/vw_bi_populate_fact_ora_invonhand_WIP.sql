open schema emdoracle4ae;

TRUNCATE TABLE temp_WipDiscrete;

INSERT INTO temp_WipDiscrete
select msi.INVENTORY_ITEM_ID, wdj.ORGANIZATION_ID , we.wip_entity_name,we.wip_entity_id ,


SUM(nvl(wdj.START_QUANTITY,0) - ( nvl(wdj.quantity_completed,0) + nvl(wdj.QUANTITY_SCRAPPED,0) )) open_qty,

sum(round(

         nvl(wpb.pl_material_in,0)

       + nvl(wpb.pl_material_overhead_in,0)

       + nvl(wpb.tl_resource_in,0)+ nvl(wpb.pl_resource_in,0)

       + nvl(wpb.tl_outside_processing_in,0)+ nvl(wpb.pl_outside_processing_in,0)

       + nvl(wpb.tl_overhead_in,0)+ nvl(wpb.pl_overhead_in,0)

       + nvl(wpb.tl_scrap_in,0), 2)

      )                                                                -

  sum(round(

         nvl(wpb.tl_material_out,0) + nvl(wpb.pl_material_out,0)

       + nvl(wpb.pl_material_overhead_out,0) + nvl(wpb.tl_material_overhead_out,0)

       + nvl(wpb.tl_resource_out,0) + nvl(wpb.pl_resource_out,0)

       + nvl(wpb.tl_outside_processing_out,0) + nvl(wpb.pl_outside_processing_out,0)

       + nvl(wpb.tl_overhead_out,0) + nvl(wpb.pl_overhead_out,0)

       + nvl(wpb.tl_scrap_out,0), 2)

     )                                                                 -

  sum(round(

         nvl(wpb.tl_material_var,0) + nvl(wpb.pl_material_var,0)

       + nvl(wpb.pl_material_overhead_var,0) + nvl(wpb.tl_material_overhead_var,0)

       + nvl(wpb.tl_resource_var,0) + nvl(wpb.pl_resource_var,0)

       + nvl(wpb.tl_outside_processing_var,0) + nvl(wpb.pl_outside_processing_var,0)

       + nvl(wpb.tl_overhead_var,0) + nvl(wpb.pl_overhead_var,0)

       + nvl(wpb.tl_scrap_var,0), 2))               wip_value

from  ora_mtl_parameters mp,

      ora_wip_period_balances wpb,

      ora_WIP_DISCRETE_JOBS wdj,

      ora_wip_entities we,

      ora_MTL_SYSTEM_ITEMS msi

where mp.organization_id = wdj.organization_id

and  wpb.wip_entity_id (+) = wdj.WIP_ENTITY_ID

and wdj.organization_id in (195,76,380,2,700,13253,13689,15782,22868,24528)


and wdj.status_type in (3,6 ) -- released - hold

and wdj.wip_entity_id = we.wip_entity_id

and wdj.JOB_TYPE = 1

and wdj.PRIMARY_ITEM_ID = msi.INVENTORY_ITEM_ID

and msi.ORGANIZATION_ID = 190

group by  msi.INVENTORY_ITEM_ID, wdj.ORGANIZATION_ID , we.wip_entity_name,we.wip_entity_id

having  sum(round(

         nvl(wpb.pl_material_in,0)

       + nvl(wpb.pl_material_overhead_in,0)

       + nvl(wpb.tl_resource_in,0)+ nvl(wpb.pl_resource_in,0)

       + nvl(wpb.tl_outside_processing_in,0)+ nvl(wpb.pl_outside_processing_in,0)

       + nvl(wpb.tl_overhead_in,0)+ nvl(wpb.pl_overhead_in,0)

       + nvl(wpb.tl_scrap_in,0), 2)

      )                                                                -

  sum(round(

         nvl(wpb.tl_material_out,0) + nvl(wpb.pl_material_out,0)

       + nvl(wpb.pl_material_overhead_out,0) + nvl(wpb.tl_material_overhead_out,0)

       + nvl(wpb.tl_resource_out,0) + nvl(wpb.pl_resource_out,0)

       + nvl(wpb.tl_outside_processing_out,0) + nvl(wpb.pl_outside_processing_out,0)

       + nvl(wpb.tl_overhead_out,0) + nvl(wpb.pl_overhead_out,0)

       + nvl(wpb.tl_scrap_out,0), 2)

     )                                                                 -

  sum(round(

         nvl(wpb.tl_material_var,0) + nvl(wpb.pl_material_var,0)

       + nvl(wpb.pl_material_overhead_var,0) + nvl(wpb.tl_material_overhead_var,0)

       + nvl(wpb.tl_resource_var,0) + nvl(wpb.pl_resource_var,0)

       + nvl(wpb.tl_outside_processing_var,0) + nvl(wpb.pl_outside_processing_var,0)

       + nvl(wpb.tl_overhead_var,0) + nvl(wpb.pl_overhead_var,0)

       + nvl(wpb.tl_scrap_var,0), 2))     <> 0

order by 1,3,4;

delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_invonhand';
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_invonhand',IFNULL(MAX(fact_ora_invonhandid),0)
FROM fact_ora_invonhand;

INSERT INTO Fact_ora_invonhand
(
fact_ora_invonhandid,DIM_ORA_INV_PRODUCTID,DIM_ORA_PRODUCTID,DIM_ORA_INV_ORGID,DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,DIM_ORA_MTL_SEC_INVENTORYID,CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,CT_PRIMARY_TRANSACTION_QUANTITY,DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,fact_ORA_INVMTL_UPDATE_TRXID,DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,DIM_ORA_ORGANIZATION_TYPEID,DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,DIM_ORA_PLANNING_ORGANIZATIONID,DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,CT_SECONDARY_TRANSACTION_QUANTITY,DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,CT_RESERVATION_QUANTITY,DD_SHIP_READY_FLAG,CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,CT_DEMAND_QUANTITY,dd_aging_flag,CREATE_TRANSACTION_ID,UPDATE_TRANSACTION_ID,RESERVATION_ID,INVENTORY_ITEM_ID,
ORGANIZATION_ID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID, DD_OWNING_ORGANIZATION_ID,
DD_PLANNING_ORGANIZATION_ID,DD_DATE_RECEIVED--,dd_isconsigned
,amt_wip,ct_wip_qTy, dim_ora_expiration_dateid
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_invonhand' ) + ROW_NUMBER() OVER(order by '') fact_ora_invonhandid,
1 AS DIM_ORA_INV_PRODUCTID,
1 AS DIM_ORA_PRODUCTID,
1 AS DIM_ORA_INV_ORGID,
1 AS DIM_ORA_MTL_ITEM_REVID,
1 AS DIM_ORA_MTL_ITEM_LOCATORID,
1 AS DIM_ORA_MTL_SEC_INVENTORYID,
0 AS CT_TRANSACTION_QUANTITY,
0 AS DD_TRANSACTION_UOM_CODE,
0 AS CT_PRIMARY_TRANSACTION_QUANTITY,
1 AS DIM_ORA_DATE_RECEIVEDID,
0 AS fact_ORA_INVMTL_CREATE_TRXID,
0 AS fact_ORA_INVMTL_UPDATE_TRXID,
1 AS DIM_ORA_DATE_ORIG_RECEIVEDID,
1 AS DD_CONTAINERIZED_FLAG,
1 as DIM_ORA_ORGANIZATION_TYPEID,
1 AS DIM_ORA_OWNING_ORGANIZATIONID,
1 AS DIM_ORA_OWNINING_TP_TYPEID,
1 AS DIM_ORA_PLANNING_ORGANIZATIONID,
1 AS DIM_ORA_PLANNING_TP_TYPEID,
'Not Set' AS DD_SECONDARY_UOM_CODE,
0 AS CT_SECONDARY_TRANSACTION_QUANTITY,
s.wip_entity_name AS DD_LOT_NUMBER,
1 AS DIM_ORA_DATE_REQUIREMENTID,
0 AS CT_RESERVATION_QUANTITY,
0 AS DD_SHIP_READY_FLAG,
0 AS CT_DETAILED_QUANTITY,
1 AS DIM_ORA_DATE_DEMAND_SHIPID,
0 AS CT_DEMAND_QUANTITY,
'Not Set' dd_aging_flag,
ifnull(s.wip_entity_id,0) AS CREATE_TRANSACTION_ID,
ifnull(s.wip_entity_id,0) AS UPDATE_TRANSACTION_ID,
0 AS RESERVATION_ID,
s.INVENTORY_ITEM_ID AS INVENTORY_ITEM_ID,
s.ORGANIZATION_ID AS ORGANIZATION_ID,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'WIP' AS SOURCE_ID,
0 DD_OWNING_ORGANIZATION_ID,
0 DD_PLANNING_ORGANIZATION_ID,
'9999-12-31 00:00:00.000000' DD_DATE_RECEIVED,
--0 dd_isconsigned,
wip_value as amt_wip,
s.open_qty,
1 dim_ora_expiration_dateid

FROM temp_WipDiscrete s left join FACT_ORA_INVONHAND F
ON f.inventory_item_id = s.inventory_item_id
AND f.organization_id = s.organization_id
 WHERE F.INVENTORY_ITEM_ID IS NULL
 AND F.ORGANIZATION_ID IS NULL
 AND F.SOURCE_ID = 'WIP';

UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_INV_PRODUCTID = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, temp_WipDiscrete S JOIN DIM_ORA_INVPRODUCT D
ON CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'||  CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID
and f.DD_LOT_NUMBER=s.wip_entity_name and f.CREATE_TRANSACTION_ID=s.wip_entity_id
AND f.source_id='WIP';


UPDATE fact_ora_invonhand F
SET
F.DIM_ORA_INV_ORGID= D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand F, temp_WipDiscrete S JOIN DIM_ORA_BUSINESS_ORG D
ON 'INV' ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID
and f.DD_LOT_NUMBER=s.wip_entity_name and f.CREATE_TRANSACTION_ID=s.wip_entity_id
AND f.source_id='WIP';

-- Update for mdg_part: Global Material Number --
UPDATE fact_ora_invonhand f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM fact_ora_invonhand f_inv, dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.dim_ora_inv_productid = d_invp.dim_ora_invproductid
 AND  d_invp.mdm_globalid = d_mp.partnumber
 and f_inv.source_id='WIP'
 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;


 /*update dim_ora_bwproducthierarchyid*/
 UPDATE fact_ora_invonhand f
 SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_invonhand f, dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inv_productid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND dp.productgroupsbu = bw.productgroup
AND to_date('2017-12-28') BETWEEN bw.upperhierstartdate AND bw.upperhierenddate -- TO BE REPLACED AFTER 01.01.2017: '2016-12-20' WITH CURRENT_DATE
and f.source_id='WIP'
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_invonhand f, dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inv_productid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
and f.source_id='WIP'
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

/* COGS FROM SAP */
TRUNCATE TABLE csv_cogs;
INSERT INTO csv_cogs
SELECT * FROM EMDTempoCC4.csv_cogs;

UPDATE fact_ora_invonhand f
SET f.amt_cogsactualrate_emd = IFNULL(c.Z_GCACTF,0)
	,f.amt_cogsfixedrate_emd = IFNULL(c.Z_GCFIXF,0)
	,f.amt_cogsfixedplanrate_emd = IFNULL(c.Z_GCPLFF,0)
	,f.amt_cogsplanrate_emd = IFNULL(c.Z_GCPLRF,0)
	,f.amt_cogsprevyearfixedrate_emd = IFNULL(c.Z_GCPYFF,0)
	,f.amt_cogsprevyearrate_emd = IFNULL(c.Z_GCPYRF,0)
	,f.amt_cogsprevyearto1_emd = IFNULL(c.Z_GCPYTF1,0)
	,f.amt_cogsprevyearto2_emd = IFNULL(c.Z_GCPYTF2,0)
	,f.amt_cogsprevyearto3_emd = IFNULL(c.Z_GCPYTF3,0)
	,f.amt_cogsturnoverrate1_emd = IFNULL(c.Z_GCTOF1,0)
	,f.amt_cogsturnoverrate2_emd = IFNULL(c.Z_GCTOF2,0)
	,f.amt_cogsturnoverrate3_emd = IFNULL(c.Z_GCTOF3,0)
FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_business_org dc, csv_cogs c
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
	AND f.dim_ora_inv_orgid = dc.dim_ora_business_orgid
	AND TRIM(LEADING 0 FROM c.PRODUCT)= dp.partnumber
	and f.source_id='WIP'
	AND c.Z_REPUNIT = dc.cmg_number;


UPDATE fact_ora_invonhand f
SET f.dim_ora_clusterid = dc.dim_ora_clusterid
FROM fact_ora_invonhand f, dim_ora_mdg_product mdg, dim_ora_bwproducthierarchy ph, dim_ora_cluster dc
WHERE f.dim_ora_mdg_productid = mdg.dim_mdg_partid
	AND f.dim_ora_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	AND businesssector = 'BS-02'
	AND f.source_id='WIP'
	AND mdg.primary_production_location = dc.primary_manufacturing_site
	AND f.dim_ora_clusterid <> dc.dim_ora_clusterid;


UPDATE fact_ora_invonhand f
SET f.dim_countryhierpsid = dch.dim_countryhierpsid
FROM fact_ora_invonhand f, dim_ora_business_org dc, dim_ora_countryhierps dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND f.source_id='WIP'
AND f.dim_countryhierpsid <> dch.dim_countryhierpsid;

UPDATE fact_ora_invonhand f
SET f.dim_countryhierarid  = dch.dim_countryhierarid
FROM fact_ora_invonhand f, dim_ora_business_org dc, dim_ora_countryhierar dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND f.source_id='WIP'
AND f.dim_countryhierarid <> dch.dim_countryhierarid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business IN ('DIV.32') AND dim_con.internalhierarchyid = '548WP23VF6R8ICTMIER1FW7R0'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='WIP'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV.31','DIV.35','DIV.34') AND dim_con.internalhierarchyid = '548YXNWXHSJ2Y19OSUCHY2CL7'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='WIP'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88') AND dim_con.internalhierarchyid = '549HWG7F0J8VZ1P8YIYH1CPQ3'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='WIP'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV-87') AND dim_con.internalhierarchyid = '54G8L55S3E80Q2SXZYJAELHKR'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='WIP'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;