/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mtl_supply*/
/*SELECT * FROM fact_ora_mtl_supply*/

TRUNCATE TABLE fact_ora_mtl_supply;

/*initialize NUMBER_FOUNTAIN_fact_ora_mtl_supply*/
delete from NUMBER_FOUNTAIN_fact_ora_mtl_supply where table_name = 'fact_ora_mtl_supply';	

INSERT INTO NUMBER_FOUNTAIN_fact_ora_mtl_supply
SELECT 'fact_ora_mtl_supply',IFNULL(MAX(fact_ora_mtl_supplyid),0)
FROM fact_ora_mtl_supply;

DROP TABLE IF EXISTS ora_mtl_supply_tmp;

create table ora_mtl_supply_tmp as
SELECT
supply_type_code,
supply_source_id,
last_updated_by,
last_update_date,
creation_date,
created_by,
req_line_id,
po_release_id,
po_line_location_id,
po_distribution_id,
rcv_transaction_id,
item_id,
item_revision,
category_id,
quantity,
unit_of_measure,
to_org_primary_quantity,
to_org_primary_uom,
receipt_date,
need_by_date,
expected_delivery_date,
destination_type_code,
location_id,
from_organization_id,
from_subinventory,
to_organization_id,
to_subinventory,
intransit_owning_org_id,
mrp_primary_quantity,
mrp_primary_uom,
mrp_expected_delivery_date,
mrp_destination_type_code,
mrp_to_organization_id,
mrp_to_subinventory,
change_flag,
change_type,
cost_group_id,
exclude_from_planning,
receipt_source_code,
shipped_date,
quantity_shipped,
quantity_received,
shipment_unit_of_measure,
item_description,
shipment_line_status_code,
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_fact_ora_mtl_supply WHERE TABLE_NAME = 'fact_ora_mtl_supply' ),0) + ROW_NUMBER() OVER(order by '') fact_ora_mtl_supplyid
FROM ora_mtl_supply;

TRUNCATE TABLE ora_mtl_supply;

INSERT INTO ora_mtl_supply
(
supply_type_code,supply_source_id,last_updated_by,last_update_date,creation_date,created_by,req_line_id,po_release_id,po_line_location_id,po_distribution_id,rcv_transaction_id,
item_id,item_revision,category_id,quantity,unit_of_measure,to_org_primary_quantity,to_org_primary_uom,receipt_date,need_by_date,expected_delivery_date,
destination_type_code,location_id,from_organization_id,from_subinventory,to_organization_id,to_subinventory,intransit_owning_org_id,mrp_primary_quantity,
mrp_primary_uom,mrp_expected_delivery_date,mrp_destination_type_code,mrp_to_organization_id,mrp_to_subinventory,change_flag,change_type,cost_group_id,
exclude_from_planning,receipt_source_code,shipped_date,quantity_shipped,quantity_received,shipment_unit_of_measure,item_description,
shipment_line_status_code,fact_ora_mtl_supplyid
)
SELECT
supply_type_code,
supply_source_id,
last_updated_by,
last_update_date,
creation_date,
created_by,
req_line_id,
po_release_id,
po_line_location_id,
po_distribution_id,
rcv_transaction_id,
item_id,
item_revision,
category_id,
quantity,
unit_of_measure,
to_org_primary_quantity,
to_org_primary_uom,
receipt_date,
need_by_date,
expected_delivery_date,
destination_type_code,
location_id,
from_organization_id,
from_subinventory,
to_organization_id,
to_subinventory,
intransit_owning_org_id,
mrp_primary_quantity,
mrp_primary_uom,
mrp_expected_delivery_date,
mrp_destination_type_code,
mrp_to_organization_id,
mrp_to_subinventory,
change_flag,
change_type,
cost_group_id,
exclude_from_planning,
receipt_source_code,
shipped_date,
quantity_shipped,
quantity_received,
shipment_unit_of_measure,
item_description,
shipment_line_status_code,
fact_ora_mtl_supplyid
FROM ora_mtl_supply_tmp;

DROP TABLE IF EXISTS ora_mtl_supply_tmp;

/*update fact columns*/
UPDATE fact_ora_mtl_supply T
SET
dd_rcv_transactionid = ifnull(S.RCV_TRANSACTION_ID,0),
dd_category_id = ifnull(S.CATEGORY_ID,0),
dd_cost_group_id = ifnull(S.COST_GROUP_ID,0),
dd_exclude_from_planning = ifnull(S.EXCLUDE_FROM_PLANNING,'Not Set'),
dd_supply_type_code = S.SUPPLY_TYPE_CODE,
dd_supply_source_id = S.SUPPLY_SOURCE_ID,
dd_po_release_id = ifnull(S.PO_RELEASE_ID,0),
dd_item_revision = ifnull(S.ITEM_REVISION,'Not Set'),
ct_quantity = S.QUANTITY,
dd_unit_of_measure = S.UNIT_OF_MEASURE,
ct_to_org_primary_quantity = ifnull(S.TO_ORG_PRIMARY_QUANTITY,0),
dd_to_org_primary_uom = ifnull(S.TO_ORG_PRIMARY_UOM,'Not Set'),
dd_destination_type_code = S.DESTINATION_TYPE_CODE,
dd_from_subinventory = ifnull(S.FROM_SUBINVENTORY,'Not Set'),
dd_to_subinventory = ifnull(S.TO_SUBINVENTORY,'Not Set'),
ct_mrp_primary_quantity = ifnull(S.MRP_PRIMARY_QUANTITY,0),
dd_mrp_primary_uom = ifnull(S.MRP_PRIMARY_UOM,'Not Set'),
dd_mrp_destination_type_code = ifnull(S.MRP_DESTINATION_TYPE_CODE,'Not Set'),
dd_mrp_to_subinventory = ifnull(S.MRP_TO_SUBINVENTORY,'Not Set'),
dd_change_flag = ifnull(S.CHANGE_FLAG,'Not Set'),
dd_change_type = ifnull(S.CHANGE_TYPE,'Not Set'),
dd_receipt_source_code = ifnull(S.RECEIPT_SOURCE_CODE,0),
dd_shipped_date = ifnull(S.SHIPPED_DATE,'1970-01-01'),
ct_quantity_shipped = ifnull(S.QUANTITY_SHIPPED,0),
ct_quantity_received = ifnull(S.QUANTITY_RECEIVED,0),
dd_shipment_unit_of_measure = ifnull(S.SHIPMENT_UNIT_OF_MEASURE,'Not Set'),
dd_item_description = ifnull(S.ITEM_DESCRIPTION,'Not Set'),
dd_shipment_line_status_code = ifnull(S.SHIPMENT_LINE_STATUS_CODE,'Not Set'),
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
FROM fact_ora_mtl_supply T,ora_mtl_supply S
WHERE
T.fact_ora_mtl_supplyid = S.fact_ora_mtl_supplyid;

/*insert new rows*/
INSERT INTO fact_ora_mtl_supply
(
fact_ora_mtl_supplyid,
dim_ora_to_organizationid,dim_ora_last_updated_byid,dim_ora_date_last_updateid,dim_ora_created_byid,dim_ora_date_creationid,dim_ora_req_lineid,
dim_ora_po_line_locationid,dim_ora_po_distributionid,dd_rcv_transactionid,dim_ora_item_id,dd_category_id,dim_ora_date_receiptid,dim_ora_date_need_byid,
dim_ora_date_expected_deliveryid,dim_ora_locationid,dim_ora_from_organizationid,dim_ora_intrasit_owning_orgid,dim_ora_date_mrp_expected_deliveryid,
dim_ora_mrp_to_organizationid,dd_cost_group_id,dd_exclude_from_planning,dd_supply_type_code,dd_supply_source_id,dd_po_release_id,dd_item_revision,ct_quantity,
dd_unit_of_measure,ct_to_org_primary_quantity,dd_to_org_primary_uom,dd_destination_type_code,dd_from_subinventory,dd_to_subinventory,ct_mrp_primary_quantity,
dd_mrp_primary_uom,dd_mrp_destination_type_code,dd_mrp_to_subinventory,dd_change_flag,dd_change_type,dd_receipt_source_code,dd_shipped_date,ct_quantity_shipped,
ct_quantity_received,dd_shipment_unit_of_measure,dd_item_description,dd_shipment_line_status_code,
amt_exchangerate,amt_exchangerate_gbl,dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
S.fact_ora_mtl_supplyid,
1 AS dim_ora_to_organizationid,
1 AS dim_ora_last_updated_byid,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_created_byid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_req_lineid,
1 AS dim_ora_po_line_locationid,
1 AS dim_ora_po_distributionid,
ifnull(S.RCV_TRANSACTION_ID,0) AS dd_rcv_transactionid,
1 AS dim_ora_item_id,
ifnull(S.CATEGORY_ID,0) AS dd_category_id,
1 AS dim_ora_date_receiptid,
1 AS dim_ora_date_need_byid,
1 AS dim_ora_date_expected_deliveryid,
1 AS dim_ora_locationid,
1 AS dim_ora_from_organizationid,
1 AS dim_ora_intrasit_owning_orgid,
1 AS dim_ora_date_mrp_expected_deliveryid,
1 AS dim_ora_mrp_to_organizationid,
ifnull(S.COST_GROUP_ID,0) AS dd_cost_group_id,
ifnull(S.EXCLUDE_FROM_PLANNING,'Not Set') AS dd_exclude_from_planning,
S.SUPPLY_TYPE_CODE AS dd_supply_type_code,
S.SUPPLY_SOURCE_ID AS dd_supply_source_id,
ifnull(S.PO_RELEASE_ID,0) AS dd_po_release_id,
ifnull(S.ITEM_REVISION,'Not Set') AS dd_item_revision,
S.QUANTITY AS ct_quantity,
S.UNIT_OF_MEASURE AS dd_unit_of_measure,
ifnull(S.TO_ORG_PRIMARY_QUANTITY,0) AS ct_to_org_primary_quantity,
ifnull(S.TO_ORG_PRIMARY_UOM,'Not Set') AS dd_to_org_primary_uom,
S.DESTINATION_TYPE_CODE AS dd_destination_type_code,
ifnull(S.FROM_SUBINVENTORY,'Not Set') AS dd_from_subinventory,
ifnull(S.TO_SUBINVENTORY,'Not Set') AS dd_to_subinventory,
ifnull(S.MRP_PRIMARY_QUANTITY,0) AS ct_mrp_primary_quantity,
ifnull(S.MRP_PRIMARY_UOM,'Not Set') AS dd_mrp_primary_uom,
ifnull(S.MRP_DESTINATION_TYPE_CODE,'Not Set') AS dd_mrp_destination_type_code,
ifnull(S.MRP_TO_SUBINVENTORY,'Not Set') AS dd_mrp_to_subinventory,
ifnull(S.CHANGE_FLAG,'Not Set') AS dd_change_flag,
ifnull(S.CHANGE_TYPE,'Not Set') AS dd_change_type,
ifnull(S.RECEIPT_SOURCE_CODE,'Not Set') AS dd_receipt_source_code,
ifnull(S.SHIPPED_DATE,'1970-01-01') AS dd_shipped_date,
ifnull(S.QUANTITY_SHIPPED,0) AS ct_quantity_shipped,
ifnull(S.QUANTITY_RECEIVED,0) AS ct_quantity_received,
ifnull(S.SHIPMENT_UNIT_OF_MEASURE,'Not Set') AS dd_shipment_unit_of_measure,
ifnull(S.ITEM_DESCRIPTION,'Not Set') AS dd_item_description,
ifnull(S.SHIPMENT_LINE_STATUS_CODE,'Not Set') AS dd_shipment_line_status_code,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mtl_supply S LEFT JOIN fact_ora_mtl_supply T
ON T.fact_ora_mtl_supplyid = S.fact_ora_mtl_supplyid
WHERE T.fact_ora_mtl_supplyid is null;


/*UPDATE dim_ora_to_organizationid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_to_organizationid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_ORA_BUSINESS_ORG D
ON CONVERT(VARCHAR(200),'INV') ||'~'|| CONVERT(VARCHAR(200),S.TO_ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_to_organizationid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_last_updated_byid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_last_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID AND d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_last_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_DATE D ON to_date(S.LAST_UPDATE_DATE) = D.datevalue
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID AND d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_DATE D ON to_date(S.CREATION_DATE) = D.DATEVALUE
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_req_lineid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_req_lineid = D.FACT_ORA_PURCHASEREQUISITIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN FACT_ORA_PURCHASEREQUISITION D ON S.REQ_LINE_ID = D.REQUISITION_LINE_ID AND d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_req_lineid <> D.FACT_ORA_PURCHASEREQUISITIONID;

/*UPDATE dim_ora_po_line_locationid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_po_line_locationid = D.DIM_ORA_HR_LOCATIONSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_ORA_HR_LOCATIONS D ON S.PO_LINE_LOCATION_ID = D.KEY_ID AND d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_po_line_locationid <> D.DIM_ORA_HR_LOCATIONSID;

/*UPDATE dim_ora_po_distributionid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_po_distributionid = D.FACT_ORA_PURCHASEORDERCOSTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN FACT_ORA_PURCHASEORDERCOST D ON S.PO_DISTRIBUTION_ID = D.PO_DISTRIBUTION_ID AND d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_po_distributionid <> D.FACT_ORA_PURCHASEORDERCOSTID;

/*UPDATE dim_ora_item_id*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_item_id = D.DIM_ORA_PRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN dim_ora_Product D ON S.ITEM_ID = D.KEY_ID AND d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_item_id <> D.DIM_ORA_PRODUCTID;

/*UPDATE dim_ora_date_receiptid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_date_receiptid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_DATE D ON to_date(S.RECEIPT_DATE) = D.DATEVALUE
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_date_receiptid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_need_byid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_date_need_byid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_DATE D ON to_date(S.NEED_BY_DATE) = D.DATEVALUE
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_date_need_byid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_expected_deliveryid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_date_expected_deliveryid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_DATE D ON to_date(S.EXPECTED_DELIVERY_DATE) = D.DATEVALUE
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_date_expected_deliveryid <> D.DIM_DATEID;

/*UPDATE dim_ora_locationid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_locationid = D.DIM_ORA_MTL_ITEM_LOCATIONSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_ORA_MTL_ITEM_LOCATIONS D ON S.LOCATION_ID = D.KEY_ID AND d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_locationid <> D.DIM_ORA_MTL_ITEM_LOCATIONSID;

/*UPDATE dim_ora_from_organizationid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_from_organizationid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_ORA_BUSINESS_ORG D
ON CONVERT(VARCHAR(200),'INV') ||'~'|| CONVERT(VARCHAR(200),S.FROM_ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_from_organizationid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_intrasit_owning_orgid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_intrasit_owning_orgid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_ORA_BUSINESS_ORG D
ON CONVERT(VARCHAR(200),'INV') ||'~'|| CONVERT(VARCHAR(200),S.INTRANSIT_OWNING_ORG_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_intrasit_owning_orgid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_date_mrp_expected_deliveryid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_date_mrp_expected_deliveryid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_DATE D ON to_date(S.MRP_EXPECTED_DELIVERY_DATE) = D.DATEVALUE
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_date_mrp_expected_deliveryid <> D.DIM_DATEID;

/*UPDATE dim_ora_mrp_to_organizationid*/
UPDATE fact_ora_mtl_supply F
SET
F.dim_ora_mrp_to_organizationid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mtl_supply F,ora_mtl_supply S JOIN DIM_ORA_BUSINESS_ORG D
ON CONVERT(VARCHAR(200),'INV') ||'~'|| CONVERT(VARCHAR(200),S.MRP_TO_ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mtl_supplyid =  S.fact_ora_mtl_supplyid and
F.dim_ora_mrp_to_organizationid <> D.DIM_ORA_BUSINESS_ORGID;
