/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mrp_forecast_updates*/
/*SELECT * FROM fact_ora_mrp_forecast_updates*/

/*initialize NUMBER_FOUNTAIN_fact_ora_mrp_forecast_updates*/
TRUNCATE TABLE fact_ora_mrp_forecast_updates;

delete from NUMBER_FOUNTAIN_fact_ora_mrp_forecast_updates where table_name = 'fact_ora_mrp_forecast_updates';

INSERT INTO NUMBER_FOUNTAIN_fact_ora_mrp_forecast_updates
SELECT 'fact_ora_mrp_forecast_updates',IFNULL(MAX(fact_ora_mrp_forecast_updatesid),0)
FROM fact_ora_mrp_forecast_updates;


/*generate unique key for ora_mrp_forecast_updates*/
DROP TABLE IF EXISTS ora_mrp_forecast_updates_tmp;

create table ora_mrp_forecast_updates_tmp as
select
CREATED_BY,
INVENTORY_ITEM_ID,
ORGANIZATION_ID,
FORECAST_DESIGNATOR,
UPDATE_SALES_ORDER,
SALES_ORDER_SCHEDULE_DATE,
FORECAST_UPDATE_DATE,
SALES_ORDER_QUANTITY,
DEMAND_CLASS,
UPDATE_QUANTITY,
CUSTOMER_ID,
SHIP_ID,
BILL_ID,
LINE_NUM,
UPDATE_SEQ_NUM,
TRANSACTION_ID,
LAST_UPDATE_DATE,
LAST_UPDATED_BY,
CREATION_DATE,
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN_fact_ora_mrp_forecast_updates WHERE TABLE_NAME = 'fact_ora_mrp_forecast_updates' ),0) + ROW_NUMBER() OVER(order by '') fact_ora_mrp_forecast_updatesid
from ora_mrp_forecast_updates;

TRUNCATE TABLE ora_mrp_forecast_updates;

INSERT INTO ora_mrp_forecast_updates
(
CREATED_BY,INVENTORY_ITEM_ID,ORGANIZATION_ID,FORECAST_DESIGNATOR,UPDATE_SALES_ORDER,
SALES_ORDER_SCHEDULE_DATE,FORECAST_UPDATE_DATE,SALES_ORDER_QUANTITY,DEMAND_CLASS,UPDATE_QUANTITY,CUSTOMER_ID,SHIP_ID,BILL_ID,
LINE_NUM,UPDATE_SEQ_NUM,TRANSACTION_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,fact_ora_mrp_forecast_updatesid
)
select
CREATED_BY,
INVENTORY_ITEM_ID,
ORGANIZATION_ID,
FORECAST_DESIGNATOR,
UPDATE_SALES_ORDER,
SALES_ORDER_SCHEDULE_DATE,
FORECAST_UPDATE_DATE,
SALES_ORDER_QUANTITY,
DEMAND_CLASS,
UPDATE_QUANTITY,
CUSTOMER_ID,
SHIP_ID,
BILL_ID,
LINE_NUM,
UPDATE_SEQ_NUM,
TRANSACTION_ID,
LAST_UPDATE_DATE,
LAST_UPDATED_BY,
CREATION_DATE,
fact_ora_mrp_forecast_updatesid
from ora_mrp_forecast_updates_tmp;

DROP TABLE IF EXISTS ora_mrp_forecast_updates_tmp;


/*update fact columns*/
UPDATE fact_ora_mrp_forecast_updates T
SET
DD_UPDATE_SALES_ORDER = S.UPDATE_SALES_ORDER,
CT_SALES_ORDER_QUANTITY = S.SALES_ORDER_QUANTITY,
DD_DEMAND_CLASS = ifnull(S.DEMAND_CLASS,'Not Set'),
CT_UPDATE_QUANTITY = S.UPDATE_QUANTITY,
DD_LINE_NUM = ifnull(S.LINE_NUM,'Not Set'),
DD_UPDATE_SEQ_NUM = ifnull(S.UPDATE_SEQ_NUM,0),
DD_TRANSACTION_ID = S.TRANSACTION_ID,
amt_exchangerate = 1,
amt_exchangerate_gbl = 1,
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
FROM fact_ora_mrp_forecast_updates T, ora_mrp_forecast_updates S
WHERE
T.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid;

/*insert new rows*/
INSERT INTO fact_ora_mrp_forecast_updates
(
fact_ora_mrp_forecast_updatesid,
DIM_ORA_CREATED_BYID,DIM_ORA_INVENTORY_ITEMID,DIM_ORA_ORGANIZATIONID,DIM_ORA_FORECAST_DESIGNATORID,DD_UPDATE_SALES_ORDER,
DIM_ORA_DATE_SALES_ORDER_SCHEDULEID,DIM_ORA_DATE_FORECAST_UPDATEID,CT_SALES_ORDER_QUANTITY,DD_DEMAND_CLASS,CT_UPDATE_QUANTITY,
DIM_ORA_CUSTOMERID,DIM_ORA_SHIPID,DIM_ORA_BILLID,DD_LINE_NUM,
DD_UPDATE_SEQ_NUM,DD_TRANSACTION_ID,DIM_ORA_DATE_LAST_UPDATEID,DIM_ORA_LAST_UPDATED_BYID,DIM_ORA_DATE_CREATIONID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
S.fact_ora_mrp_forecast_updatesid,
1 AS DIM_ORA_CREATED_BYID,
1 AS DIM_ORA_INVENTORY_ITEMID,
1 AS DIM_ORA_ORGANIZATIONID,
1 AS DIM_ORA_FORECAST_DESIGNATORID,
S.UPDATE_SALES_ORDER AS DD_UPDATE_SALES_ORDER,
1 AS DIM_ORA_DATE_SALES_ORDER_SCHEDULEID,
1 AS DIM_ORA_DATE_FORECAST_UPDATEID,
S.SALES_ORDER_QUANTITY AS CT_SALES_ORDER_QUANTITY,
ifnull(S.DEMAND_CLASS,'Not Set') AS DD_DEMAND_CLASS,
S.UPDATE_QUANTITY AS CT_UPDATE_QUANTITY,
1 AS DIM_ORA_CUSTOMERID,
1 AS DIM_ORA_SHIPID,
1 AS DIM_ORA_BILLID,
ifnull(S.LINE_NUM,'Not Set') AS DD_LINE_NUM,
ifnull(S.UPDATE_SEQ_NUM,0) AS DD_UPDATE_SEQ_NUM,
S.TRANSACTION_ID AS DD_TRANSACTION_ID,
1 AS DIM_ORA_DATE_LAST_UPDATEID,
1 AS DIM_ORA_LAST_UPDATED_BYID,
1 AS DIM_ORA_DATE_CREATIONID,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'INSERT' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mrp_forecast_updates S LEFT JOIN fact_ora_mrp_forecast_updates F
ON F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid
WHERE F.fact_ora_mrp_forecast_updatesid is null;

/*UPDATE DIM_ORA_CREATED_BYID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_CREATED_BYID = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_CREATED_BYID <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_INVENTORY_ITEMID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_INVENTORY_ITEMID = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_ORA_INVPRODUCT D ON CONVERT(VARCHAR(200),S.ORGANIZATION_ID) ||'~'||  CONVERT(VARCHAR(200),S.INVENTORY_ITEM_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_INVENTORY_ITEMID <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE DIM_ORA_ORGANIZATIONID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_ORGANIZATIONID = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_ORA_MTL_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_ORGANIZATIONID <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE DIM_ORA_FORECAST_DESIGNATORID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_FORECAST_DESIGNATORID = D.DIM_ORA_MRP_FORECAST_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_ORA_MRP_FORECAST_DESIGNATORS D
ON CONVERT(VARCHAR(200),S.FORECAST_DESIGNATOR) ||'~'|| CONVERT(VARCHAR(200),S.ORGANIZATION_ID) = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_FORECAST_DESIGNATORID <> D.DIM_ORA_MRP_FORECAST_DESIGNATORSID;

/*UPDATE DIM_ORA_DATE_SALES_ORDER_SCHEDULEID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_DATE_SALES_ORDER_SCHEDULEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_DATE D ON to_date(S.SALES_ORDER_SCHEDULE_DATE) = D.DATEVALUE
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_DATE_SALES_ORDER_SCHEDULEID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE_FORECAST_UPDATEID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_DATE_FORECAST_UPDATEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_DATE D ON to_date(S.FORECAST_UPDATE_DATE) = D.DATEVALUE
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_DATE_FORECAST_UPDATEID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_CUSTOMERID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_CUSTOMERID = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_ORA_HZ_CUSTACCOUNTS D ON S.CUSTOMER_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_CUSTOMERID <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE DIM_ORA_SHIPID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_SHIPID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.SHIP_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_SHIPID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_ORA_BILLID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_BILLID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.BILL_ID = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_BILLID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_ORA_DATE_LAST_UPDATEID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_DATE_LAST_UPDATEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_DATE D ON to_date(S.LAST_UPDATE_DATE) = D.DATEVALUE
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_DATE_LAST_UPDATEID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_LAST_UPDATED_BYID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_LAST_UPDATED_BYID = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_LAST_UPDATED_BYID <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_DATE_CREATIONID*/
UPDATE fact_ora_mrp_forecast_updates F
SET
F.DIM_ORA_DATE_CREATIONID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_mrp_forecast_updates F,ora_mrp_forecast_updates S JOIN DIM_DATE D ON to_date(S.CREATION_DATE) = D.DATEVALUE
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_DATE_CREATIONID <> D.DIM_DATEID;
/*update dim_ora_mdg_productid*/
UPDATE fact_ora_mrp_forecast_updates f_inv
SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid
FROM fact_ora_mrp_forecast_updates f_inv,dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp
WHERE f_inv.dim_ora_inventory_itemid = d_invp.dim_ora_invproductid
 AND  d_invp.mdm_globalid = d_mp.partnumber
 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;

 /*update dim_ora_bwproducthierarchyid*/
 UPDATE fact_ora_mrp_forecast_updates f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_mrp_forecast_updates f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inventory_itemid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND dp.productgroupsbu = bw.productgroup
AND current_date BETWEEN bw.upperhierstartdate AND bw.upperhierenddate
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

UPDATE fact_ora_mrp_forecast_updates f
SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM fact_ora_mrp_forecast_updates f,dim_ora_invproduct dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_inventory_itemid = dp.dim_ora_invproductid
AND dp.producthierarchy = bw.lowerhierarchycode
AND bw.productgroup = 'Not Set'
AND f.dim_ora_bwproducthierarchyid = 1
AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;
