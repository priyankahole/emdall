
INSERT INTO emd586.fact_inventoryaging
(
     amt_exchangerate
	,amt_exchangerate_gbl
	,ct_blockedstock
	,ct_stockqty
	,amt_OnHand
	,dd_batchno
	,dim_unitofmeasureid
	,dim_storagelocentrydateid
	,dim_plantid
	,dim_partid
	,dim_storagelocationid
	,dim_mdg_partid
	,dim_bwproducthierarchyid
	,dim_companyid
	,dw_insert_date
	,dw_update_date
	,fact_inventoryagingid
    ,dim_projectsourceid
    ,ct_StockInQInsp
    ,ct_StockInTransit
    ,ct_StockInTransfer
    ,ct_TotalRestrictedStock
    ,dim_itemcategoryid
    ,dim_stockcategoryid
    ,amt_StdUnitPrice
    ,amt_WIPBalance
    ,ct_WIPQty

	,amt_cogsactualrate_emd
	,amt_cogsfixedrate_emd
	,amt_cogsfixedplanrate_emd
	,amt_cogsplanrate_emd
	,amt_cogsprevyearfixedrate_emd
	,amt_cogsprevyearrate_emd
	,amt_cogsprevyearto1_emd
	,amt_cogsprevyearto2_emd
	,amt_cogsprevyearto3_emd
	,amt_cogsturnoverrate1_emd
	,amt_cogsturnoverrate2_emd
	,amt_cogsturnoverrate3_emd

    ,dim_bwhierarchycountryid
	,dim_clusterid
	,dim_DateidExpiryDate
	,dim_countryhierarid
	,dim_countryhierpsid
	,dim_commercialviewid
	,dd_isconsigned
	,ct_baseuomratioPCcustom
	,ct_countmaterialsafetystock
	,dim_specialstockid
	,dd_batch_status_qkz
	,amt_StockValueAmt
	,dim_productionschedulerid
	,DIM_ITEMSTATUSID
	,dim_batchstatusid
     )
SELECT
         CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
		,CASE WHEN f_inv.dd_restricted_unrestricted_flag = 'N' or f_inv.dd_restricted_unrestricted_flag = 'Not Set' THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
           ELSE 0
		 END ct_primary_transaction_quantity

		,CASE WHEN f_inv.source_id = 'OPM' THEN amt_opm_onhand_value-- contains already unit price in the amt
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		 END amt_primary_transaction_quantity  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost

		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
		,1 dim_unitofmeasureid
		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
        ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
		,f_inv.dw_insert_date
		,f_inv.dw_update_date
		,f_inv.fact_ora_invonhandid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhandid
		,p.dim_projectsourceid
		,0
		,0
		,0
		,CASE WHEN f_inv.dd_restricted_unrestricted_flag = 'Y' THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END restricted_qty
		,1
		,1
		,dd_standart_cost
		,0
		,0

		,amt_cogsactualrate_emd
		,amt_cogsfixedrate_emd
		,amt_cogsfixedplanrate_emd
		,amt_cogsplanrate_emd
		,amt_cogsprevyearfixedrate_emd
		,amt_cogsprevyearrate_emd
		,amt_cogsprevyearto1_emd
		,amt_cogsprevyearto2_emd
		,amt_cogsprevyearto3_emd
		,amt_cogsturnoverrate1_emd
		,amt_cogsturnoverrate2_emd
		,amt_cogsturnoverrate3_emd

        ,dim_ora_bwhierarchycountryid
		,dim_ora_clusterid
		,dim_ora_expiration_dateid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_expiration_dateid
		,dim_countryhierarid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierarid
	    ,dim_countryhierpsid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierpsid
		,dim_ora_commercialviewid
		,ifnull(dd_isconsigned,0)
		,1
		,ct_countmaterialsafetystock
		,1
		,'Not Set' dd_batch_status_qkz
		,CASE WHEN f_inv.source_id = 'OPM' and (f_inv.dd_restricted_unrestricted_flag = 'N' or f_inv.dd_restricted_unrestricted_flag = 'Not Set') THEN CONVERT (DECIMAL (18,5),amt_opm_onhand_value)
           ELSE 0
		 END amt_StockValueAmt
        ,1 dim_productionschedulerid
		,1 DIM_ITEMSTATUSID
		,1 dim_batchstatusid
 FROM emdoracle4ae.fact_ora_invonhand f_inv
     ,emdoracle4ae.dim_projectsource p
     
WHERE f_inv.source_id='OPM'
 AND ct_primary_transaction_quantity <> 999999999999999868928;
