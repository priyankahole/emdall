/*set session authorization oracle_data*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_invonhand_daily';
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_invonhand_daily',IFNULL(MAX(fact_ora_invonhand_dailyid),0)
FROM fact_ora_invonhand_daily;

/*DELETE FROM fact_ora_invonhand_daily*/
/*select count(*) from fact_ora_invonhand*/
/*select count(*) from fact_ora_invonhand_daily*/
/*select * from fact_ora_invonhand_daily*/
/*select count(*) from ORA_MTL_ONHAND_RESERVE*/

/*delete rows*/
/*DELETE FROM fact_ora_invonhand_daily*/
DELETE FROM fact_ora_invonhand_daily
where to_date(SNAPSHOT_DATE) = to_date(current_timestamp) - 1;

/*insert new rows*/
INSERT INTO fact_ora_invonhand_daily
(
fact_ora_invonhand_dailyid,
fact_ORA_INVONHANDid,DIM_ORA_INV_PRODUCTID,DIM_ORA_PRODUCTID,DIM_ORA_INV_ORGID,DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,DIM_ORA_MTL_SEC_INVENTORYID,CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,CT_PRIMARY_TRANSACTION_QUANTITY,DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,fact_ORA_INVMTL_UPDATE_TRXID,DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,DIM_ORA_ORGANIZATION_TYPEID,DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,DIM_ORA_PLANNING_ORGANIZATIONID,DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,CT_SECONDARY_TRANSACTION_QUANTITY,DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,CT_RESERVATION_QUANTITY,DD_SHIP_READY_FLAG,CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,CT_DEMAND_QUANTITY,CREATE_TRANSACTION_ID,UPDATE_TRANSACTION_ID,RESERVATION_ID,INVENTORY_ITEM_ID,
ORGANIZATION_ID,SNAPSHOT_DATE,DIM_ORA_DATE_SNAPSHOTID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
,dim_ora_mdg_productid
,dim_ora_bwproducthierarchyid
,dd_standart_cost
,amt_cogsactualrate_emd
,amt_cogsfixedrate_emd
,amt_cogsfixedplanrate_emd
,amt_cogsplanrate_emd
,amt_cogsprevyearfixedrate_emd
,amt_cogsprevyearrate_emd
,amt_cogsprevyearto1_emd
,amt_cogsprevyearto2_emd
,amt_cogsprevyearto3_emd
,amt_cogsturnoverrate1_emd
,amt_cogsturnoverrate2_emd
,amt_cogsturnoverrate3_emd

,dim_ora_plantid
,dim_ora_bwhierarchycountryid
,dim_ora_clusterid
,dim_countryhierpsid
,dim_countryhierarid
,ct_countmaterialsafetystock
,dd_isconsigned
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_invonhand_daily' ) + ROW_NUMBER() OVER(order by '') fact_ora_invonhand_dailyid,
fact_ora_invonhandid,
DIM_ORA_INV_PRODUCTID,
DIM_ORA_PRODUCTID,
DIM_ORA_INV_ORGID,
DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,
DIM_ORA_MTL_SEC_INVENTORYID,
CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,
CT_PRIMARY_TRANSACTION_QUANTITY,
DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,
fact_ORA_INVMTL_UPDATE_TRXID,
DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,
DIM_ORA_ORGANIZATION_TYPEID,
DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,
DIM_ORA_PLANNING_ORGANIZATIONID,
DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,
CT_SECONDARY_TRANSACTION_QUANTITY,
DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,
CT_RESERVATION_QUANTITY,
DD_SHIP_READY_FLAG,
CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,
CT_DEMAND_QUANTITY,
CREATE_TRANSACTION_ID,
UPDATE_TRANSACTION_ID,
RESERVATION_ID,
INVENTORY_ITEM_ID,
ORGANIZATION_ID,
current_timestamp - 1 SNAPSHOT_DATE,
1 DIM_ORA_DATE_SNAPSHOTID,
amt_exchangerate,
amt_exchangerate_gbl,
DW_INSERT_DATE,
DW_UPDATE_DATE,
rowstartdate,
rowenddate,
rowiscurrent,
rowchangereason,
SOURCE_ID
,dim_ora_mdg_productid
,dim_ora_bwproducthierarchyid
,dd_standart_cost
,amt_cogsactualrate_emd
,amt_cogsfixedrate_emd
,amt_cogsfixedplanrate_emd
,amt_cogsplanrate_emd
,amt_cogsprevyearfixedrate_emd
,amt_cogsprevyearrate_emd
,amt_cogsprevyearto1_emd
,amt_cogsprevyearto2_emd
,amt_cogsprevyearto3_emd
,amt_cogsturnoverrate1_emd
,amt_cogsturnoverrate2_emd
,amt_cogsturnoverrate3_emd

,dim_ora_plantid
,dim_ora_bwhierarchycountryid
,dim_ora_clusterid
,dim_countryhierpsid
,dim_countryhierarid
,ct_countmaterialsafetystock
,dd_isconsigned

FROM fact_ORA_INVONHAND F;

/*UPDATE DIM_ORA_DATE_SNAPSHOTID*/
UPDATE fact_ora_invonhand_daily F

SET
F.DIM_ORA_DATE_SNAPSHOTID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
FROM fact_ora_invonhand_daily F,DIM_DATE D
WHERE to_date(F.SNAPSHOT_DATE) = D.DATEVALUE
AND F.DIM_ORA_DATE_SNAPSHOTID =1;

/*delete daily snapshots of the previous months and keep only the snapshot of the last day of month*/
/*
delete from fact_ora_invonhand_daily
where to_date(SNAPSHOT_DATE) in
(
select dt1.datevalue from
(select extract(year from datevalue) "year", extract(month from datevalue) "month", datevalue
from dim_date
/*and calendaryear = 2014
) dt1
join
(select extract(year from datevalue) "year", extract(month from datevalue) "month", max(datevalue) max_datevalue from dim_date
where to_date(current_timestamp)>=datevalue
/*and calendaryear = 2014
group by extract(year from datevalue), extract(month from datevalue) ) dt2
on dt1."year" = dt2."year" and dt1."month" = dt2."month" and dt1.datevalue< dt2.max_datevalue
where dt1."month" <> extract(month from to_date(current_timestamp))
)
*/

/*1Week Amt Change*/
DELETE FROM fact_ora_invonhand_daily_TMP;

INSERT INTO fact_ora_invonhand_daily_TMP(dim_ora_inv_productid
                                     ,dim_ora_inv_orgid
                                     ,dim_ora_mtl_item_locatorid
                                     ,snapshotdate
                                     ,ct_primary_transaction_quantity)
   SELECT
           f_inv.dim_ora_inv_productid
          ,f_inv.dim_ora_inv_orgid
          ,f_inv.dim_ora_date_orig_receivedid
          ,d.datevalue SnapshotDate
          ,SUM(CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
		            THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		            ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		       END )amt_primary_transaction_quantity
     FROM emdoracle4ae.fact_ora_invonhand_daily f_inv, dim_date d
    WHERE DIM_ORA_DATE_SNAPSHOTID = dim_dateid
     AND d.datevalue IN ( current_date - 7, current_date)
     AND ct_primary_transaction_quantity <> 999999999999999868928
   GROUP BY f_inv.dim_ora_inv_productid,
          f_inv.dim_ora_inv_orgid,
          f_inv.dim_ora_date_orig_receivedid,
          d.datevalue;

UPDATE fact_ora_invonhand_daily_TMP ih1
SET amt_primary_transact_qty_1weekchange = ih2.ct_primary_transaction_quantity - ih1.ct_primary_transaction_quantity
FROM
	 fact_ora_invonhand_daily_TMP ih1
	,fact_ora_invonhand_daily_TMP ih2
WHERE ih1.dim_ora_inv_productid = ih2.dim_ora_inv_productid
AND ih1.dim_ora_mtl_item_locatorid = ih2.dim_ora_mtl_item_locatorid
AND ih1.dim_ora_inv_orgid = ih2.dim_ora_inv_orgid
AND ih1.SnapshotDate = current_date - 1
AND ih1.SnapshotDate - ( INTERVAL '7' DAY) = ih2.SnapshotDate;


UPDATE fact_ora_invonhand_daily f_invd
SET f_invd.amt_primary_transact_qty_1weekchange = IFNULL(f_tmp.amt_primary_transact_qty_1weekchange,0)
FROM
	 fact_ora_invonhand_daily f_invd
	,fact_ora_invonhand_daily_TMP f_tmp
	,dim_date d
	,dim_date t
WHERE f_invd.dim_ora_inv_productid = f_tmp.dim_ora_inv_productid
  AND f_invd.dim_ora_mtl_item_locatorid = f_tmp.dim_ora_mtl_item_locatorid
  AND f_invd.dim_ora_inv_orgid = f_tmp.dim_ora_inv_orgid
  AND f_invd.dim_ora_date_snapshotid = d.dim_dateid
  AND d.datevalue = current_date - 1
  AND f_tmp.snapshotdate = t.datevalue
  AND f_invd.dim_ora_date_snapshotid = t.dim_dateid;

TRUNCATE TABLE TMP_LATEST_DATE;
INSERT INTO TMP_LATEST_DATE
SELECT MAX(snp.datevalue) max_dt
FROM fact_ora_invonhand_daily f_ih
  INNER JOIN dim_date snp ON f_ih.dim_ora_date_snapshotid = snp.dim_dateid
UNION
SELECT MAX(snp.datevalue)-1 max_dt
FROM fact_ora_invonhand_daily f_ih
  INNER JOIN dim_date snp ON f_ih.dim_ora_date_snapshotid = snp.dim_dateid
UNION
SELECT MAX(snp.datevalue)-7 max_dt
FROM fact_ora_invonhand_daily f_ih
  INNER JOIN dim_date snp ON f_ih.dim_ora_date_snapshotid = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-1) max_dt
FROM fact_ora_invonhand_daily f_ih
  INNER JOIN dim_date snp ON f_ih.dim_ora_date_snapshotid = snp.dim_dateid;

TRUNCATE TABLE tmp_1day_change;
INSERT INTO tmp_1day_change 
SELECT
       TO_DATE(f.snapshot_date) snapshot_date
      ,f.dim_ora_inv_productid
      ,f.dim_ora_inv_orgid
,SUM(
CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0
     THEN

(CASE WHEN IFNULL(f.dd_transaction_uom_code,'Not Set') = 'UU'
		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
 END )
+
(CASE WHEN d_sinv.availability_type = 2 THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END * dd_standart_cost)

    ELSE
(CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)+ CONVERT (DECIMAL (18,5),f.ct_reservation_quantity ) )
       * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl)
END) cogs_on_hand

      ,amt_exchangerate_gbl
FROM fact_ora_invonhand_daily f
     ,dim_ora_mtl_sec_inventory d_sinv
WHERE
     f.dim_ora_mtl_sec_inventoryid = d_sinv.dim_ora_mtl_sec_inventoryid
 AND TO_DATE(f.snapshot_date) IN (SELECT max_dt FROM TMP_LATEST_DATE)
 AND ct_primary_transaction_quantity <> 999999999999999868928
GROUP BY TO_DATE(f.snapshot_date), f.dim_ora_inv_productid, f.dim_ora_inv_orgid,amt_exchangerate_gbl;

DROP TABLE IF EXISTS tmp_max_id;
CREATE TABLE tmp_max_id AS
SELECT
       f.dim_ora_date_snapshotid
      ,f.dim_ora_inv_productid
      ,f.dim_ora_inv_orgid
      ,MAX(F.fact_ora_invonhand_dailyid) ID
      ,amt_exchangerate_gbl
FROM fact_ora_invonhand_daily F
WHERE TO_DATE(f.snapshot_date IN (SELECT max_dt FROM TMP_LATEST_DATE)
GROUP BY
       f.dim_ora_date_snapshotid
      ,f.dim_ora_inv_productid
      ,f.dim_ora_inv_orgid
      ,amt_exchangerate_gbl;


      MERGE INTO fact_ora_invonhand_daily f_ih1

      USING (

        SELECT f_ih.fact_ora_invonhand_dailyid, ifnull(a.cogs_on_hand,0) - ifnull(b.cogs_on_hand,0) v_cogs

        FROM fact_ora_invonhand_daily f_ih

          INNER JOIN dim_date snp ON f_ih.dim_ora_date_snapshotid = snp.dim_dateid

          INNER JOIN TMP_MAX_ID i on f_ih.fact_ora_invonhand_dailyid = i.id

          LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshot_date = snp.datevalue AND f_ih.dim_ora_inv_orgid = a.dim_ora_inv_orgid AND f_ih.dim_ora_inv_productid = a.dim_ora_inv_productid AND f_ih.amt_exchangerate_gbl = a.amt_exchangerate_gbl

          LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshot_date = snp.datevalue -1 AND f_ih.dim_ora_inv_orgid = b.dim_ora_inv_orgid AND f_ih.dim_ora_inv_productid = b.dim_ora_inv_productid AND f_ih.amt_exchangerate_gbl = b.amt_exchangerate_gbl

        WHERE f_ih.amt_cogs_1daychange <> ifnull(a.cogs_on_hand,0) - ifnull(b.cogs_on_hand,0)) x

      ON f_ih1.fact_ora_invonhand_dailyid = x.fact_ora_invonhand_dailyid
      WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1daychange = x.v_cogs;


      MERGE INTO fact_ora_invonhand_daily f_ih1

      USING (

        SELECT f_ih.fact_ora_invonhand_dailyid, ifnull(a.cogs_on_hand,0) - ifnull(b.cogs_on_hand,0) v_cogs

        FROM fact_ora_invonhand_daily f_ih

          INNER JOIN dim_date snp ON f_ih.dim_ora_date_snapshotid = snp.dim_dateid

          INNER JOIN TMP_MAX_ID i on f_ih.fact_ora_invonhand_dailyid = i.id

          LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshot_date = snp.datevalue AND f_ih.dim_ora_inv_orgid = a.dim_ora_inv_orgid AND f_ih.dim_ora_inv_productid = a.dim_ora_inv_productid AND f_ih.amt_exchangerate_gbl = a.amt_exchangerate_gbl

          LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshot_date = snp.datevalue - 7 AND f_ih.dim_ora_inv_orgid = b.dim_ora_inv_orgid AND f_ih.dim_ora_inv_productid = b.dim_ora_inv_productid AND f_ih.amt_exchangerate_gbl = b.amt_exchangerate_gbl

        WHERE f_ih.amt_cogs_1weekchange <> ifnull(a.cogs_on_hand,0) - ifnull(b.cogs_on_hand,0)) x

      ON f_ih1.fact_ora_invonhand_dailyid = x.fact_ora_invonhand_dailyid
      WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1weekchange = x.v_cogs;


      MERGE INTO fact_ora_invonhand_daily f_ih1

      USING (

        SELECT f_ih.fact_ora_invonhand_dailyid, ifnull(a.cogs_on_hand,0) - ifnull(b.cogs_on_hand,0) v_cogs

        FROM fact_ora_invonhand_daily f_ih

          INNER JOIN dim_date snp ON f_ih.dim_ora_date_snapshotid = snp.dim_dateid

          INNER JOIN TMP_MAX_ID i on f_ih.fact_ora_invonhand_dailyid = i.id

          LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshot_date = snp.datevalue AND f_ih.dim_ora_inv_orgid = a.dim_ora_inv_orgid AND f_ih.dim_ora_inv_productid = a.dim_ora_inv_productid AND f_ih.amt_exchangerate_gbl = a.amt_exchangerate_gbl

          LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshot_date = add_months(snp.datevalue,-1)  AND f_ih.dim_ora_inv_orgid = b.dim_ora_inv_orgid AND f_ih.dim_ora_inv_productid = b.dim_ora_inv_productid AND f_ih.amt_exchangerate_gbl = b.amt_exchangerate_gbl

        WHERE f_ih.amt_cogs_1monthchange <> ifnull(a.cogs_on_hand,0) - ifnull(b.cogs_on_hand,0)) x

      ON f_ih1.fact_ora_invonhand_dailyid = x.fact_ora_invonhand_dailyid
      WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1monthchange = x.v_cogs;
