/* ********************************************************************************************************************* */
/*   Script         : vw_bi_post_process_salesorderline_fact                                                             */
/*   Author         : FPOPESCU                                                                                           */
/*   Created On     : 17 August 2015                                                                                     */
/*   Description    : Post Process for Sales Order Line                                                                  */
/*                                                                                                                       */
/*   Change History                                                                                                      */
/*   Date             By        Version           Desc                                                                   */
/*   20 August 2015   Victor    1.0               Updated the hold_name_new and hold_date_new according to the staging tables    */
/*						                          ora_xxcus_order_hold_header and ora_xxcus_order_hold_line.             */
/*                                                                                                                       */
/* ********************************************************************************************************************* */

/*Updated the hold_name_new and hold_date_new according to the staging tables ora_xxcus_order_hold_header and ora_xxcus_order_hold_line by Victor on 20 August 2015 */


TRUNCATE TABLE tmp_order_hold_header;         
INSERT INTO tmp_order_hold_header          
SELECT * FROM (        
     SELECT   
         hh_order_hold_id
        ,hh_hold_source_id
        ,hh_header_id
        ,hh_line_id
        ,hh_order_number        
        ,lh_order_line
        ,hh_rel_flag
        ,hh_hold_name
        ,hh_hold_date    
        ,ROW_NUMBER() OVER ( PARTITION BY hh_header_id,hh_line_id ORDER BY hh_header_id,hh_line_id,hh_hold_date) RN       
      FROM ora_xxcus_order_hold_header
        )T
    WHERE rn = 1;
    
   
TRUNCATE TABLE tmp_order_hold_line;     
INSERT INTO tmp_order_hold_line         
SELECT * FROM (        
     SELECT   
        lh_order_hold_id
        ,lh_hold_source_id
        ,lh_header_id
        ,lh_line_id
        ,lh_order_number
        ,lh_order_line
        ,lh_rel_flag
        ,lh_hold_name
        ,lh_hold_date   
        ,ROW_NUMBER() OVER ( PARTITION BY lh_header_id,lh_line_id ORDER BY lh_header_id,lh_line_id,lh_hold_date) RN       
      FROM ora_xxcus_order_hold_line
        )T
    WHERE rn = 1;  
    
UPDATE fact_ora_sales_order_line
SET dd_hold_name = 'Not Set'
WHERE  dd_hold_name <> 'Not Set';

update fact_ora_sales_order_line fso
set fso.dd_hold_name = ifnull(tmp.lh_hold_name,'Not Set'),
DW_UPDATE_DATE = current_timestamp
from fact_ora_sales_order_line fso,tmp_order_hold_line tmp
where 
fso.DD_HEADER_ID = tmp.lh_header_id
AND fso.DD_LINE_ID = tmp.lh_line_id
and fso.dd_hold_name <> ifnull(tmp.lh_hold_name,'Not Set');

update fact_ora_sales_order_line fso
set fso.dd_hold_name = ifnull(tmp.hh_hold_name,'Not Set'),
DW_UPDATE_DATE = current_timestamp
from fact_ora_sales_order_line fso,tmp_order_hold_header tmp
where 
fso.DD_HEADER_ID = tmp.hh_header_id
AND fso.dd_hold_name = 'Not Set'
and fso.dd_hold_name <> ifnull(tmp.hh_hold_name,'Not Set');


UPDATE fact_ora_sales_order_line
SET dd_hold_date = '0001-01-01 00:00:00'
WHERE dd_hold_date <> '0001-01-01 00:00:00';

update fact_ora_sales_order_line fso
set fso.dd_hold_date = ifnull(tmp.lh_hold_date,'0001-01-01 00:00:00'),
DW_UPDATE_DATE = current_timestamp
from fact_ora_sales_order_line fso,tmp_order_hold_line tmp
where 
fso.DD_HEADER_ID = tmp.lh_header_id
AND fso.DD_LINE_ID = tmp.lh_line_id
and fso.dd_hold_date <> ifnull(tmp.lh_hold_date,'0001-01-01 00:00:00');

update fact_ora_sales_order_line fso
set fso.dd_hold_date = ifnull(tmp.hh_hold_date,'0001-01-01 00:00:00'),
DW_UPDATE_DATE = current_timestamp
from fact_ora_sales_order_line fso,tmp_order_hold_header tmp
where 
fso.DD_HEADER_ID = tmp.hh_header_id
AND fso.dd_hold_date = '0001-01-01 00:00:00'
and fso.dd_hold_date <> ifnull(tmp.hh_hold_date,'0001-01-01 00:00:00');

	