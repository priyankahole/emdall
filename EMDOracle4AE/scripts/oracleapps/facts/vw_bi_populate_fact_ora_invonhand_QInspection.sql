
drop table if exists temp_QInspection;
create table temp_QInspection
as select
part,
organization_code,
item_id,
organization_id,
sum(TO_ORG_PRIMARY_QUANTITY) TO_ORG_PRIMARY_QUANTITY
from ORA_QI_RECEIVING
group by
part,
organization_code,
item_id,
organization_id;


delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_invonhand';


INSERT INTO NUMBER_FOUNTAIN

SELECT 'fact_ora_invonhand',IFNULL(MAX(fact_ora_invonhandid),0)

FROM fact_ora_invonhand;


DELETE FROM fact_ora_invonhand WHERE source_id= 'QInspection';


INSERT INTO fact_ora_invonhand

(

fact_ora_invonhandid,DIM_ORA_INV_PRODUCTID,DIM_ORA_PRODUCTID,DIM_ORA_INV_ORGID,DIM_ORA_MTL_ITEM_REVID,

DIM_ORA_MTL_ITEM_LOCATORID,DIM_ORA_MTL_SEC_INVENTORYID,CT_TRANSACTION_QUANTITY,

DD_TRANSACTION_UOM_CODE,CT_PRIMARY_TRANSACTION_QUANTITY,DIM_ORA_DATE_RECEIVEDID,

fact_ORA_INVMTL_CREATE_TRXID,fact_ORA_INVMTL_UPDATE_TRXID,DIM_ORA_DATE_ORIG_RECEIVEDID,

DD_CONTAINERIZED_FLAG,DIM_ORA_ORGANIZATION_TYPEID,DIM_ORA_OWNING_ORGANIZATIONID,

DIM_ORA_OWNINING_TP_TYPEID,DIM_ORA_PLANNING_ORGANIZATIONID,DIM_ORA_PLANNING_TP_TYPEID,

DD_SECONDARY_UOM_CODE,CT_SECONDARY_TRANSACTION_QUANTITY,DD_LOT_NUMBER,

DIM_ORA_DATE_REQUIREMENTID,CT_RESERVATION_QUANTITY,DD_SHIP_READY_FLAG,CT_DETAILED_QUANTITY,

DIM_ORA_DATE_DEMAND_SHIPID,CT_DEMAND_QUANTITY,dd_aging_flag,CREATE_TRANSACTION_ID,UPDATE_TRANSACTION_ID,RESERVATION_ID,INVENTORY_ITEM_ID,

ORGANIZATION_ID,amt_exchangerate,amt_exchangerate_gbl,

DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID,

 DD_OWNING_ORGANIZATION_ID,DD_PLANNING_ORGANIZATION_ID,DD_DATE_RECEIVED--,dd_isconsigned

 ,ct_qinspection_quantity, dim_ora_expiration_dateid

)

SELECT

(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME ='fact_ora_invonhand') + ROW_NUMBER() OVER(order by '') fact_ora_invonhandid,

1 AS DIM_ORA_INV_PRODUCTID,

1 AS DIM_ORA_PRODUCTID,

1 AS DIM_ORA_INV_ORGID,

1 AS DIM_ORA_MTL_ITEM_REVID,

1 AS DIM_ORA_MTL_ITEM_LOCATORID,

1 AS DIM_ORA_MTL_SEC_INVENTORYID,

0 AS CT_TRANSACTION_QUANTITY,

0 AS DD_TRANSACTION_UOM_CODE,

0 AS CT_PRIMARY_TRANSACTION_QUANTITY,

1 AS DIM_ORA_DATE_RECEIVEDID,

0 AS fact_ORA_INVMTL_CREATE_TRXID,

0 AS fact_ORA_INVMTL_UPDATE_TRXID,

1 AS DIM_ORA_DATE_ORIG_RECEIVEDID,

1 AS DD_CONTAINERIZED_FLAG,

1 as DIM_ORA_ORGANIZATION_TYPEID,

1 AS DIM_ORA_OWNING_ORGANIZATIONID,

1 AS DIM_ORA_OWNINING_TP_TYPEID,

1 AS DIM_ORA_PLANNING_ORGANIZATIONID,

1 AS DIM_ORA_PLANNING_TP_TYPEID,

'Not Set' AS DD_SECONDARY_UOM_CODE,

0 AS CT_SECONDARY_TRANSACTION_QUANTITY,

s.part AS DD_LOT_NUMBER,

1 AS DIM_ORA_DATE_REQUIREMENTID,

0 AS CT_RESERVATION_QUANTITY,

0 AS DD_SHIP_READY_FLAG,

0 AS CT_DETAILED_QUANTITY,

1 AS DIM_ORA_DATE_DEMAND_SHIPID,

0 AS CT_DEMAND_QUANTITY,

ifnull(s.organization_code,'Not Set') dd_aging_flag,

IFNULL(s.item_id,1) AS CREATE_TRANSACTION_ID,

IFNULL(s.item_id,1) AS UPDATE_TRANSACTION_ID,

0 AS RESERVATION_ID,

IFNULL(s.item_id,1) AS INVENTORY_ITEM_ID,

s.organization_id AS ORGANIZATION_ID,

1 as amt_exchangerate,

1 as amt_exchangerate_gbl,

current_timestamp AS DW_INSERT_DATE,

current_timestamp AS DW_UPDATE_DATE,

current_timestamp AS rowstartdate,

'9999-12-31 00:00:00.000000' AS rowenddate,

1 AS rowiscurrent,

'Insert' AS rowchangereason,

'QInspection' AS SOURCE_ID,

0 DD_OWNING_ORGANIZATION_ID,

0 DD_PLANNING_ORGANIZATION_ID,

'9999-12-31 00:00:00.000000' DD_DATE_RECEIVED,

IFNULL(TO_ORG_PRIMARY_QUANTITY,0),

1 dim_ora_expiration_dateid

FROM temp_QInspection s;



UPDATE fact_ora_invonhand F

SET

F.DIM_ORA_INV_PRODUCTID = D.DIM_ORA_INVPRODUCTID,

DW_UPDATE_DATE = current_timestamp

FROM fact_ora_invonhand F,DIM_ORA_INVPRODUCT D

where CONVERT(VARCHAR(200),f.ORGANIZATION_ID) ||'~'||  CONVERT(VARCHAR(200),f.INVENTORY_ITEM_ID) = D.KEY_ID

and d.rowiscurrent = 1


AND f.source_id='QInspection';



UPDATE fact_ora_invonhand F

SET F.DIM_ORA_INV_ORGID= D.DIM_ORA_BUSINESS_ORGID,

DW_UPDATE_DATE = current_timestamp

from fact_ora_invonhand F,DIM_ORA_BUSINESS_ORG D

where
f.organization_id=d.organization_id
and f.source_id='QInspection'
and d.key_id like 'INV%';




UPDATE fact_ora_invonhand f_inv

SET f_inv.dim_ora_mdg_productid = d_mp.dim_mdg_partid

FROM fact_ora_invonhand f_inv, dim_ora_invproduct d_invp, dim_ora_mdg_product d_mp

WHERE f_inv.dim_ora_inv_productid = d_invp.dim_ora_invproductid

 AND DECODE(d_invp.mdm_globalid,'Not Set',REPLACE(d_invp.segment1,'.',''),d_invp.mdm_globalid) = d_mp.partnumber

 and f_inv.source_id='QInspection'

 AND f_inv.dim_ora_mdg_productid <> d_mp.dim_mdg_partid;
 
 
 
UPDATE fact_ora_invonhand f

 SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid

FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode
AND CASE WHEN substring(dp.GlbProductGroup,1,3)='SBU' THEN dp.GlbProductGroup
ELSE concat('SBU-',dp.GlbProductGroup) END = bw.productgroup

AND to_date('2017-12-28') BETWEEN bw.upperhierstartdate AND bw.upperhierenddate -- TO BE REPLACED AFTER 01.01.2017: '2016-12-20' WITH CURRENT_DATE

and f.source_id='QInspection'

AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;


UPDATE fact_ora_invonhand f

SET f.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid

FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_bwproducthierarchy bw
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
AND dp.producthierarchy = bw.lowerhierarchycode

AND bw.productgroup = 'Not Set'

AND f.dim_ora_bwproducthierarchyid = 1

and f.source_id='QInspection'

AND f.dim_ora_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;



TRUNCATE TABLE hlp_ora_cst_standards_costs_tmp;
INSERT INTO hlp_ora_cst_standards_costs_tmp
SELECT
      inventory_item_id
     ,organization_id
     ,MAX(standard_cost_revision_date) standard_cost_revision_date
  FROM hlp_ora_cst_standards_costs
 GROUP BY inventory_item_id
         ,organization_id;

UPDATE fact_ora_invonhand  f_inv
SET f_inv.dd_standart_cost = IFNULL(hlp.standard_cost,0)
FROM fact_ora_invonhand  f_inv, hlp_ora_cst_standards_costs hlp, hlp_ora_cst_standards_costs_tmp hlp_t
WHERE f_inv.inventory_item_id = hlp.inventory_item_id
  AND f_inv.organization_id = hlp.organization_id
  AND hlp_t.inventory_item_id = hlp.inventory_item_id
  AND hlp_t.organization_id = hlp.organization_id
  AND hlp.standard_cost_revision_date = hlp_t.standard_cost_revision_date
  and f_inv.source_id='QInspection'
  AND  f_inv.dd_standart_cost <> IFNULL(hlp.standard_cost,0);

  UPDATE fact_ora_invonhand f
SET f.amt_cogsactualrate_emd = IFNULL(c.Z_GCACTF,0)
	,f.amt_cogsfixedrate_emd = IFNULL(c.Z_GCFIXF,0)
	,f.amt_cogsfixedplanrate_emd = IFNULL(c.Z_GCPLFF,0)
	,f.amt_cogsplanrate_emd = IFNULL(c.Z_GCPLRF,0)
	,f.amt_cogsprevyearfixedrate_emd = IFNULL(c.Z_GCPYFF,0)
	,f.amt_cogsprevyearrate_emd = IFNULL(c.Z_GCPYRF,0)
	,f.amt_cogsprevyearto1_emd = IFNULL(c.Z_GCPYTF1,0)
	,f.amt_cogsprevyearto2_emd = IFNULL(c.Z_GCPYTF2,0)
	,f.amt_cogsprevyearto3_emd = IFNULL(c.Z_GCPYTF3,0)
	,f.amt_cogsturnoverrate1_emd = IFNULL(c.Z_GCTOF1,0)
	,f.amt_cogsturnoverrate2_emd = IFNULL(c.Z_GCTOF2,0)
	,f.amt_cogsturnoverrate3_emd = IFNULL(c.Z_GCTOF3,0)
FROM fact_ora_invonhand f, dim_ora_mdg_product dp, dim_ora_business_org dc, csv_cogs c
WHERE f.dim_ora_mdg_productid = dp.dim_mdg_partid
	AND f.dim_ora_inv_orgid = dc.dim_ora_business_orgid
	AND TRIM(LEADING 0 FROM c.PRODUCT)= dp.partnumber
	AND c.Z_REPUNIT = dc.cmg_number
	and f.source_id='QInspection';



UPDATE fact_ora_invonhand f_inv
SET amt_exchangerate_gbl = IFNULL(dr.exrate,1)
FROM fact_ora_invonhand f_inv, csv_oprate dr,dim_ora_business_org dc,ora_xx_legal_entities l
WHERE f_inv.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND dc.organization_id=l.inv_org_id
AND dr.toc = 'EUR'
AND dr.fromc=l.currency_code
and f_inv.source_id='QInspection'
AND amt_exchangerate_gbl <> IFNULL(dr.exrate,1);


UPDATE fact_ora_invonhand f
SET f.dim_ora_clusterid = dc.dim_ora_clusterid
FROM fact_ora_invonhand f, dim_ora_mdg_product mdg, dim_ora_bwproducthierarchy ph, dim_ora_cluster dc
WHERE f.dim_ora_mdg_productid = mdg.dim_mdg_partid
	AND f.dim_ora_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	AND businesssector = 'BS-02'
	AND f.source_id='QInspection'
	AND mdg.primary_production_location = dc.primary_manufacturing_site
	AND f.dim_ora_clusterid <> dc.dim_ora_clusterid;


UPDATE fact_ora_invonhand f
SET f.dim_countryhierpsid = dch.dim_countryhierpsid
FROM fact_ora_invonhand f, dim_ora_business_org dc, dim_ora_countryhierps dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND f.source_id='QInspection'
AND f.dim_countryhierpsid <> dch.dim_countryhierpsid;

UPDATE fact_ora_invonhand f
SET f.dim_countryhierarid  = dch.dim_countryhierarid
FROM fact_ora_invonhand f, dim_ora_business_org dc, dim_ora_countryhierar dch
WHERE dch.country = dc.legal_entity
AND f.DIM_ORA_INV_ORGID = dc.dim_ora_business_orgid
AND f.source_id='QInspection'
AND f.dim_countryhierarid <> dch.dim_countryhierarid;


UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business IN ('DIV.32') AND dim_con.internalhierarchyid = '548WP23VF6R8ICTMIER1FW7R0'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='QInspection'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV.31','DIV.35','DIV.34') AND dim_con.internalhierarchyid = '548YXNWXHSJ2Y19OSUCHY2CL7'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='QInspection'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88') AND dim_con.internalhierarchyid = '549HWG7F0J8VZ1P8YIYH1CPQ3'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='QInspection'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;

UPDATE fact_ora_invonhand f
SET f.dim_ora_bwhierarchycountryid = dim_con.dim_ora_bwhierarchycountryid
FROM fact_ora_invonhand f, dim_ora_bwhierarchycountry dim_con,dim_ora_bwproducthierarchy dim_prd, dim_ora_business_org dp
WHERE dim_prd.dim_bwproducthierarchyid = f.dim_ora_bwproducthierarchyid
AND    dim_prd.business  IN ('DIV-87') AND dim_con.internalhierarchyid = '54G8L55S3E80Q2SXZYJAELHKR'
AND    dp.dim_ora_business_orgid = f.DIM_ORA_INV_ORGID
AND    dp.country = dim_con.nodehierarchynamelvl7
AND f.source_id='QInspection'
AND f.dim_ora_bwhierarchycountryid <> dim_con.dim_ora_bwhierarchycountryid;
