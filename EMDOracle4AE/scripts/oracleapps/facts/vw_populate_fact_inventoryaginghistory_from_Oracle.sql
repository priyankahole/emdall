
update EMDORACLE4AE.fact_ora_invonhand_daily f
set f.DD_PRIMARYPRODUCTIONLOCATION = mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
FROM  EMDORACLE4AE.fact_ora_invonhand_daily f
inner join dim_ora_mdg_product  mdg on f.dim_ora_mdg_productid=mdg.dim_mdg_partid
where f.DD_PRIMARYPRODUCTIONLOCATION <> mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
and TO_DATE(snapshot_date) = TO_DATE(CURRENT_DATE) - 1;


truncate table tmp_fact_inventoryhistory_Oracle;

INSERT INTO tmp_fact_inventoryhistory_Oracle
(
    amt_exchangerate
	,amt_exchangerate_gbl
	,ct_blockedstock
	,ct_stockqty		--,amt_OnHand
    ,amt_StockValueAmt
    ,amt_StockValueAmt_1WeekChange
	,dd_batchno
	,dim_unitofmeasureid
	,dim_storagelocentrydateid
	,dim_plantid
	,dim_partid
	,dim_storagelocationid
	,dim_mdg_partid
	,dim_bwproducthierarchyid
	,dim_companyid
	,dw_insert_date
	,dw_update_date
	,fact_inventoryhistoryid
    ,dim_projectsourceid
    ,ct_StockInQInsp
    ,ct_StockInTransit
    ,ct_StockInTransfer
    ,ct_TotalRestrictedStock
    ,dim_itemcategoryid
    ,dim_stockcategoryid
    ,amt_StdUnitPrice
    ,amt_WIPBalance
    ,ct_WIPQty
    ,dim_dateidsnapshot
    /* from On Hand Amt formula need to have default value*/
	--,amt_StockValueAmt
	,amt_StockInQInspAmt
	,amt_BlockedStockAmt
	,amt_StockInTransitAmt
	,amt_StockInTransferAmt

	,amt_StockInQInspAmt_1WeekChange
	,amt_BlockedStockAmt_1WeekChange
	,amt_StockInTransferAmt_1WeekChange
	,amt_StockInTransitAmt_1WeekChange
	,ct_TotalRestrictedStock_1WeekChange

	,amt_cogsactualrate_emd
	,amt_cogsfixedrate_emd
	,amt_cogsfixedplanrate_emd
	,amt_cogsplanrate_emd
	,amt_cogsprevyearfixedrate_emd
	,amt_cogsprevyearrate_emd
	,amt_cogsprevyearto1_emd
	,amt_cogsprevyearto2_emd
	,amt_cogsprevyearto3_emd
	,amt_cogsturnoverrate1_emd
	,amt_cogsturnoverrate2_emd
	,amt_cogsturnoverrate3_emd

	,dim_bwhierarchycountryid
	,dim_clusterid
	,ct_countmaterialsafetystock
	,amt_cogs_1daychange
        ,amt_cogs_1weekchange
        ,amt_cogs_1monthchange
	,dim_specialstockid
		,dd_isconsigned
		,dd_batch_status_qkz
		,dim_productionschedulerid
		,ct_baseuomratioPCcustom
		,dim_countryhierpsid
		,dim_countryhierarid
      		,amt_OnHand
		,dim_dateidexpirydate
    ,amt_cogsfixedrate_emd_new
    ,amt_cogsfixedplanrate_emd_new
    ,amt_cogsplanrate_emd_new
    ,amt_exchangerate_gbl_bis
    ,amt_cogsfixedplanrate_emd_bis
    ,amt_StdUnitPrice_bis
    ,DD_PRIMARYPRODUCTIONLOCATION
		)
SELECT
         CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
		,CASE WHEN d_sinv.availability_type = 1 AND SUBSTR(d_sinv.secondary_inventory_name,1,16) not in ('STERISHLD','7DANB','7DAND')
    THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
    WHEN d_sinv.availability_type = 1 AND to_date(snapshot_date)>='2017-05-07' AND SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
    THEN 0
    WHEN d_sinv.availability_type = 1 AND to_date(snapshot_date)<'2017-05-07' AND SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
   THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
    ELSE 0 END ct_primary_transaction_quantity

		,CASE WHEN d_sinv.availability_type = 1 and SUBSTR(d_sinv.secondary_inventory_name,1,16) not in ('STERISHLD','7DANB','7DAND')
then CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		 END
     WHEN d_sinv.availability_type = 1 and SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
     AND to_date(snapshot_date)>='2017-05-07' THEN 0
   WHEN d_sinv.availability_type = 1 and SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
     AND to_date(snapshot_date)<'2017-05-07' THEN
CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		 END
     else 0 end amt_primary_transaction_quantity  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost

        ,amt_primary_transact_qty_1weekchange

		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
		,d_uom.dim_unitofmeasureid
		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid4
		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
		,f_inv.dw_insert_date
		,f_inv.dw_update_date
		,f_inv.fact_ora_invonhand_dailyid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhand_dailyid
		,p.dim_projectsourceid
		,0
		,0
		,0
		,CASE WHEN d_sinv.availability_type = 2 THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
     WHEN to_date(snapshot_date)>='2017-05-07' and SUBSTR(d_sinv.secondary_inventory_name,1,16) in ('STERISHLD','7DANB','7DAND')
     THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
    ELSE 0 END restricted_qty
		,1
		,1
		,dd_standart_cost
		,0
		,0
		,DIM_ORA_DATE_SNAPSHOTID + IFNULL(p.dim_projectsourceid * p.multiplier ,0) DIM_ORA_DATE_SNAPSHOTID
		--,0
		,0
		,0
		,0
		,0

		,0
		,0
		,0
		,0
		,0

		,amt_cogsactualrate_emd
		,amt_cogsfixedrate_emd
		,amt_cogsfixedplanrate_emd
		,amt_cogsplanrate_emd
		,amt_cogsprevyearfixedrate_emd
		,amt_cogsprevyearrate_emd
		,amt_cogsprevyearto1_emd
		,amt_cogsprevyearto2_emd
		,amt_cogsprevyearto3_emd
		,amt_cogsturnoverrate1_emd
		,amt_cogsturnoverrate2_emd
		,amt_cogsturnoverrate3_emd

	    ,dim_ora_bwhierarchycountryid
		,dim_ora_clusterid
		,ct_countmaterialsafetystock
		,amt_cogs_1daychange
                ,amt_cogs_1weekchange
                ,amt_cogs_1monthchange
		,1
				,ifnull(dd_isconsigned,0)
				,'Not Set' dd_batch_status_qkz
				,1 dim_productionschedulerid
				,1 ct_baseuomratioPCcustom
	,dim_countryhierpsid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierpsid
        ,dim_countryhierarid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_countryhierarid
		,CASE WHEN IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = 'UU'
		      THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost/1000000
		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
		 END amt_primary_transaction_quantity
	,dim_ora_expiration_dateid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_dateidexpirydate
  ,amt_cogsfixedrate_emd_new
  ,amt_cogsfixedplanrate_emd_new
  ,amt_cogsplanrate_emd_new
  ,amt_exchangerate_gbl_bis
  ,amt_cogsfixedplanrate_emd_bis
  ,amt_StdUnitPrice_bis
  ,DD_PRIMARYPRODUCTIONLOCATION
 FROM emdoracle4ae.fact_ora_invonhand_daily f_inv
     ,emdoracle4ae.dim_projectsource p
     ,emd586.dim_unitofmeasure d_uom
     ,emdoracle4ae.dim_ora_mtl_sec_inventory d_sinv
WHERE IFNULL(f_inv.dd_transaction_uom_code,'Not Set') = d_uom.uom
 AND d_uom.projectsourceid = p.dim_projectsourceid
 AND f_inv.dim_ora_mtl_sec_inventoryid = d_sinv.dim_ora_mtl_sec_inventoryid
 AND d_sinv.asset_inventory = 1
 AND ct_primary_transaction_quantity <> 999999999999999868928
 AND to_date(SNAPSHOT_DATE) = to_date(current_timestamp) - 1;


 INSERT INTO tmp_fact_inventoryhistory_Oracle
 (
      amt_exchangerate
 	,amt_exchangerate_gbl
 	,ct_blockedstock
 	,ct_stockqty		--,amt_OnHand
     ,amt_StockValueAmt
     ,amt_StockValueAmt_1WeekChange
 	,dd_batchno
 	,dim_unitofmeasureid
 	,dim_storagelocentrydateid
 	,dim_plantid
 	,dim_partid
 	,dim_storagelocationid
 	,dim_mdg_partid
 	,dim_bwproducthierarchyid
 	,dim_companyid
 	,dw_insert_date
 	,dw_update_date
 	,fact_inventoryhistoryid
     ,dim_projectsourceid
     ,ct_StockInQInsp
     ,ct_StockInTransit
     ,ct_StockInTransfer
     ,ct_TotalRestrictedStock
     ,dim_itemcategoryid
     ,dim_stockcategoryid
     ,amt_StdUnitPrice
     ,amt_WIPBalance
     ,ct_WIPQty
     ,dim_dateidsnapshot
     /* from On Hand Amt formula need to have default value*/
 	--,amt_StockValueAmt
 	,amt_StockInQInspAmt
 	,amt_BlockedStockAmt
 	,amt_StockInTransitAmt
 	,amt_StockInTransferAmt

 	,amt_StockInQInspAmt_1WeekChange
 	,amt_BlockedStockAmt_1WeekChange
 	,amt_StockInTransferAmt_1WeekChange
 	,amt_StockInTransitAmt_1WeekChange
 	,ct_TotalRestrictedStock_1WeekChange

 	,amt_cogsactualrate_emd
 	,amt_cogsfixedrate_emd
 	,amt_cogsfixedplanrate_emd
 	,amt_cogsplanrate_emd
 	,amt_cogsprevyearfixedrate_emd
 	,amt_cogsprevyearrate_emd
 	,amt_cogsprevyearto1_emd
 	,amt_cogsprevyearto2_emd
 	,amt_cogsprevyearto3_emd
 	,amt_cogsturnoverrate1_emd
 	,amt_cogsturnoverrate2_emd
 	,amt_cogsturnoverrate3_emd

 	,dim_bwhierarchycountryid
 	,dim_clusterid
 	,ct_countmaterialsafetystock
 	,amt_cogs_1daychange
     ,amt_cogs_1weekchange
     ,amt_cogs_1monthchange
 	,dim_specialstockid
 	,dd_isconsigned
 	,dd_batch_status_qkz
 	,dim_productionschedulerid
 	,ct_baseuomratioPCcustom
 	,DIM_ITEMSTATUSID
 	,amt_OnHand
   ,amt_cogsfixedrate_emd_new
   ,amt_cogsfixedplanrate_emd_new
   ,amt_cogsplanrate_emd_new
   ,amt_exchangerate_gbl_bis
   ,amt_cogsfixedplanrate_emd_bis
   ,amt_StdUnitPrice_bis
 ,DD_PRIMARYPRODUCTIONLOCATION
 		)
 SELECT
          CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
 		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
 		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
 		,ct_primary_transaction_quantity
 		,0
 		,amt_primary_transact_qty_1weekchange
 		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
 		,1 dim_unitofmeasureid
 		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
 		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
 		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
 		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
 		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
 		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
         ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
 		,f_inv.dw_insert_date
 		,f_inv.dw_update_date
 		,f_inv.fact_ora_invonhand_dailyid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhand_dailyid
 		,p.dim_projectsourceid
 		,0
 		,ct_intransit_quantity
 		,0
 		,0
 		,1
 		,1
 		,dd_standart_cost
 		,0
 		,0
 		,DIM_ORA_DATE_SNAPSHOTID + IFNULL(p.dim_projectsourceid * p.multiplier ,0) DIM_ORA_DATE_SNAPSHOTID
 		,0
 		,0
 		,CONVERT (DECIMAL (18,5),ct_intransit_quantity) * dd_standart_cost
 		,0

 		,0
 		,0
 		,0
 		,0
 		,0

 		,amt_cogsactualrate_emd
 		,amt_cogsfixedrate_emd
 		,amt_cogsfixedplanrate_emd
 		,amt_cogsplanrate_emd
 		,amt_cogsprevyearfixedrate_emd
 		,amt_cogsprevyearrate_emd
 		,amt_cogsprevyearto1_emd
 		,amt_cogsprevyearto2_emd
 		,amt_cogsprevyearto3_emd
 		,amt_cogsturnoverrate1_emd
 		,amt_cogsturnoverrate2_emd
 		,amt_cogsturnoverrate3_emd
 		,dim_ora_bwhierarchycountryid
 		,dim_ora_clusterid
 		,ct_countmaterialsafetystock
 		,amt_cogs_1daychange
         ,amt_cogs_1weekchange
         ,amt_cogs_1monthchange
 		,1
 		,ifnull(dd_isconsigned,0)
 		,'Not Set' dd_batch_status_qkz
 		,1 dim_productionschedulerid
 		,1 ct_baseuomratioPCcustom
 		,1 DIM_ITEMSTATUSID
 		,CONVERT (DECIMAL (18,5),ct_intransit_quantity) * dd_standart_cost
     ,amt_cogsfixedrate_emd_new
     ,amt_cogsfixedplanrate_emd_new
     ,amt_cogsplanrate_emd_new
     ,amt_exchangerate_gbl_bis
     ,amt_cogsfixedplanrate_emd_bis
     ,amt_StdUnitPrice_bis
     ,DD_PRIMARYPRODUCTIONLOCATION

 	  FROM emdoracle4ae.fact_ora_invonhand_daily f_inv
      ,emdoracle4ae.dim_projectsource p

  WHERE f_inv.source_id ='Intransit'
  AND ct_primary_transaction_quantity <> 999999999999999868928
  AND to_date(SNAPSHOT_DATE) = to_date(current_timestamp) - 1;

  INSERT INTO tmp_fact_inventoryhistory_Oracle
  (
       amt_exchangerate
  	,amt_exchangerate_gbl
  	,ct_blockedstock
  	,ct_stockqty		--,amt_OnHand
      ,amt_StockValueAmt
      ,amt_StockValueAmt_1WeekChange
  	,dd_batchno
  	,dim_unitofmeasureid
  	,dim_storagelocentrydateid
  	,dim_plantid
  	,dim_partid
  	,dim_storagelocationid
  	,dim_mdg_partid
  	,dim_bwproducthierarchyid
  	,dim_companyid
  	,dw_insert_date
  	,dw_update_date
  	,fact_inventoryhistoryid
      ,dim_projectsourceid
      ,ct_StockInQInsp
      ,ct_StockInTransit
      ,ct_StockInTransfer
      ,ct_TotalRestrictedStock
      ,dim_itemcategoryid
      ,dim_stockcategoryid
      ,amt_StdUnitPrice
      ,amt_WIPBalance
      ,ct_WIPQty
      ,dim_dateidsnapshot
      /* from On Hand Amt formula need to have default value*/
  	--,amt_StockValueAmt
  	,amt_StockInQInspAmt
  	,amt_BlockedStockAmt
  	,amt_StockInTransitAmt
  	,amt_StockInTransferAmt

  	,amt_StockInQInspAmt_1WeekChange
  	,amt_BlockedStockAmt_1WeekChange
  	,amt_StockInTransferAmt_1WeekChange
  	,amt_StockInTransitAmt_1WeekChange
  	,ct_TotalRestrictedStock_1WeekChange

  	,amt_cogsactualrate_emd
  	,amt_cogsfixedrate_emd
  	,amt_cogsfixedplanrate_emd
  	,amt_cogsplanrate_emd
  	,amt_cogsprevyearfixedrate_emd
  	,amt_cogsprevyearrate_emd
  	,amt_cogsprevyearto1_emd
  	,amt_cogsprevyearto2_emd
  	,amt_cogsprevyearto3_emd
  	,amt_cogsturnoverrate1_emd
  	,amt_cogsturnoverrate2_emd
  	,amt_cogsturnoverrate3_emd

  	,dim_bwhierarchycountryid
  	,dim_clusterid
  	,ct_countmaterialsafetystock
  	,amt_cogs_1daychange
          ,amt_cogs_1weekchange
          ,amt_cogs_1monthchange
  	,dim_specialstockid
  		,dd_isconsigned
  		,dd_batch_status_qkz
  		,dim_productionschedulerid
  		,ct_baseuomratioPCcustom
  		,DIM_ITEMSTATUSID
  		,amt_OnHand
      ,amt_cogsfixedrate_emd_new
      ,amt_cogsfixedplanrate_emd_new
      ,amt_cogsplanrate_emd_new
      ,amt_exchangerate_gbl_bis
      ,amt_cogsfixedplanrate_emd_bis
      ,amt_StdUnitPrice_bis
      ,DD_PRIMARYPRODUCTIONLOCATION

  		)
  SELECT
           CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
  		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
  		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
  		,CASE WHEN f_inv.dd_restricted_unrestricted_flag = 'N' or f_inv.dd_restricted_unrestricted_flag = 'Not Set' THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity)
             ELSE 0
  		 END ct_primary_transaction_quantity
  		 ,CASE WHEN f_inv.source_id = 'OPM' and (f_inv.dd_restricted_unrestricted_flag = 'N' or f_inv.dd_restricted_unrestricted_flag = 'Not Set') THEN amt_opm_onhand_value-- contains already unit price in the amt
  		      ELSE 0
  		 END amt_primary_transaction_quantity  -- CONVERT(DECIMAL (18,5),ct_primary_transaction_quantity) * standard_cost
  		 ,amt_primary_transact_qty_1weekchange
  		 ,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
  		 ,1 dim_unitofmeasureid
  		 ,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
  		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
  		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
  		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
  		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
  		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
          ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
  		,f_inv.dw_insert_date
  		,f_inv.dw_update_date
  		,f_inv.fact_ora_invonhand_dailyid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhand_dailyid
  		,p.dim_projectsourceid
  		,0
  		,0
  		,0
  		,CASE WHEN f_inv.dd_restricted_unrestricted_flag = 'Y' THEN CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) ELSE 0 END restricted_qty
  		,1
  		,1
  		,dd_standart_cost
  		,0
  		,0
  		,DIM_ORA_DATE_SNAPSHOTID + IFNULL(p.dim_projectsourceid * p.multiplier ,0) DIM_ORA_DATE_SNAPSHOTID
  		,0
  		,0
  		,0
  		,0

  		,0
  		,0
  		,0
  		,0
  		,0
  		,amt_cogsactualrate_emd
  	    ,amt_cogsfixedrate_emd
  	    ,amt_cogsfixedplanrate_emd
  	    ,amt_cogsplanrate_emd
  	    ,amt_cogsprevyearfixedrate_emd
  		,amt_cogsprevyearrate_emd
  		,amt_cogsprevyearto1_emd
  		,amt_cogsprevyearto2_emd
  		,amt_cogsprevyearto3_emd
  		,amt_cogsturnoverrate1_emd
  		,amt_cogsturnoverrate2_emd
  		,amt_cogsturnoverrate3_emd
  		,dim_ora_bwhierarchycountryid
  		,dim_ora_clusterid
  		,ct_countmaterialsafetystock
  		,amt_cogs_1daychange
          ,amt_cogs_1weekchange
          ,amt_cogs_1monthchange
  		,1
  		,ifnull(dd_isconsigned,0)
  		,'Not Set' dd_batch_status_qkz
          ,1 dim_productionschedulerid
          ,1 ct_baseuomratioPCcustom
  		,1 DIM_ITEMSTATUSID
  		,CASE WHEN f_inv.source_id = 'OPM' THEN amt_opm_onhand_value-- contains already unit price in the amt
  		      ELSE CONVERT (DECIMAL (18,5),ct_primary_transaction_quantity) * dd_standart_cost
  		 END amt_primary_transaction_quantity
       ,amt_cogsfixedrate_emd_new
       ,amt_cogsfixedplanrate_emd_new
       ,amt_cogsplanrate_emd_new
       ,amt_exchangerate_gbl_bis
       ,amt_cogsfixedplanrate_emd_bis
       ,amt_StdUnitPrice_bis
       ,DD_PRIMARYPRODUCTIONLOCATION

  	 FROM emdoracle4ae.fact_ora_invonhand_daily f_inv
       ,emdoracle4ae.dim_projectsource p

   WHERE f_inv.source_id ='OPM'
   AND ct_primary_transaction_quantity <> 999999999999999868928
    AND to_date(SNAPSHOT_DATE) = to_date(current_timestamp) - 1;


    INSERT INTO tmp_fact_inventoryhistory_Oracle
    (
         amt_exchangerate
    	,amt_exchangerate_gbl
    	,ct_blockedstock
    	,ct_stockqty		--,amt_OnHand
        ,amt_StockValueAmt
        ,amt_StockValueAmt_1WeekChange
    	,dd_batchno
    	,dim_unitofmeasureid
    	,dim_storagelocentrydateid
    	,dim_plantid
    	,dim_partid
    	,dim_storagelocationid
    	,dim_mdg_partid
    	,dim_bwproducthierarchyid
    	,dim_companyid
    	,dw_insert_date
    	,dw_update_date
    	,fact_inventoryhistoryid
        ,dim_projectsourceid
        ,ct_StockInQInsp
        ,ct_StockInTransit
        ,ct_StockInTransfer
        ,ct_TotalRestrictedStock
        ,dim_itemcategoryid
        ,dim_stockcategoryid
        ,amt_StdUnitPrice
        ,amt_WIPBalance
        ,ct_WIPQty
        ,dim_dateidsnapshot
        /* from On Hand Amt formula need to have default value*/
    	--,amt_StockValueAmt
    	,amt_StockInQInspAmt
    	,amt_BlockedStockAmt
    	,amt_StockInTransitAmt
    	,amt_StockInTransferAmt

    	,amt_StockInQInspAmt_1WeekChange
    	,amt_BlockedStockAmt_1WeekChange
    	,amt_StockInTransferAmt_1WeekChange
    	,amt_StockInTransitAmt_1WeekChange
    	,ct_TotalRestrictedStock_1WeekChange

    	,amt_cogsactualrate_emd
    	,amt_cogsfixedrate_emd
    	,amt_cogsfixedplanrate_emd
    	,amt_cogsplanrate_emd
    	,amt_cogsprevyearfixedrate_emd
    	,amt_cogsprevyearrate_emd
    	,amt_cogsprevyearto1_emd
    	,amt_cogsprevyearto2_emd
    	,amt_cogsprevyearto3_emd
    	,amt_cogsturnoverrate1_emd
    	,amt_cogsturnoverrate2_emd
    	,amt_cogsturnoverrate3_emd

    	,dim_bwhierarchycountryid
    	,dim_clusterid
    	,ct_countmaterialsafetystock
    	,amt_cogs_1daychange
        ,amt_cogs_1weekchange
        ,amt_cogs_1monthchange
    	,dim_specialstockid
    	,dd_isconsigned
    	,dd_batch_status_qkz
    	,dim_productionschedulerid
    	,ct_baseuomratioPCcustom
    	,DIM_ITEMSTATUSID
    	,amt_OnHand
      ,amt_cogsfixedrate_emd_new
      ,amt_cogsfixedplanrate_emd_new
      ,amt_cogsplanrate_emd_new
      ,amt_exchangerate_gbl_bis
      ,amt_cogsfixedplanrate_emd_bis
      ,amt_StdUnitPrice_bis
      ,DD_PRIMARYPRODUCTIONLOCATION

    		)
    SELECT
             CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
    		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
    		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
    		,ct_primary_transaction_quantity
    		,0
    		,amt_primary_transact_qty_1weekchange
    		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
    		,1 dim_unitofmeasureid
    		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
    		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
    		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid
    		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
    		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
    		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
            ,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
    		,f_inv.dw_insert_date
    		,f_inv.dw_update_date
    		,f_inv.fact_ora_invonhand_dailyid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhand_dailyid
    		,p.dim_projectsourceid
    		,ct_qinspection_quantity
    		,0
    		,0
    		,ct_primary_transaction_quantity
    		,1
    		,1
    		,dd_standart_cost
    		,0
    		,0
    		,DIM_ORA_DATE_SNAPSHOTID + IFNULL(p.dim_projectsourceid * p.multiplier ,0) DIM_ORA_DATE_SNAPSHOTID
    		,CONVERT (DECIMAL (18,5),ct_qinspection_quantity) * dd_standart_cost
    		,0
    		,0
    		,0

    		,0
    		,0
    		,0
    		,0
    		,0

    		,amt_cogsactualrate_emd
    		,amt_cogsfixedrate_emd
    		,amt_cogsfixedplanrate_emd
    		,amt_cogsplanrate_emd
    		,amt_cogsprevyearfixedrate_emd
    		,amt_cogsprevyearrate_emd
    		,amt_cogsprevyearto1_emd
    		,amt_cogsprevyearto2_emd
    		,amt_cogsprevyearto3_emd
    		,amt_cogsturnoverrate1_emd
    		,amt_cogsturnoverrate2_emd
    		,amt_cogsturnoverrate3_emd
    		,dim_ora_bwhierarchycountryid
    		,dim_ora_clusterid
    		,ct_countmaterialsafetystock
    		,amt_cogs_1daychange
            ,amt_cogs_1weekchange
            ,amt_cogs_1monthchange
    		,1
    		,ifnull(dd_isconsigned,0)
    		,'Not Set' dd_batch_status_qkz
            ,1 dim_productionschedulerid
            ,1 ct_baseuomratioPCcustom
            ,1 DIM_ITEMSTATUSID
            ,CONVERT (DECIMAL (18,5),ct_qinspection_quantity) * dd_standart_cost
            ,amt_cogsfixedrate_emd_new
            ,amt_cogsfixedplanrate_emd_new
            ,amt_cogsplanrate_emd_new
            ,amt_exchangerate_gbl_bis
            ,amt_cogsfixedplanrate_emd_bis
            ,amt_StdUnitPrice_bis
	    ,DD_PRIMARYPRODUCTIONLOCATION

    	  FROM emdoracle4ae.fact_ora_invonhand_daily f_inv
         ,emdoracle4ae.dim_projectsource p

     WHERE f_inv.source_id ='QInspection'
     AND ct_primary_transaction_quantity <> 999999999999999868928
    AND to_date(SNAPSHOT_DATE) = to_date(current_timestamp) - 1;

    INSERT INTO tmp_fact_inventoryhistory_Oracle
    (
         amt_exchangerate
    	,amt_exchangerate_gbl
    	,ct_blockedstock
    	,ct_stockqty		--,amt_OnHand
        ,amt_StockValueAmt
        ,amt_StockValueAmt_1WeekChange
    	,dd_batchno
    	,dim_unitofmeasureid
    	,dim_storagelocentrydateid
    	,dim_plantid
    	,dim_partid
    	,dim_storagelocationid
    	,dim_mdg_partid
    	,dim_bwproducthierarchyid
    	,dim_companyid
    	,dw_insert_date
    	,dw_update_date
    	,fact_inventoryhistoryid
        ,dim_projectsourceid
        ,ct_StockInQInsp
        ,ct_StockInTransit
        ,ct_StockInTransfer
        ,ct_TotalRestrictedStock
        ,dim_itemcategoryid
        ,dim_stockcategoryid
        ,amt_StdUnitPrice
        ,amt_WIPBalance
        ,ct_WIPQty
        ,dim_dateidsnapshot
        /* from On Hand Amt formula need to have default value*/
    	--,amt_StockValueAmt
    	,amt_StockInQInspAmt
    	,amt_BlockedStockAmt
    	,amt_StockInTransitAmt
    	,amt_StockInTransferAmt

    	,amt_StockInQInspAmt_1WeekChange
    	,amt_BlockedStockAmt_1WeekChange
    	,amt_StockInTransferAmt_1WeekChange
    	,amt_StockInTransitAmt_1WeekChange
    	,ct_TotalRestrictedStock_1WeekChange

    	,amt_cogsactualrate_emd
    	,amt_cogsfixedrate_emd
    	,amt_cogsfixedplanrate_emd
    	,amt_cogsplanrate_emd
    	,amt_cogsprevyearfixedrate_emd
    	,amt_cogsprevyearrate_emd
    	,amt_cogsprevyearto1_emd
    	,amt_cogsprevyearto2_emd
    	,amt_cogsprevyearto3_emd
    	,amt_cogsturnoverrate1_emd
    	,amt_cogsturnoverrate2_emd
    	,amt_cogsturnoverrate3_emd

    	,dim_bwhierarchycountryid
    	,dim_clusterid
    	,ct_countmaterialsafetystock
    	,amt_cogs_1daychange
            ,amt_cogs_1weekchange
            ,amt_cogs_1monthchange
    	,dim_specialstockid
    		,dd_isconsigned
    		,dd_batch_status_qkz
    		,dim_productionschedulerid
    		,ct_baseuomratioPCcustom
    		,DIM_ITEMSTATUSID
        ,amt_cogsfixedrate_emd_new
        ,amt_cogsfixedplanrate_emd_new
        ,amt_cogsplanrate_emd_new
        ,amt_exchangerate_gbl_bis
        ,amt_cogsfixedplanrate_emd_bis
        ,amt_StdUnitPrice_bis
	,DD_PRIMARYPRODUCTIONLOCATION

    		)
    SELECT
             CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate  ) amt_exchangerate
    		,CONVERT (DECIMAL (18,6),f_inv.amt_exchangerate_gbl ) amt_exchangerate_gbl
    		,CONVERT (DECIMAL (18,5),f_inv.ct_reservation_quantity ) ct_reservation_quantity
    		,ct_primary_transaction_quantity
    		,0
            ,amt_primary_transact_qty_1weekchange

    		,SUBSTR (f_inv.dd_lot_number,1,10) dd_lot_number
    		,1 dim_unitofmeasureid
    		,f_inv.dim_ora_date_orig_receivedid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_date_orig_receivedid /* DATE */
    		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_business_orgid --f_inv.dim_ora_plantid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
    		,f_inv.dim_ora_inv_productid +  IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_inv_productid4
    		,f_inv.dim_ora_mtl_sec_inventoryid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) dim_ora_mtl_sec_inventoryid
    		,f_inv.dim_ora_mdg_productid -- contains the multiplier already
    		,f_inv.dim_ora_bwproducthierarchyid -- contains the multiplier already
    		,f_inv.dim_ora_inv_orgid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) AS dim_companyid
    		,f_inv.dw_insert_date
    		,f_inv.dw_update_date
    		,f_inv.fact_ora_invonhand_dailyid + IFNULL(p.dim_projectsourceid * p.multiplier ,0) fact_ora_invonhand_dailyid
    		,p.dim_projectsourceid
    		,0
    		,0
    		,0
    		,ct_primary_transaction_quantity restricted_qty
    		,1
    		,1
    		,dd_standart_cost
    		,AMT_WIP
    		,CT_WIP_QUANTITY
    		,DIM_ORA_DATE_SNAPSHOTID + IFNULL(p.dim_projectsourceid * p.multiplier ,0) DIM_ORA_DATE_SNAPSHOTID
    		--,0
    		,0
    		,0
    		,0
    		,0

    		,0
    		,0
    		,0
    		,0
    		,0

    		,amt_cogsactualrate_emd
    		,amt_cogsfixedrate_emd
    		,amt_cogsfixedplanrate_emd
    		,amt_cogsplanrate_emd
    		,amt_cogsprevyearfixedrate_emd
    		,amt_cogsprevyearrate_emd
    		,amt_cogsprevyearto1_emd
    		,amt_cogsprevyearto2_emd
    		,amt_cogsprevyearto3_emd
    		,amt_cogsturnoverrate1_emd
    		,amt_cogsturnoverrate2_emd
    		,amt_cogsturnoverrate3_emd

    	    ,dim_ora_bwhierarchycountryid
    		,dim_ora_clusterid
    		,ct_countmaterialsafetystock
    		,amt_cogs_1daychange
                    ,amt_cogs_1weekchange
                    ,amt_cogs_1monthchange
    		,1
    				,ifnull(dd_isconsigned,0)
    				,'Not Set' dd_batch_status_qkz
    				,1 dim_productionschedulerid
    				,1 ct_baseuomratioPCcustom
    				,1 DIM_ITEMSTATUSID
            ,amt_cogsfixedrate_emd_new
            ,amt_cogsfixedplanrate_emd_new
            ,amt_cogsplanrate_emd_new
            ,amt_exchangerate_gbl_bis
            ,amt_cogsfixedplanrate_emd_bis
            ,amt_StdUnitPrice_bis
	    ,DD_PRIMARYPRODUCTIONLOCATION


     FROM emdoracle4ae.fact_ora_invonhand_daily f_inv
         ,emdoracle4ae.dim_projectsource p

     WHERE f_inv.source_id ='WIP'
     AND ct_primary_transaction_quantity <> 999999999999999868928
     AND to_date(SNAPSHOT_DATE) = to_date(current_timestamp) - 1;

     DELETE FROM emd586.fact_inventoryhistory WHERE
     dim_projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s )
     and fact_inventoryhistoryid in (select distinct fact_inventoryhistoryid from emd586.fact_inventoryhistory f,emd586.dim_date d
     where f.dim_dateidsnapshot=d.dim_dateid and datevalue=current_date-1
     and f.dim_projectsourceid=7);


insert into emd586.FACT_INVENTORYHISTORY
(
FACT_INVENTORYHISTORYID
,CT_STOCKQTY
,AMT_STOCKVALUEAMT
,CT_LASTRECEIVEDQTY
,DD_BATCHNO
,DD_VALUATIONTYPE
,DD_DOCUMENTNO
,DD_DOCUMENTITEMNO
,DD_MOVEMENTTYPE
,DIM_STORAGELOCENTRYDATEID
,DIM_LASTRECEIVEDDATEID
,DIM_PARTID
,DIM_PLANTID
,DIM_STORAGELOCATIONID
,DIM_COMPANYID
,DIM_VENDORID
,DIM_CURRENCYID
,DIM_STOCKTYPEID
,DIM_SPECIALSTOCKID
,DIM_PURCHASEORGID
,DIM_PURCHASEGROUPID
,DIM_PRODUCTHIERARCHYID
,DIM_UNITOFMEASUREID
,DIM_MOVEMENTINDICATORID
,DIM_CONSUMPTIONTYPEID
,DIM_COSTCENTERID
,DIM_DOCUMENTSTATUSID
,DIM_DOCUMENTTYPEID
,DIM_INCOTERMID
,DIM_ITEMCATEGORYID
,DIM_ITEMSTATUSID
,DIM_TERMID
,DIM_PURCHASEMISCID
,AMT_STOCKVALUEAMT_GBL
,CT_TOTALRESTRICTEDSTOCK
,CT_STOCKINQINSP
,CT_BLOCKEDSTOCK
,CT_STOCKINTRANSFER
,CT_UNRESTRICTEDCONSGNSTOCK
,CT_RESTRICTEDCONSGNSTOCK
,CT_BLOCKEDCONSGNSTOCK
,CT_CONSGNSTOCKINQINSP
,CT_BLOCKEDSTOCKRETURNS
,AMT_BLOCKEDSTOCKAMT
,AMT_BLOCKEDSTOCKAMT_GBL
,AMT_STOCKINQINSPAMT
,AMT_STOCKINQINSPAMT_GBL
,AMT_STOCKINTRANSFERAMT
,AMT_STOCKINTRANSFERAMT_GBL
,AMT_UNRESTRICTEDCONSGNSTOCKAMT
,AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL
,AMT_STDUNITPRICE
,AMT_STDUNITPRICE_GBL
,DIM_STOCKCATEGORYID
,CT_STOCKINTRANSIT
,AMT_STOCKINTRANSITAMT
,AMT_STOCKINTRANSITAMT_GBL
,DIM_SUPPLYINGPLANTID
,AMT_MTLDLVRCOST
,AMT_OVERHEADCOST
,AMT_OTHERCOST
,AMT_WIPBALANCE
,CT_WIPAGING
,AMT_MOVINGAVGPRICE
,AMT_PREVIOUSPRICE
,AMT_COMMERICALPRICE1
,AMT_PLANNEDPRICE1
,AMT_PREVPLANNEDPRICE
,AMT_CURPLANNEDPRICE
,DIM_DATEIDPLANNEDPRICE2
,DIM_DATEIDLASTCHANGEDPRICE
,DIM_DATEIDSNAPSHOT
,SNAPSHOTDATE
,CT_STOCKQTY_1DAYCHANGE
,CT_STOCKQTY_1WEEKCHANGE
,CT_STOCKQTY_1MONTHCHANGE
,CT_STOCKQTY_1QUARTERCHANGE
,CT_STOCKINTRANSIT_1DAYCHANGE
,CT_STOCKINTRANSIT_1WEEKCHANGE
,CT_STOCKINTRANSIT_1MONTHCHANGE
,CT_STOCKINTRANSIT_1QUARTERCHANGE
,CT_STOCKINTRANSFER_1DAYCHANGE
,CT_STOCKINTRANSFER_1WEEKCHANGE
,CT_STOCKINTRANSFER_1MONTHCHANGE
,CT_STOCKINTRANSFER_1QUARTERCHANGE
,CT_STOCKINQINSP_1DAYCHANGE
,CT_STOCKINQINSP_1WEEKCHANGE
,CT_STOCKINQINSP_1MONTHCHANGE
,CT_STOCKINQINSP_1QUARTERCHANGE
,CT_TOTALRESTRICTEDSTOCK_1DAYCHANGE
,CT_TOTALRESTRICTEDSTOCK_1WEEKCHANGE
,CT_TOTALRESTRICTEDSTOCK_1MONTHCHANGE
,CT_TOTALRESTRICTEDSTOCK_1QUARTERCHANGE
,AMT_STOCKVALUEAMT_1DAYCHANGE
,AMT_STOCKVALUEAMT_1WEEKCHANGE
,AMT_STOCKVALUEAMT_1MONTHCHANGE
,AMT_STOCKVALUEAMT_1QUARTERCHANGE
,AMT_STOCKINTRANSFERAMT_1DAYCHANGE
,AMT_STOCKINTRANSFERAMT_1WEEKCHANGE
,AMT_STOCKINTRANSFERAMT_1MONTHCHANGE
,AMT_STOCKINTRANSFERAMT_1QUARTERCHANGE
,AMT_STOCKINQINSPAMT_1DAYCHANGE
,AMT_STOCKINQINSPAMT_1WEEKCHANGE
,AMT_STOCKINQINSPAMT_1QUARTERCHANGE
,AMT_STOCKINQINSPAMT_1MONTHCHANGE
,AMT_BLOCKEDSTOCKAMT_1DAYCHANGE
,AMT_BLOCKEDSTOCKAMT_1WEEKCHANGE
,AMT_BLOCKEDSTOCKAMT_1QUARTERCHANGE
,AMT_BLOCKEDSTOCKAMT_1MONTHCHANGE
,CT_BLOCKEDSTOCK_1DAYCHANGE
,CT_BLOCKEDSTOCK_1WEEKCHANGE
,CT_BLOCKEDSTOCK_1QUARTERCHANGE
,CT_BLOCKEDSTOCK_1MONTHCHANGE
,CT_POOPENQTY
,CT_SOOPENQTY
,AMT_EXCHANGERATE_GBL
,AMT_EXCHANGERATE
,DIM_PROFITCENTERID
,CT_WIPQTY
,DIM_CURRENCYID_TRA
,DIM_CURRENCYID_GBL
,CT_GRQTY_LATE30
,CT_GRQTY_31_60
,CT_GRQTY_61_90
,CT_GRQTY_91_180
,CT_GIQTY_LATE30
,CT_GIQTY_31_60
,CT_GIQTY_61_90
,CT_GIQTY_91_180
,DD_PRODORDERNUMBER
,DD_PRODORDERITEMNO
,DIM_PRODUCTIONORDERSTATUSID
,DIM_PRODUCTIONORDERTYPEID
,DIM_PARTSALESID
,AMT_STOCKINTRANSITAMT_1WEEKCHANGE
,AMT_STOCKINTRANSITAMT_1MONTHCHANGE
,AMT_STOCKINTRANSITAMT_1QUARTERCHANGE
,AMT_STOCKINTRANSITAMT_1DAYCHANGE
,DW_INSERT_DATE
,DW_UPDATE_DATE
,DIM_PROJECTSOURCEID
,DIM_MDG_PARTID
,DIM_BWPARTID
,DIM_BWPRODUCTHIERARCHYID
,DD_INVCOVFLAG_EMD
,CT_AVGFCST_EMD
,AMT_COGSACTUALRATE_EMD
,AMT_COGSFIXEDRATE_EMD
,AMT_COGSFIXEDPLANRATE_EMD
,AMT_COGSPLANRATE_EMD
,AMT_COGSPREVYEARFIXEDRATE_EMD
,AMT_COGSPREVYEARRATE_EMD
,AMT_COGSPREVYEARTO1_EMD
,AMT_COGSPREVYEARTO2_EMD
,AMT_COGSPREVYEARTO3_EMD
,AMT_COGSTURNOVERRATE1_EMD
,AMT_COGSTURNOVERRATE2_EMD
,AMT_COGSTURNOVERRATE3_EMD
,DIM_DATEIDLASTPROCESSED
,AMT_ONHAND_STOCK_HIST
,CT_ONHAND_STOCK_HIST
,DIM_BWHIERARCHYCOUNTRYID
,DIM_CLUSTERID
,DIM_COMMERCIALVIEWID
,DIM_COUNTRYHIERARID
,DIM_COUNTRYHIERPSID
,CT_COUNTMATERIALSAFETYSTOCK
,AMT_COGS_1DAYCHANGE
,AMT_COGS_1WEEKCHANGE
,AMT_COGS_1MONTHCHANGE
,DD_ISCONSIGNED
,DIM_GSAMAPIMPORTID
,DIM_CUSTOMERIDSHIPTO
,DIM_CUSTOMERID
,CT_BASEUOMRATIOPCCUSTOM
,DD_BATCH_STATUS_QKZ
,DIM_PRODUCTIONSCHEDULERID
,CT_COGSONHAND
,CT_COGSONHAND_1M
,CT_COGSONHAND_3M
,CT_COGSONHAND_12M
,AMT_COGSONHAND_1MDELTA
,AMT_COGSONHAND_3MDELTA
,AMT_COGSONHAND_12MDELTA
,AMT_COGSONHAND
,AMT_COGSONHAND_1M
,AMT_COGSONHAND_3M
,AMT_COGSONHAND_12M
,AMT_COGSONHAND_1MCEF
,AMT_COGSONHAND_3MCEF
,AMT_COGSONHAND_12MCEF
,CT_ORDERITEMQTY
,CT_GRQTY
,CT_BASEUOMRATIOKG
,CT_INTRANSITKG
,CT_DEPENDENTDEMANDKG
,AMT_ONHAND
,AMT_ONHAND_WEEKLY_DIFF
,AMT_ONHAND_MONTHLY_DIFF
,AMT_ONHAND_YEARLY_DIFF
,DD_BATCHSTATUS
,DIM_DATEIDEXPIRYDATE
,DIM_BATCHSTATUSID
,AMT_COGSFIXEDRATE_EMD_NEW
,AMT_COGSFIXEDPLANRATE_EMD_NEW
,AMT_COGSPLANRATE_EMD_NEW
,AMT_EXCHANGERATE_GBL_NEW
,AMT_ONHAND_NEW
,AMT_STDUNITPRICE_NEW
,AMT_STDUNITPRICE_VALIDATING
,CT_OPENORDERQTYUOM
,AMT_OPENORDER
,CT_NUMBEROFPALLETS
,AMT_COGSFIXEDPLANRATE_EMD_BIS
,AMT_STDUNITPRICE_BIS
,AMT_EXCHANGERATE_GBL_BIS
,AMT_COGSONHAND_1MTHDELTA
,DD_DUMMYROW
,AMT_COGSONHAND_Q_END_DELTA
,AMT_ONHAND_MTH_END_DELTA
,CT_ONHAND_MTH_END_DELTA
,AMT_ONHAND_Q_END_DELTA
,CT_ONHAND_Q_END_DELTA
,DD_PRIMARYPRODUCTIONLOCATION
)

select
FACT_INVENTORYHISTORYID
,ifnull(CT_STOCKQTY,0)
,ifnull(AMT_STOCKVALUEAMT,0)
,ifnull(CT_LASTRECEIVEDQTY,0)
,ifnull(DD_BATCHNO,'Not Set')
,ifnull(DD_VALUATIONTYPE,'Not Set')
,ifnull(DD_DOCUMENTNO,'Not Set')
,ifnull(DD_DOCUMENTITEMNO,0)
,ifnull(DD_MOVEMENTTYPE,'Not Set')
,ifnull(DIM_STORAGELOCENTRYDATEID,1)
,ifnull(DIM_LASTRECEIVEDDATEID,1)
,ifnull(DIM_PARTID,1)
,ifnull(DIM_PLANTID,1)
,ifnull(DIM_STORAGELOCATIONID,1)
,ifnull(DIM_COMPANYID,1)
,ifnull(DIM_VENDORID,1)
,ifnull(DIM_CURRENCYID,1)
,ifnull(DIM_STOCKTYPEID,1)
,ifnull(DIM_SPECIALSTOCKID,1)
,ifnull(DIM_PURCHASEORGID,1)
,ifnull(DIM_PURCHASEGROUPID,1)
,ifnull(DIM_PRODUCTHIERARCHYID,1)
,ifnull(DIM_UNITOFMEASUREID,1)
,ifnull(DIM_MOVEMENTINDICATORID,1)
,ifnull(DIM_CONSUMPTIONTYPEID,1)
,ifnull(DIM_COSTCENTERID,1)
,ifnull(DIM_DOCUMENTSTATUSID,1)
,ifnull(DIM_DOCUMENTTYPEID,1)
,ifnull(DIM_INCOTERMID,1)
,ifnull(DIM_ITEMCATEGORYID,1)
,ifnull(DIM_ITEMSTATUSID,1)
,ifnull(DIM_TERMID,1)
,ifnull(DIM_PURCHASEMISCID,1)
,ifnull(AMT_STOCKVALUEAMT_GBL,0)
,ifnull(CT_TOTALRESTRICTEDSTOCK,0)
,ifnull(CT_STOCKINQINSP,0)
,ifnull(CT_BLOCKEDSTOCK,0)
,ifnull(CT_STOCKINTRANSFER,0)
,ifnull(CT_UNRESTRICTEDCONSGNSTOCK,0)
,ifnull(CT_RESTRICTEDCONSGNSTOCK,0)
,ifnull(CT_BLOCKEDCONSGNSTOCK,0)
,ifnull(CT_CONSGNSTOCKINQINSP,0)
,ifnull(CT_BLOCKEDSTOCKRETURNS,0)
,ifnull(AMT_BLOCKEDSTOCKAMT,0)
,ifnull(AMT_BLOCKEDSTOCKAMT_GBL,0)
,ifnull(AMT_STOCKINQINSPAMT,0)
,ifnull(AMT_STOCKINQINSPAMT_GBL,0)
,ifnull(AMT_STOCKINTRANSFERAMT,0)
,ifnull(AMT_STOCKINTRANSFERAMT_GBL,0)
,ifnull(AMT_UNRESTRICTEDCONSGNSTOCKAMT,0)
,ifnull(AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,0)
,ifnull(AMT_STDUNITPRICE,0)
,ifnull(AMT_STDUNITPRICE_GBL,0)
,ifnull(DIM_STOCKCATEGORYID,1)
,ifnull(CT_STOCKINTRANSIT,0)
,ifnull(AMT_STOCKINTRANSITAMT,0)
,ifnull(AMT_STOCKINTRANSITAMT_GBL,0)
,ifnull(DIM_SUPPLYINGPLANTID,1)
,ifnull(AMT_MTLDLVRCOST,0)
,ifnull(AMT_OVERHEADCOST,0)
,ifnull(AMT_OTHERCOST,0)
,ifnull(AMT_WIPBALANCE,0)
,ifnull(CT_WIPAGING,0)
,ifnull(AMT_MOVINGAVGPRICE,0)
,ifnull(AMT_PREVIOUSPRICE,0)
,ifnull(AMT_COMMERICALPRICE1,0)
,ifnull(AMT_PLANNEDPRICE1,0)
,ifnull(AMT_PREVPLANNEDPRICE,0)
,ifnull(AMT_CURPLANNEDPRICE,0)
,ifnull(DIM_DATEIDPLANNEDPRICE2,1)
,ifnull(DIM_DATEIDLASTCHANGEDPRICE,1)
,ifnull(DIM_DATEIDSNAPSHOT,1)
,ifnull(SNAPSHOTDATE,NULL)
,ifnull(CT_STOCKQTY_1DAYCHANGE,0)
,ifnull(CT_STOCKQTY_1WEEKCHANGE,0)
,ifnull(CT_STOCKQTY_1MONTHCHANGE,0)
,ifnull(CT_STOCKQTY_1QUARTERCHANGE,0)
,ifnull(CT_STOCKINTRANSIT_1DAYCHANGE,0)
,ifnull(CT_STOCKINTRANSIT_1WEEKCHANGE,0)
,ifnull(CT_STOCKINTRANSIT_1MONTHCHANGE,0)
,ifnull(CT_STOCKINTRANSIT_1QUARTERCHANGE,0)
,ifnull(CT_STOCKINTRANSFER_1DAYCHANGE,0)
,ifnull(CT_STOCKINTRANSFER_1WEEKCHANGE,0)
,ifnull(CT_STOCKINTRANSFER_1MONTHCHANGE,0)
,ifnull(CT_STOCKINTRANSFER_1QUARTERCHANGE,0)
,ifnull(CT_STOCKINQINSP_1DAYCHANGE,0)
,ifnull(CT_STOCKINQINSP_1WEEKCHANGE,0)
,ifnull(CT_STOCKINQINSP_1MONTHCHANGE,0)
,ifnull(CT_STOCKINQINSP_1QUARTERCHANGE,0)
,ifnull(CT_TOTALRESTRICTEDSTOCK_1DAYCHANGE,0)
,ifnull(CT_TOTALRESTRICTEDSTOCK_1WEEKCHANGE,0)
,ifnull(CT_TOTALRESTRICTEDSTOCK_1MONTHCHANGE,0)
,ifnull(CT_TOTALRESTRICTEDSTOCK_1QUARTERCHANGE,0)
,ifnull(AMT_STOCKVALUEAMT_1DAYCHANGE,0)
,ifnull(AMT_STOCKVALUEAMT_1WEEKCHANGE,0)
,ifnull(AMT_STOCKVALUEAMT_1MONTHCHANGE,0)
,ifnull(AMT_STOCKVALUEAMT_1QUARTERCHANGE,0)
,ifnull(AMT_STOCKINTRANSFERAMT_1DAYCHANGE,0)
,ifnull(AMT_STOCKINTRANSFERAMT_1WEEKCHANGE,0)
,ifnull(AMT_STOCKINTRANSFERAMT_1MONTHCHANGE,0)
,ifnull(AMT_STOCKINTRANSFERAMT_1QUARTERCHANGE,0)
,ifnull(AMT_STOCKINQINSPAMT_1DAYCHANGE,0)
,ifnull(AMT_STOCKINQINSPAMT_1WEEKCHANGE,0)
,ifnull(AMT_STOCKINQINSPAMT_1QUARTERCHANGE,0)
,ifnull(AMT_STOCKINQINSPAMT_1MONTHCHANGE,0)
,ifnull(AMT_BLOCKEDSTOCKAMT_1DAYCHANGE,0)
,ifnull(AMT_BLOCKEDSTOCKAMT_1WEEKCHANGE,0)
,ifnull(AMT_BLOCKEDSTOCKAMT_1QUARTERCHANGE,0)
,ifnull(AMT_BLOCKEDSTOCKAMT_1MONTHCHANGE,0)
,ifnull(CT_BLOCKEDSTOCK_1DAYCHANGE,0)
,ifnull(CT_BLOCKEDSTOCK_1WEEKCHANGE,0)
,ifnull(CT_BLOCKEDSTOCK_1QUARTERCHANGE,0)
,ifnull(CT_BLOCKEDSTOCK_1MONTHCHANGE,0)
,ifnull(CT_POOPENQTY,0)
,ifnull(CT_SOOPENQTY,0)
,ifnull(AMT_EXCHANGERATE_GBL,1)
,ifnull(AMT_EXCHANGERATE,1)
,ifnull(DIM_PROFITCENTERID,1)
,ifnull(CT_WIPQTY,0)
,ifnull(DIM_CURRENCYID_TRA,1)
,ifnull(DIM_CURRENCYID_GBL,1)
,ifnull(CT_GRQTY_LATE30,0)
,ifnull(CT_GRQTY_31_60,0)
,ifnull(CT_GRQTY_61_90,0)
,ifnull(CT_GRQTY_91_180,0)
,ifnull(CT_GIQTY_LATE30,0)
,ifnull(CT_GIQTY_31_60,0)
,ifnull(CT_GIQTY_61_90,0)
,ifnull(CT_GIQTY_91_180,0)
,ifnull(DD_PRODORDERNUMBER,'Not Set')
,ifnull(DD_PRODORDERITEMNO,NULL)
,ifnull(DIM_PRODUCTIONORDERSTATUSID,1)
,ifnull(DIM_PRODUCTIONORDERTYPEID,1)
,ifnull(DIM_PARTSALESID,1)
,ifnull(AMT_STOCKINTRANSITAMT_1WEEKCHANGE,0)
,ifnull(AMT_STOCKINTRANSITAMT_1MONTHCHANGE,0)
,ifnull(AMT_STOCKINTRANSITAMT_1QUARTERCHANGE,0)
,ifnull(AMT_STOCKINTRANSITAMT_1DAYCHANGE,0)
,ifnull(DW_INSERT_DATE,CURRENT_TIMESTAMP)
,ifnull(DW_UPDATE_DATE,CURRENT_TIMESTAMP)
,ifnull(DIM_PROJECTSOURCEID,1)
,ifnull(DIM_MDG_PARTID,1)
,ifnull(DIM_BWPARTID,1)
,ifnull(DIM_BWPRODUCTHIERARCHYID,1)
,ifnull(DD_INVCOVFLAG_EMD,'Not Set')
,ifnull(CT_AVGFCST_EMD,0)
,ifnull(AMT_COGSACTUALRATE_EMD,0)
,ifnull(AMT_COGSFIXEDRATE_EMD,0)
,ifnull(AMT_COGSFIXEDPLANRATE_EMD,0)
,ifnull(AMT_COGSPLANRATE_EMD,0)
,ifnull(AMT_COGSPREVYEARFIXEDRATE_EMD,0)
,ifnull(AMT_COGSPREVYEARRATE_EMD,0)
,ifnull(AMT_COGSPREVYEARTO1_EMD,0)
,ifnull(AMT_COGSPREVYEARTO2_EMD,0)
,ifnull(AMT_COGSPREVYEARTO3_EMD,0)
,ifnull(AMT_COGSTURNOVERRATE1_EMD,0)
,ifnull(AMT_COGSTURNOVERRATE2_EMD,0)
,ifnull(AMT_COGSTURNOVERRATE3_EMD,0)
,ifnull(DIM_DATEIDLASTPROCESSED,1)
,ifnull(AMT_ONHAND_STOCK_HIST,0)
,ifnull(CT_ONHAND_STOCK_HIST,0)
,ifnull(DIM_BWHIERARCHYCOUNTRYID,1)
,ifnull(DIM_CLUSTERID,1)
,ifnull(DIM_COMMERCIALVIEWID,1)
,ifnull(DIM_COUNTRYHIERARID,1)
,ifnull(DIM_COUNTRYHIERPSID,1)
,ifnull(CT_COUNTMATERIALSAFETYSTOCK,0)
,ifnull(AMT_COGS_1DAYCHANGE,0)
,ifnull(AMT_COGS_1WEEKCHANGE,0)
,ifnull(AMT_COGS_1MONTHCHANGE,0)
,ifnull(DD_ISCONSIGNED,0)
,ifnull(DIM_GSAMAPIMPORTID,1)
,ifnull(DIM_CUSTOMERIDSHIPTO,1)
,ifnull(DIM_CUSTOMERID,1)
,ifnull(CT_BASEUOMRATIOPCCUSTOM,1)
,ifnull(DD_BATCH_STATUS_QKZ,'Not Set')
,ifnull(DIM_PRODUCTIONSCHEDULERID,1)
,ifnull(CT_COGSONHAND,0)
,ifnull(CT_COGSONHAND_1M,0)
,ifnull(CT_COGSONHAND_3M,0)
,ifnull(CT_COGSONHAND_12M,0)
,ifnull(AMT_COGSONHAND_1MDELTA,0)
,ifnull(AMT_COGSONHAND_3MDELTA,0)
,ifnull(AMT_COGSONHAND_12MDELTA,0)
,ifnull(AMT_COGSONHAND,0)
,ifnull(AMT_COGSONHAND_1M,0)
,ifnull(AMT_COGSONHAND_3M,0)
,ifnull(AMT_COGSONHAND_12M,0)
,ifnull(AMT_COGSONHAND_1MCEF,0)
,ifnull(AMT_COGSONHAND_3MCEF,0)
,ifnull(AMT_COGSONHAND_12MCEF,0)
,ifnull(CT_ORDERITEMQTY,0)
,ifnull(CT_GRQTY,0)
,ifnull(CT_BASEUOMRATIOKG,1)
,ifnull(CT_INTRANSITKG,0)
,ifnull(CT_DEPENDENTDEMANDKG,0)
,ifnull(AMT_ONHAND,0)
,ifnull(AMT_ONHAND_WEEKLY_DIFF,0)
,ifnull(AMT_ONHAND_MONTHLY_DIFF,0)
,ifnull(AMT_ONHAND_YEARLY_DIFF,0)
,ifnull(DD_BATCHSTATUS,'Not Set')
,ifnull(DIM_DATEIDEXPIRYDATE,1)
,ifnull(DIM_BATCHSTATUSID,1)
,ifnull(AMT_COGSFIXEDRATE_EMD_NEW,0)
,ifnull(AMT_COGSFIXEDPLANRATE_EMD_NEW,0)
,ifnull(AMT_COGSPLANRATE_EMD_NEW,0)
,ifnull(AMT_EXCHANGERATE_GBL_NEW,1)
,ifnull(AMT_ONHAND_NEW,0)
,ifnull(AMT_STDUNITPRICE_NEW,0)
,ifnull(AMT_STDUNITPRICE_VALIDATING,0)
,ifnull(CT_OPENORDERQTYUOM,0)
,ifnull(AMT_OPENORDER,0)
,ifnull(CT_NUMBEROFPALLETS,0)
,ifnull(AMT_COGSFIXEDPLANRATE_EMD_BIS,0)
,ifnull(AMT_STDUNITPRICE_BIS,0)
,ifnull(AMT_EXCHANGERATE_GBL_BIS,1)
,ifnull(AMT_COGSONHAND_1MTHDELTA,0)
,ifnull(DD_DUMMYROW,0)
,ifnull(AMT_COGSONHAND_Q_END_DELTA,0)
,ifnull(AMT_ONHAND_MTH_END_DELTA,0)
,ifnull(CT_ONHAND_MTH_END_DELTA,0)
,ifnull(AMT_ONHAND_Q_END_DELTA,0)
,ifnull(CT_ONHAND_Q_END_DELTA,0)
,DD_PRIMARYPRODUCTIONLOCATION

from tmp_fact_inventoryhistory_Oracle;

truncate table tmp_fact_inventoryhistory_Oracle;


update emd586.fact_inventoryhistory f
set f.amt_exchangerate_gbl= ff.amt_exchangerate_gbl_bis
FROM emd586.fact_inventoryhistory f
     ,emdoracle4ae.fact_ora_invonhand_daily ff
    ,emdoracle4ae.dim_projectsource p

WHERE f.fact_inventoryhistoryid=ff.fact_ora_invonhand_dailyid + IFNULL(p.dim_projectsourceid * p.multiplier ,0)
and f.dim_projectsourceid=7
and to_date(ff.snapshot_date)='2016-12-31'
and f.amt_exchangerate_gbl<>ff.amt_exchangerate_gbl_bis;


