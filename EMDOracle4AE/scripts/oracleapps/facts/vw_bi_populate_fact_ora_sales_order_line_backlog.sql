/*delete rows*/
/*DELETE FROM fact_ora_sales_order_line_backorder*/
DELETE FROM fact_ora_sales_order_line_backorder WHERE TO_DATE(snapshot_date) = TO_DATE(CURRENT_DATE) - 1;

DELETE FROM NUMBER_FOUNTAIN_fact_ora_sales_order_line_backorder WHERE table_name = 'fact_ora_sales_order_line_backorder';

INSERT INTO NUMBER_FOUNTAIN_fact_ora_sales_order_line_backorder
SELECT
     'fact_ora_sales_order_line_backorder'
	 ,IFNULL(MAX(fact_ora_sales_order_line_backorderid),0)
  FROM fact_ora_sales_order_line_backorder;

/*DELETE FROM fact_ora_sales_order_line_backlog*/
/*select count(*) from fact_ora_sales_order_line*/
/*select count(*) from fact_ora_sales_order_line_backlog*/
/*select * from fact_ora_sales_order_line_backlog*/



/*insert new rows*/
INSERT INTO fact_ora_sales_order_line_backorder
(
	 fact_ora_sales_order_line_backorderid
    ,fact_ora_sales_order_lineid
	,dim_ora_account_repid
	,dim_ora_business_orgid
	,dim_ora_channel_typeid
	,dim_ora_company_orgid
	,dim_ora_cost_centerid
	,dim_ora_customer_bill_to_locationid
	,dim_ora_customer_ship_to_locationid
	,dim_ora_customer_sold_to_locationid
	,dim_ora_customer_bill_toid
	,dim_ora_customer_ship_toid
	,dim_ora_customer_sold_toid
	,dim_ora_customer_account_bill_toid
	,dim_ora_customer_account_ship_toid
	,dim_ora_customer_account_sold_toid
	,dim_ora_inventory_orgid
	,dim_ora_inv_productid
	,dim_ora_order_line_statusid
	,dim_ora_payment_methodid
	,dim_ora_payment_termid
	,dim_ora_productid
	,dim_ora_sales_groupid
	,dim_ora_sales_office_locationid
	,dim_ora_salesrepid
	,dim_ora_servicerepid
	,dim_ora_order_line_typeid
	,dim_ora_order_header_typeid
	,dim_ora_order_header_statusid
	,dim_ora_freight_termid
	,dim_ora_shipment_methodid
	,dim_ora_date_order_bookedid
	,dim_ora_date_customer_requestedid
	,dim_ora_otd_dateid
	,dim_ora_date_enteredid
	,dim_ora_date_orderedid
	,dim_ora_date_promise_deliveryid
	,dim_ora_date_scheduled_shipid
	,dim_ora_date_actual_shipid
	,dim_ora_date_earliest_shipmentid
	,dim_ora_date_order_confirmedid
	,dim_ora_date_actual_fulfillmentid
	,dim_ora_date_fulfillmentid
	,dim_ora_date_actual_arrival_dateid
	,dim_ora_date_schedule_arrivalid
	,dim_ora_date_taxid
	,dim_ora_date_pricingid
	,dim_ora_gl_set_of_booksid
	,dim_ora_hr_operatingunitid
	,dim_ora_aging_days_bucketid
	,ct_aging_days
	,ct_product_unit_cost
	,ct_product_unit_list_price
	,ct_product_unit_selling_price
	,ct_manufacturing_lead_time
	,ct_ship_tolerance_above
	,ct_ship_tolerance_below
	,ct_delivery_lead_time
	,ct_ordered_quantity
	,ct_cancelled_quantity
	,ct_sales_quantity
	,ct_fulfilled_quantity
	,ct_confirmed_quantity
	,ct_fill_quantity
	,ct_fill_quantity_crd
	,ct_total_shipped_quantity
	,ct_total_invoiced_quantity
	,amt_net_ordered
	,amt_tax
	,amt_cost
	,amt_list
	,amt_discount
	,dd_line_category
	,dd_order_quantity_uom
	,dd_order_number
	,dd_line_number
	,dd_shipment_number
	,dd_orig_ref_doc_number
	,dd_cust_po_number
	,dd_otif_flag
	,dd_cancelled_h_flag
	,dd_cancelled_l_flag
	,dd_open_flag
	,dd_fulfilled_flag
	,dd_shippable_flag
	,dd_tax_exempt_flag
	,dd_booked_flag
	,dd_model_remnant_flag
	,dd_on_hold_flag
	,dd_financial_backlog_flag
	,dd_operation_backlog_flag
	,dd_hold_name
	,dd_header_id
	,dd_line_id
	,dd_transactional_curr_code
	,dd_functional_curr_code
	,dd_shipped_not_invoiced_flag
	,ct_so_touch_count
	,dd_line_flow_status_code
	,dd_shipping_interfaced_flag
	,amt_exchangerate
	,amt_exchangerate_gbl
	,dim_ora_created_byid
	,dim_ora_last_updated_byid
	,dim_ora_date_creationid
	,dim_ora_date_last_updateid
	,dim_ora_mtl_plannersid
	,dim_projectsourceid
	,dw_insert_date
	,dw_update_date
	,rowstartdate
	,rowenddate
	,rowiscurrent
	,rowchangereason
	,source_id
	,ct_pushout_pullin
	,ct_order_change
	,amt_goal
	,amt_revenuedollarsbilling
	,ct_quantitybilling
	,dd_crdflag
	,dd_csdflag
	,dd_hold_date
	,dd_rowiscurrent
	,dd_subinventory
	,dim_ora_mdg_productid
	,dim_ora_bwproducthierarchyid
	,dim_ora_dropship_addressid
	,dim_ora_otd_promise_dateid
	,dd_headertype
	,dd_attribute11
	,dd_context
	,dim_ora_plantid
	,dim_backorder_dateid
	,dd_backorder_responsibility
	,dd_backorder_resp_geography
	,dd_backorder_status
	,dd_backorderreason
	,dd_backorder
	,dd_lsintercompflag
	,dd_intercompflag
	,dd_tradsales_flag
	,dd_backord_reason_lookupt
	,dim_ora_clusterid
	,dim_ora_commercialviewid
	,dd_coveredbymaterial
	,dd_coveredbyplant
	,dd_lineno_compno
	,dd_ace_openorders
	,dd_deliveryontime
	,dd_deliveryisfull
	,ct_deliveryisfull
	,ct_deliveryontime
	,snapshot_date
	,dim_ora_date_snapshotid
	,ct_countsalesdocitem
	,dim_otd_dateid_dimD
	,dim_ora_otd_promise_dateid_dimD
	,dim_ora_date_customer_requestedid_dimD
	,dim_countryhierpsid
	,dim_countryhierarid
	,dim_ora_gsa_codeid
	,dim_ora_date_enddateloadingid
	,dd_inco1
	,dd_inco2
	,dd_mtomts
	,ct_LineReserved_qty
	,dd_delivery_group
  ,dd_single_lot
  ,DIM_BLOCKEDBACKORDERBYPLANTID
)
SELECT
	(SELECT max_id FROM NUMBER_FOUNTAIN_fact_ora_sales_order_line_backorder WHERE table_name = 'fact_ora_sales_order_line_backorder' ) + ROW_NUMBER() OVER (order by '') fact_ora_sales_order_line_backorderid
	,f.fact_ora_sales_order_lineid
	,f.dim_ora_account_repid
	,f.dim_ora_business_orgid
	,f.dim_ora_channel_typeid
	,f.dim_ora_company_orgid
	,f.dim_ora_cost_centerid
	,f.dim_ora_customer_bill_to_locationid
	,f.dim_ora_customer_ship_to_locationid
	,f.dim_ora_customer_sold_to_locationid
	,f.dim_ora_customer_bill_toid
	,f.dim_ora_customer_ship_toid
	,f.dim_ora_customer_sold_toid
	,f.dim_ora_customer_account_bill_toid
	,f.dim_ora_customer_account_ship_toid
	,f.dim_ora_customer_account_sold_toid
	,f.dim_ora_inventory_orgid
	,f.dim_ora_inv_productid
	,f.dim_ora_order_line_statusid
	,f.dim_ora_payment_methodid
	,f.dim_ora_payment_termid
	,f.dim_ora_productid
	,f.dim_ora_sales_groupid
	,f.dim_ora_sales_office_locationid
	,f.dim_ora_salesrepid
	,f.dim_ora_servicerepid
	,f.dim_ora_order_line_typeid
	,f.dim_ora_order_header_typeid
	,f.dim_ora_order_header_statusid
	,f.dim_ora_freight_termid
	,f.dim_ora_shipment_methodid
	,f.dim_ora_date_order_bookedid
	,f.dim_ora_date_customer_requestedid
	,f.dim_ora_otd_dateid
	,f.dim_ora_date_enteredid
	,f.dim_ora_date_orderedid
	,f.dim_ora_date_promise_deliveryid
	,f.dim_ora_date_scheduled_shipid
	,f.dim_ora_date_actual_shipid
	,f.dim_ora_date_earliest_shipmentid
	,f.dim_ora_date_order_confirmedid
	,f.dim_ora_date_actual_fulfillmentid
	,f.dim_ora_date_fulfillmentid
	,f.dim_ora_date_actual_arrival_dateid
	,f.dim_ora_date_schedule_arrivalid
	,f.dim_ora_date_taxid
	,f.dim_ora_date_pricingid
	,f.dim_ora_gl_set_of_booksid
	,f.dim_ora_hr_operatingunitid
	,f.dim_ora_aging_days_bucketid
	,f.ct_aging_days
	,f.ct_product_unit_cost
	,f.ct_product_unit_list_price
	,f.ct_product_unit_selling_price
	,f.ct_manufacturing_lead_time
	,f.ct_ship_tolerance_above
	,f.ct_ship_tolerance_below
	,f.ct_delivery_lead_time
	,f.ct_ordered_quantity
	,f.ct_cancelled_quantity
	,f.ct_sales_quantity
	,f.ct_fulfilled_quantity
	,f.ct_confirmed_quantity
	,f.ct_fill_quantity
	,f.ct_fill_quantity_crd
	,f.ct_total_shipped_quantity
	,f.ct_total_invoiced_quantity
	,f.amt_net_ordered
	,f.amt_tax
	,f.amt_cost
	,f.amt_list
	,f.amt_discount
	,f.dd_line_category
	,f.dd_order_quantity_uom
	,f.dd_order_number
	,f.dd_line_number
	,f.dd_shipment_number
	,f.dd_orig_ref_doc_number
	,f.dd_cust_po_number
	,f.dd_otif_flag
	,f.dd_cancelled_h_flag
	,f.dd_cancelled_l_flag
	,f.dd_open_flag
	,f.dd_fulfilled_flag
	,f.dd_shippable_flag
	,f.dd_tax_exempt_flag
	,f.dd_booked_flag
	,f.dd_model_remnant_flag
	,f.dd_on_hold_flag
	,f.dd_financial_backlog_flag
	,f.dd_operation_backlog_flag
	,f.dd_hold_name
	,f.dd_header_id
	,f.dd_line_id
	,f.dd_transactional_curr_code
	,f.dd_functional_curr_code
	,f.dd_shipped_not_invoiced_flag
	,f.ct_so_touch_count
	,f.dd_line_flow_status_code
	,f.dd_shipping_interfaced_flag
	,f.amt_exchangerate
	,f.amt_exchangerate_gbl
	,f.dim_ora_created_byid
	,f.dim_ora_last_updated_byid
	,f.dim_ora_date_creationid
	,f.dim_ora_date_last_updateid
	,f.dim_ora_mtl_plannersid
	,f.dim_projectsourceid
	,f.dw_insert_date
	,f.dw_update_date
	,f.rowstartdate
	,f.rowenddate
	,f.rowiscurrent
	,f.rowchangereason
	,f.source_id
	,f.ct_pushout_pullin
	,f.ct_order_change
	,f.amt_goal
	,f.amt_revenuedollarsbilling
	,f.ct_quantitybilling
	,f.dd_crdflag
	,f.dd_csdflag
	,f.dd_hold_date
	,f.dd_rowiscurrent
	,f.dd_subinventory
	,f.dim_ora_mdg_productid
	,f.dim_ora_bwproducthierarchyid
	,f.dim_ora_dropship_addressid
	,f.dim_ora_otd_promise_dateid
	,f.dd_headertype
	,f.dd_attribute11
	,f.dd_context
	,f.dim_ora_plantid
	,f.dim_backorder_dateid
	,f.dd_backorder_responsibility
	,f.dd_backorder_resp_geography
	,f.dd_backorder_status
	,f.dd_backorderreason
	,f.dd_backorder
	,f.dd_lsintercompflag
	,f.dd_intercompflag
	,f.dd_tradsales_flag
	,f.dd_backord_reason_lookupt
	,f.dim_ora_clusterid
	,f.dim_ora_commercialviewid
	,f.dd_coveredbymaterial
	,f.dd_coveredbyplant
	,f.dd_lineno_compno
	,f.dd_ace_openorders
	,f.dd_deliveryontime
	,f.dd_deliveryisfull
	,f.ct_deliveryisfull
	,f.ct_deliveryontime
	,CURRENT_DATE - 1 snapshot_date
	,1 dim_ora_date_snapshotid
	,ct_countsalesdocitem
	,f.dim_otd_dateid_dimD
	,f.dim_ora_otd_promise_dateid_dimD
	,dim_ora_date_customer_requestedid_dimD
	,dim_countryhierpsid
	,dim_countryhierarid
	,dim_ora_gsa_codeid
	,dim_ora_date_enddateloadingid
	,dd_inco1
	,dd_inco2
	,CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END as dd_mtomts
	,ct_LineReserved_qty
	,dd_delivery_group
  ,dd_single_lot
,DIM_BLOCKEDBACKORDERBYPLANTID
FROM fact_ora_sales_order_line f
	INNER JOIN dim_ora_invproduct d_inv ON f.dim_ora_inv_productid = d_inv.dim_ora_invproductid
	INNER JOIN dim_ora_bwproducthierarchy uph ON f.dim_ora_bwproducthierarchyid = uph.dim_bwproducthierarchyid
	INNER JOIN dim_date_factory_calendar mts ON f.dim_ora_date_customer_requestedid = mts.dim_dateid
	INNER JOIN dim_date_factory_calendar mto ON (case when uph.businessdesc like '%Research Solutions%'
    then f.dim_ora_date_customer_requestedid else f.dim_ora_otd_promise_dateid end) = mto.dim_dateid

WHERE
CASE
  WHEN mts.datevalue < CURRENT_DATE
	  AND mts.datevalue  <> '0001-01-01'
	  AND CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END  = 'MTS'
  THEN 'Yes'
  WHEN mto.datevalue < CURRENT_DATE
	  AND mto.datevalue <> '0001-01-01'
	  AND CASE WHEN d_inv.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END = 'MTO'
  THEN  'Yes'
  ELSE 'No'
END = 'Yes'
AND f.dd_backorder = 'X'
AND uph.businesssector = 'BS-02';


/*UPDATE DIM_ORA_DATE_SNAPSHOTID*/
UPDATE fact_ora_sales_order_line_backorder f
 SET f.dim_ora_date_snapshotid = d.dim_dateid
    ,dw_update_date = CURRENT_DATE
FROM fact_ora_sales_order_line_backorder f, dim_date d
WHERE TO_DATE(f.snapshot_date) = d.datevalue
  AND f.dim_ora_date_snapshotid = 1;



/* Added Backorder Variance by Alina on the 6th September */

DROP TABLE IF EXISTS TMP_BACKORDER_VARIANCE;

CREATE TABLE TMP_BACKORDER_VARIANCE

AS

select f_sosnp.snapshot_date as snapshot_date
,prt.segment1
,f_sosnp.DD_INTERCOMPFLAG

,SUM(cast((case when SUBSTR(sdt.name,-2,2) IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')
then (case when ((CASE WHEN dd_line_category ='RETURN' THEN ct_ordered_quantity*-1.00 ELSE ct_ordered_quantity END)
- ((CASE WHEN dd_line_category ='RETURN' THEN -1.00 * ct_total_shipped_quantity ELSE ct_total_shipped_quantity END))) < 0 then
0.0000 else ((CASE WHEN dd_line_category ='RETURN' THEN ct_ordered_quantity*-1.00 ELSE ct_ordered_quantity END ) -
(( CASE WHEN dd_line_category ='RETURN' THEN -1.00 * ct_total_shipped_quantity ELSE ct_total_shipped_quantity END))) end
 * CT_PRODUCT_UNIT_SELLING_PRICE) else
(case when ((CASE WHEN dd_line_category ='RETURN' THEN ct_ordered_quantity*-1.00 ELSE ct_ordered_quantity END) - (CASE WHEN dd_line_category ='RETURN' THEN -1.00 * ct_total_shipped_quantity ELSE ct_total_shipped_quantity END)) < 0
then 0.0000 else
((CASE WHEN dd_line_category ='RETURN' THEN ct_ordered_quantity*-1.00 ELSE ct_ordered_quantity END) - (CASE WHEN dd_line_category ='RETURN' THEN -1.00 * ct_total_shipped_quantity ELSE ct_total_shipped_quantity END)) end
* CT_PRODUCT_UNIT_SELLING_PRICE) end * amt_ExchangeRate_GBL) as decimal(18,4))) OpenAmt

from fact_ora_sales_order_line_backorder f_sosnp
inner join dim_ora_invproduct prt on f_sosnp.dim_ora_inv_productid =prt.dim_ora_invproductid

inner join dim_ora_xacttype sdt on f_sosnp.DIM_ORA_ORDER_HEADER_TYPEID = sdt.DIM_ORA_XACTTYPEID

group by f_sosnp.snapshot_date,prt.segment1,f_sosnp.DD_INTERCOMPFLAG;


DROP TABLE IF EXISTS TMP_MAX_ID;

CREATE TABLE TMP_MAX_ID

AS

SELECT f_sosnp.snapshot_date as snapshot_date

,prt.segment1
,f_sosnp.DD_INTERCOMPFLAG
,MAX(fact_ora_sales_order_line_backorderid) fact_ora_sales_order_line_backorderid

FROM fact_ora_sales_order_line_backorder f_sosnp

inner join dim_ora_invproduct prt on f_sosnp.dim_ora_inv_productid =prt.dim_ora_invproductid

GROUP BY f_sosnp.snapshot_date

,prt.segment1,f_sosnp.DD_INTERCOMPFLAG;


MERGE INTO fact_ora_sales_order_line_backorder f_sosnp

USING (SELECT f.fact_ora_sales_order_line_backorderid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt

FROM fact_ora_sales_order_line_backorder f

INNER JOIN dim_date snp ON f.dim_ora_date_snapshotid = snp.dim_dateid

inner join dim_ora_invproduct prt on f.dim_ora_inv_productid =prt.dim_ora_invproductid

INNER JOIN TMP_MAX_ID t ON f.fact_ora_sales_order_line_backorderid = t.fact_ora_sales_order_line_backorderid

LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.snapshot_date = snp.datevalue AND a.segment1 = prt.segment1 AND a.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG

LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.snapshot_date = snp.datevalue - 1 AND b.segment1 = prt.segment1 AND b.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG

WHERE f.amt_openqtyvalue1daychange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x

ON f_sosnp.fact_ora_sales_order_line_backorderid = x.fact_ora_sales_order_line_backorderid

WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1daychangepart= x.OpnAmt;


MERGE INTO fact_ora_sales_order_line_backorder f_sosnp

USING (SELECT f.fact_ora_sales_order_line_backorderid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt

FROM fact_ora_sales_order_line_backorder f

INNER JOIN dim_date snp ON f.dim_ora_date_snapshotid = snp.dim_dateid

inner join dim_ora_invproduct prt on f.dim_ora_inv_productid =prt.dim_ora_invproductid

INNER JOIN TMP_MAX_ID t ON f.fact_ora_sales_order_line_backorderid = t.fact_ora_sales_order_line_backorderid

LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.snapshot_date = snp.datevalue AND a.segment1 = prt.segment1 AND a.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG

LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.snapshot_date = snp.datevalue - 7 AND b.segment1 = prt.segment1 AND b.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG

WHERE f.amt_openqtyvalue1weekchange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x

ON f_sosnp.fact_ora_sales_order_line_backorderid = x.fact_ora_sales_order_line_backorderid

WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1weekchangepart = x.OpnAmt;



MERGE INTO fact_ora_sales_order_line_backorder f_sosnp

USING (SELECT f.fact_ora_sales_order_line_backorderid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt

FROM fact_ora_sales_order_line_backorder f

INNER JOIN dim_date snp ON f.dim_ora_date_snapshotid = snp.dim_dateid

inner join dim_ora_invproduct prt on f.dim_ora_inv_productid =prt.dim_ora_invproductid

INNER JOIN TMP_MAX_ID t ON f.fact_ora_sales_order_line_backorderid = t.fact_ora_sales_order_line_backorderid

LEFT JOIN TMP_BACKORDER_VARIANCE a ON  a.snapshot_date = snp.datevalue AND a.segment1 = prt.segment1 AND a.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG

LEFT JOIN TMP_BACKORDER_VARIANCE b ON  b.snapshot_date= add_months(snp.datevalue,-1)
AND b.segment1 = prt.segment1 AND b.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG

WHERE f.amt_openqtyvalue1monthchange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x

ON f_sosnp.fact_ora_sales_order_line_backorderid = x.fact_ora_sales_order_line_backorderid

WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1monthchangepart = x.OpnAmt;

DROP TABLE IF EXISTS TMP_BACKORDER_VARIANCE;
DROP TABLE IF EXISTS TMP_MAX_ID;
DROP TABLE IF EXISTS TMP_MAX_SNP;


DROP TABLE IF EXISTS TMP_BACKORDER_VARIANCE;

CREATE TABLE TMP_BACKORDER_VARIANCE

AS

select f_sosnp.snapshot_date as snapshot_date

,f_sosnp.dim_projectsourceid as dim_projectsourceid

,uph.businessdesc as businessdesc
,SUM(cast((case when SUBSTR(sdt.name,-2,2) IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')
then (case when ((CASE WHEN dd_line_category ='RETURN' THEN ct_ordered_quantity*-1.00 ELSE ct_ordered_quantity END)
- ((CASE WHEN dd_line_category ='RETURN' THEN -1.00 * ct_total_shipped_quantity ELSE ct_total_shipped_quantity END))) < 0 then
0.0000 else ((CASE WHEN dd_line_category ='RETURN' THEN ct_ordered_quantity*-1.00 ELSE ct_ordered_quantity END ) -
(( CASE WHEN dd_line_category ='RETURN' THEN -1.00 * ct_total_shipped_quantity ELSE ct_total_shipped_quantity END))) end
 * CT_PRODUCT_UNIT_SELLING_PRICE) else
(case when ((CASE WHEN dd_line_category ='RETURN' THEN ct_ordered_quantity*-1.00 ELSE ct_ordered_quantity END) - (CASE WHEN dd_line_category ='RETURN' THEN -1.00 * ct_total_shipped_quantity ELSE ct_total_shipped_quantity END)) < 0
then 0.0000 else
((CASE WHEN dd_line_category ='RETURN' THEN ct_ordered_quantity*-1.00 ELSE ct_ordered_quantity END) - (CASE WHEN dd_line_category ='RETURN' THEN -1.00 * ct_total_shipped_quantity ELSE ct_total_shipped_quantity END)) end
* CT_PRODUCT_UNIT_SELLING_PRICE) end * amt_ExchangeRate_GBL) as decimal(18,4))) OpenAmt

from fact_ora_sales_order_line_backorder f_sosnp

inner join dim_ora_bwproducthierarchy uph on f_sosnp.dim_ora_bwproducthierarchyid = uph.dim_bwproducthierarchyid

inner join dim_ora_xacttype sdt on f_sosnp.DIM_ORA_ORDER_HEADER_TYPEID = sdt.DIM_ORA_XACTTYPEID

group by f_sosnp.snapshot_date,uph.businessdesc,f_sosnp.dim_projectsourceid;


DROP TABLE IF EXISTS TMP_MAX_ID;

CREATE TABLE TMP_MAX_ID

AS

SELECT f_sosnp.snapshot_date as snapshot_date

,f_sosnp.dim_projectsourceid as dim_projectsourceid

,uph.businessdesc as businessdesc

,MAX(fact_ora_sales_order_line_backorderid) fact_ora_sales_order_line_backorderid

FROM fact_ora_sales_order_line_backorder f_sosnp

inner join dim_ora_bwproducthierarchy uph on f_sosnp.dim_ora_bwproducthierarchyid = uph.dim_bwproducthierarchyid

GROUP BY f_sosnp.snapshot_date

,uph.businessdesc

,f_sosnp.dim_projectsourceid;


MERGE INTO fact_ora_sales_order_line_backorder f_sosnp

USING (SELECT f.fact_ora_sales_order_line_backorderid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt

FROM fact_ora_sales_order_line_backorder f

INNER JOIN dim_date snp ON f.dim_ora_date_snapshotid = snp.dim_dateid

inner join dim_ora_bwproducthierarchy uph on f.dim_ora_bwproducthierarchyid = uph.dim_bwproducthierarchyid

INNER JOIN TMP_MAX_ID t ON f.fact_ora_sales_order_line_backorderid = t.fact_ora_sales_order_line_backorderid

LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.dim_projectsourceid = f.dim_projectsourceid AND a.snapshot_date = snp.datevalue AND a.businessdesc = uph.businessdesc

LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.dim_projectsourceid = f.dim_projectsourceid AND b.snapshot_date = snp.datevalue - 1 AND b.businessdesc = uph.businessdesc

WHERE f.amt_openqtyvalue1daychange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x

ON f_sosnp.fact_ora_sales_order_line_backorderid = x.fact_ora_sales_order_line_backorderid

WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1daychange = x.OpnAmt;


MERGE INTO fact_ora_sales_order_line_backorder f_sosnp

USING (SELECT f.fact_ora_sales_order_line_backorderid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt

FROM fact_ora_sales_order_line_backorder f

INNER JOIN dim_date snp ON f.dim_ora_date_snapshotid = snp.dim_dateid

inner join dim_ora_bwproducthierarchy uph on f.dim_ora_bwproducthierarchyid = uph.dim_bwproducthierarchyid

INNER JOIN TMP_MAX_ID t ON f.fact_ora_sales_order_line_backorderid = t.fact_ora_sales_order_line_backorderid

LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.dim_projectsourceid = f.dim_projectsourceid AND a.snapshot_date = snp.datevalue AND a.businessdesc = uph.businessdesc

LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.dim_projectsourceid = f.dim_projectsourceid AND b.snapshot_date = snp.datevalue - 7 AND b.businessdesc = uph.businessdesc

WHERE f.amt_openqtyvalue1weekchange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x

ON f_sosnp.fact_ora_sales_order_line_backorderid = x.fact_ora_sales_order_line_backorderid

WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1weekchange = x.OpnAmt;


MERGE INTO fact_ora_sales_order_line_backorder f_sosnp

USING (SELECT f.fact_ora_sales_order_line_backorderid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt

FROM fact_ora_sales_order_line_backorder f

INNER JOIN dim_date snp ON f.dim_ora_date_snapshotid = snp.dim_dateid

inner join dim_ora_bwproducthierarchy uph on f.dim_ora_bwproducthierarchyid = uph.dim_bwproducthierarchyid

INNER JOIN TMP_MAX_ID t ON f.fact_ora_sales_order_line_backorderid = t.fact_ora_sales_order_line_backorderid

LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.dim_projectsourceid = f.dim_projectsourceid AND a.snapshot_date = snp.datevalue AND a.businessdesc = uph.businessdesc

LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.dim_projectsourceid = f.dim_projectsourceid AND b.snapshot_date= add_months(snp.datevalue,-1)
AND b.businessdesc = uph.businessdesc

WHERE f.amt_openqtyvalue1monthchange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x

ON f_sosnp.fact_ora_sales_order_line_backorderid = x.fact_ora_sales_order_line_backorderid

WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1monthchange = x.OpnAmt;


DROP TABLE IF EXISTS TMP_BACKORDER_VARIANCE;
DROP TABLE IF EXISTS TMP_MAX_ID;
DROP TABLE IF EXISTS TMP_MAX_SNP;

/* Update Customer ShipTo according to CTT logic - 28th October 2016 by Alina*/

truncate table temp_CCT;
insert into temp_CCT
select distinct fo.dd_salesdocno,fo.dim_customerid
from EMDTempoCC4.fact_salesorder fo;


UPDATE fact_ora_sales_order_line_backorder f
SET dim_ora_customer_ship_to_locationid =  case when SUBSTR(d.description,1,20) like '%Internal Drop Ship%' then fo.dim_customerid
                                           else f.dim_ora_customer_ship_to_locationid end
FROM dim_ora_xacttype d,fact_ora_sales_order_line_backorder f,temp_CCT fo
WHERE f.DIM_ORA_ORDER_HEADER_TYPEID=d.DIM_ORA_XACTTYPEID
AND SUBSTR(d.description,1,20) like '%Internal Drop Ship%'
AND SUBSTR(f.dd_cust_po_number,1,40)=fo.dd_salesdocno;

UPDATE fact_ora_sales_order_line_backorder f_so
SET f_so.dim_ora_commercialviewid = ds.dim_commercialviewid
FROM fact_ora_sales_order_line_backorder f_so, dim_commercialview ds, EMDTempoCC4.dim_customer dc, dim_ora_bwproducthierarchy b
WHERE ds.soldtocountry = substring(dc.country,1,2)
  AND ds.upperprodhier = b.business
  AND f_so.dim_ora_bwproducthierarchyid = b.dim_bwproducthierarchyid
  AND f_so.dim_ora_customer_ship_to_locationid = dc.dim_customerid
  AND f_so.dim_ora_commercialviewid <> ds.dim_commercialviewid;

/*Disabled as requested Roxana D 2017-10-27
UPDATE fact_ora_sales_order_line_backorder f
SET f.dim_ora_clusterid = dc.dim_ora_clusterid
FROM fact_ora_sales_order_line_backorder f, dim_ora_mdg_product mdg, dim_ora_bwproducthierarchy ph, dim_ora_cluster dc
WHERE f.dim_ora_mdg_productid = mdg.dim_mdg_partid
    AND f.dim_ora_bwproducthierarchyid = ph.dim_bwproducthierarchyid
    AND ph.businesssector = 'BS-02'
    AND mdg.primary_production_location = dc.primary_manufacturing_site
    AND f.dim_ora_clusterid <> dc.dim_ora_clusterid
    */



  update fact_ora_sales_order_line_backorder f_so
  set dim_mercklsconforreqdateid = mlsrd.dim_dateid
  from fact_ora_sales_order_line_backorder f_so,dim_date_factory_calendar mlsrd, dim_ora_invproduct prt,dim_ora_bwproducthierarchy bw
  where f_so.dim_ora_bwproducthierarchyid = bw.dim_bwproducthierarchyid
  and f_so.dim_ora_inv_productid = prt.dim_ora_invproductid
  and CASE WHEN(CASE WHEN prt.item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END ='MTO') then (case when bw.businessdesc like '%Research Solutions%'
  then f_so.dim_ora_date_customer_requestedid else f_so.dim_ora_otd_promise_dateid end)
  ELSE dim_ora_date_customer_requestedid END = mlsrd.dim_dateid
  and dim_mercklsconforreqdateid <> mlsrd.dim_dateid;

  UPDATE fact_ora_sales_order_line_backorder f
SET f.dim_ora_customer_ship_to_locationid = dc.dim_ora_customer_ship_to_locationid
FROM fact_ora_sales_order_line_backorder f, fact_ora_sales_order_line dc
WHERE f.fact_ora_sales_order_lineid = dc.fact_ora_sales_order_lineid
AND f.dim_ora_customer_ship_to_locationid<> dc.dim_ora_customer_ship_to_locationid
 and f.dim_ora_customer_ship_to_locationid = 1;



update EMDORACLE4AE.fact_ora_sales_order_line_backorder f
set f.DD_PRIMARYPRODUCTIONLOCATION = mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
FROM  EMDORACLE4AE.fact_ora_sales_order_line_backorder f
inner join dim_ora_mdg_product  mdg on f.dim_ora_mdg_productid=mdg.dim_mdg_partid
where f.DD_PRIMARYPRODUCTIONLOCATION <> mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
and TO_DATE(snapshot_date) = TO_DATE(CURRENT_DATE) - 1;