UPDATE    dim_MRPDiscontinuationIndicator mrpi
   SET mrpi.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			mrpi.dw_update_date = current_timestamp
       FROM
          DD07T t, dim_MRPDiscontinuationIndicator mrpi
 WHERE mrpi.RowIsCurrent = 1
        AND   t.DD07T_DOMNAME = 'KZAUS'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND mrpi."indicator" = t.DD07T_DOMVALUE
;

INSERT INTO dim_MRPDiscontinuationIndicator(dim_MRPDiscontinuationIndicatorId, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MRPDiscontinuationIndicator
               WHERE dim_MRPDiscontinuationIndicatorId = 1);

delete from number_fountain m where m.table_name = 'dim_MRPDiscontinuationIndicator';
   
insert into number_fountain
select 	'dim_MRPDiscontinuationIndicator',
	ifnull(max(d.Dim_mrpDiscontinuationIndicatorID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MRPDiscontinuationIndicator d
where d.Dim_mrpDiscontinuationIndicatorID <> 1; 

INSERT INTO dim_MRPDiscontinuationIndicator(Dim_mrpDiscontinuationIndicatorID,
								Description,
                                "indicator",
                                RowStartDate,
                                RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MRPDiscontinuationIndicator') 
          + row_number() over(order by '') ,
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_MRPDiscontinuationIndicator
                    WHERE "indicator" = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'KZAUS')
   ORDER BY 2
;
