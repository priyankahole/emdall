UPDATE    Dim_Procurement p
   SET p.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			p.dw_update_date = current_timestamp
       FROM Dim_Procurement p,
          DD07T t
 WHERE p.RowIsCurrent = 1
       AND    t.DD07T_DOMNAME = 'BESKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND p.Procurement = t.DD07T_DOMVALUE 
;

INSERT INTO Dim_Procurement(Dim_ProcurementId, RowIsCurrent,description,procurement,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM Dim_Procurement
               WHERE Dim_ProcurementId = 1);

delete from number_fountain m where m.table_name = 'Dim_Procurement';
   
insert into number_fountain
select 	'Dim_Procurement',
	ifnull(max(d.Dim_ProcurementID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_Procurement d
where d.Dim_ProcurementID <> 1; 

INSERT INTO Dim_Procurement(Dim_ProcurementID,
                                Description,
                                Procurement,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_Procurement')
         + row_number() over(order by ''),
			  ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'BESKZ' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM Dim_Procurement
                    WHERE Procurement = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'BESKZ')
   ORDER BY 2 	
;

