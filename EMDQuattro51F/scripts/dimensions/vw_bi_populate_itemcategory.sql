UPDATE    dim_itemcategory ic
SET ic.Description = t.T163Y_PTEXT,
       ic.Category = ifnull(T163Y_EPSTP, 'Not Set'),
			ic.dw_update_date = current_timestamp
       FROM
          T163Y t, dim_itemcategory ic 
 WHERE ic.RowIsCurrent = 1
 AND t.T163Y_PSTYP = ic.categorycode;
 
INSERT INTO dim_itemcategory(dim_itemcategoryid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_itemcategory
               WHERE dim_itemcategoryid = 1);

delete from number_fountain m where m.table_name = 'dim_itemcategory';
   
insert into number_fountain
select 	'dim_itemcategory',
	ifnull(max(d.Dim_ItemCategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_itemcategory d
where d.Dim_ItemCategoryid <> 1; 
			   
INSERT INTO dim_itemcategory(Dim_ItemCategoryid,
                             Description,
                             CategoryCode,
                             Category,
                             RowStartDate,
                             RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_itemcategory')
          + row_number() over(order by ''),
                 T163Y_PTEXT,
          T163Y_PSTYP,
          ifnull(T163Y_EPSTP, 'Not Set'),
          current_timestamp,
          1
     FROM T163Y
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_itemcategory
                       WHERE T163Y_PSTYP = categorycode);
