UPDATE dim_MaterialPriceGroup1 a
   SET a.Description = ifnull(t.TVM1T_BEZEI, 'Not Set'),
			a.dw_update_date = current_timestamp
	 FROM dim_MaterialPriceGroup1 a, TVM1T t
 WHERE a.MaterialPriceGroup1 = TVM1T_MVGR1 
 AND a.RowIsCurrent = 1;
 
INSERT INTO dim_MaterialPriceGroup1(dim_MaterialPriceGroup1id, MaterialPriceGroup1,Description, RowIsCurrent, RowStartDate)
SELECT 1, 'Not Set', 'Not Set', 1, current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup1
               WHERE dim_MaterialPriceGroup1id = 1);

delete from number_fountain m where m.table_name = 'dim_MaterialPriceGroup1';
   
insert into number_fountain
select 	'dim_MaterialPriceGroup1',
	ifnull(max(d.dim_materialpricegroup1Id), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MaterialPriceGroup1 d
where d.dim_materialpricegroup1Id <> 1; 
			   
INSERT INTO dim_MaterialPriceGroup1(dim_materialpricegroup1Id,
                                    MaterialPriceGroup1,
                                    Description,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MaterialPriceGroup1')
          + row_number() over(order by '') ,
                         t.TVM1T_MVGR1,
          ifnull(TVM1T_BEZEI, 'Not Set'),
          current_timestamp,
          1
     FROM TVM1T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup1 a
               WHERE a.MaterialPriceGroup1 = TVM1T_MVGR1);
