/* Start Update dim_vendor Interface */
INSERT INTO dim_vendor
(dim_vendorid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_vendor where dim_vendorid = 1);

UPDATE dim_vendor v
   SET v.VendorName = ifnull(l.NAME1, 'Not Set'),
       v.City = ifnull(l.LFA1_ORT01, 'Not Set'),
       v."state" = ifnull(l.LFA1_REGIO, 'Not Set'),
       v.PostalCode = ifnull(l.LFA1_PSTLZ, 'Not Set'),
       v.Country = ifnull(l.LAND1, 'Not Set'),
       v.IndustryCode = ifnull(l.LFA1_BRSCH, 'Not Set'),
       v.AccountGroup = ifnull(vg.T077Y_TXT30, 'Not Set'),
       v.DeleteIndicator = ifnull(l.LFA1_LOEVM, 'Not Set'),
       v.PurchaseBlock = ifnull(l.LFA1_SPERM, 'Not Set'),
       v.PostingBlock = ifnull(l.LFA1_SPERR, 'Not Set'),
       v.OneTimeAccount = ifnull(l.LFA1_XCPDK, 'Not Set'),
       v.QualitySystem = ifnull(l.LFA1_QSSYS, 'Not Set'),
       v.CompanyCode = ifnull(l.LFA1_VBUND, 'Not Set'),
       v.VendorGroup = ifnull(l.LFA1_KTOKK, 'Not Set'),
       v.ACHFlag = 'N',
	   v.CountryName = ifnull(t1.t005t_landx, 'Not Set'),
       v.VendorMfgCode = IFNULL(l.LFA1_EMNFR,'Not Set'),
	   v.dw_update_date = current_timestamp
FROM dim_vendor v
		inner join LFA1 l on v.vendornumber = l.LIFNR
		INNER JOIN t077y vg ON l.LFA1_KTOKK = vg.T077Y_KTOKK
		INNER JOIN T005T t1 ON l.LAND1 = T005T_LAND1;

merge into dim_vendor dim
using (select v.dim_vendorid, 
			  ifnull(max(t1.T5UNT_NTEXT), 'Not Set') as Industry, 
			  ifnull(max(lb.LFB1_ALTKN), 'Not Set') as PreviousVendorNumber, 
			  ifnull(max(t2.T005U_BEZEI),'Not Set') as RegionDescription
		from dim_vendor v
				inner join LFA1 l on v.vendornumber = l.LIFNR
				left join T5UNT t1 on t1.T5UNT_NAICS = l.LFA1_BRSCH 
				left join LFB1 lb on lb.LFB1_LIFNR = l.LIFNR AND lb.LFB1_ALTKN is not null
				left join T005U t2 on t2.T005U_LAND1 = l.LAND1 AND t2.T005U_BLAND = l.LFA1_REGIO
	    group by v.dim_vendorid) src on dim.dim_vendorid = src.dim_vendorid
when matched then update set dim.Industry = src.Industry,
							 dim.PreviousVendorNumber = src.PreviousVendorNumber,
							 dim.RegionDescription = src.RegionDescription
where dim.Industry <> src.Industry or 
	  dim.PreviousVendorNumber <> src.PreviousVendorNumber or 
	  dim.RegionDescription <> src.RegionDescription;

	   
delete from number_fountain m where m.table_name = 'dim_vendor';

insert into number_fountain
select 	'dim_vendor',
	ifnull(max(d.Dim_Vendorid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_vendor d
where d.Dim_Vendorid <> 1;	   
	   
drop table if exists tmp_vnd_insrt_t1;
create table tmp_vnd_insrt_t1 as
select    l.LIFNR VendorNumber,
          ifnull(NAME1, 'Not Set') VendorName,
          ifnull(LFA1_ORT01, 'Not Set') City,
          ifnull(LFA1_REGIO, 'Not Set') "state",
          ifnull(LFA1_PSTLZ, 'Not Set') PostalCode,
          ifnull(l.LAND1, 'Not Set') Country,
          ifnull(LFA1_BRSCH, 'Not Set') IndustryCode,
		  convert(VARCHAR(200), 'Not Set') as Industry, 				-- ifnull((SELECT T5UNT_NTEXT FROM T5UNT WHERE T5UNT_NAICS = LFA1_BRSCH ),'Not Set') Industry,
          ifnull(vg.T077Y_TXT30, 'Not Set') AccountGroup,
          ifnull(LFA1_LOEVM, 'Not Set') DeleteIndicator,
          ifnull(LFA1_SPERM, 'Not Set') PurchaseBlock,
          ifnull(LFA1_SPERR, 'Not Set') PostingBlock,
          ifnull(LFA1_XCPDK, 'Not Set') OneTimeAccount,
          ifnull(LFA1_QSSYS, 'Not Set') QualitySystem,
          ifnull(LFA1_VBUND, 'Not Set') CompanyCode,
          ifnull(LFA1_KTOKK, 'Not Set') VendorGroup,
          convert(VARCHAR(200), 'Not Set') as ACHFlag, 					-- ifnull((SELECT 'Y' FROM LFA1 l2 WHERE EXISTS (SELECT 1 FROM LFBK WHERE LFBK_LIFNR = l2.LIFNR AND LFBK_BANKL IS NOT NULL AND LFBK_BANKN IS NOT NULL AND LFBK_BANKS IS NOT NULL) ), 'N') ACHFlag,
		  convert(VARCHAR(200), 'Not Set') as PreviousVendorNumber, 	-- ifnull((SELECT lb.LFB1_ALTKN FROM LFB1 lb WHERE lb.LFB1_LIFNR = l.LIFNR AND lb.LFB1_ALTKN is not null), 'Not Set') PreviousVendorNumber,
          current_timestamp RowStartDate,
          1 RowIsCurrent,
		  ifnull(t005t_landx, 'Not Set') CountryName,
		  convert(VARCHAR(200), 'Not Set') as RegionDescription, 		-- ifnull((SELECT T005U_BEZEI FROM T005U WHERE T005U_LAND1 = l.LAND1 AND T005U_BLAND = l.LFA1_REGIO),'Not Set') RegionDescription,
          ifnull(LFA1_EMNFR,'Not Set') vendormfgcode
		  ,row_number()over(order by '') rid
		FROM LFA1 l 
				INNER JOIN t077y vg ON l.LFA1_KTOKK = vg.T077Y_KTOKK
				INNER JOIN T005T ON l.LAND1 = T005T_LAND1
		WHERE NOT EXISTS (SELECT 1
						  FROM dim_vendor
						  WHERE vendornumber = l.LIFNR);

/* Industry */
update tmp_vnd_insrt_t1 dst
set dst.Industry = ifnull(t0.T5UNT_NTEXT, 'Not Set')
from tmp_vnd_insrt_t1 dst
		left join T5UNT t0 on t0.T5UNT_NAICS = dst.IndustryCode
where dst.Industry <> ifnull(t0.T5UNT_NTEXT, 'Not Set');

/* ACHFlag */
merge into tmp_vnd_insrt_t1 dst
using (select distinct t0.rid, ifnull(ds.ACHFlag, 'N') as ACHFlag
	   from tmp_vnd_insrt_t1 t0
				left join (select 'Y' as ACHFlag, l2.LIFNR
				           from LFA1 l2
									inner join LFBK t1 on t1.LFBK_LIFNR = l2.LIFNR
						   where t1.LFBK_BANKL IS NOT NULL AND 
								 t1.LFBK_BANKN IS NOT NULL AND 
								 t1.LFBK_BANKS IS NOT NULL) ds on t0.VendorNumber = ds.LIFNR
	  ) src on dst.rid = src.rid
when matched then update set dst.ACHFlag = src.ACHFlag
where dst.ACHFlag <> src.ACHFlag;

/* PreviousVendorNumber */
merge into tmp_vnd_insrt_t1 dst
using (select t.rid, ifnull(max(t0.LFB1_ALTKN), 'Not Set') PreviousVendorNumber
	   from tmp_vnd_insrt_t1 t
				left join LFB1 t0 on t0.LFB1_LIFNR = t.VendorNumber AND t0.LFB1_ALTKN is not null
	   group by t.rid
	  ) src on dst.rid = src.rid
when matched then update set dst.PreviousVendorNumber = src.PreviousVendorNumber
where dst.PreviousVendorNumber <> src.PreviousVendorNumber;


/* RegionDescription */
merge into tmp_vnd_insrt_t1 dst
using (select t.rid, ifnull(t0.T005U_BEZEI, 'Not Set') RegionDescription
	   from tmp_vnd_insrt_t1 t
				left join T005U t0 on t0.T005U_LAND1 = t.Country AND T005U_BLAND = t."state"
	  ) src on dst.rid = src.rid
when matched then update set dst.RegionDescription = src.RegionDescription
where dst.RegionDescription <> src.RegionDescription;


INSERT INTO dim_vendor(Dim_Vendorid,
                       VendorNumber,
                       VendorName,
                       City,
                       "state",
                       PostalCode,
                       Country,
                       IndustryCode,
                       Industry,
                       AccountGroup,
                       DeleteIndicator,
                       PurchaseBlock,
                       PostingBlock,
                       OneTimeAccount,
                       QualitySystem,
                       CompanyCode,
                       VendorGroup,
					   ACHFlag,
					   PreviousVendorNumber,
                       RowStartDate,
                       RowIsCurrent,
					   CountryName,
					   RegionDescription,
                       vendormfgcode)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_vendor') + row_number() over(order by '') Dim_Vendorid,
			   t.*
		from (select DISTINCT  VendorNumber,
							   VendorName,
							   City,
							   "state",
							   PostalCode,
							   Country,
							   IndustryCode,
							   Industry,
							   AccountGroup,
							   DeleteIndicator,
							   PurchaseBlock,
							   PostingBlock,
							   OneTimeAccount,
							   QualitySystem,
							   CompanyCode,
							   VendorGroup,
							   ACHFlag,
							   PreviousVendorNumber,
							   RowStartDate,
							   RowIsCurrent,
							   CountryName,
							   RegionDescription,
							   vendormfgcode
			  from tmp_vnd_insrt_t1) t
		WHERE NOT EXISTS (SELECT 1
						  FROM dim_vendor
						  WHERE vendornumber = t.vendornumber);

drop table if exists tmp_vnd_insrt_t1;
						  
merge into dim_vendor dst
using (select distinct t0.dim_vendorid, ifnull(ds.ACHFlag, 'N') as ACHFlag
	   from dim_vendor t0
				left join (select 'Y' as ACHFlag, l2.LIFNR
				           from LFA1 l2
									inner join LFBK t1 on t1.LFBK_LIFNR = l2.LIFNR
						   where t1.LFBK_BANKL IS NOT NULL AND 
								 t1.LFBK_BANKN IS NOT NULL AND 
								 t1.LFBK_BANKS IS NOT NULL) ds on t0.VendorNumber = ds.LIFNR
	  ) src on dst.dim_vendorid = src.dim_vendorid
when matched then update set dst.ACHFlag = src.ACHFlag,
	                         dst.dw_update_date = current_timestamp
where dst.ACHFlag <> src.ACHFlag;

UPDATE dim_vendor v
   SET v.departurezone = ifnull(tt.TZONT_VTEXT,'Not Set'),
  	   v.dw_update_date = current_timestamp
FROM TZONE_TZONT tt, LFA1 l, dim_vendor v
   WHERE tt.TZONE_LAND1 = l.LAND1
   AND tt.TZONE_ZONE1=l.LFA1_LZONE
   AND v.vendornumber = l.LIFNR
   AND v.departurezone <> ifnull(tt.TZONT_VTEXT, 'Not Set');
   
update dim_vendor
set VendorNumber2 = TRIM(LEADING '0' FROM VendorNumber)
where VendorNumber2 <> TRIM(LEADING '0' FROM VendorNumber);
 
