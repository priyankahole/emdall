

		/* Table to monitor number of records in FACT, based on fact_purchase_requisitionID */
			delete from NUMBER_FOUNTAIN where table_name = 'fact_purchase_requisition';
			
			INSERT INTO NUMBER_FOUNTAIN
			   SELECT 'fact_purchase_requisition', ifnull(max(fact_purchase_requisitionid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
				 FROM fact_purchase_requisition;
		
		/* **********  START MAIN UPDATE PROCESS   ********** */
		
		/* 1. Dim_DocumentTypeid -> EBAN_BSART, EBAN_BSTYP */
		UPDATE fact_purchase_requisition f
		SET F.Dim_DocumentTypeid = dtp.Dim_DocumentTypeid
		FROM EBAN c, Dim_DocumentType dtp, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      			AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			AND dtp.Type     = EBAN_BSART
			AND dtp.Category = EBAN_BSTYP
			AND F.Dim_DocumentTypeid <> dtp.Dim_DocumentTypeid;

		/* 2. dd_PurchaseReqDocCat -> EBAN_BSTYP */
		UPDATE fact_purchase_requisition f
		SET dd_PurchaseReqDocCat = ifnull(C.EBAN_BSTYP,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_PurchaseReqDocCat <> ifnull(C.EBAN_BSTYP,'Not Set');

		/* 3. dd_deletionindicator -> EBAN_LOEKZ */
		UPDATE fact_purchase_requisition f
		SET dd_deletionindicator = ifnull(C.EBAN_LOEKZ,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_deletionindicator <> ifnull(C.EBAN_LOEKZ,'Not Set');			  			  

		/* 4. dim_prstatusid -> EBAN_STATU */
		UPDATE fact_purchase_requisition f
		SET dd_ProcessingStatus = ifnull(C.EBAN_STATU,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_ProcessingStatus <> ifnull(C.EBAN_STATU,'Not Set');	
		
		/* 5. dd_CreationIndicator -> EBAN_ESTKZ */
		UPDATE fact_purchase_requisition f
		SET dd_CreationIndicator = ifnull(C.EBAN_ESTKZ,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_CreationIndicator <> ifnull(C.EBAN_ESTKZ,'Not Set');	

		/* 6. Dim_PurchaseGroupid -> EBAN_EKGRP */
		/*
		UPDATE fact_purchase_requisition f
		SET F.Dim_PurchaseGroupid = its.Dim_PurchaseGroupid
		FROM EBAN c, Dim_PurchaseGroup its, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND its.PurchaseGroup = C.EBAN_EKGRP
		  AND F.Dim_PurchaseGroupid <> its.Dim_PurchaseGroupid*/
		  
MERGE INTO fact_purchase_requisition f
USING (SELECT C.EBAN_BANFN,C.EBAN_BNFPO,its.Dim_PurchaseGroupid FROM EBAN c INNER JOIN Dim_PurchaseGroup its ON its.PurchaseGroup = C.EBAN_EKGRP) x
	ON f.dd_purchasereqno = x.EBAN_BANFN AND f.dd_purchasereqitemno = x.EBAN_BNFPO
WHEN MATCHED THEN UPDATE SET f.Dim_PurchaseGroupid = x.Dim_PurchaseGroupid
WHERE f.Dim_PurchaseGroupid <> x.Dim_PurchaseGroupid;
		
		/* 7. dd_ReleaseIndicator -> EBAN_FRGKZ */
		UPDATE fact_purchase_requisition f
		SET dd_ReleaseIndicator = ifnull(C.EBAN_FRGKZ,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_ReleaseIndicator <> ifnull(C.EBAN_FRGKZ,'Not Set');	

    	/* 8. dd_CreatedObjectByPerson -> EBAN_ERNAM */
		UPDATE fact_purchase_requisition f
		SET dd_CreatedObjectByPerson = ifnull(C.EBAN_ERNAM,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_CreatedObjectByPerson <> ifnull(C.EBAN_ERNAM,'Not Set');	
  
		/* 9. dim_datechangeonid -> EBAN_ERDAT */
		UPDATE fact_purchase_requisition f
		SET F.dim_datechangeonid = pd.dim_dateid
		FROM EBAN c, dim_date pd, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND pd.datevalue   = C.EBAN_ERDAT
		  AND pd.companycode = 'Not Set'
		  AND F.dim_datechangeonid <> pd.dim_dateid;	

		/* 10. DIM_MATERIALID -> EBAN_MATNR, EBAN_WERKS  */
		UPDATE fact_purchase_requisition f
		SET F.DIM_MATERIALID = pt.dim_partid
		FROM EBAN c, dim_part pt, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND pt.PartNumber = C.EBAN_MATNR
		  AND pt.Plant      = C.EBAN_WERKS
		  AND F.DIM_MATERIALID <> pt.dim_partid;	

		/* 11. Dim_Plantid -> EBAN_WERKS  */
		UPDATE fact_purchase_requisition f
		SET F.Dim_Plantid = pl.dim_plantid
		FROM EBAN c, dim_plant pl, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND pl.PlantCode = EBAN_WERKS
		  AND F.Dim_Plantid <> pl.dim_plantid;	

		/* 12. dim_storagelocationid -> EBAN_LGORT, EBAN_WERKS  */
		UPDATE fact_purchase_requisition f
		SET F.dim_storagelocationid = sl.Dim_StorageLocationid
		FROM EBAN c, dim_StorageLocation sl, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND sl.LocationCode = C.EBAN_LGORT 
		  AND sl.Plant        = C.EBAN_WERKS
		  AND F.dim_storagelocationid <> sl.Dim_StorageLocationid;
		/* 13. Dim_MaterialGroupid -> EBAN_MATKL  */
		UPDATE fact_purchase_requisition f
		SET F.Dim_MaterialGroupid = mg.Dim_MaterialGroupid
		FROM EBAN c, Dim_MaterialGroup mg, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND mg.MaterialGroupCode = C.EBAN_MATKL
		  AND F.Dim_MaterialGroupid <> mg.Dim_MaterialGroupid;			  

    	/* 14. dd_RequestTrackNo -> EBAN_BEDNR */
		UPDATE fact_purchase_requisition f
		SET dd_RequestTrackNo = ifnull(C.EBAN_BEDNR,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_RequestTrackNo <> ifnull(C.EBAN_BEDNR,'Not Set');				  
		
		/* 15. DIM_SupplPlantStockTranspOrdId -> EBAN_RESWK  */
		UPDATE fact_purchase_requisition f
		SET F.DIM_SupplPlantStockTranspOrdId = dps.dim_plantid
		FROM EBAN c, Dim_Plant dps, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND dps.PlantCode = C.EBAN_RESWK
		  AND F.DIM_SupplPlantStockTranspOrdId <> dps.dim_plantid;	
		
    	/* 16. ct_PurchaseReq -> EBAN_MENGE */
		UPDATE fact_purchase_requisition f
		SET ct_PurchaseReq = ifnull(C.EBAN_MENGE, 0)
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND ct_PurchaseReq <> ifnull(C.EBAN_MENGE, 0);		

        /* 17. DIM_SupplPlantStockTranspOrdId -> EBAN_MEINS  */
		UPDATE fact_purchase_requisition f
		SET F.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid
		FROM EBAN c, Dim_UnitOfMeasure uom, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND uom.UOM = C.EBAN_MEINS
		  AND F.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;

		/* 18. dim_dateRequsiotionid -> EBAN_BADAT */
		UPDATE fact_purchase_requisition f
		SET F.dim_dateRequsiotionid = pd.dim_dateid
		FROM EBAN c, dim_date pd, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND pd.datevalue   = C.EBAN_BADAT
		  AND pd.companycode = 'Not Set'
		  AND F.dim_dateRequsiotionid <> pd.dim_dateid;

		/* 19. dim_datePurchaseReqReleaseid -> EBAN_FRGDT */
		UPDATE fact_purchase_requisition f
		SET F.dim_datePurchaseReqReleaseid = pd.dim_dateid
		FROM EBAN c, dim_date pd, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND pd.datevalue   = C.EBAN_FRGDT
		  AND pd.companycode = 'Not Set'
		  AND F.dim_datePurchaseReqReleaseid <> pd.dim_dateid;

		/* 20. dim_dateItemDeliveryid -> EBAN_LFDAT */
		UPDATE fact_purchase_requisition f
		SET F.dim_dateItemDeliveryid = pd.dim_dateid
		FROM EBAN c, dim_date pd, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND pd.datevalue   = C.EBAN_LFDAT
		  AND pd.companycode = 'Not Set'
		  AND F.dim_dateItemDeliveryid <> pd.dim_dateid;

    	/* 21. amt_PurchaseReqPrice -> EBAN_PREIS */
		UPDATE fact_purchase_requisition f
		SET amt_PurchaseReqPrice = ifnull(C.EBAN_PREIS, 0)
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND amt_PurchaseReqPrice <> ifnull(C.EBAN_PREIS, 0);	

    	/* 22. ct_PriceUnit -> EBAN_PEINH */
		UPDATE fact_purchase_requisition f
		SET ct_PriceUnit = ifnull(C.EBAN_PEINH, 0)
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND ct_PriceUnit <> ifnull(C.EBAN_PEINH, 0);

        /* 23. Dim_ItemCategoryid -> EBAN_PSTYP  */
		UPDATE fact_purchase_requisition f
		SET F.Dim_ItemCategoryid = ic.Dim_ItemCategoryid
		FROM EBAN c, Dim_ItemCategory ic, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND ic.CategoryCode = C.EBAN_PSTYP
		  AND F.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;

	    /* 24. dim_accountcategoryid -> EBAN_KNTTP  */
		UPDATE fact_purchase_requisition f
		SET F.dim_accountcategoryid = ac.Dim_AccountCategoryid
		FROM EBAN c, Dim_AccountCategory ac, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND ac.Category = C.EBAN_KNTTP
		  AND F.dim_accountcategoryid <> ac.Dim_AccountCategoryid;

	    /* 25. dim_consumptiontypeid -> EBAN_KZVBR  */
		UPDATE fact_purchase_requisition f
		SET F.dim_consumptiontypeid = ct.Dim_ConsumptionTypeid
		FROM EBAN c, Dim_ConsumptionType ct, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND ct.ConsumptionCode = C.EBAN_KZVBR
		  AND F.dim_consumptiontypeid <> ct.Dim_ConsumptionTypeid;

	    /* 26. Dim_Vendorid -> EBAN_LIFNR  */
		UPDATE fact_purchase_requisition f
		SET F.Dim_Vendorid = v.Dim_Vendorid
		FROM EBAN c, dim_vendor v, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND v.VendorNumber = C.EBAN_LIFNR
		  AND F.Dim_Vendorid <> v.Dim_Vendorid;
		  
		/* 27. Dim_PurchaseOrgid -> EBAN_EKORG  */
		UPDATE fact_purchase_requisition f
		SET F.Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
		FROM EBAN c, Dim_PurchaseOrg po, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND po.PurchaseOrgCode = C.EBAN_EKORG
		  AND F.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;	
		
		/* 28. Dim_FixedVendorid -> EBAN_FLIEF  */
		UPDATE fact_purchase_requisition f
		SET F.Dim_FixedVendorid = v.Dim_Vendorid
		FROM EBAN c, dim_vendor v, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND v.VendorNumber = EBAN_FLIEF
		  AND F.Dim_FixedVendorid <> v.Dim_Vendorid;	
			  
    	/* 29. dd_PurchaseInforRecord -> EBAN_INFNR */
		UPDATE fact_purchase_requisition f
		SET dd_PurchaseInforRecord = ifnull(C.EBAN_INFNR,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_PurchaseInforRecord <> ifnull(C.EBAN_INFNR,'Not Set');
    	
		/* 30. dd_PurchaseOrderNo -> EBAN_EBELN */
		UPDATE fact_purchase_requisition f
		SET dd_PurchaseOrderNo = ifnull(C.EBAN_EBELN,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_PurchaseOrderNo <> ifnull(C.EBAN_EBELN,'Not Set');
			  
		/* 31. dd_PurchaseOrderItemNo -> EBAN_EBELP */
		UPDATE fact_purchase_requisition f
		SET dd_PurchaseOrderItemNo = ifnull(C.EBAN_EBELP,0)
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_PurchaseOrderItemNo <> ifnull(C.EBAN_EBELP,0);

		/* 32. dim_datePurchaseOrderId -> EBAN_BEDAT */
		UPDATE fact_purchase_requisition f
		SET F.dim_datePurchaseOrderId = pd.dim_dateid
		FROM EBAN c, dim_date pd, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND pd.datevalue   = C.EBAN_BEDAT
		  AND pd.companycode = 'Not Set'
		  AND F.dim_datePurchaseOrderId <> pd.dim_dateid;

		/* 33. ct_OrderedAgainstPurchase -> EBAN_BSMNG */
		UPDATE fact_purchase_requisition f
		SET ct_OrderedAgainstPurchase = ifnull(C.EBAN_BSMNG,0)
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.ct_OrderedAgainstPurchase <> ifnull(C.EBAN_BSMNG,0);
			  
		/* 34. dd_PurchaseReqClosed -> EBAN_EBAKZ */
		UPDATE fact_purchase_requisition f
		SET dd_PurchaseReqClosed = ifnull(C.EBAN_EBAKZ,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_PurchaseReqClosed <> ifnull(C.EBAN_EBAKZ,'Not Set');

		/* 35. Dim_Currencyid -> EBAN_WAERS  */
		UPDATE fact_purchase_requisition f
		SET F.Dim_Currencyid = dcr.Dim_Currencyid
		FROM EBAN c, Dim_Currency dcr, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND dcr.CurrencyCode = C.EBAN_WAERS
		  AND F.Dim_Currencyid <> dcr.Dim_Currencyid;			  
	  
		/* 36. dd_ManufacturePartNo -> EBAN_MFRPN */
		UPDATE fact_purchase_requisition f
		SET dd_ManufacturePartNo = ifnull(C.EBAN_MFRPN,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_ManufacturePartNo <> ifnull(C.EBAN_MFRPN,'Not Set');
			  
		/* 37. dd_PlannedDeliveryDays -> EBAN_PLIFZ */
		UPDATE fact_purchase_requisition f
		SET dd_PlannedDeliveryDays = ifnull(C.EBAN_PLIFZ,0)
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_PlannedDeliveryDays <> ifnull(C.EBAN_PLIFZ,0);

		/* 38. dd_PurchaseReqBlocked -> EBAN_BLCKD */
		UPDATE fact_purchase_requisition f
		SET dd_PurchaseReqBlocked = ifnull(C.EBAN_BLCKD,'Not Set')
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND F.dd_PurchaseReqBlocked <> ifnull(C.EBAN_BLCKD,'Not Set');
		
		/* 39. amt_ExchangeRate -> JUST FOR FACT */
		UPDATE fact_purchase_requisition f
		SET amt_ExchangeRate = 1
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
			  AND amt_ExchangeRate is null;			  

		/* 40. amt_ExchangeRate_GBL -> JUST FOR FACT */
		UPDATE fact_purchase_requisition f
		SET amt_ExchangeRate_GBL = 1
		FROM EBAN c, fact_purchase_requisition f
		WHERE     C.EBAN_BANFN = f.dd_purchasereqno
      		  AND C.EBAN_BNFPO = f.dd_purchasereqitemno
		  AND amt_ExchangeRate_GBL is null;	
		
		/* **********  FINISH MAIN UPDATE PROCESS   ********** */
		
		/* **********  START GET NEW ROWS TO INSERT   ********** */	
		
		/* CLOSE TRANSACTIONS ON FACT */
		
		DROP TABLE IF EXISTS tmp_insert_fct_prchs_rqstn;
		DROP TABLE IF EXISTS tmp_delete_fct_prchs_rqstn;
		
		/* GET ALL NEW DATA based on fact grain */
		CREATE TABLE tmp_insert_fct_prchs_rqstn AS
		SELECT EBAN_BANFN AS dd_purchasereqno,
			   EBAN_BNFPO AS dd_purchasereqitemno
		FROM EBAN;
		
		CREATE TABLE tmp_delete_fct_prchs_rqstn AS
	    SELECT *
		FROM tmp_insert_fct_prchs_rqstn
		WHERE 1=2;
		
		/* INSERT EXISTING DATA FROM FACT based on fact grain */
		INSERT INTO tmp_delete_fct_prchs_rqstn
	    SELECT DISTINCT dd_purchasereqno, dd_purchasereqitemno
		FROM fact_purchase_requisition;

		/* REMOVE EXITING FACT GRAIN KEY from new data data stage arrival*/
		MERGE INTO tmp_insert_fct_prchs_rqstn fact
		USING tmp_delete_fct_prchs_rqstn src ON fact.dd_purchasereqno = src.dd_purchasereqno
											AND fact.dd_purchasereqitemno = src.dd_purchasereqitemno
		WHEN MATCHED THEN DELETE;
			
	
		
		/* **********  FINISH GET NEW ROWS TO INSERT   ********** */	

	
	
	DROP TABLE IF EXISTS tmp_fpr_t001t;
	CREATE TABLE tmp_fpr_t001t as 	
	select ( SELECT max_id 
		FROM NUMBER_FOUNTAIN
		WHERE table_name = 'fact_purchase_requisition'
		) + row_number() over(order by '') 	AS fact_purchase_requisitionid,
		EBAN_BANFN 					AS dd_purchasereqno,
		EBAN_BNFPO 					AS dd_purchasereqitemno,
		CONVERT (bigint, 1) AS Dim_DocumentTypeid,  /* 1. */	/*ifnull((SELECT Dim_DocumentTypeid FROM Dim_DocumentType dtp WHERE     dtp.Type     = EBAN_BSART AND dtp.Category = EBAN_BSTYP),1)   AS Dim_DocumentTypeid,   */    /* 1. */			  
		ifnull(CONVERT(varchar(7), EBAN_BSTYP),'Not Set')  AS dd_PurchaseReqDocCat,     /* 2. */
		ifnull(CONVERT(varchar(7), EBAN_LOEKZ),'Not Set')  AS dd_deletionindicator,     /* 3. */
		ifnull(CONVERT(varchar(7), EBAN_STATU),'Not Set')  AS dd_ProcessingStatus,      /* 4. */
		ifnull(CONVERT(varchar(7), EBAN_ESTKZ),'Not Set')  AS dd_CreationIndicator,     /* 5. */
		CONVERT (bigint, 1) AS Dim_PurchaseGroupid,      /* 6. */  --ifnull((SELECT Dim_PurchaseGroupid FROM Dim_PurchaseGroup pg WHERE pg.PurchaseGroup = EBAN_EKGRP),1)   AS Dim_PurchaseGroupid,      /* 6. */
		ifnull(CONVERT(varchar(7), EBAN_FRGKZ),'Not Set')  AS dd_ReleaseIndicator,      /* 7. */
		ifnull(CONVERT(varchar(12), EBAN_ERNAM),'Not Set') AS dd_CreatedObjectByPerson, /* 8. */
		CONVERT (bigint, 1) AS dim_datechangeonid, 	   	/* 9. */ --ifnull((SELECT pd.dim_dateid	FROM dim_date pd WHERE     pd.datevalue   = EBAN_ERDAT AND pd.companycode = 'Not Set'),1) AS dim_datechangeonid,
		CONVERT (bigint, 1) AS DIM_MATERIALID, 			/* 10. */ --ifnull((SELECT dim_partid FROM dim_Part pt WHERE     pt.PartNumber = EBAN_MATNR AND pt.Plant      = EBAN_WERKS),1) AS DIM_MATERIALID,	
		CONVERT (bigint, 1) AS Dim_Plantid,            /* 11. */ -- ifnull((SELECT pl.dim_plantid FROM dim_plant pl WHERE pl.PlantCode = EBAN_WERKS),1) AS Dim_Plantid,
		CONVERT (bigint, 1) AS dim_storagelocationid,   	/* 12. */  -- ifnull((SELECT Dim_StorageLocationid FROM dim_StorageLocation sl WHERE     sl.LocationCode = EBAN_LGORT AND sl.Plant        = EBAN_WERKS ),1) AS dim_storagelocationid,
		CONVERT (bigint, 1) AS Dim_MaterialGroupid,   	 /* 13. */ -- ifnull((SELECT Dim_MaterialGroupid	FROM Dim_MaterialGroup mg WHERE mg.MaterialGroupCode = EBAN_MATKL), 1) AS Dim_MaterialGroupid,
		ifnull(CONVERT(varchar(14), EBAN_BEDNR),'Not Set')    AS dd_RequestTrackNo,      /* 14. */
		CONVERT (bigint, 1) AS DIM_SupplPlantStockTranspOrdId, /* 15. */ -- ifnull((SELECT Dim_Plantid FROM Dim_Plant dps WHERE dps.PlantCode = EBAN_RESWK),1) AS DIM_SupplPlantStockTranspOrdId,

		ifnull(EBAN_MENGE,0)  AS ct_PurchaseReq,        	 /* 16. */
		CONVERT (bigint, 1) AS Dim_UnitOfMeasureid,      	/* 17. */ -- ifnull((SELECT Dim_UnitOfMeasureid FROM Dim_UnitOfMeasure uom	WHERE uom.UOM = EBAN_MEINS), 1) AS Dim_UnitOfMeasureid, 
		CONVERT (bigint, 1) AS dim_dateRequsiotionid,        /* 18. */ /* check this line, solved */ --ifnull((SELECT pd.dim_dateid FROM dim_date pd WHERE     pd.datevalue   = EBAN_BADAT AND pd.companycode = 'Not Set'),1) AS dim_dateRequsiotionid,
		CONVERT (bigint, 1) AS dim_datePurchaseReqReleaseid, /* 19. */ /* check this line, solved */ --ifnull((SELECT pd.dim_dateid FROM dim_date pd WHERE     pd.datevalue   = EBAN_FRGDT AND pd.companycode = 'Not Set'),1) AS dim_datePurchaseReqReleaseid,
		CONVERT (bigint, 1) AS dim_dateItemDeliveryid,      /* 20. */ /* check this line, solved */ -- ifnull((SELECT pd.dim_dateid FROM dim_date pd WHERE     pd.datevalue   = EBAN_LFDAT AND pd.companycode = 'Not Set'),1) AS dim_dateItemDeliveryid,
		ifnull(EBAN_PREIS,0) 		 AS amt_PurchaseReqPrice,    /* 21. */
		ifnull(EBAN_PEINH,0)  		 AS ct_PriceUnit,            /* 22. */
		CONVERT (bigint, 1) AS Dim_ItemCategoryid,          /* 23. */ -- ifnull((SELECT Dim_ItemCategoryid FROM Dim_ItemCategory ic WHERE ic.CategoryCode = EBAN_PSTYP),1) 	 AS Dim_ItemCategoryid,
		CONVERT (bigint, 1) AS dim_accountcategoryid,   /* 24. */ -- ifnull((SELECT Dim_AccountCategoryid FROM Dim_AccountCategory ac WHERE ac.Category = EBAN_KNTTP), 1) 		 AS dim_accountcategoryid,		
		CONVERT (bigint, 1) AS dim_consumptiontypeid,     /* 25. */ -- ifnull((SELECT Dim_ConsumptionTypeid FROM Dim_ConsumptionType ct	WHERE ct.ConsumptionCode = EBAN_KZVBR),1)  AS dim_consumptiontypeid,
		CONVERT (bigint, 1) AS Dim_Vendorid,                /* 26. */ -- ifnull((SELECT v.Dim_Vendorid FROM dim_vendor v WHERE v.VendorNumber = EBAN_LIFNR),1) 	 AS Dim_Vendorid,
		CONVERT (bigint, 1) AS Dim_PurchaseOrgid,         /* 27. */ -- ifnull((SELECT Dim_PurchaseOrgid FROM Dim_PurchaseOrg po	WHERE po.PurchaseOrgCode = EBAN_EKORG),1)  AS Dim_PurchaseOrgid, 
		CONVERT (bigint, 1) AS Dim_FixedVendorid,           /* 28. */ -- ifnull((SELECT v.Dim_Vendorid FROM dim_vendor v WHERE v.VendorNumber = EBAN_FLIEF),1) 	 AS Dim_FixedVendorid, 
		ifnull(EBAN_INFNR,'Not Set') 	AS dd_PurchaseInforRecord,  /* 29. */
		ifnull(EBAN_EBELN,'Not Set')	AS dd_PurchaseOrderNo,      /* 30. */
		ifnull(EBAN_EBELP,0) AS dd_PurchaseOrderItemNo,  /* 31. */
		CONVERT (bigint, 1) AS dim_datePurchaseOrderId,     /* 32. */ /* check this line, solved */ -- ifnull((SELECT pd.dim_dateid FROM dim_date pd WHERE     pd.datevalue   = EBAN_BEDAT AND pd.companycode = 'Not Set'),1) AS dim_datePurchaseOrderId,
		ifnull(EBAN_BSMNG,0)                                 AS ct_OrderedAgainstPurchase, /* 33. */
		ifnull(CONVERT(varchar(7), EBAN_EBAKZ),'Not Set')     AS dd_PurchaseReqClosed,      /* 34. */
		CONVERT (bigint, 1) AS Dim_Currencyid,            /* 35. */ -- ifnull((SELECT Dim_Currencyid FROM Dim_Currency dcr WHERE dcr.CurrencyCode = EBAN_WAERS),1) 	 AS Dim_Currencyid,
		ifnull(CONVERT(varchar(45), EBAN_MFRPN),'Not Set')    	AS dd_ManufacturePartNo,      /* 36. */
		ifnull(EBAN_PLIFZ,0) 	 AS dd_PlannedDeliveryDays,    /* 37. */
		ifnull(CONVERT(varchar(7), EBAN_BLCKD),'Not Set')     	AS dd_PurchaseReqBlocked,     /* 38. */
		1 							 AS amt_ExchangeRate,
		1 							 AS amt_ExchangeRate_GBL,
		EBAN_BSART, --Dim_DocumentTypeid, /* 1. */
		EBAN_EKGRP, --Dim_PurchaseGroupid, /* 6. */
		EBAN_ERDAT, --dim_datechangeonid, /* 9. */
		EBAN_MATNR, --DIM_MATERIALID, /* 10. */
		EBAN_WERKS, --Dim_Plantid, /* 11. */
		EBAN_LGORT,--dim_storagelocationid, /* 12. */ 
		EBAN_MATKL, --Dim_MaterialGroupid,  /* 13. */
		EBAN_RESWK, --DIM_SupplPlantStockTranspOrdId,  /* 15. */
		EBAN_MEINS, --Dim_UnitOfMeasureid, /* 17. */
		EBAN_BADAT, --dim_dateRequsiotionid, /* 18. */
		EBAN_FRGDT, --dim_datePurchaseReqReleaseid, /* 19. */
		EBAN_LFDAT, --dim_dateItemDeliveryid,      /* 20. */
		EBAN_PSTYP, --Dim_ItemCategoryid,          /* 23. */
		EBAN_KNTTP, --dim_accountcategoryid,   /* 24. */
		EBAN_KZVBR, --dim_consumptiontypeid,     /* 25. */ 
		EBAN_LIFNR, --Dim_Vendorid,                /* 26. */
		EBAN_EKORG, --Dim_PurchaseOrgid,         /* 27. */
		EBAN_FLIEF, --Dim_FixedVendorid,           /* 28. */
		EBAN_BEDAT, --dim_datePurchaseOrderId,     /* 32. */
		EBAN_WAERS --Dim_Currencyid            /* 35. */
		
	FROM eban F, 
			 tmp_insert_fct_prchs_rqstn i
	WHERE     F.EBAN_BANFN = I.dd_purchasereqno
			  AND F.EBAN_BNFPO = I.dd_purchasereqitemno;
			  		
	/* Dim_DocumentTypeid */  /* 1. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_DocumentTypeid = IFNULL(dtp.Dim_DocumentTypeid, 1)
	FROM tmp_fpr_t001t f 
		LEFT JOIN  Dim_DocumentType dtp ON dtp.Type     = f.EBAN_BSART
						AND dtp.Category = f.dd_PurchaseReqDocCat /*originally point to --EBAN_BSTYP*/
	WHERE f.Dim_DocumentTypeid <> IFNULL(dtp.Dim_DocumentTypeid, 1);	
	
	/*Dim_PurchaseGroupid*/ /* 6. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_PurchaseGroupid = IFNULL (pg.Dim_PurchaseGroupid, 1)
	FROM tmp_fpr_t001t f
		LEFT JOIN Dim_PurchaseGroup pg	ON pg.PurchaseGroup = f.EBAN_EKGRP
	WHERE f.Dim_PurchaseGroupid <> IFNULL (pg.Dim_PurchaseGroupid, 1);
	
	/* dim_datechangeonid */ /* 9. */
	UPDATE tmp_fpr_t001t f
	SET f.dim_datechangeonid = IFNULL (pd.dim_dateid, 1)
	FROM tmp_fpr_t001t f
		LEFT JOIN dim_date pd	ON pd.datevalue    = f.EBAN_ERDAT
					AND pd.companycode = 'Not Set'
	WHERE f.dim_datechangeonid <> IFNULL (pd.dim_dateid, 1);
	
	/*DIM_MATERIALID*/ /* 10. */
	UPDATE tmp_fpr_t001t f
	SET f.DIM_MATERIALID = IFNULL(pt.dim_partid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_Part pt ON pt.PartNumber = f.EBAN_MATNR
			 AND pt.Plant      = f.EBAN_WERKS 
	WHERE f.DIM_MATERIALID <> IFNULL(pt.dim_partid, 1);
	
	/*Dim_Plantid*/	/* 11. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_Plantid = IFNULL(pl.dim_plantid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_plant pl ON pl.PlantCode = f.EBAN_WERKS 
	WHERE f.Dim_Plantid <> IFNULL(pl.dim_plantid, 1);
	
	/*dim_storagelocationid*/ /* 12. */
	UPDATE tmp_fpr_t001t f
	SET f.dim_storagelocationid = IFNULL(sl.Dim_StorageLocationid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_StorageLocation sl ON sl.LocationCode = f.EBAN_LGORT 
			            AND sl.Plant        = f.EBAN_WERKS  
	WHERE f.dim_storagelocationid <> IFNULL(sl.Dim_StorageLocationid, 1);
	
	/*Dim_MaterialGroupid*/    /* 13. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_MaterialGroupid = IFNULL(mg.Dim_MaterialGroupid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN Dim_MaterialGroup mg ON mg.MaterialGroupCode = f.EBAN_MATKL
	WHERE f.Dim_MaterialGroupid <> IFNULL(mg.Dim_MaterialGroupid, 1);
	
	/*DIM_SupplPlantStockTranspOrdId*/ /* 15. */
	UPDATE tmp_fpr_t001t f
	SET f.DIM_SupplPlantStockTranspOrdId = IFNULL(dps.Dim_Plantid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN Dim_Plant dps ON dps.PlantCode = f.EBAN_RESWK
	WHERE f.DIM_SupplPlantStockTranspOrdId <> IFNULL(dps.Dim_Plantid, 1);
	
	/*Dim_UnitOfMeasureid*/      /* 17. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_UnitOfMeasureid = IFNULL(uom.Dim_UnitOfMeasureid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN Dim_UnitOfMeasure uom ON uom.UOM = f.EBAN_MEINS
	WHERE f.Dim_UnitOfMeasureid <> IFNULL(uom.Dim_UnitOfMeasureid, 1); 
	
	/*dim_dateRequsiotionid*/      /* 18. */
	UPDATE tmp_fpr_t001t f
	SET f.dim_dateRequsiotionid = IFNULL(pd.dim_dateid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_date pd ON pd.datevalue  = f.EBAN_BADAT
		         AND pd.companycode = 'Not Set'
	WHERE f.dim_dateRequsiotionid <> IFNULL(pd.dim_dateid, 1);
	
	/* dim_datePurchaseReqReleaseid */ /* 19. */
	UPDATE tmp_fpr_t001t f
	SET f.dim_datePurchaseReqReleaseid = IFNULL(pd.dim_dateid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_date pd ON pd.datevalue  = f.EBAN_FRGDT
		         AND pd.companycode = 'Not Set'
	WHERE f.dim_datePurchaseReqReleaseid <> IFNULL(pd.dim_dateid, 1);
	
	/*dim_dateItemDeliveryid*/      /* 20. */
	UPDATE tmp_fpr_t001t f
	SET f.dim_dateItemDeliveryid = IFNULL(pd.dim_dateid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_date pd ON pd.datevalue   = f.EBAN_LFDAT
			 AND pd.companycode = 'Not Set' 
	WHERE f.dim_dateItemDeliveryid <> IFNULL(pd.dim_dateid, 1);
	
	/*Dim_ItemCategoryid*/          /* 23. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_ItemCategoryid = IFNULL(ic.Dim_ItemCategoryid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN Dim_ItemCategory ic ON ic.CategoryCode = f.EBAN_PSTYP
	WHERE  f.Dim_ItemCategoryid <> IFNULL(ic.Dim_ItemCategoryid, 1);
	
	/*dim_accountcategoryid*/   /* 24. */
	UPDATE tmp_fpr_t001t f
	SET f.dim_accountcategoryid = ifnull(ac.Dim_AccountCategoryid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN Dim_AccountCategory ac ON ac.Category = f.EBAN_KNTTP 
	WHERE f.dim_accountcategoryid <> IFNULL(ac.Dim_AccountCategoryid, 1);
	
	/*dim_consumptiontypeid*/     /* 25. */
	UPDATE tmp_fpr_t001t f
	SET f.dim_consumptiontypeid = IFNULL(ct.Dim_ConsumptionTypeid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN Dim_ConsumptionType ct ON ct.ConsumptionCode = f.EBAN_KZVBR 
	WHERE f.dim_consumptiontypeid <> IFNULL(ct.Dim_ConsumptionTypeid, 1);
	
	/*Dim_Vendorid*/               /* 26. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_Vendorid = IFNULL(v.Dim_Vendorid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_vendor v ON v.VendorNumber = f.EBAN_LIFNR
	WHERE f.Dim_Vendorid <> IFNULL(v.Dim_Vendorid, 1);
	
	/*Dim_PurchaseOrgid*/         /* 27. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_PurchaseOrgid = IFNULL(po.Dim_PurchaseOrgid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN Dim_PurchaseOrg po ON po.PurchaseOrgCode = f.EBAN_EKORG
	WHERE f.Dim_PurchaseOrgid <> IFNULL(po.Dim_PurchaseOrgid, 1);
	
	/*Dim_FixedVendorid*/           /* 28. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_FixedVendorid = IFNULL(v.Dim_Vendorid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_vendor v ON v.VendorNumber = f.EBAN_FLIEF
	WHERE f.Dim_FixedVendorid <> IFNULL(v.Dim_Vendorid, 1);
	
	/*dim_datePurchaseOrderId*/     /* 32. */
	UPDATE tmp_fpr_t001t f
	SET f.dim_datePurchaseOrderId = IFNULL(pd.dim_dateid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN dim_date pd ON pd.datevalue   = f.EBAN_BEDAT
			 AND pd.companycode = 'Not Set'
	WHERE f.dim_datePurchaseOrderId <> IFNULL(pd.dim_dateid, 1);
	
	/*Dim_Currencyid*/            /* 35. */
	UPDATE tmp_fpr_t001t f
	SET f.Dim_Currencyid = IFNULL(dcr.Dim_Currencyid, 1)
	FROM tmp_fpr_t001t f
	LEFT JOIN Dim_Currency dcr ON dcr.CurrencyCode = f.EBAN_WAERS
	WHERE f.Dim_Currencyid <> IFNULL(dcr.Dim_Currencyid, 1);
	
	/* **********  START INSERT NEW ROWS IN FACT   ********** */	
	insert into fact_purchase_requisition (
							fact_purchase_requisitionid   , 
							dd_purchasereqno 	      , /* EBAN_BANFN */
							dd_purchasereqitemno	      , /* EBAN_BNFPO */
							Dim_DocumentTypeid            , /* EBAN_BSART */
							dd_PurchaseReqDocCat          , /* EBAN_BSTYP */
							dd_DeletionIndicator          , /* EBAN_LOEKZ */
							dd_ProcessingStatus           , /* EBAN_STATU */
							dd_CreationIndicator          , /* EBAN_ESTKZ */
							Dim_PurchaseGroupid           , /* EBAN_EKGRP */ 
							dd_ReleaseIndicator           , /* EBAN_FRGKZ */
							dd_CreatedObjectByPerson      , /* EBAN_ERNAM */
							dim_datechangeonid            , /* EBAN_ERDAT */
							DIM_MATERIALID                , /* EBAN_MATNR */
							Dim_Plantid                   , /* EBAN_WERKS */
							dim_storagelocationid         , /* EBAN_LGORT */
							Dim_MaterialGroupid           , /* EBAN_MATKL */
							dd_RequestTrackNo             , /* EBAN_BEDNR */
							DIM_SupplPlantStockTranspOrdId, /* EBAN_RESWK */
							ct_PurchaseReq                , /* EBAN_MENGE */
							Dim_UnitOfMeasureid           , /* EBAN_MEINS */
							dim_dateRequsiotionid         , /* EBAN_BADAT */
							dim_datePurchaseReqReleaseid  , /* EBAN_FRGDT */
							dim_dateItemDeliveryid        , /* EBAN_LFDAT */
							amt_PurchaseReqPrice          , /* EBAN_PREIS */
							ct_PriceUnit                  , /* EBAN_PEINH */
							Dim_ItemCategoryid            , /* EBAN_PSTYP */
							dim_accountcategoryid         , /* EBAN_KNTTP */
							dim_consumptiontypeid         , /* EBAN_KZVBR */
							Dim_Vendorid                  , /* EBAN_LIFNR */
							Dim_PurchaseOrgid             , /* EBAN_EKORG */
							Dim_FixedVendorid             , /* EBAN_FLIEF */
							dd_PurchaseInforRecord        , /* EBAN_INFNR */
							dd_PurchaseOrderNo            , /* EBAN_EBELN */
							dd_PurchaseOrderItemNo        , /* EBAN_EBELP */
							dim_datePurchaseOrderId       , /* EBAN_BEDAT */
							ct_OrderedAgainstPurchase     , /* EBAN_BSMNG */
							dd_PurchaseReqClosed          , /* EBAN_EBAKZ */
							Dim_Currencyid                , /* EBAN_WAERS */
							dd_ManufacturePartNo          , /* EBAN_MFRPN */
							dd_PlannedDeliveryDays        , /* EBAN_PLIFZ */
							dd_PurchaseReqBlocked	      , /* EBAN_BLCKD */
							amt_ExchangeRate,
							amt_ExchangeRate_GBL
							 )
	SELECT 						
							fact_purchase_requisitionid   , 
							dd_purchasereqno 	      , /* EBAN_BANFN */
							dd_purchasereqitemno	      , /* EBAN_BNFPO *//* 00. */
							Dim_DocumentTypeid            , /* EBAN_BSART *//* 01. */
							dd_PurchaseReqDocCat          , /* EBAN_BSTYP *//* 02. */
							dd_deletionindicator          , /* EBAN_LOEKZ *//* 03. */
							dd_ProcessingStatus           , /* EBAN_STATU *//* 04. */
							dd_CreationIndicator          , /* EBAN_ESTKZ *//* 05. */
							Dim_PurchaseGroupid           , /* EBAN_EKGRP *//* 06. */ 
							dd_ReleaseIndicator           , /* EBAN_FRGKZ *//* 07. */
							dd_CreatedObjectByPerson      , /* EBAN_ERNAM *//* 08. */
							dim_datechangeonid            , /* EBAN_ERDAT *//* 09. */
							DIM_MATERIALID                , /* EBAN_MATNR *//* 10. */
							Dim_Plantid                   , /* EBAN_WERKS *//* 11. */
							dim_storagelocationid         , /* EBAN_LGORT *//* 12. */
							Dim_MaterialGroupid           , /* EBAN_MATKL *//* 13. */
							dd_RequestTrackNo             , /* EBAN_BEDNR *//* 14. */
							DIM_SupplPlantStockTranspOrdId, /* EBAN_RESWK *//* 15. */
							ct_PurchaseReq                , /* EBAN_MENGE *//* 16. */
							Dim_UnitOfMeasureid           , /* EBAN_MEINS *//* 17. */
							dim_dateRequsiotionid         , /* EBAN_BADAT *//* 18. */
							dim_datePurchaseReqReleaseid  , /* EBAN_FRGDT *//* 19. */
							dim_dateItemDeliveryid        , /* EBAN_LFDAT *//* 20. */
							amt_PurchaseReqPrice          , /* EBAN_PREIS *//* 21. */
							ct_PriceUnit                  , /* EBAN_PEINH *//* 22. */
							Dim_ItemCategoryid            , /* EBAN_PSTYP *//* 23. */
							dim_accountcategoryid         , /* EBAN_KNTTP *//* 24. */
							dim_consumptiontypeid         , /* EBAN_KZVBR *//* 25. */
							Dim_Vendorid                  , /* EBAN_LIFNR *//* 26. */
							Dim_PurchaseOrgid             , /* EBAN_EKORG *//* 27. */
							Dim_FixedVendorid             , /* EBAN_FLIEF *//* 28. */
							dd_PurchaseInforRecord        , /* EBAN_INFNR *//* 29. */
							dd_PurchaseOrderNo            , /* EBAN_EBELN *//* 30. */
							dd_PurchaseOrderItemNo        , /* EBAN_EBELP *//* 31. */
							dim_datePurchaseOrderId       , /* EBAN_BEDAT *//* 32. */
							ct_OrderedAgainstPurchase     , /* EBAN_BSMNG *//* 33. */
							dd_PurchaseReqClosed          , /* EBAN_EBAKZ *//* 34. */
							Dim_Currencyid                , /* EBAN_WAERS *//* 35. */
							dd_ManufacturePartNo          , /* EBAN_MFRPN *//* 36. */
							dd_PlannedDeliveryDays        , /* EBAN_PLIFZ *//* 37. */
							dd_PurchaseReqBlocked	      , /* EBAN_BLCKD *//* 38. */
							amt_ExchangeRate,
							amt_ExchangeRate_GBL
  	FROM tmp_fpr_t001t;
		/* **********  FINISH INSERT NEW ROWS IN FACT   ********** */		

		/* **********  START CLEAN UP  ********** */				
				 
		DROP TABLE IF EXISTS tmp_fpr_t001t;
		DROP TABLE IF EXISTS tmp_insert_fct_prchs_rqstn;
		DROP TABLE IF EXISTS tmp_delete_fct_prchs_rqstn;	