insert into dim_shippinginstruction (dim_shippinginstructionid)
	select 1 from (select 1) a where not exists (select 1 from dim_shippinginstruction where dim_shippinginstructionid = 1);

update dim_shippinginstruction d
	set d.shippinginstructiondesc = ifnull(s.T027B_EVTXT,'Not Set')
	from dim_shippinginstruction d
		inner join T027B s on d.shippinginstructioncode = s.T027B_EVERS
	where d.shippinginstructiondesc <> ifnull(s.T027B_EVTXT,'Not Set');

delete from number_fountain m where m.table_name = 'dim_shippinginstruction';
insert into number_fountain
select 'dim_shippinginstruction',
            ifnull(max(d.dim_shippinginstructionid ),
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_shippinginstruction d
where d.dim_shippinginstructionid <> 1;

insert into dim_shippinginstruction
	select
		(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_shippinginstruction')  + row_number() over(order by '') dim_shippinginstructionid
		,T027B_EVERS shippinginstructioncode
		,T027B_EVTXT shippinginstructiondesc
	from T027B t where not exists (select 1 from dim_shippinginstruction x where x.shippinginstructioncode = t.T027B_EVERS);
