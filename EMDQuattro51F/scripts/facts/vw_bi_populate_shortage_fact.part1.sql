/* Start of vw_bi_populate_shortage_fact.part1 */
/* LK: This has been completely validated against kla on 17 Jun */

drop table if exists fact_shortage_temp;
create table fact_shortage_temp like fact_shortage including defaults including identity;

alter table fact_shortage_temp add constraint primary key (fact_shortageid);

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp';

INSERT INTO NUMBER_FOUNTAIN SELECT 'fact_shortage_temp', ifnull(max(fact_shortageid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) FROM fact_shortage_temp;

/* Changes made to the temporary table based on the fields needed in the update statements for new rows. */
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_MEINS VARCHAR(10);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_NO_DISP CHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_POSTP VARCHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_MATNR VARCHAR(18);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_WERKS VARCHAR(4);
/*ALlter table fact_shortage_temp add constraint primary key*/
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_LGORT VARCHAR(4);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_PRVBE VARCHAR(10);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_BWART VARCHAR(3);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_KZVBR VARCHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_SOBKZ VARCHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_BDTER DATE;
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_SBTER DATE;

INSERT INTO fact_shortage_temp(fact_shortageid,
ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_OrderItemNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateIdLatestReqDate,
                          Dim_Unitofmeasureid,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_ComponentId,
                          Dim_ComponentPlantId,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_CompSLocId,
                          Dim_SupplyAreaId,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateidRequirement,
                          Dim_DateIdReservRequirement,
                          dd_ReservationNo,
                          dd_ReservationItemNo,
                          Dim_ProductionOrderStatusId,
                          Dim_ProfitCenterId,
						  RESB_MEINS,
						  RESB_NO_DISP,
						  RESB_POSTP,
						  RESB_MATNR,
						  RESB_WERKS,
						  RESB_LGORT,
						  RESB_PRVBE,
						  RESB_BWART,
						  RESB_KZVBR,
						  RESB_SOBKZ,
						  RESB_BDTER,
						  RESB_SBTER)
   SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over (order by ''),
   cast(0.0000 as decimal (19,4)) ct_QtyActual,
       ifnull(RESB_BDMNG,0) ct_QtyRequired,
       cast(0.0000 as decimal (19,4)) ct_QtyReservation,
       (CASE
           WHEN (r.RESB_BDMNG - r.RESB_ENMNG) > 0
           THEN
              r.RESB_BDMNG - r.RESB_ENMNG
           ELSE
              0
        END)
          ct_QtyShortage,
       ifnull(RESB_ENMNG,0) ct_QtyWithdrawn,
       ifnull((CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END),0) ct_QtyWithDrawnUOM ,
       ifnull((CASE WHEN (( r.RESB_ERFMG > (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) AND RESB_KZEAR IS NULL AND (r.RESB_KZKUP IS NULL OR pm.DeliveryComplete = 'Not Set'))
           THEN (r.RESB_ERFMG - (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) ELSE 0 END),0) ct_OpenQty,
       ifnull((CASE WHEN (r.RESB_VMENG = r.RESB_BDMNG) THEN r.RESB_ERFMG ELSE
          (r.RESB_VMENG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))) END),0) ct_ConfirmedQty ,
          0 ct_OnHandQty,
          0 ct_StockQty,
       po.dd_SequenceNo dd_ProductionSequenceNo,
       cast(0 as int) dd_DocumentItemNo,
       'Not Set' dd_DocumentNo,
       po.dd_OrderNumber dd_OrderNo,
       po.dd_OrderItemNo dd_OrderItemNo,
       cast(0 as int) dd_ScheduleNo,
       ifnull(r.RESB_BAUGR, 'Not Set') dd_PeggedRequirement ,
       ifnull(r.RESB_POSNR, 'Not Set') dd_BomItemNo,
       cast(1 as bigint) Dim_DateIdLatestReqDate,
       cast(1 as bigint) Dim_UnitOfMeasureId,
       cast(1 as bigint) Dim_EffectiveForMatPlanningId ,
       cast(1 as bigint) Dim_DocumentTypeid,
       cast(1 as bigint) Dim_ItemCategoryid,
       cast(1 as bigint) Dim_MrpControllerid,
       cast(1 as bigint) Dim_MRPElementid,
       cast(1 as bigint) Dim_Componentid ,
       cast(1 as bigint) Dim_ComponentPlantId,
       po.Dim_PartIdItem Dim_Partid,
       po.Dim_Plantid Dim_Plantid,
       po.Dim_StorageLocationId  Dim_StorageLocationid,
       cast(1 as bigint) Dim_CompSLocId,
       cast(1 as bigint) Dim_SupplyAreaid,
       cast(1 as bigint) Dim_Vendorid,
       cast(0 as int) dd_ControlCycleItemNo,
       'Not Set' dd_ControlCycleNo,
       cast(1 as bigint) Dim_MovementTypeId,
       po.Dim_DateIdBasicStart Dim_DateIdRequirement,
       cast(1 as bigint) Dim_DateIdReservRequirement,
       r.RESB_RSNUM,
       r.RESB_RSPOS,
       po.Dim_ProductionOrderStatusId dim_productionorderstatusid,
       po.Dim_ProfitCenterId Dim_ProfitCenterId,
	   r.RESB_MEINS,		-- Used when updating new added rows
	   r.RESB_NO_DISP,		-- Used when updating new added rows
	   r.RESB_POSTP,		-- Used when updating new added rows
	   r.RESB_MATNR,		-- Used when updating new added rows
	   r.RESB_WERKS,		-- Used when updating new added rows
	   r.RESB_LGORT,		-- Used when updating new added rows
	   r.RESB_PRVBE,		-- Used when updating new added rows
	   r.RESB_BWART,		-- Used when updating new added rows
	   r.RESB_KZVBR,		-- Used when updating new added rows
	   r.RESB_SOBKZ,		-- Used when updating new added rows
	   r.RESB_BDTER,		-- Used when updating new added rows
	   r.RESB_SBTER			-- Used when updating new added rows
  FROM fact_productionorder po
       INNER JOIN dim_productionordermisc pm ON po.Dim_ProductionOrderMiscId = pm.Dim_ProductionOrderMiscId
       INNER JOIN resb r ON r.RESB_AUFNR = po.dd_OrderNumber
 WHERE     r.RESB_POSTP = 'L'
       AND r.RESB_BWART = '261'
       AND r.RESB_XLOEK IS NULL
       AND NOT EXISTS
                  (SELECT 1
                     FROM fact_shortage_temp s
                    WHERE     s.dd_OrderNo = po.dd_OrderNumber
                          AND s.dd_OrderItemNo = po.dd_OrderItemNo
                          AND s.dd_ReservationNo = r.RESB_RSNUM
                          AND s.dd_ReservationItemNo = r.RESB_RSPOS);

						  
/* Update Statements for new added rows */

MERGE INTO  fact_shortage_temp FA
 USING ( SELECT
 T1.fact_shortageid, ifnull(m.MARD_LABST + m.MARD_SPEME,0),ct_OnHandQty
 FROM fact_shortage_temp T1
 LEFT JOIN MARD m ON (m.MARD_WERKS = T1.RESB_WERKS
 AND m.MARD_MATNR = T1.RESB_MATNR
 AND m.MARD_LGORT = T1.RESB_LGORT)) SRC
 ON FA.fact_shortageid = SRC.fact_shortageid
 WHEN MATCHED THEN UPDATE SET FA.ct_OnHandQty = SRC.ct_OnHandQty
 WHERE FA.ct_OnHandQty <> SRC.ct_OnHandQty;

MERGE INTO
 fact_shortage_temp FA
 USING ( SELECT
 T1.fact_shortageid, ifnull(m.MARD_LABST + m.MARD_SPEME,0),ct_OnHandQty
 FROM fact_shortage_temp T1
 LEFT JOIN MARD m ON (m.MARD_WERKS = T1.RESB_WERKS
 AND m.MARD_MATNR = T1.RESB_MATNR
 AND m.MARD_LGORT = T1.RESB_LGORT)) SRC
 ON FA.fact_shortageid = SRC.fact_shortageid
 WHEN MATCHED THEN UPDATE SET FA.ct_OnHandQty = SRC.ct_OnHandQty
 WHERE FA.ct_OnHandQty <> SRC.ct_OnHandQty;

MERGE INTO fact_shortage_temp FA
 USING ( SELECT
 T1.fact_shortageid, ifnull(m.MARD_LABST + m.MARD_SPEME,0),ct_OnHandQty
 FROM fact_shortage_temp T1
 LEFT JOIN MARD m ON (m.MARD_WERKS = T1.RESB_WERKS
 AND m.MARD_MATNR = T1.RESB_MATNR
 AND m.MARD_LGORT = T1.RESB_LGORT)) SRC
 ON FA.fact_shortageid = SRC.fact_shortageid
 WHEN MATCHED THEN UPDATE SET FA.ct_OnHandQty = SRC.ct_OnHandQty
 WHERE FA.ct_OnHandQty <> SRC.ct_OnHandQty; 

MERGE INTO fact_shortage_temp FA
USING ( SELECT
                T1.fact_shortageid, ifnull(m.MARD_LABST + m.MARD_SPEME,0),ct_OnHandQty
                FROM fact_shortage_temp T1
                                LEFT JOIN MARD m ON (m.MARD_WERKS = T1.RESB_WERKS
                                                                 AND m.MARD_MATNR = T1.RESB_MATNR
                                                                 AND m.MARD_LGORT = T1.RESB_LGORT)) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.ct_OnHandQty = SRC.ct_OnHandQty
WHERE FA.ct_OnHandQty <> SRC.ct_OnHandQty;

MERGE INTO fact_shortage_temp FA
USING ( SELECT	T1.fact_shortageid, ifnull(m.MARD_LABST + m.MARD_SPEME,0), T1.ct_StockQty
		FROM fact_shortage_temp T1
				LEFT JOIN MARD m ON (m.MARD_WERKS = T1.RESB_WERKS
								 AND m.MARD_MATNR = T1.RESB_MATNR
								 AND m.MARD_LGORT = T1.RESB_LGORT)) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.ct_StockQty = SRC.ct_StockQty
WHERE FA.ct_StockQty <> SRC.ct_StockQty;

MERGE INTO fact_shortage_temp FA
USING ( SELECT T1.fact_shortageid, ifnull(m.MARD_LABST + m.MARD_SPEME,0) ct_StockQty
		FROM fact_shortage_temp T1
				LEFT JOIN MARD m ON (m.MARD_WERKS = T1.RESB_WERKS
								 AND m.MARD_MATNR = T1.RESB_MATNR
								 AND m.MARD_LGORT = T1.RESB_LGORT)) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.ct_StockQty = SRC.ct_StockQty
WHERE FA.ct_StockQty <> SRC.ct_StockQty;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(dt.Dim_DateId,1) Dim_DateIdLatestReqDate
		FROM fact_shortage_temp T1
				INNER JOIN dim_plant pl ON T1.dim_plantid = pl.dim_plantid 
										AND pl.PlantCode = T1.RESB_WERKS 
				LEFT JOIN dim_date dt ON ( dt.DateValue = T1.RESB_SBTER
									  AND dt.CompanyCode = pl.CompanyCode
									  )
		WHERE pl.RowIsCurrent = 1) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_DateIdLatestReqDate = SRC.Dim_DateIdLatestReqDate
WHERE FA.Dim_DateIdLatestReqDate <> SRC.Dim_DateIdLatestReqDate;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(uom.Dim_UnitOfMeasureid,1) Dim_UnitOfMeasureId
		FROM fact_shortage_temp T1
				LEFT JOIN Dim_UnitOfMeasure uom ON ( uom.UOM = T1.RESB_MEINS) ) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_UnitOfMeasureId = SRC.Dim_UnitOfMeasureId
WHERE FA.Dim_UnitOfMeasureId <> SRC.Dim_UnitOfMeasureId;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(emp.Dim_EffectiveForMatPlanningId,1) Dim_EffectiveForMatPlanningId
		FROM fact_shortage_temp T1
				LEFT JOIN Dim_EffectiveForMatPlanning emp ON ( emp.EffectiveForMatPlanning = T1.RESB_NO_DISP) 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_EffectiveForMatPlanningId = SRC.Dim_EffectiveForMatPlanningId
WHERE FA.Dim_EffectiveForMatPlanningId <> SRC.Dim_EffectiveForMatPlanningId;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(ic.dim_bomitemcategoryid,1) Dim_ItemCategoryid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_bomitemcategory ic ON ( ic.ItemCategory = T1.RESB_POSTP )
	WHERE ic.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_ItemCategoryid = SRC.Dim_ItemCategoryid
WHERE FA.Dim_ItemCategoryid <> SRC.Dim_ItemCategoryid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(dp.dim_partid,1) Dim_Componentid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_part dp ON ( dp.PartNumber = T1.RESB_MATNR
									  AND dp.Plant = T1.RESB_WERKS )
	WHERE dp.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_Componentid = SRC.Dim_Componentid
WHERE FA.Dim_Componentid <> SRC.Dim_Componentid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(p.dim_plantid,1) Dim_ComponentPlantId
		FROM fact_shortage_temp T1
				LEFT JOIN dim_plant p ON ( p.PlantCode = T1.RESB_WERKS )
	WHERE p.RowIsCurrent = 1
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_ComponentPlantId = SRC.Dim_ComponentPlantId
WHERE FA.Dim_ComponentPlantId <> SRC.Dim_ComponentPlantId;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(sl.dim_StorageLocationid,1) Dim_CompSLocId
		FROM fact_shortage_temp T1
				LEFT JOIN dim_StorageLocation sl ON ( sl.Plant = T1.RESB_WERKS
												 AND sl.LocationCode = T1.RESB_LGORT) 
	WHERE sl.RowIsCurrent = 1
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_CompSLocId = SRC.Dim_CompSLocId
WHERE FA.Dim_CompSLocId <> SRC.Dim_CompSLocId;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(sa.Dim_SupplyAreaid,1) Dim_SupplyAreaid
		FROM fact_shortage_temp T1
				LEFT JOIN Dim_SupplyArea sa ON ( sa.Plant = T1.RESB_WERKS
											AND sa.SupplyArea = T1.RESB_PRVBE) 
	WHERE sa.RowIsCurrent = 1
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_SupplyAreaid = SRC.Dim_SupplyAreaid
WHERE FA.Dim_SupplyAreaid <> SRC.Dim_SupplyAreaid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, max(ifnull(T0.dim_Vendorid,1)) dim_Vendorid
		FROM fact_shortage_temp T1
				LEFT JOIN ( SELECT dv.dim_Vendorid, e.EORD_MATNR, e.EORD_WERKS, e.EORD_FLIFN, e.EORD_BDATU
							FROM dim_Vendor dv
									INNER JOIN EORD e ON (dv.vendornumber = EORD_LIFNR)
							WHERE e.EORD_FLIFN = 'X'
							AND   dv.RowIsCurrent = 1
						    	AND   e.EORD_BDATU > current_date
						   ) T0
							ON ( T0.EORD_MATNR = T1.RESB_MATNR
							AND T0.EORD_WERKS = T1.RESB_WERKS) 
		GROUP BY fact_shortageid
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.dim_Vendorid = SRC.dim_Vendorid
WHERE FA.dim_Vendorid <> SRC.dim_Vendorid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(mt.dim_movementtypeid,1) dim_movementtypeid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_movementtype mt ON ( mt.MovementType = T1.RESB_BWART
											  AND mt.ConsumptionIndicator = ifnull(T1.RESB_KZVBR, 'Not Set')
											  AND mt.SpecialStockIndicator = ifnull(T1.RESB_SOBKZ, 'Not Set') ) 
	WHERE mt.MovementIndicator = 'Not Set'
	AND mt.ReceiptIndicator = 'Not Set'
	AND mt.RowIsCurrent = 1
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.dim_movementtypeid = SRC.dim_movementtypeid
WHERE FA.dim_movementtypeid <> SRC.dim_movementtypeid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(dt.Dim_DateId,1) Dim_DateIdReservRequirement
		FROM fact_shortage_temp T1
				INNER JOIN dim_plant pl ON (pl.PlantCode = T1.RESB_WERKS)
				LEFT JOIN dim_date dt ON ( dt.DateValue = T1.RESB_BDTER
									  AND dt.CompanyCode = pl.CompanyCode)
		WHERE pl.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_DateIdReservRequirement = SRC.Dim_DateIdReservRequirement
WHERE FA.Dim_DateIdReservRequirement <> SRC.Dim_DateIdReservRequirement;

/* END Update Statements for new added rows */

/* Delete columns used when updating new records */
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_MEINS;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_NO_DISP;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_POSTP;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_MATNR;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_WERKS;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_LGORT;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_PRVBE;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_BWART;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_KZVBR;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_SOBKZ;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_BDTER;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_SBTER;

UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_shortageid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) FROM fact_shortage_temp)
 WHERE table_name = 'fact_shortage_temp';

 
DROP TABLE IF EXISTS fact_shortage_temp_1;
CREATE TABLE fact_shortage_temp_1
AS
   SELECT 
		  PKPS_PKIMG ct_QtyActual,
          cast(0.0000 as decimal (19,4)) ct_QtyRequired,
          PKPS_PKBMG ct_QtyReservation,
          (CASE WHEN PKPS_PKIMG = 0 THEN (PKPS_PKBMG - PKPS_PKIMG) ELSE 0 END) ct_QtyShortage,
          cast(0.0000 as decimal (19,4)) ct_QtyWithdrawn,
          cast(0.0000 as decimal (19,4)) ct_QtyWithdrawnUOM,
          cast(0.0000 as decimal (19,4)) ct_OpenQty,
          cast(0.0000 as decimal (19,4)) ct_ConfirmedQty,
          cast(0.0000 as decimal (19,4)) ct_OnHandQty,
          cast(0.0000 as decimal (19,4)) ct_StockQty,
          cast(0 as bigint) dd_ProductionSequenceNo,
          cast(0 as int) dd_DocumentItemNo,
          'Not Set' dd_DocumentNo,
          'Not Set' dd_OrderNo,
          cast(0 as int) dd_ScheduleNo,
          'Not Set' dd_PeggedRequirement,
          'Not Set' dd_BomItemNo,
          cast(1 as int) Dim_DateIdLatestReqDate,
          cast(1 as int) Dim_UnitOfMeasureId,
          cast(1 as int) Dim_EffectiveForMatPlanningId,
          cast(1 as int) Dim_DocumentTypeid,
          cast(1 as int) Dim_ItemCategoryid,
          cast(1 as int) Dim_MrpControllerid,
          cast(1 as int) Dim_MRPElementid,
          1 Dim_Partid,
          p.Dim_Plantid,
          1 Dim_StorageLocationid,
          1 Dim_SupplyAreaid,
        cast(1 as int) Dim_Vendorid,
       PKPS_PKKEY dd_ControlCycleItemNo,
       PKHD_PKNUM dd_ControlCycleNo,
       cast(1 as int) Dim_MovementTypeId,
       cast(1 as int) Dim_DateIdRequirement,
	   dft.PKHD_MATNR,		-- Used when updating new added rows
	   dft.PKHD_WERKS,		-- Used when updating new added rows
	   dft.PKHD_UMLGO,		-- Used when updating new added rows
	   dft.PKHD_PRVBE,		-- Used when updating new added rows
	   dft.PKHD_PKNUM,		-- Used when updating new added rows
	   dft.PKPS_PKKEY		-- Used when updating new added rows
FROM 
PKHD_PKPS dft
INNER JOIN dim_plant p ON dft.PKHD_WERKS = p.PlantCode
WHERE    
p.RowIsCurrent = 1
AND NOT EXISTS
                 (SELECT 1
                    FROM fact_shortage_temp st
                   WHERE st.dd_ControlCycleNo = dft.PKHD_PKNUM
                     AND st.dd_ControlCycleItemNo = dft.PKPS_PKKEY);

/* Update statements for new added rows */
MERGE INTO fact_shortage_temp_1 FA
USING ( SELECT  dp.dim_partid, dp.PartNumber, dp.Plant, dp.RowIsCurrent
		FROM dim_part dp 
		WHERE dp.RowIsCurrent=1) SRC
ON (FA.PKHD_MATNR = SRC.PartNumber AND FA.PKHD_WERKS = SRC.Plant)
WHEN MATCHED THEN UPDATE SET FA.dim_partid = SRC.dim_partid
WHERE FA.dim_partid <> SRC.dim_partid;

MERGE INTO fact_shortage_temp_1 FA
USING ( SELECT  sl.dim_StorageLocationid, sl.Plant, sl.LocationCode, sl.RowIsCurrent
		FROM dim_StorageLocation sl
		WHERE sl.RowIsCurrent=1 ) SRC
ON (FA.PKHD_WERKS = SRC.Plant AND FA.PKHD_UMLGO = SRC.LocationCode )
WHEN MATCHED THEN UPDATE SET FA.Dim_StorageLocationid = SRC.Dim_StorageLocationid
WHERE FA.Dim_StorageLocationid <> SRC.Dim_StorageLocationid;

MERGE INTO fact_shortage_temp_1 FA
USING ( SELECT  sa.Dim_SupplyAreaid, sa.Plant, sa.SupplyArea, sa.RowIsCurrent
		FROM Dim_SupplyArea sa 
		WHERE sa.RowIsCurrent=1) SRC
ON (FA.PKHD_WERKS = SRC.Plant AND FA.PKHD_PRVBE = SRC.SupplyArea)
WHEN MATCHED THEN UPDATE SET FA.Dim_SupplyAreaid = SRC.Dim_SupplyAreaid
WHERE FA.Dim_SupplyAreaid <> SRC.Dim_SupplyAreaid;

/* END Update statements for new added rows */
/* Delete columns used when updating new records */
ALTER TABLE fact_shortage_temp_1 DROP COLUMN PKHD_MATNR;
ALTER TABLE fact_shortage_temp_1 DROP COLUMN PKHD_WERKS;
ALTER TABLE fact_shortage_temp_1 DROP COLUMN PKHD_UMLGO;
ALTER TABLE fact_shortage_temp_1 DROP COLUMN PKHD_PRVBE;
ALTER TABLE fact_shortage_temp_1 DROP COLUMN PKHD_PKNUM;
ALTER TABLE fact_shortage_temp_1 DROP COLUMN PKPS_PKKEY;

/* PKHD_PKSTF IS NULL */
UPDATE  fact_shortage_temp_1 t
SET Dim_Vendorid = dv.dim_Vendorid
FROM fact_shortage_temp_1 t,PKHD_PKPS dft, dim_plant p,dim_Vendor dv, EORD
WHERE t.dd_ControlCycleNo = dft.PKHD_PKNUM
AND t.dd_ControlCycleItemNo = dft.PKPS_PKKEY
AND PKHD_PKSTF IS NULL
AND      dv.vendornumber = EORD_LIFNR
AND dv.RowIsCurrent = 1
AND EORD_MATNR = dft.PKHD_MATNR
AND EORD_WERKS = dft.PKHD_WERKS
AND EORD_FLIFN = 'X'
AND EORD_BDATU > current_date;

/*  PKHD_PKSTF = '0002' */
UPDATE fact_shortage_temp_1 t
SET Dim_Vendorid = dv.dim_Vendorid
FROM fact_shortage_temp_1 t,PKHD_PKPS dft, dim_Vendor dv
WHERE t.dd_ControlCycleNo = dft.PKHD_PKNUM
AND t.dd_ControlCycleItemNo = dft.PKPS_PKKEY
AND PKHD_PKSTF = '0002'
AND dv.vendornumber = PKHD_LIFNR
AND dv.RowIsCurrent = 1;


INSERT INTO fact_shortage_temp(ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateIdLatestReqDate,
                          Dim_UnitOfMeasureId,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_SupplyAreaid,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateIdRequirement,fact_shortageid)
SELECT t.*,
(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over (order by '')
FROM fact_shortage_temp_1 t;


UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_shortageid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) FROM fact_shortage_temp)
 WHERE table_name = 'fact_shortage_temp';

/* Changes made to the temporary table based on the fields needed in the update statements for new rows. */
ALTER TABLE fact_shortage_temp ADD COLUMN EKPO_MATNR VARCHAR(18);
ALTER TABLE fact_shortage_temp ADD COLUMN EKPO_WERKS VARCHAR(4);
ALTER TABLE fact_shortage_temp ADD COLUMN EKPO_LGORT VARCHAR(4);
ALTER TABLE fact_shortage_temp ADD COLUMN EKKO_BSART VARCHAR(4);
ALTER TABLE fact_shortage_temp ADD COLUMN EKKO_BSTYP VARCHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN EKPO_EBELN VARCHAR(50);
ALTER TABLE fact_shortage_temp ADD COLUMN EKPO_EBELP DECIMAL(18,0);

INSERT INTO fact_shortage_temp(ct_ScheduledQty,
                          ct_DeliveredQty,
                          ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateIdLatestReqDate,
                          Dim_UnitOfMeasureId,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateIdRequirement,
					      fact_shortageid,
						  EKPO_MATNR,
						  EKPO_WERKS,
						  EKPO_LGORT,
						  EKKO_BSART,
						  EKKO_BSTYP
						  )
SELECT 
		  EKET_MENGE ct_ScheduledQty,
          EKET_WEMNG ct_DeliveredQty,
          cast(0.00 as decimal (19,4)) ct_QtyActual,
          cast(0.00 as decimal (19,4)) ct_QtyRequired,
          cast(0.00 as decimal (19,4)) ct_QtyReservation,
          (CASE
              WHEN (EKET_MENGE - EKET_WEMNG) > 0
              THEN
                 EKET_MENGE - EKET_WEMNG
              ELSE
                 cast(0 as int)
           END) ct_QtyShortage,
          cast(0.00 as decimal (19,4)) ct_QtyWithdrawn,
          cast(0.00 as decimal (19,4)) ct_QtyWithdrawnUOM,
          cast(0.00 as decimal (19,4)) ct_OpenQty,
          cast(0.00 as decimal (19,4)) ct_ConfirmedQty,
          0 ct_OnHandQty,
          0  ct_StockQty,
          cast(0 as bigint) dd_ProductionSequenceNo,
          EKPO_EBELP dd_DocumentItemNo,
          EKPO_EBELN dd_DocumentNo,
          'Not Set' dd_OrderNo,
          EKET_ETENR dd_ScheduleNo,
          'Not Set' dd_PeggedRequirement,
          'Not Set' dd_BomItemNo,
          cast(1 as int) Dim_DateIdLatestReqDate,
          cast(1 as int) Dim_UnitOfMeasureId,
          cast(1 as int) Dim_EffectiveForMatPlanningId,
          1 Dim_DocumentTypeid,
          cast(1 as int) Dim_ItemCategoryid,
          cast(1 as int) Dim_MrpControllerid,
          cast(1 as int) Dim_MRPElementid,
          1 Dim_Partid,
          p.Dim_Plantid,
          1 Dim_StorageLocationid,
          1 Dim_Vendorid,
          cast(0 as int) dd_ControlCycleItemNo,
          'Not Set' dd_ControlCycleNo,
          cast(1 as int) Dim_MovementTypeId,
          cast(1 as int) Dim_DateIdRequirement,
(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over (order by ''),
		   sto.EKPO_MATNR,		-- Used when updating new added rows
		   sto.EKPO_WERKS,		-- Used when updating new added rows
		   sto.EKPO_LGORT,		-- Used when updating new added rows
		   sto.EKKO_BSART,		-- Used when updating new added rows
		   sto.EKKO_BSTYP		-- Used when updating new added rows
FROM 
ekko_Ekpo_eket sto
INNER JOIN Dim_Plant p ON (p.PlantCode = sto.EKPO_WERKS AND p.RowIsCurrent = 1)
WHERE     
sto.EKKO_BSART IN ('NB','UB','ZNB','ZUB','ZUBA')
AND  NOT EXISTS
                 (SELECT 1
                    FROM fact_shortage_temp st
                   WHERE st.dd_DocumentNo = sto.EKPO_EBELN
                     AND st.dd_DocumentItemNo = sto.EKPO_EBELP
                     AND st.dd_ScheduleNo = sto.EKET_ETENR);
					 
/* Update Statements for new added rows */
MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(m.MARD_LABST + m.MARD_SPEME,0) ct_OnHandQty
		FROM fact_shortage_temp T1
				LEFT JOIN MARD m ON (m.MARD_MATNR = T1.EKPO_MATNR
								 AND m.MARD_WERKS = T1.EKPO_WERKS
								 AND m.MARD_LGORT = T1.EKPO_LGORT)) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.ct_OnHandQty = SRC.ct_OnHandQty
WHERE FA.ct_OnHandQty <> SRC.ct_OnHandQty;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(m.MARD_LABST,0) ct_StockQty
		FROM fact_shortage_temp T1
				LEFT JOIN MARD m ON (m.MARD_MATNR = T1.EKPO_MATNR
								 AND m.MARD_WERKS = T1.EKPO_WERKS
								 AND m.MARD_LGORT = T1.EKPO_LGORT)) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.ct_StockQty = SRC.ct_StockQty
WHERE FA.ct_StockQty <> SRC.ct_StockQty;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(dt.Dim_DocumentTypeid,1) Dim_DocumentTypeid
		FROM fact_shortage_temp T1
				LEFT JOIN Dim_DocumentType dt ON ( dt.Type = T1.EKKO_BSART
											  AND dt.Category = T1.EKKO_BSTYP)
	WHERE dt.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_DocumentTypeid = SRC.Dim_DocumentTypeid
WHERE FA.Dim_DocumentTypeid <> SRC.Dim_DocumentTypeid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(dp.dim_partid,1) dim_partid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_part dp ON ( dp.PartNumber = T1.EKPO_MATNR
									  AND  dp.Plant = T1.EKPO_WERKS )
	WHERE dp.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_Partid = SRC.Dim_Partid
WHERE FA.Dim_Partid <> SRC.Dim_Partid;

/*
MERGE INTO fact_shortage_temp FA
USING ( SELECT 
M fact_shortage_temp T1
 LEFT JOIN dim_StorageLocation sl ON 
		T1.fact_shortageid, ifnull(sl.dim_StorageLocationid,1) dim_StorageLocationid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_StorageLocation sl ON ( sl.Plant = T1.EKPO_WERKS
												  AND sl.LocationCode = T1.EKPO_LGORT )
	--WHERE sl.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.dim_StorageLocationid = SRC.dim_StorageLocationid
WHERE FA.dim_StorageLocationid <> SRC.dim_StorageLocationid
*/


MERGE INTO fact_shortage_temp FA
 USING
(
SELECT   T1.fact_shortageid, MAX (T1.dim_StorageLocationid) AS dim_StorageLocationid
 FROM fact_shortage_temp T1
 LEFT JOIN dim_StorageLocation sl ON ( sl.Plant = T1.EKPO_WERKS
 AND sl.LocationCode = T1.EKPO_LGORT )
 WHERE sl.RowIsCurrent = 1
 GROUP BY T1.fact_shortageid, sl.dim_StorageLocationid) SRC
 ON FA.fact_shortageid = SRC.fact_shortageid
 WHEN MATCHED THEN UPDATE SET
FA.dim_StorageLocationid = SRC.dim_StorageLocationid
WHERE FA.dim_StorageLocationid <> SRC.dim_StorageLocationid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(T0.dim_Vendorid,1) dim_Vendorid
		FROM fact_shortage_temp T1
				LEFT JOIN ( SELECT dv.dim_Vendorid, e.EORD_MATNR, e.EORD_WERKS, e.EORD_FLIFN, e.EORD_BDATU, e.EORD_EBELN, e.EORD_EBELP
							FROM dim_Vendor dv
									INNER JOIN EORD e ON (dv.vendornumber = EORD_LIFNR )
							WHERE e.EORD_FLIFN = 'X'
							AND   dv.RowIsCurrent = 1
						    	AND e.EORD_BDATU > current_date
						   ) T0
							ON ( T0.EORD_MATNR = T1.EKPO_MATNR
							AND T0.EORD_WERKS = T1.EKPO_WERKS
							AND T0.EORD_EBELN = T1.dd_DocumentNo
							AND T0.EORD_EBELP = T1.dd_DocumentItemNo) 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.dim_Vendorid = SRC.dim_Vendorid
WHERE FA.dim_Vendorid <> SRC.dim_Vendorid;

/* END Update Statements for new added rows */
/* Delete columns used when updating new records */
ALTER TABLE fact_shortage_temp DROP COLUMN EKPO_MATNR;
ALTER TABLE fact_shortage_temp DROP COLUMN EKPO_WERKS;
ALTER TABLE fact_shortage_temp DROP COLUMN EKPO_LGORT;
ALTER TABLE fact_shortage_temp DROP COLUMN EKKO_BSART;
ALTER TABLE fact_shortage_temp DROP COLUMN EKKO_BSTYP;
ALTER TABLE fact_shortage_temp DROP COLUMN EKPO_EBELN;
ALTER TABLE fact_shortage_temp DROP COLUMN EKPO_EBELP;


UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_shortageid), 0) FROM fact_shortage_temp)
 WHERE table_name = 'fact_shortage_temp';

/* Changes made to the temporary table based on the fields needed in the update statements for new rows. */
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_MEINS VARCHAR(10);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_NO_DISP CHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_POSTP VARCHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_MATNR VARCHAR(18);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_WERKS VARCHAR(4);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_LGORT VARCHAR(4);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_PRVBE VARCHAR(10);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_BWART VARCHAR(3);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_KZVBR VARCHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_SOBKZ VARCHAR(1);
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_BDTER DATE;
ALTER TABLE fact_shortage_temp ADD COLUMN RESB_SBTER DATE;

INSERT INTO fact_shortage_temp(fact_shortageid,
ct_ScheduledQty,
                          ct_DeliveredQty,
                          ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_ReservationNo,
                          dd_ReservationItemNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateidLatestReqDate,
                          Dim_UnitOfMeasureId,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_SupplyAreaId,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateIdRequirement,
                          Dim_DateIdReservRequirement,
                          Dim_ComponentId,
                          Dim_ComponentPlantId,
                          Dim_CompSLocId,		
						  RESB_MEINS,
						  RESB_NO_DISP,
						  RESB_POSTP,
						  RESB_MATNR,
						  RESB_WERKS,
						  RESB_LGORT,
						  RESB_PRVBE,
						  RESB_BWART,
						  RESB_KZVBR,
						  RESB_SOBKZ,
						  RESB_BDTER,
						  RESB_SBTER					  
                          )
   SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over (order by ''),
   cast(0.00 as decimal (19,4)) ct_ScheduledQty,
          cast(0.00 as decimal (19,4)) ct_DeliveredQty,
          cast(0.00 as decimal (19,4)) ct_QtyActual,
          r.RESB_BDMNG ct_QtyRequired,
          cast(0.00 as decimal (19,4)) ct_QtyReservation,
          ( CASE WHEN (r.RESB_BDMNG - r.RESB_ENMNG) > 0 THEN (r.RESB_BDMNG - r.RESB_ENMNG) ELSE 0 END ) ct_QtyShortage,
          r.RESB_ENMNG ct_QtyWithdrawn,
                  ifnull((CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END),0) ct_QtyWithdrawnUOM,
          --0 ct_OpenQty,
                   ifnull((CASE WHEN (( r.RESB_ERFMG > (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) AND RESB_KZEAR IS NULL AND (r.RESB_KZKUP IS NULL))
           THEN (r.RESB_ERFMG - (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) ELSE 0 END), 0) ct_OpenQty,
          ifnull((CASE WHEN (r.RESB_VMENG = r.RESB_BDMNG) THEN r.RESB_ERFMG ELSE
          (r.RESB_VMENG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))) END),0) ct_ConfirmedQty,
          (m.MARD_LABST + m.MARD_SPEME) ct_OnHandQty,
          m.MARD_LABST  ct_StockQty,
          cast(0 as bigint) dd_ProductionSequenceNo,
          r.RESB_RSNUM dd_ReservationNo,
          r.RESB_RSPOS dd_ReservationItemNo,
          cast(0 as int) dd_DocumentItemNo,
          'Not Set' dd_DocumentNo,
          'Not Set' dd_OrderNo,
          cast(0 as int) dd_ScheduleNo,
          ifnull(r.RESB_BAUGR, 'Not Set') dd_PeggedRequirement,
          ifnull(r.RESB_POSNR, 'Not Set') dd_BomItemNo,
          1 Dim_DateIdLatestReqDate,
          1 Dim_UnitOfMeasureId,
          1 Dim_EffectiveForMatPlanningId,
          cast(1 as int) Dim_DocumentTypeid,
          1 Dim_ItemCategoryid,
          cast(1 as int) Dim_MrpControllerid,
          cast(1 as int) Dim_MRPElementid,
          1 Dim_Partid,
          p.Dim_Plantid,
          1 Dim_StorageLocationid,
          1 Dim_SupplyAreaid,
          1 Dim_Vendorid,
          cast(0 as int) dd_ControlCycleItemNo,
          'Not Set' dd_ControlCycleNo,
           1 Dim_MovementTypeId,
       cast(1 as int) Dim_DateIdRequirement,
       1 Dim_DateIdReservRequirement,
       cast(1 as int) Dim_ComponentId,
       cast(1 as int) Dim_ComponentPlantId,
       cast(1 as int) Dim_CompSLocId,
	   r.RESB_MEINS,		-- Used when updating new added rows
	   r.RESB_NO_DISP,		-- Used when updating new added rows
	   r.RESB_POSTP,		-- Used when updating new added rows
	   r.RESB_MATNR,		-- Used when updating new added rows
	   r.RESB_WERKS,		-- Used when updating new added rows
	   r.RESB_LGORT,		-- Used when updating new added rows
	   r.RESB_PRVBE,		-- Used when updating new added rows
	   r.RESB_BWART,		-- Used when updating new added rows
	   r.RESB_KZVBR,		-- Used when updating new added rows
	   r.RESB_SOBKZ,		-- Used when updating new added rows
	   r.RESB_BDTER,		-- Used when updating new added rows
	   r.RESB_SBTER			-- Used when updating new added rows
FROM 
RESB r
INNER JOIN MARD m ON  (r.RESB_MATNR = m.MARD_MATNR
				  AND r.RESB_WERKS = m.MARD_WERKS
				  AND r.RESB_LGORT = m.MARD_LGORT )
INNER JOIN dim_Plant p ON (p.PlantCode = m.MARD_WERKS AND p.RowIsCurrent = 1)
WHERE  r.RESB_XLOEK IS NULL
AND    r.RESB_BWART = '201'
AND NOT EXISTS
                 (SELECT 1
                    FROM fact_shortage_temp st
                   WHERE st.dd_ReservationNo = r.RESB_RSNUM
                     AND st.dd_ReservationItemNo = r.RESB_RSPOS);
			 
/* Update Statements for new added rows */

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(dt.Dim_DateId,1) Dim_DateIdLatestReqDate
		FROM fact_shortage_temp T1
				INNER JOIN dim_plant pl ON  pl.PlantCode = T1.RESB_WERKS 
				LEFT JOIN dim_date dt ON ( dt.DateValue = T1.RESB_SBTER
									  AND dt.CompanyCode = pl.CompanyCode
									  )
	WHERE pl.RowIsCurrent = 1
	) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_DateIdLatestReqDate = SRC.Dim_DateIdLatestReqDate
WHERE FA.Dim_DateIdLatestReqDate <> SRC.Dim_DateIdLatestReqDate;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(uom.Dim_UnitOfMeasureid,1) Dim_UnitOfMeasureId
		FROM fact_shortage_temp T1
				LEFT JOIN Dim_UnitOfMeasure uom ON ( uom.UOM = T1.RESB_MEINS) ) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_UnitOfMeasureId = SRC.Dim_UnitOfMeasureId
WHERE FA.Dim_UnitOfMeasureId <> SRC.Dim_UnitOfMeasureId;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(emp.Dim_EffectiveForMatPlanningId,1) Dim_EffectiveForMatPlanningId
		FROM fact_shortage_temp T1
				LEFT JOIN Dim_EffectiveForMatPlanning emp ON ( emp.EffectiveForMatPlanning = T1.RESB_NO_DISP) 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_EffectiveForMatPlanningId = SRC.Dim_EffectiveForMatPlanningId
WHERE FA.Dim_EffectiveForMatPlanningId <> SRC.Dim_EffectiveForMatPlanningId;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(ic.dim_bomitemcategoryid,1) Dim_ItemCategoryid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_bomitemcategory ic ON ( ic.ItemCategory = T1.RESB_POSTP )
	WHERE ic.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_ItemCategoryid = SRC.Dim_ItemCategoryid
WHERE FA.Dim_ItemCategoryid <> SRC.Dim_ItemCategoryid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(dp.dim_partid,1) Dim_Partid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_part dp ON ( dp.PartNumber = T1.RESB_MATNR
									  AND dp.Plant = T1.RESB_WERKS )
	WHERE dp.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_Partid = SRC.Dim_Partid
WHERE FA.Dim_Partid <> SRC.Dim_Partid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(sl.dim_StorageLocationid,1) Dim_StorageLocationid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_StorageLocation sl ON ( sl.Plant = T1.RESB_WERKS
												 AND sl.LocationCode = T1.RESB_LGORT )
		WHERE sl.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_StorageLocationid = SRC.Dim_StorageLocationid
WHERE FA.Dim_StorageLocationid <> SRC.Dim_StorageLocationid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(sa.Dim_SupplyAreaid,1) Dim_SupplyAreaid
		FROM fact_shortage_temp T1
				LEFT JOIN Dim_SupplyArea sa ON ( sa.Plant = T1.RESB_WERKS
											AND sa.SupplyArea = T1.RESB_PRVBE )
	WHERE sa.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_SupplyAreaid = SRC.Dim_SupplyAreaid
WHERE FA.Dim_SupplyAreaid <> SRC.Dim_SupplyAreaid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(T0.dim_Vendorid,1) dim_Vendorid
		FROM fact_shortage_temp T1
				LEFT JOIN ( SELECT dv.dim_Vendorid, e.EORD_MATNR, e.EORD_WERKS, e.EORD_FLIFN, e.EORD_BDATU
							FROM dim_Vendor dv
									INNER JOIN EORD e ON (dv.vendornumber = EORD_LIFNR)
							WHERE e.EORD_FLIFN = 'X'
							AND dv.RowIsCurrent = 1
						    	AND e.EORD_BDATU > current_date
						   ) T0
							ON ( T0.EORD_MATNR = T1.RESB_MATNR
							AND T0.EORD_WERKS = T1.RESB_WERKS) 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.dim_Vendorid = SRC.dim_Vendorid
WHERE FA.dim_Vendorid <> SRC.dim_Vendorid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(mt.dim_movementtypeid,1) dim_movementtypeid
		FROM fact_shortage_temp T1
				LEFT JOIN dim_movementtype mt ON ( mt.MovementType = T1.RESB_BWART
											  AND mt.ConsumptionIndicator = T1.RESB_KZVBR
											  AND mt.SpecialStockIndicator = T1.RESB_SOBKZ) 
	WHERE mt.MovementIndicator = 'Not Set'
	AND mt.ReceiptIndicator = 'Not Set'
	AND mt.RowIsCurrent = 1
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.dim_movementtypeid = SRC.dim_movementtypeid
WHERE FA.dim_movementtypeid <> SRC.dim_movementtypeid;

MERGE INTO fact_shortage_temp FA
USING ( SELECT 
		T1.fact_shortageid, ifnull(dt.Dim_DateId,1) Dim_DateIdReservRequirement
		FROM fact_shortage_temp T1
				INNER JOIN dim_plant pl ON (pl.PlantCode = T1.RESB_WERKS)
				LEFT JOIN dim_date dt ON ( dt.DateValue = T1.RESB_BDTER
									  AND dt.CompanyCode = pl.CompanyCode)
	WHERE pl.RowIsCurrent = 1 
		) SRC
ON FA.fact_shortageid = SRC.fact_shortageid
WHEN MATCHED THEN UPDATE SET FA.Dim_DateIdReservRequirement = SRC.Dim_DateIdReservRequirement
WHERE FA.Dim_DateIdReservRequirement <> SRC.Dim_DateIdReservRequirement;

/* END Update Statements for new added rows */

/* Delete columns used when updating new records */
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_MEINS;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_NO_DISP;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_POSTP;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_MATNR;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_WERKS;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_LGORT;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_PRVBE;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_BWART;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_KZVBR;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_SOBKZ;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_BDTER;
ALTER TABLE fact_shortage_temp DROP COLUMN RESB_SBTER;

DROP TABLE IF EXISTS TMP_UPDT_RESB;
CREATE TABLE TMP_UPDT_RESB
AS
SELECT RESB_RSNUM,RESB_RSPOS,RESB_SCHGT,RESB_KZKUP,RESB_KZEAR,RESB_XFEHL,RESB_DUMPS,row_number() over(partition by RESB_RSNUM,RESB_RSPOS order by '') rono
FROM RESB;
	
UPDATE fact_shortage_temp s 
SET s.dim_shortagemiscid = sm.dim_shortagemiscid
from fact_shortage_temp s, TMP_UPDT_RESB r, dim_shortagemisc sm
WHERE s.dd_ReservationNo <> 0
AND s.dd_ReservationItemNo <> 0
AND r.RESB_RSNUM = s.dd_ReservationNo
AND r.RESB_RSPOS = s.dd_ReservationItemNo
AND r.rono = 1
AND sm.BulkMaterial = ifnull(r.RESB_SCHGT, 'Not Set')
AND sm.CoProduct = ifnull(r.RESB_KZKUP, 'Not Set')
AND sm.FinalIssue = ifnull(r.RESB_KZEAR, 'Not Set')
AND sm.MissingPart = ifnull(r.RESB_XFEHL, 'Not Set')
AND sm.PhantomItem = ifnull(r.RESB_DUMPS, 'Not Set');

DROP TABLE IF EXISTS TMP_UPDT_RESB;


UPDATE fact_shortage_temp s
   SET s.amt_StdPrice = 0.0
 WHERE 
s.dd_ControlCycleNo <> 'Not Set' 
AND s.dd_ControlCycleNo IS NOT NULL;
