/**************************************************************************************************************/
/*   Script         : fact_masterdataaccuracy_snapshot                                                */
/*   Author         : Alex D                                                                                   */
/*   Created On     : 07 May 2015                                                                            */ 
/*   Description    : Populate script for  fact_masterdataaccuracy_snapshot from 2 sources fact_inspectionlot and fact_productionorder in order to create a monthly snapshot */
/**************************************************************************************************************/
/*  Date             By         Version	    Desc                                                               */
/**************************************************************************************************************/


/* Check if dd_NotUpdatable is set correct (in cases when the refresh in the last day of the month did not happen)*/
update fact_masterdataaccuracy_snapshot f
set f.dd_NotUpdatable = 1 
from dim_date dd, fact_masterdataaccuracy_snapshot f
where  month(current_timestamp) > dd.calendarmonthnumber
and year(current_timestamp)= dd.calendaryear
and dim_dateid= dim_SnapshotMonthid
and dd_NotUpdatable <> 1;

/*Populating from fact_inspectionlot only open inspections*/
 delete from number_fountain m where m.table_name = 'fact_masterdataaccuracy_snapshot';
insert into number_fountain
select 'fact_masterdataaccuracy_snapshot',
 ifnull(max(f.fact_masterdataaccuracy_snapshotid ), 
              1)
from fact_masterdataaccuracy_snapshot f;





insert into fact_masterdataaccuracy_snapshot
(
fact_masterdataaccuracy_snapshotid,
dw_insert_date,
dw_update_date,
dd_orderno,
dd_objecttype,
dim_partid,
dim_plantid,
dim_SnapshotMonthid,
dim_InpectionEndDateid,
dim_GraceEndDateid,
dim_ActualInspEndDateid,
dd_batchno,
dd_changedby,
dd_createdby,
dd_documentitemno,
dd_documentno,
dd_inspectionlotno,
dd_materialdocitemno,
dd_materialdocno,
dd_materialdocyear,
dd_scheduleno,
dd_usagedecisionchangeby,
dd_usagedecisionmadeby,
dd_NotUpdatable,
dd_GraceDays,
dim_inspectionlotoriginid,
 dim_inspusagedecisionid,
 dim_inspectionlotstatusid
)
select 
(select max_id 
 from number_fountain 
 where table_name = 'fact_masterdataaccuracy_snapshot') + row_number() over(order by '') AS fact_masterdataaccuracy_snapshotid,
 current_timestamp as dw_insert_date,
 current_timestamp as dw_update_date,
 tmp.dd_orderno,
 'Inspections' as dd_objecttype,
 dim_partid,
 dim_plantid,
(SELECT dt.dim_Dateid
        FROM dim_Date dt
        WHERE dt.DateValue = current_date
        AND dt.CompanyCode = 'Not Set') as dim_SnapshotMonthid,
 dim_dateidinspectionend as dim_InpectionEndDateid,
 dim_dateidinspectionend as dim_GraceEndDateid,
 dim_dateidactualinspectionend as dim_ActualInspEndDateid,
 dd_batchno,
dd_changedby,
dd_createdby,
dd_documentitemno,
dd_documentno,
dd_inspectionlotno,
dd_materialdocitemno,
dd_materialdocno,
dd_materialdocyear,
dd_scheduleno,
dd_usagedecisionchangeby,
dd_usagedecisionmadeby,
 0 as dd_NotUpdatable,
 0 as dd_GraceDays,
 dim_inspectionlotoriginid,
 dim_inspusagedecisionid,
 dim_inspectionlotstatusid
 from 
 fact_inspectionlot tmp 
 where dim_dateidactualinspectionend = 1
 and not exists 
 (
 select dd_orderno
 from fact_masterdataaccuracy_snapshot f_snap
 where 
 tmp.dd_orderno = f_snap.dd_orderno 
 and tmp.dd_inspectionlotno = f_snap.dd_inspectionlotno
 and f_snap.dd_objecttype = 'Inspections'
 and dd_NotUpdatable = 0 
 ) ;

 
/*Populating from fact_inspectionlot inspections that need to end this month*/
 delete from number_fountain m where m.table_name = 'fact_masterdataaccuracy_snapshot';
insert into number_fountain
select 'fact_masterdataaccuracy_snapshot',
 ifnull(max(f.fact_masterdataaccuracy_snapshotid ), 
              1)
from fact_masterdataaccuracy_snapshot f;
 
insert into fact_masterdataaccuracy_snapshot
(
fact_masterdataaccuracy_snapshotid,
dw_insert_date,
dw_update_date,
dd_orderno,
dd_objecttype,
dim_partid,
dim_plantid,
dim_SnapshotMonthid,
dim_InpectionEndDateid,
dim_GraceEndDateid,
dim_ActualInspEndDateid,
dd_batchno,
dd_changedby,
dd_createdby,
dd_documentitemno,
dd_documentno,
dd_inspectionlotno,
dd_materialdocitemno,
dd_materialdocno,
dd_materialdocyear,
dd_scheduleno,
dd_usagedecisionchangeby,
dd_usagedecisionmadeby,
dd_NotUpdatable,
dd_GraceDays,
dim_inspectionlotoriginid,
 dim_inspusagedecisionid,
 dim_inspectionlotstatusid
)
select 
(select max_id 
 from number_fountain 
 where table_name = 'fact_masterdataaccuracy_snapshot') + row_number() over(order by '') AS fact_masterdataaccuracy_snapshotid,
 current_timestamp as dw_insert_date,
 current_timestamp as dw_update_date,
 tmp.dd_orderno,
 'Inspections' as dd_objecttype,
 dim_partid,
 dim_plantid,
(SELECT dt.dim_Dateid
        FROM dim_Date dt
        WHERE dt.DateValue = current_date
        AND dt.CompanyCode = 'Not Set') as dim_SnapshotMonthid,
 dim_dateidinspectionend as dim_InpectionEndDateid,
 dim_dateidinspectionend as dim_GraceEndDateid,
 dim_dateidactualinspectionend as dim_ActualInspEndDateid,
 dd_batchno,
dd_changedby,
dd_createdby,
dd_documentitemno,
dd_documentno,
dd_inspectionlotno,
dd_materialdocitemno,
dd_materialdocno,
dd_materialdocyear,
dd_scheduleno,
dd_usagedecisionchangeby,
dd_usagedecisionmadeby,
 0 as dd_NotUpdatable,
 0 as dd_GraceDays,
 dim_inspectionlotoriginid,
 dim_inspusagedecisionid,
 dim_inspectionlotstatusid
 from 
 fact_inspectionlot tmp , dim_date dd
 where dim_dateidinspectionend = dd.dim_dateid
and month(current_timestamp) = dd.calendarmonthnumber
and year(current_timestamp)= dd.calendaryear
and not exists 
 (
 select dd_orderno
 from fact_masterdataaccuracy_snapshot f_snap
 where 
 tmp.dd_orderno = f_snap.dd_orderno 
 and tmp.dd_inspectionlotno = f_snap.dd_inspectionlotno
 and f_snap.dd_objecttype = 'Inspections'
 and dd_NotUpdatable = 0 
 ) ;

 
/*update dd_OpenClosed */
 update fact_masterdataaccuracy_snapshot
 set dd_OpenClosed = case 
                          when dim_ActualInspEndDateid = 1 then 'Open'
						  else 'Closed'
                     end,
      dw_update_date =current_timestamp
where   dd_objecttype = 'Inspections' 
and dd_NotUpdatable = 0 
and dd_OpenClosed <> case 
                          when dim_ActualInspEndDateid = 1 then 'Open'
						  else 'Closed'
                     end;




 


 

/*Populating from fact_productionorder only open orders*/
delete from number_fountain m where m.table_name = 'fact_masterdataaccuracy_snapshot';
insert into number_fountain
select 'fact_masterdataaccuracy_snapshot',
 ifnull(max(f.fact_masterdataaccuracy_snapshotid ), 
              1)
from fact_masterdataaccuracy_snapshot f;




insert into fact_masterdataaccuracy_snapshot
(
fact_masterdataaccuracy_snapshotid,
dw_insert_date,
dw_update_date,
dd_orderno,
dd_objecttype,
dim_partid,
dim_plantid,
dim_SnapshotMonthid,
dim_ScheduledFinishDateid,
dim_GraceEndDateid,
dim_TechnicalCompletionDateid,
dd_bomexplosionno,
dd_bomlevel,
dd_batchno,
dd_operationnumber,
dd_orderdescription,
dd_orderitemno,
dd_plannedorderno,
dd_salesorderdeliveryscheduleno,
dd_salesorderitemno,
dd_salesorderno,
dd_sequenceno,
dd_workcenter,
dd_NotUpdatable,
 dd_cancelledorder,
 dim_dateidconfirmedorderfinish,
 dim_dateidscheduledfinishheader,
 dd_unimitedoverdelivery_merck
)
select 
(select max_id 
 from number_fountain 
 where table_name = 'fact_masterdataaccuracy_snapshot') + row_number() over(order by '') AS fact_masterdataaccuracy_snapshotid,
 current_timestamp as dw_insert_date,
 current_timestamp as dw_update_date,
 fp.dd_ordernumber,
 'Process Order' as dd_objecttype,
 dim_partidheader as dim_partid,
 dim_plantid,
 (SELECT dt.dim_Dateid
        FROM dim_Date dt
        WHERE dt.DateValue = current_date
        AND dt.CompanyCode = 'Not Set') as dim_SnapshotMonthid,
 dim_dateidscheduledfinish as dim_ScheduledFinishDateid,
 dim_dateidscheduledfinish as dim_GraceEndDateid,
 dim_dateidtechnicalcompletion as dim_TechnicalCompletionDateid,
 dd_bomexplosionno,
dd_bomlevel,
dd_batch as dd_batchno,
dd_operationnumber,
dd_orderdescription,
dd_orderitemno,
dd_plannedorderno,
dd_salesorderdeliveryscheduleno,
dd_salesorderitemno,
dd_salesorderno,
dd_sequenceno,
dd_workcenter,
  0 as dd_NotUpdatable,
   dd_cancelledorder,
 dim_dateidconfirmedorderfinish,
 dim_dateidscheduledfinishheader,
 dd_unimitedoverdelivery_merck
 from 
 fact_productionorder fp
 where  dim_dateidtechnicalcompletion = 1
 and not exists (select 1 from fact_masterdataaccuracy_snapshot f_snap where fp.dd_ordernumber = f_snap.dd_orderno and dd_objecttype = 'Process Order'  and dd_NotUpdatable = 0 )
 and dd_orderitemno = 1;
 
/*Populating from fact_productionorder  with orders that need to end this month*/
delete from number_fountain m where m.table_name = 'fact_masterdataaccuracy_snapshot';
insert into number_fountain
select 'fact_masterdataaccuracy_snapshot',
 ifnull(max(f.fact_masterdataaccuracy_snapshotid ), 
              1)
from fact_masterdataaccuracy_snapshot f;




insert into fact_masterdataaccuracy_snapshot
(
fact_masterdataaccuracy_snapshotid,
dw_insert_date,
dw_update_date,
dd_orderno,
dd_objecttype,
dim_partid,
dim_plantid,
dim_SnapshotMonthid,
dim_ScheduledFinishDateid,
dim_GraceEndDateid,
dim_TechnicalCompletionDateid,
dd_bomexplosionno,
dd_bomlevel,
dd_batchno,
dd_operationnumber,
dd_orderdescription,
dd_orderitemno,
dd_plannedorderno,
dd_salesorderdeliveryscheduleno,
dd_salesorderitemno,
dd_salesorderno,
dd_sequenceno,
dd_workcenter,
dd_NotUpdatable,
 dd_cancelledorder,
 dim_dateidconfirmedorderfinish,
 dim_dateidscheduledfinishheader,
 dd_unimitedoverdelivery_merck
)
select 
(select max_id 
 from number_fountain 
 where table_name = 'fact_masterdataaccuracy_snapshot') + row_number() over(order by '') AS fact_masterdataaccuracy_snapshotid,
 current_timestamp as dw_insert_date,
 current_timestamp as dw_update_date,
 fp.dd_ordernumber,
 'Process Order' as dd_objecttype,
 dim_partidheader as dim_partid,
 dim_plantid,
 (SELECT dt.dim_Dateid
        FROM dim_Date dt
        WHERE dt.DateValue = current_date
        AND dt.CompanyCode = 'Not Set') as dim_SnapshotMonthid,
 dim_dateidscheduledfinish as dim_ScheduledFinishDateid,
 dim_dateidscheduledfinish as dim_GraceEndDateid,
 dim_dateidtechnicalcompletion as dim_TechnicalCompletionDateid,
 dd_bomexplosionno,
dd_bomlevel,
dd_batch as dd_batchno,
dd_operationnumber,
dd_orderdescription,
dd_orderitemno,
dd_plannedorderno,
dd_salesorderdeliveryscheduleno,
dd_salesorderitemno,
dd_salesorderno,
dd_sequenceno,
dd_workcenter,
  0 as dd_NotUpdatable,
   dd_cancelledorder,
 dim_dateidconfirmedorderfinish,
 dim_dateidscheduledfinishheader,
 dd_unimitedoverdelivery_merck
 from 
 fact_productionorder fp, dim_date dd
 where  dim_dateidscheduledfinish = dd.dim_dateid
and   month(current_timestamp) = dd.calendarmonthnumber
and year(current_timestamp)= dd.calendaryear
 and not exists (select 1 from fact_masterdataaccuracy_snapshot f_snap where fp.dd_ordernumber = f_snap.dd_orderno and dd_objecttype = 'Process Order'  and dd_NotUpdatable = 0 )
 and dd_orderitemno = 1;
 			 
					 
/*update dd_OpenClosed */
 update fact_masterdataaccuracy_snapshot
 set dd_OpenClosed = case 
                          when dim_TechnicalCompletionDateid = 1 then 'Open'
						  else 'Closed'
                     end,
      dw_update_date =current_timestamp
where   dd_objecttype = 'Process Order' 
and dd_NotUpdatable = 0 
and dd_OpenClosed <> case 
                          when dim_TechnicalCompletionDateid = 1 then 'Open'
						  else 'Closed'
                     end;





						   
/*update dd_PastDueStatus */						   
update fact_masterdataaccuracy_snapshot f
set f.dd_PastDueStatus = case when dd_objecttype = 'Inspections' and dim_ActualInspEndDateid = 1 and dd1.datevalue > dd2.datevalue then 'Past Due' 
                            when dd_objecttype = 'Inspections' and dd3.datevalue > dd2.datevalue then 'Past Due' 
							when dd_objecttype = 'Process Order' and dim_TechnicalCompletionDateid = 1 and dd1.datevalue > dd2.datevalue then 'Past Due'
                            when dd_objecttype = 'Process Order' and dd4.datevalue > dd2.datevalue	then 'Past Due'
                        else 'Not Past Due'
                        end,
     dw_update_date =current_timestamp
from dim_date dd1, dim_date dd2,  dim_date dd3, dim_date dd4, 	fact_masterdataaccuracy_snapshot f				
where dd_NotUpdatable = 0
and dim_SnapshotMonthid = dd1.dim_dateid 
and dim_GraceEndDateid = dd2.dim_dateid
and dim_ActualInspEndDateid =dd3.dim_Dateid
and dim_TechnicalCompletionDateid =dd4.dim_Dateid
and  dd_PastDueStatus <> case when dd_objecttype = 'Inspections' and dim_ActualInspEndDateid = 1 and dd1.datevalue > dd2.datevalue then 'Past Due' 
                            when dd_objecttype = 'Inspections' and dd3.datevalue > dd2.datevalue then 'Past Due' 
							when dd_objecttype = 'Process Order' and dim_TechnicalCompletionDateid = 1 and dd1.datevalue > dd2.datevalue then 'Past Due'
                            when dd_objecttype = 'Process Order' and dd4.datevalue > dd2.datevalue	then 'Past Due'
                        else 'Not Past Due'
                        end;



 
 /*ct_OpenOrders */
 update fact_masterdataaccuracy_snapshot
 set ct_OpenOrders = 1,
 dw_update_date =current_timestamp 
 where dd_OpenClosed = 'Open'
 and ct_OpenOrders <> 1;
 
 
 update fact_masterdataaccuracy_snapshot
 set ct_OpenOrders = 0,
 dw_update_date =current_timestamp 
 where dd_OpenClosed = 'Closed'
 and ct_OpenOrders <> 0;
 
 

  /*ct_AllOrders */
 update fact_masterdataaccuracy_snapshot
 set ct_AllOrders = 1,
 dw_update_date =current_timestamp
 where  ct_AllOrders <> 1;
 


 /* ct_ClosedPastDue*/ 
update fact_masterdataaccuracy_snapshot f
set f.ct_ClosedPastDue = 1,
dw_update_date =current_timestamp
from dim_date dd1, dim_date dd2, fact_masterdataaccuracy_snapshot f
where dd_objecttype = 'Process Order' 
and dim_TechnicalCompletionDateid <> 1
and dd1.datevalue > dd2.datevalue     
and  dd_NotUpdatable = 0
and dim_TechnicalCompletionDateid = dd1.dim_dateid 
and dim_GraceEndDateid = dd2.dim_dateid
and  ct_ClosedPastDue <> 1;


update fact_masterdataaccuracy_snapshot f
set ct_ClosedPastDue = 1,
dw_update_date =current_timestamp
from dim_date dd1, dim_date dd2, fact_masterdataaccuracy_snapshot f
where dd_objecttype = 'Inspections'
and dim_ActualInspEndDateid <> 1
and dd1.datevalue > dd2.datevalue     
and  dd_NotUpdatable = 0
and dim_ActualInspEndDateid = dd1.dim_dateid 
and dim_GraceEndDateid = dd2.dim_dateid
and ct_ClosedPastDue <> 1;

update fact_masterdataaccuracy_snapshot
set ct_ClosedPastDue = 0 ,dw_update_date =current_timestamp
where ct_ClosedPastDue <> 1
and  dd_NotUpdatable = 0
and ct_ClosedPastDue <> 0;


 /* ct_OpenPastDue*/
update fact_masterdataaccuracy_snapshot f
 set f.ct_OpenPastDue = 1,
 f.dw_update_date =current_timestamp
from dim_date dd1, dim_date dd2,  fact_masterdataaccuracy_snapshot f
where dd_objecttype = 'Process Order' 
and dim_TechnicalCompletionDateid = 1
and dd1.datevalue > dd2.datevalue     
and  dd_NotUpdatable = 0
and dim_SnapshotMonthid = dd1.dim_dateid 
and dim_GraceEndDateid = dd2.dim_dateid
and  ct_OpenPastDue <> 1;   
   
update fact_masterdataaccuracy_snapshot f
 set ct_OpenPastDue = 1,
 dw_update_date =current_timestamp
from dim_date dd1, dim_date dd2, fact_masterdataaccuracy_snapshot f
where dd_objecttype = 'Inspections'
and dim_ActualInspEndDateid = 1
and dd1.datevalue > dd2.datevalue     
and  dd_NotUpdatable = 0
and dim_SnapshotMonthid = dd1.dim_dateid 
and dim_GraceEndDateid = dd2.dim_dateid
and ct_OpenPastDue <> 1; 

update fact_masterdataaccuracy_snapshot
set ct_OpenPastDue = 0, dw_update_date =current_timestamp
where ct_OpenPastDue <> 1
and  dd_NotUpdatable = 0
and ct_OpenPastDue <> 0;
 
/* set SnapshotMonthid to current day for all previous data from the same month */
 update fact_masterdataaccuracy_snapshot
 set dim_SnapshotMonthid = (select dim_dateid from dim_date
where datevalue = current_date
and CompanyCode = 'Not Set')
where  dd_NotUpdatable = 0;
 
/* when NotUpdatable is 1 the data for that month cannot be changed (is the last snapshot of the month) */
update fact_masterdataaccuracy_snapshot
set dd_NotUpdatable = 1 
where day(current_timestamp) = 
(select max(dayofmonth) from dim_date 
where CompanyCode = 'Not Set' 
and month (current_timestamp)=calendarmonthnumber  
and year (current_timestamp)=calendaryear);








/*update dd_pastduecategory   */

 update fact_masterdataaccuracy_snapshot a 
 set a.dd_pastduecategory = case when c.datevalue - d.datevalue between 1 and 7 then '001-007 Days'
                                 when c.datevalue - d.datevalue between 8 and 14 then '008-014 Days'
                                 when c.datevalue - d.datevalue between 15 and 21 then '015-021 Days'
                                 when c.datevalue - d.datevalue between 22 and 89 then '022-089 Days'
                                 when c.datevalue - d.datevalue between 90 and 179 then '090-179 Days'
                                 when c.datevalue - d.datevalue between 180 and 269 then '180-269 Days'
                                 when c.datevalue - d.datevalue between 270 and 369 then '270-369 Days'
                                 when c.datevalue - d.datevalue > 369 then '> 369 Days' end,
     a.dw_update_date = current_timestamp
from dim_date b, dim_date c, dim_date d, fact_masterdataaccuracy_snapshot a 
 where a.dim_actualinspenddateid = b.dim_dateid and
       a.dim_snapshotmonthid = c.dim_dateid and 
       a.dim_graceenddateid = d.dim_dateid and 
       a.dd_objecttype = 'Inspections' and 
       b.datevalue = '0001-01-01' and 
       c.datevalue > d.datevalue;
 
   update fact_masterdataaccuracy_snapshot a 
 set a.dd_pastduecategory = case when b.datevalue - d.datevalue between 1 and 7 then '001-007 Days'
                                 when b.datevalue - d.datevalue between 8 and 14 then '008-014 Days'
                                 when b.datevalue - d.datevalue between 15 and 21 then '015-021 Days'
                                 when b.datevalue - d.datevalue between 22 and 89 then '022-089 Days'
                                 when b.datevalue - d.datevalue between 90 and 179 then '090-179 Days'
                                 when b.datevalue - d.datevalue between 180 and 269 then '180-269 Days'
                                 when b.datevalue - d.datevalue between 270 and 369 then '270-369 Days'
                                 when b.datevalue - d.datevalue > 369 then '> 369 Days' end,
     a.dw_update_date = current_timestamp
from dim_date b, dim_date c, dim_date d, fact_masterdataaccuracy_snapshot a 
 where a.dim_actualinspenddateid = b.dim_dateid and
       a.dim_snapshotmonthid = c.dim_dateid and 
       a.dim_graceenddateid = d.dim_dateid and 
       a.dd_objecttype = 'Inspections' and 
       b.datevalue  > d.datevalue;
       
 update fact_masterdataaccuracy_snapshot a 
 set a.dd_pastduecategory = case when c.datevalue - d.datevalue between 1 and 7 then '001-007 Days'
                                 when c.datevalue - d.datevalue between 8 and 14 then '008-014 Days'
                                 when c.datevalue - d.datevalue between 15 and 21 then '015-021 Days'
                                 when c.datevalue - d.datevalue between 22 and 89 then '022-089 Days'
                                 when c.datevalue - d.datevalue between 90 and 179 then '090-179 Days'
                                 when c.datevalue - d.datevalue between 180 and 269 then '180-269 Days'
                                 when c.datevalue - d.datevalue between 270 and 369 then '270-369 Days'
                                 when c.datevalue - d.datevalue > 369 then '> 369 Days' end,
     a.dw_update_date = current_timestamp
from dim_date b, dim_date c, dim_date d,fact_masterdataaccuracy_snapshot a  
 where a.dim_technicalcompletiondateid = b.dim_dateid and
       a.dim_snapshotmonthid = c.dim_dateid and 
       a.dim_graceenddateid = d.dim_dateid and 
       a.dd_objecttype = 'Process Order' and 
       b.datevalue = '0001-01-01' and 
       c.datevalue > d.datevalue;
       
      update fact_masterdataaccuracy_snapshot a 
 set a.dd_pastduecategory = case when b.datevalue - d.datevalue between 1 and 7 then '001-007 Days'
                                 when b.datevalue - d.datevalue between 8 and 14 then '008-014 Days'
                                 when b.datevalue - d.datevalue between 15 and 21 then '015-021 Days'
                                 when b.datevalue - d.datevalue between 22 and 89 then '022-089 Days'
                                 when b.datevalue - d.datevalue between 90 and 179 then '090-179 Days'
                                 when b.datevalue - d.datevalue between 180 and 269 then '180-269 Days'
                                 when b.datevalue - d.datevalue between 270 and 369 then '270-369 Days'
                                 when b.datevalue - d.datevalue > 369 then '> 369 Days' end,
     a.dw_update_date = current_timestamp
from dim_date b, dim_date c, dim_date d, fact_masterdataaccuracy_snapshot a 
 where a.dim_technicalcompletiondateid = b.dim_dateid and
       a.dim_snapshotmonthid = c.dim_dateid and 
       a.dim_graceenddateid = d.dim_dateid and 
       a.dd_objecttype = 'Process Order' and 
       b.datevalue  > d.datevalue;
	   