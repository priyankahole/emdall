
/**************************************************************************************************************/
/*   Script         : vw_bi_populate_purchasing_fact.part2.sql	 */
/*   Author         : Lokesh */
/*   Created On     : 20 Jul 2013 */
/*   Description    : Part 2 of migrated Stored Proc bi_populate_purchasing_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   01 Feb 2014      Mohamed  	1.32			  Added UnlimitedOverDelivery	   							      */
/*   14 Nov 2013      Issam  	1.21			  Dim_ValuationClassId										      */
/*   12 Sep 2013      Lokesh	1.12			  Currency changes Tran/Local/GBL								  */
/*   29 Jul 2013      Lokesh    1.3               Integer Divide by Zero errs on aei fixed						  */
/*   24 Jul 2013      Lokesh    1.2               Merge Mircea's changes for QtyConversion_EqualTo,QtyConversion_Denom	*/
/*							 PriceConversion_EqualTo and PriceConversion_Denom		  */
/*   24 Jul 2013      Lokesh    1.1               Merge Shanthi's changes for ct_qtyreduced                       */
/*   20 Jul 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/


/* Start of Part 2 of vw_bi_populate_purchasing_fact. This needs to be run after the stdprice scripts, which in turn should be run after part 1 above */
/* Reason for running stdprice in the middle - ct_ExchangeRate which is generated in part1 and needed by the function call */


/* Update 3 */
	 /* Update amt_StdUnitPrice and Dim_DateidCosting */
		
/* 3a 	amt_StdUnitPrice */	

/* Update amt_StdUnitPrice using getStdPrice */
	DROP TABLE IF EXISTS tmp_distinct_upd_tmp_getStdPrice;
	CREATE TABLE tmp_distinct_upd_tmp_getStdPrice
	AS
	SELECT DISTINCT z.pCompanyCode,z.pPlant,z.pMaterialNo,z.pFiYear,z.pPeriod,z.vUMREZ,z.vUMREN,z.PONumber,z.pUnitPrice,z.StandardPrice
	FROM tmp_getStdPrice z
	WHERE z.fact_script_name = 'bi_populate_purchasing_fact';

	UPDATE fact_purchase fp
	SET fp.amt_StdUnitPrice = (z.StandardPrice / fp.amt_ExchangeRate)			/* LK 14 Sep: As its in local curr, convert it to tra curr. Note that amt_ExchangeRate is same as ct_ExchangeRate   */
	from ekko_ekpo_eket ds, fact_purchase fp,
		 dim_date dt,
		 tmp_distinct_upd_tmp_getStdPrice z
	WHERE     fp.dd_DocumentNo     = ds.EKPO_EBELN
		  AND fp.dd_DocumentItemNo = ds.EKPO_EBELP
		  AND fp.dd_ScheduleNo     = ds.EKET_ETENR
		  AND dt.DateValue = (select min(x.EKET_BEDAT) 
							  from ekko_ekpo_eket x
							  where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
		  AND dt.CompanyCode      = ds.EKPO_BUKRS
		  AND fp.amt_StdUnitPrice = 0
		  AND z.pCompanyCode = ds.EKPO_BUKRS
		  AND z.pPlant       = ds.EKPO_WERKS
		  AND z.pMaterialNo  = ds.EKPO_MATNR
		  AND z.pFiYear      = dt.FinancialYear
		  AND z.pPeriod      = dt.FinancialMonthNumber
		  /*AND z.fact_script_name = 'bi_populate_purchasing_fact'*/
		  AND z.vUMREZ       = ds.EKPO_UMREZ
		  AND z.vUMREN       = ds.EKPO_UMREN
		  AND z.PONumber     = fp.dd_DocumentNo
		  AND z.pUnitPrice - ifnull(( (ds.EKPO_NETPR / (CASE WHEN ifnull(ds.EKPO_PEINH, 0) = 0 THEN 1 ELSE ds.EKPO_PEINH END))
									 * fp.ct_ExchangeRate
									 * (cast(ds.EKPO_BPUMZ as DECIMAL(18,4))/CASE WHEN ifnull(ds.EKPO_BPUMN,0) = 0 THEN 1 ELSE ds.EKPO_BPUMN END)),0) = 0
		  AND z.StandardPrice IS NOT NULL;
        
/* Override amt_StdUnitPrice where condition matches case */
	UPDATE fact_purchase fp	 
	SET	fp.amt_StdUnitPrice = (CASE WHEN ds.EKKO_LOEKZ = 'L' THEN 0
									WHEN ds.EKPO_LOEKZ = 'L' THEN 0
									WHEN ds.EKPO_RETPO = 'X' THEN 0
									WHEN ds.EKPO_NETPR = 0 THEN 0
									ELSE fp.amt_StdUnitPrice END)
	FROM ekko_ekpo_eket ds, fact_purchase fp
	WHERE     fp.dd_DocumentNo     = ds.EKPO_EBELN
		  AND fp.dd_DocumentItemNo = ds.EKPO_EBELP
		  AND fp.dd_ScheduleNo     = ds.EKET_ETENR
		  AND fp.amt_StdUnitPrice  = 0;
		  
		 
/* 3b Dim_DateidCosting */
	merge into fact_purchase fact
	using (select ds2.fact_purchaseid, ifnull(ds3.costingDate_Id, 1) Dim_DateidCosting
		   from (select fp.fact_purchaseid, dt.FinancialYear, dt.FinancialMonthNumber, 
					    ds1.EKPO_BUKRS, ds1.EKPO_WERKS, ds1.EKPO_MATNR
			     from fact_purchase fp
						inner join (select min(t.EKET_BEDAT)over(partition by t.EKPO_EBELN, t.EKPO_EBELP) min_EKET_BEDAT, t.*
									from ekko_ekpo_eket t) ds1 on    fp.dd_DocumentNo = ds1.EKPO_EBELN
																 AND fp.dd_DocumentItemNo = ds1.EKPO_EBELP
																 AND fp.dd_ScheduleNo = ds1.EKET_ETENR
						inner join dim_date dt on    dt.DateValue = ds1.min_EKET_BEDAT
												 and dt.CompanyCode = ds1.EKPO_BUKRS 
				 where fp.amt_StdUnitPrice = 0) ds2
			left join (select z.* from tmp_getCostingDate z where z.fact_script_name = 'bi_populate_purchasing_fact') ds3
					on    ds3.pCompanyCode = ds2.EKPO_BUKRS
					  AND ds3.pPlant = ds2.EKPO_WERKS
					  AND ds3.pMaterialNo = ds2.EKPO_MATNR
					  AND ds3.pFiYear = ds2.FinancialYear
					  AND ds3.pPeriod = ds2.FinancialMonthNumber
		  ) src
	on fact.fact_purchaseid = src.fact_purchaseid
	when matched then update set fact.Dim_DateidCosting = src.Dim_DateidCosting
	where fact.Dim_DateidCosting <> src.Dim_DateidCosting;

			/* original
			UPDATE fact_purchase fp
			FROM  ekko_ekpo_eket,
				  dim_date dt
			SET  fp.Dim_DateidCosting = ifnull((SELECT costingDate_Id
												FROM tmp_getCostingDate z
												WHERE z.pCompanyCode = EKPO_BUKRS
												  AND z.pPlant = EKPO_WERKS
												  AND z.pMaterialNo = EKPO_MATNR
												  AND z.pFiYear = dt.FinancialYear
												  AND z.pPeriod = dt.FinancialMonthNumber
												  AND z.fact_script_name = 'bi_populate_purchasing_fact'),1)
			WHERE     fp.dd_DocumentNo = EKPO_EBELN
				  AND fp.dd_DocumentItemNo = EKPO_EBELP
				  AND fp.dd_ScheduleNo = EKET_ETENR
				  AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
									  where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
				  AND dt.CompanyCode = EKPO_BUKRS
				  AND fp.amt_StdUnitPrice = 0 */										
 
/* End of Update 3 */

/* Update 4 */

UPDATE fact_purchase fp
SET amt_UnitPrice = ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                                      /* * ct_ExchangeRate */				/* LK: 12 Sep 2013: Removed as a part of currency changes */
                            * (convert(decimal (18,4), EKPO_BPUMZ) / CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END)
							),0)
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
WHERE     fp.dd_DocumentNo     = EKPO_EBELN
	  AND fp.dd_DocumentItemNo = EKPO_EBELP
	  AND fp.dd_ScheduleNo     = EKET_ETENR;		

UPDATE fact_purchase fp
SET  amt_DeliveryTotal = fp.ct_DeliveryQty * ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
													  /* * ct_ExchangeRate */		/* Removed as a part of currency changes */
													  * (convert(decimal (18,4),EKPO_BPUMZ)/CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END)),0)
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
WHERE     fp.dd_DocumentNo     = EKPO_EBELN
	  AND fp.dd_DocumentItemNo = EKPO_EBELP
	  AND fp.dd_ScheduleNo     = EKET_ETENR;	

UPDATE fact_purchase fp
SET  amt_StdPriceAmt = fp.ct_DeliveryQty * fp.amt_StdUnitPrice
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
WHERE 	fp.dd_DocumentNo = EKPO_EBELN
	AND fp.dd_DocumentItemNo = EKPO_EBELP
	AND fp.dd_ScheduleNo = EKET_ETENR;	

UPDATE fact_purchase fp
SET  amt_DeliveryPPV = ifnull((CASE WHEN amt_StdUnitPrice = 0 THEN 0
                                       ELSE ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                                                    /** ct_ExchangeRate*/
                                                    * (convert(decimal (18,4),EKPO_BPUMZ)/(CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END))),0) - amt_StdUnitPrice
                                  END), 0) * fp.ct_DeliveryQty
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
WHERE fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR;	

/* End of Update 4 */	

/* Insert 2 */
delete from NUMBER_FOUNTAIN where table_name = 'dim_purchasemisc';

INSERT INTO NUMBER_FOUNTAIN
select 'dim_purchasemisc',ifnull(max(dim_purchasemiscid),0)
FROM dim_purchasemisc;

INSERT INTO dim_purchasemisc(ItemGRIndicator,
                               ItemDeliveryComplete,
                               ItemReject,
                               ItemGRNonValue,
                               ItemFinalInvoice,
                               ItemProduceInhouse,
                               ItemReturn,
                               HeaderRelease,
                               ExchangeRateFix,
                               DeliveryFixed,
                               DeliveryComplete,
                               FirmedDelivery,
                               ItemStatistical,
							   UnlimitedOverDelivery,
							   dim_purchasemiscid)
SELECT tp.*,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_purchasemisc')+ row_number() over(order by '') as dim_purchasemiscid								   
FROM (SELECT DISTINCT
         ifnull(EKPO_WEPOS, 'Not Set') ItemGRIndicator,
         ifnull(EKPO_ELIKZ, 'Not Set') ItemDeliveryComplete,
         ifnull(EKPO_ABSKZ, 'Not Set') ItemReject,
         ifnull(EKPO_WEUNB, 'Not Set') ItemGRNonValue,
         ifnull(EKPO_EREKZ, 'Not Set') ItemFinalInvoice,
         ifnull(EKPO_J_1BOWNPRO, 'Not Set') ItemProduceInhouse,
         ifnull(EKPO_RETPO, 'Not Set') ItemReturn,
         (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END) HeaderRelease,
         ifnull(EKKO_KUFIX, 'Not Set') ExchangeRateFix,
         ifnull(EKKO_FIXPO, 'Not Set') DeliveryFixed,
         ifnull(EKKO_AUTLF, 'Not Set') DeliveryComplete,
         CASE WHEN ( (fpr.ct_FirmZone > 0) AND (dtdel.DateValue - fpr.ct_FirmZone <= CURRENT_TIMESTAMP ) ) THEN 'X' ELSE 'Not Set' END FirmedDelivery,
         ifnull(EKPO_STAPO, 'Not Set') ItemStatistical,
		 ifnull(EKPO_UEBTK, 'Not Set') UnlimitedOverDelivery
	  FROM fact_purchase fpr,
           ekko_ekpo_eket dm_di,
           Dim_Date dtdel
      WHERE     fpr.dd_DocumentNo = dm_di.EKPO_EBELN
            AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
	        AND fpr.dd_ScheduleNo = EKET_ETENR
            AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
            AND not exists (select 1 
			                from Dim_PurchaseMisc pmisc
                            where pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
                              AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
                              AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
                              AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
                              AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
                              AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
                              AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
                              AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
                              AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
                              AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
                              AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
                              AND pmisc.FirmedDelivery = CASE WHEN ((fpr.ct_FirmZone > 0) AND (dtdel.DateValue -  fpr.ct_FirmZone <= CURRENT_TIMESTAMP)) THEN 'X' ELSE 'Not Set' END
                              AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')							  
							  AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set'))) tp;

/* Expression test  */

update dim_purchasemisc
set ItemGRIndicator2 = case when ItemGRIndicator= 'X' then 'Yes' else 'No' end
where ItemGRIndicator2 <> case when ItemGRIndicator= 'X' then 'Yes' else 'No' end;  

update dim_purchasemisc
set ItemDeliveryComplete2 = case when ItemDeliveryComplete=  'X'  then  'Yes'  else  'No'  end
where ItemDeliveryComplete2 <> case when ItemDeliveryComplete=  'X'  then  'Yes'  else  'No'  end;

update dim_purchasemisc
set ItemReturn2 = case when ItemReturn=  'X'  then  'Yes'  else  'No'  end
where ItemReturn2 <> case when ItemReturn=  'X'  then  'Yes'  else  'No'  end;

update dim_purchasemisc
set ItemReject2 = case when ItemReject= 'X' then 'Yes' else 'No' end
where ItemReject2 <> case when ItemReject= 'X' then 'Yes' else 'No' end;

update dim_purchasemisc
set ItemGRNonValue2 = case when ItemGRNonValue= 'X' then 'Yes' else 'No' end 
where ItemGRNonValue2 <> case when ItemGRNonValue= 'X' then 'Yes' else 'No' end ;

update dim_purchasemisc
set ItemProduceInhouse2 = case when ItemProduceInhouse= 'X' then 'Yes' else 'No' end
where ItemProduceInhouse2 <> case when ItemProduceInhouse= 'X' then 'Yes' else 'No' end ;

update dim_purchasemisc
set ItemFinalInvoice2 = case when ItemFinalInvoice= 'X' then 'Yes' else 'No' end
where ItemFinalInvoice2 <> case when ItemFinalInvoice= 'X' then 'Yes' else 'No' end ;

update dim_purchasemisc
set HeaderRelease2 = case when HeaderRelease= 'X' then 'Yes' else 'No' end
where HeaderRelease2 <> case when HeaderRelease= 'X' then 'Yes' else 'No' end ;

update dim_purchasemisc
set ExchangeRateFix2 = case when ExchangeRateFix= 'X' then 'Yes' else 'No' end
where ExchangeRateFix2 <> case when ExchangeRateFix= 'X' then 'Yes' else 'No' end ;

update dim_purchasemisc
set DeliveryFixed2 = case when DeliveryFixed= 'X' then 'Yes' else 'No' end
where DeliveryFixed2 <> case when DeliveryFixed= 'X' then 'Yes' else 'No' end ;  

update dim_purchasemisc
set DeliveryComplete2 = case when DeliveryComplete= 'X' then 'Yes' else 'No' end
where DeliveryComplete2 <> case when DeliveryComplete= 'X' then 'Yes' else 'No' end ;   

update dim_purchasemisc
set FirmedDelivery2 = case when FirmedDelivery= 'X' then 'Yes' else 'No' end
where FirmedDelivery2 <> case when FirmedDelivery= 'X' then 'Yes' else 'No' end ; 

update dim_purchasemisc
set UnlimitedOverDelivery2 = case when UnlimitedOverDelivery= 'X' then 'Yes' else 'No' end
where UnlimitedOverDelivery2 <> case when UnlimitedOverDelivery= 'X' then 'Yes' else 'No' end;   


/* End of Insert 2 */							  
							
/* Update 5 */	
  UPDATE fact_purchase fpr
  SET fpr.Dim_PurchaseMiscid = pmisc.Dim_PurchaseMiscid
  FROM   Dim_PurchaseMisc pmisc,
         ekko_ekpo_eket dm_di,
         Dim_Date dtdel, fact_purchase fpr
   WHERE     fpr.dd_DocumentNo = dm_di.EKPO_EBELN
         AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
         AND fpr.dd_ScheduleNo = EKET_ETENR
         AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
         AND pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
         AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
         AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
         AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
         AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
         AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
         AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
         AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
         AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
         AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
         AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
		 AND fpr.ct_FirmZone > 0 AND (dtdel.DateValue - fpr.ct_FirmZone <= CURRENT_TIMESTAMP)
		 AND pmisc.FirmedDelivery = 'X'
         AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')
		 AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set')
		 AND fpr.Dim_PurchaseMiscid <> pmisc.Dim_PurchaseMiscid;	

  UPDATE fact_purchase fpr
  SET fpr.Dim_PurchaseMiscid = pmisc.Dim_PurchaseMiscid
  FROM   Dim_PurchaseMisc pmisc,
         ekko_ekpo_eket dm_di,
         Dim_Date dtdel, fact_purchase fpr
   WHERE     fpr.dd_DocumentNo = dm_di.EKPO_EBELN
         AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
	 AND fpr.dd_ScheduleNo = EKET_ETENR
         AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
         AND pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
         AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
         AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
         AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
         AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
         AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
         AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
         AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
         AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
         AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
         AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
		 AND ( fpr.ct_FirmZone <= 0 OR (dtdel.DateValue - fpr.ct_FirmZone > CURRENT_TIMESTAMP))
		 AND pmisc.FirmedDelivery = 'Not Set'
         AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')
		 AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set')
		 AND fpr.Dim_PurchaseMiscid <> pmisc.Dim_PurchaseMiscid;	

/* End of Update 5 */			 
	/* Dim_DateIdPRRelease */
	DROP TABLE IF EXISTS ekko_ekpo_eket_eban_tmppurch;
	CREATE TABLE 	ekko_ekpo_eket_eban_tmppurch AS	
	SELECT DISTINCT dt.Dim_DateId,dm_di.EBAN_BANFN, dm_di.EBAN_BNFPO, dm_di.EKPO_BUKRS, dm_di.EBAN_FRGDT, row_number() 
			over(partition by dm_di.EBAN_BANFN, dm_di.EBAN_BNFPO order by dm_di.EBAN_FRGDT desc) rownumber
	FROM 
	fact_purchase fpr, 
	ekko_ekpo_eket_eban dm_di,
	Dim_Date dt
	WHERE  fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
		   AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
		   AND dt.CompanyCode = dm_di.EKPO_BUKRS
		   AND dt.DateValue = dm_di.EBAN_FRGDT
		   AND fpr.Dim_DateIdPRRelease <> dt.Dim_DateId
		   AND fpr.dd_PurchaseReqNo <> 'Not Set';
	 
	UPDATE fact_purchase fpr
	SET fpr.Dim_DateIdPRRelease = dt.Dim_DateId
	FROM ekko_ekpo_eket_eban_tmppurch dm_di, fact_purchase fpr,
		 Dim_Date dt
	WHERE     fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
		  AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
		  AND dt.CompanyCode = dm_di.EKPO_BUKRS
		  AND dt.DateValue = dm_di.EBAN_FRGDT
		  AND fpr.Dim_DateIdPRRelease <> dt.Dim_DateId
		  AND fpr.dd_PurchaseReqNo <> 'Not Set'
		  and dm_di.rownumber = 1;
		  
		 /* Original - modified due to ambiguos replace
		  UPDATE fact_purchase fpr
		  FROM ekko_ekpo_eket_eban dm_di,
				 Dim_Date dt
			 SET fpr.Dim_DateIdPRRelease = dt.Dim_DateId
		   WHERE     fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
				 AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
				 AND dt.CompanyCode = dm_di.EKPO_BUKRS
				 AND dt.DateValue = dm_di.EBAN_FRGDT
				 AND fpr.Dim_DateIdPRRelease <> dt.Dim_DateId
				 AND fpr.dd_PurchaseReqNo <> 'Not Set'*/		  

	/* Dim_DateIdRequistionDate */			 
	DROP TABLE IF EXISTS ekko_ekpo_eket_eban_tmppurch;
	CREATE TABLE 	ekko_ekpo_eket_eban_tmppurch AS	
	SELECT DISTINCT rdt.Dim_DateId,m_di.EBAN_BANFN, m_di.EBAN_BNFPO, m_di.EKPO_BUKRS, m_di.EBAN_BADAT, row_number() 
			over(partition by m_di.EBAN_BANFN, m_di.EBAN_BNFPO order by m_di.EBAN_BADAT desc) rownumber
	FROM fact_purchase fpr, 
		 ekko_ekpo_eket_eban m_di,
		 Dim_Date rdt
	WHERE    fpr.dd_PurchaseReqNo = m_di.EBAN_BANFN
			 AND fpr.dd_PurchaseReqItemNo = m_di.EBAN_BNFPO
			 AND rdt.CompanyCode = m_di.EKPO_BUKRS
			 AND rdt.DateValue = m_di.EBAN_BADAT
			 AND fpr.Dim_DateIdRequistionDate <> rdt.Dim_DateId
			 AND fpr.dd_PurchaseReqNo <> 'Not Set';

			
	UPDATE fact_purchase fpr
	SET fpr.Dim_DateIdRequistionDate = rdt.Dim_DateId
	FROM ekko_ekpo_eket_eban_tmppurch dm_di, fact_purchase fpr,
		 Dim_Date rdt
	WHERE     fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
		  AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
		  AND rdt.CompanyCode = dm_di.EKPO_BUKRS
		  AND rdt.DateValue = dm_di.EBAN_BADAT
		  AND fpr.Dim_DateIdRequistionDate <> rdt.Dim_DateId
		  AND fpr.dd_PurchaseReqNo <> 'Not Set'
		  AND dm_di.rownumber = 1;
	
	DROP TABLE IF EXISTS ekko_ekpo_eket_eban_tmppurch;
		

		 /* Original - modified due to ambiguos replace
		  UPDATE fact_purchase fpr
		  SET fpr.Dim_DateIdRequistionDate = rdt.Dim_DateId
		  FROM ekko_ekpo_eket_eban dm_di, fact_purchase fpr,
			   Dim_Date rdt
		   WHERE     fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
				 AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
				 AND rdt.CompanyCode = dm_di.EKPO_BUKRS
				 AND rdt.DateValue = dm_di.EBAN_BADAT
				 AND fpr.Dim_DateIdRequistionDate <> rdt.Dim_DateId
				 AND fpr.dd_PurchaseReqNo <> 'Not Set' */
		 
	/* dd_IntOrder */
	DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_ekkn;
	CREATE TABLE tmp_latest_ekko_ekpo_ekkn AS
	SELECT DISTINCT dm_di.*
	FROM ekko_ekpo_ekkn dm_di,
	    (SELECT dm_di.EKKN_EBELN, dm_di.EKKN_EBELP, max(ekpo_aedat) ekpo_aedat
 		 FROM ekko_ekpo_ekkn dm_di 
		 GROUP BY dm_di.EKKN_EBELN, dm_di.EKKN_EBELP) t
	WHERE     dm_di.EKKN_EBELN = t.EKKN_EBELN 
	      AND dm_di.EKKN_EBELP = t.EKKN_EBELP 
		  AND dm_di.ekpo_aedat = t.ekpo_aedat;

	DROP TABLE IF EXISTS ekko_ekpo_ekkn_tmppurch;
	CREATE TABLE 	ekko_ekpo_ekkn_tmppurch AS	
	SELECT ek.EKKN_EBELN,ek.EKKN_EBELP,MAX(ek.EKKN_AUFNR) EKKN_AUFNR
	FROM tmp_latest_ekko_ekpo_ekkn ek
	GROUP BY ek.EKKN_EBELN,ek.EKKN_EBELP;
	  
	UPDATE fact_purchase fpr
    SET fpr.dd_IntOrder = ifnull(ek.EKKN_AUFNR, 'Not Set')
	FROM ekko_ekpo_ekkn_tmppurch ek, fact_purchase fpr
    WHERE    fpr.dd_DocumentNo = ek.EKKN_EBELN
         AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
		 AND ifnull(fpr.dd_IntOrder,'yy') <> ifnull(ek.EKKN_AUFNR, 'Not Set');
		 
	DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_ekkn;
	DROP TABLE IF EXISTS ekko_ekpo_ekkn_tmppurch;
		 
		 /* Original - modified due to ambiguos replace
		  UPDATE fact_purchase fpr
		  FROM ekko_ekpo_ekkn ek
			 SET fpr.dd_IntOrder = ifnull(ek.EKKN_AUFNR, 'Not Set')
		   WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
				 AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
				 AND ifnull(fpr.dd_IntOrder,'yy') <> ifnull(ek.EKKN_AUFNR, 'Not Set')*/		 
		 
	/* dd_WBSElement */
	UPDATE fact_purchase fpr
	SET fpr.dd_WBSElement = ifnull(p.PRPS_POSID, 'Not Set')
	FROM ekko_ekpo_ekkn ek, PRPS p, fact_purchase fpr
	WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
			 AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
			 AND p.PRPS_PSPNR = ek.EKKN_PS_PSP_PNR
			 AND ifnull(fpr.dd_WBSElement,'xx') <> ifnull(p.PRPS_POSID, 'Not Set');

	/* dd_SalesDocNo */
	DROP table if exists tmp_ekko_ekpo_ekkn_rowno;
	create table tmp_ekko_ekpo_ekkn_rowno
	as
	select EKKN_EBELN,EKKN_EBELP,EKKN_VBELN,EKKN_VBELP,EKKN_VETEN, row_number() over (order by '') rowno
	from ekko_ekpo_ekkn;

	  UPDATE fact_purchase fpr
	  SET fpr.dd_SalesDocNo = ifnull(ek.EKKN_VBELN, 'Not Set'),
			 fpr.dd_SalesItemNo = ifnull(ek.EKKN_VBELP,0),
		 fpr.dd_SalesScheduleNo = ifnull(ek.EKKN_VETEN,0)
	  FROM tmp_ekko_ekpo_ekkn_rowno ek, fact_purchase fpr
	   WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
			 AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
			 AND ek.rowno = 1;
	DROP TABLE IF EXISTS tmp_ekko_ekpo_ekkn_rowno;		 

	/* ct_MinimumPOQty */
drop table if exists tmp_EINE;
create table tmp_EINE as 
select EINE_MINBM,EBELN,EBELP, row_number() over (partition by EBELN,EBELP order by '') rowno from EINE;
	  UPDATE fact_purchase fpr
	  SET fpr.ct_MinimumPOQty = ifnull(e.EINE_MINBM, 0)
	  FROM tmp_EINE e, fact_purchase fpr
	   WHERE     e.EBELN = fpr.dd_DocumentNo AND e.EBELP = fpr.dd_DocumentItemNo
             AND rowno = 1
			 AND ifnull(fpr.ct_MinimumPOQty,-1) <> ifnull(e.EINE_MINBM, 0);
	 DROP TABLE IF EXISTS tmp_EINE;
 
	  DELETE FROM fact_purchase
	   WHERE EXISTS
				(SELECT 1
				   FROM EKKO_EKPO_EKET
				  WHERE     EKPO_EBELN = dd_DocumentNo
						AND EKPO_EBELP = dd_DocumentItemNo
						AND EKPO_LOEKZ = 'L');

/* Backup rows for tracking before they are deleted */
INSERT INTO tmp_fact_purchase_deleted
SELECT dd_documentno,dd_documentitemno,dd_scheduleno,current_timestamp,fact_purchaseid,'D'
FROM fact_purchase
WHERE EXISTS ( select 1 from CDPOS_DEL_EKKOEKPOEKET a where a.CDPOS_OBJECTID = dd_DocumentNo AND a.CDPOS_TABNAME = 'EKKO' AND a.CDPOS_CHNGIND = 'D');

/* Delete based on docno */
DELETE FROM fact_purchase
WHERE EXISTS ( select 1 from CDPOS_DEL_EKKOEKPOEKET a where a.CDPOS_OBJECTID = dd_DocumentNo AND a.CDPOS_TABNAME = 'EKKO' AND a.CDPOS_CHNGIND = 'D');


/*  DELETE FROM fact_purchase
   WHERE NOT EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo
                    AND EKPO_EBELP = dd_DocumentItemNo)
         AND EXISTS
                (SELECT 1
                   FROM EKKO_EKPO
                  WHERE EKPO_EBELN = dd_DocumentNo
                        AND EKPO_EBELP = dd_DocumentItemNo)

  DELETE FROM fact_purchase
   WHERE EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo)
         AND NOT EXISTS
                    (SELECT 1
                       FROM EKKO_EKPO_EKET
                      WHERE EKPO_EBELN = dd_DocumentNo
                            AND EKPO_EBELP = dd_DocumentItemNo)

  DELETE FROM fact_purchase
   WHERE EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo
                    AND EKPO_EBELP = dd_DocumentItemNo)
         AND NOT EXISTS
                    (SELECT 1
                       FROM EKKO_EKPO_EKET
                      WHERE     EKPO_EBELN = dd_DocumentNo
                            AND EKPO_EBELP = dd_DocumentItemNo
                            AND EKET_ETENR = dd_ScheduleNo)*/
							
							
DROP TABLE IF EXISTS tmp_pf_mnb1;
CREATE TABLE tmp_pf_mnb1 AS
SELECT 	x.MATNR,x.BWKEY, max((x.LFGJA * 100) + x.LFMON) as max_lfgja_lfmon from mbew_no_bwtar x
GROUP BY x.MATNR,x.BWKEY;

DROP TABLE IF EXISTS tmp_pf_mnb;
CREATE TABLE tmp_pf_mnb AS
SELECT m.*
FROM mbew_no_bwtar m,tmp_pf_mnb1 x
WHERE  x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = x.max_lfgja_lfmon;

  UPDATE fact_purchase f
  SET amt_PlannedPrice1 = MBEW_ZPLP1/PEINH
  FROM   tmp_pf_mnb m, fact_purchase f,
         dim_plant pl,
         dim_part prt
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY;

  UPDATE fact_purchase f
  SET amt_PlannedPrice1 = 0
  FROM   tmp_pf_mnb m, fact_purchase f,
         dim_plant pl,
         dim_part prt
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
	AND amt_PlannedPrice1 IS NULL;
	
	
  UPDATE fact_purchase f
  SET amt_PlannedPrice = MBEW_ZPLPR/PEINH
  FROM   tmp_pf_mnb m, fact_purchase f,
         dim_plant pl,
         dim_part prt
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY;		
									   
  UPDATE fact_purchase f
  SET amt_PlannedPrice = 0
  FROM   tmp_pf_mnb m, fact_purchase f,
         dim_plant pl,
         dim_part prt
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
	AND amt_PlannedPrice is null;	

/*   Begin 14 Nov 2013 */
UPDATE fact_purchase fpr
SET Dim_ValuationClassId = 1
FROM     ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ValuationClassId <> 1;

/*UPDATE fact_purchase fpr
FROM   ekko_ekpo_eket e,
         Dim_ValuationClass dvc,
         tmp_pf_mnb m,
         dim_plant pl,
         dim_part prt
SET Dim_ValuationClassId = dvc.Dim_ValuationClassId
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND		fpr.Dim_PlantidOrdering = pl.dim_plantid
AND		m.MATNR = EKPO_MATNR
AND		m.BWKEY = pl.ValuationArea
AND     dvc.ValuationClass  = m.MBEW_BKLAS
AND 	dvc.rowiscurrent = 1
AND fpr.Dim_ValuationClassId <> dvc.Dim_ValuationClassId*/
		
/*   End 14 Nov 2013 */

	UPDATE fact_purchase fpr
	SET dim_profitcenterid = ifnull(pc.Dim_ProfitCenterid, 1)
	FROM fact_purchase fpr
			inner join ekko_ekpo_eket dm_di_ds on fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
			left join tmp_dim_pc_purchasing_fact pc on pc.ProfitCenterCode = dm_di_ds.EKPO_KO_PRCTR;

		/* original 		
		UPDATE fact_purchase fpr
		FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
		SET     dim_profitcenterid = ifnull((SELECT pc.Dim_ProfitCenterid
										FROM tmp_dim_pc_purchasing_fact pc
										WHERE  pc.ProfitCenterCode = EKPO_KO_PRCTR ),1)	
		WHERE     pl.PlantCode = EKPO_WERKS
		 AND dc.companycode = EKPO_BUKRS
		 AND fpr.dd_DocumentNo = EKPO_EBELN
		 AND fpr.dd_DocumentItemNo = EKPO_EBELP
		 AND fpr.dd_ScheduleNo = EKET_ETENR */

/* Dim_DateIdVendorConfirmation */
merge into fact_purchase fact
using (select t2.fact_purchaseid, ifnull(dt.Dim_Dateid, 1) as Dim_DateIdVendorConfirmation
	   from (select fact_purchaseid, EKES_EINDT, dp.CompanyCode
			 from fact_purchase f
					inner join (select t1.*, 
									   row_number() over (partition by t1.EKPO_EBELN, t1.EKPO_EBELP order by t1.EKES_ETENS desc) rno
								from ekko_ekpo_ekes t1
								) e on    f.dd_DocumentNo = e.EKPO_EBELN
									  AND f.dd_DocumentItemNo = e.EKPO_EBELP
					inner join dim_plant dp on f.Dim_PlantidOrdering = dp.Dim_PlantId
			 where e.rno = 1 ) t2
		  left join dim_date dt on    dt.DateValue   = t2.EKES_EINDT
						          and dt.CompanyCode = t2.CompanyCode) src
on fact.fact_purchaseid = src.fact_purchaseid
when matched then update set fact.Dim_DateIdVendorConfirmation = src.Dim_DateIdVendorConfirmation
where fact.Dim_DateIdVendorConfirmation <> src.Dim_DateIdVendorConfirmation;

/* Creation Indicator Vendor COnfirmation - Marius 12 Sept 2016 */

merge into fact_purchase fact
using (select fact_purchaseid, EKES_ESTKZ
			 from fact_purchase f
					inner join (select t1.*,
									   row_number() over (partition by t1.EKPO_EBELN, t1.EKPO_EBELP order by t1.EKES_ETENS desc) rno
								from ekko_ekpo_ekes t1
								) e on    f.dd_DocumentNo = e.EKPO_EBELN
									  AND f.dd_DocumentItemNo = e.EKPO_EBELP
			 where e.rno = 1) src
on fact.fact_purchaseid = src.fact_purchaseid
when matched then update set fact.dd_creationindvendorconf = src.EKES_ESTKZ
where fact.dd_creationindvendorconf <> src.EKES_ESTKZ;

/* Confrimation Category - Marius 12 Sept 2016 */

merge into fact_purchase fact
using (select fact_purchaseid, EKES_EBTYP
			 from fact_purchase f
					inner join (select t1.*,
									   row_number() over (partition by t1.EKPO_EBELN, t1.EKPO_EBELP order by t1.EKES_ETENS desc) rno
								from ekko_ekpo_ekes t1
								) e on    f.dd_DocumentNo = e.EKPO_EBELN
									  AND f.dd_DocumentItemNo = e.EKPO_EBELP
			 where e.rno = 1) src
on fact.fact_purchaseid = src.fact_purchaseid
when matched then update set fact.dd_confirmationcateg = src.EKES_EBTYP
where fact.dd_confirmationcateg <> src.EKES_EBTYP;

				/* original script
				drop table if exists tmp_ekko_ekpo_ekes_purchasing_fact
				create table tmp_ekko_ekpo_ekes_purchasing_fact 
				as 
				select distinct e.*,dt.CompanyCode, dt.Dim_Dateid
				from ekko_ekpo_ekes e, dim_date dt
				WHERE     e.EKES_EINDT = dt.DateValue
					  AND e.EKES_LOEKZ IS NULL
				order by EKES_ETENS desc

				Note that ifnull(...,same column) was used here to  handle the discrepancy in mysql where it does nothing if the row is not found 
			UPDATE    fact_purchase f
			   SET f.Dim_DateIdVendorConfirmation =
					  ifnull((SELECT e.Dim_Dateid
						 FROM tmp_ekko_ekpo_ekes_purchasing_fact e
						WHERE     f.dd_DocumentNo = e.EKPO_EBELN
							  AND f.dd_DocumentItemNo = e.EKPO_EBELP
							  AND e.CompanyCode = dp.CompanyCode
							  AND e.EKES_LOEKZ IS NULL),f.Dim_DateIdVendorConfirmation)
			FROM dim_plant dp, fact_purchase f
			WHERE f.Dim_PlantidOrdering = dp.Dim_PlantId */

DROP TABLE IF EXISTS tmp_rbkp_rseg_latest_doc;
CREATE TABLE tmp_rbkp_rseg_latest_doc
AS
SELECT rbs.RSEG_EBELN,rbs.RSEG_EBELP,FIRST_VALUE(RBKP_BELNR) OVER(PARTITION BY rbs.RSEG_EBELN,rbs.RSEG_EBELP ORDER BY rbkp_bldat DESC ) RBKP_BELNR
FROM rbkp_rseg rbs;

DROP TABLE IF EXISTS tmp_rbkp_rseg_latest_doc_item;
CREATE TABLE tmp_rbkp_rseg_latest_doc_item
AS
SELECT rbs.RSEG_EBELN,rbs.RSEG_EBELP,rbs.RBKP_BELNR,max(item.rseg_buzei) rseg_buzei
FROM tmp_rbkp_rseg_latest_doc rbs,rbkp_rseg item
WHERE rbs.RSEG_EBELN = item.RSEG_EBELN AND rbs.RSEG_EBELP = item.RSEG_EBELP AND rbs.RBKP_BELNR = item.RBKP_BELNR
group by rbs.RSEG_EBELN,rbs.RSEG_EBELP,rbs.RBKP_BELNR;

DROP TABLE IF EXISTS tmp_upd_rbkp_rseg;
CREATE TABLE tmp_upd_rbkp_rseg
AS
SELECT DISTINCT rbs.*
FROM rbkp_rseg rbs, tmp_rbkp_rseg_latest_doc_item t
WHERE rbs.RSEG_EBELN = t.RSEG_EBELN AND rbs.RSEG_EBELP = t.RSEG_EBELP AND rbs.RBKP_BELNR = t.RBKP_BELNR AND rbs.rseg_buzei = t.rseg_buzei;

/* LK : NOTE : This query ( the next 5 querries which were 1 single query in mysql ) is updating more rows than there are in fp. Which means join returns dups */
/* Update  f.dd_InvoiceNumber */
UPDATE    fact_purchase f
SET f.dd_InvoiceNumber = ifnull(RBKP_BELNR,'Not ')
FROM  tmp_upd_rbkp_rseg rbs, fact_purchase f
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND  ifnull(f.dd_InvoiceNumber,'xx') <> ifnull(RBKP_BELNR,'yy');

/* Update f.dd_InvoiceItemNo */
UPDATE    fact_purchase f
SET f.dd_InvoiceItemNo = ifnull(RSEG_BUZEI,0)
FROM  tmp_upd_rbkp_rseg rbs, fact_purchase f
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.dd_InvoiceItemNo,-1) <> ifnull(RSEG_BUZEI,0);


/* Update f.amt_GrossInvAmt */
 UPDATE    fact_purchase f
SET f.amt_GrossInvAmt = ifnull(RBKP_RMWWR,0)
FROM  tmp_upd_rbkp_rseg rbs, fact_purchase f
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.amt_GrossInvAmt,-1) <> ifnull(RBKP_RMWWR,0);

/* Update f.ct_InvQtyinPOUnit */
 UPDATE    fact_purchase f
SET f.ct_InvQtyinPOUnit = ifnull(RSEG_BPMNG,0)
FROM  tmp_upd_rbkp_rseg rbs, fact_purchase f
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.ct_InvQtyinPOUnit,-1) <> ifnull(RSEG_BPMNG,0);

/* Update f.ct_VenInvQtyinPOUnit */
 UPDATE    fact_purchase f
SET f.ct_VenInvQtyinPOUnit = ifnull(RSEG_BPRBM,0)
FROM  tmp_upd_rbkp_rseg rbs, fact_purchase f
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.ct_VenInvQtyinPOUnit,-1) <> ifnull(RSEG_BPRBM,0);


/* LK : Looks like the joins are returning dups in this query as well ( for the next 2 queries ) */
 UPDATE    fact_purchase f
SET f.Dim_InvoicePOUOMId = uom.Dim_UnitOfMeasureId
 FROM tmp_upd_rbkp_rseg rbs,dim_unitofmeasure uom, fact_purchase f
 WHERE rbs.RSEG_EBELN = f.dd_DocumentNo
 AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
 AND uom.UOM = RSEG_BPRME AND uom.RowIsCurrent = 1
 AND f.Dim_InvoicePOUOMId = ifnull(uom.Dim_UnitOfMeasureId,-1);


 UPDATE    fact_purchase f
  SET f.Dim_DateIdInvoiceRcvdDate = dt.dim_dateid
 FROM tmp_upd_rbkp_rseg rbs,dim_date dt, fact_purchase f
 WHERE rbs.RSEG_EBELN = f.dd_DocumentNo
 AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
 AND dt.datevalue = rbs.RBKP_BLDAT AND dt.CompanyCode = rbs.RSEG_BUKRS
 AND ifnull(f.Dim_DateIdInvoiceRcvdDate,-1) <> ifnull(dt.dim_dateid,-2); 

	   
	   
UPDATE    fact_purchase p
SET p.Dim_CustomPartnerFunctionId1 = ifnull(dim_vendorid,1)
FROM fact_purchase p
	inner join EKKO_EKPO_EKET e on    p.dd_DocumentNo = e.EKPO_EBELN
								  AND p.dd_DocumentItemNo = e.EKPO_EBELP
								  AND p.dd_scheduleNo = e.EKET_ETENR		
	inner join EKPA e1 on    e.EKPO_EBELN = e1.EKPA_EBELN
						 AND e.EKKO_EKORG = e1.EKPA_EKORG
	inner join tmp_var_purchasing_fact t1row on t1row.pCustomPartnerFunction1 = e1.EKPA_PARVW
	left join dim_vendor v on v.VendorNumber = e1.EKPA_LIFN2;
			
UPDATE    fact_purchase p
SET p.Dim_CustomPartnerFunctionId2 = ifnull(dim_vendorid,1)
FROM fact_purchase p
	inner join EKKO_EKPO_EKET e on    p.dd_DocumentNo = e.EKPO_EBELN
								  AND p.dd_DocumentItemNo = e.EKPO_EBELP
								  AND p.dd_scheduleNo = e.EKET_ETENR		
	inner join EKPA e1 on    e.EKPO_EBELN = e1.EKPA_EBELN
						 AND e.EKKO_EKORG = e1.EKPA_EKORG
	inner join tmp_var_purchasing_fact t1row on t1row.pCustomPartnerFunction2 = e1.EKPA_PARVW
	left join dim_vendor v on v.VendorNumber = e1.EKPA_LIFN2;
	
UPDATE    fact_purchase p
SET p.Dim_CustomPartnerFunctionId3 = ifnull(dim_vendorid,1)
FROM fact_purchase p
	inner join EKKO_EKPO_EKET e on    p.dd_DocumentNo = e.EKPO_EBELN
								  AND p.dd_DocumentItemNo = e.EKPO_EBELP
								  AND p.dd_scheduleNo = e.EKET_ETENR		
	inner join EKPA e1 on    e.EKPO_EBELN = e1.EKPA_EBELN
						 AND e.EKKO_EKORG = e1.EKPA_EKORG
	inner join tmp_var_purchasing_fact t1row on t1row.pCustomPartnerFunction3 = e1.EKPA_PARVW
	left join dim_vendor v on v.VendorNumber = e1.EKPA_LIFN2;
	
UPDATE    fact_purchase p
SET p.Dim_CustomPartnerFunctionId4 = ifnull(dim_vendorid,1)
FROM fact_purchase p
	inner join EKKO_EKPO_EKET e on    p.dd_DocumentNo = e.EKPO_EBELN
								  AND p.dd_DocumentItemNo = e.EKPO_EBELP
								  AND p.dd_scheduleNo = e.EKET_ETENR		
	inner join EKPA e1 on    e.EKPO_EBELN = e1.EKPA_EBELN
						 AND e.EKKO_EKORG = e1.EKPA_EKORG
	inner join tmp_var_purchasing_fact t1row on t1row.pCustomPartnerFunction4 = e1.EKPA_PARVW
	left join dim_vendor v on v.VendorNumber = e1.EKPA_LIFN2;
	
/* Update BW Hierarchy */

/* update fact_purchase f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_purchase f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid
*/

MERGE INTO fact_purchase f
USING 
(
SELECT MAX(f.dim_bwproducthierarchyid) as dim_bwproducthierarchyid ,f.dim_partid
FROM fact_purchase f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid
GROUP BY f.dim_partid
) X
ON (f.dim_partid = x.dim_partid)
when matched then update
set f.dim_bwproducthierarchyid = x.dim_bwproducthierarchyid
where f.dim_bwproducthierarchyid <> x.dim_bwproducthierarchyid;

update fact_purchase f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_purchase f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

/* MDG Part */

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_PURCH;
CREATE TABLE TMP_DIM_MDG_PARTID_PURCH as
SELECT DISTINCT pr.dim_partid, md.dim_mdg_partid, md.partnumber
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18);

UPDATE fact_purchase f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM TMP_DIM_MDG_PARTID_PURCH tmp, fact_purchase f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_PURCH;

/* End of vw_bi_purchasing_fact script. Next query is the last query */  				

drop table if exists tmp_EINE;
DROP TABLE IF EXISTS tmp_var_purchasing_fact;
drop table if exists tmp_dim_pc_purchasing_fact;
drop table if exists tmp_ekko_ekpo_ekes_purchasing_fact;
DROP TABLE IF EXISTS tmp_pur_fact_konv  ;
DROP TABLE IF EXISTS tmp_pf_mnb;
DROP TABLE IF EXISTS tmp_pf_mnb1;
DROP TABLE IF EXISTS tmp_distinct_upd_tmp_getStdPrice;
DROP TABLE IF EXISTS tmp_rbkp_rseg_latest_doc;
DROP TABLE IF EXISTS tmp_rbkp_rseg_latest_doc_item;
DROP TABLE IF EXISTS tmp_upd_rbkp_rseg;


/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate;
create table tmp_updatedate as
select max(dw_update_date) dd_updatedate from  fact_purchase;


update fact_purchase f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_purchase f, tmp_updatedate t
where f.dd_updatedate <> t.dd_updatedate;
