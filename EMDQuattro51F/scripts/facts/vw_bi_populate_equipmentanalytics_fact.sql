/* ################################################################################################################## */
/* */
/*   Script         : fact_EquipmentAnalytics */
/*   Author         : Alex D */
/*   Created On     : 12 Oct 2016 */

/* */
/*   Change History */
/*   Date            By         Version           Desc */

/* #################################################################################################################### */

DROP TABLE IF EXISTS max_holder_701;
CREATE TABLE max_holder_701
AS
SELECT ifnull(max(fact_EquipmentAnalyticsid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) maxid
FROM fact_EquipmentAnalytics;


drop table if exists tmp_fact_EquipmentAnalytics;
create table tmp_fact_EquipmentAnalytics as  
select max_holder_701.maxid + row_number() over(order by '') as fact_EquipmentAnalyticsid,
ifnull(AFRU_RUECK,0) as  dd_Completionconfnumber ,
ifnull(AFRU_RMZHL,0) as dd_Confirmationcounter  ,
ifnull(AFRU_ISM01,0) as  ct_ActivitytobeConfirmed1, 
ifnull(AFRU_ISM02,0) as ct_ActivitytobeConfirmed2,  
ifnull(AFRU_LTXA1,'Not Set') as  dd_Confirmationtext,
ifnull(AFRU_GRUND,'Not Set') as dd_ReasonforVariance,
cast (1 as bigint) as dim_plantid,  
ifnull(AFRU_VORNR, 'Not Set') as  dd_OperationActivityNumber,
ifnull(AFRU_AUFNR,'Not Set') as dd_OrderNumber,
cast('Not Set' as varchar(7)) as dd_OrderType,
cast('Not Set' as varchar(10)) as dd_Costcenterbasic, 
cast(0 as DECIMAL (18,4)) as ct_BudgetedLaborhours , 
cast(0 as DECIMAL (18,4)) as ct_BudgetedCAPACITYhours,
cast(0 as DECIMAL (18,4)) as ct_Nrindividualcapacities, 
cast(0 as DECIMAL (18,4)) as ct_Capacity, 
cast(0 as DECIMAL (18,4)) as dd_ProductionProcess, 
cast (1 as bigint) as dim_companyid, 
cast('Not Set' as varchar(8))  as dd_KeyTaskListGroup,
cast('Not Set' as varchar(22)) as dd_ObjectNumber,
cast('Not Set' as varchar(7))  as dd_PlantMaintenance,
cast (1 as bigint) as dim_Basicfinishdateid,
cast (1 as bigint) as dim_ConfirmedOrderFinishDateid,
cast (1 as bigint) as dim_Actualfinishdateid,
cast (1 as bigint) as dim_Actualstartdateid,
cast (1 as bigint) as dim_BasicStartDateid,
cast(0 as DECIMAL (18,4)) as ct_Goodsreceivedorderitem,
cast(0 as DECIMAL (18,4)) as ct_Orderitemquantity,
cast('Not Set' as varchar(18)) as dd_MaterialNumber,
cast('Not Set' as varchar(7))  as dd_MRPcontrollerorder, 
cast('Not Set' as varchar(10)) as  dd_BatchNumber,
cast('Not Set' as varchar(10)) as  dd_ProductionVersion,
cast('Not Set' as varchar(7)) as  dd_Batchstatuskey,
cast('Not Set' as varchar(7)) as dd_Orderdeliveredinfull,
cast('Not Set' as varchar(7)) as dd_Orderdeliveredontime,
cast('Not Set' as varchar(7)) as dd_Orderdelivontimeinfull,
cast('Not Set' as varchar(7)) as dd_Batchaccepted,
cast (0 as DECIMAL (18,4)) as ct_Cumulativebreaksec,
cast('Not Set' as varchar(8)) as dd_Capacityname,
cast (1 as bigint) as dim_partid,
1 as dd_updated,
ifnull(CRHD_KAPID,'Not Set') as dd_CapacityID,
cast (1 as bigint) as dim_finaluagsedecdateid,
cast ('Not Set' as varchar(40)) as dd_CostCenterDescription,
cast (1 as bigint) as dim_workcenterid,
af.AFRU_WERKS,
af.AFRU_ARBID,
cast ( 'Not Set' as varchar(7)) as dd_CostCenterCategory,
cast ( 'Not Set' as varchar(25)) as dd_CompanyName,
cast ( 'Not Set' as varchar(25)) as dd_CompanySite,
cast ( 'Not Set' as varchar(15)) as dd_Country,
cast(0 as DECIMAL (18,4)) as ct_LossesOfTime,
cast (1 as bigint) as dim_GoodsReceiptDateid,
cast('Not Set' as varchar(30)) as dd_ReasonforVarianceDescription,
cast (1 as bigint) as dim_ConfirmationPostingDateid,
cast ( 'Not Set' as varchar(10)) as dd_CostCenterGroup,
cast ( 'Not Set' as varchar(40)) dd_WorkCenterCategoryDescription,
cast ( 'Not Set' as varchar(50)) dd_WorkCenterDescription,
cast(0 as DECIMAL (18,4)) as ct_ReasonCode,
cast(0 as DECIMAL (18,4)) as ct_Resources,
cast(0 as DECIMAL (18,4)) as ct_Staff,
cast(0 as DECIMAL (18,4)) as ct_PackagingMaterial,
cast(0 as DECIMAL (18,4)) as ct_RawMaterial,
cast(0 as DECIMAL (18,4)) as ct_Utilities,
cast(0 as DECIMAL (18,4)) as ct_Quality,
cast(0 as DECIMAL (18,4)) as ct_OtherWaitingTimes,
cast(0 as DECIMAL (18,4)) as ct_OtherTypesOfDisturbances,
cast(0 as DECIMAL (18,4)) as ct_Process,
cast(0 as DECIMAL (18,4)) as ct_UnexpectedBatch,
cast(0 as DECIMAL (18,4)) as ct_UnplannedProcessChange,
cast(0 as DECIMAL (18,4)) as ct_TrialRuns,
cast(0 as DECIMAL (18,4)) as ct_AdditionalCleaning,
cast(0 as DECIMAL (18,4)) as ct_OtherTypesOfProcessDisturb,
cast(0 as DECIMAL (18,4)) as ct_Equipment,
cast(0 as DECIMAL (18,4)) as ct_EquipmentPart1,
cast(0 as DECIMAL (18,4)) as ct_EquipmentPart2,
cast(0 as DECIMAL (18,4)) as ct_EquipmentPart3,
cast(0 as DECIMAL (18,4)) as ct_EquipmentPart4,
cast(0 as DECIMAL (18,4)) as ct_EquipmentPart5,
cast(0 as DECIMAL (18,4)) as ct_EquipmentPart6,
cast(0 as DECIMAL (18,4)) as ct_EquipmentPart7,
cast(0 as DECIMAL (18,4)) as ct_EquipmentPart8,
cast(0 as DECIMAL (18,4)) as ct_OtherTypesOfEquipment,
cast(0 as DECIMAL (18,4)) as ct_BudgetedRessourcehours,
cast(0 as DECIMAL (18,4)) as ct_SetUp,
cast(0 as DECIMAL (18,4)) as ct_normalproduction,
cast(0 as DECIMAL (18,4)) as ct_yieldloss,
cast(0 as DECIMAL (18,4)) as ct_OOSBatches,
cast(0 as DECIMAL (18,4)) as ct_QualityLosses,
cast(0 as DECIMAL (18,4)) as ct_RessourceShortage,
cast(0 as DECIMAL (18,4)) as ct_processprolongation,
cast(0 as DECIMAL (18,4)) as ct_TechnicalInterruption,
cast (1 as bigint) dim_bwproducthierarchyid,
ifnull(CRHD_OBJTY ,'Not Set') as dd_ObjecttypesCIMresource,
ifnull(CRHD_OBJID,'Not Set') as dd_ObjectIDoftheresource,
ifnull(CRHD_VGWTS,'Not Set') as dd_Standardvaluekey,
ifnull(CRHD_ARBPL,'Not Set') as dd_WorkCenter,
ifnull(CRHD_VERWE,'Not Set') as dd_WorkCenterCategory,
af.AFRU_APLZL,
af.AFRU_AUFPL,
ifnull(AFRU_BUDAT,'0001-01-01') as dd_ConfirmationPostingDate,
ifnull(AFRU_STOKZ,'Not Set') as dd_CancellationFlag,
ifnull(AFRU_STZHL,0) as dd_CancellationCounter,
cast(0 as DECIMAL (18,4)) as ct_StaffedResourceHours,
cast ( 1 as bigint) as dim_snapshotdateid,
cast ( 'Not Set' as varchar(7)) as dd_capacitycategory,
ifnull(CRHD_LVORM,'Not Set') as dd_Resourcedeletionflag,
cast('Not Set' as varchar(7)) as dd_calendarid,
cast (0 as decimal (18,0)) as ct_workingdays,
cast (1 as bigint) as dim_valuationclassid,
cast (0 as decimal (18,4)) as ct_grosshoursselected,
cast (0 as decimal (18,0)) as ct_calendardays
FROM AFRU af 
right join CRHD cr on af.AFRU_ARBID = cr.CRHD_OBJID
cross join max_holder_701;




UPDATE tmp_fact_EquipmentAnalytics f
SET dd_MaterialNumber = IFNULL(AFPO_MATNR, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR;


UPDATE tmp_fact_EquipmentAnalytics f
set f.dim_plantid = d.dim_plantid
from  tmp_fact_EquipmentAnalytics f,dim_plant d, KAKO
where d.plantcode = KAKO_WERKS
and   f.dd_CapacityID = KAKO_KAPID;


UPDATE tmp_fact_EquipmentAnalytics f
SET f.dim_partid = dp.dim_partid
FROM tmp_fact_EquipmentAnalytics f,dim_part dp
WHERE dp.PartNumber = dd_MaterialNumber
  AND dp.Plant = AFRU_WERKS;


/*UPDATE tmp_fact_EquipmentAnalytics f
set f.dim_plantid = d.dim_plantid
from  tmp_fact_EquipmentAnalytics f,dim_plant d
where d.plantcode = f.AFRU_WERKS*/


UPDATE tmp_fact_EquipmentAnalytics f
SET f.dim_companyid = d.dim_companyid
FROM  tmp_fact_EquipmentAnalytics f,dim_company d, AFKO_AFPO_AUFK afk
WHERE  f.dd_OrderNumber = afk.AFKO_AUFNR
AND d.CompanyCode = afk.AUFK_BUKRS
AND d.RowIsCurrent = 1;


UPDATE tmp_fact_EquipmentAnalytics f
SET f.dim_snapshotdateid = dd.dim_dateid
FROM  tmp_fact_EquipmentAnalytics f,dim_date dd
WHERE   dd.datevalue = current_date
   AND dd.companycode = 'Not Set';

   
/*UPDATE tmp_fact_EquipmentAnalytics f
SET dd_ObjecttypesCIMresource = ifnull(CRHD_OBJTY ,'Not Set'),
    dd_ObjectIDoftheresource = ifnull(CRHD_OBJID,'Not Set'),
    dd_Standardvaluekey = ifnull(CRHD_VGWTS,'Not Set'),
	dd_WorkCenter = ifnull(CRHD_ARBPL,'Not Set'),
	dd_WorkCenterCategory =  ifnull(CRHD_VERWE,'Not Set')
FROM  tmp_fact_EquipmentAnalytics f,CRHD cr
where f.AFRU_ARBID = cr.CRHD_OBJID*/


DROP TABLE IF EXISTS TMP_CRCO;

CREATE TABLE TMP_CRCO AS
SELECT distInct CRCO_LANUM,CRCO_KOSTL,CRCO_OBJID FROM CRCO;


UPDATE tmp_fact_EquipmentAnalytics f
SET dd_Costcenterbasic = IFNULL(c.CRCO_KOSTL, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,TMP_CRCO c
WHERE c.CRCO_OBJID = f.dd_ObjectIDoftheresource 
AND c.CRCO_LANUM = '1';


UPDATE tmp_fact_EquipmentAnalytics 
set dd_capacitycategory = kako_kapar
FROM tmp_fact_EquipmentAnalytics f, KAKO kk
WHERE  f.dd_CapacityID = kk.KAKO_KAPID;


UPDATE tmp_fact_EquipmentAnalytics 
set dd_calendarid = kako_kalid
FROM tmp_fact_EquipmentAnalytics f, KAKO kk
WHERE  f.dd_CapacityID = kk.KAKO_KAPID;


/*UPDATE tmp_fact_EquipmentAnalytics f
SET ct_Cumulativebreaksec = ifnull(1.00*KAKO_PAUSE,0)/3600 * ifnull(KAKO_AZNOR,0)
FROM  tmp_fact_EquipmentAnalytics f,KAKO kk, CRHD cr
WHERE f.AFRU_ARBID = cr.CRHD_OBJID
AND cr.CRHD_KAPID = kk.KAKO_KAPID
AND kk.kako_kapar = '008'*/

UPDATE tmp_fact_EquipmentAnalytics 
SET ct_Cumulativebreaksec = ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2
WHERE dd_OperationActivityNumber <> '9999'
AND dd_CancellationFlag <> 'X'
AND dd_CancellationCounter < 1;  


UPDATE tmp_fact_EquipmentAnalytics f
SET dd_Capacityname = ifnull(KAKO_NAME,'Not Set')
FROM  tmp_fact_EquipmentAnalytics f,KAKO kk
WHERE f.dd_CapacityID = kk.KAKO_KAPID; 


/*drop table if exists tmp_ConfirmationPostingDate
create table tmp_ConfirmationPostingDate as
select max(dd_ConfirmationPostingDate) dd_ConfirmationPostingDate,dd_costcenterbasic,dd_capacityid--,dd_completionconfnumber 
from tmp_fact_EquipmentAnalytics
where dd_ConfirmationPostingDate <> '0001-01-01'
group by dd_costcenterbasic,dd_capacityid


update tmp_fact_EquipmentAnalytics f
set f.dd_ConfirmationPostingDate = t.dd_ConfirmationPostingDate
from tmp_fact_EquipmentAnalytics f,tmp_ConfirmationPostingDate t
where f.dd_costcenterbasic=t.dd_costcenterbasic
and f.dd_capacityid=t.dd_capacityid
and f.dd_ConfirmationPostingDate<>t.dd_ConfirmationPostingDate*/


drop table if exists tmp_confdate;
create table tmp_confdate as select distinct datevalue, max(dim_dateid) dim_dateid from  tmp_fact_EquipmentAnalytics f, dim_date_factory_calendar d
where dd_ConfirmationPostingDate = datevalue
group by  datevalue ;

UPDATE tmp_fact_EquipmentAnalytics f
SET dim_ConfirmationPostingDateid = dim_dateid 
FROM tmp_fact_EquipmentAnalytics f, tmp_confdate
WHERE  dd_ConfirmationPostingDate = datevalue;


drop table if exists tmp_maxcomplconfno ;
create table tmp_maxcomplconfno as
select distinct dd_CapacityID, monthyear, max(dd_Completionconfnumber) dd_Completionconfnumber
from tmp_fact_equipmentanalytics f, dim_date_factory_calendar d
where dim_ConfirmationPostingDateid = dim_dateid
group by dd_CapacityID, monthyear;


UPDATE tmp_fact_EquipmentAnalytics f
SET ct_Nrindividualcapacities = ifnull(KAKO_AZNOR,0)*24
FROM tmp_fact_EquipmentAnalytics f, KAKO kk,dim_date_factory_calendar d, tmp_maxcomplconfno t
WHERE dim_ConfirmationPostingDateid = dim_dateid
AND f.dd_CapacityID = kk.KAKO_KAPID
AND f.dd_CapacityID = t.dd_CapacityID
AND f.dd_Completionconfnumber = t.dd_Completionconfnumber
AND f.dd_Confirmationcounter = 1
AND d.monthyear = t.monthyear;



UPDATE tmp_fact_EquipmentAnalytics f
SET  ct_Capacity = ifnull (case when (KAKO_ENDZT - KAKO_BEGZT) = 0 Then
							((86400 - KAKO_PAUSE) /3600) * KAKO_AZNOR * (KAKO_NGRAD /100)
 					else ((KAKO_ENDZT - KAKO_BEGZT - KAKO_PAUSE) /3600) * KAKO_AZNOR * (KAKO_NGRAD /100) end,0)
FROM  tmp_fact_EquipmentAnalytics f,KAKO kk, tmp_maxcomplconfno t,dim_date_factory_calendar d
WHERE dim_ConfirmationPostingDateid = dim_dateid
AND f.dd_CapacityID = kk.KAKO_KAPID
AND f.dd_CapacityID = t.dd_CapacityID
AND f.dd_Completionconfnumber = t.dd_Completionconfnumber
AND f.dd_Confirmationcounter = 1
AND d.monthyear = t.monthyear;






UPDATE tmp_fact_EquipmentAnalytics f
SET dd_ProductionProcess = ifnull(AUFK_PROCNR,0) ,
    dd_KeyTaskListGroup = ifnull(AFKO_PLNNR, 'Not Set'),
    dd_OrderType = ifnull(AUFK_AUART, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR;





/*
UPDATE tmp_fact_EquipmentAnalytics f
SET dd_CostCenterDescription = IFNULL ( cskt_ltext, 'Not Set') 
FROM tmp_fact_EquipmentAnalytics f,CSKT
WHERE  cskt_kostl = dd_Costcenterbasic */

DROP TABLE IF EXISTS TMP_CSKT;
CREATE TABLE TMP_CSKT AS
select max(cskt_ltext) cskt_ltext,cskt_kostl
from CSKT
group by  cskt_kostl;

UPDATE tmp_fact_EquipmentAnalytics f
 SET dd_CostCenterDescription = IFNULL ( cskt_ltext, 'Not Set') 
 FROM tmp_fact_EquipmentAnalytics f,TMP_CSKT
 WHERE cskt_kostl = dd_Costcenterbasic;

DROP TABLE IF EXISTS TMP_CSKT;
 
/* Roxana - Add new measure - Losses of Time */

UPDATE tmp_fact_EquipmentAnalytics f
SET ct_LossesOfTime = a.AFVV_ISM01 + a.AFVV_ISM02 - ct_ActivitytobeConfirmed1 - ct_ActivitytobeConfirmed2
FROM  tmp_fact_EquipmentAnalytics f,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL
AND a.AFVC_APLZL = f.AFRU_APLZL;


/* Roxana - BI 4492 - Add measures in OEE Report */


DROP TABLE IF EXISTS TMP_CSKS_062917;
CREATE TABLE TMP_CSKS_062917 AS
SELECT max(CSKS_KOSAR) AS CSKS_KOSAR_MAX,CSKS_KOSTL
FROM  CSKS
GROUP BY CSKS_KOSTL;

 UPDATE tmp_fact_EquipmentAnalytics f
 SET dd_CostCenterCategory = CSKS_KOSAR_MAX
 FROM TMP_CSKS_062917 , tmp_fact_EquipmentAnalytics f
 WHERE CSKS_KOSTL = dd_Costcenterbasic;

DROP TABLE IF EXISTS TMP_CSKS_062917;



/*
UPDATE tmp_fact_EquipmentAnalytics f
SET dd_CostCenterCategory = IFNULL ( CSKS_KOSAR, 'Not Set') 
FROM tmp_fact_EquipmentAnalytics f,CSKS
WHERE  CSKS_KOSTL = dd_Costcenterbasic
*/



UPDATE tmp_fact_EquipmentAnalytics f
SET dd_CompanyName = IFNULL ( BUTXT, 'Not Set'), 
	dd_CompanySite = IFNULL ( ORT01, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f, T001 t, dim_company d
WHERE  t.BUKRS = d.companycode
AND f.dim_companyid = d.dim_companyid;

UPDATE tmp_fact_EquipmentAnalytics f
SET dd_Country = IFNULL ( T005T_LANDX, 'Not Set') 
FROM tmp_fact_EquipmentAnalytics f, T005T a, T001 t, dim_company d
WHERE  a.T005T_LAND1 = t.LAND1
AND t.BUKRS = d.companycode
AND	f.dim_companyid = d.dim_companyid;

/* end of BI 4492 */

UPDATE tmp_fact_EquipmentAnalytics f
SET dim_Basicfinishdateid = dim_dateid 
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk, dim_date dd1
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR
AND ifnull(AFKO_GLTRP,'0001-01-01')  = datevalue 
AND companycode = ifnull(afk.AUFK_BUKRS, 'Not Set') ;

UPDATE tmp_fact_EquipmentAnalytics f
SET dim_ConfirmedOrderFinishDateid = dim_dateid 
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk, dim_date dd1
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR
AND ifnull(AFKO_GETRI,'0001-01-01')  = datevalue 
AND companycode = ifnull(afk.AUFK_BUKRS, 'Not Set') ;

UPDATE tmp_fact_EquipmentAnalytics f
SET dim_Actualfinishdateid = dim_dateid 
FROM tmp_fact_EquipmentAnalytics f, AFKO_AFPO_AUFK afk, dim_date dd1
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR
AND ifnull(AFKO_GLTRI,'0001-01-01')  = datevalue 
AND companycode = ifnull(afk.AUFK_BUKRS, 'Not Set') ;



UPDATE tmp_fact_EquipmentAnalytics f
SET dim_Actualstartdateid = dim_dateid 
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk, dim_date dd1
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR
AND ifnull(AFKO_GSTRI,'0001-01-01')  = datevalue 
AND companycode = ifnull(afk.AUFK_BUKRS, 'Not Set') ;


/*---Final Usage decision date */


/* Roxana - change the logic for Final Usage Decision Date */

DROP TABLE IF EXISTS tmp_QALS;
CREATE TABLE tmp_QALS AS SELECT max(QALS_PRUEFLOS) AS QALS_PRUEFLOS, QALS_AUFNR FROM QALS
GROUP BY QALS_AUFNR;  

UPDATE tmp_fact_EquipmentAnalytics f
SET f.dim_finaluagsedecdateid = dd.dim_dateid 
FROM tmp_fact_EquipmentAnalytics f, tmp_QALS q, QAVE qv, dim_date dd
WHERE f.dd_OrderNumber = q.QALS_AUFNR
AND qv.QAVE_PRUEFLOS = q.QALS_PRUEFLOS
AND ifnull(qv.QAVE_VDATUM ,'0001-01-01') = dd.datevalue 
AND dd.companycode = 'Not Set'; 





UPDATE tmp_fact_EquipmentAnalytics f
SET dim_BasicStartDateid = dim_dateid 
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk, dim_date dd1
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR
AND ifnull(AFKO_GSTRP,'0001-01-01')  = datevalue 
AND companycode = ifnull(afk.AUFK_BUKRS, 'Not Set') ;

UPDATE tmp_fact_EquipmentAnalytics f
SET ct_Goodsreceivedorderitem = IFNULL(AFPO_WEMNG, 0)
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR;

UPDATE tmp_fact_EquipmentAnalytics f
SET ct_Orderitemquantity = IFNULL(AFPO_PSMNG, 0)
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR;


UPDATE tmp_fact_EquipmentAnalytics f
SET dd_MRPcontrollerorder = IFNULL(AFKO_DISPO, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR;

UPDATE tmp_fact_EquipmentAnalytics f
SET dd_BatchNumber = IFNULL(AFPO_CHARG, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR;

UPDATE tmp_fact_EquipmentAnalytics f
SET dd_ProductionVersion = IFNULL(AFPO_VERID, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,AFKO_AFPO_AUFK afk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR;


drop table if exists tmp_coslobjnr;
create table tmp_coslobjnr as select distinct COSL_OBJNR from COSL;

UPDATE tmp_fact_EquipmentAnalytics f
SET dd_ObjectNumber = ifnull(COSL_OBJNR,'Not Set')
FROM  tmp_fact_EquipmentAnalytics f,tmp_coslobjnr co, AFKO_AFPO_AUFK afk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR
AND afk.AUFK_OBJNR = co.COSL_OBJNR;


drop table if exists tmp_cosl;
create table tmp_cosl as
select distinct  CRCO_KOSTL,CRCO_OBJID,
sum(COSL_LST001+COSL_LST002+COSL_LST003+COSL_LST004+COSL_LST005+COSL_LST006+COSL_LST007+COSL_LST008+COSL_LST009+COSL_LST010+COSL_LST011+COSL_LST012+COSL_LST013+COSL_LST014+COSL_LST015+COSL_LST016) COSL_LST,
sum(COSL_LST001+COSL_LST002+COSL_LST003+COSL_LST004+COSL_LST005+COSL_LST006+COSL_LST007+COSL_LST008+COSL_LST009+COSL_LST010+COSL_LST011+COSL_LST012) COSL_LST2,
sum(COSL_KAP001+COSL_KAP002+COSL_KAP003+COSL_KAP004+COSL_KAP005+COSL_KAP006+COSL_KAP007+COSL_KAP008+COSL_KAP009+COSL_KAP010+COSL_KAP011+COSL_KAP012) COSL_KAP
from COSL, CRCO
where CRCO_KOSTL = SUBSTR( COSL_OBJNR,-16,10)
group by CRCO_KOSTL,CRCO_OBJID;


UPDATE tmp_fact_EquipmentAnalytics f
SET ct_BudgetedLaborhours = (ifnull(COSL_LST,0)/3600 ) 
FROM  tmp_fact_EquipmentAnalytics f,tmp_cosl co
WHERE CRCO_OBJID = f.AFRU_ARBID ;






drop table if exists tmp_cosl2;
create table tmp_cosl2 as
select distinct  CRCO_OBJID,COSL_OBJNR,
sum(COSL_LST001+COSL_LST002+COSL_LST003+COSL_LST004+COSL_LST005+COSL_LST006+COSL_LST007+COSL_LST008+COSL_LST009+COSL_LST010+COSL_LST011+COSL_LST012+COSL_LST013+COSL_LST014+COSL_LST015+COSL_LST016) COSL_LST,
sum(COSL_LST001+COSL_LST002+COSL_LST003+COSL_LST004+COSL_LST005+COSL_LST006+COSL_LST007+COSL_LST008+COSL_LST009+COSL_LST010+COSL_LST011+COSL_LST012) COSL_LST2,
sum(COSL_KAP001+COSL_KAP002+COSL_KAP003+COSL_KAP004+COSL_KAP005+COSL_KAP006+COSL_KAP007+COSL_KAP008+COSL_KAP009+COSL_KAP010+COSL_KAP011+COSL_KAP012) COSL_KAP
from COSL, CRCO
where CRCO_KOSTL = SUBSTR( COSL_OBJNR,-16,10)
group by CRCO_OBJID,COSL_OBJNR;


UPDATE tmp_fact_EquipmentAnalytics f
SET ct_StaffedResourceHours = (ifnull(COSL_LST2,0)/360 )
FROM  tmp_fact_EquipmentAnalytics f,tmp_cosl2 co
WHERE CRCO_OBJID = f.AFRU_ARBID 
AND dd_costcenterbasic =  SUBSTR( COSL_OBJNR,7,10) 
AND COSL_OBJNR like '%T11004'
AND COSL_OBJNR like 'KL%';



UPDATE tmp_fact_EquipmentAnalytics f
SET ct_BudgetedCAPACITYhours = (ifnull(COSL_KAP,0)/3600 ) * KAKO_AZNOR
FROM  tmp_fact_EquipmentAnalytics f,tmp_cosl2 co, AFKO_AFPO_AUFK afk,KAKO kk
WHERE f.dd_OrderNumber = afk.AFKO_AUFNR
AND afk.AUFK_OBJNR = co.COSL_OBJNR 
AND f.dd_CapacityID = kk.KAKO_KAPID;


/* Roxana - 7.08.2017 change logic for Budgeted Resource Hours */

/*UPDATE tmp_fact_EquipmentAnalytics f
SET ct_BudgetedRessourcehours = (ifnull(COSL_KAP,0)/3600)
FROM  tmp_fact_EquipmentAnalytics f,tmp_cosl co
WHERE CRCO_OBJID = f.AFRU_ARBID */  

drop table if exists tmp_cosl3;
create table tmp_cosl3 as
select distinct  
(COSL_KAP001+COSL_KAP002+COSL_KAP003+COSL_KAP004+COSL_KAP005+COSL_KAP006+COSL_KAP007+COSL_KAP008+COSL_KAP009+COSL_KAP010+COSL_KAP011+COSL_KAP012) COSL_KAP
,dd_costcenterbasic
from COSL, CRCO, tmp_fact_EquipmentAnalytics f
where CRCO_KOSTL = SUBSTR( COSL_OBJNR,-16,10) 
and CRCO_OBJID = f.AFRU_ARBID 
and COSL_GJAHR = year(current_date)
AND dd_costcenterbasic =  SUBSTR( COSL_OBJNR,7,10) 
and COSL_VERSN = '000' 
and COSL_VRGNG = 'RKP2' 
and COSL_OBJNR like '%T11004'
AND COSL_OBJNR like 'KL%';


drop table if exists tmp_countofcapacityids ;
create table tmp_countofcapacityids as
select count(distinct dd_capacityid) countofcapacityids, dd_costcenterbasic, monthyear
from tmp_fact_EquipmentAnalytics f, dim_date_factory_calendar d
where f.dim_ConfirmationPostingDateid = d.dim_dateid 
and year(datevalue) = year(current_date)
and dd_Resourcedeletionflag = 'Not Set' 
group by dd_costcenterbasic,monthyear;


drop table if exists tmp_maxcompletionconfno ;
create table tmp_maxcompletionconfno as 
select max(dd_completionconfnumber) maxcompletionconfno, dd_capacityid, dd_costcenterbasic, monthyear
from tmp_fact_EquipmentAnalytics f, dim_date_factory_calendar d
where f.dim_ConfirmationPostingDateid = d.dim_dateid 
and year(datevalue) = year(current_date)
and dd_Resourcedeletionflag = 'Not Set'
group by dd_capacityid, dd_costcenterbasic,monthyear;


UPDATE tmp_fact_EquipmentAnalytics f
SET f.ct_BudgetedRessourcehours =  ifnull(COSL_KAP,0)/12/countofcapacityids
FROM  tmp_fact_EquipmentAnalytics f, tmp_cosl3 co, tmp_maxcompletionconfno t, tmp_countofcapacityids tmp, dim_date_factory_calendar d
WHERE f.dim_ConfirmationPostingDateid = d.dim_dateid 
and f.dd_costcenterbasic = co.dd_costcenterbasic 
and f.dd_costcenterbasic = t.dd_costcenterbasic
and f.dd_capacityid = t.dd_capacityid
and f.dd_costcenterbasic = tmp.dd_costcenterbasic
and f.dd_completionconfnumber = t.maxcompletionconfno
and d.monthyear = t.monthyear
and d.monthyear = tmp.monthyear
and f.dd_Confirmationcounter = 1
and f.ct_BudgetedRessourcehours <>  ifnull(COSL_KAP,0)/12/countofcapacityids;



UPDATE tmp_fact_EquipmentAnalytics f
SET dd_PlantMaintenance = ifnull(t001w_iwerk, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,T001W t
WHERE t.werks = f.AFRU_WERKS;


	
update tmp_fact_EquipmentAnalytics f
set f.dd_Batchstatuskey = ifnull(q.AUSP_ATWRT,'Not Set') 
from tmp_fact_EquipmentAnalytics f, mch1 b, AUSP_QKZ q, INOB i
where f.dd_MaterialNumber = IFNULL(b.mch1_matnr,'Not Set')
and f.dd_BatchNumber = IFNULL(b.mch1_charg,'Not Set')
and q.AUSP_OBJEK = i.INOB_CUOBJ
and REPLACE(i.INOB_OBJEK, ' ', '') = concat (b.MCH1_MATNR,b.mch1_charg)
and f.dd_Batchstatuskey <> ifnull(q.AUSP_ATWRT,'Not Set');

update tmp_fact_EquipmentAnalytics f
set f.dd_Batchstatuskey = ifnull(q.AUSP_ATWRT,'Not Set')
from tmp_fact_EquipmentAnalytics f, mch1 b, AUSP_QKZ q
where f.dd_MaterialNumber = IFNULL(b.mch1_matnr,'Not Set')
and f.dd_BatchNumber = IFNULL(b.mch1_charg,'Not Set')
and q.AUSP_OBJEK = b.MCH1_CUOBJ_BM
and f.dd_Batchstatuskey <> ifnull(q.AUSP_ATWRT,'Not Set');
	

/* BI 4673 - Roxana- Add Goods Receipt Date to Process Yield report */

UPDATE tmp_fact_EquipmentAnalytics f
SET dim_GoodsReceiptDateid = dim_dateid 
FROM tmp_fact_EquipmentAnalytics f, MCH1 b, dim_date dd1, dim_company dc
where f.dd_MaterialNumber = IFNULL(b.mch1_matnr,'Not Set')
and f.dd_BatchNumber = IFNULL(b.mch1_charg,'Not Set')
AND ifnull(b.MCH1_LWEDT,'0001-01-01')  = dd1.datevalue 
AND f.dim_companyid = dc.dim_companyid
AND dd1.companycode = dc.companycode;


/* Roxana  - 25 Nov 2016 - Add New Measures */

UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_OOSBatches = ifnull((a.AFVV_ISM01 + a.AFVV_ISM02),0)
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND (dd_Batchstatuskey = '1' or dd_Batchstatuskey = '2' or dd_Batchstatuskey = '3' or dd_Batchstatuskey = '4'
or dd_Batchstatuskey = '5' or dd_Batchstatuskey = '6' or dd_Batchstatuskey = '7' or dd_Batchstatuskey = '8');


drop table if exists tmp_JEST;
create table tmp_JEST as select distinct JEST_OBJNR from JEST_INSP where JEST_STAT in( 'I0009', 'I0012');

UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_yieldloss = ifnull((AFPO_PSMNG - AFPO_WEMNG) / AFPO_PSMNG * AFVV_ISM02,0) 
FROM  tmp_fact_EquipmentAnalytics f ,AFKO_AFPO_AUFK afk,  tmp_JEST j, AFVC_AFVV a
WHERE  dd_Standardvaluekey LIKE 'E%'
AND JEST_OBJNR = concat ('OR', AFKO_AUFNR)
AND f.dd_OrderNumber = afk.AFKO_AUFNR
AND a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL;  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_setup = ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2
FROM tmp_fact_EquipmentAnalytics f, AFVC_AFVV a
WHERE  a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_OrderType <> 'RW01'
AND f.dd_OperationActivityNumber <> '9999'
AND a.AFVC_STEUS = 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_normalproduction = ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2
FROM tmp_fact_EquipmentAnalytics f,AFVC_AFVV a
WHERE  a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND AFVC_STEUS <> 'ZSET'
AND f.dd_OrderType <> 'RW01'
AND f.dd_OperationActivityNumber <> '9999';


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_qualitylosses = ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2
WHERE f.dd_OrderType = 'RW01'
AND f.dd_OperationActivityNumber <> '9999'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1';  



UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_processprolongation = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND dd_ReasonforVariance like '2%'
AND a.AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_ressourceshortage =  case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND dd_ReasonforVariance like '3%'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_technicalinterruption = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND dd_ReasonforVariance like '1%'
AND a.AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_ReasonCode = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND dd_ReasonforVariance not like '1%' and dd_ReasonforVariance not like '2%' and dd_ReasonforVariance not like '3%'
AND AFVC_STEUS <> 'ZSET' 
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_Resources = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '3000'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_Staff = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '3100'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_RawMaterial = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '3200'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_PackagingMaterial = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '3300'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_Utilities = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '3400'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_Quality = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '3500'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 
 

UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_OtherWaitingTimes = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '3600'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_OtherTypesOfDisturbances = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '3900'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_Process = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '2000'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_UnexpectedBatch = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '2100'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_UnplannedProcessChange =  case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '2200'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_TrialRuns = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '2300'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_AdditionalCleaning = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '2400'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_OtherTypesOfProcessDisturb = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '2900'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_Equipment = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1000'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_EquipmentPart1 = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1100'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f
SET ct_EquipmentPart2 = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1200'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_EquipmentPart3 = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1300'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_EquipmentPart4 = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1400'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_EquipmentPart5 = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1500'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01';  


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_EquipmentPart6 = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1600'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_EquipmentPart7 = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1700'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_EquipmentPart8 = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1800'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics f 
SET ct_OtherTypesOfEquipment = case when (ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) = 0 then 0
		else ((ct_ActivitytobeConfirmed1 + ct_ActivitytobeConfirmed2) - ifnull((AFVV_VGW01 + AFVV_VGW02),0)) end
FROM tmp_fact_EquipmentAnalytics f ,AFVC_AFVV a
WHERE a.AFVC_AUFPL = f.AFRU_AUFPL 
AND a.AFVC_APLZL = f.AFRU_APLZL 
AND f.dd_ReasonforVariance = '1900'
AND AFVC_STEUS <> 'ZSET'
AND f.dd_CancellationFlag <> 'X'
AND f.dd_CancellationCounter < '1'
AND f.dd_OrderType <> 'RW01'; 


UPDATE tmp_fact_EquipmentAnalytics
SET dd_Orderdeliveredinfull = case when ct_orderitemquantity = 0 then 'No' 
							       when ct_goodsreceivedorderitem/ct_orderitemquantity >= 1 
									then 'Yes' 
									else 'No'
								end;

								
UPDATE tmp_fact_EquipmentAnalytics	
SET dd_Orderdeliveredontime = case when dd2.DateValue = '0001-01-01' then 'No'
                                   else case when dd2.DateValue <= dd1.DateValue then 'Yes'
else 'No'
   end 
       end
FROM tmp_fact_EquipmentAnalytics,dim_date dd1, dim_date dd2
WHERE dd1.dim_dateid = dim_basicfinishdateid
AND   dd2.dim_dateid = dim_actualfinishdateid;



	
UPDATE tmp_fact_EquipmentAnalytics		
SET dd_Batchaccepted = case when dd_batchstatuskey = '0'  then 'Yes'
                            when dd_batchstatuskey = 'Not Set' then 'Not Set'
                           else 'No'
                       end;
	
UPDATE tmp_fact_EquipmentAnalytics								
SET dd_Orderdelivontimeinfull = case when 	dd_Orderdeliveredontime = 'Yes' 
                                         and dd_Orderdeliveredinfull = 'Yes'	
										 and dd_Batchaccepted = 'Yes'
									then 'Yes' 
                                    else 'No'
                                end;


  
UPDATE tmp_fact_EquipmentAnalytics f
SET dd_ReasonforVarianceDescription = ifnull (TRUGT_GRDTX, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,TRUGT t
WHERE t.TRUGT_GRUND = f.dd_ReasonforVariance 
	AND t.TRUGT_WERKS = f.AFRU_WERKS;


/*UPDATE tmp_fact_EquipmentAnalytics f
SET dim_ConfirmationPostingDateid = dim_dateid 
FROM tmp_fact_EquipmentAnalytics f, dim_date dd1,dim_company d
WHERE ifnull(AFRU_BUDAT,'0001-01-01') = datevalue
	AND  f.dd_CompanyName = d.CompanyName
	and dd1.companycode = d.companycode*/


UPDATE tmp_fact_EquipmentAnalytics f
SET dd_CostCenterGroup = CONCAT( left(dd_costcenterbasic,8),'00')
WHERE dd_costcenterbasic <> 'Not Set';


drop table if exists tmp_CRTX;
create table tmp_CRTX as select distinct CRTX_KTEXT,CRTX_OBJID from CRTX;

UPDATE tmp_fact_EquipmentAnalytics f
SET dd_WorkCenterDescription = ifnull(c.CRTX_KTEXT,'Not Set')
FROM tmp_fact_EquipmentAnalytics f, tmp_CRTX c
WHERE ifnull(c.CRTX_OBJID,1) = dd_ObjectIDoftheresource;

/*MERGE INTO tmp_fact_EquipmentAnalytics f
 USING (SELECT f.dd_ObjectIDoftheresource, MAX(ifnull(c.CRTX_KTEXT,'Not Set')) as CRTX_KTEXT, ifnull(c.CRTX_OBJID,1) CRTX_OBJID
		FROM tmp_fact_EquipmentAnalytics f, tmp_CRTX c
		where f.dd_ObjectIDoftheresource = ifnull(c.CRTX_OBJID,1)
		GROUP BY f.dd_ObjectIDoftheresource, c.CRTX_KTEXT, c.CRTX_OBJID) x
ON (f.dd_ObjectIDoftheresource = x.dd_ObjectIDoftheresource)
WHEN MATCHED THEN
UPDATE SET f.dd_WorkCenterDescription = x.CRTX_KTEXT
WHERE f.dd_ObjectIDoftheresource <> x.CRTX_OBJID


DROP TABLE IF EXISTS tmp_fact_EquipmentAnalytics_maxKTEXT_05092017

CREATE TABLE tmp_fact_EquipmentAnalytics_maxKTEXT_05092017
as
select CRTX_OBJID,max(CRTX_KTEXT) as CRTX_KTEXT_MAX
from tmp_CRTX
group by CRTX_OBJID

UPDATE tmp_fact_EquipmentAnalytics f
SET dd_WorkCenterDescription = ifnull(c.CRTX_KTEXT_MAX,'Not Set')
FROM tmp_fact_EquipmentAnalytics f, tmp_fact_EquipmentAnalytics_maxKTEXT_05092017 C
WHERE ifnull(c.CRTX_OBJID,1) = dd_ObjectIDoftheresource

DROP TABLE IF EXISTS tmp_fact_EquipmentAnalytics_maxKTEXT_05092017*/ 


UPDATE tmp_fact_EquipmentAnalytics f
SET dd_WorkCenterCategoryDescription = ifnull(t.TC30T_KTEXT, 'Not Set')
FROM tmp_fact_EquipmentAnalytics f,TC30T t
WHERE ifnull(t.TC30T_VERWE,1) = dd_WorkCenterCategory;


UPDATE tmp_fact_EquipmentAnalytics f
SET f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
FROM  tmp_fact_EquipmentAnalytics f,dim_part dp ,dim_bwproducthierarchy bw
WHERE f.dd_MaterialNumber = dp.partnumber
and f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate;



/*update tmp_fact_EquipmentAnalytics 
set dd_Resourcedeletionflag = ifnull(CRHD_LVORM,'Not Set')
from tmp_fact_EquipmentAnalytics f, CRHD cr
where f.AFRU_ARBID = cr.CRHD_OBJID*/


/* Roxana - 31st of July - Add Valuation class in Equipment Analytics */


DROP TABLE IF EXISTS TMP_BKLAS;
CREATE TABLE TMP_BKLAS AS 
SELECT DISTINCT MBEW_MATNR, MBEW_BWKEY, MBEW_BKLAS from MARA_MARC_MBEW mm where 
MARA_LAEDA = (SELECT max(MARA_LAEDA) from MARA_MARC_MBEW h
              WHERE mm.MBEW_MATNR = h.MBEW_MATNR
                AND mm.MBEW_BWKEY = h.MBEW_BWKEY)
GROUP BY MBEW_MATNR, MBEW_BWKEY,MBEW_BKLAS;


UPDATE tmp_fact_EquipmentAnalytics f
SET  f.dim_ValuationClassid = dim.dim_ValuationClassid 
FROM dim_ValuationClass dim, tmp_fact_EquipmentAnalytics f, dim_part dp ,TMP_BKLAS bk,dim_plant p
WHERE f.dim_partid = dp.dim_partid
AND dp.PartNumber = bk.MBEW_MATNR
AND dim.VALUATIONCLASS = bk.MBEW_BKLAS
AND bk.MBEW_BWKEY = p.ValuationArea
AND p.dim_Plantid = f.dim_Plantid
AND ifnull(f.dim_ValuationClassid,-1) <> ifnull(dim.dim_ValuationClassid,1) ;


/* Roxana - 3rd of August - Calculate no of working days */


/*drop table if exists tmp_workingdays
create table tmp_workingdays as
select distinct max(dd_Completionconfnumber) dd_Completionconfnumber, dd_capacityid, datevalue, calendaryear,CalendarMonthNumber,DayofMonth, companycode,dd_calendarid,cast (0 as bigint) as workingdays
from dim_date d,tmp_fact_equipmentanalytics f
where f.dim_actualstartdateid = d.dim_dateid 
group by dd_capacityid, datevalue, calendaryear,CalendarMonthNumber,DayofMonth, companycode,dd_calendarid

update tmp_workingdays 
set  workingdays = CASE SUBSTRING(case a.CalendarMonthNumber
                                              when 1 then t.TFACS_MON01
                                              when 2 then t.TFACS_MON02
                                              when 3 then t.TFACS_MON03
                                              when 4 then t.TFACS_MON04
                                              when 5 then t.TFACS_MON05
                                              when 6 then t.TFACS_MON06
                                              when 7 then t.TFACS_MON07
                                              when 8 then t.TFACS_MON08
                                              when 9 then t.TFACS_MON09
                                              when 10 then t.TFACS_MON10
                                              when 11 then t.TFACS_MON11
                                              when 12 then t.TFACS_MON12
                                          end, a.DayofMonth, 1) WHEN '0' then 0 else 1 END 
From TFACS t ,tmp_workingdays a
Where  t.TFACS_JAHR = calendaryear
and TFACS_IDENT = dd_calendarid



update tmp_fact_equipmentanalytics f
set ct_workingdays = workingdays 
from tmp_fact_equipmentanalytics f,tmp_workingdays t, dim_date d
where f.dim_actualstartdateid = d.dim_dateid
and t.dd_Completionconfnumber = f.dd_Completionconfnumber
and t.dd_Capacityid = f.dd_Capacityid
and f.dd_calendarid =  t.dd_calendarid
and t.companycode = d.companycode
and t.datevalue = d.datevalue*/

drop table if exists tmp_workingdays;
create table tmp_workingdays as
select distinct monthyear,cast(0 as decimal (18,0)) as noofworkingdays, dim_dateid
from dim_date where year(datevalue) = '2017';


update tmp_workingdays
set noofworkingdays = case when month(datevalue) = 1 then 22 
when month(datevalue) = 2 then 20 
when month(datevalue) = 3 then 22 
when month(datevalue) = 4 then 20 
when month(datevalue) = 5 then 21 
when month(datevalue) = 6 then 21 
when month(datevalue) = 7 then 21 
when month(datevalue) = 8 then 22 
when month(datevalue) = 9 then 21 
when month(datevalue) = 10 then 21 
when month(datevalue) = 11 then 22 
when month(datevalue) = 12 then 20 
end
from dim_date d, tmp_workingdays t
where d.dim_dateid = t.dim_Dateid;


drop table if exists tmp_workingdays2;
create table tmp_workingdays2 as 
select max(noofworkingdays) noofworkingdays, monthyear,plantcode,
	   max(fact_equipmentanalyticsid) fact_equipmentanalyticsid
from tmp_fact_equipmentanalytics f,tmp_workingdays t, dim_plant d
where f.dim_ConfirmationPostingDateid = t.dim_dateid
and f.dim_plantid = d.dim_plantid
and d.dim_plantid in (select distinct dim_plantid from dim_plant where countryname = 'South Korea') 
and dim_bwproducthierarchyid <> 1
and (ct_Nrindividualcapacities <> 0 or ct_Capacity <> 0)
and dd_Resourcedeletionflag = 'Not Set'
and dd_workcenter like 'E%'
and dd_workcenter not in ('M%','L%','Q%','EM-%','EM0KRW02 - M30 Washing Machine%','EM0KRW01 - Jumbo CIP/SDS%',
	'EF0KRW01 - Jumbo bottle Washing machine%','EF0KRW02 - M30 bottle Washing machine%')
group by monthyear,plantcode;


update tmp_fact_equipmentanalytics f
set ct_workingdays = t2.noofworkingdays 
from tmp_fact_equipmentanalytics f,tmp_workingdays2 t2
where f.fact_equipmentanalyticsid = t2.fact_equipmentanalyticsid;


/*drop table if exists tmp_grosssselected
create table tmp_grosssselected as 
select max(ct_workingdays) ct_workingdays,sum(ct_Nrindividualcapacities) ct_Nrindividualcapacities, 
	monthyear, max(fact_equipmentanalyticsid) fact_equipmentanalyticsid
from tmp_fact_equipmentanalytics f, dim_date d, tmp_workingdays2 t
where f.dim_ConfirmationPostingDateid = d.dim_dateid 
and f.fact_equipmentanalyticsid = t.fact_equipmentanalyticsid
and year(datevalue) = '2017'
group by monthyear


update tmp_fact_equipmentanalytics
set ct_grossselected = t.ct_Nrindividualcapacities * t.ct_workingdays
from tmp_fact_equipmentanalytics f, tmp_grosssselected t
where f.fact_equipmentanalyticsid = t.fact_equipmentanalyticsid */

/* end of changes */


drop table if exists tmp_calendaristicdays;
create table tmp_calendaristicdays as
select case when count( distinct cpdt.MonthYear) =1 then 
case when max(month(cpdt.datevalue)) = max(month(current_date)) 
then max(day( current_date ) ) 
else max(day( (cpdt.DateValue) )) end 
else case when count( distinct cpdt.MonthYear) >1 and max(month(cpdt.datevalue)) = max(month(current_date)) 
then (max(day( (cpdt.DateValue) )) * (count( distinct cpdt.MonthYear)-1) + day(current_date)) 
else (max(day( (cpdt.DateValue) )) * count( distinct cpdt.MonthYear)) end 
end as calendaristicdays, cpdt.MonthYear
from tmp_fact_equipmentanalytics f, dim_date cpdt
where f.dim_ConfirmationPostingDateid = cpdt.dim_dateid
and year(datevalue) = '2017'
group by cpdt.MonthYear;


update tmp_fact_equipmentanalytics
set ct_calendardays = calendaristicdays
from tmp_fact_equipmentanalytics f, dim_date cpdt,tmp_calendaristicdays t
where f.dim_ConfirmationPostingDateid = cpdt.dim_dateid
and cpdt.monthyear=t.monthyear;


drop table if exists tmp_grosshoursselected;
create table tmp_grosshoursselected as 
select (ct_Nrindividualcapacities) * max(ct_calendardays) as grosshoursselected,cpdt.monthyear
,fact_equipmentanalyticsid
from tmp_fact_equipmentanalytics f, dim_date cpdt
where f.dim_ConfirmationPostingDateid = cpdt.dim_dateid
group by cpdt.monthyear,ct_Nrindividualcapacities,fact_equipmentanalyticsid;


update tmp_fact_equipmentanalytics f
set ct_grosshoursselected = grosshoursselected
from tmp_fact_equipmentanalytics f, dim_date cpdt, tmp_grosshoursselected t
where f.dim_ConfirmationPostingDateid = cpdt.dim_dateid
and f.fact_equipmentanalyticsid = t.fact_equipmentanalyticsid
and cpdt.monthyear = t.monthyear;

/* Insert in fact */

 
delete from fact_EquipmentAnalytics f;


insert into fact_EquipmentAnalytics
(
fact_EquipmentAnalyticsid,
dim_GoodsReceiptDateid,
ct_ActivitytobeConfirmed1,
ct_ActivitytobeConfirmed2,
ct_AdditionalCleaning,
ct_BudgetedCAPACITYhours,
ct_BudgetedLaborhours,
ct_BudgetedRessourcehours,
ct_Capacity,
ct_Cumulativebreaksec,
ct_Equipment,
ct_Equipmentpart1,
ct_Equipmentpart2,
ct_Equipmentpart3,
ct_Equipmentpart4,
ct_Equipmentpart5,
ct_Equipmentpart6,
ct_Equipmentpart7,
ct_Equipmentpart8,
ct_Goodsreceivedorderitem,
ct_LossesOfTime,
ct_normalproduction,
ct_Nrindividualcapacities,
ct_OOSBatches,
ct_Orderitemquantity,
ct_OtherTypesOfDisturbances,
ct_OtherTypesOfEquipment,
ct_OtherTypesOfProcessDisturb,
ct_OtherWaitingTimes,
ct_PackagingMaterial,
ct_Process,
ct_processprolongation,
ct_Quality,
ct_QualityLosses,
ct_RawMaterial,
ct_ReasonCode,
ct_Resources,
ct_RessourceShortage,
ct_SetUp,
ct_Staff,
ct_StaffedResourceHours,
ct_TechnicalInterruption,
ct_TrialRuns,
ct_UnexpectedBatch,
ct_UnplannedProcessChange,
ct_Utilities,
ct_yieldloss,
dd_Batchaccepted,
dd_BatchNumber,
dd_Batchstatuskey,
dd_CancellationCounter,
dd_CancellationFlag,
dd_capacitycategory,
dd_CapacityID,
dd_Capacityname,
dd_CompanyName,
dd_CompanySite,
dd_Completionconfnumber,
dd_Confirmationcounter,
dd_ConfirmationPostingDate,
dd_Confirmationtext,
dd_Costcenterbasic,
dd_CostCenterCategory,
dd_CostCenterDescription,
dd_CostCenterGroup,
dd_country,
dd_KeyTaskListGroup,
dd_MaterialNumber,
dd_MRPcontrollerorder,
dd_ObjectIDoftheresource,
dd_ObjectNumber,
dd_ObjecttypesCIMresource,
dd_OperationActivityNumber,
dd_Orderdeliveredinfull,
dd_Orderdeliveredontime,
dd_Orderdelivontimeinfull,
dd_OrderNumber,
dd_OrderType,
dd_PlantMaintenance,
dd_ProductionProcess,
dd_ProductionVersion,
dd_ReasonforVariance,
dd_ReasonforVarianceDescription,
dd_Resourcedeletionflag,
dd_Standardvaluekey,
dd_WorkCenter,
dd_WorkCenterCategory,
dd_WorkCenterCategoryDescription,
dd_WorkCenterDescription,
dim_Actualfinishdateid,
dim_Actualstartdateid,
dim_Basicfinishdateid,
dim_BasicStartDateid,
dim_bwproducthierarchyid,
dim_companyid,
dim_ConfirmationPostingDateid,
dim_ConfirmedOrderFinishDateid,
dim_finaluagsedecdateid,
dim_partid,
dim_plantid,
dim_snapshotdateid,
dim_workcenterid,
dd_calendarid,
ct_workingdays,
dim_valuationclassid,
ct_grosshoursselected,
ct_calendardays
)
select 
fact_EquipmentAnalyticsid,
cast (1 as bigint) as dim_GoodsReceiptDateid,
ifnull(ct_ActivitytobeConfirmed1,0) ct_ActivitytobeConfirmed1,
ifnull(ct_ActivitytobeConfirmed2,0) ct_ActivitytobeConfirmed2,
ifnull(ct_AdditionalCleaning,0) ct_AdditionalCleaning,
ifnull(ct_BudgetedCAPACITYhours,0) ct_BudgetedCAPACITYhours,
ifnull(ct_BudgetedLaborhours,0) ct_BudgetedLaborhours,
ifnull(ct_BudgetedRessourcehours,0) ct_BudgetedRessourcehours,
ifnull(ct_Capacity,0) ct_Capacity,
ifnull(ct_Cumulativebreaksec,0) ct_Cumulativebreaksec,
ifnull(ct_Equipment,0) ct_Equipment,
ifnull(ct_Equipmentpart1,0) ct_Equipmentpart1,
ifnull(ct_Equipmentpart2,0) ct_Equipmentpart2,
ifnull(ct_Equipmentpart3,0) ct_Equipmentpart3,
ifnull(ct_Equipmentpart4,0) ct_Equipmentpart4,
ifnull(ct_Equipmentpart5,0) ct_Equipmentpart5,
ifnull(ct_Equipmentpart6,0) ct_Equipmentpart6,
ifnull(ct_Equipmentpart7,0) ct_Equipmentpart7,
ifnull(ct_Equipmentpart8,0) ct_Equipmentpart8,
ifnull(ct_Goodsreceivedorderitem,0) ct_Goodsreceivedorderitem,
ifnull(ct_LossesOfTime,0) ct_LossesOfTime,
ifnull(ct_normalproduction,0) ct_normalproduction,
ifnull(ct_Nrindividualcapacities,0) ct_Nrindividualcapacities,
ifnull(ct_OOSBatches,0) ct_OOSBatches,
ifnull(ct_Orderitemquantity,0) ct_Orderitemquantity,
ifnull(ct_OtherTypesOfDisturbances,0) ct_OtherTypesOfDisturbances,
ifnull(ct_OtherTypesOfEquipment,0) ct_OtherTypesOfEquipment,
ifnull(ct_OtherTypesOfProcessDisturb,0) ct_OtherTypesOfProcessDisturb,
ifnull(ct_OtherWaitingTimes,0) ct_OtherWaitingTimes,
ifnull(ct_PackagingMaterial,0) ct_PackagingMaterial,
ifnull(ct_Process,0) ct_Process,
ifnull(ct_processprolongation,0) ct_processprolongation,
ifnull(ct_Quality,0) ct_Quality,
ifnull(ct_QualityLosses,0) ct_QualityLosses,
ifnull(ct_RawMaterial,0) ct_RawMaterial,
ifnull(ct_ReasonCode,0) ct_ReasonCode,
ifnull(ct_Resources,0) ct_Resources,
ifnull(ct_RessourceShortage,0) ct_RessourceShortage,
ifnull(ct_SetUp,0) ct_SetUp,
ifnull(ct_Staff,0) ct_Staff,
ifnull(ct_StaffedResourceHours,0) ct_StaffedResourceHours,
ifnull(ct_TechnicalInterruption,0) ct_TechnicalInterruption,
ifnull(ct_TrialRuns,0) ct_TrialRuns,
ifnull(ct_UnexpectedBatch,0) ct_UnexpectedBatch,
ifnull(ct_UnplannedProcessChange,0) ct_UnplannedProcessChange,
ifnull(ct_Utilities,0) ct_Utilities,
ifnull(ct_yieldloss,0) ct_yieldloss,
ifnull(dd_Batchaccepted,'Not Set') dd_Batchaccepted,
ifnull(dd_BatchNumber,'Not Set') dd_BatchNumber,
ifnull(dd_Batchstatuskey,'Not Set') dd_Batchstatuskey,
ifnull(dd_CancellationCounter,'Not Set') dd_CancellationCounter,
ifnull(dd_CancellationFlag,'Not Set') dd_CancellationFlag,
ifnull(dd_capacitycategory,'Not Set') dd_capacitycategory,
ifnull(dd_CapacityID,'Not Set') dd_CapacityID,
ifnull(dd_Capacityname,'Not Set') dd_Capacityname,
ifnull(dd_CompanyName,'Not Set') dd_CompanyName,
ifnull(dd_CompanySite,'Not Set') dd_CompanySite,
ifnull(dd_Completionconfnumber,'Not Set') dd_Completionconfnumber,
ifnull(dd_Confirmationcounter,'Not Set') dd_Confirmationcounter,
ifnull(dd_ConfirmationPostingDate,'Not Set') dd_ConfirmationPostingDate,
ifnull(dd_Confirmationtext,'Not Set') dd_Confirmationtext,
ifnull(dd_Costcenterbasic,'Not Set') dd_Costcenterbasic,
ifnull(dd_CostCenterCategory,'Not Set') dd_CostCenterCategory,
ifnull(dd_CostCenterDescription,'Not Set') dd_CostCenterDescription,
ifnull(dd_CostCenterGroup,'Not Set') dd_CostCenterGroup,
ifnull(dd_country,'Not Set') dd_country,
ifnull(dd_KeyTaskListGroup,'Not Set') dd_KeyTaskListGroup,
ifnull(dd_MaterialNumber,'Not Set') dd_MaterialNumber,
ifnull(dd_MRPcontrollerorder,'Not Set') dd_MRPcontrollerorder,
ifnull(dd_ObjectIDoftheresource,'Not Set') dd_ObjectIDoftheresource,
ifnull(dd_ObjectNumber,'Not Set') dd_ObjectNumber,
ifnull(dd_ObjecttypesCIMresource,'Not Set') dd_ObjecttypesCIMresource,
ifnull(dd_OperationActivityNumber,'Not Set') dd_OperationActivityNumber,
ifnull(dd_Orderdeliveredinfull,'Not Set') dd_Orderdeliveredinfull,
ifnull(dd_Orderdeliveredontime,'Not Set') dd_Orderdeliveredontime,
ifnull(dd_Orderdelivontimeinfull,'Not Set') dd_Orderdelivontimeinfull,
ifnull(dd_OrderNumber,'Not Set') dd_OrderNumber,
ifnull(dd_OrderType,'Not Set') dd_OrderType,
ifnull(dd_PlantMaintenance,'Not Set') dd_PlantMaintenance,
ifnull(dd_ProductionProcess,'Not Set') dd_ProductionProcess,
ifnull(dd_ProductionVersion,'Not Set') dd_ProductionVersion,
ifnull(dd_ReasonforVariance,'Not Set') dd_ReasonforVariance,
ifnull(dd_ReasonforVarianceDescription,'Not Set') dd_ReasonforVarianceDescription,
ifnull(dd_Resourcedeletionflag,'Not Set') dd_Resourcedeletionflag,
ifnull(dd_Standardvaluekey,'Not Set') dd_Standardvaluekey,
ifnull(dd_WorkCenter,'Not Set') dd_WorkCenter,
ifnull(dd_WorkCenterCategory,'Not Set') dd_WorkCenterCategory,
ifnull(dd_WorkCenterCategoryDescription,'Not Set') dd_WorkCenterCategoryDescription,
ifnull(dd_WorkCenterDescription,'Not Set') dd_WorkCenterDescription,
ifnull(dim_Actualfinishdateid,1) dim_Actualfinishdateid,
ifnull(dim_Actualstartdateid,1) dim_Actualstartdateid,
ifnull(dim_Basicfinishdateid,1) dim_Basicfinishdateid,
ifnull(dim_BasicStartDateid,1) dim_BasicStartDateid,
ifnull(dim_bwproducthierarchyid,1) dim_bwproducthierarchyid,
ifnull(dim_companyid,1) dim_companyid,
ifnull(dim_ConfirmationPostingDateid,1) dim_ConfirmationPostingDateid,
ifnull(dim_ConfirmedOrderFinishDateid,1) dim_ConfirmedOrderFinishDateid,
ifnull(dim_finaluagsedecdateid,1) dim_finaluagsedecdateid,
ifnull(dim_partid,1) dim_partid,
ifnull(dim_plantid,1) dim_plantid,
ifnull(dim_snapshotdateid,1) dim_snapshotdateid,
ifnull(dim_workcenterid,1) dim_workcenterid,
ifnull(dd_calendarid,'Not Set') dd_calendarid,
ifnull(ct_workingdays,0) ct_workingdays,
ifnull(dim_valuationclassid,1) dim_valuationclassid,
ifnull(ct_grosshoursselected,0) ct_grosshoursselected,
ifnull(ct_calendardays,0) ct_calendardays
from tmp_fact_EquipmentAnalytics;



update fact_equipmentanalytics 
set ct_ProdResourceRunTime = CASE
           WHEN (ct_cumulativebreaksec) - (ct_setup) - (ct_qualitylosses) - (ct_ressourceshortage)
            - (ct_processprolongation) - (ct_technicalinterruption) - (ct_reasoncode) < 0 THEN 0
           ELSE (ct_cumulativebreaksec) - (ct_setup) - (ct_qualitylosses) - (ct_ressourceshortage)
            - (ct_processprolongation) - (ct_technicalinterruption) - (ct_reasoncode)
         END;

	
		 
update dim_oeemeasures
set sorter = case 
		  when measurename = 'Set Up' then 'b'
		  when measurename = 'Quality Losses' then 'c'
		  when measurename = 'Technical Interruptions' then 'd'
		  when measurename = 'Process Deviations' then 'e'
		  when measurename = 'Resource Shortages' then 'f'
		  when measurename = 'Missing or wrong Reason Code' then 'g'
		  when measurename = 'Productive Resource Run Time' then 'h' else 'Not Set' end ;
	
/* Fixing OEE - Wrong values for multiple month - to be moved in tmp table after validation - Bogdan S - Start*/
drop table if exists tmp_workingdays;
create table tmp_workingdays as
select 
case when max(month(cpdt.datevalue)) = max(month(current_date)) 
  then max(dd.noofworkingdaysofar) 
  else max(dd.noofworkingdays) 
end as workingdays, dd.calendarid, cpdt.MonthYear 
from fact_equipmentanalytics f, dim_date cpdt, tmp_dim_date_factory_calendar dd
where f.dim_ConfirmationPostingDateid = cpdt.dim_dateid
and dd.datevalue  =cpdt.datevalue
and f.dd_calendarid = dd.calendarid
group by dd.calendarid,cpdt.MonthYear;

update fact_equipmentanalytics f
set ct_workingdays2 = workingdays
from fact_equipmentanalytics f, dim_date cpdt,tmp_workingdays t
where f.dim_ConfirmationPostingDateid = cpdt.dim_dateid
and f.dd_calendarid = t.calendarid
and cpdt.monthyear=t.monthyear
and ct_workingdays2 <> workingdays;

drop table if exists tmp_nethoursselected2;
create table tmp_nethoursselected2 as 
select (CT_CAPACITY) * max(ct_workingdays2) as nethoursselected,cpdt.monthyear
,fact_equipmentanalyticsid
from fact_equipmentanalytics f, dim_date cpdt
where f.dim_ConfirmationPostingDateid = cpdt.dim_dateid
group by ct_capacity,cpdt.monthyear,fact_equipmentanalyticsid
;

update fact_equipmentanalytics f
set ct_nethoursselected = nethoursselected
from fact_equipmentanalytics f, dim_date cpdt, tmp_nethoursselected2 t
where f.dim_ConfirmationPostingDateid = cpdt.dim_dateid
and f.fact_equipmentanalyticsid = t.fact_equipmentanalyticsid
and cpdt.monthyear = t.monthyear
and ct_nethoursselected <> nethoursselected
;
/* Fixing OEE - Wrong values for multiple month - to be moved in tmp table after validation - Bogdan S - End*/

/*Drop temporary tables */


DROP TABLE IF EXISTS max_holder_701;
DROP TABLE IF EXISTS tmp_fact_EquipmentAnalytics;
DROP TABLE IF EXISTS tmp_cosl;
DROP TABLE IF EXISTS tmp_cosl2;
DROP TABLE IF EXISTS tmp_cosl3;
DROP TABLE IF EXISTS tmp_coslobjnr;
DROP TABLE IF EXISTS tmp_workingdays;
DROP TABLE IF EXISTS TMP_BKLAS;
DROP TABLE IF EXISTS tmp_maxcomplconfno;
DROP TABLE IF EXISTS tmp_nethoursselected2;
