

DROP TABLE IF EXISTS max_holder_701;
CREATE TABLE max_holder_701
AS
SELECT ifnull(max(fact_InspectionProcessid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) maxid
FROM fact_InspectionProcess;




drop table if exists tmp_fact_InspectionProcess;
create table tmp_fact_InspectionProcess as
select max_holder_701.maxid + row_number() over(order by '') as fact_InspectionProcessid,
ifnull(QAVE_PRUEFLOS , 0) as dd_inspectionlotno,
ifnull(QAVE_VCODE ,'Not Set') as dd_UsageDecisionCode,
ifnull(QAVE_VCODEGRP,'Not Set') as dd_CodeGroupUsageDecision,
ifnull(QALS_AUFNR,'Not Set') as dd_OrderNo,
ifnull(QALS_CHARG, 'Not Set') as dd_BatchNo,
ifnull(QALS_ART, 'Not Set') as dd_InspectionType,
cast (1 as bigint) as dim_DateCodeUsedUsageDecision,
cast (1 as bigint) as dim_plantid,
cast (1 as bigint) as dim_partid,
cast (1 as bigint) as dim_PostingDateinDocument,
cast ('Not Set' as varchar(18)) as dd_Producthierarchy,
cast ('Not Set' as varchar(7)) as  dd_MaterialType,
cast ('Not Set' as varchar(18)) as dd_ExternalMaterialGroup,
cast ('Not Set' as varchar(20)) as dd_MaterialGroup,
cast ('Not Set' as varchar(7)) as dd_Movementtype,
cast ('Not Set' as varchar(12)) as dd_OrderNumber,
cast ('Not Set' as varchar(10)) as  dd_Batch,
cast ('Not Set' as varchar(40)) as dd_MaterialDescriptionST,
cast ('Not Set' as varchar(40)) as dd_ProductionVersion,
cast ('Not Set' as varchar(7))  as dd_OrderType,
cast ('Not Set' as varchar(7))  as dd_userstatus,
cast ('Not Set' as varchar(30))  as dd_userstatusdescription,
cast ('Not Set' as varchar(7))  as dd_userstatuspo,
cast ('Not Set' as varchar(30))  as dd_userstatusdescriptionpo,
-- cast(0 as decimal (18,4)) as ct_TotalProductionbatches,
-- cast(0 as decimal (18,4)) as ct_RejectedBatches,
-- cast(0 as decimal (18,4)) as ct_ftr,
QAVE_VDATUM,  -- dim_DateCodeUsedUsageDecision
QAVE_VWERKS,  -- plant
QALS_MATNR,   -- dim_part
QALS_BUDAT,    -- dim_PostingDateinDocument
QALS_STSMA,
QALS_OBJNR,
QAVE_VEZEITERF,
QALS_ENTSTEZEIT,
QALS_ENSTEHDAT,
QALS_ART,
QALS_SELMATNR,
QALS_WERK,
cast (1 as bigint) as dim_DateIdRegistration,
cast ('1900-01-01 00:00:00.000' as TIMESTAMP) as dd_UsageDecisionDateTime,
cast ('Not Set' as VARCHAR(50)) as dd_InspectionLeadTime,
cast ('1900-01-01 00:00:00.000' as TIMESTAMP) as dd_RegistrationDateTime,
cast ('1900-01-01 00:00:00.000' as TIMESTAMP) as dd_InspectionLotCreateDateTime,
cast (0 as DECIMAL(15,4)) as dd_PlannedInspectionTime
from QAVE qve
left join QALS qls on qve.QAVE_PRUEFLOS = qls.QALS_PRUEFLOS
cross join max_holder_701;


/* FTR */

/*drop table if exists tmp_TotalProductionbatches
create table tmp_TotalProductionbatches as
SELECT  dd_BatchNo, count(*)batchcount from tmp_fact_InspectionProcess
group by  dd_BatchNo


UPDATE tmp_fact_InspectionProcess tmp
FROM tmp_TotalProductionbatches btc
SET tmp.ct_TotalProductionbatches = btc.batchcount
WHERE tmp.dd_BatchNo = btc.dd_BatchNo


drop table if exists tmp_RejectedBatches
create table tmp_RejectedBatches as
SELECT  dd_BatchNo, count(*)batchcount from tmp_fact_InspectionProcess
where dd_UsageDecisionCode in ('R','RC3','RC5','RC7','RC8')
group by  dd_BatchNo

UPDATE tmp_fact_InspectionProcess tmp
FROM tmp_RejectedBatches rej
SET tmp.ct_RejectedBatches = rej.batchcount
WHERE tmp.dd_BatchNo = rej.dd_BatchNo

UPDATE tmp_fact_InspectionProcess tmp
SET ct_ftr = case when ct_RejectedBatches <> 0 then (1- ct_RejectedBatches/ct_TotalProductionbatches) *100
else 100 end */



/* FTR END */



UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dim_DateCodeUsedUsageDecision = dd.dim_dateid
FROM  tmp_fact_InspectionProcess tmp,dim_date dd
where dd.Datevalue = ifnull(QAVE_VDATUM,'0001-01-01')
and companycode = 'Not Set';

UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dim_plantid = dp.dim_plantid
FROM  tmp_fact_InspectionProcess tmp,dim_plant dp
WHERE dp.Plantcode = IFNULL(QAVE_VWERKS,'Not Set');

UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dim_partid = dp.dim_partid
FROM tmp_fact_InspectionProcess tmp,dim_part dp
WHERE dp.PartNumber  =  tmp.QALS_MATNR
  AND dp.Plant  =  tmp.QAVE_VWERKS;

UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dim_PostingDateinDocument = dd.dim_dateid
FROM  tmp_fact_InspectionProcess tmp,dim_date dd
where dd.Datevalue = ifnull(QALS_BUDAT,'0001-01-01')
and companycode = 'Not Set';



UPDATE tmp_fact_InspectionProcess tmp
SET dd_Producthierarchy =  IFNULL(MARA_PRDHA,'Not Set'),
 dd_MaterialType = IFNULL(MARA_MTART,'Not Set'),
 dd_ExternalMaterialGroup = IFNULL( MARA_EXTWG,'Not Set'),
 dd_MaterialGroup = IFNULL(  MARA_MATKL,'Not Set'),
 dd_MaterialDescriptionST = ifnull(MAKT_MAKTX,'Not Set')
FROM tmp_fact_InspectionProcess tmp,MARA_MARC_MAKT m
WHERE  tmp.QALS_MATNR = m.MARA_MATNR
   AND tmp.QAVE_VWERKS = m.MARC_WERKS;



UPDATE  tmp_fact_InspectionProcess tmp
SET dd_ProductionVersion = IFNULL(mkal_text1  ,'Not Set')
FROM  tmp_fact_InspectionProcess tmp,AFKO_AFPO_AUFK afk, MKAL mk
WHERE tmp.dd_OrderNo = afk.afko_aufnr
 AND     afk.AFPO_VERID = mk.mkal_verid
 AND     mk.mkal_matnr = tmp.QALS_MATNR
 AND     mk.mkal_werks = tmp.QAVE_VWERKS;

UPDATE  tmp_fact_InspectionProcess tmp
SET dd_OrderType = ifnull(AUFK_AUART, 'Not Set')
FROM  tmp_fact_InspectionProcess tmp,AFKO_AFPO_AUFK afk
WHERE tmp.dd_OrderNo = afk.afko_aufnr;


/* BI 4662 - Roxana - Added user status for First Time Right Report */

/* UPDATE  tmp_fact_InspectionProcess tmp
SET dd_userstatus = ifnull(TJ30T_TXT04, 'Not Set'),
	dd_userstatusdescription = ifnull(TJ30T_TXT30, 'Not Set')
FROM tmp_fact_InspectionProcess tmp, TJ30T a, JEST_INSP b
WHERE   a.TJ30T_ESTAT = b.JEST_STAT
	AND a.TJ30T_STSMA = tmp.QALS_STSMA
	AND b.JEST_OBJNR  = tmp.QALS_OBJNR */

MERGE INTO tmp_fact_InspectionProcess tmp
USING ( SELECT QALS_STSMA,QALS_OBJNR,max(TJ30T_TXT30) as dd_userstatusdescription,MAX(TJ30T_TXT04) AS dd_userstatus
FROM tmp_fact_InspectionProcess tmp, TJ30T a, JEST_INSP b
WHERE a.TJ30T_ESTAT = b.JEST_STAT
  AND a.TJ30T_STSMA = tmp.QALS_STSMA
   AND b.JEST_OBJNR = tmp.QALS_OBJNR
GROUP BY QALS_STSMA,QALS_OBJNR )X
 ON (tmp.QALS_STSMA = X.QALS_STSMA AND tmp.QALS_OBJNR = X.QALS_OBJNR )
WHEN MATCHED THEN UPDATE 
SET tmp.dd_userstatus = X.dd_userstatus,
    tmp.dd_userstatusdescription = X.dd_userstatusdescription
WHERE tmp.dd_userstatus <> X.dd_userstatus 
  AND tmp.dd_userstatusdescription <> X.dd_userstatusdescription;

/* Add User Status for Process Order */

/*UPDATE  tmp_fact_InspectionProcess tmp
SET dd_userstatuspo = ifnull(TJ30T_TXT04, 'Not Set'),
	dd_userstatusdescriptionpo = ifnull(TJ30T_TXT30, 'Not Set')
FROM tmp_fact_InspectionProcess tmp, TJ30T a, JEST_INSP b, JSTO c
WHERE   c.JSTO_OBJNR = concat ('OR', tmp.dd_OrderNo)
    AND b.JEST_OBJNR = concat ('OR', tmp.dd_OrderNo)
    AND b.JEST_STAT  = a.TJ30T_ESTAT
    AND a.TJ30T_STSMA = c.JSTO_STSMA */

MERGE INTO tmp_fact_InspectionProcess tmp
USING ( SELECT dd_OrderNo,max(TJ30T_TXT30) as dd_userstatusdescriptionpo,MAX(TJ30T_TXT04) AS dd_userstatuspo
FROM tmp_fact_InspectionProcess tmp, TJ30T a, JEST_INSP b, JSTO c
WHERE c.JSTO_OBJNR = concat ('OR', tmp.dd_OrderNo)
  AND b.JEST_OBJNR = concat ('OR', tmp.dd_OrderNo)
  AND b.JEST_STAT  = a.TJ30T_ESTAT
  AND a.TJ30T_STSMA = c.JSTO_STSMA
GROUP BY dd_OrderNo) X
  ON tmp.dd_OrderNo = X.dd_OrderNo 
WHEN MATCHED THEN UPDATE 
SET tmp.dd_userstatuspo = X.dd_userstatuspo,
    tmp.dd_userstatusdescriptionpo = X.dd_userstatusdescriptionpo
WHERE tmp.dd_userstatuspo <> X.dd_userstatuspo 
AND   tmp.dd_userstatusdescriptionpo <> X.dd_userstatusdescriptionpo;

/* end of BI-4662 */

/* BI-5887|30.03.2017 - Cristian C - Added new measures: Registration Date/Time, Usage Decision Date/Time, Inspection Lead Time*/

drop table if exists tmp_JCDS;
create table tmp_JCDS
as select distinct JCDS_OBJNR,JCDS_UDATE, max(JCDS_UTIME)JCDS_UTIME from JCDS
group by JCDS_OBJNR,JCDS_UDATE;


UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dim_DateIdRegistration = dd.dim_dateid
FROM tmp_fact_InspectionProcess tmp, tmp_JCDS, dim_date dd
where dd.Datevalue = ifnull(JCDS_UDATE,'0001-01-01')
and companycode = 'Not Set'
and tmp.QALS_OBJNR = JCDS_OBJNR;

/*UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dim_DateIdRegistration = dd.dim_dateid
FROM  tmp_fact_InspectionProcess tmp, JCDS, dim_date dd
where dd.Datevalue = ifnull(JCDS_UDATE,'0001-01-01')
and companycode = 'Not Set'
and tmp.QALS_OBJNR = JCDS_OBJNR*/

UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dd_RegistrationDateTime = TO_TIMESTAMP(CONCAT(IFNULL(JCDS_UDATE,'1900-01-01'),IFNULL(JCDS_UTIME,'000000')),'YYYY-MM-DDHH24MISS')
FROM tmp_fact_InspectionProcess tmp, tmp_JCDS
WHERE tmp.QALS_OBJNR = JCDS_OBJNR;

drop table if exists tmp_JCDS;

UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dd_UsageDecisionDateTime = TO_TIMESTAMP(CONCAT(IFNULL(QAVE_VDATUM,'1900-01-01'),IFNULL(QAVE_VEZEITERF,'000000')),'YYYY-MM-DDHH24MISS')
FROM tmp_fact_InspectionProcess tmp;

UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dd_InspectionLeadTime = ltrim(rtrim(to_char(dd_UsageDecisionDateTime-dd_RegistrationDateTime),'.0000000000'),'+0000000')
FROM tmp_fact_InspectionProcess tmp;

UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dd_InspectionLotCreateDateTime = TO_TIMESTAMP(CONCAT(IFNULL(QALS_ENSTEHDAT,'1900-01-01'),IFNULL(QALS_ENTSTEZEIT,'000000')),'YYYY-MM-DDHH24MISS')
FROM tmp_fact_InspectionProcess tmp;

UPDATE tmp_fact_InspectionProcess tmp
SET tmp.dd_PlannedInspectionTime = QMAT_MPDAU
FROM tmp_fact_InspectionProcess tmp, QMAT
WHERE tmp.QALS_ART = QMAT_ART
AND TMP.QALS_SELMATNR = QMAT_MATNR
AND TMP.QALS_WERK = QMAT_WERKS;

/* END BI-5887*/

delete from fact_InspectionProcess;
INSERT INTO fact_InspectionProcess
(
fact_InspectionProcessid,
dd_inspectionlotno,
dd_UsageDecisionCode,
dd_CodeGroupUsageDecision,
dd_OrderNo,
dd_BatchNo,
dim_DateCodeUsedUsageDecision,
dim_plantid,
dim_partid,
dim_PostingDateinDocument,
dd_Producthierarchy,
dd_MaterialType,
dd_ExternalMaterialGroup,
dd_MaterialGroup,
dd_Movementtype,
dd_OrderNumber,
dd_Batch,
dd_InspectionType,
dd_MaterialDescriptionST,
dd_ProductionVersion,
dd_OrderType,
dd_userstatus,
dd_userstatusdescription,
dd_userstatuspo,
dd_userstatusdescriptionpo,
dim_DateIdRegistration,
dd_UsageDecisionDateTime,
dd_InspectionLeadTime,
dd_RegistrationDateTime,
dd_InspectionLotCreateDateTime,
dd_PlannedInspectionTime
)
SELECT
fact_InspectionProcessid,
dd_inspectionlotno,
dd_UsageDecisionCode,
dd_CodeGroupUsageDecision,
dd_OrderNo,
dd_BatchNo,
dim_DateCodeUsedUsageDecision,
dim_plantid,
dim_partid,
dim_PostingDateinDocument,
dd_Producthierarchy,
dd_MaterialType,
dd_ExternalMaterialGroup,
dd_MaterialGroup,
dd_Movementtype,
dd_OrderNumber,
dd_Batch,
dd_InspectionType,
dd_MaterialDescriptionST,
dd_ProductionVersion,
dd_OrderType,
dd_userstatus,
dd_userstatusdescription,
dd_userstatuspo,
dd_userstatusdescriptionpo,
dim_DateIdRegistration,
dd_UsageDecisionDateTime,
dd_InspectionLeadTime,
dd_RegistrationDateTime,
dd_InspectionLotCreateDateTime,
dd_PlannedInspectionTime
FROM tmp_fact_InspectionProcess;
