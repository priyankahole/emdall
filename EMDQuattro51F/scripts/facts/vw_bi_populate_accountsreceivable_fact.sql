
/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 10 May 2013 */
/*   Description    : Stored Proc bi_populate_accountsreceivable_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                             */
/*   08 Mar 2014      George    1.17            Added dim_salesdivisionid                                          */
/*   06 Nov 2013  	  Issam		1.16			Added dd_CreditMgr												  */
/*   05 Sep 2013      Lokesh	1.1  		  Exchange rate changes for transaction,local and global currencies */
/*   10 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/

/* Refreshing relevant tables */

/*
mysqldump --default-character-set=utf8 -uoctet -poctet --skip-triggers --routines --single-transaction columbia275 --tables
bsad bsid but000 but050 cvi_cust_link dim_accountreceivablestatus dim_accountsreceivabledocstatus dim_blockingpaymentreason
dim_businessarea dim_chartofaccounts dim_company dim_creditcontrolarea dim_currency dim_customer dim_customerpaymentterms 
dim_date dim_documenttypetext dim_paymentmethod dim_paymentreason dim_postingkey dim_riskclass dim_specialglindicator dim_specialgltransactiontype
fact_accountsreceivable fact_billing fact_disputemanagement fact_salesorder fdm_dcproc scmg_t_case_attr systemproperty ukmbp_cms ukmbp_cms_sgm
| mysql -uoctet -poctet --host=192.168.200.125  -C columbia99 &

*/

/*********************************************START****************************************************************/

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

insert into number_fountain
select 'processinglog',ifnull(max(f.processinglogid ), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from processinglog f;

Drop table if exists tmp_far_variable_holder;

create table tmp_far_variable_holder
AS
Select  CONVERT(varchar(7), ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD')) AS pGlobalCurrency;

DROP TABLE IF EXISTS fact_accountsreceivable_temp;

create table fact_accountsreceivable_temp like fact_accountsreceivable INCLUDING DEFAULTS INCLUDING IDENTITY;

alter table fact_accountsreceivable_temp add constraint primary key (fact_accountsreceivableid);

/*
CREATE TABLE fact_accountsreceivable_temp AS 
select * from fact_accountsreceivable where 1 = 2*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_accountsreceivable_temp';

insert into number_fountain
select 'fact_accountsreceivable_temp',ifnull(max(f.fact_accountsreceivableid ), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_accountsreceivable_temp f;


INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'bi_populate_accountsreceivable_fact START',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


UPDATE BSID SET BSID_ZUONR = ifnull(BSID_ZUONR,'Not Set'), BSID_BUKRS = ifnull(BSID_BUKRS,'Not Set'), BSID_KUNNR = ifnull(BSID_KUNNR,'Not Set');
UPDATE BSAD SET BSAD_ZUONR = ifnull(BSAD_ZUONR,'Not Set'), BSAD_BUKRS = ifnull(BSAD_BUKRS,'Not Set'), BSAD_KUNNR = ifnull(BSAD_KUNNR,'Not Set');

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE  fact_accountsreceivable_temp far',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


/* Insert 1 */

INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency,
                                    amt_CashDiscountLocalCurrency,
                                    amt_InDocCurrency,
                                    amt_InLocalCurrency,
                                    amt_TaxInDocCurrency,
                                    amt_TaxInLocalCurrency,
                                    ct_CashDiscountDays1,
                                    dd_AccountingDocItemNo,
                                    dd_AccountingDocNo,
                                    dd_AssignmentNumber,
                                    Dim_ClearedFlagId,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdCreated,
                                    Dim_DateIdPosting,
                                    dd_debitcreditid,
                                    dd_ClearingDocumentNo,
                                    dd_FiscalPeriod,
                                    dd_FiscalYear,
                                    dd_FixedPaymentTerms,
                                    dd_InvoiceNumberTransBelongTo,
                                    dd_NetPaymentTermsPeriod,
                                    Dim_BlockingPaymentReasonId,
                                    Dim_BusinessAreaId,
                                    Dim_ChartOfAccountsId,
                                    Dim_CompanyId,
                                    Dim_CreditControlAreaId,
									/* Local currency */
                                    Dim_CurrencyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    Dim_DocumentTypeId,
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    Dim_SpecialGLIndicatorId,
                                    Dim_TargetSpecialGLIndicatorId,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    dd_BillingNo,
                                    dd_CashDiscountPercentage1,
                                    dd_CashDiscountPercentage2,
                                    dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    Dim_PaymentTermsId,
                                    dd_DunningLevel,
                                    Dim_PaymentMethodId,
                                    dim_RiskClassId,
                                    dd_CreditRep,
/* Begin 06 Nov 2013 */									
                                    dd_CreditMgr,
/* End 06 Nov 2013 */									
                                    Dim_DateidSONextDate,
				    fact_accountsreceivableid,
									/* Transaction and Global currencies */
									dim_Currencyid_TRA,
									dim_Currencyid_GBL,
									amt_exchangerate,
									amt_exchangerate_GBL,
									dim_salesdivisionid)
   SELECT (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WSKTO amt_CashDiscountDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_SKNTO amt_CashDiscountLocalCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WRBTR amt_InDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_MWSTS amt_TaxInLocalCurrency,
          BSID_ZBD1P ct_CashDiscountDays1,
          BSID_BUZEI dd_AccountingDocItemNo,
          BSID_BELNR dd_AccountingDocNo,
          ifnull(BSID_ZUONR,'Not Set') dd_AssignmentNumber,
          1 Dim_ClearedFlagId,
          1 Dim_DateIdAccDocDateEntered,
          1 Dim_DateIdBaseDateForDueDateCalc,
          1 Dim_DateIdCreated,
          1 Dim_DateIdPosting,
          (CASE WHEN BSID_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END) dd_debitcreditid,
          ifnull(BSID_AUGBL,'Not Set') dd_ClearingDocumentNo,
          ifnull(BSID_MONAT,0) dd_FiscalPeriod,
          ifnull(BSID_GJAHR,0) dd_FiscalYear,
          ifnull(BSID_ZBFIX,'Not Set') dd_FixedPaymentTerms,
          ifnull(BSID_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
          ifnull(BSID_ZBD3T,0) dd_NetPaymentTermsPeriod,
          1 Dim_BlockingPaymentReasonId,
          1 Dim_BusinessAreaId,
          1 Dim_ChartOfAccountsId,
          dc.Dim_CompanyId,
          1 Dim_CreditControlAreaId,
		/* dim_currencyid ( local currency ) is populated from dim_company */
          1 Dim_Currencyid,
          cm.Dim_CustomerId Dim_CustomerID,
          1 Dim_DateIdClearing,
          1 Dim_DocumentTypeId,
          1 Dim_PaymentReasonId,
          1 Dim_PostingKeyId,
          1 Dim_SpecialGLIndicatorId,
          1 Dim_TargetSpecialGLIndicatorId,
          ifnull(BSID_VBEL2, 'Not Set') dd_SalesDocNo,
          BSID_POSN2 dd_SalesItemNo,
          BSID_ETEN2 dd_SalesScheduleNo,
          ifnull(BSID_VBELN,'Not Set') dd_BillingNo,
          BSID_ZBD1P dd_CashDiscountPercentage1,
          BSID_ZBD2P dd_CashDiscountPercentage2,
          ifnull(BSID_AUFNR,'Not Set') dd_ProductionOrderNo,
          1 Dim_SpecialGlTransactionTypeId ,
          1 Dim_AccountsReceivableDocStatusId,
          1 Dim_PaymentTermsId,
          1 Dim_PaymentMethodId,
          BSID_MANST,
          0 dim_RiskClassId,
          'Not Set' dd_CreditRep,
/* Begin 06 Nov 2013 */									
           'Not Set' dd_CreditMgr,
/* End 06 Nov 2013 */				  
          1 Dim_DateidSONextDate,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_accountsreceivable_temp') + row_number() over (order by ''),
		  /* dim_Currencyid_TRA ( local currency ) is populated from BSID_WAERS */
          1  dim_Currencyid_TRA,
          1 dim_Currencyid_GBL,
		  1 amt_exchangerate,	/*Get local rate from dmbtr and wrbtr columns*/
		  1 amt_exchangerate_GBL,  /*Get global rate from exchange rate script */
  		  1 dim_salesdivisionid	 
    FROM tmp_far_variable_holder,BSID arc
      INNER JOIN dim_company dc ON dc.CompanyCode = arc.BSID_BUKRS
      INNER JOIN dim_customer cm ON cm.CustomerNumber = arc.BSID_KUNNR
    WHERE not exists ( SELECT 1
                    FROM fact_accountsreceivable_temp ar
                   WHERE     ar.dd_AccountingDocNo = arc.BSID_BELNR
                         AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
                         AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
                         AND ar.dd_fiscalyear = arc.BSID_GJAHR
                         AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
                         AND ar.Dim_CompanyId = dc.Dim_CompanyId
                         AND ar.Dim_CustomerId = cm.Dim_CustomerId);


						 
						 
drop table if exists frcv_upd_1;
create table frcv_upd_1 as 
select distinct ar.fact_accountsreceivableid , BSID_REBZG,BSID_REBZJ,BSID_BUKRS,BSID_CPUDT,BSID_ZFBDT,BSID_BLDAT,BSID_BUDAT,BSID_ZLSPR,BSID_GSBER,BSID_SAKNR,BSID_KKBER,BSID_AUGDT
,BSID_BLART,BSID_RSTGR,BSID_BSCHL,BSID_UMSKZ,BSID_ZUMSK,BSID_UMSKS,BSID_BSTAT,BSID_ZTERM,BSID_ZLSCH,BSID_VBEL2,BSID_WAERS,BSID_XREF3
from 
fact_accountsreceivable_temp ar, BSID arc, dim_company dc,dim_customer cm
where ar.dd_AccountingDocNo = arc.BSID_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
AND ar.dd_fiscalyear = arc.BSID_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
AND dc.CompanyCode = arc.BSID_BUKRS
AND cm.CustomerNumber = arc.BSID_KUNNR 
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId    
;
  

update fact_accountsreceivable_temp
set Dim_ClearedFlagId = ars.Dim_AccountReceivableStatusId 
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_AccountReceivableStatus ars
where 
ars.Status = CASE WHEN u.BSID_REBZG IS NOT NULL AND u.BSID_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and Dim_ClearedFlagId <> ars.Dim_AccountReceivableStatusId
;

update fact_accountsreceivable_temp
set Dim_DateIdAccDocDateEntered = dt.dim_dateid  
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_date dt
where 
dt.DateValue = u.BSID_CPUDT AND dt.CompanyCode = u.BSID_BUKRS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and Dim_DateIdAccDocDateEntered <> dt.dim_dateid 
;

update fact_accountsreceivable_temp
set Dim_DateIdBaseDateForDueDateCalc = dt.dim_dateid 
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_date dt
where 
dt.DateValue = u.BSID_ZFBDT
              AND dt.CompanyCode = u.BSID_BUKRS    
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                 
and ar.Dim_DateIdBaseDateForDueDateCalc <> dt.dim_dateid
;


update fact_accountsreceivable_temp
set Dim_DateIdCreated = dt.dim_dateid 
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_date dt
where 
dt.DateValue = BSID_BLDAT
             AND  dt.CompanyCode = u.BSID_BUKRS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.Dim_DateIdCreated <> dt.dim_dateid
;


     
update fact_accountsreceivable_temp
set Dim_DateIdPosting = dt.dim_dateid 
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_date dt
where 
dt.DateValue = BSID_BUDAT
              AND dt.CompanyCode = u.BSID_BUKRS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_DateIdPosting <> dt.dim_dateid
;

 
update fact_accountsreceivable_temp
set Dim_BlockingPaymentReasonId = bpr.Dim_BlockingPaymentReasonId 
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_BlockingPaymentReason bpr
where 
bpr.BlockingKeyPayment = u.BSID_ZLSPR
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId
;


update fact_accountsreceivable_temp
set Dim_BusinessAreaId = ba.Dim_BusinessAreaId 
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_BusinessArea ba
where 
ba.BusinessArea = BSID_GSBER
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                       
and ar.Dim_BusinessAreaId <> ba.Dim_BusinessAreaId
;
		

/*ambiguous update using tmp*/
update fact_accountsreceivable_temp
set Dim_ChartOfAccountsId = coa.Dim_ChartOfAccountsId 
from  fact_accountsreceivable_temp ar, BSID arc,dim_company dc,dim_customer cm,
Dim_ChartOfAccounts coa
where 
dc.ChartOfAccounts = coa.CharOfAccounts
AND coa.GLAccountNumber = BSID_SAKNR
and ar.dd_AccountingDocNo = arc.BSID_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
AND ar.dd_fiscalyear = arc.BSID_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
AND dc.CompanyCode = arc.BSID_BUKRS
AND cm.CustomerNumber = arc.BSID_KUNNR 
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                        
and ar.Dim_ChartOfAccountsId <> coa.Dim_ChartOfAccountsId
;		

update fact_accountsreceivable_temp
set dim_currencyid = c.dim_currencyid
from  fact_accountsreceivable_temp ar, BSID arc,dim_company dc,dim_customer cm,
dim_currency c
where c.CurrencyCode = dc.Currency
and ar.dd_AccountingDocNo = arc.BSID_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
AND ar.dd_fiscalyear = arc.BSID_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
AND dc.CompanyCode = arc.BSID_BUKRS
AND cm.CustomerNumber = arc.BSID_KUNNR 
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                        
and ar.dim_currencyid <> c.dim_currencyid
;

/*ambiguous update using tmp end */

update fact_accountsreceivable_temp
set Dim_CreditControlAreaId = cca.Dim_CreditControlAreaId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_creditcontrolarea cca
where cca.CreditControlAreaCode = u.BSID_KKBER
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                            
and ar.Dim_CreditControlAreaId <> cca.Dim_CreditControlAreaId
;


update fact_accountsreceivable_temp
set Dim_DateIdClearing = dt.dim_dateid
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_date dt
where dt.DateValue = BSID_AUGDT
                  AND dt.CompanyCode = u.BSID_BUKRS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_DateIdClearing <> dt.dim_dateid
;



update fact_accountsreceivable_temp
set Dim_DocumentTypeId = dtt.dim_documenttypetextid
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_documenttypetext dtt
where  dtt.type = BSID_BLART
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_DocumentTypeId <> dtt.dim_documenttypetextid
;


update fact_accountsreceivable_temp
set Dim_PaymentReasonId = pr.Dim_PaymentReasonId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_PaymentReason pr
where  pr.PaymentReasonCode = u.BSID_RSTGR
       AND pr.CompanyCode = u.BSID_BUKRS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                          
and ar.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId
;


update fact_accountsreceivable_temp
set Dim_PostingKeyId = pk.Dim_PostingKeyId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_PostingKey pk
where  pk.PostingKey= BSID_BSCHL
             AND pk.SpecialGLIndicator = ifnull(BSID_UMSKZ,'Not Set')
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_PostingKeyId <> pk.Dim_PostingKeyId
;


update fact_accountsreceivable_temp
set Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_SpecialGLIndicator sgl
where  sgl.SpecialGLIndicator = BSID_UMSKZ
              AND sgl.AccountType =  'D' 
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
;

update fact_accountsreceivable_temp
set Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_SpecialGLIndicator sgl
where  sgl.SpecialGLIndicator = BSID_ZUMSK
              AND sgl.AccountType =  'D' 
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
;


update fact_accountsreceivable_temp
set Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_SpecialGlTransactionType sgt
where  sgt.SpecialGlTransactionTypeId = BSID_UMSKS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId
;


update fact_accountsreceivable_temp
set Dim_AccountsReceivableDocStatusId = ards.Dim_AccountsReceivableDocStatusId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_AccountsReceivableDocStatus ards
where  ards.Status = BSID_BSTAT
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_AccountsReceivableDocStatusId <> ards.Dim_AccountsReceivableDocStatusId
;


update fact_accountsreceivable_temp
set Dim_PaymentTermsId = pt.Dim_CustomerPaymentTermsid
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
Dim_CustomerPaymentTerms pt
where  pt.PaymentTermCode = BSID_ZTERM
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_PaymentTermsId <> pt.Dim_CustomerPaymentTermsid
;


update fact_accountsreceivable_temp
set Dim_PaymentMethodId = pm.Dim_PaymentMethodId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,dim_customer cm,
 Dim_PaymentMethod pm
where pm.PaymentMethod = BSID_ZLSCH
      AND pm.Country = cm.Country
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_PaymentMethodId <> pm.Dim_PaymentMethodId
;


update fact_accountsreceivable_temp
set dim_RiskClassId = dr.Dim_RiskClassId
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,dim_customer cm,
 dim_riskclass dr,UKMBP_CMS a,BUT000 b,cvi_cust_link c
where  a.PARTNER = b.PARTNER and systimestamp <= ifnull(a.RATING_VAL_DATE,systimestamp)
       and c.PARTNER_GUID = b.PARTNER_GUID 
       and dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1
       and c.CUSTOMER = cm.CustomerNumber
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.dim_RiskClassId <> dr.Dim_RiskClassId
;


drop table if exists tmp_so_updt;
create table tmp_so_updt
as
select distinct so.dd_SalesDocNo,so.dim_dateidnextdate from fact_salesorder so;

update fact_accountsreceivable_temp
set Dim_DateidSONextDate = so.dim_dateidnextdate
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
 tmp_so_updt so
where  so.dd_SalesDocNo = BSID_VBEL2
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                            
and ar.Dim_DateidSONextDate <> so.dim_dateidnextdate
;

drop table if exists tmp_so_updt;


update fact_accountsreceivable_temp
set dim_Currencyid_TRA = c.dim_currencyid
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_currency c
where  c.CurrencyCode = u.BSID_WAERS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                   
and ar.dim_Currencyid_TRA <> c.dim_currencyid
;



update fact_accountsreceivable_temp
set dim_Currencyid_GBL = c.dim_currencyid
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_currency c,tmp_far_variable_holder 
where  c.CurrencyCode = tmp_far_variable_holder.pGlobalCurrency
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.dim_Currencyid_GBL <> c.dim_currencyid
;



update fact_accountsreceivable_temp
set amt_exchangerate_GBL = z.exchangeRate
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
tmp_getExchangeRate1 z,tmp_far_variable_holder
where  z.pFromCurrency  = BSID_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact' 
	   and z.pToCurrency = tmp_far_variable_holder.pGlobalCurrency AND z.pFromExchangeRate = 0 AND z.pDate = systimestamp		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and amt_exchangerate_GBL <> z.exchangeRate
;



update fact_accountsreceivable_temp
set dim_salesdivisionid = dsdi.dim_salesdivisionid
from  fact_accountsreceivable_temp ar,frcv_upd_1 u,
dim_salesdivision dsdi 
where  dsdi.divisioncode = BSID_XREF3			  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                       
and ar.dim_salesdivisionid <> dsdi.dim_salesdivisionid
;

update fact_accountsreceivable_temp
set amt_exchangerate = (arc.BSID_DMBTR/case when ifnull(arc.BSID_WRBTR,0) <> 0 then arc.BSID_WRBTR ELSE (CASE WHEN arc.BSID_DMBTR <> 0 THEN arc.BSID_DMBTR ELSE 1 END) END )
from  fact_accountsreceivable_temp ar, BSID arc,dim_company dc,dim_customer cm
where  
 ar.dd_AccountingDocNo = arc.BSID_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
AND ar.dd_fiscalyear = arc.BSID_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
AND dc.CompanyCode = arc.BSID_BUKRS
AND cm.CustomerNumber = arc.BSID_KUNNR 
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                      
and cast(ar.amt_exchangerate as decimal (19,6)) <> cast(arc.BSID_DMBTR/(case when ifnull(arc.BSID_WRBTR,0) <> 0 then arc.BSID_WRBTR ELSE (CASE WHEN arc.BSID_DMBTR <> 0 THEN arc.BSID_DMBTR ELSE 1 END) END ) as decimal (19,6));

drop table if exists frcv_upd_1;

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_accountsreceivableid),0) from fact_accountsreceivable_temp)
where table_name = 'fact_accountsreceivable_temp';
					 

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp farINNER JOIN BSAD arc ON far.dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp farINNER JOIN BSAD arc ON far.dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';


/* Insert 2 */

INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency,
                                    amt_CashDiscountLocalCurrency,
                                    amt_InDocCurrency,
                                    amt_InLocalCurrency,
                                    amt_TaxInDocCurrency,
                                    amt_TaxInLocalCurrency,
                                    ct_CashDiscountDays1,
                                    dd_AccountingDocItemNo,
                                    dd_AccountingDocNo,
                                    dd_AssignmentNumber,
                                    Dim_ClearedFlagId,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdCreated,
                                    Dim_DateIdPosting,
                                    dd_debitcreditid,
                                    dd_ClearingDocumentNo,
                                    dd_FiscalPeriod,
                                    dd_FiscalYear,
                                    dd_FixedPaymentTerms,
                                    dd_InvoiceNumberTransBelongTo,
                                    dd_NetPaymentTermsPeriod,
                                    Dim_BlockingPaymentReasonId,
                                    Dim_BusinessAreaId,
                                    Dim_ChartOfAccountsId,
                                    Dim_CompanyId,
                                    Dim_CreditControlAreaId,
									/* Local curr */
                                    Dim_CurrencyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    Dim_DocumentTypeId,
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    Dim_SpecialGLIndicatorId,
                                    Dim_TargetSpecialGLIndicatorId,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    dd_BillingNo,
                                    dd_CashDiscountPercentage1,
                                    dd_CashDiscountPercentage2,
                                    dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    Dim_PaymentTermsId,
                                    dd_DunningLevel,
                                    Dim_PaymentMethodId,
                                    dim_RiskClassId,
                                    dd_CreditRep,
/* Begin 06 Nov 2013 */									
									dd_CreditMgr,
/* End 06 Nov 2013 */				  									
                                    Dim_DateidSONextDate,fact_accountsreceivableid,
										/* Transaction and Global currencies */
									dim_Currencyid_TRA,
									dim_Currencyid_GBL,
									amt_exchangerate,
									amt_exchangerate_GBL)
   SELECT (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WSKTO amt_CashDiscountDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_SKNTO amt_CashDiscountLocalCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WRBTR amt_InDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_MWSTS amt_TaxInLocalCurrency,
       
        BSAD_ZBD1P ct_CashDiscountDays1,
          BSAD_BUZEI dd_AccountingDocItemNo,
          BSAD_BELNR dd_AccountingDocNo,
          ifnull(BSAD_ZUONR, 'Not Set') dd_AssignmentNumber,
          1 Dim_ClearedFlagId,
          1 Dim_DateIdAccDocDateEntered,
          1 Dim_DateIdBaseDateForDueDateCalc,
          1 Dim_DateIdCreated,
          1 Dim_DateIdPosting,
          (CASE WHEN BSAD_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)  dd_debitcreditid,
          ifnull(BSAD_AUGBL,'Not Set') dd_ClearingDocumentNo,
          BSAD_MONAT dd_FiscalPeriod,
          BSAD_GJAHR dd_FiscalYear,
          ifnull(BSAD_ZBFIX,'Not Set') dd_FixedPaymentTerms,
          ifnull(BSAD_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
          BSAD_ZBD3T dd_NetPaymentTermsPeriod,
          1 Dim_BlockingPaymentReasonId,
          1 Dim_BusinessAreaId,
          1 Dim_ChartOfAccountsId,
          dc.Dim_CompanyId,
          1 Dim_CreditControlAreaId,
          1 Dim_Currencyid,
          cm.Dim_CustomerId Dim_CustomerID,
          1 Dim_DateIdClearing,
          1 Dim_DocumentTypeId,
          1 Dim_PaymentReasonId,
          1 Dim_PostingKeyId,
          1 Dim_SpecialGLIndicatorId,
          1 Dim_TargetSpecialGLIndicatorId,
          ifnull(BSAD_VBEL2, 'Not Set') dd_SalesDocNo,
          BSAD_POSN2 dd_SalesItemNo,
          BSAD_ETEN2 dd_SalesScheduleNo,
          ifnull(BSAD_VBELN,'Not Set') dd_BillingNo,
          BSAD_ZBD1P dd_CashDiscountPercentage1,
          BSAD_ZBD2P dd_CashDiscountPercentage2,
          ifnull(BSAD_AUFNR,'Not Set') dd_ProductionOrderNo,
          1 Dim_SpecialGlTransactionTypeId,
          1 Dim_AccountsReceivableDocStatusId,
          1 Dim_PaymentTermsId,
          BSAD_MANST,
          1 Dim_PaymentMethodId,
          0 dim_RiskClassId,
          'Not Set' dd_CreditRep,
/* Begin 06 Nov 2013 */									
           'Not Set' dd_CreditMgr,
/* End 06 Nov 2013 */					  
          1 Dim_DateidSONextDate,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_accountsreceivable_temp') + row_number() over (order by ''),
		  		  /* dim_Currencyid_TRA ( local currency ) is populated from BSAD_WAERS */
          1 dim_Currencyid_TRA,
          1 dim_Currencyid_GBL,
		  1 amt_exchangerate,			
		  1 amt_exchangerate_GBL	 
    FROM tmp_far_variable_holder,BSAD arc
      INNER JOIN dim_company dc ON dc.CompanyCode = BSAD_BUKRS
      INNER JOIN dim_customer cm ON cm.CustomerNumber = BSAD_KUNNR
    WHERE not exists
                 (SELECT 1
                    FROM fact_accountsreceivable_temp ar
                   WHERE     ar.dd_AccountingDocNo = arc.BSAD_BELNR
                         AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
                         AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
                         AND ar.dd_fiscalyear = arc.BSAD_GJAHR
                         AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
                         AND ar.Dim_CompanyId = dc.Dim_CompanyId
                         AND ar.Dim_CustomerId = cm.Dim_CustomerId);
						 

drop table if exists frcv_upd_2;
create table frcv_upd_2 as 
select distinct ar.fact_accountsreceivableid , BSAD_REBZG,BSAD_REBZJ,BSAD_CPUDT,BSAD_BUKRS,BSAD_ZFBDT,BSAD_BLDAT,BSAD_BUDAT,BSAD_ZLSPR,BSAD_GSBER,
BSAD_SAKNR,BSAD_KKBER,BSAD_AUGDT,BSAD_BLART,BSAD_RSTGR,BSAD_BSCHL,BSAD_UMSKZ,BSAD_ZUMSK,BSAD_UMSKS,BSAD_BSTAT,BSAD_ZTERM,BSAD_ZLSCH,BSAD_VBEL2,BSAD_WAERS,BSAD_DMBTR,BSAD_WRBTR
from 
fact_accountsreceivable_temp ar, BSAD arc, dim_company dc,dim_customer cm
where  dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId    
;


update fact_accountsreceivable_temp
set Dim_ClearedFlagId = ars.Dim_AccountReceivableStatusId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_AccountReceivableStatus ars
where  ars.Status = CASE WHEN BSAD_REBZG IS NOT NULL AND BSAD_REBZJ <> 0 THEN 'F - Partial' ELSE 'C - Cleared' END	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and ar.Dim_ClearedFlagId <> ars.Dim_AccountReceivableStatusId
;

update fact_accountsreceivable_temp
set Dim_DateIdAccDocDateEntered = dt.dim_dateid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
dim_date dt
where  dt.DateValue = BSAD_CPUDT
                  AND dt.CompanyCode = BSAD_BUKRS	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                  
and ar.Dim_DateIdAccDocDateEntered <> dt.dim_dateid
;

update fact_accountsreceivable_temp
set Dim_DateIdBaseDateForDueDateCalc = dt.dim_dateid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
dim_date dt
where  dt.DateValue = BSAD_ZFBDT
                  AND dt.CompanyCode = BSAD_BUKRS	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_DateIdBaseDateForDueDateCalc <> dt.dim_dateid
;


update fact_accountsreceivable_temp
set Dim_DateIdCreated = dt.dim_dateid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
dim_date dt
where dt.DateValue = BSAD_BLDAT
                  AND dt.CompanyCode = BSAD_BUKRS		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid      
and ar.Dim_DateIdCreated <> dt.dim_dateid
;



update fact_accountsreceivable_temp
set Dim_DateIdPosting = dt.dim_dateid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
dim_date dt
where dt.DateValue = BSAD_BUDAT
                  AND dt.CompanyCode = BSAD_BUKRS		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_DateIdPosting <> dt.dim_dateid
;



update fact_accountsreceivable_temp
set Dim_BlockingPaymentReasonId = bpr.Dim_BlockingPaymentReasonId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_BlockingPaymentReason bpr
where bpr.BlockingKeyPayment = BSAD_ZLSPR		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId
;

update fact_accountsreceivable_temp
set Dim_BusinessAreaId = ba.Dim_BusinessAreaId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_BusinessArea ba
where  ba.BusinessArea = BSAD_GSBER		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_BusinessAreaId <>  ba.Dim_BusinessAreaId
;


update fact_accountsreceivable_temp
set Dim_ChartOfAccountsId = coa.Dim_ChartOfAccountsId
from  fact_accountsreceivable_temp ar, BSAD arc,dim_company dc,dim_customer cm,
Dim_ChartOfAccounts coa
where  dc.ChartOfAccounts = coa.CharOfAccounts
        AND coa.GLAccountNumber = BSAD_SAKNR		  
and dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                    
and ar.Dim_ChartOfAccountsId <> coa.Dim_ChartOfAccountsId
;


update fact_accountsreceivable_temp
set Dim_CreditControlAreaId = cca.Dim_CreditControlAreaId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
dim_creditcontrolarea cca
where  cca.CreditControlAreaCode = BSAD_KKBER	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                     
and ar.Dim_CreditControlAreaId <> cca.Dim_CreditControlAreaId
;


update fact_accountsreceivable_temp
set Dim_Currencyid = c.Dim_Currencyid
from  fact_accountsreceivable_temp ar, BSAD arc,dim_company dc,dim_customer cm,
dim_currency c
where  c.CurrencyCode = dc.Currency	  
and dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                    
and ar.Dim_Currencyid <> c.Dim_Currencyid
;


update fact_accountsreceivable_temp
set Dim_DateIdClearing = dt.dim_dateid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
dim_date dt
where  dt.DateValue = BSAD_AUGDT
AND dt.CompanyCode = BSAD_BUKRS		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_DateIdClearing <> dt.dim_dateid
;


update fact_accountsreceivable_temp
set Dim_DocumentTypeId = dtt.dim_documenttypetextid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
dim_documenttypetext dtt
where  dtt.type = BSAD_BLART		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_DocumentTypeId <> dtt.dim_documenttypetextid
;


update fact_accountsreceivable_temp
set Dim_PaymentReasonId = pr.Dim_PaymentReasonId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_PaymentReason pr
where  pr.PaymentReasonCode = BSAD_RSTGR
               AND pr.CompanyCode = BSAD_BUKRS		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                 
and ar.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId
;


update fact_accountsreceivable_temp
set Dim_PostingKeyId = pk.Dim_PostingKeyId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_PostingKey pk
where  pk.PostingKey= BSAD_BSCHL
            AND pk.SpecialGLIndicator = ifnull(BSAD_UMSKZ,'Not Set')		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                   
and ar.Dim_PostingKeyId <> pk.Dim_PostingKeyId
;


update fact_accountsreceivable_temp
set Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_SpecialGLIndicator sgl
where sgl.SpecialGLIndicator = BSAD_UMSKZ
              AND sgl.AccountType =  'D'	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                    
and ar.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
;



update fact_accountsreceivable_temp
set Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_SpecialGLIndicator sgl
where sgl.SpecialGLIndicator = BSAD_ZUMSK
              AND sgl.AccountType =  'D'		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                    
and ar.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
;



update fact_accountsreceivable_temp
set Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_SpecialGlTransactionType sgt
where sgt.SpecialGlTransactionTypeId = BSAD_UMSKS		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId
;


update fact_accountsreceivable_temp
set Dim_AccountsReceivableDocStatusId = ards.Dim_AccountsReceivableDocStatusId
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_AccountsReceivableDocStatus ards
where ards.Status = BSAD_BSTAT		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                     
and ar.Dim_AccountsReceivableDocStatusId <> ards.Dim_AccountsReceivableDocStatusId
;



update fact_accountsreceivable_temp
set Dim_PaymentTermsId = pt.Dim_CustomerPaymentTermsid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
Dim_CustomerPaymentTerms pt
where pt.PaymentTermCode = BSAD_ZTERM		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid     
and ar.Dim_PaymentTermsId <> pt.Dim_CustomerPaymentTermsid
;



update fact_accountsreceivable_temp
set Dim_PaymentMethodId = pm.Dim_PaymentMethodId
from  fact_accountsreceivable_temp ar, BSAD arc,dim_company dc,dim_customer cm,
Dim_PaymentMethod pm
where  pm.PaymentMethod = BSAD_ZLSCH
              AND pm.Country = cm.Country		  
and dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId 
and ar.Dim_PaymentMethodId <> pm.Dim_PaymentMethodId
;



update fact_accountsreceivable_temp
set dim_RiskClassId = dr.Dim_RiskClassId
from  fact_accountsreceivable_temp ar, BSAD arc,dim_company dc,dim_customer cm,
dim_riskclass dr,UKMBP_CMS a,BUT000 b,cvi_cust_link c 
where  a.PARTNER = b.PARTNER and systimestamp <= ifnull(a.RATING_VAL_DATE,systimestamp) and
	   c.PARTNER_GUID = b.PARTNER_GUID  and
	   dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1 and
	   c.CUSTOMER = cm.CustomerNumber	  
and dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId 
and ar.dim_RiskClassId <> dr.Dim_RiskClassId
;


drop table if exists tmp_so_upd2;
create table tmp_so_upd2 as 
select distinct so.dd_SalesDocNo, so.dim_dateidnextdate from fact_salesorder so
;


update fact_accountsreceivable_temp
set Dim_DateidSONextDate = so.dim_dateidnextdate
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
tmp_so_upd2 so
where  so.dd_SalesDocNo = BSAD_VBEL2		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.Dim_DateidSONextDate <> so.dim_dateidnextdate
;
drop table if exists tmp_so_upd2;




update fact_accountsreceivable_temp
set dim_Currencyid_TRA = c.dim_currencyid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u ,
dim_currency c
where  c.CurrencyCode = u.BSAD_WAERS	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.dim_Currencyid_TRA <> c.dim_currencyid
;



update fact_accountsreceivable_temp
set dim_Currencyid_GBL = c.dim_currencyid
from  fact_accountsreceivable_temp ar, frcv_upd_2 u,
dim_currency c,tmp_far_variable_holder
where  c.CurrencyCode = tmp_far_variable_holder.pGlobalCurrency	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.dim_Currencyid_GBL <> c.dim_currencyid
;


update fact_accountsreceivable_temp
set amt_exchangerate = (BSAD_DMBTR/case when ifnull(BSAD_WRBTR,0) <> 0 then BSAD_WRBTR ELSE (CASE WHEN BSAD_DMBTR <> 0 THEN BSAD_DMBTR ELSE 1 END) END )
from  fact_accountsreceivable_temp ar, frcv_upd_2 u
where  ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and cast(ar.amt_exchangerate as decimal (19,6)) <> cast((BSAD_DMBTR/case when ifnull(BSAD_WRBTR,0) <> 0 then BSAD_WRBTR ELSE (CASE WHEN BSAD_DMBTR <> 0 THEN BSAD_DMBTR ELSE 1 END) END ) as  decimal (19,6))
;


update fact_accountsreceivable_temp
set amt_exchangerate_GBL = z.exchangeRate
from  fact_accountsreceivable_temp ar, frcv_upd_2 u,
tmp_getExchangeRate1 z, tmp_far_variable_holder
where z.pFromCurrency  = BSAD_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
and  z.pToCurrency = pGlobalCurrency AND z.pFromExchangeRate = 0 AND z.pDate = systimestamp
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.amt_exchangerate_GBL <> z.exchangeRate
;


drop table if exists frcv_upd_2;
						 
						 
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_accountsreceivableid),0) from fact_accountsreceivable_temp)
where table_name = 'fact_accountsreceivable_temp';
					 
		 
					 
INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'INSERT INTO fact_accountsreceivable_temp(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp INNER JOIN BSID arc ON dd_AccountingDocNo = arc.BSID_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 

/* START OF Update 3*/

/* Update 3 could be broken up like this if required. But that will increase the size of code by a lot. But might be required if it causes performance problems */

/*
DROP TABLE IF EXISTS TMP_BSID_accountsreceivable
CREATE TABLE TMP_BSID_accountsreceivable
AS
SELECT b.*,(CASE WHEN BSID_ZFBDT IS NULL THEN BSID_BLDAT ELSE BSID_ZFBDT END ) as datevalue_upd,'N' as update_flag
FROM BSID b

UPDATE TMP_BSID_accountsreceivable
SET 	update_flag = 'Y'
WHERE  BSID_REBZG IS NULL AND BSID_SHKZG = 'H'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD3T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD3T IS NOT NULL AND BSID_ZBD3T <> 0
AND update_flag = 'N'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD2T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD2T IS NOT NULL AND BSID_ZBD2T <> 0
AND update_flag = 'N'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD1T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD1T IS NOT NULL AND BSID_ZBD1T <> 0
AND update_flag = 'N'

*/

/* Update 3 - Part 1*/

UPDATE fact_accountsreceivable_temp far
SET 
far.Dim_NetDueDateId = ifnull(DIM_DATEID,1)
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc, 
     fact_accountsreceivable_temp far ,
     DIM_DATE dt
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR
AND dt.CompanyCode = arc.BSID_BUKRS
and far.Dim_NetDueDateId <> ifnull(DIM_DATEID,1)
and dt.DateValue = ( CASE WHEN BSID_ZFBDT IS NULL THEN BSID_BLDAT
                                             ELSE BSID_ZFBDT END) + 
												  CAST((
												  CASE WHEN (BSID_REBZG IS NULL AND BSID_SHKZG = 'H') THEN 0 
												  ELSE ( CASE WHEN BSID_ZBD3T IS NOT NULL AND BSID_ZBD3T <> 0 THEN BSID_ZBD3T
														WHEN BSID_ZBD2T IS NOT NULL AND BSID_ZBD2T <> 0 THEN BSID_ZBD2T
														WHEN BSID_ZBD1T IS NOT NULL AND BSID_ZBD1T <> 0 THEN BSID_ZBD1T  ELSE 0 END) END) as integer)
;

UPDATE fact_accountsreceivable_temp far
SET 
far.Dim_NetDueDateWrtCashDiscountTerms1 = 
				CASE WHEN (BSID_REBZG IS NULL AND (BSID_SHKZG = 'H'))  
						THEN Dim_NetDueDateId
				ELSE Dim_NetDueDateId  END,
far.Dim_NetDueDateWrtCashDiscountTerms2 = 
				CASE WHEN (BSID_REBZG IS NULL AND (BSID_SHKZG = 'H')) 
						THEN Dim_NetDueDateId
                ELSE Dim_NetDueDateId  END	
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc, 
	 fact_accountsreceivable_temp far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR
;	 
	 


/* Update 3 - Part 2*/

UPDATE fact_accountsreceivable_temp far
SET far.Dim_NetDueDateWrtCashDiscountTerms1 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 dim_date dt,
	 fact_accountsreceivable_temp far	
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR	
AND BSID_ZBD1T IS NOT NULL AND dt.DateValue = CAST((BSID_ZFBDT + ifnull(CAST(BSID_ZBD1T as INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSID_BUKRS
and far.Dim_NetDueDateWrtCashDiscountTerms1 <> dt.DIM_DATEID
; 


	 
/* Update 3 - Part 3*/	 
	 
UPDATE fact_accountsreceivable_temp far
SET far.Dim_NetDueDateWrtCashDiscountTerms2 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 dim_date dt,
     fact_accountsreceivable_temp far	 
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR	
/* Join to update Dim_NetDueDateWrtCashDiscountTerms2 not updated in part 1 */
AND BSID_ZBD2T IS NOT NULL AND dt.DateValue = CAST((BSID_ZFBDT + ifnull(CAST(BSID_ZBD2T as INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSID_BUKRS ;		 



INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp INNER JOIN BSID arc ON dd_AccountingDocNo = arc.BSID_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 

/* END OF Update 3 */

/* START OF Update 4 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp INNER JOIN BSAD arc ON dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

/* Update 4 - Part 1*/

UPDATE fact_accountsreceivable_temp far
SET 
far.Dim_NetDueDateId = ifnull(dt.DIM_DATEID,1)
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 fact_accountsreceivable_temp far,
	 DIM_DATE dt 
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
and dt.DateValue = ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
                                             ELSE BSAD_ZFBDT END) + 
												  CAST((
												  CASE WHEN (BSAD_REBZG IS NULL AND BSAD_SHKZG = 'H') THEN 0 
												  ELSE ( CASE WHEN BSAD_ZBD3T IS NOT NULL AND BSAD_ZBD3T <> 0 THEN BSAD_ZBD3T
														WHEN BSAD_ZBD2T IS NOT NULL AND BSAD_ZBD2T <> 0 THEN BSAD_ZBD2T
														WHEN BSAD_ZBD1T IS NOT NULL AND BSAD_ZBD1T <> 0 THEN BSAD_ZBD1T  ELSE 0 END) END) as integer)
AND dt.CompanyCode = arc.BSAD_BUKRS 
and far.Dim_NetDueDateId <> ifnull(dt.DIM_DATEID,1)
;	 

UPDATE fact_accountsreceivable_temp far
SET 
far.Dim_NetDueDateWrtCashDiscountTerms1 = 
				CASE WHEN (BSAD_REBZG IS NULL AND (BSAD_SHKZG = 'H'))  
						THEN Dim_NetDueDateId
				ELSE Dim_NetDueDateId  END,
far.Dim_NetDueDateWrtCashDiscountTerms2 = 
				CASE WHEN (BSAD_REBZG IS NULL AND (BSAD_SHKZG = 'H')) 
						THEN Dim_NetDueDateId
                ELSE Dim_NetDueDateId  END	
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 fact_accountsreceivable_temp far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR 
;	 


/* Update 4 - Part 2*/

UPDATE fact_accountsreceivable_temp far
SET far.Dim_NetDueDateWrtCashDiscountTerms1 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 dim_date dt,
	 fact_accountsreceivable_temp far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
/* Join to update Dim_NetDueDateWrtCashDiscountTerms1 not updated in part 1 */
AND BSAD_ZBD1T IS NOT NULL AND dt.DateValue = CAST((BSAD_ZFBDT + ifnull(CAST(BSAD_ZBD1T as INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSAD_BUKRS 	 
and  far.Dim_NetDueDateWrtCashDiscountTerms1 <> dt.DIM_DATEID ; 
	 
/* Update 4 - Part 3*/	 
	 
UPDATE fact_accountsreceivable_temp far	 
SET far.Dim_NetDueDateWrtCashDiscountTerms2 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 dim_date dt,
	 fact_accountsreceivable_temp far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
/* Join to update Dim_NetDueDateWrtCashDiscountTerms2 not updated in part 1 */
AND BSAD_ZBD2T IS NOT NULL AND dt.DateValue = CAST((BSAD_ZFBDT + ifnull(CAST(BSAD_ZBD2T as INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSAD_BUKRS 		 
and far.Dim_NetDueDateWrtCashDiscountTerms2 <> dt.DIM_DATEID ;


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp INNER JOIN BSAD arc ON dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 




/* START OF Update 5 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far INNER JOIN  VBRK_VBRP',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE fact_accountsreceivable_temp far
SET amt_ExchangeRate_GBL = z.exchangeRate
FROM dim_currency tra,tmp_getExchangeRate1 z, tmp_far_variable_holder, fact_accountsreceivable_temp far
WHERE far.dim_currencyid_tra = tra.dim_currencyid
AND z.pFromCurrency = tra.currencycode
AND z.pToCurrency = pGlobalCurrency
AND z.pFromExchangeRate = 0
AND z.pDate = systimestamp
AND z.fact_script_name = 'bi_populate_accountsreceivable_fact'
AND far.amt_ExchangeRate_GBL <> z.exchangeRate;

UPDATE fact_accountsreceivable_temp far
SET amt_ExchangeRate = ifnull((BSID_DMBTR/case when ifnull(BSID_WRBTR,0) <> 0 then BSID_WRBTR ELSE (CASE WHEN BSID_DMBTR <> 0 THEN BSID_DMBTR ELSE 1 END) END ),1)
FROM BSID arc, dim_customer dc,dim_company dcm,fact_accountsreceivable_temp far
WHERE far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND far.dd_billingno = arc.BSID_VBELN
AND dcm.dim_companyid = far.dim_companyid AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.Dim_CustomerId = far.Dim_CustomerId AND dc.CustomerNumber = arc.BSID_KUNNR;


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far INNER JOIN  VBRK_VBRP',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

/* END OF Update 5 */  
  
/* START OF Update 6 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far INNER JOIN  VBRK_VBRP part2',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE fact_accountsreceivable_temp far
SET amt_ExchangeRate_GBL = z.exchangeRate
FROM dim_currency tra,tmp_getExchangeRate1 z, tmp_far_variable_holder, fact_accountsreceivable_temp far
WHERE far.dim_currencyid_tra = tra.dim_currencyid
AND z.pFromCurrency = tra.currencycode
AND z.pToCurrency = pGlobalCurrency
AND z.pFromExchangeRate = 0
AND z.pDate = systimestamp
AND z.fact_script_name = 'bi_populate_accountsreceivable_fact'
AND far.amt_ExchangeRate_GBL <> z.exchangeRate;

UPDATE fact_accountsreceivable_temp far
SET amt_ExchangeRate = ifnull((BSAD_DMBTR/case when ifnull(BSAD_WRBTR,0) <> 0 then BSAD_WRBTR ELSE (CASE WHEN BSAD_DMBTR <> 0 THEN BSAD_DMBTR ELSE 1 END) END ),1)
FROM BSAD arc, dim_customer dc,dim_company dcm, fact_accountsreceivable_temp far
WHERE far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND far.dd_billingno = arc.BSAD_VBELN
AND dcm.dim_companyid = far.dim_companyid AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.Dim_CustomerId = far.Dim_CustomerId AND dc.CustomerNumber = arc.BSAD_KUNNR;
  

/* END OF Update 6 */

/* Get all rows for the latest billing item for each billing doc */
DROP TABLE IF EXISTS tmp_billing_update_ar;
CREATE TABLE tmp_billing_update_ar
AS
SELECT DISTINCT f_bill.*
FROM fact_billing f_bill,(SELECT dd_billing_no,max(dd_billing_item_no) dd_billing_item_no
FROM fact_billing fb GROUP BY dd_billing_no) t
WHERE f_bill.dd_billing_no = t.dd_billing_no AND f_bill.dd_billing_item_no = t.dd_billing_item_no;


/* Update 7 */

UPDATE fact_accountsreceivable_temp far
SET  far.Dim_PartId = ifnull(fb.Dim_PartId,1),
 far.Dim_PlantId = ifnull(fb.dim_plantid,1),
 far.dd_CustomerPONumber = fb.dd_CustomerPONumber,
 far.dd_salesdocno = case when far.dd_salesdocno = 'Not Set' then fb.dd_salesdocno else far.dd_salesdocno end
,dw_update_date = current_timestamp 
FROM tmp_billing_update_ar fb,fact_accountsreceivable_temp far
WHERE fb.dd_billing_no = far.dd_billingno
and far.Dim_PartId <> ifnull(fb.Dim_PartId,1)
;


drop table if exists tmp_sum_rcv_upd;
create table 	tmp_sum_rcv_upd 
as
select ifnull(SUM(f_bill.ct_BillingQtySalesUOM),0) sum_ct_BillingQtySalesUOM, ifnull( SUM(f_bill.ct_BillingQtyStockUOM),0) sum_ct_BillingQtyStockUOM, dd_billing_no 
from  fact_billing f_bill
group by dd_billing_no
;

UPDATE fact_accountsreceivable_temp far
SET far.ct_bill_QtySalesUOM = ifnull(sum_ct_BillingQtySalesUOM,0),
    far.ct_bill_QtyStockUOM = ifnull(sum_ct_BillingQtyStockUOM,0),
dw_update_date = current_timestamp 
FROM tmp_billing_update_ar fb, fact_accountsreceivable_temp far, tmp_sum_rcv_upd f_bill 
WHERE fb.dd_billing_no = far.dd_billingno
and far.dd_BillingNo = f_bill.dd_billing_no;

drop table if exists tmp_sum_rcv_upd;		 
		 
INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far INNER JOIN  VBRK_VBRP part2',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

/* Update 8 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far INNER JOIN BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		

drop table if exists rcv_so_upd;

create table rcv_so_upd as 
select max(so.dim_dateidnextdate) dim_dateidnextdate, so.dd_SalesDocNo from fact_salesorder so group by so.dd_SalesDocNo
;

UPDATE fact_accountsreceivable_temp far
SET	 amt_EligibleForDiscount = BSID_SKFBT, 
     far.Dim_DateidSONextDate = ifnull(so.dim_dateidnextdate , 1)
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 rcv_so_upd so,fact_accountsreceivable_temp far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.CustomerNumber = arc.BSID_KUNNR
AND so.dd_SalesDocNo = far.dd_salesdocno;
	 
drop table if exists rcv_so_upd;


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far INNER JOIN BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		

/* Update 9 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far INNER JOIN BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE fact_accountsreceivable_temp far
SET	 amt_EligibleForDiscount = BSID_SKFBT
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 fact_accountsreceivable_temp far 
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.CustomerNumber = arc.BSID_KUNNR
AND amt_EligibleForDiscount <> BSID_SKFBT
;

drop table if exists tmp_so_rcv_upd;
create table 	tmp_so_rcv_upd 
as
select max(so.dim_dateidnextdate) dim_dateidnextdate,so.dd_SalesDocNo from fact_salesorder so group by so.dd_SalesDocNo;

UPDATE fact_accountsreceivable_temp far
SET	 far.Dim_DateidSONextDate = ifnull(so.dim_dateidnextdate, 1)
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 tmp_so_rcv_upd so,
	 fact_accountsreceivable_temp far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.CustomerNumber = arc.BSID_KUNNR
AND so.dd_SalesDocNo = far.dd_salesdocno 
AND far.Dim_DateidSONextDate <> ifnull(so.dim_dateidnextdate, 1);

drop table if exists tmp_so_rcv_upd;

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far INNER JOIN BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	


/* Update 10 */

DROP TABLE IF EXISTS tmp_upd_ar1;
CREATE TABLE tmp_upd_ar1
AS
SELECT arc.BSID_BELNR,arc.BSID_BUZEI,arc.BSID_ZUONR,arc.BSID_GJAHR,arc.BSID_MONAT,MAX(armisc.Dim_ARMiscellaneousId) Dim_ARMiscellaneousId
FROM fact_accountsreceivable_temp far,Dim_ARMiscellaneous armisc, BSID arc
WHERE       far.dd_AccountingDocNo = arc.BSID_BELNR
       AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
       AND far.dd_AssignmentNumber = arc.BSID_ZUONR
       AND far.dd_fiscalyear = arc.BSID_GJAHR
       AND far.dd_FiscalPeriod  = arc.BSID_MONAT
       AND armisc.CustomerItemsClearingReversed = ifnull(arc.BSID_XRAGL,'Not Set')
       AND armisc.CustomerItemsDocumentPostedYet = ifnull(arc.BSID_XNETB,'Not Set')
       AND armisc.NegativePosting = ifnull(arc.BSID_XNEGP,'Not Set')
GROUP BY arc.BSID_BELNR,arc.BSID_BUZEI,arc.BSID_ZUONR,arc.BSID_GJAHR,arc.BSID_MONAT;

UPDATE fact_accountsreceivable_temp far
SET far.Dim_ARMiscellaneousId = arc.Dim_ARMiscellaneousId
FROM tmp_upd_ar1 arc, fact_accountsreceivable_temp far
 WHERE       far.dd_AccountingDocNo = arc.BSID_BELNR
       AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
       AND far.dd_AssignmentNumber = arc.BSID_ZUONR
       AND far.dd_fiscalyear = arc.BSID_GJAHR
       AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND IFNULL(far.Dim_ARMiscellaneousId,-1) <> arc.Dim_ARMiscellaneousId;


/* Update 11 */

DROP TABLE IF EXISTS tmp_upd_ar1;
CREATE TABLE tmp_upd_ar1
AS
SELECT arc.BSAD_BELNR,arc.BSAD_BUZEI,arc.BSAD_ZUONR,arc.BSAD_GJAHR,arc.BSAD_MONAT,MAX(armisc.Dim_ARMiscellaneousId) Dim_ARMiscellaneousId
FROM fact_accountsreceivable_temp far,Dim_ARMiscellaneous armisc, BSAD arc
WHERE       far.dd_AccountingDocNo = arc.BSAD_BELNR
       AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
       AND far.dd_fiscalyear = arc.BSAD_GJAHR
       AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
       AND armisc.CustomerItemsClearingReversed = ifnull(arc.BSAD_XRAGL,'Not Set')
       AND armisc.CustomerItemsDocumentPostedYet = ifnull(arc.BSAD_XNETB,'Not Set')
       AND armisc.NegativePosting = ifnull(arc.BSAD_XNEGP,'Not Set')
GROUP BY arc.BSAD_BELNR,arc.BSAD_BUZEI,arc.BSAD_ZUONR,arc.BSAD_GJAHR,arc.BSAD_MONAT;

UPDATE fact_accountsreceivable_temp far
SET far.Dim_ARMiscellaneousId = arc.Dim_ARMiscellaneousId
FROM tmp_upd_ar1 arc,fact_accountsreceivable_temp far 
 WHERE       far.dd_AccountingDocNo = arc.BSAD_BELNR
       AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
       AND far.dd_fiscalyear = arc.BSAD_GJAHR
       AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND IFNULL(far.Dim_ARMiscellaneousId,-1) <> arc.Dim_ARMiscellaneousId;
	   

/* Update 12 */	   
	   
UPDATE    fact_accountsreceivable_temp far
   SET far.dd_ARAge =    
          (CASE
              WHEN current_date - ndt.DateValue BETWEEN 0 AND 30
              THEN
                 '1 - 30'
              WHEN current_date - ndt.DateValue BETWEEN 31 AND 60
              THEN
                 '31 - 60'
              WHEN current_date - ndt.DateValue BETWEEN 61 AND 90
              THEN
                 '61 - 90'
              WHEN current_date - ndt.DateValue > 90
              THEN
                 ' > 90 '
              WHEN current_date - ndt.DateValue < 0
              THEN
                 'Future'
              ELSE
                 'Not Set'
           END)
  FROM  dim_date ndt, fact_accountsreceivable_temp far
 WHERE far.dim_clearedflagid IN (2, 4)
 AND far.Dim_NetDueDateId = ndt.dim_dateid;
 


UPDATE fact_accountsreceivable_temp ar
SET ar.Dim_DateIdActualGIDate = sod.Dim_DateIdActualGI_Original,
    ar.dw_update_date = current_timestamp 
FROM tmp_billing_update_ar b,(SELECT sod.dd_salesdlvrdocno,sod.dd_SalesDlvrItemNo,MAX(sod.Dim_DateIdActualGI_Original) Dim_DateIdActualGI_Original
FROM fact_salesorderdelivery sod
GROUP BY sod.dd_salesdlvrdocno,sod.dd_SalesDlvrItemNo) sod,fact_accountsreceivable_temp ar
WHERE ar.dd_accountingdocno = b.dd_billing_no
AND b.dd_Salesdlvrdocno = sod.dd_salesdlvrdocno
AND b.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
AND ar.dd_InvoiceNumberTransBelongTo = 'Not Set'
AND ifnull(ar.Dim_DateIdActualGIDate,1) <> ifnull(sod.Dim_DateIdActualGI_Original,-1);

update fact_accountsreceivable_temp ar
set ar.Dim_DateIdActualGIDate = 1
where ar.Dim_DateIdActualGIDate IS NULL;

UPDATE fact_accountsreceivable_temp ar
SET ar.Dim_partid = b.dim_partid
FROM tmp_billing_update_ar b, fact_accountsreceivable_temp ar
WHERE ar.dd_accountingdocno = b.dd_billing_no
AND ar.dim_partid = 1
AND  ar.Dim_partid <> b.dim_partid;

UPDATE fact_accountsreceivable_temp ar
SET ar.Dim_SalesOrgId = b.dim_Salesorgid
FROM tmp_billing_update_ar b,fact_accountsreceivable_temp ar
WHERE ar.dd_accountingdocno = b.dd_billing_no
AND ar.dim_Salesorgid = 1
AND  ar.dim_Salesorgid <> b.dim_Salesorgid;

DROP TABLE IF EXISTS tmp_CustomerCreditLimit;

CREATE TABLE tmp_CustomerCreditLimit as
select distinct c.CUSTOMER,ifnull(a.CREDIT_LIMIT,0) as Credit_Limit,a.LIMIT_VALID_DATE,RANK() OVER (PARTITION BY c.CUSTOMER ORDER BY a.LIMIT_VALID_DATE ASC) 
  AS Rank from UKMBP_CMS_SGM a 
  inner join BUT000 b on a.PARTNER = b.PARTNER and systimestamp <= a.LIMIT_VALID_DATE
  inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID;

DELETE FROM tmp_CustomerCreditLimit WHERE Rank <> 1;

UPDATE fact_accountsreceivable_temp far
SET     dd_CreditLimit = t.Credit_Limit
FROM    dim_customer dc,
        tmp_CustomerCreditLimit  t,fact_accountsreceivable_temp far
WHERE	 dc.Dim_CustomerId = far.Dim_CustomerId
AND dc.CustomerNumber = t.customer;

UPDATE fact_accountsreceivable_temp far
SET dd_CreditLimit = 0
WHERE dd_CreditLimit IS NULL;
                                  
DROP TABLE IF EXISTS tmp_CustomerCreditLimit; 





/* Begin 06 Nov 2013 */
DROP TABLE IF EXISTS tmp_upd_702;

Create table tmp_upd_702 as Select  
(case when b.NAME_FIRST is null then b.NAME_LAST else b.NAME_FIRST || ' ' || ifnull(b.NAME_LAST,'') end) updcol1,d.BUT050_PARTNER2 updVBAK_KUNNR
from cvi_cust_link c inner join but000 b1 on c.PARTNER_GUID = b1.PARTNER_GUID
	inner join BUT050 d on b1.PARTNER = d.BUT050_PARTNER2 AND d.BUT050_RELTYP = 'UKMSB0'
	inner join but000 b on b.PARTNER = d.BUT050_PARTNER1
where  sysdate between to_date(d.BUT050_DATE_FROM) and to_date(d.BUT050_DATE_TO)
and (b.NAME_FIRST is not null or b.NAME_LAST is not null and b.BU_GROUP = 'CRED')
order by c.customer ;

update fact_accountsreceivable_temp so
Set dd_CreditRep = t.updcol1
from   tmp_upd_702 t,
       Dim_Customer c,
       fact_accountsreceivable_temp so
Where so.dim_Customerid = c.dim_customerid
  and t.updVBAK_KUNNR = c.CustomerNumber;

DROP TABLE IF EXISTS tmp_upd_702;

/* End 06 Nov 2013 */


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE fact_accountsreceivable_temp far, Dim_ARMiscellaneous armisc, BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		
 

/* Update 13 */

/* Need to break up this query, as the original update with just changes for concat results in Rewrite Error in VW */

/* This is required otherwise the tmp_dim.. creation query fails with error Invalid qualifier 'f' .. */
drop table if exists tmp_fact_accountsreceivable1;
create table tmp_fact_accountsreceivable1
as
select f.*,cast(f.dd_AccountingDocNo as varchar(100)) as char_dd_AccountingDocNo,cast(f.dd_fiscalyear as varchar(100)) as char_dd_fiscalyear,lpad(rtrim(cast(f.dd_AccountingDocItemNo as varchar(100))),3,'0') as char_dd_AccountingDocItemNo , cast(dcm.CompanyCode as varchar(100)) as char_CompanyCode
FROM fact_accountsreceivable_temp f,dim_company dcm
WHERE   dcm.Dim_CompanyId = f.Dim_CompanyId;




DROP TABLE IF EXISTS tmp_dim_company_fact_accountsreceivable;
CREATE TABLE tmp_dim_company_fact_accountsreceivable
AS
SELECT f.*,
	   (f.char_CompanyCode || f.char_dd_AccountingDocNo || f.char_dd_fiscalyear || f.char_dd_AccountingDocItemNo) as FDM_DCPROC_OBJ_KEY
FROM 	 tmp_fact_accountsreceivable1 f;

/* Added explicit commits as they were not working earlier with wrapper script */

drop table if exists tmp_scmg_t_case_attr;
create table tmp_scmg_t_case_attr
as
select a.ext_key,b.fdm_dcproc_obj_key
from scmg_t_case_attr a inner join fdm_dcproc b on a.case_guid = b.fdm_dcproc_case_guid_loc
where b.fdm_dcproc_obj_type = 'BSEG' and b.fdm_dcproc_new_class = 'DISP_RES' ;

update tmp_dim_company_fact_accountsreceivable f 
  set dd_disputecaseid = ifnull(a.ext_key,'Not Set')
                                from tmp_dim_company_fact_accountsreceivable f, tmp_scmg_t_case_attr a 
where a.fdm_dcproc_obj_key = f.fdm_dcproc_obj_key
and dd_disputecaseid <> ifnull(a.ext_key,'Not Set')
;


/* Update 14 */			

DROP TABLE IF EXISTS tmp_SCMG_T_CASE_ATTR2;
CREATE TABLE tmp_SCMG_T_CASE_ATTR2
AS
SELECT a.EXT_KEY,b.FDM_DCPROC_OBJ_KEY
from SCMG_T_CASE_ATTR a inner join FDM_DCPROC b on a.CASE_GUID = b.FDM_DCPROC_CASE_GUID_LOC
where b.FDM_DCPROC_OBJ_TYPE = 'BSEG' and b.FDM_DCPROC_NEW_CLASS in ('DISP_INV','DISP_PAY');


UPDATE tmp_dim_company_fact_accountsreceivable f 
  SET dd_DisputeCaseId = ifnull( a.EXT_KEY,'Not Set')
    from tmp_dim_company_fact_accountsreceivable f , tmp_SCMG_T_CASE_ATTR2 a 
WHERE dd_DisputeCaseId = 'Not Set'
  and a.FDM_DCPROC_OBJ_KEY = f.FDM_DCPROC_OBJ_KEY;


/* Andra changes 14 Jan 2014: Add column dd_ReasonCode*/

drop table if exists tmp_scmg_t_case_attr3;
create table tmp_scmg_t_case_attr3 as
select a.reason_code,b.fdm_dcproc_obj_key
from scmg_t_case_attr a 
inner join fdm_dcproc b 
on a.case_guid = b.fdm_dcproc_case_guid_loc;


update tmp_dim_company_fact_accountsreceivable f 
  set dd_reasoncode = ifnull(a.reason_code,'Not Set')
from tmp_dim_company_fact_accountsreceivable f, tmp_scmg_t_case_attr3 a 
where  a.fdm_dcproc_obj_key = f.fdm_dcproc_obj_key
and  dd_reasoncode <> ifnull(a.reason_code,'Not Set');

drop table if exists tmp_scmg_t_case_attr3;

/* END Andra changes 14 Jan 2014 */			

DROP TABLE IF EXISTS tmp_d;
CREATE TABLE tmp_d
AS
select a.EXT_KEY, b.FDM_DCPROC_OBJ_KEY,
                          SUM(FDM_DCPROC_DELTA_CREDITED) SUM_FDM_DCPROC_DELTA_CREDITED,
                          SUM(FDM_DCPROC_DELTA_DISPUTED) SUM_FDM_DCPROC_DELTA_DISPUTED,
                          SUM(FDM_DCPROC_DELTA_NOT_SOLVED) SUM_FDM_DCPROC_DELTA_NOT_SOLVED,
                          SUM(FDM_DCPROC_DELTA_ORIGINAL) SUM_FDM_DCPROC_DELTA_ORIGINAL,
                          SUM(FDM_DCPROC_DELTA_PAID) SUM_FDM_DCPROC_DELTA_PAID,
                          SUM(FDM_DCPROC_DELTA_WRITE_OFF) SUM_FDM_DCPROC_DELTA_WRITE_OFF
                  from SCMG_T_CASE_ATTR a inner join FDM_DCPROC b on a.CASE_GUID = b.FDM_DCPROC_CASE_GUID_LOC
                  where b.FDM_DCPROC_OBJ_TYPE = 'BSEG' and b.FDM_DCPROC_NEW_CLASS in ('DISP_INV','DISP_PAY','DISP_RES')
                  group by a.EXT_KEY, b.FDM_DCPROC_OBJ_KEY;

/*DROP TABLE tmp_dim_company_fact_accountsreceivable*/
DROP TABLE if exists tmp_fact_accountsreceivable1;
DROP TABLE if exists tmp_SCMG_T_CASE_ATTR2;
DROP TABLE if exists tmp_SCMG_T_CASE_ATTR;

 
/* Update 15 - Final Update */	

UPDATE tmp_dim_company_fact_accountsreceivable f 
SET amt_OriginalAmtDisputed = SUM_FDM_DCPROC_DELTA_ORIGINAL,
    amt_DisputedAmt = SUM_FDM_DCPROC_DELTA_DISPUTED,
    amt_DisputeAmtPaid = SUM_FDM_DCPROC_DELTA_PAID,
    amt_DisputeAmtCredited = SUM_FDM_DCPROC_DELTA_CREDITED,
    amt_DisputeAmtCleared = SUM_FDM_DCPROC_DELTA_WRITE_OFF,
    amt_DisputeAmtWrittenOff = SUM_FDM_DCPROC_DELTA_NOT_SOLVED
FROM tmp_d d, tmp_dim_company_fact_accountsreceivable f 
WHERE f.FDM_DCPROC_OBJ_KEY = d.FDM_DCPROC_OBJ_KEY
AND EXT_KEY = f.dd_DisputeCaseId;

								
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_AccountingDocNo RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_fiscalyear RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_AccountingDocItemNo RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_CompanyCode RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column FDM_DCPROC_OBJ_KEY RESTRICT;


truncate table fact_accountsreceivable
;

insert into fact_accountsreceivable
(
FACT_ACCOUNTSRECEIVABLEID,
DIM_COMPANYID,
DIM_CUSTOMERID,
DIM_SPECIALGLINDICATORID,
DIM_DATEIDCLEARING,
DD_CLEARINGDOCUMENTNO,
DD_ASSIGNMENTNUMBER,
DD_FISCALYEAR,
DD_ACCOUNTINGDOCNO,
DD_ACCOUNTINGDOCITEMNO,
DIM_DATEIDPOSTING,
DIM_DATEIDCREATED,
DIM_BUSINESSAREAID,
DD_DEBITCREDITID,
DIM_CREDITCONTROLAREAID,
DIM_DOCUMENTTYPEID,
DD_FISCALPERIOD,
DIM_CHARTOFACCOUNTSID,
DIM_CURRENCYID,
DIM_BLOCKINGPAYMENTREASONID,
DIM_PAYMENTREASONID,
DD_FIXEDPAYMENTTERMS,
DD_NETPAYMENTTERMSPERIOD,
DIM_POSTINGKEYID,
DIM_TARGETSPECIALGLINDICATORID,
AMT_CASHDISCOUNTLOCALCURRENCY,
AMT_CASHDISCOUNTDOCCURRENCY,
AMT_TAXINLOCALCURRENCY,
AMT_TAXINDOCCURRENCY,
AMT_INLOCALCURRENCY,
AMT_INDOCCURRENCY,
DD_INVOICENUMBERTRANSBELONGTO,
DIM_DATEIDBASEDATEFORDUEDATECALC,
DIM_DATEIDACCDOCDATEENTERED,
DIM_CLEAREDFLAGID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DD_SALESDOCNO,
DD_SALESITEMNO,
DD_SALESSCHEDULENO,
DIM_NETDUEDATEID,
DIM_NETDUEDATEWRTCASHDISCOUNTTERMS1,
DIM_NETDUEDATEWRTCASHDISCOUNTTERMS2,
DD_BILLINGNO,
DD_PRODUCTIONORDERNO,
DD_CASHDISCOUNTPERCENTAGE1,
DD_CASHDISCOUNTPERCENTAGE2,
DIM_SPECIALGLTRANSACTIONTYPEID,
AMT_ELIGIBLEFORDISCOUNT,
DIM_ACCOUNTSRECEIVABLEDOCSTATUSID,
DD_RELEVANTINVOICEFISCALYEAR,
DIM_ARMISCELLANEOUSID,
CT_BILL_QTYSALESUOM,
CT_BILL_QTYSTOCKUOM,
DIM_PARTID,
DIM_PLANTID,
DIM_PAYMENTMETHODID,
DIM_PAYMENTTERMSID,
DD_DUNNINGLEVEL,
CT_CASHDISCOUNTDAYS1,
DD_CUSTOMERPONUMBER,
DD_CREDITREP,
DD_CREDITLIMIT,
DIM_RISKCLASSID,
DIM_DATEIDSONEXTDATE,
DD_DISPUTECASEID,
AMT_ORIGINALAMTDISPUTED,
AMT_DISPUTEDAMT,
AMT_DISPUTEAMTPAID,
AMT_DISPUTEAMTCREDITED,
AMT_DISPUTEAMTCLEARED,
AMT_DISPUTEAMTWRITTENOFF,
DIM_SALESORGID,
DIM_DISTRIBUTIONCHANNELID,
DIM_ACCOUNTASSIGNMENTGROUPID,
DIM_SALESDOCUMENTTYPEID,
AMT_SALESORDERNETVALUE,
AMT_SALESORDERTAXAMT,
AMT_SALESORDEROPENAMTWTAX,
DIM_DATEIDSOCREATED,
DIM_DATEIDSOSCHEDDLVRREQ,
DIM_DATEIDSOSCHEDDLVR,
DD_ARAGE,
DIM_DATEIDACTUALGIDATE,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DD_CREDITMGR,
DIM_CHARTOFACCOUNTSIDARDOCGL,
DD_REASONCODE,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DIM_PROJECTSOURCEID,
DIM_SALESDIVISIONID

)
(
select distinct 
FACT_ACCOUNTSRECEIVABLEID,
DIM_COMPANYID,
DIM_CUSTOMERID,
DIM_SPECIALGLINDICATORID,
DIM_DATEIDCLEARING,
DD_CLEARINGDOCUMENTNO,
DD_ASSIGNMENTNUMBER,
DD_FISCALYEAR,
DD_ACCOUNTINGDOCNO,
DD_ACCOUNTINGDOCITEMNO,
DIM_DATEIDPOSTING,
DIM_DATEIDCREATED,
DIM_BUSINESSAREAID,
DD_DEBITCREDITID,
DIM_CREDITCONTROLAREAID,
DIM_DOCUMENTTYPEID,
DD_FISCALPERIOD,
DIM_CHARTOFACCOUNTSID,
DIM_CURRENCYID,
DIM_BLOCKINGPAYMENTREASONID,
DIM_PAYMENTREASONID,
DD_FIXEDPAYMENTTERMS,
DD_NETPAYMENTTERMSPERIOD,
DIM_POSTINGKEYID,
DIM_TARGETSPECIALGLINDICATORID,
AMT_CASHDISCOUNTLOCALCURRENCY,
AMT_CASHDISCOUNTDOCCURRENCY,
AMT_TAXINLOCALCURRENCY,
AMT_TAXINDOCCURRENCY,
AMT_INLOCALCURRENCY,
AMT_INDOCCURRENCY,
DD_INVOICENUMBERTRANSBELONGTO,
DIM_DATEIDBASEDATEFORDUEDATECALC,
DIM_DATEIDACCDOCDATEENTERED,
DIM_CLEAREDFLAGID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DD_SALESDOCNO,
DD_SALESITEMNO,
DD_SALESSCHEDULENO,
ifnull(DIM_NETDUEDATEID,1),
ifnull(DIM_NETDUEDATEWRTCASHDISCOUNTTERMS1,1),
ifnull(DIM_NETDUEDATEWRTCASHDISCOUNTTERMS2,1),
DD_BILLINGNO,
DD_PRODUCTIONORDERNO,
DD_CASHDISCOUNTPERCENTAGE1,
DD_CASHDISCOUNTPERCENTAGE2,
DIM_SPECIALGLTRANSACTIONTYPEID,
ifnull(AMT_ELIGIBLEFORDISCOUNT,0),
DIM_ACCOUNTSRECEIVABLEDOCSTATUSID,
ifnull(DD_RELEVANTINVOICEFISCALYEAR, 0),
ifnull(DIM_ARMISCELLANEOUSID,1),
ifnull(CT_BILL_QTYSALESUOM,0),
ifnull(CT_BILL_QTYSTOCKUOM,0),
ifnull(DIM_PARTID,1),
ifnull(DIM_PLANTID,1),
ifnull(DIM_PAYMENTMETHODID,1),
ifnull(DIM_PAYMENTTERMSID,1),
DD_DUNNINGLEVEL,
CT_CASHDISCOUNTDAYS1,
ifnull(DD_CUSTOMERPONUMBER, 'Not Set'),
DD_CREDITREP,
DD_CREDITLIMIT,
ifnull(DIM_RISKCLASSID,1),
ifnull(DIM_DATEIDSONEXTDATE,1),
ifnull(DD_DISPUTECASEID,1),
ifnull(AMT_ORIGINALAMTDISPUTED,0),
ifnull(AMT_DISPUTEDAMT,0),
ifnull(AMT_DISPUTEAMTPAID,0),
ifnull(AMT_DISPUTEAMTCREDITED,0),
ifnull(AMT_DISPUTEAMTCLEARED,0),
ifnull(AMT_DISPUTEAMTWRITTENOFF,0),
ifnull(DIM_SALESORGID,1),
ifnull(DIM_DISTRIBUTIONCHANNELID,1),
ifnull(DIM_ACCOUNTASSIGNMENTGROUPID,1),
ifnull(DIM_SALESDOCUMENTTYPEID,1),
ifnull(AMT_SALESORDERNETVALUE,0),
ifnull(AMT_SALESORDERTAXAMT,0),
ifnull(AMT_SALESORDEROPENAMTWTAX,0),
ifnull(DIM_DATEIDSOCREATED,1),
ifnull(DIM_DATEIDSOSCHEDDLVRREQ,1),
ifnull(DIM_DATEIDSOSCHEDDLVR,1),
ifnull(DD_ARAGE,'Not Set'),
ifnull(DIM_DATEIDACTUALGIDATE,1),
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DD_CREDITMGR,
ifnull(DIM_CHARTOFACCOUNTSIDARDOCGL,1),
ifnull(DD_REASONCODE, 'Not Set'),
current_timestamp,
ifnull(DW_UPDATE_DATE,current_timestamp),
ifnull(DIM_PROJECTSOURCEID,1),
ifnull(DIM_SALESDIVISIONID,1)
from tmp_dim_company_fact_accountsreceivable
);



/* Exchange rates : Posting dates are used for both global and local rate */
/* Populate the exchange rates for BSID data */
/* Local exchange rate */
DROP TABLE IF EXISTS TMP_UPDT_BSID;
CREATE TABLE TMP_UPDT_BSID
AS
SELECT BSID_BELNR,BSID_BUZEI,BSID_ZUONR,BSID_GJAHR,BSID_MONAT,BSID_BUKRS,BSID_WAERS,BSID_BUDAT, ROW_NUMBER() OVER(PARTITION BY BSID_BELNR,BSID_BUZEI,BSID_ZUONR,BSID_GJAHR,BSID_MONAT ORDER BY BSID_BUDAT desc) rono
FROM BSID;


UPDATE fact_accountsreceivable fap
 SET amt_ExchangeRate =  z.exchangeRate
 from TMP_UPDT_BSID arc, dim_Company dcm, tmp_getexchangerate1 z,fact_accountsreceivable fap
 WHERE     fap.dd_AccountingDocNo = arc.BSID_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSID_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSID_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSID_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSID_MONAT
       AND dcm.CompanyCode = arc.BSID_BUKRS AND dcm.RowIsCurrent = 1
        AND z.pFromCurrency  = BSID_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
        AND z.pToCurrency = dcm.currency AND z.pDate = BSID_BUDAT
        AND arc.rono = 1
        AND fap.amt_ExchangeRate <> z.exchangeRate;

DROP TABLE IF EXISTS TMP_UPDT_BSID;

/* Global rate */
   drop table if exists tmp_getexchangerate1_upd2;
    create table tmp_getexchangerate1_upd2 as    
    select distinct  pFromCurrency, fact_script_name, pToCurrency, pDate, exchangeRate,  row_number() over (order by '') rownr
    from tmp_getexchangerate1;
	
/*
UPDATE fact_accountsreceivable fap 
   SET amt_ExchangeRate_gbl =  z.exchangeRate
  from BSID arc, tmp_getexchangerate1_upd2 z, tmp_gblcurr_fap, fact_accountsreceivable fap
 WHERE     fap.dd_AccountingDocNo = arc.BSID_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSID_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSID_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSID_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSID_MONAT
        AND z.pFromCurrency  = BSID_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
        AND z.pToCurrency = pGlobalCurrency AND z.pDate = BSID_BUDAT
        AND fap.amt_ExchangeRate_GBL <> z.exchangeRate
        and rownr=1
*/

/* OP rate - OanaV: ticket BI-3284 */
UPDATE fact_accountsreceivable fap 
	SET amt_ExchangeRate_gbl = ifnull(r.exrate,1)
	FROM fact_accountsreceivable fap, dim_currency dc, csv_oprate r
	WHERE fap.dim_currencyid_tra = dc.dim_currencyid
	AND dc.CurrencyCode = r.fromc
	AND r.toc = (SELECT ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD'))
	AND ifnull(fap.amt_ExchangeRate_GBL,-1) <> ifnull(r.exrate,1);   
/* END OP rate - OanaV: ticket BI-3284 */

/* Populate the exchange rates for BSAD data*/
    
	UPDATE fact_accountsreceivable fap 
   SET amt_ExchangeRate =  z.exchangeRate
  from BSAD arc, dim_Company dcm, tmp_getexchangerate1_upd2 z, fact_accountsreceivable fap 
 WHERE     fap.dd_AccountingDocNo = arc.BSAD_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSAD_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSAD_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSAD_MONAT
       AND dcm.CompanyCode = arc.BSAD_BUKRS AND dcm.RowIsCurrent = 1
        AND z.pFromCurrency  = BSAD_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
        AND z.pToCurrency = dcm.currency AND z.pDate = BSAD_BUDAT
        AND fap.amt_ExchangeRate <> z.exchangeRate
        and  rownr=1
        ;
		
/* BI-3940 - OanaV 27 Sept 2016 */	
update fact_accountsreceivable f
	set dd_PAYMENTTERMNAME = (case when PAYMENTTERMNAME = 'Not Set' then ifnull(NAME, 'Not Set')
								else PAYMENTTERMNAME
							end)
from fact_accountsreceivable f
inner join dim_customerpaymentterms on dim_customerpaymenttermsid = dim_paymenttermsid
left outer join dim_term on TERMCODE = PAYMENTTERMCODE
where (dd_PAYMENTTERMNAME = 'Not Set' or dd_PAYMENTTERMNAME <> PAYMENTTERMNAME);
/* END BI-3940 - OanaV 27 Sept 2016 */	

/*
 UPDATE fact_accountsreceivable fap 
 SET amt_ExchangeRate_gbl =  z.exchangeRate
 from BSAD arc, tmp_getexchangerate1_upd2 z, tmp_gblcurr_fap, fact_accountsreceivable fap 
 WHERE     fap.dd_AccountingDocNo = arc.BSAD_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSAD_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSAD_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSAD_MONAT
       AND z.pFromCurrency  = BSAD_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
       AND z.pToCurrency = pGlobalCurrency AND z.pDate = BSAD_BUDAT
       AND fap.amt_ExchangeRate_gbl <> z.exchangeRate
       and  rownr=1 
*/        

/* Update BW Hierarchy */

update fact_accountsreceivable f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_accountsreceivable f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

update fact_accountsreceivable f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_accountsreceivable f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

drop table if exists tmp_getexchangerate1_upd2;

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'bi_populate_accountsreceivable_fact END',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';

/* MDG Part */

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_ACCR;
CREATE TABLE TMP_DIM_MDG_PARTID_ACCR as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;

UPDATE fact_accountsreceivable f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM TMP_DIM_MDG_PARTID_ACCR tmp, fact_accountsreceivable f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_ACCR;

/* Inv Price/Cogs */

drop table if exists tmp_inv_cogs;
create table tmp_inv_cogs
as
select
	dim_partid
	,MAX(amt_cogsfixedplanrate_emd) COGS
	,max(cast(amt_StdUnitPrice * amt_exchangerate_gbl as decimal (18,4))) stdPrice
	,SUM(cast(amt_OnHand * amt_exchangerate_gbl as decimal (18,4))) OHValue
	,SUM(ct_StockQty + ct_StockInQInsp + ct_BlockedStock + ct_StockInTransit + ct_StockInTransfer + ct_TotalRestrictedStock) Qty
	,MIN((CASE WHEN mdg_part.PPU = 0 THEN 1 ELSE mdg_part.PPU END) * (CASE WHEN mdg_part.ACT_CONV = 0 THEN 1 ELSE mdg_part.ACT_CONV END)) PPU
from fact_inventoryaging f
	inner join dim_mdg_part mdg_part on f.dim_mdg_partid = mdg_part.dim_mdg_partid
group by dim_partid;

insert into tmp_inv_cogs (dim_partid,COGS,stdPrice,OHValue,Qty,PPU)
select f.dim_partid,MAX(ifnull(c.Z_GCPLFF,0)),0,0,0,MIN((CASE WHEN mdg_part.PPU = 0 THEN 1 ELSE mdg_part.PPU END) * (CASE WHEN mdg_part.ACT_CONV = 0 THEN 1 ELSE mdg_part.ACT_CONV END))
from fact_accountsreceivable f
	inner join dim_mdg_part mdg_part on f.dim_mdg_partid = mdg_part.dim_mdg_partid
	inner join dim_part dp on f.dim_partid = dp.dim_partid
	inner join dim_company dc on f.dim_companyid = dc.dim_companyid
	left join csv_cogs c on lpad(c.PRODUCT,18,'0') = lpad(dp.partnumber,18,'0')	and right('000000' || c.Z_REPUNIT, 6) = dc.company
where f.dim_partid not in (select x.dim_partid from tmp_inv_cogs x)
group by f.dim_partid;

MERGE INTO fact_accountsreceivable f
	USING tmp_inv_cogs t ON f.dim_partid = t.dim_partid
WHEN MATCHED THEN UPDATE
	SET f.amt_invcogsprice = CASE WHEN t.COGS =0 AND t.stdPrice = 0 AND t.Qty <> 0 THEN cast(t.OHValue / t.Qty as decimal (18,4)) WHEN t.COGS =0 AND t.stdPrice <> 0 THEN t.stdPrice ELSE t.COGS END;

drop table if exists tmp_inv_cogs;

/* Inventory Value per Business Sector */

drop table if exists tmp_hc_inventory_value;
create table tmp_hc_inventory_value
as
select
	uph.BUSINESSSECTOR
	,sum(cast((CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0 THEN (f_invagng.amt_OnHand) ELSE
		((f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock)) * f_invagng.ct_baseuomratioPCcustom * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END)
		* (CASE WHEN mdg_part.PPU = 0 THEN 1 ELSE mdg_part.PPU END) * (CASE WHEN mdg_part.ACT_CONV = 0 THEN 1 ELSE mdg_part.ACT_CONV END) * amt_exchangerate_gbl as decimal (18,4))) OnHandValue
from fact_inventoryaging f_invagng
	inner join dim_mdg_part mdg_part on f_invagng.dim_mdg_partid = mdg_part.dim_mdg_partid
	inner join dim_part prt on f_invagng.dim_partid = prt.dim_partid
	inner join dim_company dc on f_invagng.dim_companyid = dc.dim_companyid
	inner join dim_bwproducthierarchy uph on f_invagng.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
where prt.partnumber||dc.company not in ('3408009060001298','5030270000001500','1046RPB.0040001046','FR21030170085001047')
group by uph.BUSINESSSECTOR;

update fact_accountsreceivable f
set f.amt_onhandvalueinv = t.OnHandValue
from fact_accountsreceivable f
	inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
	inner join tmp_hc_inventory_value t on t.BUSINESSSECTOR = uph.BUSINESSSECTOR;

drop table if exists tmp_hc_inventory_value;

/* Sales Value from Billing at Billing DOc lvl */

drop table if exists tmp_billingdoc_value;
create table tmp_billingdoc_value
as
select dd_billing_no,sum(cast(ct_BillingQtyStockUOM*amt_invcogsprice*(CASE WHEN (mdg_part.PPU) = 0 THEN 1 ELSE (mdg_part.PPU) END * CASE WHEN (mdg_part.ACT_CONV) = 0 THEN 1 ELSE (mdg_part.ACT_CONV) END) as decimal (18,4))) billingdoc_value
from fact_billing f
	inner join dim_mdg_part mdg_part on f.dim_mdg_partid = mdg_part.dim_mdg_partid
group by dd_billing_no;

drop table if exists tmp_billingdocid;
create table tmp_billingdocid
as
select dd_billingno, max(fact_accountsreceivableid) fact_accountsreceivableid
from fact_accountsreceivable f
group by dd_billingno;

merge into fact_accountsreceivable f
using (select fact_accountsreceivableid, billingdoc_value billingdoc_value from tmp_billingdocid inner join tmp_billingdoc_value on dd_billingno = dd_billing_no) x
on f.fact_accountsreceivableid = x.fact_accountsreceivableid
when matched then update set f.amt_billingdocvalue = x.billingdoc_value;
