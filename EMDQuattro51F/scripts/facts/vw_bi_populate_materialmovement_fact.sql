

/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 9 Sep 2013 */
/*   Description    : Stored Proc bi_populate_materialmovement_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   21 May 2014      Cornelia	1.0  		 Add dim_uomunitofentryid */
/*   19 May 2014      Cornelia	1.0  		 Add dim_unitofmeasureorderunitid */
/*   21 Sep 2013      Lokesh	1.2 		Performance changes - use hash function and use combine */
/*   09 Sep 2013      Lokesh	1.1           Exchange rate changes	  */
/*											  Note that amts are in local curr e.g MSEG_WAERS is same as dc.currency */
/* 											  We will take MSEG_WAERS as TRA and dim_company.currency as local curr */
/*   06 Sep 2013      Lokesh	1.0  		  Existing version migrated from CVS */
/******************************************************************************************************************/

drop table if exists pGlobalCurrency_tbl;
create table pGlobalCurrency_tbl ( pGlobalCurrency varchar(3));
drop table if exists dim_profitcenter_789;
create table dim_profitcenter_789 as select * from dim_profitcenter order by ValidTo;

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'bi_populate_materialmovement_fact START');

Insert into pGlobalCurrency_tbl values('USD');

Update pGlobalCurrency_tbl
SET pGlobalCurrency =
        ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');
                
INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'UPDATE fact_materialmovement ms');

UPDATE MKPF_MSEG 
SET MSEG_BUKRS = ifnull(MSEG_BUKRS,'Not Set')
where MSEG_BUKRS <>  ifnull(MSEG_BUKRS,'Not Set');

UPDATE MKPF_MSEG1 
SET MSEG1_BUKRS = ifnull(MSEG1_BUKRS,'Not Set') 
where  MSEG1_BUKRS <>  ifnull(MSEG1_BUKRS,'Not Set');

/* delete existing movement from fact */

drop table if exists tmp_fact_materialmovement_del;
create table tmp_fact_materialmovement_del 
as
select DISTINCT ms.fact_materialmovementid
FROM fact_materialmovement ms, MKPF_MSEG m,dim_plant dp,dim_company dc
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

ALTER TABLE tmp_fact_materialmovement_del
add  PRIMARY KEY (fact_materialmovementid);

insert into tmp_fact_materialmovement_del
select DISTINCT ms.fact_materialmovementid
FROM fact_materialmovement ms, MKPF_MSEG1 m,dim_plant dp,dim_company dc
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;



merge into fact_materialmovement fmm
using tmp_fact_materialmovement_del tfmd
on fmm.fact_materialmovementid = tfmd.fact_materialmovementid
when matched then delete;


/* LK:  Used a tmp table to convert similar updates into insert-combine */

DROP TABLE IF EXISTS tmp_fact_materialmovement_perf1;
CREATE TABLE tmp_fact_materialmovement_perf1 like fact_materialmovement INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE tmp_fact_materialmovement_perf1 ADD PRIMARY KEY (fact_materialmovementid);


drop table if exists tmp_fmatmov_t001t;
create table tmp_fmatmov_t001t as
SELECT  ms.fact_materialmovementid,
          ms.dd_MaterialDocNo,
          ms.dd_MaterialDocItemNo,
         ms.dd_MaterialDocYear,
				ifnull(m.MSEG_EBELN,'Not Set') dd_DocumentNo,
				m.MSEG_EBELP dd_DocumentItemNo,
				ifnull(m.MSEG_KDAUF,'Not Set') dd_SalesOrderNo ,
				m.MSEG_KDPOS dd_SalesOrderItemNo,
				ifnull(m.MSEG_KDEIN,0) dd_SalesOrderDlvrNo,
				ifnull(m.MSEG_SAKTO,'Not Set') as dd_GLAccountNo,
				ifnull(m.MSEG_LFBNR,'Not Set') as dd_ReferenceDocNo,
				ifnull(m.MSEG_LFPOS,0) as dd_ReferenceDocItem,
				CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END as dd_debitcreditid,
				ifnull(m.MSEG_AUFNR,'Not Set') as dd_productionordernumber,
  				m.MSEG_AUFPS as dd_productionorderitemno ,
  				m.MSEG_GRUND as dd_GoodsMoveReason,
 				ifnull(m.MSEG_CHARG, 'Not Set') as dd_BatchNumber,
  				 m.MSEG_BWTAR as dd_ValuationType ,
                               ms.amt_LocalCurrAmt,
                               ms.amt_StdUnitPrice,
			(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR as amt_DeliveryCost ,
			(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT as amt_AltPriceControl,
			 m.MSEG_SALK3 as amt_OnHand_ValStock,
        convert(decimal (18,4), 1) as amt_ExchangeRate,
       convert(decimal (18,4), 1) as amt_ExchangeRate_GBL,
	(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE as ct_Quantity,
	(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG as ct_QtyEntryUOM,
	 (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG as ct_QtyGROrdUnit ,
  		m.MSEG_LBKUM as ct_QtyOnHand_ValStock,
	(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG as ct_QtyOrdPriceUnit,
                               ms.Dim_MovementTypeid,
                               ms.Dim_Companyid,
	convert(bigint, 1) as Dim_CurrencyId, 
	convert(bigint, 1) as Dim_Partid, 
                               ms.Dim_Plantid,
	convert(bigint, 1) as Dim_StorageLocationid,
                               ms.Dim_Vendorid,
	convert(bigint, 1) as dim_DateIDMaterialDocDate, 
	convert(bigint, 1) as dim_DateIDPostingDate,
                               ms.Dim_producthierarchyid,
      convert(bigint, 1) as  Dim_MovementIndicatorid ,
                               ms.Dim_Customerid,
                               ms.Dim_CostCenterid,
      convert(bigint, 1) as  Dim_specialstockid,
      convert(bigint, 1) as  Dim_unitofmeasureid,
      convert(bigint, 1) as  Dim_controllingareaid,
     ms.Dim_profitcenterid,
    convert(bigint, 1) as   Dim_consumptiontypeid,
    convert(bigint, 1) as    Dim_stocktypeid ,
	convert(bigint, 1) as Dim_PurchaseGroupid,
	convert(bigint, 1) as Dim_materialgroupid,
     convert(bigint, 1) as   Dim_ReceivingPlantId,
     convert(bigint, 1) as   Dim_ReceivingPartId,
     convert(bigint, 1) as   Dim_RecvIssuStorLocid,
	 convert(bigint, 1) as Dim_CurrencyId_TRA , 
	convert(bigint, 1) as Dim_CurrencyId_GBL ,
ms.amt_plannedprice,
ms.amt_plannedprice1,
ms.amt_pounitprice,
ms.amt_vendorspend,
ms.ct_orderquantity,
ms.dd_consignmentflag,
ms.dd_documentscheduleno,
ms.dd_incoterms2,
ms.dd_vendorspendflag,
ms.dim_accountcategoryid,
ms.dim_afssizeid,
ms.dim_customergroup1id,
ms.dim_dateidcosting,
ms.dim_dateiddelivery,
ms.dim_dateiddoccreation,
ms.dim_dateidordered,
ms.dim_dateidstatdelivery,
ms.dim_documentcategoryid,
ms.dim_documenttypeid,
ms.dim_grstatusid,
ms.dim_incotermid,
ms.dim_inspusagedecisionid,
ms.dim_itemcategoryid,
ms.dim_itemstatusid,
ms.dim_poissustoragelocid,
ms.dim_poplantidordering,
ms.dim_poplantidsupplying,
ms.dim_postoragelocid,
ms.dim_productionorderstatusid,
ms.dim_productionordertypeid,
ms.dim_purchaseorgid,
ms.dim_salesdocumenttypeid,
ms.dim_salesgroupid,
ms.dim_salesorderheaderstatusid,
ms.dim_salesorderitemstatusid,
ms.dim_salesorderrejectreasonid,
ms.dim_salesorgid,
ms.dim_termid,
ms.dirtyrow,
ms.lotsacceptedflag,
ms.lotsawaitinginspectionflag,
ms.lotsinspectedflag,
ms.lotsrejectedflag,
ms.lotsskippedflag,
ms.percentlotsacceptedflag,
ms.percentlotsinspectedflag,
ms.percentlotsrejectedflag,
ms.qtyrejectedexternalamt,
ms.qtyrejectedinternalamt,
ms.dd_intorder,
ms.ct_lotnotthruqm,
ms.lotsreceivedflag,
m.MSEG_WAERS, --amt_ExchangeRate
m.MKPF_BUDAT,  --amt_ExchangeRate
a.pGlobalCurrency, --amt_ExchangeRate_GBL
dc.currency, --Dim_CurrencyId
m.MSEG_MATNR, --Dim_Partid
m.MSEG_WERKS,   --Dim_Partid
m.MSEG_LGORT,  --Dim_StorageLocationid
m.MSEG_BUKRS, --dim_DateIDMaterialDocDate
m.MKPF_BLDAT,  --dim_DateIDMaterialDocDate
m.MSEG_KZBEW, --Dim_MovementIndicatorid
m.MSEG_SOBKZ, --Dim_specialstockid
m.MSEG_MEINS,  --Dim_unitofmeasureid
m.MSEG_KOKRS,  --Dim_controllingareaid
m.MSEG_KZVBR, --Dim_consumptiontypeid
 m.MSEG_INSMK, --Dim_stocktypeid
m.MSEG_UMWRK, --Dim_ReceivingPlantId
m.MSEG_UMMAT, --Dim_ReceivingPartId
m.MSEG_UMLGO  --Dim_RecvIssuStorLocid
FROM fact_materialmovement ms, MKPF_MSEG m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl a
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

update tmp_fmatmov_t001t f
set   f.amt_ExchangeRate = ifnull(CASE WHEN f.MSEG_WAERS = f.Currency THEN 1.00 ELSE  z.exchangeRate end,1)
from tmp_fmatmov_t001t f
             left join (select * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_materialmovement_fact') z on
              z.pFromCurrency  = f.MSEG_WAERS and z.pToCurrency = f.Currency and z.pDate =f.MKPF_BUDAT
where f.amt_ExchangeRate <> ifnull(CASE WHEN f.MSEG_WAERS = f.Currency THEN 1.00 ELSE  z.exchangeRate end,1);

update tmp_fmatmov_t001t f
set   f.amt_ExchangeRate_GBL = ifnull(CASE WHEN f.MSEG_WAERS = f.pGlobalCurrency THEN 1.00 ELSE  z.exchangeRate end,1)
from tmp_fmatmov_t001t f
             left join (select * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_materialmovement_fact') z on
              z.pFromCurrency  = f.MSEG_WAERS and z.pToCurrency = f.pGlobalCurrency and z.pDate = current_date
where f.amt_ExchangeRate_GBL <> ifnull(CASE WHEN f.MSEG_WAERS = f.pGlobalCurrency THEN 1.00 ELSE  z.exchangeRate end,1);

update tmp_fmatmov_t001t f
set   f.Dim_CurrencyId = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t001t f
             left join dim_currency dcr on
              dcr.CurrencyCode = f.currency
where  f.Dim_CurrencyId <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t001t f
set   f.Dim_Partid = ifnull(dpr.Dim_Partid,1)
from tmp_fmatmov_t001t f
             left join Dim_Part dpr on
              dpr.PartNumber = f.MSEG_MATNR  AND dpr.Plant = f.MSEG_WERKS
where  f.Dim_Partid <> ifnull(dpr.Dim_Partid,1);

update tmp_fmatmov_t001t f
set   f.Dim_StorageLocationid = ifnull(dsl.Dim_StorageLocationid,1)
from tmp_fmatmov_t001t f
             left join dim_storagelocation dsl on
              dsl.LocationCode = f.MSEG_LGORT AND dsl.Plant = f.MSEG_WERKS
where  f.Dim_StorageLocationid <> ifnull(dsl.Dim_StorageLocationid,1);

update tmp_fmatmov_t001t f
set   f.dim_DateIDMaterialDocDate = ifnull(mdd.Dim_Dateid,1)
from tmp_fmatmov_t001t f
             left join dim_date mdd  on
              mdd.CompanyCode = f.MSEG_BUKRS AND f.MKPF_BLDAT = mdd.DateValue
where  f.dim_DateIDMaterialDocDate <> ifnull(mdd.Dim_Dateid,1);

update tmp_fmatmov_t001t f
set   f.dim_DateIDPostingDate = ifnull(pd.Dim_Dateid,1)
from tmp_fmatmov_t001t f
             left join dim_date pd  on
             pd.CompanyCode = f.MSEG_BUKRS AND f.MKPF_BUDAT = pd.DateValue
where  f.dim_DateIDPostingDate <> ifnull(pd.Dim_Dateid,1);

update tmp_fmatmov_t001t f
set   f.Dim_MovementIndicatorid = ifnull(dmi.Dim_MovementIndicatorid,1)
from tmp_fmatmov_t001t f
             left join dim_movementindicator dmi  on
             dmi.TypeCode = f.MSEG_KZBEW
where  f.Dim_MovementIndicatorid <> ifnull(dmi.Dim_MovementIndicatorid,1);

update tmp_fmatmov_t001t f
set   f.Dim_specialstockid = ifnull(spt.dim_specialstockid,1)
from tmp_fmatmov_t001t f
             left join dim_specialstock spt  on
             spt.specialstockindicator = f.MSEG_SOBKZ
where  f.Dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

update tmp_fmatmov_t001t f
set   f.Dim_unitofmeasureid = ifnull(uom.Dim_unitofmeasureid,1)
from tmp_fmatmov_t001t f
             left join dim_unitofmeasure uom  on
             uom.UOM = f.MSEG_MEINS
where  f.Dim_unitofmeasureid <> ifnull(uom.Dim_unitofmeasureid,1);

update tmp_fmatmov_t001t f
set   f.Dim_controllingareaid = ifnull(ca.Dim_controllingareaid,1)
from tmp_fmatmov_t001t f
             left join dim_controllingarea ca  on
             ca.ControllingAreaCode = f.MSEG_KOKRS
where f.Dim_controllingareaid <> ifnull(ca.Dim_controllingareaid,1);

update tmp_fmatmov_t001t f
set   f.Dim_consumptiontypeid = ifnull(ct.Dim_consumptiontypeid,1)
from tmp_fmatmov_t001t f
             left join dim_consumptiontype ct  on
             ct.ConsumptionCode = f.MSEG_KZVBR
where f.Dim_consumptiontypeid <> ifnull(ct.Dim_consumptiontypeid,1);

update tmp_fmatmov_t001t f
set   f.Dim_stocktypeid = ifnull(st.Dim_StockTypeid,1)
from tmp_fmatmov_t001t f
             left join dim_stocktype st  on
             st.TypeCode = f.MSEG_INSMK
where  f.Dim_stocktypeid <> ifnull(st.Dim_StockTypeid,1);

update tmp_fmatmov_t001t f
set   f.Dim_PurchaseGroupid = ifnull(pg1.Dim_PurchaseGroupid,1)
from tmp_fmatmov_t001t f
             left join 
          (select dpr.PartNumber,dpr.Plant,pg.Dim_PurchaseGroupid FROM dim_part dpr 
              inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup) pg1
         on pg1.PartNumber = f.MSEG_MATNR AND pg1.Plant = f.MSEG_WERKS
where  f.Dim_PurchaseGroupid <> ifnull(pg1.Dim_PurchaseGroupid,1);

update tmp_fmatmov_t001t f
set   f.Dim_materialgroupid = ifnull(mg1.Dim_materialgroupid,1)
from tmp_fmatmov_t001t f
             left join 
          (select dpr.PartNumber,dpr.Plant,mg.Dim_materialgroupid FROM dim_part dpr 
              inner join dim_materialgroup mg on mg.MaterialGroupCode = dpr.MaterialGroup) mg1
         on mg1.PartNumber = f.MSEG_MATNR AND mg1.Plant = f.MSEG_WERKS
where  f.Dim_materialgroupid <> ifnull(mg1.Dim_materialgroupid,1);


update tmp_fmatmov_t001t f
set   f.Dim_ReceivingPlantId = ifnull(pl.dim_plantid,1)
from tmp_fmatmov_t001t f
             left join dim_plant pl  on
             pl.PlantCode = f.MSEG_UMWRK
where f.Dim_ReceivingPlantId <> ifnull(pl.dim_plantid,1);

update tmp_fmatmov_t001t f
set   f.Dim_ReceivingPartId = ifnull(prt.Dim_Partid,1)
from tmp_fmatmov_t001t f
             left join dim_part prt
            on prt.Plant = f.MSEG_UMWRK AND prt.PartNumber = f.MSEG_UMMAT
where f.Dim_ReceivingPartId <> ifnull(prt.Dim_Partid,1);

update tmp_fmatmov_t001t f
set   f.Dim_RecvIssuStorLocid = ifnull(dsl.Dim_StorageLocationid,1)
from tmp_fmatmov_t001t f
             left join dim_storagelocation dsl
            on dsl.LocationCode = f.MSEG_UMLGO AND dsl.Plant = f.MSEG_UMWRK
where f.Dim_RecvIssuStorLocid <> ifnull(dsl.Dim_StorageLocationid,1);

update tmp_fmatmov_t001t f
set   f.Dim_CurrencyId_TRA = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t001t f
             left join dim_currency dcr
            on dcr.CurrencyCode = f.MSEG_WAERS
where f.Dim_CurrencyId_TRA <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t001t f
set   f.Dim_CurrencyId_GBL = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t001t f
             left join dim_currency dcr
            on dcr.CurrencyCode = pGlobalCurrency
where f.Dim_CurrencyId_GBL <> ifnull(dcr.Dim_Currencyid,1);


  INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
				dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
/* These columns are not actually being updated. But need to keep the values. So they are needed in combine */
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
dd_consignmentflag,
dd_documentscheduleno,
dd_incoterms2,
dd_vendorspendflag,
dim_accountcategoryid,
dim_afssizeid,
dim_customergroup1id,
dim_dateidcosting,
dim_dateiddelivery,
dim_dateiddoccreation,
dim_dateidordered,
dim_dateidstatdelivery,
dim_documentcategoryid,
dim_documenttypeid,
dim_grstatusid,
dim_incotermid,
dim_inspusagedecisionid,
dim_itemcategoryid,
dim_itemstatusid,
dim_poissustoragelocid,
dim_poplantidordering,
dim_poplantidsupplying,
dim_postoragelocid,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_purchaseorgid,
dim_salesdocumenttypeid,
dim_salesgroupid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_salesorderrejectreasonid,
dim_salesorgid,
dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
dd_intorder,
ct_lotnotthruqm,
lotsreceivedflag)
select distinct  fact_materialmovementid,
                        dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
				dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
/* These columns are not actually being updated. But need to keep the values. So they are needed in combine */
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
dd_consignmentflag,
dd_documentscheduleno,
dd_incoterms2,
dd_vendorspendflag,
dim_accountcategoryid,
dim_afssizeid,
dim_customergroup1id,
dim_dateidcosting,
dim_dateiddelivery,
dim_dateiddoccreation,
dim_dateidordered,
dim_dateidstatdelivery,
dim_documentcategoryid,
dim_documenttypeid,
dim_grstatusid,
dim_incotermid,
dim_inspusagedecisionid,
dim_itemcategoryid,
dim_itemstatusid,
dim_poissustoragelocid,
dim_poplantidordering,
dim_poplantidsupplying,
dim_postoragelocid,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_purchaseorgid,
dim_salesdocumenttypeid,
dim_salesgroupid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_salesorderrejectreasonid,
dim_salesorgid,
dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
dd_intorder,
ct_lotnotthruqm,
lotsreceivedflag
from tmp_fmatmov_t001t;

/*call vectorwise(combine 'fact_materialmovement+tmp_fact_materialmovement_perf1')*/
 
insert into  fact_materialmovement
(LOTSACCEPTEDFLAG,
LOTSREJECTEDFLAG,
PERCENTLOTSINSPECTEDFLAG,
PERCENTLOTSREJECTEDFLAG,
PERCENTLOTSACCEPTEDFLAG,
LOTSSKIPPEDFLAG,
LOTSAWAITINGINSPECTIONFLAG,
LOTSINSPECTEDFLAG,
LOTSRECEIVEDFLAG,
DD_MATERIALDOCNO,
DD_DOCUMENTNO,
DD_SALESORDERNO,
DD_GLACCOUNTNO,
DD_PRODUCTIONORDERNUMBER,
DD_BATCHNUMBER,
DD_VALUATIONTYPE,
DD_DEBITCREDITID,
DD_INCOTERMS2,
DD_INTORDER,
DD_REFERENCEDOCNO,
DD_INSPECTIONSTATUSINGRDOC,
DD_ITEMAUTOMATICALLYCREATED,
DD_MATERIALDOCITEMNO,
DD_MATERIALDOCYEAR,
DD_GOODSMOVEREASON,
AMT_LOCALCURRAMT,
AMT_DELIVERYCOST,
AMT_ALTPRICECONTROL,
AMT_ONHAND_VALSTOCK,
CT_QUANTITY,
CT_QTYENTRYUOM,
CT_QTYGRORDUNIT,
CT_QTYONHAND_VALSTOCK,
CT_QTYORDPRICEUNIT,
DIRTYROW,
DD_CONSIGNMENTFLAG,
AMT_POUNITPRICE,
AMT_STDUNITPRICE,
AMT_PLANNEDPRICE,
AMT_PLANNEDPRICE1,
DD_DOCUMENTSCHEDULENO,
CT_ORDERQUANTITY,
DD_REFERENCEDOCITEM,
QTYREJECTEDEXTERNALAMT,
QTYREJECTEDINTERNALAMT,
DD_VENDORSPENDFLAG,
DD_RESERVATIONNUMBER,
DIM_MOVEMENTTYPEID,
DIM_COMPANYID,
DIM_CURRENCYID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_VENDORID,
DIM_DATEIDMATERIALDOCDATE,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCCREATION,
DIM_DATEIDORDERED,
DIM_PRODUCTHIERARCHYID,
DIM_MOVEMENTINDICATORID,
DIM_CUSTOMERID,
DIM_COSTCENTERID,
DIM_ACCOUNTCATEGORYID,
DIM_CONSUMPTIONTYPEID,
DIM_CONTROLLINGAREAID,
DIM_CUSTOMERGROUP1ID,
DIM_DOCUMENTCATEGORYID,
DIM_DOCUMENTTYPEID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEORGID,
DIM_SALESDOCUMENTTYPEID,
DIM_SALESGROUPID,
DIM_SALESORDERHEADERSTATUSID,
DIM_SALESORDERITEMSTATUSID,
DIM_SALESORDERREJECTREASONID,
DIM_SALESORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKTYPEID,
DIM_UNITOFMEASUREID,
DIM_MATERIALGROUPID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_TERMID,
DIM_PROFITCENTERID,
DIM_RECEIVINGPLANTID,
DIM_RECEIVINGPARTID,
DIM_DATEIDCOSTING,
DIM_INCOTERMID,
DIM_DATEIDDELIVERY,
DIM_DATEIDSTATDELIVERY,
DIM_GRSTATUSID,
DIM_RECVISSUSTORLOCID,
DIM_POPLANTIDORDERING,
DIM_POPLANTIDSUPPLYING,
DIM_POSTORAGELOCID,
DIM_POISSUSTORAGELOCID,
DIM_INSPUSAGEDECISIONID,
DIM_AFSSIZEID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DIM_UNITOFMEASUREORDERUNITID,
DIM_UOMUNITOFENTRYID,
DIM_PROJECTSOURCEID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_DOCUMENTITEMNO,
DD_SALESORDERITEMNO,
DD_SALESORDERDLVRNO,
DD_PRODUCTIONORDERITEMNO,
CT_LOTNOTTHRUQM,
AMT_VENDORSPEND,
FACT_MATERIALMOVEMENTID) 
select IFNULL(ftmp.LOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSSKIPPEDFLAG,'N'),
IFNULL(ftmp.LOTSAWAITINGINSPECTIONFLAG,'N'),
IFNULL(ftmp.LOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.LOTSRECEIVEDFLAG,'N'),
IFNULL(ftmp.DD_MATERIALDOCNO,'Not Set'),
IFNULL(ftmp.DD_DOCUMENTNO,'Not Set'),
IFNULL(ftmp.DD_SALESORDERNO,'Not Set'),
IFNULL(ftmp.DD_GLACCOUNTNO,'Not Set'),
IFNULL(ftmp.DD_PRODUCTIONORDERNUMBER,'Not Set'),
IFNULL(ftmp.DD_BATCHNUMBER,'Not Set'),
IFNULL(ftmp.DD_VALUATIONTYPE,'Not Set'),
IFNULL(ftmp.DD_DEBITCREDITID,'Not Set'),
IFNULL(ftmp.DD_INCOTERMS2,'Not Set'),
IFNULL(ftmp.DD_INTORDER,'Not Set'),
IFNULL(ftmp.DD_REFERENCEDOCNO,'Not Set'),
IFNULL(ftmp.DD_INSPECTIONSTATUSINGRDOC,'Not Set'),
IFNULL(ftmp.DD_ITEMAUTOMATICALLYCREATED,'Not Set'),
IFNULL(ftmp.DD_MATERIALDOCITEMNO,0),
IFNULL(ftmp.DD_MATERIALDOCYEAR,0),
IFNULL(ftmp.DD_GOODSMOVEREASON,0),
IFNULL(ftmp.AMT_LOCALCURRAMT,0),
IFNULL(ftmp.AMT_DELIVERYCOST,0),
IFNULL(ftmp.AMT_ALTPRICECONTROL,0),
IFNULL(ftmp.AMT_ONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QUANTITY,0),
IFNULL(ftmp.CT_QTYENTRYUOM,0),
IFNULL(ftmp.CT_QTYGRORDUNIT,0),
IFNULL(ftmp.CT_QTYONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QTYORDPRICEUNIT,0),
IFNULL(ftmp.DIRTYROW,0),
IFNULL(ftmp.DD_CONSIGNMENTFLAG,0),
IFNULL(ftmp.AMT_POUNITPRICE,0),
IFNULL(ftmp.AMT_STDUNITPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE1,0),
IFNULL(ftmp.DD_DOCUMENTSCHEDULENO,0),
IFNULL(ftmp.CT_ORDERQUANTITY,0),
IFNULL(ftmp.DD_REFERENCEDOCITEM,0),
IFNULL(ftmp.QTYREJECTEDEXTERNALAMT,0),
IFNULL(ftmp.QTYREJECTEDINTERNALAMT,0),
IFNULL(ftmp.DD_VENDORSPENDFLAG,0),
IFNULL(ftmp.DD_RESERVATIONNUMBER,0),
IFNULL(ftmp.DIM_MOVEMENTTYPEID,1),
IFNULL(ftmp.DIM_COMPANYID,1),
IFNULL(ftmp.DIM_CURRENCYID,1),
IFNULL(ftmp.DIM_PARTID,1),
IFNULL(ftmp.DIM_PLANTID,1),
IFNULL(ftmp.DIM_STORAGELOCATIONID,1),
IFNULL(ftmp.DIM_VENDORID,1),
IFNULL(ftmp.DIM_DATEIDMATERIALDOCDATE,1),
IFNULL(ftmp.DIM_DATEIDPOSTINGDATE,1),
IFNULL(ftmp.DIM_DATEIDDOCCREATION,1),
IFNULL(ftmp.DIM_DATEIDORDERED,1),
IFNULL(ftmp.DIM_PRODUCTHIERARCHYID,1),
IFNULL(ftmp.DIM_MOVEMENTINDICATORID,1),
IFNULL(ftmp.DIM_CUSTOMERID,1),
IFNULL(ftmp.DIM_COSTCENTERID,1),
IFNULL(ftmp.DIM_ACCOUNTCATEGORYID,1),
IFNULL(ftmp.DIM_CONSUMPTIONTYPEID,1),
IFNULL(ftmp.DIM_CONTROLLINGAREAID,1),
IFNULL(ftmp.DIM_CUSTOMERGROUP1ID,1),
IFNULL(ftmp.DIM_DOCUMENTCATEGORYID,1),
IFNULL(ftmp.DIM_DOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_ITEMCATEGORYID,1),
IFNULL(ftmp.DIM_ITEMSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERTYPEID,1),
IFNULL(ftmp.DIM_PURCHASEGROUPID,1),
IFNULL(ftmp.DIM_PURCHASEORGID,1),
IFNULL(ftmp.DIM_SALESDOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_SALESGROUPID,1),
IFNULL(ftmp.DIM_SALESORDERHEADERSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERITEMSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERREJECTREASONID,1),
IFNULL(ftmp.DIM_SALESORGID,1),
IFNULL(ftmp.DIM_SPECIALSTOCKID,1),
IFNULL(ftmp.DIM_STOCKTYPEID,1),
IFNULL(ftmp.DIM_UNITOFMEASUREID,1),
IFNULL(ftmp.DIM_MATERIALGROUPID,1),
IFNULL(ftmp.AMT_EXCHANGERATE_GBL,1),
IFNULL(ftmp.AMT_EXCHANGERATE,1),
IFNULL(ftmp.DIM_TERMID,1),
IFNULL(ftmp.DIM_PROFITCENTERID,1),
IFNULL(ftmp.DIM_RECEIVINGPLANTID,1),
IFNULL(ftmp.DIM_RECEIVINGPARTID,1),
IFNULL(ftmp.DIM_DATEIDCOSTING,1),
IFNULL(ftmp.DIM_INCOTERMID,1),
IFNULL(ftmp.DIM_DATEIDDELIVERY,1),
IFNULL(ftmp.DIM_DATEIDSTATDELIVERY,1),
IFNULL(ftmp.DIM_GRSTATUSID,1),
IFNULL(ftmp.DIM_RECVISSUSTORLOCID,1),
IFNULL(ftmp.DIM_POPLANTIDORDERING,1),
IFNULL(ftmp.DIM_POPLANTIDSUPPLYING,1),
IFNULL(ftmp.DIM_POSTORAGELOCID,1),
IFNULL(ftmp.DIM_POISSUSTORAGELOCID,1),
IFNULL(ftmp.DIM_INSPUSAGEDECISIONID,1),
IFNULL(ftmp.DIM_AFSSIZEID,1),
IFNULL(ftmp.DIM_CURRENCYID_TRA,1),
IFNULL(ftmp.DIM_CURRENCYID_GBL,1),
IFNULL(ftmp.DIM_UNITOFMEASUREORDERUNITID,1),
IFNULL(ftmp.DIM_UOMUNITOFENTRYID,1),
IFNULL(ftmp.DIM_PROJECTSOURCEID,1),
IFNULL(ftmp.DW_INSERT_DATE,CURRENT_TIMESTAMP),
IFNULL(ftmp.DW_UPDATE_DATE,CURRENT_TIMESTAMP),
ftmp.DD_DOCUMENTITEMNO,
ftmp.DD_SALESORDERITEMNO,
ftmp.DD_SALESORDERDLVRNO,
ftmp.DD_PRODUCTIONORDERITEMNO,
ftmp.CT_LOTNOTTHRUQM,
ftmp.AMT_VENDORSPEND,
ftmp.FACT_MATERIALMOVEMENTID
 from fact_materialmovement  f
right join tmp_fact_materialmovement_perf1 ftmp                /*only new rec in PERF1*/ 
on f.fact_materialmovementid = ftmp.fact_materialmovementid
where f.fact_materialmovementid is  null;

  UPDATE fact_materialmovement ms
  SET     ms.dim_profitcenterid = ifnull(pc.Dim_ProfitCenterid,1)
 from fact_materialmovement ms 
          inner join MKPF_MSEG m
                  on ms.dd_MaterialDocNo = m.MSEG_MBLNR 
                 AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE 
                 AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
          inner join dim_plant dp
                  on m.MSEG_WERKS = dp.PlantCode
          inner join dim_company dc 
                  on m.MSEG_BUKRS = dc.CompanyCode
           left join dim_profitcenter_789 pc
                 on pc.ControllingArea = m.MSEG_KOKRS
                      AND pc.ProfitCenterCode = m.MSEG_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT
where  ms.dim_profitcenterid <> ifnull(pc.Dim_ProfitCenterid,1);



         UPDATE fact_materialmovement ms     
  SET     amt_LocalCurrAmt = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR * ms.amt_ExchangeRate
          from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
		  fact_materialmovement ms 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND amt_LocalCurrAmt <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR * ms.amt_ExchangeRate;

         UPDATE fact_materialmovement ms     
  SET     amt_StdUnitPrice = m.MSEG_DMBTR / (case when m.MSEG_ERFMG=0 then null else m.MSEG_ERFMG end)	
         from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
		  fact_materialmovement ms  
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND amt_StdUnitPrice <> ( m.MSEG_DMBTR / (case when m.MSEG_ERFMG=0 then null else m.MSEG_ERFMG end));







 UPDATE fact_materialmovement ms
  SET     Dim_Vendorid = 1
  from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
		  fact_materialmovement ms
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_LIFNR is null;


        UPDATE fact_materialmovement ms
  SET     ms.Dim_Vendorid = dv.Dim_Vendorid
   from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_vendor dv,
    fact_materialmovement ms
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  dv.VendorNumber = m.MSEG_LIFNR
	AND ms.Dim_Vendorid <> dv.Dim_Vendorid;


  UPDATE fact_materialmovement ms
  SET     ms.dim_producthierarchyid = ifnull(dpr1.dim_producthierarchyid, 1)
        from fact_materialmovement ms 
                 inner join MKPF_MSEG m
                         on ms.dd_MaterialDocNo = m.MSEG_MBLNR
                        AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                        AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
                 inner join   dim_plant dp
                        on m.MSEG_WERKS = dp.PlantCode
                 inner join dim_company dc 
                        on m.MSEG_BUKRS = dc.CompanyCode
                 left join (SELECT dpr.PartNumber,dpr.Plant,dph.dim_producthierarchyid
                           FROM dim_producthierarchy dph, dim_part dpr
                           WHERE     dph.ProductHierarchy = dpr.ProductHierarchy) dpr1
                        on dpr1.PartNumber = m.MSEG_MATNR
                         AND dpr1.Plant = m.MSEG_WERKS
 where  ms.dim_producthierarchyid <> ifnull(dpr1.dim_producthierarchyid, 1);

		  
/* Updates from MSEG1 */		  

truncate table tmp_fact_materialmovement_perf1;


/* Create denorm table for insert */
DROP TABLE IF EXISTS tmp_denorm_fact_mm_mseg1;
create table tmp_denorm_fact_mm_mseg1
as
select distinct ms.*,m.*,dc.Currency dc_currency,pGlobalCurrency
 FROM fact_materialmovement ms, MKPF_MSEG1 m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
 AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;



 drop table if exists tmp_fmatmov_t002t;
create table tmp_fmatmov_t002t as
SELECT 			ms.fact_materialmovementid,
                      ms.dd_MaterialDocNo,
                               ms.dd_MaterialDocItemNo,
                               ms.dd_MaterialDocYear,
				ifnull(ms.MSEG1_EBELN,'Not Set') dd_DocumentNo,
				ms.MSEG1_EBELP dd_DocumentItemNo,
				ifnull(ms.MSEG1_KDAUF,'Not Set') dd_SalesOrderNo ,
				ms.MSEG1_KDPOS dd_SalesOrderItemNo,
				ifnull(ms.MSEG1_KDEIN,0) as dd_SalesOrderDlvrNo,
				ifnull(ms.MSEG1_SAKTO,'Not Set') as dd_GLAccountNo,
				ifnull(ms.MSEG1_LFBNR,'Not Set') as dd_ReferenceDocNo,
				ifnull(ms.MSEG1_LFPOS,0) as dd_ReferenceDocItem,
				CASE WHEN ms.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END as dd_debitcreditid,
				ifnull(ms.MSEG1_AUFNR,'Not Set') as dd_productionordernumber,
  				ms.MSEG1_AUFPS  as dd_productionorderitemno,
  				ms.MSEG1_GRUND as dd_GoodsMoveReason,
 				ifnull(ms.MSEG1_CHARG, 'Not Set') as dd_BatchNumber,
  				ms.MSEG1_BWTAR as  dd_ValuationType,
               ms.amt_LocalCurrAmt,
	   		   ms.MSEG1_DMBTR / (case when ms.MSEG1_ERFMG=0 then null else ms.MSEG1_ERFMG end) as amt_StdUnitPrice,
     			(CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BNBTR as amt_DeliveryCost,
			    (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BUALT as amt_AltPriceControl ,
			   ms.MSEG1_SALK3 amt_OnHand_ValStock,
        convert(decimal (18,4), 1) as amt_ExchangeRate,
        	convert(decimal (18,4), 1) as amt_ExchangeRate_GBL,
				(CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_MENGE as ct_Quantity ,
				 (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_ERFMG as ct_QtyEntryUOM ,
				(CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BSTMG  as ct_QtyGROrdUnit,
  				ms.MSEG1_LBKUM  ct_QtyOnHand_ValStock,
				(CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BPMNG ct_QtyOrdPriceUnit,
                               ms.Dim_MovementTypeid,
                               ms.Dim_Companyid,
	convert(bigint, 1) as Dim_CurrencyId, 
	convert(bigint, 1) as Dim_Partid, 
                               ms.Dim_Plantid,
	convert(bigint, 1) as Dim_StorageLocationid,
	convert(bigint, 1) as Dim_Vendorid,						                               
	convert(bigint, 1) as dim_DateIDMaterialDocDate, 
	convert(bigint, 1) as dim_DateIDPostingDate,
                               ms.Dim_producthierarchyid,
       convert(bigint, 1) as  Dim_MovementIndicatorid ,
                               ms.Dim_Customerid,
                               ms.Dim_CostCenterid,
       convert(bigint, 1) as  Dim_specialstockid,
      convert(bigint, 1) as  Dim_unitofmeasureid,
      convert(bigint, 1) as  Dim_controllingareaid,
                          	ms.Dim_profitcenterid,
        convert(bigint, 1) as   Dim_consumptiontypeid,
    convert(bigint, 1) as    Dim_stocktypeid ,
	convert(bigint, 1) as Dim_PurchaseGroupid,
	convert(bigint, 1) as Dim_materialgroupid,
     convert(bigint, 1) as   Dim_ReceivingPlantId,
     convert(bigint, 1) as   Dim_ReceivingPartId,
     convert(bigint, 1) as   Dim_RecvIssuStorLocid,
	 convert(bigint, 1) as Dim_CurrencyId_TRA , 
	convert(bigint, 1) as Dim_CurrencyId_GBL ,
ms.amt_plannedprice,
ms.amt_plannedprice1,
ms.amt_pounitprice,
ms.amt_vendorspend,
ms.ct_orderquantity,
ms.dd_consignmentflag,
ms.dd_documentscheduleno,
ms.dd_incoterms2,
ms.dd_vendorspendflag,
ms.dim_accountcategoryid,
ms.dim_afssizeid,
ms.dim_customergroup1id,
ms.dim_dateidcosting,
ms.dim_dateiddelivery,
ms.dim_dateiddoccreation,
ms.dim_dateidordered,
ms.dim_dateidstatdelivery,
ms.dim_documentcategoryid,
ms.dim_documenttypeid,
ms.dim_grstatusid,
ms.dim_incotermid,
ms.dim_inspusagedecisionid,
ms.dim_itemcategoryid,
ms.dim_itemstatusid,
ms.dim_poissustoragelocid,
ms.dim_poplantidordering,
ms.dim_poplantidsupplying,
ms.dim_postoragelocid,
ms.dim_productionorderstatusid,
ms.dim_productionordertypeid,
ms.dim_purchaseorgid,
ms.dim_salesdocumenttypeid,
ms.dim_salesgroupid,
ms.dim_salesorderheaderstatusid,
ms.dim_salesorderitemstatusid,
ms.dim_salesorderrejectreasonid,
ms.dim_salesorgid,
ms.dim_termid,
ms.dirtyrow,
ms.lotsacceptedflag,
ms.lotsawaitinginspectionflag,
ms.lotsinspectedflag,
ms.lotsrejectedflag,
ms.lotsskippedflag,
ms.percentlotsacceptedflag,
ms.percentlotsinspectedflag,
ms.percentlotsrejectedflag,
ms.qtyrejectedexternalamt,
ms.qtyrejectedinternalamt,
				ms.dd_intorder,
				ms.ct_lotnotthruqm,
				ms.lotsreceivedflag,
ms.MSEG1_WAERS, --amt_ExchangeRate
ms.MKPF_BUDAT,  --amt_ExchangeRate
ms.pGlobalCurrency, --amt_ExchangeRate_GBL
ms.dc_currency, --Dim_CurrencyId
ms.MSEG1_MATNR, --Dim_Partid
ms.MSEG1_WERKS,   --Dim_Partid
ms.MSEG1_LGORT,  --Dim_StorageLocationid
ms.MSEG1_LIFNR,  --Dim_Vendorid
ms.MSEG1_BUKRS, --dim_DateIDMaterialDocDate
ms.MKPF_BLDAT,  --dim_DateIDMaterialDocDate
ms.MSEG1_KZBEW, --Dim_MovementIndicatorid
ms.MSEG1_SOBKZ, --Dim_specialstockid
ms.MSEG1_MEINS,  --Dim_unitofmeasureid
ms.MSEG1_KOKRS,  --Dim_controllingareaid
ms.MSEG1_KZVBR, --Dim_consumptiontypeid
 ms.MSEG1_INSMK, --Dim_stocktypeid
ms.MSEG1_UMWRK, --Dim_ReceivingPlantId
ms.MSEG1_UMMAT, --Dim_ReceivingPartId
ms.MSEG1_UMLGO  --Dim_RecvIssuStorLocid
FROM tmp_denorm_fact_mm_mseg1 ms;

update tmp_fmatmov_t002t f
set   f.amt_ExchangeRate = ifnull(CASE WHEN f.MSEG1_WAERS = f.dc_currency THEN 1.00 ELSE  z.exchangeRate end,1)
from tmp_fmatmov_t002t f
             left join (select * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_materialmovement_fact') z on
              z.pFromCurrency  = f.MSEG1_WAERS and z.pToCurrency = f.dc_currency and z.pDate =f.MKPF_BUDAT
where f.amt_ExchangeRate <> ifnull(CASE WHEN f.MSEG1_WAERS = f.dc_currency THEN 1.00 ELSE  z.exchangeRate end,1);

update tmp_fmatmov_t002t f
set   f.amt_ExchangeRate_GBL = ifnull(CASE WHEN f.MSEG1_WAERS = f.pGlobalCurrency THEN 1.00 ELSE  z.exchangeRate end,1)
from tmp_fmatmov_t002t f
             left join (select * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_materialmovement_fact') z on
              z.pFromCurrency  = f.MSEG1_WAERS and z.pToCurrency = f.pGlobalCurrency and z.pDate = current_date
where f.amt_ExchangeRate_GBL <> ifnull(CASE WHEN f.MSEG1_WAERS = f.pGlobalCurrency THEN 1.00 ELSE  z.exchangeRate end,1);

update tmp_fmatmov_t002t f
set   f.Dim_CurrencyId = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t002t f
             left join dim_currency dcr on
              dcr.CurrencyCode = f.dc_currency
where  f.Dim_CurrencyId <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t002t f
set   f.Dim_Partid = ifnull(dpr.Dim_Partid,1)
from tmp_fmatmov_t002t f
             left join Dim_Part dpr on
              dpr.PartNumber = f.MSEG1_MATNR  AND dpr.Plant = f.MSEG1_WERKS
where  f.Dim_Partid <> ifnull(dpr.Dim_Partid,1);

update tmp_fmatmov_t002t f
set   f.Dim_StorageLocationid = ifnull(dsl.Dim_StorageLocationid,1)
from tmp_fmatmov_t002t f
             left join dim_storagelocation dsl on
              dsl.LocationCode = f.MSEG1_LGORT AND dsl.Plant = f.MSEG1_WERKS
where  f.Dim_StorageLocationid <> ifnull(dsl.Dim_StorageLocationid,1);

update tmp_fmatmov_t002t f
set   f.Dim_Vendorid = ifnull(dv.Dim_Vendorid,1)
from tmp_fmatmov_t002t f
             left join Dim_Vendor dv
            on dv.VendorNumber = f.MSEG1_LIFNR
where f.Dim_Vendorid <> ifnull(dv.Dim_Vendorid,1);

update tmp_fmatmov_t002t f
set   f.dim_DateIDMaterialDocDate = ifnull(mdd.Dim_Dateid,1)
from tmp_fmatmov_t002t f
             left join dim_date mdd  on
              mdd.CompanyCode = f.MSEG1_BUKRS AND f.MKPF_BLDAT = mdd.DateValue
where  f.dim_DateIDMaterialDocDate <> ifnull(mdd.Dim_Dateid,1);

update tmp_fmatmov_t002t f
set   f.dim_DateIDPostingDate = ifnull(pd.Dim_Dateid,1)
from tmp_fmatmov_t002t f
             left join dim_date pd  on
             pd.CompanyCode = f.MSEG1_BUKRS AND f.MKPF_BUDAT = pd.DateValue
where  f.dim_DateIDPostingDate <> ifnull(pd.Dim_Dateid,1);

update tmp_fmatmov_t002t f
set   f.Dim_MovementIndicatorid = ifnull(dmi.Dim_MovementIndicatorid,1)
from tmp_fmatmov_t002t f
             left join dim_movementindicator dmi  on
             dmi.TypeCode = f.MSEG1_KZBEW
where  f.Dim_MovementIndicatorid <> ifnull(dmi.Dim_MovementIndicatorid,1);

update tmp_fmatmov_t002t f
set   f.Dim_specialstockid = ifnull(spt.dim_specialstockid,1)
from tmp_fmatmov_t002t f
             left join dim_specialstock spt  on
             spt.specialstockindicator = f.MSEG1_SOBKZ
where  f.Dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

update tmp_fmatmov_t002t f
set   f.Dim_unitofmeasureid = ifnull(uom.Dim_unitofmeasureid,1)
from tmp_fmatmov_t002t f
             left join dim_unitofmeasure uom  on
             uom.UOM = f.MSEG1_MEINS
where  f.Dim_unitofmeasureid <> ifnull(uom.Dim_unitofmeasureid,1);

update tmp_fmatmov_t002t f
set   f.Dim_controllingareaid = ifnull(ca.Dim_controllingareaid,1)
from tmp_fmatmov_t002t f
             left join dim_controllingarea ca  on
             ca.ControllingAreaCode = f.MSEG1_KOKRS
where f.Dim_controllingareaid <> ifnull(ca.Dim_controllingareaid,1);

update tmp_fmatmov_t002t f
set   f.Dim_consumptiontypeid = ifnull(ct.Dim_consumptiontypeid,1)
from tmp_fmatmov_t002t f
             left join dim_consumptiontype ct  on
             ct.ConsumptionCode = f.MSEG1_KZVBR
where f.Dim_consumptiontypeid <> ifnull(ct.Dim_consumptiontypeid,1);

update tmp_fmatmov_t002t f
set   f.Dim_stocktypeid = ifnull(st.Dim_StockTypeid,1)
from tmp_fmatmov_t002t f
             left join dim_stocktype st  on
             st.TypeCode = f.MSEG1_INSMK
where  f.Dim_stocktypeid <> ifnull(st.Dim_StockTypeid,1);

update tmp_fmatmov_t002t f
set   f.Dim_PurchaseGroupid = ifnull(pg1.Dim_PurchaseGroupid,1)
from tmp_fmatmov_t002t f
             left join 
          (select dpr.PartNumber,dpr.Plant,pg.Dim_PurchaseGroupid FROM dim_part dpr 
              inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup) pg1
         on pg1.PartNumber = f.MSEG1_MATNR AND pg1.Plant = f.MSEG1_WERKS
where  f.Dim_PurchaseGroupid <> ifnull(pg1.Dim_PurchaseGroupid,1);

update tmp_fmatmov_t002t f
set   f.Dim_materialgroupid = ifnull(mg1.Dim_materialgroupid,1)
from tmp_fmatmov_t002t f
             left join 
          (select dpr.PartNumber,dpr.Plant,mg.Dim_materialgroupid FROM dim_part dpr 
              inner join dim_materialgroup mg on mg.MaterialGroupCode = dpr.MaterialGroup) mg1
         on mg1.PartNumber = f.MSEG1_MATNR AND mg1.Plant = f.MSEG1_WERKS
where  f.Dim_materialgroupid <> ifnull(mg1.Dim_materialgroupid,1);


update tmp_fmatmov_t002t f
set   f.Dim_ReceivingPlantId = ifnull(pl.dim_plantid,1)
from tmp_fmatmov_t002t f
             left join dim_plant pl  on
             pl.PlantCode = f.MSEG1_UMWRK
where f.Dim_ReceivingPlantId <> ifnull(pl.dim_plantid,1);

update tmp_fmatmov_t002t f
set   f.Dim_ReceivingPartId = ifnull(prt.Dim_Partid,1)
from tmp_fmatmov_t002t f
             left join dim_part prt
            on prt.Plant = f.MSEG1_UMWRK AND prt.PartNumber = f.MSEG1_UMMAT
where f.Dim_ReceivingPartId <> ifnull(prt.Dim_Partid,1);

update tmp_fmatmov_t002t f
set   f.Dim_RecvIssuStorLocid = ifnull(dsl.Dim_StorageLocationid,1)
from tmp_fmatmov_t002t f
             left join dim_storagelocation dsl
            on dsl.LocationCode = f.MSEG1_UMLGO AND dsl.Plant = f.MSEG1_UMWRK
where f.Dim_RecvIssuStorLocid <> ifnull(dsl.Dim_StorageLocationid,1);

update tmp_fmatmov_t002t f
set   f.Dim_CurrencyId_TRA = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t002t f
             left join dim_currency dcr
            on dcr.CurrencyCode = f.MSEG1_WAERS
where f.Dim_CurrencyId_TRA <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t002t f
set   f.Dim_CurrencyId_GBL = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t002t f
             left join dim_currency dcr
            on dcr.CurrencyCode = pGlobalCurrency
where f.Dim_CurrencyId_GBL <> ifnull(dcr.Dim_Currencyid,1);

INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
				dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
dd_consignmentflag,
dd_documentscheduleno,
dd_incoterms2,
dd_vendorspendflag,
dim_accountcategoryid,
dim_afssizeid,
dim_customergroup1id,
dim_dateidcosting,
dim_dateiddelivery,
dim_dateiddoccreation,
dim_dateidordered,
dim_dateidstatdelivery,
dim_documentcategoryid,
dim_documenttypeid,
dim_grstatusid,
dim_incotermid,
dim_inspusagedecisionid,
dim_itemcategoryid,
dim_itemstatusid,
dim_poissustoragelocid,
dim_poplantidordering,
dim_poplantidsupplying,
dim_postoragelocid,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_purchaseorgid,
dim_salesdocumenttypeid,
dim_salesgroupid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_salesorderrejectreasonid,
dim_salesorgid,
dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
				dd_intorder,
				ct_lotnotthruqm,
lotsreceivedflag)
select fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
				dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
dd_consignmentflag,
dd_documentscheduleno,
dd_incoterms2,
dd_vendorspendflag,
dim_accountcategoryid,
dim_afssizeid,
dim_customergroup1id,
dim_dateidcosting,
dim_dateiddelivery,
dim_dateiddoccreation,
dim_dateidordered,
dim_dateidstatdelivery,
dim_documentcategoryid,
dim_documenttypeid,
dim_grstatusid,
dim_incotermid,
dim_inspusagedecisionid,
dim_itemcategoryid,
dim_itemstatusid,
dim_poissustoragelocid,
dim_poplantidordering,
dim_poplantidsupplying,
dim_postoragelocid,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_purchaseorgid,
dim_salesdocumenttypeid,
dim_salesgroupid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_salesorderrejectreasonid,
dim_salesorgid,
dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
				dd_intorder,
				ct_lotnotthruqm,
lotsreceivedflag from tmp_fmatmov_t002t;



/*call vectorwise(combine 'fact_materialmovement+tmp_fact_materialmovement_perf1')*/

insert into  fact_materialmovement
(LOTSACCEPTEDFLAG,
LOTSREJECTEDFLAG,
PERCENTLOTSINSPECTEDFLAG,
PERCENTLOTSREJECTEDFLAG,
PERCENTLOTSACCEPTEDFLAG,
LOTSSKIPPEDFLAG,
LOTSAWAITINGINSPECTIONFLAG,
LOTSINSPECTEDFLAG,
LOTSRECEIVEDFLAG,
DD_MATERIALDOCNO,
DD_DOCUMENTNO,
DD_SALESORDERNO,
DD_GLACCOUNTNO,
DD_PRODUCTIONORDERNUMBER,
DD_BATCHNUMBER,
DD_VALUATIONTYPE,
DD_DEBITCREDITID,
DD_INCOTERMS2,
DD_INTORDER,
DD_REFERENCEDOCNO,
DD_INSPECTIONSTATUSINGRDOC,
DD_ITEMAUTOMATICALLYCREATED,
DD_MATERIALDOCITEMNO,
DD_MATERIALDOCYEAR,
DD_GOODSMOVEREASON,
AMT_LOCALCURRAMT,
AMT_DELIVERYCOST,
AMT_ALTPRICECONTROL,
AMT_ONHAND_VALSTOCK,
CT_QUANTITY,
CT_QTYENTRYUOM,
CT_QTYGRORDUNIT,
CT_QTYONHAND_VALSTOCK,
CT_QTYORDPRICEUNIT,
DIRTYROW,
DD_CONSIGNMENTFLAG,
AMT_POUNITPRICE,
AMT_STDUNITPRICE,
AMT_PLANNEDPRICE,
AMT_PLANNEDPRICE1,
DD_DOCUMENTSCHEDULENO,
CT_ORDERQUANTITY,
DD_REFERENCEDOCITEM,
QTYREJECTEDEXTERNALAMT,
QTYREJECTEDINTERNALAMT,
DD_VENDORSPENDFLAG,
DD_RESERVATIONNUMBER,
DIM_MOVEMENTTYPEID,
DIM_COMPANYID,
DIM_CURRENCYID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_VENDORID,
DIM_DATEIDMATERIALDOCDATE,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCCREATION,
DIM_DATEIDORDERED,
DIM_PRODUCTHIERARCHYID,
DIM_MOVEMENTINDICATORID,
DIM_CUSTOMERID,
DIM_COSTCENTERID,
DIM_ACCOUNTCATEGORYID,
DIM_CONSUMPTIONTYPEID,
DIM_CONTROLLINGAREAID,
DIM_CUSTOMERGROUP1ID,
DIM_DOCUMENTCATEGORYID,
DIM_DOCUMENTTYPEID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEORGID,
DIM_SALESDOCUMENTTYPEID,
DIM_SALESGROUPID,
DIM_SALESORDERHEADERSTATUSID,
DIM_SALESORDERITEMSTATUSID,
DIM_SALESORDERREJECTREASONID,
DIM_SALESORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKTYPEID,
DIM_UNITOFMEASUREID,
DIM_MATERIALGROUPID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_TERMID,
DIM_PROFITCENTERID,
DIM_RECEIVINGPLANTID,
DIM_RECEIVINGPARTID,
DIM_DATEIDCOSTING,
DIM_INCOTERMID,
DIM_DATEIDDELIVERY,
DIM_DATEIDSTATDELIVERY,
DIM_GRSTATUSID,
DIM_RECVISSUSTORLOCID,
DIM_POPLANTIDORDERING,
DIM_POPLANTIDSUPPLYING,
DIM_POSTORAGELOCID,
DIM_POISSUSTORAGELOCID,
DIM_INSPUSAGEDECISIONID,
DIM_AFSSIZEID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DIM_UNITOFMEASUREORDERUNITID,
DIM_UOMUNITOFENTRYID,
DIM_PROJECTSOURCEID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_DOCUMENTITEMNO,
DD_SALESORDERITEMNO,
DD_SALESORDERDLVRNO,
DD_PRODUCTIONORDERITEMNO,
CT_LOTNOTTHRUQM,
AMT_VENDORSPEND,
FACT_MATERIALMOVEMENTID) 
select IFNULL(ftmp.LOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSSKIPPEDFLAG,'N'),
IFNULL(ftmp.LOTSAWAITINGINSPECTIONFLAG,'N'),
IFNULL(ftmp.LOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.LOTSRECEIVEDFLAG,'N'),
IFNULL(ftmp.DD_MATERIALDOCNO,'Not Set'),
IFNULL(ftmp.DD_DOCUMENTNO,'Not Set'),
IFNULL(ftmp.DD_SALESORDERNO,'Not Set'),
IFNULL(ftmp.DD_GLACCOUNTNO,'Not Set'),
IFNULL(ftmp.DD_PRODUCTIONORDERNUMBER,'Not Set'),
IFNULL(ftmp.DD_BATCHNUMBER,'Not Set'),
IFNULL(ftmp.DD_VALUATIONTYPE,'Not Set'),
IFNULL(ftmp.DD_DEBITCREDITID,'Not Set'),
IFNULL(ftmp.DD_INCOTERMS2,'Not Set'),
IFNULL(ftmp.DD_INTORDER,'Not Set'),
IFNULL(ftmp.DD_REFERENCEDOCNO,'Not Set'),
IFNULL(ftmp.DD_INSPECTIONSTATUSINGRDOC,'Not Set'),
IFNULL(ftmp.DD_ITEMAUTOMATICALLYCREATED,'Not Set'),
IFNULL(ftmp.DD_MATERIALDOCITEMNO,0),
IFNULL(ftmp.DD_MATERIALDOCYEAR,0),
IFNULL(ftmp.DD_GOODSMOVEREASON,0),
IFNULL(ftmp.AMT_LOCALCURRAMT,0),
IFNULL(ftmp.AMT_DELIVERYCOST,0),
IFNULL(ftmp.AMT_ALTPRICECONTROL,0),
IFNULL(ftmp.AMT_ONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QUANTITY,0),
IFNULL(ftmp.CT_QTYENTRYUOM,0),
IFNULL(ftmp.CT_QTYGRORDUNIT,0),
IFNULL(ftmp.CT_QTYONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QTYORDPRICEUNIT,0),
IFNULL(ftmp.DIRTYROW,0),
IFNULL(ftmp.DD_CONSIGNMENTFLAG,0),
IFNULL(ftmp.AMT_POUNITPRICE,0),
IFNULL(ftmp.AMT_STDUNITPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE1,0),
IFNULL(ftmp.DD_DOCUMENTSCHEDULENO,0),
IFNULL(ftmp.CT_ORDERQUANTITY,0),
IFNULL(ftmp.DD_REFERENCEDOCITEM,0),
IFNULL(ftmp.QTYREJECTEDEXTERNALAMT,0),
IFNULL(ftmp.QTYREJECTEDINTERNALAMT,0),
IFNULL(ftmp.DD_VENDORSPENDFLAG,0),
IFNULL(ftmp.DD_RESERVATIONNUMBER,0),
IFNULL(ftmp.DIM_MOVEMENTTYPEID,1),
IFNULL(ftmp.DIM_COMPANYID,1),
IFNULL(ftmp.DIM_CURRENCYID,1),
IFNULL(ftmp.DIM_PARTID,1),
IFNULL(ftmp.DIM_PLANTID,1),
IFNULL(ftmp.DIM_STORAGELOCATIONID,1),
IFNULL(ftmp.DIM_VENDORID,1),
IFNULL(ftmp.DIM_DATEIDMATERIALDOCDATE,1),
IFNULL(ftmp.DIM_DATEIDPOSTINGDATE,1),
IFNULL(ftmp.DIM_DATEIDDOCCREATION,1),
IFNULL(ftmp.DIM_DATEIDORDERED,1),
IFNULL(ftmp.DIM_PRODUCTHIERARCHYID,1),
IFNULL(ftmp.DIM_MOVEMENTINDICATORID,1),
IFNULL(ftmp.DIM_CUSTOMERID,1),
IFNULL(ftmp.DIM_COSTCENTERID,1),
IFNULL(ftmp.DIM_ACCOUNTCATEGORYID,1),
IFNULL(ftmp.DIM_CONSUMPTIONTYPEID,1),
IFNULL(ftmp.DIM_CONTROLLINGAREAID,1),
IFNULL(ftmp.DIM_CUSTOMERGROUP1ID,1),
IFNULL(ftmp.DIM_DOCUMENTCATEGORYID,1),
IFNULL(ftmp.DIM_DOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_ITEMCATEGORYID,1),
IFNULL(ftmp.DIM_ITEMSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERTYPEID,1),
IFNULL(ftmp.DIM_PURCHASEGROUPID,1),
IFNULL(ftmp.DIM_PURCHASEORGID,1),
IFNULL(ftmp.DIM_SALESDOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_SALESGROUPID,1),
IFNULL(ftmp.DIM_SALESORDERHEADERSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERITEMSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERREJECTREASONID,1),
IFNULL(ftmp.DIM_SALESORGID,1),
IFNULL(ftmp.DIM_SPECIALSTOCKID,1),
IFNULL(ftmp.DIM_STOCKTYPEID,1),
IFNULL(ftmp.DIM_UNITOFMEASUREID,1),
IFNULL(ftmp.DIM_MATERIALGROUPID,1),
IFNULL(ftmp.AMT_EXCHANGERATE_GBL,1),
IFNULL(ftmp.AMT_EXCHANGERATE,1),
IFNULL(ftmp.DIM_TERMID,1),
IFNULL(ftmp.DIM_PROFITCENTERID,1),
IFNULL(ftmp.DIM_RECEIVINGPLANTID,1),
IFNULL(ftmp.DIM_RECEIVINGPARTID,1),
IFNULL(ftmp.DIM_DATEIDCOSTING,1),
IFNULL(ftmp.DIM_INCOTERMID,1),
IFNULL(ftmp.DIM_DATEIDDELIVERY,1),
IFNULL(ftmp.DIM_DATEIDSTATDELIVERY,1),
IFNULL(ftmp.DIM_GRSTATUSID,1),
IFNULL(ftmp.DIM_RECVISSUSTORLOCID,1),
IFNULL(ftmp.DIM_POPLANTIDORDERING,1),
IFNULL(ftmp.DIM_POPLANTIDSUPPLYING,1),
IFNULL(ftmp.DIM_POSTORAGELOCID,1),
IFNULL(ftmp.DIM_POISSUSTORAGELOCID,1),
IFNULL(ftmp.DIM_INSPUSAGEDECISIONID,1),
IFNULL(ftmp.DIM_AFSSIZEID,1),
IFNULL(ftmp.DIM_CURRENCYID_TRA,1),
IFNULL(ftmp.DIM_CURRENCYID_GBL,1),
IFNULL(ftmp.DIM_UNITOFMEASUREORDERUNITID,1),
IFNULL(ftmp.DIM_UOMUNITOFENTRYID,1),
IFNULL(ftmp.DIM_PROJECTSOURCEID,1),
IFNULL(ftmp.DW_INSERT_DATE,CURRENT_TIMESTAMP),
IFNULL(ftmp.DW_UPDATE_DATE,CURRENT_TIMESTAMP),
ftmp.DD_DOCUMENTITEMNO,
ftmp.DD_SALESORDERITEMNO,
ftmp.DD_SALESORDERDLVRNO,
ftmp.DD_PRODUCTIONORDERITEMNO,
ftmp.CT_LOTNOTTHRUQM,
ftmp.AMT_VENDORSPEND,
ftmp.FACT_MATERIALMOVEMENTID
 from fact_materialmovement  f
right join tmp_fact_materialmovement_perf1 ftmp
on f.fact_materialmovementid = ftmp.fact_materialmovementid
where f.fact_materialmovementid is  null;


truncate table tmp_fact_materialmovement_perf1;
truncate table tmp_fact_materialmovement_del;
 		  
      UPDATE fact_materialmovement ms
  SET     amt_LocalCurrAmt = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR * ms.amt_exchangerate
  FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
		  fact_materialmovement ms
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;


     UPDATE fact_materialmovement ms
  SET     ms.dim_profitcenterid = ifnull(pc.Dim_ProfitCenterid,1)
 from fact_materialmovement ms 
          inner join MKPF_MSEG1 m
                  on ms.dd_MaterialDocNo = m.MSEG1_MBLNR 
                 AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE 
                 AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
          inner join dim_plant dp
                  on m.MSEG1_WERKS = dp.PlantCode
          inner join dim_company dc 
                  on m.MSEG1_BUKRS = dc.CompanyCode
           left join dim_profitcenter_789 pc
                 on pc.ControllingArea = m.MSEG1_KOKRS
                      AND pc.ProfitCenterCode = m.MSEG1_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT
where  ms.dim_profitcenterid <> ifnull(pc.Dim_ProfitCenterid,1);

		  
      UPDATE fact_materialmovement ms
  SET     ms.dim_producthierarchyid = ifnull(dpr1.dim_producthierarchyid, 1)
        from fact_materialmovement ms 
                 inner join  MKPF_MSEG1 m
                         on ms.dd_MaterialDocNo = m.MSEG1_MBLNR
                        AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                        AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
                 inner join   dim_plant dp
                        on m.MSEG1_WERKS = dp.PlantCode
                 inner join dim_company dc 
                        on m.MSEG1_BUKRS = dc.CompanyCode
                 left join (SELECT dpr.PartNumber,dpr.Plant,dph.dim_producthierarchyid
                           FROM dim_producthierarchy dph, dim_part dpr
                           WHERE     dph.ProductHierarchy = dpr.ProductHierarchy) dpr1
                        on dpr1.PartNumber = m.MSEG1_MATNR
                         AND dpr1.Plant = m.MSEG1_WERKS
 where  ms.dim_producthierarchyid <> ifnull(dpr1.dim_producthierarchyid, 1);       


INSERT INTO processinglog (processinglogid, referencename, enddate, description)
 VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),
'bi_populate_materialmovement_fact',current_timestamp, 
'UPDATE fact_materialmovement ms');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',
current_timestamp, 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');



Drop table if exists max_holder_789;
create table max_holder_789
as
Select ifnull(max(fact_materialmovementid),ifnull((select DIM_PROJECTSOURCEID * MULTIPLIER from dim_projectsource), 0)) as maxid
from fact_materialmovement;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG;
CREATE TABLE tmp_mm_MKPF_MSEG as
select MSEG_MBLNR dd_MaterialDocNo, MSEG_ZEILE dd_MaterialDocItemNo, MSEG_MJAHR dd_MaterialDocYear
from MKPF_MSEG;


DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_del;
CREATE TABLE tmp_mm_MKPF_MSEG_del
as 
select * from tmp_mm_MKPF_MSEG where 1=2;
insert into tmp_mm_MKPF_MSEG_del
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
from fact_materialmovement ;

merge into tmp_mm_MKPF_MSEG dst
using (select distinct t1.rowid rid
	  from tmp_mm_MKPF_MSEG t1
				inner join tmp_mm_MKPF_MSEG_del t2 on ifnull(t1.dd_MaterialDocNo,'Not Set') = ifnull(t2.dd_MaterialDocNo,'Not Set')
                                                  and ifnull(t1.dd_MaterialDocItemNo,-1) = ifnull(t2.dd_MaterialDocItemNo,-1)
                                                  and ifnull(t1.dd_MaterialDocYear,-1) = ifnull(t2.dd_MaterialDocYear,-1)) src
on dst.rowid = src.rid
when matched then delete;



DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_allcols;
CREATE TABLE tmp_mm_MKPF_MSEG_allcols as
SELECT distinct m.*
FROM MKPF_MSEG m,tmp_mm_MKPF_MSEG m1
where m.MSEG_MBLNR = m1.dd_MaterialDocNo
and m.MSEG_ZEILE = m1.dd_MaterialDocItemNo
and m.MSEG_MJAHR = m1.dd_MaterialDocYear;



drop table if exists tmp_fmatmov_t003t;
create table tmp_fmatmov_t003t as
   select max_holder_789.maxid + row_number() over(order by '') fact_materialmovementid,
		  /*hash(concat(m.MSEG_MBLNR,m.MSEG_ZEILE,m.MSEG_MJAHR)) fact_materialmovementid,*/
		  bb.* 
		  from max_holder_789,(SELECT DISTINCT
          m.MSEG_MBLNR dd_MaterialDocNo,
          m.MSEG_ZEILE dd_MaterialDocItemNo,
          m.MSEG_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG_AUFNR,'Not Set') dd_productionordernumber, 
          ifnull(m.MSEG_AUFPS,1) dd_productionorderitemno,
          m.MSEG_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG_CHARG, 'Not Set') dd_BatchNumber,
          ifnull(m.MSEG_BWTAR,'Not Set') dd_ValuationType,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR amt_LocalCurrAmt,
          ifnull(m.MSEG_DMBTR,0) / (case when m.MSEG_ERFMG =0 then 1 else  m.MSEG_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT amt_AltPriceControl,
          ifnull(m.MSEG_SALK3,0) amt_OnHand_ValStock,
	  convert( decimal (18,4),1) amt_ExchangeRate,
	  convert( decimal (18,4),1)  amt_ExchangeRate_GBL,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE ct_Quantity,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG ct_QtyGROrdUnit,
          ifnull(m.MSEG_LBKUM,0) ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG ct_QtyOrdPriceUnit,
          convert(bigint, 1) Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          convert(bigint, 1) Dim_Currencyid,	/* Local curr */
	 convert(bigint, 1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
	  convert(bigint, 1) Dim_StorageLocationid,
    convert(bigint, 1) Dim_Vendorid,
    convert(bigint, 1) dim_DateidMaterialDocDate,
    convert(bigint, 1) dim_DateIDPostingDate,
	convert(bigint, 1) dim_producthierarchyid,
   convert(bigint, 1) dim_MovementIndicatorid,
    convert(bigint, 1) dim_Customerid,
	convert(bigint, 1) dim_CostCenterid,
    convert(bigint, 1) dim_specialstockid,
    convert(bigint, 1) dim_unitofmeasureid,
    convert(bigint, 1) dim_controllingareaid,
    convert(bigint, 1) Dim_ProfitCenterId,
    convert(bigint, 1) dim_consumptiontypeid,
    convert(bigint, 1) dim_stocktypeid,
	convert(bigint, 1) Dim_PurchaseGroupid,
     convert(bigint, 1) Dim_materialgroupid,
     convert(bigint, 1) Dim_ReceivingPlantId,
     convert(bigint, 1) Dim_ReceivingPartId,
	convert(bigint, 1) Dim_RecvIssuStorLocid,
         convert(bigint, 1) Dim_Currencyid_TRA,								
          convert(bigint, 1) Dim_Currencyid_GBL,
				'N' as lotsreceivedflag,
     m.MSEG_LIFNR,  --Dim_Vendorid
m.MSEG_BUKRS, --dim_DateIDMaterialDocDate
m.MKPF_BLDAT,  --dim_DateIDMaterialDocDate
m.MKPF_BUDAT,  --dim_DateIDPostingDate
m.MSEG_MATNR,   --dim_producthierarchyid
m.MSEG_WERKS,    --dim_producthierarchyid
m.MSEG_KZBEW,    -- dim_MovementIndicatorid
m.MSEG_KUNNR,    --dim_Customerid
m.MSEG_SOBKZ, --dim_specialstockid
m.MSEG_MEINS, --dim_unitofmeasureid
m.MSEG_KOKRS, --dim_controllingareaid
m.MSEG_KZVBR, --dim_consumptiontypeid
m.MSEG_INSMK, --dim_stocktypeid
m.MSEG_UMMAT,  --Dim_ReceivingPartId
m.MSEG_UMLGO,  --Dim_RecvIssuStorLocid
m.MSEG_UMWRK,  --Dim_RecvIssuStorLocid
m.MSEG_WAERS,  --Dim_Currencyid_TRA
pGlobalCurrency --Dim_Currencyid_GBL
      FROM pGlobalCurrency_tbl, tmp_mm_MKPF_MSEG_allcols m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode ) bb;

update tmp_fmatmov_t003t f
set   f.Dim_Vendorid = ifnull(dv.Dim_Vendorid,1)
from tmp_fmatmov_t003t f
             left join Dim_Vendor dv
            on dv.VendorNumber = f.MSEG_LIFNR
where f.Dim_Vendorid <> ifnull(dv.Dim_Vendorid,1);

update tmp_fmatmov_t003t f
set   f.dim_DateIDMaterialDocDate = ifnull(mdd.Dim_Dateid,1)
from tmp_fmatmov_t003t f
             left join dim_date mdd  on
              mdd.CompanyCode = f.MSEG_BUKRS AND f.MKPF_BLDAT = mdd.DateValue
where  f.dim_DateIDMaterialDocDate <> ifnull(mdd.Dim_Dateid,1);

update tmp_fmatmov_t003t f
set   f.dim_DateIDPostingDate = ifnull(pd.Dim_Dateid,1)
from tmp_fmatmov_t003t f
             left join dim_date pd  on
             pd.CompanyCode = f.MSEG_BUKRS AND f.MKPF_BUDAT = pd.DateValue
where  f.dim_DateIDPostingDate <> ifnull(pd.Dim_Dateid,1);

UPDATE tmp_fmatmov_t003t ms
  SET     ms.dim_producthierarchyid = ifnull(case WHEN ms.MSEG_MATNR IS NULL then 1 else dpr1.dim_producthierarchyid end, 1)
        from tmp_fmatmov_t003t ms 
                 left join (SELECT dpr.PartNumber,dpr.Plant,dph.dim_producthierarchyid
                           FROM dim_producthierarchy dph, dim_part dpr
                           WHERE     dph.ProductHierarchy = dpr.ProductHierarchy) dpr1
                        on dpr1.PartNumber = ms.MSEG_MATNR
                         AND dpr1.Plant = ms.MSEG_WERKS
 where   ms.dim_producthierarchyid = ifnull(case WHEN ms.MSEG_MATNR IS NULL then 1 else dpr1.dim_producthierarchyid end, 1);    

update tmp_fmatmov_t003t f
set   f.Dim_MovementIndicatorid = ifnull(dmi.Dim_MovementIndicatorid,1)
from tmp_fmatmov_t003t f
             left join dim_movementindicator dmi  on
             dmi.TypeCode = f.MSEG_KZBEW
where  f.Dim_MovementIndicatorid <> ifnull(dmi.Dim_MovementIndicatorid,1);

update tmp_fmatmov_t003t f
set   f.dim_Customerid = ifnull(dcu.dim_Customerid,1)
from tmp_fmatmov_t003t f
             left join dim_customer dcu  on
             dcu.CustomerNumber = f.MSEG_KUNNR
where  f.dim_Customerid <> ifnull(dcu.dim_Customerid,1);


update tmp_fmatmov_t003t f
set   f.Dim_specialstockid = ifnull(spt.dim_specialstockid,1)
from tmp_fmatmov_t003t f
             left join dim_specialstock spt  on
             spt.specialstockindicator = f.MSEG_SOBKZ
where  f.Dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

update tmp_fmatmov_t003t f
set   f.Dim_unitofmeasureid = ifnull(uom.Dim_unitofmeasureid,1)
from tmp_fmatmov_t003t f
             left join dim_unitofmeasure uom  on
             uom.UOM = f.MSEG_MEINS
where  f.Dim_unitofmeasureid <> ifnull(uom.Dim_unitofmeasureid,1);

update tmp_fmatmov_t003t f
set   f.Dim_controllingareaid = ifnull(ca.Dim_controllingareaid,1)
from tmp_fmatmov_t003t f
             left join dim_controllingarea ca  on
             ca.ControllingAreaCode = f.MSEG_KOKRS
where f.Dim_controllingareaid <> ifnull(ca.Dim_controllingareaid,1);

update tmp_fmatmov_t003t f
set   f.Dim_consumptiontypeid = ifnull(ct.Dim_consumptiontypeid,1)
from tmp_fmatmov_t003t f
             left join dim_consumptiontype ct  on
             ct.ConsumptionCode = f.MSEG_KZVBR
where f.Dim_consumptiontypeid <> ifnull(ct.Dim_consumptiontypeid,1);

update tmp_fmatmov_t003t f
set   f.Dim_stocktypeid = ifnull(st.Dim_StockTypeid,1)
from tmp_fmatmov_t003t f
             left join dim_stocktype st  on
             st.TypeCode = f.MSEG_INSMK
where  f.Dim_stocktypeid <> ifnull(st.Dim_StockTypeid,1);

update tmp_fmatmov_t003t f
set   f.Dim_PurchaseGroupid = ifnull(case when  f.MSEG_MATNR IS NULL then 1 else  pg1.Dim_PurchaseGroupid end ,1)
from tmp_fmatmov_t003t f
             left join 
          (select dpr.PartNumber,dpr.Plant,pg.Dim_PurchaseGroupid FROM dim_part dpr 
              inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup) pg1
         on pg1.PartNumber = f.MSEG_MATNR AND pg1.Plant = f.MSEG_WERKS
where  f.Dim_PurchaseGroupid <> ifnull(case when  f.MSEG_MATNR IS NULL then 1 else  pg1.Dim_PurchaseGroupid end ,1);

update tmp_fmatmov_t003t f
set   f.Dim_materialgroupid = ifnull(mg1.Dim_materialgroupid,1)
from tmp_fmatmov_t003t f
             left join 
          (select dpr.PartNumber,dpr.Plant,mg.Dim_materialgroupid FROM dim_part dpr 
              inner join dim_materialgroup mg on mg.MaterialGroupCode = dpr.MaterialGroup) mg1
         on mg1.PartNumber = f.MSEG_MATNR AND mg1.Plant = f.MSEG_WERKS
where  f.Dim_materialgroupid <> ifnull(mg1.Dim_materialgroupid,1);

update tmp_fmatmov_t003t f
set   f.Dim_ReceivingPlantId = ifnull(pl.dim_plantid,1)
from tmp_fmatmov_t003t  f
             left join dim_plant pl  on
             pl.PlantCode = f.MSEG_UMWRK
where f.Dim_ReceivingPlantId <> ifnull(pl.dim_plantid,1);

update tmp_fmatmov_t003t f
set   f.Dim_ReceivingPartId = ifnull(prt.Dim_Partid,1)
from tmp_fmatmov_t003t f
             left join dim_part prt
            on prt.Plant = f.MSEG_UMWRK AND prt.PartNumber = f.MSEG_UMMAT
where f.Dim_ReceivingPartId <> ifnull(prt.Dim_Partid,1);

update tmp_fmatmov_t003t f
set   f.Dim_RecvIssuStorLocid = ifnull((case when  f.MSEG_UMLGO IS NULL then 1 else dsl.Dim_StorageLocationid end),1)
from tmp_fmatmov_t003t f
             left join dim_storagelocation dsl
            on dsl.LocationCode = f.MSEG_UMLGO AND dsl.Plant = f.MSEG_UMWRK
where f.Dim_RecvIssuStorLocid <> ifnull((case when  f.MSEG_UMLGO IS NULL then 1 else dsl.Dim_StorageLocationid end),1);

update tmp_fmatmov_t003t f
set   f.Dim_CurrencyId_TRA = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t003t f
             left join dim_currency dcr
            on dcr.CurrencyCode = f.MSEG_WAERS
where f.Dim_CurrencyId_TRA <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t003t f
set   f.Dim_CurrencyId_GBL = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t003t f
             left join dim_currency dcr
            on dcr.CurrencyCode = pGlobalCurrency
where f.Dim_CurrencyId_GBL <> ifnull(dcr.Dim_Currencyid,1);


INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,                   
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
							   dim_Currencyid_TRA,
							   dim_Currencyid_GBL,
lotsreceivedflag)
select fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,                   
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
							   dim_Currencyid_TRA,
							   dim_Currencyid_GBL,
lotsreceivedflag
from tmp_fmatmov_t003t;

DROP TABLE IF EXISTS TMP_UPDT_MM_MKPF_MSEG_ALLCOLS;
CREATE TABLE TMP_UPDT_MM_MKPF_MSEG_ALLCOLS
AS
SELECT m.*,row_number() over(partition by MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR order by MKPF_AEDAT desc) rono
FROM tmp_mm_MKPF_MSEG_allcols m;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_StdUnitPrice = 0
FROM TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG_ERFMG = 0
AND m.rono = 1
AND ms.amt_StdUnitPrice <> 0;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_currencyid = dcr.dim_currencyid
FROM TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc, dim_currency dcr, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dcr.CurrencyCode = dc.currency
AND m.rono = 1
AND ms.dim_currencyid <> dcr.dim_currencyid;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_partid = dpr.dim_partid
FROM TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc,dim_part dpr, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dpr.PartNumber = m.MSEG_MATNR
AND dpr.Plant = m.MSEG_WERKS
AND m.rono = 1
AND ms.dim_partid <> dpr.dim_partid;



UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_storagelocationid = dsl.dim_storagelocationid
FROM TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc,dim_storagelocation dsl, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dsl.LocationCode = m.MSEG_LGORT AND dsl.Plant = m.MSEG_WERKS
AND m.rono = 1
AND ms.dim_storagelocationid <> dsl.dim_storagelocationid;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_exchangerate = z.exchangerate
FROM dim_currency tra, dim_currency lcl,dim_date d, tmp_getExchangeRate1 z, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_currencyid_tra = tra.dim_currencyid
AND ms.dim_currencyid = lcl.dim_currencyid
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = lcl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact' 
AND ms.amt_exchangerate <> z.exchangerate;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_exchangerate_gbl = z.exchangerate
FROM dim_currency tra, dim_currency gbl,dim_date d, tmp_getExchangeRate1 z,
tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_currencyid_tra = tra.dim_currencyid
AND ms.dim_currencyid_gbl = gbl.dim_currencyid
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = gbl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate_gbl <> z.exchangerate;


UPDATE tmp_fact_materialmovement_perf1 ms
SET Dim_MovementTypeid = mt.Dim_MovementTypeid
FROM dim_movementtype mt, TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG_WERKS = dp.PlantCode
AND  m.MSEG_BUKRS = dc.CompanyCode
AND mt.MovementType = m.MSEG_BWART 
AND mt.ConsumptionIndicator = ifnull(m.MSEG_KZVBR, 'Not Set') 
AND mt.MovementIndicator = ifnull(m.MSEG_KZBEW, 'Not Set') 
AND mt.ReceiptIndicator = ifnull(m.MSEG_KZZUG, 'Not Set') 
AND mt.SpecialStockIndicator = ifnull(m.MSEG_SOBKZ, 'Not Set')
AND m.rono = 1
AND ms.Dim_MovementTypeid <> mt.Dim_MovementTypeid;

DROP TABLE IF EXISTS TMP_UPDT_MM_MKPF_MSEG_ALLCOLS;
			 
/* Replaced NOT EXISTS with combine-insert*/		

		 
 /*   WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)) bb */

drop table if exists max_holder_789;

drop table if exists tmp2_MKPF_MSEG;
create table tmp2_MKPF_MSEG as select distinct * from MKPF_MSEG;

DROP TABLE IF EXISTS tmp1_MKPF_MSEG;
CREATE TABLE tmp1_MKPF_MSEG
AS
SELECT m.*, ROW_NUMBER() OVER(PARTITION BY MSEG_MBLNR, MSEG_ZEILE, MSEG_MJAHR ORDER BY MKPF_AEDAT DESC) rono
FROM tmp2_MKPF_MSEG m;

drop table if exists tmp2_MKPF_MSEG;

Update tmp_fact_materialmovement_perf1 fmt
set fmt.Dim_ProfitCenterId =  ifnull( pc.Dim_ProfitCenterid,1) 
From tmp1_MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
          inner join tmp_fact_materialmovement_perf1 fmt
    on  fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
        AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
        AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR and m.rono = 1
           left join dim_profitcenter_789 pc
    on pc.ControllingArea = m.MSEG_KOKRS
		      AND pc.ProfitCenterCode = m.MSEG_PRCTR
		      AND pc.ValidTo >= m.MKPF_BUDAT
where fmt.Dim_ProfitCenterId <> ifnull( pc.Dim_ProfitCenterid,1);


Update tmp_fact_materialmovement_perf1 fmt
set fmt.dim_CostCenterid = ifnull(case  WHEN m.MSEG_KOSTL IS NULL then 1 else dcc1.dim_CostCenterid end,1)
From tmp1_MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
          inner join tmp_fact_materialmovement_perf1 fmt
    on  fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
        AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
        AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR and m.rono = 1
           left join (select dcc.Code,dcc.ControllingArea,min(dcc.dim_CostCenterid) as dim_CostCenterid from MKPF_MSEG m 
inner join dim_costcenter dcc
    on   dcc.Code = m.MSEG_KOSTL
       AND m.MSEG_KOKRS = dcc.ControllingArea
    where dcc.validTo >= m.MKPF_BUDAT
    group by dcc.Code,dcc.ControllingArea) dcc1
     on   dcc1.Code = m.MSEG_KOSTL
       AND m.MSEG_KOKRS = dcc1.ControllingArea
where fmt.dim_CostCenterid <> ifnull(case  WHEN m.MSEG_KOSTL IS NULL then 1 else dcc1.dim_CostCenterid end,1);

insert into  fact_materialmovement
(LOTSACCEPTEDFLAG,
LOTSREJECTEDFLAG,
PERCENTLOTSINSPECTEDFLAG,
PERCENTLOTSREJECTEDFLAG,
PERCENTLOTSACCEPTEDFLAG,
LOTSSKIPPEDFLAG,
LOTSAWAITINGINSPECTIONFLAG,
LOTSINSPECTEDFLAG,
LOTSRECEIVEDFLAG,
DD_MATERIALDOCNO,
DD_DOCUMENTNO,
DD_SALESORDERNO,
DD_GLACCOUNTNO,
DD_PRODUCTIONORDERNUMBER,
DD_BATCHNUMBER,
DD_VALUATIONTYPE,
DD_DEBITCREDITID,
DD_INCOTERMS2,
DD_INTORDER,
DD_REFERENCEDOCNO,
DD_INSPECTIONSTATUSINGRDOC,
DD_ITEMAUTOMATICALLYCREATED,
DD_MATERIALDOCITEMNO,
DD_MATERIALDOCYEAR,
DD_GOODSMOVEREASON,
AMT_LOCALCURRAMT,
AMT_DELIVERYCOST,
AMT_ALTPRICECONTROL,
AMT_ONHAND_VALSTOCK,
CT_QUANTITY,
CT_QTYENTRYUOM,
CT_QTYGRORDUNIT,
CT_QTYONHAND_VALSTOCK,
CT_QTYORDPRICEUNIT,
DIRTYROW,
DD_CONSIGNMENTFLAG,
AMT_POUNITPRICE,
AMT_STDUNITPRICE,
AMT_PLANNEDPRICE,
AMT_PLANNEDPRICE1,
DD_DOCUMENTSCHEDULENO,
CT_ORDERQUANTITY,
DD_REFERENCEDOCITEM,
QTYREJECTEDEXTERNALAMT,
QTYREJECTEDINTERNALAMT,
DD_VENDORSPENDFLAG,
DD_RESERVATIONNUMBER,
DIM_MOVEMENTTYPEID,
DIM_COMPANYID,
DIM_CURRENCYID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_VENDORID,
DIM_DATEIDMATERIALDOCDATE,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCCREATION,
DIM_DATEIDORDERED,
DIM_PRODUCTHIERARCHYID,
DIM_MOVEMENTINDICATORID,
DIM_CUSTOMERID,
DIM_COSTCENTERID,
DIM_ACCOUNTCATEGORYID,
DIM_CONSUMPTIONTYPEID,
DIM_CONTROLLINGAREAID,
DIM_CUSTOMERGROUP1ID,
DIM_DOCUMENTCATEGORYID,
DIM_DOCUMENTTYPEID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEORGID,
DIM_SALESDOCUMENTTYPEID,
DIM_SALESGROUPID,
DIM_SALESORDERHEADERSTATUSID,
DIM_SALESORDERITEMSTATUSID,
DIM_SALESORDERREJECTREASONID,
DIM_SALESORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKTYPEID,
DIM_UNITOFMEASUREID,
DIM_MATERIALGROUPID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_TERMID,
DIM_PROFITCENTERID,
DIM_RECEIVINGPLANTID,
DIM_RECEIVINGPARTID,
DIM_DATEIDCOSTING,
DIM_INCOTERMID,
DIM_DATEIDDELIVERY,
DIM_DATEIDSTATDELIVERY,
DIM_GRSTATUSID,
DIM_RECVISSUSTORLOCID,
DIM_POPLANTIDORDERING,
DIM_POPLANTIDSUPPLYING,
DIM_POSTORAGELOCID,
DIM_POISSUSTORAGELOCID,
DIM_INSPUSAGEDECISIONID,
DIM_AFSSIZEID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DIM_UNITOFMEASUREORDERUNITID,
DIM_UOMUNITOFENTRYID,
DIM_PROJECTSOURCEID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_DOCUMENTITEMNO,
DD_SALESORDERITEMNO,
DD_SALESORDERDLVRNO,
DD_PRODUCTIONORDERITEMNO,
CT_LOTNOTTHRUQM,
AMT_VENDORSPEND,
FACT_MATERIALMOVEMENTID) 
select IFNULL(ftmp.LOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSSKIPPEDFLAG,'N'),
IFNULL(ftmp.LOTSAWAITINGINSPECTIONFLAG,'N'),
IFNULL(ftmp.LOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.LOTSRECEIVEDFLAG,'N'),
IFNULL(ftmp.DD_MATERIALDOCNO,'Not Set'),
IFNULL(ftmp.DD_DOCUMENTNO,'Not Set'),
IFNULL(ftmp.DD_SALESORDERNO,'Not Set'),
IFNULL(ftmp.DD_GLACCOUNTNO,'Not Set'),
IFNULL(ftmp.DD_PRODUCTIONORDERNUMBER,'Not Set'),
IFNULL(ftmp.DD_BATCHNUMBER,'Not Set'),
IFNULL(ftmp.DD_VALUATIONTYPE,'Not Set'),
IFNULL(ftmp.DD_DEBITCREDITID,'Not Set'),
IFNULL(ftmp.DD_INCOTERMS2,'Not Set'),
IFNULL(ftmp.DD_INTORDER,'Not Set'),
IFNULL(ftmp.DD_REFERENCEDOCNO,'Not Set'),
IFNULL(ftmp.DD_INSPECTIONSTATUSINGRDOC,'Not Set'),
IFNULL(ftmp.DD_ITEMAUTOMATICALLYCREATED,'Not Set'),
IFNULL(ftmp.DD_MATERIALDOCITEMNO,0),
IFNULL(ftmp.DD_MATERIALDOCYEAR,0),
IFNULL(ftmp.DD_GOODSMOVEREASON,0),
IFNULL(ftmp.AMT_LOCALCURRAMT,0),
IFNULL(ftmp.AMT_DELIVERYCOST,0),
IFNULL(ftmp.AMT_ALTPRICECONTROL,0),
IFNULL(ftmp.AMT_ONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QUANTITY,0),
IFNULL(ftmp.CT_QTYENTRYUOM,0),
IFNULL(ftmp.CT_QTYGRORDUNIT,0),
IFNULL(ftmp.CT_QTYONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QTYORDPRICEUNIT,0),
IFNULL(ftmp.DIRTYROW,0),
IFNULL(ftmp.DD_CONSIGNMENTFLAG,0),
IFNULL(ftmp.AMT_POUNITPRICE,0),
IFNULL(ftmp.AMT_STDUNITPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE1,0),
IFNULL(ftmp.DD_DOCUMENTSCHEDULENO,0),
IFNULL(ftmp.CT_ORDERQUANTITY,0),
IFNULL(ftmp.DD_REFERENCEDOCITEM,0),
IFNULL(ftmp.QTYREJECTEDEXTERNALAMT,0),
IFNULL(ftmp.QTYREJECTEDINTERNALAMT,0),
IFNULL(ftmp.DD_VENDORSPENDFLAG,0),
IFNULL(ftmp.DD_RESERVATIONNUMBER,0),
IFNULL(ftmp.DIM_MOVEMENTTYPEID,1),
IFNULL(ftmp.DIM_COMPANYID,1),
IFNULL(ftmp.DIM_CURRENCYID,1),
IFNULL(ftmp.DIM_PARTID,1),
IFNULL(ftmp.DIM_PLANTID,1),
IFNULL(ftmp.DIM_STORAGELOCATIONID,1),
IFNULL(ftmp.DIM_VENDORID,1),
IFNULL(ftmp.DIM_DATEIDMATERIALDOCDATE,1),
IFNULL(ftmp.DIM_DATEIDPOSTINGDATE,1),
IFNULL(ftmp.DIM_DATEIDDOCCREATION,1),
IFNULL(ftmp.DIM_DATEIDORDERED,1),
IFNULL(ftmp.DIM_PRODUCTHIERARCHYID,1),
IFNULL(ftmp.DIM_MOVEMENTINDICATORID,1),
IFNULL(ftmp.DIM_CUSTOMERID,1),
IFNULL(ftmp.DIM_COSTCENTERID,1),
IFNULL(ftmp.DIM_ACCOUNTCATEGORYID,1),
IFNULL(ftmp.DIM_CONSUMPTIONTYPEID,1),
IFNULL(ftmp.DIM_CONTROLLINGAREAID,1),
IFNULL(ftmp.DIM_CUSTOMERGROUP1ID,1),
IFNULL(ftmp.DIM_DOCUMENTCATEGORYID,1),
IFNULL(ftmp.DIM_DOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_ITEMCATEGORYID,1),
IFNULL(ftmp.DIM_ITEMSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERTYPEID,1),
IFNULL(ftmp.DIM_PURCHASEGROUPID,1),
IFNULL(ftmp.DIM_PURCHASEORGID,1),
IFNULL(ftmp.DIM_SALESDOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_SALESGROUPID,1),
IFNULL(ftmp.DIM_SALESORDERHEADERSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERITEMSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERREJECTREASONID,1),
IFNULL(ftmp.DIM_SALESORGID,1),
IFNULL(ftmp.DIM_SPECIALSTOCKID,1),
IFNULL(ftmp.DIM_STOCKTYPEID,1),
IFNULL(ftmp.DIM_UNITOFMEASUREID,1),
IFNULL(ftmp.DIM_MATERIALGROUPID,1),
IFNULL(ftmp.AMT_EXCHANGERATE_GBL,1),
IFNULL(ftmp.AMT_EXCHANGERATE,1),
IFNULL(ftmp.DIM_TERMID,1),
IFNULL(ftmp.DIM_PROFITCENTERID,1),
IFNULL(ftmp.DIM_RECEIVINGPLANTID,1),
IFNULL(ftmp.DIM_RECEIVINGPARTID,1),
IFNULL(ftmp.DIM_DATEIDCOSTING,1),
IFNULL(ftmp.DIM_INCOTERMID,1),
IFNULL(ftmp.DIM_DATEIDDELIVERY,1),
IFNULL(ftmp.DIM_DATEIDSTATDELIVERY,1),
IFNULL(ftmp.DIM_GRSTATUSID,1),
IFNULL(ftmp.DIM_RECVISSUSTORLOCID,1),
IFNULL(ftmp.DIM_POPLANTIDORDERING,1),
IFNULL(ftmp.DIM_POPLANTIDSUPPLYING,1),
IFNULL(ftmp.DIM_POSTORAGELOCID,1),
IFNULL(ftmp.DIM_POISSUSTORAGELOCID,1),
IFNULL(ftmp.DIM_INSPUSAGEDECISIONID,1),
IFNULL(ftmp.DIM_AFSSIZEID,1),
IFNULL(ftmp.DIM_CURRENCYID_TRA,1),
IFNULL(ftmp.DIM_CURRENCYID_GBL,1),
IFNULL(ftmp.DIM_UNITOFMEASUREORDERUNITID,1),
IFNULL(ftmp.DIM_UOMUNITOFENTRYID,1),
IFNULL(ftmp.DIM_PROJECTSOURCEID,1),
IFNULL(ftmp.DW_INSERT_DATE,CURRENT_TIMESTAMP),
IFNULL(ftmp.DW_UPDATE_DATE,CURRENT_TIMESTAMP),
ftmp.DD_DOCUMENTITEMNO,
ftmp.DD_SALESORDERITEMNO,
ftmp.DD_SALESORDERDLVRNO,
ftmp.DD_PRODUCTIONORDERITEMNO,
ftmp.CT_LOTNOTTHRUQM,
ftmp.AMT_VENDORSPEND,
ftmp.FACT_MATERIALMOVEMENTID
 from fact_materialmovement  f
right join tmp_fact_materialmovement_perf1 ftmp
on f.fact_materialmovementid = ftmp.fact_materialmovementid
where f.fact_materialmovementid is  null;

/*Truncate perf table to be reused again*/
truncate table tmp_fact_materialmovement_perf1;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');

Drop table if exists max_holder_789;
create table max_holder_789
as
Select ifnull(max(fact_materialmovementid),ifnull((select DIM_PROJECTSOURCEID * MULTIPLIER from dim_projectsource),0)) as maxid
from fact_materialmovement;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1;
CREATE TABLE tmp_mm_MKPF_MSEG1 as
select MSEG1_MBLNR dd_MaterialDocNo, MSEG1_ZEILE dd_MaterialDocItemNo, MSEG1_MJAHR dd_MaterialDocYear
from MKPF_MSEG1;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_del;
CREATE TABLE tmp_mm_MKPF_MSEG1_del
as 
select * from tmp_mm_MKPF_MSEG1 where 1=2;
insert into tmp_mm_MKPF_MSEG1_del
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
From fact_materialmovement;

rename tmp_mm_MKPF_MSEG1 to tmp_mm_MKPF_MSEG1_1;

create table tmp_mm_MKPF_MSEG1 as 
(select * from tmp_mm_MKPF_MSEG1_1) minus (Select * from tmp_mm_MKPF_MSEG1_del);

drop table tmp_mm_MKPF_MSEG1_1;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_allcols;
CREATE TABLE tmp_mm_MKPF_MSEG1_allcols as
SELECT distinct m.*, row_number() over (partition by MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR order by '') rowno
FROM MKPF_MSEG1 m,tmp_mm_MKPF_MSEG1 m1
where m.MSEG1_MBLNR = m1.dd_MaterialDocNo
and m.MSEG1_ZEILE = m1.dd_MaterialDocItemNo
and m.MSEG1_MJAHR = m1.dd_MaterialDocYear;

drop table if exists tmp_fmatmov_t004t;
create table tmp_fmatmov_t004t as
   select max_holder_789.maxid + row_number() over(order by '') fact_materialmovementid,
		  /*hash(concat(m.MSEG_MBLNR,m.MSEG_ZEILE,m.MSEG_MJAHR)) fact_materialmovementid,*/
		  bb.* 
		  from max_holder_789,(SELECT DISTINCT
          m.MSEG1_MBLNR dd_MaterialDocNo,
          m.MSEG1_ZEILE dd_MaterialDocItemNo,
          m.MSEG1_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG1_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG1_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG1_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG1_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG1_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG1_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG1_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG1_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG1_AUFNR,'Not Set') dd_productionordernumber, 
          ifnull(m.MSEG1_AUFPS,1) dd_productionorderitemno,
          ifnull(m.MSEG1_GRUND,'Not Set') dd_GoodsMoveReason,
          ifnull(m.MSEG1_CHARG, 'Not Set') dd_BatchNumber,
          ifnull(m.MSEG1_BWTAR,'Not Set') dd_ValuationType,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR amt_LocalCurrAmt,
          ifnull(m.MSEG1_DMBTR,0) / (case when m.MSEG1_ERFMG =0 then 1 else  m.MSEG1_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BUALT amt_AltPriceControl,
          ifnull(m.MSEG1_SALK3,0) amt_OnHand_ValStock,
	  convert( decimal (18,4),1) amt_ExchangeRate,
	  convert( decimal (18,4),1) amt_ExchangeRate_GBL,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_MENGE ct_Quantity,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BSTMG ct_QtyGROrdUnit,
          ifnull(m.MSEG1_LBKUM,0) ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BPMNG ct_QtyOrdPriceUnit,
          convert(bigint, 1) Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          convert(bigint, 1) Dim_Currencyid,	/* Local curr */
	 convert(bigint, 1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
	convert(bigint, 1) Dim_StorageLocationid,
    convert(bigint, 1) Dim_Vendorid,
    convert(bigint, 1) dim_DateidMaterialDocDate,
    convert(bigint, 1) dim_DateIDPostingDate,
	convert(bigint, 1) dim_producthierarchyid,
   convert(bigint, 1) dim_MovementIndicatorid,
    convert(bigint, 1) dim_Customerid,
	convert(bigint, 1) dim_CostCenterid,
    convert(bigint, 1) dim_specialstockid,
    convert(bigint, 1) dim_unitofmeasureid,
    convert(bigint, 1) dim_controllingareaid,
    convert(bigint, 1) Dim_ProfitCenterId,
    convert(bigint, 1) dim_consumptiontypeid,
    convert(bigint, 1) dim_stocktypeid,
	convert(bigint, 1) Dim_PurchaseGroupid,
     convert(bigint, 1) Dim_materialgroupid,
     convert(bigint, 1) Dim_ReceivingPlantId,
     convert(bigint, 1) Dim_ReceivingPartId,
	convert(bigint, 1) Dim_RecvIssuStorLocid,
         convert(bigint, 1) Dim_Currencyid_TRA,								
          convert(bigint, 1) Dim_Currencyid_GBL,
	  convert(bigint, 1) DIM_DATEIDDOCCREATION,
				'N' as lotsreceivedflag,
     m.MSEG1_LIFNR,  --Dim_Vendorid
m.MSEG1_BUKRS, --dim_DateIDMaterialDocDate
m.MKPF_BLDAT,  --dim_DateIDMaterialDocDate
m.MKPF_BUDAT,  --dim_DateIDPostingDate
m.MSEG1_MATNR,   --dim_producthierarchyid
m.MSEG1_WERKS,    --dim_producthierarchyid
m.MSEG1_KZBEW,    -- dim_MovementIndicatorid
m.MSEG1_KUNNR,    --dim_Customerid
m.MSEG1_SOBKZ, --dim_specialstockid
m.MSEG1_MEINS, --dim_unitofmeasureid
m.MSEG1_KOKRS, --dim_controllingareaid
m.MSEG1_KZVBR, --dim_consumptiontypeid
m.MSEG1_INSMK, --dim_stocktypeid
m.MSEG1_UMMAT,  --Dim_ReceivingPartId
m.MSEG1_UMLGO,  --Dim_RecvIssuStorLocid
m.MSEG1_UMWRK,  --Dim_RecvIssuStorLocid
m.MSEG1_WAERS,  --Dim_Currencyid_TRA
pGlobalCurrency --Dim_Currencyid_GBL
      FROM pGlobalCurrency_tbl, tmp_mm_MKPF_MSEG1_allcols m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode ) bb;


update tmp_fmatmov_t004t f
set   f.Dim_Vendorid = ifnull(dv.Dim_Vendorid,1)
from tmp_fmatmov_t004t f
             left join Dim_Vendor dv
            on dv.VendorNumber = f.MSEG1_LIFNR
where f.Dim_Vendorid <> ifnull(dv.Dim_Vendorid,1);

update tmp_fmatmov_t004t f
set   f.dim_DateIDMaterialDocDate = ifnull(mdd.Dim_Dateid,1)
from tmp_fmatmov_t004t f
             left join dim_date mdd  on
              mdd.CompanyCode = f.MSEG1_BUKRS AND f.MKPF_BLDAT = mdd.DateValue
where  f.dim_DateIDMaterialDocDate <> ifnull(mdd.Dim_Dateid,1);

update tmp_fmatmov_t004t f
set   f.dim_DateIDPostingDate = ifnull(pd.Dim_Dateid,1)
from tmp_fmatmov_t004t f
             left join dim_date pd  on
             pd.CompanyCode = f.MSEG1_BUKRS AND f.MKPF_BUDAT = pd.DateValue
where  f.dim_DateIDPostingDate <> ifnull(pd.Dim_Dateid,1);

UPDATE tmp_fmatmov_t004t ms
  SET     ms.dim_producthierarchyid = ifnull(case WHEN ms.MSEG1_MATNR IS NULL then 1 else dpr1.dim_producthierarchyid end, 1)
        from tmp_fmatmov_t004t ms 
                 left join (SELECT dpr.PartNumber,dpr.Plant,dph.dim_producthierarchyid
                           FROM dim_producthierarchy dph, dim_part dpr
                           WHERE     dph.ProductHierarchy = dpr.ProductHierarchy) dpr1
                        on dpr1.PartNumber = ms.MSEG1_MATNR
                         AND dpr1.Plant = ms.MSEG1_WERKS
 where   ms.dim_producthierarchyid = ifnull(case WHEN ms.MSEG1_MATNR IS NULL then 1 else dpr1.dim_producthierarchyid end, 1);    

update tmp_fmatmov_t004t f
set   f.Dim_MovementIndicatorid = ifnull(dmi.Dim_MovementIndicatorid,1)
from tmp_fmatmov_t004t f
             left join dim_movementindicator dmi  on
             dmi.TypeCode = f.MSEG1_KZBEW
where  f.Dim_MovementIndicatorid <> ifnull(dmi.Dim_MovementIndicatorid,1);

update tmp_fmatmov_t004t f
set   f.dim_Customerid = ifnull(dcu.dim_Customerid,1)
from tmp_fmatmov_t004t f
             left join dim_customer dcu  on
             dcu.CustomerNumber = f.MSEG1_KUNNR
where  f.dim_Customerid <> ifnull(dcu.dim_Customerid,1);


update tmp_fmatmov_t004t f
set   f.Dim_specialstockid = ifnull(spt.dim_specialstockid,1)
from tmp_fmatmov_t004t f
             left join dim_specialstock spt  on
             spt.specialstockindicator = f.MSEG1_SOBKZ
where  f.Dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

update tmp_fmatmov_t004t f
set   f.Dim_unitofmeasureid = ifnull(uom.Dim_unitofmeasureid,1)
from tmp_fmatmov_t004t f
             left join dim_unitofmeasure uom  on
             uom.UOM = f.MSEG1_MEINS
where  f.Dim_unitofmeasureid <> ifnull(uom.Dim_unitofmeasureid,1);

update tmp_fmatmov_t004t f
set   f.Dim_controllingareaid = ifnull(ca.Dim_controllingareaid,1)
from tmp_fmatmov_t004t f
             left join dim_controllingarea ca  on
             ca.ControllingAreaCode = f.MSEG1_KOKRS
where f.Dim_controllingareaid <> ifnull(ca.Dim_controllingareaid,1);

update tmp_fmatmov_t004t f
set   f.Dim_consumptiontypeid = ifnull(ct.Dim_consumptiontypeid,1)
from tmp_fmatmov_t004t f
             left join dim_consumptiontype ct  on
             ct.ConsumptionCode = f.MSEG1_KZVBR
where f.Dim_consumptiontypeid <> ifnull(ct.Dim_consumptiontypeid,1);

update tmp_fmatmov_t004t f
set   f.Dim_stocktypeid = ifnull(st.Dim_StockTypeid,1)
from tmp_fmatmov_t004t f
             left join dim_stocktype st  on
             st.TypeCode = f.MSEG1_INSMK
where  f.Dim_stocktypeid <> ifnull(st.Dim_StockTypeid,1);

update tmp_fmatmov_t004t f
set   f.Dim_PurchaseGroupid = ifnull(case when  f.MSEG1_MATNR IS NULL then 1 else  pg1.Dim_PurchaseGroupid end ,1)
from tmp_fmatmov_t004t f
             left join 
          (select dpr.PartNumber,dpr.Plant,pg.Dim_PurchaseGroupid FROM dim_part dpr 
              inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup) pg1
         on pg1.PartNumber = f.MSEG1_MATNR AND pg1.Plant = f.MSEG1_WERKS
where  f.Dim_PurchaseGroupid <> ifnull(case when  f.MSEG1_MATNR IS NULL then 1 else  pg1.Dim_PurchaseGroupid end ,1);

update tmp_fmatmov_t004t f
set   f.Dim_materialgroupid = ifnull(mg1.Dim_materialgroupid,1)
from tmp_fmatmov_t004t f
             left join 
          (select dpr.PartNumber,dpr.Plant,mg.Dim_materialgroupid FROM dim_part dpr 
              inner join dim_materialgroup mg on mg.MaterialGroupCode = dpr.MaterialGroup) mg1
         on mg1.PartNumber = f.MSEG1_MATNR AND mg1.Plant = f.MSEG1_WERKS
where  f.Dim_materialgroupid <> ifnull(mg1.Dim_materialgroupid,1);

update tmp_fmatmov_t004t f
set   f.Dim_ReceivingPlantId = ifnull(pl.dim_plantid,1)
from tmp_fmatmov_t004t  f
             left join dim_plant pl  on
             pl.PlantCode = f.MSEG1_UMWRK
where f.Dim_ReceivingPlantId <> ifnull(pl.dim_plantid,1);

update tmp_fmatmov_t004t f
set   f.Dim_ReceivingPartId = ifnull(prt.Dim_Partid,1)
from tmp_fmatmov_t004t f
             left join dim_part prt
            on prt.Plant = f.MSEG1_UMWRK AND prt.PartNumber = f.MSEG1_UMMAT
where f.Dim_ReceivingPartId <> ifnull(prt.Dim_Partid,1);


update tmp_fmatmov_t004t f
set   f.Dim_RecvIssuStorLocid = ifnull((case when  f.MSEG1_UMLGO IS NULL then 1 else dsl.Dim_StorageLocationid end),1)
from tmp_fmatmov_t004t f
             left join dim_storagelocation dsl
            on dsl.LocationCode = f.MSEG1_UMLGO AND dsl.Plant = f.MSEG1_UMWRK
where f.Dim_RecvIssuStorLocid <> ifnull((case when  f.MSEG1_UMLGO IS NULL then 1 else dsl.Dim_StorageLocationid end),1);

update tmp_fmatmov_t004t f
set   f.Dim_CurrencyId_TRA = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t004t f
             left join dim_currency dcr
            on dcr.CurrencyCode = f.MSEG1_WAERS
where f.Dim_CurrencyId_TRA <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t004t f
set   f.Dim_CurrencyId_GBL = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t004t f
             left join dim_currency dcr
            on dcr.CurrencyCode = pGlobalCurrency
where f.Dim_CurrencyId_GBL <> ifnull(dcr.Dim_Currencyid,1);


  INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,Dim_Currencyid_TRA, Dim_Currencyid_GBL,
lotsreceivedflag)
select fact_materialmovementid,
              dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,Dim_Currencyid_TRA, Dim_Currencyid_GBL,
lotsreceivedflag
from tmp_fmatmov_t004t;	 

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_StdUnitPrice = 0
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_ERFMG = 0
AND ms.amt_StdUnitPrice <> 0;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_currencyid = dcr.dim_currencyid
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc, dim_currency dcr, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dcr.CurrencyCode = dc.currency
AND m.rowno = 1
AND ms.dim_currencyid <> dcr.dim_currencyid;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_partid = dpr.dim_partid
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc,dim_part dpr, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dpr.PartNumber = m.MSEG1_MATNR
AND dpr.Plant = m.MSEG1_WERKS
AND m.rowno = 1
AND ms.dim_partid <> dpr.dim_partid;



UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_storagelocationid = dsl.dim_storagelocationid
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc,dim_storagelocation dsl, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dsl.LocationCode = m.MSEG1_LGORT 
AND dsl.Plant = m.MSEG1_WERKS
AND m.rowno = 1
AND ms.dim_storagelocationid <> dsl.dim_storagelocationid;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_exchangerate = z.exchangerate
FROM dim_currency tra, dim_currency lcl,dim_date d, tmp_getExchangeRate1 z, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_currencyid_tra = tra.dim_currencyid
AND ms.dim_currencyid = lcl.dim_currencyid
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = lcl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate <> z.exchangerate;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_exchangerate_gbl = z.exchangerate
FROM dim_currency tra, dim_currency gbl,dim_date d, tmp_getExchangeRate1 z, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_currencyid_tra = tra.dim_currencyid
AND ms.dim_currencyid_gbl = gbl.dim_currencyid
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = gbl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate_gbl <> z.exchangerate;

UPDATE tmp_fact_materialmovement_perf1 ms
SET Dim_MovementTypeid = mt.Dim_MovementTypeid
FROM dim_movementtype mt, tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_WERKS = dp.PlantCode
AND  m.MSEG1_BUKRS = dc.CompanyCode
AND mt.MovementType = m.MSEG1_BWART
AND mt.ConsumptionIndicator = ifnull(m.MSEG1_KZVBR, 'Not Set')
AND mt.MovementIndicator = ifnull(m.MSEG1_KZBEW, 'Not Set')
AND mt.ReceiptIndicator = ifnull(m.MSEG1_KZZUG, 'Not Set')
AND mt.SpecialStockIndicator = ifnull(m.MSEG1_SOBKZ, 'Not Set')
AND m.rowno = 1
AND ms.Dim_MovementTypeid <> mt.Dim_MovementTypeid;


drop table if exists max_holder_789;


drop table if exists tmp_MKPF_MSEG1up;
create table tmp_MKPF_MSEG1up as 
select m.*, row_number() over(partition by MSEG1_MJAHR,MSEG1_MBLNR,MSEG1_ZEILE order by '') rowno from MKPF_MSEG1 m;

Update tmp_fact_materialmovement_perf1 fmt
set fmt.Dim_ProfitCenterId =  ifnull( pc.Dim_ProfitCenterid,1) 
From pGlobalCurrency_tbl, tmp_MKPF_MSEG1up m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
          inner join tmp_fact_materialmovement_perf1 fmt
    on  fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
        AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear
        AND  fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
        AND  m.rowno = 1
           left join dim_profitcenter_789 pc
    on pc.ControllingArea = m.MSEG1_KOKRS
		      AND pc.ProfitCenterCode = m.MSEG1_PRCTR
		      AND pc.ValidTo >= m.MKPF_BUDAT
where fmt.Dim_ProfitCenterId <> ifnull( pc.Dim_ProfitCenterid,1);


Update tmp_fact_materialmovement_perf1 fmt
set fmt.dim_CostCenterid = ifnull(case  WHEN m.MSEG1_KOSTL IS NULL then 1 else dcc1.dim_CostCenterid end,1)
From pGlobalCurrency_tbl, tmp_MKPF_MSEG1up m   /*MKPF_MSEG1 m*/
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
          inner join tmp_fact_materialmovement_perf1 fmt
    on  fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
        AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear
        AND  fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
         AND  m.rowno = 1
           left join (select dcc.Code,dcc.ControllingArea,min(dcc.dim_CostCenterid) as dim_CostCenterid from MKPF_MSEG1 m 
inner join dim_costcenter dcc
    on   dcc.Code = m.MSEG1_KOSTL
       AND m.MSEG1_KOKRS = dcc.ControllingArea
    where dcc.validTo >= m.MKPF_BUDAT
    group by dcc.Code,dcc.ControllingArea) dcc1
     on   dcc1.Code = m.MSEG1_KOSTL
       AND m.MSEG1_KOKRS = dcc1.ControllingArea
where fmt.dim_CostCenterid <> ifnull(case  WHEN m.MSEG1_KOSTL IS NULL then 1 else dcc1.dim_CostCenterid end,1);


/*Populate mm with data in tmp table. Note that this won't need any not exists as its already handled in combine before */
insert into  fact_materialmovement
(LOTSACCEPTEDFLAG,
LOTSREJECTEDFLAG,
PERCENTLOTSINSPECTEDFLAG,
PERCENTLOTSREJECTEDFLAG,
PERCENTLOTSACCEPTEDFLAG,
LOTSSKIPPEDFLAG,
LOTSAWAITINGINSPECTIONFLAG,
LOTSINSPECTEDFLAG,
LOTSRECEIVEDFLAG,
DD_MATERIALDOCNO,
DD_DOCUMENTNO,
DD_SALESORDERNO,
DD_GLACCOUNTNO,
DD_PRODUCTIONORDERNUMBER,
DD_BATCHNUMBER,
DD_VALUATIONTYPE,
DD_DEBITCREDITID,
DD_INCOTERMS2,
DD_INTORDER,
DD_REFERENCEDOCNO,
DD_INSPECTIONSTATUSINGRDOC,
DD_ITEMAUTOMATICALLYCREATED,
DD_MATERIALDOCITEMNO,
DD_MATERIALDOCYEAR,
DD_GOODSMOVEREASON,
AMT_LOCALCURRAMT,
AMT_DELIVERYCOST,
AMT_ALTPRICECONTROL,
AMT_ONHAND_VALSTOCK,
CT_QUANTITY,
CT_QTYENTRYUOM,
CT_QTYGRORDUNIT,
CT_QTYONHAND_VALSTOCK,
CT_QTYORDPRICEUNIT,
DIRTYROW,
DD_CONSIGNMENTFLAG,
AMT_POUNITPRICE,
AMT_STDUNITPRICE,
AMT_PLANNEDPRICE,
AMT_PLANNEDPRICE1,
DD_DOCUMENTSCHEDULENO,
CT_ORDERQUANTITY,
DD_REFERENCEDOCITEM,
QTYREJECTEDEXTERNALAMT,
QTYREJECTEDINTERNALAMT,
DD_VENDORSPENDFLAG,
DD_RESERVATIONNUMBER,
DIM_MOVEMENTTYPEID,
DIM_COMPANYID,
DIM_CURRENCYID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_VENDORID,
DIM_DATEIDMATERIALDOCDATE,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCCREATION,
DIM_DATEIDORDERED,
DIM_PRODUCTHIERARCHYID,
DIM_MOVEMENTINDICATORID,
DIM_CUSTOMERID,
DIM_COSTCENTERID,
DIM_ACCOUNTCATEGORYID,
DIM_CONSUMPTIONTYPEID,
DIM_CONTROLLINGAREAID,
DIM_CUSTOMERGROUP1ID,
DIM_DOCUMENTCATEGORYID,
DIM_DOCUMENTTYPEID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEORGID,
DIM_SALESDOCUMENTTYPEID,
DIM_SALESGROUPID,
DIM_SALESORDERHEADERSTATUSID,
DIM_SALESORDERITEMSTATUSID,
DIM_SALESORDERREJECTREASONID,
DIM_SALESORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKTYPEID,
DIM_UNITOFMEASUREID,
DIM_MATERIALGROUPID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_TERMID,
DIM_PROFITCENTERID,
DIM_RECEIVINGPLANTID,
DIM_RECEIVINGPARTID,
DIM_DATEIDCOSTING,
DIM_INCOTERMID,
DIM_DATEIDDELIVERY,
DIM_DATEIDSTATDELIVERY,
DIM_GRSTATUSID,
DIM_RECVISSUSTORLOCID,
DIM_POPLANTIDORDERING,
DIM_POPLANTIDSUPPLYING,
DIM_POSTORAGELOCID,
DIM_POISSUSTORAGELOCID,
DIM_INSPUSAGEDECISIONID,
DIM_AFSSIZEID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DIM_UNITOFMEASUREORDERUNITID,
DIM_UOMUNITOFENTRYID,
DIM_PROJECTSOURCEID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_DOCUMENTITEMNO,
DD_SALESORDERITEMNO,
DD_SALESORDERDLVRNO,
DD_PRODUCTIONORDERITEMNO,
CT_LOTNOTTHRUQM,
AMT_VENDORSPEND,
FACT_MATERIALMOVEMENTID) 
select IFNULL(ftmp.LOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSSKIPPEDFLAG,'N'),
IFNULL(ftmp.LOTSAWAITINGINSPECTIONFLAG,'N'),
IFNULL(ftmp.LOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.LOTSRECEIVEDFLAG,'N'),
IFNULL(ftmp.DD_MATERIALDOCNO,'Not Set'),
IFNULL(ftmp.DD_DOCUMENTNO,'Not Set'),
IFNULL(ftmp.DD_SALESORDERNO,'Not Set'),
IFNULL(ftmp.DD_GLACCOUNTNO,'Not Set'),
IFNULL(ftmp.DD_PRODUCTIONORDERNUMBER,'Not Set'),
IFNULL(ftmp.DD_BATCHNUMBER,'Not Set'),
IFNULL(ftmp.DD_VALUATIONTYPE,'Not Set'),
IFNULL(ftmp.DD_DEBITCREDITID,'Not Set'),
IFNULL(ftmp.DD_INCOTERMS2,'Not Set'),
IFNULL(ftmp.DD_INTORDER,'Not Set'),
IFNULL(ftmp.DD_REFERENCEDOCNO,'Not Set'),
IFNULL(ftmp.DD_INSPECTIONSTATUSINGRDOC,'Not Set'),
IFNULL(ftmp.DD_ITEMAUTOMATICALLYCREATED,'Not Set'),
IFNULL(ftmp.DD_MATERIALDOCITEMNO,0),
IFNULL(ftmp.DD_MATERIALDOCYEAR,0),
IFNULL(ftmp.DD_GOODSMOVEREASON,0),
IFNULL(ftmp.AMT_LOCALCURRAMT,0),
IFNULL(ftmp.AMT_DELIVERYCOST,0),
IFNULL(ftmp.AMT_ALTPRICECONTROL,0),
IFNULL(ftmp.AMT_ONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QUANTITY,0),
IFNULL(ftmp.CT_QTYENTRYUOM,0),
IFNULL(ftmp.CT_QTYGRORDUNIT,0),
IFNULL(ftmp.CT_QTYONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QTYORDPRICEUNIT,0),
IFNULL(ftmp.DIRTYROW,0),
IFNULL(ftmp.DD_CONSIGNMENTFLAG,0),
IFNULL(ftmp.AMT_POUNITPRICE,0),
IFNULL(ftmp.AMT_STDUNITPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE1,0),
IFNULL(ftmp.DD_DOCUMENTSCHEDULENO,0),
IFNULL(ftmp.CT_ORDERQUANTITY,0),
IFNULL(ftmp.DD_REFERENCEDOCITEM,0),
IFNULL(ftmp.QTYREJECTEDEXTERNALAMT,0),
IFNULL(ftmp.QTYREJECTEDINTERNALAMT,0),
IFNULL(ftmp.DD_VENDORSPENDFLAG,0),
IFNULL(ftmp.DD_RESERVATIONNUMBER,0),
IFNULL(ftmp.DIM_MOVEMENTTYPEID,1),
IFNULL(ftmp.DIM_COMPANYID,1),
IFNULL(ftmp.DIM_CURRENCYID,1),
IFNULL(ftmp.DIM_PARTID,1),
IFNULL(ftmp.DIM_PLANTID,1),
IFNULL(ftmp.DIM_STORAGELOCATIONID,1),
IFNULL(ftmp.DIM_VENDORID,1),
IFNULL(ftmp.DIM_DATEIDMATERIALDOCDATE,1),
IFNULL(ftmp.DIM_DATEIDPOSTINGDATE,1),
IFNULL(ftmp.DIM_DATEIDDOCCREATION,1),
IFNULL(ftmp.DIM_DATEIDORDERED,1),
IFNULL(ftmp.DIM_PRODUCTHIERARCHYID,1),
IFNULL(ftmp.DIM_MOVEMENTINDICATORID,1),
IFNULL(ftmp.DIM_CUSTOMERID,1),
IFNULL(ftmp.DIM_COSTCENTERID,1),
IFNULL(ftmp.DIM_ACCOUNTCATEGORYID,1),
IFNULL(ftmp.DIM_CONSUMPTIONTYPEID,1),
IFNULL(ftmp.DIM_CONTROLLINGAREAID,1),
IFNULL(ftmp.DIM_CUSTOMERGROUP1ID,1),
IFNULL(ftmp.DIM_DOCUMENTCATEGORYID,1),
IFNULL(ftmp.DIM_DOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_ITEMCATEGORYID,1),
IFNULL(ftmp.DIM_ITEMSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERTYPEID,1),
IFNULL(ftmp.DIM_PURCHASEGROUPID,1),
IFNULL(ftmp.DIM_PURCHASEORGID,1),
IFNULL(ftmp.DIM_SALESDOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_SALESGROUPID,1),
IFNULL(ftmp.DIM_SALESORDERHEADERSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERITEMSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERREJECTREASONID,1),
IFNULL(ftmp.DIM_SALESORGID,1),
IFNULL(ftmp.DIM_SPECIALSTOCKID,1),
IFNULL(ftmp.DIM_STOCKTYPEID,1),
IFNULL(ftmp.DIM_UNITOFMEASUREID,1),
IFNULL(ftmp.DIM_MATERIALGROUPID,1),
IFNULL(ftmp.AMT_EXCHANGERATE_GBL,1),
IFNULL(ftmp.AMT_EXCHANGERATE,1),
IFNULL(ftmp.DIM_TERMID,1),
IFNULL(ftmp.DIM_PROFITCENTERID,1),
IFNULL(ftmp.DIM_RECEIVINGPLANTID,1),
IFNULL(ftmp.DIM_RECEIVINGPARTID,1),
IFNULL(ftmp.DIM_DATEIDCOSTING,1),
IFNULL(ftmp.DIM_INCOTERMID,1),
IFNULL(ftmp.DIM_DATEIDDELIVERY,1),
IFNULL(ftmp.DIM_DATEIDSTATDELIVERY,1),
IFNULL(ftmp.DIM_GRSTATUSID,1),
IFNULL(ftmp.DIM_RECVISSUSTORLOCID,1),
IFNULL(ftmp.DIM_POPLANTIDORDERING,1),
IFNULL(ftmp.DIM_POPLANTIDSUPPLYING,1),
IFNULL(ftmp.DIM_POSTORAGELOCID,1),
IFNULL(ftmp.DIM_POISSUSTORAGELOCID,1),
IFNULL(ftmp.DIM_INSPUSAGEDECISIONID,1),
IFNULL(ftmp.DIM_AFSSIZEID,1),
IFNULL(ftmp.DIM_CURRENCYID_TRA,1),
IFNULL(ftmp.DIM_CURRENCYID_GBL,1),
IFNULL(ftmp.DIM_UNITOFMEASUREORDERUNITID,1),
IFNULL(ftmp.DIM_UOMUNITOFENTRYID,1),
IFNULL(ftmp.DIM_PROJECTSOURCEID,1),
IFNULL(ftmp.DW_INSERT_DATE,CURRENT_TIMESTAMP),
IFNULL(ftmp.DW_UPDATE_DATE,CURRENT_TIMESTAMP),
ftmp.DD_DOCUMENTITEMNO,
ftmp.DD_SALESORDERITEMNO,
ftmp.DD_SALESORDERDLVRNO,
ftmp.DD_PRODUCTIONORDERITEMNO,
ftmp.CT_LOTNOTTHRUQM,
ftmp.AMT_VENDORSPEND,
ftmp.FACT_MATERIALMOVEMENTID
 from fact_materialmovement  f
right join tmp_fact_materialmovement_perf1 ftmp
on f.fact_materialmovementid = ftmp.fact_materialmovementid
where f.fact_materialmovementid is  null;

truncate table tmp_fact_materialmovement_perf1;	

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');                        

/*call vectorwise (combine 'fact_materialmovement')*/

UPDATE fact_materialmovement mm 
SET  mm.dd_VendorSpendFlag = 1
 FROM dim_movementtype mt,fact_materialmovement mm  
WHERE mm.dim_movementtypeid = mt.dim_movementtypeid
AND ( mt.movementtype IN (101,102,105,106,122,123,161,162,501,502,521,522) OR mm.dd_ConsignmentFlag = 1)
AND mm.dd_VendorSpendFlag <> 1;

UPDATE fact_materialmovement mm
SET mm.amt_VendorSpend = ( CASE WHEN mm.amt_AltPriceControl = 0 THEN mm.amt_LocalCurrAmt ELSE mm.amt_AltPriceControl END) + amt_DeliveryCost
WHERE mm.dd_VendorSpendFlag = 1;


     INSERT INTO processinglog (processinglogid, referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'UPDATE fact_materialmovement ms,fact_purchase po');

DROP TABLE IF EXISTS tmp_upd_po_to_mm;
CREATE TABLE tmp_upd_po_to_mm
AS
SELECT po.*
FROM fact_purchase po,
(SELECT fp.dd_DocumentNo,fp.dd_DocumentItemNo,min(dd_scheduleno) dd_scheduleno
FROM fact_purchase fp GROUP BY fp.dd_DocumentNo,fp.dd_DocumentItemNo) t
WHERE po.dd_DocumentNo = t.dd_DocumentNo
AND po.dd_DocumentItemNo = t.dd_DocumentItemNo
AND po.dd_scheduleno = t.dd_scheduleno;
     
   UPDATE fact_materialmovement ms
    SET ms.dim_purchasegroupid =  po.dim_purchasegroupid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  po.dim_purchasegroupid <> 1
	AND  ms.dim_purchasegroupid <>  po.dim_purchasegroupid;

DROP TABLE IF EXISTS tmp_upd_po_to_mm_maxpurchorgid;
CREATE TABLE tmp_upd_po_to_mm_maxpurchorgid
AS
SELECT dd_DocumentNo,dd_DocumentItemNo,max(dim_purchaseorgid) dim_purchaseorgid_max
from tmp_upd_po_to_mm
group by dd_DocumentNo,dd_DocumentItemNo;

UPDATE fact_materialmovement ms
    SET        ms.dim_purchaseorgid = po.dim_purchaseorgid_max
From tmp_upd_po_to_mm_maxpurchorgid po, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND  ms.dim_purchaseorgid <> po.dim_purchaseorgid_max;

DROP TABLE IF EXISTS tmp_upd_po_to_mm_maxpurchorgid; 

    UPDATE fact_materialmovement ms
    SET      ms.dim_itemcategoryid = po.dim_itemcategoryid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_itemcategoryid <> po.dim_itemcategoryid;

         UPDATE fact_materialmovement ms
    SET ms.dim_Termid = po.dim_Termid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl,fact_materialmovement ms 
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_Termid <> po.dim_Termid;

       UPDATE fact_materialmovement ms
    SET ms.dim_documenttypeid = po.dim_documenttypeid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_documenttypeid <> po.dim_documenttypeid;


     UPDATE fact_materialmovement ms
    SET ms.dim_accountcategoryid = po.dim_accountcategoryid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_accountcategoryid <>  po.dim_accountcategoryid;

     UPDATE fact_materialmovement ms
    SET ms.dim_itemstatusid = po.dim_itemstatusid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.dim_itemstatusid <> po.dim_itemstatusid;


      UPDATE fact_materialmovement ms
    SET ms.Dim_DateIDDocCreation = po.Dim_DateidCreate
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDDocCreation <> po.Dim_DateidCreate;

      UPDATE fact_materialmovement ms
    SET ms.Dim_DateIDOrdered = po.Dim_DateidOrder
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDOrdered <> po.Dim_DateidOrder;

       UPDATE fact_materialmovement ms
    SET ms.Dim_MaterialGroupid = po.Dim_MaterialGroupid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_MaterialGroupid <>  po.Dim_MaterialGroupid;

    UPDATE fact_materialmovement ms
    SET ms.amt_POUnitPrice = po.amt_UnitPrice * po.amt_ExchangeRate
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.amt_POUnitPrice <>  (po.amt_UnitPrice * po.amt_ExchangeRate);

    UPDATE fact_materialmovement ms
    SET ms.amt_StdUnitPrice = po.amt_StdUnitPrice * po.amt_ExchangeRate
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.amt_StdUnitPrice <> (po.amt_StdUnitPrice * po.amt_ExchangeRate);

      UPDATE fact_materialmovement ms
    SET ms.Dim_DateidCosting = po.Dim_DateidCosting
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateidCosting <> po.Dim_DateidCosting;

     UPDATE fact_materialmovement ms
    SET ms.Dim_IncoTermid = po.Dim_IncoTerm1id
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_IncoTermid <> po.Dim_IncoTerm1id;

    UPDATE fact_materialmovement ms
    SET ms.dd_incoterms2 = po.dd_incoterms2
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dd_incoterms2 <>  po.dd_incoterms2;

    UPDATE fact_materialmovement ms
    SET ms.dd_IntOrder = po.dd_IntOrder
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  ms.dd_IntOrder <> po.dd_IntOrder;

       UPDATE fact_materialmovement ms
    SET ms.dd_IntOrder = po.dd_IntOrder
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
        AND  ms.dd_IntOrder is null 
	AND po.dd_IntOrder is not null;

      UPDATE fact_materialmovement ms
    SET ms.Dim_POPlantidOrdering = po.Dim_PlantidOrdering
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POPlantidOrdering <> po.Dim_PlantidOrdering;

     UPDATE fact_materialmovement ms
    SET ms.Dim_POPlantidSupplying = po.Dim_PlantidSupplying
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POPlantidSupplying <> po.Dim_PlantidSupplying;

    UPDATE fact_materialmovement ms
    SET ms.Dim_POStorageLocid = po.Dim_StorageLocationid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl,fact_materialmovement ms 
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POStorageLocid <>  po.Dim_StorageLocationid;

    UPDATE fact_materialmovement ms
    SET ms.Dim_POIssuStorageLocid = po.Dim_IssuStorageLocid
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_POIssuStorageLocid <>  po.Dim_IssuStorageLocid;

       UPDATE fact_materialmovement ms
    SET ms.Dim_ReceivingPlantId = (case when ms.Dim_ReceivingPlantId = 1 and ms.Dim_Plantid <> po.Dim_PlantidOrdering 
                                              and (ms.Dim_Plantid = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId end)
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  ms.Dim_ReceivingPlantId <> (case when ms.Dim_ReceivingPlantId = 1 and ms.Dim_Plantid <> po.Dim_PlantidOrdering 
                                              and (ms.Dim_Plantid = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId end);

    UPDATE fact_materialmovement ms
    SET ms.Dim_RecvIssuStorLocid = (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid end)
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  ms.Dim_RecvIssuStorLocid <> (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid end);
        
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'UPDATE fact_materialmovement ms,fact_purchase po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement m,fact_salesorder so');
/*
DROP TABLE IF EXISTS tmp_upd_so_to_mm
CREATE TABLE tmp_upd_so_to_mm
AS
SELECT po.*
FROM fact_salesorder po,
(SELECT fp.dd_SalesDocNo,fp.dd_SalesItemNo,min(dd_scheduleno) dd_scheduleno
FROM fact_salesorder fp GROUP BY fp.dd_SalesDocNo,fp.dd_SalesItemNo) t
WHERE po.dd_SalesDocNo = t.dd_SalesDocNo
AND po.dd_SalesItemNo = t.dd_SalesItemNo
AND po.dd_scheduleno = t.dd_scheduleno

DROP TABLE IF EXISTS tmp_upd_so_to_mm_agg
CREATE TABLE tmp_upd_so_to_mm_agg as 
select dd_SalesDocNo,dd_SalesItemNo,max(dim_salesorderheaderstatusid) as dim_salesorderheaderstatusid,
max(dim_salesorderitemstatusid) as dim_salesorderitemstatusid,
max(dim_salesdocumenttypeid) as dim_salesdocumenttypeid,
max(dim_salesorderrejectreasonid) as dim_salesorderrejectreasonid,
max(dim_salesgroupid) as dim_salesgroupid,
max(dim_salesorgid) as dim_salesorgid,
max(dim_customergroup1id) as dim_customergroup1id,
max(dim_documentcategoryid)as dim_documentcategoryid,
max(Dim_DateidSalesOrderCreated) as Dim_DateidSalesOrderCreated
from tmp_upd_so_to_mm
group by dd_SalesDocNo,dd_SalesItemNo*/

drop table if exists tmp_upd_so_to_mm_agg;
create table tmp_upd_so_to_mm_agg
as
select dd_SalesDocNo,dd_SalesItemNo,dim_salesorderheaderstatusid,dim_salesorderitemstatusid,dim_salesdocumenttypeid,dim_salesorderrejectreasonid,dim_salesgroupid,dim_salesorgid,dim_customergroup1id,dim_documentcategoryid,Dim_DateidSalesOrderCreated,
row_number() over(partition by dd_SalesDocNo,dd_SalesItemNo order by dd_scheduleno asc) rono
from fact_salesorder  where (dd_SalesDocNo,dd_SalesItemNo) in (select distinct dd_SalesOrderNo,dd_SalesOrderItemNo from fact_materialmovement);
  
UPDATE fact_materialmovement ms
    SET ms.dim_salesorderheaderstatusid = so.dim_salesorderheaderstatusid
	FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.dim_salesorderheaderstatusid <> so.dim_salesorderheaderstatusid;

   UPDATE fact_materialmovement ms
    SET         ms.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.dim_salesorderitemstatusid <>  so.dim_salesorderitemstatusid;

          UPDATE fact_materialmovement ms
    SET ms.dim_salesdocumenttypeid = so.dim_salesdocumenttypeid
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.dim_salesdocumenttypeid <> so.dim_salesdocumenttypeid;

          UPDATE fact_materialmovement ms
    SET ms.dim_salesorderrejectreasonid = so.dim_salesorderrejectreasonid
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.dim_salesorderrejectreasonid <> so.dim_salesorderrejectreasonid;

          UPDATE fact_materialmovement ms
    SET ms.dim_salesgroupid = so.dim_salesgroupid
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.dim_salesgroupid <>  so.dim_salesgroupid;

          UPDATE fact_materialmovement ms
    SET ms.dim_salesorgid = so.dim_salesorgid
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.dim_salesorgid <> so.dim_salesorgid;

          UPDATE fact_materialmovement ms
    SET ms.dim_customergroup1id = so.dim_customergroup1id
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.dim_customergroup1id <>  so.dim_customergroup1id;

          UPDATE fact_materialmovement ms
    SET ms.dim_documentcategoryid = so.dim_documentcategoryid
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.dim_documentcategoryid <> so.dim_documentcategoryid;

          UPDATE fact_materialmovement ms
    SET ms.Dim_DateIDDocCreation = so.Dim_DateidSalesOrderCreated
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.Dim_DateIDDocCreation <>  so.Dim_DateidSalesOrderCreated;

          UPDATE fact_materialmovement ms
    SET ms.Dim_DateIDOrdered = so.Dim_DateidSalesOrderCreated
FROM tmp_upd_so_to_mm_agg so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo and so.rono = 1
AND  ms.Dim_DateIDOrdered <> so.Dim_DateidSalesOrderCreated; 
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement m,fact_salesorder so');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement ms,fact_productionorder po');

UPDATE fact_materialmovement ms
    SET ms.dim_productionorderstatusid = po.dim_productionorderstatusid
FROM fact_productionorder po,fact_materialmovement ms
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionorderstatusid <>  po.dim_productionorderstatusid;

UPDATE fact_materialmovement ms
    SET  ms.dim_productionordertypeid = po.Dim_ordertypeid
FROM fact_productionorder po,fact_materialmovement ms
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionordertypeid <> po.Dim_ordertypeid;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement ms,fact_productionorder po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

  UPDATE fact_materialmovement ms
    SET ms.dim_inspusagedecisionid = fil.dim_inspusagedecisionid
FROM fact_inspectionlot fil, fact_materialmovement ms
  WHERE     ms.dd_MaterialDocItemNo = fil.dd_MaterialDocItemNo
        AND ms.dd_MaterialDocNo = fil.dd_MaterialDocNo
        AND ms.dd_MaterialDocYear = fil.dd_MaterialDocYear
AND  ms.dim_inspusagedecisionid <> fil.dim_inspusagedecisionid;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'bi_populate_materialmovement_fact END');

drop table if exists bwtarupd_tmp_100;

create table bwtarupd_tmp_100 as
Select  x.MATNR, x.BWKEY, max((x.LFGJA * 100) + x.LFMON) updcol 
from mbew_no_bwtar x
Group by  x.MATNR, x.BWKEY;

drop table if exists tmp_mbew_no_bwtar_rono;
create table tmp_mbew_no_bwtar_rono
as
select x.*,row_number() over(partition by MATNR,BWKEY,LFGJA,LFMON order by '') rono from mbew_no_bwtar x;
		
		
UPDATE fact_materialmovement ia
SET amt_PlannedPrice1 = ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0)
FROM tmp_mbew_no_bwtar_rono m,
bwtarupd_tmp_100 x,
dim_plant pl,
dim_part prt,
fact_materialmovement ia
WHERE ia.dim_partid = prt.Dim_partid
and ia.dim_plantid = pl.dim_plantid
and prt.PartNumber = m.MATNR
and pl.ValuationArea = m.BWKEY
and ((m.LFGJA * 100) + m.LFMON) = updcol
and x.MATNR = m.MATNR
and x.BWKEY = m.BWKEY
and m.rono = 1
and amt_PlannedPrice1 <> ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0);

UPDATE fact_materialmovement ia
SET amt_PlannedPrice = ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0)
   FROM  tmp_mbew_no_bwtar_rono m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt,
       fact_materialmovement ia
 WHERE ia.dim_partid = prt.Dim_partid
  and ia.dim_plantid = pl.dim_plantid
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol 
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and m.rono = 1
  and  amt_PlannedPrice <> ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0);

drop table if exists tmp_mbew_no_bwtar_rono;
  
/*call bi_purchase_matmovement_dlvr_link()*/

UPDATE fact_materialmovement ms
  SET ms.dim_unitofmeasureorderunitid = uom.dim_unitofmeasureid
 FROM dim_unitofmeasure uom,tmp1_MKPF_MSEG m,dim_plant dp,dim_company dc, fact_materialmovement ms
  WHERE uom.uom = m.MSEG_BSTME
AND m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND m.rono = 1
AND ifnull(ms.dim_unitofmeasureorderunitid,-1) <> ifnull(uom.dim_unitofmeasureid,-2);

UPDATE fact_materialmovement ms
  SET ms.dim_unitofmeasureorderunitid = uom.dim_unitofmeasureid
FROM dim_unitofmeasure uom,tmp_MKPF_MSEG1up m,dim_plant dp,dim_company dc, fact_materialmovement ms
  WHERE uom.uom = m.MSEG1_BSTME
AND m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.rowno = 1
AND ifnull(ms.dim_unitofmeasureorderunitid,-1) <> ifnull(uom.dim_unitofmeasureid,-2);
  
UPDATE fact_materialmovement ms
set ms.dim_unitofmeasureorderunitid = 1
WHERE  ms.dim_unitofmeasureorderunitid IS NULL;
  
 UPDATE fact_materialmovement ms
  SET ms.dim_uomunitofentryid = uom.dim_unitofmeasureid
 FROM dim_unitofmeasure uom,tmp1_MKPF_MSEG m,dim_plant dp,dim_company dc,fact_materialmovement ms
  WHERE uom.uom = m.MSEG_ERFME
AND m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
and m.rono = 1
AND ifnull(ms.dim_uomunitofentryid,-1) <> ifnull(uom.dim_unitofmeasureid,-2); 
  
 UPDATE fact_materialmovement ms
  SET ms.dim_uomunitofentryid = uom.dim_unitofmeasureid
  FROM dim_unitofmeasure uom,tmp_MKPF_MSEG1up m,dim_plant dp,dim_company dc,fact_materialmovement ms 
  WHERE uom.uom = m.MSEG1_ERFME
AND m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.rowno = 1
AND ifnull(ms.dim_uomunitofentryid,-1) <> ifnull(uom.dim_unitofmeasureid,-2); 

UPDATE fact_materialmovement ms
set ms.dim_uomunitofentryid = 1
WHERE  ms.dim_uomunitofentryid IS NULL;

/* Update BW Hierarchy */

update fact_materialmovement f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_materialmovement f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;


/*update fact_materialmovement f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_materialmovement f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and bw.UPPERHIERID = '50CPPKTEWMWIK6D42R04HCPIH'
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid*/


update fact_materialmovement f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_materialmovement f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

/* MDG Part */

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_MM;
CREATE TABLE TMP_DIM_MDG_PARTID_MM as
SELECT DISTINCT pr.dim_partid, md.dim_mdg_partid, md.partnumber
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18);

UPDATE fact_materialmovement f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM TMP_DIM_MDG_PARTID_MM tmp, fact_materialmovement f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);
	  
	  
	  
/* ct_ProductionOutput and  ct_Ullage */


DROP TABLE IF EXISTS tmp_productionoutputullage;
CREATE TABLE tmp_productionoutputullage AS SELECT   dd_productionordernumber, dp.partnumber, min(fact_materialmovementid) minid,
         Sum( 
         CASE 
                  WHEN mv1.movementtype = '101' THEN ct_Quantity 
                  ELSE 0 
         END ) m1 , 
         Sum( 
         CASE 
                  WHEN mv1.movementtype = '102' THEN ct_Quantity 
                  ELSE 0 
         END ) m2 , 
         Sum( 
         CASE 
                  WHEN mv1.movementtype = '531' THEN ct_Quantity 
                  ELSE 0 
         END ) m31 , 
         Sum( 
         CASE 
                  WHEN mv1.movementtype = '532' THEN ct_Quantity 
                  ELSE 0 
         END ) m32 
FROM     fact_materialmovement f , 
         dim_movementtype mv1 ,
         dim_part dp
WHERE    f.dim_movementtypeid = mv1.dim_movementtypeid 
AND f.dim_partid = dp.dim_partid
AND dd_productionordernumber <> 'Not Set'
GROUP BY dd_productionordernumber, dp.partnumber;




UPDATE  fact_materialmovement f
SET f.ct_ProductionOutput = m1+m2,
   f.ct_Ullage = m31 + m32
FROM  fact_materialmovement f,tmp_productionoutputullage tmp, dim_part dp
WHERE f.dd_productionordernumber = tmp.dd_productionordernumber
     AND f.dim_partid = dp.dim_partid
     AND dp.Partnumber = tmp.partnumber;
     
UPDATE  fact_materialmovement f
SET f.ct_ProductionOutputmin = m1+m2,
   f.ct_Ullagemin = m31 + m32
FROM  fact_materialmovement f,tmp_productionoutputullage tmp, dim_part dp
WHERE f.dd_productionordernumber = tmp.dd_productionordernumber
     AND f.dim_partid = dp.dim_partid
     AND dp.Partnumber = tmp.partnumber
     AND f.fact_materialmovementid =  tmp.minid;
     
UPDATE  fact_materialmovement f
SET  f.dd_PartNoUllage = partnumber 
FROM   fact_materialmovement f,tmp_productionoutputullage tmp
WHERE f.dd_productionordernumber = tmp.dd_productionordernumber
     AND (m31 + m32 > 0);
	
	

UPDATE  fact_materialmovement f
SET dd_PartNoProductionOutput = partnumber
FROM  fact_materialmovement f,tmp_productionoutputullage tmp
WHERE f.dd_productionordernumber = tmp.dd_productionordernumber
     AND (m1 + m2 > 0) ; 


/* BI 4695 - Roxana - add new fields */

UPDATE  fact_materialmovement f
SET dd_ProductionVersion = ifnull(MKAL_TEXT1, 'Not Set')
FROM fact_materialmovement f, MKAL m, dim_part dpr, AFKO_AFPO_AUFK afk
WHERE f.dim_partid = dpr.dim_partid
	AND dpr.PartNumber = m.MKAL_MATNR
	AND m.MKAL_WERKS = dpr.plant
	AND afk.AFPO_VERID = m.MKAL_VERID
	AND trim (leading '0' from f.dd_productionordernumber) = trim (leading '0' from afk.AFKO_AUFNR)
	AND dd_ProductionVersion <> ifnull(MKAL_TEXT1, 'Not Set');


UPDATE  fact_materialmovement f
SET dd_UserStatus = ifnull(TJ30T_TXT04, 'Not Set')
FROM fact_materialmovement f, TJ30T t, JEST_INSP a , AFKO_AFPO_AUFK afk, JSTO b
WHERE t.TJ30T_ESTAT = a.JEST_STAT
	AND t.TJ30T_STSMA = 'ZQM_N_02'
	AND a.JEST_OBJNR = afk.AUFK_OBJNR
	AND a.JEST_OBJNR = b.JSTO_OBJNR  
	AND trim (leading '0' from f.dd_productionordernumber) = trim (leading '0' from afk.AFKO_AUFNR)
	AND dd_UserStatus <> ifnull(TJ30T_TXT04, 'Not Set');

	
UPDATE  fact_materialmovement f
SET dd_UserStatusDescription = ifnull(TJ30T_TXT30, 'Not Set')
FROM fact_materialmovement f, TJ30T t, JEST_INSP a , AFKO_AFPO_AUFK afk, JSTO b
WHERE t.TJ30T_ESTAT = a.JEST_STAT
	AND t.TJ30T_STSMA = 'ZQM_N_02'
	AND a.JEST_OBJNR = afk.AUFK_OBJNR
	AND a.JEST_OBJNR = b.JSTO_OBJNR  
	AND trim (leading '0' from f.dd_productionordernumber) = trim (leading '0' from afk.AFKO_AUFNR)
	AND dd_UserStatusDescription <> ifnull(TJ30T_TXT30, 'Not Set');
	

/*
UPDATE  fact_materialmovement f
SET dd_ProcessOrderStatus = ifnull(TJ30T_TXT04, 'Not Set')
FROM fact_materialmovement f, TJ30T t, JEST_INSP a , AFKO_AFPO_AUFK afk, JSTO b
WHERE t.TJ30T_ESTAT = a.JEST_STAT
	AND t.TJ30T_STSMA = b.JSTO_STSMA 
	AND a.JEST_OBJNR = afk.AUFK_OBJNR
	AND a.JEST_OBJNR = b.JSTO_OBJNR
	AND trim (leading '0' from f.dd_productionordernumber) = trim (leading '0' from afk.AFKO_AUFNR) 
	AND dd_ProcessOrderStatus <> ifnull(TJ30T_TXT04, 'Not Set')

UPDATE  fact_materialmovement f
SET dd_ProcessOrderStatusDescription = ifnull(TJ30T_TXT30, 'Not Set')
FROM fact_materialmovement f, TJ30T t, JEST_INSP a , AFKO_AFPO_AUFK afk, JSTO b
WHERE t.TJ30T_ESTAT = a.JEST_STAT
	AND t.TJ30T_STSMA = b.JSTO_STSMA 
	AND a.JEST_OBJNR = afk.AUFK_OBJNR
	AND a.JEST_OBJNR = b.JSTO_OBJNR
	AND trim (leading '0' from f.dd_productionordernumber) = trim (leading '0' from afk.AFKO_AUFNR) 
	AND dd_ProcessOrderStatusDescription <> ifnull(TJ30T_TXT30, 'Not Set')
*/
	
MERGE INTO fact_materialmovement f
 USING (SELECT trim (leading '0' from f.dd_productionordernumber) as dd_productionordernumber, MAX (ifnull(TJ30T_TXT04, 'Not Set')) TJ30T_TXT04
		FROM fact_materialmovement f, TJ30T t, JEST_INSP a , AFKO_AFPO_AUFK afk, JSTO b
		WHERE t.TJ30T_ESTAT = a.JEST_STAT
		AND t.TJ30T_STSMA = b.JSTO_STSMA 
		AND a.JEST_OBJNR = afk.AUFK_OBJNR
		AND a.JEST_OBJNR = b.JSTO_OBJNR
		AND trim (leading '0' from f.dd_productionordernumber) = trim (leading '0' from afk.AFKO_AUFNR)
		GROUP BY f.dd_productionordernumber) x
ON (f.dd_productionordernumber = x.dd_productionordernumber)
WHEN MATCHED THEN
UPDATE SET f.dd_ProcessOrderStatus = x.TJ30T_TXT04
WHERE f.dd_ProcessOrderStatus <> x.TJ30T_TXT04;
 
MERGE INTO fact_materialmovement f
 USING (SELECT trim (leading '0' from f.dd_productionordernumber) as dd_productionordernumber, MAX (ifnull(TJ30T_TXT30, 'Not Set')) TJ30T_TXT30
		FROM fact_materialmovement f, TJ30T t, JEST_INSP a , AFKO_AFPO_AUFK afk, JSTO b
		WHERE t.TJ30T_ESTAT = a.JEST_STAT
		AND t.TJ30T_STSMA = b.JSTO_STSMA 
		AND a.JEST_OBJNR = afk.AUFK_OBJNR
		AND a.JEST_OBJNR = b.JSTO_OBJNR
		AND trim (leading '0' from f.dd_productionordernumber) = trim (leading '0' from afk.AFKO_AUFNR)
		GROUP BY f.dd_productionordernumber) x
ON (f.dd_productionordernumber = x.dd_productionordernumber)
WHEN MATCHED THEN
UPDATE SET f.dd_ProcessOrderStatusDescription = x.TJ30T_TXT30
WHERE f.dd_ProcessOrderStatusDescription <> x.TJ30T_TXT30;
	
UPDATE  fact_materialmovement f
SET dd_RecipeGroup = ifnull(AFKO_PLNNR, 'Not Set')
FROM fact_materialmovement f, AFKO_AFPO_AUFK afk
WHERE trim (leading '0' from f.dd_productionordernumber) = trim (leading '0' from afk.AFKO_AUFNR)
AND dd_RecipeGroup <> ifnull(AFKO_PLNNR, 'Not Set');

/* end of BI 4695 changes */ 

/* Harmonized KG  */
UPDATE fact_materialmovement f
SET ct_baseuomratioKG = 1/1000
FROM fact_materialmovement f,MARM uc, dim_part dp
WHERE f.dim_partid = dp.dim_partid 
and uc.marm_matnr = dp.partnumber 
and uc.marm_meinh = 'G';

UPDATE fact_materialmovement f
SET ct_baseuomratioKG = ifnull(MARM_UMREN/MARM_UMREZ,1)
FROM fact_materialmovement f,MARM uc, dim_part dp
WHERE f.dim_partid = dp.dim_partid 
and uc.marm_matnr = dp.partnumber 
and uc.marm_meinh = 'KG';	
	

UPDATE fact_materialmovement f
SET f.amt_ExchangeRate_GBL = ifnull(r.exrate,1)
FROM fact_materialmovement f, dim_currency dc, csv_oprate r
WHERE f.Dim_Currencyid_TRA = dc.dim_currencyid
	AND dc.CurrencyCode = r.fromc
	AND r.toc = (SELECT ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD'))
	AND ifnull(f.amt_ExchangeRate_GBL,-1) <> ifnull(r.exrate,1);


DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_MM;

drop table if exists dim_profitcenter_789;
drop table if exists bwtarupd_tmp_100;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_allcols;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_del;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_allcols;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_del;
drop table if exists tmp_fact_materialmovement_perf1;
drop table if exists tmp_fact_materialmovement_del;
DROP TABLE IF EXISTS tmp_denorm_fact_mm_mseg1;
DROP TABLE IF EXISTS tmp_upd_po_to_mm;
DROP TABLE IF EXISTS tmp_upd_so_to_mm;



/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate_matmov;
create table tmp_updatedate_matmov as
select max(dw_update_date) dd_updatedate from  fact_materialmovement;


update fact_materialmovement f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_materialmovement f, tmp_updatedate_matmov t
where f.dd_updatedate <> t.dd_updatedate;