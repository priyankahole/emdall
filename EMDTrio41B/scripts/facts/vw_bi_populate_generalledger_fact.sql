

/* Update 1 */

   UPDATE fact_generalledger fgl 
   SET amt_InLocalCurrency = (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_DMBTR
   from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
   WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND amt_InLocalCurrency <> (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_DMBTR;



   UPDATE fact_generalledger fgl 
   SET amt_TaxInDocCurrency = (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_WMWST
   from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
   WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND  amt_TaxInDocCurrency <> (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_WMWST;


   UPDATE fact_generalledger fgl 
   SET amt_TaxInLocalCurrency = (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_MWSTS
   from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
   WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND amt_TaxInLocalCurrency <> (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIS_MWSTS;


   UPDATE fact_generalledger fgl 
   SET dd_AccountingDocItemNo = BSIS_BUZEI
   from  BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AccountingDocItemNo  <> BSIS_BUZEI;


    UPDATE fact_generalledger fgl 
    SET dd_AccountingDocNo = BSIS_BELNR
       from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
    WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AccountingDocNo <>  BSIS_BELNR;



   UPDATE fact_generalledger fgl 
   SET dd_AssignmentNumber = ifnull(BSIS_ZUONR, 'Not Set')
    from  BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
   WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	   AND dd_AssignmentNumber <>  ifnull(BSIS_ZUONR, 'Not Set');

	UPDATE fact_generalledger fgl 
	SET Dim_ClearedFlagId = ifnull(gls.Dim_GeneralLedgerStatusId,1)
     from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       Dim_GeneralLedgerStatus gls,
        fact_generalledger fgl
    WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS  AND dcm.dim_companyid = fgl.dim_companyid 
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
       AND  gls.Status = 'O - Open' AND gls.RowIsCurrent = 1
	   AND Dim_ClearedFlagId <> ifnull(gls.Dim_GeneralLedgerStatusId,1);

   
   UPDATE fact_generalledger fgl                            
   SET       Dim_DateIdBaseDateForDueDateCalc =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSIS_ZFBDT and dt.CompanyCode = glc.BSIS_BUKRS
   where     Dim_DateIdBaseDateForDueDateCalc <>  ifnull(dt.dim_dateid,1);

	UPDATE fact_generalledger fgl                            
   SET       Dim_DateIdDocument =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSIS_BLDAT and dt.CompanyCode = glc.BSIS_BUKRS
   where     Dim_DateIdDocument <>  ifnull(dt.dim_dateid,1);

    UPDATE fact_generalledger fgl                            
   SET       Dim_DateIdPosting =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSIS_BUDAT and dt.CompanyCode = glc.BSIS_BUKRS
   where     Dim_DateIdPosting <>  ifnull(dt.dim_dateid,1);


   UPDATE fact_generalledger fgl                            
   SET      Dim_DateIdReferenceDate =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSIS_DABRZ and dt.CompanyCode = glc.BSIS_BUKRS
   where    Dim_DateIdReferenceDate <>  ifnull(dt.dim_dateid,1);
 
    UPDATE fact_generalledger fgl 
    SET dd_DebitCreditInd = (CASE WHEN BSIS_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)
      from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
    WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	   AND dd_DebitCreditInd <> (CASE WHEN BSIS_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END);

      UPDATE fact_generalledger fgl 
   SET  dd_ClearingDocumentNo = ifnull(BSIS_AUGBL, 'Not Set')
       from BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_ClearingDocumentNo  <> ifnull(BSIS_AUGBL, 'Not Set');

     UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_BusinessAreaId =  ifnull(ba.Dim_BusinessAreaId,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_BusinessArea where RowIsCurrent = 1) ba on ba.BusinessArea = BSIS_GSBER
   where    fgl.Dim_BusinessAreaId <>  ifnull(ba.Dim_BusinessAreaId,1);

   UPDATE fact_generalledger fgl 
   SET fgl.dim_companyid = ifnull(dcm.Dim_CompanyId, 1)
       from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
   WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid 
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	   AND  fgl.dim_companyid  <>  ifnull(dcm.Dim_CompanyId, 1);

	UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_Currencyid =  ifnull(c.Dim_Currencyid,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_currency c on c.CurrencyCode = glc.BSIS_WAERS
   where    fgl.Dim_Currencyid <>  ifnull(c.Dim_Currencyid,1);

   
UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_GLCurrencyid =  ifnull(c.dim_currencyid,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_currency c on c.CurrencyCode = glc.BSIS_PSWSL
   where    fgl.Dim_GLCurrencyid <>  ifnull(c.dim_currencyid,1);

 UPDATE fact_generalledger fgl                            
   SET      Dim_DateIdClearing =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSIS_AUGDT and dt.CompanyCode = glc.BSIS_BUKRS
   where    Dim_DateIdClearing <>  ifnull(dt.dim_dateid,1);


UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_DocumentTypeId =  ifnull(dtt.dim_documenttypetextid,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from dim_documenttypetext where RowIsCurrent = 1) dtt on dtt.type = BSIS_BLART
   where    fgl.Dim_DocumentTypeId <>  ifnull(dtt.dim_documenttypetextid,1);


UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_PostingKeyId =  ifnull(pk.Dim_PostingKeyId,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_PostingKey where RowIsCurrent = 1 AND SpecialGLIndicator = 'Not Set') pk on pk.PostingKey = BSIS_BSCHL
   where    fgl.Dim_PostingKeyId <>  ifnull(pk.Dim_PostingKeyId,1);




UPDATE fact_generalledger fgl 
   SET fgl.Dim_ChartOfAccountsId = ifnull(coa.Dim_ChartOfAccountsId, 1)
       from
       BSIS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
	   fact_generalledger fgl 
 WHERE     fgl.dd_AccountingDocNo = glc.BSIS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSIS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSIS_MONAT
       AND dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid 
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSIS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
       AND  fgl.Dim_ChartOfAccountsId <>  ifnull(coa.Dim_ChartOfAccountsId, 1);

UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_FunctionalAreaId =  ifnull(fa.Dim_FunctionalAreaId,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_FunctionalArea where RowIsCurrent = 1) fa on fa.FunctionalArea = BSIS_FKBER
   where    fgl.Dim_FunctionalAreaId <>  ifnull(fa.Dim_FunctionalAreaId,1);

UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_PlantId =  ifnull(p.Dim_PlantId,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_Plant where RowIsCurrent = 1) p on p.PlantCode = BSIS_WERKS
   where    fgl.Dim_PlantId <>  ifnull(p.Dim_PlantId,1);

UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_ProfitCenterId =  ifnull(pc.Dim_ProfitCenterId,1)
       from fact_generalledger fgl inner join  BSIS glc on fgl.dd_AccountingDocNo = glc.BSIS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSIS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSIS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSIS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSIS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSIS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_ProfitCenter where RowIsCurrent = 1 and ControllingArea = 'Not Set') pc on pc.ProfitCenterCode = BSIS_PRCTR
   where    fgl.Dim_ProfitCenterId <>  ifnull(pc.Dim_ProfitCenterId,1);


/* End of Update 1 */

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_generalledger';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_generalledger', ifnull(max(fact_generalledgerid), 0)
     FROM fact_generalledger;


drop table if exists tmp_fgenled_t001t;

create table tmp_fgenled_t001t as
 SELECT (SELECT max_id
             FROM NUMBER_FOUNTAIN
            WHERE table_name = 'fact_generalledger')
          + row_number()
             over (order by '') as fact_generalledgerid,
           (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSIS_WRBTR amt_InDocCurrency,
          (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSIS_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSIS_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSIS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSIS_MWSTS amt_TaxInLocalCurrency,
          BSIS_BUZEI dd_AccountingDocItemNo,
          BSIS_BELNR dd_AccountingDocNo,
          ifnull(BSIS_ZUONR,'Not Set') dd_AssignmentNumber,
		convert(bigint, 1) as Dim_ClearedFlagId,
		convert(bigint, 1) as Dim_DateIdBaseDateForDueDateCalc,
		convert(bigint, 1) as Dim_DateIdDocument,
		convert(bigint, 1) as Dim_DateIdPosting,
		convert(bigint, 1) as Dim_DateIdReferenceDate,
		(CASE WHEN BSIS_SHKZG ='H' THEN 'Credit' ELSE 'Debit' END) dd_DebitCreditInd,
          ifnull(BSIS_AUGBL,'Not Set') dd_ClearingDocumentNo,
          BSIS_MONAT dd_FiscalPeriod,
          BSIS_GJAHR dd_FiscalYear,
		convert(bigint, 1) as Dim_BusinessAreaId,
		ifnull(dc.Dim_CompanyId, 1) as Dim_CompanyId,
		convert(bigint, 1) as Dim_Currencyid,
		convert(bigint, 1) as Dim_GLCurrencyid,
		convert(bigint, 1) as Dim_DateIdClearing,
		convert(bigint, 1) as Dim_DocumentTypeId,
		convert(bigint, 1) as Dim_PostingKeyId,
        ifnull(coa.dim_chartofaccountsid, 1) as dim_chartofaccountsid,
		convert(bigint, 1) as Dim_FunctionalAreaId,
		convert(bigint, 1) as Dim_PlantId,
		convert(bigint, 1) as Dim_ProfitCenterId,       
        BSIS_BUKRS, --CompanyCode
        BSIS_ZFBDT, --Dim_DateIdBaseDateForDueDateCalc
        BSIS_BLDAT, --Dim_DateIdDocument
        BSIS_BUDAT,  --Dim_DateIdPosting
        BSIS_DABRZ,  --Dim_DateIdReferenceDate
        BSIS_GSBER,   --Dim_BusinessAreaId
        BSIS_WAERS,   --Dim_Currencyid
        BSIS_PSWSL,   --Dim_GLCurrencyid
        BSIS_AUGDT,   --Dim_DateIdClearing
        BSIS_BLART,   --Dim_DocumentTypeId
        BSIS_BSCHL,    --Dim_PostingKeyId
        BSIS_FKBER,    --Dim_FunctionalAreaId
        BSIS_WERKS,    --Dim_PlantId
        BSIS_PRCTR    --Dim_ProfitCenterId
        FROM BSIS glc
      INNER JOIN dim_company dc ON dc.CompanyCode = glc.BSIS_BUKRS AND dc.RowIsCurrent = 1
      INNER JOIN dim_chartofaccounts coa ON coa.GLAccountNumber = glc.BSIS_HKONT AND coa.CharOfAccounts = dc.ChartOfAccounts AND coa.RowIsCurrent = 1
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_generalledger ar
                   WHERE     ar.dd_AccountingDocNo = glc.BSIS_BELNR
                         AND ar.dd_AccountingDocItemNo = glc.BSIS_BUZEI
                         AND ar.dd_AssignmentNumber = ifnull(glc.BSIS_ZUONR,'Not Set')
                         AND ar.dd_fiscalyear = glc.BSIS_GJAHR
                         AND ar.dd_FiscalPeriod  = glc.BSIS_MONAT
                         AND ar.dim_companyid = dc.Dim_CompanyId
                         AND ar.Dim_ChartOfAccountsId = coa.Dim_ChartOfAccountsId);

		/*Dim_ClearedFlagId */
		update tmp_fgenled_t001t f
		set f.Dim_ClearedFlagId = ifnull(gls.Dim_GeneralLedgerStatusId,1)
		from tmp_fgenled_t001t f
                   cross join Dim_GeneralLedgerStatus gls 
	    WHERE gls.Status = 'O - Open'
             AND gls.RowIsCurrent = 1
			 AND f.Dim_ClearedFlagId <> ifnull(gls.Dim_GeneralLedgerStatusId,1);

       /*Dim_DateIdBaseDateForDueDateCalc*/
       update tmp_fgenled_t001t f
		set f.Dim_DateIdBaseDateForDueDateCalc = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t001t f
                   left  join dim_date dt on dt.datevalue = BSIS_ZFBDT AND dt.CompanyCode = BSIS_BUKRS
       where f.Dim_DateIdBaseDateForDueDateCalc <> ifnull(dt.dim_dateid,1);
   

       /*Dim_DateIdDocument*/
       update tmp_fgenled_t001t f
		set f.Dim_DateIdDocument = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t001t f
                   left  join dim_date dt on dt.datevalue = BSIS_BLDAT AND dt.CompanyCode = BSIS_BUKRS
       where f.Dim_DateIdDocument <> ifnull(dt.dim_dateid,1);

       /*Dim_DateIdPosting*/
       update tmp_fgenled_t001t f
		set f.Dim_DateIdPosting = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t001t f
                   left  join dim_date dt on dt.datevalue = BSIS_BUDAT AND dt.CompanyCode = BSIS_BUKRS
       where f.Dim_DateIdPosting <> ifnull(dt.dim_dateid,1);
 
       /*Dim_DateIdReferenceDate*/
        update tmp_fgenled_t001t f
		set f.Dim_DateIdReferenceDate = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t001t f
                   left  join dim_date dt on dt.datevalue = BSIS_DABRZ AND dt.CompanyCode = BSIS_BUKRS
       where f.Dim_DateIdReferenceDate <> ifnull(dt.dim_dateid,1);

      /*Dim_BusinessAreaId*/
       update tmp_fgenled_t001t f
		set f.Dim_BusinessAreaId = ifnull(ba.Dim_BusinessAreaId,1)
		from tmp_fgenled_t001t f
                   left  join (select * from Dim_BusinessArea
                                         where RowIsCurrent = 1) ba on ba.BusinessArea = f.BSIS_GSBER
       where f.Dim_BusinessAreaId <> ifnull(ba.Dim_BusinessAreaId,1);

       /*Dim_Currencyid*/
       update tmp_fgenled_t001t f
		set f.dim_currencyId = ifnull(c.dim_currencyId,1)
		from tmp_fgenled_t001t f
                   left  join dim_currency c on c.CurrencyCode = f.BSIS_WAERS
       where f.dim_currencyId <> ifnull(c.dim_currencyId,1);

       /*Dim_GLCurrencyid*/
       update tmp_fgenled_t001t f
		set f.Dim_GLCurrencyid = ifnull(c.dim_currencyId,1)
		from tmp_fgenled_t001t f
                   left  join dim_currency c on c.CurrencyCode = f.BSIS_PSWSL
       where f.Dim_GLCurrencyid <> ifnull(c.dim_currencyId,1);
     
        /*Dim_DateIdClearing*/
        update tmp_fgenled_t001t f
		set f.Dim_DateIdClearing = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t001t f
                   left  join dim_date dt on dt.datevalue = BSIS_AUGDT AND dt.CompanyCode = f.BSIS_BUKRS
       where f.Dim_DateIdClearing <> ifnull(dt.dim_dateid,1);
      
       /*Dim_DocumentTypeId*/
       update tmp_fgenled_t001t f
		set f.Dim_DocumentTypeId = ifnull(dtt.dim_documenttypetextid,1)
		from tmp_fgenled_t001t f
                   left  join (select * from dim_documenttypetext 
                               where RowIsCurrent = 1) dtt on  dtt.type = f.BSIS_BLART
       where f.Dim_DocumentTypeId <>  ifnull(dtt.dim_documenttypetextid,1);

       /*Dim_PostingKeyId*/
       update tmp_fgenled_t001t f
		set f.Dim_PostingKeyId = ifnull(pk.Dim_PostingKeyId,1)
		from tmp_fgenled_t001t f
                   left  join (select * from Dim_PostingKey 
                               where RowIsCurrent = 1 and SpecialGLIndicator = 'Not Set') pk on  pk.PostingKey = f.BSIS_BSCHL
       where f.Dim_PostingKeyId <> ifnull(pk.Dim_PostingKeyId,1);

       /*Dim_FunctionalAreaId*/
       update tmp_fgenled_t001t f
		set f.Dim_FunctionalAreaId = ifnull(fa.Dim_FunctionalAreaId,1)
		from tmp_fgenled_t001t f
                   left  join (select * from Dim_FunctionalArea
                               where RowIsCurrent = 1 ) fa on fa.FunctionalArea = f.BSIS_FKBER
       where f.Dim_FunctionalAreaId <> ifnull(fa.Dim_FunctionalAreaId,1);

      /*Dim_PlantId*/
       update tmp_fgenled_t001t f
		set f.Dim_PlantId = ifnull(p.Dim_PlantId,1)
		from tmp_fgenled_t001t f
                   left  join (select * from Dim_Plant
                               where RowIsCurrent = 1 ) p on p.PlantCode = f.BSIS_WERKS
       where f.Dim_PlantId <> ifnull(p.Dim_PlantId,1);

       /*Dim_ProfitCenterId*/
       update tmp_fgenled_t001t f
		set f.Dim_ProfitCenterId = ifnull(pc.Dim_ProfitCenterId,1)
		from tmp_fgenled_t001t f
                   left  join (select * from Dim_ProfitCenter
                               where RowIsCurrent = 1 and ControllingArea = 'Not Set') pc on pc.ProfitCenterCode = f.BSIS_PRCTR
       where f.Dim_ProfitCenterId <> ifnull(pc.Dim_ProfitCenterId,1);

       INSERT INTO fact_generalledger
                          (fact_generalledgerid,
                               amt_InDocCurrency,
                               amt_InLocalCurrency,
                               amt_TaxInDocCurrency,
                               amt_TaxInLocalCurrency,
                               dd_AccountingDocItemNo,
                               dd_AccountingDocNo,
                               dd_AssignmentNumber,
                               Dim_ClearedFlagId,
                               Dim_DateIdBaseDateForDueDateCalc,
                               Dim_DateIdDocument,
                               Dim_DateIdPosting,
                               Dim_DateIdReferenceDate,
                               dd_DebitCreditInd,
                               dd_ClearingDocumentNo,
                               dd_FiscalPeriod,
                               dd_FiscalYear,
                               Dim_BusinessAreaId,
                               Dim_CompanyId,
                               Dim_CurrencyId,
                               Dim_GLCurrencyId,
                               Dim_DateIdClearing,
                               Dim_DocumentTypeId,
                               Dim_PostingKeyId,
                               Dim_ChartOfAccountsId,
                               Dim_FunctionalAreaId,
                               Dim_PlantId,
                               Dim_ProfitCenterId)
         select fact_generalledgerid,
                               amt_InDocCurrency,
                               amt_InLocalCurrency,
                               amt_TaxInDocCurrency,
                               amt_TaxInLocalCurrency,
                               dd_AccountingDocItemNo,
                               dd_AccountingDocNo,
                               dd_AssignmentNumber,
                               Dim_ClearedFlagId,
                               Dim_DateIdBaseDateForDueDateCalc,
                               Dim_DateIdDocument,
                               Dim_DateIdPosting,
                               Dim_DateIdReferenceDate,
                               dd_DebitCreditInd,
                               dd_ClearingDocumentNo,
                               dd_FiscalPeriod,
                               dd_FiscalYear,
                               Dim_BusinessAreaId,
                               Dim_CompanyId,
                               Dim_CurrencyId,
                               Dim_GLCurrencyId,
                               Dim_DateIdClearing,
                               Dim_DocumentTypeId,
                               Dim_PostingKeyId,
                               Dim_ChartOfAccountsId,
                               Dim_FunctionalAreaId,
                               Dim_PlantId,
                               Dim_ProfitCenterId
                   from tmp_fgenled_t001t;

    drop table if exists tmp_fgenled_t001t;


/* Update 2 */

UPDATE fact_generalledger fgl
   SET amt_InLocalCurrency = (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_DMBTR
   from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND  amt_InLocalCurrency <> (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_DMBTR;


UPDATE fact_generalledger fgl
   SET        amt_TaxInDocCurrency =
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_WMWST
     from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND  amt_TaxInDocCurrency  <>  (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_WMWST;


       UPDATE fact_generalledger fgl
   SET amt_TaxInLocalCurrency =
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_MWSTS
      from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND amt_TaxInLocalCurrency <> (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAS_MWSTS;


          UPDATE fact_generalledger fgl
   SET dd_AccountingDocItemNo = BSAS_BUZEI
       from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AccountingDocItemNo <>  BSAS_BUZEI;


UPDATE fact_generalledger fgl
   SET dd_AccountingDocNo = BSAS_BELNR
      from BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AccountingDocNo <> BSAS_BELNR;


UPDATE fact_generalledger fgl
   SET dd_AssignmentNumber = ifnull(BSAS_ZUONR, 'Not Set')
   from BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_AssignmentNumber <> ifnull(BSAS_ZUONR, 'Not Set');

UPDATE fact_generalledger fgl
   SET
       Dim_ClearedFlagId =
          ifnull(
             (SELECT Dim_GeneralLedgerStatusId
                FROM Dim_GeneralLedgerStatus gls
               WHERE gls.Status = 'C - Cleared' AND gls.RowIsCurrent = 1),
             1)
            from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1;


    UPDATE fact_generalledger fgl                            
   SET       Dim_DateIdBaseDateForDueDateCalc =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSAS_ZFBDT and dt.CompanyCode = glc.BSAS_BUKRS
   where     Dim_DateIdBaseDateForDueDateCalc <>  ifnull(dt.dim_dateid,1);


      
UPDATE fact_generalledger fgl                            
   SET       Dim_DateIdDocument =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSAS_BLDAT and dt.CompanyCode = glc.BSAS_BUKRS
   where     Dim_DateIdDocument <>  ifnull(dt.dim_dateid,1);


      
UPDATE fact_generalledger fgl                            
   SET       Dim_DateIdPosting =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSAS_BUDAT and dt.CompanyCode = glc.BSAS_BUKRS
   where     Dim_DateIdPosting <>  ifnull(dt.dim_dateid,1);

UPDATE fact_generalledger fgl                            
   SET      Dim_DateIdReferenceDate =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSAS_DABRZ and dt.CompanyCode = glc.BSAS_BUKRS
   where    Dim_DateIdReferenceDate <>  ifnull(dt.dim_dateid,1);



UPDATE fact_generalledger fgl    
   SET dd_DebitCreditInd =
         (CASE glc.BSAS_SHKZG WHEN 'H' THEN '1' WHEN 'S' THEN '-1' END)
                            from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND dd_DebitCreditInd <> (CASE glc.BSAS_SHKZG WHEN 'H' THEN '1' WHEN 'S' THEN '-1' END);


UPDATE fact_generalledger fgl                          
   SET dd_ClearingDocumentNo = ifnull(BSAS_AUGBL, 'Not Set')
     from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl  
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
       AND dd_ClearingDocumentNo <> ifnull(BSAS_AUGBL, 'Not Set');


UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_BusinessAreaId =  ifnull(ba.Dim_BusinessAreaId,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_BusinessArea where RowIsCurrent = 1) ba on ba.BusinessArea = BSAS_GSBER
   where    fgl.Dim_BusinessAreaId <>  ifnull(ba.Dim_BusinessAreaId,1);


UPDATE fact_generalledger fgl                        
   SET
       fgl.dim_companyid = ifnull(dcm.Dim_CompanyId, 1)
          from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
       AND fgl.dim_companyid <> ifnull(dcm.Dim_CompanyId, 1);

UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_Currencyid =  ifnull(c.Dim_Currencyid,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_currency c on c.CurrencyCode = glc.BSAS_WAERS
   where    fgl.Dim_Currencyid <>  ifnull(c.Dim_Currencyid,1);




UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_GLCurrencyid =  ifnull(c.dim_currencyid,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_currency c on c.CurrencyCode = glc.BSAS_PSWSL
   where    fgl.Dim_GLCurrencyid <>  ifnull(c.dim_currencyid,1);



     UPDATE fact_generalledger fgl                            
   SET      Dim_DateIdClearing =  ifnull(dt.dim_dateid,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join dim_date dt on dt.DateValue = BSAS_AUGDT and dt.CompanyCode = glc.BSAS_BUKRS
   where    Dim_DateIdClearing <>  ifnull(dt.dim_dateid,1);



    UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_DocumentTypeId =  ifnull(dtt.dim_documenttypetextid,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from dim_documenttypetext where RowIsCurrent = 1) dtt on dtt.type = BSAS_BLART
   where    fgl.Dim_DocumentTypeId <>  ifnull(dtt.dim_documenttypetextid,1);


	UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_PostingKeyId =  ifnull(pk.Dim_PostingKeyId,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_PostingKey where RowIsCurrent = 1 AND SpecialGLIndicator = 'Not Set') pk on pk.PostingKey = BSAS_BSCHL
   where    fgl.Dim_PostingKeyId <>  ifnull(pk.Dim_PostingKeyId,1);




UPDATE fact_generalledger fgl                         
   SET
       fgl.Dim_ChartOfAccountsId = ifnull(coa.Dim_ChartOfAccountsId, 1)
    from
       BSAS glc,
       dim_company dcm,
       dim_chartofaccounts coa,
       fact_generalledger fgl 
 WHERE     fgl.dd_AccountingDocNo = glc.BSAS_BELNR
       AND fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI
       AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
       AND fgl.dd_fiscalyear = glc.BSAS_GJAHR
       AND fgl.dd_FiscalPeriod = glc.BSAS_MONAT
       AND dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND coa.GLAccountNumber = glc.BSAS_HKONT
       AND coa.CharOfAccounts = dcm.ChartOfAccounts
       AND coa.RowIsCurrent = 1
	AND fgl.Dim_ChartOfAccountsId  <> ifnull(coa.Dim_ChartOfAccountsId, 1);




UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_FunctionalAreaId =  ifnull(fa.Dim_FunctionalAreaId,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_FunctionalArea where RowIsCurrent = 1) fa on fa.FunctionalArea = BSAS_FKBER
   where    fgl.Dim_FunctionalAreaId <>  ifnull(fa.Dim_FunctionalAreaId,1);


UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_PlantId =  ifnull(p.Dim_PlantId,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_Plant where RowIsCurrent = 1) p on p.PlantCode = BSAS_WERKS
   where    fgl.Dim_PlantId <>  ifnull(p.Dim_PlantId,1);



UPDATE fact_generalledger fgl                            
   SET      fgl.Dim_ProfitCenterId =  ifnull(pc.Dim_ProfitCenterId,1)
       from fact_generalledger fgl inner join  BSAS glc on fgl.dd_AccountingDocNo = glc.BSAS_BELNR AND  fgl.dd_AccountingDocItemNo = glc.BSAS_BUZEI 
                                        AND fgl.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set') AND  fgl.dd_fiscalyear = glc.BSAS_GJAHR AND  fgl.dd_FiscalPeriod = glc.BSAS_MONAT 
                                   inner join (select * from dim_company where RowIsCurrent = 1) dcm on dcm.CompanyCode = glc.BSAS_BUKRS AND dcm.dim_companyid = fgl.dim_companyid
                                   inner join (select * from dim_chartofaccounts where RowIsCurrent = 1) coa on coa.GLAccountNumber = glc.BSAS_HKONT and coa.CharOfAccounts = dcm.ChartOfAccounts
                                   left join (select * from Dim_ProfitCenter where RowIsCurrent = 1 and ControllingArea = 'Not Set') pc on pc.ProfitCenterCode = BSAS_PRCTR
   where    fgl.Dim_ProfitCenterId <>  ifnull(pc.Dim_ProfitCenterId,1);



/* End of Update 2 */

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_generalledger';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_generalledger', ifnull(max(fact_generalledgerid), 0)
     FROM fact_generalledger;

drop table if exists tmp_fgenled_t002t;

create table tmp_fgenled_t002t as
   SELECT (SELECT max_id
             FROM NUMBER_FOUNTAIN
            WHERE table_name = 'fact_generalledger')
          + row_number()
             over
 (order by '') as fact_generalledgerid,
       (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAS_WRBTR amt_InDocCurrency,
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAS_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAS_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSAS_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAS_MWSTS amt_TaxInLocalCurrency,
          BSAS_BUZEI dd_AccountingDocItemNo,
          BSAS_BELNR dd_AccountingDocNo,
          ifnull(BSAS_ZUONR, 'Not Set') dd_AssignmentNumber,
          convert(bigint, 1) Dim_ClearedFlagId,
          convert(bigint, 1) Dim_DateIdBaseDateForDueDateCalc,
          convert(bigint, 1) Dim_DateIdDocument,
          convert(bigint, 1) Dim_DateIdPosting,
          BSAS_SHKZG  dd_DebitCreditInd,
          ifnull(BSAS_AUGBL,'Not Set') dd_ClearingDocumentNo,
          BSAS_MONAT dd_FiscalPeriod,
          BSAS_GJAHR dd_FiscalYear,
          convert(bigint, 1) Dim_BusinessAreaId,
          ifnull(dc.Dim_CompanyId,1) Dim_CompanyId,
          convert(bigint, 1) Dim_Currencyid,
          convert(bigint, 1) Dim_GLCurrencyid,
          convert(bigint, 1) Dim_DateIdClearing,
          convert(bigint, 1) Dim_DateIdReferenceDate,
          convert(bigint, 1) Dim_DocumentTypeId,
          convert(bigint, 1) Dim_PostingKeyId,
          ifnull(coa.Dim_ChartOfAccountsId, 1) Dim_ChartOfAccountsId,
          convert(bigint, 1) Dim_FunctionalAreaId,
          convert(bigint, 1) Dim_PlantId,
          convert(bigint, 1) as Dim_ProfitCenterId,
          BSAS_BUKRS, --CompanyCode
          BSAS_ZFBDT, --Dim_DateIdBaseDateForDueDateCalc
          BSAS_BLDAT, --Dim_DateIdDocument
          BSAS_BUDAT,  --Dim_DateIdPosting
          BSAS_DABRZ,  --Dim_DateIdReferenceDate
          BSAS_GSBER,   --Dim_BusinessAreaId
          BSAS_WAERS,   --Dim_Currencyid
          BSAS_PSWSL,   --Dim_GLCurrencyid
          BSAS_AUGDT,   --Dim_DateIdClearing
          BSAS_BLART,   --Dim_DocumentTypeId
          BSAS_BSCHL,    --Dim_PostingKeyId
          BSAS_FKBER,    --Dim_FunctionalAreaId
          BSAS_WERKS,    --Dim_PlantId
          BSAS_PRCTR    --Dim_ProfitCenterId
            FROM BSAS glc
      INNER JOIN dim_company dc ON dc.CompanyCode = BSAS_BUKRS AND dc.RowIsCurrent = 1
      INNER JOIN dim_chartofaccounts coa ON coa.GLAccountNumber = glc.BSAS_HKONT AND coa.CharOfAccounts = dc.ChartOfAccounts AND coa.RowIsCurrent = 1
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_generalledger ar
                   WHERE     ar.dd_AccountingDocNo = glc.BSAS_BELNR
                         AND ar.dd_AccountingDocItemNo = glc.BSAS_BUZEI
                         AND ar.dd_AssignmentNumber = ifnull(glc.BSAS_ZUONR, 'Not Set')
                         AND ar.dd_fiscalyear = glc.BSAS_GJAHR
                         AND ar.dd_FiscalPeriod  = glc.BSAS_MONAT
                         AND ar.dim_companyid = dc.Dim_CompanyId
                         AND ar.Dim_ChartOfAccountsId = coa.Dim_ChartOfAccountsId);


     /*Dim_ClearedFlagId */
		update tmp_fgenled_t002t f
		set f.Dim_ClearedFlagId = ifnull(gls.Dim_GeneralLedgerStatusId,1)
		from tmp_fgenled_t002t f
                   cross join Dim_GeneralLedgerStatus gls 
	    WHERE gls.Status = 'O - Open'
             AND gls.RowIsCurrent = 1
			 AND f.Dim_ClearedFlagId <> ifnull(gls.Dim_GeneralLedgerStatusId,1);

       /*Dim_DateIdBaseDateForDueDateCalc*/
       update tmp_fgenled_t002t f
		set f.Dim_DateIdBaseDateForDueDateCalc = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t002t f
                   left  join dim_date dt on dt.datevalue = BSAS_ZFBDT AND dt.CompanyCode = BSAS_BUKRS
       where f.Dim_DateIdBaseDateForDueDateCalc <> ifnull(dt.dim_dateid,1);
   

       /*Dim_DateIdDocument*/
       update tmp_fgenled_t002t f
		set f.Dim_DateIdDocument = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t002t f
                   left  join dim_date dt on dt.datevalue = BSAS_BLDAT AND dt.CompanyCode = BSAS_BUKRS
       where f.Dim_DateIdDocument <> ifnull(dt.dim_dateid,1);

       /*Dim_DateIdPosting*/
       update tmp_fgenled_t002t f
		set f.Dim_DateIdPosting = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t002t f
                   left  join dim_date dt on dt.datevalue = BSAS_BUDAT AND dt.CompanyCode = BSAS_BUKRS
       where f.Dim_DateIdPosting <> ifnull(dt.dim_dateid,1);
 
       /*Dim_DateIdReferenceDate*/
        update tmp_fgenled_t002t f
		set f.Dim_DateIdReferenceDate = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t002t f
                   left  join dim_date dt on dt.datevalue = BSAS_DABRZ AND dt.CompanyCode = BSAS_BUKRS
       where f.Dim_DateIdReferenceDate <> ifnull(dt.dim_dateid,1);

      /*Dim_BusinessAreaId*/
       update tmp_fgenled_t002t f
		set f.Dim_BusinessAreaId = ifnull(ba.Dim_BusinessAreaId,1)
		from tmp_fgenled_t002t f
                   left  join (select * from Dim_BusinessArea
                                         where RowIsCurrent = 1) ba on ba.BusinessArea = f.BSAS_GSBER
       where f.Dim_BusinessAreaId <> ifnull(ba.Dim_BusinessAreaId,1);

       /*Dim_Currencyid*/
       update tmp_fgenled_t002t f
		set f.dim_currencyId = ifnull(c.dim_currencyId,1)
		from tmp_fgenled_t002t f
                   left  join dim_currency c on c.CurrencyCode = f.BSAS_WAERS
       where f.dim_currencyId <> ifnull(c.dim_currencyId,1);

       /*Dim_GLCurrencyid*/
       update tmp_fgenled_t002t f
		set f.Dim_GLCurrencyid = ifnull(c.dim_currencyId,1)
		from tmp_fgenled_t002t f
                   left  join dim_currency c on c.CurrencyCode = f.BSAS_PSWSL
       where f.Dim_GLCurrencyid <> ifnull(c.dim_currencyId,1);
     
        /*Dim_DateIdClearing*/
        update tmp_fgenled_t002t f
		set f.Dim_DateIdClearing = ifnull(dt.dim_dateid,1)
		from tmp_fgenled_t002t f
                   left  join dim_date dt on dt.datevalue = BSAS_AUGDT AND dt.CompanyCode = f.BSAS_BUKRS
       where f.Dim_DateIdClearing <> ifnull(dt.dim_dateid,1);
      
       /*Dim_DocumentTypeId*/
       update tmp_fgenled_t002t f
		set f.Dim_DocumentTypeId = ifnull(dtt.dim_documenttypetextid,1)
		from tmp_fgenled_t002t f
                   left  join (select * from dim_documenttypetext 
                               where RowIsCurrent = 1) dtt on  dtt.type = f.BSAS_BLART
       where f.Dim_DocumentTypeId <>  ifnull(dtt.dim_documenttypetextid,1);

       /*Dim_PostingKeyId*/
       update tmp_fgenled_t002t f
		set f.Dim_PostingKeyId = ifnull(pk.Dim_PostingKeyId,1)
		from tmp_fgenled_t002t f
                   left  join (select * from Dim_PostingKey 
                               where RowIsCurrent = 1 and SpecialGLIndicator = 'Not Set') pk on  pk.PostingKey = f.BSAS_BSCHL
       where f.Dim_PostingKeyId <> ifnull(pk.Dim_PostingKeyId,1);

       /*Dim_FunctionalAreaId*/
       update tmp_fgenled_t002t f
		set f.Dim_FunctionalAreaId = ifnull(fa.Dim_FunctionalAreaId,1)
		from tmp_fgenled_t002t f
                   left  join (select * from Dim_FunctionalArea
                               where RowIsCurrent = 1 ) fa on fa.FunctionalArea = f.BSAS_FKBER
       where f.Dim_FunctionalAreaId <> ifnull(fa.Dim_FunctionalAreaId,1);

      /*Dim_PlantId*/
       update tmp_fgenled_t002t f
		set f.Dim_PlantId = ifnull(p.Dim_PlantId,1)
		from tmp_fgenled_t002t f
                   left  join (select * from Dim_Plant
                               where RowIsCurrent = 1 ) p on p.PlantCode = f.BSAS_WERKS
       where f.Dim_PlantId <> ifnull(p.Dim_PlantId,1);

       /*Dim_ProfitCenterId*/
       update tmp_fgenled_t002t f
		set f.Dim_ProfitCenterId = ifnull(pc.Dim_ProfitCenterId,1)
		from tmp_fgenled_t002t f
                   left  join (select * from Dim_ProfitCenter
                               where RowIsCurrent = 1 and ControllingArea = 'Not Set') pc on pc.ProfitCenterCode = f.BSAS_PRCTR
       where f.Dim_ProfitCenterId <> ifnull(pc.Dim_ProfitCenterId,1);


   INSERT INTO fact_generalledger
                          (fact_generalledgerid,
                               amt_InDocCurrency,
                               amt_InLocalCurrency,
                               amt_TaxInDocCurrency,
                               amt_TaxInLocalCurrency,
                               dd_AccountingDocItemNo,
                               dd_AccountingDocNo,
                               dd_AssignmentNumber,
                               Dim_ClearedFlagId,
                               Dim_DateIdBaseDateForDueDateCalc,
                               Dim_DateIdDocument,
                               Dim_DateIdPosting,
                               Dim_DateIdReferenceDate,
                               dd_DebitCreditInd,
                               dd_ClearingDocumentNo,
                               dd_FiscalPeriod,
                               dd_FiscalYear,
                               Dim_BusinessAreaId,
                               Dim_CompanyId,
                               Dim_CurrencyId,
                               Dim_GLCurrencyId,
                               Dim_DateIdClearing,
                               Dim_DocumentTypeId,
                               Dim_PostingKeyId,
                               Dim_ChartOfAccountsId,
                               Dim_FunctionalAreaId,
                               Dim_PlantId,
                               Dim_ProfitCenterId)
         select fact_generalledgerid,
                               amt_InDocCurrency,
                               amt_InLocalCurrency,
                               amt_TaxInDocCurrency,
                               amt_TaxInLocalCurrency,
                               dd_AccountingDocItemNo,
                               dd_AccountingDocNo,
                               dd_AssignmentNumber,
                               Dim_ClearedFlagId,
                               Dim_DateIdBaseDateForDueDateCalc,
                               Dim_DateIdDocument,
                               Dim_DateIdPosting,
                               Dim_DateIdReferenceDate,
                               dd_DebitCreditInd,
                               dd_ClearingDocumentNo,
                               dd_FiscalPeriod,
                               dd_FiscalYear,
                               Dim_BusinessAreaId,
                               Dim_CompanyId,
                               Dim_CurrencyId,
                               Dim_GLCurrencyId,
                               Dim_DateIdClearing,
                               Dim_DocumentTypeId,
                               Dim_PostingKeyId,
                               Dim_ChartOfAccountsId,
                               Dim_FunctionalAreaId,
                               Dim_PlantId,
                               Dim_ProfitCenterId
                   from tmp_fgenled_t002t;

drop table if exists tmp_fgenled_t002t;



/* Update 3 */

UPDATE fact_generalledger gl
SET
 gl.dd_RefDocNo = mm.dd_MaterialDocNo
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl, fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND  ifnull(gl.dd_RefDocNo,'xx') <> ifnull(mm.dd_MaterialDocNo,'yy');

UPDATE fact_generalledger gl
SET gl.Dim_VendorId = mm.Dim_Vendorid
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl,fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.Dim_VendorId <> mm.Dim_Vendorid;

UPDATE fact_generalledger gl
SET gl.Dim_MovementTypeId = mm.Dim_MovementTypeid
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl, fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.Dim_MovementTypeId,-1) <> ifnull(mm.Dim_MovementTypeid,-2);

UPDATE fact_generalledger gl
SET gl.dd_DocumentNo = mm.dd_DocumentNo
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl, fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.dd_DocumentNo,'xx') <> ifnull(mm.dd_DocumentNo,'yy');

UPDATE fact_generalledger gl
SET gl.dd_DocumentItemNo = mm.dd_DocumentItemNo
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl, fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.dd_DocumentItemNo,-1) <> ifnull(mm.dd_DocumentItemNo,-2);

UPDATE fact_generalledger gl
SET gl.Dim_PartId = mm.Dim_PartId
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl, fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.Dim_PartId <> mm.Dim_PartId;

UPDATE fact_generalledger gl
SET gl.Dim_PurchasePlantId = mm.Dim_Plantid
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl,fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.Dim_PurchasePlantId <> mm.Dim_Plantid;

UPDATE fact_generalledger gl
SET gl.ct_Quantity = mm.ct_Quantity
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl,fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.ct_Quantity,-2) <> ifnull(mm.ct_Quantity,-2);

UPDATE fact_generalledger gl
SET gl.amt_StdPrice = w.STPRS
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl, fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.amt_StdPrice,-1) <> ifnull(w.STPRS,-2);

UPDATE fact_generalledger gl
SET gl.amt_UnitPrice = w.PEINH
from fact_materialmovement mm, MBEW w, BKPF p, Dim_Part pt, Dim_Plant pl, fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = p.BKPF_BELNR
 AND p.BKPF_AWKEY = mm.dd_MaterialDocNo
 AND mm.Dim_Partid = pt.Dim_Partid
 AND mm.Dim_PlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND ifnull(gl.amt_UnitPrice,-1) <> ifnull(w.PEINH,-2);



/* Update 4 */

UPDATE fact_generalledger gl
SET
 gl.Dim_ReceivingPartId = mm.Dim_ReceivingPartId
from fact_materialmovement mm, MBEW w, Dim_Part pt, Dim_Plant pl,fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = mm.dd_MaterialDocNo
 AND mm.Dim_ReceivingPartid = pt.Dim_Partid
 AND mm.Dim_ReceivingPlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND  gl.Dim_ReceivingPartId <> mm.Dim_ReceivingPartId;

UPDATE fact_generalledger gl
SET gl.Dim_ReceivingPlantId = mm.Dim_ReceivingPlantid
from fact_materialmovement mm, MBEW w, Dim_Part pt, Dim_Plant pl, fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = mm.dd_MaterialDocNo
 AND mm.Dim_ReceivingPartid = pt.Dim_Partid
 AND mm.Dim_ReceivingPlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.Dim_ReceivingPlantId <> mm.Dim_ReceivingPlantid;

UPDATE fact_generalledger gl
SET gl.amt_StdPriceatRecPlant = w.STPRS
from fact_materialmovement mm, MBEW w, Dim_Part pt, Dim_Plant pl,fact_generalledger gl
WHERE     gl.dd_AccountingDocNo = mm.dd_MaterialDocNo
 AND mm.Dim_ReceivingPartid = pt.Dim_Partid
 AND mm.Dim_ReceivingPlantId = pl.Dim_Plantid
 AND pt.PartNumber = w.MATNR
 AND pt.RowIsCurrent = 1
 AND pl.ValuationArea = w.BWKEY
 AND pl.RowIsCurrent = 1
 AND w.BWTAR IS NULL
 AND w.MBEW_LBKUM > 0
AND gl.amt_StdPriceatRecPlant <> w.STPRS;


DROP TABLE IF EXISTS TMP_amt_UnitPrice_GL;
CREATE TABLE TMP_amt_UnitPrice_GL
AS
SELECT p.dd_DocumentNo,p.dd_DocumentItemNo, AVG(p.amt_UnitPrice) as avg_amt_UnitPrice
FROM fact_purchase p,fact_generalledger gl
WHERE p.dd_DocumentNo = gl.dd_DocumentNo
AND p.dd_DocumentItemNo = gl.dd_DocumentItemNo
GROUP BY p.dd_DocumentNo,p.dd_DocumentItemNo;


UPDATE fact_generalledger gl
   SET gl.amt_POUnitPrice = p.avg_amt_UnitPrice
 FROM TMP_amt_UnitPrice_GL p, fact_generalledger gl         
            WHERE p.dd_DocumentNo = gl.dd_DocumentNo
                  AND p.dd_DocumentItemNo = gl.dd_DocumentItemNo
		  AND ifnull(gl.amt_POUnitPrice, 0) <> ifnull(p.avg_amt_UnitPrice, 0);

DROP TABLE IF EXISTS TMP_amt_UnitPrice_GL;