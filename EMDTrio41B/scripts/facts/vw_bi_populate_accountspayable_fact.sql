Drop table if exists tmp_gblcurr_fap;

Create table tmp_gblcurr_fap(
pGlobalCurrency varchar(3) null);

Insert into tmp_gblcurr_fap(pGlobalCurrency) values(null);

Update tmp_gblcurr_fap
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');


/* Update 1 */

/* Update column Dim_ClearedFlagId */

/* This will first update all Dim_ClearedFlagId ( for rows that match the main join condition ) to 1 */

UPDATE fact_accountspayable fap
SET Dim_ClearedFlagId = 1
	,dw_update_date = current_timestamp 
	from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_ClearedFlagId <> 1;

UPDATE fact_accountspayable fap
SET Dim_ClearedFlagId = ars.Dim_AccountPayableStatusId
	,dw_update_date = current_timestamp 
	from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_AccountPayableStatus ars, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND ars.Status = CASE WHEN BSIK_REBZG IS NOT NULL AND BSIK_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END AND ars.RowIsCurrent = 1
AND fap.Dim_ClearedFlagId <> ars.Dim_AccountPayableStatusId;



/* Update column Dim_DateIdAccDocDateEntered */

UPDATE fact_accountspayable fap
SET Dim_DateIdAccDocDateEntered = 1
	,dw_update_date = current_timestamp 
	from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND  fap.Dim_DateIdAccDocDateEntered <> 1;


UPDATE fact_accountspayable fap
SET Dim_DateIdAccDocDateEntered = dt.dim_dateid
	,dw_update_date = current_timestamp 
	from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_CPUDT AND dt.CompanyCode = arc.BSIK_BUKRS
AND fap.Dim_DateIdAccDocDateEntered <> dt.dim_dateid; 


/* Update column Dim_DateIdBaseDateForDueDateCalc */

UPDATE fact_accountspayable fap
SET Dim_DateIdBaseDateForDueDateCalc = 1 
,dw_update_date = current_timestamp
from bsik arc, dim_company dcm, dim_vendor dv,  fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdBaseDateForDueDateCalc <> 1;


UPDATE fact_accountspayable fap
SET Dim_DateIdBaseDateForDueDateCalc = dt.dim_dateid
	,dw_update_date = current_timestamp 
	from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_ZFBDT AND dt.CompanyCode = arc.BSIK_BUKRS
AND Dim_DateIdBaseDateForDueDateCalc <> dt.dim_dateid;


/* Update column Dim_DateIdCreated */

UPDATE fact_accountspayable fap
SET Dim_DateIdCreated = 1
	,dw_update_date = current_timestamp 
	from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND Dim_DateIdCreated <> 1;


UPDATE fact_accountspayable fap
SET Dim_DateIdCreated = dt.dim_dateid
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_BLDAT AND dt.CompanyCode = arc.BSIK_BUKRS
AND Dim_DateIdCreated <> dt.dim_dateid;


/* Update column Dim_DateIdPosting */

UPDATE fact_accountspayable fap
SET Dim_DateIdPosting = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND Dim_DateIdPosting <> 1;


UPDATE fact_accountspayable fap

SET Dim_DateIdPosting = dt.dim_dateid
	,dw_update_date = current_timestamp 
	from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_BUDAT AND dt.CompanyCode = arc.BSIK_BUKRS
AND Dim_DateIdPosting <> dt.dim_dateid;


/* Update column Dim_BlockingPaymentReasonId */

UPDATE fact_accountspayable fap
SET Dim_BlockingPaymentReasonId = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_BlockingPaymentReasonId <> 1;


DROP TABLE IF EXISTS TMP_UPDT_BSIK;
CREATE TABLE TMP_UPDT_BSIK
AS
SELECT BSIK_BELNR,BSIK_BUZEI,BSIK_ZUONR,BSIK_GJAHR,BSIK_MONAT,max(BSIK_ZLSPR) BSIK_ZLSPR
FROM BSIK
GROUP BY BSIK_BELNR,BSIK_BUZEI,BSIK_ZUONR,BSIK_GJAHR,BSIK_MONAT;

UPDATE fact_accountspayable fap
SET Dim_BlockingPaymentReasonId = bpr.Dim_BlockingPaymentReasonId
from TMP_UPDT_BSIK arc, Dim_BlockingPaymentReason bpr, fact_accountspayable fap 
WHERE fap.dd_AccountingDocNo = arc.BSIK_BELNR
	AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
	AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
	AND fap.dd_fiscalyear = arc.BSIK_GJAHR
	AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
	AND bpr.BlockingKeyPayment = BSIK_ZLSPR AND bpr.RowIsCurrent = 1
	AND fap.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId;
	
DROP TABLE IF EXISTS TMP_UPDT_BSIK;


/* Update column Dim_BusinessAreaId */

UPDATE fact_accountspayable fap
SET Dim_BusinessAreaId = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_BusinessAreaId <> 1;


UPDATE fact_accountspayable fap

SET Dim_BusinessAreaId = ba.Dim_BusinessAreaId
	,dw_update_date = current_timestamp 
	from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_BusinessArea ba, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND ba.BusinessArea = BSIK_GSBER AND ba.RowIsCurrent = 1
AND fap.Dim_BusinessAreaId <> ba.Dim_BusinessAreaId;



/* Update column Dim_Currencyid */

UPDATE fact_accountspayable fap
SET Dim_Currencyid = 1
	,dw_update_date = current_timestamp
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_Currencyid <> 1;



UPDATE fact_accountspayable fap
SET Dim_Currencyid = c.dim_currencyid
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_currency c,fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
 AND c.CurrencyCode = dcm.Currency
AND fap.Dim_Currencyid <> c.dim_currencyid;


/* Update tran and global currencies */

UPDATE fact_accountspayable fap
SET Dim_Currencyid_TRA = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_Currencyid_TRA <> 1;

/* BSIK_SKFBT is the transaction/doc currency */

UPDATE fact_accountspayable fap
SET Dim_Currencyid_TRA = c.dim_currencyid
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_currency c, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND c.CurrencyCode = bsik_waers
AND fap.Dim_Currencyid_TRA <> c.dim_currencyid;


UPDATE fact_accountspayable fap
SET Dim_Currencyid_GBL = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_Currencyid_GBL <> 1;


UPDATE fact_accountspayable fap
SET Dim_Currencyid_GBL = c.dim_currencyid
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_currency c, tmp_gblcurr_fap, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND c.CurrencyCode = pGlobalCurrency
AND fap.Dim_Currencyid_GBL <> c.dim_currencyid;




/* Update column Dim_DateIdClearing */

UPDATE fact_accountspayable fap
SET Dim_DateIdClearing = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdClearing <> 1;


UPDATE fact_accountspayable fap
SET Dim_DateIdClearing = dt.dim_dateid
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap,dim_date dt
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_AUGDT AND dt.CompanyCode = arc.BSIK_BUKRS
AND  fap.Dim_DateIdClearing <> dt.dim_dateid;

/* Update column Dim_DocumentTypeId */

UPDATE fact_accountspayable fap
SET Dim_DocumentTypeId = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_DocumentTypeId <> 1;


UPDATE fact_accountspayable fap
SET Dim_DocumentTypeId = dtt.dim_documenttypetextid
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_documenttypetext dtt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
 AND dtt.type = BSIK_BLART AND dtt.RowIsCurrent = 1
AND fap.Dim_DocumentTypeId <> dtt.dim_documenttypetextid;


/* Update column Dim_PaymentReasonId */

UPDATE fact_accountspayable fap
SET Dim_PaymentReasonId = 1
	,dw_update_date = current_timestamp
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap	
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_PaymentReasonId <> 1;


UPDATE fact_accountspayable fap
SET Dim_PaymentReasonId = pr.Dim_PaymentReasonId
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap,Dim_PaymentReason pr
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
 AND pr.PaymentReasonCode = BSIK_RSTGR AND pr.CompanyCode = BSIK_BUKRS AND pr.RowIsCurrent = 1
AND fap.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId;


/* Update column Dim_PostingKeyId */

UPDATE fact_accountspayable fap
SET Dim_PostingKeyId = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_PostingKeyId <> 1;


UPDATE fact_accountspayable fap
SET Dim_PostingKeyId = pk.Dim_PostingKeyId
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_PostingKey pk, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND pk.PostingKey = BSIK_BSCHL AND pk.SpecialGLIndicator = ifnull(BSIK_UMSKZ, 'Not Set') AND pk.RowIsCurrent = 1
AND fap.Dim_PostingKeyId <> pk.Dim_PostingKeyId;


/* Update column Dim_SpecialGLIndicatorId */

UPDATE fact_accountspayable fap
SET Dim_SpecialGLIndicatorId = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_SpecialGLIndicatorId <> 1;


UPDATE fact_accountspayable fap
SET Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
	,dw_update_date = current_timestamp
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap  ,Dim_SpecialGLIndicator sgl
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND sgl.SpecialGLIndicator = BSIK_UMSKZ AND sgl.AccountType = 'D' AND sgl.RowIsCurrent = 1
AND fap.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId;


/* Update column Dim_TargetSpecialGLIndicatorId */

UPDATE fact_accountspayable fap
SET Dim_TargetSpecialGLIndicatorId = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_TargetSpecialGLIndicatorId <> 1;


UPDATE fact_accountspayable fap
SET Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_SpecialGLIndicator sgl, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND sgl.SpecialGLIndicator = BSIK_ZUMSK AND sgl.AccountType = 'D' AND sgl.RowIsCurrent = 1
AND fap.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId;


/* Update column Dim_SpecialGlTransactionTypeId */

UPDATE fact_accountspayable fap
SET Dim_SpecialGlTransactionTypeId = 1
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_SpecialGlTransactionTypeId <> 1;


UPDATE fact_accountspayable fap
SET Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv,Dim_SpecialGlTransactionType sgt,fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND sgt.SpecialGlTransactionTypeId = BSIK_UMSKS AND sgt.RowIsCurrent = 1
AND fap.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId;

/* Update column Dim_CustomerPaymentTermsid */

UPDATE fact_accountspayable fap
SET Dim_CustomerPaymentTermsid = cpt.Dim_Termid
	,dw_update_date = current_timestamp
from BSIK arc, dim_company dcm, dim_Vendor dv
 ,Dim_Term cpt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND cpt.TermCode = BSIK_ZTERM 
AND  arc.BSIK_ZTERM IS NOT NULL;

UPDATE fact_accountspayable fap
SET Dim_CustomerPaymentTermsid = 1
	,dw_update_date = current_timestamp 
from BSIK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND arc.BSIK_ZTERM IS NULL;
/* Now the colums which did not have inner subqueries */

UPDATE fact_accountspayable fap 
SET  amt_CashDiscountDocCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_WSKTO
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap 
SET amt_CashDiscountLocalCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_SKNTO
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap 
SET amt_InLocalCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_DMBTR
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv,fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap 
SET amt_TaxInDocCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_WMWST
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap 
SET amt_TaxInLocalCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_MWSTS
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap 
SET dd_AccountingDocItemNo = BSIK_BUZEI
	,dw_update_date = current_timestamp
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_AccountingDocItemNo <> BSIK_BUZEI;


UPDATE fact_accountspayable fap 
SET dd_AccountingDocNo = BSIK_BELNR
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_AccountingDocNo <> BSIK_BELNR;


UPDATE fact_accountspayable fap 
SET
dd_AssignmentNumber = ifnull(BSIK_ZUONR, 'Not Set')
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv,fact_accountspayable fap  
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_AssignmentNumber <> ifnull(BSIK_ZUONR, 'Not Set');


UPDATE fact_accountspayable fap 
SET dd_debitcreditid = (CASE WHEN BSIK_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1;

UPDATE fact_accountspayable fap 
SET
dd_ClearingDocumentNo = ifnull(BSIK_AUGBL, 'Not Set')
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_ClearingDocumentNo <> ifnull(BSIK_AUGBL, 'Not Set');


UPDATE fact_accountspayable fap 
SET
dd_FixedPaymentTerms = ifnull(BSIK_ZBFIX, 'Not Set')
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_FixedPaymentTerms <> ifnull(BSIK_ZBFIX, 'Not Set');


UPDATE fact_accountspayable fap 
SET
dd_InvoiceNumberTransBelongTo = ifnull(BSIK_REBZG, 'Not Set')
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_InvoiceNumberTransBelongTo <> ifnull(BSIK_REBZG, 'Not Set');


UPDATE fact_accountspayable fap 
SET fap.dim_companyid = dcm.Dim_CompanyId
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.dim_companyid <> dcm.Dim_CompanyId;


UPDATE fact_accountspayable fap 
SET fap.Dim_VendorID = dv.dim_VendorId
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_VendorID <> dv.dim_VendorId;


UPDATE fact_accountspayable fap 
SET
dd_DocumentNo = ifnull(BSIK_EBELN, 'Not Set')
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_DocumentNo <> ifnull(BSIK_EBELN, 'Not Set');


UPDATE fact_accountspayable fap 
SET dd_DocumentItemNo = BSIK_EBELP
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_DocumentItemNo <> BSIK_EBELP;

UPDATE fact_accountspayable fap 
SET
dd_ReferenceDocumentNo = ifnull(BSIK_XBLNR, 'Not Set')
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap  
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_ReferenceDocumentNo <> ifnull(BSIK_XBLNR, 'Not Set');


UPDATE fact_accountspayable fap 
SET dd_CashDiscountPercentage1 = BSIK_ZBD1P
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_CashDiscountPercentage1 <> BSIK_ZBD1P;


UPDATE fact_accountspayable fap 
SET dd_CashDiscountPercentage2 = BSIK_ZBD2P
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_CashDiscountPercentage2 <> BSIK_ZBD2P;


UPDATE fact_accountspayable fap 
SET
dd_ProductionOrderNo = ifnull(BSIK_AUFNR, 'Not Set')
	,dw_update_date = current_timestamp 
from bsik arc, dim_company dcm, dim_vendor dv, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dd_ProductionOrderNo <> ifnull(BSIK_AUFNR, 'Not Set');



/* End of Update 1 */

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_accountspayable';

INSERT INTO NUMBER_FOUNTAIN
   select 'fact_accountspayable',ifnull(max(f.fact_accountspayableid ), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_accountspayable f;




INSERT INTO fact_accountspayable(fact_accountspayableid,
                                 amt_CashDiscountDocCurrency,
                                 amt_CashDiscountLocalCurrency,
                                 amt_InDocCurrency,
                                 amt_InLocalCurrency,
                                 amt_TaxInDocCurrency,
                                 amt_TaxInLocalCurrency,
                                 dd_AccountingDocItemNo,
                                 dd_AccountingDocNo,
                                 dd_AssignmentNumber,
                                 Dim_ClearedFlagId,
                                 Dim_DateIdAccDocDateEntered,
                                 Dim_DateIdBaseDateForDueDateCalc,
                                 Dim_DateIdCreated,
                                 Dim_DateIdPosting,
                                 dd_debitcreditid,
                                 dd_ClearingDocumentNo,
                                 dd_FiscalPeriod,
                                 dd_FiscalYear,
                                 dd_FixedPaymentTerms,
                                 dd_InvoiceNumberTransBelongTo,
                                 Dim_BlockingPaymentReasonId,
                                 Dim_BusinessAreaId,
                                 Dim_CompanyId,
                                 Dim_CurrencyId,
                                 Dim_VendorId,
                                 Dim_DateIdClearing,
                                 Dim_DocumentTypeId,
                                 Dim_PaymentReasonId,
                                 Dim_PostingKeyId,
                                 Dim_SpecialGLIndicatorId,
                                 Dim_TargetSpecialGLIndicatorId,
                                 dd_DocumentNo,
                                 dd_DocumentItemNo,
                                 dd_ReferenceDocumentNo,
                                 dd_CashDiscountPercentage1,
                                 dd_CashDiscountPercentage2,
                                 dd_ProductionOrderNo,
                                 Dim_SpecialGlTransactionTypeId,
								 Dim_CustomerPaymentTermsid,
				                 dim_Currencyid_TRA, 
				                 dim_Currencyid_GBL,
				                 amt_exchangerate,
				                 amt_exchangerate_GBL)
   SELECT ((SELECT max_id
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_accountspayable')
           + row_number() over (order by '')) as fact_accountspayableid,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_WSKTO amt_CashDiscountDocCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_SKNTO amt_CashDiscountLocalCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_WRBTR amt_InDocCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_DMBTR amt_InLocalCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_WMWST amt_TaxInDocCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_MWSTS amt_TaxInLocalCurrency,
 BSIK_BUZEI dd_AccountingDocItemNo,
 BSIK_BELNR dd_AccountingDocNo,
 ifnull(BSIK_ZUONR,'Not Set') dd_AssignmentNumber,
 1 Dim_ClearedFlagId,
 1 Dim_DateIdAccDocDateEntered,
 1 Dim_DateIdBaseDateForDueDateCalc,
 1 Dim_DateIdCreated,
 1 Dim_DateIdPosting,
 (CASE WHEN BSIK_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END) dd_debitcreditid,
 ifnull(BSIK_AUGBL,'Not Set') dd_ClearingDocumentNo,
 BSIK_MONAT dd_FiscalPeriod,
 BSIK_GJAHR dd_FiscalYear,
 ifnull(BSIK_ZBFIX, 'Not Set') dd_FixedPaymentTerms,
 ifnull(BSIK_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
 1 Dim_BlockingPaymentReasonId,
 1 Dim_BusinessAreaId,
 dc.Dim_CompanyId,
 1 Dim_Currencyid,
 dv.Dim_VendorID,
 1 Dim_DateIdClearing,
 1 Dim_DocumentTypeId,
 1 Dim_PaymentReasonId,
 1 Dim_PostingKeyId,
 1 Dim_SpecialGLIndicatorId,
 1 Dim_TargetSpecialGLIndicatorId,
 ifnull(BSIK_EBELN, 'Not Set') dd_DocumentNo,
 BSIK_EBELP dd_DocumentItemNo,
 ifnull(BSIK_XBLNR,'Not Set') dd_ReferenceDocumentNo,
 BSIK_ZBD1P dd_CashDiscountPercentage1,
 BSIK_ZBD2P dd_CashDiscountPercentage2,
 ifnull(BSIK_AUFNR,'Not Set') dd_ProductionOrderNo,
 1 Dim_SpecialGlTransactionTypeId,
 1 Dim_CustomerPaymentTermsid,
 1 Dim_Currencyid_TRA,
 1 Dim_Currencyid_GBL,
 1 amt_exchangerate,
 1 amt_exchangerate_GBL		
 FROM BSIK arc
 INNER JOIN dim_company dc ON dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 INNER JOIN dim_vendor dv ON dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1,
 tmp_gblcurr_fap
 WHERE NOT EXISTS
 (SELECT 1
 FROM fact_accountspayable ar
 WHERE ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 AND ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId);
;

/* update of columns that were inserted with default value*/
update fact_accountspayable ar 
set Dim_ClearedFlagId = Dim_AccountPayableStatusId 
from Dim_AccountPayableStatus ars, fact_accountspayable ar , BSIK arc, dim_company dc, dim_vendor dv
where ars.Status = CASE WHEN BSIK_REBZG IS NOT NULL AND BSIK_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END
 AND ars.RowIsCurrent = 1 
 /*general conditions*/
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_ClearedFlagId <> Dim_AccountPayableStatusId
 ;
 
 update fact_accountspayable ar 
set Dim_DateIdAccDocDateEntered = dim_dateid 
from fact_accountspayable ar, dim_date dt ,BSIK arc, dim_company dc, dim_vendor dv
WHERE dt.DateValue = BSIK_CPUDT
AND dt.CompanyCode = arc.BSIK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_DateIdAccDocDateEntered <> dim_dateid 
 ;
 
update fact_accountspayable ar 
set Dim_DateIdBaseDateForDueDateCalc = dim_dateid 
from fact_accountspayable ar, dim_date dt ,BSIK arc, dim_company dc, dim_vendor dv
WHERE dt.DateValue = BSIK_ZFBDT
 AND dt.CompanyCode = arc.BSIK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_DateIdBaseDateForDueDateCalc <> dim_dateid 
 ;
 
 update fact_accountspayable ar 
set Dim_DateIdCreated = dim_dateid 
from fact_accountspayable ar, dim_date dt ,BSIK arc, dim_company dc, dim_vendor dv
WHERE dt.DateValue = BSIK_BLDAT
 AND dt.CompanyCode = arc.BSIK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_DateIdCreated <> dim_dateid 
 ;
 
 update fact_accountspayable ar 
set Dim_DateIdPosting = dim_dateid 
from fact_accountspayable ar, dim_date dt ,BSIK arc, dim_company dc, dim_vendor dv
WHERE dt.DateValue = BSIK_BUDAT
 AND dt.CompanyCode = arc.BSIK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_DateIdPosting <> dim_dateid 
 ;
 
 update fact_accountspayable ar 
set Dim_BlockingPaymentReasonId = Dim_BlockingPaymentReasonId 
from fact_accountspayable ar, Dim_BlockingPaymentReason bpr ,BSIK arc, dim_company dc, dim_vendor dv
WHERE bpr.BlockingKeyPayment = BSIK_ZLSPR
 AND bpr.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId 
 ;
 
 update fact_accountspayable ar 
set Dim_BusinessAreaId = Dim_BusinessAreaId 
from fact_accountspayable ar, Dim_BusinessArea ba ,BSIK arc, dim_company dc, dim_vendor dv
WHERE ba.BusinessArea = BSIK_GSBER
 AND ba.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_BusinessAreaId <> ba.Dim_BusinessAreaId 
 ;
 
 
 update fact_accountspayable ar 
set dim_currencyid = dim_currencyid 
from fact_accountspayable ar, dim_currency c ,BSIK arc, dim_company dc, dim_vendor dv
WHERE c.CurrencyCode = dc.Currency
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.dim_currencyid <> c.dim_currencyid 
 ;
 
 
 update fact_accountspayable ar 
set Dim_DateIdClearing = dt.dim_dateid 
from fact_accountspayable ar, dim_date dt ,BSIK arc, dim_company dc, dim_vendor dv
WHERE dt.DateValue = BSIK_AUGDT
 AND dt.CompanyCode = arc.BSIK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_DateIdClearing <> dt.dim_dateid
 ;
 
 update fact_accountspayable ar 
set Dim_DocumentTypeId = dtt.dim_documenttypetextid
from fact_accountspayable ar, dim_documenttypetext dtt ,BSIK arc, dim_company dc, dim_vendor dv
WHERE dtt.type = BSIK_BLART
 AND dtt.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_DocumentTypeId <> dtt.dim_documenttypetextid
 ;
 
 update fact_accountspayable ar 
set Dim_PaymentReasonId = pr.Dim_PaymentReasonId
from fact_accountspayable ar, Dim_PaymentReason pr ,BSIK arc, dim_company dc, dim_vendor dv
WHERE pr.PaymentReasonCode = BSIK_RSTGR
 AND pr.CompanyCode = BSIK_BUKRS
 AND pr.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId
 ;
 
 update fact_accountspayable ar 
set Dim_PostingKeyId = pk.Dim_PostingKeyId
from fact_accountspayable ar, Dim_PostingKey pk ,BSIK arc, dim_company dc, dim_vendor dv
WHERE pk.PostingKey= BSIK_BSCHL
 AND pk.SpecialGLIndicator = ifnull(BSIK_UMSKZ,'Not Set')
 AND pk.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_PostingKeyId <> pk.Dim_PostingKeyId
 ;
 
 update fact_accountspayable ar 
set Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from fact_accountspayable ar, Dim_SpecialGLIndicator sgl ,BSIK arc, dim_company dc, dim_vendor dv
WHERE sgl.SpecialGLIndicator = BSIK_UMSKZ
 AND sgl.AccountType = 'D'
 AND sgl.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
 ;
 
 update fact_accountspayable ar 
set Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from fact_accountspayable ar, Dim_SpecialGLIndicator sgl ,BSIK arc, dim_company dc, dim_vendor dv
WHERE sgl.SpecialGLIndicator = BSIK_ZUMSK
 AND sgl.AccountType = 'D'
 AND sgl.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
 ;
 
 
 update fact_accountspayable ar 
set Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
from fact_accountspayable ar, Dim_SpecialGlTransactionType sgt ,BSIK arc, dim_company dc, dim_vendor dv
WHERE sgt.SpecialGlTransactionTypeId = BSIK_UMSKS
 AND sgt.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId
 ;
 
 
 update fact_accountspayable ar 
set Dim_CustomerPaymentTermsid = cpt.Dim_Termid
from fact_accountspayable ar, Dim_Term cpt ,BSIK arc, dim_company dc, dim_vendor dv
WHERE cpt.TermCode = BSIK_ZTERM
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_CustomerPaymentTermsid <> cpt.Dim_Termid
 ;
 
 update fact_accountspayable ar 
set Dim_Currencyid_TRA = c.dim_currencyid
from fact_accountspayable ar, dim_currency c ,BSIK arc, dim_company dc, dim_vendor dv
WHERE c.CurrencyCode = bsik_waers
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_Currencyid_TRA <> c.dim_currencyid
 ;
 
 
 update fact_accountspayable ar 
set Dim_Currencyid_GBL = dcr.Dim_Currencyid
from fact_accountspayable ar, Dim_Currency dcr ,BSIK arc, dim_company dc, dim_vendor dv,tmp_gblcurr_fap
WHERE dcr.CurrencyCode = tmp_gblcurr_fap.pGlobalCurrency
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_Currencyid_GBL <> dcr.Dim_Currencyid
 ;
 
 
drop table if exists tmp_fap_exrate;
create table tmp_fap_exrate
as
select fact_accountspayableid,max(z.exchangeRate) exchangeRate
from fact_accountspayable ar, tmp_getExchangeRate1 z ,BSIK arc, dim_company dc, dim_vendor dv
WHERE z.pFromCurrency  = BSIK_WAERS and z.fact_script_name = 'bi_populate_accountspayable_fact'
      and z.pToCurrency = dc.currency AND z.pDate = BSIK_BUDAT
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.amt_exchangerate <> z.exchangeRate 
 group by fact_accountspayableid;
 
 UPDATE fact_accountspayable fap
SET amt_ExchangeRate = z.exchangeRate
from tmp_fap_exrate z ,fact_accountspayable fap
WHERE fap.fact_accountspayableid = z.fact_accountspayableid;

drop table if exists tmp_fap_exrate;
 
 update fact_accountspayable ar 
set amt_exchangerate_GBL = z.exchangeRate
from fact_accountspayable ar, tmp_getExchangeRate1 z ,BSIK arc, dim_company dc, dim_vendor dv, tmp_gblcurr_fap
WHERE z.pFromCurrency  = BSIK_WAERS and z.fact_script_name = 'bi_populate_accountspayable_fact'
      and z.pToCurrency = tmp_gblcurr_fap.pGlobalCurrency AND z.pDate = BSIK_BUDAT  
 and ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 and dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and amt_exchangerate_GBL <> z.exchangeRate
 ;
 
/* update of columns that were inserted with default value*/

/* Update 2 */


/* Update column Dim_ClearedFlagId */

UPDATE fact_accountspayable fap
SET Dim_ClearedFlagId = 1
	,dw_update_date = current_timestamp
from BSAK arc, dim_company dcm, dim_Vendor dv,  fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_ClearedFlagId <> 1;


drop table if exists fact_accountspayable_apsd_tmp;
create table fact_accountspayable_apsd_tmp as
select distinct fact_accountspayableid, ars.Dim_AccountPayableStatusId
from 
fact_accountspayable fap
,BSAK arc, dim_company dcm, dim_Vendor dv
,Dim_AccountPayableStatus ars
WHERE 
1=1
and fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND ars.Status = CASE WHEN BSAK_REBZG IS NOT NULL AND BSAK_REBZJ <> 0 THEN 'F - Partial' ELSE 'C - Cleared' END AND ars.RowIsCurrent = 1
AND fap.Dim_ClearedFlagId <> ars.Dim_AccountPayableStatusId;


UPDATE fact_accountspayable fap

SET Dim_ClearedFlagId = tmp.Dim_AccountPayableStatusId
,dw_update_date = current_timestamp 
from fact_accountspayable_apsd_tmp tmp, fact_accountspayable fap
WHERE
fap.fact_accountspayableid=tmp.fact_accountspayableid
AND fap.Dim_ClearedFlagId <> tmp.Dim_AccountPayableStatusId;

drop table if exists fact_accountspayable_apsd_tmp;



/* Update column Dim_DateIdAccDocDateEntered */

UPDATE fact_accountspayable fap
		SET Dim_DateIdAccDocDateEntered = 1
			,dw_update_date = current_timestamp 
		from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
		WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
		AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
		AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
		AND fap.dd_fiscalyear = arc.BSAK_GJAHR
		AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
		AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
		AND dcm.RowIsCurrent = 1
		AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
		AND dv.RowIsCurrent = 1
		AND fap.Dim_DateIdAccDocDateEntered <> 1;

UPDATE fact_accountspayable fap
			SET Dim_DateIdAccDocDateEntered = dt.dim_dateid
				,dw_update_date = current_timestamp 
			from BSAK arc, dim_company dcm, dim_Vendor dv
			 ,dim_date dt, fact_accountspayable fap
			WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
			AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
			AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
			AND fap.dd_fiscalyear = arc.BSAK_GJAHR
			AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
			AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
			AND dcm.RowIsCurrent = 1
			AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
			AND dv.RowIsCurrent = 1
			  AND dt.DateValue = BSAK_CPUDT AND dt.CompanyCode = arc.BSAK_BUKRS
			AND fap.Dim_DateIdAccDocDateEntered <> dt.dim_dateid 
			;

/* Update column Dim_DateIdBaseDateForDueDateCalc */

UPDATE fact_accountspayable fap
SET Dim_DateIdBaseDateForDueDateCalc = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdBaseDateForDueDateCalc <> 1;


UPDATE fact_accountspayable fap
SET Dim_DateIdBaseDateForDueDateCalc = dt.dim_dateid
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt,fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_ZFBDT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdBaseDateForDueDateCalc <> dt.dim_dateid;


/* Update column Dim_DateIdCreated */

UPDATE fact_accountspayable fap
SET Dim_DateIdCreated = 1
	,dw_update_date = current_timestamp
from BSAK arc, dim_company dcm, dim_Vendor dv,  fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdCreated <> 1;


UPDATE fact_accountspayable fap
SET Dim_DateIdCreated = dt.dim_dateid
	,dw_update_date = current_timestamp
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt, fact_accountspayable fap 
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_BLDAT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdCreated <> dt.dim_dateid;

/* Update column Dim_DateIdPosting */

UPDATE fact_accountspayable fap
SET Dim_DateIdPosting = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdPosting <> 1;


UPDATE fact_accountspayable fap
SET Dim_DateIdPosting = dt.dim_dateid
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_BUDAT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdPosting <> dt.dim_dateid;


/* Update column Dim_BlockingPaymentReasonId */

UPDATE fact_accountspayable fap
SET Dim_BlockingPaymentReasonId = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_BlockingPaymentReasonId <> 1;

/*below can crash because of unstable no of rows -- see merge*/
UPDATE fact_accountspayable fap
SET Dim_BlockingPaymentReasonId = bpr.Dim_BlockingPaymentReasonId
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_BlockingPaymentReason bpr, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND bpr.BlockingKeyPayment = BSAK_ZLSPR AND bpr.RowIsCurrent = 1
AND fap.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId;


/* Update column Dim_BusinessAreaId */

UPDATE fact_accountspayable fap
SET Dim_BusinessAreaId = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_BusinessAreaId <> 1;


UPDATE fact_accountspayable fap
SET Dim_BusinessAreaId = ba.Dim_BusinessAreaId
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_BusinessArea ba, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND ba.BusinessArea = BSAK_GSBER AND ba.RowIsCurrent = 1
AND fap.Dim_BusinessAreaId <> ba.Dim_BusinessAreaId;


/* Update column Dim_Currencyid */

UPDATE fact_accountspayable fap
SET Dim_Currencyid = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_Currencyid <> 1;


UPDATE fact_accountspayable fap
SET Dim_Currencyid = c.dim_currencyid
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_currency c, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
 AND c.CurrencyCode = dcm.Currency
AND fap.Dim_Currencyid <> c.dim_currencyid;


/* Update column Dim_DateIdClearing */

UPDATE fact_accountspayable fap
SET Dim_DateIdClearing = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdClearing <> 1;


UPDATE fact_accountspayable fap
SET Dim_DateIdClearing = dt.dim_dateid
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_AUGDT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdClearing <> dt.dim_dateid;


/* Update column Dim_DocumentTypeId */

UPDATE fact_accountspayable fap
SET Dim_DocumentTypeId = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_DocumentTypeId <> 1;


UPDATE fact_accountspayable fap
SET Dim_DocumentTypeId = dtt.dim_documenttypetextid
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_documenttypetext dtt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
 AND dtt.type = BSAK_BLART AND dtt.RowIsCurrent = 1
AND fap.Dim_DocumentTypeId <> dtt.dim_documenttypetextid;


/* Update column Dim_PaymentReasonId */

UPDATE fact_accountspayable fap
SET Dim_PaymentReasonId = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_PaymentReasonId <> 1;


UPDATE fact_accountspayable fap
SET Dim_PaymentReasonId = pr.Dim_PaymentReasonId
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_PaymentReason pr, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND pr.PaymentReasonCode = BSAK_RSTGR AND pr.CompanyCode = BSAK_BUKRS AND pr.RowIsCurrent = 1
AND fap.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId;


/* Update column Dim_PostingKeyId */

UPDATE fact_accountspayable fap
SET Dim_PostingKeyId = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_PostingKeyId <> 1;


UPDATE fact_accountspayable fap
SET Dim_PostingKeyId = pk.Dim_PostingKeyId
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_PostingKey pk, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND pk.PostingKey = BSAK_BSCHL AND pk.SpecialGLIndicator =  ifnull(BSAK_UMSKZ, 'Not Set') AND pk.RowIsCurrent = 1
AND fap.Dim_PostingKeyId <> pk.Dim_PostingKeyId;


/* Update column Dim_SpecialGLIndicatorId */

UPDATE fact_accountspayable fap
SET Dim_SpecialGLIndicatorId = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_SpecialGLIndicatorId <> 1;


UPDATE fact_accountspayable fap
SET Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_SpecialGLIndicator sgl, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND sgl.SpecialGLIndicator = BSAK_UMSKZ AND sgl.AccountType = 'D' AND sgl.RowIsCurrent = 1
AND fap.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId;


/* Update column Dim_TargetSpecialGLIndicatorId */

UPDATE fact_accountspayable fap
SET Dim_TargetSpecialGLIndicatorId = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_TargetSpecialGLIndicatorId <> 1;


UPDATE fact_accountspayable fap
SET Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_SpecialGLIndicator sgl, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND sgl.SpecialGLIndicator = BSAK_ZUMSK AND sgl.AccountType = 'D' AND sgl.RowIsCurrent = 1
AND fap.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId;


/* Update column Dim_SpecialGlTransactionTypeId */

UPDATE fact_accountspayable fap
SET Dim_SpecialGlTransactionTypeId = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND fap.Dim_SpecialGlTransactionTypeId <> 1;


UPDATE fact_accountspayable fap
SET Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
 ,Dim_SpecialGlTransactionType sgt
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND sgt.SpecialGlTransactionTypeId = BSAK_UMSKS AND sgt.RowIsCurrent = 1
AND fap.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId;

/* Update column Dim_CustomerPaymentTermsid */

UPDATE fact_accountspayable fap
SET Dim_CustomerPaymentTermsid = cpt.Dim_Termid
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_Term cpt, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
  AND cpt.TermCode = BSAK_ZTERM
AND arc.BSAK_ZTERM  IS NOT NULL;

UPDATE fact_accountspayable fap
SET Dim_CustomerPaymentTermsid = 1
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND arc.BSAK_ZTERM IS NULL;
/* Now the columns that did not have inner subquery in mysql */

UPDATE fact_accountspayable fap
SET  amt_CashDiscountDocCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_WSKTO
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
	WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
SET amt_CashDiscountLocalCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_SKNTO
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1;


 UPDATE fact_accountspayable fap
SET amt_InLocalCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_DMBTR
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
SET amt_TaxInDocCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_WMWST
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
SET amt_TaxInLocalCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_MWSTS
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
SET dd_AccountingDocItemNo = BSAK_BUZEI
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_AccountingDocItemNo  <>  BSAK_BUZEI;

 UPDATE fact_accountspayable fap
SET dd_AccountingDocNo = BSAK_BELNR
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_AccountingDocNo  <>  BSAK_BELNR;

 UPDATE fact_accountspayable fap
SET
dd_AssignmentNumber = ifnull(BSAK_ZUONR, 'Not Set')
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_AssignmentNumber  <>  ifnull(BSAK_ZUONR, 'Not Set');

 UPDATE fact_accountspayable fap
SET dd_debitcreditid = (CASE WHEN BSAK_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
SET
dd_ClearingDocumentNo = ifnull(BSAK_AUGBL, 'Not Set')
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_ClearingDocumentNo  <>  ifnull(BSAK_AUGBL, 'Not Set');

 UPDATE fact_accountspayable fap
SET
dd_FixedPaymentTerms = ifnull(BSAK_ZBFIX, 'Not Set')
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_FixedPaymentTerms  <>  ifnull(BSAK_ZBFIX, 'Not Set');

 UPDATE fact_accountspayable fap
SET
dd_InvoiceNumberTransBelongTo = ifnull(BSAK_REBZG, 'Not Set')
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_InvoiceNumberTransBelongTo  <>  ifnull(BSAK_REBZG, 'Not Set');

 UPDATE fact_accountspayable fap
SET fap.dim_companyid = dcm.Dim_CompanyId
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dim_companyid  <>  dcm.Dim_CompanyId;

 UPDATE fact_accountspayable fap
SET fap.Dim_VendorID = dv.Dim_Vendorid
	,dw_update_date = current_timestamp
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap	
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.Dim_VendorID  <>  dv.Dim_Vendorid;

 UPDATE fact_accountspayable fap
SET
dd_DocumentNo = ifnull(BSAK_EBELN, 'Not Set')
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_DocumentNo  <>  ifnull(BSAK_EBELN, 'Not Set');

 UPDATE fact_accountspayable fap
SET dd_DocumentItemNo = BSAK_EBELP
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_DocumentItemNo  <>  BSAK_EBELP;

 UPDATE fact_accountspayable fap
SET
dd_ReferenceDocumentNo = ifnull(BSAK_XBLNR, 'Not Set')
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_ReferenceDocumentNo  <>  ifnull(BSAK_XBLNR, 'Not Set');

 UPDATE fact_accountspayable fap
SET dd_CashDiscountPercentage1 = BSAK_ZBD1P
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_CashDiscountPercentage1  <>  BSAK_ZBD1P;

 UPDATE fact_accountspayable fap
SET dd_CashDiscountPercentage2 = BSAK_ZBD2P
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_CashDiscountPercentage2  <>  BSAK_ZBD2P;

 UPDATE fact_accountspayable fap
SET
dd_ProductionOrderNo = ifnull(BSAK_AUFNR, 'Not Set')
	,dw_update_date = current_timestamp 
from BSAK arc, dim_company dcm, dim_Vendor dv, fact_accountspayable fap
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
 AND dv.RowIsCurrent = 1
AND fap.dd_ProductionOrderNo  <>  ifnull(BSAK_AUFNR, 'Not Set');


/* End of Update 2 */


DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_accountspayable';

INSERT INTO NUMBER_FOUNTAIN
   select 'fact_accountspayable',ifnull(max(f.fact_accountspayableid ), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_accountspayable f;



INSERT INTO fact_accountspayable(fact_accountspayableid,
                                 amt_CashDiscountDocCurrency,
                                 amt_CashDiscountLocalCurrency,
                                 amt_InDocCurrency,
                                 amt_InLocalCurrency,
                                 amt_TaxInDocCurrency,
                                 amt_TaxInLocalCurrency,
                                 dd_AccountingDocItemNo,
                                 dd_AccountingDocNo,
                                 dd_AssignmentNumber,
                                 Dim_ClearedFlagId,
                                 Dim_DateIdAccDocDateEntered,
                                 Dim_DateIdBaseDateForDueDateCalc,
                                 Dim_DateIdCreated,
                                 Dim_DateIdPosting,
                                 dd_debitcreditid,
                                 dd_ClearingDocumentNo,
                                 dd_FiscalPeriod,
                                 dd_FiscalYear,
                                 dd_FixedPaymentTerms,
                                 dd_InvoiceNumberTransBelongTo,
                                 Dim_BlockingPaymentReasonId,
                                 Dim_BusinessAreaId,
                                 Dim_CompanyId,
                                 Dim_CurrencyId,
                                 Dim_VendorId,
                                 Dim_DateIdClearing,
                                 Dim_DocumentTypeId,
                                 Dim_PaymentReasonId,
                                 Dim_PostingKeyId,
                                 Dim_SpecialGLIndicatorId,
                                 Dim_TargetSpecialGLIndicatorId,
                                 dd_DocumentNo,
                                 dd_DocumentItemNo,
                                 dd_ReferenceDocumentNo,
                                 dd_CashDiscountPercentage1,
                                 dd_CashDiscountPercentage2,
                                 dd_ProductionOrderNo,
                                 Dim_SpecialGlTransactionTypeId,
								 Dim_CustomerPaymentTermsid,
								 dim_Currencyid_TRA,
								 dim_Currencyid_GBL,
								 amt_exchangerate,
								 amt_exchangerate_GBL)
SELECT ((SELECT max_id
           FROM NUMBER_FOUNTAIN
          WHERE table_name = 'fact_accountspayable')
        + row_number() over (ORDER BY '')) as fact_accountspayableid,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_WSKTO amt_CashDiscountDocCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_SKNTO amt_CashDiscountLocalCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_WRBTR amt_InDocCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_DMBTR amt_InLocalCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_WMWST amt_TaxInDocCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_MWSTS amt_TaxInLocalCurrency,
 BSAK_BUZEI dd_AccountingDocItemNo,
 BSAK_BELNR dd_AccountingDocNo,
 ifnull(BSAK_ZUONR, 'Not Set') dd_AssignmentNumber,
1  Dim_ClearedFlagId,
1 Dim_DateIdAccDocDateEntered,
 1 Dim_DateIdBaseDateForDueDateCalc,
 1 Dim_DateIdCreated,
 1 Dim_DateIdPosting,
 (CASE WHEN BSAK_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END) dd_debitcreditid,
 ifnull(BSAK_AUGBL,'Not Set') dd_ClearingDocumentNo,
 BSAK_MONAT dd_FiscalPeriod,
 BSAK_GJAHR dd_FiscalYear,
 ifnull(BSAK_ZBFIX, 'Not Set') dd_FixedPaymentTerms,
 ifnull(BSAK_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
 1 Dim_BlockingPaymentReasonId,
 1 Dim_BusinessAreaId,
 dc.Dim_CompanyId,
 1 Dim_Currencyid,
 dv.Dim_VendorId,
 1 Dim_DateIdClearing,
 1 Dim_DocumentTypeId,
 1 Dim_PaymentReasonId,
 1 Dim_PostingKeyId,
 1 Dim_SpecialGLIndicatorId,
 1 Dim_TargetSpecialGLIndicatorId,
 ifnull(BSAK_EBELN, 'Not Set') dd_DocumentNo,
 BSAK_EBELP dd_DocumentItemNo,
 ifnull(BSAK_XBLNR,'Not Set') dd_ReferenceDocumentNo,
 BSAK_ZBD1P dd_CashDiscountPercentage1,
 BSAK_ZBD2P dd_CashDiscountPercentage2,
 ifnull(BSAK_AUFNR,'Not Set') dd_ProductionOrderNo,
 1 Dim_SpecialGlTransactionTypeId,
 1 Dim_CustomerPaymentTermsid,
 1 Dim_Currencyid_TRA,
 1 Dim_Currencyid_GBL,
 1 amt_exchangerate,
 1 amt_exchangerate_GBL
 FROM BSAK arc
 INNER JOIN dim_company dc ON dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 INNER JOIN dim_Vendor dv ON dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 WHERE NOT EXISTS
 (SELECT 1
 FROM fact_accountspayable ar
 WHERE ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 AND ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId);
 
 
 
 UPDATE fact_accountspayable
SET Dim_ClearedFlagId = ars.Dim_AccountPayableStatusId 
FROM Dim_AccountPayableStatus ars, 
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE ars.Status = CASE WHEN BSAK_REBZG IS NOT NULL AND BSAK_REBZJ <> 0 THEN 'F - Partial' ELSE 'C - Cleared' END
 AND ars.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_ClearedFlagId <> ars.Dim_AccountPayableStatusId 
;


UPDATE fact_accountspayable
SET Dim_DateIdAccDocDateEntered = dim_dateid  
FROM dim_date dt, 
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE dt.DateValue = BSAK_CPUDT
 AND dt.CompanyCode = BSAK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_DateIdAccDocDateEntered <> dim_dateid 
;


UPDATE fact_accountspayable
SET Dim_DateIdBaseDateForDueDateCalc = dim_dateid  
FROM dim_date dt, 
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE dt.DateValue = BSAK_ZFBDT
 AND dt.CompanyCode = BSAK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_DateIdBaseDateForDueDateCalc <> dim_dateid 
;


UPDATE fact_accountspayable
SET Dim_DateIdCreated = dim_dateid  
FROM dim_date dt, 
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE dt.DateValue = BSAK_BLDAT
 AND dt.CompanyCode = BSAK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_DateIdCreated <> dim_dateid 
;


UPDATE fact_accountspayable
SET Dim_DateIdPosting = dim_dateid  
FROM dim_date dt, 
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE dt.DateValue = BSAK_BUDAT
 AND dt.CompanyCode = BSAK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and Dim_DateIdPosting <> dim_dateid 
;


UPDATE fact_accountspayable
SET Dim_BlockingPaymentReasonId = Dim_BlockingPaymentReasonId  
FROM Dim_BlockingPaymentReason bpr,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE  bpr.BlockingKeyPayment = BSAK_ZLSPR
 AND bpr.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId 
;


UPDATE fact_accountspayable
SET Dim_BusinessAreaId = Dim_BusinessAreaId  
FROM Dim_BusinessArea ba,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE  ba.BusinessArea = BSAK_GSBER
 AND ba.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_BusinessAreaId <> ba.Dim_BusinessAreaId  
;


UPDATE fact_accountspayable
SET dim_currencyid = dim_currencyid  
FROM dim_currency c,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE c.CurrencyCode = dc.Currency
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.dim_currencyid <> c.dim_currencyid  
;


UPDATE fact_accountspayable
SET Dim_DateIdClearing = dt.dim_dateid  
FROM dim_date dt,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE dt.DateValue = BSAK_AUGDT
 AND dt.CompanyCode = BSAK_BUKRS
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_DateIdClearing <> dt.dim_dateid   
;


UPDATE fact_accountspayable
SET Dim_DocumentTypeId = dtt.dim_documenttypetextid  
FROM dim_documenttypetext dtt,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE  dtt.type = BSAK_BLART
 AND dtt.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_DocumentTypeId <> dtt.dim_documenttypetextid  
;


UPDATE fact_accountspayable
SET Dim_PaymentReasonId = pr.Dim_PaymentReasonId  
FROM Dim_PaymentReason pr,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE   pr.PaymentReasonCode = BSAK_RSTGR
 AND pr.CompanyCode = BSAK_BUKRS
 AND pr.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId 
;


UPDATE fact_accountspayable
SET Dim_PostingKeyId = pk.Dim_PostingKeyId  
FROM Dim_PostingKey pk,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE   pk.PostingKey= BSAK_BSCHL
 AND pk.SpecialGLIndicator = ifnull(BSAK_UMSKZ,'Not Set')
 AND pk.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_PostingKeyId <> pk.Dim_PostingKeyId 
;



UPDATE fact_accountspayable
SET Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId  
FROM Dim_SpecialGLIndicator sgl,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE   sgl.SpecialGLIndicator = BSAK_UMSKZ
 AND sgl.AccountType = 'D'
 AND sgl.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId 
;



UPDATE fact_accountspayable
SET Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId  
FROM Dim_SpecialGLIndicator sgl,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE   sgl.SpecialGLIndicator = BSAK_ZUMSK
 AND sgl.AccountType = 'D'
 AND sgl.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId  
;


UPDATE fact_accountspayable
SET Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId  
FROM Dim_SpecialGlTransactionType sgt,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE   sgt.SpecialGlTransactionTypeId = BSAK_UMSKS
 AND sgt.RowIsCurrent = 1
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId  
;


UPDATE fact_accountspayable
SET Dim_CustomerPaymentTermsid = cpt.Dim_Termid  
FROM Dim_Term cpt,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE   cpt.TermCode = BSAK_ZTERM
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_CustomerPaymentTermsid <> cpt.Dim_Termid  
;

UPDATE fact_accountspayable
SET Dim_Currencyid_TRA = c.dim_currencyid  
FROM dim_currency c,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE   c.CurrencyCode = bsak_waers
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_Currencyid_TRA <> c.dim_currencyid  
;


UPDATE fact_accountspayable
SET Dim_Currencyid_GBL = c.dim_currencyid  
FROM dim_currency c,tmp_gblcurr_fap t,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE   c.CurrencyCode = t.pGlobalCurrency
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and ar.Dim_Currencyid_GBL <> c.dim_currencyid  
;

UPDATE fact_accountspayable
SET amt_exchangerate = z.exchangeRate 
FROM tmp_getExchangeRate1 z,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE  z.pFromCurrency  = BSAK_WAERS and z.fact_script_name = 'bi_populate_accountspayable_fact'
                          and z.pToCurrency = dc.currency AND z.pDate = BSAK_BUDAT
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and amt_exchangerate  <> z.exchangeRate 
;



UPDATE fact_accountspayable
SET amt_exchangerate_GBL = z.exchangeRate 
FROM tmp_getExchangeRate1 z,tmp_gblcurr_fap,
BSAK arc, fact_accountspayable ar,dim_company dc, dim_Vendor dv
WHERE  z.pFromCurrency  = BSAK_WAERS and z.fact_script_name = 'bi_populate_accountspayable_fact'
      and z.pToCurrency = pGlobalCurrency AND z.pDate = BSAK_BUDAT
 and ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 and dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 and dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 and ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId
 and amt_exchangerate_GBL  <> z.exchangeRate 
;



/* Split Update3 into 2 queries : (Dim_NetDueDateId1,Dim_NetDueDateId2,Dim_NetDueDateId3 in the first query,
   Dim_NetDueDateWrtCashDiscountTerms1 and Dim_NetDueDateWrtCashDiscountTerms2 in the 2nd query */


/* Update3 - Part 1 */


/* Update Dim_NetDueDateId1 */

UPDATE fact_accountspayable fap
SET Dim_NetDueDateId1 = 1
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId1 <> 1;

UPDATE fact_accountspayable fap
SET Dim_NetDueDateId1 = dt.DIM_DATEID
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv,
DIM_DATE dt
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND dt.CompanyCode = arc.BSIK_BUKRS 
AND dt.DateValue = cast((ifnull(BSIK_ZFBDT, BSIK_BLDAT)+ (INTERVAL '1' DAY) * ifnull(BSIK_ZBD1T, 0)) AS DATE)
AND Dim_NetDueDateId1 <> dt.DIM_DATEID;

/* Update Dim_NetDueDateId2 */
UPDATE fact_accountspayable  fap
SET Dim_NetDueDateId2 = 1
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId2 <> 1;

UPDATE fact_accountspayable  fap
SET Dim_NetDueDateId1 = dt.DIM_DATEID
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv,
DIM_DATE dt
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId1 <> 1
AND dt.CompanyCode = arc.BSIK_BUKRS 
AND dt.DateValue = cast((ifnull(BSIK_ZFBDT, BSIK_BLDAT)+ (INTERVAL '1' DAY) * ifnull(BSIK_ZBD2T, 0)) AS DATE)
AND Dim_NetDueDateId1 <> dt.DIM_DATEID;

/* Update Dim_NetDueDateId3 */
UPDATE fact_accountspayable fap
SET Dim_NetDueDateId3 = 1
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId3 <> 1;

UPDATE fact_accountspayable  fap
SET Dim_NetDueDateId1 = dt.DIM_DATEID
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv,
DIM_DATE dt
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId1 <> 1
AND dt.CompanyCode = arc.BSIK_BUKRS 
AND dt.DateValue = cast((ifnull(BSIK_ZFBDT, BSIK_BLDAT)+ (INTERVAL '1' DAY) * ifnull(BSIK_ZBD3T, 0)) AS DATE)
AND Dim_NetDueDateId1 <> dt.DIM_DATEID;


/* Update3 - Part 2a */

UPDATE fact_accountspayable fap
   SET Dim_NetDueDateWrtCashDiscountTerms1 =
          ifnull((CASE BSIK_SHKZG WHEN 'S' THEN Dim_NetDueDateId1 END), 1),
       Dim_NetDueDateWrtCashDiscountTerms2 =
          ifnull((CASE BSIK_SHKZG WHEN 'S' THEN Dim_NetDueDateId1 END), 1)
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv
 WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
       AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSIK_GJAHR
       AND dd_FiscalPeriod = arc.BSIK_MONAT
       AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1;


/* Update3 - Part 2b */

UPDATE fact_accountspayable fap
   SET Dim_NetDueDateWrtCashDiscountTerms1 = DIM_DATEID
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv, DIM_DATE dt
       WHERE dd_AccountingDocNo = arc.BSIK_BELNR
       AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSIK_GJAHR
       AND dd_FiscalPeriod = arc.BSIK_MONAT
       AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND BSIK_ZBD1T IS NOT NULL
       AND dt.CompanyCode = arc.BSIK_BUKRS
       AND Dim_NetDueDateWrtCashDiscountTerms1 <> DIM_DATEID
       AND dt.DateValue = CAST((BSIK_ZFBDT + (INTERVAL '1' DAY) * ifnull(BSIK_ZBD1T, 0)) AS DATE);

/* Update3 - Part 2c */

UPDATE fact_accountspayable fap
   SET Dim_NetDueDateWrtCashDiscountTerms2 = DIM_DATEID
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv, DIM_DATE dt
	WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
       AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSIK_GJAHR
       AND dd_FiscalPeriod = arc.BSIK_MONAT
       AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND BSIK_ZBD2T IS NOT NULL
       AND Dim_NetDueDateWrtCashDiscountTerms2 <> DIM_DATEID
       AND dt.CompanyCode = arc.BSIK_BUKRS
       AND dt.DateValue = CAST((BSIK_ZFBDT + (INTERVAL '1' DAY) * ifnull(BSIK_ZBD2T, 0)) AS DATE);

/* Update 4 */

DROP TABLE IF EXISTS TMP_FP_BSAK;
CREATE TABLE TMP_FP_BSAK
AS
SELECT BSAK.*,
ifnull(BSAK_ZFBDT, BSAK_BLDAT) + ifnull(BSAK_ZBD1T, 0) as bsak_datevalue1,
ifnull(BSAK_ZFBDT, BSAK_BLDAT) + ifnull(BSAK_ZBD2T, 0) as bsak_datevalue2,
ifnull(BSAK_ZFBDT, BSAK_BLDAT) + ifnull(BSAK_ZBD3T, 0) as bsak_datevalue3,
BSAK_ZFBDT + ifnull(BSAK_ZBD1T, 0) as CDT1_datevalue,
BSAK_ZFBDT + ifnull(BSAK_ZBD2T, 0) as CDT2_datevalue 
FROM BSAK;


/* Update 4 - part 1 */

UPDATE fact_accountspayable fap
   SET Dim_NetDueDateId1 = DIM_DATEID
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv, DIM_DATE dt
   WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND  dt.CompanyCode = arc.BSAK_BUKRS
       AND Dim_NetDueDateId1 <> DIM_DATEID
       AND dt.DateValue  = bsak_datevalue1;

UPDATE fact_accountspayable fap
       SET Dim_NetDueDateId2 = DIM_DATEID   
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv, DIM_DATE dt
 WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND dt.CompanyCode = arc.BSAK_BUKRS
       AND Dim_NetDueDateId2 <> DIM_DATEID   
       AND dt.DateValue  = bsak_datevalue2;

UPDATE fact_accountspayable fap
       SET Dim_NetDueDateId3 = DIM_DATEID
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv, DIM_DATE dt
            WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND dt.CompanyCode = arc.BSAK_BUKRS
       AND Dim_NetDueDateId3 <> DIM_DATEID
       AND dt.DateValue  = bsak_datevalue3;

/* Update 4 - part 2a */


UPDATE fact_accountspayable fap
   SET Dim_NetDueDateWrtCashDiscountTerms1 =
          ifnull((CASE BSAK_SHKZG WHEN 'S' THEN Dim_NetDueDateId1 END), 1)
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSAK arc, dim_Company dcm, dim_vendor dv
 WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1;

UPDATE fact_accountspayable fap 
   SET 
       Dim_NetDueDateWrtCashDiscountTerms2 =
          ifnull((CASE BSAK_SHKZG WHEN 'S' THEN Dim_NetDueDateId1 END), 1)
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSAK arc, dim_Company dcm, dim_vendor dv
 WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1;


/* Update 4 - part 2b */


UPDATE fact_accountspayable fap 
   SET Dim_NetDueDateWrtCashDiscountTerms1 =
          ifnull( dt.DIM_DATEID, 1)	
	  ,dw_update_date = current_timestamp 
from fact_accountspayable fap, TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv,DIM_DATE dt
 WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND BSAK_ZBD1T IS NOT NULL
       and dt.DateValue  = CDT1_datevalue
       AND dt.CompanyCode = arc.BSAK_BUKRS
       and Dim_NetDueDateWrtCashDiscountTerms1 <> ifnull( dt.DIM_DATEID, 1)
       ;


/* Update 4 - part 2c */

UPDATE fact_accountspayable fap 
   SET Dim_NetDueDateWrtCashDiscountTerms2 = DIM_DATEID     
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv, DIM_DATE dt
 WHERE dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND BSAK_ZBD2T IS NOT NULL
       AND dt.DateValue = CDT2_datevalue
       AND dt.CompanyCode = arc.BSAK_BUKRS
       AND Dim_NetDueDateWrtCashDiscountTerms2 <> DIM_DATEID;
	   
	   
/* Populate the exchange rates for BSIK data */

/* Update 5  */
UPDATE fact_accountspayable fap 
   SET amt_EligibleForDiscount = BSIK_SKFBT /** amt_ExchangeRate*/
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSIK arc, dim_Company dcm, dim_Vendor dv
 WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSIK_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
       AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSIK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND amt_EligibleForDiscount <> (BSIK_SKFBT );


/* Update 6  */

UPDATE fact_accountspayable fap 
   SET amt_EligibleForDiscount = BSAK_SKFBT /** amt_ExchangeRate*/
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, BSAK arc, dim_Company dcm, dim_Vendor dv
 WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSAK_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.dim_companyid = fap.dim_companyid
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR AND dv.dim_vendorid = fap.dim_vendorid
       AND dv.RowIsCurrent = 1
       AND amt_EligibleForDiscount <> (BSAK_SKFBT );

/* Update 7  */

DROP TABLE IF EXISTS tmp1_fact_accountspayable_MBEW;

CREATE TABLE tmp1_fact_accountspayable_MBEW
AS
   SELECT MATNR, BWKEY, MAX(MBEW_ZPLD1) max_MBEW_ZPLD1
     FROM MBEW
    WHERE MBEW_LBKUM > 0 AND BWTAR IS NULL
   GROUP BY MATNR, BWKEY;

DROP TABLE IF EXISTS tmp2_fact_accountspayable_MBEW;

CREATE TABLE tmp2_fact_accountspayable_MBEW
AS
   SELECT w.*
     FROM MBEW w, tmp1_fact_accountspayable_MBEW t
    WHERE     w.MATNR = t.MATNR
          AND w.BWKEY = t.BWKEY
          AND w.MBEW_ZPLD1 = t.max_MBEW_ZPLD1;



/* Update column fap.Dim_PartId */

UPDATE fact_accountspayable fap
SET fap.Dim_PartId = 1
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.Dim_PartId <> 1;


UPDATE fact_accountspayable fap
SET fap.Dim_PartId = p.dim_partid
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
 ,dim_part p
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
  AND p.Partnumber = rsg.RSEG_MATNR AND p.Plant = rsg.RSEG_WERKS AND p.RowIsCurrent = 1
AND fap.Dim_PartId <> p.dim_partid;


/* Update column fap.Dim_UnitOfMeasureId */

UPDATE fact_accountspayable fap
SET fap.Dim_UnitOfMeasureId = 1
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.Dim_UnitOfMeasureId <> 1;


UPDATE fact_accountspayable fap
SET fap.Dim_UnitOfMeasureId = uom.dim_unitofmeasureid
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
 ,dim_unitofmeasure uom
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
 AND uom.UOM = rsg.RSEG_BSTME AND uom.RowIsCurrent = 1
AND fap.Dim_UnitOfMeasureId <> uom.dim_unitofmeasureid;


/* Update column fap.amt_InvStdUnitPrice */

UPDATE fact_accountspayable fap
SET fap.amt_InvStdUnitPrice = 0
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.amt_InvStdUnitPrice <> 0;


UPDATE fact_accountspayable fap
SET fap.amt_InvStdUnitPrice = w.STPRS
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
 , tmp2_fact_accountspayable_MBEW w, dim_plant pl
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
  AND pl.PlantCode = rsg.RSEG_WERKS AND pl.RowIsCurrent = 1 AND w.MATNR = rsg.RSEG_MATNR AND w.BWKEY = pl.ValuationArea
AND fap.amt_InvStdUnitPrice <> w.STPRS;

/* Now update columns that did not have inner subquery */

UPDATE fact_accountspayable fap
SET     fap.dd_PurchaseDocumentNo = rsg.RSEG_EBELN
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND     fap.dd_PurchaseDocumentNo  <>  rsg.RSEG_EBELN;

UPDATE fact_accountspayable fap
SET fap.dd_PurchaseDocItemNo = rsg.RSEG_EBELP
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.dd_PurchaseDocItemNo  <>  rsg.RSEG_EBELP;

UPDATE fact_accountspayable fap
SET fap.amt_InvoiceInDocCurrency = rsg.RSEG_WRBTR
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.amt_InvoiceInDocCurrency  <>  rsg.RSEG_WRBTR;

UPDATE fact_accountspayable fap
SET fap.ct_InvoiceQty = rsg.RSEG_MENGE
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.ct_InvoiceQty  <>  rsg.RSEG_MENGE;

UPDATE fact_accountspayable fap
SET fap.amt_InvPOUnitPrice = (CASE WHEN rsg.RSEG_BPMNG <> 0 THEN rsg.RSEG_WRBTR / rsg.RSEG_BPMNG ELSE 0 END)
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, rseg rsg, dim_date dt
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid;



/* End of Update 7 */

/* Update 8 */

/* Update column Dim_MovementTypeid */

UPDATE fact_accountspayable fap
SET Dim_MovementTypeid = 1
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, ekbe et, dim_plant pl
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
AND  fap.dd_PurchaseDocItemNo <> 0
AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
AND  et.EKBE_WERKS = pl.PlantCode
AND  pl.RowIsCurrent = 1
AND Dim_MovementTypeid <> 1;


UPDATE fact_accountspayable fap
SET Dim_MovementTypeid = mt.Dim_MovementTypeid
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, ekbe et, dim_plant pl
 ,dim_movementtype mt
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
AND  fap.dd_PurchaseDocItemNo <> 0
AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
AND  et.EKBE_WERKS = pl.PlantCode
AND  pl.RowIsCurrent = 1
  AND mt.MovementType = et.EKBE_BWART AND mt.ConsumptionIndicator = 'Not Set' 
  AND mt.MovementIndicator = 'Not Set' AND mt.ReceiptIndicator = 'Not Set' 
  AND mt.SpecialStockIndicator = 'Not Set' AND mt.RowIsCurrent = 1
AND fap.Dim_MovementTypeid <> mt.Dim_MovementTypeid;


/* Update column fap.Dim_PoPartId */

UPDATE fact_accountspayable fap
SET fap.Dim_PoPartId = 1
	,dw_update_date = current_timestamp 
from fact_accountspayable fap, ekbe et, dim_plant pl
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
AND  fap.dd_PurchaseDocItemNo <> 0
AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
AND  et.EKBE_WERKS = pl.PlantCode
AND  pl.RowIsCurrent = 1
AND fap.Dim_PoPartId <> 1;


UPDATE fact_accountspayable fap
SET fap.Dim_PoPartId = pt.Dim_Partid
from fact_accountspayable fap, ekbe et, dim_plant pl
 ,dim_part pt
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
AND  fap.dd_PurchaseDocItemNo <> 0
AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
AND  et.EKBE_WERKS = pl.PlantCode
AND  pl.RowIsCurrent = 1
  AND pt.PartNumber = et.EKBE_MATNR AND pt.Plant = et.EKBE_WERKS AND pt.RowIsCurrent = 1
AND fap.Dim_PoPartId <> pt.Dim_Partid;

UPDATE fact_accountspayable fap
SET  fap.Dim_PoPlantId = pl.Dim_PlantId
from fact_accountspayable fap, ekbe et, dim_plant pl
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
 AND  fap.dd_PurchaseDocItemNo <> 0
 AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
 AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
 AND  et.EKBE_WERKS = pl.PlantCode
 AND  pl.RowIsCurrent = 1
AND  fap.Dim_PoPlantId  <>  pl.Dim_PlantId;

UPDATE fact_accountspayable fap
SET fap.ct_PoQuantity = et.EKBE_MENGE
from fact_accountspayable fap, ekbe et, dim_plant pl
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
 AND  fap.dd_PurchaseDocItemNo <> 0
 AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
 AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
 AND  et.EKBE_WERKS = pl.PlantCode
 AND  pl.RowIsCurrent = 1
AND fap.ct_PoQuantity  <>  et.EKBE_MENGE;

UPDATE fact_accountspayable fap
SET fap.amt_PoAmtInDocCurrency = et.EKBE_REEWR
from fact_accountspayable fap, ekbe et, dim_plant pl
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
 AND  fap.dd_PurchaseDocItemNo <> 0
 AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
 AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
 AND  et.EKBE_WERKS = pl.PlantCode
 AND  pl.RowIsCurrent = 1
AND fap.amt_PoAmtInDocCurrency  <>  et.EKBE_REEWR;

UPDATE fact_accountspayable fap
SET
fap.dd_CreatedBy = ifnull(et.EKBE_ERNAM, 'Not Set')
from fact_accountspayable fap, ekbe et, dim_plant pl
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
 AND  fap.dd_PurchaseDocItemNo <> 0
 AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
 AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
 AND  et.EKBE_WERKS = pl.PlantCode
 AND  pl.RowIsCurrent = 1
AND fap.dd_CreatedBy  <>  ifnull(et.EKBE_ERNAM, 'Not Set');

/* End of Update 8 */


/* Update 9  */

/* Update 9  */

merge into fact_accountspayable fap
using 
(SELECT AVG(amt_StdUnitPrice) avgamt,dd_DocumentNo,dd_DocumentItemNo
             FROM fact_purchase           
           GROUP BY dd_DocumentNo, dd_DocumentItemNo) p
     on (fap.dd_PurchaseDocumentNo = p.dd_DocumentNo
                  AND fap.dd_PurchaseDocItemNo = p.dd_DocumentItemNo )
     when matched then update 
     set fap.amt_StdPOUnitPrice = ifnull(p.avgamt,0)
        where fap.dd_PurchaseDocumentNo <> 'Not Set'
              AND fap.dd_PurchaseDocItemNo <> 0;

/* Update 10  */
DROP TABLE IF EXISTS tmp_BSIK;
CREATE TABLE tmp_BSIK as 
select fact_accountspayableid,max(apmisc.Dim_APMiscellaneousId) Dim_APMiscellaneousId from fact_accountspayable fap, Dim_APMiscellaneous apmisc, BSIK bik
 WHERE     fap.dd_AccountingDocNo = bik.BSIK_BELNR
       AND fap.dd_AccountingDocItemNo = bik.BSIK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(bik.BSIK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = bik.BSIK_GJAHR
       AND fap.dd_FiscalPeriod = bik.BSIK_MONAT
       AND apmisc.ItemsClearingReversed = ifnull(bik.BSIK_XRAGL, 'Not Set')
       AND apmisc.DocumentPostedYet = ifnull(bik.BSIK_XNETB, 'Not Set')
       AND fap.Dim_APMiscellaneousId <> apmisc.Dim_APMiscellaneousId
GROUP BY fact_accountspayableid;

UPDATE fact_accountspayable fap 
   SET fap.Dim_APMiscellaneousId = bik.Dim_APMiscellaneousId
  from fact_accountspayable fap, tmp_BSIK bik
 WHERE     fap.fact_accountspayableid = bik.fact_accountspayableid;


/* Update 11  */

DROP TABLE IF EXISTS BSAK_for_upd;
CREATE TABLE BSAK_for_upd 
AS
SELECT fact_accountspayableid,max(apmisc.Dim_APMiscellaneousId) Dim_APMiscellaneousId
FROM fact_accountspayable fap,
Dim_APMiscellaneous apmisc, BSAK bak
 WHERE     fap.dd_AccountingDocNo = bak.BSAK_BELNR
       AND fap.dd_AccountingDocItemNo = bak.BSAK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(bak.BSAK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = bak.BSAK_GJAHR
       AND fap.dd_FiscalPeriod = bak.BSAK_MONAT
       AND apmisc.ItemsClearingReversed = ifnull(bak.BSAK_XRAGL, 'Not Set')
       AND apmisc.DocumentPostedYet = ifnull(bak.BSAK_XNETB, 'Not Set')
       AND fap.Dim_APMiscellaneousId <> apmisc.Dim_APMiscellaneousId
GROUP BY fact_accountspayableid;

UPDATE fact_accountspayable fap 
   SET fap.Dim_APMiscellaneousId = bak.Dim_APMiscellaneousId
   	,dw_update_date = current_timestamp 
from BSAK_for_upd bak, fact_accountspayable fap 
 WHERE     fap.fact_accountspayableid = bak.fact_accountspayableid;

/* Exchange rates : Posting dates are used for both global and local rate */
/* Populate the exchange rates for BSIK data */
/* Local exchange rate */
		
MERGE INTO fact_accountspayable fap 
using (select fact_accountspayableid, max(z.exchangeRate) exchangeRate
	from BSIK arc, dim_Company dcm, tmp_getexchangerate1 z ,fact_accountspayable fap
	 WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
		   AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
		   AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
		   AND fap.dd_fiscalyear = arc.BSIK_GJAHR
		   AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
		   AND dcm.CompanyCode = arc.BSIK_BUKRS AND dcm.RowIsCurrent = 1
			AND z.pFromCurrency  = BSIK_WAERS and z.fact_script_name = 'bi_populate_accountspayable_fact'
			AND z.pToCurrency = dcm.currency AND z.pDate = BSIK_BUDAT
			AND fap.amt_ExchangeRate <> z.exchangeRate
	group by fact_accountspayableid) src on fap.fact_accountspayableid = src.fact_accountspayableid
when matched then update set fap.amt_ExchangeRate = src.exchangeRate;

/* Global rate */

drop table if exists tmp_globalrate_ap;
create table tmp_globalrate_ap
as
select fact_accountspayableid, max(z.exchangeRate) exchangeRate
from BSIK arc, tmp_getexchangerate1 z, tmp_gblcurr_fap,fact_accountspayable fap
 WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSIK_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
        AND z.pFromCurrency  = BSIK_WAERS and z.fact_script_name = 'bi_populate_accountspayable_fact'
        AND z.pToCurrency = pGlobalCurrency AND z.pDate = BSIK_BUDAT
        AND fap.amt_ExchangeRate_GBL <> z.exchangeRate
group by fact_accountspayableid;

UPDATE fact_accountspayable fap 
   SET amt_ExchangeRate_gbl =  z.exchangeRate
 from tmp_globalrate_ap z,fact_accountspayable fap
 WHERE     fap.fact_accountspayableid = z.fact_accountspayableid
        AND fap.amt_ExchangeRate_GBL <> z.exchangeRate;
		
drop table if exists tmp_globalrate_ap;

/* Populate the exchange rates for BSAK data*/

UPDATE fact_accountspayable fap 
   SET amt_ExchangeRate =  z.exchangeRate
 from BSAK arc, dim_Company dcm, tmp_getexchangerate1 z,fact_accountspayable fap
 WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSAK_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.RowIsCurrent = 1
	   and dcm.dim_companyid = fap.dim_companyid
        AND z.pFromCurrency  = BSAK_WAERS and z.fact_script_name = 'bi_populate_accountspayable_fact'
        AND z.pToCurrency = dcm.currency AND z.pDate = BSAK_BUDAT
        AND fap.amt_ExchangeRate <> z.exchangeRate;

UPDATE fact_accountspayable fap 
   SET amt_ExchangeRate_gbl =  z.exchangeRate
 from BSAK arc, tmp_getexchangerate1 z, tmp_gblcurr_fap,dim_Company dcm,fact_accountspayable fap
 WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSAK_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS AND dcm.RowIsCurrent = 1
       and dcm.dim_companyid = fap.dim_companyid
        AND z.pFromCurrency  = BSAK_WAERS and z.fact_script_name = 'bi_populate_accountspayable_fact'
        AND z.pToCurrency = pGlobalCurrency AND z.pDate = BSAK_BUDAT
        AND fap.amt_ExchangeRate_gbl <> z.exchangeRate;

DROP TABLE IF EXISTS tmp_fact_accountspayable_MBEW;
DROP TABLE IF EXISTS tmp2_fact_accountspayable_MBEW;
drop table if exists BSAK_for_upd;
DROP TABLE IF EXISTS TMP_FP_BSAK;
