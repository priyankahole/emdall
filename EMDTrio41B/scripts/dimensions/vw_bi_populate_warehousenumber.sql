UPDATE    dim_warehousenumber w
      SET w.WarehouseName = t.T300T_LNUMT,
			w.dw_update_date = current_timestamp
			FROM
          dim_warehousenumber w,T300T t
 WHERE w.RowIsCurrent = 1
     AND w.WarehouseCode = t.T300T_LGNUM 
;

INSERT INTO dim_warehousenumber(dim_Warehouseid, dim_warehousenumberid, RowIsCurrent,rowstartdate,warehousename,warehousecode)
SELECT 1, 1, 1,current_timestamp,'Not Set','Not Set'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_warehousenumber
               WHERE dim_warehousenumberid = 1);

delete from number_fountain m where m.table_name = 'dim_warehousenumber';

insert into number_fountain
select 	'dim_warehousenumber',
	ifnull(max(d.dim_Warehouseid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_warehousenumber d
where d.dim_Warehouseid <> 1;

INSERT INTO dim_warehousenumber(dim_Warehouseid,
                                         WarehouseCode,
                                          WarehouseName,
                                          RowStartDate,
                                          RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_warehousenumber') 
          + row_number() over(order by ''),
			  t.T300T_LGNUM,
          t.T300T_LNUMT,
          current_timestamp,
          1
     FROM T300T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_warehousenumber w
               WHERE w.WarehouseCode = t.T300T_LGNUM AND w.RowIsCurrent = 1)
;

