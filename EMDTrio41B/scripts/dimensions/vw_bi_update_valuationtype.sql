UPDATE  dim_ValuationType vt 
   SET AccountCategory = ifnull(T149D_KKREF, 'Not Set')
 FROM dim_ValuationType vt,
      T149D t
 WHERE vt.RowIsCurrent = 1
 AND t.T149D_BWTAR = vt.ValuationType;
