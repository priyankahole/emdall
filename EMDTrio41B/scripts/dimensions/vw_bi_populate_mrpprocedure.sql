UPDATE    dim_MRPProcedure mrpp
   SET mrpp.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			mrpp.dw_update_date = current_timestamp
FROM  DD07T t, dim_MRPProcedure mrpp
 WHERE mrpp.RowIsCurrent = 1
       AND t.DD07T_DOMNAME = 'DISVF'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND mrpp.MRPProcedure = t.DD07T_DOMVALUE 
;

INSERT INTO dim_MRPProcedure(dim_MRPProcedureId, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MRPProcedure
               WHERE dim_MRPProcedureId = 1);

delete from number_fountain m where m.table_name = 'dim_MRPProcedure';
   
insert into number_fountain
select 	'dim_MRPProcedure',
	ifnull(max(d.Dim_MRPProcedureID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MRPProcedure d
where d.Dim_MRPProcedureID <> 1; 

INSERT INTO dim_MRPProcedure(Dim_MRPProcedureID,
                                Description,
                                MRPProcedure,
                                RowStartDate,
                                RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MRPProcedure') 
          + row_number() over(order by '') ,
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'DISVF' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_MRPProcedure
                    WHERE MRPProcedure = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'DISVF')
   ORDER BY 2 ;
