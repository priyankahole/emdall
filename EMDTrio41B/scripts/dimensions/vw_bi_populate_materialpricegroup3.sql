UPDATE 
dim_MaterialPriceGroup3 a 
SET a.Description = ifnull(TVM3T_BEZEI, 'Not Set'),
    a.dw_update_date = current_timestamp
FROM 
dim_MaterialPriceGroup3 a,
TVM3T
WHERE a.MaterialPriceGroup3 = TVM3T_MVGR3 AND RowIsCurrent = 1;
 
INSERT INTO dim_materialpricegroup3(dim_materialpricegroup3Id,MaterialPriceGroup3,Description, RowIsCurrent, RowStartDate)
SELECT 1, 'Not Set', 'Not Set', 1, current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_materialpricegroup3
               WHERE dim_materialpricegroup3Id = 1);

delete from number_fountain m where m.table_name = 'dim_MaterialPriceGroup3';
   
insert into number_fountain
select 	'dim_MaterialPriceGroup3',
	ifnull(max(d.dim_materialpricegroup3Id), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MaterialPriceGroup3 d
where d.dim_materialpricegroup3Id <> 1; 
			   
INSERT INTO dim_MaterialPriceGroup3(dim_materialpricegroup3Id,
                                    MaterialPriceGroup3,
                                    Description,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MaterialPriceGroup3')
          + row_number() over(order by '') ,
                         t.TVM3T_MVGR3,
          ifnull(TVM3T_BEZEI, 'Not Set'),
          current_timestamp,
          1
     FROM TVM3T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup3 a
               WHERE a.MaterialPriceGroup3 = TVM3T_MVGR3);
