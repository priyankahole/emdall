UPDATE    dim_inspectionlotorigin o
   SET o.InspectionLotOriginName = t.TQ31T_HERKTXT,
			o.dw_update_date = current_timestamp
	FROM
       dim_inspectionlotorigin o join TQ31T t
	ON o.InspectionLotOriginCode = t.TQ31T_HERKUNFT
 AND o.RowIsCurrent = 1;
 
INSERT INTO dim_inspectionlotorigin(dim_inspectionlotoriginid,
                                     RowIsCurrent, rowstartdate,inspectionlotorigincode,inspectionlotoriginname)

   SELECT 1,
          1,current_timestamp,'Not Set','No Set'
     FROM (SELECT 1) D
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionlotorigin
               WHERE dim_inspectionlotoriginid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectionlotorigin';
   
insert into number_fountain
select 	'dim_inspectionlotorigin',
	ifnull(max(d.dim_inspectionlotoriginid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectionlotorigin d
where d.dim_inspectionlotoriginid <> 1; 

INSERT INTO dim_inspectionlotorigin(dim_inspectionlotoriginid,
                                    InspectionLotOriginCode,
                                    InspectionLotOriginName,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectionlotorigin')
          + row_number() over(order by '') ,
          t.TQ31T_HERKUNFT,
          t.TQ31T_HERKTXT,
          current_timestamp,
          1
     FROM TQ31T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionlotorigin o
               WHERE o.InspectionLotOriginCode = t.TQ31T_HERKUNFT
                     AND o.RowIsCurrent = 1);

