
INSERT INTO dim_CreditRepresentativeGroup(dim_CreditRepresentativeGroupId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_CreditRepresentativeGroup
               WHERE dim_CreditRepresentativeGroupId = 1);

UPDATE dim_CreditRepresentativeGroup crg
   SET CreditRepGroupName = ifnull(t.T024B_STEXT, 'Not Set'),
       RMailUserName = ifnull(t.T024B_SMAIL, 'Not Set'),
			dw_update_date = current_timestamp
	FROM dim_CreditRepresentativeGroup crg, T024B t
 WHERE     crg.CreditRepresentativeGroup = t.T024B_SBGRP
       AND crg.CreditControlArea = t.T024B_KKBER
       AND crg.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_CreditRepresentativeGroup';

insert into number_fountain
select 	'dim_CreditRepresentativeGroup',
	ifnull(max(d.dim_CreditRepresentativeGroupId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_CreditRepresentativeGroup d
where d.dim_CreditRepresentativeGroupId <> 1;

INSERT INTO dim_CreditRepresentativeGroup(dim_CreditRepresentativeGroupId,
									CreditRepresentativeGroup,
                                    CreditRepGroupName,
                                    CreditControlArea,
                                    RMailUserName,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_CreditRepresentativeGroup') 
          + row_number() over(order by '') , a.* FROM ( SELECT DISTINCT
			 t.T024B_SBGRP,
          ifnull(T024B_STEXT, 'Not Set'),
          ifnull(T024B_KKBER, 'Not Set'),
          ifnull(T024B_SMAIL, 'Not Set'),
          current_timestamp,
          1
     FROM T024B t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_CreditRepresentativeGroup a
               WHERE a.CreditRepresentativeGroup = T024B_SBGRP AND a.CreditControlArea = t.T024B_KKBER)) a;

delete from number_fountain m where m.table_name = 'dim_CreditRepresentativeGroup';
			   