insert into dim_cluster(dim_clusterid)
select 1 from (select 1) a where not exists (select 1 from dim_cluster where dim_clusterid = 1);

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'dim_cluster';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'dim_cluster', ifnull(max(dim_clusterid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM dim_cluster where dim_clusterid <> 1;

update dim_cluster dc
set dc.clust = clst.clust
from dim_cluster dc, csv_cluster_ls clst
where dc.primary_manufacturing_site = ifnull(clst.prim_man_site,'Not Set')
	and dc.clust <> clst.clust;

insert into dim_cluster(dim_clusterid,primary_manufacturing_site,clust)
select (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_cluster') + row_number() over (order by '') dim_clusterid
	,ifnull(clst.prim_man_site,'Not Set') primary_manufacturing_site
	,ifnull(clst.clust, 'Not Set') clust
from csv_cluster_ls clst
where not exists (select 1 from dim_cluster dc where primary_manufacturing_site = ifnull(clst.prim_man_site,'Not Set'));