/*insert default row*/
INSERT INTO dim_ora_mtl_item_locations
(
dim_ora_mtl_item_locationsid,
availability_type,inventory_atp_code,reservable_type,alias,dropping_order,attribute4,organization_id,last_update_date,
last_updated_by,creation_date,created_by,last_update_login,description,descriptive_text,disable_date,inventory_location_type,picking_order,
physical_location_code,location_maximum_units,subinventory_code,inventory_account_id,summary_flag,enabled_flag,start_date_active,
end_date_active,locator_status,empty_flag,mixed_items_flag,location_current_units,location_available_units,inventory_item_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_mtl_item_locationsid,
0 AVAILABILITY_TYPE,
0 INVENTORY_ATP_CODE,
0 RESERVABLE_TYPE,
'Not Set' ALIAS,
0 DROPPING_ORDER,
'Not Set' ATTRIBUTE4,
0 ORGANIZATION_ID,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' DESCRIPTION,
'Not Set' DESCRIPTIVE_TEXT,
timestamp '0001-01-01 00:00:00.000000' DISABLE_DATE,
0 INVENTORY_LOCATION_TYPE,
0 PICKING_ORDER,
'Not Set' PHYSICAL_LOCATION_CODE,
0 LOCATION_MAXIMUM_UNITS,
'Not Set' SUBINVENTORY_CODE,
0 INVENTORY_ACCOUNT_ID,
'Not Set' SUMMARY_FLAG,
'Not Set' ENABLED_FLAG,
timestamp '0001-01-01 00:00:00.000000' START_DATE_ACTIVE,
timestamp '0001-01-01 00:00:00.000000' END_DATE_ACTIVE,
0 LOCATOR_STATUS,
'Not Set' EMPTY_FLAG,
'Not Set' MIXED_ITEMS_FLAG,
0 LOCATION_CURRENT_UNITS,
0 LOCATION_AVAILABLE_UNITS,
0 INVENTORY_ITEM_ID,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_mtl_item_locations D
ON T.dim_ora_mtl_item_locationsid = D.dim_ora_mtl_item_locationsid
WHERE D.dim_ora_mtl_item_locationsid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_mtl_item_locations';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_mtl_item_locations',IFNULL(MAX(dim_ora_mtl_item_locationsid),0)
FROM dim_ora_mtl_item_locations;

/*update dimension rows*/
UPDATE dim_ora_mtl_item_locations T
FROM dim_ora_mtl_item_locations D JOIN ORA_MTL_ITEM_LOCATIONS S
ON D.KEY_ID = S.INVENTORY_LOCATION_ID
SET 
T.AVAILABILITY_TYPE = ifnull(S.AVAILABILITY_TYPE, 0),
T.INVENTORY_ATP_CODE = ifnull(S.INVENTORY_ATP_CODE, 0),
T.RESERVABLE_TYPE = ifnull(S.RESERVABLE_TYPE, 0),
T.ALIAS = ifnull(S.ALIAS, 'Not Set'),
T.DROPPING_ORDER = ifnull(S.DROPPING_ORDER, 0),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ORGANIZATION_ID = ifnull(S.ORGANIZATION_ID, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.DESCRIPTIVE_TEXT = ifnull(S.DESCRIPTIVE_TEXT, 'Not Set'),
T.DISABLE_DATE = ifnull(S.DISABLE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.INVENTORY_LOCATION_TYPE = ifnull(S.INVENTORY_LOCATION_TYPE, 0),
T.PICKING_ORDER = ifnull(S.PICKING_ORDER, 0),
T.PHYSICAL_LOCATION_CODE = ifnull(S.PHYSICAL_LOCATION_CODE, 'Not Set'),
T.LOCATION_MAXIMUM_UNITS = ifnull(S.LOCATION_MAXIMUM_UNITS, 0),
T.SUBINVENTORY_CODE = ifnull(S.SUBINVENTORY_CODE, 'Not Set'),
T.INVENTORY_ACCOUNT_ID = ifnull(S.INVENTORY_ACCOUNT_ID, 0),
T.SUMMARY_FLAG = ifnull(S.SUMMARY_FLAG, 'Not Set'),
T.ENABLED_FLAG = ifnull(S.ENABLED_FLAG, 'Not Set'),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.LOCATOR_STATUS = ifnull(S.LOCATOR_STATUS, 0),
T.EMPTY_FLAG = ifnull(S.EMPTY_FLAG, 'Not Set'),
T.MIXED_ITEMS_FLAG = ifnull(S.MIXED_ITEMS_FLAG, 'Not Set'),
T.LOCATION_CURRENT_UNITS = ifnull(S.LOCATION_CURRENT_UNITS, 0),
T.LOCATION_AVAILABLE_UNITS = ifnull(S.LOCATION_AVAILABLE_UNITS, 0),
T.INVENTORY_ITEM_ID = ifnull(S.INVENTORY_ITEM_ID, 0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE S.INVENTORY_LOCATION_ID = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_mtl_item_locations
(
dim_ora_mtl_item_locationsid,
availability_type,inventory_atp_code,reservable_type,alias,dropping_order,attribute4,organization_id,last_update_date,
last_updated_by,creation_date,created_by,last_update_login,description,descriptive_text,disable_date,inventory_location_type,picking_order,
physical_location_code,location_maximum_units,subinventory_code,inventory_account_id,summary_flag,enabled_flag,start_date_active,
end_date_active,locator_status,empty_flag,mixed_items_flag,location_current_units,location_available_units,inventory_item_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_mtl_item_locations' ),0) + ROW_NUMBER() OVER() dim_ora_mtl_item_locationsid,
ifnull(S.AVAILABILITY_TYPE, 0) AVAILABILITY_TYPE,
ifnull(S.INVENTORY_ATP_CODE, 0) INVENTORY_ATP_CODE,
ifnull(S.RESERVABLE_TYPE, 0) RESERVABLE_TYPE,
ifnull(S.ALIAS, 'Not Set') ALIAS,
ifnull(S.DROPPING_ORDER, 0) DROPPING_ORDER,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ORGANIZATION_ID, 0) ORGANIZATION_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.DESCRIPTIVE_TEXT, 'Not Set') DESCRIPTIVE_TEXT,
ifnull(S.DISABLE_DATE, timestamp '0001-01-01 00:00:00.000000') DISABLE_DATE,
ifnull(S.INVENTORY_LOCATION_TYPE, 0) INVENTORY_LOCATION_TYPE,
ifnull(S.PICKING_ORDER, 0) PICKING_ORDER,
ifnull(S.PHYSICAL_LOCATION_CODE, 'Not Set') PHYSICAL_LOCATION_CODE,
ifnull(S.LOCATION_MAXIMUM_UNITS, 0) LOCATION_MAXIMUM_UNITS,
ifnull(S.SUBINVENTORY_CODE, 'Not Set') SUBINVENTORY_CODE,
ifnull(S.INVENTORY_ACCOUNT_ID, 0) INVENTORY_ACCOUNT_ID,
ifnull(S.SUMMARY_FLAG, 'Not Set') SUMMARY_FLAG,
ifnull(S.ENABLED_FLAG, 'Not Set') ENABLED_FLAG,
ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.LOCATOR_STATUS, 0) LOCATOR_STATUS,
ifnull(S.EMPTY_FLAG, 'Not Set') EMPTY_FLAG,
ifnull(S.MIXED_ITEMS_FLAG, 'Not Set') MIXED_ITEMS_FLAG,
ifnull(S.LOCATION_CURRENT_UNITS, 0) LOCATION_CURRENT_UNITS,
ifnull(S.LOCATION_AVAILABLE_UNITS, 0) LOCATION_AVAILABLE_UNITS,
ifnull(S.INVENTORY_ITEM_ID, 0) INVENTORY_ITEM_ID,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.INVENTORY_LOCATION_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_MTL_ITEM_LOCATIONS S LEFT JOIN dim_ora_mtl_item_locations D 
ON D.KEY_ID = S.INVENTORY_LOCATION_ID
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_mtl_item_locations');