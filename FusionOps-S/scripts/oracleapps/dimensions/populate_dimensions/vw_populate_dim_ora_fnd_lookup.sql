/*insert default row*/
INSERT INTO dim_ora_fnd_lookup
(
dim_ora_fnd_lookupid,
end_date_active,start_date_active,enabled_flag,description,meaning,lookup_code,lookup_type,
security_group_id,view_application_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_fnd_lookupid,
timestamp '0001-01-01 00:00:00.000000' END_DATE_ACTIVE,
timestamp '0001-01-01 00:00:00.000000' START_DATE_ACTIVE,
'Not Set' ENABLED_FLAG,
'Not Set' DESCRIPTION,
'Not Set' MEANING,
'Not Set' LOOKUP_CODE ,
'Not Set' LOOKUP_TYPE ,
0 SECURITY_GROUP_ID ,
0 VIEW_APPLICATION_ID, 
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_fnd_lookup D
ON T.dim_ora_fnd_lookupid = D.dim_ora_fnd_lookupid
WHERE D.dim_ora_fnd_lookupid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_fnd_lookup';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_fnd_lookup',IFNULL(MAX(dim_ora_fnd_lookupid),0)
FROM dim_ora_fnd_lookup;

/*update dimension rows*/
UPDATE dim_ora_fnd_lookup T
FROM dim_ora_fnd_lookup D JOIN ORA_FND_LOOKUP S
ON 
D.KEY_ID =  ifnull(VARCHAR(S.LOOKUP_CODE, 200),'') +'~' + ifnull(VARCHAR(S.LOOKUP_TYPE, 200),'') +'~' + VARCHAR(S.SECURITY_GROUP_ID, 200) +'~'  + VARCHAR(S.VIEW_APPLICATION_ID, 200)
SET 
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.ENABLED_FLAG = ifnull(S.ENABLED_FLAG, 'Not Set'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.MEANING = ifnull(S.MEANING, 'Not Set'),
T.LOOKUP_CODE = ifnull(T.LOOKUP_CODE, 'Not Set') ,
T.LOOKUP_TYPE = ifnull(T.LOOKUP_TYPE, 'Not Set'),
T.SECURITY_GROUP_ID = ifnull(T.SECURITY_GROUP_ID,0),
T.VIEW_APPLICATION_ID = ifnull(T.VIEW_APPLICATION_ID,0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
ifnull(VARCHAR(S.LOOKUP_CODE, 200),'') +'~' + ifnull(VARCHAR(S.LOOKUP_TYPE, 200),'') +'~' + VARCHAR(S.SECURITY_GROUP_ID, 200) +'~'  + VARCHAR(S.VIEW_APPLICATION_ID, 200)  = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_fnd_lookup
(
dim_ora_fnd_lookupid,
end_date_active,start_date_active,enabled_flag,description,meaning,lookup_code,lookup_type,
security_group_id,view_application_id,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_fnd_lookup' ),0) + ROW_NUMBER() OVER() dim_ora_fnd_lookupid,
ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.ENABLED_FLAG, 'Not Set') ENABLED_FLAG,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.MEANING, 'Not Set') MEANING,
ifnull(S.LOOKUP_CODE,'Not Set') LOOKUP_CODE,
ifnull(S.LOOKUP_TYPE,'Not Set') LOOKUP_TYPE,
ifnull(S.SECURITY_GROUP_ID,0) SECURITY_GROUP_ID,
ifnull(S.VIEW_APPLICATION_ID,0) VIEW_APPLICATION_ID,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
ifnull(VARCHAR(S.LOOKUP_CODE, 200),'') +'~' + ifnull(VARCHAR(S.LOOKUP_TYPE, 200),'') +'~' + VARCHAR(S.SECURITY_GROUP_ID, 200) +'~'  + VARCHAR(S.VIEW_APPLICATION_ID, 200) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_FND_LOOKUP S LEFT JOIN dim_ora_fnd_lookup D ON
D.KEY_ID =  ifnull(VARCHAR(S.LOOKUP_CODE, 200),'') +'~' + ifnull(VARCHAR(S.LOOKUP_TYPE, 200),'') +'~' + VARCHAR(S.SECURITY_GROUP_ID, 200) +'~'  + VARCHAR(S.VIEW_APPLICATION_ID, 200)
JOIN 
(
select
LOOKUP_CODE,
LOOKUP_TYPE,
SECURITY_GROUP_ID,
VIEW_APPLICATION_ID,
meaning,
description,
row_number() OVER (partition by LOOKUP_CODE,LOOKUP_TYPE, SECURITY_GROUP_ID,VIEW_APPLICATION_ID
order by LOOKUP_CODE,LOOKUP_TYPE, SECURITY_GROUP_ID,VIEW_APPLICATION_ID
) row_number
from
ORA_FND_LOOKUP
) t
on s.LOOKUP_CODE = t.LOOKUP_CODE  and s.LOOKUP_TYPE = t.LOOKUP_TYPE and s.SECURITY_GROUP_ID = t.SECURITY_GROUP_ID and s.VIEW_APPLICATION_ID = t.VIEW_APPLICATION_ID 
and ifnull(s.meaning,'') = ifnull(t.meaning,'') and ifnull(s.description,'') = ifnull(t.description,'')
and row_number =1
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_fnd_lookup');