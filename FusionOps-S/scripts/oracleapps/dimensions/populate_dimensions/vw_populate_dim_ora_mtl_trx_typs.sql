/*insert default row*/
INSERT INTO dim_ora_mtl_trx_typs
(
dim_ora_mtl_trx_typsid,
location_required_flag,last_update_date,last_updated_by,creation_date,created_by,
transaction_type_name,description,transaction_action_id,transaction_source_type_id,shortage_msg_background_flag,shortage_msg_online_flag,
disable_date,user_defined_flag,attribute1,status_control_flag,attribute2,attribute3,attribute4,attribute5,attribute6,attribute7,
attribute8,attribute9,attribute10,attribute11,attribute12,attribute13,attribute14,attribute15,attribute_category,type_class,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_mtl_trx_typsid,
'Not Set' LOCATION_REQUIRED_FLAG,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
'Not Set' TRANSACTION_TYPE_NAME,
'Not Set' DESCRIPTION,
0 TRANSACTION_ACTION_ID,
0 TRANSACTION_SOURCE_TYPE_ID,
'Not Set' SHORTAGE_MSG_BACKGROUND_FLAG,
'Not Set' SHORTAGE_MSG_ONLINE_FLAG,
timestamp '0001-01-01 00:00:00.000000' DISABLE_DATE,
'Not Set' USER_DEFINED_FLAG,
'Not Set' ATTRIBUTE1,
0 STATUS_CONTROL_FLAG,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE4,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE8,
'Not Set' ATTRIBUTE9,
'Not Set' ATTRIBUTE10,
'Not Set' ATTRIBUTE11,
'Not Set' ATTRIBUTE12,
'Not Set' ATTRIBUTE13,
'Not Set' ATTRIBUTE14,
'Not Set' ATTRIBUTE15,
'Not Set' ATTRIBUTE_CATEGORY,
0 TYPE_CLASS,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_mtl_trx_typs D
ON T.dim_ora_mtl_trx_typsid = D.dim_ora_mtl_trx_typsid
WHERE D.dim_ora_mtl_trx_typsid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_mtl_trx_typs';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_mtl_trx_typs',IFNULL(MAX(dim_ora_mtl_trx_typsid),0)
FROM dim_ora_mtl_trx_typs;

/*update dimension rows*/
UPDATE dim_ora_mtl_trx_typs T
FROM dim_ora_mtl_trx_typs D JOIN ORA_MTL_TRX_TYPS S
ON D.KEY_ID = S.TRANSACTION_TYPE_ID
SET 
T.LOCATION_REQUIRED_FLAG = ifnull(S.LOCATION_REQUIRED_FLAG, 'Not Set'),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.TRANSACTION_TYPE_NAME = ifnull(S.TRANSACTION_TYPE_NAME, 'Not Set'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.TRANSACTION_ACTION_ID = ifnull(S.TRANSACTION_ACTION_ID, 0),
T.TRANSACTION_SOURCE_TYPE_ID = ifnull(S.TRANSACTION_SOURCE_TYPE_ID, 0),
T.SHORTAGE_MSG_BACKGROUND_FLAG = ifnull(S.SHORTAGE_MSG_BACKGROUND_FLAG, 'Not Set'),
T.SHORTAGE_MSG_ONLINE_FLAG = ifnull(S.SHORTAGE_MSG_ONLINE_FLAG, 'Not Set'),
T.DISABLE_DATE = ifnull(S.DISABLE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.USER_DEFINED_FLAG = ifnull(S.USER_DEFINED_FLAG, 'Not Set'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.STATUS_CONTROL_FLAG = ifnull(S.STATUS_CONTROL_FLAG, 0),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.ATTRIBUTE9 = ifnull(S.ATTRIBUTE9, 'Not Set'),
T.ATTRIBUTE10 = ifnull(S.ATTRIBUTE10, 'Not Set'),
T.ATTRIBUTE11 = ifnull(S.ATTRIBUTE11, 'Not Set'),
T.ATTRIBUTE12 = ifnull(S.ATTRIBUTE12, 'Not Set'),
T.ATTRIBUTE13 = ifnull(S.ATTRIBUTE13, 'Not Set'),
T.ATTRIBUTE14 = ifnull(S.ATTRIBUTE14, 'Not Set'),
T.ATTRIBUTE15 = ifnull(S.ATTRIBUTE15, 'Not Set'),
T.ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set'),
T.TYPE_CLASS = ifnull(S.TYPE_CLASS, 0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE S.TRANSACTION_TYPE_ID = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_mtl_trx_typs
(
dim_ora_mtl_trx_typsid,
location_required_flag,last_update_date,last_updated_by,creation_date,created_by,
transaction_type_name,description,transaction_action_id,transaction_source_type_id,shortage_msg_background_flag,shortage_msg_online_flag,
disable_date,user_defined_flag,attribute1,status_control_flag,attribute2,attribute3,attribute4,attribute5,attribute6,attribute7,
attribute8,attribute9,attribute10,attribute11,attribute12,attribute13,attribute14,attribute15,attribute_category,type_class,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_mtl_trx_typs' ),0) + ROW_NUMBER() OVER() dim_ora_mtl_trx_typsid,
ifnull(S.LOCATION_REQUIRED_FLAG, 'Not Set') LOCATION_REQUIRED_FLAG,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.TRANSACTION_TYPE_NAME, 'Not Set') TRANSACTION_TYPE_NAME,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.TRANSACTION_ACTION_ID, 0) TRANSACTION_ACTION_ID,
ifnull(S.TRANSACTION_SOURCE_TYPE_ID, 0) TRANSACTION_SOURCE_TYPE_ID,
ifnull(S.SHORTAGE_MSG_BACKGROUND_FLAG, 'Not Set') SHORTAGE_MSG_BACKGROUND_FLAG,
ifnull(S.SHORTAGE_MSG_ONLINE_FLAG, 'Not Set') SHORTAGE_MSG_ONLINE_FLAG,
ifnull(S.DISABLE_DATE, timestamp '0001-01-01 00:00:00.000000') DISABLE_DATE,
ifnull(S.USER_DEFINED_FLAG, 'Not Set') USER_DEFINED_FLAG,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.STATUS_CONTROL_FLAG, 0) STATUS_CONTROL_FLAG,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.ATTRIBUTE9, 'Not Set') ATTRIBUTE9,
ifnull(S.ATTRIBUTE10, 'Not Set') ATTRIBUTE10,
ifnull(S.ATTRIBUTE11, 'Not Set') ATTRIBUTE11,
ifnull(S.ATTRIBUTE12, 'Not Set') ATTRIBUTE12,
ifnull(S.ATTRIBUTE13, 'Not Set') ATTRIBUTE13,
ifnull(S.ATTRIBUTE14, 'Not Set') ATTRIBUTE14,
ifnull(S.ATTRIBUTE15, 'Not Set') ATTRIBUTE15,
ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set') ATTRIBUTE_CATEGORY,
ifnull(S.TYPE_CLASS, 0) TYPE_CLASS,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.TRANSACTION_TYPE_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_MTL_TRX_TYPS S LEFT JOIN dim_ora_mtl_trx_typs D
ON D.KEY_ID = S.TRANSACTION_TYPE_ID
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_mtl_trx_typs');