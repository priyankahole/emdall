/*insert default row*/
INSERT INTO dim_ora_oe_sales_credit_types
(
dim_ora_oe_sales_credit_typesid,
CREATION_DATE,CREATED_BY,
LAST_UPDATE_DATE,LAST_UPDATED_BY,
LAST_UPDATE_LOGIN,NAME,DESCRIPTION,QUOTA_FLAG,ENABLED_FLAG,CONTEXT,
ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3,ATTRIBUTE4,ATTRIBUTE5,ATTRIBUTE6,ATTRIBUTE7,ATTRIBUTE8,ATTRIBUTE9,
ATTRIBUTE10,ATTRIBUTE11,ATTRIBUTE12,ATTRIBUTE13,ATTRIBUTE14,ATTRIBUTE15,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_oe_sales_credit_typesid,	
timestamp '1970-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
timestamp '1970-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' NAME,
'Not Set' DESCRIPTION,
'Not Set' QUOTA_FLAG,
'Not Set' ENABLED_FLAG,
'Not Set' CONTEXT,
'Not Set' ATTRIBUTE1,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE4,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE8,
'Not Set' ATTRIBUTE9,
'Not Set' ATTRIBUTE10,
'Not Set' ATTRIBUTE11,
'Not Set' ATTRIBUTE12,
'Not Set' ATTRIBUTE13,
'Not Set' ATTRIBUTE14,
'Not Set' ATTRIBUTE15,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_oe_sales_credit_types D
ON T.dim_ora_oe_sales_credit_typesid = D.dim_ora_oe_sales_credit_typesid
WHERE D.dim_ora_oe_sales_credit_typesid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_oe_sales_credit_types';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_oe_sales_credit_types',IFNULL(MAX(dim_ora_oe_sales_credit_typesid),0)
FROM dim_ora_oe_sales_credit_types;

/*update dimension rows*/
UPDATE dim_ora_oe_sales_credit_types T	
FROM ora_oe_sales_credit_types S
SET
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.NAME = ifnull(S.NAME, 'Not Set'),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.QUOTA_FLAG = ifnull(S.QUOTA_FLAG, 'Not Set'),
T.ENABLED_FLAG = ifnull(S.ENABLED_FLAG, 'Not Set'),
T.CONTEXT = ifnull(S.CONTEXT, 'Not Set'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.ATTRIBUTE9 = ifnull(S.ATTRIBUTE9, 'Not Set'),
T.ATTRIBUTE10 = ifnull(S.ATTRIBUTE10, 'Not Set'),
T.ATTRIBUTE11 = ifnull(S.ATTRIBUTE11, 'Not Set'),
T.ATTRIBUTE12 = ifnull(S.ATTRIBUTE12, 'Not Set'),
T.ATTRIBUTE13 = ifnull(S.ATTRIBUTE13, 'Not Set'),
T.ATTRIBUTE14 = ifnull(S.ATTRIBUTE14, 'Not Set'),
T.ATTRIBUTE15 = ifnull(S.ATTRIBUTE15, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
S.SALES_CREDIT_TYPE_ID = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_oe_sales_credit_types
(
dim_ora_oe_sales_credit_typesid,
CREATION_DATE,CREATED_BY,
LAST_UPDATE_DATE,LAST_UPDATED_BY,
LAST_UPDATE_LOGIN,NAME,DESCRIPTION,QUOTA_FLAG,ENABLED_FLAG,CONTEXT,
ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3,ATTRIBUTE4,ATTRIBUTE5,ATTRIBUTE6,ATTRIBUTE7,ATTRIBUTE8,ATTRIBUTE9,
ATTRIBUTE10,ATTRIBUTE11,ATTRIBUTE12,ATTRIBUTE13,ATTRIBUTE14,ATTRIBUTE15,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_oe_sales_credit_types' ),0) + ROW_NUMBER() OVER() dim_ora_oe_sales_credit_typesid,
ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.NAME, 'Not Set') NAME,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.QUOTA_FLAG, 'Not Set') QUOTA_FLAG,
ifnull(S.ENABLED_FLAG, 'Not Set') ENABLED_FLAG,
ifnull(S.CONTEXT, 'Not Set') CONTEXT,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.ATTRIBUTE9, 'Not Set') ATTRIBUTE9,
ifnull(S.ATTRIBUTE10, 'Not Set') ATTRIBUTE10,
ifnull(S.ATTRIBUTE11, 'Not Set') ATTRIBUTE11,
ifnull(S.ATTRIBUTE12, 'Not Set') ATTRIBUTE12,
ifnull(S.ATTRIBUTE13, 'Not Set') ATTRIBUTE13,
ifnull(S.ATTRIBUTE14, 'Not Set') ATTRIBUTE14,
ifnull(S.ATTRIBUTE15, 'Not Set') ATTRIBUTE15,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
SALES_CREDIT_TYPE_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ora_oe_sales_credit_types S LEFT JOIN dim_ora_oe_sales_credit_types D ON
D.KEY_ID = SALES_CREDIT_TYPE_ID
WHERE
D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_oe_sales_credit_types');