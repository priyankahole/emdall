/*insert default row*/
INSERT INTO dim_ora_hroperatingunit
(
dim_ora_hroperatingunitid,
usable_flag,default_legal_context_id,set_of_books_id,short_code,date_to,date_from,name,business_group_id,dw_insert_date,
dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_hroperatingunitid,
'Not Set' USABLE_FLAG,
'Not Set' DEFAULT_LEGAL_CONTEXT_ID,
'Not Set' SET_OF_BOOKS_ID,
'Not Set' SHORT_CODE,
timestamp '0001-01-01 00:00:00.000000' DATE_TO,
timestamp '0001-01-01 00:00:00.000000' DATE_FROM,
'Not Set' NAME,
0 BUSINESS_GROUP_ID,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_hroperatingunit D
ON T.dim_ora_hroperatingunitid = D.dim_ora_hroperatingunitid
WHERE D.dim_ora_hroperatingunitid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_hroperatingunit';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_hroperatingunit',IFNULL(MAX(dim_ora_hroperatingunitid),0)
FROM dim_ora_hroperatingunit;

/*update dimension rows*/
UPDATE dim_ora_hroperatingunit T
FROM dim_ora_hroperatingunit D JOIN ORA_HR_OPER_UNIT S
ON D.KEY_ID = S.ORGANIZATION_ID
SET 
T.USABLE_FLAG = ifnull(S.USABLE_FLAG, 'Not Set'),
T.DEFAULT_LEGAL_CONTEXT_ID = ifnull(S.DEFAULT_LEGAL_CONTEXT_ID, 'Not Set'),
T.SET_OF_BOOKS_ID = ifnull(S.SET_OF_BOOKS_ID, 'Not Set'),
T.SHORT_CODE = ifnull(S.SHORT_CODE, 'Not Set'),
T.DATE_TO = ifnull(S.DATE_TO, timestamp '0001-01-01 00:00:00.000000'),
T.DATE_FROM = ifnull(S.DATE_FROM, timestamp '0001-01-01 00:00:00.000000'),
T.NAME = ifnull(S.NAME, 'Not Set'),
T.BUSINESS_GROUP_ID = ifnull(S.BUSINESS_GROUP_ID, 0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
S.ORGANIZATION_ID   = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_hroperatingunit
(
dim_ora_hroperatingunitid,
usable_flag,default_legal_context_id,set_of_books_id,short_code,date_to,date_from,name,business_group_id,dw_insert_date,
dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_hroperatingunit' ),0) + ROW_NUMBER() OVER() dim_ora_hroperatingunitid,
ifnull(S.USABLE_FLAG, 'Not Set') USABLE_FLAG,
ifnull(S.DEFAULT_LEGAL_CONTEXT_ID, 'Not Set') DEFAULT_LEGAL_CONTEXT_ID,
ifnull(S.SET_OF_BOOKS_ID, 'Not Set') SET_OF_BOOKS_ID,
ifnull(S.SHORT_CODE, 'Not Set') SHORT_CODE,
ifnull(S.DATE_TO, timestamp '0001-01-01 00:00:00.000000') DATE_TO,
ifnull(S.DATE_FROM, timestamp '0001-01-01 00:00:00.000000') DATE_FROM,
ifnull(S.NAME, 'Not Set') NAME,
ifnull(S.BUSINESS_GROUP_ID, 0) BUSINESS_GROUP_ID,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.ORGANIZATION_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_HR_OPER_UNIT S LEFT JOIN dim_ora_hroperatingunit D ON
D.KEY_ID = S.ORGANIZATION_ID
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_hroperatingunit');