/*set session authorization oracle_data*/

/*select * from dim_date*/
/*ALTER table dim_date*/
/*add column yearflag  varchar(30) NULL*/
/*ALTER table dim_date*/
/*add column quarterflag varchar(30) NULL*/
/*ALTER table dim_date*/
/*add column monthflag  varchar(30) NULL*/
/*ALTER table dim_date*/
/*add column weekflag  varchar(30) NULL*/
/*ALTER table dim_date*/
/*add column dayflag  varchar(30) NULL*/

/*select * from dim_date*/

/*dim_date yearflag dayflag quarterflag monthflag weekflag*/
update dim_date
set 
yearflag    = 'Not Set',
quarterflag = 'Not Set',
monthflag   = 'Not Set',
weekflag    = 'Not Set',
dayflag     = 'Not Set';

/*yearflag*/
update dim_date
set 
yearflag = 'Current'
where extract(year from current_timestamp)= calendaryear;

update dim_date
set 
yearflag = 'Previous'
where extract(year from current_timestamp)-1= calendaryear;

update dim_date
set 
yearflag = 'Next'
where extract(year from current_timestamp)+1= calendaryear;

/*dayflag*/
update dim_date
set 
dayflag = 'Current'
where to_date(current_timestamp)= datevalue;

update dim_date
set 
dayflag = 'Previous'
where to_date(current_timestamp)-1= datevalue;

update dim_date
set 
dayflag = 'Next'
where to_date(current_timestamp)+1= datevalue;

/*create temp table for date calculations*/
DROP TABLE IF EXISTS dim_date_calculations_tmp;


CREATE TABLE dim_date_calculations_tmp as
select 
dim_dateid, datevalue,
DENSE_RANK() over(order by calendaryear asc, calendarquarter asc) RANK_quarter,
DENSE_RANK() over(order by calendaryear asc, calendarmonthnumber asc) RANK_month,
DENSE_RANK() over(order by calendaryear asc, calendarweek asc) RANK_week
from dim_date
order by datevalue;

/*quarterflag*/
update dim_date d
set 
d.quarterflag = 'Current'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
rank_quarter = (select distinct rank_quarter from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);

update dim_date d
set 
d.quarterflag = 'Previous'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
rank_quarter+1 = (select distinct rank_quarter from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);

update dim_date d
set 
d.quarterflag = 'Next'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
rank_quarter-1 = (select distinct rank_quarter from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);


/*monthflag*/
update dim_date d
set 
d.monthflag = 'Current'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
rank_month = (select distinct rank_month from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);

update dim_date d
set 
d.monthflag = 'Previous'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
rank_month+1 = (select distinct rank_month from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);

update dim_date d
set 
d.monthflag = 'Next'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
rank_month-1 = (select distinct rank_month from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);

/*weekflag*/
update dim_date d
set 
d.weekflag = 'Current'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
RANK_week = (select distinct RANK_week from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);

update dim_date d
set 
d.weekflag = 'Previous'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
RANK_week+1 = (select distinct RANK_week from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);

update dim_date d
set 
d.weekflag = 'Next'
from dim_date d, dim_date_calculations_tmp t1
where d.datevalue =t1.datevalue and 
RANK_week-1 = (select distinct RANK_week from dim_date_calculations_tmp where to_date(current_timestamp) = datevalue);

/*drop temp table*/
DROP TABLE IF EXISTS dim_date_calculations_tmp;
