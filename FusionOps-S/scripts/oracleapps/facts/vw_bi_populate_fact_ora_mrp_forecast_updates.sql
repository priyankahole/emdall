/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mrp_forecast_updates*/
/*SELECT * FROM fact_ora_mrp_forecast_updates*/

/*initialize NUMBER_FOUNTAIN*/
MODIFY fact_ora_mrp_forecast_updates TO TRUNCATED;

delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_mrp_forecast_updates';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_mrp_forecast_updates',IFNULL(MAX(fact_ora_mrp_forecast_updatesid),0)
FROM fact_ora_mrp_forecast_updates;


/*generate unique key for ora_mrp_forecast_updates*/
DROP IF EXISTS ora_mrp_forecast_updates_tmp;

create table ora_mrp_forecast_updates_tmp as
select 
CREATED_BY,
INVENTORY_ITEM_ID,
ORGANIZATION_ID,
FORECAST_DESIGNATOR,
UPDATE_SALES_ORDER,
SALES_ORDER_SCHEDULE_DATE,
FORECAST_UPDATE_DATE,
SALES_ORDER_QUANTITY,
DEMAND_CLASS,
UPDATE_QUANTITY,
CUSTOMER_ID,
SHIP_ID,
BILL_ID,
LINE_NUM,
UPDATE_SEQ_NUM,
TRANSACTION_ID,
LAST_UPDATE_DATE,
LAST_UPDATED_BY,
CREATION_DATE,
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_mrp_forecast_updates' ),0) + ROW_NUMBER() OVER() fact_ora_mrp_forecast_updatesid
from ora_mrp_forecast_updates;

MODIFY ora_mrp_forecast_updates TO TRUNCATED;

INSERT INTO ora_mrp_forecast_updates
(
CREATED_BY,INVENTORY_ITEM_ID,ORGANIZATION_ID,FORECAST_DESIGNATOR,UPDATE_SALES_ORDER,
SALES_ORDER_SCHEDULE_DATE,FORECAST_UPDATE_DATE,SALES_ORDER_QUANTITY,DEMAND_CLASS,UPDATE_QUANTITY,CUSTOMER_ID,SHIP_ID,BILL_ID,
LINE_NUM,UPDATE_SEQ_NUM,TRANSACTION_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,CREATION_DATE,fact_ora_mrp_forecast_updatesid
)
select 
CREATED_BY,
INVENTORY_ITEM_ID,
ORGANIZATION_ID,
FORECAST_DESIGNATOR,
UPDATE_SALES_ORDER,
SALES_ORDER_SCHEDULE_DATE,
FORECAST_UPDATE_DATE,
SALES_ORDER_QUANTITY,
DEMAND_CLASS,
UPDATE_QUANTITY,
CUSTOMER_ID,
SHIP_ID,
BILL_ID,
LINE_NUM,
UPDATE_SEQ_NUM,
TRANSACTION_ID,
LAST_UPDATE_DATE,
LAST_UPDATED_BY,
CREATION_DATE,
fact_ora_mrp_forecast_updatesid
from ora_mrp_forecast_updates_tmp;

DROP IF EXISTS ora_mrp_forecast_updates_tmp;


/*update fact columns*/
UPDATE fact_ora_mrp_forecast_updates T	
FROM ora_mrp_forecast_updates S
SET 
DD_UPDATE_SALES_ORDER = S.UPDATE_SALES_ORDER,
CT_SALES_ORDER_QUANTITY = S.SALES_ORDER_QUANTITY,
DD_DEMAND_CLASS = ifnull(S.DEMAND_CLASS,'Not Set'),
CT_UPDATE_QUANTITY = S.UPDATE_QUANTITY,
DD_LINE_NUM = ifnull(S.LINE_NUM,'Not Set'),
DD_UPDATE_SEQ_NUM = ifnull(S.UPDATE_SEQ_NUM,0),
DD_TRANSACTION_ID = S.TRANSACTION_ID,
amt_exchangerate = 1,
amt_exchangerate_gbl = 1,
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE 
T.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid;

/*insert new rows*/
INSERT INTO fact_ora_mrp_forecast_updates
(
fact_ora_mrp_forecast_updatesid,
DIM_ORA_CREATED_BYID,DIM_ORA_INVENTORY_ITEMID,DIM_ORA_ORGANIZATIONID,DIM_ORA_FORECAST_DESIGNATORID,DD_UPDATE_SALES_ORDER,
DIM_ORA_DATE_SALES_ORDER_SCHEDULEID,DIM_ORA_DATE_FORECAST_UPDATEID,CT_SALES_ORDER_QUANTITY,DD_DEMAND_CLASS,CT_UPDATE_QUANTITY,
DIM_ORA_CUSTOMERID,DIM_ORA_SHIPID,DIM_ORA_BILLID,DD_LINE_NUM,
DD_UPDATE_SEQ_NUM,DD_TRANSACTION_ID,DIM_ORA_DATE_LAST_UPDATEID,DIM_ORA_LAST_UPDATED_BYID,DIM_ORA_DATE_CREATIONID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
S.fact_ora_mrp_forecast_updatesid,
1 AS DIM_ORA_CREATED_BYID,
1 AS DIM_ORA_INVENTORY_ITEMID,
1 AS DIM_ORA_ORGANIZATIONID,
1 AS DIM_ORA_FORECAST_DESIGNATORID,
S.UPDATE_SALES_ORDER AS DD_UPDATE_SALES_ORDER,
1 AS DIM_ORA_DATE_SALES_ORDER_SCHEDULEID,
1 AS DIM_ORA_DATE_FORECAST_UPDATEID,
S.SALES_ORDER_QUANTITY AS CT_SALES_ORDER_QUANTITY,
ifnull(S.DEMAND_CLASS,'Not Set') AS DD_DEMAND_CLASS,
S.UPDATE_QUANTITY AS CT_UPDATE_QUANTITY,
1 AS DIM_ORA_CUSTOMERID,
1 AS DIM_ORA_SHIPID,
1 AS DIM_ORA_BILLID,
ifnull(S.LINE_NUM,'Not Set') AS DD_LINE_NUM,
ifnull(S.UPDATE_SEQ_NUM,0) AS DD_UPDATE_SEQ_NUM,
S.TRANSACTION_ID AS DD_TRANSACTION_ID,
1 AS DIM_ORA_DATE_LAST_UPDATEID,
1 AS DIM_ORA_LAST_UPDATED_BYID,
1 AS DIM_ORA_DATE_CREATIONID,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'INSERT' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mrp_forecast_updates S LEFT JOIN fact_ora_mrp_forecast_updates F 
ON F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid
WHERE F.fact_ora_mrp_forecast_updatesid is null;

/*UPDATE DIM_ORA_CREATED_BYID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1
SET 
F.DIM_ORA_CREATED_BYID = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_CREATED_BYID <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_INVENTORY_ITEMID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_ORA_INVPRODUCT D ON VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.INVENTORY_ITEM_ID, 200) = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_INVENTORY_ITEMID = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_INVENTORY_ITEMID <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE DIM_ORA_ORGANIZATIONID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_ORA_MTL_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_ORGANIZATIONID = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_ORGANIZATIONID <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE DIM_ORA_FORECAST_DESIGNATORID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_ORA_MRP_FORECAST_DESIGNATORS D 
ON VARCHAR(S.FORECAST_DESIGNATOR,200) + '~' + VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_FORECAST_DESIGNATORID = D.DIM_ORA_MRP_FORECAST_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_FORECAST_DESIGNATORID <> D.DIM_ORA_MRP_FORECAST_DESIGNATORSID;

/*UPDATE DIM_ORA_DATE_SALES_ORDER_SCHEDULEID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_DATE D ON ANSIDATE(S.SALES_ORDER_SCHEDULE_DATE) = D.DATEVALUE  
SET 
F.DIM_ORA_DATE_SALES_ORDER_SCHEDULEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_DATE_SALES_ORDER_SCHEDULEID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE_FORECAST_UPDATEID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_DATE D ON ANSIDATE(S.FORECAST_UPDATE_DATE) = D.DATEVALUE 
SET 
F.DIM_ORA_DATE_FORECAST_UPDATEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_DATE_FORECAST_UPDATEID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_CUSTOMERID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_ORA_HZ_CUSTACCOUNTS D ON S.CUSTOMER_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_CUSTOMERID = D.DIM_ORA_HZ_CUSTACCOUNTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_CUSTOMERID <> D.DIM_ORA_HZ_CUSTACCOUNTSID;

/*UPDATE DIM_ORA_SHIPID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.SHIP_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_SHIPID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_SHIPID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_ORA_BILLID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_ORA_CUSTOMERLOCATION D ON S.BILL_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_BILLID = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_BILLID <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE DIM_ORA_DATE_LAST_UPDATEID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_DATE D ON ANSIDATE(S.LAST_UPDATE_DATE) = D.DATEVALUE  
SET 
F.DIM_ORA_DATE_LAST_UPDATEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_DATE_LAST_UPDATEID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_LAST_UPDATED_BYID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_LAST_UPDATED_BYID = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_LAST_UPDATED_BYID <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_DATE_CREATIONID*/
UPDATE fact_ora_mrp_forecast_updates F
FROM ora_mrp_forecast_updates S JOIN DIM_DATE D ON ANSIDATE(S.CREATION_DATE) = D.DATEVALUE  
SET 
F.DIM_ORA_DATE_CREATIONID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_mrp_forecast_updatesid = S.fact_ora_mrp_forecast_updatesid AND
F.DIM_ORA_DATE_CREATIONID <> D.DIM_DATEID;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_mrp_forecast_updates');