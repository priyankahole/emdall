/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_cogs*/
/*SELECT * FROM fact_ora_cogs*/

MODIFY fact_ora_cogs TO TRUNCATED;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_cogs';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_cogs',IFNULL(MAX(fact_ora_cogsid),0)
FROM fact_ora_cogs;

DROP IF EXISTS ora_mtl_transaction_accounts_cogs_tmp;

create table ora_mtl_transaction_accounts_cogs_tmp as
SELECT 
transaction_id,
reference_account,
transaction_action_id,
accounting_line_type,
cost_element_id,
last_update_date,
last_updated_by,
creation_date,
created_by,
inventory_item_id,
organization_id,
transaction_date,
transaction_value,
primary_quantity,
gl_batch_id,
base_transaction_value,
currency_code,
transaction_type_id,
trx_source_line_id,
mmt_organization_id,
acct_period_id,
ship_to_org_id,
invoice_to_org_id,
org_id,
order_number,
line_number,
oel_creation_date,
ordered_date,
sold_to_org_id,
ship_to_contact_id,
payment_term_id,
territory_id,
inv_sub_ledger_id,
project_id,
task_id,
expenditure_type,
pa_expenditure_org_id,
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_cogs' ),0) + ROW_NUMBER() OVER() fact_ora_cogsid
FROM ora_mtl_transaction_accounts_cogs;

MODIFY ora_mtl_transaction_accounts_cogs TO TRUNCATED;

INSERT INTO ora_mtl_transaction_accounts_cogs
(
transaction_id,reference_account,transaction_action_id,accounting_line_type,cost_element_id,last_update_date,last_updated_by,creation_date,created_by,
inventory_item_id,organization_id,transaction_date,transaction_value,primary_quantity,gl_batch_id,base_transaction_value,currency_code,transaction_type_id,
trx_source_line_id,mmt_organization_id,acct_period_id,ship_to_org_id,invoice_to_org_id,org_id,order_number,line_number,oel_creation_date,ordered_date,
sold_to_org_id,ship_to_contact_id,payment_term_id,territory_id,inv_sub_ledger_id,project_id,task_id,expenditure_type,pa_expenditure_org_id,fact_ora_cogsid
)
SELECT 
transaction_id,reference_account,transaction_action_id,accounting_line_type,cost_element_id,last_update_date,last_updated_by,creation_date,created_by,
inventory_item_id,organization_id,transaction_date,transaction_value,primary_quantity,gl_batch_id,base_transaction_value,currency_code,transaction_type_id,
trx_source_line_id,mmt_organization_id,acct_period_id,ship_to_org_id,invoice_to_org_id,org_id,order_number,line_number,oel_creation_date,
ordered_date,sold_to_org_id,ship_to_contact_id,payment_term_id,territory_id,inv_sub_ledger_id,project_id,task_id,expenditure_type,pa_expenditure_org_id,
fact_ora_cogsid
FROM ora_mtl_transaction_accounts_cogs_tmp;

DROP IF EXISTS ora_mtl_transaction_accounts_cogs_tmp;

/*update fact columns*/
UPDATE fact_ora_cogs T	
FROM ora_mtl_transaction_accounts_cogs S
SET 
dd_reference_account = S.REFERENCE_ACCOUNT,
amt_transaction_value = S.TRANSACTION_VALUE,
ct_primary_quantity = S.PRIMARY_QUANTITY,
amt_base_transaction_value = S.BASE_TRANSACTION_VALUE,
dd_currency_code = ifnull(S.CURRENCY_CODE,'Not Set'),
dd_order_number = S.ORDER_NUMBER,
dd_line_number = S.LINE_NUMBER,
dd_ship_to_contact_id = S.SHIP_TO_CONTACT_ID,
dd_inv_sub_ledger_id = S.INV_SUB_LEDGER_ID,
dd_project_id = S.PROJECT_ID,
dd_task_id = S.TASK_ID,
dd_expenditure_type = S.EXPENDITURE_TYPE,
dd_pa_expenditure_org_id = S.PA_EXPENDITURE_ORG_ID,
dd_transaction_action_id = S.TRANSACTION_ACTION_ID,
TRANSACTION_ID = S.TRANSACTION_ID,
DW_UPDATE_DATE = current_timestamp, 
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE
T.fact_ora_cogsid = S.fact_ora_cogsid;

/*insert new rows*/
INSERT INTO fact_ora_cogs
(
fact_ora_cogsid,dd_reference_account,dim_ora_transaction_actionid,dim_ora_accounting_line_typeid,
dim_ora_cost_elementid,dim_ora_date_last_updateid,dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_created_byid,dim_ora_inventory_itemid,
dim_ora_organizationid,dim_ora_date_transactionid,amt_transaction_value,ct_primary_quantity,dim_ora_gl_batchid,amt_base_transaction_value,dd_currency_code,
dim_ora_transaction_typeid,dim_ora_trx_source_lineid,dim_ora_mmt_organizationid,dim_ora_acct_periodid,dim_ora_ship_to_orgid,dim_ora_invoice_to_orgid,
dim_ora_orgid,dd_order_number,dd_line_number,dim_ora_date_oel_creationid,dim_ora_date_orderedid,dim_ora_sold_to_orgid,dd_ship_to_contact_id,dim_ora_payment_termid,
dim_ora_territoryid,dd_inv_sub_ledger_id,dd_project_id,dd_task_id,dd_expenditure_type,dd_pa_expenditure_org_id,dd_transaction_action_id,TRANSACTION_ID,
amt_exchangerate,amt_exchangerate_gbl,dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
S.fact_ora_cogsid,
ifnull(S.REFERENCE_ACCOUNT,0) AS dd_reference_account,
1 AS dim_ora_transaction_actionid,
1 AS dim_ora_accounting_line_typeid,
1 AS dim_ora_cost_elementid,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_last_updated_byid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_created_byid,
1 AS dim_ora_inventory_itemid,
1 AS dim_ora_organizationid,
1 AS dim_ora_date_transactionid,
ifnull(S.TRANSACTION_VALUE,0) AS amt_transaction_value,
ifnull(S.PRIMARY_QUANTITY,0) AS ct_primary_quantity,
1 AS dim_ora_gl_batchid,
ifnull(S.BASE_TRANSACTION_VALUE,0) AS amt_base_transaction_value,
ifnull(S.CURRENCY_CODE,'Not Set') AS dd_currency_code,
1 AS dim_ora_transaction_typeid,
1 AS dim_ora_trx_source_lineid,
1 AS dim_ora_mmt_organizationid,
1 AS dim_ora_acct_periodid,
1 AS dim_ora_ship_to_orgid,
1 AS dim_ora_invoice_to_orgid,
1 AS dim_ora_orgid,
ifnull(S.ORDER_NUMBER,0) AS dd_order_number,
ifnull(S.LINE_NUMBER,0) AS dd_line_number,
1 AS dim_ora_date_oel_creationid,
1 AS dim_ora_date_orderedid,
1 AS dim_ora_sold_to_orgid,
ifnull(S.SHIP_TO_CONTACT_ID,0) AS dd_ship_to_contact_id,
1 AS dim_ora_payment_termid,
1 AS dim_ora_territoryid,
ifnull(S.INV_SUB_LEDGER_ID,0) AS dd_inv_sub_ledger_id,
ifnull(S.PROJECT_ID,0) AS dd_project_id,
ifnull(S.TASK_ID,0) AS dd_task_id,
ifnull(S.EXPENDITURE_TYPE,'Not Set') AS dd_expenditure_type,
ifnull(S.PA_EXPENDITURE_ORG_ID,0) AS dd_pa_expenditure_org_id,
ifnull(S.TRANSACTION_ACTION_ID,0) AS dd_transaction_action_id,
ifnull(S.TRANSACTION_ID,0) AS TRANSACTION_ID,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mtl_transaction_accounts_cogs S LEFT JOIN fact_ora_cogs T
ON T.fact_ora_cogsid = S.fact_ora_cogsid
WHERE T.fact_ora_cogsid is null;

/*UPDATE dim_ora_transaction_actionid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S 
JOIN DIM_ORA_FND_LOOKUP D 
ON S.TRANSACTION_ACTION_ID = D.LOOKUP_CODE AND D.LOOKUP_TYPE ='MTL_TRANSACTION_ACTION' and d.rowiscurrent = 1
SET 
F.dim_ora_transaction_actionid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and 
F.dim_ora_transaction_actionid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_accounting_line_typeid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S 
JOIN DIM_ORA_FND_LOOKUP D 
ON S.ACCOUNTING_LINE_TYPE = D.LOOKUP_CODE AND D.LOOKUP_TYPE = 'CST_ACCOUNTING_LINE_TYPE'
SET 
F.dim_ora_accounting_line_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_accounting_line_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_cost_elementid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S 
JOIN DIM_ORA_CST_COST_ELEMENTS D ON S.COST_ELEMENT_ID = D.KEY_ID 
SET 
F.dim_ora_cost_elementid = D.DIM_ORA_CST_COST_ELEMENTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_cost_elementid <> D.DIM_ORA_CST_COST_ELEMENTSID;

/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_DATE D ON ANSIDATE(S.LAST_UPDATE_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_last_updated_byid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID 
SET 
F.dim_ora_last_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_last_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_DATE D ON ANSIDATE(S.CREATION_DATE) = D.DATEVALUE  
SET 
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID  
SET 
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_inventory_itemid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_INVPRODUCT D 
ON VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.INVENTORY_ITEM_ID, 200) = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_inventory_itemid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND 
F.dim_ora_inventory_itemid <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE dim_ora_organizationid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_BUSINESS_ORG D 
ON  'INV' + '~' +VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_organizationid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_organizationid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_date_transactionid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_DATE D ON ANSIDATE(S.TRANSACTION_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_transactionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_date_transactionid <> D.DIM_DATEID;

/*UPDATE dim_ora_gl_batchid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_ORG_GL_BATCHES D 
ON VARCHAR(ifnull(S.ORGANIZATION_ID,'Not Set'), 200) + '~'+ ifnull(VARCHAR(S.GL_BATCH_ID, 200),'Not Set')= D.KEY_ID 
SET 
F.dim_ora_gl_batchid = D.DIM_ORA_ORG_GL_BATCHESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_gl_batchid <> D.DIM_ORA_ORG_GL_BATCHESID;

/*UPDATE dim_ora_transaction_typeid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S 
JOIN DIM_ORA_MTL_TRX_TYPS D ON S.TRANSACTION_TYPE_ID = D.KEY_ID 
SET 
F.dim_ora_transaction_typeid = D.DIM_ORA_MTL_TRX_TYPSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_transaction_typeid <> D.DIM_ORA_MTL_TRX_TYPSID;

/*UPDATE dim_ora_trx_source_lineid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S 
JOIN DIM_ORA_MTL_TRX_SOURCE_TYPS D ON S.TRX_SOURCE_LINE_ID = D.KEY_ID  
SET 
F.dim_ora_trx_source_lineid = D.DIM_ORA_MTL_TRX_SOURCE_TYPSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_trx_source_lineid <> D.DIM_ORA_MTL_TRX_SOURCE_TYPSID;

/*UPDATE dim_ora_mmt_organizationid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_BUSINESS_ORG D 
ON  'INV' + '~' +VARCHAR(S.MMT_ORGANIZATION_ID,200) = D.KEY_ID
SET 
F.dim_ora_mmt_organizationid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_mmt_organizationid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_acct_periodid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_ORG_ACCT_PERIODS D 
ON ifnull(VARCHAR(S.ACCT_PERIOD_ID,200),'Not Set') + '~' + ifnull(VARCHAR(S.ORGANIZATION_ID, 200),'Not Set') = D.KEY_ID AND d.rowiscurrent = 1   
SET 
F.dim_ora_acct_periodid = D.DIM_ORA_ORG_ACCT_PERIODSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_acct_periodid <> D.DIM_ORA_ORG_ACCT_PERIODSID;

/*UPDATE dim_ora_ship_to_orgid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S 
JOIN DIM_ORA_CUSTOMERLOCATION D ON S.SHIP_TO_ORG_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_ship_to_orgid = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_ship_to_orgid <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE dim_ora_invoice_to_orgid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S 
JOIN DIM_ORA_CUSTOMERLOCATION D ON S.INVOICE_TO_ORG_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_invoice_to_orgid = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_invoice_to_orgid <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE dim_ora_orgid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_BUSINESS_ORG D 
ON  'OPERATING_UNIT' + '~' +VARCHAR(S.ORG_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_orgid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid and
F.dim_ora_orgid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_date_oel_creationid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_DATE D ON ANSIDATE(S.OEL_CREATION_DATE) = D.DATEVALUE  
SET 
F.dim_ora_date_oel_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_date_oel_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_orderedid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_DATE D ON ANSIDATE(S.ORDERED_DATE) = D.DATEVALUE  
SET 
F.dim_ora_date_orderedid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_date_orderedid <> D.DIM_DATEID;

/*UPDATE dim_ora_payment_termid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S 
JOIN DIM_ORA_AR_PAYMENT_TERMS D ON S.PAYMENT_TERM_ID = D.KEY_ID and d.rowiscurrent = 1   
SET 
F.dim_ora_payment_termid = D.DIM_ORA_AR_PAYMENT_TERMSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_payment_termid <> D.DIM_ORA_AR_PAYMENT_TERMSID;

/*UPDATE dim_ora_territoryid*/
UPDATE fact_ora_cogs F
FROM ora_mtl_transaction_accounts_cogs S JOIN DIM_ORA_RA_TERRITORIES D 
ON S.TERRITORY_ID = D.KEY_ID  
SET 
F.dim_ora_territoryid = D.DIM_ORA_RA_TERRITORIESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.fact_ora_cogsid =  S.fact_ora_cogsid AND
F.dim_ora_territoryid <> D.DIM_ORA_RA_TERRITORIESID;

CALL VECTORWISE( COMBINE 'fact_ora_cogs');