/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_wip_transactions*/
/*SELECT * FROM fact_ora_wip_transactions*/

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_wip_transactions';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_wip_transactions',IFNULL(MAX(fact_ora_wip_transactionsid),0)
FROM fact_ora_wip_transactions;

/*update fact columns*/
UPDATE fact_ora_wip_transactions T	
FROM ora_wip_transactions S
SET 
dd_wip_entity_id = S.wip_entity_id,
dd_transaction_type = S.transaction_type,
dd_group_id = ifnull(S.group_id,0),
dd_source_code = ifnull(S.source_code,'Not Set'),
dd_operation_seq_num = ifnull(S.operation_seq_num,0),
dd_resource_seq_num = ifnull(S.resource_seq_num,0),
dd_autocharge_type = ifnull(S.autocharge_type,0),
dd_standard_rate_flag = ifnull(S.standard_rate_flag,0),
amt_usage_rate_or_amount = ifnull(S.usage_rate_or_amount,0),
dd_basis_type = ifnull(S.basis_type,0),
ct_transaction_quantity = ifnull(S.transaction_quantity,0),
dd_transaction_uom = ifnull(S.transaction_uom,'Not Set'),
ct_primary_quantity = ifnull(S.primary_quantity,0),
dd_primary_uom = ifnull(S.primary_uom,'Not Set'),
amt_actual_resource_rate = ifnull(S.actual_resource_rate,0),
amt_standard_resource_rate = ifnull(S.standard_resource_rate,0),
dd_currency_code = ifnull(S.currency_code,'Not Set'),
dd_currency_conversion_type = ifnull(S.currency_conversion_type,'Not Set'),
amt_currency_conversion_rate = ifnull(S.currency_conversion_rate,0),
amt_currency_actual_resource_rate = ifnull(S.currency_actual_resource_rate,0),
dd_activity_id = ifnull(S.activity_id,0),
dd_reference = ifnull(S.reference,'Not Set'),
dd_move_transaction_id = ifnull(S.move_transaction_id,0),
dd_rcv_transaction_id = ifnull(S.rcv_transaction_id,0),
dd_attribute_category = ifnull(S.attribute_category,'Not Set'),
dd_attribute1 = ifnull(S.attribute1,'Not Set'),
dd_attribute2 = ifnull(S.attribute2,'Not Set'),
dd_attribute3 = ifnull(S.attribute3,'Not Set'),
dd_attribute4 = ifnull(S.attribute4,'Not Set'),
dd_attribute5 = ifnull(S.attribute5,'Not Set'),
dd_attribute6 = ifnull(S.attribute6,'Not Set'),
dd_attribute7 = ifnull(S.attribute7,'Not Set'),
dd_attribute8 = ifnull(S.attribute8,'Not Set'),
dd_attribute9 = ifnull(S.attribute9,'Not Set'),
dd_attribute10 = ifnull(S.attribute10,'Not Set'),
dd_attribute11 = ifnull(S.attribute11,'Not Set'),
dd_attribute12 = ifnull(S.attribute12,'Not Set'),
dd_attribute13 = ifnull(S.attribute13,'Not Set'),
dd_attribute14 = ifnull(S.attribute14,'Not Set'),
dd_attribute15 = ifnull(S.attribute15,'Not Set'),
dd_cost_update_id = ifnull(S.cost_update_id,0),
dd_pm_cost_collected = ifnull(S.pm_cost_collected,'Not Set'),
dd_pm_cost_collector_group_id = ifnull(S.pm_cost_collector_group_id,0),
dd_project_id = ifnull(S.project_id,0),
dd_task_id = ifnull(S.task_id,0),
dd_completion_transaction_id = ifnull(S.completion_transaction_id,0),
dd_charge_department_id = ifnull(S.charge_department_id,0),
dd_instance_id = ifnull(S.instance_id,0),
dd_encumbrance_type_id = ifnull(S.encumbrance_type_id,0),
amt_encumbrance_amount = ifnull(S.encumbrance_amount,0),
ct_encumbrance_quantity = ifnull(S.encumbrance_quantity,0),
dd_encumbrance_ccid = ifnull(S.encumbrance_ccid,0),
dd_wip_entity_name = ifnull(S.wip_entity_name,'Not Set'),
dd_entity_type = ifnull(S.entity_type,'Not Set'),
dd_entity_description = ifnull(S.entity_description,'Not Set'),

DW_UPDATE_DATE = current_timestamp, 
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE
T.TRANSACTION_ID =  S.TRANSACTION_ID;

/*insert new rows*/
INSERT INTO fact_ora_wip_transactions
(
fact_ora_wip_transactionsid,
transaction_id,dim_ora_date_last_updateid,dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_created_byid,dim_ora_organizationid,dd_wip_entity_id,dim_ora_primary_itemid,dim_ora_acct_periodid,
dim_ora_departmentid,dd_transaction_type,dim_ora_date_transactionid,dd_group_id,dim_ora_lineid,dd_source_code,dim_ora_source_lineid,dd_operation_seq_num,
dd_resource_seq_num,dim_ora_employeeid,dim_ora_resourceid,dd_autocharge_type,dd_standard_rate_flag,amt_usage_rate_or_amount,dd_basis_type,ct_transaction_quantity,
dd_transaction_uom,ct_primary_quantity,dd_primary_uom,amt_actual_resource_rate,amt_standard_resource_rate,dd_currency_code,dim_ora_date_currency_conversionid,
dd_currency_conversion_type,amt_currency_conversion_rate,amt_currency_actual_resource_rate,dd_activity_id,dim_ora_reasonid,dd_reference,dd_move_transaction_id,
fact_ora_po_lineid,dd_rcv_transaction_id,dd_attribute_category,dd_attribute1,dd_attribute2,dd_attribute3,dd_attribute4,dd_attribute5,dd_attribute6,dd_attribute7,
dd_attribute8,dd_attribute9,dd_attribute10,dd_attribute11,dd_attribute12,dd_attribute13,dd_attribute14,dd_attribute15,dd_cost_update_id,dd_pm_cost_collected,
dd_pm_cost_collector_group_id,dd_project_id,dd_task_id,dd_completion_transaction_id,dd_charge_department_id,dd_instance_id,dd_encumbrance_type_id,
amt_encumbrance_amount,ct_encumbrance_quantity,dd_encumbrance_ccid,dd_wip_entity_name,dd_entity_type,dd_entity_description,amt_exchangerate,amt_exchangerate_gbl,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_ar_sales_invoices' ),0) + ROW_NUMBER() OVER() fact_ora_wip_transactionsid,
S.transaction_id AS transaction_id,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_last_updated_byid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_created_byid,
1 AS dim_ora_organizationid,
S.wip_entity_id AS dd_wip_entity_id,
1 AS dim_ora_primary_itemid,
1 AS dim_ora_acct_periodid,
1 AS dim_ora_departmentid,
S.transaction_type AS dd_transaction_type,
1 AS dim_ora_date_transactionid,
ifnull(S.group_id,0) AS dd_group_id,
1 AS dim_ora_lineid,
ifnull(S.source_code,'Not Set') AS dd_source_code,
1 AS dim_ora_source_lineid,
ifnull(S.operation_seq_num,0) AS dd_operation_seq_num,
ifnull(S.resource_seq_num,0) AS dd_resource_seq_num,
1 AS dim_ora_employeeid,
1 AS dim_ora_resourceid,
ifnull(S.autocharge_type,0) AS dd_autocharge_type,
ifnull(S.standard_rate_flag,0) AS dd_standard_rate_flag,
ifnull(S.usage_rate_or_amount,0) AS amt_usage_rate_or_amount,
ifnull(S.basis_type,0) AS dd_basis_type,
ifnull(S.transaction_quantity,0) AS ct_transaction_quantity,
ifnull(S.transaction_uom,'Not Set') AS dd_transaction_uom,
ifnull(S.primary_quantity,0) AS ct_primary_quantity,
ifnull(S.primary_uom,'Not Set') AS dd_primary_uom,
ifnull(S.actual_resource_rate,0) AS amt_actual_resource_rate,
ifnull(S.standard_resource_rate,0) AS amt_standard_resource_rate,
ifnull(S.currency_code,'Not Set') AS dd_currency_code,
1 AS dim_ora_date_currency_conversionid,
ifnull(S.currency_conversion_type,'Not Set') AS dd_currency_conversion_type,
ifnull(S.currency_conversion_rate,0) AS amt_currency_conversion_rate,
ifnull(S.currency_actual_resource_rate,0) AS amt_currency_actual_resource_rate,
ifnull(S.activity_id,0) AS dd_activity_id,
1 AS dim_ora_reasonid,
ifnull(S.reference,'Not Set') AS dd_reference,
ifnull(S.move_transaction_id,0) AS dd_move_transaction_id,
1 AS fact_ora_po_lineid,
ifnull(S.rcv_transaction_id,0) AS dd_rcv_transaction_id,
ifnull(S.attribute_category,'Not Set') AS dd_attribute_category,
ifnull(S.attribute1,'Not Set') AS dd_attribute1,
ifnull(S.attribute2,'Not Set') AS dd_attribute2,
ifnull(S.attribute3,'Not Set') AS dd_attribute3,
ifnull(S.attribute4,'Not Set') AS dd_attribute4,
ifnull(S.attribute5,'Not Set') AS dd_attribute5,
ifnull(S.attribute6,'Not Set') AS dd_attribute6,
ifnull(S.attribute7,'Not Set') AS dd_attribute7,
ifnull(S.attribute8,'Not Set') AS dd_attribute8,
ifnull(S.attribute9,'Not Set') AS dd_attribute9,
ifnull(S.attribute10,'Not Set') AS dd_attribute10,
ifnull(S.attribute11,'Not Set') AS dd_attribute11,
ifnull(S.attribute12,'Not Set') AS dd_attribute12,
ifnull(S.attribute13,'Not Set') AS dd_attribute13,
ifnull(S.attribute14,'Not Set') AS dd_attribute14,
ifnull(S.attribute15,'Not Set') AS dd_attribute15,
ifnull(S.cost_update_id,0) AS dd_cost_update_id,
ifnull(S.pm_cost_collected,'Not Set') AS dd_pm_cost_collected,
ifnull(S.pm_cost_collector_group_id,0) AS dd_pm_cost_collector_group_id,
ifnull(S.project_id,0) AS dd_project_id,
ifnull(S.task_id,0) AS dd_task_id,
ifnull(S.completion_transaction_id,0) AS dd_completion_transaction_id,
ifnull(S.charge_department_id,0) AS dd_charge_department_id,
ifnull(S.instance_id,0) AS dd_instance_id,
ifnull(S.encumbrance_type_id,0) AS dd_encumbrance_type_id,
ifnull(S.encumbrance_amount,0) AS amt_encumbrance_amount,
ifnull(S.encumbrance_quantity,0) AS ct_encumbrance_quantity,
ifnull(S.encumbrance_ccid,0) AS dd_encumbrance_ccid,
ifnull(S.wip_entity_name,'Not Set') AS dd_wip_entity_name,
ifnull(S.entity_type,'Not Set') AS dd_entity_type,
ifnull(S.entity_description,'Not Set') AS dd_entity_description,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_wip_transactions S LEFT JOIN fact_ora_wip_transactions F 
ON F.TRANSACTION_ID =  S.TRANSACTION_ID
WHERE F.TRANSACTION_ID is null;

/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_DATE D ON ansidate(S.LAST_UPDATE_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_last_updated_byid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_last_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_last_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_DATE D ON ansidate(S.CREATION_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_organizationid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_WIP_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_organizationid = D.DIM_ORA_WIP_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_organizationid <> D.DIM_ORA_WIP_PARAMETERSID;

/*UPDATE dim_ora_primary_itemid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_INVPRODUCT D 
ON S.PRIMARY_ITEM_ID = D.INVENTORY_ITEM_ID AND S.ORGANIZATION_ID = D.ORGANIZATION_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_primary_itemid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_primary_itemid <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE dim_ora_acct_periodid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_ORG_ACCT_PERIODS D 
ON ifnull(VARCHAR(S.ACCT_PERIOD_ID,200),'Not Set') + '~' + ifnull(VARCHAR(S.ORGANIZATION_ID, 200),'Not Set') = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_acct_periodid = D.DIM_ORA_ORG_ACCT_PERIODSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_acct_periodid <> D.DIM_ORA_ORG_ACCT_PERIODSID;

/*UPDATE dim_ora_departmentid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_BOM_DEPARTMENTS D ON S.DEPARTMENT_ID = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_departmentid = D.DIM_ORA_BOM_DEPARTMENTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_departmentid <> D.DIM_ORA_BOM_DEPARTMENTSID;

/*UPDATE dim_ora_date_transactionid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_DATE D ON ansidate(S.TRANSACTION_DATE) = D.DATEVALUE   
SET 
F.dim_ora_date_transactionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_date_transactionid <> D.DIM_DATEID;

/*UPDATE dim_ora_lineid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_WIP_LINES D 
ON VARCHAR(S.LINE_ID,200) + '~' +  VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_lineid = D.DIM_ORA_WIP_LINESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_lineid <> D.DIM_ORA_WIP_LINESID;

/*UPDATE dim_ora_source_lineid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_WIP_LINES D
ON VARCHAR(S.SOURCE_LINE_ID,200) + '~' +  VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_source_lineid = D.DIM_ORA_WIP_LINESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_source_lineid <> D.DIM_ORA_WIP_LINESID;

/*UPDATE dim_ora_employeeid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_HR_EMPLOYEES D
ON S.EMPLOYEE_ID = D.PERSON_ID and S.CREATION_DATE BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE and d.rowiscurrent = 1 
SET 
F.dim_ora_employeeid = D.DIM_ORA_HR_EMPLOYEESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_employeeid <> D.DIM_ORA_HR_EMPLOYEESID;

/*UPDATE dim_ora_resourceid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_BOM_RESOURCES D ON S.RESOURCE_ID = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_resourceid = D.DIM_ORA_BOM_RESOURCESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_resourceid <> D.DIM_ORA_BOM_RESOURCESID;

/*UPDATE dim_ora_date_currency_conversionid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_DATE D ON ansidate(S.CURRENCY_CONVERSION_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_currency_conversionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_date_currency_conversionid <> D.DIM_DATEID;

/*UPDATE dim_ora_reasonid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN DIM_ORA_MTL_TRX_REASONS D ON S.REASON_ID = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_reasonid = D.DIM_ORA_MTL_TRX_REASONSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_reasonid <> D.DIM_ORA_MTL_TRX_REASONSID;

/*UPDATE fact_ora_po_lineid*/
UPDATE fact_ora_wip_transactions F
FROM ora_wip_transactions S JOIN FACT_ORA_PURCHASEORDER D 
ON S.PO_HEADER_ID = D.PO_HEADER_ID and S.PO_LINE_ID = D.PO_LINE_ID and d.rowiscurrent = 1 
SET 
F.fact_ora_po_lineid = D.FACT_ORA_PURCHASEORDERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.fact_ora_po_lineid <> D.FACT_ORA_PURCHASEORDERID;