/*amt_freight_cost*/
/*reset amt_freight_cost values*/
UPDATE fact_ORA_SALES_DELIVERY
SET amt_freight_cost = 0;

/*calculate amt_freight_cost*/
DROP IF EXISTS hlp_ora_wsh_freight_cost_tmp;

CREATE TABLE hlp_ora_wsh_freight_cost_tmp as
select DELIVERY_DETAIL_ID ,DELIVERY_ID, sum(TOTAL_AMOUNT*ifnull(conversion_rate,1)) AMT_FREIGHT_COST
from hlp_ora_wsh_freight_cost
where DELIVERY_DETAIL_ID is not null and DELIVERY_ID is not null and rowiscurrent =1
group by DELIVERY_DETAIL_ID ,DELIVERY_ID;

/*update amt_freight_cost*/
UPDATE fact_ORA_SALES_DELIVERY f
FROM hlp_ora_wsh_freight_cost_tmp tmp
set f.amt_freight_cost = tmp.amt_freight_cost
WHERE f.DELIVERY_DETAIL_ID = tmp.DELIVERY_DETAIL_ID and f.DELIVERY_ID = tmp.DELIVERY_ID;

/*drop temp tables*/
DROP TABLE hlp_ora_wsh_freight_cost_tmp;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ORA_SALES_DELIVERY');