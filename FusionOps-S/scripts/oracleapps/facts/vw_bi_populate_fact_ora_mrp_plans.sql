/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mrp_plans*/
/*SELECT * FROM fact_ora_mrp_plans*/

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_mrp_plans';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_mrp_plans',IFNULL(MAX(fact_ora_mrp_plansid),0)
FROM fact_ora_mrp_plans;

/*update fact columns*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S
SET 
dd_curr_operation_schedule_type = S.curr_operation_schedule_type,
dd_current_planner_level = ifnull(S.current_planner_level,0),
dd_planner_batch_number = ifnull(S.planner_batch_number,0),
dd_description = ifnull(S.description,'Not Set'),
dd_update_bom = ifnull(S.update_bom,0),
dd_demand_class = ifnull(S.demand_class,'Not Set'),
dd_attribute_category = ifnull(S.attribute_category,'Not Set'),
dd_attribute1 = ifnull(S.attribute1,'Not Set'),
dd_attribute2 = ifnull(S.attribute2,'Not Set'),
dd_attribute3 = ifnull(S.attribute3,'Not Set'),
dd_attribute4 = ifnull(S.attribute4,'Not Set'),
dd_attribute5 = ifnull(S.attribute5,'Not Set'),
dd_attribute6 = ifnull(S.attribute6,'Not Set'),
dd_attribute7 = ifnull(S.attribute7,'Not Set'),
dd_attribute8 = ifnull(S.attribute8,'Not Set'),
dd_attribute9 = ifnull(S.attribute9,'Not Set'),
dd_attribute10 = ifnull(S.attribute10,'Not Set'),
dd_attribute11 = ifnull(S.attribute11,'Not Set'),
dd_attribute12 = ifnull(S.attribute12,'Not Set'),
dd_attribute13 = ifnull(S.attribute13,'Not Set'),
dd_attribute14 = ifnull(S.attribute14,'Not Set'),
dd_attribute15 = ifnull(S.attribute15,'Not Set'),
dd_curr_resched_assumption = ifnull(S.curr_resched_assumption,0),
dd_resched_assumption = ifnull(S.resched_assumption,0),
dd_use_new_planner = ifnull(S.use_new_planner,0),
dd_curr_full_pegging = ifnull(S.curr_full_pegging,0),
dd_full_pegging = ifnull(S.full_pegging,0),
dd_organization_selection = ifnull(S.organization_selection,0),
dd_curr_reservation_level = ifnull(S.curr_reservation_level,0),
dd_curr_hard_pegging_level = ifnull(S.curr_hard_pegging_level,0),
dd_reservation_level = ifnull(S.reservation_level,0),
dd_hard_pegging_level = ifnull(S.hard_pegging_level,0),
dd_curr_plan_capacity_flag = ifnull(S.curr_plan_capacity_flag,0),
dd_curr_simulation_set = ifnull(S.curr_simulation_set,'Not Set'),
dd_curr_bill_of_resources = ifnull(S.curr_bill_of_resources,'Not Set'),
dd_plan_capacity_flag = ifnull(S.plan_capacity_flag,0),
dd_simulation_set = ifnull(S.simulation_set,'Not Set'),
dd_bill_of_resources = ifnull(S.bill_of_resources,'Not Set'),
dd_min_wf_except_id = ifnull(S.min_wf_except_id,0),
dd_max_wf_except_id = ifnull(S.max_wf_except_id,0),
dd_curr_overwrite_option = S.curr_overwrite_option,
dd_curr_append_planned_orders = S.curr_append_planned_orders,
dd_curr_planning_time_fence_flag = S.curr_planning_time_fence_flag,
dd_curr_demand_time_fence_flag = S.curr_demand_time_fence_flag,
dd_curr_consider_reservations = S.curr_consider_reservations,
dd_curr_plan_safety_stock = S.curr_plan_safety_stock,
dd_curr_consider_wip = S.curr_consider_wip,
dd_curr_consider_po = S.curr_consider_po,
dd_curr_snapshot_lock = S.curr_snapshot_lock,
dd_operation_schedule_type = ifnull(S.operation_schedule_type,0),
dd_overwrite_option = ifnull(S.overwrite_option,0),
dd_append_planned_orders = ifnull(S.append_planned_orders,0),
dd_planning_time_fence_flag = ifnull(S.planning_time_fence_flag,0),
dd_demand_time_fence_flag = ifnull(S.demand_time_fence_flag,0),
dd_consider_reservations = ifnull(S.consider_reservations,0),
dd_plan_safety_stock = ifnull(S.plan_safety_stock,0),
dd_consider_wip = ifnull(S.consider_wip,0),
dd_consider_po = ifnull(S.consider_po,0),
dd_snapshot_lock = ifnull(S.snapshot_lock,0),
amt_exchangerate = 1,
amt_exchangerate_gbl = 1,
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE
F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID;

/*insert new rows*/
INSERT INTO fact_ora_mrp_plans
(
fact_ora_mrp_plansid,
dd_curr_operation_schedule_type,dim_ora_date_data_completionid,dim_ora_date_crp_plan_startid,
dd_current_planner_level,dd_planner_batch_number,dd_description,dd_update_bom,dd_demand_class,
dd_attribute_category,dd_attribute1,dd_attribute2,dd_attribute3,dd_attribute4,dd_attribute5,
dd_attribute6,dd_attribute7,dd_attribute8,dd_attribute9,dd_attribute10,dd_attribute11,
dd_attribute12,dd_attribute13,dd_attribute14,dd_attribute15,dd_curr_resched_assumption,
dd_resched_assumption,dim_ora_date_curr_user_plan_startid,dim_ora_date_user_plan_startid,
dim_ora_date_online_planner_startid,dim_ora_date_online_planner_completionid,dd_use_new_planner,dd_curr_full_pegging,
dd_full_pegging,dim_ora_assignment_setid,dim_ora_curr_assignment_setid,dd_organization_selection,
dd_curr_reservation_level,dd_curr_hard_pegging_level,dd_reservation_level,dd_hard_pegging_level,
dd_curr_plan_capacity_flag,dd_curr_simulation_set,dd_curr_bill_of_resources,dd_plan_capacity_flag,
dd_simulation_set,dd_bill_of_resources,dd_min_wf_except_id,dd_max_wf_except_id,dim_ora_organizationid,
dim_ora_compile_designatorid,dim_ora_date_last_updateid,dim_ora_last_updated_byid,dim_ora_date_creationid,
dim_ora_created_byid,dim_ora_curr_schedule_designatorid,dim_ora_curr_plan_typeid,dd_curr_overwrite_option,
dd_curr_append_planned_orders,dim_ora_curr_schedule_typeid,dim_ora_date_curr_cutoffid,dim_ora_curr_part_include_typeid,
dd_curr_planning_time_fence_flag,dd_curr_demand_time_fence_flag,dd_curr_consider_reservations,dd_curr_plan_safety_stock,
dd_curr_consider_wip,dd_curr_consider_po,dd_curr_snapshot_lock,dim_ora_date_compile_definitionid,
dim_ora_schedule_designatorid,dd_operation_schedule_type,dim_ora_plan_typeid,dd_overwrite_option,dd_append_planned_orders,
dim_ora_schedule_typeid,dim_ora_date_cutoffid,dim_ora_part_include_typeid,dd_planning_time_fence_flag,
dd_demand_time_fence_flag,dd_consider_reservations,dd_plan_safety_stock,dd_consider_wip,dd_consider_po,
dd_snapshot_lock,dim_ora_date_explosion_startid,dim_ora_date_explosion_completionid,
dim_ora_date_data_startid,dim_ora_date_plan_startid,dim_ora_date_crp_plan_completionid,dim_ora_date_plan_completionid,
organization_id,compile_designator,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_mrp_plans' ),0) + ROW_NUMBER() OVER() fact_ora_mrp_plansid,
S.curr_operation_schedule_type AS dd_curr_operation_schedule_type,
1 AS dim_ora_date_data_completionid,
1 AS dim_ora_date_crp_plan_startid,
ifnull(S.current_planner_level,0) AS dd_current_planner_level,
ifnull(S.planner_batch_number,0) AS dd_planner_batch_number,
ifnull(S.description,'Not Set') AS dd_description,
ifnull(S.update_bom,0) AS dd_update_bom,
ifnull(S.demand_class,'Not Set') AS dd_demand_class,
ifnull(S.attribute_category,'Not Set') AS dd_attribute_category,
ifnull(S.attribute1,'Not Set') AS dd_attribute1,
ifnull(S.attribute2,'Not Set') AS dd_attribute2,
ifnull(S.attribute3,'Not Set') AS dd_attribute3,
ifnull(S.attribute4,'Not Set') AS dd_attribute4,
ifnull(S.attribute5,'Not Set') AS dd_attribute5,
ifnull(S.attribute6,'Not Set') AS dd_attribute6,
ifnull(S.attribute7,'Not Set') AS dd_attribute7,
ifnull(S.attribute8,'Not Set') AS dd_attribute8,
ifnull(S.attribute9,'Not Set') AS dd_attribute9,
ifnull(S.attribute10,'Not Set') AS dd_attribute10,
ifnull(S.attribute11,'Not Set') AS dd_attribute11,
ifnull(S.attribute12,'Not Set') AS dd_attribute12,
ifnull(S.attribute13,'Not Set') AS dd_attribute13,
ifnull(S.attribute14,'Not Set') AS dd_attribute14,
ifnull(S.attribute15,'Not Set') AS dd_attribute15,
ifnull(S.curr_resched_assumption,0) AS dd_curr_resched_assumption,
ifnull(S.resched_assumption,0) AS dd_resched_assumption,
1 AS dim_ora_date_curr_user_plan_startid,
1 AS dim_ora_date_user_plan_startid,
1 AS dim_ora_date_online_planner_startid,
1 AS dim_ora_date_online_planner_completionid,
ifnull(S.use_new_planner,0) AS dd_use_new_planner,
ifnull(S.curr_full_pegging,0) AS dd_curr_full_pegging,
ifnull(S.full_pegging,0) AS dd_full_pegging,
1 AS dim_ora_assignment_setid,
1 AS dim_ora_curr_assignment_setid,
ifnull(S.organization_selection,0) AS dd_organization_selection,
ifnull(S.curr_reservation_level,0) AS dd_curr_reservation_level,
ifnull(S.curr_hard_pegging_level,0) AS dd_curr_hard_pegging_level,
ifnull(S.reservation_level,0) AS dd_reservation_level,
ifnull(S.hard_pegging_level,0) AS dd_hard_pegging_level,
ifnull(S.curr_plan_capacity_flag,0) AS dd_curr_plan_capacity_flag,
ifnull(S.curr_simulation_set,'Not Set') AS dd_curr_simulation_set,
ifnull(S.curr_bill_of_resources,'Not Set') AS dd_curr_bill_of_resources,
ifnull(S.plan_capacity_flag,0) AS dd_plan_capacity_flag,
ifnull(S.simulation_set,'Not Set') AS dd_simulation_set,
ifnull(S.bill_of_resources,'Not Set') AS dd_bill_of_resources,
ifnull(S.min_wf_except_id,0) AS dd_min_wf_except_id,
ifnull(S.max_wf_except_id,0) AS dd_max_wf_except_id,
1 AS dim_ora_organizationid,
1 AS dim_ora_compile_designatorid,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_last_updated_byid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_created_byid,
1 AS dim_ora_curr_schedule_designatorid,
1 AS dim_ora_curr_plan_typeid,
S.curr_overwrite_option AS dd_curr_overwrite_option,
S.curr_append_planned_orders AS dd_curr_append_planned_orders,
1 AS dim_ora_curr_schedule_typeid,
1 AS dim_ora_date_curr_cutoffid,
1 AS dim_ora_curr_part_include_typeid,
S.curr_planning_time_fence_flag AS dd_curr_planning_time_fence_flag,
S.curr_demand_time_fence_flag AS dd_curr_demand_time_fence_flag,
S.curr_consider_reservations AS dd_curr_consider_reservations,
S.curr_plan_safety_stock AS dd_curr_plan_safety_stock,
S.curr_consider_wip AS dd_curr_consider_wip,
S.curr_consider_po AS dd_curr_consider_po,
S.curr_snapshot_lock AS dd_curr_snapshot_lock,
1 AS dim_ora_date_compile_definitionid,
1 AS dim_ora_schedule_designatorid,
ifnull(S.operation_schedule_type,0) AS dd_operation_schedule_type,
1 AS dim_ora_plan_typeid,
ifnull(S.overwrite_option,0) AS dd_overwrite_option,
ifnull(S.append_planned_orders,0) AS dd_append_planned_orders,
1 AS dim_ora_schedule_typeid,
1 AS dim_ora_date_cutoffid,
1 AS dim_ora_part_include_typeid,
ifnull(S.planning_time_fence_flag,0) AS dd_planning_time_fence_flag,
ifnull(S.demand_time_fence_flag,0) AS dd_demand_time_fence_flag,
ifnull(S.consider_reservations,0) AS dd_consider_reservations,
ifnull(S.plan_safety_stock,0) AS dd_plan_safety_stock,
ifnull(S.consider_wip,0) AS dd_consider_wip,
ifnull(S.consider_po,0) AS dd_consider_po,
ifnull(S.snapshot_lock,0) AS dd_snapshot_lock,
1 AS dim_ora_date_explosion_startid,
1 AS dim_ora_date_explosion_completionid,
1 AS dim_ora_date_data_startid,
1 AS dim_ora_date_plan_startid,
1 AS dim_ora_date_crp_plan_completionid,
1 AS dim_ora_date_plan_completionid,
S.organization_id AS organization_id,
S.compile_designator AS compile_designator,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,

current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'INSERT' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mrp_plans S LEFT JOIN fact_ora_mrp_plans F ON
F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID 
WHERE F.COMPILE_DESIGNATOR  is null and F.ORGANIZATION_ID is null;

/*UPDATE dim_ora_date_data_completionid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ansidate(S.data_completion_date) = D.datevalue
SET 
F.dim_ora_date_data_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_data_completionid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_crp_plan_startid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.crp_plan_start_date) = D.datevalue
SET 
F.dim_ora_date_crp_plan_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_crp_plan_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_curr_user_plan_startid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.curr_user_plan_start_date) = D.datevalue
SET 
F.dim_ora_date_curr_user_plan_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_curr_user_plan_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_user_plan_startid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.user_plan_start_date) = D.datevalue
SET 
F.dim_ora_date_user_plan_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_user_plan_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_online_planner_startid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.online_planner_start_date) = D.datevalue
SET 
F.dim_ora_date_online_planner_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_online_planner_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_online_planner_completionid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.online_planner_completion_date) = D.datevalue
SET 
F.dim_ora_date_online_planner_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_online_planner_completionid <> D.DIM_DATEID;

/*UPDATE dim_ora_assignment_setid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_MRP_ASSIGNMENTS D ON S.assignment_set_id = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_assignment_setid = D.DIM_ORA_MRP_ASSIGNMENTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_assignment_setid <> D.DIM_ORA_MRP_ASSIGNMENTSID;

/*UPDATE dim_ora_curr_assignment_setid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_MRP_ASSIGNMENTS D ON S.curr_assignment_set_id= D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_curr_assignment_setid = D.DIM_ORA_MRP_ASSIGNMENTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_curr_assignment_setid <> D.DIM_ORA_MRP_ASSIGNMENTSID;

/*UPDATE dim_ora_organizationid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_MTL_PARAMETERS D ON S.organization_id = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_organizationid = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_organizationid <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE dim_ora_compile_designatorid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_MRP_DESIGNATORS D
ON VARCHAR(S.COMPILE_DESIGNATOR, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_compile_designatorid = D.DIM_ORA_MRP_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_compile_designatorid <> D.DIM_ORA_MRP_DESIGNATORSID;

/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.last_update_date) = D.datevalue
SET 
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_last_updated_byid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_FNDUSER D ON S.last_updated_by = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_last_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_last_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.creation_date) = D.datevalue
SET 
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_FNDUSER D ON S.created_by = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_curr_schedule_designatorid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_MRP_SCHEDULE_DESIGNATORS D
ON VARCHAR(S.curr_schedule_designator, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_curr_schedule_designatorid = D.DIM_ORA_MRP_SCHEDULE_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_curr_schedule_designatorid <> D.DIM_ORA_MRP_SCHEDULE_DESIGNATORSID;

/*UPDATE dim_ora_curr_plan_typeid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_FND_LOOKUP D
ON S.curr_plan_type = D.LOOKUP_CODE AND D.LOOKUP_TYPE = 'MRP_PLAN_TYPE' and d.rowiscurrent = 1 
SET 
F.dim_ora_curr_plan_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_curr_plan_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_curr_schedule_typeid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_FND_LOOKUP D
ON S.curr_schedule_type = D.LOOKUP_CODE AND D.LOOKUP_TYPE = 'MRP_SCHEDULE_TYPE' and d.rowiscurrent = 1 
SET 
F.dim_ora_curr_schedule_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_curr_schedule_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_date_curr_cutoffid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.curr_cutoff_date) = D.datevalue
SET 
F.dim_ora_date_curr_cutoffid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_curr_cutoffid <> D.DIM_DATEID;

/*UPDATE dim_ora_curr_part_include_typeid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_FND_LOOKUP D
ON S.curr_part_include_type = D.LOOKUP_CODE AND D.LOOKUP_TYPE = 'MRP_PART_INCLUDE_TYPE' and d.rowiscurrent = 1 
SET 
F.dim_ora_curr_part_include_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_curr_part_include_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_date_compile_definitionid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.compile_definition_date) = D.datevalue
SET 
F.dim_ora_date_compile_definitionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_compile_definitionid <> D.DIM_DATEID;

/*UPDATE dim_ora_schedule_designatorid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_MRP_SCHEDULE_DESIGNATORS D 
ON VARCHAR(S.SCHEDULE_DESIGNATOR, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_schedule_designatorid = D.DIM_ORA_MRP_SCHEDULE_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_schedule_designatorid <> D.DIM_ORA_MRP_SCHEDULE_DESIGNATORSID;

/*UPDATE dim_ora_plan_typeid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_FND_LOOKUP D ON S.plan_type = D.LOOKUP_CODE AND D.LOOKUP_TYPE = 'MRP_PLAN_TYPE' and d.rowiscurrent = 1
SET 
F.dim_ora_plan_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_plan_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_schedule_typeid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_FND_LOOKUP D ON S.schedule_type = D.LOOKUP_CODE AND D.LOOKUP_TYPE = 'MRP_SCHEDULE_TYPE' and d.rowiscurrent = 1
SET 
F.dim_ora_schedule_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_schedule_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_date_cutoffid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.cutoff_date) = D.datevalue
SET 
F.dim_ora_date_cutoffid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_cutoffid <> D.DIM_DATEID;

/*UPDATE dim_ora_part_include_typeid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_ORA_FND_LOOKUP D ON S.part_include_type = D.LOOKUP_CODE AND D.LOOKUP_TYPE = 'MRP_PART_INCLUDE_TYPE' and d.rowiscurrent = 1
SET 
F.dim_ora_part_include_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_part_include_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_date_explosion_startid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.explosion_start_date) = D.datevalue
SET 
F.dim_ora_date_explosion_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_explosion_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_explosion_completionid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.explosion_completion_date) = D.datevalue
SET 
F.dim_ora_date_explosion_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_explosion_completionid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_data_startid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.data_start_date) = D.datevalue
SET 
F.dim_ora_date_data_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_data_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_plan_startid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.plan_start_date) = D.datevalue
SET 
F.dim_ora_date_plan_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_plan_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_crp_plan_completionid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.crp_plan_completion_date) = D.datevalue
SET 
F.dim_ora_date_crp_plan_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_crp_plan_completionid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_plan_completionid*/
UPDATE fact_ora_mrp_plans F
FROM ora_mrp_plans S JOIN DIM_DATE D ON ANSIDATE(S.plan_completion_date) = D.datevalue
SET 
F.dim_ora_date_plan_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.COMPILE_DESIGNATOR =  S.COMPILE_DESIGNATOR AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.dim_ora_date_plan_completionid <> D.DIM_DATEID;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_mrp_plans');