/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mrp_available_to_promise*/
/*SELECT * FROM fact_ora_mrp_available_to_promise*/


/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_mrp_available_to_promise';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_mrp_available_to_promise',IFNULL(MAX(fact_ora_mrp_available_to_promiseid),0) 
FROM fact_ora_mrp_available_to_promise;

/*update fact columns*/
UPDATE fact_ora_mrp_available_to_promise T	
FROM ora_mrp_available_to_promise S
SET 
DD_STATUS = ifnull(S.STATUS,0),
DD_UPDATED = ifnull(S.UPDATED,0),
CT_QUANTITY_AVAILABLE = S.QUANTITY_AVAILABLE,
TRANSACTION_ID = S.TRANSACTION_ID,
amt_exchangerate = 1,
amt_exchangerate_gbl = 1,
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE
T.TRANSACTION_ID = S.TRANSACTION_ID;

/*insert new rows*/
INSERT INTO fact_ora_mrp_available_to_promise
(
fact_ora_mrp_available_to_promiseid,
DD_STATUS,DD_UPDATED,CT_QUANTITY_AVAILABLE,DIM_ORA_DATE_SCHEDULEID,
DIM_ORA_ORGANIZATIONID,DIM_ORA_COMPILE_DESIGNATORID,DIM_ORA_INVENTORY_ITEMID,DIM_ORA_CREATED_BYID,
DIM_ORA_DATE_CREATIONID,DIM_ORA_LAST_UPDATED_BYID,DIM_ORA_DATE_LAST_UPDATEID,TRANSACTION_ID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_mrp_available_to_promise' ),0) + ROW_NUMBER() OVER() fact_ora_mrp_available_to_promiseid,
ifnull(S.STATUS,0) AS DD_STATUS,
ifnull(S.UPDATED,0) AS DD_UPDATED,
S.QUANTITY_AVAILABLE AS CT_QUANTITY_AVAILABLE,
1 AS DIM_ORA_DATE_SCHEDULEID,
1 AS DIM_ORA_ORGANIZATIONID,
1 AS DIM_ORA_COMPILE_DESIGNATORID,
1 AS DIM_ORA_INVENTORY_ITEMID,
1 AS DIM_ORA_CREATED_BYID,
1 AS DIM_ORA_DATE_CREATIONID,
1 AS DIM_ORA_LAST_UPDATED_BYID,
1 AS DIM_ORA_DATE_LAST_UPDATEID,
S.TRANSACTION_ID AS TRANSACTION_ID,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'INSERT' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mrp_available_to_promise S 
LEFT JOIN fact_ora_mrp_available_to_promise F ON
F.TRANSACTION_ID = S.TRANSACTION_ID
WHERE F.TRANSACTION_ID is null;

/*UPDATE DIM_ORA_DATE_SCHEDULEID*/
UPDATE fact_ora_mrp_available_to_promise F
FROM ora_mrp_available_to_promise S JOIN DIM_DATE D ON ansidate(S.SCHEDULE_DATE) = D.datevalue 
SET 
F.DIM_ORA_DATE_SCHEDULEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID AND 
F.DIM_ORA_DATE_SCHEDULEID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_ORGANIZATIONID*/
UPDATE fact_ora_mrp_available_to_promise F
FROM ora_mrp_available_to_promise S JOIN DIM_ORA_MTL_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1
SET 
F.DIM_ORA_ORGANIZATIONID = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID AND 
F.DIM_ORA_ORGANIZATIONID <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE DIM_ORA_COMPILE_DESIGNATORID*/
UPDATE fact_ora_mrp_available_to_promise F
FROM ora_mrp_available_to_promise S JOIN DIM_ORA_MRP_DESIGNATORS D 
ON VARCHAR(S.COMPILE_DESIGNATOR,200)  +'~' + VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_COMPILE_DESIGNATORID = D.DIM_ORA_MRP_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID AND 
F.DIM_ORA_COMPILE_DESIGNATORID <> D.DIM_ORA_MRP_DESIGNATORSID;

/*UPDATE DIM_ORA_INVENTORY_ITEMID*/
UPDATE fact_ora_mrp_available_to_promise F
FROM ora_mrp_available_to_promise S JOIN DIM_ORA_MRP_SYSTEM_ITEMS D 
ON VARCHAR(S.COMPILE_DESIGNATOR,200) + '~' + VARCHAR(S.ORGANIZATION_ID,200) + '~' + VARCHAR(S.INVENTORY_ITEM_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_INVENTORY_ITEMID = D.DIM_ORA_MRP_SYSTEM_ITEMSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID AND 
F.DIM_ORA_INVENTORY_ITEMID <> D.DIM_ORA_MRP_SYSTEM_ITEMSID;

/*UPDATE DIM_ORA_CREATED_BYID*/
UPDATE fact_ora_mrp_available_to_promise F
FROM ora_mrp_available_to_promise S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_CREATED_BYID = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID AND 
F.DIM_ORA_CREATED_BYID <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_DATE_CREATIONID*/
UPDATE fact_ora_mrp_available_to_promise F
FROM ora_mrp_available_to_promise S JOIN DIM_DATE D ON ansidate(S.CREATION_DATE) = D.datevalue 
SET 
F.DIM_ORA_DATE_CREATIONID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID AND 
F.DIM_ORA_DATE_CREATIONID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_LAST_UPDATED_BYID*/
UPDATE fact_ora_mrp_available_to_promise F
FROM ora_mrp_available_to_promise S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_LAST_UPDATED_BYID = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID AND 
F.DIM_ORA_LAST_UPDATED_BYID <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_DATE_LAST_UPDATEID*/
UPDATE fact_ora_mrp_available_to_promise F
FROM ora_mrp_available_to_promise S JOIN DIM_DATE D ON ansidate(S.LAST_UPDATE_DATE) = D.datevalue 
SET 
F.DIM_ORA_DATE_LAST_UPDATEID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID AND 
F.DIM_ORA_DATE_LAST_UPDATEID <> D.DIM_DATEID;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_mrp_available_to_promise');