/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mrp_reservations*/
/*SELECT * FROM fact_ora_mrp_reservations*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_mrp_reservations';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_mrp_reservations',IFNULL(MAX(fact_ora_mrp_reservationsid),0)
FROM fact_ora_mrp_reservations;	

/*update fact columns*/
UPDATE fact_ora_mrp_reservations F	
FROM ora_mrp_reservations S
SET 
ct_reserved_quantity = S.RESERVED_QUANTITY,
ct_nonnet_quantity_reserved = S.NONNET_QUANTITY_RESERVED,
dd_disposition_type = S.DISPOSITION_TYPE,
dd_parent_demand_id = S.PARENT_DEMAND_ID,
dd_demand_class = ifnull(S.DEMAND_CLASS,'Not Set'),
dd_revision = ifnull(S.REVISION,'Not Set'),
dd_planning_group = ifnull(S.PLANNING_GROUP,'Not Set'),
amt_exchangerate = 1,
amt_exchangerate_gbl = 1,
DW_UPDATE_DATE = current_timestamp, 
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE	
F.TRANSACTION_ID =  S.TRANSACTION_ID; 

/*insert new rows*/
INSERT INTO fact_ora_mrp_reservations
(
fact_ora_mrp_reservationsid,
TRANSACTION_ID,dim_ora_date_last_updateid,dim_ora_last_updatebyid,
dim_ora_date_creationid,dim_ora_date_created_byid,dim_ora_inventory_itemid,dim_ora_compile_designatorid,dim_ora_organizationid,
dim_ora_date_reservationid,dim_ora_date_requirementid,ct_reserved_quantity,ct_nonnet_quantity_reserved,dd_disposition_type,
dim_ora_dispositionid,dd_parent_demand_id,dim_ora_subinventoryid,dd_demand_class,dd_revision,dd_planning_group,
amt_exchangerate,amt_exchangerate_gbl,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_mrp_reservations' ),0) + ROW_NUMBER() OVER() fact_ora_mrp_reservationsid,
S.TRANSACTION_ID AS TRANSACTION_ID,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_last_updatebyid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_date_created_byid,
1 AS dim_ora_inventory_itemid,
1 AS dim_ora_compile_designatorid,
1 AS dim_ora_organizationid,
1 AS dim_ora_date_reservationid,
1 AS dim_ora_date_requirementid,
S.RESERVED_QUANTITY AS ct_reserved_quantity,
S.NONNET_QUANTITY_RESERVED AS ct_nonnet_quantity_reserved,
S.DISPOSITION_TYPE AS dd_disposition_type,
1 AS dim_ora_dispositionid,
S.PARENT_DEMAND_ID AS dd_parent_demand_id,
1 AS dim_ora_subinventoryid,
ifnull(S.DEMAND_CLASS,'Not Set') AS dd_demand_class,
ifnull(S.REVISION,'Not Set') AS dd_revision,
ifnull(S.PLANNING_GROUP,'Not Set') AS dd_planning_group,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mrp_reservations S LEFT JOIN fact_ora_mrp_reservations F
ON F.TRANSACTION_ID = S.TRANSACTION_ID
WHERE F.TRANSACTION_ID is null;	

/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_DATE D ON ANSIDATE(S.LAST_UPDATE_DATE) = D.datevalue
SET 
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_last_updatebyid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_last_updatebyid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_last_updatebyid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_DATE D ON ANSIDATE(S.CREATION_DATE) = D.datevalue  
SET 
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_created_byid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_date_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_date_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_inventory_itemid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_ORA_MRP_SYSTEM_ITEMS D 
ON VARCHAR(S.COMPILE_DESIGNATOR,200) + '~' + VARCHAR(S.ORGANIZATION_ID,200) + '~' + VARCHAR(S.INVENTORY_ITEM_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_inventory_itemid = D.DIM_ORA_MRP_SYSTEM_ITEMSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_inventory_itemid <> D.DIM_ORA_MRP_SYSTEM_ITEMSID;

/*UPDATE dim_ora_compile_designatorid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_ORA_MRP_DESIGNATORS D 
ON VARCHAR(S.COMPILE_DESIGNATOR,200)  +'~' + VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_compile_designatorid = D.DIM_ORA_MRP_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_compile_designatorid <> D.DIM_ORA_MRP_DESIGNATORSID;

/*UPDATE dim_ora_organizationid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_ORA_MTL_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_organizationid = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_organizationid <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE dim_ora_date_reservationid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_DATE D ON ANSIDATE(S.RESERVATION_DATE) = D.datevalue  
SET 
F.dim_ora_date_reservationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_date_reservationid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_requirementid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_DATE D ON ANSIDATE(S.REQUIREMENT_DATE) = D.datevalue  
SET 
F.dim_ora_date_requirementid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_date_requirementid <> D.DIM_DATEID;

/*UPDATE dim_ora_dispositionid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_ORA_MTL_SALESORDERS D ON S.DISPOSITION_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_dispositionid = D.DIM_ORA_MTL_SALESORDERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_dispositionid <> D.DIM_ORA_MTL_SALESORDERSID;

/*UPDATE dim_ora_subinventoryid*/
UPDATE fact_ora_mrp_reservations F
FROM ora_mrp_reservations S JOIN DIM_ORA_MRP_SUBINVENTORIES D 
ON VARCHAR(S.SUBINVENTORY, 200) + '~' + VARCHAR(S.ORGANIZATION_ID, 200) + '~' +VARCHAR(S.COMPILE_DESIGNATOR, 200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_subinventoryid = D.DIM_ORA_MRP_SUBINVENTORIESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID AND 
F.dim_ora_subinventoryid <> D.DIM_ORA_MRP_SUBINVENTORIESID;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_mrp_reservations');