/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_purchaserequisitioncost*/
/*SELECT * FROM fact_ora_purchaserequisitioncost*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_purchaserequisitioncost';
	
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_purchaserequisitioncost',IFNULL(MAX(fact_ora_purchaserequisitioncostid),0)
FROM fact_ora_purchaserequisitioncost;


/*update fact columns*/
UPDATE fact_ora_purchaserequisitioncost F 
FROM ORA_PURCH_REQ_LINE_COST S
SET
dd_authorization_status = ifnull(S.AUTHORIZATION_STATUS,'Not Set'),
dd_closed_code = ifnull(S.CLOSED_CODE,'Not Set'),
dd_source_type_code = ifnull(S.SOURCE_TYPE_CODE,'Not Set'),
ct_unit_price = ifnull(S.UNIT_PRICE,0),
ct_quantity = ifnull(S.QUANTITY,0),
dd_unit_measure_lookup_code = ifnull(S.UNIT_MEAS_LOOKUP_CODE,'Not Set'),
dd_currency_code = ifnull(S.PR_DOC_CURR,'Not Set'),
ct_rate = ifnull(S.RATE,1),
dd_segment1 = S.SEGMENT1,
dd_line_num = S.LINE_NUM,
dd_item_description = S.ITEM_DESCRIPTION,
requisition_line_id = S.REQUISITION_LINE_ID,
dd_attribute14 = ifnull(S.ATTRIBUTE14,'Not Set'),
dd_attribute1 = ifnull(S.ATTRIBUTE1,'Not Set'),
dd_distribution_num = S.DISTRIBUTION_NUM,
distribution_id = S.DISTRIBUTION_ID,
ct_req_line_quantity = ifnull(S.REQ_LINE_QUANTITY,0),
dd_allocation_type = ifnull(S.ALLOCATION_TYPE,'Not Set'),
amt_allocation_value = ifnull(S.ALLOCATION_VALUE,0),
dd_type_lookup_code = ifnull(S.TYPE_LOOKUP_CODE,'Not Set'),
dd_cancel_flag = ifnull(S.CANCEL_FLAG,'Not Set'),
dd_modified_by_agent_flag = ifnull(S.MODIFIED_BY_AGENT_FLAG,'Not Set'),
dd_matching_basis = S.MATCHING_BASIS,
amt_req_line_amount = ifnull(S.REQ_LINE_AMOUNT,0),
dd_rate_type = ifnull(S.RATE_TYPE,'Not Set'),
DW_UPDATE_DATE = current_timestamp, 
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE
F.DISTRIBUTION_ID = S.DISTRIBUTION_ID;

/*insert new rows*/
INSERT INTO fact_ora_purchaserequisitioncost
(
fact_ora_purchaserequisitioncostid,dim_ora_to_personid,dim_ora_vendorid,
dim_ora_vendorsiteid,dim_ora_inv_productid,dd_authorization_status,dd_closed_code,dd_source_type_code,dim_ora_deliver_to_locationid,
dim_ora_date_need_byid,ct_unit_price,ct_quantity,dd_unit_measure_lookup_code,dd_currency_code,ct_rate,dd_segment1,dd_line_num,
dd_item_description,requisition_line_id,dd_attribute14,dim_ora_set_of_booksid,dim_ora_gl_code_combinationid,dd_attribute1,dim_ora_orgid,
dd_distribution_num,distribution_id,ct_req_line_quantity,dim_ora_date_pr_distribution_updateid,dim_ora_pr_distribution_update_byid,
dim_ora_date_pr_distribution_createdid,dim_ora_pr_distribution_created_byid,dd_allocation_type,amt_allocation_value,dim_ora_accrual_accountid,
dim_ora_variance_accountid,dim_ora_destination_orgid,dd_type_lookup_code,dim_ora_date_pr_submittedid,dim_ora_date_pr_approvalid,
dd_cancel_flag,dim_ora_date_pr_line_updateid,dim_ora_date_pr_header_updateid,dim_ora_date_line_location_updateid,dim_ora_line_typeid,dim_ora_categoryid,
dd_modified_by_agent_flag,dd_matching_basis,amt_req_line_amount,dd_rate_type,dim_ora_date_promisedid,dim_ora_date_po_line_loc_need_byid,
dd_functional_curr_code,amt_exchangerate,amt_exchangerate_gbl,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_purchaserequisitioncost' ),0) + ROW_NUMBER() OVER() fact_ora_purchaserequisitioncostid,
1 AS dim_ora_to_personid,
1 AS dim_ora_vendorid,
1 AS dim_ora_vendorsiteid,
1 AS dim_ora_inv_productid,
ifnull(S.AUTHORIZATION_STATUS,'Not Set') AS dd_authorization_status,
ifnull(S.CLOSED_CODE,'Not Set') AS dd_closed_code,
ifnull(S.SOURCE_TYPE_CODE,'Not Set') AS dd_source_type_code,
1 AS dim_ora_deliver_to_locationid,
1 AS dim_ora_date_need_byid,
ifnull(S.UNIT_PRICE,0) AS ct_unit_price,
ifnull(S.QUANTITY,0) AS ct_quantity,
ifnull(S.UNIT_MEAS_LOOKUP_CODE,'Not Set') AS dd_unit_measure_lookup_code,
ifnull(S.PR_DOC_CURR,'Not Set') AS dd_currency_code,
ifnull(S.RATE,1) AS ct_rate,
S.SEGMENT1 AS dd_segment1,
S.LINE_NUM AS dd_line_num,
S.ITEM_DESCRIPTION AS dd_item_description,
S.REQUISITION_LINE_ID AS requisition_line_id,
ifnull(S.ATTRIBUTE14,'Not Set') AS dd_attribute14,
1 AS dim_ora_set_of_booksid,
1 AS dim_ora_gl_code_combinationid,
ifnull(S.ATTRIBUTE1,'Not Set') AS dd_attribute1,
1 AS dim_ora_orgid,
S.DISTRIBUTION_NUM AS dd_distribution_num,
S.DISTRIBUTION_ID AS distribution_id,
ifnull(S.REQ_LINE_QUANTITY,0) AS ct_req_line_quantity,
1 AS dim_ora_date_pr_distribution_updateid,
1 AS dim_ora_pr_distribution_update_byid,
1 AS dim_ora_date_pr_distribution_createdid,
1 AS dim_ora_pr_distribution_created_byid,
ifnull(S.ALLOCATION_TYPE,'Not Set') AS dd_allocation_type,
ifnull(S.ALLOCATION_VALUE,0) AS amt_allocation_value,
1 AS dim_ora_accrual_accountid,
1 AS dim_ora_variance_accountid,
1 AS dim_ora_destination_orgid,
ifnull(S.TYPE_LOOKUP_CODE,'Not Set') AS dd_type_lookup_code,
1 AS dim_ora_date_pr_submittedid,
1 AS dim_ora_date_pr_approvalid,
ifnull(S.CANCEL_FLAG,'Not Set') AS dd_cancel_flag,
1 AS dim_ora_date_pr_line_updateid,
1 AS dim_ora_date_pr_header_updateid,
1 AS dim_ora_date_line_location_updateid,
1 AS dim_ora_line_typeid,
1 AS dim_ora_categoryid,
ifnull(S.MODIFIED_BY_AGENT_FLAG,'Not Set') AS dd_modified_by_agent_flag,
S.MATCHING_BASIS AS dd_matching_basis,
ifnull(S.REQ_LINE_AMOUNT,0) AS amt_req_line_amount,
ifnull(S.RATE_TYPE,'Not Set') AS dd_rate_type,
1 AS dim_ora_date_promisedid,
1 AS dim_ora_date_po_line_loc_need_byid,
'Not Set' AS DD_FUNCTIONAL_CURR_CODE,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ORA_PURCH_REQ_LINE_COST S LEFT JOIN fact_ora_purchaserequisitioncost F 
ON F.DISTRIBUTION_ID = S.DISTRIBUTION_ID
WHERE F.DISTRIBUTION_ID is null;

/*UPDATE dim_ora_to_personid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_HR_EMPLOYEES D 
ON S.TO_PERSON_ID = D.PERSON_ID and S.CREATION_DATE BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE and d.rowiscurrent = 1
SET 
F.dim_ora_to_personid = D.DIM_ORA_HR_EMPLOYEESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_to_personid <> D.DIM_ORA_HR_EMPLOYEESID;

/*UPDATE dim_ora_vendorid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_PO_VENDORS D ON S.VENDOR_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_vendorid = D.DIM_ORA_PO_VENDORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_vendorid <> D.DIM_ORA_PO_VENDORSID;

/*UPDATE dim_ora_vendorsiteid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_PO_VENDORSITES D ON S.VENDOR_SITE_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_vendorsiteid = D.DIM_ORA_PO_VENDORSITESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_vendorsiteid <> D.DIM_ORA_PO_VENDORSITESID;

/*UPDATE dim_ora_inv_productid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_INVPRODUCT D 
ON S.ITEM_ID = D.INVENTORY_ITEM_ID  AND S.ORG_ID = D.ORGANIZATION_ID  and d.rowiscurrent = 1 
SET 
F.dim_ora_inv_productid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_inv_productid <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE dim_ora_deliver_to_locationid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_HR_LOCATIONS D ON S.DELIVER_TO_LOCATION_ID = D.KEY_ID  and d.rowiscurrent = 1
SET 
F.dim_ora_deliver_to_locationid = D.DIM_ORA_HR_LOCATIONSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_deliver_to_locationid <> D.DIM_ORA_HR_LOCATIONSID;

/*UPDATE dim_ora_date_need_byid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.NEED_BY_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_need_byid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_need_byid <> D.DIM_DATEID;

/*UPDATE dim_ora_set_of_booksid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_GL_SETOFBOOKS D ON S.SET_OF_BOOKS_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_set_of_booksid = D.DIM_ORA_GL_SETOFBOOKSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_set_of_booksid <> D.DIM_ORA_GL_SETOFBOOKSID;

/*UPDATE dim_ora_gl_code_combinationid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_GL_CODE_COMB D ON S.CODE_COMBINATION_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_gl_code_combinationid = D.DIM_ORA_GL_CODE_COMBID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_gl_code_combinationid <> D.DIM_ORA_GL_CODE_COMBID;

/*UPDATE dim_ora_orgid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_BUSINESS_ORG D 
ON 'HR_BG'+'~'+VARCHAR(S.ORG_ID,200) = D.KEY_ID   and d.rowiscurrent = 1
SET 
F.dim_ora_orgid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_orgid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_date_pr_distribution_updateid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.LAST_UPDATE_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_pr_distribution_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_pr_distribution_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_pr_distribution_update_byid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID  and d.rowiscurrent = 1
SET 
F.dim_ora_pr_distribution_update_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_pr_distribution_update_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_pr_distribution_createdid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.CREATION_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_pr_distribution_createdid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_pr_distribution_createdid <> D.DIM_DATEID;

/*UPDATE dim_ora_pr_distribution_created_byid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_pr_distribution_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_pr_distribution_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_accrual_accountid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_GL_CODE_COMB D ON S.ACCRUAL_ACCOUNT_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_accrual_accountid = D.DIM_ORA_GL_CODE_COMBID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_accrual_accountid <> D.DIM_ORA_GL_CODE_COMBID;

/*UPDATE dim_ora_variance_accountid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_GL_CODE_COMB D ON S.VARIANCE_ACCOUNT_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_variance_accountid = D.DIM_ORA_GL_CODE_COMBID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_variance_accountid <> D.DIM_ORA_GL_CODE_COMBID;

/*UPDATE dim_ora_destination_orgid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_BUSINESS_ORG D 
ON 'INV'+'~'+VARCHAR(S.DESTINATION_ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_destination_orgid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_destination_orgid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_date_pr_submittedid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.SUBMIT_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_pr_submittedid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_pr_submittedid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_pr_approvalid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.APPROVAL_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_pr_approvalid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_pr_approvalid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_pr_line_updateid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.PRL_UPDATE_DT) = D.DATEVALUE 
SET 
F.dim_ora_date_pr_line_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_pr_line_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_pr_header_updateid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.PRH_UPDATE_DT) = D.DATEVALUE 
SET 
F.dim_ora_date_pr_header_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_pr_header_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_line_location_updateid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.PRLL_UPDATE_DT) = D.DATEVALUE 
SET 
F.dim_ora_date_line_location_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_line_location_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_line_typeid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_PO_LINETYPES D ON S.LINE_TYPE_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_line_typeid = D.DIM_ORA_PO_LINETYPESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_line_typeid <> D.DIM_ORA_PO_LINETYPESID;

/*UPDATE dim_ora_categoryid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_ORA_PO_CATEGORIES D ON S.CATEGORY_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_categoryid = D.DIM_ORA_PO_CATEGORIESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_categoryid <> D.DIM_ORA_PO_CATEGORIESID;

/*UPDATE dim_ora_date_promisedid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN DIM_DATE D ON ANSIDATE(S.PROMISED_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_promisedid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dim_ora_date_promisedid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_po_line_loc_need_byid*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN dim_ora_hroperatingunit D ON S.ORG_ID = D.KEY_ID and d.rowiscurrent = 1  
JOIN dim_ora_gl_setofbooks DSB ON DSB.KEY_ID = D.SET_OF_BOOKS_ID
SET F.DD_FUNCTIONAL_CURR_CODE = DSB.CURRENCY_CODE,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dd_functional_curr_code <> DSB.CURRENCY_CODE;

/*UPDATE dd_functional_curr_code*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S JOIN dim_ora_hroperatingunit D ON S.ORG_ID = D.KEY_ID and d.rowiscurrent = 1  
JOIN dim_ora_gl_setofbooks DSB ON DSB.KEY_ID = D.SET_OF_BOOKS_ID and d.rowiscurrent = 1
SET F.DD_FUNCTIONAL_CURR_CODE = DSB.CURRENCY_CODE,
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.dd_functional_curr_code <> DSB.CURRENCY_CODE;

/*UPDATE amt_exchangerate*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S
SET 
F.amt_exchangerate =  ifnull(S.RATE,1),
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.amt_exchangerate <> ifnull(S.RATE,1);

/*UPDATE amt_exchangerate_gbl*/
UPDATE fact_ora_purchaserequisitioncost F
FROM ORA_PURCH_REQ_LINE_COST S
JOIN hlp_ORA_GL_DAILY_RATES GLR ON  ANSIDATE(GLR.CONVERSION_DATE) = ANSIDATE(S.CREATION_DATE)  AND GLR.FROM_CURRENCY  = S.pr_doc_curr and glr.rowiscurrent = 1
JOIN systemproperty glcurr ON GLR.TO_CURRENCY = glcurr.property_value AND glcurr.property = 'customer.global.currency'
JOIN systemproperty contyp ON GLR.CONVERSION_TYPE = contyp.property_value AND contyp.property  = 'GL.conversion_type'
SET F.AMT_EXCHANGERATE_GBL = ifnull(GLR.CONVERSION_RATE,1),
DW_UPDATE_DATE = current_timestamp
WHERE F.DISTRIBUTION_ID = S.DISTRIBUTION_ID AND 
F.AMT_EXCHANGERATE_GBL <>  ifnull(GLR.CONVERSION_RATE,1);

/*UPDATE fact_ora_purchaseordercost*/
/*UPDATE fact_ora_purchasereqcostid*/
UPDATE fact_ora_purchaseordercost F
FROM ORA_PURCH_ORDER_COST S JOIN fact_ora_purchaserequisitioncost D ON S.REQ_DISTRIBUTION_ID = D.DISTRIBUTION_ID and d.rowiscurrent = 1
SET
F.fact_ora_purchasereqcostid = D.fact_ora_purchaserequisitioncostid,
DW_UPDATE_DATE = current_timestamp
WHERE F.PO_DISTRIBUTION_ID = S.PO_DISTRIBUTION_ID AND
F.fact_ora_purchasereqcostid <> D.fact_ora_purchaserequisitioncostid;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_purchaserequisitioncost');
CALL VECTORWISE( COMBINE 'fact_ora_purchaseordercost');