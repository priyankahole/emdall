/*set session authorization oracle_data*/
/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mtl_demand*/
/*SELECT * FROM fact_ora_mtl_demand*/

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_mtl_demand';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_mtl_demand',IFNULL(MAX(fact_ora_mtl_demandid),0)
FROM fact_ora_mtl_demand;

/*update fact columns*/
UPDATE fact_ora_mtl_demand T	
FROM ora_mtl_demand S
SET 
dd_demand_source_name = S.DEMAND_SOURCE_NAME,
dd_uom_code = S.UOM_CODE,
ct_line_item_quantity = S.LINE_ITEM_QUANTITY,
ct_primary_uom_quantity = S.PRIMARY_UOM_QUANTITY,
ct_line_item_reservation_qty = ifnull(S.LINE_ITEM_RESERVATION_QTY,0),
ct_reservation_quantity = ifnull(S.RESERVATION_QUANTITY,0),
ct_completed_quantity = S.COMPLETED_QUANTITY,
dd_reservation_type = S.RESERVATION_TYPE,
dd_user_line_num = ifnull(S.USER_LINE_NUM,'Not Set'),
dd_user_delivery = ifnull(S.USER_DELIVERY,'Not Set'),
dd_updated_flag = ifnull(S.UPDATED_FLAG,0),
dd_revision = ifnull(S.REVISION,'Not Set'),
dd_lot_number = ifnull(S.LOT_NUMBER,'Not Set'),
dd_serial_number = ifnull(S.SERIAL_NUMBER,'Not Set'),
dd_subinventory = ifnull(S.SUBINVENTORY,'Not Set'),
dd_component_sequence_id = ifnull(S.COMPONENT_SEQUENCE_ID,0),
dd_parent_component_seq_id = ifnull(S.PARENT_COMPONENT_SEQ_ID,0),
dd_rto_model_source_line = ifnull(S.RTO_MODEL_SOURCE_LINE,'Not Set'),
ct_rto_previous_qty = ifnull(S.RTO_PREVIOUS_QTY,0),
dd_config_status = ifnull(S.CONFIG_STATUS,0),
dd_available_to_mrp = ifnull(S.AVAILABLE_TO_MRP,0),
dd_available_to_atp = ifnull(S.AVAILABLE_TO_ATP,0),
dd_demand_class = ifnull(S.DEMAND_CLASS,'Not Set'),
dd_row_status_flag = ifnull(S.ROW_STATUS_FLAG,0),
dd_order_change_report_flag = ifnull(S.ORDER_CHANGE_REPORT_FLAG,0),
dd_atp_lead_time = ifnull(S.ATP_LEAD_TIME,0),
dd_bom_level = ifnull(S.BOM_LEVEL,0),
ct_mrp_quantity = ifnull(S.MRP_QUANTITY,0),
dd_territory_id = ifnull(S.TERRITORY_ID,0),
DW_UPDATE_DATE = current_timestamp, 
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE
T.DEMAND_ID = S.DEMAND_ID;

/*insert new rows*/
INSERT INTO fact_ora_mtl_demand
(
fact_ora_mtl_demandid,
dim_ora_organizationid,dim_ora_inventory_itemid,
dim_ora_demand_source_typeid,dim_ora_demand_source_headerid,dim_ora_demand_source_lineid,dim_ora_demand_source_deliveryid,dd_demand_source_name,dd_uom_code,
ct_line_item_quantity,ct_primary_uom_quantity,ct_line_item_reservation_qty,ct_reservation_quantity,ct_completed_quantity,dim_ora_dare_requirementid,dd_reservation_type,
dim_ora_date_last_updateid,dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_created_byid,dim_ora_parent_demandid,dd_user_line_num,dd_user_delivery,
dim_ora_revisionid,dim_ora_subinventoryid,dim_ora_locatorid,dim_ora_customerid,dim_ora_bill_to_site_useid,dim_ora_ship_to_site_useid,dim_ora_supply_source_typeid,
dim_ora_supply_source_headerid,dd_updated_flag,dd_revision,dd_lot_number,dd_serial_number,dd_subinventory,dd_component_sequence_id,dd_parent_component_seq_id,
dd_rto_model_source_line,ct_rto_previous_qty,dd_config_status,dd_available_to_mrp,dd_available_to_atp,dim_ora_date_estimated_releaseid,dd_demand_class,
dd_row_status_flag,dd_order_change_report_flag,dd_atp_lead_time,dim_ora_date_explosion_effectivityid,dd_bom_level,dim_ora_date_mrpid,ct_mrp_quantity,dd_territory_id,demand_id,
amt_exchangerate,amt_exchangerate_gbl,dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_mtl_demand' ),0) + ROW_NUMBER() OVER() fact_ora_mtl_demandid,
1 AS dim_ora_organizationid,
1 AS dim_ora_inventory_itemid,
1 AS dim_ora_demand_source_typeid,
1 AS dim_ora_demand_source_headerid,
1 AS dim_ora_demand_source_lineid,
1 AS dim_ora_demand_source_deliveryid,
S.DEMAND_SOURCE_NAME AS dd_demand_source_name,
S.UOM_CODE AS dd_uom_code,
S.LINE_ITEM_QUANTITY AS ct_line_item_quantity,
S.PRIMARY_UOM_QUANTITY AS ct_primary_uom_quantity,
ifnull(S.LINE_ITEM_RESERVATION_QTY,0) AS ct_line_item_reservation_qty,
ifnull(S.RESERVATION_QUANTITY,0) AS ct_reservation_quantity,
S.COMPLETED_QUANTITY AS ct_completed_quantity,
1 AS dim_ora_dare_requirementid,
S.RESERVATION_TYPE AS dd_reservation_type,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_last_updated_byid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_created_byid,
1 AS dim_ora_parent_demandid,
ifnull(S.USER_LINE_NUM,'Not Set') AS dd_user_line_num,
ifnull(S.USER_DELIVERY,'Not Set') AS dd_user_delivery,
1 AS dim_ora_revisionid,
1 AS dim_ora_subinventoryid,
1 AS dim_ora_locatorid,
1 AS dim_ora_customerid,
1 AS dim_ora_bill_to_site_useid,
1 AS dim_ora_ship_to_site_useid,
1 AS dim_ora_supply_source_typeid,
1 AS dim_ora_supply_source_headerid,
ifnull(S.UPDATED_FLAG,0) AS dd_updated_flag,
ifnull(S.REVISION,'Not Set') AS dd_revision,
ifnull(S.LOT_NUMBER,'Not Set') AS dd_lot_number,
ifnull(S.SERIAL_NUMBER,'Not Set') AS dd_serial_number,
ifnull(S.SUBINVENTORY,'Not Set') AS dd_subinventory,
ifnull(S.COMPONENT_SEQUENCE_ID,0) AS dd_component_sequence_id,
ifnull(S.PARENT_COMPONENT_SEQ_ID,0) AS dd_parent_component_seq_id,
ifnull(S.RTO_MODEL_SOURCE_LINE,'Not Set') AS dd_rto_model_source_line,
ifnull(S.RTO_PREVIOUS_QTY,0) AS ct_rto_previous_qty,
ifnull(S.CONFIG_STATUS,0) AS dd_config_status,
ifnull(S.AVAILABLE_TO_MRP,0) AS dd_available_to_mrp,
ifnull(S.AVAILABLE_TO_ATP,0) AS dd_available_to_atp,
1 AS dim_ora_date_estimated_releaseid,
ifnull(S.DEMAND_CLASS,'Not Set') AS dd_demand_class,
ifnull(S.ROW_STATUS_FLAG,0) AS dd_row_status_flag,
ifnull(S.ORDER_CHANGE_REPORT_FLAG,0) AS dd_order_change_report_flag,
ifnull(S.ATP_LEAD_TIME,0) AS dd_atp_lead_time,
1 AS dim_ora_date_explosion_effectivityid,
ifnull(S.BOM_LEVEL,0) AS dd_bom_level,
1 AS dim_ora_date_mrpid,
ifnull(S.MRP_QUANTITY,0) AS ct_mrp_quantity,
ifnull(S.TERRITORY_ID,0) AS dd_territory_id,
ifnull(S.DEMAND_ID,0) AS demand_id,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mtl_demand S LEFT JOIN fact_ora_mtl_demand T
ON T.DEMAND_ID = S.DEMAND_ID
WHERE T.DEMAND_ID is null;

/*UPDATE dim_ora_organizationid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_BUSINESS_ORG D 
ON VARCHAR('INV', 200) +'~' +VARCHAR(S.ORGANIZATION_ID, 200) = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_organizationid = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_organizationid <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE dim_ora_inventory_itemid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_INVPRODUCT D 
ON VARCHAR(S.ORGANIZATION_ID, 200) + '~' + VARCHAR(S.INVENTORY_ITEM_ID, 200) = D.KEY_ID and d.rowiscurrent = 1
SET 
F.dim_ora_inventory_itemid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_inventory_itemid <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE dim_ora_demand_source_typeid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_FND_LOOKUP D 
ON VARCHAR(S.DEMAND_SOURCE_TYPE,200) = D.lookup_code AND D.lookup_type like '%MTL_SUPPLY_DEMAND_SOURCE_TYPE%' and d.rowiscurrent = 1
SET 
F.dim_ora_demand_source_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_demand_source_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_demand_source_headerid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_MTL_SALESORDERS D ON S.DEMAND_SOURCE_HEADER_ID = D.KEY_ID AND  d.rowiscurrent = 1
SET 
F.dim_ora_demand_source_headerid = D.DIM_ORA_MTL_SALESORDERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and 
F.dim_ora_demand_source_headerid <> D.DIM_ORA_MTL_SALESORDERSID;

/*UPDATE dim_ora_demand_source_lineid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN FACT_ORA_SALES_ORDER_LINE D ON S.DEMAND_SOURCE_LINE = D.dd_line_id  AND  d.rowiscurrent = 1
SET 
F.dim_ora_demand_source_lineid = D.FACT_ORA_SALES_ORDER_LINEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_demand_source_lineid <> D.FACT_ORA_SALES_ORDER_LINEID;

/*UPDATE dim_ora_demand_source_deliveryid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN FACT_ORA_SALES_DELIVERY D ON S.DEMAND_SOURCE_DELIVERY = D.DELIVERY_DETAIL_ID AND D.rowiscurrent = 1 
SET 
F.dim_ora_demand_source_deliveryid = D.FACT_ORA_SALES_DELIVERYID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_demand_source_deliveryid <> D.FACT_ORA_SALES_DELIVERYID;

/*UPDATE dim_ora_dare_requirementid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_DATE D ON ansidate(S.REQUIREMENT_DATE)  = D.datevalue  
SET 
F.dim_ora_dare_requirementid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_dare_requirementid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_DATE D ON ansidate(S.LAST_UPDATE_DATE) = D.datevalue
SET 
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_last_updated_byid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_FNDUSER D ON S.last_updated_by = D.KEY_ID AND D.rowiscurrent = 1   
SET 
F.dim_ora_last_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID AND
F.dim_ora_last_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_DATE D ON ansidate(S.CREATION_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID AND D.rowiscurrent = 1   
SET 
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID AND
F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_parent_demandid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN FACT_ORA_MTL_DEMAND D ON S.PARENT_DEMAND_ID = D.DEMAND_ID AND D.rowiscurrent = 1 
SET 
F.dim_ora_parent_demandid = D.FACT_ORA_MTL_DEMANDID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID AND
F.dim_ora_parent_demandid <> D.FACT_ORA_MTL_DEMANDID;

/*UPDATE dim_ora_revisionid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_MTL_ITEM_REV D 
ON VARCHAR(S.INVENTORY_ITEM_ID, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.REVISION, 200)  = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_revisionid = D.DIM_ORA_MTL_ITEM_REVID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_revisionid <> D.DIM_ORA_MTL_ITEM_REVID;

/*UPDATE dim_ora_subinventoryid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_MTL_SEC_INVENTORY D
ON D.KEY_ID = VARCHAR(S.SUBINVENTORY, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) and d.rowiscurrent = 1
SET 
F.dim_ora_subinventoryid = D.DIM_ORA_MTL_SEC_INVENTORYID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and 
F.dim_ora_subinventoryid <> D.DIM_ORA_MTL_SEC_INVENTORYID;

/*UPDATE dim_ora_locatorid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_MTL_ITEM_LOCATIONS D 
ON S.LOCATOR_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.dim_ora_locatorid = D.DIM_ORA_MTL_ITEM_LOCATIONSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and 
F.dim_ora_locatorid <> D.DIM_ORA_MTL_ITEM_LOCATIONSID;

/*UPDATE dim_ora_customerid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_HZ_PARTY D ON S.CUSTOMER_ID = D.KEY_ID
SET 
F.dim_ora_customerid = D.DIM_ORA_HZ_PARTYID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_customerid <> D.DIM_ORA_HZ_PARTYID;

/*UPDATE dim_ora_bill_to_site_useid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_CUSTOMERLOCATION D 
ON S.BILL_TO_SITE_USE_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_bill_to_site_useid = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_bill_to_site_useid <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE dim_ora_ship_to_site_useid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_CUSTOMERLOCATION D 
ON S.SHIP_TO_SITE_USE_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_ship_to_site_useid = D.DIM_ORA_CUSTOMERLOCATIONID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_ship_to_site_useid <> D.DIM_ORA_CUSTOMERLOCATIONID;

/*UPDATE dim_ora_supply_source_typeid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_FND_LOOKUP D 
ON VARCHAR(S.SUPPLY_SOURCE_TYPE,200) = D.lookup_code AND D.lookup_type like '%MTL_SUPPLY_DEMAND_SOURCE_TYPE%' and d.rowiscurrent = 1
SET 
F.dim_ora_supply_source_typeid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_supply_source_typeid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE dim_ora_supply_source_headerid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_ORA_WIP_ENTITIES D ON S.SUPPLY_SOURCE_HEADER_ID = D.WIP_ENTITY_ID AND d.rowiscurrent = 1
SET 
F.dim_ora_supply_source_headerid = D.DIM_ORA_WIP_ENTITIESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and 
F.dim_ora_supply_source_headerid <> D.DIM_ORA_WIP_ENTITIESID;

/*UPDATE dim_ora_date_estimated_releaseid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_DATE D ON ANSIDATE(S.ESTIMATED_RELEASE_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_estimated_releaseid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and 
F.dim_ora_date_estimated_releaseid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_explosion_effectivityid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_DATE D ON ANSIDATE(S.EXPLOSION_EFFECTIVITY_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_explosion_effectivityid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_date_explosion_effectivityid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_mrpid*/
UPDATE fact_ora_mtl_demand F
FROM ora_mtl_demand S JOIN DIM_DATE D ON ANSIDATE(S.MRP_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_mrpid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.DEMAND_ID =  S.DEMAND_ID and
F.dim_ora_date_mrpid <> D.DIM_DATEID;

CALL VECTORWISE( COMBINE 'fact_ora_mtl_demand');