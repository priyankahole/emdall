/*set session authorization oracle_data*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_invonhand_daily';	
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_invonhand_daily',IFNULL(MAX(fact_ora_invonhand_dailyid),0)
FROM fact_ora_invonhand_daily;

/*DELETE FROM fact_ora_invonhand_daily*/
/*select count(*) from fact_ora_invonhand*/
/*select count(*) from fact_ora_invonhand_daily*/
/*select * from fact_ora_invonhand_daily*/
/*select count(*) from ORA_MTL_ONHAND_RESERVE*/

/*delete rows*/
/*DELETE FROM fact_ora_invonhand_daily*/
DELETE FROM fact_ora_invonhand_daily
where ANSIDATE(SNAPSHOT_DATE) = ANSIDATE(current_timestamp);

/*insert new rows*/
INSERT INTO fact_ora_invonhand_daily
(
fact_ora_invonhand_dailyid,
fact_ORA_INVONHANDid,DIM_ORA_INV_PRODUCTID,DIM_ORA_PRODUCTID,DIM_ORA_INV_ORGID,DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,DIM_ORA_MTL_SEC_INVENTORYID,CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,CT_PRIMARY_TRANSACTION_QUANTITY,DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,fact_ORA_INVMTL_UPDATE_TRXID,DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,DIM_ORA_ORGANIZATION_TYPEID,DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,DIM_ORA_PLANNING_ORGANIZATIONID,DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,CT_SECONDARY_TRANSACTION_QUANTITY,DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,CT_RESERVATION_QUANTITY,DD_SHIP_READY_FLAG,CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,CT_DEMAND_QUANTITY,CREATE_TRANSACTION_ID,UPDATE_TRANSACTION_ID,RESERVATION_ID,INVENTORY_ITEM_ID,
ORGANIZATION_ID,SNAPSHOT_DATE,DIM_ORA_DATE_SNAPSHOTID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_invonhand_daily' ) + ROW_NUMBER() OVER() fact_ora_invonhand_dailyid,
fact_ora_invonhandid,
DIM_ORA_INV_PRODUCTID,
DIM_ORA_PRODUCTID,
DIM_ORA_INV_ORGID,
DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,
DIM_ORA_MTL_SEC_INVENTORYID,
CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,
CT_PRIMARY_TRANSACTION_QUANTITY,
DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,
fact_ORA_INVMTL_UPDATE_TRXID,
DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,
DIM_ORA_ORGANIZATION_TYPEID,
DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,
DIM_ORA_PLANNING_ORGANIZATIONID,
DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,
CT_SECONDARY_TRANSACTION_QUANTITY,
DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,
CT_RESERVATION_QUANTITY,
DD_SHIP_READY_FLAG,
CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,
CT_DEMAND_QUANTITY,
CREATE_TRANSACTION_ID,
UPDATE_TRANSACTION_ID,
RESERVATION_ID,
INVENTORY_ITEM_ID,
ORGANIZATION_ID,
current_timestamp SNAPSHOT_DATE,
1 DIM_ORA_DATE_SNAPSHOTID,
amt_exchangerate,
amt_exchangerate_gbl,
DW_INSERT_DATE,
DW_UPDATE_DATE,
rowstartdate,
rowenddate,
rowiscurrent,
rowchangereason,
SOURCE_ID
FROM fact_ORA_INVONHAND F;

/*UPDATE DIM_ORA_DATE_SNAPSHOTID*/
UPDATE fact_ora_invonhand_daily F
FROM DIM_DATE D
SET 
F.DIM_ORA_DATE_SNAPSHOTID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE ANSIDATE(F.SNAPSHOT_DATE) = D.DATEVALUE
AND F.DIM_ORA_DATE_SNAPSHOTID =1;

/*delete daily snapshots of the previous months and keep only the snapshot of the last day of month*/
delete from fact_ora_invonhand_daily
where ANSIDATE(SNAPSHOT_DATE) in 
(
select dt1.datevalue from 
(select year(datevalue) year, month(datevalue) month, datevalue
from dim_date
/*and calendaryear = 2014*/) dt1
join 
(select year(datevalue) year, month(datevalue) month, max(datevalue) max_datevalue from dim_date
where ansidate(current_timestamp)>=datevalue
/*and calendaryear = 2014*/
group by year(datevalue), month(datevalue) ) dt2
on dt1.year = dt2.year and dt1.month = dt2.month and dt1.datevalue< dt2.max_datevalue
where dt1.month <> month(ansidate(current_timestamp))
);

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_invonhand_daily');