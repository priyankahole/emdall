/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_wip_move_transactions*/
/*SELECT * FROM fact_ora_wip_move_transactions*/

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_wip_move_transactions';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_wip_move_transactions',IFNULL(MAX(fact_ora_wip_move_transactionsid),0)
FROM fact_ora_wip_move_transactions;

/*update fact columns*/
UPDATE fact_ora_wip_move_transactions T	
FROM ora_wip_move_transactions S
SET 
dd_group_id = S.group_id,
dd_source_code = ifnull(S.source_code,'Not Set'),
dd_wip_entity_id = S.wip_entity_id,
dd_fm_operation_seq_num = S.fm_operation_seq_num,
dd_fm_operation_code = ifnull(S.fm_operation_code,'Not Set'),
dd_fm_intraoperation_step_type = S.fm_intraoperation_step_type,
dd_to_operation_seq_num = S.to_operation_seq_num,
dd_to_operation_code = ifnull(S.to_operation_code,'Not Set'),
dd_to_intraoperation_step_type = S.to_intraoperation_step_type,
ct_transaction_quantity = S.transaction_quantity,
dd_transaction_uom = S.transaction_uom,
ct_primary_quantity = S.primary_quantity,
dd_primary_uom = S.primary_uom,
dd_reference = ifnull(S.reference,'Not Set'),
dd_attribute_category = ifnull(S.attribute_category,'Not Set'),
dd_attribute1 = ifnull(S.attribute1,'Not Set'),
dd_attribute2 = ifnull(S.attribute2,'Not Set'),
dd_attribute3 = ifnull(S.attribute3,'Not Set'),
dd_attribute4 = ifnull(S.attribute4,'Not Set'),
dd_attribute5 = ifnull(S.attribute5,'Not Set'),
dd_attribute6 = ifnull(S.attribute6,'Not Set'),
dd_attribute7 = ifnull(S.attribute7,'Not Set'),
dd_attribute8 = ifnull(S.attribute8,'Not Set'),
dd_attribute9 = ifnull(S.attribute9,'Not Set'),
dd_attribute10 = ifnull(S.attribute10,'Not Set'),
dd_attribute11 = ifnull(S.attribute11,'Not Set'),
dd_attribute12 = ifnull(S.attribute12,'Not Set'),
dd_attribute13 = ifnull(S.attribute13,'Not Set'),
dd_attribute14 = ifnull(S.attribute14,'Not Set'),
dd_attribute15 = ifnull(S.attribute15,'Not Set'),
ct_overcompletion_transaction_qty = ifnull(S.overcompletion_transaction_qty,0),
ct_overcompletion_primary_qty = ifnull(S.overcompletion_primary_qty,0),
dd_wf_itemtype = ifnull(S.wf_itemtype,'Not Set'),
dd_wf_itemkey = ifnull(S.wf_itemkey,'Not Set'),
dd_wsm_undo_txn_id = ifnull(S.wsm_undo_txn_id,0),
ct_job_quantity_snapshot = ifnull(S.job_quantity_snapshot,0),
dd_batch_id = ifnull(S.batch_id,0),
dd_completed_instructions = ifnull(S.completed_instructions,0),
dd_wip_entity_name = S.wip_entity_name,
dd_entity_type = S.entity_type,
dd_entity_description = ifnull(S.entity_description,'Not Set'),
DW_UPDATE_DATE = current_timestamp, 
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE
T.TRANSACTION_ID =  S.TRANSACTION_ID;

/*insert new rows*/
INSERT INTO fact_ora_wip_move_transactions
(
fact_ora_wip_move_transactionsid,
transaction_id,dim_ora_date_last_updateid,
dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_created_byid,dd_group_id,dd_source_code,dim_ora_source_lineid,dim_ora_organizationid,dd_wip_entity_id,
dim_ora_lineid,dim_ora_primary_itemid,dim_ora_date_transactionid,dim_ora_acct_periodid,dd_fm_operation_seq_num,dd_fm_operation_code,dim_ora_fm_departmentid,
dd_fm_intraoperation_step_type,dd_to_operation_seq_num,dd_to_operation_code,dim_ora_to_departmentid,dd_to_intraoperation_step_type,ct_transaction_quantity,
dd_transaction_uom,ct_primary_quantity,dd_primary_uom,dim_ora_scrap_accountid,dim_ora_reasonid,dd_reference,dd_attribute_category,dd_attribute1,dd_attribute2,
dd_attribute3,dd_attribute4,dd_attribute5,dd_attribute6,dd_attribute7,dd_attribute8,dd_attribute9,dd_attribute10,dd_attribute11,dd_attribute12,dd_attribute13,
dd_attribute14,dd_attribute15,dim_ora_qa_collectionid,ct_overcompletion_transaction_qty,ct_overcompletion_primary_qty,fact_ora_overcompletion_transactionid,
dd_wf_itemtype,dd_wf_itemkey,dd_wsm_undo_txn_id,ct_job_quantity_snapshot,dd_batch_id,dim_ora_employeeid,dd_completed_instructions,dd_wip_entity_name,dd_entity_type,
dd_entity_description,amt_exchangerate,amt_exchangerate_gbl,dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_wip_move_transactions' ),0) + ROW_NUMBER() OVER() fact_ora_wip_move_transactionsid,
S.transaction_id AS transaction_id,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_last_updated_byid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_created_byid,
S.group_id AS dd_group_id,
ifnull(S.source_code,'Not Set') AS dd_source_code,
1 AS dim_ora_source_lineid,
1 AS dim_ora_organizationid,
S.wip_entity_id AS dd_wip_entity_id,
1 AS dim_ora_lineid,
1 AS dim_ora_primary_itemid,
1 AS dim_ora_date_transactionid,
1 AS dim_ora_acct_periodid,
S.fm_operation_seq_num AS dd_fm_operation_seq_num,
ifnull(S.fm_operation_code,'Not Set') AS dd_fm_operation_code,
1 AS dim_ora_fm_departmentid,
S.fm_intraoperation_step_type AS dd_fm_intraoperation_step_type,
S.to_operation_seq_num AS dd_to_operation_seq_num,
ifnull(S.to_operation_code,'Not Set') AS dd_to_operation_code,
1 AS dim_ora_to_departmentid,
S.to_intraoperation_step_type AS dd_to_intraoperation_step_type,
S.transaction_quantity AS ct_transaction_quantity,
S.transaction_uom AS dd_transaction_uom,
S.primary_quantity AS ct_primary_quantity,
S.primary_uom AS dd_primary_uom,
1 AS dim_ora_scrap_accountid,
1 AS dim_ora_reasonid,
ifnull(S.reference,'Not Set') AS dd_reference,
ifnull(S.attribute_category,'Not Set') AS dd_attribute_category,
ifnull(S.attribute1,'Not Set') AS dd_attribute1,
ifnull(S.attribute2,'Not Set') AS dd_attribute2,
ifnull(S.attribute3,'Not Set') AS dd_attribute3,
ifnull(S.attribute4,'Not Set') AS dd_attribute4,
ifnull(S.attribute5,'Not Set') AS dd_attribute5,
ifnull(S.attribute6,'Not Set') AS dd_attribute6,
ifnull(S.attribute7,'Not Set') AS dd_attribute7,
ifnull(S.attribute8,'Not Set') AS dd_attribute8,
ifnull(S.attribute9,'Not Set') AS dd_attribute9,
ifnull(S.attribute10,'Not Set') AS dd_attribute10,
ifnull(S.attribute11,'Not Set') AS dd_attribute11,
ifnull(S.attribute12,'Not Set') AS dd_attribute12,
ifnull(S.attribute13,'Not Set') AS dd_attribute13,
ifnull(S.attribute14,'Not Set') AS dd_attribute14,
ifnull(S.attribute15,'Not Set') AS dd_attribute15,
1 AS dim_ora_qa_collectionid,
ifnull(S.overcompletion_transaction_qty,0) AS ct_overcompletion_transaction_qty,
ifnull(S.overcompletion_primary_qty,0) AS ct_overcompletion_primary_qty,
1 AS fact_ora_overcompletion_transactionid,
ifnull(S.wf_itemtype,'Not Set') AS dd_wf_itemtype,
ifnull(S.wf_itemkey,'Not Set') AS dd_wf_itemkey,
ifnull(S.wsm_undo_txn_id,0) AS dd_wsm_undo_txn_id,
ifnull(S.job_quantity_snapshot,0) AS ct_job_quantity_snapshot,
ifnull(S.batch_id,0) AS dd_batch_id,
1 AS dim_ora_employeeid,
ifnull(S.completed_instructions,0) AS dd_completed_instructions,
S.wip_entity_name AS dd_wip_entity_name,
S.entity_type AS dd_entity_type,
ifnull(S.entity_description,'Not Set') AS dd_entity_description,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_wip_move_transactions S LEFT JOIN fact_ora_wip_move_transactions F 
ON F.TRANSACTION_ID =  S.TRANSACTION_ID
WHERE F.TRANSACTION_ID is null;


/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_DATE D ON ansidate(S.LAST_UPDATE_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_last_updated_byid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_last_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_last_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_DATE D ON ansidate(S.CREATION_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_source_lineid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_WIP_LINES D 
ON VARCHAR(S.SOURCE_LINE_ID,200) + '~' +  VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_source_lineid = D.DIM_ORA_WIP_LINESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_source_lineid <> D.DIM_ORA_WIP_LINESID;

/*UPDATE dim_ora_organizationid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_WIP_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_organizationid = D.DIM_ORA_WIP_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_organizationid <> D.DIM_ORA_WIP_PARAMETERSID;

/*UPDATE dim_ora_lineid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_WIP_LINES D
ON VARCHAR(S.LINE_ID,200) + '~' +  VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_lineid = D.DIM_ORA_WIP_LINESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_lineid <> D.DIM_ORA_WIP_LINESID;

/*UPDATE dim_ora_primary_itemid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_INVPRODUCT D 
ON S.PRIMARY_ITEM_ID = D.INVENTORY_ITEM_ID AND S.ORGANIZATION_ID = D.ORGANIZATION_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_primary_itemid = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_primary_itemid <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE dim_ora_date_transactionid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_DATE D ON ansidate(S.TRANSACTION_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_transactionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_date_transactionid <> D.DIM_DATEID;

/*UPDATE dim_ora_acct_periodid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_ORG_ACCT_PERIODS D 
ON ifnull(VARCHAR(S.ACCT_PERIOD_ID,200),'Not Set') + '~' + ifnull(VARCHAR(S.ORGANIZATION_ID, 200),'Not Set') = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_acct_periodid = D.DIM_ORA_ORG_ACCT_PERIODSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_acct_periodid <> D.DIM_ORA_ORG_ACCT_PERIODSID;

/*UPDATE dim_ora_fm_departmentid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_BOM_DEPARTMENTS D ON S.FM_DEPARTMENT_ID = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_fm_departmentid = D.DIM_ORA_BOM_DEPARTMENTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_fm_departmentid <> D.DIM_ORA_BOM_DEPARTMENTSID;

/*UPDATE dim_ora_to_departmentid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_BOM_DEPARTMENTS D ON S.TO_DEPARTMENT_ID = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_to_departmentid = D.DIM_ORA_BOM_DEPARTMENTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_to_departmentid <> D.DIM_ORA_BOM_DEPARTMENTSID;

/*UPDATE dim_ora_scrap_accountid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_GL_CODE_COMB D ON S.SCRAP_ACCOUNT_ID = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_scrap_accountid = D.DIM_ORA_GL_CODE_COMBID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_scrap_accountid <> D.DIM_ORA_GL_CODE_COMBID;

/*UPDATE dim_ora_reasonid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_MTL_TRX_REASONS D ON S.REASON_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_reasonid = D.DIM_ORA_MTL_TRX_REASONSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_reasonid <> D.DIM_ORA_MTL_TRX_REASONSID;

/*UPDATE dim_ora_qa_collectionid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S 
JOIN (select COLLECTION_ID, max(OCCURRENCE) OCCURRENCE from DIM_ORA_QA_RESULTS GROUP BY COLLECTION_ID) DS
ON DS.COLLECTION_ID = S.QA_COLLECTION_ID
JOIN DIM_ORA_QA_RESULTS D 
ON VARCHAR(ifnull(S.QA_COLLECTION_ID,0), 200) + '~' + VARCHAR(ifnull(DS.OCCURRENCE,0), 200) = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_qa_collectionid = D.DIM_ORA_QA_RESULTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_qa_collectionid <> D.DIM_ORA_QA_RESULTSID;

/*UPDATE fact_ora_overcompletion_transactionid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN FACT_ORA_WIP_MOVE_TRANSACTIONS D ON S.OVERCOMPLETION_TRANSACTION_ID = D.TRANSACTION_ID AND d.rowiscurrent = 1 
SET 
F.fact_ora_overcompletion_transactionid = D.FACT_ORA_WIP_MOVE_TRANSACTIONSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID = S.TRANSACTION_ID
and F.fact_ora_overcompletion_transactionid <> D.FACT_ORA_WIP_MOVE_TRANSACTIONSID;

/*UPDATE dim_ora_employeeid*/
UPDATE fact_ora_wip_move_transactions F
FROM ora_wip_move_transactions S JOIN DIM_ORA_HR_EMPLOYEES D 
ON S.EMPLOYEE_ID = D.PERSON_ID and S.CREATION_DATE BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE and d.rowiscurrent = 1 
SET 
F.dim_ora_employeeid = D.DIM_ORA_HR_EMPLOYEESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.TRANSACTION_ID =  S.TRANSACTION_ID
and F.dim_ora_employeeid <> D.DIM_ORA_HR_EMPLOYEESID;