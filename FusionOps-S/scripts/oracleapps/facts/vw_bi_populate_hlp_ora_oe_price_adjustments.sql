/*set session authorization oracle_data*/


/*DELETE FROM hlp_ora_oe_price_adjustments*/
/*SELECT * FROM hlp_ora_oe_price_adjustments*/
delete from NUMBER_FOUNTAIN where table_name = 'hlp_ora_oe_price_adjustments';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'hlp_ora_oe_price_adjustments',IFNULL(MAX(HLP_OE_PRICE_ADJUSTMENTSID),0)
FROM hlp_ora_oe_price_adjustments;

/*update rows*/
UPDATE hlp_ora_oe_price_adjustments F
FROM ORA_OE_PRICE_ADJUSTMENTS S
SET 
F.OPERAND_PER_PQTY = S.OPERAND_PER_PQTY,
F.OPERAND = S.OPERAND,
F.ADJUSTED_AMOUNT_PER_PQTY = S.ADJUSTED_AMOUNT_PER_PQTY,
F.INTERCO_INVOICED_FLAG = S.INTERCO_INVOICED_FLAG,
F.INVOICED_AMOUNT = S.INVOICED_AMOUNT,
F.CREATION_DATE = S.CREATION_DATE,
F.CREATED_BY = S.CREATED_BY,
F.LAST_UPDATE_DATE = S.LAST_UPDATE_DATE,
F.LAST_UPDATED_BY = S.LAST_UPDATED_BY,
F.LAST_UPDATE_LOGIN = S.LAST_UPDATE_LOGIN,
F.HEADER_ID = S.HEADER_ID,
F.DISCOUNT_ID = S.DISCOUNT_ID,
F.DISCOUNT_LINE_ID = S.DISCOUNT_LINE_ID,
F.PERCENT = S.PERCENT,
F.LINE_ID = S.LINE_ID,
F.LIST_HEADER_ID = S.LIST_HEADER_ID,
F.LIST_LINE_ID = S.LIST_LINE_ID,
F.TAX_RATE_ID = S.TAX_RATE_ID,
F.LIST_LINE_TYPE_CODE = S.LIST_LINE_TYPE_CODE,
F.MODIFIED_FROM = S.MODIFIED_FROM,
F.MODIFIED_TO = S.MODIFIED_TO,
F.UPDATE_ALLOWED = S.UPDATE_ALLOWED,
F.UPDATED_FLAG = S.UPDATED_FLAG,
F.APPLIED_FLAG = S.APPLIED_FLAG,
F.ARITHMETIC_OPERATOR = S.ARITHMETIC_OPERATOR,
F.PARENT_ADJUSTMENT_ID = S.PARENT_ADJUSTMENT_ID,
F.INVOICED_FLAG = S.INVOICED_FLAG,
F.ESTIMATED_FLAG = S.ESTIMATED_FLAG,
F.ADJUSTED_AMOUNT = S.ADJUSTED_AMOUNT,
F.rowiscurrent = 1,
F.DW_UPDATE_DATE = current_timestamp
WHERE S.PRICE_ADJUSTMENT_ID = F.PRICE_ADJUSTMENT_ID;

/*insert new rows*/
INSERT INTO hlp_ora_oe_price_adjustments
(

hlp_oe_price_adjustmentsid,
price_adjustment_id,operand_per_pqty,operand,adjusted_amount_per_pqty,interco_invoiced_flag,invoiced_amount,creation_date,
created_by,last_update_date,last_updated_by,last_update_login,header_id,discount_id,
discount_line_id,percent,line_id,list_header_id,list_line_id,tax_rate_id,list_line_type_code,modified_from,modified_to,
update_allowed,updated_flag,applied_flag,arithmetic_operator,parent_adjustment_id,invoiced_flag,estimated_flag,adjusted_amount,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'hlp_ora_oe_price_adjustments' ),0) + ROW_NUMBER() OVER() HLP_OE_PRICE_ADJUSTMENTSID,
S.PRICE_ADJUSTMENT_ID,
S.OPERAND_PER_PQTY,
S.OPERAND,
S.ADJUSTED_AMOUNT_PER_PQTY,
S.INTERCO_INVOICED_FLAG,
S.INVOICED_AMOUNT,
S.CREATION_DATE,
S.CREATED_BY,
S.LAST_UPDATE_DATE,
S.LAST_UPDATED_BY,
S.LAST_UPDATE_LOGIN,
S.HEADER_ID,
S.DISCOUNT_ID,
S.DISCOUNT_LINE_ID,
S.PERCENT,
S.LINE_ID,
S.LIST_HEADER_ID,
S.LIST_LINE_ID,
S.TAX_RATE_ID,
S.LIST_LINE_TYPE_CODE,
S.MODIFIED_FROM,
S.MODIFIED_TO,
S.UPDATE_ALLOWED,
S.UPDATED_FLAG,
S.APPLIED_FLAG,
S.ARITHMETIC_OPERATOR,
S.PARENT_ADJUSTMENT_ID,
S.INVOICED_FLAG,
S.ESTIMATED_FLAG,
S.ADJUSTED_AMOUNT,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' SOURCE_ID
FROM ORA_OE_PRICE_ADJUSTMENTS S
LEFT JOIN hlp_ora_oe_price_adjustments F
ON   S.PRICE_ADJUSTMENT_ID = F.PRICE_ADJUSTMENT_ID 
WHERE F.PRICE_ADJUSTMENT_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'hlp_ora_oe_price_adjustments');