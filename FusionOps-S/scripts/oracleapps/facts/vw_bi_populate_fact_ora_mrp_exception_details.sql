/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_mrp_exception_details*/
/*SELECT * FROM fact_ora_mrp_exception_details*/


/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_mrp_exception_details';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_mrp_exception_details',IFNULL(MAX(fact_ora_mrp_exception_detailsid),0) 
FROM fact_ora_mrp_exception_details;

/*update fact columns*/
UPDATE fact_ora_mrp_exception_details T	
FROM ora_mrp_exception_details S
SET 
CT_QUANTITY = ifnull(S.QUANTITY,0),
CT_NUMBER1 = ifnull(S.NUMBER1,0),
CT_NUMBER2 = ifnull(S.NUMBER2,0),
CT_NUMBER3 = ifnull(S.NUMBER3,0),
CT_NUMBER4 = ifnull(S.NUMBER4,0),
DD_CHAR1 = ifnull(S.CHAR1,'Not Set'),
DD_CHAR2 = ifnull(S.CHAR2,'Not Set'),
EXCEPTION_ID = S.EXCEPTION_ID,
amt_exchangerate = 1,
amt_exchangerate_gbl = 1,
DW_UPDATE_DATE = current_timestamp,
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE	
T.EXCEPTION_ID = S.EXCEPTION_ID;

/*insert new rows*/
INSERT INTO fact_ora_mrp_exception_details
(
fact_ora_mrp_exception_detailsid,
DIM_ORA_CREATED_BYid,DIM_ORA_COMPILE_DESIGNATORid,DIM_ORA_ORGANIZATIONID,
DIM_ORA_INVENTORY_ITEMID,DIM_ORA_EXCEPTION_TYPEid,DIM_ORA_DATE_LAST_UPDATEid,DIM_ORA_LAST_UPDATED_BYid,
DIM_ORA_DATE_CREATIONid,CT_QUANTITY,DIM_ORA_DATE1id,DIM_ORA_DATE2id,DIM_ORA_DATE3id,DIM_ORA_DATE4id,
CT_NUMBER1,CT_NUMBER2,CT_NUMBER3,CT_NUMBER4,DD_CHAR1,DD_CHAR2,DIM_ORA_DEPARTMENTid,
DIM_ORA_RESOURCEid,DIM_ORA_LINEid,EXCEPTION_ID,DIM_ORA_SUPPLIERid,DIM_ORA_SUPPLIER_SITEid,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_mrp_exception_details' ),0) + ROW_NUMBER() OVER() fact_ora_mrp_exception_detailsid,
1 AS DIM_ORA_CREATED_BYid,
1 AS DIM_ORA_COMPILE_DESIGNATORid,
1 AS DIM_ORA_ORGANIZATIONID,
1 AS DIM_ORA_INVENTORY_ITEMID,
1 AS DIM_ORA_EXCEPTION_TYPEid,
1 AS DIM_ORA_DATE_LAST_UPDATEid,
1 AS DIM_ORA_LAST_UPDATED_BYid,
1 AS DIM_ORA_DATE_CREATIONid,
ifnull(S.QUANTITY,0) AS CT_QUANTITY,
1 AS DIM_ORA_DATE1id,
1 AS DIM_ORA_DATE2id,
1 AS DIM_ORA_DATE3id,
1 AS DIM_ORA_DATE4id,
ifnull(S.NUMBER1,0) AS CT_NUMBER1,
ifnull(S.NUMBER2,0) AS CT_NUMBER2,
ifnull(S.NUMBER3,0) AS CT_NUMBER3,
ifnull(S.NUMBER4,0) AS CT_NUMBER4,
ifnull(S.CHAR1,'Not Set') AS DD_CHAR1,
ifnull(S.CHAR2,'Not Set') AS DD_CHAR2,
1 AS DIM_ORA_DEPARTMENTid,
1 AS DIM_ORA_RESOURCEid,
1 AS DIM_ORA_LINEid,
S.EXCEPTION_ID AS EXCEPTION_ID,
1 AS DIM_ORA_SUPPLIERid,
1 AS DIM_ORA_SUPPLIER_SITEid,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'INSERT' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_mrp_exception_details S LEFT JOIN fact_ora_mrp_exception_details F ON
F.EXCEPTION_ID = S.EXCEPTION_ID
WHERE F.EXCEPTION_ID is null;

/*UPDATE DIM_ORA_CREATED_BYid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID and d.rowiscurrent = 1
SET 
F.DIM_ORA_CREATED_BYid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_CREATED_BYid <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_COMPILE_DESIGNATORid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_MRP_DESIGNATORS D 
ON VARCHAR(S.COMPILE_DESIGNATOR,200) + '~' + VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_COMPILE_DESIGNATORid = D.DIM_ORA_MRP_DESIGNATORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_COMPILE_DESIGNATORid <> D.DIM_ORA_MRP_DESIGNATORSID;

/*UPDATE DIM_ORA_ORGANIZATIONID*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_MTL_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_ORGANIZATIONID = D.DIM_ORA_MTL_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_ORGANIZATIONID <> D.DIM_ORA_MTL_PARAMETERSID;

/*UPDATE DIM_ORA_INVENTORY_ITEMID*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_MRP_SYSTEM_ITEMS D 
ON VARCHAR(S.COMPILE_DESIGNATOR,200) + '~' + VARCHAR(S.ORGANIZATION_ID,200) + '~' + VARCHAR(S.INVENTORY_ITEM_ID,200) = D.KEY_ID and d.rowiscurrent = 1
SET 
F.DIM_ORA_INVENTORY_ITEMID = D.DIM_ORA_MRP_SYSTEM_ITEMSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_INVENTORY_ITEMID <> D.DIM_ORA_MRP_SYSTEM_ITEMSID;

/*UPDATE DIM_ORA_EXCEPTION_TYPEid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_FND_LOOKUP D 
ON S.EXCEPTION_TYPE= D.LOOKUP_CODE AND D.LOOKUP_TYPE = 'MRP_EXCEPTION_TYPE' and d.rowiscurrent = 1
SET 
F.DIM_ORA_EXCEPTION_TYPEid = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_EXCEPTION_TYPEid <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_DATE_LAST_UPDATEid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_DATE D ON ANSIDATE(S.LAST_UPDATE_DATE) = D.datevalue 
SET 
F.DIM_ORA_DATE_LAST_UPDATEid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_DATE_LAST_UPDATEid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_LAST_UPDATED_BYid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_LAST_UPDATED_BYid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_LAST_UPDATED_BYid <> D.DIM_ORA_FNDUSERID;

/*UPDATE DIM_ORA_DATE_CREATIONid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_DATE D ON ANSIDATE(S.CREATION_DATE) = D.datevalue 
SET 
F.DIM_ORA_DATE_CREATIONid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_DATE_CREATIONid <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE1id*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_DATE D ON ANSIDATE(S.DATE1) = D.datevalue 
SET 
F.DIM_ORA_DATE1id = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_DATE1id <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE2id*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_DATE D ON ANSIDATE(S.DATE2) = D.datevalue 
SET 
F.DIM_ORA_DATE2id = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_DATE2id <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE3id*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_DATE D ON ANSIDATE(S.DATE3) = D.datevalue 
SET 
F.DIM_ORA_DATE3id = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_DATE3id <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE4id*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_DATE D ON ANSIDATE(S.DATE4) = D.datevalue 
SET 
F.DIM_ORA_DATE4id = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_DATE4id <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DEPARTMENTid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_BOM_DEPARTMENTS D ON S.DEPARTMENT_ID = D.KEY_ID and d.rowiscurrent = 1
SET 
F.DIM_ORA_DEPARTMENTid = D.DIM_ORA_BOM_DEPARTMENTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_DEPARTMENTid <> D.DIM_ORA_BOM_DEPARTMENTSID;

/*UPDATE DIM_ORA_RESOURCEid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_BOM_RESOURCES D ON S.RESOURCE_ID = D.KEY_ID and d.rowiscurrent = 1
SET 
F.DIM_ORA_RESOURCEid = D.DIM_ORA_BOM_RESOURCESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_RESOURCEid <> D.DIM_ORA_BOM_RESOURCESID;

/*UPDATE DIM_ORA_LINEid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_WIP_LINES D 
ON VARCHAR(S.LINE_ID, 200) + '~' + VARCHAR(S.ORGANIZATION_ID, 200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_LINEid = D.DIM_ORA_WIP_LINESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_LINEid <> D.DIM_ORA_WIP_LINESID;

/*UPDATE DIM_ORA_SUPPLIERid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_PO_VENDORS D ON S.SUPPLIER_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_SUPPLIERid = D.DIM_ORA_PO_VENDORSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_SUPPLIERid <> D.DIM_ORA_PO_VENDORSID;

/*UPDATE DIM_ORA_SUPPLIER_SITEid*/
UPDATE fact_ora_mrp_exception_details F
FROM ora_mrp_exception_details S JOIN DIM_ORA_PO_VENDORSITES D ON S.SUPPLIER_SITE_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_SUPPLIER_SITEid = D.DIM_ORA_PO_VENDORSITESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.EXCEPTION_ID= S.EXCEPTION_ID AND 
F.DIM_ORA_SUPPLIER_SITEid <> D.DIM_ORA_PO_VENDORSITESID;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_mrp_exception_details');