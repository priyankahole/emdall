/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_sales_order_change*/
/*SELECT * FROM fact_ora_sales_order_change*/

DELETE FROM fact_ora_sales_order_change
WHERE snapshot_date = timestamp(ansidate(current_timestamp));

drop if exists fact_ora_sales_order_change_tmp;

create table fact_ora_sales_order_change_tmp as
select fact_ora_sales_order_lineid,
max(datevalue) last_update_date 
from fact_ora_sales_order_change f
join dim_date d on d.dim_dateid = f.dim_ora_date_last_updateid
group by fact_ora_sales_order_lineid;

delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_sales_order_change';	


INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_sales_order_change',IFNULL(MAX(fact_ora_sales_order_changeID),0)
FROM fact_ora_sales_order_change;

/*insert new rows*/
INSERT INTO fact_ora_sales_order_change
(
fact_ora_sales_order_changeid,
fact_ora_sales_order_lineid,dim_ora_account_repid,dim_ora_business_orgid,dim_ora_channel_typeid,dim_ora_company_orgid,dim_ora_cost_centerid,dim_ora_customer_bill_to_locationid,
dim_ora_customer_ship_to_locationid,dim_ora_customer_sold_to_locationid,dim_ora_customer_bill_toid,dim_ora_customer_ship_toid,dim_ora_customer_sold_toid,
dim_ora_customer_account_bill_toid,dim_ora_customer_account_ship_toid,dim_ora_customer_account_sold_toid,dim_ora_inventory_orgid,dim_ora_inv_productid,dim_ora_order_line_statusid,
dim_ora_payment_methodid,dim_ora_payment_termid,dim_ora_productid,dim_ora_sales_groupid,dim_ora_sales_office_locationid,dim_ora_salesrepid,dim_ora_servicerepid,
dim_ora_order_line_typeid,dim_ora_order_header_typeid,dim_ora_order_header_statusid,dim_ora_freight_termid,dim_ora_shipment_methodid,dim_ora_date_order_bookedid,
dim_ora_date_customer_requestedid,dim_ora_date_enteredid,dim_ora_date_orderedid,dim_ora_date_promise_deliveryid,dim_ora_date_scheduled_shipid,
dim_ora_date_actual_shipid,dim_ora_date_earliest_shipmentid,dim_ora_date_order_confirmedid,dim_ora_date_actual_fulfillmentid,dim_ora_date_fulfillmentid,
dim_ora_date_actual_arrival_dateid,dim_ora_date_schedule_arrivalid,dim_ora_date_taxid,dim_ora_date_pricingid,dim_ora_gl_set_of_booksid,dim_ora_hr_operatingunitid,
dim_ora_aging_days_bucketid,ct_aging_days,ct_product_unit_cost,ct_product_unit_list_price,ct_product_unit_selling_price,ct_manufacturing_lead_time,
ct_ship_tolerance_above,ct_ship_tolerance_below,ct_delivery_lead_time,ct_ordered_quantity,ct_cancelled_quantity,ct_sales_quantity,
ct_fulfilled_quantity,ct_confirmed_quantity,ct_fill_quantity,ct_fill_quantity_crd,ct_total_shipped_quantity,
ct_total_invoiced_quantity,amt_net_ordered,amt_tax,amt_cost,amt_list,amt_discount,dd_line_category,dd_order_quantity_uom,dd_order_number,
dd_line_number,dd_shipment_number,dd_orig_ref_doc_number,dd_cust_po_number,dd_otif_flag,dd_cancelled_h_flag,dd_cancelled_l_flag,
dd_open_flag,dd_fulfilled_flag,dd_shippable_flag,dd_tax_exempt_flag,dd_booked_flag,dd_model_remnant_flag,dd_on_hold_flag,
dd_financial_backlog_flag,dd_operation_backlog_flag,dd_hold_name,dd_header_id,dd_line_id,dd_transactional_curr_code,dd_functional_curr_code,
dd_shipped_not_invoiced_flag,ct_so_touch_count,dd_line_flow_status_code,dd_shipping_interfaced_flag,amt_exchangerate,amt_exchangerate_gbl,dim_ora_created_byid,
dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_date_last_updateid,dim_ora_mtl_plannersid,dim_projectsourceid,
snapshot_date, dim_ora_date_snapshotid,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_sales_order_change' ) + ROW_NUMBER() OVER() fact_ora_sales_order_changeID,
f.fact_ora_sales_order_lineid,
f.dim_ora_account_repid,
f.dim_ora_business_orgid,
f.dim_ora_channel_typeid,
f.dim_ora_company_orgid,
f.dim_ora_cost_centerid,
f.dim_ora_customer_bill_to_locationid,
f.dim_ora_customer_ship_to_locationid,
f.dim_ora_customer_sold_to_locationid,
f.dim_ora_customer_bill_toid,
f.dim_ora_customer_ship_toid,
f.dim_ora_customer_sold_toid,
f.dim_ora_customer_account_bill_toid,
f.dim_ora_customer_account_ship_toid,
f.dim_ora_customer_account_sold_toid,
f.dim_ora_inventory_orgid,
f.dim_ora_inv_productid,
f.dim_ora_order_line_statusid,
f.dim_ora_payment_methodid,
f.dim_ora_payment_termid,
f.dim_ora_productid,
f.dim_ora_sales_groupid,
f.dim_ora_sales_office_locationid,
f.dim_ora_salesrepid,
f.dim_ora_servicerepid,
f.dim_ora_order_line_typeid,
f.dim_ora_order_header_typeid,
f.dim_ora_order_header_statusid,
f.dim_ora_freight_termid,
f.dim_ora_shipment_methodid,
f.dim_ora_date_order_bookedid,
f.dim_ora_date_customer_requestedid,
f.dim_ora_date_enteredid,
f.dim_ora_date_orderedid,
f.dim_ora_date_promise_deliveryid,
f.dim_ora_date_scheduled_shipid,
f.dim_ora_date_actual_shipid,
f.dim_ora_date_earliest_shipmentid,
f.dim_ora_date_order_confirmedid,
f.dim_ora_date_actual_fulfillmentid,
f.dim_ora_date_fulfillmentid,
f.dim_ora_date_actual_arrival_dateid,
f.dim_ora_date_schedule_arrivalid,
f.dim_ora_date_taxid,
f.dim_ora_date_pricingid,
f.dim_ora_gl_set_of_booksid,
f.dim_ora_hr_operatingunitid,
f.dim_ora_aging_days_bucketid,
f.ct_aging_days,
f.ct_product_unit_cost,
f.ct_product_unit_list_price,
f.ct_product_unit_selling_price,
f.ct_manufacturing_lead_time,
f.ct_ship_tolerance_above,
f.ct_ship_tolerance_below,
f.ct_delivery_lead_time,
f.ct_ordered_quantity,
f.ct_cancelled_quantity,
f.ct_sales_quantity,
f.ct_fulfilled_quantity,
f.ct_confirmed_quantity,
f.ct_fill_quantity,
f.ct_fill_quantity_crd,
f.ct_total_shipped_quantity,
f.ct_total_invoiced_quantity,
f.amt_net_ordered,
f.amt_tax,
f.amt_cost,
f.amt_list,
f.amt_discount,
f.dd_line_category,
f.dd_order_quantity_uom,
f.dd_order_number,
f.dd_line_number,
f.dd_shipment_number,
f.dd_orig_ref_doc_number,
f.dd_cust_po_number,
f.dd_otif_flag,
f.dd_cancelled_h_flag,
f.dd_cancelled_l_flag,
f.dd_open_flag,
f.dd_fulfilled_flag,
f.dd_shippable_flag,
f.dd_tax_exempt_flag,
f.dd_booked_flag,
f.dd_model_remnant_flag,
f.dd_on_hold_flag,
f.dd_financial_backlog_flag,
f.dd_operation_backlog_flag,
f.dd_hold_name,
f.dd_header_id,
f.dd_line_id,
f.dd_transactional_curr_code,
f.dd_functional_curr_code,
f.dd_shipped_not_invoiced_flag,
f.ct_so_touch_count,
f.dd_line_flow_status_code,
f.dd_shipping_interfaced_flag,
f.amt_exchangerate,
f.amt_exchangerate_gbl,
f.dim_ora_created_byid,
f.dim_ora_last_updated_byid,
f.dim_ora_date_creationid,
f.dim_ora_date_last_updateid,
f.dim_ora_mtl_plannersid,
f.dim_projectsourceid,
current_timestamp snapshot_date, 
1 dim_ora_date_snapshotid,
f.dw_insert_date,
f.dw_update_date,
f.rowstartdate,
f.rowenddate,
f.rowiscurrent,
f.rowchangereason,
f.source_id
FROM 
ORA_OE_ORDER_HEADER_LINE s
join fact_ora_sales_order_line f
on f.DD_HEADER_ID = s.HEADER_ID AND f.DD_LINE_ID   = s.LINE_ID
left join  fact_ora_sales_order_change_tmp t
on f.fact_ora_sales_order_lineid = t.fact_ora_sales_order_lineid
where t.fact_ora_sales_order_lineid is null;

INSERT INTO fact_ora_sales_order_change
(
fact_ora_sales_order_changeid,
fact_ora_sales_order_lineid,dim_ora_account_repid,dim_ora_business_orgid,dim_ora_channel_typeid,dim_ora_company_orgid,dim_ora_cost_centerid,dim_ora_customer_bill_to_locationid,
dim_ora_customer_ship_to_locationid,dim_ora_customer_sold_to_locationid,dim_ora_customer_bill_toid,dim_ora_customer_ship_toid,dim_ora_customer_sold_toid,
dim_ora_customer_account_bill_toid,dim_ora_customer_account_ship_toid,dim_ora_customer_account_sold_toid,dim_ora_inventory_orgid,dim_ora_inv_productid,dim_ora_order_line_statusid,
dim_ora_payment_methodid,dim_ora_payment_termid,dim_ora_productid,dim_ora_sales_groupid,dim_ora_sales_office_locationid,dim_ora_salesrepid,dim_ora_servicerepid,
dim_ora_order_line_typeid,dim_ora_order_header_typeid,dim_ora_order_header_statusid,dim_ora_freight_termid,dim_ora_shipment_methodid,dim_ora_date_order_bookedid,
dim_ora_date_customer_requestedid,dim_ora_date_enteredid,dim_ora_date_orderedid,dim_ora_date_promise_deliveryid,dim_ora_date_scheduled_shipid,
dim_ora_date_actual_shipid,dim_ora_date_earliest_shipmentid,dim_ora_date_order_confirmedid,dim_ora_date_actual_fulfillmentid,dim_ora_date_fulfillmentid,
dim_ora_date_actual_arrival_dateid,dim_ora_date_schedule_arrivalid,dim_ora_date_taxid,dim_ora_date_pricingid,dim_ora_gl_set_of_booksid,dim_ora_hr_operatingunitid,
dim_ora_aging_days_bucketid,ct_aging_days,ct_product_unit_cost,ct_product_unit_list_price,ct_product_unit_selling_price,ct_manufacturing_lead_time,
ct_ship_tolerance_above,ct_ship_tolerance_below,ct_delivery_lead_time,ct_ordered_quantity,ct_cancelled_quantity,ct_sales_quantity,
ct_fulfilled_quantity,ct_confirmed_quantity,ct_fill_quantity,ct_fill_quantity_crd,ct_total_shipped_quantity,
ct_total_invoiced_quantity,amt_net_ordered,amt_tax,amt_cost,amt_list,amt_discount,dd_line_category,dd_order_quantity_uom,dd_order_number,
dd_line_number,dd_shipment_number,dd_orig_ref_doc_number,dd_cust_po_number,dd_otif_flag,dd_cancelled_h_flag,dd_cancelled_l_flag,
dd_open_flag,dd_fulfilled_flag,dd_shippable_flag,dd_tax_exempt_flag,dd_booked_flag,dd_model_remnant_flag,dd_on_hold_flag,
dd_financial_backlog_flag,dd_operation_backlog_flag,dd_hold_name,dd_header_id,dd_line_id,dd_transactional_curr_code,dd_functional_curr_code,
dd_shipped_not_invoiced_flag,ct_so_touch_count,dd_line_flow_status_code,dd_shipping_interfaced_flag,amt_exchangerate,amt_exchangerate_gbl,dim_ora_created_byid,
dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_date_last_updateid,dim_ora_mtl_plannersid,dim_projectsourceid,
snapshot_date, dim_ora_date_snapshotid,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_sales_order_change' ) + ROW_NUMBER() OVER() fact_ora_sales_order_changeID,
f.fact_ora_sales_order_lineid,
f.dim_ora_account_repid,
f.dim_ora_business_orgid,
f.dim_ora_channel_typeid,
f.dim_ora_company_orgid,
f.dim_ora_cost_centerid,
f.dim_ora_customer_bill_to_locationid,
f.dim_ora_customer_ship_to_locationid,
f.dim_ora_customer_sold_to_locationid,
f.dim_ora_customer_bill_toid,
f.dim_ora_customer_ship_toid,
f.dim_ora_customer_sold_toid,
f.dim_ora_customer_account_bill_toid,
f.dim_ora_customer_account_ship_toid,
f.dim_ora_customer_account_sold_toid,
f.dim_ora_inventory_orgid,
f.dim_ora_inv_productid,
f.dim_ora_order_line_statusid,
f.dim_ora_payment_methodid,
f.dim_ora_payment_termid,
f.dim_ora_productid,
f.dim_ora_sales_groupid,
f.dim_ora_sales_office_locationid,
f.dim_ora_salesrepid,
f.dim_ora_servicerepid,
f.dim_ora_order_line_typeid,
f.dim_ora_order_header_typeid,
f.dim_ora_order_header_statusid,
f.dim_ora_freight_termid,
f.dim_ora_shipment_methodid,
f.dim_ora_date_order_bookedid,
f.dim_ora_date_customer_requestedid,
f.dim_ora_date_enteredid,
f.dim_ora_date_orderedid,
f.dim_ora_date_promise_deliveryid,
f.dim_ora_date_scheduled_shipid,
f.dim_ora_date_actual_shipid,
f.dim_ora_date_earliest_shipmentid,
f.dim_ora_date_order_confirmedid,
f.dim_ora_date_actual_fulfillmentid,
f.dim_ora_date_fulfillmentid,
f.dim_ora_date_actual_arrival_dateid,
f.dim_ora_date_schedule_arrivalid,
f.dim_ora_date_taxid,
f.dim_ora_date_pricingid,
f.dim_ora_gl_set_of_booksid,
f.dim_ora_hr_operatingunitid,
f.dim_ora_aging_days_bucketid,
f.ct_aging_days,
f.ct_product_unit_cost,
f.ct_product_unit_list_price,
f.ct_product_unit_selling_price,
f.ct_manufacturing_lead_time,
f.ct_ship_tolerance_above,
f.ct_ship_tolerance_below,
f.ct_delivery_lead_time,
f.ct_ordered_quantity,
f.ct_cancelled_quantity,
f.ct_sales_quantity,
f.ct_fulfilled_quantity,
f.ct_confirmed_quantity,
f.ct_fill_quantity,
f.ct_fill_quantity_crd,
f.ct_total_shipped_quantity,
f.ct_total_invoiced_quantity,
f.amt_net_ordered,
f.amt_tax,
f.amt_cost,
f.amt_list,
f.amt_discount,
f.dd_line_category,
f.dd_order_quantity_uom,
f.dd_order_number,
f.dd_line_number,
f.dd_shipment_number,
f.dd_orig_ref_doc_number,
f.dd_cust_po_number,
f.dd_otif_flag,
f.dd_cancelled_h_flag,
f.dd_cancelled_l_flag,
f.dd_open_flag,
f.dd_fulfilled_flag,
f.dd_shippable_flag,
f.dd_tax_exempt_flag,
f.dd_booked_flag,
f.dd_model_remnant_flag,
f.dd_on_hold_flag,
f.dd_financial_backlog_flag,
f.dd_operation_backlog_flag,
f.dd_hold_name,
f.dd_header_id,
f.dd_line_id,
f.dd_transactional_curr_code,
f.dd_functional_curr_code,
f.dd_shipped_not_invoiced_flag,
f.ct_so_touch_count,
f.dd_line_flow_status_code,
f.dd_shipping_interfaced_flag,
f.amt_exchangerate,
f.amt_exchangerate_gbl,
f.dim_ora_created_byid,
f.dim_ora_last_updated_byid,
f.dim_ora_date_creationid,
f.dim_ora_date_last_updateid,
f.dim_ora_mtl_plannersid,
f.dim_projectsourceid,
current_timestamp snapshot_date, 
1 dim_ora_date_snapshotid,
f.dw_insert_date,
f.dw_update_date,
f.rowstartdate,
f.rowenddate,
f.rowiscurrent,
f.rowchangereason,
f.source_id
FROM 
ORA_OE_ORDER_HEADER_LINE s
join fact_ora_sales_order_line f
on f.DD_HEADER_ID = s.HEADER_ID AND f.DD_LINE_ID   = s.LINE_ID
join  fact_ora_sales_order_change_tmp t
on f.fact_ora_sales_order_lineid = t.fact_ora_sales_order_lineid
where t.last_update_date <ansidate(s.last_update_date);

drop if exists fact_ora_sales_order_change_del_tmp;

create table fact_ora_sales_order_change_del_tmp as 
select f.fact_ora_sales_order_lineid, f.dim_ora_date_last_updateid 
from fact_ora_sales_order_line f 
join ORA_OE_ORDER_HEADER_LINE s 
on f.DD_HEADER_ID = s.HEADER_ID AND f.DD_LINE_ID = s.LINE_ID 
join dim_date d on d.dim_dateid = f.dim_ora_date_last_updateid 
join fact_ora_sales_order_change_tmp t 
on f.fact_ora_sales_order_lineid = t.fact_ora_sales_order_lineid 
and t.last_update_date =ansidate(s.last_update_date) 
and t.last_update_date = d.datevalue;

delete from fact_ora_sales_order_change c 
where exists (select fact_ora_sales_order_lineid, dim_ora_date_last_updateid 
from fact_ora_sales_order_change_del_tmp t 
where c.fact_ora_sales_order_lineid = t.fact_ora_sales_order_lineid 
and c.dim_ora_date_last_updateid = t.dim_ora_date_last_updateid);

drop if exists fact_ora_sales_order_change_del_tmp;

drop if exists fact_ora_sales_order_line_tmp;

create table fact_ora_sales_order_line_tmp as 
select f.fact_ora_sales_order_lineid 
from ORA_OE_ORDER_HEADER_LINE s 
join fact_ora_sales_order_line f on f.DD_HEADER_ID = s.HEADER_ID 
AND f.DD_LINE_ID = s.LINE_ID 
join fact_ora_sales_order_change_tmp t 
on f.fact_ora_sales_order_lineid = t.fact_ora_sales_order_lineid 
where t.last_update_date =ansidate(s.last_update_date);

INSERT INTO fact_ora_sales_order_change
(
fact_ora_sales_order_changeid,
fact_ora_sales_order_lineid,dim_ora_account_repid,dim_ora_business_orgid,dim_ora_channel_typeid,dim_ora_company_orgid,dim_ora_cost_centerid,dim_ora_customer_bill_to_locationid,
dim_ora_customer_ship_to_locationid,dim_ora_customer_sold_to_locationid,dim_ora_customer_bill_toid,dim_ora_customer_ship_toid,dim_ora_customer_sold_toid,
dim_ora_customer_account_bill_toid,dim_ora_customer_account_ship_toid,dim_ora_customer_account_sold_toid,dim_ora_inventory_orgid,dim_ora_inv_productid,dim_ora_order_line_statusid,
dim_ora_payment_methodid,dim_ora_payment_termid,dim_ora_productid,dim_ora_sales_groupid,dim_ora_sales_office_locationid,dim_ora_salesrepid,dim_ora_servicerepid,
dim_ora_order_line_typeid,dim_ora_order_header_typeid,dim_ora_order_header_statusid,dim_ora_freight_termid,dim_ora_shipment_methodid,dim_ora_date_order_bookedid,
dim_ora_date_customer_requestedid,dim_ora_date_enteredid,dim_ora_date_orderedid,dim_ora_date_promise_deliveryid,dim_ora_date_scheduled_shipid,
dim_ora_date_actual_shipid,dim_ora_date_earliest_shipmentid,dim_ora_date_order_confirmedid,dim_ora_date_actual_fulfillmentid,dim_ora_date_fulfillmentid,
dim_ora_date_actual_arrival_dateid,dim_ora_date_schedule_arrivalid,dim_ora_date_taxid,dim_ora_date_pricingid,dim_ora_gl_set_of_booksid,dim_ora_hr_operatingunitid,
dim_ora_aging_days_bucketid,ct_aging_days,ct_product_unit_cost,ct_product_unit_list_price,ct_product_unit_selling_price,ct_manufacturing_lead_time,
ct_ship_tolerance_above,ct_ship_tolerance_below,ct_delivery_lead_time,ct_ordered_quantity,ct_cancelled_quantity,ct_sales_quantity,
ct_fulfilled_quantity,ct_confirmed_quantity,ct_fill_quantity,ct_fill_quantity_crd,ct_total_shipped_quantity,
ct_total_invoiced_quantity,amt_net_ordered,amt_tax,amt_cost,amt_list,amt_discount,dd_line_category,dd_order_quantity_uom,dd_order_number,
dd_line_number,dd_shipment_number,dd_orig_ref_doc_number,dd_cust_po_number,dd_otif_flag,dd_cancelled_h_flag,dd_cancelled_l_flag,
dd_open_flag,dd_fulfilled_flag,dd_shippable_flag,dd_tax_exempt_flag,dd_booked_flag,dd_model_remnant_flag,dd_on_hold_flag,
dd_financial_backlog_flag,dd_operation_backlog_flag,dd_hold_name,dd_header_id,dd_line_id,dd_transactional_curr_code,dd_functional_curr_code,
dd_shipped_not_invoiced_flag,ct_so_touch_count,dd_line_flow_status_code,dd_shipping_interfaced_flag,amt_exchangerate,amt_exchangerate_gbl,dim_ora_created_byid,
dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_date_last_updateid,dim_ora_mtl_plannersid,dim_projectsourceid,
snapshot_date, dim_ora_date_snapshotid,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_sales_order_change' ) + ROW_NUMBER() OVER() fact_ora_sales_order_changeID,
f.fact_ora_sales_order_lineid,
f.dim_ora_account_repid,
f.dim_ora_business_orgid,
f.dim_ora_channel_typeid,
f.dim_ora_company_orgid,
f.dim_ora_cost_centerid,
f.dim_ora_customer_bill_to_locationid,
f.dim_ora_customer_ship_to_locationid,
f.dim_ora_customer_sold_to_locationid,
f.dim_ora_customer_bill_toid,
f.dim_ora_customer_ship_toid,
f.dim_ora_customer_sold_toid,
f.dim_ora_customer_account_bill_toid,
f.dim_ora_customer_account_ship_toid,
f.dim_ora_customer_account_sold_toid,
f.dim_ora_inventory_orgid,
f.dim_ora_inv_productid,
f.dim_ora_order_line_statusid,
f.dim_ora_payment_methodid,
f.dim_ora_payment_termid,
f.dim_ora_productid,
f.dim_ora_sales_groupid,
f.dim_ora_sales_office_locationid,
f.dim_ora_salesrepid,
f.dim_ora_servicerepid,
f.dim_ora_order_line_typeid,
f.dim_ora_order_header_typeid,
f.dim_ora_order_header_statusid,
f.dim_ora_freight_termid,
f.dim_ora_shipment_methodid,
f.dim_ora_date_order_bookedid,
f.dim_ora_date_customer_requestedid,
f.dim_ora_date_enteredid,
f.dim_ora_date_orderedid,
f.dim_ora_date_promise_deliveryid,
f.dim_ora_date_scheduled_shipid,
f.dim_ora_date_actual_shipid,
f.dim_ora_date_earliest_shipmentid,
f.dim_ora_date_order_confirmedid,
f.dim_ora_date_actual_fulfillmentid,
f.dim_ora_date_fulfillmentid,
f.dim_ora_date_actual_arrival_dateid,
f.dim_ora_date_schedule_arrivalid,
f.dim_ora_date_taxid,
f.dim_ora_date_pricingid,
f.dim_ora_gl_set_of_booksid,
f.dim_ora_hr_operatingunitid,
f.dim_ora_aging_days_bucketid,
f.ct_aging_days,
f.ct_product_unit_cost,
f.ct_product_unit_list_price,
f.ct_product_unit_selling_price,
f.ct_manufacturing_lead_time,
f.ct_ship_tolerance_above,
f.ct_ship_tolerance_below,
f.ct_delivery_lead_time,
f.ct_ordered_quantity,
f.ct_cancelled_quantity,
f.ct_sales_quantity,
f.ct_fulfilled_quantity,
f.ct_confirmed_quantity,
f.ct_fill_quantity,
f.ct_fill_quantity_crd,
f.ct_total_shipped_quantity,
f.ct_total_invoiced_quantity,
f.amt_net_ordered,
f.amt_tax,
f.amt_cost,
f.amt_list,
f.amt_discount,
f.dd_line_category,
f.dd_order_quantity_uom,
f.dd_order_number,
f.dd_line_number,
f.dd_shipment_number,
f.dd_orig_ref_doc_number,
f.dd_cust_po_number,
f.dd_otif_flag,
f.dd_cancelled_h_flag,
f.dd_cancelled_l_flag,
f.dd_open_flag,
f.dd_fulfilled_flag,
f.dd_shippable_flag,
f.dd_tax_exempt_flag,
f.dd_booked_flag,
f.dd_model_remnant_flag,
f.dd_on_hold_flag,
f.dd_financial_backlog_flag,
f.dd_operation_backlog_flag,
f.dd_hold_name,
f.dd_header_id,
f.dd_line_id,
f.dd_transactional_curr_code,
f.dd_functional_curr_code,
f.dd_shipped_not_invoiced_flag,
f.ct_so_touch_count,
f.dd_line_flow_status_code,
f.dd_shipping_interfaced_flag,
f.amt_exchangerate,
f.amt_exchangerate_gbl,
f.dim_ora_created_byid,
f.dim_ora_last_updated_byid,
f.dim_ora_date_creationid,
f.dim_ora_date_last_updateid,
f.dim_ora_mtl_plannersid,
f.dim_projectsourceid,
current_timestamp snapshot_date, 
1 dim_ora_date_snapshotid,
f.dw_insert_date,
f.dw_update_date,
f.rowstartdate,
f.rowenddate,
f.rowiscurrent,
f.rowchangereason,
f.source_id
FROM 
fact_ora_sales_order_line f 
join fact_ora_sales_order_line_tmp t 
on f.fact_ora_sales_order_lineid = t.fact_ora_sales_order_lineid;


drop if exists fact_ora_sales_order_change_tmp;
drop if exists fact_ora_sales_order_line_tmp;


/*dim_ora_date_snapshotid*/
UPDATE fact_ora_sales_order_change f
from DIM_DATE D 
SET 
f.dim_ora_date_snapshotid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE ansidate(f.SNAPSHOT_DATE) = D.DATEVALUE
AND f.DIM_ORA_DATE_SNAPSHOTID =1;

/*capture all the Sales Order Line where the Schedule Ship Date has changed*/
drop if exists fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_1_tmp;
drop if exists fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_2_tmp;
drop if exists fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_3_tmp;

create table fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_1_tmp as
select fact_ora_sales_order_lineid, DIM_ORA_DATE_SCHEDULED_SHIPID
from fact_ora_sales_order_change
where snapshot_date = timestamp(ansidate(current_timestamp));

create table fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_2_tmp as
select fact_ora_sales_order_lineid, max(snapshot_date) snapshot_date
from fact_ora_sales_order_change 
where snapshot_date<timestamp(ansidate(current_timestamp))
group by fact_ora_sales_order_lineid;

create table fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_3_tmp as
select c.fact_ora_sales_order_lineid, c.DIM_ORA_DATE_SCHEDULED_SHIPID
from fact_ora_sales_order_change c
join fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_2_tmp t 
on c.fact_ora_sales_order_lineid = t.fact_ora_sales_order_lineid
and c.snapshot_date = t.snapshot_date;

update fact_ora_sales_order_change c
from fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_1_tmp t1
join fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_3_tmp t3 on t1.fact_ora_sales_order_lineid = t3.fact_ora_sales_order_lineid 
set c.rowchangereason = 'SSD DATE'
where c.fact_ora_sales_order_lineid = t1.fact_ora_sales_order_lineid and c.snapshot_date = timestamp(ansidate(current_timestamp))
and t1.DIM_ORA_DATE_SCHEDULED_SHIPID <> t3.DIM_ORA_DATE_SCHEDULED_SHIPID;

drop if exists fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_1_tmp;
drop if exists fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_2_tmp;
drop if exists fact_ora_sales_order_change_DIM_ORA_DATE_SCHEDULED_SHIPID_3_tmp;

/*Added by Victor on 11 May 2015 */
UPDATE fact_ora_sales_order_line f_soc
FROM (
		SELECT
		        dd_line_id  
		       ,TIMESTAMPDIFF(DAY, MIN(datevalue), MAX(datevalue)) diff_days
		    FROM fact_ora_sales_order_change f_soc
		       ,dim_date dcrq
		   WHERE f_soc.dim_ora_date_customer_requestedid = dcrq.dim_dateid
		  GROUP BY dd_line_id ) temp
SET ct_PushOut_PullIn = temp.diff_days
WHERE f_soc.dd_line_id = temp.dd_line_id
  AND IFNULL(ct_PushOut_PullIn,0) <> IFNULL(temp.diff_days,-1);
  
DROP TABLE IF EXISTS tmp_lineo_dates;
CREATE TABLE tmp_lineo_dates AS
SELECT
        dd_line_id  
       ,MIN(dim_ora_date_customer_requestedid) min_date
       ,MAX(dim_ora_date_customer_requestedid) max_date
    FROM fact_ora_sales_order_change f_soc
  GROUP BY dd_line_id;

UPDATE fact_ora_sales_order_line f_soc
FROM ( 
		 SELECT 
		       tmp.dd_line_id
		      ,f_soc1.ct_ordered_quantity - f_soc2.ct_ordered_quantity ct_order_change
		  FROM tmp_lineo_dates tmp
		      ,fact_ora_sales_order_change f_soc1
		      ,fact_ora_sales_order_change f_soc2
		WHERE tmp.dd_line_id = f_soc1.dd_line_id 
		  AND tmp.dd_line_id = f_soc2.dd_line_id 
		  AND tmp.min_date = f_soc1.dim_ora_date_customer_requestedid
		  AND tmp.max_date = f_soc2.dim_ora_date_customer_requestedid 
		  ) tmp
SET f_soc.ct_order_change  = IFNULL(tmp.ct_order_change,0)
WHERE f_soc.dd_line_id = tmp.dd_line_id 
  AND IFNULL(f_soc.ct_order_change,0) <> IFNULL(tmp.ct_order_change,-1);  
  
/*END Added by Victor on 11 May 2015 */  

/*call vectorwise combine*/
CALL VECTORWISE( COMBINE 'fact_ora_sales_order_change');