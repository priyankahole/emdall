/*set session authorization oracle_data*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_invonhand';	
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_invonhand',IFNULL(MAX(fact_ora_invonhandid),0)
FROM fact_ora_invonhand;

/*DELETE FROM fact_ora_invonhand*/
/*select count(*) from fact_ora_invonhand*/
/*select count(*) from ORA_MTL_ONHAND_RESERVE*/

/*delete rows*/
/*DELETE FROM fact_ora_invonhand*/
MODIFY fact_ora_invonhand TO TRUNCATED;

/*insert new rows*/
INSERT INTO fact_ora_invonhand
(
fact_ora_invonhandid,DIM_ORA_INV_PRODUCTID,DIM_ORA_PRODUCTID,DIM_ORA_INV_ORGID,DIM_ORA_MTL_ITEM_REVID,
DIM_ORA_MTL_ITEM_LOCATORID,DIM_ORA_MTL_SEC_INVENTORYID,CT_TRANSACTION_QUANTITY,
DD_TRANSACTION_UOM_CODE,CT_PRIMARY_TRANSACTION_QUANTITY,DIM_ORA_DATE_RECEIVEDID,
fact_ORA_INVMTL_CREATE_TRXID,fact_ORA_INVMTL_UPDATE_TRXID,DIM_ORA_DATE_ORIG_RECEIVEDID,
DD_CONTAINERIZED_FLAG,DIM_ORA_ORGANIZATION_TYPEID,DIM_ORA_OWNING_ORGANIZATIONID,
DIM_ORA_OWNINING_TP_TYPEID,DIM_ORA_PLANNING_ORGANIZATIONID,DIM_ORA_PLANNING_TP_TYPEID,
DD_SECONDARY_UOM_CODE,CT_SECONDARY_TRANSACTION_QUANTITY,DD_LOT_NUMBER,
DIM_ORA_DATE_REQUIREMENTID,CT_RESERVATION_QUANTITY,DD_SHIP_READY_FLAG,CT_DETAILED_QUANTITY,
DIM_ORA_DATE_DEMAND_SHIPID,CT_DEMAND_QUANTITY,dd_aging_flag,CREATE_TRANSACTION_ID,UPDATE_TRANSACTION_ID,RESERVATION_ID,INVENTORY_ITEM_ID,
ORGANIZATION_ID,
amt_exchangerate,amt_exchangerate_gbl,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,SOURCE_ID
)
SELECT
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_invonhand' ) + ROW_NUMBER() OVER() fact_ora_invonhandid,
1 AS DIM_ORA_INV_PRODUCTID,
1 AS DIM_ORA_PRODUCTID,
1 AS DIM_ORA_INV_ORGID,
1 AS DIM_ORA_MTL_ITEM_REVID,
1 AS DIM_ORA_MTL_ITEM_LOCATORID,
1 AS DIM_ORA_MTL_SEC_INVENTORYID,
S.ON_HAND AS CT_TRANSACTION_QUANTITY,
S.TRANSACTION_UOM_CODE AS DD_TRANSACTION_UOM_CODE,
S.PRIMARY_TRANSACTION_QUANTITY AS CT_PRIMARY_TRANSACTION_QUANTITY,
1 AS DIM_ORA_DATE_RECEIVEDID,
0 AS fact_ORA_INVMTL_CREATE_TRXID,
0 AS fact_ORA_INVMTL_UPDATE_TRXID,
1 AS DIM_ORA_DATE_ORIG_RECEIVEDID,
ifnull(S.CONTAINERIZED_FLAG,0) AS DD_CONTAINERIZED_FLAG,
1 as DIM_ORA_ORGANIZATION_TYPEID,
1 AS DIM_ORA_OWNING_ORGANIZATIONID,
1 AS DIM_ORA_OWNINING_TP_TYPEID,
1 AS DIM_ORA_PLANNING_ORGANIZATIONID,
1 AS DIM_ORA_PLANNING_TP_TYPEID,
ifnull(S.SECONDARY_UOM_CODE,'Not Set') AS DD_SECONDARY_UOM_CODE,
ifnull(S.SECONDARY_TRANSACTION_QUANTITY,0) AS CT_SECONDARY_TRANSACTION_QUANTITY,
ifnull(S.LOT_NUMBER,'Not Set') AS DD_LOT_NUMBER,
1 AS DIM_ORA_DATE_REQUIREMENTID,
ifnull(S.RESERVATION_QUANTITY,0) AS CT_RESERVATION_QUANTITY,
ifnull(S.SHIP_READY_FLAG,0) AS DD_SHIP_READY_FLAG,
ifnull(S.DETAILED_QUANTITY,0) AS CT_DETAILED_QUANTITY,
1 AS DIM_ORA_DATE_DEMAND_SHIPID,
0 AS CT_DEMAND_QUANTITY,
'Not Set' dd_aging_flag,
ifnull(S.CREATE_TRANSACTION_ID,0) AS CREATE_TRANSACTION_ID,
ifnull(S.UPDATE_TRANSACTION_ID,0) AS UPDATE_TRANSACTION_ID,
ifnull(S.RESERVATION_ID,0) AS RESERVATION_ID,
S.INVENTORY_ITEM_ID AS INVENTORY_ITEM_ID,
S.ORGANIZATION_ID AS ORGANIZATION_ID,
1 as amt_exchangerate,
1 as amt_exchangerate_gbl,
current_timestamp AS DW_INSERT_DATE,
current_timestamp AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ORA_MTL_ONHAND_RESERVE S LEFT JOIN fact_ora_invonhand F ON
F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND
F.ORGANIZATION_ID = S.ORGANIZATION_ID AND
F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND
F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND
ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND
ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
WHERE F.INVENTORY_ITEM_ID IS NULL AND F.ORGANIZATION_ID IS NULL; 

/*UPDATE DIM_ORA_INV_PRODUCTID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_INVPRODUCT D 
ON VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.INVENTORY_ITEM_ID, 200) = D.KEY_ID and d.rowiscurrent = 1
SET 
F.DIM_ORA_INV_PRODUCTID = D.DIM_ORA_INVPRODUCTID,
DW_UPDATE_DATE = current_timestamp
WHERE F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_INV_PRODUCTID <> D.DIM_ORA_INVPRODUCTID;

/*UPDATE DIM_ORA_PRODUCTID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_PRODUCT D ON S.INVENTORY_ITEM_ID = D.KEY_ID and d.rowiscurrent = 1
SET 
F.DIM_ORA_PRODUCTID = D.DIM_ORA_PRODUCTID,
DW_UPDATE_DATE = current_timestamp
WHERE F.INVENTORY_ITEM_ID = S.INVENTORY_ITEM_ID AND F.ORGANIZATION_ID = S.ORGANIZATION_ID AND F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_PRODUCTID <> D.DIM_ORA_PRODUCTID;

/*UPDATE DIM_ORA_INV_ORGID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_BUSINESS_ORG D 
ON  'INV' + '~' +VARCHAR(S.ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_INV_ORGID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_INV_ORGID <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE DIM_ORA_MTL_ITEM_REVID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_MTL_ITEM_REV D 
ON VARCHAR(S.INVENTORY_ITEM_ID, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) +'~' + VARCHAR(S.REVISION, 200)  = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.DIM_ORA_MTL_ITEM_REVID = D.DIM_ORA_MTL_ITEM_REVID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_MTL_ITEM_REVID <> D.DIM_ORA_MTL_ITEM_REVID;

/*UPDATE DIM_ORA_MTL_ITEM_LOCATORID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_MTL_ITEM_LOCATIONS D 
ON S.LOCATOR_ID = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_MTL_ITEM_LOCATORID = D.DIM_ORA_MTL_ITEM_LOCATIONSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_MTL_ITEM_LOCATORID <> D.DIM_ORA_MTL_ITEM_LOCATIONSID;

/*UPDATE DIM_ORA_MTL_SEC_INVENTORYID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_MTL_SEC_INVENTORY D 
ON D.KEY_ID = VARCHAR(S.SUBINVENTORY_CODE, 200) +'~' + VARCHAR(S.ORGANIZATION_ID, 200) and d.rowiscurrent = 1
SET 
F.DIM_ORA_MTL_SEC_INVENTORYID = D.DIM_ORA_MTL_SEC_INVENTORYID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_MTL_SEC_INVENTORYID <> D.DIM_ORA_MTL_SEC_INVENTORYID;

/*UPDATE DIM_ORA_DATE_RECEIVEDID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_DATE D ON ANSIDATE(S.DATE_RECEIVED) = D.DATEVALUE
SET 
F.DIM_ORA_DATE_RECEIVEDID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_DATE_RECEIVEDID <> D.DIM_DATEID;

/*UPDATE fact_ORA_INVMTL_CREATE_TRXID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN FACT_ORA_INVMTLTRX D ON S.CREATE_TRANSACTION_ID = D.TRANSACTION_ID and d.rowiscurrent = 1
SET 
F.fact_ORA_INVMTL_CREATE_TRXID = D.FACT_ORA_INVMTLTRXID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.fact_ORA_INVMTL_CREATE_TRXID <> D.FACT_ORA_INVMTLTRXID;

/*UPDATE fact_ORA_INVMTL_UPDATE_TRXID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN FACT_ORA_INVMTLTRX D ON S.UPDATE_TRANSACTION_ID = D.TRANSACTION_ID and d.rowiscurrent = 1
SET 
F.fact_ORA_INVMTL_UPDATE_TRXID = D.FACT_ORA_INVMTLTRXID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.fact_ORA_INVMTL_UPDATE_TRXID <> D.FACT_ORA_INVMTLTRXID;

/*UPDATE DIM_ORA_DATE_ORIG_RECEIVEDID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_DATE D ON ANSIDATE(S.ORIG_DATE_RECEIVED) = D.DATEVALUE 
SET 
F.DIM_ORA_DATE_ORIG_RECEIVEDID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_DATE_ORIG_RECEIVEDID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_OWNING_ORGANIZATIONID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_BUSINESS_ORG D 
ON  'HR_BG' + '~' +VARCHAR(S.OWNING_ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1   
SET 
F.DIM_ORA_OWNING_ORGANIZATIONID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_OWNING_ORGANIZATIONID <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE DIM_ORA_PLANNING_ORGANIZATIONID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_BUSINESS_ORG D 
ON  'HR_BG' + '~' +VARCHAR(S.PLANNING_ORGANIZATION_ID,200) = D.KEY_ID and d.rowiscurrent = 1 
SET 
F.DIM_ORA_PLANNING_ORGANIZATIONID = D.DIM_ORA_BUSINESS_ORGID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_PLANNING_ORGANIZATIONID <> D.DIM_ORA_BUSINESS_ORGID;

/*UPDATE DIM_ORA_DATE_REQUIREMENTID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_DATE D ON ANSIDATE(S.REQUIREMENT_DATE) = D.DATEVALUE  
SET 
F.DIM_ORA_DATE_REQUIREMENTID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_DATE_REQUIREMENTID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_DATE_DEMAND_SHIPID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_DATE D ON ANSIDATE(S.DEMAND_SHIP_DATE) = D.DATEVALUE  
SET 
F.DIM_ORA_DATE_DEMAND_SHIPID = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
AND F.DIM_ORA_DATE_DEMAND_SHIPID <> D.DIM_DATEID;

/*UPDATE DIM_ORA_ORGANIZATION_TYPEID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_FND_LOOKUP D ON S.ORGANIZATION_TYPE = D.LOOKUP_CODE AND D.lookup_type ='MTL_TP_TYPES' and d.rowiscurrent = 1
SET 
F.DIM_ORA_ORGANIZATION_TYPEID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
and F.DIM_ORA_ORGANIZATION_TYPEID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_OWNINING_TP_TYPEID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_FND_LOOKUP D ON S.OWNING_TP_TYPE = D.LOOKUP_CODE AND D.lookup_type ='MTL_TP_TYPES' and d.rowiscurrent = 1
SET 
F.DIM_ORA_OWNINING_TP_TYPEID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
and F.DIM_ORA_OWNINING_TP_TYPEID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE DIM_ORA_PLANNING_TP_TYPEID*/
UPDATE fact_ora_invonhand F
FROM ORA_MTL_ONHAND_RESERVE S JOIN DIM_ORA_FND_LOOKUP D ON S.PLANNING_TP_TYPE = D.LOOKUP_CODE AND D.lookup_type ='MTL_TP_TYPES' and d.rowiscurrent = 1
SET 
F.DIM_ORA_PLANNING_TP_TYPEID = D.DIM_ORA_FND_LOOKUPID,
DW_UPDATE_DATE = current_timestamp
WHERE F.CREATE_TRANSACTION_ID = S.CREATE_TRANSACTION_ID AND F.UPDATE_TRANSACTION_ID = S.UPDATE_TRANSACTION_ID AND ifnull(F.RESERVATION_ID,0) = ifnull(S.RESERVATION_ID,0) AND ifnull(F.DD_LOT_NUMBER,'Not Set')     = ifnull(S.LOT_NUMBER,'Not Set')
and F.DIM_ORA_PLANNING_TP_TYPEID <> D.DIM_ORA_FND_LOOKUPID;

/*UPDATE CT_DEMAND_QUANTITY*/
DROP IF EXISTS fact_ora_sales_order_line_quantity_open_orders;

CREATE TABLE fact_ora_sales_order_line_quantity_open_orders as 
select 
sol.fact_ora_sales_order_lineid,
sol.dim_ora_inv_productid,
sol.ct_ordered_quantity quantity 
from fact_ora_sales_order_line sol
join dim_ora_xacttype cttype
on cttype.dim_ora_xacttypeid = sol.dim_ora_order_line_typeid and sol.rowiscurrent = 1 and cttype.rowiscurrent = 1
join dim_ora_fnd_lookup lfsc
on lfsc.dim_ora_fnd_lookupid = sol.dim_ora_order_line_statusid and lfsc.rowiscurrent = 1
where sol.ct_ordered_quantity<>0
and not(cttype.NAME like '%Return%')
and not (lfsc.lookup_code like '%CANCELLED%' or lfsc.lookup_code like '%CLOSED%' or lfsc.lookup_code like '%REJECTED%')
order by sol.fact_ora_sales_order_lineid;

DROP IF EXISTS fact_ora_sales_order_line_quantity_last3months;

CREATE TABLE fact_ora_sales_order_line_quantity_last3months as 
select 
sol.fact_ora_sales_order_lineid,
sol.dim_ora_inv_productid,
sol.ct_ordered_quantity quantity 
from fact_ora_sales_order_line sol
join dim_ora_xacttype cttype
on cttype.dim_ora_xacttypeid = sol.dim_ora_order_line_typeid and sol.rowiscurrent = 1 and cttype.rowiscurrent = 1
join dim_ora_fnd_lookup lfsc
on lfsc.dim_ora_fnd_lookupid = sol.dim_ora_order_line_statusid and lfsc.rowiscurrent = 1
join dim_date d
on d.dim_dateid = sol.dim_ora_date_orderedid
where sol.ct_ordered_quantity<>0
and not(cttype.NAME like '%Return%')
and lfsc.lookup_code like '%CLOSED%'
and d.datevalue >= ansidate(current_timestamp)-90
order by sol.fact_ora_sales_order_lineid;

DROP IF EXISTS fact_ora_sales_order_line_quantity;

CREATE TABLE fact_ora_sales_order_line_quantity
as
select dim_ora_inv_productid, sum(quantity) quantity
from 
(
select fact_ora_sales_order_lineid,dim_ora_inv_productid, quantity from fact_ora_sales_order_line_quantity_open_orders
UNION
select fact_ora_sales_order_lineid,dim_ora_inv_productid, quantity from fact_ora_sales_order_line_quantity_last3months
) t
group by dim_ora_inv_productid
order by dim_ora_inv_productid;

DROP IF EXISTS fact_ora_invonhand_byinvprod_tmp; 

CREATE TABLE fact_ora_invonhand_byinvprod_tmp as
select dim_ora_inv_productid,dim_ora_inv_orgid,sum(ct_transaction_quantity) ct_transaction_quantity
from fact_ora_invonhand
where rowiscurrent = 1
group by dim_ora_inv_productid,dim_ora_inv_orgid;

UPDATE fact_ora_invonhand F
FROM fact_ora_invonhand_byinvprod_tmp tmpprod
JOIN fact_ora_sales_order_line_quantity tmp
on tmpprod.dim_ora_inv_productid =  tmp.dim_ora_inv_productid
SET F.CT_DEMAND_QUANTITY = 
case when tmpprod.ct_transaction_quantity = 0 then 0
else
round(tmp.QUANTITY *( f.ct_transaction_quantity/ tmpprod.ct_transaction_quantity),2)
end
WHERE 
f.dim_ora_inv_productid = tmpprod.dim_ora_inv_productid
and f.dim_ora_inv_orgid = tmpprod.dim_ora_inv_orgid;

DROP TABLE fact_ora_invonhand_byinvprod_tmp;
DROP TABLE fact_ora_sales_order_line_quantity_open_orders;
DROP TABLE fact_ora_sales_order_line_quantity_last3months;
DROP TABLE fact_ora_sales_order_line_quantity;

/*dd_aging_flag*/
DROP IF EXISTS fact_ora_invonhand_items_tmp;

create table fact_ora_invonhand_items_tmp as
select distinct dim_ora_inv_productid
from fact_ora_invonhand
where rowiscurrent = 1
order by dim_ora_inv_productid;

DROP IF EXISTS fact_ora_invmtltrx_tmp;

CREATE table fact_ora_invmtltrx_tmp as
select trx.dim_ora_inv_productid, max(d.datevalue) transaction_date, 
case when TIMESTAMPDIFF(DAY,max(d.datevalue), ansidate(current_timestamp))<100 then 1 else 0 end before_100_days
from fact_ora_invmtltrx trx
join fact_ora_invonhand_items_tmp tmp 
on trx.dim_ora_inv_productid =  tmp.dim_ora_inv_productid and trx.rowiscurrent = 1
join dim_date d
on d.dim_Dateid = trx.DIM_ORA_DATE_TRANSACTIONID
group by trx.dim_ora_inv_productid;

UPDATE fact_ora_invonhand inv
FROM fact_ora_invmtltrx_tmp tmp
SET dd_aging_flag = case when tmp.before_100_days = 1 then 'N' else 'Y' end
WHERE inv.dim_ora_inv_productid = tmp.dim_ora_inv_productid;

DROP IF EXISTS fact_ora_invonhand_items_tmp;
DROP IF EXISTS fact_ora_invmtltrx_tmp;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_invonhand');