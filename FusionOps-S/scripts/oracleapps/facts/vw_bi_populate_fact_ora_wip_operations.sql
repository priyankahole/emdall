/*set session authorization oracle_data*/

/*DELETE FROM fact_ora_wip_operations*/
/*SELECT * FROM fact_ora_wip_operations*/

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_ora_wip_operations';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ora_wip_operations',IFNULL(MAX(fact_ora_wip_operationsid),0)
FROM fact_ora_wip_operations;

/*update fact columns*/
UPDATE fact_ora_wip_operations T	
FROM ora_wip_operations S
SET 
dd_description = ifnull(S.DESCRIPTION,'Not Set'),
ct_scheduled_quantity = S.SCHEDULED_QUANTITY,
ct_quantity_in_queue = S.QUANTITY_IN_QUEUE,
ct_quantity_running = S.QUANTITY_RUNNING,
ct_quantity_waiting_to_move = S.QUANTITY_WAITING_TO_MOVE,
ct_quantity_rejected = S.QUANTITY_REJECTED,
ct_quantity_scrapped = S.QUANTITY_SCRAPPED,
ct_quantity_completed = S.QUANTITY_COMPLETED,
dd_previous_operation_seq_num = ifnull(S.PREVIOUS_OPERATION_SEQ_NUM,0),
dd_next_operation_seq_num = ifnull(S.NEXT_OPERATION_SEQ_NUM,0),
dd_count_point_type = S.COUNT_POINT_TYPE,
dd_backflush_flag = S.BACKFLUSH_FLAG,
ct_minimum_transfer_quantity = S.MINIMUM_TRANSFER_QUANTITY,
dd_attribute_category = ifnull(S.ATTRIBUTE_CATEGORY,'Not Set'),
dd_attribute1 = ifnull(S.ATTRIBUTE1,'Not Set'),
dd_attribute2 = ifnull(S.ATTRIBUTE2,'Not Set'),
dd_attribute3 = ifnull(S.ATTRIBUTE3,'Not Set'),
dd_attribute4 = ifnull(S.ATTRIBUTE4,'Not Set'),
dd_attribute5 = ifnull(S.ATTRIBUTE5,'Not Set'),
dd_attribute6 = ifnull(S.ATTRIBUTE6,'Not Set'),
dd_attribute7 = ifnull(S.ATTRIBUTE7,'Not Set'),
dd_attribute8 = ifnull(S.ATTRIBUTE8,'Not Set'),
dd_attribute9 = ifnull(S.ATTRIBUTE9,'Not Set'),
dd_attribute10 = ifnull(S.ATTRIBUTE10,'Not Set'),
dd_attribute11 = ifnull(S.ATTRIBUTE11,'Not Set'),
dd_attribute12 = ifnull(S.ATTRIBUTE12,'Not Set'),
dd_attribute13 = ifnull(S.ATTRIBUTE13,'Not Set'),
dd_attribute14 = ifnull(S.ATTRIBUTE14,'Not Set'),
dd_attribute15 = ifnull(S.ATTRIBUTE15,'Not Set'),
dd_wf_itemtype = ifnull(S.WF_ITEMTYPE,'Not Set'),
dd_wf_itemkey = ifnull(S.WF_ITEMKEY,'Not Set'),
ct_operation_yield = ifnull(S.OPERATION_YIELD,0),
dd_operation_yield_enabled = ifnull(S.OPERATION_YIELD_ENABLED,0),
ct_pre_split_quantity = ifnull(S.PRE_SPLIT_QUANTITY,0),
dd_operation_completed = ifnull(S.OPERATION_COMPLETED,'Not Set'),
dd_shutdown_type = ifnull(S.SHUTDOWN_TYPE,'Not Set'),
ct_x_pos = ifnull(S.X_POS,0),
ct_y_pos = ifnull(S.Y_POS,0),
dd_skip_flag = ifnull(S.SKIP_FLAG,0),
dd_long_description = ifnull(S.LONG_DESCRIPTION,0),
ct_cumulative_scrap_quantity = ifnull(S.CUMULATIVE_SCRAP_QUANTITY,0),
dd_recommended = ifnull(S.RECOMMENDED,'Not Set'),
ct_progress_percentage = ifnull(S.PROGRESS_PERCENTAGE,0),
dd_wsm_op_seq_num = ifnull(S.WSM_OP_SEQ_NUM,0),
ct_wsm_bonus_quantity = ifnull(S.WSM_BONUS_QUANTITY,0),
dd_wsm_update_quantity_txn_id = ifnull(S.WSM_UPDATE_QUANTITY_TXN_ID,0),
ct_wsm_costed_quantity_completed = ifnull(S.WSM_COSTED_QUANTITY_COMPLETED,0),
ct_lowest_acceptable_yield = ifnull(S.LOWEST_ACCEPTABLE_YIELD,0),
dd_check_skill = ifnull(S.CHECK_SKILL,0),
dd_wip_entity_name = S.WIP_ENTITY_NAME,
dd_entity_type = S.ENTITY_TYPE,
dd_entity_description = ifnull(S.ENTITY_DESCRIPTION,'Not Set'),
DW_UPDATE_DATE = current_timestamp, 
rowiscurrent = 1,
SOURCE_ID = 'ORA 12.1'
WHERE
T.WIP_ENTITY_ID = S.WIP_ENTITY_ID and T.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and ifnull(T.REPETITIVE_SCHEDULE_ID,0) = ifnull(S.REPETITIVE_SCHEDULE_ID,0);

/*insert new rows*/
INSERT INTO fact_ora_wip_operations
(
fact_ora_wip_operationsid,
wip_entity_id,operation_seq_num,repetitive_schedule_id,
dim_ora_organizationid,dim_ora_repetitive_scheduleid,dim_ora_date_last_updateid,dim_ora_last_updated_byid,dim_ora_date_creationid,dim_ora_created_byid,
dim_ora_operation_sequenceid,dim_ora_standard_operationid,dim_ora_departmentid,dd_description,ct_scheduled_quantity,ct_quantity_in_queue,ct_quantity_running,
ct_quantity_waiting_to_move,ct_quantity_rejected,ct_quantity_scrapped,ct_quantity_completed,dim_ora_date_first_unit_startid,dim_ora_date_first_unit_completionid,
dim_ora_date_last_unit_startid,dim_ora_date_last_unit_completionid,dd_previous_operation_seq_num,dd_next_operation_seq_num,dd_count_point_type,dd_backflush_flag,
ct_minimum_transfer_quantity,dim_ora_date_last_movedid,dd_attribute_category,dd_attribute1,dd_attribute2,dd_attribute3,dd_attribute4,dd_attribute5,dd_attribute6,
dd_attribute7,dd_attribute8,dd_attribute9,dd_attribute10,dd_attribute11,dd_attribute12,dd_attribute13,dd_attribute14,dd_attribute15,dd_wf_itemtype,dd_wf_itemkey,
ct_operation_yield,dd_operation_yield_enabled,ct_pre_split_quantity,dd_operation_completed,dd_shutdown_type,ct_x_pos,ct_y_pos,dim_ora_previous_operation_seqid,
dd_skip_flag,dd_long_description,dim_ora_date_disableid,ct_cumulative_scrap_quantity,dd_recommended,ct_progress_percentage,dd_wsm_op_seq_num,ct_wsm_bonus_quantity,
dim_ora_employeeid,dim_ora_date_actual_startid,dim_ora_date_actual_completionid,dim_ora_date_projected_completionid,dd_wsm_update_quantity_txn_id,
ct_wsm_costed_quantity_completed,ct_lowest_acceptable_yield,dd_check_skill,dd_wip_entity_name,dd_entity_type,dd_entity_description,
amt_exchangerate,amt_exchangerate_gbl,dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_ora_wip_operations' ),0) + ROW_NUMBER() OVER() fact_ora_wip_operationsid,
S.WIP_ENTITY_ID AS wip_entity_id,
S.OPERATION_SEQ_NUM AS operation_seq_num,
ifnull(S.REPETITIVE_SCHEDULE_ID,0) AS repetitive_schedule_id,
1 AS dim_ora_organizationid,
1 AS dim_ora_repetitive_scheduleid,
1 AS dim_ora_date_last_updateid,
1 AS dim_ora_last_updated_byid,
1 AS dim_ora_date_creationid,
1 AS dim_ora_created_byid,
1 AS dim_ora_operation_sequenceid,
1 AS dim_ora_standard_operationid,
1 AS dim_ora_departmentid,
ifnull(S.DESCRIPTION,'Not Set') AS dd_description,
S.SCHEDULED_QUANTITY AS ct_scheduled_quantity,
S.QUANTITY_IN_QUEUE AS ct_quantity_in_queue,
S.QUANTITY_RUNNING AS ct_quantity_running,
S.QUANTITY_WAITING_TO_MOVE AS ct_quantity_waiting_to_move,
S.QUANTITY_REJECTED AS ct_quantity_rejected,
S.QUANTITY_SCRAPPED AS ct_quantity_scrapped,
S.QUANTITY_COMPLETED AS ct_quantity_completed,
1 AS dim_ora_date_first_unit_startid,
1 AS dim_ora_date_first_unit_completionid,
1 AS dim_ora_date_last_unit_startid,
1 AS dim_ora_date_last_unit_completionid,
ifnull(S.PREVIOUS_OPERATION_SEQ_NUM,0) AS dd_previous_operation_seq_num,
ifnull(S.NEXT_OPERATION_SEQ_NUM,0) AS dd_next_operation_seq_num,
S.COUNT_POINT_TYPE AS dd_count_point_type,
S.BACKFLUSH_FLAG AS dd_backflush_flag,
S.MINIMUM_TRANSFER_QUANTITY AS ct_minimum_transfer_quantity,
1 AS dim_ora_date_last_movedid,
ifnull(S.ATTRIBUTE_CATEGORY,'Not Set') AS dd_attribute_category,
ifnull(S.ATTRIBUTE1,'Not Set') AS dd_attribute1,
ifnull(S.ATTRIBUTE2,'Not Set') AS dd_attribute2,
ifnull(S.ATTRIBUTE3,'Not Set') AS dd_attribute3,
ifnull(S.ATTRIBUTE4,'Not Set') AS dd_attribute4,
ifnull(S.ATTRIBUTE5,'Not Set') AS dd_attribute5,
ifnull(S.ATTRIBUTE6,'Not Set') AS dd_attribute6,
ifnull(S.ATTRIBUTE7,'Not Set') AS dd_attribute7,
ifnull(S.ATTRIBUTE8,'Not Set') AS dd_attribute8,
ifnull(S.ATTRIBUTE9,'Not Set') AS dd_attribute9,
ifnull(S.ATTRIBUTE10,'Not Set') AS dd_attribute10,
ifnull(S.ATTRIBUTE11,'Not Set') AS dd_attribute11,
ifnull(S.ATTRIBUTE12,'Not Set') AS dd_attribute12,
ifnull(S.ATTRIBUTE13,'Not Set') AS dd_attribute13,
ifnull(S.ATTRIBUTE14,'Not Set') AS dd_attribute14,
ifnull(S.ATTRIBUTE15,'Not Set') AS dd_attribute15,
ifnull(S.WF_ITEMTYPE,'Not Set') AS dd_wf_itemtype,
ifnull(S.WF_ITEMKEY,'Not Set') AS dd_wf_itemkey,
ifnull(S.OPERATION_YIELD,0) AS ct_operation_yield,
ifnull(S.OPERATION_YIELD_ENABLED,0) AS dd_operation_yield_enabled,
ifnull(S.PRE_SPLIT_QUANTITY,0) AS ct_pre_split_quantity,
ifnull(S.OPERATION_COMPLETED,'Not Set') AS dd_operation_completed,
ifnull(S.SHUTDOWN_TYPE,'Not Set') AS dd_shutdown_type,
ifnull(S.X_POS,0) AS ct_x_pos,
ifnull(S.Y_POS,0) AS ct_y_pos,
1 AS dim_ora_previous_operation_seqid,
ifnull(S.SKIP_FLAG,0) AS dd_skip_flag,
ifnull(S.LONG_DESCRIPTION,0) AS dd_long_description,
1 AS dim_ora_date_disableid,
ifnull(S.CUMULATIVE_SCRAP_QUANTITY,0) AS ct_cumulative_scrap_quantity,
ifnull(S.RECOMMENDED,'Not Set') AS dd_recommended,
ifnull(S.PROGRESS_PERCENTAGE,0) AS ct_progress_percentage,
ifnull(S.WSM_OP_SEQ_NUM,0) AS dd_wsm_op_seq_num,
ifnull(S.WSM_BONUS_QUANTITY,0) AS ct_wsm_bonus_quantity,
1 AS dim_ora_employeeid,
1 AS dim_ora_date_actual_startid,
1 AS dim_ora_date_actual_completionid,
1 AS dim_ora_date_projected_completionid,
ifnull(S.WSM_UPDATE_QUANTITY_TXN_ID,0) AS dd_wsm_update_quantity_txn_id,
ifnull(S.WSM_COSTED_QUANTITY_COMPLETED,0) AS ct_wsm_costed_quantity_completed,
ifnull(S.LOWEST_ACCEPTABLE_YIELD,0) AS ct_lowest_acceptable_yield,
ifnull(S.CHECK_SKILL,0) AS dd_check_skill,
S.WIP_ENTITY_NAME AS dd_wip_entity_name,
S.ENTITY_TYPE AS dd_entity_type,
ifnull(S.ENTITY_DESCRIPTION,'Not Set') AS dd_entity_description,
1 AS AMT_EXCHANGERATE,
1 AS AMT_EXCHANGERATE_GBL,
current_timestamp  AS DW_INSERT_DATE,
current_timestamp  AS DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' AS SOURCE_ID
FROM ora_wip_operations S LEFT JOIN fact_ora_wip_operations T
ON T.WIP_ENTITY_ID = S.WIP_ENTITY_ID and T.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and ifnull(T.REPETITIVE_SCHEDULE_ID,0) = ifnull(S.REPETITIVE_SCHEDULE_ID,0)
WHERE T.WIP_ENTITY_ID is null;

/*UPDATE dim_ora_organizationid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_WIP_PARAMETERS D ON S.ORGANIZATION_ID = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_organizationid = D.DIM_ORA_WIP_PARAMETERSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_organizationid <> D.DIM_ORA_WIP_PARAMETERSID;

/*UPDATE dim_ora_repetitive_scheduleid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_WIP_REPETITIVE_SCHEDULES D ON S.REPETITIVE_SCHEDULE_ID = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_repetitive_scheduleid = D.DIM_ORA_WIP_REPETITIVE_SCHEDULESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_repetitive_scheduleid <> D.DIM_ORA_WIP_REPETITIVE_SCHEDULESID;

/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.LAST_UPDATE_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_last_updateid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_last_updateid <> D.DIM_DATEID;

/*UPDATE dim_ora_last_updated_byid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_FNDUSER D ON S.LAST_UPDATED_BY = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_last_updated_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_last_updated_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.CREATION_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_creationid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_creationid <> D.DIM_DATEID;

/*UPDATE dim_ora_created_byid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_FNDUSER D ON S.CREATED_BY = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_created_byid = D.DIM_ORA_FNDUSERID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_created_byid <> D.DIM_ORA_FNDUSERID;

/*UPDATE dim_ora_operation_sequenceid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_BOM_OPERATION_SEQUENCES D ON S.OPERATION_SEQUENCE_ID = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_operation_sequenceid = D.DIM_ORA_BOM_OPERATION_SEQUENCESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_operation_sequenceid <> D.DIM_ORA_BOM_OPERATION_SEQUENCESID;

/*UPDATE dim_ora_standard_operationid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_BOM_STANDARD_OPERATIONS D ON S.STANDARD_OPERATION_ID = D.KEY_ID and d.rowiscurrent = 1  
SET 
F.dim_ora_standard_operationid = D.DIM_ORA_BOM_STANDARD_OPERATIONSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_standard_operationid <> D.DIM_ORA_BOM_STANDARD_OPERATIONSID;

/*UPDATE dim_ora_departmentid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_BOM_DEPARTMENTS D ON S.DEPARTMENT_ID = D.KEY_ID AND d.rowiscurrent = 1  
SET 
F.dim_ora_departmentid = D.DIM_ORA_BOM_DEPARTMENTSID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_departmentid <> D.DIM_ORA_BOM_DEPARTMENTSID;

/*UPDATE dim_ora_date_first_unit_startid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.FIRST_UNIT_START_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_first_unit_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_first_unit_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_first_unit_completionid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.FIRST_UNIT_COMPLETION_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_first_unit_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_first_unit_completionid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_last_unit_startid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.LAST_UNIT_START_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_last_unit_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_last_unit_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_last_unit_completionid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.LAST_UNIT_COMPLETION_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_last_unit_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_last_unit_completionid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_last_movedid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.DATE_LAST_MOVED) = D.DATEVALUE
SET 
F.dim_ora_date_last_movedid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_last_movedid <> D.DIM_DATEID;

/*UPDATE dim_ora_previous_operation_seqid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_BOM_OPERATION_SEQUENCES D ON S.OPERATION_SEQUENCE_ID = D.KEY_ID AND d.rowiscurrent = 1 
SET 
F.dim_ora_previous_operation_seqid = D.DIM_ORA_BOM_OPERATION_SEQUENCESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_previous_operation_seqid <> D.DIM_ORA_BOM_OPERATION_SEQUENCESID;

/*UPDATE dim_ora_date_disableid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.DISABLE_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_disableid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_disableid <> D.DIM_DATEID;

/*UPDATE dim_ora_employeeid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_ORA_HR_EMPLOYEES D 
ON S.EMPLOYEE_ID = D.PERSON_ID and S.CREATION_DATE BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE and d.rowiscurrent = 1 
SET 
F.dim_ora_employeeid = D.DIM_ORA_HR_EMPLOYEESID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_employeeid <> D.DIM_ORA_HR_EMPLOYEESID;

/*UPDATE dim_ora_date_actual_startid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.ACTUAL_START_DATE) = D.DATEVALUE
SET 
F.dim_ora_date_actual_startid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_actual_startid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_actual_completionid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.ACTUAL_COMPLETION_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_actual_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_actual_completionid <> D.DIM_DATEID;

/*UPDATE dim_ora_date_projected_completionid*/
UPDATE fact_ora_wip_operations F
FROM ora_wip_operations S JOIN DIM_DATE D ON ansidate(S.PROJECTED_COMPLETION_DATE) = D.DATEVALUE 
SET 
F.dim_ora_date_projected_completionid = D.DIM_DATEID,
DW_UPDATE_DATE = current_timestamp
WHERE F.WIP_ENTITY_ID = S.WIP_ENTITY_ID and F.OPERATION_SEQ_NUM = S.OPERATION_SEQ_NUM and F.REPETITIVE_SCHEDULE_ID = S.REPETITIVE_SCHEDULE_ID
and F.dim_ora_date_projected_completionid <> D.DIM_DATEID;