/*set session authorization oracle_data*/


/*DELETE FROM hlp_ora_gl_daily_rates*/
/*SELECT * FROM hlp_ora_gl_daily_rates*/
delete from NUMBER_FOUNTAIN where table_name = 'hlp_ora_gl_daily_rates';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'hlp_ora_gl_daily_rates',IFNULL(MAX(hlp_ora_gl_daily_ratesID),0)
FROM hlp_ora_gl_daily_rates;

/*update rows*/
UPDATE hlp_ora_gl_daily_rates F
FROM ora_gl_daily_rates S
SET 
F.RATE_SOURCE_CODE = S.RATE_SOURCE_CODE,
F.STATUS_CODE = S.STATUS_CODE,
F.CREATION_DATE= S.CREATION_DATE,
F.CREATED_BY= S.CREATED_BY,
F.LAST_UPDATE_DATE= S.LAST_UPDATE_DATE,
F.LAST_UPDATED_BY= S.LAST_UPDATED_BY,
F.CONTEXT= ifnull(S.CONTEXT,0),
F.rowiscurrent = 1,
F.DW_UPDATE_DATE = current_timestamp
WHERE S.FROM_CURRENCY = F.FROM_CURRENCY 
AND  S.TO_CURRENCY = F.TO_CURRENCY
AND  S.CONVERSION_DATE = F.CONVERSION_DATE
AND  S.CONVERSION_TYPE = F.CONVERSION_TYPE;

/*insert new rows*/
INSERT INTO hlp_ora_gl_daily_rates
(
hlp_ora_gl_daily_ratesid,
rate_source_code,from_currency,to_currency,
conversion_date,conversion_type,conversion_rate,status_code,creation_date,created_by,last_update_date,last_updated_by,context,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'hlp_ora_gl_daily_rates' ),0) + ROW_NUMBER() OVER() hlp_ora_gl_daily_ratesID,
S.RATE_SOURCE_CODE,
S.FROM_CURRENCY,
S.TO_CURRENCY,
S.CONVERSION_DATE,
S.CONVERSION_TYPE,
S.CONVERSION_RATE,
S.STATUS_CODE,
S.CREATION_DATE,
S.CREATED_BY,
S.LAST_UPDATE_DATE,
S.LAST_UPDATED_BY,
ifnull(S.CONTEXT,0) CONTEXT,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'ORA 12.1' SOURCE_ID
FROM ora_gl_daily_rates S
LEFT JOIN hlp_ora_gl_daily_rates F
ON   S.FROM_CURRENCY = F.FROM_CURRENCY 
AND  S.TO_CURRENCY = F.TO_CURRENCY
AND  S.CONVERSION_DATE = F.CONVERSION_DATE
AND  S.CONVERSION_TYPE = F.CONVERSION_TYPE
WHERE F.FROM_CURRENCY is null and F.TO_CURRENCY is null and F.CONVERSION_DATE is null and F.CONVERSION_TYPE is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'hlp_ora_gl_daily_rates');