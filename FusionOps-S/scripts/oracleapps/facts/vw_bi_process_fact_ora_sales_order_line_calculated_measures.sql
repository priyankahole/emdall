/*CT_CONFIRMED_QUANTITY*/
/*reset confirmed quantity values*/
UPDATE fact_ora_sales_order_line
SET ct_confirmed_quantity = 0;


/*update confirmed quantity  - shipped quantity for the CLOSED orders*/
DROP IF EXISTS fact_ora_sales_order_line_tmp_3;

CREATE TABLE fact_ora_sales_order_line_tmp_3 as 
select 
sol.fact_ora_sales_order_lineid,
sol.ct_total_shipped_quantity 
from fact_ora_sales_order_line sol
join dim_ora_xacttype cttype
on cttype.dim_ora_xacttypeid = sol.dim_ora_order_line_typeid and sol.rowiscurrent = 1 and cttype.rowiscurrent = 1
join dim_ora_fnd_lookup lfsc
on lfsc.dim_ora_fnd_lookupid = sol.dim_ora_order_line_statusid and lfsc.rowiscurrent = 1
where sol.ct_ordered_quantity<>0
and not(cttype.NAME like '%Return%')
and lfsc.lookup_code like '%CLOSED%'
order by sol.fact_ora_sales_order_lineid;

UPDATE fact_ora_sales_order_line sol
FROM fact_ora_sales_order_line_tmp_3 tmp 
SET sol.ct_confirmed_quantity = tmp.ct_total_shipped_quantity
WHERE sol.fact_ora_sales_order_lineid = tmp.fact_ora_sales_order_lineid;

/*calculate onhand quantity per segment1(item number)*/
DROP IF EXISTS fact_ORA_INVONHAND_tmp;

CREATE TABLE fact_ORA_INVONHAND_tmp as
select prd.segment1, sum(ct_transaction_quantity) ct_transaction_quantity   
from   	fact_ORA_INVONHAND oh, dim_ora_invproduct prd      
where oh.dim_ora_inv_productid =prd.dim_ora_invproductid and oh.rowiscurrent = 1 and prd.rowiscurrent = 1
group by prd.segment1;

/*generate temp table with orders and quantities to be taken into account for confirmed quantity calculation*/
DROP IF EXISTS fact_ora_sales_order_line_tmp_1;

CREATE TABLE fact_ora_sales_order_line_tmp_1 as 
select 
sol.fact_ora_sales_order_lineid,
crd.datevalue customer_requested_date, 
itm.segment1,
sol.ct_ordered_quantity, 
ioht.ct_transaction_quantity onhand,
row_number() OVER(ORDER BY itm.segment1,crd.datevalue,sol.fact_ora_sales_order_lineid) AS row_number
from fact_ora_sales_order_line sol
join dim_ora_invproduct itm
on sol.dim_ora_inv_productid = itm.dim_ora_invproductid and sol.rowiscurrent = 1 and itm.rowiscurrent = 1
join dim_date crd 
on crd.dim_dateid= sol.dim_ora_date_customer_requestedid 
join fact_ORA_INVONHAND_tmp ioht
on itm.segment1 = ioht.segment1
join dim_ora_xacttype cttype
on cttype.dim_ora_xacttypeid = sol.dim_ora_order_line_typeid and cttype.rowiscurrent = 1
join dim_ora_fnd_lookup lfsc
on lfsc.dim_ora_fnd_lookupid = sol.dim_ora_order_line_statusid and lfsc.rowiscurrent = 1
where sol.ct_ordered_quantity<>0
and not(cttype.NAME like '%Return%')
and not (lfsc.lookup_code like '%CANCELLED%' or lfsc.lookup_code like '%CLOSED%' or lfsc.lookup_code like '%REJECTED%')
order by itm.segment1, crd.datevalue;

/*calculate confirmed quantity and cumulative ordered quantity*/
DROP IF EXISTS fact_ora_sales_order_line_tmp_2;

CREATE TABLE fact_ora_sales_order_line_tmp_2 as 
select 
t1.fact_ora_sales_order_lineid,
t1.customer_requested_date,
t1.segment1,
t1.ct_ordered_quantity,
t1.onhand,
t1.row_number,
sum(t2.ct_ordered_quantity) ct_ordered_quantity_cum,
case when t1.onhand - sum(t2.ct_ordered_quantity) >= 0 then t1.ct_ordered_quantity else 0 end ct_confirmed_quantity
from fact_ora_sales_order_line_tmp_1 t1
join  fact_ora_sales_order_line_tmp_1 t2
on t1.segment1 = t2.segment1 and t2.row_number <=t1.row_number
group by 
t1.fact_ora_sales_order_lineid,
t1.customer_requested_date,
t1.segment1,
t1.ct_ordered_quantity,
t1.onhand,
t1.row_number;

/*update calculated confirmed quantity*/
UPDATE fact_ora_sales_order_line f
FROM fact_ora_sales_order_line_tmp_2 tmp
set f.ct_confirmed_quantity = tmp.ct_confirmed_quantity
WHERE f.fact_ora_sales_order_lineid = tmp.fact_ora_sales_order_lineid;

/*drop temp tables*/
DROP TABLE fact_ORA_INVONHAND_tmp;
DROP TABLE fact_ora_sales_order_line_tmp_1;
DROP TABLE fact_ora_sales_order_line_tmp_2;
DROP TABLE fact_ora_sales_order_line_tmp_3;

/*CT_FILL_QUANTITY before promise date*/
/*reset ct_fill_quantity values*/
UPDATE fact_ora_sales_order_line
SET ct_fill_quantity = 0;

/*temp table - values to be taken into account*/
DROP IF EXISTS fact_ora_sales_delivery_fill_qty_before_promisedate_tmp;

CREATE TABLE fact_ora_sales_delivery_fill_qty_before_promisedate_tmp AS
select sd.fact_ora_sales_order_lineid, sum(sd.ct_actual_shipped_quantity) ct_actual_shipped_quantity
from fact_ora_sales_delivery sd
join fact_ora_sales_order_line sol
on sd.fact_ora_sales_order_lineid = sol.fact_ora_sales_order_lineid and sd.rowiscurrent = 1 and sol.rowiscurrent = 1
join dim_date dd
on dd.dim_dateid = sol.DIM_ORA_DATE_ACTUAL_SHIPID
join dim_date pd
on pd.dim_dateid = sol.dim_ora_date_promise_deliveryid
where pd.datevalue<= dd.datevalue
group by sd.fact_ora_sales_order_lineid;

/*update fact table*/
UPDATE fact_ora_sales_order_line sol
FROM fact_ora_sales_delivery_fill_qty_before_promisedate_tmp tmp
SET sol.ct_fill_quantity = tmp.ct_actual_shipped_quantity
WHERE sol.fact_ora_sales_order_lineid = tmp.fact_ora_sales_order_lineid;

DROP TABLE fact_ora_sales_delivery_fill_qty_before_promisedate_tmp;

/*CT_FILL_QUANTITY before customer request date*/
/*reset ct_fill_quantity_crd values*/
UPDATE fact_ora_sales_order_line
SET ct_fill_quantity_crd = 0;

/*temp table - values to be taken into account*/
DROP IF EXISTS fact_ora_sales_delivery_fill_qty_before_custrequestdate_tmp;

CREATE TABLE fact_ora_sales_delivery_fill_qty_before_custrequestdate_tmp AS
select sd.fact_ora_sales_order_lineid, sum(sd.ct_actual_shipped_quantity) ct_actual_shipped_quantity
from fact_ora_sales_delivery sd
join fact_ora_sales_order_line sol
on sd.fact_ora_sales_order_lineid = sol.fact_ora_sales_order_lineid and sd.rowiscurrent = 1 and sol.rowiscurrent = 1
join dim_date dd
on dd.dim_dateid = sol.DIM_ORA_DATE_ACTUAL_SHIPID
join dim_date crd
on crd.dim_dateid = sol.dim_ora_date_customer_requestedid
where crd.datevalue<= dd.datevalue
group by sd.fact_ora_sales_order_lineid;

/*update fact table*/
UPDATE fact_ora_sales_order_line sol
FROM fact_ora_sales_delivery_fill_qty_before_custrequestdate_tmp tmp
SET sol.ct_fill_quantity_crd = tmp.ct_actual_shipped_quantity
WHERE sol.fact_ora_sales_order_lineid = tmp.fact_ora_sales_order_lineid;

DROP TABLE fact_ora_sales_delivery_fill_qty_before_custrequestdate_tmp;


/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'fact_ora_sales_order_line');