set session authorization ge7df;

/*initialize NUMBER_FOUNTAIN*/
DELETE FROM number_fountain WHERE table_name = 'fact_ora_inv_turns';	

INSERT INTO number_fountain
SELECT 'fact_ora_inv_turns',IFNULL(MAX(fact_ora_inv_turnsid),0)
FROM fact_ora_inv_turns;

INSERT INTO fact_ora_inv_turns
(
fact_ora_inv_turnsid,
dim_ora_business_orgid,
dim_ora_org_acct_periodsid,
amt_onhand,
amt_wip,
amt_intransit,
amt_cogs,
dim_ora_date_last_updateid,
dim_last_updated_byid,
dim_ora_date_creationid,
dim_created_byid,
dd_last_update_login,
dim_ora_invproductid,
amt_bop_onhand,
amt_bop_wip,
amt_bop_intransit,
dd_inventory_item_id_acct_period_id,
amt_exchangerate,
amt_exchangerate_gbl,
dim_projectsourceid,
dw_insert_date,
dw_update_date,
rowstartdate,
rowenddate,
rowiscurrent,
rowchangereason,
source_id
)
SELECT 
		IFNULL((SELECT MAX_ID FROM number_fountain WHERE table_name = 'fact_ora_inv_turns' ),0) + ROW_NUMBER() OVER() fact_ora_inv_turnsid,
		1 AS dim_ora_business_orgid,
		1 AS dim_ora_org_acct_periodsid,
		IFNULL(S.onhand,0) AS amt_onhand,
		IFNULL(S.wip,0) AS amt_wip,
		IFNULL(S.intransit,0) AS amt_intransit,
		IFNULL(S.cogs,0) AS amt_cogs,
		1 AS dim_ora_date_last_updateid,
		1 AS dim_last_updated_byid,
		1 AS dim_ora_date_creationid,
		1 AS dim_created_byid,
		IFNULL(S.last_update_login,1) AS dd_last_update_login,
		1 AS dim_ora_invproductid,
		IFNULL(S.bop_onhand,0) AS amt_bop_onhand,
		IFNULL(S.bop_wip,0) AS amt_bop_wip,
		IFNULL(S.bop_intransit,0) AS amt_bop_intransit,
		CONCAT(S.inventory_item_id,'-',S.acct_period_id) AS dd_inventory_item_id_acct_period_id,
		
		1 AS amt_exchangerate,
		1 AS amt_exchangerate_gbl,
		1 AS dim_projectsourceid,
		CURRENT_TIMESTAMP  AS dw_insert_date,
		CURRENT_TIMESTAMP  AS dw_update_date,
		CURRENT_TIMESTAMP AS rowstartdate,
		timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
		1 AS rowiscurrent,
        'Insert' AS rowchangereason,
        'ORA 12.1' AS source_id
FROM ora_mtl_bis_inv_by_period s
WHERE CONCAT(S.inventory_item_id,'-',S.acct_period_id) NOT IN (SELECT dd_inventory_item_id_acct_period_id FROM fact_ora_inv_turns F);

 
/*UPDATE dim_ora_business_orgid*/
UPDATE 
      fact_ora_inv_turns F
FROM ora_mtl_bis_inv_by_period S JOIN dim_ora_business_org D ON s.organization_id = d.organization_id
SET 
	F.dim_ora_business_orgid = d.dim_ora_business_orgid
   ,dw_update_date = CURRENT_TIMESTAMP
WHERE F.dd_inventory_item_id_acct_period_id = CONCAT(S.inventory_item_id,'-',S.acct_period_id)
  AND d.org_type = 'INV'
  AND F.dim_ora_business_orgid <> D.dim_ora_business_orgid;

  
/*UPDATE dim_ora_org_acct_periodsid*/
UPDATE fact_ora_inv_turns F
FROM ora_mtl_bis_inv_by_period S JOIN dim_ora_org_acct_periods D ON CONCAT(VARCHAR(S.acct_period_id,200),'~',VARCHAR(S.organization_id,200)) = D.key_id  
SET 
	F.dim_ora_org_acct_periodsid = D.dim_ora_org_acct_periodsid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE F.dd_inventory_item_id_acct_period_id = CONCAT(S.inventory_item_id,'-',S.acct_period_id)
F.dim_ora_org_acct_periodsid <> D.dim_ora_org_acct_periodsid


/*UPDATE dim_ora_date_last_updateid*/
UPDATE fact_ora_inv_turns F
FROM ora_mtl_bis_inv_by_period S JOIN dim_date D ON ANSIDATE(S.last_update_date) = D.datevalue
SET 
	F.dim_ora_date_last_updateid = D.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE F.dd_inventory_item_id_acct_period_id = CONCAT(S.inventory_item_id,'-',S.acct_period_id)
F.dim_ora_date_last_updateid <> D.dim_dateid;

/*UPDATE dim_last_updated_byid*/
UPDATE fact_ora_inv_turns F
FROM ora_mtl_bis_inv_by_period S JOIN dim_ora_FndUser D ON S.last_updated_by = D.key_id   
SET 
	F.dim_last_updated_byid = D.dim_ora_fnduserid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE F.dd_inventory_item_id_acct_period_id = CONCAT(S.inventory_item_id,'-',S.acct_period_id)
F.dim_last_updated_byid <> D.dim_ora_fnduserid;


/*UPDATE dim_ora_date_creationid*/
UPDATE fact_ora_inv_turns F
FROM ora_mtl_bis_inv_by_period S JOIN dim_date D ON ANSIDATE(creation_date) = D.datevalue 
SET 
	F.dim_ora_date_creationid = D.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE F.dd_inventory_item_id_acct_period_id = CONCAT(S.inventory_item_id,'-',S.acct_period_id)
F.dim_ora_date_creationid <> D.dim_dateid;


/*UPDATE dim_created_byid*/
UPDATE fact_ora_inv_turns F
FROM ora_mtl_bis_inv_by_period S JOIN dim_ora_FndUser D ON s.created_by = D.key_id   
SET 
	F.dim_created_byid = D.dim_ora_fnduserid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE F.dd_inventory_item_id_acct_period_id = CONCAT(S.inventory_item_id,'-',S.acct_period_id)
F.dim_created_byid <> D.dim_ora_fnduserid

/*UPDATE dim_ora_invproductid*/
UPDATE fact_ora_inv_turns F
FROM ora_mtl_bis_inv_by_period S JOIN dim_ora_invProduct D ON CONCAT(VARCHAR(S.organization_id,200),'~',VARCHAR(S.inventory_item_id,200)) = D.key_id   
SET 
	F.dim_ora_invproductid = D.dim_ora_invproductid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE F.dd_inventory_item_id_acct_period_id = CONCAT(S.inventory_item_id,'-',S.acct_period_id)
F.dim_ora_invproductid <> D.dim_ora_invproductid;



