/*set session authorization oracle_data*/

/*DIM_ORA_AR_PAYMENT_TERMS*/
/*SELECT distinct rowiscurrent from DIM_ORA_AR_PAYMENT_TERMS*/
UPDATE dim_ora_ar_payment_terms T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS TERM_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_ar_payment_terms' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_ar_payment_terms')>0
AND dim_ora_ar_payment_termsid <>1;

/*DIM_ORA_BUSINESS_ORG*/
/*SELECT distinct rowiscurrent from DIM_ORA_BUSINESS_ORG*/
UPDATE dim_ora_business_org T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS ORG_TYPE,INTEGER1 AS ORGANIZATION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_business_org' 
AND T.KEY_ID = VARCHAR(VARCHAR1, 200) +'~' +VARCHAR(INTEGER1, 200) 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_business_org')>0
AND dim_ora_business_orgid <>1;

/*DIM_ORA_HR_EMPLOYEES*/
/*SELECT distinct rowiscurrent from DIM_ORA_HR_EMPLOYEES*/
UPDATE dim_ora_hr_employees T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS PERSON_ID, DATE1 AS EFFECTIVE_START_DATE, DATE2 AS EFFECTIVE_END_DATE
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_hr_employees' 
AND T.KEY_ID = VARCHAR(INTEGER1, 200) +'~' + VARCHAR(DATE1, 200) +'~' + VARCHAR(DATE2, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_hr_employees')>0
AND dim_ora_hr_employeesid <>1;

/*DIM_ORA_HR_LOCATIONS*/
/*SELECT distinct rowiscurrent from DIM_ORA_HR_LOCATIONS*/
UPDATE dim_ora_hr_locations T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS LOCATION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_hr_locations' 
AND T.KEY_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_hr_locations')>0
AND dim_ora_hr_locationsid <>1;

/*DIM_ORA_HZ_CUSTACCOUNTS*/
/*SELECT distinct rowiscurrent from DIM_ORA_HZ_CUSTACCOUNTS*/
UPDATE dim_ora_hz_custaccounts T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS CUST_ACCOUNT_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_hz_custaccounts' 
AND T.KEY_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_hz_custaccounts')>0
AND dim_ora_hz_custaccountsid <>1;

/*DIM_ORA_CUSTOMERLOCATION*/
/*SELECT distinct rowiscurrent from DIM_ORA_CUSTOMERLOCATION*/
UPDATE dim_ora_CustomerLocation T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS SITE_USE_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_hz_custlocation' 
AND T.KEY_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_hz_custlocation')>0
AND dim_ora_CustomerLocationid <>1;

/*DIM_ORA_HZ_PARTY*/
/*SELECT distinct rowiscurrent from DIM_ORA_HZ_PARTY*/
UPDATE dim_ora_hz_party T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS PARTY_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_hz_parties' 
AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_hz_parties')>0
AND dim_ora_hz_partyid <>1;

/*DIM_ORA_INVPRODUCT*/
/*SELECT distinct rowiscurrent from DIM_ORA_INVPRODUCT*/
UPDATE dim_ora_invProduct T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS ORGANIZATION_ID, INTEGER2 AS INVENTORY_ITEM_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_inventory_prod' 
AND T.KEY_ID = VARCHAR(INTEGER1, 200) +'~' + VARCHAR(INTEGER2, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_inventory_prod')>0
AND dim_ora_invProductid <>1;

/*DIM_ORA_PRODUCT*/
/*SELECT distinct rowiscurrent from DIM_ORA_PRODUCT*/
UPDATE dim_ora_Product T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS ORGANIZATION_ID, INTEGER2 AS INVENTORY_ITEM_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_inventory_prod' 
AND T.KEY_ID = INTEGER2
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_inventory_prod')>0
AND dim_ora_Productid <>1;

/*DIM_ORA_MTL_ITEM_LOCATIONS*/
/*SELECT distinct rowiscurrent from DIM_ORA_MTL_ITEM_LOCATIONS*/
UPDATE dim_ora_mtl_item_locations T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS INVENTORY_LOCATION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_item_locations' 
AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_item_locations')>0
AND dim_ora_mtl_item_locationsid <>1;

/*DIM_ORA_MTL_ITEM_REV*/
/*SELECT distinct rowiscurrent from DIM_ORA_MTL_ITEM_REV*/
UPDATE dim_ora_mtl_item_rev T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS INVENTORY_ITEM_ID,INTEGER2 AS ORGANIZATION_ID,VARCHAR1 AS REVISION 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_item_rev' 
AND T.KEY_ID = VARCHAR(INTEGER1, 200) +'~' + VARCHAR(INTEGER2, 200) +'~' + VARCHAR(VARCHAR1, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_item_rev')>0
AND dim_ora_mtl_item_revid <>1;

/*DIM_ORA_MTL_SEC_INVENTORY*/
/*SELECT distinct rowiscurrent from DIM_ORA_MTL_SEC_INVENTORY*/
UPDATE dim_ora_mtl_sec_inventory T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS SECONDARY_INVENTORY_NAME,INTEGER1 AS ORGANIZATION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_sec_inventory' 
AND T.KEY_ID = VARCHAR(VARCHAR1, 200) + '~' + VARCHAR(INTEGER1, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_sec_inventory')>0
AND dim_ora_mtl_sec_inventoryid <>1;

/*DIM_ORA_MTL_TRX_TYPS*/
/*SELECT distinct rowiscurrent from DIM_ORA_MTL_TRX_TYPS*/
UPDATE dim_ora_mtl_trx_typs T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS TRANSACTION_TYPE_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_trx_typs' 
AND T.KEY_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_trx_typs')>0
AND dim_ora_mtl_trx_typsid <>1;

/*DIM_ORA_XACTTYPE*/
/*SELECT distinct rowiscurrent from DIM_ORA_XACTTYPE*/
UPDATE dim_ora_XACTType T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS TRANSACTION_TYPE_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_oe_transaction_types' 
AND T.KEY_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_oe_transaction_types')>0
AND dim_ora_XACTTypeid <>1;

/*DIM_ORA_FND_LOOKUP*/
/*SELECT distinct rowiscurrent from DIM_ORA_FND_LOOKUP*/
UPDATE dim_ora_fnd_lookup T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS LOOKUP_CODE, VARCHAR2 AS LOOKUP_TYPE, INTEGER1 AS SECURITY_GROUP_ID, INTEGER2 AS VIEW_APPLICATION_ID
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_fnd_lookup' 
AND T.KEY_ID = VARCHAR(VARCHAR1, 200) +'~'  + VARCHAR(VARCHAR2, 200) +'~' + VARCHAR(INTEGER1, 200) +'~' + VARCHAR(INTEGER2, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_fnd_lookup')>0
AND dim_ora_fnd_lookupid <>1;

/*DIM_ORA_GL_SETOFBOOKS*/
/*SELECT distinct rowiscurrent from DIM_ORA_GL_SETOFBOOKS*/
UPDATE dim_ora_GL_SetofBooks T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS SET_OF_BOOKS_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_gl_setofbooks' 
AND T.KEY_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_gl_setofbooks')>0
AND dim_ora_GL_SetofBooksid <>1;

/*DIM_ORA_HROPERATINGUNIT*/
/*SELECT distinct rowiscurrent from DIM_ORA_HROPERATINGUNIT*/
UPDATE dim_ora_hroperatingUnit T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS ORGANIZATION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_hr_oper_unit' 
AND T.KEY_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_hr_oper_unit')>0
AND dim_ora_hroperatingUnitid <>1;

/*DIM_ORA_SALESREP*/
/*SELECT distinct rowiscurrent from DIM_ORA_SALESREP*/
UPDATE dim_ora_SalesRep T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS SALESREP_ID, INTEGER2 AS ORG_ID
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_jtf_salesrep' 
AND T.KEY_ID = VARCHAR(INTEGER1, 200) +'~' + VARCHAR(INTEGER2, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_jtf_salesrep')>0
AND dim_ora_SalesRepid <>1;

/*DIM_ORA_FNDUSER*/
/*SELECT distinct rowiscurrent from DIM_ORA_FNDUSER*/
UPDATE dim_ora_FndUser T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS USER_ID FROM ORA_SOURCE_TABLEIDS 
WHERE TABLE_NAME ='ora_fnd_users' 
AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_fnd_users')>0
AND dim_ora_FndUserid <>1;

/*DIM_ORA_DELIVERYLOCATIONS*/
/*SELECT distinct rowiscurrent from DIM_ORA_DELIVERYLOCATIONS*/
UPDATE dim_ora_deliverylocations T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS WSH_LOCATION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_wsh_locations' 
AND T.KEY_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_wsh_locations')>0
AND dim_ora_deliverylocationsid <>1;

/*DIM_ORA_GL_CODE_COMB*/
/*SELECT distinct rowiscurrent from DIM_ORA_GL_CODE_COMB*/
UPDATE DIM_ORA_GL_CODE_COMB T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS CODE_COMBINATION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_gl_code_comb' 
AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_gl_code_comb')>0
and DIM_ORA_GL_CODE_COMBid <>1;

/*DIM_ORA_MTL_TRX_SOURCE_TYPS*/
/*SELECT distinct rowiscurrent from DIM_ORA_MTL_TRX_SOURCE_TYPS*/
UPDATE DIM_ORA_MTL_TRX_SOURCE_TYPS T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS TRANSACTION_SOURCE_TYPE_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_trx_source' 
AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_trx_source')>0
and DIM_ORA_MTL_TRX_SOURCE_TYPSid <>1;

/*DIM_ORA_MTL_TRX_REASONS*/
/*SELECT distinct rowiscurrent from DIM_ORA_MTL_TRX_REASONS*/
UPDATE DIM_ORA_MTL_TRX_REASONS T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS REASON_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_trx_reasons' 
AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_trx_reasons')>0
and DIM_ORA_MTL_TRX_REASONSid <>1;

/*HLP_ORA_GL_DAILY_RATES*/
/*SELECT distinct rowiscurrent from HLP_ORA_GL_DAILY_RATES*/
UPDATE HLP_ORA_GL_DAILY_RATES T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS FROM_CURRENCY, VARCHAR2 AS TO_CURRENCY,DATE1 AS CONVERSION_DATE,VARCHAR3 AS CONVERSION_TYPE
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_gl_daily_rates' 
AND T.FROM_CURRENCY = VARCHAR1 AND T.TO_CURRENCY = VARCHAR2 AND T.CONVERSION_DATE = DATE1 AND T.CONVERSION_TYPE = VARCHAR3 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_gl_daily_rates')>0;

/*HLP_ORA_OE_PRICE_ADJUSTMENTS*/
/*SELECT distinct rowiscurrent from HLP_ORA_OE_PRICE_ADJUSTMENTS*/
UPDATE HLP_ORA_OE_PRICE_ADJUSTMENTS T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS PRICE_ADJUSTMENT_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_oe_price_adjustments' 
AND T.PRICE_ADJUSTMENT_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_oe_price_adjustments')>0;

/*FACT_ORA_SALES_ORDER_LINE*/
/*SELECT distinct rowiscurrent from FACT_ORA_SALES_ORDER_LINE*/
UPDATE fact_ORA_SALES_ORDER_LINE T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS 
(
SELECT INTEGER1 AS HEADER_ID,INTEGER2 AS LINE_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_oe_order_header_line' 
AND T.DD_HEADER_ID = INTEGER1 AND T.DD_LINE_ID =  INTEGER2
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_oe_order_header_line')>0;

/*FACT_ORA_INVRESERVATION*/
/*SELECT distinct rowiscurrent from FACT_ORA_INVRESERVATION*/
UPDATE  fact_ORA_INVRESERVATION T 
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS 
(
SELECT INTEGER1 AS RESERVATION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_reservations' 
AND T.DD_RESERVATION_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_reservations')>0;

/*FACT_ORA_INVMTLTRX*/
/*SELECT distinct rowiscurrent from FACT_ORA_INVMTLTRX*/
UPDATE fact_ORA_INVMTLTRX T 
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS 
(
SELECT INTEGER1 AS TRANSACTION_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_transactions' 
AND T.TRANSACTION_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_transactions')>0;

/*FACT_ORA_SALES_DELIVERY*/
/*SELECT distinct rowiscurrent from FACT_ORA_SALES_DELIVERY*/
UPDATE  fact_ORA_SALES_DELIVERY T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS DELIVERY_DETAIL_ID, INTEGER2 AS DELIVERY_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_wsh_deliveries' 
AND T.DELIVERY_DETAIL_ID = INTEGER1 AND T.DELIVERY_ID = INTEGER2
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_wsh_deliveries')>0;

/*DIM_ORA_PO_CATEGORIES*/
/*SELECT distinct rowiscurrent from DIM_ORA_PO_CATEGORIES*/
UPDATE  DIM_ORA_PO_CATEGORIES T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS CATEGORY_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_cat' AND T.CATEGORY_ID = INTEGER1
) 
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_cat')>0
and DIM_ORA_PO_CATEGORIESid <>1;

/*DIM_ORA_PO_DOCUMENTTYPES*/
/*SELECT distinct rowiscurrent from DIM_ORA_PO_DOCUMENTTYPES*/
UPDATE  DIM_ORA_PO_DOCUMENTTYPES T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS ORG_ID, VARCHAR1 AS DOCUMENT_TYPE_CODE,VARCHAR2 AS DOCUMENT_SUBTYPE 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_doctype' 
AND T.DOCUMENT_TYPE_CODE = VARCHAR1 AND T.DOCUMENT_SUBTYPE = VARCHAR2 AND T.ORG_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_doctype')>0
and DIM_ORA_PO_DOCUMENTTYPESid <>1;

/*DIM_ORA_PO_LINETYPES*/
/*SELECT distinct rowiscurrent from DIM_ORA_PO_LINETYPES*/
UPDATE  DIM_ORA_PO_LINETYPES T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS LINE_TYPE_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_ltype' 
AND T.LINE_TYPE_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_ltype')>0
and DIM_ORA_PO_LINETYPESid <>1;

/*DIM_ORA_PO_VENDORS*/
/*SELECT distinct rowiscurrent from DIM_ORA_PO_VENDORS*/
UPDATE  DIM_ORA_PO_VENDORS T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS VENDOR_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_vendor' 
AND T.VENDOR_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_vendor')>0
and DIM_ORA_PO_VENDORSid <>1;

/*DIM_ORA_PO_VENDORSITES*/
/*SELECT distinct rowiscurrent from DIM_ORA_PO_VENDORSITES*/
UPDATE  DIM_ORA_PO_VENDORSITES T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS VENDOR_SITE_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_vensite' 
AND T.VENDOR_SITE_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_vensite')>0
and DIM_ORA_PO_VENDORSITESid <>1;

/*DIM_ORA_PO_BUYER*/
/*SELECT distinct rowiscurrent from DIM_ORA_PO_BUYER*/
UPDATE  DIM_ORA_PO_BUYER T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS AGENT_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_agent' 
AND T.AGENT_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_agent')>0
and DIM_ORA_PO_BUYERid <>1;

/*fact_ORA_PURCHASEORDER*/
/*SELECT distinct rowiscurrent from fact_ORA_PURCHASEORDER*/
UPDATE  fact_ORA_PURCHASEORDER T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS PO_HEADER_ID,INTEGER2 AS PO_LINE_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_order' 
AND T.PO_HEADER_ID = INTEGER1 AND T.PO_LINE_ID = INTEGER2
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_order')>0;

/*fact_ora_purchaseordercost*/
/*SELECT distinct rowiscurrent from fact_ora_purchaseordercost*/
UPDATE fact_ora_purchaseordercost T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS PO_DISTRIBUTION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_order_cost' AND T.PO_DISTRIBUTION_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_order_cost')>0;

/*fact_ora_purchasereceipt*/
/*SELECT distinct rowiscurrent from fact_ora_purchasereceipt*/
UPDATE fact_ora_purchasereceipt T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS TRANSACTION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_receipt' AND T.TRANSACTION_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_receipt')>0;

/*fact_ora_purchaserequisition*/
/*SELECT distinct rowiscurrent from fact_ora_purchaserequisition*/
UPDATE fact_ora_purchaserequisition T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS REQUISITION_LINE_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_req_line' AND T.REQUISITION_LINE_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_req_line')>0;

/*fact_ora_purchaserequisitioncost*/
/*SELECT distinct rowiscurrent from fact_ora_purchaserequisitioncost*/
UPDATE fact_ora_purchaserequisitioncost T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS DISTRIBUTION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_purch_req_line_cost' AND T.DISTRIBUTION_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_purch_req_line_cost')>0;

/*hlp_ora_wsh_freight_cost*/
/*SELECT distinct rowiscurrent from hlp_ora_wsh_freight_cost*/
UPDATE hlp_ora_wsh_freight_cost T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS FREIGHT_COST_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_wsh_freight_cost' AND T.FREIGHT_COST_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_wsh_freight_cost')>0;

/*dim_ora_ap_terms*/
/*SELECT distinct rowiscurrent from dim_ora_ap_terms*/
UPDATE dim_ora_ap_terms T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS TERM_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_ap_terms' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_ap_terms')>0
AND dim_ora_ap_termsid <>1;

/*dim_ora_bom_departments*/
/*SELECT distinct rowiscurrent from dim_ora_bom_departments*/
UPDATE dim_ora_bom_departments T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS DEPARTMENT_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_bom_department' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_bom_department')>0
AND dim_ora_bom_departmentsid <>1;

/*dim_ora_bom_resources*/
/*SELECT distinct rowiscurrent from dim_ora_bom_resources*/
UPDATE dim_ora_bom_resources T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS RESOURCE_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_bom_resource' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_bom_resource')>0
AND dim_ora_bom_resourcesid <>1;

/*dim_ora_mrp_designators*/
/*SELECT distinct rowiscurrent from dim_ora_mrp_designators*/
UPDATE dim_ora_mrp_designators T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS COMPILE_DESIGNATOR,INTEGER1 AS ORGANIZATION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_designators' AND T.KEY_ID = VARCHAR(VARCHAR1, 200) +'~' + VARCHAR(INTEGER1, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_designators')>0
AND dim_ora_mrp_designatorsid <>1;

/*dim_ora_mrp_forecast_designators*/
/*SELECT distinct rowiscurrent from dim_ora_mrp_forecast_designators*/
UPDATE dim_ora_mrp_forecast_designators T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS FORECAST_DESIGNATOR,INTEGER1 AS ORGANIZATION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_forecast_designators' AND T.KEY_ID = VARCHAR(VARCHAR1, 200) +'~' + VARCHAR(INTEGER1, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_forecast_designators')>0
AND dim_ora_mrp_forecast_designatorsid <>1;

/*dim_ora_mrp_schedule_designators*/
/*SELECT distinct rowiscurrent from dim_ora_mrp_schedule_designators*/
UPDATE dim_ora_mrp_schedule_designators T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS SCHEDULE_DESIGNATOR,INTEGER1 AS ORGANIZATION_ID  FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_schedule_designators' AND T.KEY_ID = VARCHAR(VARCHAR1, 200) +'~' + VARCHAR(INTEGER1, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_schedule_designators')>0
AND dim_ora_mrp_schedule_designatorsid <>1;

/*dim_ora_mrp_forecast_rules*/
/*SELECT distinct rowiscurrent from dim_ora_mrp_forecast_rules*/
UPDATE dim_ora_mrp_forecast_rules T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS FORECAST_RULE_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_forecast_rules' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_forecast_rules')>0
AND dim_ora_mrp_forecast_rulesid <>1;

/*dim_ora_mrp_system_items*/
/*SELECT distinct rowiscurrent from dim_ora_mrp_system_items*/
UPDATE dim_ora_mrp_system_items T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS ORGANIZATION_ID,VARCHAR1 AS COMPILE_DESIGNATOR, INTEGER2 AS INVENTORY_ITEM_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_system_items' AND T.KEY_ID = VARCHAR(VARCHAR1, 200) + '~' + VARCHAR(INTEGER1, 200) + '~' + VARCHAR(INTEGER2, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_system_items')>0
AND dim_ora_mrp_system_itemsid <>1;

/*dim_ora_mtl_parameters*/
/*SELECT distinct rowiscurrent from dim_ora_mtl_parameters*/
UPDATE dim_ora_mtl_parameters T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS ORGANIZATION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_parameters' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_parameters')>0
AND dim_ora_mtl_parametersid <>1;

/*dim_ora_wip_schedule_groups*/
/*SELECT distinct rowiscurrent from dim_ora_wip_schedule_groups*/
UPDATE dim_ora_wip_schedule_groups T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS SCHEDULE_GROUP_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_wip_schedule_groups' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_wip_schedule_groups')>0
AND dim_ora_wip_schedule_groupsid <>1;

/*dim_ora_wip_lines*/
/*SELECT distinct rowiscurrent from dim_ora_wip_lines*/
UPDATE dim_ora_wip_lines T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS LINE_ID,INTEGER2 AS ORGANIZATION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_wip_lines' AND T.KEY_ID = VARCHAR(INTEGER1, 200) + '~' + VARCHAR(INTEGER2, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_wip_lines')>0
AND dim_ora_wip_linesid <>1;

/*fact_ora_mrp_gross_requirements*/
/*SELECT distinct rowiscurrent from fact_ora_mrp_gross_requirements*/
UPDATE fact_ora_mrp_gross_requirements T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS DEMAND_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_gross_requirements' AND T.DEMAND_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_gross_requirements')>0;

/*fact_ora_mrp_recommendations*/
/*SELECT distinct rowiscurrent from fact_ora_mrp_recommendations*/
UPDATE fact_ora_mrp_recommendations T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS TRANSACTION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_recommendations' AND T.TRANSACTION_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_recommendations')>0;

/*fact_ora_mrp_item_wip_entities*/
/*SELECT distinct rowiscurrent from fact_ora_mrp_item_wip_entities*/
UPDATE fact_ora_mrp_item_wip_entities T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS WIP_ENTITY_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_item_wip_entities' AND T.WIP_ENTITY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_item_wip_entities')>0;

/*fact_ora_mrp_available_to_promise*/
/*SELECT distinct rowiscurrent from fact_ora_mrp_available_to_promise*/
UPDATE fact_ora_mrp_available_to_promise T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS TRANSACTION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_available_to_promise' AND T.TRANSACTION_ID = INTEGER1 
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_available_to_promise')>0;

/*fact_ora_mrp_exception_details*/
/*SELECT distinct rowiscurrent from fact_ora_mrp_exception_details*/
UPDATE fact_ora_mrp_exception_details T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS EXCEPTION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_exception_details' AND T.EXCEPTION_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_exception_details')>0;

/*fact_ora_mrp_forecast_dates*/
/*SELECT distinct rowiscurrent from fact_ora_mrp_forecast_dates*/
UPDATE fact_ora_mrp_forecast_dates T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS TRANSACTION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_forecast_dates' AND T.TRANSACTION_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_forecast_dates')>0;

/*dim_ora_ar_invoice_types*/
/*SELECT distinct rowiscurrent from dim_ora_ar_invoice_types*/
UPDATE dim_ora_ar_invoice_types T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS CUST_TRX_TYPE_ID,INTEGER2 AS ORG_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_ar_invoice_types' AND T.CUST_TRX_TYPE_ID = INTEGER1 AND T.ORG_ID = INTEGER2
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_ar_invoice_types')>0
AND dim_ora_ar_invoice_typesid <>1;

/*fact_ora_ar_sales_invoices*/
/*SELECT distinct rowiscurrent from fact_ora_ar_sales_invoices*/
UPDATE fact_ora_ar_sales_invoices T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS CUST_TRX_LINE_GL_DIST_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_ar_sales_invoices' AND T.CUST_TRX_LINE_GL_DIST_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_ar_sales_invoices')>0;

/*dim_ora_mtl_salesorders*/
/*SELECT distinct rowiscurrent from dim_ora_mtl_salesorders*/
UPDATE dim_ora_mtl_salesorders T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS SALES_ORDER_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_salesorders' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_salesorders')>0
AND dim_ora_mtl_salesordersid <>1;

/*dim_ora_mrp_subinventories*/
/*SELECT distinct rowiscurrent from dim_ora_mrp_subinventories*/
UPDATE dim_ora_mrp_subinventories T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS SUB_INVENTORY_CODE, INTEGER1 AS ORGANIZATION_ID, VARCHAR2 AS COMPILE_DESIGNATOR FROM ORA_SOURCE_TABLEIDS 
WHERE TABLE_NAME ='ora_mrp_subinventories' AND T.KEY_ID = VARCHAR(VARCHAR1, 200) + '~' + VARCHAR(INTEGER1, 200) + '~' +VARCHAR(VARCHAR2, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_subinventories')>0
AND dim_ora_mrp_subinventoriesid <>1;

/*fact_ora_mrp_reservations*/
/*SELECT distinct rowiscurrent from fact_ora_mrp_reservations*/
UPDATE fact_ora_mrp_reservations T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS TRANSACTION_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_reservations' AND T.TRANSACTION_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_reservations')>0;

/*dim_ora_bom_item_costs*/
/*SELECT distinct rowiscurrent from dim_ora_bom_item_costs*/
UPDATE dim_ora_bom_item_costs T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS INVENTORY_ITEM_ID,INTEGER2 AS ORGANIZATION_ID, INTEGER3 AS COST_TYPE_ID 
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_bom_item_costs'
AND T.KEY_ID = VARCHAR(INTEGER1, 200) + '~' + VARCHAR(INTEGER2, 200) + '~' +VARCHAR(INTEGER3, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_bom_item_costs')>0
AND dim_ora_bom_item_costsid <>1;

/*dim_ora_mrp_assignments*/
/*SELECT distinct rowiscurrent from dim_ora_mrp_assignments*/
UPDATE dim_ora_mrp_assignments T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT INTEGER1 AS ASSIGNMENT_SET_ID FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mrp_assignments' AND T.KEY_ID = INTEGER1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_assignments')>0
AND dim_ora_mrp_assignmentsid <>1;

/*fact_ora_mrp_plans*/
/*SELECT distinct rowiscurrent from fact_ora_mrp_plans*/
UPDATE fact_ora_mrp_plans T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS
(
SELECT INTEGER1 AS ORGANIZATION_ID,VARCHAR1 AS COMPILE_DESIGNATOR FROM ORA_SOURCE_TABLEIDS 
WHERE TABLE_NAME ='ora_mrp_plans' AND T.ORGANIZATION_ID = INTEGER1  AND T.COMPILE_DESIGNATOR = VARCHAR1
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mrp_plans')>0;

/*dim_ora_mtl_planners*/
/*SELECT distinct rowiscurrent from dim_ora_mtl_planners*/
UPDATE dim_ora_mtl_planners T
SET rowiscurrent = 0,
DW_UPDATE_DATE = current_timestamp,
rowenddate = current_timestamp,
rowchangereason ='Delete'
WHERE NOT EXISTS (
SELECT VARCHAR1 AS PLANNER_CODE,INTEGER1 AS ORGANIZATION_ID  
FROM ORA_SOURCE_TABLEIDS WHERE TABLE_NAME ='ora_mtl_planners' AND T.KEY_ID =  VARCHAR(VARCHAR1, 200) +'~'+ VARCHAR(INTEGER1, 200)
)
and (select count(*) cnt from ORA_SOURCE_TABLEIDS  WHERE TABLE_NAME ='ora_mtl_planners')>0
AND dim_ora_mtl_plannersid <>1;

/* Update dd_rowiscurrent by Florian on 22th of October 2015 */

/*fact_ora_sales_order_line*/
UPDATE fact_ora_sales_order_line 
SET dd_rowiscurrent = CASE WHEN rowiscurrent = 0 THEN 'Y' ELSE 'N' END
WHERE dd_rowiscurrent <> CASE WHEN rowiscurrent = 0 THEN 'Y' ELSE 'N' END;

/* END Update dd_rowiscurrent by Florian on 22th of October 2015 */

call vectorwise (combine 'fact_ora_sales_order_line');