
/* Custom proc for bi_inventoryturn_processing.sql starts here */

/* Call this between part1 and part2 */

DELETE FROM tmp_funct_fiscal_year_parallel
where fact_script_name = 'bi_inventoryturn_processing';
      

INSERT INTO tmp_funct_fiscal_year_parallel
(pCompanyCode,FiscalYear,Period,fact_script_name)
SELECT DISTINCT pCompCode, pFiYear, pPeriodFrom,'bi_inventoryturn_processing'
FROM tmp_inv_t_cursor t;


			
