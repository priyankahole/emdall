
/* Custom proc for bi_populate_inventory_fact.sql and process_S031 starts here */

/* Run this script,then std fiscal year script before bi_populate_inventory_fact. */


DELETE FROM tmp_funct_fiscal_year
where fact_script_name = 'bi_populate_inventory_fact';

/* After the following insert, call the fiscal year standard script */
INSERT INTO tmp_funct_fiscal_year
(pCompanyCode,FiscalYear,Period,fact_script_name)
SELECT DISTINCT BUKRS, LFGJA, LFMON,'bi_populate_inventory_fact'
FROM (SELECT b.BUKRS, a.LFGJA, a.LFMON FROM MBEW a INNER JOIN T001K b ON a.BWKEY = b.BWKEY
		UNION
	  SELECT b.BUKRS, a.LFGJA, a.LFMON FROM MBEWH a INNER JOIN T001K b ON a.BWKEY = b.BWKEY) x;

DELETE FROM tmp_funct_fiscal_year
where fact_script_name = 'process_S031';

/* After the following insert, call the fiscal year standard script */
INSERT INTO tmp_funct_fiscal_year
(pCompanyCode,FiscalYear,Period,fact_script_name)
SELECT DISTINCT pl.CompanyCode, substring(SPBUP,1,4), substring(SPBUP,5,2),'process_S031'
FROM S031, dim_plant pl
WHERE WERKS = pl.PlantCode;


