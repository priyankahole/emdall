/**************************************************************************************************************/
/*   Script         :  vw_getExchangeRate.bi_populate_direct_ship_so_po_discrepancies_fact  */
/*   Author         : George I.   */
/*   Created On     : 22 Nov 2013 */
/*   Description    : Current version  Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                               */
/*                                                                                                                    */
/******************************************************************************************************************/

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_direct_ship_so_po_discrepancies_fact';

Drop table if exists tmp_globalcur;

Declare global temporary table tmp_globalcur(
	pGlobalCurrency)
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD'),varchar(3)) ON COMMIT PRESERVE ROWS;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, co.currency pToCurrency, NULL exchangeRate,'vw_bi_populate_direct_ship_so_po_discrepancies_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'vw_bi_populate_direct_ship_so_po_discrepancies_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, a.VBAK_STWAE pToCurrency, NULL exchangeRate,'vw_bi_populate_direct_ship_so_po_discrepancies_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,VBAK_AUDAT as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'vw_bi_populate_direct_ship_so_po_discrepancies_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1( pFromCurrency, pDate, pToCurrency, exchangeRate, pFromExchangeRate,fact_script_name)
SELECT pFromCurrency, pDate, pToCurrency, exchangeRate,0,'vw_bi_populate_direct_ship_so_po_discrepancies_fact'
FROM tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_direct_ship_so_po_discrepancies_fact';

drop table if exists tmp_getExchangeRate1_nodups_dsspd;
create table tmp_getExchangeRate1_nodups_dsspd
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_direct_ship_so_po_discrepancies_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_direct_ship_so_po_discrepancies_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_dsspd;

drop table tmp_getExchangeRate1_nodups_dsspd;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
