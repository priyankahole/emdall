
/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Cornelia */
/*   Created On     : 15 Nov 2013 */
/*   Description    : Current version  Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                               */
/*   15 Nov 2013      Cornelia    1.0                Currency and exchange rate chgs. tran->local,tran->global                                       */
/******************************************************************************************************************/

drop table if exists tmp_pGlobalCurrency;

CREATE TABLE tmp_pGlobalCurrency AS
SELECT ifnull(property_value,'USD') as pGlobalCurrency
                                FROM systemproperty
                               WHERE property = 'customer.global.currency';


/*
CREATE TABLE tmp_pGlobalCurrency(pGlobalCurrency) AS
ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
*/

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_notificationitem_fact';


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pToCurrency,pFromExchangeRate, pDate,fact_script_name )
SELECT DISTINCT cp.Currency as pFromCurrency, cp.Currency, null as pFromExchangeRate,ansidate(LOCAL_TIMESTAMP) as pDate,
'vw_bi_populate_notificationitem_fact' as fact_script_name
FROM QMFE, dim_plant w, dim_company cp 
	WHERE w.PlantCode = IFNULL(QMFE.QMFE_WERKS,'Not Set') AND
	cp.CompanyCode=w.CompanyCode;


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pToCurrency,pFromExchangeRate, pDate,fact_script_name )
SELECT DISTINCT cp.Currency as pFromCurrency,  t.pGlobalCurrency, null as pFromExchangeRate, ansidate(LOCAL_TIMESTAMP) as pDate,
'vw_bi_populate_notificationitem_fact' as fact_script_name
FROM QMFE, dim_plant w, dim_company cp, tmp_pGlobalCurrency t
	WHERE w.PlantCode = IFNULL(QMFE.QMFE_WERKS,'Not Set') AND
	cp.CompanyCode=w.CompanyCode;


UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'vw_bi_populate_notificationitem_fact';


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT cp.Currency as pFromCurrency, cp.Currency as pToCurrency, null as pFromExchangeRate, ansidate(LOCAL_TIMESTAMP) as pDate,
'vw_bi_populate_notificationitem_fact' as fact_script_name
FROM QMFE, dim_plant w, dim_company cp 
	WHERE w.PlantCode = IFNULL(QMFE.QMFE_WERKS,'Not Set') AND
	cp.CompanyCode=w.CompanyCode;

/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_notificationitem;
create table tmp_getExchangeRate1_nodups_notificationitem
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_notificationitem_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_notificationitem_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_notificationitem;

drop table tmp_getExchangeRate1_nodups_notificationitem;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');

