
/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_planorder_fact';

/* Tran curr is local currency in this case */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT dc.Currency,'USD' pToCurrency,NULL pFromExchangeRate,PLAF_PEDTR pDate,NULL exchangeRate,'bi_populate_planorder_fact'
FROM plaf p
          INNER JOIN dim_plant pl
             ON pl.PlantCode = p.PLAF_PLWRK
          INNER JOIN dim_company dc
             ON dc.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT dc.Currency,'USD' pToCurrency,NULL pFromExchangeRate,ANSIDATE(LOCAL_TIMESTAMP) pDate,NULL exchangeRate,'bi_populate_planorder_fact'
FROM plaf p
          INNER JOIN dim_plant pl
             ON pl.PlantCode = p.PLAF_PLWRK
          INNER JOIN dim_company dc
             ON dc.CompanyCode = pl.CompanyCode;



UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_planorder_fact';




CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
