
/**************************************************************************************************************/
/*   Script         : vw_getExchangeRate_std   */
/*   Author         : Hiten */
/*   Created On     : 29 Jul 2013 */
/*   Description    : Current version  Vectorwise syntax   */
/*   Last Update    : 01 Jan 2014 */
/******************************************************************************************************************/

UPDATE tmp_getExchangeRate1
set vReverse = 'N',
	updflag = 'N',
	processed_flag = 'Y',
	vFromCurr = pFromCurrency,
	vToCurr = pToCurrency,
	vFTfact = 0,
	vffact = 0,
	vtfact = 0
WHERE processed_flag IS NULL;      

UPDATE tmp_getExchangeRate1
SET exchangeRate = 0
WHERE exchangeRate is null
	AND processed_flag = 'Y' 
	AND updflag = 'N';    

UPDATE tmp_getExchangeRate1
SET pFromExchangeRate = 0
WHERE pFromExchangeRate is null
	AND processed_flag = 'Y' 
	AND updflag = 'N';    

UPDATE tmp_getExchangeRate1
SET exchangeRate = 1,
	updflag = 'Y'
WHERE pFromCurrency = pToCurrency
	AND processed_flag = 'Y' 
	AND updflag = 'N';


UPDATE tmp_getExchangeRate1
SET vReverse = 'Y',
	pFromExchangeRate = -1 * pFromExchangeRate,
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND pFromExchangeRate < 0
	AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 x
SET vReverse = 'Y',
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND NOT EXISTS ( SELECT tcurr_fcurr FROM tcurr y where y.tcurr_fcurr = x.pFromCurrency AND y.tcurr_tcurr = x.pToCurrency )
	AND EXISTS ( SELECT tcurr_fcurr FROM tcurr z where z.tcurr_fcurr = x.pToCurrency AND z.tcurr_tcurr = x.pFromCurrency )
	AND processed_flag = 'Y'
	AND vRefCur is null
	AND ifnull(pFromExchangeRate,0) = 0; /* added 01/24/2014 to fix varian PO 6512990 */ 


UPDATE  tmp_getExchangeRate1
SET vType = ifnull((select TCURV_KURST from tcurv ),'M'),
	vRefCur = (select TCURV_BWAER from tcurv ),
	vInvAllow = ifnull((select 'Y' from tcurv where TCURV_XINVR is not null),'N'),
	vTCURFExists = ifnull((select distinct 1 from TCURF ),0)
WHERE updflag = 'N'
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET vRefCur = null
WHERE updflag = 'N' 
	AND vRefCur = pToCurrency
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET vReverse = 'Y',
	pFromExchangeRate = -1 * pFromExchangeRate,
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency,
	vRefCur = null
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND vRefCur = pFromCurrency
	AND processed_flag = 'Y'	
	AND ifnull(pFromExchangeRate,0) = 0; /* added 01/24/2014 to fix varian PO 6512990 */ 
	
UPDATE tmp_getExchangeRate1
SET pFromCurrency = vFromCurr,
	pToCurrency = vToCurr
WHERE updflag = 'N' 
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET vToCurr = vRefCur
WHERE updflag = 'N' 
	AND vRefCur is not null
	AND vRefCur <> vToCurr
	AND processed_flag = 'Y';


/***** Prepare and use TCURF *****/

drop table if exists tmp_tcurf;
CREATE table tmp_tcurf
AS
SELECT  TCURF_FCURR,TCURF_TCURR,TCURF_KURST,tcurf_gdatu,TCURF_FFACT,TCURF_TFACT,
cast(left((99999999 - tcurf_gdatu),4) || '-'|| left(right((99999999 - tcurf_gdatu),4),2) || '-'|| right((99999999 - tcurf_gdatu),2) AS date) cast_tcurf_gdatu
FROM TCURF;

/* Denormalize to get date ranges */

drop table if exists tmp_tcurf_denorm;
CREATE table tmp_tcurf_denorm
AS
SELECT t.*,cast_tcurf_gdatu as cast_tcurf_gdatu_to
FROM tmp_tcurf t;

merge into tmp_tcurf_denorm x 
using ( SELECT min(cast_tcurf_gdatu)cast_tcurf_gdatu ,TCURF_FCURR,TCURF_TCURR,TCURF_KURST from tmp_tcurf group by TCURF_FCURR,TCURF_TCURR,TCURF_KURST ) y 
on (x.TCURF_FCURR = y.TCURF_FCURR 
                                                        AND x.TCURF_TCURR = y.TCURF_TCURR
                                                        AND x.TCURF_KURST = y.TCURF_KURST ) 
                                                        when matched then update set cast_tcurf_gdatu_to = y.cast_tcurf_gdatu 
                                                        where y.cast_tcurf_gdatu > x.cast_tcurf_gdatu;
/* Now update the latest row with 31 Dec 9999 */

UPDATE tmp_tcurf_denorm x
SET cast_tcurf_gdatu_to = '31 Dec 9999'
WHERE cast_tcurf_gdatu_to IS NULL;

/**********/

/***** Prepare and use TCURR *****/

drop table if exists tmp_tcurr;
CREATE table tmp_tcurr
AS
SELECT  tcurr_fcurr,tcurr_tcurr,tcurr_kurst,tcurr_gdatu,tcurr_ffact,tcurr_tfact,tcurr_ukurs,
cast(left((99999999 - tcurr_gdatu),4) || '-' || left(right((99999999 - tcurr_gdatu),4),2) || '-' || right((99999999 - tcurr_gdatu),2) AS date) cast_tcurr_gdatu
FROM tcurr;

/* Denormalize to get date ranges */

drop table if exists tmp_tcurr_denorm;
CREATE table tmp_tcurr_denorm
AS
SELECT t.*,cast_tcurr_gdatu as cast_tcurr_gdatu_to
FROM tmp_tcurr t;

 merge into tmp_tcurr_denorm x 
using ( SELECT min(cast_tcurr_gdatu)cast_tcurr_gdatu ,tcurr_fcurr,tcurr_tcurr,tcurr_kurst from tmp_tcurr y group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst ) y 
on (x.tcurr_fcurr = y.tcurr_fcurr
                                                        AND x.tcurr_tcurr = y.tcurr_tcurr
                                                        AND x.tcurr_kurst = y.tcurr_kurst) 
                                                        when matched then update set cast_tcurr_gdatu_to = y.cast_tcurr_gdatu 
                                                        where y.cast_tcurr_gdatu > x.cast_tcurr_gdatu;
/* Now update the latest row with 31 Dec 9999 */

UPDATE tmp_tcurr_denorm x
SET cast_tcurr_gdatu_to = '31 Dec 9999'
WHERE cast_tcurr_gdatu_to IS NULL;

/**********/

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_tcurf_denorm t2, tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_tcurf_denorm t2, tmp_getExchangeRate1 g 
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND CURRENT_DATE >= t2.cast_tcurf_gdatu AND  CURRENT_DATE < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

drop table if exists tmp_tcurf_denorm_tmp;
create table tmp_tcurf_denorm_tmp 
as
select distinct TCURF_FFACT,TCURF_TFACT,TCURF_KURST,TCURF_TCURR,TCURF_FCURR, row_number() over(order by'') rownr from tmp_tcurf_denorm 
;


	UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_tcurf_denorm_tmp t2, tmp_getExchangeRate1 g 
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y'
	and rownr=1;

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
from tmp_getExchangeRate1 g,tmp_tcurf_denorm t2
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
from tmp_getExchangeRate1 g,tmp_tcurf_denorm t2
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND CURRENT_DATE >= t2.cast_tcurf_gdatu AND  CURRENT_DATE < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
from tmp_getExchangeRate1 g,tmp_tcurf_denorm_tmp t2
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y'
	and rownr=1;

	drop table if exists tmp_tcurf_denorm_tmp;
	
	
UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURR_FFACT * t2.TCURR_TFACT,
	vffact = t2.TCURR_FFACT,
	vtfact = t2.TCURR_TFACT
from tmp_getExchangeRate1 g,tmp_tcurr_denorm t2
WHERE g.pFromCurrency = t2.TCURR_FCURR AND g.pToCurrency = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURR_FFACT * t2.TCURR_TFACT,
	vffact = t2.TCURR_FFACT,
	vtfact = t2.TCURR_TFACT
from tmp_getExchangeRate1 g,tmp_tcurr_denorm t2
WHERE g.pFromCurrency = t2.TCURR_FCURR AND g.pToCurrency = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2.cast_tcurr_gdatu AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';



UPDATE tmp_getExchangeRate1 g
SET exchangeRate = case vReverse 
			when 'Y' then round(pFromExchangeRate * case vFTfact when 0 then 1.00000000 else vFTfact end,6)
			else round(pFromExchangeRate / case vFTfact when 0 then 1.00000000 else vFTfact end,6)
		   end,
	updflag = 'A'
WHERE pFromExchangeRate > 0
	AND updflag = 'N' 
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate,
	updflag = 'A'
WHERE pFromExchangeRate > 0
	AND updflag = 'A'
	AND exchangeRate = 0
	AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = round(tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6),
	updflag = 'B'
FROM tmp_tcurr_denorm t2, tmp_getExchangeRate1 g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND vReverse = 'Y'
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = round(tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6),
	updflag = 'B'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2.cast_tcurr_gdatu AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND vReverse = 'Y'
	AND g.processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = CASE WHEN tcurr_ukurs = 0 THEN 1
			WHEN tcurr_ukurs > 0
                        THEN round(tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6)
			ELSE round(-1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end),6)
		   END,
	updflag = 'B'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = CASE WHEN tcurr_ukurs = 0 THEN 1
			WHEN tcurr_ukurs > 0
                        THEN round(tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6)
			ELSE round(-1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end),6)
		   END,
	updflag = 'B'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2.cast_tcurr_gdatu AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND g.processed_flag = 'Y';



UPDATE tmp_getExchangeRate1
SET vFromCurr = pToCurrency, updflag = 'C'
WHERE updflag = 'B' 
	AND vRefCur is not null
	AND vRefCur = vToCurr
	AND vRefCur <> pToCurrency
	AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = exchangeRate / CASE WHEN tcurr_ukurs = 0 THEN 1
					WHEN tcurr_ukurs > 0
					THEN round(tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6)
					ELSE round(-1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end),6)
				   END,
	updflag = 'D'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'C' 
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = exchangeRate / CASE WHEN tcurr_ukurs = 0 THEN 1
					WHEN tcurr_ukurs > 0
					THEN round(tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6)
					ELSE round(-1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end),6)
				   END,
	updflag = 'D'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2.cast_tcurr_gdatu AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'C' 
	AND g.processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g
SET exchangeRate = -1 * exchangeRate
WHERE updflag <> 'N'
	AND vReverse = 'Y'
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = 1, updflag = 'E'
WHERE exchangeRate = 0
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = round(-1/exchangeRate,6), updflag = 'E'
WHERE exchangeRate < 0
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET vFromCurr = pFromCurrency,
	vToCurr = pToCurrency
WHERE processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET pFromCurrency = vToCurr,
	pToCurrency = vFromCurr,
	pfromexchangerate = -1 * pfromexchangerate
WHERE processed_flag = 'Y'
	AND vReverse = 'Y';

UPDATE tmp_getExchangeRate1
SET processed_flag = 'D'
WHERE processed_flag = 'Y';



