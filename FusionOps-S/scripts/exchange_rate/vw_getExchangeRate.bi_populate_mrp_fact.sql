
/* Start of custom exchange rate proc for bi_populate_mrp_fact. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_mrp_fact';

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT c.Currency pFromCurrency,'USD' pToCurrency,null pFromExchangeRate,MDKP_DSDAT pDate,'bi_populate_mrp_fact' fact_script_name
FROM mdkp m, dim_plant pl,dim_company dc,dim_currency c
WHERE m.MDKP_PLWRK = pl.PlantCode
AND  pl.CompanyCode = dc.CompanyCode
AND c.currencycode = dc.currency;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT EBAN_WAERS pFromCurrency,'USD' pToCurrency,null pFromExchangeRate,eban_badat pDate,'bi_populate_mrp_fact' fact_script_name
FROM eban;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT c.Currency pFromCurrency,'USD' pToCurrency,null pFromExchangeRate,ansidate(LOCAL_TIMESTAMP) pDate,'bi_populate_mrp_fact' fact_script_name
FROM mdkp m, dim_plant pl,dim_company dc,dim_currency c
WHERE m.MDKP_PLWRK = pl.PlantCode
AND  pl.CompanyCode = dc.CompanyCode
AND c.currencycode = dc.currency;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT EBAN_WAERS pFromCurrency,'USD' pToCurrency,null pFromExchangeRate,ansidate(LOCAL_TIMESTAMP) pDate,'bi_populate_mrp_fact' fact_script_name
FROM eban;


UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_mrp_fact' ;

/* EBAN_WAERS -> Local */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT EBAN_WAERS pFromCurrency,dc.currency,null pFromExchangeRate,eban_badat pDate,'bi_populate_mrp_fact' fact_script_name
FROM eban pr, mdtb t,mdkp m ,dim_plant pl,dim_company dc
WHERE t.MDTB_DELNR = pr.eban_BANFN
AND t.MDTB_DELPS = pr.eban_BNFPO
AND t.MDTB_DTNUM = m.MDKP_DTNUM
AND m.MDKP_PLWRK = pl.PlantCode
AND pl.CompanyCode = dc.CompanyCode;

UPDATE tmp_getExchangeRate1
set exchangeRate = NULL
WHERE fact_script_name = 'bi_populate_mrp_fact';

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
