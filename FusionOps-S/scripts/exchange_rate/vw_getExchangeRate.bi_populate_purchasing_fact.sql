/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
select 'processinglog',ifnull(max(processinglogid),0)
FROM processinglog;


/* ekko_ekpo_eket updates need to be handled here as this is used for exchange rate processing of purchasing_fact */

/* Start of ekko_ekpo_eket updates */

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

 
INSERT INTO ekko_ekpo_eket
SELECT distinct * 
FROM ekko_ekpo_eket_mseg a
WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);
	
	
DELETE FROM ekko_ekpo_eket_mseg;
/*Georgiana Changes 11 Jun 2015- enumerating the columns for insert*/
INSERT INTO ekko_ekpo_eket (ekpo_ebeln,
ekpo_ebelp,
eket_etenr,
eket_banfn,
eket_menge,
eket_eindt,
eket_bedat,
eket_wemng,
eket_slfdt,
ekko_aedat,
ekko_bukrs,
ekko_bstyp,
ekko_bsart,
ekko_ernam,
ekko_ekorg,
ekko_lifnr,
ekko_waers,
ekko_bedat,
ekko_ekgrp,
ekko_wkurs,
ekko_loekz,
ekko_statu,
ekko_reswk,
ekko_autlf,
ekko_fixpo,
ekko_kufix,
ekko_frgzu,
ekko_zterm,
ekko_kalsm,
ekpo_matnr,
ekpo_werks,
ekpo_meins,
ekpo_netpr,
ekpo_loekz,
ekpo_bprme,
ekpo_peinh,
ekpo_menge,
ekpo_matkl,
ekpo_mtart,
ekpo_knttp,
ekpo_netwr,
ekpo_pstyp,
ekpo_elikz,
ekpo_retpo,
ekpo_umrez,
ekpo_umren,
ekpo_lmein,
ekpo_insmk,
ekpo_bwtar,
ekpo_infnr,
ekpo_webaz,
ekpo_uebto,
ekpo_untto,
ekpo_idnlf,
ekpo_aedat,
ekpo_prdat,
ekpo_kzvbr,
ekpo_inco1,
ekpo_status,
ekpo_adrnr,
ekpo_lgort,
ekpo_mwskz,
ekpo_vrtkz,
ekpo_erekz,
ekpo_wepos,
ekpo_weunb,
ekpo_j_1bownpro,
ekpo_abskz,
ekpo_bnfpo,
ekpo_bpumz,
ekpo_bpumn,
ekpo_effwr,
eket_sernr,
ekpo_adrn2,
ekko_absgr,
ekpo_packno,
ekpo_etfz1,
ekpo_lgbzo,
ekpo_lgbzo_b,
ekpo_plifz,
ekpo_inco2,
ekko_inco1,
ekko_inco2,
ekpo_revlv,
ekko_ihrez,
ekpo_ko_prctr,
eket_wamng,
ekpo_reslo,
ekko_kdatb,
ekko_kdate,
ekpo_stapo,
ekpo_txz01,
eket_bnfpo,
ekpo_banfn,
ekpo_bukrs,
ekko_knumv,
ekpo_brtwr,
ekpo_kzwi1,
ekpo_kzwi2,
ekpo_kzwi3,
ekpo_kzwi4,
ekpo_kzwi5,
ekpo_kzwi6,
ekpo_j_3aexfcm,
ekpo_route,
ekpo_tsbed,
eket_j_3anetp,
eket_j_3anetw,
eket_j_4kscat,
eket_j_3asize,
ekpo_j_3asean,
ekpo_j_3adat,
ekpo_afs_collection,
ekpo_afs_theme,
eket_j_3aefda,
eket_dabmg,
ekko_frgke,
eket_j_3aelikz,
eket_j_3aetenr,
ekpo_zzppv_rc,
ekpo_zzppv_rct,
ekko_lponr,
ekpo_ltsnr,
ekpo_zzprior,
ekpo_uebtk,
ekko_unsez,
eket_j_3auanr,
eket_j_3aupos,
eket_j_3aetenv,
ekko_exnum,
eket_mbdat,
eket_charg,
eket_licha,
ekpo_j_3aexfcp)
SELECT distinct 
ekpo_ebeln,
ekpo_ebelp,
eket_etenr,
eket_banfn,
eket_menge,
eket_eindt,
eket_bedat,
eket_wemng,
eket_slfdt,
ekko_aedat,
ekko_bukrs,
ekko_bstyp,
ekko_bsart,
ekko_ernam,
ekko_ekorg,
ekko_lifnr,
ekko_waers,
ekko_bedat,
ekko_ekgrp,
ekko_wkurs,
ekko_loekz,
ekko_statu,
ekko_reswk,
ekko_autlf,
ekko_fixpo,
ekko_kufix,
ekko_frgzu,
ekko_zterm,
ekko_kalsm,
ekpo_matnr,
ekpo_werks,
ekpo_meins,
ekpo_netpr,
ekpo_loekz,
ekpo_bprme,
ekpo_peinh,
ekpo_menge,
ekpo_matkl,
ekpo_mtart,
ekpo_knttp,
ekpo_netwr,
ekpo_pstyp,
ekpo_elikz,
ekpo_retpo,
ekpo_umrez,
ekpo_umren,
ekpo_lmein,
ekpo_insmk,
ekpo_bwtar,
ekpo_infnr,
ekpo_webaz,
ekpo_uebto,
ekpo_untto,
ekpo_idnlf,
ekpo_aedat,
ekpo_prdat,
ekpo_kzvbr,
ekpo_inco1,
ekpo_status,
ekpo_adrnr,
ekpo_lgort,
ekpo_mwskz,
ekpo_vrtkz,
ekpo_erekz,
ekpo_wepos,
ekpo_weunb,
ekpo_j_1bownpro,
ekpo_abskz,
ekpo_bnfpo,
ekpo_bpumz,
ekpo_bpumn,
ekpo_effwr,
eket_sernr,
ekpo_adrn2,
ekko_absgr,
ekpo_packno,
ekpo_etfz1,
ekpo_lgbzo,
ekpo_lgbzo_b,
ekpo_plifz,
ekpo_inco2,
ekko_inco1,
ekko_inco2,
ekpo_revlv,
ekko_ihrez,
ekpo_ko_prctr,
eket_wamng,
ekpo_reslo,
ekko_kdatb,
ekko_kdate,
ekpo_stapo,
ekpo_txz01,
eket_bnfpo,
ekpo_banfn,
ekpo_bukrs,
ekko_knumv,
ekpo_brtwr,
ekpo_kzwi1,
ekpo_kzwi2,
ekpo_kzwi3,
ekpo_kzwi4,
ekpo_kzwi5,
ekpo_kzwi6,
ekpo_j_3aexfcm,
ekpo_route,
ekpo_tsbed,
eket_j_3anetp,
eket_j_3anetw,
eket_j_4kscat,
eket_j_3asize,
ekpo_j_3asean,
ekpo_j_3adat,
ekpo_afs_collection,
ekpo_afs_theme,
eket_j_3aefda,
eket_dabmg,
ekko_frgke,
eket_j_3aelikz,
eket_j_3aetenr,
ekpo_zzppv_rc,
ekpo_zzppv_rct,
ekko_lponr,
ekpo_ltsnr,
ekpo_zzprior,
ekpo_uebtk,
ekko_unsez,
eket_j_3auanr,
eket_j_3aupos,
eket_j_3aetenv,
ekko_exnum,
eket_mbdat,
eket_charg,
eket_licha,
ekpo_j_3aexfcp /*End Changes 11 Jun 2015*/
FROM ekko_ekpo_eket_ekbe a
WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);

DELETE FROM ekko_ekpo_eket_ekbe;

UPDATE ekko_ekpo_eket
SET EKPO_BWTAR = IFNULL(EKPO_BWTAR,'Not Set'), EKPO_BUKRS = IFNULL(EKPO_BUKRS,EKKO_BUKRS);

INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

/* End of ekko_ekpo_eket updates */

				 
								 
DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchasing_fact';

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT EKKO_WAERS as pFromCurrency, 'USD' as pToCurrency, 0 as pFromExchangeRate, EKKO_BEDAT as pDate, 
'bi_populate_purchasing_fact' as fact_script_name
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT EKKO_WAERS as pFromCurrency, 'USD' as pToCurrency, 0 as pFromExchangeRate, ansidate(LOCAL_TIMESTAMP) as pDate,
'bi_populate_purchasing_fact' as fact_script_name
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS;

 

 UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_purchasing_fact'; 

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT EKKO_WAERS as pFromCurrency, dc.Currency as pToCurrency, EKKO_WKURS as pFromExchangeRate, EKKO_BEDAT as pDate,
'bi_populate_purchasing_fact' as fact_script_name
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS;


drop table if exists tmp_getExchangeRate1_nodups_pur_fact;
create table tmp_getExchangeRate1_nodups_pur_fact
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchasing_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchasing_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_pur_fact;

drop table tmp_getExchangeRate1_nodups_pur_fact;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
