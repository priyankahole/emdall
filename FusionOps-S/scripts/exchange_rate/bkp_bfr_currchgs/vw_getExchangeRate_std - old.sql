
/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Lokesh */
/*   Created On     : 25 Jul 2013 */
/*   Description    : Current version  Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   25 Jul 2013      Lokesh    1.1               Reverse Exchange rate logic as asked by Hiten.                  */
/*                                                Check for existence of from and to currencies in tcurr.         */
/*                                                If not found, set reverse flag to Y                             */
/*                                                Also, make decimal(10,6) changes mentioned by Hiten	         */
/*   25 Jul 2013      Lokesh    1.0               Named current version as 1.0                                  */
/******************************************************************************************************************/


CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');

UPDATE tmp_getExchangeRate1
set vReverse = 'N',updflag = 'N', processed_flag = 'Y'
WHERE processed_flag IS NULL;       /* Only pick up the rows which have been recently inserted by the corresponding fact-specific exchange rate proc */


UPDATE tmp_getExchangeRate1
SET exchangeRate = 1,updflag = 'Y'
WHERE pFromCurrency = pToCurrency
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1
SET vReverse = 'Y',
 pFromExchangeRate = -1 * pFromExchangeRate,
 pFromCurrency = pToCurrency,
 pToCurrency = pFromCurrency
WHERE updflag = 'N' AND vReverse = 'N'
AND pFromExchangeRate < 0
AND processed_flag = 'Y';

/* Set reverse flag to Y where from-to combination is not found but to-from combination is found in tcurr */

UPDATE tmp_getExchangeRate1 x
SET vReverse = 'Y',
 pFromExchangeRate = -1 * pFromExchangeRate,
 pFromCurrency = pToCurrency,
 pToCurrency = pFromCurrency
WHERE updflag = 'N' AND vReverse = 'N'
AND NOT EXISTS ( SELECT 1 FROM tcurr y where y.tcurr_fcurr = x.pFromCurrency AND y.tcurr_tcurr = x.pToCurrency )
AND EXISTS ( SELECT 1 FROM tcurr z where z.tcurr_fcurr = x.pToCurrency AND z.tcurr_tcurr = x.pFromCurrency )
AND processed_flag = 'Y';


UPDATE  tmp_getExchangeRate1
SET vType = ifnull((select TCURV_KURST from tcurv ),'M'),
vRefCur = (select TCURV_BWAER from tcurv ),
vInvAllow = ifnull((select 'Y' from tcurv where TCURV_XINVR is not null),'N'),
vFromCurr = pFromCurrency,
vToCurr = pToCurrency,
vTCURFExists = ifnull((select count(*) from TCURF ),0)
WHERE updflag = 'N'
AND processed_flag = 'Y';


/* Prepare and use TCURF ( Moved this section up, so that data is available for Hiten's changes */

drop table if exists tmp_tcurf;
CREATE table tmp_tcurf
AS
SELECT  TCURF_FCURR,TCURF_TCURR,TCURF_KURST,tcurf_gdatu,TCURF_FFACT,TCURF_TFACT,
cast(left((99999999 - tcurf_gdatu),4) + '-' + left(right((99999999 - tcurf_gdatu),4),2) + '-' + right((99999999 - tcurf_gdatu),2) AS date) "cast_tcurf_gdatu"
FROM TCURF;


/* Denormalize to get date ranges */

drop table if exists tmp_tcurf_denorm;
CREATE table tmp_tcurf_denorm
AS
SELECT t.*,cast_tcurf_gdatu as cast_tcurf_gdatu_to
FROM tmp_tcurf t;


UPDATE  tmp_tcurf_denorm x
SET cast_tcurf_gdatu_to = ( SELECT min(cast_tcurf_gdatu) from tmp_tcurf y
                                                        WHERE x.TCURF_FCURR = y.TCURF_FCURR
                                                        AND x.TCURF_TCURR = y.TCURF_TCURR
                                                        AND x.TCURF_KURST = y.TCURF_KURST
                                                        AND y.cast_tcurf_gdatu > x.cast_tcurf_gdatu );

/* Now update the latest row with 31 Dec 9999 */

UPDATE tmp_tcurf_denorm x
SET cast_tcurf_gdatu_to = '31 Dec 9999'
WHERE cast_tcurf_gdatu_to IS NULL;

/* Now get the max value of cast_tcurf_gdatu for each row of tmp_tcurf, where cast_tcurf_gdatu <= g.pDate */

drop table if exists tmp2_TCURF;
CREATE TABLE tmp2_TCURF
AS
SELECT   TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate,max(cast_tcurf_gdatu) as max_cast_tcurf_gdatu
FROM tmp_tcurf T,tmp_getExchangeRate1 g
where T.TCURF_FCURR = g.vFromCurr and T.TCURF_TCURR = g.vToCurr and T.TCURF_KURST = g.vType
                                  and g.pDate >= cast_tcurf_gdatu
AND g.updflag = 'N'
group by TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate;

/*Wherever row with g.pDate >= cast_tcurf_gdatu was not found, use TIMESTAMP(LOCAL_TIMESTAMP)*/

/* This wont be required according to the new logic as we have extended the last date to 31 Dec 9999 : LK 25 May 2013 */
/* Just keeping it here, but the queries that depended on this have been removed */

INSERT INTO tmp2_TCURF
SELECT   TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate,max(cast_tcurf_gdatu) as max_cast_tcurf_gdatu
FROM tmp_tcurf T,tmp_getExchangeRate1 g
where T.TCURF_FCURR = g.vFromCurr and T.TCURF_TCURR = g.vToCurr and T.TCURF_KURST = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_tcurf_gdatu
AND g.updflag = 'N'
AND NOT EXISTS ( SELECT 1 FROM tmp2_TCURF t2 where t2.TCURF_FCURR = T.TCURF_FCURR and t2.TCURF_TCURR = T.TCURF_TCURR and
                                        t2.TCURF_KURST = T.TCURF_KURST AND g.pDate = t2.pDate )
group by TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate;



/*Update vFTfact. Note that there should be 1-1 mapping for g.pDate >= t2.max_cast_tcurf_gdatu due to the above queries*/

UPDATE tmp_getExchangeRate1 g
SET updflag = 'Z'
WHERE g.updflag = 'N'
AND g.pFromExchangeRate > 0
AND g.vTCURFExists > 0
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g FROM tmp_tcurf_denorm t2
SET vFTfact = decimal(t2.TCURF_FFACT * t2.TCURF_TFACT,19,5)
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
AND g.updflag = 'Z'
AND g.processed_flag = 'Y';

-- This is to take care of rows which we had stored using TIMESTAMP(LOCAL_TIMESTAMP) >= cast_tcurf_gdatu
/*
UPDATE tmp_getExchangeRate1 g FROM tmp_tcurf_denorm t2
SET vFTfact = decimal(t2.TCURF_FFACT * t2.TCURF_TFACT,19,5)
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_tcurf_gdatu = t2.max_cast_tcurf_gdatu
AND g.updflag = 'Z'*/


UPDATE tmp_getExchangeRate1 g
SET vFTfact = 1
where vFTfact IS NULL
AND g.updflag = 'Z'
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g
SET exchangeRate = round(pFromExchangeRate * vFTfact,6)
WHERE vReverse = 'Y'
AND g.updflag = 'Z'
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = round(pFromExchangeRate / vFTfact,6)
WHERE vReverse <> 'Y'
AND g.updflag = 'Z'
AND processed_flag = 'Y';

/* End of Hiten's changes1 18th Jan     */

UPDATE  tmp_getExchangeRate1
set vToCurr = vRefCur
where vRefCur is not null AND vRefCur <> pToCurrency
AND updflag = 'N'
AND processed_flag = 'Y';


UPDATE  tmp_getExchangeRate1
set vReverse = 'Y', vToCurr = pFromCurrency, pFromCurrency = pToCurrency,pToCurrency = vToCurr,vFromCurr = pFromCurrency
where vRefCur is not null AND vRefCur <> pToCurrency
AND vReverse = 'N' AND vRefCur = pFromCurrency
AND updflag = 'N'
AND processed_flag = 'Y';

/*
Section 2
Update vFfact. Note that there should be 1-1 mapping for g.pDate >= t2.max_cast_tcurf_gdatu
due to the above queries */

UPDATE tmp_getExchangeRate1 g FROM tmp_tcurf_denorm t2
SET vFfact = t2.TCURF_FFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
AND updflag = 'N'
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1
SET vFfact = 1
where vFfact IS NULL
AND updflag = 'N'
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g FROM tmp_tcurf_denorm t2
SET vTfact = t2.TCURF_TFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
AND updflag = 'N'
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET vTfact = 1
where vTfact IS NULL
AND updflag = 'N'
AND processed_flag = 'Y';

/*Block 1 ( vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0 )*/


UPDATE tmp_getExchangeRate1
SET updflag = 'A'
WHERE updflag = 'N'
AND vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0
AND processed_flag = 'Y';



UPDATE tmp_getExchangeRate1
SET updflag = 'B'
WHERE updflag = 'N'
AND processed_flag = 'Y';



UPDATE tmp_getExchangeRate1 g FROM tmp_tcurf_denorm t2
SET exchangeRate = round(pFromExchangeRate * t2.TCURF_FFACT * t2.TCURF_TFACT,6),updflag = 'A1'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
AND g.vReverse = 'Y'
AND g.updflag = 'A'
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g
SET exchangeRate = round(pFromExchangeRate * 1,6),updflag='A3'
where exchangeRate IS NULL
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'Y'
AND g.updflag = 'A'
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g FROM tmp_tcurf_denorm t2
SET exchangeRate = round(pFromExchangeRate / t2.TCURF_FFACT * t2.TCURF_TFACT,6),updflag='A4'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'N'
AND g.updflag = 'A'
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = round(pFromExchangeRate / 1,6),updflag='A6'
where exchangeRate IS NULL
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'N'
AND g.updflag = 'A'
AND processed_flag = 'Y';


/* Block 2 ( pFromExchangeRate = 0 )
 and ! vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0 */


drop table if exists tmp_tcurr;
CREATE table tmp_tcurr
AS
SELECT  tcurr_fcurr,tcurr_tcurr,tcurr_kurst,tcurr_gdatu,tcurr_ffact,tcurr_tfact,tcurr_ukurs,
cast(left((99999999 - tcurr_gdatu),4) + '-' + left(right((99999999 - tcurr_gdatu),4),2) + '-' + right((99999999 - tcurr_gdatu),2) AS date) "cast_tcurr_gdatu"
FROM tcurr;



/* Denormalize to get date ranges */

drop table if exists tmp_tcurr_denorm;
CREATE table tmp_tcurr_denorm
AS
SELECT t.*,cast_tcurr_gdatu as cast_tcurr_gdatu_to
FROM tmp_tcurr t;


UPDATE  tmp_tcurr_denorm x
SET cast_tcurr_gdatu_to = ( SELECT min(cast_tcurr_gdatu) from tmp_tcurr y
                                                        WHERE x.tcurr_fcurr = y.tcurr_fcurr
                                                        AND x.tcurr_tcurr = y.tcurr_tcurr
                                                        AND x.tcurr_kurst = y.tcurr_kurst
                                                        AND y.cast_tcurr_gdatu > x.cast_tcurr_gdatu );


/* Now update the latest row with 31 Dec 9999 */

UPDATE tmp_tcurr_denorm x
SET cast_tcurr_gdatu_to = '31 Dec 9999'
WHERE cast_tcurr_gdatu_to IS NULL;



/*Now get the max value of cast_tcurf_gdatu for each row of tmp_tcurf, where cast_tcurf_gdatu <= g.pDate*/

drop table if exists tmp2_tcurr;
CREATE TABLE tmp2_tcurr
AS
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and g.pDate >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;


/*Wherever row with g.pDate >= cast_tcurr_gdatu was not found, use TIMESTAMP(LOCAL_TIMESTAMP)*/

INSERT INTO tmp2_tcurr
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
AND NOT EXISTS ( SELECT 1 FROM tmp2_tcurr t2 where t2.tcurr_fcurr = T.tcurr_fcurr and t2.tcurr_tcurr = T.tcurr_tcurr and
                                        t2.tcurr_kurst = T.tcurr_kurst AND g.pDate = t2.pDate )
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;

/*tmp3 : This just has the max dates ( no g.pDate >= cast_tcurr_gdatu condition ), as some queries require just this*/

drop table if exists tmp3_tcurr;
CREATE TABLE tmp3_tcurr
AS
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;


UPDATE tmp_getExchangeRate1
SET updflag='C1'
WHERE updflag = 'B'
and pFromExchangeRate = 0
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate = round(tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),6)
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND g.updflag = 'C1'
AND ( g.pFromExchangeRate = 0 )
AND t2.tcurr_ukurs > 0
AND processed_flag = 'Y';




UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate = round(-1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),6)
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND g.updflag = 'C1'
AND ( g.pFromExchangeRate = 0 )
AND t2.tcurr_ukurs < 0
AND processed_flag = 'Y';


/*
Upd2a ( Not updated in the above block : pFromExchangeRate = 0 ).
AND tcurr_ukurs = pFromExchangeRate instead of pFromExchangeRate = 0
where B ( set as C2 ) */

UPDATE tmp_getExchangeRate1
SET updflag='C2'
WHERE updflag = 'B'
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate = round(tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),6)
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND g.updflag = 'C2'
AND tcurr_ukurs = pFromExchangeRate
AND t2.tcurr_ukurs > 0
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate = round(-1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),6)
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND g.updflag = 'C2'
AND tcurr_ukurs = pFromExchangeRate
AND t2.tcurr_ukurs < 0
AND processed_flag = 'Y';


/* Block 3 ( exchangeRate IS NULL ). Exactly similar to Block 1, other than these 3 conditions
(Block 1 ( vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0 ) ) */

UPDATE tmp_getExchangeRate1
SET updflag='D'
WHERE updflag = 'C2'
AND exchangeRate IS NULL
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate = round(pFromExchangeRate * t2.TCURR_FFACT * t2.TCURR_TFACT,6),updflag='E'
WHERE g.pFromCurrency = t2.TCURR_FCURR AND g.pToCurrency = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND g.vReverse = 'Y'
AND g.updflag = 'D'
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = round(pFromExchangeRate * 1,6),updflag='E'
where g.vReverse = 'Y'
AND g.updflag = 'D'
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate = round(pFromExchangeRate / (CASE WHEN (t2.TCURR_FFACT * t2.TCURR_TFACT) = 0 THEN 1 ELSE t2.TCURR_FFACT * t2.TCURR_TFACT END),6),updflag='D5'
WHERE g.pFromCurrency = t2.TCURR_FCURR AND g.pToCurrency = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND g.vReverse = 'N'
AND g.updflag = 'D'
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g
SET exchangeRate = round(pFromExchangeRate / 1,6),updflag='D5'
where exchangeRate IS NULL
AND g.updflag = 'D'
AND g.vReverse = 'N'
AND processed_flag = 'Y';


/* Block 4 ( exchangeRate IS NULL , but this time no condition on updflag = 'N' )
: 21 Mar 2013 : THIS BLOCK NEEDS TO BE RELOOKED till C

Exactly similar to Block 2, but no condition on pFromExchangeRate = 0
Also, no condition on g.pDate >= t2.max_cast_tcurr_gdatu

LK:Main Block  (  IF (exchangeRate IS NULL)       --LK:where flag = B )
albea gets updated here
Note : This should use tmp3 and not tmp2 as it cares just about the max dates ( and does not need to check if its less than pDate )*/


UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate = round(tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),6)
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
/*AND g.updflag = 'B'*/
AND g.updflag NOT IN ('A','Y','Z')
AND exchangeRate IS NULL
AND t2.tcurr_ukurs > 0
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate = round(-1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),6)
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
/*AND g.updflag = 'B'*/
AND g.updflag NOT IN ('A','Y','Z')
AND exchangeRate IS NULL
AND t2.tcurr_ukurs < 0
AND processed_flag = 'Y';




/* Block 5 (vRefCur is not null AND vRefCur <> pToCurrency)
Exactly similar to Block 2, but no condition on pFromExchangeRate = 0
Instead vRefCur is not null AND vRefCur <> pToCurrency
And exchangeRate1 is updated instead of exchangeRate*/


UPDATE tmp_getExchangeRate1
set updflag='F'
where updflag NOT IN ( 'A','Y','Z')
AND vRefCur is not null AND vRefCur <> pToCurrency
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET  vFromCurr = pToCurrency
where updflag='F'
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate1 = round(tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),6),
        updflag='F1'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND updflag='F'
AND t2.tcurr_ukurs > 0
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate1 = round(-1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),6),
        updflag='F2'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND updflag='F'
AND t2.tcurr_ukurs < 0
AND processed_flag = 'Y';

/* Block 5 a - Remove the condition on g.pDate >= t2.max_cast_tcurr_gdatu where exchangeRate1 is null and
vRefCur is not null AND vRefCur <> pToCurrency
Refer this line in getExchangeRate.sql.orig.albea.comments
IF (exchangeRate1 IS NULL)          --LK: WHERE Flag = F and exchangeRate1 IS NULL in getExchangeRate.sql.orig.albea.comments */

UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate1 = round(tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),6),
        updflag='F3'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND exchangeRate1 IS NULL
AND updflag IN ( 'F','F1','F2' )
AND t2.tcurr_ukurs > 0
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g FROM tmp_tcurr_denorm t2
SET exchangeRate1 = round(-1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t2.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),6),
        updflag='F4'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
AND updflag IN ( 'F','F1','F2' )
AND exchangeRate1 IS NULL
AND t2.tcurr_ukurs < 0
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET exchangeRate = round(exchangeRate / exchangeRate1,6)
WHERE updflag IN ( 'F','F1','F2','F3','F4')
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1
SET exchangeRate = ifnull(pFromExchangeRate,0)
WHERE ifnull(exchangeRate, 0) = 0
AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1
SET exchangeRate = -1 * exchangeRate
where vReverse = 'Y'
AND processed_flag = 'Y';

/* Update the tocurr to fromcurr and fromcurr to tocurr for reversed currs - so that the where condition in fact queries can be used  */
UPDATE tmp_getExchangeRate1
SET pToCurrency = pFromCurrency,
         pFromCurrency = pToCurrency
WHERE vReverse = 'Y'
AND  processed_flag = 'Y';


UPDATE tmp_getExchangeRate1
SET exchangeRate = 1
WHERE ifnull(exchangeRate, 0) = 0
AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET exchangeRate = round(-1/exchangeRate,6)
WHERE exchangeRate < 0
AND processed_flag = 'Y';

/* Mark the processed flag as Done */

UPDATE tmp_getExchangeRate1
SET processed_flag = 'D'
WHERE processed_flag = 'Y';

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
