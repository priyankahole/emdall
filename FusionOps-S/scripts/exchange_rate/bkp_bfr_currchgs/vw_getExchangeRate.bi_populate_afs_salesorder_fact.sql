

/* ##################################################################################################################
  
     Script         : gw_getExchangeRate.bi_populate_afs_salesorder_fact.sql
     Author         : Lokesh
     Created On     : 7-Jul-2013
  
  
     Description    : Exchange rate script for afs_salesorder_fact - after changes for parallel processing to previous script
  
     Change History
     Date            By        Version           Desc
     7-Jul-2013      Lokesh    1.1               Brought in sync with latest bi_populate_afs_salesorder_fact script
     7-Jul-2013      Lokesh    1.0               Modified the previous afs sof exchange rate script to use fact_script_name flag. 
							Removed drop-create. Renamed ( removed _custom from name )
#################################################################################################################### */

/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_salesorder_fact';

/* This section populates tmp_getExchangeRate1. Only this will change for different procs, remaining code will remain the same */

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pDate,pToCurrency,pFromExchangeRate,fact_script_name)
SELECT DISTINCT co.Currency,VBAK_AUDAT,'USD' pToCurrency,vbap_stcur, 'bi_populate_afs_salesorder_fact'  fact_script_name
FROM VBAK_VBAP_VBEP,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
UNION
SELECT DISTINCT co.Currency,VBAK_AUDAT,s.property_value pToCurrency,vbap_stcur,'bi_populate_afs_salesorder_fact'  fact_script_name
FROM VBAK_VBAP_VBEP,
     Dim_Plant pl,
     Dim_Company co,
         systemproperty s
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
AND s.property = 'customer.global.currency';

/* 7-July : Additional queries to populate data, as per the queries used in bi_populate_afs_salesorder_fact script. 
   Synch up the queries so that no data is missing in tmp_getExchangeRate1  */
/* 7-July : No need of the previous insert queries looking at the current queries in afs_so_fact script. But keeping them in case they are required later. 
   Wont cause any harm, just that it might add some unnecessary data */


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pDate,pToCurrency,fact_script_name)
SELECT DISTINCT VBAP_WAERK pFromCurrency, prsdt pDate, vbak_stwae pToCurrency,'bi_populate_afs_salesorder_fact'  fact_script_name
FROM VBAK_VBAP_VBEP;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pDate,pToCurrency,fact_script_name)
SELECT DISTINCT VBAP_WAERK pFromCurrency, VBAK_AUDAT pDate, vbak_stwae pToCurrency,'bi_populate_afs_salesorder_fact'  fact_script_name
FROM VBAK_VBAP_VBEP;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pDate,pToCurrency,pFromExchangeRate,fact_script_name)
SELECT DISTINCT co.Currency pFromCurrency, VBAK_AUDAT pDate, vbak_stwae pToCurrency,vbap_stcur pFromExchangeRate, 'bi_populate_afs_salesorder_fact'  fact_script_name
FROM VBAK_VBAP_VBEP, Dim_Company co,Dim_Plant pl
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_afs_sof;
create table tmp_getExchangeRate1_nodups_afs_sof
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_salesorder_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_salesorder_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_afs_sof;

drop table if exists tmp_getExchangeRate1_nodups_afs_sof;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');