/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

Drop table if exists tmp_globalcur;

Declare global temporary table tmp_globalcur(
	pGlobalCurrency)
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD'),varchar(3)) ON COMMIT PRESERVE ROWS;



INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, co.currency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ansidate(LOCAL_TIMESTAMP) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;



INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, a.VBAK_STWAE pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, co.currency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co, tmp_globalcur
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency, ansidate(LOCAL_TIMESTAMP) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co, tmp_globalcur
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, a.VBAK_STWAE pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

/* pFromExchangeRate vbap_stcur is not the correct rate for vbap_waerk --> vbak_stwae . This is still populating tmp_getexchangerate1, but not used anywhere in the fact script */


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,VBAK_AUDAT as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,VBAK_AUDAT as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;


/* For all from-to curr combinations, add rows with fromexchangerate = 0 as well */
INSERT INTO tmp_getExchangeRate1( pFromCurrency, pDate, pToCurrency, exchangeRate, pFromExchangeRate,fact_script_name)
SELECT pFromCurrency, pDate, pToCurrency, exchangeRate,0,'bi_populate_salesorder_fact'
FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

drop table if exists tmp_getExchangeRate1_nodups_sof;
create table tmp_getExchangeRate1_nodups_sof
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_sof;

drop table tmp_getExchangeRate1_nodups_sof;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
