/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
select 'processinglog',ifnull(max(processinglogid),0)
FROM processinglog;


/* ekko_ekpo_eket updates need to be handled here as this is used for exchange rate processing of purchasing_fact */

/* Start of ekko_ekpo_eket updates */

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_afs_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

 
INSERT INTO ekko_ekpo_eket
SELECT distinct * 
FROM ekko_ekpo_eket_mseg a
WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);
	
	
DELETE FROM ekko_ekpo_eket_mseg;

INSERT INTO ekko_ekpo_eket
SELECT distinct * 
FROM ekko_ekpo_eket_ekbe a
WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);

DELETE FROM ekko_ekpo_eket_ekbe;

UPDATE ekko_ekpo_eket
SET EKPO_BWTAR = IFNULL(EKPO_BWTAR,'Not Set'), EKPO_BUKRS = IFNULL(EKPO_BUKRS,EKKO_BUKRS);

INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_afs_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

/* End of ekko_ekpo_eket updates */

				 
								 
DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_purchasing_fact';

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT EKKO_WAERS as pFromCurrency, 'USD' as pToCurrency, 0 as pFromExchangeRate, EKKO_BEDAT as pDate, 
'bi_populate_afs_purchasing_fact' as fact_script_name
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT EKKO_WAERS as pFromCurrency, 'USD' as pToCurrency, 0 as pFromExchangeRate, ANSIDATE(LOCAL_TIMESTAMP) as pDate, 
'bi_populate_afs_purchasing_fact' as fact_script_name
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS;
 
 
UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_afs_purchasing_fact'; 

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT EKKO_WAERS as pFromCurrency, dc.Currency as pToCurrency, EKKO_WKURS as pFromExchangeRate, EKKO_BEDAT as pDate,
'bi_populate_afs_purchasing_fact' as fact_script_name
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS;


drop table if exists tmp_getExchangeRate1_nodups_pur_fact;
create table tmp_getExchangeRate1_nodups_pur_fact
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_purchasing_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_purchasing_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_pur_fact;

drop table tmp_getExchangeRate1_nodups_pur_fact;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
