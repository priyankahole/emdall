/* Standard proc starts here */

select timestamp(local_timestamp);


/* Query 1. Rows touched by Q1 should not be touched by other updates ( as it had a RETURN stmt ) */

UPDATE  tmp_getStdPrice
SET StandardPrice = 0,flag_upd = 'Q1'
WHERE PONumber is not null AND pUnitPrice = 0;

/* Q2 */



UPDATE tmp_getStdPrice
SET pUMREZ_UMREN = 1,flag_upd = 'Q2A'   --Indicate with Q2A so that the next query ( which was in CASE - ELSE in mysql, should not update the same rows )
WHERE vUMREZ = 0 AND vUMREN <> 0
AND flag_upd <> 'Q1';

UPDATE tmp_getStdPrice
SET pUMREZ_UMREN = NULL, flag_upd = 'Q2A'
WHERE vUMREN = 0
AND flag_upd <> 'Q1';


UPDATE tmp_getStdPrice
SET pUMREZ_UMREN = vUMREZ / vUMREN,flag_upd = 'Q2B'
WHERE vUMREZ <> 0
AND flag_upd NOT IN ( 'Q1', 'Q2A') ;    --As Q2A was inside Case, touch only rows not already updated by Q2A

/* There is no if condition on above query. Which means these rows should be processed by next queries and now the Q2 flags can be removed */

UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE flag_upd in ( 'Q2A','Q2B' );

/* Q3 MBEW_NO_BWTAR */

DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_getstdprice;
CREATE TABLE tmp_MBEW_NO_BWTAR_getstdprice
AS
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM MBEW_NO_BWTAR a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

INSERT INTO tmp_MBEW_NO_BWTAR_getstdprice
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM MBEW_NO_BWTAR a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

/* By default, Q3 updated all rows to 0 in mysql where no match was found */
UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE flag_upd = 'N';

UPDATE tmp_getStdPrice x
FROM tmp_MBEW_NO_BWTAR_getstdprice a
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
        flag_upd = 'Q3'
WHERE     a.MATNR = pMaterialNo
AND a.LFGJA = pFiYear
AND a.LFMON = pPeriod
AND a.BWKEY = pPlant
AND x.flag_upd = 'N';

/* Q4  MBEW */

DROP TABLE IF EXISTS tmp_MBEW_getstdprice;
CREATE TABLE tmp_MBEW_getstdprice
AS
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM MBEW a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

INSERT INTO tmp_MBEW_getstdprice
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM MBEW a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

select timestamp(local_timestamp);

/* By default, Q4 updated all rows to 0 in mysql where no match was found */
UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE StandardPrice < 0
AND flag_upd <> 'Q1';

UPDATE tmp_getStdPrice x
FROM tmp_MBEW_getstdprice a
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
        flag_upd = 'Q4'
WHERE     a.MATNR = pMaterialNo
AND a.LFGJA = pFiYear
AND a.LFMON = pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1';


/* Q5  MBEWH_NO_BWTAR */

DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_getstdprice;
CREATE TABLE tmp_MBEWH_NO_BWTAR_getstdprice
AS
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM MBEWH_NO_BWTAR a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

INSERT INTO tmp_MBEWH_NO_BWTAR_getstdprice
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM MBEWH_NO_BWTAR a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

/* By default, Q4 updated all rows to 0 in mysql where no match was found */
UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE StandardPrice < 0
AND flag_upd <> 'Q1';


UPDATE tmp_getStdPrice x
FROM tmp_MBEWH_NO_BWTAR_getstdprice a
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
        flag_upd = 'Q5'
WHERE     a.MATNR = pMaterialNo
AND a.LFGJA = pFiYear
AND a.LFMON = pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1';

/* Q6  MBEWH */
select timestamp(local_timestamp);

DROP TABLE IF EXISTS tmp_MBEWH_getstdprice;
CREATE TABLE tmp_MBEWH_getstdprice
AS
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM MBEWH a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

INSERT INTO tmp_MBEWH_getstdprice
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM MBEWH a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;


UPDATE tmp_getStdPrice
SET StandardPrice = 0  * pUMREZ_UMREN
WHERE StandardPrice < 0
AND flag_upd <> 'Q1';


UPDATE tmp_getStdPrice x
FROM tmp_MBEWH_getstdprice a
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
        flag_upd = 'Q6'
WHERE     a.MATNR = pMaterialNo
AND a.LFGJA = pFiYear
AND a.LFMON = pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1';



/* Q7 */


DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt;
CREATE TABLE tmp_MBEW_NO_BWTAR_alt
as
SELECT a.MATNR,a.BWKEY,(a.LFGJA * 100) + a.LFMON as LFGJA_LFMON,a.VPRSV,a.STPRS,a.VERPR,a.PEINH
FROM MBEW_NO_BWTAR a
WHERE ( a.VPRSV = 'S' AND a.STPRS > 0) or  (a.VPRSV = 'V' AND a.VERPR > 0) ;

UPDATE tmp_getStdPrice
SET pFiYear_pPeriod = (pFiYear * 100) + pPeriod;



DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_1;
CREATE TABLE tmp_MBEW_NO_BWTAR_alt_1
AS
SELECT a.MATNR,a.BWKEY,pFiYear,pPeriod,max(LFGJA_LFMON) as max_LFGJA_LFMON
FROM tmp_getStdPrice t,tmp_MBEW_NO_BWTAR_alt a
WHERE a.MATNR = pMaterialNo
AND a.BWKEY = pPlant
AND LFGJA_LFMON < pFiYear_pPeriod
AND t.StandardPrice <= 0
GROUP BY a.MATNR,a.BWKEY,pFiYear,pPeriod;


DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_2;
CREATE TABLE tmp_MBEW_NO_BWTAR_alt_2
AS
SELECT a.* ,b.pFiYear,b.pPeriod
FROM tmp_MBEW_NO_BWTAR_alt a,tmp_MBEW_NO_BWTAR_alt_1 b
WHERE a.MATNR = b.MATNR
AND a.BWKEY = b.BWKEY
AND a.LFGJA_LFMON = b.max_LFGJA_LFMON;

DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_3;
CREATE TABLE tmp_MBEW_NO_BWTAR_alt_3
AS
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM tmp_MBEW_NO_BWTAR_alt_2 a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

INSERT INTO tmp_MBEW_NO_BWTAR_alt_3
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM tmp_MBEW_NO_BWTAR_alt_2 a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

UPDATE tmp_getStdPrice
SET StandardPrice = 0  * pUMREZ_UMREN
WHERE StandardPrice <= 0;


UPDATE tmp_getStdPrice x
FROM tmp_MBEW_NO_BWTAR_alt_3 a
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
        flag_upd = 'Q7'
WHERE     a.MATNR = pMaterialNo
AND a.pFiYear = x.pFiYear
AND a.pPeriod = x.pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1';




/* Q8MBEW */

DROP TABLE IF EXISTS tmp_MBEW_alt;
CREATE TABLE tmp_MBEW_alt
as
SELECT a.MATNR,a.BWKEY,(a.LFGJA * 100) + a.LFMON as LFGJA_LFMON,a.VPRSV,a.STPRS,a.VERPR,a.PEINH
FROM MBEW a
WHERE ( a.VPRSV = 'S' AND a.STPRS > 0) or  (a.VPRSV = 'V' AND a.VERPR > 0) ;

UPDATE tmp_getStdPrice
SET pFiYear_pPeriod = (pFiYear * 100) + pPeriod;



DROP TABLE IF EXISTS tmp_MBEW_alt_1;
CREATE TABLE tmp_MBEW_alt_1
AS
SELECT a.MATNR,a.BWKEY,pFiYear,pPeriod,max(LFGJA_LFMON) as max_LFGJA_LFMON
FROM tmp_getStdPrice t,tmp_MBEW_alt a
WHERE a.MATNR = pMaterialNo
AND a.BWKEY = pPlant
AND LFGJA_LFMON < pFiYear_pPeriod
AND t.StandardPrice <= 0
GROUP BY a.MATNR,a.BWKEY,pFiYear,pPeriod;


DROP TABLE IF EXISTS tmp_MBEW_alt_2;
CREATE TABLE tmp_MBEW_alt_2
AS
SELECT a.* ,b.pFiYear,b.pPeriod
FROM tmp_MBEW_alt a,tmp_MBEW_alt_1 b
WHERE a.MATNR = b.MATNR
AND a.BWKEY = b.BWKEY
AND a.LFGJA_LFMON = b.max_LFGJA_LFMON;

DROP TABLE IF EXISTS tmp_MBEW_alt_3;
CREATE TABLE tmp_MBEW_alt_3
AS
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM tmp_MBEW_alt_2 a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

INSERT INTO tmp_MBEW_alt_3
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM tmp_MBEW_alt_2 a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

UPDATE tmp_getStdPrice
SET StandardPrice = 0  * pUMREZ_UMREN
WHERE StandardPrice <= 0;


UPDATE tmp_getStdPrice x
FROM tmp_MBEW_alt_3 a
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
        flag_upd = 'Q8'
WHERE     a.MATNR = pMaterialNo
AND a.pFiYear = x.pFiYear
AND a.pPeriod = x.pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1';


select timestamp(local_timestamp);


/* Q9 MBEWH_NO_BWTAR */

DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt;
CREATE TABLE tmp_MBEWH_NO_BWTAR_alt
as
SELECT a.MATNR,a.BWKEY,(a.LFGJA * 100) + a.LFMON as LFGJA_LFMON,a.VPRSV,a.STPRS,a.VERPR,a.PEINH
FROM MBEWH_NO_BWTAR a
WHERE ( a.VPRSV = 'S' AND a.STPRS > 0) or  (a.VPRSV = 'V' AND a.VERPR > 0) ;

UPDATE tmp_getStdPrice
SET pFiYear_pPeriod = (pFiYear * 100) + pPeriod;



DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_1;
CREATE TABLE tmp_MBEWH_NO_BWTAR_alt_1
AS
SELECT a.MATNR,a.BWKEY,pFiYear,pPeriod,max(LFGJA_LFMON) as max_LFGJA_LFMON
FROM tmp_getStdPrice t,tmp_MBEWH_NO_BWTAR_alt a
WHERE a.MATNR = pMaterialNo
AND a.BWKEY = pPlant
AND LFGJA_LFMON < pFiYear_pPeriod
AND t.StandardPrice <= 0
GROUP BY a.MATNR,a.BWKEY,pFiYear,pPeriod;


DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_2;
CREATE TABLE tmp_MBEWH_NO_BWTAR_alt_2
AS
SELECT a.* ,b.pFiYear,b.pPeriod
FROM tmp_MBEWH_NO_BWTAR_alt a,tmp_MBEWH_NO_BWTAR_alt_1 b
WHERE a.MATNR = b.MATNR
AND a.BWKEY = b.BWKEY
AND a.LFGJA_LFMON = b.max_LFGJA_LFMON;

DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_3;
CREATE TABLE tmp_MBEWH_NO_BWTAR_alt_3
AS
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM tmp_MBEWH_NO_BWTAR_alt_2 a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

INSERT INTO tmp_MBEWH_NO_BWTAR_alt_3
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM tmp_MBEWH_NO_BWTAR_alt_2 a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE StandardPrice < 0;


UPDATE tmp_getStdPrice x
FROM tmp_MBEWH_NO_BWTAR_alt_3 a
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
        flag_upd = 'Q9'
WHERE     a.MATNR = pMaterialNo
AND a.pFiYear = x.pFiYear
AND a.pPeriod = x.pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1';


/* Q10 MBEWH */


DROP TABLE IF EXISTS tmp_MBEWH_alt;
CREATE TABLE tmp_MBEWH_alt
as
SELECT a.MATNR,a.BWKEY,(a.LFGJA * 100) + a.LFMON as LFGJA_LFMON,a.VPRSV,a.STPRS,a.VERPR,a.PEINH
FROM MBEWH a
WHERE ( a.VPRSV = 'S' AND a.STPRS > 0) or  (a.VPRSV = 'V' AND a.VERPR > 0) ;

UPDATE tmp_getStdPrice
SET pFiYear_pPeriod = (pFiYear * 100) + pPeriod;



DROP TABLE IF EXISTS tmp_MBEWH_alt_1;
CREATE TABLE tmp_MBEWH_alt_1
AS
SELECT a.MATNR,a.BWKEY,pFiYear,pPeriod,max(LFGJA_LFMON) as max_LFGJA_LFMON
FROM tmp_getStdPrice t,tmp_MBEWH_alt a
WHERE a.MATNR = pMaterialNo
AND a.BWKEY = pPlant
AND LFGJA_LFMON < pFiYear_pPeriod
AND t.StandardPrice <= 0
GROUP BY a.MATNR,a.BWKEY,pFiYear,pPeriod;


DROP TABLE IF EXISTS tmp_MBEWH_alt_2;
CREATE TABLE tmp_MBEWH_alt_2
AS
SELECT a.* ,b.pFiYear,b.pPeriod
FROM tmp_MBEWH_alt a,tmp_MBEWH_alt_1 b
WHERE a.MATNR = b.MATNR
AND a.BWKEY = b.BWKEY
AND a.LFGJA_LFMON = b.max_LFGJA_LFMON;

DROP TABLE IF EXISTS tmp_MBEWH_alt_3;
CREATE TABLE tmp_MBEWH_alt_3
AS
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM tmp_MBEWH_alt_2 a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

INSERT INTO tmp_MBEWH_alt_3
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM tmp_MBEWH_alt_2 a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE StandardPrice < 0;


UPDATE tmp_getStdPrice x
FROM tmp_MBEWH_alt_3 a
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
        flag_upd = 'Q9'
WHERE     a.MATNR = pMaterialNo
AND a.pFiYear = x.pFiYear
AND a.pPeriod = x.pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1';


/* Q11 MBEWH */
/* After the last query commenting here as can't put that onlast line */
/* Q11B  This calls another function which should populate this table tmp_Funct_Fiscal_Year */

/* Break up getstdprice_std into 2 parts so that funct_fiscal_year can be called */
/* Call vw_funct_fiscal_year.custom.getStdPrice.sql then vw_funct_fiscal_year.standard.sql then part 2 of this proc */

UPDATE tmp_getStdPrice
SET pprevFiYear = pFiYear - 1,pprevPeriod = 12,flag_upd = 'Q11A'
WHERE PONumber is null AND StandardPrice <= 0
AND pPeriod = 1;

UPDATE tmp_getStdPrice
SET pprevFiYear = pFiYear,
        pprevPeriod = pPeriod - 1,
        flag_upd = 'Q12B'
WHERE PONumber is null AND StandardPrice <= 0
AND IFNULL(pPeriod,-1) <> 1 ;

select timestamp(local_timestamp);

DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_getstdprice;
DROP TABLE IF EXISTS tmp_MBEW_getstdprice;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_getstdprice;
DROP TABLE IF EXISTS tmp_MBEWH_getstdprice;
DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt;
DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_1;
DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_2;
DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_3;
DROP TABLE IF EXISTS tmp_MBEW_alt;
DROP TABLE IF EXISTS tmp_MBEW_alt_1;
DROP TABLE IF EXISTS tmp_MBEW_alt_2;
DROP TABLE IF EXISTS tmp_MBEW_alt_3;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_1;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_2;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_3;
DROP TABLE IF EXISTS tmp_MBEWH_alt;
DROP TABLE IF EXISTS tmp_MBEWH_alt_1;
DROP TABLE IF EXISTS tmp_MBEWH_alt_2;
DROP TABLE IF EXISTS tmp_MBEWH_alt_3;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_getStdPrice;

select timestamp(local_timestamp);


