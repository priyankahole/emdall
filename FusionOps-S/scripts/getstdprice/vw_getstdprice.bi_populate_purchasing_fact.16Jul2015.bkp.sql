

/**************************************************************************************************************/
/*   Script         : vw_getstdprice.bi_populate_purchasing_fact.sql	 */
/*   Author         : Lokesh */
/*   Created On     : 28 Jul 2013 */
/*   Description    : Function to populate tmp_getStdPrice for getstdprice  */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   28 Jul 2013      Lokesh    1.0               New script                          */
/******************************************************************************************************************/

/* Custom proc for  starts here bi_populate_purchasing_fact*/

/* Run this after the end of part1 and before the start of part2 of bi_populate_purchasing_fact  */


/* Populate tmp_getStdPrice  This will change for each proc where this function is required*/

DROP TABLE IF EXISTS tmp_getStdPrice_q2_pur_fact;

CREATE TABLE tmp_getStdPrice_q2_pur_fact 
as
SELECT * FROM tmp_getStdPrice
WHERE 1=2;

call vectorwise(combine 'tmp_getStdPrice_q2_pur_fact');

drop table if exists ekko_ekpo_eket_tt980;

create table ekko_ekpo_eket_tt980 as select * from ekko_ekpo_eket;

INSERT INTO tmp_getStdPrice_q2_pur_fact
(
 pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
SELECT DISTINCT
EKPO_BUKRS,
EKPO_WERKS,
EKPO_MATNR,
dt.FinancialYear,
dt.FinancialMonthNumber, 
EKPO_UMREZ, 
EKPO_UMREN, 
fp.dd_DocumentNo,
ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
* ct_ExchangeRate
* (EKPO_BPUMZ/CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END)),0),
'bi_populate_purchasing_fact'
FROM fact_purchase fp,
         ekko_ekpo_eket,
         dim_date dt
 WHERE     fp.dd_DocumentNo = EKPO_EBELN
         AND fp.dd_DocumentItemNo = EKPO_EBELP
         AND fp.dd_ScheduleNo = EKET_ETENR
         AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket_tt980 x
                             where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
         AND dt.CompanyCode = EKPO_BUKRS
         AND fp.amt_StdUnitPrice = 0;		 
	
INSERT INTO tmp_getStdPrice
SELECT DISTINCT * FROM tmp_getStdPrice_q2_pur_fact;

drop table if exists ekko_ekpo_eket_tt980;

DROP TABLE IF EXISTS tmp_getStdPrice_q2_pur_fact;					  


UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE fact_script_name = 'bi_populate_purchasing_fact';


