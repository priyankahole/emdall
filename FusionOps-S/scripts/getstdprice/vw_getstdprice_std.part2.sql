

/* Call this part ( part 2 ) after vw_funct_fiscal_year.custom.getStdPrice.sql and vw_funct_fiscal_year.standard.sql */

UPDATE tmp_getStdPrice t
FROM tmp_Funct_Fiscal_Year z
SET pprevDates = z.pReturn 
WHERE z.pCompanyCode = t.pCompanyCode
and z.FiscalYear = t.pprevFiYear
and z.Period = t.pprevPeriod 
AND PONumber is null AND StandardPrice <= 0;

UPDATE tmp_getStdPrice
SET pprevFromDate = cast(substr(pprevDates, 1, 10) AS date)
WHERE PONumber is null AND StandardPrice <= 0;
UPDATE tmp_getStdPrice
SET pprevToDate = cast(substr(pprevDates, 12, 10) AS date)
WHERE PONumber is null AND StandardPrice <= 0;


DROP TABLE IF EXISTS tmp_getStdPrice_FP1;
CREATE TABLE tmp_getStdPrice_FP1
AS
SELECT pprevFromDate,pprevToDate,pMaterialNo,pPlant,dd_DocumentNo,DateValue,
cast(b.amt_UnitPrice as decimal(18,5)) amt_UnitPrice ,
row_number() over (ORDER BY dd_DocumentNo desc,DateValue desc) row_num
FROM tmp_getStdPrice,
fact_purchase b inner join dim_part p on b.Dim_Partid = p.Dim_Partid
inner join dim_date dt on dt.Dim_Dateid = b.Dim_DateidCreate
inner join Dim_PurchaseMisc pmisc on b.Dim_PurchaseMiscid = pmisc.Dim_PurchaseMiscid
WHERE dt.DateValue BETWEEN ( pprevFromDate - INTERVAL '2' MONTH )
AND pprevToDate
AND p.PartNumber = pMaterialNo
AND b.amt_UnitPrice > 0
AND p.Plant = pPlant
AND pmisc.ItemReturn = 'Not Set'
AND PONumber is null AND StandardPrice <= 0
ORDER BY dd_DocumentNo desc,DateValue desc offset 0 ;

DELETE FROM tmp_getStdPrice_FP1  x  
WHERE EXISTS ( SELECT 1 FROM tmp_getStdPrice_FP1 y
WHERE x.pprevFromDate = y.pprevFromDate
AND x.pprevToDate = y.pprevToDate
AND x.pMaterialNo = y.pMaterialNo
AND x.pPlant = y.pPlant
AND x.row_num > y.row_num );


UPDATE tmp_getStdPrice x
FROM tmp_getStdPrice_FP1 y
SET StandardPrice = y.amt_UnitPrice
WHERE x.pprevFromDate = y.pprevFromDate
AND x.pprevToDate = y.pprevToDate
AND x.pMaterialNo = y.pMaterialNo
AND x.pPlant = y.pPlant
AND x.PONumber is null AND x.StandardPrice <= 0;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_getStdPrice;