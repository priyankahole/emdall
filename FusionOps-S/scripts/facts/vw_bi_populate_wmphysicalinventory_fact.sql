

drop table if exists fact_wmphysicalinventory_tmp;

CREATE TABLE fact_wmphysicalinventory_tmp 
AS 
select * from fact_wmphysicalinventory where 1 = 2;

/*initialize NUMBER_FOUNTAIN*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_wmphysicalinventory';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_wmphysicalinventory',ifnull(max(fact_wmphysicalinventoryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_wmphysicalinventory;



INSERT INTO fact_wmphysicalinventory_tmp(fact_wmphysicalinventoryid,
						dd_physicalinventorydocno, 
						dd_physicalinventorydocitemno,
						dd_physicalinventorydocyear,
						dim_plantid,
						dim_storagelocationid,
						dd_countedby,
						dd_adjustpostby,
						dd_materialdocno,
						dd_materialdocyear,
						ct_countqty,
						ct_bookqty,
						ct_adjustmentqty,
						dim_dateidcount,
						dim_dateidadjustment,
						dim_dateiddocument,
						  dim_partid,
						  dd_batchnumber,
						  dim_specialstockid,
						  dim_wmstocktypeid,
						  dd_sddocno,
						  dd_sddocitemno,
						  dd_sdscheduleno,
						  dim_vendorid,
						  dim_customerid,
						  dd_productionstoragebin,
						  dd_physinventref,
						  dim_baseunitofmeasureid,
						  ct_entryqty,
						  dim_entryunitofmeasureid,
						  dd_materialdocitemno,
						  dd_recountdocno,
						  amt_difference,
						  dim_diffcurrencyid,
						  dd_physinvindicator,
						  dd_wmbno,
						  amt_salesvalueincvat,
						  amt_salesvalue,
						  amt_bookvalue,
						  amt_salesvaluewovat,
						  amt_salesvaluediffincvat,
						  amt_salesvaluediffwovat,
						  amt_physinvvalue,
						  amt_bookqtyvalue,
						  amt_differencevalue,
						  dd_invdiffreason,
						  dim_crossplantpartid,
						  dim_dateidcountopeningtime,
						  dd_counttime,
						  dim_dateidfreeze,
						  dd_freezetime,
						  amt_retailchangevalue,
						  Dim_TransactionEventTypeid,
						  dim_dateidplannedcount,
						  dim_dateidlastcount,
						  dim_dateidposting,
						  dd_countstatus,
						  dd_adjustmentstatus,
						  dd_deletestatus,
						  dim_wmgroupingtypeid,
						  dd_groupingcriterion,
						  dd_physinventoryno,
						  dd_calcstatus,
						  dim_wmphysicalinvmiscellaneousid)
	   SELECT 	((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN 
                  WHERE table_name = 'fact_wmphysicalinventory') 
                  + row_number() over () ) fact_wmphysicalinventoryid ,
				ifnull(st.ISEG_IBLNR, 'Not Set') as dd_physicalinventorydocno,
				ifnull(st.ISEG_ZEILI, 0) as dd_physicalinventorydocitemno,
				st.ISEG_GJAHR as dd_physicalinventorydocyear,
				ifnull(pl.Dim_Plantid, 1),
				ifnull((SELECT sl.dim_storagelocationid
						  FROM dim_storagelocation sl
						 WHERE sl.plant = ifnull(st.ISEG_WERKS, 'Not Set')
							   AND sl.locationcode = st.ISEG_LGORT),
					   1) as dim_storagelocationid,
				ifnull(st.ISEG_USNAZ, 'Not Set') as dd_countedby,
				ifnull(st.ISEG_USNAD, 'Not Set') as dd_adjustpostby,
				ifnull(st.ISEG_MBLNR, 'Not Set') as dd_materialdocno,
				ifnull(st.ISEG_MJAHR, 0) as dd_materialdocyear,
				ifnull(st.ISEG_MENGE, 0) as ct_countqty,
				ifnull(st.ISEG_BUCHM, 0) as ct_bookqty,
				ifnull(st.ISEG_WSTI_POSM, 0) as ct_adjustmentqty,
				ifnull((SELECT cdt.dim_dateid
						  FROM dim_date cdt
						 WHERE cdt.DateValue = st.ISEG_ZLDAT
							   AND cdt.CompanyCode = pl.CompanyCode), 1) as dim_dateidcount,
				ifnull((SELECT cdt.dim_dateid
						  FROM dim_date cdt
						 WHERE cdt.DateValue = st.ISEG_BUDAT
							   AND cdt.CompanyCode = pl.CompanyCode), 1) as dim_dateidadjustment,
				ifnull((SELECT cdt.dim_dateid
						  FROM dim_date cdt
						 WHERE cdt.DateValue = st.IKPF_BLDAT
							   AND cdt.CompanyCode = pl.CompanyCode), 1) as dim_dateiddocument,
				ifnull((SELECT dp.dim_partid
									   FROM dim_part dp
									  WHERE dp.PartNumber = st.ISEG_MATNR AND dp.plant = ifnull(st.ISEG_WERKS, 'Not Set')),
									1) as dim_partid,
				ifnull(st.ISEG_CHARG,'Not Set') as dd_batchnumber,
				ifnull((SELECT ss.dim_specialstockid
						  FROM dim_specialstock ss
						 WHERE ss.specialstockindicator = st.ISEG_SOBKZ),
						  1) as dim_specialstockid,
				ifnull((SELECT sty.dim_wmstocktypeid
						  FROM dim_wmstocktype sty
						 WHERE sty.stocktype = st.ISEG_BSTAR),
						  1) as dim_wmstocktypeid,
				ifnull(st.ISEG_KDAUF,'Not Set') as dd_sddocno,
				ifnull(st.ISEG_KDPOS,0) as dd_sddocitemno,
				ifnull(st.ISEG_KDEIN,0) as dd_sdscheduleno,
				ifnull((select v.Dim_Vendorid from dim_vendor v
									where v.VendorNumber = st.ISEG_LIFNR),1) as dim_vendorid,
				ifnull((select c.Dim_Customerid from dim_customer c
									where c.CustomerNumber = st.ISEG_KUNNR),1) as dim_customerid,
				ifnull(st.ISEG_PLPLA,'Not Set') as dd_productionstoragebin,
				ifnull(st.ISEG_XBLNI,'Not Set') as dd_physinventref,
				ifnull((SELECT bum.dim_unitofmeasureid
						  FROM dim_unitofmeasure bum
						 WHERE bum.uom = st.ISEG_MEINS),
						  1) as dim_baseunitofmeasureid,
				ifnull(st.ISEG_ERFMG, 0) as ct_entryqty,
				ifnull((SELECT eum.dim_unitofmeasureid
						  FROM dim_unitofmeasure eum
						 WHERE eum.uom = st.ISEG_ERFME),
						  1) as dim_entryunitofmeasureid,
				ifnull(st.ISEG_ZEILE, 0) as dd_materialdocitemno,
				ifnull(st.ISEG_NBLNR,'Not Set') as dd_recountdocno,
				ifnull(st.ISEG_DMBTR, 0) as amt_difference,
				ifnull((SELECT curr.dim_currencyid
						  FROM dim_currency curr
						 WHERE curr.currencycode = st.ISEG_WAERS),
						  1) as dim_diffcurrencyid,
				ifnull(st.ISEG_ABCIN, 'Not Set') as dd_physinvindicator,
				ifnull(st.ISEG_PS_PSP_PNR,'Not Set') as dd_wmbno,
				ifnull(ISEG_VKWRT, 0) as amt_salesvalueincvat,
				ifnull(ISEG_EXVKW, 0) as amt_salesvalue,
				ifnull(ISEG_BUCHW, 0) as amt_bookvalue,
				ifnull(ISEG_VKWRA, 0) as amt_salesvaluewovat,
				ifnull(ISEG_VKMZL, 0) as amt_salesvaluediffincvat,
				ifnull(ISEG_VKNZL, 0) as amt_salesvaluediffwovat,
				ifnull(ISEG_WRTZL, 0) as amt_physinvvalue,
				ifnull(ISEG_WRTBM, 0) as amt_bookqtyvalue,
				ifnull(ISEG_DIWZL, 0) as amt_differencevalue,
				ifnull(st.ISEG_GRUND, 'Not Set') as dd_invdiffreason,
				ifnull((SELECT dpcm.dim_partid
									   FROM dim_part dpcm
									  WHERE dpcm.PartNumber = st.ISEG_SAMAT AND dpcm.plant = ifnull(st.ISEG_WERKS, 'Not Set')),
									1) as dim_crossplantpartid,

				ifnull((SELECT cotdt.dim_dateid
									  FROM dim_date cotdt
									 WHERE cotdt.DateValue = st.ISEG_WSTI_COUNTDATE
										   AND cotdt.CompanyCode = pl.CompanyCode),
								   1) as dim_dateidcountopeningtime,
				TO_TIME(st.ISEG_WSTI_COUNTTIME,'HH24MISS') as dd_counttime,
				ifnull((SELECT fdt.dim_dateid
									  FROM dim_date fdt
									 WHERE fdt.DateValue = st.ISEG_WSTI_FREEZEDATE
										   AND fdt.CompanyCode = pl.CompanyCode),
								   1) as dim_dateidfreeze,
				TO_TIME(st.ISEG_WSTI_FREEZETIME,'HH24MISS') as dd_freezetime,
				ifnull(st.ISEG_WSTI_POSW, 0) as amt_retailchangevalue,
				ifnull((SELECT tet.Dim_TransactionEventTypeid
									   FROM dim_transactioneventtype tet
									  WHERE tet.TransactionEventTypeCode = st.IKPF_VGART),
									1) as Dim_TransactionEventTypeid,
				ifnull((SELECT pcdt.dim_dateid
									  FROM dim_date pcdt
									 WHERE pcdt.DateValue = st.IKPF_GIDAT
										   AND pcdt.CompanyCode = pl.CompanyCode),
								   1) as dim_dateidplannedcount,
				ifnull((SELECT lcdt.dim_dateid
									  FROM dim_date lcdt
									 WHERE lcdt.DateValue = st.IKPF_ZLDAT
										   AND lcdt.CompanyCode = pl.CompanyCode),
								   1) as dim_dateidlastcount,
				ifnull((SELECT pdt.dim_dateid
									  FROM dim_date pdt
									 WHERE pdt.DateValue = st.IKPF_BUDAT
										   AND pdt.CompanyCode = pl.CompanyCode),
								   1) as dim_dateidposting,
				ifnull(st.IKPF_ZSTAT,'Not Set') as dd_countstatus,
				ifnull(st.IKPF_DSTAT,'Not Set') as dd_adjustmentstatus,
				ifnull(st.IKPF_LSTAT,'Not Set') as dd_deletestatus,
				ifnull((SELECT wmgt.dim_wmgroupingtypeid
						  FROM dim_wmgroupingtype wmgt
						 WHERE wmgt.WMGroupingType= st.IKPF_KEORD),
					   1) as dim_wmgroupingtypeid,
				ifnull(st.IKPF_ORDNG,'Not Set') as dd_groupingcriterion,
				ifnull(st.IKPF_INVNU,'Not Set') as dd_physinventoryno,
				ifnull(st.IKPF_WSTI_BSTAT,'Not Set') as dd_calcstatus,
				ifnull((select wmpimisc.dim_wmphysicalinvmiscellaneousid 
					    from dim_wmphysicalinvmiscellaneous wmpimisc 
					    where     wmpimisc.ItemCounted = ifnull(st.ISEG_XZAEL, 'Not Set') 
						      AND wmpimisc.DifferencePosted = ifnull(st.ISEG_XDIFF, 'Not Set')
					    	  AND wmpimisc.Recount = ifnull(st.ISEG_XNZAE, 'Not Set')
							  AND wmpimisc.ItemDeleted = ifnull(st.ISEG_XLOEK, 'Not Set')
							  AND wmpimisc.AlternativeUnit = ifnull(st.ISEG_XAMEI, 'Not Set')
							  AND wmpimisc.ZeroCount = ifnull(st.ISEG_XNULL, 'Not Set')
							  AND wmpimisc.InventoryValueOnlyMaterial = ifnull(st.ISEG_KWART, 'Not Set')
							  AND wmpimisc.PhysInvDiffDistribution = ifnull(st.ISEG_XDISPATCH, 'Not Set')
							  AND wmpimisc.BookInventoryCalculated = ifnull(st.ISEG_WSTI_XCALC, 'Not Set')
							  AND wmpimisc.PostingBlock = ifnull(st.IKPF_SPERR, 'Not Set')
							  AND wmpimisc.FreezeBookInvntory = ifnull(st.IKPF_XBUFI, 'Not Set')),1) 
							      as dim_wmphysicalinvmiscellaneousid
		 FROM IKPF_ISEG st,
			  dim_plant pl
		WHERE ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
			  AND NOT EXISTS
					(SELECT 1
					   FROM fact_wmphysicalinventory wmphi
					  WHERE wmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
							AND wmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
							AND wmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR);
							

							
UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET fwmphi.dim_plantid = ifnull(pl.Dim_Plantid, 1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.dim_plantid <> pl.Dim_Plantid;


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_storagelocation sl
SET    fwmphi.dim_storagelocationid = ifnull(sl.dim_storagelocationid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND sl.plant = st.ISEG_WERKS
AND sl.locationcode = st.ISEG_LGORT
AND fwmphi.dim_storagelocationid <> ifnull(sl.dim_storagelocationid,1);
	
UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_countedby = ifnull(st.ISEG_USNAZ, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_countedby <> ifnull(st.ISEG_USNAZ, 'Not Set');
						
	
UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_adjustpostby = ifnull(st.ISEG_USNAD, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_adjustpostby <> ifnull(st.ISEG_USNAD, 'Not Set');


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_materialdocno = ifnull(st.ISEG_MBLNR, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_materialdocno <> ifnull(st.ISEG_MBLNR, 'Not Set');

	
UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_materialdocyear = ifnull(st.ISEG_MJAHR, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_materialdocyear <> ifnull(st.ISEG_MJAHR, 0);
	
	
UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.ct_countqty = ifnull(st.ISEG_MENGE, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND fwmphi.ct_countqty <> st.ISEG_MENGE;				

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.ct_bookqty = ifnull(st.ISEG_BUCHM, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.ct_bookqty <> st.ISEG_BUCHM;		

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.ct_adjustmentqty = ifnull(st.ISEG_WSTI_POSM, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.ct_adjustmentqty <> st.ISEG_WSTI_POSM;

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date cdt
SET    fwmphi.dim_dateidcount = ifnull(cdt.dim_dateid, 1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND cdt.DateValue = st.ISEG_ZLDAT
AND cdt.CompanyCode = pl.CompanyCode
AND fwmphi.dim_dateidcount <> ifnull(cdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date cdt
SET    fwmphi.dim_dateidadjustment = ifnull(cdt.dim_dateid, 1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND cdt.DateValue = st.ISEG_BUDAT
AND cdt.CompanyCode = pl.CompanyCode
AND fwmphi.dim_dateidadjustment <> ifnull(cdt.dim_dateid, 1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date cdt
SET    fwmphi.dim_dateiddocument = ifnull(cdt.dim_dateid, 1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND cdt.DateValue = st.IKPF_BLDAT
AND cdt.CompanyCode = pl.CompanyCode
AND fwmphi.dim_dateiddocument <> ifnull(cdt.dim_dateid, 1);


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_part dp
SET    fwmphi.dim_partid = ifnull(dp.dim_partid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND dp.PartNumber = st.ISEG_MATNR
AND dp.plant = st.ISEG_WERKS
AND fwmphi.dim_partid <> ifnull(dp.dim_partid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_batchnumber = ifnull(st.ISEG_CHARG,'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode		
AND fwmphi.dd_batchnumber <> ifnull(st.ISEG_CHARG,'Not Set');


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_specialstock ss
SET    fwmphi.dim_specialstockid = ifnull(ss.dim_specialstockid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND ss.specialstockindicator = st.ISEG_SOBKZ
AND fwmphi.dim_specialstockid <> ifnull(ss.dim_specialstockid,1);


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_wmstocktype sty
SET    fwmphi.dim_wmstocktypeid = ifnull(sty.dim_wmstocktypeid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND sty.stocktype = st.ISEG_BSTAR
AND fwmphi.dim_wmstocktypeid <> ifnull(sty.dim_wmstocktypeid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_sddocno = ifnull(st.ISEG_KDAUF,'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_sddocno <> ifnull(st.ISEG_KDAUF,'Not Set');

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_sddocitemno = ifnull(st.ISEG_KDPOS,0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_sddocitemno <> ifnull(st.ISEG_KDPOS,0);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_sdscheduleno = ifnull(st.ISEG_KDEIN,0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_sdscheduleno <> ifnull(st.ISEG_KDEIN,0);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_vendor v
SET    fwmphi.dim_vendorid = ifnull(v.Dim_Vendorid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND v.VendorNumber = st.ISEG_LIFNR
AND fwmphi.dim_vendorid <> ifnull(v.Dim_Vendorid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_customer c
SET    fwmphi.dim_customerid = ifnull(c.Dim_Customerid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND c.CustomerNumber = st.ISEG_KUNNR
AND fwmphi.dim_customerid <> ifnull(c.Dim_Customerid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_productionstoragebin = ifnull(ISEG_PLPLA,'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_productionstoragebin <> ifnull(ISEG_PLPLA,'Not Set');			

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.dd_physinventref = ifnull(ISEG_XBLNI,'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_physinventref <> ifnull(ISEG_XBLNI,'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_unitofmeasure bum
SET    fwmphi.dim_baseunitofmeasureid = ifnull(bum.dim_unitofmeasureid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND bum.uom = st.ISEG_MEINS
AND fwmphi.dim_baseunitofmeasureid <> ifnull(bum.dim_unitofmeasureid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.ct_entryqty = st.ISEG_ERFMG
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.ct_entryqty <> st.ISEG_ERFMG;						

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_unitofmeasure eum
SET    fwmphi.dim_entryunitofmeasureid = ifnull(eum.dim_unitofmeasureid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND eum.uom = st.ISEG_ERFME
AND fwmphi.dim_entryunitofmeasureid <> ifnull(eum.dim_unitofmeasureid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.dd_materialdocitemno = ifnull(st.ISEG_ZEILE,0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_materialdocitemno <> ifnull(st.ISEG_ZEILE,0);		

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.dd_materialdocitemno = ifnull(st.ISEG_ZEILE,0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_materialdocitemno <> ifnull(st.ISEG_ZEILE,0);		

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_recountdocno = ifnull(st.ISEG_NBLNR,'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_recountdocno <> ifnull(st.ISEG_NBLNR,'Not Set');			

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl	 
SET    fwmphi.amt_difference = ifnull(st.ISEG_DMBTR, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_difference <> ifnull(st.ISEG_DMBTR, 0);							
	
UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_currency curr
SET    fwmphi.dim_diffcurrencyid = ifnull(curr.dim_currencyid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND curr.currencycode = st.ISEG_WAERS
AND fwmphi.dim_diffcurrencyid <> ifnull(curr.dim_currencyid,1);


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.dd_physinvindicator = ifnull(st.ISEG_ABCIN, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_physinvindicator <> ifnull(st.ISEG_ABCIN, 'Not Set');			

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_wmbno = ifnull(st.ISEG_PS_PSP_PNR,'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_wmbno <> ifnull(st.ISEG_PS_PSP_PNR,'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.amt_salesvalueincvat = ifnull(st.ISEG_VKWRT, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvalueincvat <> ifnull(st.ISEG_VKWRT, 0);		


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.amt_salesvalue = ifnull(st.ISEG_EXVKW, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvalue <> ifnull(st.ISEG_EXVKW, 0);		
							
UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.amt_bookvalue = ifnull(st.ISEG_BUCHW, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_bookvalue <> ifnull(st.ISEG_BUCHW, 0);	

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.amt_salesvaluewovat = ifnull(st.ISEG_VKWRA, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvaluewovat <> ifnull(st.ISEG_VKWRA, 0);	

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.amt_salesvaluediffincvat = ifnull(st.ISEG_VKMZL, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvaluediffincvat <> ifnull(st.ISEG_VKMZL, 0);			

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl	 
SET    fwmphi.amt_salesvaluediffwovat = ifnull(st.ISEG_VKNZL, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvaluediffwovat <> ifnull(st.ISEG_VKNZL, 0);		
		
UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.amt_physinvvalue = ifnull(st.ISEG_WRTZL, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_physinvvalue <> ifnull(st.ISEG_WRTZL, 0);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.amt_bookqtyvalue = ifnull(st.ISEG_WRTBM, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.amt_bookqtyvalue <> ifnull(st.ISEG_WRTBM, 0);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.amt_differencevalue = ifnull(st.ISEG_DIWZL, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.amt_differencevalue <> ifnull(st.ISEG_DIWZL, 0);						

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_part dpcm
SET    fwmphi.dim_crossplantpartid = ifnull(dpcm.dim_partid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND dpcm.PartNumber = st.ISEG_SAMAT
AND dpcm.plant = st.ISEG_WERKS
AND fwmphi.dim_crossplantpartid <> ifnull(dpcm.dim_partid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.dd_invdiffreason = ifnull(st.ISEG_GRUND, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = st.ISEG_ZEILI
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.dd_invdiffreason <> ifnull(st.ISEG_GRUND, 'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date cotdt
SET    fwmphi.dim_dateidcountopeningtime = ifnull(cotdt.dim_dateid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode		
AND cotdt.DateValue = st.ISEG_WSTI_COUNTDATE
AND cotdt.CompanyCode = pl.CompanyCode
AND fwmphi.dim_dateidcountopeningtime <> ifnull(cotdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_counttime = TO_TIME(st.ISEG_WSTI_COUNTTIME,'HH24MISS')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_counttime <> TO_TIME(st.ISEG_WSTI_COUNTTIME,'HH24MISS');


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date fdt
SET    fwmphi.dim_dateidfreeze = ifnull(fdt.dim_dateid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = st.ISEG_ZEILI
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode			
AND fdt.DateValue = st.ISEG_WSTI_FREEZEDATE
AND fdt.CompanyCode = pl.CompanyCode
AND fwmphi.dim_dateidfreeze <> ifnull(fdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.dd_freezetime = TO_TIME(st.ISEG_WSTI_FREEZETIME,'HH24MISS')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_freezetime <> TO_TIME(st.ISEG_WSTI_FREEZETIME,'HH24MISS');	
					

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.amt_retailchangevalue = ifnull(ISEG_WSTI_POSW, 0)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode				
AND fwmphi.amt_retailchangevalue <> ifnull(ISEG_WSTI_POSW, 0);	

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_transactioneventtype tet
SET    fwmphi.Dim_TransactionEventTypeid = ifnull(tet.Dim_TransactionEventTypeid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode					
AND tet.TransactionEventTypeCode = st.IKPF_VGART
AND fwmphi.Dim_TransactionEventTypeid <> ifnull(tet.Dim_TransactionEventTypeid,1);


UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date pcdt
SET    fwmphi.dim_dateidplannedcount = ifnull(pcdt.dim_dateid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND pcdt.DateValue = st.IKPF_GIDAT
AND pcdt.CompanyCode = pl.CompanyCode
AND fwmphi.dim_dateidplannedcount <> ifnull(pcdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date lcdt
SET    fwmphi.dim_dateidlastcount = ifnull(lcdt.dim_dateid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode			
AND lcdt.DateValue = st.IKPF_ZLDAT
AND lcdt.CompanyCode = pl.CompanyCode
AND fwmphi.dim_dateidlastcount <> ifnull(lcdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date pdt
SET    fwmphi.dim_dateidposting = ifnull(pdt.dim_dateid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND pdt.DateValue = st.IKPF_BUDAT
AND pdt.CompanyCode = pl.CompanyCode
AND fwmphi.dim_dateidposting <> ifnull(pdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_countstatus = ifnull(st.IKPF_ZSTAT, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode			
AND fwmphi.dd_countstatus <> ifnull(st.IKPF_ZSTAT, 'Not Set');		

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_adjustmentstatus = ifnull(st.IKPF_DSTAT, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_adjustmentstatus <> ifnull(st.IKPF_DSTAT, 'Not Set');

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl 
SET    fwmphi.dd_deletestatus = ifnull(st.IKPF_LSTAT, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_deletestatus <> ifnull(st.IKPF_LSTAT, 'Not Set');		

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_wmgroupingtype wmgt
SET    fwmphi.dim_wmgroupingtypeid = ifnull(wmgt.dim_wmgroupingtypeid,1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND wmgt.WMGroupingType= st.IKPF_KEORD
AND fwmphi.dim_wmgroupingtypeid <> ifnull(wmgt.dim_wmgroupingtypeid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_groupingcriterion = ifnull(st.IKPF_ORDNG,'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.dd_groupingcriterion <> ifnull(st.IKPF_ORDNG,'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_physinventoryno = ifnull(st.IKPF_INVNU,'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_physinventoryno <> ifnull(st.IKPF_INVNU,'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl
SET    fwmphi.dd_calcstatus = ifnull(st.IKPF_WSTI_BSTAT, 'Not Set')
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_calcstatus <> ifnull(st.IKPF_WSTI_BSTAT, 'Not Set');						

UPDATE fact_wmphysicalinventory_tmp fwmphi
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_wmphysicalinvmiscellaneous wmpimisc	 
SET    fwmphi.dim_wmphysicalinvmiscellaneousid = ifnull(wmpimisc.dim_wmphysicalinvmiscellaneousid, 1)
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND wmpimisc.ItemCounted = ifnull(st.ISEG_XZAEL, 'Not Set')
AND wmpimisc.DifferencePosted = ifnull(st.ISEG_XDIFF, 'Not Set')
AND wmpimisc.Recount = ifnull(st.ISEG_XNZAE, 'Not Set')
AND wmpimisc.ItemDeleted = ifnull(st.ISEG_XLOEK, 'Not Set')
AND wmpimisc.AlternativeUnit = ifnull(st.ISEG_XAMEI, 'Not Set')
AND wmpimisc.ZeroCount = ifnull(st.ISEG_XNULL, 'Not Set')
AND wmpimisc.InventoryValueOnlyMaterial = ifnull(st.ISEG_KWART, 'Not Set')
AND wmpimisc.PhysInvDiffDistribution = ifnull(st.ISEG_XDISPATCH, 'Not Set')
AND wmpimisc.BookInventoryCalculated = ifnull(st.ISEG_WSTI_XCALC, 'Not Set')
AND wmpimisc.PostingBlock = ifnull(st.IKPF_SPERR, 'Not Set')
AND wmpimisc.FreezeBookInvntory = ifnull(st.IKPF_XBUFI, 'Not Set')
AND fwmphi.dim_wmphysicalinvmiscellaneousid <> ifnull(wmpimisc.dim_wmphysicalinvmiscellaneousid, 1);	

call vectorwise(combine 'fact_wmphysicalinventory_tmp');
call vectorwise(combine 'fact_wmphysicalinventory');
call vectorwise(combine 'fact_wmphysicalinventory + fact_wmphysicalinventory_tmp');

drop table if exists fact_wmphysicalinventory_tmp;

