Update getleadtime_tbl 
SET v_leadTime = 0,
    idone=0;

Update getleadtime_tbl
Set v_leadTime = 0,
    idone=1
where pPart IS NULL OR pPlant IS NULL;


Update getleadtime_tbl
Set v_leadTime = ifnull(( Select ekt.EKPO_PLIFZ 
		   FROM EKKO_EKPO_EKET ekt
		   WHERE ekt.EKPO_EBELN = DocumentNumber
		   AND ekt.EKPO_EBELP = DocumentLineNumber),0) 
Where DocumentNumber IS NOT NULL AND DocumentLineNumber > 0
      AND DocumentType = 'PO'
      AND idone = 0;

Drop table if exists EINE_00e;
Create table EINE_00e as Select first 0 * from EINE  ORDER BY EINE_PRDAT DESC, APLFZ DESC;

Update getleadtime_tbl
Set v_leadTime = ifnull(( Select e.APLFZ
                           FROM EINE_00e e
                          WHERE     e.EBELN = DocumentNumber
                                AND e.EBELP = DocumentLineNumber
                                AND ifnull(e.EINE_ESOKZ, '0') = '0'),0)
Where DocumentNumber IS NOT NULL AND DocumentLineNumber > 0
      AND DocumentType = 'PO'
      AND idone = 0
      AND v_leadTime = 0 ;

Update getleadtime_tbl
Set v_leadTime =  IFNULL(( SELECT e.APLFZ
                              FROM EINE_00e e
                              INNER JOIN EINA ea
                                      ON e.INFNR = ea.INFNR
                                   INNER JOIN EKKO_EKPO_EKET b
                                      ON ea.MATNR = b.EKPO_MATNR
                                         AND e.WERKS = b.EKPO_WERKS
                                         AND ea.LIFNR = b.EKKO_LIFNR
                             WHERE     b.EKPO_EBELN = DocumentNumber
                                   AND b.EKPO_EBELP = DocumentLineNumber
                                   AND e.EBELN IS NULL
                                   AND e.APLFZ <> 999
                                   AND e.EINE_ERDAT <=
                                         ifnull(b.EKPO_AEDAT, b.EKKO_BEDAT)
                                   AND e.EINE_PRDAT >=
                                         ifnull(b.EKPO_AEDAT, b.EKKO_BEDAT)
                                   AND ifnull(e.EINE_ESOKZ, '0') = '0'),0)
Where DocumentNumber IS NOT NULL AND DocumentLineNumber > 0
      AND DocumentType = 'PO'
      AND idone = 0
      AND v_leadTime = 0 ;


Update getleadtime_tbl
Set v_leadTime =  ifnull( (SELECT pr.EBAN_PLIFZ
                            FROM eban pr
                           WHERE pr.BANFN = DocumentNumber
                                 AND pr.BNFPO = DocumentLineNumber), 0)
Where DocumentType = 'PR'
      AND idone = 0
      AND v_leadTime = 0;


Update getleadtime_tbl
Set v_leadTime =   ifnull( (SELECT e.aplfz
                            FROM eine e , eban pr
                           WHERE e.INFNR = pr.EBAN_INFNR
                                 AND pr.BANFN = DocumentNumber
                                 AND pr.BNFPO = DocumentLineNumber
                                 AND e.APLFZ <> 999 ), 0)
Where DocumentType = 'PR'
      AND idone = 0
      AND v_leadTime = 0;
 

Update getleadtime_tbl glt
Set v_leadTime =    ifnull((  SELECT e.APLFZ
                                  FROM eina a, eine e,eban pr
                                  Where a.INFNR = e.INFNR
                                             AND a.MATNR = glt.pPart
                                             AND e.WERKS = glt.pPlant
                                          AND    pr.MATNR = a.MATNR
                                             AND pr.WERKS = e.WERKS
                                             AND a.LIFNR = glt.pVendor
                                             AND glt.pVendor IS NOT NULL
                                             AND e.APLFZ <> 999
                                             AND((pr.EBAN_LIFNR = glt.pVendor
                                                  AND pr.eban_lifnr IS NOT NULL)
                                                 OR(pr.EBAN_FLIEF = glt.pVendor
                                                    AND pr.EBAN_FLIEF IS NOT NULL))
                                             AND e.EINE_PRDAT >= pr.BADAT
                                             AND e.EINE_ERDAT <= pr.BADAT
                                             AND ifnull(e.EINE_ESOKZ, '0') = '0'),0)
Where DocumentType = 'PR'
      AND idone = 0
      AND v_leadTime = 0;


Update getleadtime_tbl
Set v_leadTime = ifnull((SELECT e.APLFZ
                               FROM eina a , eine e,plaf p
                               WHERE a.INFNR = e.INFNR
                                          AND a.MATNR = pPart
                                          AND e.WERKS = pPlant
                                          AND a.LIFNR = pVendor
                                          AND pVendor IS NOT NULL
                                          AND e.APLFZ <> 999
                                          AND ifnull(e.EINE_ESOKZ, '0') = '0'
                                       AND     p.PLAF_MATNR = a.MATNR
                                          AND p.PLAF_PLWRK = e.WERKS
                                          AND e.EINE_ERDAT <= p.PLAF_PERTR
                                          AND e.EINE_PRDAT >= p.PLAF_PERTR),0)
Where DocumentType = 'PLAN_ORDER'
      AND idone = 0
      AND v_leadTime = 0;


Update getleadtime_tbl
Set v_leadTime =  IFNULL((SELECT p.MARC_PLIFZ
                          FROM MARA_MARC_MAKT p
                         WHERE p.MARA_MATNR = pPart AND p.MARC_WERKS = pPlant),0)
Where  idone = 0
      AND v_leadTime = 0;


Update getleadtime_tbl
Set v_leadTime =  IFNULL((SELECT m.PLIFZ
                          FROM    LFM1 m,dim_plant dp
                               where  m.LIFNR = pVendor
                                  AND pVendor IS NOT NULL
                                  AND m.EKORG = dp.PurchOrg
                                  AND m.PLIFZ <> 999
                                  AND dp.PlantCode = pPlant),0)
Where  idone = 0
      AND v_leadTime = 0;

Drop table if exists EINE_00e;
