/* ***********************************************************************************************************************************
   Script         - vw_bi_custom_poulate_replenstaging.sql                                                              
   Author         - Suchithra                                                                                   
   Created ON     - Mar 2014                                                                                            
   Description    - Script to populate the replenishment staging table with already extracted data which is copied to a temp table.
		    This is required as for Replenishment, extraction is to be done only if there is a change in the last update
		    date in the source table in SQL server. 
			
 ********************************************Change History**************************************************************************   
*************************************************************************************************************************************/


/* 
The temp table replenishmentforecastlogilityPrevDay has been created as an exact copy of the STG table
inside the populate script for replenishment subject area. 
The same data is being copied back to the STG table incase its empty after that particular extraction as
there has been no change in the last update date.
*/

insert into replenishment_forecast_logility
select * From replenishment_forecast_logility_LatestSnapshot;




