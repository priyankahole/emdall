/**********************************************************************************/
/*  Script : vw_bi_populate_inventoryaging_fact.sql                               */
/*  Description : This script is recreated to allocate total stock quantity into  */
/*                each material movement quantity to reflect actual stock aging.  */
/*  Created On : July 2, 2013                                                     */
/*  Create By : Hiten Suthar                                                      */
/*                                                                                */
/*  Change History                                                                */
/*  Date         CVSversion  By          Description                              */
/*  02 July 2013  1.0         Hiten      New Script                               */
/*  31 July 2013  1.42        Ashu       Optimized an Insert statement            */
/*  14 Aug 2013   1.47        Hiten      Only latest ex rate                      */
/*                                       and added MARC, STO and WIP stock        */
/*  27 Aug 2013   1.54        Hiten      Default for null column values           */
/*  27 Aug 2013   1.55        Shanthi    Checking in Profit Center dimension      */
/*  28 Aug 2013   1.56	      Shanthi    New Field WIP Qty from PRod Order	  */
/*  10 Sep 2013   1.57        Lokesh	 Currency changes			  */
/*  15 Oct 2013   1.81        Issam 	 Added material movement measures	  */
/*  01 Nov 2013   1.86        Hiten 	 On Hand Amount Adjustment       	  */
/*  20 Feb 2014	  1.87		  Nicoleta	 Added dim_DateidExpiryDate       */
/*  19 May 2014	  1.98		  Hiten	 special stock ind citeria change for MARD  */
/**********************************************************************************/

DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

DROP TABLE IF EXISTS fact_inventoryaging_tmp_populate;
CREATE TABLE fact_inventoryaging_tmp_populate
AS
SELECT * FROM fact_inventoryaging
where 1=2;

ALTER TABLE fact_inventoryaging_tmp_populate ADD PRIMARY KEY (fact_inventoryagingid);


delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryaging_tmp_populate',ifnull(max(fact_inventoryagingid),0)
FROM fact_inventoryaging_tmp_populate;

DELETE FROM fact_inventoryaging_tmp_populate;

DELETE FROM MCHB WHERE MCHB_CLABS = 0 AND MCHB_CUMLM = 0 AND MCHB_CINSM = 0 AND MCHB_CEINM = 0 AND MCHB_CSPEM = 0;
DELETE FROM MARD WHERE MARD_LABST = 0 AND MARD_UMLME = 0 AND MARD_INSME = 0 AND MARD_EINME = 0 AND MARD_SPEME = 0
			AND MARD_KSPEM = 0 AND MARD_KINSM = 0 AND MARD_KEINM = 0 AND MARD_KLABS = 0;
DELETE FROM MSLB WHERE MSLB_LBLAB = 0 AND MSLB_LBINS = 0 AND MSLB_LBEIN = 0;
DELETE FROM MSKU WHERE MSKU_KULAB = 0 AND MSKU_KUINS = 0 AND MSKU_KUEIN = 0;
DELETE FROM MARC WHERE MARC_UMLMC = 0 AND MARC_TRAME = 0 AND MARC_GLGMG = 0 AND MARC_BWESB = 0;
DELETE FROM MARC WHERE MARC_LVORM = 'X';

delete from mard 
where exists (select 1 from mard b where mard.mard_matnr = b.mard_matnr and mard.mard_werks = b.mard_werks 
			and mard.mard_lgort <> b.mard_lgort and mard.mard_labst = -1 * b.mard_labst
			and b.mard_labst > 0);

UPDATE MCHB SET MCHB_CHARG = 'Not Set' WHERE MCHB_CHARG IS NULL;
UPDATE MSLB SET MSLB_CHARG = 'Not Set' WHERE MSLB_CHARG IS NULL;
UPDATE MSKU SET MSKU_CHARG = 'Not Set' WHERE MSKU_CHARG IS NULL;

UPDATE fact_materialmovement SET dd_BatchNumber = 'Not Set' WHERE dd_BatchNumber IS NULL;

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'dim_storagelocation';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'dim_storagelocation', IFNULL(MAX(dim_storagelocationid), 0)
     FROM dim_storagelocation;

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
				Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent) 
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_storagelocation') + row_number() over (),tty.*
FROM
   (SELECT DISTINCT MCHB_LGORT,
                   MCHB_LGORT,
                   'Not Set' Division,
                   'Not Set' FreezingBookInventory,
                   'Not Set' MRPExclude,
                   'Not Set' StorageResource,
                   'Not Set' PartnerStorageLocation,
                   'Not Set' StorageSalesOrg,
                   'Not Set' DistributionChannel,
                   'Not Set' ShipReceivePoint ,
                   MCHB_WERKS Plant ,
                   'Not Set' InTransit,
                   TIMESTAMP(LOCAL_TIMESTAMP) RowStartDate,
                   1 RowIsCurrent
     FROM MCHB m
    WHERE MCHB_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE LocationCode = MCHB_LGORT AND Plant = MCHB_WERKS)) tty;

 UPDATE NUMBER_FOUNTAIN 
 SET max_id = ifnull(( select max(dim_storagelocationid) from dim_storagelocation), 0)
 WHERE table_name = 'dim_storagelocation';

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
				Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent) 
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_storagelocation') + row_number() over (),tty.*
FROM
   (SELECT DISTINCT MARD_LGORT,
                   MARD_LGORT,
                   'Not Set' Division,
                   'Not Set' FreezingBookInventory ,
                   'Not Set' MRPExclude,
                   'Not Set' StorageResource,
                   'Not Set' PartnerStorageLocation,
                   'Not Set' StorageSalesOrg,
                   'Not Set' DistributionChannel,
                   'Not Set' ShipReceivePoint,
                   MARD_WERKS Plant ,
                   'Not Set' InTransit,
                   TIMESTAMP(LOCAL_TIMESTAMP) RowStartDate,
                   1 RowIsCurrent          
     FROM MARD m
    WHERE MARD_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE LocationCode = MARD_LGORT AND Plant = MARD_WERKS)) tty;

\i /db/schema_migration/bin/wrapper_optimizedb.sh dim_storagelocation;

DELETE FROM fact_materialmovement_tmp_invagin;

INSERT INTO fact_materialmovement_tmp_invagin
  (Dim_Partid, Dim_Plantid, Dim_MovementTypeid, dd_BatchNumber, dd_MaterialDocYearItem)
  SELECT x.Dim_Partid, x.Dim_Plantid, x.Dim_MovementTypeid, ifnull(x.dd_BatchNumber,'Not Set') dd_BatchNumber,
      max(concat(x.dd_MaterialDocYear,LPAD(x.dd_MaterialDocNo,10,'0'),LPAD(x.dd_MaterialDocItemNo,5,'0')))
  FROM fact_materialmovement x
        INNER JOIN dim_movementtype dmt ON x.Dim_MovementTypeid = dmt.Dim_MovementTypeid
  WHERE dmt.MovementType in (101,131,105,501,511,521)
  GROUP BY x.Dim_Partid, x.Dim_Plantid, x.Dim_MovementTypeid, ifnull(x.dd_BatchNumber,'Not Set');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_invagin;

DROP TABLE IF EXISTS tmp_MARD_TotStk_001;
CREATE TABLE tmp_MARD_TotStk_001 AS
SELECT MARD_MATNR PartNumber,
	MARD_WERKS PlantCode, 
	MARD_LGORT StorLocCode, 
	cast('Not Set' as varchar(10)) BatchNumber,
	MARD_UMLME MARD_UMLME_1,
	MARD_LABST MARD_LABST_2, 
	MARD_SPEME MARD_SPEME_3, 
	MARD_INSME MARD_INSME_4, 
	MARD_EINME MARD_EINME_5,
       (MARD_LABST + MARD_EINME + MARD_INSME + MARD_SPEME + MARD_UMLME) TotalStock,
	MARD_KSPEM, 
	MARD_KINSM, 
	MARD_KEINM,
	MARD_KLABS, 
	MARD_RETME,
	MARD_DLINL,
	MARD_ERSDA
FROM MARD;

INSERT INTO tmp_MARD_TotStk_001
SELECT MCHB_MATNR PartNumber,
	MCHB_WERKS PlantCode, 
	MCHB_LGORT StorLocCode, 
	MCHB_CHARG BatchNumber,
	MCHB_CUMLM MARD_UMLME_1,
	MCHB_CLABS MARD_LABST_2, 
	MCHB_CSPEM MARD_SPEME_3, 
	MCHB_CINSM MARD_INSME_4, 
	MCHB_CEINM MARD_EINME_5,
       (MCHB_CLABS + MCHB_CEINM + MCHB_CINSM + MCHB_CSPEM + MCHB_CUMLM) TotalStock,
	0 MARD_KSPEM, 
	0 MARD_KINSM, 
	0 MARD_KEINM,
	0 MARD_KLABS, 
	MCHB_CRETM MARD_RETME,
	MCHB_ERSDA MARD_DLINL,
	MCHB_ERSDA MARD_ERSDA 
FROM MCHB
WHERE NOT EXISTS (SELECT 1 FROM MARD
		   WHERE MCHB_MATNR = MARD_MATNR
			 AND MCHB_WERKS = MARD_WERKS
			 AND MCHB_LGORT = MARD_LGORT);

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_MARD_TotStk_001;


DROP TABLE IF EXISTS tmp_MARD_TotStk_001_U;
CREATE TABLE tmp_MARD_TotStk_001_U AS
SELECT PartNumber,
	PlantCode,
	SUM(TotalStock) TotalStockQty
FROM tmp_MARD_TotStk_001
GROUP BY PartNumber,
	PlantCode;


DROP TABLE IF EXISTS tmp_MARD_TotStk_t01;
CREATE TABLE tmp_MARD_TotStk_t01 AS
SELECT PartNumber,
	PlantCode, 
	StorLocCode, 
	BatchNumber,
	TotalStock,
    ROW_NUMBER() OVER(PARTITION BY PartNumber, PlantCode 
    			ORDER BY MARD_UMLME_1 desc, 
				MARD_LABST_2 desc, 
				MARD_SPEME_3 desc, 
				MARD_INSME_4 desc, 
				MARD_EINME_5 desc, 
				StorLocCode, BatchNumber) M_RowSeqNo
FROM tmp_MARD_TotStk_001;

DROP TABLE IF EXISTS tmp_MARD_TotStk_t02;
CREATE TABLE tmp_MARD_TotStk_t02 AS
SELECT a.PartNumber,
	a.PlantCode, 
	a.StorLocCode, 
	a.BatchNumber,
	a.TotalStock,
        a.M_RowSeqNo,
	SUM(b.TotalStock) - a.TotalStock TotalStockCUM_A,
	SUM(b.TotalStock) TotalStockCUM_B
FROM tmp_MARD_TotStk_t01 a
	inner join tmp_MARD_TotStk_t01 b on a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode
WHERE a.M_RowSeqNo >= b.M_RowSeqNo
GROUP BY a.PartNumber,
	a.PlantCode, 
	a.StorLocCode, 
	a.BatchNumber,
	a.TotalStock,
        a.M_RowSeqNo;


DROP TABLE IF EXISTS materialmovement_tmp_001_pre;
CREATE TABLE materialmovement_tmp_001_pre AS
      SELECT cx.fact_materialmovementid,
          cx.ct_Quantity,
	      dp1.PartNumber, 
		  dpl.PlantCode,  
		  dt1.DateValue, 
		  cx.dd_MaterialDocNo, cx.dd_MaterialDocItemNo, cx.dd_DocumentScheduleNo
      FROM fact_materialmovement cx 
	    INNER JOIN dim_movementtype dmt
		      ON cx.Dim_MovementTypeid = dmt.Dim_MovementTypeid AND cx.dd_debitcreditid = 'Debit'
			 AND dmt.MovementType in (101,105,131,222,301,305,309,351,453,501,503,505,511,521,523,525,541,561,602,621,631,643,645,647,644,653,655,657,673,701)
            INNER JOIN dim_Plant dpl ON cx.dim_Plantid = dpl.dim_Plantid
            INNER JOIN dim_Part dp1 ON cx.dim_Partid = dp1.dim_Partid
            INNER JOIN dim_date dt1 ON dt1.Dim_Dateid = cx.dim_DateIDPostingDate
	    INNER JOIN tmp_MARD_TotStk_001_U mrd ON dp1.PartNumber = mrd.PartNumber
						  AND dpl.PlantCode = mrd.PlantCode
	    INNER JOIN dim_specialstock spst ON spst.dim_specialstockid = cx.dim_specialstockid
     WHERE cx.ct_Quantity > 0 AND spst.specialstockindicator not in ('O','V','W','E','Q')
	   AND (dmt.MovementType not in ('351','641','643','645','647')
			OR (dmt.MovementType in ('351','641','643','645','647')
				AND NOT EXISTS (SELECT 1 
						FROM fact_materialmovement cx1 
							INNER JOIN dim_movementtype dmt1 ON cx1.Dim_MovementTypeid = dmt1.Dim_MovementTypeid
						WHERE cx1.dd_debitcreditid = 'Debit' AND dmt1.MovementType = 101
						     AND cx1.dim_Partid = cx.dim_Partid AND cx1.dim_specialstockid = cx.dim_specialstockid
						     AND cx1.dd_DocumentNo = cx.dd_DocumentNo AND cx1.dd_DocumentItemNo = cx.dd_DocumentItemNo
						     AND cx1.ct_QtyEntryUOM = cx.ct_QtyEntryUOM)));


DROP TABLE IF EXISTS materialmovement_tmp_001;
CREATE TABLE materialmovement_tmp_001 AS
      SELECT cx.fact_materialmovementid,
          cx.ct_Quantity ct_Quantity,
	      cx.PartNumber PartNumber, 
		  cx.PlantCode PlantCode,  
		  cx.DateValue PostingDate,
		  ROW_NUMBER() OVER(PARTITION BY cx.PartNumber, cx.PlantCode
					ORDER BY cx.DateValue DESC, cx.dd_MaterialDocNo DESC, cx.dd_MaterialDocItemNo DESC, cx.dd_DocumentScheduleNo DESC) RowSeqNo
      FROM materialmovement_tmp_001_pre cx;

DROP TABLE IF EXISTS materialmovement_tmp_001_pre;

\i /db/schema_migration/bin/wrapper_optimizedb.sh materialmovement_tmp_001;

DROP TABLE IF EXISTS tmp_mm_lastrcptinfo_001;
CREATE TABLE tmp_mm_lastrcptinfo_001 AS
SELECT cx.PartNumber,
	cx.PlantCode,
	cx.ct_Quantity,
	cx.PostingDate
FROM  materialmovement_tmp_001 cx
WHERE cx.RowSeqNo = 1;

DROP TABLE IF EXISTS mm_tmp_maxseqno_001;
CREATE TABLE mm_tmp_maxseqno_001 AS
SELECT a.PartNumber, 
	  a.PlantCode, 
	  MAX(a.RowSeqNo) MaxRowSeqNo
FROM materialmovement_tmp_001 a
GROUP BY a.PartNumber, 
	  a.PlantCode;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_mm_lastrcptinfo_001;
\i /db/schema_migration/bin/wrapper_optimizedb.sh mm_tmp_maxseqno_001;

DROP TABLE IF EXISTS materialmovement_tmp_002;
CREATE TABLE materialmovement_tmp_002 AS
SELECT a.fact_materialmovementid,
	  a.ct_Quantity,
	  a.PartNumber, 
	  a.PlantCode, 
	  a.PostingDate,
	  a.RowSeqNo,
	  SUM(b.ct_Quantity) - a.ct_Quantity QuantityCUM_A,
	  SUM(b.ct_Quantity) QuantityCUM_B,
	  c.MaxRowSeqNo
FROM materialmovement_tmp_001 a
	inner join materialmovement_tmp_001 b on a.PartNumber = b.PartNumber
						and a.PlantCode = b.PlantCode
	inner join mm_tmp_maxseqno_001 c on a.PartNumber = c.PartNumber
						and a.PlantCode = c.PlantCode
WHERE a.RowSeqNo >= b.RowSeqNo
GROUP BY a.fact_materialmovementid,
	  a.ct_Quantity,
	  a.PartNumber, 
	  a.PlantCode, 
	  a.PostingDate,
	  a.RowSeqNo,
	  c.MaxRowSeqNo;

\i /db/schema_migration/bin/wrapper_optimizedb.sh materialmovement_tmp_002;

DROP TABLE IF EXISTS materialmovement_tmp_new;
CREATE TABLE materialmovement_tmp_new AS
SELECT cx.fact_materialmovementid,
	cx.dd_MaterialDocNo dd_MaterialDocNo,
	cx.dd_MaterialDocItemNo dd_MaterialDocItemNo,
	cx.dd_MaterialDocYear dd_MaterialDocYear,
	ifnull(cx.dd_DocumentNo,'Not Set') dd_DocumentNo,
	cx.dd_DocumentItemNo dd_DocumentItemNo,
	cx.dd_DocumentScheduleNo,
	ifnull(cx.dd_SalesOrderNo,'Not Set') dd_SalesOrderNo,
	cx.dd_SalesOrderItemNo dd_SalesOrderItemNo,
	ifnull(cx.dd_SalesOrderDlvrNo,0) dd_SalesOrderDlvrNo,
	cx.dd_BatchNumber dd_BatchNumber,
	cx.dd_ValuationType dd_ValuationType,
	cx.dd_debitcreditid dd_debitcreditid,
	cx.amt_LocalCurrAmt amt_LocalCurrAmt,
	cx.amt_DeliveryCost amt_DeliveryCost,
	cx.amt_ExchangeRate_GBL amt_ExchangeRate_GBL,
	cx.amt_AltPriceControl amt_AltPriceControl,
	cx.amt_OnHand_ValStock amt_OnHand_ValStock,
	cx.ct_Quantity ct_Quantity,
	cx.ct_Quantity ct_QtyEntryUOM,
	cx.Dim_MovementTypeid Dim_MovementTypeid,
	cx.dim_Companyid dim_Companyid,
	cx.Dim_Partid Dim_Partid,
	cx.Dim_Plantid Dim_Plantid,
	cx.Dim_StorageLocationid Dim_StorageLocationid,
	cx.Dim_Vendorid Dim_Vendorid,
	cx.dim_DateIDPostingDate dim_DateIDPostingDate,
	cx.dim_MovementIndicatorid dim_MovementIndicatorid,
	cx.dim_CostCenterid dim_CostCenterid,
	cx.dim_specialstockid dim_specialstockid,
	cx.Dim_StockTypeid Dim_StockTypeid,
	cx.Dim_Currencyid Dim_Currencyid,
	cx.amt_ExchangeRate amt_ExchangeRate,	
	a.PartNumber,
	a.PlantCode,
	a.PostingDate,	
	a.QuantityCUM_A,
	a.QuantityCUM_B,
	b.ct_Quantity LatestReceiptQty,
	b.PostingDate LatestPostingDate,
	a.RowSeqNo,
	a.MaxRowSeqNo,
	cx.Dim_Currencyid_TRA Dim_Currencyid_TRA,
	cx.Dim_Currencyid_GBL Dim_Currencyid_GBL
FROM fact_materialmovement cx
	inner join materialmovement_tmp_002 a on a.fact_materialmovementid = cx.fact_materialmovementid
	inner join tmp_mm_lastrcptinfo_001 b on a.PartNumber = b.PartNumber
						and a.PlantCode = b.PlantCode
	inner join tmp_MARD_TotStk_001_U c on a.PartNumber = c.PartNumber
						and a.PlantCode = c.PlantCode
						and a.QuantityCUM_A < c.TotalStockQty;

DROP TABLE IF EXISTS mm_tmp_maxseqno_001;
CREATE TABLE mm_tmp_maxseqno_001 AS
SELECT a.PartNumber, 
	  a.PlantCode, 
	  MAX(a.RowSeqNo) MaxRowSeqNo
FROM materialmovement_tmp_new a
GROUP BY a.PartNumber, 
	  a.PlantCode;

update materialmovement_tmp_new a
from mm_tmp_maxseqno_001 b
set a.MaxRowSeqNo = b.MaxRowSeqNo
where a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode;

\i /db/schema_migration/bin/wrapper_optimizedb.sh materialmovement_tmp_new;

DROP TABLE IF EXISTS tmp_c2;
CREATE TABLE tmp_c2 AS
SELECT PartNumber,PlantCode,max(QuantityCUM_B) as max_stk
FROM materialmovement_tmp_new c1
GROUP BY PartNumber,PlantCode;

DROP TABLE IF EXISTS tmp_c3;
CREATE TABLE tmp_c3 AS
SELECT c1.*
FROM materialmovement_tmp_new c1, tmp_c2 c2
WHERE c2.PartNumber = c1.PartNumber AND c2.PlantCode = c1.PlantCode
	AND c1.QuantityCUM_B = ifnull(c2.max_stk,0) ;


/*** MBEW NO BWTAR values ***/

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x 
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY)
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR AS
SELECT DISTINCT m.MATNR,
       m.BWKEY,
       m.LFGJA,
       m.LFMON,
       m.VPRSV,
       m.VERPR,
       m.STPRS,
       m.PEINH,
       m.SALK3,
       m.BWTAR,
       m.MBEW_LBKUM,
       ((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, 
       m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;	

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.STPRS / b.PEINH), 18,5)
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.VERPR / b.PEINH), 18,5)
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

/* divide inventory stock into material movement qty */

DROP TABLE IF EXISTS tmp_fact_invaging_001;
CREATE TABLE tmp_fact_invaging_001 AS
SELECT b.fact_materialmovementid,
	a.PartNumber,
	a.PlantCode,
	a.StorLocCode,
	a.BatchNumber,
	case when a.MARD_UMLME_1 <= 0 then 0
		when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_UMLME_1
				when a.MARD_UMLME_1 <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A)
				else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
			end
		else 0
	end MARD_UMLME_1,
	case when a.MARD_LABST_2 <= 0 then 0
		when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_LABST_2
				when (a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then case when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - (a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when a.MARD_UMLME_1 <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - (a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end							
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_LABST_2,
	case when a.MARD_SPEME_3 <= 0 then 0
		when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_SPEME_3
				when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then case when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_SPEME_3, 
	case when a.MARD_INSME_4 <= 0 then 0
		when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_INSME_4
				when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										- ((a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_INSME_4, 
	case when a.MARD_EINME_5 <= 0 then 0
		when (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_EINME_5
				when (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_EINME_5,			
	b.PostingDate,	
	b.dim_DateIDPostingDate,
	b.LatestReceiptQty,
	b.LatestPostingDate,
	a.BatchNumber dd_BatchNumber,
	b.dd_ValuationType,
	b.Dim_StorageLocationid, 
	b.dim_Companyid,
	b.dim_stocktypeid, 
	b.dim_specialstockid,
	b.Dim_MovementIndicatorid, 
	b.dim_CostCenterid,
	b.Dim_MovementTypeid,
	b.RowSeqNo,
	b.MaxRowSeqNo,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KSPEM ELSE 0 END MARD_KSPEM, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_KINSM ELSE 0 END MARD_KINSM, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_KEINM ELSE 0 END MARD_KEINM,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KLABS ELSE 0 END MARD_KLABS, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_RETME ELSE 0 END MARD_RETME,
	a.MARD_DLINL,
	a.MARD_ERSDA,
	ROW_NUMBER() OVER(PARTITION BY a.PartNumber, a.PlantCode
			  ORDER BY dt1.DateValue, b.dd_MaterialDocNo, b.dd_MaterialDocItemNo, b.dd_DocumentScheduleNo) MinRowSeqNo
FROM tmp_MARD_TotStk_001 a
	inner join materialmovement_tmp_new b on a.PartNumber = b.PartNumber
						and a.PlantCode = b.PlantCode
	inner join dim_date dt1 on b.dim_DateIDPostingDate = dt1.dim_dateid
	inner join tmp_MARD_TotStk_t02 c on c.PartNumber = a.PartNumber
						and c.PlantCode = a.PlantCode
						and c.StorLocCode = a.StorLocCode
						and c.BatchNumber = a.BatchNumber
WHERE ((b.QuantityCUM_A = 0 and c.TotalStockCUM_A = 0) 
	or (b.QuantityCUM_A < c.TotalStockCUM_B and b.QuantityCUM_B > c.TotalStockCUM_A));

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_fact_invaging_001;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp2_MBEW_NO_BWTAR;

/*Insert 1 */

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MARD';

INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL) 
SELECT MARD_LABST_2 ct_StockQty, 
	MARD_EINME_5 ct_TotalRestrictedStock, 
	MARD_INSME_4 ct_StockInQInsp, 
	MARD_SPEME_3 ct_BlockedStock, 
	MARD_UMLME_1 ct_StockInTransfer, 
	MARD_KSPEM ct_BlockedConsgnStock, 
	MARD_KINSM ct_ConsgnStockInQInsp, 
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock, 
	MARD_RETME ct_BlockedStockReturns, 
	CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END amt_StockValueAmt,
	ifnull(((CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END ) 
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockValueAmt_GBL,
	CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END amt_BlockedStockAmt,
	ifnull(((CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_BlockedStockAmt_GBL,
	CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END amt_StockInQInspAmt,
	ifnull(((CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockInQInspAmt_GBL,
	CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END amt_StockInTransferAmt,
	ifnull(((CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockInTransferAmt_GBL,
	CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_KLABS) END amt_UnrestrictedConsgnStockAmt,
	ifnull(((CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_KLABS) END )
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_UnrestrictedConsgnStockAmt_GBL,
	b.multiplier amt_StdUnitPrice,
	ifnull((b.multiplier
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StdUnitPrice_GBL,
	CASE WHEN MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END
			  + CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END
			  + CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END
			  + CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END
			  + CASE WHEN MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_EINME_5) END)
	END amt_OnHand,
	ifnull(((CASE WHEN MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END
			  + CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END
			  + CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END
			  + CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END
			  + CASE WHEN MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_EINME_5) END) 
		  END ) * ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
				  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
					and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) LastReceivedQty, 
	c1.dd_BatchNumber BatchNo, 
	ifnull(c1.dd_ValuationType,'Not Set') ValuationType, 
	'Not Set' DocumentNo,
	0 DocumentItemNo, 
	dmt.MovementType MovementType, 
	c1.dim_DateIDPostingDate StorageLocEntryDate, 
	ifnull((select lrd.dim_dateid from dim_date lrd where c1.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = p.CompanyCode),1) LastReceivedDate,
	dp.Dim_PartID, 
	p.dim_plantid, 
	sl.Dim_StorageLocationid, 
	c.dim_Companyid,
	1 dim_vendorid, 
	dc.dim_currencyid, 
	c1.dim_stocktypeid, 
	1 dim_specialstockid,
	ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
	pg.Dim_PurchaseGroupid, 
	dph.dim_producthierarchyid, 
	uom.Dim_UnitOfMeasureid, 
	c1.Dim_MovementIndicatorid, 
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
			and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
	1 amt_ExchangeRate, 
	dc.Dim_Currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL	
FROM tmp_fact_invaging_001 c1 
	INNER JOIN dim_plant p ON c1.PlantCode = p.PlantCode
	INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = c1.PartNumber AND b.BWKEY = p.ValuationArea
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_part dp ON dp.PartNumber = c1.PartNumber AND dp.Plant = c1.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = c1.StorLocCode AND sl.Plant = c1.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON varchar(dc.CurrencyCode,10) = varchar(c.Currency,10),
	tmp_GlobalCurr_001 gbl,
	tmp_stkcategory_001 stkc
WHERE MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 > 0;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';
	
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

/* Changes in above query - 
	a) Found out all columns where were not insert column list. Populate the default values ( according to earlier defn in MySQL )
	b) ifnull for c1.LatestReceiptQty,lrd.dim_dateid and c1.dd_ValuationType ( which were found after debugging errors )
	c) Fact row_number logic */
/* Above query works now */


/* INSERT 2 */

/* For now replaced getExchangeRate return value with 1 ( checked that tcurr does not have anything in fossil2 or the qa db on mysql 
   For dbs which have data in this table, the function would need to be converted to SQLs. 
   Already started that - refer getExchangeRate_func.sql */

drop table if exists tmp_tbl_StorageLocEntryDate;
CREATE table  tmp_tbl_StorageLocEntryDate AS 
SELECT  c2.PartNumber, c2.PlantCode, MAX(c2.LatestPostingDate) as LatestPostingDate
FROM	 materialmovement_tmp_new c2
GROUP BY c2.PartNumber, c2.PlantCode;


INSERT INTO fact_inventoryaging_tmp_populate(
	ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer,
	ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns,
	amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, amt_BlockedStockAmt_GBL, amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice, amt_StdUnitPrice_GBL, 
	amt_OnHand,	/*Lokesh 26apr*/
	amt_OnHand_GBL,/*Lokesh 26apr*/
	ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid,
	dim_Partid, dim_Plantid, dim_StorageLocationid, dim_Companyid, dim_Vendorid,
	dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid,
	dim_producthierarchyid, Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_stockcategoryid,
	fact_inventoryagingid, Dim_ConsumptionTypeid, Dim_DocumentStatusid, Dim_DocumentTypeid, Dim_IncoTermid,
	Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid, Dim_PurchaseMiscid, ct_StockInTransit,
	amt_StockInTransitAmt, amt_StockInTransitAmt_GBL, Dim_SupplyingPlantId, amt_MtlDlvrCost, amt_OverheadCost,
	amt_OtherCost, amt_WIPBalance, ct_WIPAging, amt_MovingAvgPrice, amt_PreviousPrice,
	amt_CommericalPrice1, amt_PlannedPrice1, amt_PrevPlannedPrice, amt_CurPlannedPrice, Dim_DateIdPlannedPrice2,
	Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT DISTINCT a.MARD_LABST_2 ct_StockQty, 
	a.MARD_EINME_5 ct_TotalRestrictedStock, 
	a.MARD_INSME_4 ct_StockInQInsp, 
	a.MARD_SPEME_3 ct_BlockedStock, 
	a.MARD_UMLME_1 ct_StockInTransfer, 
	a.MARD_KSPEM ct_BlockedConsgnStock, 
	a.MARD_KINSM ct_ConsgnStockInQInsp, 
	a.MARD_KEINM ct_RestrictedConsgnStock,
	a.MARD_KLABS ct_UnrestrictedConsgnStock, 
	a.MARD_RETME ct_BlockedStockReturns, 
	CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END amt_StockValueAmt,
	ifnull(((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END )
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockValueAmt_GBL,
	CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END amt_BlockedStockAmt,
	ifnull(((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_BlockedStockAmt_GBL,
	CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END amt_StockInQInspAmt,
	ifnull(((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockInQInspAmt_GBL,
	CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END amt_StockInTransferAmt,
	ifnull(((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockInTransferAmt_GBL,
	CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END amt_UnrestrictedConsgnStockAmt,
	ifnull(((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END )
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_UnrestrictedConsgnStockAmt_GBL,
	b.multiplier amt_StdUnitPrice,
	ifnull((b.multiplier
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StdUnitPrice_GBL,
	CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END)
	END amt_OnHand,
	ifnull(((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END) 
		  END )
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) LastReceivedQty, 
	a.BatchNumber BatchNo, 
	ifnull(c1.dd_ValuationType,'Not Set') ValuationType,
	'Not Set' dd_DocumentNo, 
	0 dd_DocumentItemNo, 
	dmt.MovementType MovementType, 
	c1.dim_DateIDPostingDate StorageLocEntryDate, 
	ifnull((select lrd.dim_dateid from dim_date lrd where c1.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = p.CompanyCode),1) LastReceivedDate,
	Dim_Partid, p.dim_plantid, sl.Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po where po.PurchaseOrgCode = p.PurchOrg), 1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid,
	c1.Dim_MovementIndicatorid, 
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over () , 
	1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
			and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL		
     FROM tmp_MARD_TotStk_001 a 
	  INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.PartNumber AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
	  INNER JOIN tmp_fact_invaging_001 c1 ON a.PlantCode = c1.PlantCode AND a.PartNumber = c1.PartNumber AND c1.MinRowSeqNo = 1
	  INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
          INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode 
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON varchar(dc.CurrencyCode,10) = varchar(c.Currency,10), 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
    WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid 
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and c2.dd_BatchNo = a.BatchNumber);

/* INSERT 3 - No MBEW */

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO fact_inventoryaging_tmp_populate(
	ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer,
	ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns,
	amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, amt_BlockedStockAmt_GBL, amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice, amt_StdUnitPrice_GBL, 
	amt_OnHand,	
	amt_OnHand_GBL,
	ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid,
	dim_Partid, dim_Plantid, dim_StorageLocationid, dim_Companyid, dim_Vendorid,
	dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid,
	dim_producthierarchyid, Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_stockcategoryid,
	fact_inventoryagingid, Dim_ConsumptionTypeid, Dim_DocumentStatusid, Dim_DocumentTypeid, Dim_IncoTermid,
	Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid, Dim_PurchaseMiscid, ct_StockInTransit,
	amt_StockInTransitAmt, amt_StockInTransitAmt_GBL, Dim_SupplyingPlantId, amt_MtlDlvrCost, amt_OverheadCost,
	amt_OtherCost, amt_WIPBalance, ct_WIPAging, amt_MovingAvgPrice, amt_PreviousPrice,
	amt_CommericalPrice1, amt_PlannedPrice1, amt_PrevPlannedPrice, amt_CurPlannedPrice, Dim_DateIdPlannedPrice2,
	Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT DISTINCT a.MARD_LABST_2 ct_StockQty, 
	a.MARD_EINME_5 ct_TotalRestrictedStock, 
	a.MARD_INSME_4 ct_StockInQInsp, 
	a.MARD_SPEME_3 ct_BlockedStock, 
	a.MARD_UMLME_1 ct_StockInTransfer, 
	a.MARD_KSPEM ct_BlockedConsgnStock, 
	a.MARD_KINSM ct_ConsgnStockInQInsp, 
	a.MARD_KEINM ct_RestrictedConsgnStock,
	a.MARD_KLABS ct_UnrestrictedConsgnStock, 
	a.MARD_RETME ct_BlockedStockReturns, 
	CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END amt_StockValueAmt,
	ifnull(((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END )
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockValueAmt_GBL,
	CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END amt_BlockedStockAmt,
	ifnull(((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_BlockedStockAmt_GBL,
	CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END amt_StockInQInspAmt,
	ifnull(((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockInQInspAmt_GBL,
	CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END amt_StockInTransferAmt,
	ifnull(((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StockInTransferAmt_GBL,
	CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END amt_UnrestrictedConsgnStockAmt,
	ifnull(((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END )
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_UnrestrictedConsgnStockAmt_GBL,
	b.multiplier amt_StdUnitPrice,
	ifnull((b.multiplier
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_StdUnitPrice_GBL,
	CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END)
	END amt_OnHand,
	ifnull(((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END) 
		  END )
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			  where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
				and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)), 0) amt_OnHand_GBL,
	0 LastReceivedQty, 
	a.BatchNumber BatchNo, 
	'Not Set' ValuationType,
	'Not Set' dd_DocumentNo, 
	0 dd_DocumentItemNo, 
	'Not Set' MovementType, 
	ifnull((SELECT lrd.Dim_Dateid FROM tmp_tbl_StorageLocEntryDate c2 inner join dim_date lrd on lrd.DateValue = c2.LatestPostingDate 
		WHERE c2.PartNumber = a.PartNumber AND c2.PlantCode = a.PlantCode and lrd.CompanyCode = p.CompanyCode), sled.Dim_Dateid) StorageLocEntryDate,
	1 LastReceivedDate,
	Dim_Partid, p.dim_plantid, sl.Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po where po.PurchaseOrgCode = p.PurchOrg), 1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid,
	1 Dim_MovementIndicatorid, 
	1 dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over () , 
	1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
			and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL		
     FROM tmp_MARD_TotStk_001 a 
	  INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.PartNumber AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
          INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode 
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON varchar(dc.CurrencyCode,10) = varchar(c.Currency,10) ,
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
    WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid 
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and c2.dd_BatchNo = a.BatchNumber);

/* INSERT 4 - No MBEW */

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL) 
SELECT MARD_LABST_2 ct_StockQty, 
	MARD_EINME_5 ct_TotalRestrictedStock, 
	MARD_INSME_4 ct_StockInQInsp, 
	MARD_SPEME_3 ct_BlockedStock, 
	MARD_UMLME_1 ct_StockInTransfer, 
	MARD_KSPEM ct_BlockedConsgnStock, 
	MARD_KINSM ct_ConsgnStockInQInsp, 
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock, 
	MARD_RETME ct_BlockedStockReturns, 
	0 amt_StockValueAmt,
	0 amt_StockValueAmt_GBL,
	0 amt_BlockedStockAmt,
	0 amt_BlockedStockAmt_GBL,
	0 amt_StockInQInspAmt,
	0 amt_StockInQInspAmt_GBL,
	0 amt_StockInTransferAmt,
	0 amt_StockInTransferAmt_GBL,
	0 amt_UnrestrictedConsgnStockAmt,
	0 amt_UnrestrictedConsgnStockAmt_GBL,
	0 amt_StdUnitPrice,
	0 amt_StdUnitPrice_GBL,
	0 amt_OnHand,
	0 amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) LastReceivedQty, 
	c1.dd_BatchNumber BatchNo, 
	ifnull(c1.dd_ValuationType,'Not Set') ValuationType, 
	'Not Set' DocumentNo,
	0 DocumentItemNo, 
	dmt.MovementType MovementType, 
	c1.dim_DateIDPostingDate StorageLocEntryDate, 
	ifnull((select lrd.dim_dateid from dim_date lrd where c1.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = p.CompanyCode),1) LastReceivedDate,
	dp.Dim_PartID, 
	p.dim_plantid, 
	sl.Dim_StorageLocationid, 
	c.dim_Companyid,
	1 dim_vendorid, 
	dc.dim_currencyid, 
	c1.dim_stocktypeid, 
	1 dim_specialstockid,
	ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
	pg.Dim_PurchaseGroupid, 
	dph.dim_producthierarchyid, 
	uom.Dim_UnitOfMeasureid, 
	c1.Dim_MovementIndicatorid, 
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
			and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
	1 amt_ExchangeRate, 
	dc.Dim_Currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL	
FROM tmp_fact_invaging_001 c1 
	INNER JOIN dim_plant p ON c1.PlantCode = p.PlantCode
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_part dp ON dp.PartNumber = c1.PartNumber AND dp.Plant = c1.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = c1.StorLocCode AND sl.Plant = c1.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON varchar(dc.CurrencyCode,10) = varchar(c.Currency,10),
	tmp_GlobalCurr_001 gbl,
	tmp_stkcategory_001 stkc
WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
		WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid 
				and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
				and c2.dd_BatchNo = c1.dd_BatchNumber);

/* INSERT 5 - No MBEW - No Movement TYpe*/

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';
	
INSERT INTO fact_inventoryaging_tmp_populate(
	ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer,
	ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns,
	amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, amt_BlockedStockAmt_GBL, amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice, amt_StdUnitPrice_GBL, 
	amt_OnHand,	/*Lokesh 26apr*/
	amt_OnHand_GBL,/*Lokesh 26apr*/
	ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid,
	dim_Partid, dim_Plantid, dim_StorageLocationid, dim_Companyid, dim_Vendorid,
	dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid,
	dim_producthierarchyid, Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_stockcategoryid,
	fact_inventoryagingid, Dim_ConsumptionTypeid, Dim_DocumentStatusid, Dim_DocumentTypeid, Dim_IncoTermid,
	Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid, Dim_PurchaseMiscid, ct_StockInTransit,
	amt_StockInTransitAmt, amt_StockInTransitAmt_GBL, Dim_SupplyingPlantId, amt_MtlDlvrCost, amt_OverheadCost,
	amt_OtherCost, amt_WIPBalance, ct_WIPAging, amt_MovingAvgPrice, amt_PreviousPrice,
	amt_CommericalPrice1, amt_PlannedPrice1, amt_PrevPlannedPrice, amt_CurPlannedPrice, Dim_DateIdPlannedPrice2,
	Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT DISTINCT MARD_LABST_2 ct_StockQty, 
	MARD_EINME_5 ct_TotalRestrictedStock, 
	MARD_INSME_4 ct_StockInQInsp, 
	MARD_SPEME_3 ct_BlockedStock, 
	MARD_UMLME_1 ct_StockInTransfer, 
	MARD_KSPEM ct_BlockedConsgnStock, 
	MARD_KINSM ct_ConsgnStockInQInsp, 
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock, 
	MARD_RETME ct_BlockedStockReturns, 
	0 amt_StockValueAmt,
	0 amt_StockValueAmt_GBL,
	0 amt_BlockedStockAmt,
	0 amt_BlockedStockAmt_GBL,
	0 amt_StockInQInspAmt,
	0 amt_StockInQInspAmt_GBL,
	0 amt_StockInTransferAmt,
	0 amt_StockInTransferAmt_GBL,
	0 amt_UnrestrictedConsgnStockAmt,
	0 amt_UnrestrictedConsgnStockAmt_GBL,
	0 amt_StdUnitPrice,
	0 amt_StdUnitPrice_GBL,
	0 amt_OnHand,
	0 amt_OnHand_GBL,
	0 LastReceivedQty, 
	a.BatchNumber BatchNo, 
	'Not Set' ValuationType,
	'Not Set' dd_DocumentNo, 
	0 dd_DocumentItemNo, 
	'Not Set' MovementType,
	ifnull((SELECT lrd.Dim_Dateid FROM tmp_tbl_StorageLocEntryDate c2 inner join dim_date lrd on lrd.DateValue = c2.LatestPostingDate 
		WHERE c2.PartNumber = a.PartNumber AND c2.PlantCode = a.PlantCode and lrd.CompanyCode = p.CompanyCode), sled.Dim_Dateid) StorageLocEntryDate,
	1 LastReceivedDate,
	Dim_Partid, p.dim_plantid, Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po where po.PurchaseOrgCode = p.PurchOrg), 1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid, 1 Dim_MovementIndicatorid, 1 dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over () , 
	1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
			and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL	
     FROM tmp_MARD_TotStk_001 a 
	  INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
          INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
          INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode 
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON varchar(dc.CurrencyCode,10) = varchar(c.Currency, 10),
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
    WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid 
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and c2.dd_BatchNo = a.BatchNumber);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

 UPDATE NUMBER_FOUNTAIN 
 SET max_id = ifnull((select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 WHERE table_name = 'fact_inventoryaging_tmp_populate';
 
/***********************LK: 26 Apr change : Insert queries 7/8/9/10/11 Starts ***********************/

/*Insert 7 */

/* Temporary tables to be used in inserts 7 to 11 */

/* LastReceivedQty */

/* Store mm and datevalue in a tmp table */

DROP TABLE IF EXISTS TMP_fact_mm_0;
CREATE TABLE TMP_fact_mm_0 
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'O';

/* Only retrive the min dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_1;
CREATE TABLE TMP_fact_mm_1 
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,min(mm.DateValue) as min_DateValue
from TMP_fact_mm_0 mm 
GROUP by mm.Dim_Partid,mm.Dim_Vendorid;

/* Now get the ct_QtyEntryUOM corresponding to the min datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_2;
CREATE TABLE TMP_fact_mm_2
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,mm.ct_Quantity ct_QtyEntryUOM 
FROM TMP_fact_mm_0 mm, TMP_fact_mm_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.min_DateValue;


/* StorageLocEntryDate/LastReceivedDate */

/* Simply use the posting date id for the min/max date */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0 
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where  mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'O';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm 
GROUP by mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.min_DateValue;

\i /db/schema_migration/bin/wrapper_optimizedb.sh TMP_fact_mm_postingdate_0;
\i /db/schema_migration/bin/wrapper_optimizedb.sh TMP_fact_mm_postingdate_1;
\i /db/schema_migration/bin/wrapper_optimizedb.sh TMP_fact_mm_postingdate_2;
\i /db/schema_migration/bin/wrapper_optimizedb.sh TMP_fact_mm_postingdate_2_min;

drop table if exists fitp_tmp_p01;
create table fitp_tmp_p01 as 
select  *,
	Integer(0) Dim_Partid_upd,
	Integer(0) Dim_Vendorid_upd,
	varchar(null,20) PurchOrg_upd  
from fact_inventoryaging_tmp_populate where 1=2;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MSLB';

INSERT INTO fitp_tmp_p01(ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL,
				Dim_Partid_upd,
				Dim_Vendorid_upd,
				PurchOrg_upd,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
								
 SELECT  a.MSLB_LBLAB ct_StockQty,
          a.MSLB_LBEIN ct_TotalRestrictedStock,
          a.MSLB_LBINS ct_StockInQInsp,
          CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END  StockValueAmt,
          ifnull(((CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END) 
		* ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)  StockValueAmt_GBL,
          (b.multiplier * a.MSLB_LBINS)	 amt_StockInQInspAmt,
          ifnull(((b.multiplier * a.MSLB_LBINS)	 
		* ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)  amt_StockInQInspAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
	CASE WHEN MSLB_LBLAB+MSLB_LBINS+MSLB_LBEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END 
			+ CASE WHEN MSLB_LBINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBINS) END
			+ CASE WHEN MSLB_LBEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBEIN) END
		  ) END amt_OnHand,
	ifnull((CASE WHEN MSLB_LBLAB+MSLB_LBINS+MSLB_LBEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END 
			+ CASE WHEN MSLB_LBINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBINS) END
			+ CASE WHEN MSLB_LBEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBEIN) END
		  ) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)
	amt_OnHand_GBL,
	  1 LastReceivedQty,
          a.MSLB_CHARG BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
	  1 StorageLocEntryDate,
	  1 LastReceivedDate,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          v.dim_vendorid,
          dim_currencyid,
          1 dim_stocktypeid,
          ifnull(( select mm.dim_specialstockid
                  from dim_specialstock mm	
		  WHERE mm.specialstockindicator = a.MSLB_SOBKZ),1) dim_specialstockid,
	 1 Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
	  stkc.dim_StockCategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), /* table name is correct here do not change it to fitp_tmp_p01 */ 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1),
	dp.Dim_Partid Dim_Partid_upd,
	v.Dim_Vendorid Dim_Vendorid_upd,
	p.PurchOrg PurchOrg_upd,
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL
     FROM MSLB a
          INNER JOIN dim_plant p ON a.MSLB_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSLB_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSLB_MATNR AND dp.Plant = a.MSLB_WERKS
          INNER JOIN dim_vendor v ON v.VendorNumber = MSLB_LIFNR
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency, tmp_GlobalCurr_001 gbl,
          tmp_stkcategory_001  stkc;		

Update fitp_tmp_p01 fitp
Set dim_StorageLocEntryDateid = ifnull(( select mm.dim_DateIDPostingDate
					from TMP_fact_mm_postingdate_2_min mm /* This has min date */
					WHERE mm.Dim_Partid = fitp.Dim_Partid_upd and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd ),1);

Update fitp_tmp_p01 fitp
Set dim_LastReceivedDateid = ifnull(( select mm.dim_DateIDPostingDate
					from TMP_fact_mm_postingdate_2 mm     /* This has max date */
					WHERE mm.Dim_Partid = fitp.Dim_Partid_upd and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd ),1);

Update fitp_tmp_p01 fitp
Set ct_LastReceivedQty = ifnull((select mm.ct_QtyEntryUOM
				from TMP_fact_mm_2 mm
                                  where mm.Dim_Partid = fitp.Dim_Partid_upd and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd ),1);

Update fitp_tmp_p01 fitp
Set Dim_PurchaseOrgid = ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
				where po.PurchaseOrgCode = fitp.PurchOrg_upd),1);

Insert into fact_inventoryaging_tmp_populate(ct_StockQty,
		ct_TotalRestrictedStock,
		ct_StockInQInsp,
		amt_StockValueAmt,
		amt_StockValueAmt_GBL,
		amt_StockInQInspAmt,
		amt_StockInQInspAmt_GBL,
		amt_StdUnitPrice,
		amt_StdUnitPrice_GBL,
		amt_OnHand,
		amt_OnHand_GBL,
		ct_LastReceivedQty,
		dd_BatchNo,
		dd_DocumentNo,
		dd_DocumentItemNo,
		dim_StorageLocEntryDateid,
		dim_LastReceivedDateid,
		dim_Partid,
		dim_Plantid,
		dim_StorageLocationid,
		dim_Companyid,
		dim_Vendorid,
		dim_Currencyid,
		dim_StockTypeid,
		dim_specialstockid,
		Dim_PurchaseOrgid,
		Dim_PurchaseGroupid,
		dim_producthierarchyid,
		Dim_UnitOfMeasureid,
		Dim_MovementIndicatorid,
		dim_CostCenterid,
		dim_stockcategoryid,
		fact_inventoryagingid,
		Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
		Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
		amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
		amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
		amt_ExchangeRate_GBL,
		amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
Select ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
from fitp_tmp_p01;

drop table if exists fitp_tmp_p01;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;
 
/*Insert 8 */
/* -- Vendor Stocks with no valuation	*/
INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid, 
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
  SELECT  a.MSLB_LBLAB ct_StockQty,
          a.MSLB_LBEIN ct_TotalRestrictedStock,
          a.MSLB_LBINS ct_StockInQInsp,
          0 StockValueAmt,
          0 StockValueAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
	  0 amt_OnHand,
	  0 amt_OnHand_GBL,
         ifnull((select mm.ct_QtyEntryUOM
                  from TMP_fact_mm_2 mm 
		  where mm.Dim_Partid = dp.Dim_Partid and mm.Dim_Vendorid = v.Dim_Vendorid
                  ),1) LastReceivedQty,
          a.MSLB_CHARG BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2_min mm	/* This has min date */
		  WHERE mm.Dim_Partid = dp.Dim_Partid and mm.Dim_Vendorid = v.Dim_Vendorid ),1) StorageLocEntryDate,
          ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2 mm	/* This has max date */
		  WHERE mm.Dim_Partid = dp.Dim_Partid and mm.Dim_Vendorid = v.Dim_Vendorid ),1) LastReceivedDate,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          v.dim_vendorid,
          dim_currencyid,
          1 dim_stocktypeid,
          ifnull(( select mm.dim_specialstockid
                  from dim_specialstock mm	
		  WHERE mm.specialstockindicator = a.MSLB_SOBKZ),1) dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_StockCategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL
     FROM MSLB a
          INNER JOIN dim_plant p
             ON a.MSLB_WERKS = p.PlantCode
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSLB_MATNR AND dp.Plant = a.MSLB_WERKS
          INNER JOIN dim_vendor v
             ON v.VendorNumber = MSLB_LIFNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency, tmp_GlobalCurr_001 gbl,
          tmp_stkcategory_001  stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_inventoryaging_tmp_populate c2
           WHERE c2.Dim_Partid = dp.Dim_Partid
                AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_Vendorid = v.Dim_Vendorid);				
				
 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';
			
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;
	
/*Insert 9 */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0 
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'W';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm 
GROUP by mm.Dim_Partid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.min_DateValue;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MSKU';

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Customerid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
  SELECT  a.MSKU_KULAB ct_StockQty,
          a.MSKU_KUEIN ct_TotalRestrictedStock,
          a.MSKU_KUINS ct_StockInQInsp,
          CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3
               ELSE (b.multiplier * a.MSKU_KULAB)
          END StockValueAmt,
          ifnull(((CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3
               ELSE (b.multiplier * a.MSKU_KULAB)
          END) * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)
		  StockValueAmt_GBL,
          (b.multiplier * a.MSKU_KUINS) amt_StockInQInspAmt,
          ifnull(((b.multiplier * a.MSKU_KUINS) * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StockInQInspAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
	CASE WHEN MSKU_KULAB+MSKU_KUINS+MSKU_KUEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KULAB) END 
			+ CASE WHEN MSKU_KUINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUINS) END
			+ CASE WHEN MSKU_KUEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUEIN) END
		  ) END amt_OnHand,
	ifnull((CASE WHEN MSKU_KULAB+MSKU_KUINS+MSKU_KUEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KULAB) END 
			+ CASE WHEN MSKU_KUINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUINS) END
			+ CASE WHEN MSKU_KUEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUEIN) END
		  ) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)
	amt_OnHand_GBL,
          0.00 LastReceivedQty,
          a.MSKU_CHARG BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2_min mm	/* This has min date */
		  WHERE mm.Dim_Partid = dp.Dim_Partid),1) StorageLocEntryDate,
          ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2 mm	/* This has max date */
		  WHERE mm.Dim_Partid = dp.Dim_Partid),1) LastReceivedDate,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          cm.dim_customerid,
          dim_currencyid,
          1 dim_stocktypeid,
          ifnull(( select mm.dim_specialstockid
                  from dim_specialstock mm	
		  WHERE mm.specialstockindicator = a.MSKU_SOBKZ),1) dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL
     FROM MSKU a
          INNER JOIN dim_plant p
             ON a.MSKU_WERKS = p.PlantCode
         INNER JOIN tmp2_MBEW_NO_BWTAR b ON     b.MATNR = a.MSKU_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKU_MATNR AND dp.Plant = a.MSKU_WERKS
          INNER JOIN dim_customer cm
             ON cm.CustomerNumber = MSKU_KUNNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc;
				
 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

/*Insert 10 */
/* -- Customer stocks with no valuation	*/
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Customerid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
  SELECT  a.MSKU_KULAB ct_StockQty,
          a.MSKU_KUEIN ct_TotalRestrictedStock,
          a.MSKU_KUINS ct_StockInQInsp,
          0 StockValueAmt,
          0 StockValueAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          0 amt_OnHand,
          0 amt_OnHand_GBL,
          0.00 LastReceivedQty,
          a.MSKU_CHARG BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2_min mm	/* This has min date */
		  WHERE mm.Dim_Partid = dp.Dim_Partid),1) StorageLocEntryDate,
          ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2 mm	/* This has max date */
		  WHERE mm.Dim_Partid = dp.Dim_Partid),1) LastReceivedDate,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          cm.dim_customerid,
          dim_currencyid,
          1 dim_stocktypeid,
          ifnull(( select mm.dim_specialstockid
                  from dim_specialstock mm	
		  WHERE mm.specialstockindicator = a.MSKU_SOBKZ),1) dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
			1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  pGlobalCurrency ),1) dim_Currencyid_GBL
     FROM MSKU a
          INNER JOIN dim_plant p
             ON a.MSKU_WERKS = p.PlantCode
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKU_MATNR AND dp.Plant = a.MSKU_WERKS
          INNER JOIN dim_customer cm
             ON cm.CustomerNumber = MSKU_KUNNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_inventoryaging_tmp_populate c2
           WHERE c2.Dim_Partid = dp.Dim_Partid
                AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_CustomerId = cm.Dim_CustomerId);

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
						where table_name = 'fact_inventoryaging_tmp_populate';

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.Dim_Partid, mm.Dim_StorageLocationid, mmdt.Dim_Dateid,
	row_number() over(partition by mm.Dim_Partid, mm.Dim_StorageLocationid order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate;

UPDATE fact_inventoryaging_tmp_populate f
SET f.dim_dateidtransaction = IFNULL((select mmdt.Dim_Dateid
                                          from tmp_custom_postproc_dim_date mmdt
					  where mmdt.Dim_Partid = f.dim_Partid and mmdt.Dim_StorageLocationid = f.dim_StorageLocationid
						and mmdt.RowSeqNo = 1),1)
WHERE f.dim_stockcategoryid in (2,3);

/*Insert 11 */

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm 
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = 641;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MARC';

	
	
/* LK: 12 Sep - Done till here. Do fact_purchase before coming back here */	
	
/* # INTRA stock analytics */

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (fp.ct_QtyIssued - (CASE WHEN fp.ct_ReceivedQty < 0 THEN -1*fp.ct_ReceivedQty ELSE fp.ct_ReceivedQty END))
		* (fp.PriceConversion_EqualTo / fp.PriceConversion_Denom) ct_StockInTransit,
          fp.dd_DocumentNo,
          fp.dd_DocumentItemNo,
          fp.Dim_Partid,
          fp.dim_currencyid,	/* Local currency */
		  fp.Dim_currencyid Dim_CurrencyId_TRA,		/* inventoryaging should have tran curr same as local curr */
		  fp.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,	
		  1 amt_ExchangeRate,	/* Tran->Local exch rate is 1 */
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          fp.Dim_PlantidSupplying, 
	  (fp.amt_ExchangeRate_GBL/case when fp.amt_ExchangeRate = 0 THEN 1 ELSE fp.amt_ExchangeRate END) amt_ExchangeRate_GBL,	
	  /*LK: amt_ExchangeRate_GBL : (Tran to Global) * (Local to Tran) = (Tran to Global)/(Tran to Local)  = Local to Global */
	  (fp.amt_UnitPrice * case when fp.amt_ExchangeRate = 0 THEN 1 ELSE fp.amt_ExchangeRate END) amt_UnitPrice	/* LK: fp in tran, ivntryaging in local */
     FROM fact_purchase fp
          INNER JOIN dim_purchasemisc pm ON fp.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
          INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = fp.Dim_Currencyid
     WHERE CASE WHEN fp.ct_ReceivedQty < 0 THEN -1*fp.ct_ReceivedQty ELSE fp.ct_ReceivedQty END < fp.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and fp.Dim_PlantidSupplying <> 1 and fp.Dim_Vendorid = 1;

  INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
   SELECT fp.ct_StockInTransit,
          (b.multiplier * fp.ct_StockInTransit) amt_StockInTransitAmt,
          (b.multiplier * fp.ct_StockInTransit) * fp.amt_ExchangeRate_GBL amt_StockInTransitAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          b.multiplier * fp.amt_ExchangeRate_GBL amt_StdUnitPrice_GBL,
          (b.multiplier * fp.ct_StockInTransit) amt_OnHand,
          0 amt_OnHand_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          fp.dd_DocumentNo dd_DocumentNo,
          fp.dd_DocumentItemNo dd_DocumentItemNo,
          ifnull((select mmdt.Dim_Dateid
                  from tmp_custom_postproc_dim_date mmdt
                  where mmdt.dd_DocumentNo = fp.dd_DocumentNo and mmdt.dd_DocumentItemNo = fp.dd_DocumentItemNo
			and mmdt.RowSeqNo = 1),1)  StorageLocEntryDate,
          1 LastReceivedDate,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          fp.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          fp.Dim_PlantidSupplying, 
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	  1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
	  1 amt_ExchangeRate,
	  fp.dim_currencyid_TRA,
	  fp.dim_currencyid_GBL
     FROM dim_plant p
          INNER JOIN MARC a ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN tmp_fact_fp_001 fp ON fp.Dim_Partid = dp.Dim_Partid
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency, 
 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc;

/********* Replaced with above ********
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                ct_StockInTransfer,
                                amt_StockInTransferAmt,
                                amt_StockInTransferAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
   SELECT ifnull(MARC_TRAME,0) ct_StockInTransit,
          ifnull((b.multiplier * MARC_TRAME),0) amt_StockInTransitAmt,
          ifnull(b.multiplier * MARC_TRAME
	   * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0)
            amt_StockInTransitAmt_GBL,
          ifnull(MARC_UMLMC, 0) ct_StockInTransfer,
          ifnull((b.multiplier * MARC_UMLMC),0) amt_StockInTransferAmt,
          ifnull(b.multiplier * MARC_UMLMC
           * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0) 
	    amt_StockInTransferAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          1  StorageLocEntryDate,
          1 LastReceivedDate,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          1,
          ifnull(pg.Dim_PurchaseGroupid,1),
          ifnull(dph.dim_producthierarchyid,1),
          1,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          1,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	  1,1,1,1,1,1, 1,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)
     FROM dim_plant p
          INNER JOIN MARC a ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
********************************************/

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

 
  /*# INTER stock analytics */

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm 
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = 643;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'STO';

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (a.ct_QtyIssued - (CASE WHEN a.ct_ReceivedQty < 0 THEN -1*a.ct_ReceivedQty ELSE a.ct_ReceivedQty END))
		* (a.PriceConversion_EqualTo / a.PriceConversion_Denom) ct_StockInTransit,
          a.amt_StdUnitPrice,
	  a.amt_UnitPrice,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
          a.Dim_Partid,
          a.dim_plantidordering,
          a.dim_currencyid,
		  a.Dim_currencyid Dim_CurrencyId_TRA,			
		  a.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,	
		  1 amt_ExchangeRate,	 
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          a.Dim_PlantIdSupplying, 
	     (a.amt_ExchangeRate_GBL/case when a.amt_ExchangeRate = 0 THEN 1 ELSE a.amt_ExchangeRate END) amt_ExchangeRate_GBL
     FROM fact_purchase a
          INNER JOIN dim_purchasemisc pm ON a.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
     WHERE CASE WHEN a.ct_ReceivedQty < 0 THEN -1*a.ct_ReceivedQty ELSE a.ct_ReceivedQty END < a.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and a.Dim_PlantidSupplying <> 1
           and not exists (select 1 from fact_inventoryaging_tmp_populate fi inner join dim_stockcategory sc on fi.dim_stockcategoryid = sc.Dim_StockCategoryid
                          where fi.dim_Partid = a.dim_partid and fi.dd_DocumentNo = a.dd_DocumentNo and sc.CategoryCode = 'MARC');

drop table if exists fitp_ui0;
create table fitp_ui0 as Select * from fact_inventoryaging_tmp_populate where 1=2;

  INSERT INTO fitp_ui0(ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
   SELECT a.ct_StockInTransit,
          (b.multiplier * a.ct_StockInTransit) amt_StockInTransitAmt,
          (b.multiplier * a.ct_StockInTransit * a.amt_ExchangeRate_GBL) amt_StockInTransitAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          (b.multiplier * a.amt_ExchangeRate_GBL)  amt_StdUnitPrice_GBL,
          (b.multiplier * a.ct_StockInTransit) amt_OnHand,
          0 amt_OnHand_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
	  1 StorageLocEntryDate,
          1 LastReceivedDate,
          a.Dim_Partid,
          a.dim_plantidordering,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          a.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          a.Dim_PlantIdSupplying, 
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
	  a.amt_ExchangeRate, 
	  a.dim_Currencyid_TRA,
	  a.dim_Currencyid_GBL
     FROM tmp_fact_fp_001 a
          INNER JOIN dim_plant p ON a.dim_plantidordering = p.dim_plantid
          INNER JOIN dim_part dp ON dp.dim_partid = a.dim_partid
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = dp.PartNumber AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency,
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc;

Update fitp_ui0 a
Set dim_StorageLocEntryDateid= ifnull((select mmdt.Dim_Dateid
                   from tmp_custom_postproc_dim_date mmdt
                   where mmdt.dd_DocumentNo = a.dd_DocumentNo and mmdt.dd_DocumentItemNo = a.dd_DocumentItemNo
                         and mmdt.RowSeqNo = 1),1);


Insert into fact_inventoryaging_tmp_populate(ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
                                fact_inventoryagingid,
                                amt_ExchangeRate_GBL,
                                amt_ExchangeRate, 
				dim_Currencyid_TRA,
				dim_Currencyid_GBL)
Select ct_StockInTransit,
	amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
        amt_OnHand,
        amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	Dim_SupplyingPlantId,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, 
	dim_Currencyid_TRA,
	dim_Currencyid_GBL
from fitp_ui0;

drop table if exists fitp_ui0;


update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';


/*********************** LK: 26 Apr change : Insert queries 7/8/9/10/11 Ends ***********************/

/* Update 1 */

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
and ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x 
			       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
				     AND ((x.VPRSV = 'S' AND x.STPRS > 0) OR (x.VPRSV = 'V' AND x.VERPR > 0)))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,
((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, 
m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.STPRS / b.PEINH), 18,5)
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.VERPR / b.PEINH), 18,5)
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

drop table if exists pl_prt_MBEW_NO_BWTAR;
create table pl_prt_MBEW_NO_BWTAR
as
select distinct ia.dim_partid,ia.dim_plantid,prt.PartNumber,pl.ValuationArea,pl.CompanyCode
from fact_inventoryaging_tmp_populate ia,dim_plant pl,dim_part prt
WHERE ia.dim_partid = prt.Dim_partid
 and ia.dim_plantid = pl.dim_plantid;

UPDATE fact_inventoryaging_tmp_populate ia
FROM  tmp2_MBEW_NO_BWTAR m,
        pl_prt_MBEW_NO_BWTAR pl
  set amt_CommericalPrice1 = ifnull(MBEW_BWPRH,0),
      amt_CurPlannedPrice = ifnull(MBEW_LPLPR,0),
      amt_MovingAvgPrice = ifnull(MBEW_VMVER,0),
      amt_PlannedPrice1 = ifnull(MBEW_ZPLP1,0),
      amt_PreviousPrice = ifnull(MBEW_STPRV,0),
      amt_PrevPlannedPrice = ifnull(MBEW_VPLPR,0),
      Dim_DateIdLastChangedPrice = ifnull((SELECT dt.Dim_Dateid from dim_Date dt
                                            WHERE dt.DateValue = MBEW_LAEPR AND dt.CompanyCode = pl.CompanyCode ),1),
      Dim_DateIdPlannedPrice2 = ifnull((SELECT dt.Dim_Dateid from dim_Date dt
                                            WHERE dt.DateValue = MBEW_ZPLD2 AND dt.CompanyCode = pl.CompanyCode ),1),
      amt_MtlDlvrCost = ifnull((CASE m.VPRSV WHEN 'S' THEN m.STPRS / m.PEINH
                                WHEN 'V' THEN m.VERPR / m.PEINH END), 0)
  WHERE ia.dim_partid = pl.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and pl.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* 2nd and 3rd updates	*/

UPDATE fact_inventoryaging_tmp_populate ia
FROM fact_purchase fp
   SET ia.Dim_ConsumptionTypeid = fp.Dim_ConsumptionTypeid,
       ia.Dim_DocumentStatusid = fp.Dim_DocumentStatusid,
       ia.Dim_DocumentTypeid = fp.Dim_DocumentTypeid,
       ia.Dim_IncoTermid = fp.Dim_IncoTerm1id,
       ia.Dim_ItemCategoryid = fp.Dim_ItemCategoryid,
       ia.Dim_ItemStatusid = fp.Dim_ItemStatusid,
       ia.Dim_Termid = fp.Dim_Termid,
       ia.Dim_PurchaseMiscid = fp.Dim_PurchaseMiscid,
       ia.Dim_SupplyingPlantId = fp.Dim_PlantidSupplying
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo;

/* COMMENT - THIS UPDATE NEEDS TO BE CHECKED AGAIN.  */

UPDATE fact_purchase fp
FROM fact_inventoryaging_tmp_populate ia
   SET fp.ct_UnrestrictedOnHandQty = ia.ct_StockQty
 WHERE ia.dim_Partid = fp.Dim_Partid
       AND ia.dim_Plantid = fp.Dim_PlantidSupplying
	AND fp.ct_UnrestrictedOnHandQty <> ia.ct_StockQty;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* Update 4	*/
/* This update is using up a lot of memory on VW and does not complete */

drop table if exists tmp_keph;
CREATE TABLE tmp_keph 
AS 
SELECT KEPH_KALNR,KEPH_BWVAR,KEPH_KADKY,KEPH_KST001,k.KEPH_KST002,k.KEPH_KST003 + k.KEPH_KST004 + k.KEPH_KST005 + k.KEPH_KST006 + k.KEPH_KST007 + k.KEPH_KST008
                        + k.KEPH_KST009 + k.KEPH_KST010 + k.KEPH_KST011 + k.KEPH_KST012 + k.KEPH_KST013 + k.KEPH_KST014
                        + k.KEPH_KST015 + k.KEPH_KST016 + k.KEPH_KST017 + k.KEPH_KST018 + k.KEPH_KST019 + k.KEPH_KST020
                        + k.KEPH_KST021 + k.KEPH_KST022 + k.KEPH_KST023 + k.KEPH_KST024 + k.KEPH_KST025 + k.KEPH_KST026
                        + k.KEPH_KST027 as keph_othercosts, k.KEPH_KST001 + k.KEPH_KST002 as keph_allcosts
FROM keph k;

update tmp_keph
set keph_allcosts = keph_allcosts + keph_othercosts;

drop table if exists tmp_dim_date;
create table tmp_dim_date as
select DateValue,FinancialYear,FinancialMonthNumber,(FinancialYear * 100) + FinancialMonthNumber as yymm,CompanyCode from dim_date;

/*Use intermediate table ia1 as this was failing in fossil2 */

drop table if exists ia1;
create table ia1 as
select distinct ia.dim_Partid,ia.dim_plantid,pl.CompanyCode,pl.ValuationArea,p.PartNumber
from fact_inventoryaging_tmp_populate ia,dim_part p, dim_plant pl
where  ia.dim_Partid = p.Dim_Partid and ia.dim_plantid = pl.dim_plantid;

drop table if exists tmp3_MBEW_NO_BWTAR;
create table tmp3_MBEW_NO_BWTAR as
select distinct ia.dim_Partid,ia.dim_plantid,ia.CompanyCode, m.MATNR,m.BWKEY,m.MBEW_KALN1,m.MBEW_BWVA2,((m.MBEW_PDATL * 100) + m.MBEW_PPRDL) as m_yymm,m.multiplier
from ia1 ia, tmp2_MBEW_NO_BWTAR m
where ia.PartNumber = m.MATNR and ia.ValuationArea = m.BWKEY;

/*Create an intermediate table, which would decrease the number of rows read from keph */

drop table if exists tmp4_MBEW_NO_BWTAR;
create table tmp4_MBEW_NO_BWTAR as
select distinct m.*
from tmp3_MBEW_NO_BWTAR m,tmp_dim_date dt
where dt.yymm = m.m_yymm;

drop table if exists tmp4_dim_date;
create table tmp4_dim_date as
select distinct dt.*
from tmp4_MBEW_NO_BWTAR m,tmp_dim_date dt
where dt.yymm = m.m_yymm;

drop table if exists tmp2_keph;
create table tmp2_keph as
select DISTINCT m.dim_Partid,m.dim_plantid,m.CompanyCode,m.m_yymm,k.KEPH_KST001,k.KEPH_KST002,k.keph_othercosts,k.keph_allcosts,k.KEPH_KADKY,k.KEPH_KALNR,k.KEPH_BWVAR
FROM   tmp4_MBEW_NO_BWTAR m, tmp_keph k
where k.KEPH_KALNR = m.MBEW_KALN1 and k.KEPH_BWVAR = m.MBEW_BWVA2 and k.keph_allcosts = m.multiplier;

call vectorwise(combine 'tmp2_keph');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

UPDATE fact_inventoryaging_tmp_populate ia
	FROM tmp2_keph k,tmp4_dim_date dt
   SET ia.amt_MtlDlvrCost = k.KEPH_KST001,
       ia.amt_OverheadCost = k.KEPH_KST002,
       ia.amt_OtherCost = k.keph_othercosts
 WHERE ia.dim_Partid = k.Dim_Partid and ia.dim_plantid = k.dim_plantid
    and k.KEPH_KADKY = dt.DateValue and k.CompanyCode = dt.CompanyCode
    and dt.yymm = k.m_yymm;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* Issam change 24th Jun */
/* Update Purchase Order and Sales Order Open Qtys */

UPDATE fact_inventoryaging_tmp_populate ia
SET ct_POOpenQty = ifnull((SELECT SUM(case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (fpr.ct_DeliveryQty - fpr.ct_ReceivedQty) < 0 then 0.0000 else (fpr.ct_DeliveryQty - fpr.ct_ReceivedQty) end) 
					FROM fact_purchase fpr,
					Dim_PurchaseMisc atrb
					WHERE ia.dim_partid = fpr.Dim_partid
					AND fpr.Dim_PurchaseMiscid = atrb.Dim_PurchaseMiscid
					GROUP BY fpr.Dim_partid), 0),
	ct_SOOpenQty = ifnull((SELECT SUM(fso.ct_AfsOpenQty)
					FROM fact_salesorder fso
					WHERE ia.dim_partid = fso.Dim_partid), 0);

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* Update 5	*/

UPDATE dim_part
SET TotalPlantStock = ifnull((select sum(f_inv.ct_StockQty+f_inv.ct_StockInQInsp+f_inv.ct_BlockedStock+f_inv.ct_StockInTransfer)
                              from fact_inventoryaging_tmp_populate f_inv
                              where f_inv.dim_Partid = dim_part.Dim_Partid),0);
							  
UPDATE dim_part
SET TotalPlantStockAmt = ifnull((SELECT sum(f_inv.amt_StockValueAmt+f_inv.amt_StockInQInspAmt+f_inv.amt_BlockedStockAmt+f_inv.amt_StockInTransferAmt)
                              from fact_inventoryaging_tmp_populate f_inv
                              where f_inv.dim_Partid = dim_part.Dim_Partid),0);							  

call vectorwise(combine 'dim_part');

/* # WIP stock analytics */

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'WIP';

  INSERT INTO fact_inventoryaging_tmp_populate(ct_StockQty,
                               amt_StockValueAmt,
                               ct_LastReceivedQty,
                               dim_StorageLocEntryDateid,
                               dim_LastReceivedDateid,
                               dim_Partid,
                               dim_Plantid,
                               dim_StorageLocationid,
                               dim_Companyid,
                               dim_Vendorid,
                               dim_Currencyid,
				   dim_Currencyid_TRA,
				   dim_Currencyid_GBL,
                               dim_StockTypeid,
                               dim_specialstockid,
                               Dim_PurchaseOrgid,
                               Dim_PurchaseGroupid,
                               dim_producthierarchyid,
                               Dim_UnitOfMeasureid,
                               Dim_MovementIndicatorid,
                               Dim_ConsumptionTypeid,
                               dim_costcenterid,
                               Dim_DocumentStatusid,
                               Dim_DocumentTypeid,
                               Dim_IncoTermid,
                               Dim_ItemCategoryid,
                               Dim_ItemStatusid,
                               Dim_Termid,
                               Dim_PurchaseMiscid,
                               amt_StockValueAmt_GBL,
                               ct_TotalRestrictedStock,
                               ct_StockInQInsp,
                               ct_BlockedStock,
                               ct_StockInTransfer,
                               ct_UnrestrictedConsgnStock,
                               ct_RestrictedConsgnStock,
                               ct_BlockedConsgnStock,
                               ct_ConsgnStockInQInsp,
                               ct_BlockedStockReturns,
                               amt_BlockedStockAmt,
                               amt_BlockedStockAmt_GBL,
                               amt_StockInQInspAmt,
                               amt_StockInQInspAmt_GBL,
                               amt_StockInTransferAmt,
                               amt_StockInTransferAmt_GBL,
                               amt_UnrestrictedConsgnStockAmt,
                               amt_UnrestrictedConsgnStockAmt_GBL,
                               amt_StdUnitPrice,
                               amt_StdUnitPrice_GBL,
                               dim_stockcategoryid,
                               ct_StockInTransit,
                               amt_StockInTransitAmt,
                               amt_StockInTransitAmt_GBL,
                               Dim_SupplyingPlantId,
                               amt_MtlDlvrCost,
                               amt_OverheadCost,
                               amt_OtherCost,
                               amt_WIPBalance,
                               ct_WIPAging,
			       ct_WIPQty,
                               amt_MovingAvgPrice,
                               amt_PreviousPrice,
                               amt_CommericalPrice1,
                               amt_PlannedPrice1,
                               amt_PrevPlannedPrice,
                               amt_CurPlannedPrice,
                               Dim_DateIdPlannedPrice2,
                               Dim_DateIdLastChangedPrice,
                               Dim_DateidActualRelease,
                               Dim_DateidActualStart,
                               dd_ProdOrdernumber,
                               dd_ProdOrderitemno,
                               dd_Plannedorderno,
                               dim_productionorderstatusid,
                               Dim_productionordertypeid,
			       fact_inventoryagingid,
			       amt_ExchangeRate_GBL,amt_ExchangeRate)
  SELECT 0 ct_StockQty,
        0 amt_StockValueAmt,
        0 ct_LastReceivedQty,
        1 dim_StorageLocEntryDateid,
        1 dim_LastReceivedDateid,
        po.Dim_PartidItem dim_Partid,
        po.Dim_Plantid dim_Plantid,
        po.Dim_StorageLocationid dim_StorageLocationid,
        po.Dim_Companyid dim_Companyid,
        1 dim_Vendorid,
        po.Dim_Currencyid dim_Currencyid,
		po.Dim_Currencyid dim_Currencyid_TRA,
		po.Dim_Currencyid_GBL dim_Currencyid_GBL,
        po.Dim_StockTypeid dim_StockTypeid,
        po.dim_specialstockid dim_specialstockid,
        po.Dim_PurchaseOrgid Dim_PurchaseOrgid,
        1 Dim_PurchaseGroupid,
        1 dim_producthierarchyid,
        po.Dim_UnitOfMeasureid Dim_UnitOfMeasureid,
        1 Dim_MovementIndicatorid,
        po.Dim_ConsumptionTypeid Dim_ConsumptionTypeid,
        1 dim_costcenterid,
        1 Dim_DocumentStatusid,
        1 Dim_DocumentTypeid,
        1 Dim_IncoTermid,
        1 Dim_ItemCategoryid,
        1 Dim_ItemStatusid,
        1 Dim_Termid,
        1 Dim_PurchaseMiscid,
        0 amt_StockValueAmt_GBL,
        0 ct_TotalRestrictedStock,
        0 ct_StockInQInsp,
        0 ct_BlockedStock,
        0 ct_StockInTransfer,
        0 ct_UnrestrictedConsgnStock,
        0 ct_RestrictedConsgnStock,
        0 ct_BlockedConsgnStock,
        0 ct_ConsgnStockInQInsp,
        0 ct_BlockedStockReturns,
        0 amt_BlockedStockAmt,
        0 amt_BlockedStockAmt_GBL,
        0 amt_StockInQInspAmt,
        0 amt_StockInQInspAmt_GBL,
        0 amt_StockInTransferAmt,
        0 amt_StockInTransferAmt_GBL,
        0 amt_UnrestrictedConsgnStockAmt,
        0 amt_UnrestrictedConsgnStockAmt_GBL,
        0 amt_StdUnitPrice,
        0 amt_StdUnitPrice_GBL,
        stkc.dim_stockcategoryid,
        0 ct_StockInTransit,
        0 amt_StockInTransitAmt,
        0 amt_StockInTransitAmt_GBL,
        1 Dim_SupplyingPlantId,
        0 amt_MtlDlvrCost,
        0 amt_OverheadCost,
        0 amt_OtherCost,
        po.amt_WIPBalance * po.amt_ExchangeRate amt_WIPBalance,
	ifnull((ANSIDATE(LOCAL_TIMESTAMP) - (select pard.DateValue from dim_date pard
						where pard.Dim_Dateid = po.Dim_DateidActualRelease and po.Dim_DateidActualRelease > 1)),0)  ct_WIPAging,
	ifnull(po.ct_WIPQty,0),
        0 amt_MovingAvgPrice,
        0 amt_PreviousPrice,
        0 amt_CommericalPrice1,
        0 amt_PlannedPrice1,
        0 amt_PrevPlannedPrice,
        0 amt_CurPlannedPrice,
        1 Dim_DateIdPlannedPrice2,
        1 Dim_DateIdLastChangedPrice,
        po.Dim_DateidActualRelease Dim_DateidActualRelease,
        po.Dim_DateidActualStart Dim_DateidActualStart,
        po.dd_ordernumber dd_ProdOrdernumber,
        po.dd_orderitemno dd_ProdOrderitemno,
        po.dd_plannedorderno dd_Plannedorderno,
        po.dim_productionorderstatusid dim_productionorderstatusid,
        po.Dim_ordertypeid Dim_productionordertypeid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	(po.amt_ExchangeRate_GBL/case when po.amt_ExchangeRate = 0 THEN 1 ELSE po.amt_ExchangeRate END) amt_ExchangeRate_GBL,	/* LK: GBL exchange rate for inventoryaging is local-->global */
	1 amt_ExchangeRate			
  FROM fact_productionorder po 
	INNER JOIN dim_productionorderstatus ost ON po.dim_productionorderstatusid = ost.dim_productionorderstatusid
        INNER JOIN dim_part p ON po.Dim_PartidItem = p.dim_partid
        INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = po.Dim_Currencyid, 
	tmp_stkcategory_001 stkc
  WHERE ost.Closed = 'Not Set' 
      and ost.Delivered = 'Not Set'
      and ost.Created = 'Not Set' 
      and ost.TechnicallyCompleted = 'Not Set'
      and po.ct_OrderItemQty - po.ct_GRQty > 0;

DELETE FROM fact_materialmovement_tmp_invagin;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;


/* Begin 15 Oct 2013 changes */
Update fact_inventoryaging_tmp_populate fi
SET ct_GRQty_Late30 = IFNULL((SELECT SUM(ct_Quantity) FROM fact_materialmovement fmm JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
								JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
								WHERE fmm.dim_partid = fi.dim_partid AND mt.movementtype IN (101, 501) AND pd.DateValue >= (current_date - INTERVAL '30' DAY)
								GROUP BY fmm.dim_partid), 0);
								
Update fact_inventoryaging_tmp_populate fi
SET ct_GRQty_31_60 = IFNULL((SELECT SUM(ct_Quantity) FROM fact_materialmovement fmm JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
								JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
								WHERE fmm.dim_partid = fi.dim_partid AND mt.movementtype IN (101, 501) 
								AND pd.DateValue >= (current_date - INTERVAL '60' DAY) AND pd.DateValue <= (current_date - INTERVAL '31' DAY)
								GROUP BY fmm.dim_partid), 0);	

Update fact_inventoryaging_tmp_populate fi
SET ct_GRQty_61_90 = IFNULL((SELECT SUM(ct_Quantity) FROM fact_materialmovement fmm JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
								JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
								WHERE fmm.dim_partid = fi.dim_partid AND mt.movementtype IN (101, 501) 
								AND pd.DateValue >= (current_date - INTERVAL '90' DAY) AND pd.DateValue <= (current_date - INTERVAL '61' DAY)
								GROUP BY fmm.dim_partid), 0);

Update fact_inventoryaging_tmp_populate fi
SET ct_GRQty_91_180 = IFNULL((SELECT SUM(ct_Quantity) FROM fact_materialmovement fmm JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
								JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
								WHERE fmm.dim_partid = fi.dim_partid AND mt.movementtype IN (101, 501) 
								AND pd.DateValue >= (current_date - INTERVAL '180' DAY) AND pd.DateValue <= (current_date - INTERVAL '91' DAY)
								GROUP BY fmm.dim_partid), 0);								
								
Update fact_inventoryaging_tmp_populate fi
SET ct_GIQty_Late30 = IFNULL((SELECT SUM(ct_Quantity) FROM fact_materialmovement fmm JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
								JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
								WHERE fmm.dim_partid = fi.dim_partid AND mt.movementtype IN (201, 261, 301, 601, 641) AND pd.DateValue >= (current_date - INTERVAL '30' DAY)
								GROUP BY fmm.dim_partid), 0);

Update fact_inventoryaging_tmp_populate fi
SET ct_GIQty_31_60 = IFNULL((SELECT SUM(ct_Quantity) FROM fact_materialmovement fmm JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
								JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
								WHERE fmm.dim_partid = fi.dim_partid AND mt.movementtype IN (201, 261, 301, 601, 641) 
								AND pd.DateValue >= (current_date - INTERVAL '60' DAY) AND pd.DateValue <= (current_date - INTERVAL '31' DAY)
								GROUP BY fmm.dim_partid), 0);	

Update fact_inventoryaging_tmp_populate fi
SET ct_GIQty_61_90 = IFNULL((SELECT SUM(ct_Quantity) FROM fact_materialmovement fmm JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
								JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
								WHERE fmm.dim_partid = fi.dim_partid AND mt.movementtype IN (201, 261, 301, 601, 641) 
								AND pd.DateValue >= (current_date - INTERVAL '90' DAY) AND pd.DateValue <= (current_date - INTERVAL '61' DAY)
								GROUP BY fmm.dim_partid), 0);

Update fact_inventoryaging_tmp_populate fi
SET ct_GIQty_91_180 = IFNULL((SELECT SUM(ct_Quantity) FROM fact_materialmovement fmm JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
								JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
								WHERE fmm.dim_partid = fi.dim_partid AND mt.movementtype IN (201, 261, 301, 601, 641) 
								AND pd.DateValue >= (current_date - INTERVAL '180' DAY) AND pd.DateValue <= (current_date - INTERVAL '91' DAY)
								GROUP BY fmm.dim_partid), 0);
								
call vectorwise(combine 'fact_inventoryaging_tmp_populate');								
/* End 15 Oct 2013 changes */

update fact_inventoryaging_tmp_populate inv
from fact_purchase fp
set inv.dim_profitcenterid = fp.dim_profitcenterid
where fp.dd_documentno = inv.dd_documentno and fp.dd_documentitemno = inv.dd_documentitemno
	and inv.dim_profitcenterid = 1 and fp.dim_profitcenterid <> 1;



/* Sales Order stock analytics */


DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MSKA';

update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';

DELETE FROM MSKA WHERE MSKA_KAEIN = 0 and MSKA_KAINS = 0 and MSKA_KALAB = 0 and MSKA_KASPE = 0;

   /* Intermediate tables -- replace MBEW_NO_BWTAR  */
drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = decimal((b.STPRS / b.PEINH), 18, 5)
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.VERPR / b.PEINH), 18, 5)
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;


  INSERT INTO dim_storagelocation(Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent,dim_storagelocationid)
   SELECT DISTINCT MSKA_LGORT,
                   MSKA_LGORT,
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   MSKA_WERKS,
                   'Not Set',
                   date(LOCAL_TIMESTAMP),
                   1,row_number() over ()	--This should be changed later ( to max + 1 ). This should be standard - common for all such scenarios
     FROM MSKA m
    WHERE MSKA_LGORT IS NOT NULL
          AND NOT EXISTS (SELECT 1
                            FROM dim_storagelocation
                           WHERE LocationCode = MSKA_LGORT AND Plant = MSKA_WERKS);

		
/* StorageLocEntryDate/LastReceivedDate */

/* Simply use the posting date id for the min/max date */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0 
AS
SELECT mm.*,mmdt.DateValue,mt.MovementType
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'E';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm 
GROUP by mm.Dim_Partid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate,mm.Dim_MovementTypeid,mm.MovementType,
	mm.dim_specialstockid, mm.Dim_MovementIndicatorid,
	ROW_NUMBER() OVER(PARTITION BY mm.Dim_Partid ORDER BY mm.DateValue DESC) RowSeqNo
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate,mm.Dim_MovementTypeid,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.min_DateValue;

  INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                ct_BlockedStock,
                                ct_StockInTransfer,
                                ct_BlockedConsgnStock,
                                ct_ConsgnStockInQInsp,
                                ct_RestrictedConsgnStock,
                                ct_UnrestrictedConsgnStock,
                                ct_BlockedStockReturns,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_BlockedStockAmt,
                                amt_BlockedStockAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StockInTransferAmt,
                                amt_StockInTransferAmt_GBL,
                                amt_UnrestrictedConsgnStockAmt,
                                amt_UnrestrictedConsgnStockAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_ValuationType,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dd_MovementType,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
				dim_Currencyid_TRA,
				dim_Currencyid_GBL,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_StockCategoryid,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL,amt_ExchangeRate)
  SELECT a.MSKA_KALAB StockQty,
          a.MSKA_KAEIN ct_TotalRestrictedStock,
          a.MSKA_KAINS ct_StockInQInsp,
          a.MSKA_KASPE ct_BlockedStock,
          0 ct_StockInTransfer,
          0 ct_BlockedConsgnStock,
          0 ct_ConsgnStockInQInsp,
          0 ct_RestrictedConsgnStock,
          0 ct_UnrestrictedConsgnStock,
          0 ct_BlockedStockReturns,
          (b.multiplier * a.MSKA_KALAB) StockValueAmt,
          ifnull((b.multiplier * a.MSKA_KALAB) 
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1),0) StockValueAmt_GBL,
          ifnull((b.multiplier * a.MSKA_KASPE), 0) amt_BlockedStockAmt,
          ifnull((b.multiplier * a.MSKA_KASPE)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1),0) amt_BlockedStockAmt_GBL,
          ifnull((b.multiplier * a.MSKA_KAINS), 0) amt_StockInQInspAmt,
          ifnull((b.multiplier * a.MSKA_KAINS)
		* ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1),0) amt_StockInQInspAmt_GBL,
          0 amt_StockInTransferAmt,
          0 amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt,
          0 amt_UnrestrictedConsgnStockAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          (b.multiplier * (a.MSKA_KALAB+a.MSKA_KAEIN+a.MSKA_KAINS+a.MSKA_KASPE)) amt_OnHand,
          0 amt_OnHand_GBL,
          0 LastReceivedQty,
          ifnull(a.MSKA_CHARG,'Not Set') BatchNo,
          'Not Set' ValuationType,
          a.MSKA_VBELN DocumentNo,
          a.MSKA_POSNR DocumentItemNo,
          ifnull((select mm.MovementType from TMP_fact_mm_postingdate_2 mm where mm.dim_partid = dp.Dim_PartID and mm.RowSeqNo = 1),'Not Set') MovementType,
          ifnull((select mm.dim_DateIDPostingDate from TMP_fact_mm_postingdate_2 mm where mm.dim_partid = dp.Dim_PartID and mm.RowSeqNo = 1),dt.dim_dateid) StorageLocEntryDate,
          ifnull((select mm.dim_DateIDPostingDate from TMP_fact_mm_postingdate_2 mm where mm.dim_partid = dp.Dim_PartID and mm.RowSeqNo = 1),dt.dim_dateid) LastReceivedDate,
          dp.Dim_PartID,
          p.dim_plantid,
          sloc.Dim_StorageLocationid,
          cmp.dim_Companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
	  dc.dim_currencyid dim_currencyid_TRA,	
	  ifnull((SELECT Dim_Currencyid
                 FROM Dim_Currency dcr
                WHERE dcr.CurrencyCode = pGlobalCurrency ), 1) Dim_Currencyid_GBL,
          1 dim_stocktypeid,
          spstk.dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          pg.Dim_PurchaseGroupid,
          dph.dim_producthierarchyid,
          uom.Dim_UnitOfMeasureid,
          ifnull((select mm.Dim_MovementIndicatorid from TMP_fact_mm_postingdate_2 mm where mm.dim_partid = dp.Dim_PartID and mm.RowSeqNo = 1),1) Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid, 
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
	  1 amt_ExchangeRate
     FROM MSKA a
          INNER JOIN dim_plant p ON a.MSKA_WERKS = p.PlantCode
          INNER JOIN dim_Company cmp ON cmp.CompanyCode = p.CompanyCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSKA_MATNR AND dp.Plant = a.MSKA_WERKS
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_date dt ON a.MSKA_ERSDA = dt.DateValue and dt.CompanyCode = p.CompanyCode
          INNER JOIN dim_storagelocation sloc ON sloc.LocationCode = MSKA_LGORT AND sloc.Plant = MSKA_WERKS
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = cmp.Currency
          INNER JOIN dim_specialstock spstk ON spstk.specialstockindicator = a.MSKA_SOBKZ, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc;


update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';

  INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                ct_BlockedStock,
                                ct_StockInTransfer,
                                ct_BlockedConsgnStock,
                                ct_ConsgnStockInQInsp,
                                ct_RestrictedConsgnStock,
                                ct_UnrestrictedConsgnStock,
                                ct_BlockedStockReturns,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_BlockedStockAmt,
                                amt_BlockedStockAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StockInTransferAmt,
                                amt_StockInTransferAmt_GBL,
                                amt_UnrestrictedConsgnStockAmt,
                                amt_UnrestrictedConsgnStockAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_ValuationType,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dd_MovementType,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
				dim_Currencyid_TRA,
				dim_Currencyid_GBL,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_StockCategoryid,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL,amt_ExchangeRate)
  SELECT a.MSKA_KALAB StockQty,
          a.MSKA_KAEIN ct_TotalRestrictedStock,
          a.MSKA_KAINS ct_StockInQInsp,
          a.MSKA_KASPE ct_BlockedStock,
          0 ct_StockInTransfer,
          0 ct_BlockedConsgnStock,
          0 ct_ConsgnStockInQInsp,
          0 ct_RestrictedConsgnStock,
          0 ct_UnrestrictedConsgnStock,
          0 ct_BlockedStockReturns,
          0 StockValueAmt,
          0 StockValueAmt_GBL,
          0 amt_BlockedStockAmt,
          0 amt_BlockedStockAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StockInTransferAmt,
          0 amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt,
          0 amt_UnrestrictedConsgnStockAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          0 amt_OnHand,
          0 amt_OnHand_GBL,
          0 LastReceivedQty,
          ifnull(a.MSKA_CHARG,'Not Set') BatchNo,
          'Not Set' ValuationType,
          a.MSKA_VBELN DocumentNo,
          a.MSKA_POSNR DocumentItemNo,
          ifnull((select mm.MovementType from TMP_fact_mm_postingdate_2 mm where mm.dim_partid = dp.Dim_PartID and mm.RowSeqNo = 1),'Not Set') MovementType,
          ifnull((select mm.dim_DateIDPostingDate from TMP_fact_mm_postingdate_2 mm where mm.dim_partid = dp.Dim_PartID and mm.RowSeqNo = 1),dt.dim_dateid) StorageLocEntryDate,
          ifnull((select mm.dim_DateIDPostingDate from TMP_fact_mm_postingdate_2 mm where mm.dim_partid = dp.Dim_PartID and mm.RowSeqNo = 1),dt.dim_dateid) LastReceivedDate,
          dp.Dim_PartID,
          p.dim_plantid,
          sloc.Dim_StorageLocationid,
          cmp.dim_Companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
	  dc.dim_currencyid dim_currencyid_TRA,	
	  ifnull((SELECT Dim_Currencyid
                 FROM Dim_Currency dcr
                WHERE dcr.CurrencyCode = pGlobalCurrency ), 1) Dim_Currencyid_GBL,
          1 dim_stocktypeid,
          spstk.dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          pg.Dim_PurchaseGroupid,
          dph.dim_producthierarchyid,
          uom.Dim_UnitOfMeasureid,
          ifnull((select mm.Dim_MovementIndicatorid from TMP_fact_mm_postingdate_2 mm where mm.dim_partid = dp.Dim_PartID and mm.RowSeqNo = 1),1) Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid, 
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
			where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
	  1 amt_ExchangeRate
     FROM MSKA a
          INNER JOIN dim_plant p ON a.MSKA_WERKS = p.PlantCode
          INNER JOIN dim_Company cmp ON cmp.CompanyCode = p.CompanyCode
          -- INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSKA_MATNR AND dp.Plant = a.MSKA_WERKS
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_date dt ON a.MSKA_ERSDA = dt.DateValue and dt.CompanyCode = p.CompanyCode
          INNER JOIN dim_storagelocation sloc ON sloc.LocationCode = MSKA_LGORT AND sloc.Plant = MSKA_WERKS
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = cmp.Currency
          INNER JOIN dim_specialstock spstk ON spstk.specialstockindicator = a.MSKA_SOBKZ, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
 WHERE not exists (select 1 from tmp2_MBEW_NO_BWTAR b where b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea);


UPDATE fact_inventoryaging_tmp_populate ig
  FROM dim_part pt, Dim_ProfitCenter pc
SET ig.dim_profitcenterid = pc.dim_profitcenterid
WHERE ig.dim_PartId = pt.Dim_PartId
 AND pc.ProfitCenterCode = pt.ProfitCenterCode
 AND pc.validto >= current_date
 AND pc.RowIsCurrent = 1;
 
UPDATE fact_inventoryaging_tmp_populate ig
SET ig.dim_profitcenterid = 1
WHERE ig.dim_profitcenterid IS NULL;
 

UPDATE fact_inventoryaging_tmp_populate
SET Dim_DateidActualRelease = 1
WHERE Dim_DateidActualRelease is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_DateidActualStart = 1
WHERE Dim_DateidActualStart is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_ProdOrdernumber = 'Not Set'
WHERE dd_ProdOrdernumber is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_ProdOrderitemno = 0
WHERE dd_ProdOrderitemno is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_Plannedorderno = 'Not Set'
WHERE dd_Plannedorderno is null;

UPDATE fact_inventoryaging_tmp_populate
SET dim_productionorderstatusid = 1
WHERE dim_productionorderstatusid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_productionordertypeid = 1
WHERE Dim_productionordertypeid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_profitcenterid = 1
WHERE Dim_profitcenterid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_partsalesid = 1
WHERE Dim_partsalesid is null;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* Drop original table and rename staging table to orig table name */
drop table if exists fact_inventoryaging;
rename table fact_inventoryaging_tmp_populate to fact_inventoryaging;

/* start changes 20 Feb for dim_DateidExpiryDate */

update fact_inventoryaging i
from	mch1 m,
	dim_Part dp1,
	dim_company dc,
	dim_date dt1
set dim_DateidExpiryDate = dt1.dim_dateid
where i.dim_Partid = dp1.dim_Partid
	and dp1.partnumber =  m.mch1_matnr    			
	and i.dd_BatchNo = m.mch1_charg			
	and dt1.datevalue = m.mch1_vfdat
	and dt1.CompanyCode = dc.CompanyCode
	and i.dim_companyid = dc.dim_companyid;

UPDATE fact_inventoryaging i
from	dim_Part dp1, dim_vendor v
set i.Dim_vendorid = v.dim_vendorid
where i.dim_Partid = dp1.dim_Partid
	and dp1.ManfacturerNumber =  v.vendornumber    			
	and v.rowiscurrent = 1
	AND i.Dim_VendorId = 1;

update fact_inventoryaging i
set dim_DateidExpiryDate = 1 where dim_DateidExpiryDate is null;

call vectorwise(combine 'fact_inventoryaging');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;

/* Drop all the intermediate tables created */

drop table if exists materialmovement_tmp_new; 
drop table if exists a1;
drop table if exists a99;
drop table if exists b2;
drop table if exists b2;	
drop table if exists b3;
drop table if exists b4;
drop table if exists b;
drop table if exists ia1;
drop table if exists pl_prt_mbew_no_bwtar;
drop table if exists tmp2_keph;
drop table if exists tmp_MBEW_NO_BWTAR;
drop table if exists tmp_MBEWH_NO_BWTAR;
drop table if exists tmp2_mbew_no_bwtar;
drop table if exists tmp2_tcurf;
drop table if exists tmp2_tcurr;
drop table if exists tmp3_mbew_no_bwtar;
drop table if exists tmp3_tcurr;
drop table if exists tmp4_dim_date;
drop table if exists tmp4_mbew_no_bwtar;
drop table if exists tmp_all_x;
drop table if exists tmp_all_x_2;
drop table if exists tmp_c1;
drop table if exists tmp_c2;
drop table if exists tmp_c3;
drop table if exists tmp_dim_date;
drop table if exists tmp_fact_mm_0;
drop table if exists tmp_fact_mm_1;
drop table if exists tmp_fact_mm_2;
drop table if exists tmp_fact_mm_postingdate_0;
drop table if exists tmp_fact_mm_postingdate_1;
drop table if exists tmp_fact_mm_postingdate_2;
drop table if exists tmp_fact_mm_postingdate_2_min;
drop table if exists tmp_keph;
drop table if exists tmp_lrd;
drop table if exists tmp_mard;
drop table if exists tmp_mbew_no_bwtar;
drop table if exists tmp_tbl_storagelocentrydate;
drop table if exists tmp_tcurf;
drop table if exists tmp_tcurr;
DROP TABLE IF EXISTS tmp_GlobalCurr_001;
DROP TABLE IF EXISTS tmp_stkcategory_001;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins_c1;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_del;

