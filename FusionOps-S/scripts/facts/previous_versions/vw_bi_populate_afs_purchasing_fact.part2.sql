
/**************************************************************************************************************/
/*   Script         : vw_bi_populate_afs_purchasing_fact.part2.sql	 */
/*   Author         : Lokesh */
/*   Created On     : 28 Jul 2013 */
/*   Description    : Stored Proc bi_populate_afs_purchasing_fact migration from MySQL to Vectorwise syntax   */
/*   Note           : AFS and non-AFS Part 2 scripts for purchasing_fact do not have any difference           */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                             */
/*   01 Feb 2014      Mohamed  	1.32			  Added UnlimitedOverDelivery	   							      */
/*   14 Nov 2013      Issam  	1.16			  Dim_ValuationClassId										      */
/*   05 Nov 2013      Issam  	1.21			  Added EKET_J_3AELIKZ   						  				  */
/*   12 Sep 2013      Lokesh	1.2				  Currency changes Tran/Local/GBL								  */
/*   31 Jul 2013      Lokesh    1.1               Merged changes from non-afs script                                   */
/*   28 Jul 2013      Lokesh    1.0               Existing code migrated to Vectorwise.				   */
/* 							 Also merged Shanthi/Mircea's latest changes for 		   */
/*							 and other columns                    				   */
/***************************************************************************************************************** */

/* Start of Part 2 of vw_bi_populate_afs_purchasing_fact. This needs to be run after the stdprice scripts, which in turn should be run after part 1 above */
/* Reason for running stdprice in the middle - ct_ExchangeRate which is generated in part1 and needed by the function call */


/* Update 3 */
	 /* Update amt_StdUnitPrice and Dim_DateidCosting */

select 'START OF PROC vw_bi_populate_afs_purchasing_fact.part2 ',TIMESTAMP(LOCAL_TIMESTAMP);


\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_purchase;
		
/* 3a 	amt_StdUnitPrice */	
 /* Update amt_StdUnitPrice using getStdPrice */

UPDATE fact_purchase fp
 from    ekko_ekpo_eket,
         dim_date dt,
                 tmp_getStdPrice z 
 SET    fp.amt_StdUnitPrice =   ( z.StandardPrice / amt_ExchangeRate  )
   WHERE     fp.dd_DocumentNo = EKPO_EBELN
        AND fp.dd_DocumentItemNo = EKPO_EBELP
        AND fp.dd_ScheduleNo = EKET_ETENR
        AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
        where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
        AND dt.CompanyCode = EKPO_BUKRS
        AND fp.amt_StdUnitPrice = 0
        AND  z.pCompanyCode = EKPO_BUKRS
        AND z.pPlant = EKPO_WERKS
        AND z.pMaterialNo = EKPO_MATNR
        AND z.pFiYear = dt.FinancialYear
        AND z.pPeriod = dt.FinancialMonthNumber
        AND z.fact_script_name = 'bi_populate_purchasing_fact'
        AND z.vUMREZ = EKPO_UMREZ
        AND z.vUMREN = EKPO_UMREN
        AND z.PONumber = fp.dd_DocumentNo
	AND z.pUnitPrice - ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                * ct_ExchangeRate
                * (DECIMAL(EKPO_BPUMZ,18,4)/CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END)),0)  = 0
        AND z.StandardPrice IS NOT NULL;
        
 /* Override amt_StdUnitPrice where condition matches case */
 UPDATE fact_purchase fp	 
 from    ekko_ekpo_eket,
         dim_date dt
 SET 	fp.amt_StdUnitPrice = (CASE
                              WHEN EKKO_LOEKZ = 'L' THEN 0
                              WHEN EKPO_LOEKZ = 'L' THEN 0
                              WHEN EKPO_RETPO = 'X' THEN 0
                              WHEN EKPO_NETPR = 0 THEN 0
							  ELSE fp.amt_StdUnitPrice
							  END )
   WHERE     fp.dd_DocumentNo = EKPO_EBELN
         AND fp.dd_DocumentItemNo = EKPO_EBELP
         AND fp.dd_ScheduleNo = EKET_ETENR
         AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
                             where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
         AND dt.CompanyCode = EKPO_BUKRS
         AND fp.amt_StdUnitPrice = 0;		

 	 
		 
 /* 3b 	Dim_DateidCosting */
  
  UPDATE fact_purchase fp
  FROM   ekko_ekpo_eket,
         dim_date dt
     SET  fp.Dim_DateidCosting = ifnull((SELECT costingDate_Id
					      FROM tmp_getCostingDate z
					      WHERE z.pCompanyCode = EKPO_BUKRS
					      AND z.pPlant = EKPO_WERKS
					      AND z.pMaterialNo = EKPO_MATNR
					      AND z.pFiYear = dt.FinancialYear
					      AND z.pPeriod = dt.FinancialMonthNumber
					      AND z.fact_script_name = 'bi_populate_purchasing_fact'),1)
   WHERE     fp.dd_DocumentNo = EKPO_EBELN
         AND fp.dd_DocumentItemNo = EKPO_EBELP
         AND fp.dd_ScheduleNo = EKET_ETENR
         AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
                             where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
         AND dt.CompanyCode = EKPO_BUKRS
         AND fp.amt_StdUnitPrice = 0;										
 
/* End of Update 3 */

 INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fp, ekko_ekpo_eket',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fp, ekko_ekpo_eket dm_di_ds',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 2 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


/* Update 4 */

UPDATE fact_purchase fp
FROM ekko_ekpo_eket dm_di_ds
    SET amt_UnitPrice = ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                                      /** ct_ExchangeRate*/
                                      * (DECIMAL(EKPO_BPUMZ,18,4)/CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END)),0)
WHERE fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR;		

UPDATE fact_purchase fp
FROM ekko_ekpo_eket dm_di_ds
    SET  amt_DeliveryTotal = fp.ct_DeliveryQty * ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                                                          /** ct_ExchangeRate*/
                                                          * (DECIMAL(EKPO_BPUMZ,18,4)/CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END)),0)
WHERE fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR;	

UPDATE fact_purchase fp
FROM ekko_ekpo_eket dm_di_ds
    SET  amt_StdPriceAmt = fp.ct_DeliveryQty * fp.amt_StdUnitPrice
WHERE fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR;	

UPDATE fact_purchase fp
FROM ekko_ekpo_eket dm_di_ds
    SET  amt_DeliveryPPV = ifnull((CASE WHEN amt_StdUnitPrice = 0 THEN 0
                                       ELSE ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                                                    /** ct_ExchangeRate*/
                                                    * (DECIMAL(EKPO_BPUMZ,18,4)/EKPO_BPUMN)),0) - amt_StdUnitPrice
                                  END), 0) * fp.ct_DeliveryQty
WHERE fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR;	

/* End of Update 4 */	

 INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fp, ekko_ekpo_eket dm_di_ds',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fpr, Dim_PurchaseMisc pmisc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 2 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';
		
	

/* Insert 2 */

delete from NUMBER_FOUNTAIN where table_name = 'dim_purchasemisc';

INSERT INTO NUMBER_FOUNTAIN
select 'dim_purchasemisc',ifnull(max(dim_purchasemiscid),0)
FROM dim_purchasemisc;

  INSERT INTO dim_purchasemisc(dim_purchasemiscid,
                               ItemGRIndicator,
                               ItemDeliveryComplete,
                               ItemReject,
                               ItemGRNonValue,
                               ItemFinalInvoice,
                               ItemProduceInhouse,
                               ItemReturn,
                               HeaderRelease,
                               ExchangeRateFix,
                               DeliveryFixed,
                               DeliveryComplete,
                               FirmedDelivery,
                               ItemStatistical,
/*   Begin 05 Nov 2013 */
							   afsdeliverycomplete,
/*   End 05 Nov 2013 */		

/*   Begin 01 Feb 2014 */
							   UnlimitedOverDelivery
/*   End 01 Feb 2014  */
					   
							   )
  SELECT ((SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_purchasemisc')+ row_number() over ()),
		tp.*
  FROM ( SELECT 
		DISTINCT
         ifnull(EKPO_WEPOS, 'Not Set') ItemGRIndicator,
         ifnull(EKPO_ELIKZ, 'Not Set') ItemDeliveryComplete,
         ifnull(EKPO_ABSKZ, 'Not Set') ItemReject,
         ifnull(EKPO_WEUNB, 'Not Set') ItemGRNonValue,
         ifnull(EKPO_EREKZ, 'Not Set') ItemFinalInvoice,
         ifnull(EKPO_J_1BOWNPRO, 'Not Set') ItemProduceInhouse,
         ifnull(EKPO_RETPO, 'Not Set') ItemReturn,
         (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END) HeaderRelease,
         ifnull(EKKO_KUFIX, 'Not Set') ExchangeRateFix,
         ifnull(EKKO_FIXPO, 'Not Set') DeliveryFixed,
         ifnull(EKKO_AUTLF, 'Not Set') DeliveryComplete,
         CASE WHEN ( (fpr.ct_FirmZone > 0) AND (dtdel.DateValue - fpr.ct_FirmZone <= TIMESTAMP(LOCAL_TIMESTAMP) ) ) THEN 'X' ELSE 'Not Set' END FirmedDelivery,
         ifnull(EKPO_STAPO, 'Not Set') ItemStatistical,
/*   Begin 05 Nov 2013 */
		 ifnull(EKET_J_3AELIKZ, 'Not Set') afsdeliverycomplete,
/*   End 05 Nov 2013 */	

/*   Begin 01 Feb 2014 */
		 ifnull(EKPO_UEBTK, 'Not Set') UnlimitedOverDelivery
/*   End 01 Feb 2014 */	
		 		  
  FROM fact_purchase fpr,
         ekko_ekpo_eket dm_di,
         Dim_Date dtdel
  WHERE fpr.dd_DocumentNo = dm_di.EKPO_EBELN
         AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
         AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
         AND not exists (select 1 from Dim_PurchaseMisc pmisc
                         where pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
                              AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
                              AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
                              AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
                              AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
                              AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
                              AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
                              AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
                              AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
                              AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
                              AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
                              AND pmisc.FirmedDelivery = CASE WHEN ((fpr.ct_FirmZone > 0) AND (dtdel.DateValue -  fpr.ct_FirmZone <= TIMESTAMP(LOCAL_TIMESTAMP))) THEN 'X' ELSE 'Not Set' END
                              AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')
/*   End 05 Nov 2013 */								  
							  AND pmisc.afsdeliverycomplete = ifnull(EKET_J_3AELIKZ, 'Not Set')
/*   End 05 Nov 2013 */	

/*   End 01 Feb 2014 */								  
							  AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set')
/*   End 01 Feb 2014 */	

							  )) tp;
/* End of Insert 2 */							  
							
/* Update 5 */	

  UPDATE fact_purchase fpr
  FROM   Dim_PurchaseMisc pmisc,
         ekko_ekpo_eket dm_di,
         Dim_Date dtdel
     SET fpr.Dim_PurchaseMiscid = pmisc.Dim_PurchaseMiscid
   WHERE     fpr.dd_DocumentNo = dm_di.EKPO_EBELN
         AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
         AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
         AND pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
         AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
         AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
         AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
         AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
         AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
         AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
         AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
         AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
         AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
         AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
		 AND fpr.ct_FirmZone > 0 AND (dtdel.DateValue - fpr.ct_FirmZone <= TIMESTAMP(LOCAL_TIMESTAMP))
		 AND pmisc.FirmedDelivery = 'X'
         AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')
/*   End 05 Nov 2013 */								  
		 AND pmisc.afsdeliverycomplete = ifnull(EKET_J_3AELIKZ, 'Not Set')
/*   End 05 Nov 2013 */		

/*   End 01 Feb 2014 */								  
		 AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set')
/*   End 01 Feb 2014 */
	 
		 AND fpr.Dim_PurchaseMiscid <> pmisc.Dim_PurchaseMiscid;	

  UPDATE fact_purchase fpr
  FROM   Dim_PurchaseMisc pmisc,
         ekko_ekpo_eket dm_di,
         Dim_Date dtdel
     SET fpr.Dim_PurchaseMiscid = pmisc.Dim_PurchaseMiscid
   WHERE     fpr.dd_DocumentNo = dm_di.EKPO_EBELN
         AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
         AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
         AND pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
         AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
         AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
         AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
         AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
         AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
         AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
         AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
         AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
         AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
         AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
		 AND ( fpr.ct_FirmZone <= 0 OR (dtdel.DateValue - fpr.ct_FirmZone > TIMESTAMP(LOCAL_TIMESTAMP)))
		 AND pmisc.FirmedDelivery = 'Not Set'
         AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')
/*   End 05 Nov 2013 */								  
		 AND pmisc.afsdeliverycomplete = ifnull(EKET_J_3AELIKZ, 'Not Set')
/*   End 05 Nov 2013 */	

/*   End 01 Feb 2014 */								  
		 AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set')
/*   End 01 Feb 2014 */	
			 
		 AND fpr.Dim_PurchaseMiscid <> pmisc.Dim_PurchaseMiscid;	


\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_purchase;

 INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fpr, Dim_PurchaseMisc pmisc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fpr, ekko_ekpo_eket_eban dm_di',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 2 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';
		

  UPDATE fact_purchase fpr
  FROM ekko_ekpo_eket_eban dm_di,
         Dim_Date dt
     SET fpr.Dim_DateIdPRRelease = dt.Dim_DateId
   WHERE     fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
         AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
         AND dt.CompanyCode = dm_di.EKPO_BUKRS
         AND dt.DateValue = dm_di.EBAN_FRGDT
         AND fpr.Dim_DateIdPRRelease <> dt.Dim_DateId
         AND fpr.dd_PurchaseReqNo <> 'Not Set';

UPDATE fact_purchase fpr
FROM 
         ekko_ekpo_eket_eban dm_di,
         Dim_Date rdt
     SET fpr.Dim_DateIdRequistionDate = rdt.Dim_DateId
   WHERE     fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
         AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
         AND rdt.CompanyCode = dm_di.EKPO_BUKRS
         AND rdt.DateValue = dm_di.EBAN_BADAT
         AND fpr.Dim_DateIdRequistionDate <> rdt.Dim_DateId
         AND fpr.dd_PurchaseReqNo <> 'Not Set';

  UPDATE fact_purchase fpr
  FROM ekko_ekpo_ekkn ek
     SET fpr.dd_IntOrder = ifnull(ek.EKKN_AUFNR, 'Not Set')
   WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
         AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
		 AND ifnull(fpr.dd_IntOrder,'yy') <> ifnull(ek.EKKN_AUFNR, 'Not Set');

  UPDATE fact_purchase fpr
FROM ekko_ekpo_ekkn ek, PRPS p
     SET fpr.dd_WBSElement = ifnull(p.PRPS_POSID, 'Not Set')
   WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
         AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
         AND p.PRPS_PSPNR = ek.EKKN_PS_PSP_PNR
		 AND ifnull(fpr.dd_WBSElement,'xx') <> ifnull(p.PRPS_POSID, 'Not Set');

  UPDATE fact_purchase fpr
  FROM ekko_ekpo_ekkn ek
     SET fpr.dd_SalesDocNo = ifnull(ek.EKKN_VBELN, 'Not Set'),
         fpr.dd_SalesItemNo = ifnull(ek.EKKN_VBELP,0),
	 fpr.dd_SalesScheduleNo = ifnull(ek.EKKN_VETEN,0)
   WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
         AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP;


UPDATE fact_purchase fp
FROM EINE e
   SET fp.ct_MinimumPOQty = ifnull(e.EINE_MINBM, 0)
 WHERE e.EBELN = fp.dd_DocumentNo AND e.EBELP = fp.dd_DocumentItemNo
 AND ifnull(fp.ct_MinimumPOQty,'xx') <> ifnull(e.EINE_MINBM, 0);
 
  INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fpr, ekko_ekpo_eket_eban dm_di',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'DELETE FROM fact_purchase',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 2 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';
		

		
  DELETE FROM fact_purchase
   WHERE EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE     EKPO_EBELN = dd_DocumentNo
                    AND EKPO_EBELP = dd_DocumentItemNo
                    AND EKPO_LOEKZ = 'L');

  DELETE FROM fact_purchase
   WHERE NOT EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo
                    AND EKPO_EBELP = dd_DocumentItemNo)
         AND EXISTS
                (SELECT 1
                   FROM EKKO_EKPO
                  WHERE EKPO_EBELN = dd_DocumentNo
                        AND EKPO_EBELP = dd_DocumentItemNo);

  DELETE FROM fact_purchase
   WHERE EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo)
         AND NOT EXISTS
                    (SELECT 1
                       FROM EKKO_EKPO_EKET
                      WHERE EKPO_EBELN = dd_DocumentNo
                            AND EKPO_EBELP = dd_DocumentItemNo);

  DELETE FROM fact_purchase
   WHERE EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo
                    AND EKPO_EBELP = dd_DocumentItemNo)
         AND NOT EXISTS
                    (SELECT 1
                       FROM EKKO_EKPO_EKET
                      WHERE     EKPO_EBELN = dd_DocumentNo
                            AND EKPO_EBELP = dd_DocumentItemNo
                            AND EKET_ETENR = dd_ScheduleNo);
							
							


INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'DELETE FROM fact_purchase',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';
			
DROP TABLE IF EXISTS tmp_pf_mnb1;
CREATE TABLE 	tmp_pf_mnb1
AS
SELECT 		x.MATNR,x.BWKEY, max((x.LFGJA * 100) + x.LFMON) as max_lfgja_lfmon from mbew_no_bwtar x
GROUP BY x.MATNR,x.BWKEY;

DROP TABLE IF EXISTS tmp_pf_mnb;
CREATE TABLE 	tmp_pf_mnb
AS
SELECT m.*
FROM mbew_no_bwtar m,tmp_pf_mnb1 x
WHERE  x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = x.max_lfgja_lfmon;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_purchase; 


  UPDATE fact_purchase f
  FROM   tmp_pf_mnb m,
         dim_plant pl,
         dim_part prt
  SET amt_PlannedPrice1 = MBEW_ZPLP1/PEINH
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY;

  UPDATE fact_purchase f
  FROM   tmp_pf_mnb m,
         dim_plant pl,
         dim_part prt
  SET amt_PlannedPrice1 = 0
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
	AND amt_PlannedPrice1 IS NULL;
	
	
  UPDATE fact_purchase f
  FROM   tmp_pf_mnb m,
         dim_plant pl,
         dim_part prt
  SET amt_PlannedPrice = MBEW_ZPLPR/PEINH
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY;		
									   
  UPDATE fact_purchase f
  FROM   tmp_pf_mnb m,
         dim_plant pl,
         dim_part prt
  SET amt_PlannedPrice = 0
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
	AND amt_PlannedPrice is null;	

/*   Begin 14 Nov 2013 */
UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket e
SET Dim_ValuationClassId = 1
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ValuationClassId <> 1;

UPDATE fact_purchase fpr
FROM   ekko_ekpo_eket e,
         Dim_ValuationClass dvc,
         tmp_pf_mnb m,
         dim_plant pl,
         dim_part prt
SET Dim_ValuationClassId = dvc.Dim_ValuationClassId
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND		fpr.Dim_PlantidOrdering = pl.dim_plantid
AND		m.MATNR = EKPO_MATNR
AND		m.BWKEY = pl.ValuationArea
AND     dvc.ValuationClass  = m.MBEW_BKLAS
AND 	dvc.rowiscurrent = 1
AND fpr.Dim_ValuationClassId <> dvc.Dim_ValuationClassId;
		
/*   End 14 Nov 2013 */
				

INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fpr FROM EKES',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';			



drop table if exists tmp_ekko_ekpo_ekes_purchasing_fact;
create table tmp_ekko_ekpo_ekes_purchasing_fact 
as 
select first 0 distinct e.*,dt.CompanyCode, dt.Dim_Dateid
from ekko_ekpo_ekes e, dim_date dt
WHERE e.EKES_EINDT = dt.DateValue
AND e.EKES_LOEKZ IS NULL
order by EKES_ETENS desc;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET     dim_profitcenterid = ifnull((SELECT pc.Dim_ProfitCenterid
								FROM tmp_dim_pc_purchasing_fact pc
								WHERE  pc.ProfitCenterCode = EKPO_KO_PRCTR ),1)	
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR;		   

/* Note that ifnull(...,same column) was used here to  handle the discrepancy in mysql where it does nothing if the row is not found */
UPDATE    fact_purchase f
FROM dim_plant dp
   SET f.Dim_DateIdVendorConfirmation =
          ifnull((SELECT e.Dim_Dateid
             FROM tmp_ekko_ekpo_ekes_purchasing_fact e
            WHERE     f.dd_DocumentNo = e.EKPO_EBELN
                  AND f.dd_DocumentItemNo = e.EKPO_EBELP
                  AND e.CompanyCode = dp.CompanyCode
                  AND e.EKES_LOEKZ IS NULL),f.Dim_DateIdVendorConfirmation)
WHERE f.Dim_PlantidOrdering = dp.Dim_PlantId;

  INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'bi_populate_purchasing_fact END',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fpr FROM rbkp_rseg',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 2 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

/* LK : NOTE : This query ( the next 5 querries which were 1 single query in mysql ) is updating more rows than there are in fp. Which means join returns dups */
/* Update  f.dd_InvoiceNumber */
 UPDATE    fact_purchase f
FROM  rbkp_rseg rbs
SET
 f.dd_InvoiceNumber = ifnull(RBKP_BELNR,'Not ')
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND  ifnull(f.dd_InvoiceNumber,'xx') <> ifnull(RBKP_BELNR,'yy');

/* Update f.dd_InvoiceItemNo */
 UPDATE    fact_purchase f
FROM  rbkp_rseg rbs
SET f.dd_InvoiceItemNo = ifnull(RSEG_BUZEI,0)
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.dd_InvoiceItemNo,-1) <> ifnull(RSEG_BUZEI,0);

/* Update f.amt_GrossInvAmt */
 UPDATE    fact_purchase f
FROM  rbkp_rseg rbs
SET f.amt_GrossInvAmt = ifnull(RBKP_RMWWR,0)
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.amt_GrossInvAmt,-1) <> ifnull(RBKP_RMWWR,0);

/* Update f.ct_InvQtyinPOUnit */
 UPDATE    fact_purchase f
FROM  rbkp_rseg rbs
SET f.ct_InvQtyinPOUnit = ifnull(RSEG_BPMNG,0)
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.ct_InvQtyinPOUnit,-1) <> ifnull(RSEG_BPMNG,0);

/* Update f.ct_VenInvQtyinPOUnit */
 UPDATE    fact_purchase f
FROM  rbkp_rseg rbs
SET f.ct_VenInvQtyinPOUnit = ifnull(RSEG_BPRBM,0)
WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.ct_VenInvQtyinPOUnit,-1) <> ifnull(RSEG_BPRBM,0);


/* LK : Looks like the joins are returning dups in this query as well ( for the next 2 queries ) */
 UPDATE    fact_purchase f
 FROM rbkp_rseg rbs,dim_unitofmeasure uom
SET f.Dim_InvoicePOUOMId = uom.Dim_UnitOfMeasureId
 WHERE rbs.RSEG_EBELN = f.dd_DocumentNo
 AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
 AND uom.UOM = RSEG_BPRME AND uom.RowIsCurrent = 1
 AND f.Dim_InvoicePOUOMId = ifnull(uom.Dim_UnitOfMeasureId,-1);
 

 UPDATE    fact_purchase f
 FROM rbkp_rseg rbs,dim_date dt
  SET f.Dim_DateIdInvoiceRcvdDate = dt.dim_dateid
 WHERE rbs.RSEG_EBELN = f.dd_DocumentNo
 AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
 AND dt.datevalue = rbs.RBKP_BLDAT AND dt.CompanyCode = rbs.RSEG_BUKRS	   
 AND ifnull(f.Dim_DateIdInvoiceRcvdDate,-1) <> ifnull(dt.dim_dateid,-2);
 

	   
	   
DROP TABLE IF EXISTS tmp_pur_fact_konv	;
CREATE TABLE   tmp_pur_fact_konv
AS 
SELECT  KONV_KSCHL,KONV_KNUMV,sum(konv_kwert) as sum_konv_kwert
FROM konv k
GROUP BY KONV_KSCHL,KONV_KNUMV;
  

  UPDATE    facT_purchase p
  FROM EKKO_EKPO_EKET e
         
    SET p.amt_ConditionValueTax =
            ifnull(
              (SELECT sum_konv_kwert
                  FROM tmp_pur_fact_konv k
                WHERE k.KONV_KSCHL = 'NAVS' AND k.KONV_KNUMV = e.EKKO_KNUMV),
              0)
WHERE     p.dd_DocumentNo = e.EKPO_EBELN
AND p.dd_DocumentItemNo = e.EKPO_EBELP
AND p.dd_scheduleNo = e.EKET_ETENR;



  UPDATE    fact_purchase p
  FROM EKPA e1,EKKO_EKPO_EKET e,tmp_var_purchasing_fact
    SET p.Dim_CustomPartnerFunctionId1 =
            ifnull(
              (SELECT dim_vendorid
                  FROM dim_vendor v
                WHERE v.VendorNumber = e1.EKPA_LIFN2),
              1)
  WHERE    p.dd_DocumentNo = e.EKPO_EBELN
        AND p.dd_DocumentItemNo = e.EKPO_EBELP
        AND p.dd_scheduleNo = e.EKET_ETENR
        AND e.EKPO_EBELN = e1.EKPA_EBELN
        AND e.EKKO_EKORG = e1.EKPA_EKORG
        AND e1.EKPA_PARVW = pCustomPartnerFunction1;
		
	

UPDATE    fact_purchase p
FROM EKPA e1,EKKO_EKPO_EKET e,tmp_var_purchasing_fact
    SET p.Dim_CustomPartnerFunctionId2 =
            ifnull(
              (SELECT dim_vendorid
                  FROM dim_vendor v
                WHERE v.VendorNumber = e1.EKPA_LIFN2),
              1)
  WHERE    p.dd_DocumentNo = e.EKPO_EBELN
        AND p.dd_DocumentItemNo = e.EKPO_EBELP
        AND p.dd_scheduleNo = e.EKET_ETENR
        AND e.EKPO_EBELN = e1.EKPA_EBELN
        AND e.EKKO_EKORG = e1.EKPA_EKORG
        AND e1.EKPA_PARVW = pCustomPartnerFunction2;
		
			

UPDATE    fact_purchase p
FROM EKPA e1,EKKO_EKPO_EKET e,tmp_var_purchasing_fact
    SET p.Dim_CustomPartnerFunctionId3 =
            ifnull(
              (SELECT dim_vendorid
                  FROM dim_vendor v
                WHERE v.VendorNumber = e1.EKPA_LIFN2),
              1)
  WHERE    p.dd_DocumentNo = e.EKPO_EBELN
        AND p.dd_DocumentItemNo = e.EKPO_EBELP
        AND p.dd_scheduleNo = e.EKET_ETENR
        AND e.EKPO_EBELN = e1.EKPA_EBELN
        AND e.EKKO_EKORG = e1.EKPA_EKORG
        AND e1.EKPA_PARVW = pCustomPartnerFunction3;
		


  UPDATE    fact_purchase p
FROM EKPA e1,EKKO_EKPO_EKET e,tmp_var_purchasing_fact
    SET p.Dim_CustomPartnerFunctionId4 =
            ifnull(
              (SELECT dim_vendorid
                  FROM dim_vendor v
                WHERE v.VendorNumber = e1.EKPA_LIFN2),
              1)
  WHERE    p.dd_DocumentNo = e.EKPO_EBELN
        AND p.dd_DocumentItemNo = e.EKPO_EBELP
        AND p.dd_scheduleNo = e.EKET_ETENR
        AND e.EKPO_EBELN = e1.EKPA_EBELN
        AND e.EKKO_EKORG = e1.EKPA_EKORG
        AND e1.EKPA_PARVW = pCustomPartnerFunction4;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_purchase; 
		
/* End of vw_bi_purchasing_fact script. Next query is the last query */  				

INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'bi_populate_purchasing_fact END',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

call vectorwise(combine 'fact_purchase');
call vectorwise(combine 'dim_purchasemisc');
call vectorwise(combine 'processinglog');

DROP TABLE IF EXISTS tmp_var_purchasing_fact;
drop table if exists tmp_dim_pc_purchasing_fact;
drop table if exists tmp_ekko_ekpo_ekes_purchasing_fact;
DROP TABLE IF EXISTS tmp_pur_fact_konv  ;
DROP TABLE IF EXISTS tmp_pf_mnb;
DROP TABLE IF EXISTS tmp_pf_mnb1;


