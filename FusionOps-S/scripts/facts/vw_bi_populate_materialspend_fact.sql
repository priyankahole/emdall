
/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 19 May 2013 */
/*   Description    : Stored Proc bi_populate_materialspend_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   17-Jul-2013	  Lokesh    1.1               Remove all updates, truncate and insert fact_materialspend	  */
/*   19 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/



DROP TABLE IF EXISTS fact_materialspend_tmp_populate;
CREATE TABLE fact_materialspend_tmp_populate
AS
SELECT * FROM fact_materialspend
where 1=2;

ALTER TABLE fact_materialspend_tmp_populate
add  PRIMARY KEY (fact_materialspendid);


delete from NUMBER_FOUNTAIN where table_name = 'fact_materialspend_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_materialspend_tmp_populate',ifnull(max(fact_materialspendid),0)
FROM fact_materialspend_tmp_populate;

/* Insert 1 */

/* Split the insert to remove not exists and use a combine and a join instead */

DROP TABLE IF EXISTS tmp_ins_fact_materialspend;
CREATE TABLE tmp_ins_fact_materialspend
AS
SELECT distinct dd_MaterialDocNo,f.dd_MaterialDocItemNo,f.dd_MaterialDocYear
FROM fact_materialmovement f INNER JOIN dim_movementtype mt
          ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
    WHERE mt.MovementType IN (101,102,105,106,122,123,161,162,501,502,521,522)
          AND dd_ConsignmentFlag = 0;

drop table if exists tmp_purchas_uom_001;
create table tmp_purchas_uom_001 as 
    select distinct f.dd_DocumentNo, f.dd_DocumentItemNo, ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) po_UMREN, ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1)) po_UMREZ
    from fact_materialmovement f 
	  INNER JOIN dim_movementtype mt ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
	  INNER JOIN fact_purchase po on f.dd_DocumentNo = po.dd_DocumentNo and f.dd_DocumentItemNo = po.dd_DocumentItemNo
          INNER JOIN dim_Part dp1 ON po.dim_Partid = dp1.dim_Partid
	  INNER JOIN dim_unitofmeasure uom ON po.dim_unitofmeasureid = uom.dim_unitofmeasureid
	  LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	  LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
    WHERE ((mt.MovementType IN (101,102,105,106,122,123,161,162,501,502,521,522)
		AND dd_ConsignmentFlag = 0)) or dd_ConsignmentFlag = 1;
		  

  INSERT INTO fact_materialspend_tmp_populate(dd_MaterialDocNo,
                              dd_MaterialDocItemNo,
                              dd_MaterialDocYear,
                              dd_DocumentNo,
                              dd_DocumentItemNo,
                              dd_SalesOrderNo,
                              dd_SalesOrderItemNo,
                              dd_SalesOrderDlvrNo,
                              dd_GLAccountNo,
                              amt_LocalCurrAmt,
                              amt_DeliveryCost,
                              amt_AltPriceControl,
                              amt_GRValue,
                              amt_StdGRAmt,
                              ct_Quantity,
                              Dim_MovementTypeid,
                              dim_Companyid,
                              Dim_Currencyid,
                              Dim_Currencyid_TRA,
                              Dim_Currencyid_GBL,
                              amt_exchangerate,
                              amt_exchangerate_gbl,
                              Dim_Partid,
                              Dim_Plantid,
                              Dim_StorageLocationid,
                              Dim_Vendorid,
                              dim_DateIDMaterialDocDate,
                              dim_DateIDPostingDate,
                              Dim_DateIDDocCreation,
                              Dim_DateIDOrdered,
                              dim_producthierarchyid,
                              dim_MovementIndicatorid,
                              dirtyrow,
                              dim_Customerid,
                              dim_CostCenterid,
                              dd_debitcreditid,
                              dd_GoodsMoveReason,
                              Dim_AccountCategoryid,
                              Dim_ConsumptionTypeid,
                              Dim_ControllingAreaid,
                              Dim_ProfitCenterId,
                              Dim_CustomerGroup1id,
                              Dim_DocumentCategoryid,
                              Dim_DocumentTypeid,
                              Dim_ItemCategoryid,
                              Dim_Termid,
                              Dim_ItemStatusid,
                              dim_productionorderstatusid,
                              Dim_productionordertypeid,
                              Dim_PurchaseGroupid,
                              Dim_PurchaseOrgid,
                              Dim_SalesDocumentTypeid,
                              Dim_SalesGroupid,
                              Dim_SalesOrderHeaderStatusid,
                              Dim_SalesOrderItemStatusid,
                              Dim_SalesOrderRejectReasonid,
                              Dim_SalesOrgid,
                              dim_specialstockid,
                              Dim_StockTypeid,
                              Dim_UnitOfMeasureid,
                              dd_productionordernumber,
                              dd_productionorderitemno,
                              Dim_MaterialGroupid,
                              dd_ConsignmentFlag,
			      fact_materialspendid,
			      dim_uomunitofentryid)
    SELECT f.dd_MaterialDocNo,
          f.dd_MaterialDocItemNo,
          f.dd_MaterialDocYear,
          ifnull(f.dd_DocumentNo,'Not Set'),
          f.dd_DocumentItemNo,
          ifnull(f.dd_SalesOrderNo,'Not Set'),
          f.dd_SalesOrderItemNo,
          ifnull(f.dd_SalesOrderDlvrNo,'Not Set'),
          ifnull(f.dd_GLAccountNo,'Not Set'),
          f.amt_LocalCurrAmt,
          f.amt_DeliveryCost,
          f.amt_AltPriceControl,
          (f.amt_POUnitPrice * f.ct_QtyEntryUOM * (ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1)) / ifnull(uc.marm_umren,ifnull(ucf.DENOM,1))) * ifnull(po_UMREN,1)/ifnull(po_UMREZ,1)) amt_GRValue,
          cast(ifnull((f.amt_StdUnitPrice * f.ct_QtyEntryUOM), 0) as decimal(36,16)) amt_StdGRAmt,
          f.ct_QtyEntryUOM,
          f.Dim_MovementTypeid,
          f.dim_Companyid,
          f.Dim_Currencyid,
          f.Dim_Currencyid_TRA,
          f.Dim_Currencyid_GBL,
          f.amt_exchangerate,
          f.amt_exchangerate_gbl,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_Vendorid,
          f.dim_DateIDMaterialDocDate,
          f.dim_DateIDPostingDate,
          f.Dim_DateIDDocCreation,
          f.Dim_DateIDOrdered,
          f.dim_producthierarchyid,
          f.dim_MovementIndicatorid,
          f.dirtyrow,
          f.dim_Customerid,
          f.dim_CostCenterid,
          f.dd_debitcreditid,
          f.dd_GoodsMoveReason,
          f.Dim_AccountCategoryid,
          f.Dim_ConsumptionTypeid,
          f.Dim_ControllingAreaid,
          f.Dim_ProfitCenterId,
          f.Dim_CustomerGroup1id,
          f.Dim_DocumentCategoryid,
          f.Dim_DocumentTypeid,
          f.Dim_ItemCategoryid,
          f.Dim_Termid,
          f.Dim_ItemStatusid,
          f.dim_productionorderstatusid,
          f.Dim_productionordertypeid,
          f.Dim_PurchaseGroupid,
          f.Dim_PurchaseOrgid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesGroupid,
          f.Dim_SalesOrderHeaderStatusid,
          f.Dim_SalesOrderItemStatusid,
          f.Dim_SalesOrderRejectReasonid,
          f.Dim_SalesOrgid,
          f.dim_specialstockid,
          f.Dim_StockTypeid,
          f.Dim_UnitOfMeasureid,
          f.dd_productionordernumber,
          f.dd_productionorderitemno,
          f.Dim_MaterialGroupid,
          f.dd_ConsignmentFlag,
	  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_materialspend_tmp_populate') + row_number() over(),
	  f.dim_uomunitofentryid
    FROM fact_materialmovement f
	  INNER JOIN dim_movementtype mt ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
	  INNER JOIN tmp_ins_fact_materialspend ms ON ms.dd_MaterialDocNo = f.dd_MaterialDocNo
							AND ms.dd_MaterialDocItemNo = f.dd_MaterialDocItemNo
							AND ms.dd_MaterialDocYear = f.dd_MaterialDocYear
          INNER JOIN dim_Part dp1 ON f.dim_Partid = dp1.dim_Partid
	  INNER JOIN dim_unitofmeasure uom ON f.dim_uomunitofentryid = uom.dim_unitofmeasureid
	  LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	  LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
	  LEFT JOIN tmp_purchas_uom_001 po ON f.dd_DocumentNo = po.dd_DocumentNo and f.dd_DocumentItemNo = po.dd_DocumentItemNo
    WHERE mt.MovementType IN (101,102,105,106,122,123,161,162,501,502,521,522)
          AND dd_ConsignmentFlag = 0;

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_materialspendid),0) from fact_materialspend_tmp_populate)
					where table_name = 'fact_materialspend_tmp_populate';	  		  

/* Insert 2 */		

call vectorwise (combine 'tmp_ins_fact_materialspend - tmp_ins_fact_materialspend');				 

INSERT INTO tmp_ins_fact_materialspend
SELECT distinct dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
FROM fact_materialmovement f INNER JOIN dim_movementtype mt
		ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
WHERE dd_ConsignmentFlag = 1;


  INSERT INTO fact_materialspend_tmp_populate(dd_MaterialDocNo,
                              dd_MaterialDocItemNo,
                              dd_MaterialDocYear,
                              dd_DocumentNo,
                              dd_DocumentItemNo,
                              dd_SalesOrderNo,
                              dd_SalesOrderItemNo,
                              dd_SalesOrderDlvrNo,
                              dd_GLAccountNo,
                              amt_LocalCurrAmt,
                              amt_DeliveryCost,
                              amt_AltPriceControl,
                              amt_GRValue,
                              amt_StdGRAmt,
                              ct_Quantity,
                              Dim_MovementTypeid,
                              dim_Companyid,
                              Dim_Currencyid,
                              Dim_Currencyid_TRA,
                              Dim_Currencyid_GBL,
                              amt_exchangerate,
                              amt_exchangerate_gbl,
                              Dim_Partid,
                              Dim_Plantid,
                              Dim_StorageLocationid,
                              Dim_Vendorid,
                              dim_DateIDMaterialDocDate,
                              dim_DateIDPostingDate,
                              Dim_DateIDDocCreation,
                              Dim_DateIDOrdered,
                              dim_producthierarchyid,
                              dim_MovementIndicatorid,
                              dirtyrow,
                              dim_Customerid,
                              dim_CostCenterid,
                              dd_debitcreditid,
                              dd_GoodsMoveReason,
                              Dim_AccountCategoryid,
                              Dim_ConsumptionTypeid,
                              Dim_ControllingAreaid,
                              Dim_ProfitCenterId,
                              Dim_CustomerGroup1id,
                              Dim_DocumentCategoryid,
                              Dim_DocumentTypeid,
                              Dim_ItemCategoryid,
                              Dim_Termid,
                              Dim_ItemStatusid,
                              dim_productionorderstatusid,
                              Dim_productionordertypeid,
                              Dim_PurchaseGroupid,
                              Dim_PurchaseOrgid,
                              Dim_SalesDocumentTypeid,
                              Dim_SalesGroupid,
                              Dim_SalesOrderHeaderStatusid,
                              Dim_SalesOrderItemStatusid,
                              Dim_SalesOrderRejectReasonid,
                              Dim_SalesOrgid,
                              dim_specialstockid,
                              Dim_StockTypeid,
                              Dim_UnitOfMeasureid,
                              dd_productionordernumber,
                              dd_productionorderitemno,
                              Dim_MaterialGroupid,
                              dd_ConsignmentFlag,
			      fact_materialspendid,
			      dim_uomunitofentryid)
    SELECT f.dd_MaterialDocNo,
          f.dd_MaterialDocItemNo,
          f.dd_MaterialDocYear,
          ifnull(f.dd_DocumentNo,'Not Set'),
          f.dd_DocumentItemNo,
          ifnull(f.dd_SalesOrderNo,'Not Set'),
          f.dd_SalesOrderItemNo,
          ifnull(f.dd_SalesOrderDlvrNo,'Not Set'),
          ifnull(f.dd_GLAccountNo,'Not Set'),
          f.amt_LocalCurrAmt,
          f.amt_DeliveryCost,
          f.amt_AltPriceControl,
          (f.amt_POUnitPrice * f.ct_QtyEntryUOM * (ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1)) / ifnull(uc.marm_umren,ifnull(ucf.DENOM,1))) * ifnull(po_UMREN,1)/ifnull(po_UMREZ,1)) amt_GRValue,
          ifnull((f.amt_StdUnitPrice * f.ct_QtyEntryUOM), 0) amt_StdGRAmt,
          f.ct_QtyEntryUOM,
          f.Dim_MovementTypeid,
          f.dim_Companyid,
          f.Dim_Currencyid,
          f.Dim_Currencyid_TRA,
          f.Dim_Currencyid_GBL,
          f.amt_exchangerate,
          f.amt_exchangerate_gbl,
          f.Dim_Partid,
          Dim_Plantid,
          Dim_StorageLocationid,
          Dim_Vendorid,
          dim_DateIDMaterialDocDate,
          dim_DateIDPostingDate,
          Dim_DateIDDocCreation,
          Dim_DateIDOrdered,
          dim_producthierarchyid,
          dim_MovementIndicatorid,
          dirtyrow,
          dim_Customerid,
          dim_CostCenterid,
          dd_debitcreditid,
          dd_GoodsMoveReason,
          Dim_AccountCategoryid,
          Dim_ConsumptionTypeid,
          Dim_ControllingAreaid,
          Dim_ProfitCenterId,
          Dim_CustomerGroup1id,
          Dim_DocumentCategoryid,
          Dim_DocumentTypeid,
          Dim_ItemCategoryid,
          Dim_Termid,
          Dim_ItemStatusid,
          dim_productionorderstatusid,
          Dim_productionordertypeid,
          Dim_PurchaseGroupid,
          Dim_PurchaseOrgid,
          Dim_SalesDocumentTypeid,
          Dim_SalesGroupid,
          Dim_SalesOrderHeaderStatusid,
          Dim_SalesOrderItemStatusid,
          Dim_SalesOrderRejectReasonid,
          Dim_SalesOrgid,
          dim_specialstockid,
          Dim_StockTypeid,
          f.Dim_UnitOfMeasureid,
          dd_productionordernumber,
          dd_productionorderitemno,
          Dim_MaterialGroupid,
          f.dd_ConsignmentFlag,
	  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_materialspend_tmp_populate') + row_number() over(),
	  f.dim_uomunitofentryid
    FROM fact_materialmovement f 
	 INNER JOIN dim_movementtype mt ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 INNER JOIN tmp_ins_fact_materialspend ms ON ms.dd_MaterialDocNo = f.dd_MaterialDocNo
						 AND ms.dd_MaterialDocItemNo = f.dd_MaterialDocItemNo
						 AND ms.dd_MaterialDocYear = f.dd_MaterialDocYear
          INNER JOIN dim_Part dp1 ON f.dim_Partid = dp1.dim_Partid
	  INNER JOIN dim_unitofmeasure uom ON f.dim_uomunitofentryid = uom.dim_unitofmeasureid
	  LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	  LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
	  LEFT JOIN tmp_purchas_uom_001 po ON f.dd_DocumentNo = po.dd_DocumentNo and f.dd_DocumentItemNo = po.dd_DocumentItemNo
    WHERE dd_ConsignmentFlag = 1;
	 
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_materialspendid),0) from fact_materialspend_tmp_populate)
where table_name = 'fact_materialspend_tmp_populate';	  

/* Drop original table and rename staging table to orig table name */
drop table if exists fact_materialspend;
rename table fact_materialspend_tmp_populate to fact_materialspend;	

CALL VECTORWISE(combine 'fact_materialspend');

DROP TABLE IF EXISTS tmp_ins_fact_materialspend;

