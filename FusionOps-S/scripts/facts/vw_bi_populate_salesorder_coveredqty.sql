
/* v1.15	 21-Jan-2013		Lokesh	 Tuned queries for performance */

select 'START OF PROC bi_populate_salesorder_coveredqty',TIMESTAMP(LOCAL_TIMESTAMP);


/*Reset ct_OnHandCoveredQty and  ct_DeliveredQty */
  UPDATE fact_salesorder f
  SET ct_OnHandCoveredQty = 0
WHERE (ct_OnHandCoveredQty<> 0 OR ct_OnHandCoveredQty IS NULL);
  
  UPDATE fact_salesorder f
  FROM	 Dim_ScheduleLineCategory slc
  SET ct_DeliveredQty = 0
  WHERE f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
        AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
        AND f.dd_ItemRelForDelv = 'Not Set'
AND (ct_DeliveredQty <> 0 OR ct_DeliveredQty IS NULL);

/* The main table which replaces the cursor in the mysql procedure */
DROP TABLE IF EXISTS TMP_CURSOR_salesorder_coveredqty;
CREATE TABLE TMP_CURSOR_salesorder_coveredqty
AS
 SELECT Fact_SalesOrderid v_Fact_SalesOrderid,
         dd_SalesDocNo v_dd_SalesDocNo,
         dd_SalesItemNo v_dd_SalesItemNo,
         dd_ScheduleNo v_dd_ScheduleNo,
         mad.DateValue v_GIDate,
         dim_partid v_dim_part_id,
         f.ct_ConfirmedQty v_ConfQty,
         f.dd_ItemRelForDelv v_ItmRelDlvr,
         f.ct_DeliveredQty v_DlvrQty,
         f.ct_ScheduleQtySalesUnit v_OrdQty,
	 row_number() over (order by dim_partid, mad.DateValue, dd_SalesDocNo, dd_SalesItemNo, dd_ScheduleNo) sr_no
  FROM fact_salesorder f INNER JOIN Dim_Date mad ON f.Dim_DateidGoodsIssue = mad.Dim_Dateid
      inner join dim_documentcategory dg on dg.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
      inner join Dim_ScheduleLineCategory slc on f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
  WHERE f.ct_ConfirmedQty > 0
        AND f.Dim_SalesOrderRejectReasonid = 1
        AND dg.DocumentCategory <> 'H'
        AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
  ORDER BY dim_partid, mad.DateValue, dd_SalesDocNo, dd_SalesItemNo, dd_ScheduleNo offset 0;

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN min_sr_no int null;

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_Pre_SalesOrderid    INT;

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_CoveredQty    DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_Pre_CoveredQty    DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_OpenQty    DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_DlvrQtyOrd    DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_nxt_ConfQty    DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_nxt_OrdQty    DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_CmlRcvdQty    DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_CmlRcvdQty_orig    DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD column v_Pre_part_id INT;

ALTER TABLE 	TMP_CURSOR_salesorder_coveredqty
ADD column sum_ct_QtyDelivered DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN v_ConfQty_sum_prev  DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN diff_ct_qtydelivered  DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN sr_no_diff_lt_vconfqty INT;


ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN v_OrdQty_sum_prev  DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN diff_v_OrdQty  DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN sr_no_diff_lt_v_OrdQty INT;

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN max_prev_notset_sr_no int;

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN prev_notset_v_CmlRcvdQty DECIMAL(18,4);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN prev_notset_v_DlvrQtyOrd DECIMAL(18,4);


ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN zero_v_nxt_ConfQty char(1);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN zero_v_nxt_ordqty char(1);

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN flag_v_CmlRcvdQty char(1);

UPDATE TMP_CURSOR_salesorder_coveredqty
SET zero_v_nxt_ordqty = 'N',zero_v_nxt_ConfQty = 'N',flag_v_CmlRcvdQty = 'N';



/*UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_DlvrQtyOrd = 0,v_CmlRcvdQty = 0*/

UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_CURSOR_salesorder_coveredqty y
SET x.v_Pre_part_id = y.v_dim_part_id,x.v_Pre_SalesOrderid = y.v_Fact_SalesOrderid
WHERE x.sr_no = y.sr_no + 1;

select 'B',TIMESTAMP(LOCAL_TIMESTAMP);

/* DO THIS IN THE END AS WE DON'T HAVE v_Pre_CoveredQty YET */
 /*     UPDATE fact_salesorder f
      FROM TMP_CURSOR_salesorder_coveredqty t 
      SET f.ct_OnHandCoveredQty = v_Pre_CoveredQty
      WHERE f.Fact_SalesOrderid = v_Pre_SalesOrderid
      AND v_Pre_part_id and v_Pre_part_id <>  v_dim_part_id\g	*/
      


/****************************************************************************************************************************************/
--UPDATE B1-1,B1-2,B1-3 : UPDATE v_nxt_ConfQty,v_nxt_OrdQty,v_CmlRcvdQty
/* v_nxt_ConfQty and v_nxt_OrdQty are not updated anywhere else in the actual proc. Also, these depend on ct_ConfirmedQty/ct_ScheduleQtySalesUnit
which do not get updated in fact_salesorder in this proc */
/* So,  v_nxt_ConfQty,v_nxt_OrdQty updated here are final and won't need any further changes. These can then be used for calculating other vars */

/* Similarly about v_CmlRcvdQty */

DROP TABLE IF EXISTS TMP2_fact_salesorder_v_nxt_ConfQty;
CREATE TABLE TMP2_fact_salesorder_v_nxt_ConfQty
AS
SELECT f.dd_SalesDocNo,f.dd_SalesItemNo,f.dd_ItemRelForDelv,f.ct_ConfirmedQty,d.DateValue,f.dd_ScheduleNo,f.ct_CmlQtyReceived,f.ct_ScheduleQtySalesUnit
FROM fact_salesorder f,dim_date d,Dim_ScheduleLineCategory slc
WHERE f.Dim_DateidGoodsIssue = d.Dim_Dateid
AND f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1');


DROP TABLE IF EXISTS TMP3_fact_salesorder_v_nxt_ConfQty;
CREATE TABLE TMP3_fact_salesorder_v_nxt_ConfQty
AS 
SELECT f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.DateValue,f_so_sq_001.ct_ConfirmedQty, f_so_sq_001.ct_scheduleqtysalesunit,
x.v_GIDate 
FROM TMP2_fact_salesorder_v_nxt_ConfQty f_so_sq_001, TMP_CURSOR_salesorder_coveredqty x
WHERE f_so_sq_001.dd_SalesDocNo = x.v_dd_SalesDocNo
AND f_so_sq_001.dd_SalesItemNo = x.v_dd_SalesItemNo
AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
AND (f_so_sq_001.DateValue > x.v_GIDate);


DROP TABLE IF EXISTS TMP4_fact_salesorder_v_nxt_ConfQty;
CREATE TABLE TMP4_fact_salesorder_v_nxt_ConfQty
AS 
SELECT f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.v_GIDate, sum(ct_ConfirmedQty) sum_ct_ConfirmedQty
FROM TMP3_fact_salesorder_v_nxt_ConfQty f_so_sq_001
GROUP BY f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.v_GIDate;

/* For ordqty ( uses ScheduleQtySalesUnit ) */

DROP TABLE IF EXISTS TMP5_fact_salesorder_v_nxt_ConfQty;
CREATE TABLE TMP5_fact_salesorder_v_nxt_ConfQty
AS 
SELECT f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.v_GIDate, sum(ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit
FROM TMP3_fact_salesorder_v_nxt_ConfQty f_so_sq_001
GROUP BY f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.v_GIDate;


UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_nxt_ConfQty = ifnull((SELECT sum_ct_ConfirmedQty
                                    FROM TMP4_fact_salesorder_v_nxt_ConfQty f_so_sq_001
                                    WHERE f_so_sq_001.dd_SalesDocNo = x.v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = x.v_dd_SalesItemNo
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (f_so_sq_001.v_GIDate = x.v_GIDate)),0)
WHERE v_ItmRelDlvr = 'Not Set';

select 'C',TIMESTAMP(LOCAL_TIMESTAMP);

/* same gidate, but newer sched no */

DROP TABLE IF EXISTS TMP6_fact_salesorder_v_nxt_ConfQty;
CREATE TABLE TMP6_fact_salesorder_v_nxt_ConfQty
AS 
SELECT f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.DateValue,f_so_sq_001.dd_ScheduleNo,f_so_sq_001.ct_ConfirmedQty,
x.v_dd_ScheduleNo
FROM TMP2_fact_salesorder_v_nxt_ConfQty f_so_sq_001, TMP_CURSOR_salesorder_coveredqty x
WHERE f_so_sq_001.dd_SalesDocNo = x.v_dd_SalesDocNo
AND f_so_sq_001.dd_SalesItemNo = x.v_dd_SalesItemNo
AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
AND f_so_sq_001.DateValue = x.v_GIDate
AND f_so_sq_001.dd_ScheduleNo > x.v_dd_ScheduleNo;

DROP TABLE IF EXISTS TMP7_fact_salesorder_v_nxt_ConfQty;
CREATE TABLE TMP7_fact_salesorder_v_nxt_ConfQty
AS 
SELECT f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.DateValue, f_so_sq_001.v_dd_ScheduleNo,
sum(ct_ConfirmedQty) sum_ct_ConfirmedQty
FROM TMP6_fact_salesorder_v_nxt_ConfQty f_so_sq_001
GROUP BY f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.DateValue,f_so_sq_001.v_dd_ScheduleNo;
					  
UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_nxt_ConfQty = ifnull((SELECT sum_ct_ConfirmedQty
                                    FROM TMP7_fact_salesorder_v_nxt_ConfQty f_so_sq_001
                                    WHERE f_so_sq_001.dd_SalesDocNo = x.v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = x.v_dd_SalesItemNo
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (f_so_sq_001.DateValue = x.v_GIDate
                                                  AND f_so_sq_001.v_dd_ScheduleNo = x.v_dd_ScheduleNo)),0)
WHERE v_nxt_ConfQty = 0
AND v_ItmRelDlvr = 'Not Set' ;

select 'D',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_nxt_OrdQty = ifnull((SELECT sum_ct_ScheduleQtySalesUnit
                                    FROM TMP5_fact_salesorder_v_nxt_ConfQty f_so_sq_001
                                    WHERE f_so_sq_001.dd_SalesDocNo = x.v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = x.v_dd_SalesItemNo
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (f_so_sq_001.v_GIDate= x.v_GIDate)),0)
WHERE v_ItmRelDlvr = 'Not Set'  ;

select 'E',TIMESTAMP(LOCAL_TIMESTAMP);

/* same gidate, but newer sched no */

DROP TABLE IF EXISTS TMP8_fact_salesorder_v_nxt_ConfQty;
CREATE TABLE TMP8_fact_salesorder_v_nxt_ConfQty
AS 
SELECT f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.DateValue,f_so_sq_001.dd_ScheduleNo,f_so_sq_001.ct_ScheduleQtySalesUnit,
x.v_dd_ScheduleNo
FROM TMP2_fact_salesorder_v_nxt_ConfQty f_so_sq_001, TMP_CURSOR_salesorder_coveredqty x
WHERE f_so_sq_001.dd_SalesDocNo = x.v_dd_SalesDocNo
AND f_so_sq_001.dd_SalesItemNo = x.v_dd_SalesItemNo
AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
AND f_so_sq_001.DateValue = x.v_GIDate
AND f_so_sq_001.dd_ScheduleNo > x.v_dd_ScheduleNo;

DROP TABLE IF EXISTS TMP9_fact_salesorder_v_nxt_ConfQty;
CREATE TABLE TMP9_fact_salesorder_v_nxt_ConfQty
AS 
SELECT f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.DateValue, f_so_sq_001.v_dd_ScheduleNo,
sum(ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit
FROM TMP8_fact_salesorder_v_nxt_ConfQty f_so_sq_001
GROUP BY f_so_sq_001.dd_SalesDocNo,f_so_sq_001.dd_SalesItemNo,f_so_sq_001.dd_ItemRelForDelv,f_so_sq_001.DateValue,f_so_sq_001.v_dd_ScheduleNo;
					  
UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_nxt_OrdQty = ifnull((SELECT sum_ct_ScheduleQtySalesUnit
                                    FROM TMP9_fact_salesorder_v_nxt_ConfQty f_so_sq_001
                                    WHERE f_so_sq_001.dd_SalesDocNo = x.v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = x.v_dd_SalesItemNo
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (f_so_sq_001.DateValue = x.v_GIDate
                                                  AND f_so_sq_001.v_dd_ScheduleNo = x.v_dd_ScheduleNo)),0)
WHERE v_nxt_OrdQty = 0 
AND v_ItmRelDlvr = 'Not Set';

select 'F',TIMESTAMP(LOCAL_TIMESTAMP);


UPDATE TMP_CURSOR_salesorder_coveredqty x
      set v_CmlRcvdQty = ifnull((select max(VBLB_ABEFZ)
                                from VBLB where VBLB_VBELN = v_dd_SalesDocNo and VBLB_POSNR = v_dd_SalesItemNo),0)
                          - ifnull((SELECT sum(f_so_sq_001.ct_CmlQtyReceived)
                                    FROM TMP2_fact_salesorder_v_nxt_ConfQty f_so_sq_001                                         
                                    WHERE f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (f_so_sq_001.DateValue < v_GIDate
                                              OR (f_so_sq_001.DateValue = v_GIDate
                                                  AND f_so_sq_001.dd_ScheduleNo < v_dd_ScheduleNo))),0),
    flag_v_CmlRcvdQty = 'Y'
WHERE v_ItmRelDlvr = 'Not Set';

select 'G',TIMESTAMP(LOCAL_TIMESTAMP);


UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_CmlRcvdQty_orig = v_CmlRcvdQty;

UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_CmlRcvdQty = v_OrdQty,flag_v_CmlRcvdQty = 'U'
WHERE v_nxt_OrdQty > 0 and v_CmlRcvdQty > v_OrdQty
AND v_ItmRelDlvr = 'Not Set';

/*Where its already been updated, update the next row to 0 */

UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_CURSOR_salesorder_coveredqty y
SET x.v_CmlRcvdQty = 0
WHERE x.v_dd_SalesDocNo = y.v_dd_SalesDocNo
AND x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
AND x.v_ItmRelDlvr = 'Not Set'
AND y.flag_v_CmlRcvdQty = 'Y'
AND x.sr_no > y.sr_no
AND y.v_CmlRcvdQty > 0
AND (x.v_CmlRcvdQty <> 0 OR x.v_CmlRcvdQty IS NULL);


UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_CURSOR_salesorder_coveredqty y
SET x.v_CmlRcvdQty = 0
WHERE x.v_dd_SalesDocNo = y.v_dd_SalesDocNo
AND x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
AND x.v_ItmRelDlvr = 'Not Set'
AND y.flag_v_CmlRcvdQty = 'Y'
AND x.sr_no > y.sr_no
AND y.v_CmlRcvdQty > 0
AND  ( x.v_CmlRcvdQty <> 0 OR x.v_CmlRcvdQty IS NULL );


select 'H',TIMESTAMP(LOCAL_TIMESTAMP);


/*Now, wherever v_CmlRcvdQty has been set equal to v_OrdQty, add this much to the next v_CmlRcvdQty



--select v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_CmlRcvdQty,v_nxt_OrdQty,v_nxt_ConfQty,v_Pre_part_id,v_DlvrQty,v_OrdQty,v_ItmRelDlvr,sum_ct_QtyDelivered
--from TMP_CURSOR_salesorder_coveredqty


--END OF UPDATE B1-1,B1-2,B1-3 : UPDATE v_nxt_ConfQty,v_nxt_OrdQty,v_CmlRcvdQty */
/****************************************************************************************************************************************/

UPDATE TMP_CURSOR_salesorder_coveredqty x
SET sum_ct_QtyDelivered = ifnull((SELECT sum(f_dlvr.ct_QtyDelivered)
                              FROM fact_salesorderdelivery f_dlvr inner join dim_salesorderitemstatus sois on f_dlvr.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                              WHERE f_dlvr.dd_SalesDocNo = x.v_dd_SalesDocNo
                                    and f_dlvr.dd_SalesItemNo = x.v_dd_SalesItemNo
                                    and sois.GoodsMovementStatus <> 'Not yet processed'),0);

/*				    
select v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_CmlRcvdQty,v_nxt_OrdQty,v_nxt_ConfQty,v_Pre_part_id,v_DlvrQty,v_OrdQty,v_ItmRelDlvr,sum_ct_QtyDelivered
from TMP_CURSOR_salesorder_coveredqty	
*/			    

select 'I',TIMESTAMP(LOCAL_TIMESTAMP);			    	    

/* --CASE-A for v_DlvrQty. v_nxt_ConfQty = 0
--For any salesdoc,salesitem combination where v_nxt_ConfQty = 0, v_DlvrQty would become the same as sum_ct_QtyDelivered 
--So, any new row for the same salesdoc and salesitem would obviously have v_DlvrQty which is ALWAYS	<= v_ConfQty		    
--So, for all such cases ( v_nxt_ConfQty = 0 ) , v_DlvrQty would be sum_ct_QtyDelivered for first row and 0 for all others */



UPDATE TMP_CURSOR_salesorder_coveredqty x
SET min_sr_no = ( SELECT MIN(sr_no) from TMP_CURSOR_salesorder_coveredqty y where x.v_dd_SalesDocNo = y.v_dd_SalesDocNo and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo );


/* --QUERIES FOR CASE-A for v_DlvrQty. Update only the first row for a given doc/item combination */
UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_DlvrQty = sum_ct_QtyDelivered,zero_v_nxt_ConfQty = 'A'
WHERE v_nxt_ConfQty = 0
AND sr_no = min_sr_no
AND v_ItmRelDlvr = 'Not Set';


/*Update v_DlvrQty to sum_ct_QtyDelivered where sr_no <> x.min_sr_no, but v_nxt_ConfQty is 0 ( and another row for the same doc/item has v_nxt_ConfQty > 0 ) */
UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_CURSOR_salesorder_coveredqty y
SET x.v_DlvrQty = ifnull(x.sum_ct_QtyDelivered,0)
WHERE x.v_nxt_ConfQty = 0
AND x.sr_no <> x.min_sr_no
AND x.v_ItmRelDlvr = 'Not Set'
AND x.v_dd_SalesDocNo = y.v_dd_SalesDocNo
and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
and y.v_nxt_ConfQty > 0
AND (x.v_DlvrQty<> x.sum_ct_QtyDelivered OR x.v_DlvrQty IS NULL OR x.sum_ct_QtyDelivered is null) ;

/* WHERE v_Fact_SalesOrderid in (415175,417363)
--Update the remaining to 0 */

UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_DlvrQty = 0
WHERE v_nxt_ConfQty = 0
AND sr_no <> min_sr_no
AND v_ItmRelDlvr = 'Not Set'
AND (v_DlvrQty <> 0 OR v_DlvrQty IS NULL);

/*--END OF QUERIES FOR CASE-A for v_DlvrQty */

select 'J',TIMESTAMP(LOCAL_TIMESTAMP);

/*
--------------------Case B for v_DlvrQty-----------------------------------

--CASE-B for v_DlvrQty. v_nxt_ConfQty > 0
--Now, consider the salesdoc,salesitem combination where v_nxt_ConfQty > 0
--Say row1 has sum_ct_QtyDelivered = 100. v_ConfQty = 20 for the first row
--So, v_DlvrQty will be 20 for the first row ( and not 100. As 100 > 20 ) 
--Now, for the 2nd row, sum_ct_QtyDelivered = 100, sum(f_so_sq_001.ct_DeliveredQty) = 20. So, v_DlvrQty = 100 - 20 = 80 initially. 
--Now, say v_ConfQty = 25 for the 2nd row. 
--So, v_DlvrQty = 25 ( since 80 > 25 )
--3rd row : sum_ct_QtyDelivered = 100, sum(f_so_sq_001.ct_DeliveredQty) = 20 + 25 = 45. So, v_DlvrQty = 100 - 45 = 55 initially. 
--Now, say v_ConfQty = 10. So, v_DlvrQty = 10. Since 55 > 10
--And so on...
--100,0,20 --> 20
--100,20,25 --> 25
--100,45,10 --> 10
--100,55,30 --> 30
--100,85,40 --> 15  : LK: Here, 100-85 will result in 15. Which is lesser than v_ConfQty ( 40 ). So, in this row, 100-85 will be used. 
--100,100,10 --> 0 : In the previous row, diff was used instead of v_ConfQty. In all further rows for the same doc/item combination, this will remain 0. 
*/


update TMP_CURSOR_salesorder_coveredqty x
 set v_ConfQty_sum_prev = ifnull(( select sum(v_ConfQty) 
 		from TMP_CURSOR_salesorder_coveredqty y
                where x.v_dd_SalesDocNo = y.v_dd_SalesDocNo 
		and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
                and y.sr_no < x.sr_no
                GROUP BY y.v_dd_SalesDocNo,y.v_dd_SalesItemNo),0);
/*--WHERE v_ItmRelDlvr = 'Not Set'\g */

select 'K',TIMESTAMP(LOCAL_TIMESTAMP);			    

UPDATE TMP_CURSOR_salesorder_coveredqty
SET diff_ct_qtydelivered = 0;				    
		

 update TMP_CURSOR_salesorder_coveredqty x		
 SET diff_ct_qtydelivered = sum_ct_QtyDelivered - v_ConfQty_sum_prev
 WHERE v_ItmRelDlvr = 'Not Set'
 AND sum_ct_QtyDelivered > 0; 
 
 /*
 --UPDATE THE sr_no for which diff becomes lesser than conf. All rows after this sr_no ( for that doc/item ) should have 0 v_dlvrqty */
 UPDATE TMP_CURSOR_salesorder_coveredqty x
 SET sr_no_diff_lt_vconfqty = ( SELECT min(y.sr_no) FROM TMP_CURSOR_salesorder_coveredqty y 
 				WHERE x.v_dd_SalesDocNo = y.v_dd_SalesDocNo 
				and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
				AND y.diff_ct_qtydelivered < y.v_confqty )
WHERE v_ItmRelDlvr = 'Not Set';
 
 
UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_DlvrQty = 0 
WHERE v_nxt_ConfQty > 0
AND  v_ItmRelDlvr = 'Not Set'
AND (v_DlvrQty <> 0 OR v_DlvrQty IS NULL);

/*--Now update only those rows where sr_no_diff_lt_vconfqty >= sr_no.  */
 UPDATE TMP_CURSOR_salesorder_coveredqty x
 SET v_DlvrQty = v_ConfQty
 WHERE v_nxt_ConfQty > 0
 AND sr_no_diff_lt_vconfqty > sr_no
 AND v_ItmRelDlvr = 'Not Set'
AND (v_DlvrQty <> v_ConfQty OR v_DlvrQty IS NULL OR v_ConfQty is null);
 
 
 UPDATE TMP_CURSOR_salesorder_coveredqty x
 SET v_DlvrQty = v_ConfQty
 WHERE sr_no_diff_lt_vconfqty IS NULL
 AND v_nxt_ConfQty > 0
 and v_ItmRelDlvr = 'Not Set'
AND (v_DlvrQty <> v_ConfQty OR v_DlvrQty IS NULL OR v_ConfQty is null) ;	
 
 /*
 --WHERE v_nxt_ConfQty > 0
 --AND sr_no_diff_lt_vconfqty > sr_no
 --AND v_ItmRelDlvr = 'Not Set'\g
*/

  UPDATE TMP_CURSOR_salesorder_coveredqty x
 SET v_DlvrQty = diff_ct_qtydelivered
 WHERE v_nxt_ConfQty > 0
 AND sr_no_diff_lt_vconfqty = sr_no
 AND v_ItmRelDlvr = 'Not Set'
AND (v_DlvrQty <> diff_ct_qtydelivered OR v_DlvrQty IS NULL OR diff_ct_qtydelivered is null);
 
  
 UPDATE TMP_CURSOR_salesorder_coveredqty x
 FROM TMP_CURSOR_salesorder_coveredqty y
  SET x.v_DlvrQty = x.diff_ct_qtydelivered
where x.v_dd_SalesDocNo = y.v_dd_SalesDocNo 
and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
AND y.v_nxt_ConfQty > 0
AND x.v_nxt_ConfQty = 0
AND x.v_ItmRelDlvr = 'Not Set'
AND (x.v_DlvrQty <>  x.diff_ct_qtydelivered OR x.v_DlvrQty IS NULL OR x.diff_ct_qtydelivered is null ); 


select 'L',TIMESTAMP(LOCAL_TIMESTAMP);

/*
select v_dd_SalesDocNo,v_dd_SalesItemNo,v_nxt_ConfQty,diff_ct_qtydelivered,sr_no_diff_lt_vconfqty,sr_no,v_ConfQty,v_DlvrQty
from TMP_CURSOR_salesorder_coveredqty\g

select v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_CmlRcvdQty,v_nxt_OrdQty,v_nxt_ConfQty,v_Pre_part_id,v_DlvrQty,v_OrdQty,v_ItmRelDlvr,sum_ct_QtyDelivered,
v_ConfQty_sum_prev,v_fact_salesorderid,diff_ct_qtydelivered,sr_no_diff_lt_vconfqty,v_GIDate
from TMP_CURSOR_salesorder_coveredqty	
*/



/*--Now update v_DlvrQty = 0 where its < 0. Also update all consequent rows to 0 */
UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_DlvrQty = 0, zero_v_nxt_ConfQty = 'P'
WHERE v_DlvrQty < 0;

UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_CURSOR_salesorder_coveredqty y
SET x.v_DlvrQty = 0,x.zero_v_nxt_ConfQty = 'Q'
WHERE x.v_ItmRelDlvr = 'Not Set'
AND x.v_dd_SalesDocNo = y.v_dd_SalesDocNo
and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
AND y.zero_v_nxt_ConfQty IN ( 'P')
AND x.sr_no > y.sr_no;


/*
--------------------End of Case B-----------------------------------


--v_DlvrQtyOrd. This is similar to how v_DlvrQty is processed. 
--For case A, the values updated in v_DlvrQtyOrd would be exactly similar to those in case A of v_DlvrQty ( only that use v_nxt_OrdQty instead of v_nxt_ConfQty ) 

--For any combination, if the first row has v_nxt_OrdQty = 0, update v_DlvrQtyOrd = sum_ct_QtyDelivered
*/
UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_DlvrQtyOrd = sum_ct_QtyDelivered,zero_v_nxt_ordqty = 'Y'
WHERE v_nxt_OrdQty = 0
AND sr_no = min_sr_no
AND v_ItmRelDlvr = 'Not Set';


/*
--Now that the amt of sum_ct_QtyDelivered has been met, all other rows for this combination would have v_DlvrQtyOrd = 0 */

UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_CURSOR_salesorder_coveredqty y
SET x.v_DlvrQtyOrd = 0,x.zero_v_nxt_ordqty = 'Z'

/*--WHERE x.v_nxt_OrdQty = 0
--AND x.sr_no <> x.min_sr_no */

WHERE x.sr_no <> x.min_sr_no
AND x.v_ItmRelDlvr = 'Not Set'
AND x.v_dd_SalesDocNo = y.v_dd_SalesDocNo
and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
AND y.zero_v_nxt_ordqty = 'Y';
/* --and y.v_nxt_OrdQty > 0 \g

--UPDATE TMP_CURSOR_salesorder_coveredqty x
--SET v_DlvrQtyOrd = 0,zero_v_nxt_ordqty = 'Y'
--WHERE v_nxt_OrdQty = 0
--AND sr_no <> min_sr_no
--AND v_ItmRelDlvr = 'Not Set'\g


--------------------Case B for v_DlvrQtyOrd -----------------------------------

--Only handle those doc/item combinations where
--a) sr_no = min_sr_no and v_nxt_OrdQty <> 0
--b) sr_no <> min_sr_no and doc/item same as a above
--This means - handle rows where zero_v_nxt_ordqty = 'N'
*/

DROP TABLE IF EXISTS TMP_CURSOR_salesorder_coveredqty_caseB2;

CREATE TABLE TMP_CURSOR_salesorder_coveredqty_caseB2
AS 
SELECT * FROM TMP_CURSOR_salesorder_coveredqty
WHERE zero_v_nxt_ordqty = 'N';

select 'M',TIMESTAMP(LOCAL_TIMESTAMP);

 update TMP_CURSOR_salesorder_coveredqty_caseB2 x
 set v_OrdQty_sum_prev = ifnull(( select sum(v_OrdQty) 
 		from TMP_CURSOR_salesorder_coveredqty_caseB2 y
                where x.v_dd_SalesDocNo = y.v_dd_SalesDocNo 
		and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
                and y.sr_no < x.sr_no
                GROUP BY y.v_dd_SalesDocNo,y.v_dd_SalesItemNo),0);
/* --WHERE v_ItmRelDlvr = 'Not Set' */

/*--If any of the previous one has v_nxt_ordqty = 0 , then its v_dlvrqtyord is going to become sum_ct_qtydelivered ( or the diff with the sum )
--So for all such cases, update v_ordqty_sum_prev = sum_ct_qtydelivered */

UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2 x
FROM TMP_CURSOR_salesorder_coveredqty_caseB2 y
SET x.v_ordqty_sum_prev = x.sum_ct_qtydelivered
WHERE x.v_dd_SalesDocNo = y.v_dd_SalesDocNo 
and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
AND x.sr_no > y.sr_no
AND y.v_nxt_ordqty = 0
AND (x.v_ordqty_sum_prev <> x.sum_ct_qtydelivered OR x.v_ordqty_sum_prev is null or x.sum_ct_qtydelivered is null );
		
		
UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2
SET diff_v_OrdQty = 0;			

/*
--100,0,20 --> 0
--100,0,20 --> 20
--100,20,25 --> 25
--100,45,10 --> 10
--a.100,55,30 --> 30
--b.100,55,30 ( but with v_nxt_ordqty = 0 )  --> 45. All subsuquent rows would have -->0
--100,85,40 --> 15  : LK: Here, 100-85 will result in 15. Which is lesser than v_ConfQty ( 40 ). So, in this row, 100-85 will be used. 
--
--100,100,10 --> 0 : In the previous row, diff was used instead of v_ConfQty. In all further rows for the same doc/item combination, this will remain 0. 
*/  

 update TMP_CURSOR_salesorder_coveredqty_caseB2 x		
 SET diff_v_OrdQty = sum_ct_QtyDelivered - v_OrdQty_sum_prev
 WHERE v_ItmRelDlvr = 'Not Set'
 AND sum_ct_QtyDelivered > 0 ; 
 
 
/* --At some point, if diff_v_OrdQty < v_OrdQty, mark zero_v_nxt_ordqty = 'F'. So, all sr no after F for the given doc/item should have 0 */
 UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2
 SET v_DlvrQtyOrd = diff_v_OrdQty,zero_v_nxt_ordqty = 'F'
 WHERE diff_v_OrdQty <= v_OrdQty;
 
 
 UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2
 SET v_DlvrQtyOrd = v_OrdQty
 WHERE diff_v_OrdQty > v_OrdQty
AND  (v_DlvrQtyOrd <>  v_OrdQty OR v_DlvrQtyOrd is null or v_OrdQty is null );
 
 /*--v_nxt_ordqty is 0, so v_DlvrQtyOrd = diff. All new rows for same doc/item would have 0 */
 UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2
 SET v_DlvrQtyOrd = diff_v_ordqty,zero_v_nxt_ordqty = 'A'
 WHERE diff_v_OrdQty > v_OrdQty
 AND v_nxt_ordqty = 0;
 
/*
--All rows after the previous row ( zero_v_nxt_ordqty = 'A' ) for the same doc/item would have v_DlvrQtyOrd = 0
--Wherever diff has been updated, update the subsequent rows to 0  */
UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2 x
FROM TMP_CURSOR_salesorder_coveredqty_caseB2 y
SET x.v_DlvrQtyOrd = 0,x.zero_v_nxt_ordqty = 'B'
WHERE x.v_ItmRelDlvr = 'Not Set'
AND x.v_dd_SalesDocNo = y.v_dd_SalesDocNo
and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
AND y.zero_v_nxt_ordqty IN ( 'A','F')
AND x.sr_no > y.sr_no;
 
 
/*
 ----UPDATE THE sr_no for which diff becomes lesser than conf. All rows after this sr_no ( for that doc/item ) should have 0 v_dlvrqty
 --UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2 x
 --SET sr_no_diff_lt_v_OrdQty = ( SELECT y.sr_no FROM TMP_CURSOR_salesorder_coveredqty_caseB2 y 
 --				WHERE x.v_dd_SalesDocNo = y.v_dd_SalesDocNo 
--				and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
--				AND y.diff_v_OrdQty < y.v_OrdQty )
--WHERE v_ItmRelDlvr = 'Not Set'\g

--Note that sr_no_diff_lt_v_OrdQty will be null for all rows with a doc/item combination if the diff never becomes less than v_OrdQty
 
 
--UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2
--SET v_DlvrQtyOrd = 0 
--WHERE v_nxt_OrdQty > 0
--AND  v_ItmRelDlvr = 'Not Set'\g

--Now update only those rows where sr_no_diff_lt_vconfqty >= sr_no. 
 --UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2 x
 --SET v_DlvrQtyOrd = v_OrdQty
 --WHERE v_nxt_OrdQty > 0
 --AND sr_no_diff_lt_v_OrdQty > sr_no
 --AND v_ItmRelDlvr = 'Not Set'
 --AND sum_ct_QtyDelivered > 0\g
 
 --UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2 x
 --SET v_DlvrQtyOrd = v_OrdQty
 --WHERE sr_no_diff_lt_v_OrdQty IS NULL
 --AND v_nxt_OrdQty > 0
 --and v_ItmRelDlvr = 'Not Set'
 --AND sum_ct_QtyDelivered > 0\g
 
 --UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2 x
 --SET v_DlvrQtyOrd = diff_v_OrdQty
 --WHERE v_nxt_OrdQty > 0
 --AND sr_no_diff_lt_v_OrdQty = sr_no
 --AND v_ItmRelDlvr = 'Not Set'\g


 --UPDATE TMP_CURSOR_salesorder_coveredqty_caseB2 x
 --FROM TMP_CURSOR_salesorder_coveredqty y
 -- SET x.v_DlvrQtyOrd = x.diff_v_OrdQty
--where x.v_dd_SalesDocNo = y.v_dd_SalesDocNo 
--and x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
--AND y.v_nxt_OrdQty > 0
--AND x.v_nxt_OrdQty = 0
--AND x.v_ItmRelDlvr = 'Not Set'\g */

UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_CURSOR_salesorder_coveredqty_caseB2 y
SET x.v_DlvrQtyOrd = y.v_DlvrQtyOrd
WHERE x.sr_no = y.sr_no
AND (x.v_DlvrQtyOrd <>  y.v_DlvrQtyOrd OR x.v_DlvrQtyOrd is null or y.v_DlvrQtyOrd is null);


/*--------------------End of Case B for v_DlvrQtyOrd -----------------------------------*/


select 'N',TIMESTAMP(LOCAL_TIMESTAMP);

-----------------------------Update v_DlvrQtyOrd and v_CmlRcvdQty where v_ItmRelDlvr is not 'Not Set'-------------------------------------
DROP TABLE IF EXISTS TMP_v_ItmRelDlvr_not_notset;
CREATE TABLE TMP_v_ItmRelDlvr_not_notset
AS 
SELECT * 
FROM TMP_CURSOR_salesorder_coveredqty;

/*--WHERE v_ItmRelDlvr <> 'Not Set'
--Note that v_DlvrQtyOrd/v_CmlRcvdQty is null for all these rows

--Get the max previous sr_no that has v_ItmRelDlvr = 'Not Set'


--DROP TABLE IF EXISTS TMP_CURSOR_salesorder_coveredqty_notset
--CREATE TABLE TMP_CURSOR_salesorder_coveredqty_notset
--as
--SELECT * 
--FROM TMP_CURSOR_salesorder_coveredqty
--WHERE v_ItmRelDlvr = 'Not Set' */

select 'N1',TIMESTAMP(LOCAL_TIMESTAMP);

/*
--DROP TABLE IF EXISTS TMP_v_ItmRelDlvr_not_notset1
--CREATE TABLE TMP_v_ItmRelDlvr_not_notset1
--AS
--SELECT row_number() over() iden,x.sr_no,y.sr_no as max_y_sr_no
--FROM TMP_v_ItmRelDlvr_not_notset x, TMP_CURSOR_salesorder_coveredqty_notset y
--WHERE x.sr_no > y.sr_no
--AND y.v_ItmRelDlvr = 'Not Set'

--UPDATE TMP_v_ItmRelDlvr_not_notset x
--SET max_prev_notset_sr_no = ( SELECT max_y_sr_no
--				FROM  TMP_v_ItmRelDlvr_not_notset1 y
--				WHERE y.sr_no = x.sr_no)

--e.g 
--1, Not Set    
--2, X  - 1
--3, X  - 1
--4, X  - 1
--5, Not Set
--6, Not Set
--7, Not Set
--8, X   - 7
--9 , X  - 7

--This query will update max_prev to 1 for row with sr_no = 2. Similarly, it will set max_prev to 7 for row with sr_no = 8 */
UPDATE TMP_v_ItmRelDlvr_not_notset x
FROM TMP_v_ItmRelDlvr_not_notset y
SET x.max_prev_notset_sr_no = y.sr_no
WHERE y.v_ItmRelDlvr = 'Not Set'
AND x.v_ItmRelDlvr <> 'Not Set'
AND x.sr_no > y.sr_no
AND x.sr_no = y.sr_no + 1
AND ( x.max_prev_notset_sr_no <> y.sr_no or x.max_prev_notset_sr_no is null);


/*--Now for the remaining ones, use the usual max with group by for x.sr_no > y.sr_no */
DROP TABLE IF EXISTS tmp_update_all_prev;
Drop table if exists TMP_v_ItmRelDlvr_not_notset_01;
Drop table if exists TMP_v_ItmRelDlvr_not_notset_02;

Create table TMP_v_ItmRelDlvr_not_notset_01 as Select * from TMP_v_ItmRelDlvr_not_notset where max_prev_notset_sr_no is null;
Create table TMP_v_ItmRelDlvr_not_notset_02 as Select * from TMP_v_ItmRelDlvr_not_notset where max_prev_notset_sr_no is not null;

DROP TABLE IF EXISTS TMP_v_ItmRelDlvr_not_notset_02_b;
CREATE TABLE TMP_v_ItmRelDlvr_not_notset_02_b
AS
SELECT y.sr_no,y.max_prev_notset_sr_no,row_number() over (order by y.sr_no) sr_no_inner, cast(99999 as int) next_sr_no
from TMP_v_ItmRelDlvr_not_notset_02 y;

update TMP_v_ItmRelDlvr_not_notset_02_b x
from  TMP_v_ItmRelDlvr_not_notset_02_b y
set x.next_sr_no = y.sr_no
where x.sr_no_inner = y.sr_no_inner - 1;

create table tmp_update_all_prev
AS
SELECT x.sr_no,cast(99999 as int) max_prev_notset_sr_no
from TMP_v_ItmRelDlvr_not_notset_01 x;

UPDATE tmp_update_all_prev x
FROM  TMP_v_ItmRelDlvr_not_notset_02_b y
SET x.max_prev_notset_sr_no = y.max_prev_notset_sr_no
where x.sr_no > y.sr_no
AND x.sr_no < y.next_sr_no
AND ifnull(x.max_prev_notset_sr_no,-99) <> y.max_prev_notset_sr_no;


Drop table if exists TMP_v_ItmRelDlvr_not_notset_01;
Drop table if exists TMP_v_ItmRelDlvr_not_notset_02;
DROP TABLE IF EXISTS TMP_v_ItmRelDlvr_not_notset_02_b;


update TMP_v_ItmRelDlvr_not_notset x 
set x.max_prev_notset_sr_no = ( select y.max_prev_notset_sr_no
					from tmp_update_all_prev y 
					where x.sr_no = y.sr_no 
					and x.max_prev_notset_sr_no is null 
					and y.max_prev_notset_sr_no is not null )
where x.max_prev_notset_sr_no is null
AND x.v_ItmRelDlvr = 'X';

/*--update TMP_v_ItmRelDlvr_not_notset x 
--set x.max_prev_notset_sr_no = ( select max(y.max_prev_notset_sr_no) 
--					from TMP_v_ItmRelDlvr_not_notset y 
--					where x.sr_no > y.sr_no 
--					--AND x.sr_no = y.sr_no + 1
--					and x.max_prev_notset_sr_no is null 
--					and y.max_prev_notset_sr_no is not null )
--where x.max_prev_notset_sr_no is null
--AND x.v_ItmRelDlvr = 'X'

--drop table if exists tmp_x
--CREATE TABLE tmp_x
--AS 
--SELECT * FROM TMP_v_ItmRelDlvr_not_notset
--WHERE max_prev_notset_sr_no is not null

--drop table if exists tmp_y
--create table tmp_y
--AS
--select x.sr_no,max(y.max_prev_notset_sr_no) as max_prev_notset_sr_no
--FROM TMP_v_ItmRelDlvr_not_notset x, tmp_x y
--where   x.v_ItmRelDlvr = 'X'
--AND x.sr_no > y.sr_no
--GROUP by x.sr_no

--UPDATE TMP_v_ItmRelDlvr_not_notset x
--FROM tmp_y y
--SET max_prev_notset_sr_no = y.max_prev_notset_sr_no
--WHERE x.v_ItmRelDlvr = 'X'
--And x.max_prev_notset_sr_no IS NULL
--AND x.sr_no = y.sr_no
------------- */					

UPDATE 	TMP_v_ItmRelDlvr_not_notset x
SET prev_notset_v_CmlRcvdQty = ( SELECT y.v_CmlRcvdQty 
				FROM TMP_CURSOR_salesorder_coveredqty y
				WHERE x.max_prev_notset_sr_no = y.sr_no );	

select 'N2',TIMESTAMP(LOCAL_TIMESTAMP);
				
UPDATE 	TMP_v_ItmRelDlvr_not_notset x
SET prev_notset_v_DlvrQtyOrd = ( SELECT y.v_DlvrQtyOrd  
				FROM TMP_CURSOR_salesorder_coveredqty y
				WHERE x.max_prev_notset_sr_no = y.sr_no );
				

UPDATE 	TMP_CURSOR_salesorder_coveredqty x
SET v_CmlRcvdQty = ( SELECT prev_notset_v_CmlRcvdQty 
			FROM TMP_v_ItmRelDlvr_not_notset y
			WHERE x.sr_no = y.sr_no )
WHERE v_ItmRelDlvr <> 'Not Set';

UPDATE 	TMP_CURSOR_salesorder_coveredqty x
SET v_DlvrQtyOrd = ( SELECT prev_notset_v_DlvrQtyOrd 
			FROM TMP_v_ItmRelDlvr_not_notset y
			WHERE x.sr_no = y.sr_no ),zero_v_nxt_ordqty = 'U'
WHERE v_ItmRelDlvr <> 'Not Set';


select 'N3',TIMESTAMP(LOCAL_TIMESTAMP);

/*--THIS IS A QUICKFIX. To really fix, this, identify the non-U row which has -ve v_DlvrQtyOrd */
UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_DlvrQtyOrd = 0 
WHERE zero_v_nxt_ordqty = 'U'
AND v_DlvrQtyOrd < 0;
		


UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_OpenQty = v_ConfQty - v_DlvrQty;

UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_OpenQty = 0
WHERE v_OpenQty < 0;

select 'O',TIMESTAMP(LOCAL_TIMESTAMP);

/*
---------------------------------------coveredqty----------------------------------
*/

DROP TABLE IF EXISTS TMP_DISTINCT_dim_Partid;
CREATE TABLE TMP_DISTINCT_dim_Partid
AS
SELECT DISTINCT v_dim_part_id
FROM TMP_CURSOR_salesorder_coveredqty;

ALTER TABLE TMP_DISTINCT_dim_Partid
ADD column sum_fact_ivaging DECIMAL(18,4);


UPDATE TMP_DISTINCT_dim_Partid x
SET sum_fact_ivaging = ifnull(( SELECT sum(f_inv_sq_001.ct_StockQty)
                                FROM fact_inventoryaging f_inv_sq_001
                                WHERE f_inv_sq_001.dim_Partid = x.v_dim_part_id),0);


select 'O2',TIMESTAMP(LOCAL_TIMESTAMP);
				
/*--Now update for each 	v_dim_part_id according to sr_no, until it crosses sum_fact_ivaging */

DROP TABLE IF EXISTS v_CoveredQty_calc_TMP;
CREATE TABLE v_CoveredQty_calc_TMP
AS
SELECT  x.v_dim_part_id, v_GIDate, v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,y.sum_fact_ivaging,v_CoveredQty sum_prev_v_CoveredQty,
v_OpenQty,v_CoveredQty,
 row_number() over (order by x.v_dim_part_id, v_GIDate, v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo) sr_no
FROM TMP_CURSOR_salesorder_coveredqty x,TMP_DISTINCT_dim_Partid y
WHERE x.v_OpenQty <> 0
AND x.v_dim_part_id = y.v_dim_part_id
ORDER BY x.v_dim_part_id, v_GIDate, v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo offset 0;

UPDATE v_CoveredQty_calc_TMP
set sum_prev_v_CoveredQty = 0;


 update v_CoveredQty_calc_TMP a set sum_prev_v_CoveredQty = ( select sum(v_OpenQty) from v_CoveredQty_calc_TMP b
                                                        where a.v_dim_part_id = b.v_dim_part_id
                                                        and b.sr_no < a.sr_no
                                                        GROUP BY b.v_dim_part_id );
														
UPDATE v_CoveredQty_calc_TMP
set sum_prev_v_CoveredQty = 0
WHERE sum_prev_v_CoveredQty IS NULL;	

UPDATE 	v_CoveredQty_calc_TMP
SET 	v_CoveredQty = 	v_OpenQty
WHERE sum_fact_ivaging - sum_prev_v_coveredqty > v_OpenQty;


UPDATE 	v_CoveredQty_calc_TMP
SET 	v_CoveredQty = 	 sum_fact_ivaging - sum_prev_v_coveredqty
WHERE sum_fact_ivaging - sum_prev_v_coveredqty <= v_OpenQty;	


UPDATE 	v_CoveredQty_calc_TMP
SET v_CoveredQty = 0
WHERE v_CoveredQty < 0;

UPDATE 	TMP_CURSOR_salesorder_coveredqty x
FROM 	v_CoveredQty_calc_TMP y
SET x.v_CoveredQty = y.v_CoveredQty
WHERE x.v_dim_part_id = y.v_dim_part_id
AND x.v_GIDate = y.v_GIDate
AND x.v_dd_SalesDocNo = y.v_dd_SalesDocNo
AND x.v_dd_SalesItemNo = y.v_dd_SalesItemNo
AND x.v_dd_ScheduleNo = y.v_dd_ScheduleNo;

UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_CoveredQty = 0 
WHERE v_CoveredQty is null;

/*
--For a given partid, get sum_fact_ivaging. Say partid p1, sum s1
-- row_number() over (order by dim_partid, mad.DateValue, dd_SalesDocNo, dd_SalesItemNo, dd_ScheduleNo) sr_no

 
UPDATE TMP_CURSOR_salesorder_coveredqty x
    SET v_CoveredQty = ifnull((SELECT sum(f_inv_sq_001.ct_StockQty + f_inv_sq_001.ct_BlockedStock
                                          + f_inv_sq_001.ct_StockInQInsp + f_inv_sq_001.ct_StockInTransfer
                                          + f_inv_sq_001.ct_TotalRestrictedStock)
                                FROM fact_inventoryaging f_inv_sq_001
                                WHERE f_inv_sq_001.dim_Partid = v_dim_part_id),0)
                        - ifnull((SELECT sum(f_so_sq_001.ct_OnHandCoveredQty)
                                  FROM fact_salesorder f_so_sq_001 INNER JOIN dim_date d_GI_sq_001
                                        ON f_so_sq_001.Dim_DateidGoodsIssue = d_GI_sq_001.Dim_Dateid
                                  WHERE f_so_sq_001.Dim_Partid = v_dim_part_id
                                        AND (d_GI_sq_001.DateValue < v_GIDate
                                            OR (d_GI_sq_001.DateValue = v_GIDate
                                                AND (f_so_sq_001.dd_SalesDocNo < v_dd_SalesDocNo
                                                      OR (f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                                          AND (f_so_sq_001.dd_SalesItemNo < v_dd_SalesItemNo
                                                              OR (f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                                                  AND f_so_sq_001.dd_ScheduleNo < v_dd_ScheduleNo))))))),0)
								  
*/								  

UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_Pre_CoveredQty = ( SELECT v_CoveredQty FROM TMP_CURSOR_salesorder_coveredqty y where y.sr_no = x.sr_no - 1 );


call vectorwise(combine 'TMP_CURSOR_salesorder_coveredqty');

ALTER TABLE TMP_CURSOR_salesorder_coveredqty
ADD COLUMN v_CoveredQty_orig DECIMAL(18,4);

UPDATE TMP_CURSOR_salesorder_coveredqty x
SET v_CoveredQty_orig = IFNULL((SELECT sum_fact_ivaging FROM TMP_DISTINCT_dim_Partid y
				 WHERE x.v_dim_part_id = y.v_dim_part_id ),0);
call vectorwise(combine 'TMP_CURSOR_salesorder_coveredqty');

UPDATE TMP_CURSOR_salesorder_coveredqty
set v_CoveredQty_orig = v_CoveredQty
WHERE v_CoveredQty_orig IS NULL;


UPDATE TMP_CURSOR_salesorder_coveredqty
SET v_CoveredQty = 0 
where v_CoveredQty < 0;

/* --Assuming v_OpenQty is always >0

--UPDATE TMP_CURSOR_salesorder_coveredqty
--SET v_CoveredQty = v_OpenQty
--where v_CoveredQty > v_OpenQty
--AND v_CoveredQty >0 

--Update pre_coveredqty where curr v_dim_part_id <> prev v_dim_part_id */

DROP TABLE IF EXISTS TMP_ONEMORE;
CREATE TABLE TMP_ONEMORE
AS
SELECT v_dim_part_id,sum(v_coveredqty) sum_v_coveredqty
FROM TMP_CURSOR_salesorder_coveredqty
GROUP BY v_dim_part_id;

UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_CURSOR_salesorder_coveredqty y,TMP_ONEMORE z
SET x.v_coveredqty = x.v_coveredqty_orig
WHERE x.sr_no = y.sr_no - 1
AND y.v_pre_part_id <> y.v_dim_part_id
AND x.v_dim_part_id = z.v_dim_part_id
AND z.sum_v_coveredqty = 0;

ALTER TABLE TMP_ONEMORE
ADD COLUMN sum_fact_ivaging DECIMAL(18,4);

ALTER TABLE TMP_ONEMORE
ADD COLUMN max_sr_no INT;

call vectorwise(combine 'TMP_ONEMORE');

UPDATE TMP_ONEMORE x
SET sum_fact_ivaging = ( SELECT sum_fact_ivaging FROM TMP_DISTINCT_dim_Partid y where x.v_dim_part_id = y.v_dim_part_id );

UPDATE TMP_ONEMORE x
SET sum_v_coveredqty = ( SELECT SUM(v_coveredqty) FROM TMP_CURSOR_salesorder_coveredqty y where x.v_dim_part_id = y.v_dim_part_id );

UPDATE TMP_ONEMORE x
SET max_sr_no = ( SELECT MAX(sr_no) FROM TMP_CURSOR_salesorder_coveredqty y where x.v_dim_part_id = y.v_dim_part_id );

/*--For the last row, it does not check for precoveredqty

--For the last row for any part id, add covered qty so that total becomes same as sum_fact_ivaging */
UPDATE TMP_CURSOR_salesorder_coveredqty x
FROM TMP_ONEMORE y
SET x.v_coveredqty = x.v_coveredqty + ( y.sum_fact_ivaging - y.sum_v_coveredqty)
WHERE x.sr_no = y.max_sr_no 
AND x.sr_no NOT IN ( SELECT MAX(sr_no) sr_no_max_overall FROM TMP_CURSOR_salesorder_coveredqty);

select 'P',TIMESTAMP(LOCAL_TIMESTAMP);

    UPDATE fact_salesorder f
    FROM TMP_CURSOR_salesorder_coveredqty x
    SET ct_OnHandCoveredQty = x.v_CoveredQty
    WHERE Fact_SalesOrderid = x.v_Fact_SalesOrderid;

    UPDATE fact_salesorder f
    FROM TMP_CURSOR_salesorder_coveredqty x
    SET ct_DeliveredQty = x.v_DlvrQty,
        ct_ShippedAgnstOrderQty = x.v_DlvrQtyOrd,
        ct_CmlQtyReceived = x.v_CmlRcvdQty
    WHERE Fact_SalesOrderid = x.v_Fact_SalesOrderid
	and f.dd_ItemRelForDelv = 'Not Set';
	
call vectorwise(combine 'fact_salesorder');

DROP TABLE IF EXISTS TMP_CURSOR_salesorder_coveredqty;
DROP TABLE IF EXISTS TMP2_fact_salesorder_v_nxt_ConfQty;
DROP TABLE IF EXISTS TMP_CURSOR_salesorder_coveredqty_caseB2;
DROP TABLE IF EXISTS TMP_v_ItmRelDlvr_not_notset;
DROP TABLE IF EXISTS TMP_CURSOR_salesorder_coveredqty_notset;
DROP TABLE IF EXISTS TMP_DISTINCT_dim_Partid;
DROP TABLE IF EXISTS v_CoveredQty_calc_TMP;
DROP TABLE IF EXISTS TMP_ONEMORE;
DROP TABLE IF EXISTS TMP3_fact_salesorder_v_nxt_ConfQty;
DROP TABLE IF EXISTS TMP4_fact_salesorder_v_nxt_ConfQty;

select 'END OF PROC bi_populate_salesorder_coveredqty',TIMESTAMP(LOCAL_TIMESTAMP);
