DROP TABLE IF EXISTS tmp_dim_inspectionlotstatus;
CREATE TABLE  tmp_dim_inspectionlotstatus AS 
SELECT distinct j1.JEST_OBJNR AS dd_ObjectNumber,
       'Not Set' AS ApprovalGranted,
       'Not Set' AS ApprovalNotGranted,
       'Not Set' AS Created,
       'Not Set' AS LotCancelled,
       'Not Set' as LotDetailDataDeleted,
       'Not Set' as LotSampleDataDeleted,
       'Not Set' as Released,
       'Not Set' as SkippedLot,
       'Not Set' as UsageDecisionMade FROM JEST_QALS j1;

UPDATE   tmp_dim_inspectionlotstatus
SET ApprovalGranted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0322' 
AND j.JEST_INACT is NULL; 

UPDATE   tmp_dim_inspectionlotstatus
SET ApprovalNotGranted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0325' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET Created = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0001' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotCancelled = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0224' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotDetailDataDeleted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0227' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotSampleDataDeleted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0228' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET Released = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0002' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET SkippedLot = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0209' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET UsageDecisionMade = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0218' 
AND j.JEST_INACT is NULL;
     
UPDATE fact_inspectionlot fil
   SET fil.dim_inspectionlotstatusid = ils.Dim_InspectionLotStatusId
	FROM fact_inspectionlot fil,
       dim_inspectionlotstatus ils,
       tmp_dim_inspectionlotstatus j1	
 WHERE ils.ApprovalGranted = j1.ApprovalGranted
       AND ils.ApprovalNotGranted = j1.ApprovalNotGranted 
       AND ils.Created = j1.Created 
       AND ils.LotCancelled = j1.LotCancelled
       AND ils.LotDetailDataDeleted = j1.LotDetailDataDeleted
       AND ils.LotSampleDataDeleted = j1.LotSampleDataDeleted
       AND ils.Released = j1.Released 
       AND ils.SkippedLot = j1.SkippedLot
       AND ils.UsageDecisionMade = j1.UsageDecisionMade			 
       AND j1.dd_ObjectNumber = fil.dd_ObjectNumber;
DROP TABLE IF EXISTS tmp_dim_inspectionlotstatus;

/* Update Usage Decision Code */

DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot;	 
CREATE TABLE tmp_qave_fact_inspectionlot AS SELECT QAVE_PRUEFLOS,QAVE_VKATART,QAVE_VCODEGRP,QAVE_VERSIONCD,QAVE_VCODE,QAVE_VAEDATUM,QAVE_VNAME,
								MAX(QAVE_ZAEHLER) MAX_QAVE_ZAEHLER
                                 FROM qave
								GROUP BY QAVE_PRUEFLOS,QAVE_VKATART,QAVE_VCODEGRP,QAVE_VERSIONCD,QAVE_VCODE,QAVE_VAEDATUM,QAVE_VNAME;							
UPDATE fact_inspectionlot il
   SET Dim_InspUsageDecisionId = Dim_CodeTextId,
       dd_UsageDecisionMadeBy = ifnull(QAVE_VNAME,'Not Set')
   FROM fact_inspectionlot il,
       dim_inspectionlotstatus ils,
       tmp_qave_fact_inspectionlot q,
       dim_CodeText t,
       dim_plant pl
 WHERE q.QAVE_PRUEFLOS = il.dd_inspectionlotno
       AND ils.dim_inspectionlotstatusid = il.dim_inspectionlotstatusid
       AND pl.dim_plantid = il.dim_plantid
       AND q.QAVE_VKATART = t.Catalog
       AND q.QAVE_VCODEGRP = t.CodeGroup
       AND q.QAVE_VERSIONCD = t.Version
       AND q.QAVE_VCODE = t.Code
       AND ils.UsageDecisionMade = 'X';

UPDATE fact_inspectionlot il
   SET Dim_DateIdActualInspectionEnd = ifnull(dt.dim_dateid,1)
   FROM fact_inspectionlot il,
       dim_inspectionlotstatus ils,
       tmp_qave_fact_inspectionlot q,
       dim_CodeText t,
       dim_plant pl,
	   dim_date dt
 WHERE q.QAVE_PRUEFLOS = il.dd_inspectionlotno
       AND ils.dim_inspectionlotstatusid = il.dim_inspectionlotstatusid
       AND pl.dim_plantid = il.dim_plantid
       AND q.QAVE_VKATART = t.Catalog
       AND q.QAVE_VCODEGRP = t.CodeGroup
       AND q.QAVE_VERSIONCD = t.Version
       AND q.QAVE_VCODE = t.Code
       AND ils.UsageDecisionMade = 'X'
	   AND dt.CompanyCode = pl.CompanyCode AND dt.DateValue = q.QAVE_VAEDATUM;
DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot;

DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot2;	 
CREATE TABLE tmp_qave_fact_inspectionlot2 AS 
SELECT QAVE_PRUEFLOS,QAVE_VDATUM,QAVE_VAENAME,ROW_NUMBER() over (PARTITION BY QAVE_PRUEFLOS ORDER BY QAVE_ZAEHLER DESC) AS RNK
FROM QAVE;

/*UPDATE fact_inspectionlot il
SET il.dim_dateidUsageDecisionMade=ifnull(dd.dim_datei,1)
FROM fact_inspectionlot il, tmp_qave_fact_inspectionlot2 q2,
dim_plant pl, dd.dim_dateid
WHERE il.dd_inspectionlotno=q2.QAVE_PRUEFLOS
AND il.dim_plantid=pl.dim_plantid	
AND q2.RNK=1 AND dd.datevalue=q2.QAVE_VDATUM AND dd.CompanyCode=pl.CompanyCode*/

UPDATE fact_inspectionlot il
SET il.dim_dateidUsageDecisionMade=ifnull(dd.dim_dateid,1)
FROM fact_inspectionlot il, tmp_qave_fact_inspectionlot2 q2,
dim_plant pl, dim_date dd
WHERE il.dd_inspectionlotno=q2.QAVE_PRUEFLOS
AND il.dim_plantid=pl.dim_plantid	
AND q2.RNK=1 AND dd.datevalue=q2.QAVE_VDATUM AND dd.CompanyCode=pl.CompanyCode;

UPDATE fact_inspectionlot il
SET il.dd_UsageDecisionChangeby=ifnull(q2.QAVE_VAENAME,'Not Set')
FROM fact_inspectionlot il, tmp_qave_fact_inspectionlot2 q2,
dim_plant pl
WHERE il.dd_inspectionlotno=q2.QAVE_PRUEFLOS
AND il.dim_plantid=pl.dim_plantid	
AND q2.RNK=1;

DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot2;	 
