/* ################################################################################################################## */
/* */
/*   Script         : bi_populate_salesorder_fact */
/*   Author         : Ashu */
/*   Created On     : 17 Jan 2013 */
/*  */
/*  */
/*   Description    : Stored Proc bi_populate_salesorder_fact from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*   18 Mar 2014     Lokesh	1.48	         Added ct_CumOrderQty population */
/*   14 Feb 2014     George     1.47             Added Dim_CustomerGroup4id */
/*   12 Feb 2014     George     1.46             Added Dim_ScheduleDeliveryBlockid */
/*   03 Feb 2014     George     1.45		 Added: dim_CustomerConditionGroups1id, dim_CustomerConditionGroups2id, dim_CustomerConditionGroups3id  */
/*   29 Nov 2013    Lokesh	1.40             Changed amt_ScheduleTotal calculation to use VBAP_NETWR when vbep_wmeng = VBAP_KWMENG */
/* 						As discussed with Issam, fixed the issue with vbak_vbap_vbep and vbak_vbap updates */
/*						update from item data if it exists (posnr <> 0), otherwise update from header data (posnr = 0) */
/*	 26 Sep 2013     Issam      1.33             Added fields dd_SOCreateTime dd_ReqDeliveryTime, dd_SOLineCreateTime,
												 dd_DeliveryTime, dd_PlannedGITime  								  */
/*   07 Sep 2013     Lokesh     1.25             Exchange Rate and Currency changes. Merged prev versions                               */
/*   29 Aug 2013     Lokesh     1.22             Changes done for amt_Subtotal3inCustConfig_Billing and amt_Subtotal3_OrderQty.  */
/*   29 Aug 2013     Shanthi    1.21		 	 Made fixes related to the Textron    */
/*   20 Aug 2013     Shanthi    1.20               added new fields */
/*   11 Mar 2013     Lokesh     1.3              Sync with Shanthi's latest changes in mysql   */
/*   25 Mar 2013     Lokesh     1.2				 Changes to fix Dim_BillToPartyPartnerFunctionId discrepancies - add order by cpf.PartnerCounter desc limit 1 */
/*   20 Mar 2013     Lokesh     1.1              Minor changes, sync with current prod/qa version. */
/*   17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise > */
/* #################################################################################################################### */

/* Refresh all tables in VW from MySQL */
/* cd /home/fusionops/ispring/db/schema_migration/bin	*/
/* */

select 'START OF PROC VW_bi_populate_salesorder_fact',TIMESTAMP(LOCAL_TIMESTAMP);

Drop table if exists variable_holder_701;

Declare global temporary table variable_holder_701(
	pGlobalCurrency,
	pCustomPartnerFunctionKey,
	pCustomPartnerFunctionKey1,
	pCustomPartnerFunctionKey2,
	pBillToPartyPartnerFunction,
	pPayerPartnerFunction)
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD'),varchar(3)),
	CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'custom.partnerfunction.key'),
              'Not Set'), varchar(10)),
	CAST( ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'custom.partnerfunction.key1'),
              'Not Set'),varchar(10)),
	CAST( ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'custom.partnerfunction.key2'),
              'Not Set'),varchar(10)),
	CAST('RE',varchar(5)),
	CAST('RG',varchar(5)) ON COMMIT PRESERVE ROWS;


/* 
  UPDATE vbak_vbap_vbep p
   FROM vbak_vbap_vbkd k 
  SET p.VBAP_STCUR = k.VBKD_KURSK, p.PRSDT = k.VBKD_PRSDT
  where p.vbak_vbeln = k.VBKD_VBELN

  UPDATE vbak_vbap p
   FROM vbak_vbap_vbkd k 
  SET p.VBAP_STCUR = k.VBKD_KURSK, p.PRSDT = k.VBKD_PRSDT
  where p.vbap_vbeln = k.VBKD_VBELN
*/
/* update from item data if it exists (posnr <> 0), otherwise update from header data (posnr = 0) */
  UPDATE vbak_vbap_vbep p
  FROM vbak_vbap_vbkd k
  SET p.VBAP_STCUR = k.VBKD_KURSK
  where p.vbak_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0 
  and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);


  UPDATE vbak_vbap_vbep p
  FROM vbak_vbap_vbkd k
  SET p.PRSDT = k.VBKD_PRSDT
  where p.vbak_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0 
  and ifnull(p.PRSDT,'01011900') <> ifnull(k.VBKD_PRSDT,'01011900');

  UPDATE vbak_vbap p
  FROM vbak_vbap_vbkd k
  SET p.VBAP_STCUR = k.VBKD_KURSK
  where p.vbap_vbeln = k.VBKD_VBELN and VBKD_POSNR = 0 
  and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);

  UPDATE vbak_vbap p
  FROM vbak_vbap_vbkd k
  SET p.PRSDT = k.VBKD_PRSDT
  where p.vbap_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0 
  AND ifnull(p.PRSDT,'01011900') <> ifnull(k.VBKD_PRSDT,'01011900');

/* Update item data if it exists. So this will overwrite updates from header, where a match is found */

  UPDATE vbak_vbap_vbep p
  FROM vbak_vbap_vbkd k
  SET p.VBAP_STCUR = k.VBKD_KURSK
  where p.vbak_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
  and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);

  UPDATE vbak_vbap_vbep p
  FROM vbak_vbap_vbkd k 
  SET p.PRSDT = k.VBKD_PRSDT
  where p.vbak_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
  AND ifnull(p.PRSDT,'01011900') <> ifnull(k.VBKD_PRSDT,'01011900');

  UPDATE vbak_vbap p
  FROM vbak_vbap_vbkd k 
  SET p.VBAP_STCUR = k.VBKD_KURSK
  where p.vbap_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
  and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);

  UPDATE vbak_vbap p
  FROM vbak_vbap_vbkd k 
  SET p.PRSDT = k.VBKD_PRSDT
  where p.vbap_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
  AND ifnull(p.PRSDT,'01011900') <> ifnull(k.VBKD_PRSDT,'01011900');



DELETE FROM fact_salesorder
WHERE exists (select 1 from CDPOS_VBAK a where a.CDPOS_OBJECTID = dd_SalesDocNo AND a.CDPOS_CHNGIND = 'D' AND a.CDPOS_TABNAME = 'VBAK');

drop table if exists Dim_CostCenter_701;
create table Dim_CostCenter_701 as Select first 0 * from Dim_CostCenter ORDER BY ValidTo DESC;
drop table if exists dim_profitcenter_701;
create table dim_profitcenter_701 as Select first 0 * from dim_profitcenter  ORDER BY ValidTo ASC;



/* ################################### 12 Apr changes - sync with mysql changes	by Shanthi part 2 starts ###################################	*/	
/* Create temporary tables to avoide multiple joins for updating dd_CreditLimit, Dim_CustomerRiskCategoryId and dd_CreditRep */

/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/
DROP TABLE IF EXISTS TMP_UPD_dd_CreditRep1;		
CREATE TABLE TMP_UPD_dd_CreditRep1
AS
SELECT case when b.NAME_FIRST is null then b.NAME_LAST else b.NAME_FIRST + ' ' + ifnull(b.NAME_LAST,'') end upd_dd_CreditRep1,
d.BUT050_PARTNER2
from cvi_cust_link c inner join but000 b1 on c.PARTNER_GUID = b1.PARTNER_GUID
                          inner join BUT050 d on b1.PARTNER = d.BUT050_PARTNER2
                          inner join but000 b on b.PARTNER = d.BUT050_PARTNER1
WHERE ANSIDATE(LOCAL_TIMESTAMP) between d.BUT050_DATE_FROM and d.BUT050_DATE_TO						  
AND  b.NAME_FIRST is not null or b.NAME_LAST is not null and b.BU_GROUP = 'CRED';


/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/
DROP TABLE IF EXISTS TMP_UPD_dd_CreditLimit;		
CREATE TABLE TMP_UPD_dd_CreditLimit
AS
SELECT  c.CUSTOMER,a.CREDIT_LIMIT
from  UKMBP_CMS_SGM a
inner join  BUT000 b on a.PARTNER = b.PARTNER and  ANSIDATE(LOCAL_TIMESTAMP) <= a.LIMIT_VALID_DATE
inner join  cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID;

/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/
DROP TABLE IF EXISTS TMP_UPD_Dim_CustomerRiskCategoryId;		
CREATE TABLE TMP_UPD_Dim_CustomerRiskCategoryId
AS
SELECT c.CUSTOMER,src.CreditControlArea,src.dim_salesriskcategoryid
from ukmbp_cms a
inner join BUT000 b on a.PARTNER = b.PARTNER
inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
inner join dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1;


/*###################################End of 12 Apr tmp table creation ###################################*/
 
  
drop table if exists staging_update_701;
drop table if exists VBAK_VBAP_VBEP_701;

create table VBAK_VBAP_VBEP_701 as 
select min(x.VBEP_ETENR) _SalesSchedNo, x.VBAK_VBELN _SaleseDocNo, x.VBAP_POSNR _SalesItemNo
from VBAK_VBAP_VBEP x group by x.VBAK_VBELN, x.VBAP_POSNR;

create table staging_update_701 as 
Select vbep_wmeng ct_ScheduleQtySalesUnit,
	   vbep_bmeng ct_ConfirmedQty,
           vbep_cmeng ct_CorrectedQty,
	   ifnull(Decimal(vbap_netpr,18,4), 0) amt_UnitPrice,	--LK: Removed multiplication with stat exchg rate
           vbap_kpein ct_PriceUnit,
	   Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0) 
			WHEN vbep_wmeng = VBAP_KWMENG THEN VBAP_NETWR
			ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0) 
		   END ,18,4) amt_ScheduleTotal,	--LK: Removed multiplication with stat exchg rate
           Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
			THEN vbap_wavwr
			ELSE 0
		  END ),18,4) amt_StdCost,
	   Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
		    THEN vbap_zwert
		    ELSE 0
		END ),18,4) amt_TargetValue,
	   ifnull(Decimal((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0) = 0 THEN 1
						WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
								WHEN 0 THEN 1
								ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							END
						ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
					 END) 
				* vbep_bmeng,18,4), 0) amt_Tax,
           CASE WHEN VBEP_ETENR = y._SalesSchedNo
			  THEN vbap_zmeng
			  ELSE 0
		    END  ct_TargetQty,
           ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                 where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pToCurrency =  co.Currency and z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate,	--LK: Changed this to use co.currency as to currency
           ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                 where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ANSIDATE(ifnull(PRSDT,VBAP_ERDAT)) AND z.pToCurrency = pGlobalCurrency and z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate_GBL,	--Changed this to use pGlobalCurrency as pToCurrency
           vbap_uebto ct_OverDlvrTolerance,
           vbap_untto ct_UnderDlvrTolerance,
           ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
           ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_kmein
                        AND uom.RowIsCurrent = 1), 1) Dim_UnitOfMeasureId,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_meins
                        AND uom.RowIsCurrent = 1), 1) Dim_BaseUoMid,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_vrkme
                        AND uom.RowIsCurrent = 1), 1) Dim_SalesUoMid,
	   Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END ,18,4) amt_UnitPriceUoM,	/*LK: 8 Sep 2013: Removed multiplication by stat exch rate*/
	/* LK: 8 Sep 2013: Added 4 new columns */
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,
 ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.Currency),1) Dim_Currencyid,				  
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pGlobalCurrency),1) dim_Currencyid_GBL,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,
           ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z 
		   where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' 
		   and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1) amt_exchangerate_STAT,
	ifnull(VBAP_KWMENG,0) ct_CumOrderQty

from fact_salesorder so, 
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_701 y,variable_holder_701
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
	and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
	and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo AND VBEP_ETENR = y._SalesSchedNo
	AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

INSERT INTO staging_update_701(
ct_ScheduleQtySalesUnit,
ct_ConfirmedQty,
ct_CorrectedQty,
amt_UnitPrice,
ct_PriceUnit,
amt_ScheduleTotal,
amt_StdCost,
amt_TargetValue,
amt_Tax,
ct_TargetQty,
amt_ExchangeRate,
amt_ExchangeRate_GBL,
ct_OverDlvrTolerance,
ct_UnderDlvrTolerance,
Dim_DateidSalesOrderCreated,
Dim_DateidFirstDate,
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo,
Dim_UnitOfMeasureId,
Dim_BaseUoMid,
Dim_SalesUoMid,
amt_UnitPriceUoM,
dim_Currencyid_TRA,
dim_Currencyid,
dim_Currencyid_GBL,
dim_currencyid_STAT,
amt_exchangerate_STAT,
ct_CumOrderQty)
Select vbep_wmeng ct_ScheduleQtySalesUnit,
	   vbep_bmeng ct_ConfirmedQty,
           vbep_cmeng ct_CorrectedQty,
	   ifnull(Decimal(vbap_netpr,18,4), 0) amt_UnitPrice,
           vbap_kpein ct_PriceUnit,
           Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0) 
			WHEN vbep_wmeng = VBAP_KWMENG THEN VBAP_NETWR
			ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0) 
		   END ,18,4) amt_ScheduleTotal,
           0 amt_StdCost,
           0 amt_TargetValue,
	   ifnull(Decimal(((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0) = 0 THEN 1
						WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						THEN ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
						ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
					 END) 
			* (vbep_bmeng)),18,4), 0) amt_Tax,
           0 ct_TargetQty,

 ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                 where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pToCurrency =  co.Currency and z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate,	
           ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                 where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ANSIDATE(ifnull(PRSDT,VBAP_ERDAT)) AND z.pToCurrency = pGlobalCurrency and z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate_GBL,	
           
           vbap_uebto ct_OverDlvrTolerance,
           vbap_untto ct_UnderDlvrTolerance,
           ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
           ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT
                  AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_kmein
                        AND uom.RowIsCurrent = 1), 1) Dim_UnitOfMeasureId,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_meins
                        AND uom.RowIsCurrent = 1), 1) Dim_BaseUoMid,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_vrkme
                        AND uom.RowIsCurrent = 1), 1) Dim_SalesUoMid,
	   Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END ,18,4) amt_UnitPriceUoM,

    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,
				  
  ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.currency ),1) Dim_Currencyid,				  
				  
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pGlobalCurrency),1) dim_Currencyid_GBL,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,
           ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1) amt_exchangerate_STAT,
ifnull(VBAP_KWMENG,0) ct_CumOrderQty

from fact_salesorder so, 
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,variable_holder_701 
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
      AND NOT EXISTS ( SELECT 1 FROM VBAK_VBAP_VBEP_701 y
			WHERE VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo AND VBEP_ETENR = y._SalesSchedNo);	

drop table if exists VBAK_VBAP_VBEP_701;

update fact_salesorder so
from staging_update_701 sut
set so.ct_ScheduleQtySalesUnit=sut.ct_ScheduleQtySalesUnit,
	so.ct_ConfirmedQty=sut.ct_ConfirmedQty,
	so.ct_CorrectedQty=sut.ct_CorrectedQty
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set 
	so.amt_UnitPrice=sut.amt_UnitPrice,
	so.amt_UnitPriceUoM=sut.amt_UnitPriceUoM,
	so.ct_PriceUnit=sut.ct_PriceUnit
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set 	so.amt_ScheduleTotal=sut.amt_ScheduleTotal,
	so.amt_StdCost=sut.amt_StdCost
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set
	so.amt_TargetValue=sut.amt_TargetValue,
	so.amt_Tax=sut.amt_Tax
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set
	so.ct_TargetQty=sut.ct_TargetQty,
	so.amt_ExchangeRate=sut.amt_ExchangeRate
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set
	so.amt_ExchangeRate_GBL=sut.amt_ExchangeRate_GBL,
	so.ct_OverDlvrTolerance=sut.ct_OverDlvrTolerance
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set
	so.ct_UnderDlvrTolerance=sut.ct_UnderDlvrTolerance,
	so.Dim_DateidSalesOrderCreated=sut.Dim_DateidSalesOrderCreated
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set
	so.Dim_DateidFirstDate=sut.Dim_DateidFirstDate
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;

update fact_salesorder so
from staging_update_701 sut
set
	so.Dim_UnitOfMeasureId = sut.Dim_UnitOfMeasureId,
	so.Dim_BaseUoMid = sut.Dim_BaseUoMid,
	so.Dim_SalesUoMid = sut.Dim_SalesUoMid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;

/* LK: 7 Sep : Update 4 new columns. Also update local currency id. local rate and global rate have been already updated above */

update fact_salesorder so
from staging_update_701 sut
set
	so.dim_Currencyid_TRA = sut.dim_Currencyid_TRA
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;

update fact_salesorder so
from staging_update_701 sut
set
        so.dim_Currencyid_GBL = sut.dim_Currencyid_GBL
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;

update fact_salesorder so
from staging_update_701 sut
set
        so.dim_currencyid_STAT = sut.dim_currencyid_STAT
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;

update fact_salesorder so
from staging_update_701 sut
set
        so.amt_exchangerate_STAT = sut.amt_exchangerate_STAT
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set
        so.dim_Currencyid = sut.dim_Currencyid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set
        so.ct_CumOrderQty = sut.ct_CumOrderQty
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;



/* LK: 7 Sep : End of update */



drop table if exists staging_update_701;
/* ashu temporary code split to handle stringparse issue - Actian is working on to fix it-- */
/*create table staging_update_701 as
Select  ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_edatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDelivery,
         ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGoodsIssue,
         ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMtrlAvail,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoading,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,                  
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_tddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidTransport,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) Dim_Currencyid,
           ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid Dim_Plantid,
           co.Dim_Companyid Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort and sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          ifnull((SELECT Dim_ScheduleLineCategoryId
                  FROM Dim_ScheduleLineCategory slc
                  WHERE slc.ScheduleLineCategory = VBEP_ETTYP),1) Dim_ScheduleLineCategoryId,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
          1 Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((select Dim_SalesOrderHeaderStatusid
                    from Dim_SalesOrderHeaderStatus sohs
                    where sohs.SalesDocumentNumber = VBAK_VBELN),1) Dim_SalesOrderHeaderStatusid,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo
from fact_salesorder so, 
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	(select min(x.VBEP_ETENR) _SalesSchedNo, x.VBAK_VBELN _SaleseDocNo, x.VBAP_POSNR _SalesItemNo
	 from VBAK_VBAP_VBEP x group by x.VBAK_VBELN, x.VBAP_POSNR) y
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
*/

create table staging_update_701(
        dim_dateidscheddeliveryreq integer null,
        dim_dateidscheddelivery integer null,
        dim_dateidgoodsissue integer null,
        dim_dateidmtrlavail integer null,
        dim_dateidloading integer null,
        dim_dateidguaranteedate integer null,
        dim_dateidtransport integer null,
        dim_currencyid smallint null,
		dim_currencyid_TRA smallint null,
		dim_currencyid_GBL smallint null,
		dim_currencyid_STAT smallint null,
        dim_producthierarchyid integer null,
        dim_plantid smallint null,
        dim_companyid smallint null,
        dim_storagelocationid smallint null,
        dim_salesdivisionid integer null,
        dim_shipreceivepointid integer null,
        dim_documentcategoryid integer null,
        dim_salesdocumenttypeid integer null,
        dim_salesorgid integer null,
        dim_customerid integer null,
        dim_schedulelinecategoryid smallint null,
        dim_dateidvalidfrom integer null,
        dim_dateidvalidto integer null,
        dim_salesgroupid integer null,
        dim_costcenterid smallint null,
        dim_controllingareaid integer null,
        dim_billingblockid integer null,
        dim_transactiongroupid integer null,
        dim_salesorderrejectreasonid integer null,
        dim_partid integer null,
        dim_salesorderheaderstatusid integer null,
        dd_salesdocno varchar(10) null default 'Not Set' collate ucs_basic,
        dd_salesitemno integer null default 0,
        dd_scheduleno integer null default 0
);

drop table if exists VBAK_VBAP_VBEP_701;
create table VBAK_VBAP_VBEP_701 as
select min(x.VBEP_ETENR) _SalesSchedNo, x.VBAK_VBELN _SaleseDocNo, x.VBAP_POSNR _SalesItemNo
          from VBAK_VBAP_VBEP x group by x.VBAK_VBELN, x.VBAP_POSNR;



Insert into staging_update_701 (
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo
)
select
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo
from fact_salesorder so, 
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_701 y
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_701 sg
from fact_salesorder so, 
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_701 y
Set Dim_DateidSchedDeliveryReq=ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) ,
    Dim_DateidSchedDelivery = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_edatu AND dd.CompanyCode = pl.CompanyCode),1),
     Dim_DateidGoodsIssue = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_wadat AND dd.CompanyCode = pl.CompanyCode),1) ,
     Dim_DateidMtrlAvail =  ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_mbdat AND dd.CompanyCode = pl.CompanyCode),1),
    Dim_DateidLoading = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_lddat AND dd.CompanyCode = pl.CompanyCode),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo = so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo = so.dd_ScheduleNo;



update staging_update_701 sg
from fact_salesorder so, 
	VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set Dim_DateidGuaranteedate =  ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) ,
    Dim_DateidTransport = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_tddat AND dd.CompanyCode = pl.CompanyCode),1),
    Dim_Currencyid_TRA = ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK ),1) ,
    Dim_ProductHierarchyid = ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo = so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo = so.dd_ScheduleNo;



update staging_update_701 sg
from fact_salesorder so, 
	VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set Dim_Currencyid_STAT = ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae ),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo = so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo = so.dd_ScheduleNo;


update staging_update_701 sg
from fact_salesorder so, 
	VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set Dim_Currencyid = ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode =  co.currency ),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo = so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo = so.dd_ScheduleNo;



update staging_update_701 sg
from fact_salesorder so, 
	VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,variable_holder_701
Set Dim_Currencyid_GBL = ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode =  pGlobalCurrency ),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo = so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo = so.dd_ScheduleNo;


update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set       Dim_Plantid = pl.Dim_Plantid ,
          Dim_Companyid = co.Dim_Companyid ,
          Dim_StorageLocationid = ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort and sl.plant = vbap_werks),1),
          Dim_SalesDivisionid = ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo = so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo = so.dd_ScheduleNo;




update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set     Dim_ShipReceivePointid = ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) ,
        Dim_DocumentCategoryid = ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory = vbak_vbtyp),1),
        Dim_SalesDocumentTypeid = ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) ,
        Dim_SalesOrgid = ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo = so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo = so.dd_ScheduleNo;



update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
         VBAK_VBAP_VBEP_701 y 
Set       Dim_CustomerID=     ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) ,
          Dim_ScheduleLineCategoryId = ifnull((SELECT Dim_ScheduleLineCategoryId
                  FROM Dim_ScheduleLineCategory slc
                  WHERE slc.ScheduleLineCategory = VBEP_ETTYP),1) ,
          Dim_DateidValidFrom = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) ,
          Dim_DateidValidTo = ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo;


update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set      Dim_SalesGroupid =     ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) ,
          Dim_CostCenterid =1,
          Dim_ControllingAreaid = ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) ,
          Dim_BillingBlockid = ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo;



update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set       Dim_TransactionGroupid =     ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo;



update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set   Dim_SalesOrderRejectReasonid = ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo;



update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set  Dim_Partid = ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo;



update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y
Set  Dim_SalesOrderHeaderStatusid = ifnull((select Dim_SalesOrderHeaderStatusid
                    from Dim_SalesOrderHeaderStatus sohs
                    where sohs.SalesDocumentNumber = VBAK_VBELN),1)
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = so.dd_SalesDocNo and VBAP_POSNR = so.dd_SalesItemNo and VBEP_ETENR = so.dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
AND sg.dd_SalesDocNo= so.dd_SalesDocNo and sg.dd_SalesItemNo = so.dd_SalesItemNo and sg.dd_ScheduleNo= so.dd_ScheduleNo;



/*End of temporary code split */


update staging_update_701 sut
from  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
           VBAK_VBAP_VBEP_701  y
set Dim_CostCenterid = 
 ifnull((SELECT first 1 Dim_CostCenterid
                  FROM Dim_CostCenter_701 cc
                  WHERE cc.Code = vbak_kostl and cc.ControllingArea = vbak_kokrs and cc.RowIsCurrent = 1
                  ),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;



update fact_salesorder so
from staging_update_701 sut
set     so.Dim_DateidSchedDeliveryReq=sut.Dim_DateidSchedDeliveryReq,
	so.Dim_DateidSchedDelivery=sut.Dim_DateidSchedDelivery,
	so.Dim_DateidGoodsIssue=sut.Dim_DateidGoodsIssue,
	so.Dim_DateidMtrlAvail=sut.Dim_DateidMtrlAvail
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;



update fact_salesorder so
from staging_update_701 sut
set	so.Dim_DateidLoading=sut.Dim_DateidLoading,
	so.Dim_DateidGuaranteedate=sut.Dim_DateidGuaranteedate,
	so.Dim_DateidTransport=sut.Dim_DateidTransport,
	so.Dim_Currencyid=sut.Dim_Currencyid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set	so.Dim_ProductHierarchyid=sut.Dim_ProductHierarchyid,
	so.Dim_Plantid=sut.Dim_Plantid,
	so.Dim_Companyid=sut.Dim_Companyid,
	so.Dim_StorageLocationid=sut.Dim_StorageLocationid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set	so.Dim_SalesDivisionid=sut.Dim_SalesDivisionid,
	so.Dim_ShipReceivePointid=sut.Dim_ShipReceivePointid,
	so.Dim_DocumentCategoryid=sut.Dim_DocumentCategoryid,
	so.Dim_SalesDocumentTypeid=sut.Dim_SalesDocumentTypeid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set	so.Dim_SalesOrgid=sut.Dim_SalesOrgid,
	so.Dim_CustomerID=sut.Dim_CustomerID,
	so.Dim_ScheduleLineCategoryId=sut.Dim_ScheduleLineCategoryId,
	so.Dim_DateidValidFrom=sut.Dim_DateidValidFrom
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set	so.Dim_DateidValidTo=sut.Dim_DateidValidTo,
	so.Dim_SalesGroupid=sut.Dim_SalesGroupid,
	so.Dim_CostCenterid=sut.Dim_CostCenterid,
	so.Dim_ControllingAreaid=sut.Dim_ControllingAreaid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;


update fact_salesorder so
from staging_update_701 sut
set	so.Dim_BillingBlockid=sut.Dim_BillingBlockid,
	so.Dim_TransactionGroupid=sut.Dim_TransactionGroupid,
	so.Dim_SalesOrderRejectReasonid=sut.Dim_SalesOrderRejectReasonid,
	so.Dim_Partid=sut.Dim_Partid,
	so.Dim_SalesOrderHeaderStatusid=sut.Dim_SalesOrderHeaderStatusid
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;




drop table if exists staging_update_701;

create table staging_update_701 as
Select   ifnull((select sois.Dim_SalesOrderItemStatusid
                    from Dim_SalesOrderItemStatus sois
                    where sois.SalesDocumentNumber = VBAK_VBELN and sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
         ifnull((select cg1.Dim_CustomerGroup1id
                    from Dim_CustomerGroup1 cg1
                    where cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
         ifnull((select cg2.Dim_CustomerGroup2id
                    from Dim_CustomerGroup2 cg2
                    where cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
	soic.Dim_SalesOrderItemCategoryid Dim_salesorderitemcategoryid,
          ifnull(vbep_lfrel,'Not Set') dd_ItemRelForDelv,
	1 Dim_ProfitCenterId,
          ifnull((select dc.Dim_DistributionChannelid
                    from dim_DistributionChannel dc 
                    where   dc.DistributionChannelCode = VBAK_VTWEG
                        AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_kmein
                        AND uom.RowIsCurrent = 1), 1) Dim_UnitOfMeasureId,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_meins
                        AND uom.RowIsCurrent = 1), 1) Dim_BaseUoMid,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_vrkme
                        AND uom.RowIsCurrent = 1), 1) Dim_SalesUoMid,			
          ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
          ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
          ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
	ifnull((SELECT r.dim_routeid from dim_route r where
			       r.RouteCode = VBAP_ROUTE and r.RowIsCurrent = 1),1) Dim_RouteId,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701  y
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
	AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update staging_update_701 sut
from  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
           VBAK_VBAP_VBEP_701 y
set Dim_ProfitCenterId = ifnull((select first 1 pc.dim_profitcenterid
                    from dim_profitcenter_701 pc
                    where   pc.ProfitCenterCode = VBAP_PRCTR
                        AND pc.ControllingArea = VBAK_KOKRS
                        AND pc.ValidTo >= VBAK_ERDAT
                        AND pc.RowIsCurrent = 1 ),1) 
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
      AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;



update fact_salesorder so
from staging_update_701 sut
set  so.Dim_SalesOrderItemStatusid=sut.Dim_SalesOrderItemStatusid,
so.Dim_CustomerGroup1id=sut.Dim_CustomerGroup1id,
so.Dim_CustomerGroup2id=sut.Dim_CustomerGroup2id,
so.Dim_salesorderitemcategoryid=sut.Dim_salesorderitemcategoryid,
so.dd_ItemRelForDelv=sut.dd_ItemRelForDelv
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;

 
/* x */

update fact_salesorder so
from staging_update_701 sut
set  so.Dim_ProfitCenterId=sut.Dim_ProfitCenterId,
so.Dim_DistributionChannelId=sut.Dim_DistributionChannelId,
so.Dim_UnitOfMeasureId=sut.Dim_UnitOfMeasureId,
so.Dim_BaseUoMid = sut.Dim_BaseUoMid,
so.Dim_SalesUoMid = sut.Dim_SalesUoMid,
so.dd_BatchNo=sut.dd_BatchNo,
so.dd_CreatedBy=sut.dd_CreatedBy,
so.Dim_DateidNextDate=sut.Dim_DateidNextDate,
so.Dim_RouteId=sut.Dim_RouteId
where so.dd_SalesDocNo=sut.dd_SalesDocNo and so.dd_SalesItemNo=sut.dd_SalesItemNo and so.dd_ScheduleNo=sut.dd_ScheduleNo;



drop table if exists vvv_min_i20_1;
create table vvv_min_i20_1 as SELECT min(x.VBEP_ETENR) _SalesSchedNo, x.VBAK_VBELN _SaleseDocNo, x.VBAP_POSNR _SalesItemNo
FROM VBAK_VBAP_VBEP x GROUP BY x.VBAK_VBELN, x.VBAP_POSNR;

update fact_salesorder so
FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
          Dim_SalesOrderItemCategory soic,
          vvv_min_i20_1 y
SET  Dim_SalesRiskCategoryId = ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where
                       src.SalesRiskCategory = VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER and src.RowIsCurrent = 1),1)
  WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
      AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
      AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
        AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;


update fact_salesorder so
FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
          Dim_SalesOrderItemCategory soic,
          vvv_min_i20_1 y
SET dd_CreditLimit = ifnull((select first 1 c.CREDIT_LIMIT
                                    from TMP_UPD_dd_CreditLimit c 
                                    WHERE  c.CUSTOMER = VBAK_KUNNR),0)
  WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
      AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
      AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
        AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;



 update fact_salesorder so
FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
          Dim_SalesOrderItemCategory soic,
          vvv_min_i20_1 y
SET  Dim_CustomerRiskCategoryId = ifnull((select first 1 src.dim_salesriskcategoryid
                                    from TMP_UPD_Dim_CustomerRiskCategoryId src
                                    where src.CUSTOMER = VBAK_KUNNR and src.CreditControlArea = VBAK_KKBER ),1)
  WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
      AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
      AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
        AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;



update fact_salesorder so
FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
          Dim_SalesOrderItemCategory soic,
          vvv_min_i20_1 y
SET dd_CreditRep = ifnull((select b.upd_dd_CreditRep1
                          from TMP_UPD_dd_CreditRep1 b
                          where b.BUT050_PARTNER2 = VBAK_KUNNR ),'Not Set')									
					   
  WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
      AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
      AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
        AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;		  
		
		
drop table if exists vvv_min_i20_1;
	

/* 12 Apr changes - sync with mysql changes	by Shanthi part 2 ends	*/	

					   

drop table if exists staging_update_701;

drop table if exists Dim_CostCenter_first1_701;
create table Dim_CostCenter_first1_701 as select  code,ControllingArea,RowIsCurrent,max(Validto) validto , cast(null,integer) dim_costcenterid from Dim_CostCenter_701  group by code,ControllingArea,RowIsCurrent;

Update Dim_CostCenter_first1_701 b
set b.dim_costcenterid=( select first 1 a.dim_costcenterid from Dim_CostCenter_701 a where b.code=a.code and b.ControllingArea=a.ControllingArea and b.RowIsCurrent=a.RowIsCurrent and b.validto=a.validto );


Drop table if exists max_holder_701;
Declare global temporary table max_holder_701(maxid)
as
Select ifnull(max(fact_salesorderid),0)
from fact_salesorder
on commit preserve rows;

drop table if exists fact_salesorder_tmptbl;
create table fact_salesorder_tmptbl as select * from fact_salesorder where 1=2;

drop table if exists fact_salesorder_useinsub;

create table fact_salesorder_useinsub as Select dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
from fact_salesorder;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorder_useinsub;
\i /db/schema_migration/bin/wrapper_optimizedb.sh VBAK_VBAP_VBEP_701;
\i /db/schema_migration/bin/wrapper_optimizedb.sh VBAK_VBAP_VBEP;

drop table if exists vvv_di90;
create table vvv_di90 as select * from VBAK_VBAP_VBEP where exists (select 1 from dim_date mdt where mdt.DateValue = vbap_erdat and mdt.Dim_Dateid > 1);
\i /db/schema_migration/bin/wrapper_optimizedb.sh vvv_di90;


DROP TABLE IF EXISTS tmp_pop_fact_salesorder_tmptbl;
CREATE TABLE tmp_pop_fact_salesorder_tmptbl
AS
SELECT v.*, pl.PlantCode, pl.CompanyCode, pl.dim_plantid, soic.Dim_SalesOrderItemCategoryid, co.Dim_Companyid,
y._SalesSchedNo, co.Currency
FROM vvv_di90 v
      inner join Dim_Plant pl on pl.PlantCode = VBAP_WERKS
      inner join Dim_Company co on co.CompanyCode = pl.CompanyCode
	INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
      inner join VBAK_VBAP_VBEP_701 y
              on VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
  WHERE not exists (select 1 from fact_salesorder_useinsub f 
                    where f.dd_SalesDocNo = VBAK_VBELN and f.dd_SalesItemNo = VBAP_POSNR and f.dd_ScheduleNo = VBEP_ETENR);

drop table if exists vvv_di90;

  INSERT
    INTO fact_salesorder_tmptbl(
	    fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
	    Dim_RouteId,
	    Dim_SalesRiskCategoryId,
            dd_CreditRep,
            dd_CreditLimit,
            Dim_CustomerRiskCategoryId,
	    amt_UnitPriceUoM,
	    dim_Currencyid_TRA,
	    dim_Currencyid_GBL,
	    dim_currencyid_STAT,
	    amt_exchangerate_STAT)
  SELECT 
	  max_holder_701.maxid + row_number() over(),
          vbak_vbeln dd_SalesDocNo,
          vbap_posnr dd_SalesItemNo,
          vbep_etenr dd_ScheduleNo,
          vbep_wmeng ct_ScheduleQtySalesUnit,
          vbep_bmeng ct_ConfirmedQty,
          vbep_cmeng ct_CorrectedQty,
          ifnull(Decimal((vbap_netpr ),18,4),0) amt_UnitPrice,
          vbap_kpein ct_PriceUnit,
          Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0) 
			WHEN vbep_wmeng = VBAP_KWMENG THEN VBAP_NETWR
			ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0) 
		   END ,18,4) amt_ScheduleTotal,
          0 amt_StdCost,
          0 amt_TargetValue,
	  ifnull(Decimal(CASE ifnull(vbap_mwsbp,0) WHEN 0 THEN 0
				ELSE
				(vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0) = 0 THEN 1
						WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1.000000 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							WHEN 0 THEN 1
							ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1.000000 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							END
						ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1.000000 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
					 END) 
		END * (vbep_bmeng),18,4), 0) amt_Tax,
          0 ct_TargetQty,
          ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                 where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pToCurrency = pl.Currency and z.pFromExchangeRate = 0 ),1) amt_ExchangeRate,
	   ifnull( ( select z.exchangeRate from tmp_getExchangeRate1 z
      where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ANSIDATE(ifnull(PRSDT,VBAP_ERDAT)) AND z.pToCurrency = pGlobalCurrency and z.pFromExchangeRate = 0 ),1) amt_ExchangeRate_GBL ,
          vbap_uebto ct_OverDlvrTolerance,
          vbap_untto ct_UnderDlvrTolerance,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,                  
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_edatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDelivery,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGoodsIssue,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMtrlAvail,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoading,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_tddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidTransport,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,                    
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pl.currency ),1) Dim_Currencyid,
          ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid,
          pl.Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort and sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
          ifnull((SELECT Dim_CostCenterid
                  FROM Dim_CostCenter_first1_701 cc
                  WHERE cc.Code = vbak_kostl and cc.ControllingArea = vbak_kokrs and cc.RowIsCurrent = 1
                  ),1) Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((select Dim_SalesOrderHeaderStatusid
                    from Dim_SalesOrderHeaderStatus sohs
                    where sohs.SalesDocumentNumber = VBAK_VBELN),1) Dim_SalesOrderHeaderStatusid,
          ifnull((select Dim_SalesOrderItemStatusid
                    from Dim_SalesOrderItemStatus sois
                    where sois.SalesDocumentNumber = VBAK_VBELN and sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
          ifnull((select Dim_CustomerGroup1id
                    from Dim_CustomerGroup1 cg1
                    where cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
          ifnull((select Dim_CustomerGroup2id
                    from Dim_CustomerGroup2 cg2
                    where cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
	ifnull((pl.Dim_SalesOrderItemCategoryid),1)  Dim_SalesOrderItemCategoryid,
          ifnull((SELECT slc.Dim_ScheduleLineCategoryId
                  FROM Dim_ScheduleLineCategory slc
                  WHERE slc.ScheduleLineCategory = VBEP_ETTYP),1) Dim_ScheduleLineCategoryId,
          ifnull(vbep_lfrel,'Not Set') dd_ItemRelForDelv,
	1 Dim_ProfitCenterId,
          ifnull((select dc.Dim_DistributionChannelid
                    from dim_DistributionChannel dc
                    where   dc.DistributionChannelCode = VBAK_VTWEG
                        AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
        ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
          ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
          ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
	  ifnull((SELECT r.dim_routeid from dim_route r where
                       r.RouteCode = VBAP_ROUTE and r.RowIsCurrent = 1),1),
            ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where
                       src.SalesRiskCategory = VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER and src.RowIsCurrent = 1),1),
    ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2 = VBAK_KUNNR ),'Not Set') dd_CreditRep1,    
   	 ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER = VBAK_KUNNR ),0) dd_CreditLimit,    
	1 Dim_CustomerRiskCategoryId,
	   Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END ,18,4) amt_UnitPriceUoM,

    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pGlobalCurrency),1) dim_Currencyid_GBL,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,

           ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1) amt_exchangerate_STAT

FROM max_holder_701,tmp_pop_fact_salesorder_tmptbl pl,variable_holder_701;

call vectorwise (combine 'tmp_pop_fact_salesorder_tmptbl');

UPDATE fact_salesorder_tmptbl f
 FROM tmp_pop_fact_salesorder_tmptbl pl
set amt_StdCost = ifnull(Decimal((vbap_wavwr),18,4),0),
    amt_TargetValue = ifnull(Decimal((vbap_zwert),18,4),0),
    ct_TargetQty = ifnull(vbap_zmeng,0)
WHERE pl.VBEP_ETENR = f.dd_ScheduleNo
  AND pl.VBAP_POSNR = f.dd_SalesItemNo
  AND pl.VBAK_VBELN = f.dd_SalesDocNo
  AND f.dd_ScheduleNo = pl._SalesSchedNo;

UPDATE fact_salesorder_tmptbl f
FROM tmp_pop_fact_salesorder_tmptbl vbk
SET f.ct_CumOrderQty = ifnull(vbk.VBAP_KWMENG, 0)
Where vbk.VBAK_VBELN = f.dd_SalesDocNo
AND vbk.VBAP_POSNR = f.dd_SalesItemNo
AND vbk.VBEP_ETENR = f.dd_ScheduleNo
AND f.ct_CumOrderQty <> ifnull(vbk.VBAP_KWMENG, 0);


call vectorwise (combine 'fact_salesorder_tmptbl');

/* Update this column separately to avoid the previous insert from failing */

UPDATE fact_salesorder_tmptbl f
 FROM tmp_pop_fact_salesorder_tmptbl ft
set Dim_CustomerRiskCategoryId = 
		      ifnull((select c.dim_salesriskcategoryid 
				 from TMP_UPD_Dim_CustomerRiskCategoryId c
				 where c.CUSTOMER = VBAK_KUNNR and c.CreditControlArea = VBAK_KKBER),1)
WHERE  ft.VBEP_ETENR = f.dd_ScheduleNo
  AND ft.VBAP_POSNR = f.dd_SalesItemNo
  AND ft.VBAK_VBELN = f.dd_SalesDocNo;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND VBAP_KMEIN IS NOT NULL
    AND uom.UOM = vbap_kmein
    AND uom.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  Dim_UnitOfMeasureId = 1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND VBAP_KMEIN IS NULL;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_BaseUoMid =  uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_meins IS NOT NULL
    AND uom.UOM = vbap_meins
    AND uom.RowIsCurrent = 1;

    Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  Dim_BaseUoMid =  1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_meins IS NULL;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  Dim_SalesUoMid = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_vrkme IS NOT NULL
    AND uom.UOM = vbap_vrkme
    AND uom.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  Dim_SalesUoMid = 1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_vrkme IS NULL;

call vectorwise (combine 'fact_salesorder_tmptbl');
		
drop table if exists dim_profitcenter_upd_701;

create table dim_profitcenter_upd_701 as
select max_holder_701.maxid+row_number() over() iid,VBAP_PRCTR,VBAK_KOKRS,VBAK_ERDAT,1 dim_profitcenterid
From max_holder_701,VBAK_VBAP_VBEP
        INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
        INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
	INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
        INNER JOIN VBAK_VBAP_VBEP_701 y
                ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
WHERE  NOT EXISTS (SELECT 1 FROM fact_salesorder_useinsub f
                      WHERE f.dd_SalesDocNo = VBAK_VBELN AND f.dd_SalesItemNo = VBAP_POSNR AND f.dd_ScheduleNo = VBEP_ETENR)
          AND EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1);

drop table if exists fact_salesorder_useinsub;

update dim_profitcenter_upd_701
set dim_profitcenterid = ifnull((SELECT first 1 pc.dim_profitcenterid
                      FROM dim_profitcenter_701 pc
                      WHERE   pc.ProfitCenterCode = VBAP_PRCTR
                          AND pc.ControllingArea = VBAK_KOKRS
                          AND pc.ValidTo >= VBAK_ERDAT
                          AND pc.RowIsCurrent = 1 ),1);

Update fact_salesorder_tmptbl
from dim_profitcenter_upd_701 ud
set Dim_ProfitCenterId =ud.Dim_ProfitCenterId
where fact_salesorder_tmptbl.fact_salesorderid = ud.iid;

drop table if exists dim_profitcenter_upd_701;

call vectorwise (combine 'fact_salesorder+fact_salesorder_tmptbl');

drop table if exists Dim_CostCenterid_2nd_701;
drop table if exists fact_salesorder_tmptbl;

create table  Dim_CostCenterid_2nd_701 as 
SELECT Dim_CostCenterid,cc.Code,cc.ControllingArea,cc.RowIsCurrent,row_number() over (partition by cc.Code,cc.ControllingArea,cc.RowIsCurrent order by cc.ValidTo DESC) tt FROM Dim_CostCenter cc  ;

create table fact_salesorder_tmptbl as 
Select vbap_kwmeng ct_ScheduleQtySalesUnit,
	 ifnull(VBAP_KWMENG,0) ct_CumOrderQty,
          vbap_kbmeng ct_ConfirmedQty,
          0.0000 ct_CorrectedQty,
          ifnull(Decimal((vbap_netpr),18,4), 0) amt_UnitPrice,
          vbap_kpein ct_PriceUnit,
          Decimal((vbap_netwr ),18,4) amt_ScheduleTotal,
          ifnull(decimal((vbap_wavwr ),18,4),0) amt_StdCost,
          ifnull(decimal((vbap_zwert ),18,4),0) amt_TargetValue,
          ifnull((vbap_mwsbp ),0) amt_Tax,	/* Shanthi changes 12 Apr */
          vbap_zmeng ct_TargetQty,
          ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                 where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pToCurrency = co.Currency and z.pFromExchangeRate = 0 ),1) amt_ExchangeRate,
          ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                 where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ANSIDATE(ifnull(PRSDT,VBAP_ERDAT)) AND z.pToCurrency = pGlobalCurrency and z.pFromExchangeRate = 0 ),1) amt_ExchangeRate_GBL,
          vbap_uebto ct_OverDlvrTolerance,
          vbap_untto ct_UnderDlvrTolerance,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          1 Dim_DateidSchedDelivery,
          1 Dim_DateidGoodsIssue,
          1 Dim_DateidMtrlAvail,
          1 Dim_DateidLoading,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
          1 Dim_DateidTransport,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.currency ),1) Dim_Currencyid,
          ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid Dim_Plantid,
          co.Dim_Companyid  Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort AND sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          1 Dim_ScheduleLineCategoryId,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid ,
           ifnull((SELECT Dim_CostCenterid
                  FROM Dim_CostCenterid_2nd_701 cc
                  WHERE cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs AND cc.RowIsCurrent = 1 and tt=1),1) Dim_CostCenterid, 
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((SELECT Dim_SalesOrderHeaderStatusid
                    FROM Dim_SalesOrderHeaderStatus sohs
                    WHERE sohs.SalesDocumentNumber = VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
          ifnull((SELECT sois.Dim_SalesOrderItemStatusid
                    FROM Dim_SalesOrderItemStatus sois
                    WHERE sois.SalesDocumentNumber = VBAP_VBELN AND sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
          ifnull((SELECT cg1.Dim_CustomerGroup1id
                    FROM Dim_CustomerGroup1 cg1
                    WHERE cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
          ifnull((SELECT cg2.Dim_CustomerGroup2id
                    FROM Dim_CustomerGroup2 cg2
                    WHERE cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
          ifnull((soic.Dim_SalesOrderItemCategoryid),1) Dim_salesorderitemcategoryid,
         'Not Set' dd_ItemRelForDelv,
          1 Dim_ProfitCenterId,
          ifnull((SELECT dc.Dim_DistributionChannelid
                    FROM dim_DistributionChannel dc
                    WHERE   dc.DistributionChannelCode = VBAK_VTWEG
                        AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_kmein
                        AND uom.RowIsCurrent = 1), 1) Dim_UnitOfMeasureId,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_meins
                        AND uom.RowIsCurrent = 1), 1) Dim_BaseUoMid,
	ifnull((select uom.Dim_UnitOfMeasureId
                    from Dim_UnitOfMeasure uom
                    where   uom.UOM = vbap_vrkme
                        AND uom.RowIsCurrent = 1), 1) Dim_SalesUoMid,
          ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
          ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
          ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
          ifnull((SELECT r.dim_routeid from dim_route r where
                       r.RouteCode = VBAP_ROUTE and r.RowIsCurrent = 1),1) Dim_RouteId,
	dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,
	ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where
			       src.SalesRiskCategory = VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER and src.RowIsCurrent = 1),1) Dim_SalesRiskCategoryId,
	    ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2 = VBAK_KUNNR ),'Not Set') dd_CreditRep,    
	    ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER = VBAK_KUNNR ),0) dd_CreditLimit,    
         ifnull((select c.dim_salesriskcategoryid
                                 from TMP_UPD_Dim_CustomerRiskCategoryId c
                                 where c.CUSTOMER = VBAK_KUNNR and c.CreditControlArea = VBAK_KKBER),1) Dim_CustomerRiskCategoryId,
	   Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END ,18,4) amt_UnitPriceUoM,

    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pGlobalCurrency),1) dim_Currencyid_GBL,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,

           ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1) amt_exchangerate_STAT

From fact_salesorder so,VBAK_VBAP,
          dim_SalesOrderItemCategory soic,
          Dim_Plant pl,
          Dim_Company co,variable_holder_701
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
      AND VBAP_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND
     soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 AND dd_ScheduleNo = 0;	/* Shanthi changes - sync on 12 Apr */

drop table if exists dim_profitcenteri_701;
create table dim_profitcenteri_701 as Select first 0 * from dim_profitcenter  ORDER BY ValidTo ASC;

Update fact_salesorder_tmptbl so 
From VBAK_VBAP,
          Dim_Plant pl,
          Dim_Company co
set so.Dim_ProfitCenterId = ifnull((SELECT first 1 pc.dim_profitcenterid
                    FROM dim_profitcenteri_701 pc
                    WHERE   pc.ProfitCenterCode = VBAP_PRCTR
                        AND pc.ControllingArea = VBAK_KOKRS
                        AND pc.ValidTo >= VBAK_ERDAT
                        AND pc.RowIsCurrent = 1 ),1)
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
      AND VBAP_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND dd_ScheduleNo = 0;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set  so.ct_ScheduleQtySalesUnit   = sot.ct_ScheduleQtySalesUnit          ,
so.ct_ConfirmedQty	 = sot.ct_ConfirmedQty,
so.ct_CorrectedQty	 = sot.ct_CorrectedQty
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.amt_UnitPrice	 = sot.amt_UnitPrice,
so.amt_UnitPriceUoM	 = sot.amt_UnitPriceUoM,
so.ct_PriceUnit	 = sot.ct_PriceUnit,
so.amt_ScheduleTotal	 = sot.amt_ScheduleTotal
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.amt_StdCost	 = sot.amt_StdCost,
so.amt_TargetValue	 = sot.amt_TargetValue,
so.amt_Tax	 = sot.amt_Tax
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.ct_TargetQty	 = sot.ct_TargetQty,
so.amt_ExchangeRate	 = sot.amt_ExchangeRate,
so.amt_ExchangeRate_GBL	 = sot.amt_ExchangeRate_GBL
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.ct_OverDlvrTolerance	 = sot.ct_OverDlvrTolerance,
so.ct_UnderDlvrTolerance	 = sot.ct_UnderDlvrTolerance,
so.Dim_DateidSalesOrderCreated	 = sot.Dim_DateidSalesOrderCreated
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_DateidFirstDate 	 = sot.Dim_DateidFirstDate ,
so.Dim_DateidSchedDeliveryReq	 = sot.Dim_DateidSchedDeliveryReq,
so.Dim_DateidSchedDelivery 	 = sot.Dim_DateidSchedDelivery 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_DateidGoodsIssue 	 = sot.Dim_DateidGoodsIssue ,
so.Dim_DateidMtrlAvail 	 = sot.Dim_DateidMtrlAvail ,
so.Dim_DateidLoading 	 = sot.Dim_DateidLoading 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_DateidGuaranteedate 	 = sot.Dim_DateidGuaranteedate ,
so.Dim_DateidTransport 	 = sot.Dim_DateidTransport ,
so.Dim_Currencyid 	 = sot.Dim_Currencyid 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_ProductHierarchyid	 = sot.Dim_ProductHierarchyid,
so.Dim_Plantid	 = sot.Dim_Plantid,
so.Dim_Companyid 	 = sot.Dim_Companyid 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_StorageLocationid	 = sot.Dim_StorageLocationid,
so.Dim_SalesDivisionid	 = sot.Dim_SalesDivisionid,
so.Dim_ShipReceivePointid	 = sot.Dim_ShipReceivePointid
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_DocumentCategoryid	 = sot.Dim_DocumentCategoryid,
so.Dim_SalesDocumentTypeid 	 = sot.Dim_SalesDocumentTypeid ,
so.Dim_SalesOrgid 	 = sot.Dim_SalesOrgid 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_CustomerID 	 = sot.Dim_CustomerID ,
so.Dim_ScheduleLineCategoryId 	 = sot.Dim_ScheduleLineCategoryId ,
so.Dim_DateidValidFrom 	 = sot.Dim_DateidValidFrom 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_DateidValidTo	 = sot.Dim_DateidValidTo,
so.Dim_SalesGroupid 	 = sot.Dim_SalesGroupid ,
so.Dim_CostCenterid 	 = sot.Dim_CostCenterid 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_ControllingAreaid 	 = sot.Dim_ControllingAreaid ,
so.Dim_BillingBlockid 	 = sot.Dim_BillingBlockid ,
so.Dim_TransactionGroupid	 = sot.Dim_TransactionGroupid
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_SalesOrderRejectReasonid 	 = sot.Dim_SalesOrderRejectReasonid ,
so.Dim_Partid 	 = sot.Dim_Partid ,
so.Dim_SalesOrderHeaderStatusid 	 = sot.Dim_SalesOrderHeaderStatusid 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_SalesOrderItemStatusid 	 = sot.Dim_SalesOrderItemStatusid ,
so.Dim_CustomerGroup1id 	 = sot.Dim_CustomerGroup1id ,
so.Dim_CustomerGroup2id 	 = sot.Dim_CustomerGroup2id 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_salesorderitemcategoryid 	 = sot.Dim_salesorderitemcategoryid ,
so.dd_ItemRelForDelv 	 = sot.dd_ItemRelForDelv ,
so.Dim_ProfitCenterId 	 = sot.Dim_ProfitCenterId 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_DistributionChannelId 	 = sot.Dim_DistributionChannelId ,
so.Dim_UnitOfMeasureId = sot.Dim_UnitOfMeasureId ,
so.Dim_BaseUoMid = sot.Dim_BaseUoMid,
so.Dim_SalesUoMid = sot.Dim_SalesUoMid,
so.dd_BatchNo = sot.dd_BatchNo 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.dd_CreatedBy 	 = sot.dd_CreatedBy ,
so.Dim_DateidNextDate 	 = sot.Dim_DateidNextDate ,
so.Dim_RouteId 	 = sot.Dim_RouteId 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

/* Shanthi changes - 12 Apr sync part X starts	*/

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.Dim_SalesRiskCategoryId 	 = sot.Dim_SalesRiskCategoryId ,
so.dd_CreditRep 	 = sot.dd_CreditRep ,
so.dd_CreditLimit 	 = sot.dd_CreditLimit ,
so.Dim_CustomerRiskCategoryId = sot.Dim_CustomerRiskCategoryId
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.ct_CumOrderQty  = sot.ct_CumOrderQty
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo
AND so.ct_CumOrderQty <> sot.ct_CumOrderQty;

/* Shanthi changes - 12 Apr sync part X ends	*/

/* LK: 8 Sep : 4 new columns */

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.dim_Currencyid_TRA   = sot.dim_Currencyid_TRA 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.dim_Currencyid_GBL   = sot.dim_Currencyid_GBL 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.dim_currencyid_STAT   = sot.dim_currencyid_STAT 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

Update fact_salesorder so
From fact_salesorder_tmptbl sot
Set so.amt_exchangerate_STAT   = sot.amt_exchangerate_STAT 
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo;

/* LK: 8 Sep : End of 4 new columns */


Drop table if exists max_holder_701;
Declare global temporary table max_holder_701(maxid)
as
Select ifnull(max(fact_salesorderid),0)
from fact_salesorder
on commit preserve rows;

drop table if exists fact_salesorder_tmptbl;
create table fact_salesorder_tmptbl as select * from fact_salesorder where 1=2;
drop table if exists fact_salesorder_useinsub;

create table fact_salesorder_useinsub as Select dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
from fact_salesorder;


 INSERT
    INTO fact_salesorder_tmptbl(
 fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
            Dim_routeid,
	    Dim_SalesRiskCategoryId,
            dd_CreditRep,
            dd_CreditLimit,
            Dim_CustomerRiskCategoryId,
	    amt_UnitPriceUoM,
		dim_Currencyid_TRA,
		dim_Currencyid_GBL,
		dim_currencyid_STAT,
		amt_exchangerate_STAT,
		ct_CumOrderQty)
  SELECT
		max_holder_701.maxid + row_number() over(),
          vbap_vbeln dd_SalesDocNo,
          vbap_posnr dd_SalesItemNo,
          0 dd_ScheduleNo,
          vbap_kwmeng ct_ScheduleQtySalesUnit,
          vbap_kbmeng ct_ConfirmedQty,
          0.0000 ct_CorrectedQty,
          ifnull(Decimal((vbap_netpr),18,4),0) amt_UnitPrice,
          vbap_kpein ct_PriceUnit,
          decimal((vbap_netwr ),18,4) amt_ScheduleTotal,
          ifnull(decimal((vbap_wavwr ),18,4),0) amt_StdCost,
          ifnull(decimal((vbap_zwert ),18,4),0) amt_TargetValue,
           ifnull((vbap_mwsbp ),0.0000) amt_Tax,
          vbap_zmeng ct_TargetQty,
          ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                 where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pToCurrency = co.Currency and z.pFromExchangeRate = 0 ),1) amt_ExchangeRate,
	   ifnull( ( select z.exchangeRate from tmp_getExchangeRate1 z
      where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ANSIDATE(ifnull(PRSDT,VBAP_ERDAT)) AND z.pToCurrency = pGlobalCurrency and z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate_GBL ,
          vbap_uebto ct_OverDlvrTolerance,
          vbap_untto ct_UnderDlvrTolerance,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
          1 Dim_DateidSchedDelivery,
          1 Dim_DateidGoodsIssue,
          1 Dim_DateidMtrlAvail,
          1 Dim_DateidLoading,
          1 Dim_DateidTransport,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.currency ),1) Dim_Currencyid,
          ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid,
          co.Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort AND sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
	  1 Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((SELECT Dim_SalesOrderHeaderStatusid
                    FROM Dim_SalesOrderHeaderStatus sohs
                    WHERE sohs.SalesDocumentNumber = VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
          ifnull((SELECT Dim_SalesOrderItemStatusid
                    FROM Dim_SalesOrderItemStatus sois
                    WHERE sois.SalesDocumentNumber = VBAP_VBELN AND sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
          ifnull((SELECT Dim_CustomerGroup1id
                    FROM Dim_CustomerGroup1 cg1
                    WHERE cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
          ifnull((SELECT Dim_CustomerGroup2id
                    FROM Dim_CustomerGroup2 cg2
                    WHERE cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
          ifnull((soic.Dim_SalesOrderItemCategoryid),1) Dim_SalesOrderItemCategoryid,
          1 Dim_ScheduleLineCategoryId,
          'Not Set' dd_ItemRelForDelv,
	1 Dim_ProfitCenterId,
         ifnull((SELECT dc.Dim_DistributionChannelid
                    FROM dim_DistributionChannel dc
                    WHERE   dc.DistributionChannelCode = VBAK_VTWEG
                        AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
          ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
          ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
          ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
           ifnull((SELECT r.dim_routeid from dim_route r where
                       r.RouteCode = VBAP_ROUTE and r.RowIsCurrent = 1),1),
					    ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where
                       src.SalesRiskCategory = VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER and src.RowIsCurrent = 1),1),
      ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2 = VBAK_KUNNR ),'Not Set') dd_CreditRep,
      ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER = VBAK_KUNNR ),0) dd_CreditLimit,   
    ifnull((select c.dim_salesriskcategoryid
            from TMP_UPD_Dim_CustomerRiskCategoryId c
            where c.CUSTOMER = VBAK_KUNNR and c.CreditControlArea = VBAK_KKBER),1) Dim_CustomerRiskCategoryId,
	   Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END ,18,4) amt_UnitPriceUoM,

    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pGlobalCurrency),1) dim_Currencyid_GBL,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,

           ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = VBAP_WAERK and z.fact_script_name = 'bi_populate_salesorder_fact' and z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1) amt_exchangerate_STAT,
	ifnull(VBAP_KWMENG, 0) ct_CumOrderQty

  FROM max_holder_701,VBAK_VBAP
      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
      INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
      INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1, variable_holder_701
  WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder_useinsub f
                    WHERE f.dd_SalesDocNo = VBAP_VBELN AND f.dd_SalesItemNo = VBAP_POSNR AND f.dd_ScheduleNo = 0)
        AND EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
		 AND NOT EXISTS ( SELECT 1 FROM vbak_vbap_vbep v WHERE vbak_vbap.VBAP_VBELN = v.VBAK_VBELN
                                                         AND vbak_vbap.VBAP_POSNR = v.VBAP_POSNR);

drop table if exists Dim_CostCenter_701;
create table Dim_CostCenter_701 as Select first 0 * from Dim_CostCenter ORDER BY ValidTo DESC;
drop table if exists dim_profitcenter_701;
create table dim_profitcenter_701 as Select first 0 * from dim_profitcenter  ORDER BY ValidTo ASC;


Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP,Dim_CostCenter cc
Set fo.Dim_CostCenterid = cc.Dim_CostCenterid
WHERE fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
AND cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs AND cc.RowIsCurrent = 1;


Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, dim_profitcenter pc
Set fo.Dim_ProfitCenterId = pc.dim_profitcenterid
  WHERE  fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
         AND pc.ProfitCenterCode = VBAP_PRCTR
	 AND pc.ControllingArea = VBAK_KOKRS
	 AND pc.ValidTo >= VBAK_ERDAT
         AND pc.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  fo.Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND VBAP_KMEIN IS NOT NULL
    AND uom.UOM = vbap_kmein
    AND uom.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  fo.Dim_UnitOfMeasureId = 1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND VBAP_KMEIN IS NULL;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  fo.Dim_BaseUoMid =  uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_meins IS NOT NULL
    AND uom.UOM = vbap_meins
    AND uom.RowIsCurrent = 1;

    Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  fo.Dim_BaseUoMid =  1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_meins IS NULL;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET  fo.Dim_SalesUoMid = uom.Dim_UnitOfMeasureId
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_vrkme IS NOT NULL
    AND uom.UOM = vbap_vrkme
    AND uom.RowIsCurrent = 1;

Update fact_salesorder_tmptbl fo
  FROM VBAK_VBAP
   SET  fo.Dim_SalesUoMid = 1
where   fo.dd_SalesDocNo = VBAP_VBELN
    AND fo.dd_SalesItemNo = VBAP_POSNR
    AND fo.dd_ScheduleNo = 0
    AND vbap_vrkme IS NULL;

call vectorwise (combine 'fact_salesorder+fact_salesorder_tmptbl');

drop table if exists fact_salesorder_useinsub;

  UPDATE fact_salesorder fso
From Dim_SalesMisc smisc, VBAK_VBAP vkp
     SET fso.Dim_SalesMiscId = smisc.Dim_SalesMiscId
  WHERE     fso.dd_SalesDocNo = vkp.VBAP_VBELN
         AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
         AND fso.dd_ScheduleNo = 0
         AND smisc.DeliveryDateQuantityFixed = ifnull(VBAP_FIXMG,'Not Set')
         AND smisc.FixedQuantity = ifnull(VBAP_FMENG, 'Not Set')
         AND smisc.OverDeliveryAllowed = ifnull(VBAP_UEBTK, 'Not Set')
         AND smisc.CashDiscountIndicator = ifnull(VBAP_SKTOF, 'Not Set')
         AND smisc.ReturnsItem = ifnull(VBAP_SHKZG, 'Not Set')
         AND smisc.PricingOk = ifnull(VBAP_PRSOK, 'Not Set')
         AND smisc.CustomerNotPostedGoodsReceipt = ifnull(VBAP_NACHL, 'Not Set')
         AND smisc.ItemRelevantForDelivery = ifnull(VBAP_LFREL, 'Not Set')
         AND smisc.ScheduleConfirmStatus = 'Not Set'
         AND smisc.InvoiceReceiptIndicator = 'Not Set';



  UPDATE fact_salesorder fso
from Dim_SalesMisc smisc, VBAK_VBAP_VBEP vkp
     SET fso.Dim_SalesMiscId = smisc.Dim_SalesMiscId
  WHERE     fso.dd_SalesDocNo = vkp.VBAK_VBELN
         AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
         AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
         AND smisc.DeliveryDateQuantityFixed = ifnull(VBAP_FIXMG,'Not Set')
         AND smisc.FixedQuantity = ifnull(VBAP_FMENG, 'Not Set')
         AND smisc.OverDeliveryAllowed = ifnull(VBAP_UEBTK, 'Not Set')
         AND smisc.CashDiscountIndicator = ifnull(VBAP_SKTOF, 'Not Set')
         AND smisc.ReturnsItem = ifnull(VBAP_SHKZG, 'Not Set')
         AND smisc.PricingOk = ifnull(VBAP_PRSOK, 'Not Set')
         AND smisc.CustomerNotPostedGoodsReceipt = ifnull(VBAP_NACHL, 'Not Set')
         AND smisc.ItemRelevantForDelivery = ifnull(VBAP_LFREL, 'Not Set')
         AND smisc.ScheduleConfirmStatus = ifnull(VBEP_WEPOS, 'Not Set')
         AND smisc.InvoiceReceiptIndicator = ifnull(VBEP_REPOS, 'Not Set')
	AND fso.dd_ScheduleNo <> 0;
         

UPDATE fact_salesorder fso
From vbak_vbap_vbep vkp, KNVV k
   SET fso.Dim_CustomerGroupId =
          ifnull(
             (SELECT cg.Dim_CustomerGroupId
                FROM dim_CustomerGroup cg
               WHERE cg.CustomerGroup = k.knvv_kdgrp AND cg.RowIsCurrent = 1),
             1),
       fso.Dim_SalesOfficeId =
          ifnull(
             (SELECT so.Dim_SalesOfficeId
                FROM dim_SalesOffice so
               WHERE so.SalesOfficeCode = k.knvv_vkbur
                     AND so.RowIsCurrent = 1),
             1),
    fso.Dim_CustomerPaymentTermsId = 
		ifnull(
			(SELECT cpt.dim_Customerpaymenttermsid
              FROM dim_customerpaymentterms cpt
              WHERE cpt.PaymentTermCode = k.knvv_zterm
              AND cpt.RowIsCurrent = 1), 
			 1)			 
Where fso.dd_SalesDocNo = vkp.VBAK_VBELN
      AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
      AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
AND k.knvv_VTWEG = vkp.VBAK_VTWEG
          AND k.knvv_SPART = vkp.VBAK_SPART
          AND k.knvv_vkorg = vkp.VBAK_VKORG
          AND k.knvv_KUNNR = vkp.VBAK_KUNNR
AND fso.dd_ScheduleNo <> 0 ;



UPDATE fact_salesorder fso
FROM           vbak_vbap vkp,
          KNVV k
   SET fso.Dim_CustomerGroupId =
          ifnull(
             (SELECT cg.Dim_CustomerGroupId
                FROM dim_CustomerGroup cg
               WHERE cg.CustomerGroup = k.knvv_kdgrp AND cg.RowIsCurrent = 1),
             1),
       fso.Dim_SalesOfficeId =
          ifnull(
             (SELECT so.Dim_SalesOfficeId
                FROM dim_SalesOffice so
               WHERE so.SalesOfficeCode = k.knvv_vkbur
                     AND so.RowIsCurrent = 1),
             1),
      fso.Dim_CustomerPaymentTermsId = 
		ifnull((SELECT cpt.dim_Customerpaymenttermsid
              FROM dim_customerpaymentterms cpt
              WHERE cpt.PaymentTermCode = k.knvv_zterm
              AND cpt.RowIsCurrent = 1),
			  1)			 
Where  fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
AND  k.knvv_VTWEG = vkp.VBAK_VTWEG
          AND k.knvv_SPART = vkp.VBAK_SPART
          AND k.knvv_vkorg = vkp.VBAK_VKORG
          AND k.knvv_KUNNR = vkp.VBAK_KUNNR ;


 
UPDATE       fact_salesorder so
FROM     vbuk v, dim_overallstatusforcreditcheck oscc
SET so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckID
WHERE so.dd_SalesDocNo = v.VBUK_VBELN 
AND oscc.overallstatusforcreditcheck = ifnull(v.VBUK_CMGST, 'Not Set')
AND oscc.RowIsCurrent = 1;


/*tmp table to handle Dim_BillToPartyPartnerFunctionId updates */

drop table if exists tmp1_sof_Dim_CustomerPartnerFunctions;
create table tmp1_sof_Dim_CustomerPartnerFunctions
as
select cpf.CustomerNumber1,
cpf.SalesOrgCode, 
cpf.DivisionCode, 
cpf.DistributionChannelCode, 
cpf.PartnerFunction, 
cpf.RowIsCurrent,max(PartnerCounter) max_PartnerCounter
FROM Dim_CustomerPartnerFunctions cpf
group by cpf.CustomerNumber1,cpf.SalesOrgCode, cpf.DivisionCode, cpf.DistributionChannelCode, cpf.PartnerFunction, cpf.RowIsCurrent;

drop table if exists tmp_sof_Dim_CustomerPartnerFunctions;
create table tmp_sof_Dim_CustomerPartnerFunctions
as
select cpf.CustomerNumber1,
cpf.SalesOrgCode, 
cpf.DivisionCode, 
cpf.DistributionChannelCode, 
cpf.PartnerFunction, 
cpf.RowIsCurrent,Dim_CustomerPartnerFunctionsID
from Dim_CustomerPartnerFunctions cpf, tmp1_sof_Dim_CustomerPartnerFunctions t
WHERE cpf.CustomerNumber1  = t.CustomerNumber1  
and cpf.SalesOrgCode = t.SalesOrgCode 
and cpf.DivisionCode = t.DivisionCode 
and cpf.DistributionChannelCode = t.DistributionChannelCode 
and cpf.PartnerFunction = t.PartnerFunction 
and cpf.RowIsCurrent = t.RowIsCurrent
AND cpf.PartnerCounter = t.max_PartnerCounter;

drop table if exists  tmp1_sof_Dim_CustomerPartnerFunctions;

/*tmp table to handle Dim_BillToPartyPartnerFunctionId updates */



 UPDATE       fact_salesorder so
 FROM     vbak_vbap_vbep shi, vbpa sdp,variable_holder_701 vb
SET so.Dim_BillToPartyPartnerFunctionId =
          ifnull(
             (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID
                FROM tmp_sof_Dim_CustomerPartnerFunctions cpf
               WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                     AND cpf.SalesOrgCode = shi.vbak_vkorg
                     AND cpf.DivisionCode = shi.vbap_spart
                     AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                     AND cpf.PartnerFunction = vb.pBillToPartyPartnerFunction
                     AND cpf.RowIsCurrent = 1
               ),
             1)
WHERE so.dd_SalesDocNo = shi.vbak_vbeln
             AND so.dd_SalesItemNo = shi.vbap_posnr
             AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln
and sdp.vbpa_parvw = pBillToPartyPartnerFunction AND so.dd_scheduleno <> 0;



UPDATE       fact_salesorder so
FROM vbak_vbap_vbep shi, vbpa sdp,variable_holder_701 vb
SET so.Dim_BillToPartyPartnerFunctionId =
          ifnull(
             (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID
                FROM tmp_sof_Dim_CustomerPartnerFunctions cpf
               WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                     AND cpf.SalesOrgCode = shi.vbak_vkorg
                     AND cpf.DivisionCode = shi.vbap_spart
                     AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                     AND cpf.PartnerFunction = vb.pBillToPartyPartnerFunction
                     AND cpf.RowIsCurrent = 1
               ),
             1)
Where so.dd_SalesDocNo = shi.vbak_vbeln
             AND so.dd_SalesItemNo = shi.vbap_posnr
             AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
and sdp.vbpa_parvw = pBillToPartyPartnerFunction AND so.dd_scheduleno <> 0;


UPDATE       fact_salesorder so
FROM vbak_vbap_vbep shi, vbpa sdp,variable_holder_701 vb
SET so.Dim_PayerPartnerFunctionId =
          ifnull(
             (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID
                FROM Dim_CustomerPartnerFunctions cpf
               WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                     AND cpf.SalesOrgCode = shi.vbak_vkorg
                     AND cpf.DivisionCode = shi.vbap_spart
                     AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                     AND cpf.PartnerFunction = vb.pPayerPartnerFunction
                     AND cpf.RowIsCurrent = 1
               ),
             1)
Where so.dd_SalesDocNo = shi.vbak_vbeln
             AND so.dd_SalesItemNo = shi.vbap_posnr
             AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln
and sdp.vbpa_parvw = pPayerPartnerFunction AND so.dd_scheduleno <> 0;



UPDATE       fact_salesorder so
FROM   vbak_vbap_vbep shi, vbpa sdp,variable_holder_701 vb
SET so.Dim_PayerPartnerFunctionId =
          ifnull(
             (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID
                FROM Dim_CustomerPartnerFunctions cpf
               WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                     AND cpf.SalesOrgCode = shi.vbak_vkorg
                     AND cpf.DivisionCode = shi.vbap_spart
                     AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                     AND cpf.PartnerFunction = vb.pPayerPartnerFunction
                     AND cpf.RowIsCurrent = 1
               ),
             1)
WHERE  so.dd_SalesDocNo = shi.vbak_vbeln
             AND so.dd_SalesItemNo = shi.vbap_posnr
             AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
and sdp.vbpa_parvw = pPayerPartnerFunction AND so.dd_scheduleno <> 0;





  UPDATE fact_salesorder so
  FROM vbak_vbap_vbep shi , vbpa sdp ,variable_holder_701 vb
  SET  so.Dim_CustomPartnerFunctionId = ifnull( (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID FROM Dim_CustomerPartnerFunctions cpf
                                                    WHERE cpf.CustomerNumber1 = shi.vbak_kunnr
                                                    AND cpf.SalesOrgCode = shi.vbak_vkorg
                                                    AND cpf.DivisionCode = shi.vbap_spart
                                                    AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                                                    AND cpf.PartnerFunction = vb.pCustomPartnerFunctionKey
                                                    AND cpf.RowIsCurrent = 1), 1)
WHERE so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln AND vb.pCustomPartnerFunctionKey <> 'Not Set'
and sdp.VBPA_PARVW = pCustomPartnerFunctionKey AND so.dd_scheduleno <> 0;


UPDATE       fact_salesorder so
FROM  vbak_vbap_vbep shi, vbpa sdp,variable_holder_701 vb
SET so.Dim_CustomPartnerFunctionId =
          ifnull(
             (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID
                FROM Dim_CustomerPartnerFunctions cpf
               WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                     AND cpf.SalesOrgCode = shi.vbak_vkorg
                     AND cpf.DivisionCode = shi.vbap_spart
                     AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                     AND cpf.PartnerFunction = vb.pCustomPartnerFunctionKey
                     AND cpf.RowIsCurrent = 1
               ),
             1)
WHERE  so.dd_SalesDocNo = shi.vbak_vbeln
             AND so.dd_SalesItemNo = shi.vbap_posnr
             AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
AND vb.pCustomPartnerFunctionKey <> 'Not Set'
AND sdp.vbpa_parvw = pCustomPartnerFunctionKey AND so.dd_scheduleno <> 0;



  UPDATE fact_salesorder so
  FROM vbak_vbap_vbep shi,vbpa sdp ,variable_holder_701 vb
  SET  so.Dim_CustomPartnerFunctionId1 = ifnull( (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID FROM Dim_CustomerPartnerFunctions cpf
                                                    WHERE cpf.CustomerNumber1 = shi.vbak_kunnr
                                                    AND cpf.SalesOrgCode = shi.vbak_vkorg
                                                    AND cpf.DivisionCode = shi.vbap_spart
                                                    AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                                                    AND cpf.PartnerFunction = vb.pCustomPartnerFunctionKey1
                                                    AND cpf.RowIsCurrent = 1 ), 1)
WHERE so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln AND vb.pCustomPartnerFunctionKey1 <> 'Not Set'
AND sdp.vbpa_parvw = pCustomPartnerFunctionKey1 AND so.dd_scheduleno <> 0;


UPDATE       fact_salesorder so
FROM vbak_vbap_vbep shi, vbpa sdp,variable_holder_701 vb
SET so.Dim_CustomPartnerFunctionId1 =
          ifnull(
             (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID
                FROM Dim_CustomerPartnerFunctions cpf
               WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                     AND cpf.SalesOrgCode = shi.vbak_vkorg
                     AND cpf.DivisionCode = shi.vbap_spart
                     AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                     AND cpf.PartnerFunction = vb.pCustomPartnerFunctionKey1
                     AND cpf.RowIsCurrent = 1
               ),
             1)
WHERE  so.dd_SalesDocNo = shi.vbak_vbeln
             AND so.dd_SalesItemNo = shi.vbap_posnr
             AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr 
AND  vb.pCustomPartnerFunctionKey1 <> 'Not Set'
AND sdp.vbpa_parvw = pCustomPartnerFunctionKey1 AND so.dd_scheduleno <> 0;



UPDATE fact_salesorder so
FROM vbak_vbap_vbep shi ,vbpa sdp ,variable_holder_701 vb
  SET  so.Dim_CustomPartnerFunctionId2 = ifnull( (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID FROM Dim_CustomerPartnerFunctions cpf
                                                    WHERE cpf.CustomerNumber1 = shi.vbak_kunnr
                                                    AND cpf.SalesOrgCode = shi.vbak_vkorg
                                                    AND cpf.DivisionCode = shi.vbap_spart
                                                    AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                                                    AND cpf.PartnerFunction = vb.pCustomPartnerFunctionKey2
                                                    AND cpf.RowIsCurrent = 1 ), 1)
WHERE so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
AND sdp.vbpa_vbeln = shi.vbak_vbeln and vb.pCustomPartnerFunctionKey2 <> 'Not Set' 
AND sdp.vbpa_parvw = pCustomPartnerFunctionKey2 AND so.dd_scheduleno <> 0;


UPDATE       fact_salesorder so
FROM vbak_vbap_vbep shi, vbpa sdp,variable_holder_701 vb
   SET so.Dim_CustomPartnerFunctionId2 =
          ifnull(
             (SELECT FIRST 1 Dim_CustomerPartnerFunctionsID
                FROM Dim_CustomerPartnerFunctions cpf
               WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                     AND cpf.SalesOrgCode = shi.vbak_vkorg
                     AND cpf.DivisionCode = shi.vbap_spart
                     AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                     AND cpf.PartnerFunction = vb.pCustomPartnerFunctionKey2
                     AND cpf.RowIsCurrent = 1
               ),
             1)
WHERE  so.dd_SalesDocNo = shi.vbak_vbeln
             AND so.dd_SalesItemNo = shi.vbap_posnr
             AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln AND sdp.vbpa_posnr = shi.vbap_posnr
AND  vb.pCustomPartnerFunctionKey2 <> 'Not Set' 
AND sdp.vbpa_parvw = pCustomPartnerFunctionKey2 AND so.dd_scheduleno <> 0;



UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl
   SET so.dd_CustomerPONo = ifnull(vbk.VBAK_BSTNK,'Not Set')
WHERE  vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_scheduleno <> 0 
AND so.dd_CustomerPONo <> ifnull(vbk.VBAK_BSTNK,'Not Set');

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_creditrepresentativegroup cg
   SET  so.Dim_CreditRepresentativeId =
          ifnull( Dim_CreditRepresentativegroupId, 1)
WHERE  vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND  cg.CreditRepresentativeGroup = vbk.VBAK_SBGRP
                     AND cg.CreditControlArea = vbk.VBAK_KKBER
                     AND cg.RowIsCurrent = 1  AND so.dd_scheduleno <> 0 
AND so.Dim_CreditRepresentativeId <> ifnull( Dim_CreditRepresentativegroupId, 1);


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_MaterialPriceGroup1Id =
          ifnull(
             (SELECT Dim_MaterialPriceGroup1Id
                FROM Dim_MaterialPriceGroup1 mpg
               WHERE mpg.MaterialPriceGroup1 = vbk.VBAP_MVGR1
                     AND mpg.RowIsCurrent = 1),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_DeliveryBlockId =
          ifnull(
             (SELECT Dim_DeliveryBlockId
                FROM Dim_DeliveryBlock db
               WHERE db.DeliveryBlock = vbk.VBAK_LIFSK
                     AND db.RowIsCurrent = 1),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_SalesDocOrderReasonId =
          ifnull(
             (SELECT dim_salesdocorderreasonid
                FROM dim_salesdocorderreason sor
               WHERE sor.ReasonCode = vbk.VBAK_AUGRU AND sor.RowIsCurrent = 1),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_MaterialGroupId =
          ifnull(
             (SELECT dim_materialgroupid
                FROM dim_materialgroup mg
               WHERE mg.MaterialGroupCode = vbk.VBAP_MATKL
                     AND mg.RowIsCurrent = 1),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.amt_SubTotal3 = ifnull((vbk.VBAP_KZWI3 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)),1)
													   END) 
								* vbep_bmeng , 0),
       so.amt_SubTotal4 = ifnull((vbk.VBAP_KZWI4 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)),1)
													   END) 
										* vbep_bmeng , 0)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0
AND vbap_netpr  <> 0
AND vbap_netwr <> 0;

/* LK : 29 Aug changes - amt_Subtotal3_OrderQty */

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.amt_Subtotal3_OrderQty = ifnull((vbk.VBAP_KZWI3 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)),1)
													   END) 
								* vbep_wmeng , 0)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0
AND vbap_netpr  <> 0
AND vbap_netwr <> 0;

/* LK : 29 Aug changes - End of amt_Subtotal3_OrderQty changes */

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_PurchaseOrderTypeId =
          ifnull(
             (SELECT cpt.dim_customerpurchaseordertypeid
                FROM dim_customerpurchaseordertype cpt
               WHERE cpt.CustomerPOType = vbk.VBAK_BSARK
                     AND cpt.RowIsCurrent = 1),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;



UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_DateIdPurchaseOrder =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_BSTDK
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_DateIdQuotationValidFrom =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_ANGDT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_DateIdQuotationValidTo =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_BNDDT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;


UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_DateIdSOCreated =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_ERDAT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0;



UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET so.Dim_DateIdSODocument =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_AUDAT
                     AND dt.CompanyCode = pl.CompanyCode),
             1),
       so.dd_ReferenceDocumentNo = ifnull(vbk.VBAP_VGBEL,'Not Set')
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0 ;



UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.dd_CustomerPONo = ifnull(vbk.VBAK_BSTNK,'Not Set'),
       so.Dim_CreditRepresentativeId =
          ifnull(
             (SELECT Dim_CreditRepresentativegroupId
                FROM dim_creditrepresentativegroup cg
               WHERE     cg.CreditRepresentativeGroup = vbk.VBAK_SBGRP
                     AND cg.CreditControlArea = vbk.VBAK_KKBER
                     AND cg.RowIsCurrent = 1),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0;


UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_MaterialPriceGroup1Id =
          ifnull(
             (SELECT Dim_MaterialPriceGroup1Id
                FROM Dim_MaterialPriceGroup1 mpg
               WHERE mpg.MaterialPriceGroup1 = vbk.VBAP_MVGR1
                     AND mpg.RowIsCurrent = 1),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0;



UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_DeliveryBlockId =
          ifnull(
             (SELECT Dim_DeliveryBlockId
                FROM Dim_DeliveryBlock db
               WHERE db.DeliveryBlock = vbk.VBAK_LIFSK
                     AND db.RowIsCurrent = 1),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0;


UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_SalesDocOrderReasonId =
          ifnull(
             (SELECT dim_salesdocorderreasonid
                FROM dim_salesdocorderreason sor
               WHERE sor.ReasonCode = vbk.VBAK_AUGRU AND sor.RowIsCurrent = 1),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0;


UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_MaterialGroupId =
          ifnull(
             (SELECT dim_materialgroupid
                FROM dim_materialgroup mg
               WHERE mg.MaterialGroupCode = vbk.VBAP_MATKL
                     AND mg.RowIsCurrent = 1),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo =0;

UPDATE fact_salesorder so
from vbak_vbap_vbep, dim_materialpricegroup4 mg4
SET so.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
WHERE so.dd_SalesDocNo = VBAK_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = VBEP_ETENR
AND VBAP_MVGR4 is not null
AND VBAP_MVGR4 = mg4.MaterialPriceGroup4
AND mg4.RowIsCurrent = 1;

UPDATE fact_salesorder so
from vbak_vbap, dim_materialpricegroup4 mg4
SET so.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
WHERE so.dd_SalesDocNo = VBAP_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = 0
AND VBAP_MVGR4 is not null
AND VBAP_MVGR4 = mg4.MaterialPriceGroup4
AND mg4.RowIsCurrent = 1;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id = 1
WHERE so.dim_materialpricegroup4id IS NULL;

UPDATE fact_salesorder so
from vbak_vbap_vbep, dim_materialpricegroup5 mg5
SET so.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
WHERE so.dd_SalesDocNo = VBAK_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = VBEP_ETENR
AND VBAP_MVGR5 is not null
AND VBAP_MVGR5 = mg5.MaterialPriceGroup5
AND mg5.RowIsCurrent = 1;

UPDATE fact_salesorder so
from vbak_vbap, dim_materialpricegroup5 mg5
SET so.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
WHERE so.dd_SalesDocNo = VBAP_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = 0
AND VBAP_MVGR5 is not null
AND VBAP_MVGR5 = mg5.MaterialPriceGroup5
AND mg5.RowIsCurrent = 1;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id = 1
WHERE so.dim_materialpricegroup5id IS NULL;

UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.amt_SubTotal3 = ifnull(vbk.VBAP_KZWI3, 0) ,
       so.amt_SubTotal4 = ifnull(vbk.VBAP_KZWI4, 0) 
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo =0;

/* LK : 29 Aug change - Update new column amt_Subtotal3_OrderQty */

/* No need to split when there are no schedules */

UPDATE fact_salesorder so
   SET so.amt_Subtotal3_OrderQty = amt_SubTotal3
WHERE so.dd_ScheduleNo = 0;

/* End of 29 Aug change - Update new column amt_Subtotal3_OrderQty */

UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_PurchaseOrderTypeId =
          ifnull(
             (SELECT cpt.dim_customerpurchaseordertypeid
                FROM dim_customerpurchaseordertype cpt
               WHERE cpt.CustomerPOType = vbk.VBAK_BSARK
                     AND cpt.RowIsCurrent = 1),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo =0;



UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_DateIdPurchaseOrder =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_BSTDK
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo =0;



UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_DateIdQuotationValidFrom =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_ANGDT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo =0;


UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_DateIdQuotationValidTo =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_BNDDT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo =0;


UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_DateIdSOCreated =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_ERDAT
                     AND dt.CompanyCode = pl.CompanyCode),
             1)
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo =0;



UPDATE          fact_salesorder so
FROM vbak_vbap vbk, dim_plant pl
   SET so.Dim_DateIdSODocument =
          ifnull(
             (SELECT dt.Dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = vbk.VBAK_AUDAT
                     AND dt.CompanyCode = pl.CompanyCode),
             1),
       so.dd_ReferenceDocumentNo = ifnull(vbk.VBAP_VGBEL,'Not Set')
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo =0 ;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd, dim_salesdistrict sd
SET so.Dim_SalesDistrictId = sd.Dim_SalesDistrictid
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN
AND vkd.VBKD_BZIRK IS NOT NULL
AND sd.SalesDistrict = vkd.VBKD_BZIRK
AND sd.RowIsCurrent = 1;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd, dim_salesdistrict sd
SET so.Dim_SalesDistrictId = sd.Dim_SalesDistrictid
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN
AND so.dd_SalesItemNo = vkd.VBKD_POSNR
AND vkd.VBKD_BZIRK IS NOT NULL
AND sd.SalesDistrict = vkd.VBKD_BZIRK
AND sd.RowIsCurrent = 1;

UPDATE    fact_salesorder so
SET so.Dim_SalesDistrictId = 1
WHERE so.Dim_SalesDistrictId  IS NULL;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd, dim_accountassignmentgroup aag
SET so.Dim_AccountAssignmentGroupId = aag.Dim_AccountAssignmentGroupId
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN
AND vkd.VBKD_KTGRD IS NOT NULL
AND aag.AccountAssignmentGroup = vkd.VBKD_KTGRD
AND aag.RowIsCurrent = 1;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd, dim_accountassignmentgroup aag
SET so.Dim_AccountAssignmentGroupId = aag.Dim_AccountAssignmentGroupId
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN
AND so.dd_SalesItemNo = vkd.VBKD_POSNR
AND vkd.VBKD_KTGRD IS NOT NULL
AND aag.AccountAssignmentGroup = vkd.VBKD_KTGRD
AND aag.RowIsCurrent = 1;

UPDATE    fact_salesorder so
SET so.Dim_AccountAssignmentGroupId = 1
WHERE so.Dim_AccountAssignmentGroupId  IS NULL;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd
SET so.dd_BusinessCustomerPONo = ifnull(vkd.VBKD_BSTKD,'Not Set')
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd
SET so.dd_BusinessCustomerPONo = ifnull(vkd.VBKD_BSTKD,'Not Set')
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN
AND so.dd_SalesItemNo = vkd.VBKD_POSNR;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd, dim_date dt, dim_company dc
SET so.Dim_BillingDateId = dt.dim_dateid 
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN
AND so.dim_companyid = dc.dim_companyid
AND dt.datevalue = VBKD_FKDAT
AND dt.companycode = dc.CompanyCode
AND VBKD_FKDAT IS NOT NULL;

UPDATE    fact_salesorder so
FROM vbak_vbap_vbkd vkd, dim_date dt, dim_company dc
SET so.Dim_BillingDateId = dt.dim_dateid
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN
AND so.dd_SalesItemNo = vkd.VBKD_POSNR 
AND so.dim_companyid = dc.dim_companyid
AND dt.datevalue = VBKD_FKDAT
AND dt.companycode = dc.CompanyCode
AND VBKD_FKDAT IS NOT NULL;

UPDATE    fact_salesorder so
SET so.Dim_BillingDateId = 1
WHERE so.Dim_BillingDateId IS NULL;

UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
SET so.Dim_DateIdSOItemChangedOn =
          dt.Dim_Dateid
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND  dt.DateValue = vbk.VBAP_AEDAT AND dt.CompanyCode = pl.CompanyCode  AND so.dd_scheduleno <> 0 
AND vbk.VBAP_AEDAT IS NOT NULL;

UPDATE fact_salesorder so
SET so.Dim_DateIdSOItemChangedOn = 1
WHERE   so.Dim_DateIdSOItemChangedOn IS NULL;

UPDATE    fact_salesorder so
FROM vbfa_vbak_vbap f
SET so.ct_AfsTotalDrawn = ifnull(f.VBFA_RFMNG,0),
          so.dd_SubsequentDocNo = ifnull(f.VBFA_VBELN, 'Not Set'),
          so.dd_SubsDocItemNo = ifnull(f.VBFA_POSNN,0),
          so.Dim_SubsDocCategoryId=
              ifnull((SELECT Dim_DocumentCategoryid
                        FROM Dim_DocumentCategory dc
                      WHERE dc.DocumentCategory = f.VBFA_VBTYP_N),
                    1)
    WHERE f.VBFA_VBTYP_N = 'J'
AND  f.VBFA_VBELV = so.dd_SalesDocNo
      AND f.VBFA_POSNV = so.dd_SalesItemNo ;

update fact_salesorder
set Dim_CustomeridShipTo = Dim_Customerid
where Dim_CustomeridShipTo = 1;
	  
	  
	  
/* LK : 29 Aug changes - amt_Subtotal3inCustConfig_Billing */

/* First sum amt_customerconfigsubtotal3 over docno+itemno */ 
DROP TABLE IF EXISTS tmp_sof_fb_amt_CustomerConfigSubtotal3;
CREATE TABLE tmp_sof_fb_amt_CustomerConfigSubtotal3
AS
SELECT dd_salesdocno, dd_salesitemno, sum(amt_customerconfigsubtotal3) sum_amt_customerconfigsubtotal3
FROM fact_billing fb, dim_billingdocumenttype bdt, dim_billingmisc bm
where fb.dim_documenttypeid = bdt.dim_billingdocumenttypeid
and fb.dim_billingmiscid = bm.dim_billingmiscid
and bm.cancelflag <> 'X'
AND bdt.type IN ('F1','F2','ZF2','ZF2C')
GROUP BY dd_salesdocno, dd_salesitemno;

/* Update amt_Subtotal3inCustConfig_Billing in sof for dd_salesdlvrdocno, dd_salesdlvritemno with minimum schedule no */
DROP TABLE IF EXISTS tmp_sof_sof_minsche; 
CREATE TABLE tmp_sof_sof_minsche
AS
SELECT dd_salesdocno,dd_salesitemno,min(dd_scheduleno) min_dd_scheduleno
FROM fact_salesorder
GROUP BY dd_salesdocno,dd_salesitemno;

DROP TABLE IF EXISTS tmp_sof_amt_CustomerConfigSubtotal3_upd; 
CREATE TABLE tmp_sof_amt_CustomerConfigSubtotal3_upd
AS
SELECT so.dd_salesdocno,so.dd_salesitemno,so.min_dd_scheduleno,fb.sum_amt_customerconfigsubtotal3
FROM tmp_sof_fb_amt_CustomerConfigSubtotal3 fb,tmp_sof_sof_minsche so
WHERE so.dd_salesdocno = fb.dd_salesdocno
AND so.dd_salesitemno = fb.dd_salesitemno;

UPDATE fact_salesorder so
SET amt_Subtotal3inCustConfig_Billing = 0;

UPDATE fact_salesorder so 
FROM tmp_sof_amt_CustomerConfigSubtotal3_upd t
SET so.amt_Subtotal3inCustConfig_Billing = ifnull(t.sum_amt_customerconfigsubtotal3, 0)
WHERE so.dd_salesdocno = t.dd_salesdocno
AND so.dd_salesitemno = t.dd_salesitemno
AND so.dd_scheduleno = t.min_dd_scheduleno;

UPDATE fact_salesorder so
SET so.dd_SalesOrderBlocked = 'Not Set';

UPDATE fact_salesorder so
   FROM dim_overallstatusforcreditcheck os
SET so.dd_SalesOrderBlocked = 'X'
WHERE so.Dim_OverallStatusCreditCheckId = os.dim_overallstatusforcreditcheckid
AND ( so.Dim_DeliveryBlockId <> 1 OR so.Dim_BillingBlockid <> 1 
OR os.overallstatusforcreditcheck = 'B');



/* End of 29 Aug changes - amt_Subtotal3inCustConfig_Billing */

/* Delete 1 */
Drop table if exists deletepart_701 ;

Create table deletepart_701 
As
Select * from fact_salesorder
WHERE EXISTS (SELECT 1 FROM VBAK_VBAP_VBEP WHERE VBAK_VBELN = dd_SalesDocNo)
      AND NOT EXISTS (SELECT 1 FROM VBAK_VBAP_VBEP WHERE VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo)
      AND dd_ScheduleNo <> 0;



call vectorwise ( combine 'fact_salesorder-deletepart_701');


/* Delete 2 */

Drop table if exists deletepart_701 ;

Create table deletepart_701
As
Select * from fact_salesorder 
  WHERE EXISTS (SELECT 1 FROM VBAK_VBAP_VBEP WHERE VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo)
      AND NOT EXISTS (SELECT 1 FROM VBAK_VBAP_VBEP WHERE VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo)
      AND dd_ScheduleNo <> 0;

      

call vectorwise ( combine 'fact_salesorder-deletepart_701');

/* Delete 3 */

  DELETE FROM fact_salesorder
  WHERE dd_ScheduleNo = 0
        AND EXISTS
                (SELECT 1
                  FROM VBAK_VBAP
                  WHERE VBAP_VBELN = dd_SalesDocNo)
        AND NOT EXISTS
                    (SELECT 1
                      FROM VBAK_VBAP
                      WHERE VBAP_VBELN = dd_SalesDocNo
                            AND VBAP_POSNR = dd_SalesItemNo);
							
							
/* Delete 4	*/

DROP Table if exists tmp_MinSalesSchedules_0;
CREATE table tmp_MinSalesSchedules_0
AS 
SELECT dd_SalesDocNo,dd_SalesItemNo,min(dd_ScheduleNo) as MinScheduleNo
from facT_salesorder
where dd_ScheduleNo <> 0
group by dd_SalesDocNo,dd_SalesItemNo;

drop table if exists deletepart_702;
Create table deletepart_702
AS
Select *  FROM fact_salesorder f
  WHERE dd_ScheduleNo = 0
        AND EXISTS
                (SELECT 1 FROM tmp_MinSalesSchedules_0 t WHERE t.dd_SalesDocNo = f.dd_SalesDocNo
                  AND t.dd_SalesItemNo = f.dd_SalesItemNo);

call vectorwise(combine 'deletepart_702');
call vectorwise ( combine 'fact_salesorder-deletepart_702');
call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder so
  FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm
   SET so.dim_Customermastersalesid = cms.dim_Customermastersalesid
 WHERE     so.dim_salesorgid = sorg.dim_salesorgid
       AND so.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = so.dim_salesdivisionid
       AND cm.dim_customerid = so.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber;

UPDATE fact_salesorder so
SET so.dim_Customermastersalesid = 1
WHERE so.dim_Customermastersalesid IS NULL;

UPDATE fact_salesorder so
FROM  fact_productionorder po
	SET so.dd_ProdOrderNo = ifnull(po.dd_OrderNumber,'Not Set'),
	so.dd_ProdOrderItemNo = ifnull(po.dd_OrderItemNo,0)
WHERE  SO.dd_SalesDocNo = PO.dd_SalesOrderNo AND
	SO.dd_SalesItemNo = PO.dd_SalesOrderItemNo;

call vectorwise(combine 'fact_salesorder');
UPDATE          fact_salesorder so
FROM vbak_vbap_vbep vbk 
SET so.dd_CustomerMaterialNo = vbk.VBAP_KDMAT
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND  so.dd_scheduleno <> 0 
AND  vbk.VBAP_KDMAT IS NOT NULL;

UPDATE          fact_salesorder so
FROM vbak_vbap vbk 
SET so.dd_CustomerMaterialNo = vbk.VBAP_KDMAT
Where vbk.VBAP_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND  so.dd_scheduleno = 0 
AND  vbk.VBAP_KDMAT IS NOT NULL;

UPDATE          fact_salesorder so
SET so.dd_CustomerMaterialNo = 'Not Set'
Where so.dd_CustomerMaterialNo IS NULL;

call vectorwise(combine 'fact_salesorder');


/* Start of the final update	*/

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
  /*  SET sd.amt_Cost =
            Decimal(((CASE
                  WHEN f.amt_ExchangeRate < 0
                  THEN
                      (1 / (-1 * f.amt_ExchangeRate))
                  ELSE
                      f.amt_ExchangeRate
                END) * sd.amt_Cost_DocCurr),18,4) , */
	SET sd.amt_Cost = sd.amt_Cost_DocCurr,			--Not multiplying by local exchg rate. Stored as it is in doc/tran curr
        sd.ct_PriceUnit = f.ct_PriceUnit,
        sd.amt_UnitPrice = f.amt_UnitPrice,
        sd.amt_UnitPriceUoM = f.amt_UnitPriceUoM
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.amt_ExchangeRate = f.amt_ExchangeRate,
        sd.amt_ExchangeRate_GBL = f.amt_ExchangeRate_GBL
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET 
        sd.Dim_DateidSalesOrderCreated = f.Dim_DateidSalesOrderCreated,
        sd.Dim_DateidSchedDeliveryReq = f.Dim_DateidSchedDeliveryReq
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;       
 

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_DateidSchedDlvrReqPrev = f.Dim_DateidSchedDlvrReqPrev,
        sd.Dim_DateidMatlAvailOriginal = f.Dim_DateidMtrlAvail
        WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_DateidFirstDate = f.Dim_DateidFirstDate,
        sd.Dim_Currencyid = f.Dim_Currencyid
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
		

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_Currencyid_TRA = f.Dim_Currencyid_TRA
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;		
		

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_Currencyid_GBL = f.Dim_Currencyid_GBL
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;		


UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_Currencyid_STAT = f.Dim_Currencyid_STAT
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;				
        

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.amt_ExchangeRate_STAT = f.amt_ExchangeRate_STAT
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;			

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_Companyid = f.Dim_Companyid,
        sd.Dim_SalesDivisionid = f.Dim_SalesDivisionid
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
        

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_ShipReceivePointid = f.Dim_ShipReceivePointid,
        sd.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
        

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_SalesDocumentTypeid = f.Dim_SalesDocumentTypeid,
        sd.Dim_SalesOrgid = f.Dim_SalesOrgid
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
        

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_SalesGroupid = f.Dim_SalesGroupid,
        sd.Dim_CostCenterid = f.Dim_CostCenterid
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
        

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_BillingBlockid = f.Dim_BillingBlockid,
        sd.Dim_TransactionGroupid = f.Dim_TransactionGroupid
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.dim_purchaseordertypeid = f.dim_purchaseordertypeid
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
       

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_CustomeridSoldTo =
            CASE sd.Dim_CustomeridSoldTo
              WHEN 1 THEN f.Dim_CustomerID
              ELSE sd.Dim_CustomeridSoldTo
            END,
        sd.Dim_CustomerGroup1id = f.Dim_CustomerGroup1id
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
		
        
update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_MaterialPriceGroup4Id = so.Dim_MaterialPriceGroup4Id
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup4Id,1) <> so.Dim_MaterialPriceGroup4Id;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_MaterialPriceGroup5Id = so.Dim_MaterialPriceGroup5Id
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup5Id,1) <> so.Dim_MaterialPriceGroup5Id;

UPDATE fact_salesorderdelivery sd
From fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.Dim_CustomerGroup2id = f.Dim_CustomerGroup2id,
        sd.dim_salesorderitemcategoryid = f.dim_salesorderitemcategoryid,
        sd.Dim_ScheduleLineCategoryId = f.Dim_ScheduleLineCategoryId
  WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

/* Start Changes 03 Feb 2014 */

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg
SET dim_CustomerConditionGroups1id = cg.dim_CustomerConditionGroupsid
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND fso.dd_SalesItemNo = vp.VBKD_POSNR 
AND VBKD_KDKG1 IS NOT NULL
AND cg.CustomerCondGrp = VBKD_KDKG1
AND ifnull(fso.dim_CustomerConditionGroups1id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);
update fact_salesorder set dim_CustomerConditionGroups1id = 1 where dim_CustomerConditionGroups1id is NULL;

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg
SET dim_CustomerConditionGroups2id = cg.dim_CustomerConditionGroupsid
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND fso.dd_SalesItemNo = vp.VBKD_POSNR 
AND VBKD_KDKG2 IS NOT NULL
AND cg.CustomerCondGrp = VBKD_KDKG2
AND ifnull(fso.dim_CustomerConditionGroups2id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);
update fact_salesorder set dim_CustomerConditionGroups2id = 1 where dim_CustomerConditionGroups2id is NULL;

UPDATE fact_salesorder fso
FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg
SET dim_CustomerConditionGroups3id = cg.dim_CustomerConditionGroupsid
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND fso.dd_SalesItemNo = vp.VBKD_POSNR 
AND VBKD_KDKG3 IS NOT NULL
AND cg.CustomerCondGrp = VBKD_KDKG3
AND fso.dim_CustomerConditionGroups3id <> ifnull(cg.dim_CustomerConditionGroupsid,1);
update fact_salesorder set dim_CustomerConditionGroups3id = 1 where dim_CustomerConditionGroups3id is NULL;

/* End Changes 03 Feb 2014 */

call vectorwise(combine 'fact_salesorder');

/* Start Changes 12 Feb 2014 */

UPDATE fact_salesorder so
FROM vbak_vbap_vbep vbk, Dim_DeliveryBlock db
SET so.dim_scheduledeliveryblockid =  db.Dim_DeliveryBlockId
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBEP_LIFSP IS NOT NULL
AND db.DeliveryBlock = vbk.VBEP_LIFSP
AND db.RowIsCurrent = 1 
and ifnull(so.dim_scheduledeliveryblockid,-1) <> ifnull(db.Dim_DeliveryBlockId,-2);

UPDATE fact_salesorder so SET so.dim_scheduledeliveryblockid = 1 WHERE so.dim_scheduledeliveryblockid is NULL;

/* End Changes 12 Feb 2014 */

/* Start Changes 14 Feb 2014 */

UPDATE fact_salesorder so
FROM vbak_vbap_vbep vbk, Dim_CustomerGroup4 cg4
set so.Dim_CustomerGroup4id = ifnull(cg4.Dim_CustomerGroup4id,1)
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND cg4.CustomerGroup = VBAK_KVGR4
and ifnull(so.Dim_CustomerGroup4id,-1) <> ifnull(cg4.Dim_CustomerGroup4id,-2);

update fact_salesorder set Dim_CustomerGroup4id = 1 where Dim_CustomerGroup4id is NULL;

/* End Changes 14 Feb 2014 */

/*  CALL bi_populate_customer_hierarchy() */

/* 
drop table if exists deletepart_701
drop table if exists Dim_CostCenter_701
drop table if exists Dim_CostCenter_first1_701
drop table if exists dim_profitcenter_701
drop table if exists dim_profitcenteri_701
drop table if exists dim_profitcenter_upd_701
drop table if exists fact_salesorder_tmptbl
drop table if exists staging_update_701
drop table if exists max_holder_701
drop table if exists variable_holder_701 
drop table if exists VBAK_VBAP_VBEP_701
drop table if exists tmp_sof_Dim_CustomerPartnerFunctions
select 'Process Complete'
select 'sales order count ',count(*) from fact_salesorder
DROP TABLE IF EXISTS TMP_UPD_dd_CreditRep1		
*/

DROP TABLE IF EXISTS tmp_sof_fb_amt_CustomerConfigSubtotal3;
DROP TABLE IF EXISTS tmp_sof_sof_minsche; 
DROP TABLE IF EXISTS tmp_sof_amt_CustomerConfigSubtotal3_upd; 

select 'END OF PROC VW_bi_populate_salesorder_fact',TIMESTAMP(LOCAL_TIMESTAMP);
