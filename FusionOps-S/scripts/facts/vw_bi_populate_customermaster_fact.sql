
/* ***********************************************************************************************************************************
   Script         - vi_bw_populate_customermaster_fact.sql 								
   Author         - Cristina  									  		
   Created ON     - Jun 2015 												
   Description    - Populate fact_customermaster for base consolidation project
					
 ********************************************Change History**************************************************************************	
  Date			    By			 Version		Desc        
  28 Jun 2015		Cristina		1.0		Modify the script from Columbia project, keep only View1 which is using STD SAP tables
								
 *************************************************************************************************************************************/

 /* Temp table used for INSERTS */
 DROP TABLE if exists fact_customermaster_tmp;
 CREATE TABLE fact_customermaster_tmp
 AS
 SELECT * FROM fact_customermaster
 WHERE 1 = 2;

 /*  ------- VIEW 1 -------- */
/* KNA1, ADRC, KNVV, KNVP */
DROP TABLE IF EXISTS tmp_masterView_001;
CREATE TABLE tmp_masterView_001
AS
SELECT 	
	 kna.KNA1_KUNNR  /* KNA1 key for dim_customerid (Master Table)*/
	,kna.KNA1_ADRNR
	,adr.ADRC_ADDRNUMBER /* ADRC key for dim_address */
	,knv.KNVV_KUNNR
	,knv.KNVV_SPART /* KNVV keys for dim_customermastersalesid*/
	,knv.KNVV_VKORG
	,knv.KNVV_VTWEG
	,knv.KNVV_J_3AGRSGY
	,knv.KNVV_INCO1
	,knv.KNVV_VSBED
	,knv.KNVV_WAERS
	,kvp.KNVP_KUNNR	/* KNVP keys for dim_customerpartnerfunctionsid*/
	,kvp.KNVP_VKORG		
	,kvp.KNVP_SPART
	,kvp.KNVP_VTWEG
	,kvp.KNVP_PARVW
	,kvp.KNVP_PARZA 
FROM KNA1 kna
LEFT JOIN ADRC adr
	ON kna.KNA1_ADRNR = adr.ADRC_ADDRNUMBER
INNER JOIN KNVV knv
	ON kna.KNA1_KUNNR = knv.KNVV_KUNNR
INNER JOIN KNVP kvp	
	ON kna.KNA1_KUNNR = kvp.KNVP_KUNNR;


/* To generate surrogate keys*/
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_customermaster_tmp';

INSERT INTO NUMBER_FOUNTAIN
select 	'fact_customermaster_tmp',
	ifnull(max(f.fact_customermasterid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_customermaster_tmp f;


/* Insert data FROM  KNA1, ADRC, KNVV, KNVP  table into fact_customermaster  -- View 1*/
INSERT INTO fact_customermaster_tmp(
	fact_customermasterid
	,dim_customerid
	,dim_addressid
	,dim_customermastersalesid
	/* AFS Retail only ,dim_allocationreleaseruleid */
	,dim_customerpartnerfunctionsid
	,dim_incotermid
	,dim_shippingconditionid
	,dim_currencyid
	,dim_currencyid_gbl
	,amt_exchangerate
	,amt_exchangerate_gbl
	)
SELECT
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_customermaster_tmp') + row_number() over() as fact_customermasterid
	,ifnull(dim_customerid,1)
	,ifnull(dadr.dim_addressid, 1)
	,ifnull(dsal.dim_customermastersalesid ,1)
	/* AFS Retail only
	,ifnull((SELECT dim_allocationreleaseruleID FROM dim_allocationreleaserule alc, J_3ARESG_T t WHERE tmp.KNVV_J_3AGRSGY =  t.J_3ARESG_T_J_3ARESGY AND alc.releaserule = tmp.KNVV_J_3AGRSGY AND t.J_3ARESG_T_SPRAS='E'),1) as dim_allocationreleaseruleID
	*/
	,ifnull(dpn.dim_customerpartnerfunctionsid,1)
	,ifnull((SELECT dim_incotermid FROM dim_incoterm inco WHERE tmp.KNVV_INCO1 = inco.incotermcode),1) as dim_incotermid
	,ifnull((SELECT dim_ShippingConditionid FROM dim_ShippingCondition dsh, TVSBT t WHERE tmp.KNVV_VSBED = t.TVSBT_VSBED AND t.TVSBT_VSBED = dsh.shippingconditioncode), 1) as dim_ShippingConditionid
	,ifnull((SELECT cur.dim_currencyid FROM dim_currency cur WHERE tmp.KNVV_WAERS = cur.currencycode) ,1)
	,1 dim_currencyid_gbl
	,1 amt_exchangerate
	,1 amt_exchangerate_gbl
FROM tmp_masterView_001 tmp
INNER JOIN dim_customer dcust
	ON tmp.KNA1_KUNNR = dcust.customernumber
LEFT OUTER JOIN dim_address dadr
	ON	tmp.KNA1_ADRNR = dadr.addressnumber
INNER JOIN dim_customermastersales dsal
	ON  tmp.KNVV_KUNNR = dsal.customernumber
	AND tmp.KNVV_SPART = dsal.divisioncode
	AND tmp.KNVV_VKORG = dsal.salesorg
	AND tmp.KNVV_VTWEG = dsal.distributionchannel
LEFT JOIN  dim_customerpartnerfunctions dpn
	ON tmp.KNVP_KUNNR = dpn.customernumber1
	AND tmp.KNVP_VKORG	= dpn.salesorgcode
	AND tmp.KNVP_SPART = dpn.divisioncode
	AND tmp.KNVP_VTWEG = dpn.distributionchannelcode
	AND tmp.KNVP_PARVW = dpn.partnerfunction
	AND tmp.KNVP_PARZA = dpn.partnercounter
WHERE NOT EXISTS (SELECT 1 
		FROM fact_customermaster_tmp f 
		WHERE f.dim_customerid = dcust.dim_customerid
		AND f.dim_addressid = dadr.dim_addressid
		AND f.dim_customermastersalesid = dsal.dim_customermastersalesid
		AND f.dim_customerpartnerfunctionsid = dpn.dim_customerpartnerfunctionsid
	);	


/* Freshly Insert Data from Temp Table to Fact Table*/
CALL VECTORWISE(COMBINE 'fact_customermaster_tmp');
CALL VECTORWISE(COMBINE 'fact_customermaster - fact_customermaster');
CALL VECTORWISE(COMBINE 'fact_customermaster');
CALL VECTORWISE(COMBINE 'fact_customermaster + fact_customermaster_tmp');



DROP TABLE IF EXISTS tmp_masterView_001;
DROP TABLE IF EXISTS fact_customermaster_tmp;
