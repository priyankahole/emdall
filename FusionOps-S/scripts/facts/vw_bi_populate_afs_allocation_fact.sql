/* ##################################################################################################################
  
     Script         : bi_populate_afs_allocation_fact
     Author         : Shanthi
     Created On     : 22 May 2013
  
  
     Description    : Stored Proc bi_populate_afs_allocation_fact from MySQL to Vectorwise syntax
  
     Change History
     Date            By        Version           Desc
     29 Apr 2014     Andra     1.2               Added columns dd_CancelvsMatAvailBucket,dd_CDDvsMatAvailBucket,dd_RDDvsMatAvailBucket 
     23 Jan 2013     Andra     1.1               Added column dd_clearedblockedsts
     22 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
#################################################################################################################### */

select 'START OF PROC bi_populate_afs_allocation_fact',TIMESTAMP(LOCAL_TIMESTAMP);

Drop table if exists fact_afsallocation_temp;

create table fact_afsallocation_temp
AS
Select * from fact_afsallocation where 1 = 2;

call vectorwise(combine 'fact_salesorderdelivery');

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE J_3ABDBS SET J_3ABDBS_J_3ASTAT = 'Not Set' WHERE J_3ABDBS_J_3ASTAT IS NULL;

UPDATE J_3ARESH SET J_3ARESH_J_3ASTAT = 'Not Set' WHERE J_3ARESH_J_3ASTAT IS NULL;

select 'Insert 1 on fact_afsallocation_temp',TIMESTAMP(LOCAL_TIMESTAMP);

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

/* This insert query is for SOs that are not open, which
may be shipped or rejected. */
INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
				    dd_ExpectedMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid,
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  'Not Set',
	  (CASE WHEN f.Dim_AfsRejectionReasonId > 1 THEN 'Rejected' WHEN Dim_DateIdActualGI_Original > 1 THEN 'Shipped' WHEN Dim_DateIdActualGI_Original = 1 AND dd_Salesdlvrdocno <> 'Not Set' THEN 'On Delivery' ELSE  'Not Set' END) dd_ExpectedMRPStatus,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
	  'Not Set',
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          f.ct_AfsOpenQty,
	  0 ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ct_ScheduleQtySalesUnit,
          ct_ConfirmedQty,
	  0,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f. Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          f.ct_ReservedQty,
          f.ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull(Dim_SalesOfficeId,1)
     FROM fact_salesorderdelivery f,dim_Documentcategory dc
     WHERE dc.dim_documentcategoryid =  f.dim_documentcategoryid
           AND NOT EXISTS( SELECT 1 FROM j_3abdsi j
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo);

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
				    dd_ExpectedMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  'Not Set',
          'Unallocated',
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
	  'Not Set',
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull(j.J_3abdsi_menge,0) ct_AfsOpenQty,
          0 ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          f.ct_ScheduleQtySalesUnit,
          f.ct_ConfirmedQty,
	  0.0000 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull(f.Dim_SalesOfficeId,1)
     FROM fact_salesorderdelivery f, j_3abdsi j
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND NOT EXISTS ( SELECT 1 FROM J_3ABDBS j1
			    WHERE j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
	    		      AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			      AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM J_3ARESH j2
			    WHERE f.dd_SalesdocNo = j2.J_3ARESH_AUFNR
			     AND  f.dd_SalesItemNo = j2.J_3ARESH_POSNR
			     AND f.dd_ScheduleNo = j2.J_3ARESH_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND f1.dd_AfsMrpStatus = 'Not Set'
			    AND f1.dd_AfsReshMrpStatus = 'Not Set' 
			    AND f1.dd_AfsStockType = f.dd_AfsStockType
			    AND f1.dd_ArunStockType = 'Not Set');

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
				    dd_ExpectedMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  j1.J_3ABDBS_J_3ASTAT,
	  'Not Set',
	  (CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'R' then 'Reserved' WHEN j1.J_3ABDBS_J_3ASTAT = 'F' then 'Fixed' WHEN j1.J_3ABDBS_J_3ASTAT = 'D' then 'On Delivery' ELSE 'Not Set' END) dd_ExpectedMRPStatus,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
	  'Not Set',
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          (CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'D' THEN 0 ELSE ifnull((CASE WHEN j.J_3abdsi_menge > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0) END) ct_AfsOpenQty,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'D' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ifnull((CASE WHEN ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN ct_ConfirmedQty > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  J_3ABDBS_MENGE ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'R' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ReservedQty,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'F' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull(f.Dim_SalesOfficeId,1)
     FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_J_3ASTAT NOT IN ('D')
			  AND NOT EXISTS ( SELECT 1 FROM J_3ARESH j2
			    WHERE f.dd_SalesdocNo = j2.J_3ARESH_AUFNR
			     AND  f.dd_SalesItemNo = j2.J_3ARESH_POSNR
			     AND f.dd_ScheduleNo = j2.J_3ARESH_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND f1.dd_AfsReshMrpStatus = 'Not Set'
			    AND f1.dd_AfsStockType = f.dd_AfsStockType
			    AND f1.dd_ArunStockType = 'Not Set');

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;
		    
INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
				    dd_ExpectedMRPStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  j1.J_3ARESH_J_3ASTAT,
	  (CASE WHEN j1.J_3ARESH_J_3ASTAT = 'R' then 'Reserved' WHEN j1.J_3ARESH_J_3ASTAT = 'F' then 'Fixed' WHEN j1.J_3ARESH_J_3ASTAT = 'D' then 'On Delivery' ELSE 'Not Set' END) dd_ExpectedMRPStatus,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          'Not Set',
	  ifnull(j1.J_3ARESH_J_3ABSKZ,'Not Set'),
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          (CASE WHEN  j1.J_3ARESH_J_3ASTAT = 'D'  THEN 0 ELSE ifnull((CASE WHEN j.J_3ABDSI_menge > j1.J_3ARESH_MENGE THEN j1.J_3ARESH_MENGE ELSE j.J_3abdsi_menge END),0) END),
	  ifnull((CASE WHEN j1.J_3ARESH_J_3ASTAT = 'D' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ifnull((CASE WHEN ct_ScheduleQtySalesUnit > j1.J_3ARESH_MENGE THEN j1.J_3ARESH_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN ct_ConfirmedQty > j1.J_3ARESH_MENGE THEN j1.J_3ARESH_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  0 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j1.J_3ARESH_J_3ASTAT = 'R' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExpectedReservedQty,
	  ifnull((CASE WHEN j1.J_3ARESH_J_3ASTAT = 'F' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExecptedFixedQty,
	  ifnull(f.Dim_SalesOfficeId,1)
 FROM fact_salesorderdelivery f, j_3abdsi j, J_3ARESH j1
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j1.J_3ARESH_AUFNR = f.dd_SalesDocNo
			  AND j1.J_3ARESH_POSNR = f.dd_SalesItemNo
			  AND j1.J_3ARESH_ETENR = f.dd_ScheduleNo
			  AND NOT EXISTS ( SELECT 1 FROM J_3abdbs j2
			  WHERE j2.J_3ABDBS_AUFNR = f.dd_SalesDocNo
			  AND j2.J_3ABDBS_POSNR = f.dd_SalesItemNo
			  AND j2.J_3ABDBS_ETENR = f.dd_ScheduleNo)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp t1
			   WHERE t1.dd_salesdocno = f.dd_SalesDocNo
			     AND t1.dd_SalesItemNo = f.dd_SalesItemNo
			     AND t1.dd_ScheduleNo = f.dd_scheduleno
			     AND t1.dd_AfsMrpstatus = 'Not Set'
			     AND t1.dd_AfsReshMrpStatus = j1.J_3ARESH_J_3ASTAT
			     AND t1.dd_AfsStockType = f.dd_AfsStockType
			     AND t1.dd_ArunStockType = j1.J_3ARESH_J_3ABSKZ);

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;


INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
				    dd_ExpectedMRPStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  j1.J_3ABDBS_J_3ASTAT,
	  j2.J_3ARESH_J_3ASTAT,
	  (CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'R' THEN 'Reserved' WHEN j1.J_3ABDBS_J_3ASTAT = 'F' THEN 'Fixed' WHEN j1.J_3ABDBS_J_3ASTAT = 'D' THEN 'On Delivery' ELSE 'Not Set' END),
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
	  ifnull(j2.J_3ARESH_J_3ABSKZ,'Not Set'),
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          (CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'D' THEN 0 ELSE ifnull((CASE WHEN j.J_3abdsi_menge > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0) END),
	  ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'D' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
	  ifnull((CASE WHEN (ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ScheduleQtySalesUnit > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (ct_ConfirmedQty > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ConfirmedQty > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  ifnull(j1.J_3ABDBS_MENGE,0) ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
	  f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'R' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ReservedQty,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'F' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'R' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ExpectedReservedQty,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'F' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ExecptedFixedQty,
	  ifnull(f.Dim_SalesOfficeId,1)
     FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j1.J_3ABDBS_J_3ASTAT = j2.J_3ARESH_J_3ASTAT
			  AND j1.J_3ABDBS_J_3ABSKZ = j2.J_3ARESH_J_3ABSKZ
			  AND j1.J_3ABDBS_J_3ASTAT NOT IN ('D')			  
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus
			    AND f1.dd_AfsStockType = j1.J_3ABDBS_J_3ABSKZ
			    AND f1.dd_ArunStockType = j2.J_3ARESH_J_3ABSKZ);



call vectorwise ( combine 'fact_afsallocation_temp');

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;


INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
				    dd_ExpectedMRPStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  j2.J_3ARESH_J_3ASTAT,
	  (CASE WHEN j2.J_3ARESH_J_3ASTAT = 'R' THEN 'Reserved' WHEN j2.J_3ARESH_J_3ASTAT = 'F' THEN 'Fixed' WHEN j2.J_3ARESH_J_3ASTAT = 'D' THEN 'On Delivery' ELSE 'Not Set' END),
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          'Not Set',
	  ifnull(j2.J_3ARESH_J_3ABSKZ,'Not Set'),
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          (CASE WHEN j2.J_3ARESH_J_3ASTAT = 'D' THEN  0 ELSE ifnull((CASE WHEN (j.J_3abdsi_menge > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE > j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (j.J_3abdsi_menge > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE < j2.J_3ARESH_MENGE) THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0) END),
	  0 ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
	  ifnull((CASE WHEN (ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ScheduleQtySalesUnit > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (ct_ConfirmedQty > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ConfirmedQty > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  0 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
	  f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'R' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExpectedReservedQty,
          ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'F' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExecptedFixedQty,
	  ifnull(f.Dim_SalesOfficeId,1)
     FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j1.J_3ABDBS_J_3ASTAT = j2.J_3ARESH_J_3ASTAT
			  AND j1.J_3ABDBS_J_3ASTAT NOT IN ('D')			  
			  AND j2.J_3ARESH_J_3ABSKZ NOT IN   ( SELECT DISTINCT j3.J_3ABDBS_J_3ABSKZ FROM J_3ABDBS j3
								  where j2.J_3ARESH_AUFNR = j3.J_3ABDBS_AUFNR
								  AND j2.J_3ARESH_POSNR = j3.J_3ABDBS_POSNR
								  AND j2.J_3ARESH_ETENR = j3.J_3ABDBS_ETENR) 
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus
			    AND f1.dd_AfsStockType = 'Not Set'
			    AND f1.dd_ArunStockType = j2.J_3ARESH_J_3ABSKZ);


call vectorwise(combine 'fact_afsallocation_temp');

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;
			    
INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  j2.J_3ARESH_J_3ASTAT,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
	  ifnull(j2.J_3ARESH_J_3ABSKZ,'Not Set'),
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          (CASE WHEN j2.J_3ARESH_J_3ASTAT = 'D' THEN 0 ELSE ifnull((CASE WHEN j.J_3abdsi_menge > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0) END),
	  ifnull((CASE WHEN J_3ABDBS_J_3ASTAT = 'D' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
	  ifnull((CASE WHEN (ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ScheduleQtySalesUnit > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (ct_ConfirmedQty > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ConfirmedQty > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  0 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'R' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExpectedReservedQty,
          ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'F' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExecptedFixedQty,
	  ifnull(f.Dim_SalesOfficeId,1)
	  FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j2.J_3ARESH_J_3ABSKZ = j1.J_3ABDBS_J_3ABSKZ
			  AND j1.J_3ABDBS_J_3ASTAT NOT IN ('D')			  
			  AND j2.J_3ARESH_J_3ASTAT NOT IN ( SELECT DISTINCT j3.J_3ABDBS_J_3ASTAT FROM J_3ABDBS j3
								  where j2.J_3ARESH_AUFNR = j3.J_3ABDBS_AUFNR
								  AND j2.J_3ARESH_POSNR = j3.J_3ABDBS_POSNR
								  AND j2.J_3ARESH_ETENR = j3.J_3ABDBS_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus
			    AND f.dd_AfsStockType = j1.J_3ABDBS_J_3ABSKZ
			    AND f1.dd_ArunStockType = j2.J_3ARESH_J_3ABSKZ);

		  
call vectorwise(combine 'fact_afsallocation_temp');

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;
			    
INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  J_3ABDBS_J_3ASTAT,
	  'Not Set',
	  dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
	  ifnull(j2.J_3ARESH_J_3ABSKZ,'Not Set'),
	  ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          (CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'D' THEN 0 ELSE ifnull((CASE WHEN j.J_3abdsi_menge > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0) END),
	  ifnull((CASE WHEN J_3ABDBS_J_3ASTAT = 'D' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
	  ifnull((CASE WHEN (ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ScheduleQtySalesUnit > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (ct_ConfirmedQty > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ConfirmedQty > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  ifnull(j1.J_3ABDBS_MENGE,0) ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          ifnull((CASE WHEN J_3ABDBS_J_3ASTAT = 'R' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ReservedQty,
          ifnull((CASE WHEN J_3ABDBS_J_3ASTAT = 'F' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  0 ct_ExpectedReservedQty,
          0 ct_ExecptedFixedQty,
	  ifnull(f.Dim_SalesOfficeId,1)
	  FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j1.J_3ABDBS_J_3ABSKZ = j2.J_3ARESH_J_3ABSKZ
			  AND j1.J_3ABDBS_J_3ASTAT NOT IN ('D')			  
			  AND j1.J_3ABDBS_J_3ASTAT NOT IN ( SELECT DISTINCT j3.J_3ARESH_J_3ASTAT FROM J_3ARESH j3
								  where j2.J_3ARESH_AUFNR = j3.J_3ARESH_AUFNR
								  AND j1.J_3ABDBS_POSNR = j3.J_3ARESH_POSNR
								  AND j1.J_3ABDBS_ETENR = j3.J_3ARESH_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus
			    AND f.dd_AfsStocktype = f1.dd_AfsStockType
			    AND f1.dd_ArunStockType = j2.J_3ARESH_J_3ABSKZ);

		  
call vectorwise(combine 'fact_afsallocation_temp');

DROP TABLE IF EXISTS tmp_AllocatedAndForecastRESH;

CREATE TABLE tmp_AllocatedAndForecastRESH AS 
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,dd_AfsReshMrpStatus,SUM(ct_ExpectedReservedQty) as ExpReservedQty,SUM(ct_ExpectedFixedQty) as ExpFixedQty
FROM fact_afsallocation_temp
group by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,dd_AfsReshMrpStatus;


DROP TABLE IF EXISTS tmp_TotalOrdConfQtyAtSchedule;

CREATE TABLE tmp_TotalOrdConfQtyAtSchedule AS 
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,SUM(ct_AfsOpenQty) as OpenQty, sum(ct_ConfirmedQty) as ConfQty, SUM(ct_ScheduleQtySalesUnit) as OrderQty
FROM fact_afsallocation_temp
group by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo;

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty,
				    Dim_SalesOfficeId
				    )
SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by t.dd_SalesDocNo,t.dd_SalesItemNo,t.dd_ScheduleNo)) fact_afsallocationid, 
	  t.dd_SalesDocNo,
          t.dd_SalesItemNo,
          t.dd_ScheduleNo,
	  'Not Set',
	  j2.J_3ARESH_J_3ASTAT,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
	  ifnull(j2.J_3ARESH_J_3ABSKZ,'Not Set'),
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull((CASE WHEN (J_3abdsi_menge - t1.OpenQty ) >= (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) THEN (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) ELSE 0 END),0),
	      0 ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ifnull((CASE WHEN (f.ct_ScheduleQtySalesUnit - t1.OrderQty) >= (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) THEN (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) ELSE 0 END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (f.ct_ConfirmedQty - t1.ConfQty) >= (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) THEN (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) Else 0 END),0) ct_ConfirmedQty,
	      0 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'R' THEN (j2.J_3ARESH_MENGE - t.ExpReservedQty) ELSE 0 END),0) ct_ExpectedReservedQty,
          ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'F' THEN (j2.J_3ARESH_MENGE - t.ExpFixedQty) ELSE 0 END),0) ct_ExecptedFixedQty,
	  ifnull(f.Dim_SalesOfficeId,1)
	  FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2, tmp_AllocatedAndForecastRESH t, tmp_TotalOrdConfQtyAtSchedule t1
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = t1.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = t1.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = t1.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j1.J_3ABDBS_J_3ASTAT NOT IN ('D')
			  AND j1.J_3ABDBS_AUFNR  = t.dd_SalesDocNo
			  AND j1.J_3ABDBS_POSNR = t.dd_SalesItemNo
			  AND j1.J_3ABDBS_ETENR = t.dd_ScheduleNo
			  AND j2.J_3ARESH_J_3ASTAT = t.dd_AfsReshMrpStatus
			  AND ((t.dd_AfsReshMrpStatus = 'R' AND t.ExpReservedQty < j2.J_3ARESH_MENGE) OR
			       (t.dd_AfsReshMrpStatus = 'F' AND t.ExpFixedQty < j2.J_3ARESH_MENGE))
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus
			    AND f1.dd_AfsStockType = f.dd_AfsStocktype
			    AND f1.dd_ArunStockType = j2.J_3ARESH_J_3ABSKZ);
			    
delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

/* This insert query is for SOs that are not open, which
may be shipped or rejected. */
INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
				    dd_ExpectedMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
				    dd_ArunStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    Dim_SalesOfficeId
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid,
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  'Not Set',
	  (CASE WHEN f.Dim_AfsRejectionReasonId > 1 THEN 'Rejected' WHEN Dim_DateIdActualGI_Original > 1 THEN 'Shipped' WHEN Dim_DateIdActualGI_Original = 1 AND dd_Salesdlvrdocno <> 'Not Set' THEN 'On Delivery' ELSE  'Not Set' END) dd_ExpectedMRPStatus,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
	  'Not Set',
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          f.ct_AfsOpenQty,
	  ifnull((CASE WHEN J_3ABDBS_MENGE >= f.ct_QtyDelivered THEN J_3ABDBS_MENGE  ELSE f.ct_QtyDelivered END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ct_ScheduleQtySalesUnit,
          ct_ConfirmedQty,
	  0,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f. Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          f.ct_ReservedQty,
          f.ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull(Dim_SalesOfficeId,1)
     FROM fact_salesorderdelivery f,dim_Documentcategory dc,j_3abdbs j 
     WHERE dc.dim_documentcategoryid =  f.dim_documentcategoryid
	and j.j_3abdbs_aufnr = f.dd_SAlesdocno
	and j.j_3abdbs_posnr = f.dd_Salesitemno
	and j.j_3abdbs_etenr = f.dd_scheduleno
	and j.j_3abdbs_j_3astat = 'D'
	and  not exists ( select 1 from fact_afsallocation_temp f1
	where f1.dd_SAlesdocno = f.dd_SAlesdocno
	and f1.dd_Salesitemno = f.dd_Salesitemno
	and f1.dd_scheduleno = f.dd_scheduleno
    	AND f1.dd_AfsMrpStatus = 'Not Set'
    	AND f1.dd_AfsReshMrpStatus = 'Not Set'
    	AND f1.dd_AfsStockType = f.dd_AfsStocktype
    	AND f1.dd_ArunStockType = 'Not Set');

DROP TABLE IF EXISTS tmp_AllocationOrderQtyDiff;
CREATE TABLE tmp_AllocationOrderQtyDiff as
SELECT f.dd_SalesDocNo, f.dd_SalesItemNo,f.dd_ScheduleNo, f.dd_SalesDlvrDocNo,f.dd_SalesDlvrItemNo, 
sum(f.ct_ScheduleQtySalesUnit) as AllocOrderQty,sum(f.ct_ConfirmedQty)as AllocConfirmedQty
from fact_afsallocation_temp f
group by f.dd_SalesDocNo, f.dd_SalesItemNo,f.dd_ScheduleNo,f.dd_SalesDlvrDocNo,f.dd_SalesDlvrItemNo;

DROP TABLE IF EXISTS tmp_AllocationOrderQtyDiff_2;
CREATE TABLE tmp_AllocationOrderQtyDiff_2 AS
select f.dd_SalesDocNo, f.dd_SalesItemNo,f.dd_ScheduleNo,f.dd_SalesDlvrDocNo,f.dd_SalesDlvrItemNo,f.allocorderqty,sod.ct_ScheduleQtySalesUnit
from tmp_AllocationOrderQtyDiff f, fact_salesorderdelivery sod
where f.dd_SalesDocNo = sod.dd_SalesDocNo
AND f.dd_SalesItemNo = sod.dd_SalesItemNo
and f.dd_scheduleno = sod.dd_scheduleno
and f.dd_salesdlvrdocno  = sod.dd_salesdlvrdocno 
and f.dd_salesdlvritemno = sod.dd_salesdlvritemno
and  f.allocorderqty < sod.ct_ScheduleQtySalesUnit;

DROP TABLE IF EXISTS tmp_AllocationOrderQtyDiff_3;
CREATE TABLE tmp_AllocationOrderQtyDiff_3 AS
select f.dd_SalesDocNo, f.dd_SalesItemNo,f.dd_ScheduleNo,f.dd_SalesDlvrDocNo,f.dd_SalesDlvrItemNo,f.AllocConfirmedQty,sod.ct_confirmedqty
from tmp_AllocationOrderQtyDiff f, fact_salesorderdelivery sod
where f.dd_SalesDocNo = sod.dd_SalesDocNo
AND f.dd_SalesItemNo = sod.dd_SalesItemNo
and f.dd_scheduleno = sod.dd_scheduleno
and f.dd_salesdlvrdocno  = sod.dd_salesdlvrdocno 
and f.dd_salesdlvritemno = sod.dd_salesdlvritemno
and  f.AllocConfirmedQty < sod.ct_confirmedqty;

UPDATE fact_afsallocation_temp  fa
FROM tmp_AllocationOrderQtyDiff_2 t
SET fa.ct_ScheduleQtySalesUnit =  fa.ct_ScheduleQtySalesUnit + (t.ct_ScheduleQtySalesUnit - fa.ct_ScheduleQtySalesUnit)
WHERE fa.dd_SalesDocNo = t.dd_SalesDocNo
AND fa.dd_SalesItemNo = t.dd_SalesItemNo
AND fa.dd_scheduleNo = t.dd_ScheduleNo
and fa.dd_salesdlvrdocno  = t.dd_salesdlvrdocno 
and fa.dd_salesdlvritemno = t.dd_salesdlvritemno
AND (fa.dd_AfsReshMrpStatus in ( 'R','F','D') OR fa.dd_AfsMrpStatus in ( 'R','F','D'));


UPDATE fact_afsallocation_temp  fa
FROM tmp_AllocationOrderQtyDiff_3 t
SET fa.ct_confirmedqty =  fa.ct_confirmedqty + (t.ct_confirmedqty - fa.ct_ConfirmedQty)
WHERE fa.dd_SalesDocNo = t.dd_SalesDocNo
AND fa.dd_SalesItemNo = t.dd_SalesItemNo
AND fa.dd_scheduleNo = t.dd_ScheduleNo
and fa.dd_salesdlvrdocno  = t.dd_salesdlvrdocno 
and fa.dd_salesdlvritemno = t.dd_salesdlvritemno
AND (fa.dd_AfsReshMrpStatus in ( 'R','F','D') OR fa.dd_AfsMrpStatus in ( 'R','F','D'));

DROP TABLE IF EXISTS tmp_AllocationOrderQtyDiff_3;
DROP TABLE IF EXISTS tmp_AllocationOrderQtyDiff_2;
DROP TABLE IF EXISTS tmp_AllocationOrderQtyDiff;

UPDATE fact_afsallocation_temp  t
  FROM J_3ARESH jr, Dim_Date dt, Dim_Company c
SET t.Dim_DateidMatlAvail = dt.Dim_DateId
WHERE t.dim_Companyid = c.Dim_CompanyId
 AND t.dd_Salesdocno = jr.J_3aRESH_AUFNR
 AND t.dd_SalesItemNo = jr.J_3ARESH_POSNR
 AND t.dd_ScheduleNo = jr.J_3ARESH_ETENR
 AND t.dd_ArunStockType = jr.J_3ARESH_J_3ABSKZ
 AND t.dd_AfsReshMrpStatus = jr.J_3ARESH_J_3ASTAT
 AND dt.DateValue = jr.J_3ARESH_MBDAT
 AND dt.CompanyCode = c.CompanyCode;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE fact_afsallocation_temp t
SET t.ct_CalculatedOpenQty  = 0
WHERE t.ct_CalculatedOpenQty  <> 0;

UPDATE fact_afsallocation_temp t
 FROM dim_documentcategory dc
SET t.ct_CalculatedOpenQty  = (t.ct_ConfirmedQty - t.ct_AfsTotalDrawn)
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory in ('G','g','B','b') 
AND t.Dim_AfsRejectionReasonId = 1 
AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0);

UPDATE fact_afsallocation_temp t
 FROM dim_documentcategory dc
SET t.ct_CalculatedOpenQty  = (t.ct_AfsOpenQty - t.ct_QtyDelivered)
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory NOT IN ('G','g','B','b') 
AND t.Dim_DateidActualGI_Original = 1 
AND t.Dim_AfsRejectionReasonId = 1 
AND ((t.ct_AfsOpenQty - t.ct_QtyDelivered) >= 0);

UPDATE fact_afsallocation_temp t
 FROM dim_documentcategory dc
SET t.ct_CalculatedOpenQty  = t.ct_AfsOpenQty
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory NOT IN ('G','g','B','b') 
AND t.Dim_DateidActualGI_Original <> 1 
AND t.Dim_AfsRejectionReasonId = 1 
AND ((t.ct_AfsOpenQty - (t.ct_ConfirmedQty - t.ct_QtyDelivered)) >= 0);

UPDATE fact_afsallocation_temp t
SET t.ct_CalculatedOpenQty  = 0
WHERE t.ct_CalculatedOpenQty IS NULL;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE fact_afsallocation_temp afsa
SET Dim_DateIdRejection = (CASE WHEN afsa.Dim_DateIdSOItemChangedOn = 1 OR afsa.Dim_DateIdSOItemChangedOn IS NULL THEN  afsa.Dim_DateIdSOCreated ELSE afsa.Dim_DateIdSOItemChangedOn END)
WHERE afsa.Dim_AfsRejectionReasonId <> 1
and Dim_DateIdRejection = 1;

UPDATE fact_Afsallocation_temp afsa
SET ct_AfsUnallocatedQty = afsa.ct_CalculatedOpenQty - afsa.ct_AfsAllocatedQty
WHERE afsa.ct_CalculatedOpenQty >= afsa.ct_AfsAllocatedQty;

UPDATE fact_afsallocation_temp
SET dd_ExpectedMrpStatus = 'Fixed'
WHERE (dd_ExpectedMrpStatus IS NULL OR dd_ExpectedMrpStatus = 'Not Set')
AND ((dd_AfsMrpStatus = 'F' AND dd_AfsReshMrpStatus IN ('Not Set','R')) OR (dd_AfsMrpStatus IN ('Not Set','R') AND dd_AfsReshMrpStatus = 'F'));

UPDATE fact_afsallocation_temp
SET dd_ExpectedMrpStatus = 'Reserved'
WHERE (dd_ExpectedMrpStatus IS NULL OR dd_ExpectedMrpStatus = 'Not Set')
AND ((dd_AfsMrpStatus = 'R' AND dd_AfsReshMrpStatus = 'Not Set') OR (dd_AfsMrpStatus = 'Not Set' AND dd_AfsReshMrpStatus = 'R'));

UPDATE fact_afsallocation_temp
SET dd_ExpectedMrpStatus = 'On Delivery'
WHERE (dd_ExpectedMrpStatus IS NULL OR dd_ExpectedMrpStatus = 'Not Set')
AND ((dd_AfsMrpStatus = 'D' AND dd_AfsReshMrpStatus = 'Not Set') OR (dd_AfsMrpStatus = 'Not Set' AND dd_AfsReshMrpStatus = 'D'));

UPDATE fact_afsallocation_temp t
   FROM dim_DocumentCategory dc
SET dd_AllocationStatus = (CASE  WHEN dc.documentcategory IN ('B','b') AND t.ct_CalculatedOpenQty > 0 THEN 'Open Quotation' WHEN dc.DocumentCategory IN ('G','g') THEN 'Unallocated Contract' ELSE 'Unallocated Order' END )
WHERE dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dd_ExpectedMrpStatus IN ('Not Set', 'Unallocated');


UPDATE fact_afsallocation_temp t
   FROM dim_DocumentCategory dc
SET dd_AllocationStatus =  'Rejected Order'
WHERE dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dc.DocumentCategory IN ('C','c')
AND dd_ExpectedMrpStatus IN ('Rejected');

UPDATE fact_afsallocation_temp t
   FROM dim_DocumentCategory dc
SET dd_AllocationStatus =  'Rejected Contract'
WHERE dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dc.DocumentCategory IN ('G','g')
AND dd_ExpectedMrpStatus IN ('Rejected');

UPDATE fact_afsallocation_temp t
   FROM dim_DocumentCategory dc
SET dd_AllocationStatus =  'Rejected Quotation'
WHERE dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dc.DocumentCategory IN ('B','b')
AND dd_ExpectedMrpStatus IN ('Rejected');

UPDATE fact_afsallocation_temp t
   FROM dim_DocumentCategory dc
SET dd_AllocationStatus =  'Shipped'
WHERE dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dc.DocumentCategory IN ('C','c')
AND dd_ExpectedMrpStatus IN ('Shipped');

UPDATE fact_afsallocation_temp t
   FROM dim_DocumentCategory dc
SET dd_AllocationStatus = (CASE  WHEN dc.documentcategory IN ('G','g') THEN 'Allocated Contract' WHEN dc.DocumentCategory IN ('C','c') THEN 'Allocated Order' END )
WHERE dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dd_ExpectedMrpStatus IN ('Reserved', 'Fixed');

UPDATE fact_afsallocation_temp t
SET dd_AllocationStatus = 'On Delivery'
WHERE dd_ExpectedMrpStatus IN ('On Delivery');

UPDATE fact_afsallocation_temp t
   FROM dim_DocumentCategory dc
SET dd_AllocationStatus = 'Drawn Quotation'
WHERE dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dc.DocumentCategory IN ('B','b')
AND t.ct_CalculatedOpenQty = 0
AND t.Dim_AfsRejectionReasonId = 1;
 
UPDATE fact_afsallocation_temp t
   FROM dim_DocumentCategory dc
SET dd_AllocationStatus = 'Drawn Contract'
WHERE dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dc.DocumentCategory IN ('G','g')
AND t.ct_CalculatedOpenQty = 0
AND t.Dim_AfsRejectionReasonId = 1;

update fact_afsallocation_temp t
set t.dd_Allocationstatus = 'Not Set'
where t.dd_Allocationstatus is null; 

UPDATE fact_afsallocation_temp t
  FROM dim_DocumentCategory dc
SET ct_AllocationStatusQty =  (t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) 
WHERE  dc.dim_DocumentCategoryid = t.Dim_DocumentCategoryid
AND dc.DocumentCategory IN ('B','b')
AND t.Dim_DateidActualGI_Original = 1
AND t.Dim_AfsRejectionReasonId = 1
AND (t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) > 0;

UPDATE fact_afsallocation_Temp t
SET ct_AllocationStatusQty = t.ct_AfsUnallocatedQty
WHERE t.ct_AfsUnallocatedQty > 0
AND t.dd_ExpectedMrpStatus IN ('Not Set','Unallocated');

UPDATE fact_afsallocation_Temp t
SET ct_AllocationStatusQty = (CASE WHEN t.ct_ReservedQty <> 0 THEN ct_ReservedQty ELSE ct_ExpectedReservedQty END )
WHERE t.ct_AfsAllocatedQty >= 0
AND t.dd_ExpectedMrpStatus IN ('Reserved');

UPDATE fact_afsallocation_Temp t
SET ct_AllocationStatusQty = (CASE WHEN t.ct_FixedQty <> 0 THEN ct_FixedQty ELSE ct_ExpectedFixedQty END )
WHERE t.ct_AfsAllocatedQty >= 0
AND t.dd_ExpectedMrpStatus IN ('Fixed');

UPDATE fact_afsallocation_Temp t
SET ct_AllocationStatusQty = t.ct_AfsOnDeliveryqty
WHERE t.ct_AfsOnDeliveryqty >= 0
AND t.dd_ExpectedMrpStatus IN ('On Delivery');

UPDATE fact_afsallocation_Temp t
SET ct_AllocationStatusQty = t.ct_ScheduleQtySalesUnit
WHERE t.ct_ScheduleQtySalesUnit >= 0
AND t.Dim_AfsRejectionReasonId > 1
AND t.dd_ExpectedMrpStatus IN ('Rejected');

UPDATE fact_afsallocation_Temp t
SET ct_AllocationStatusQty = t.ct_QtyDelivered
WHERE t.ct_QtyDelivered >= 0
AND t.Dim_DateIdActualGI_Original > 1
AND t.dd_ExpectedMrpStatus IN ('Shipped');

DROP TABLE IF EXISTS tmp_AllocatedAndForecastRESH;

DROP TABLE IF EXISTS tmp_TotalOrdConfQtyAtSchedule;

call vectorwise ( combine 'fact_afsallocation_temp');

UPDATE fact_afsallocation_temp afsa
 FROM J_3ARESH j, dim_plant pl, dim_date dt
 SET afsa.Dim_DateIdReshPOSchedLineAvailability = dt.dim_Dateid
 WHERE j.J_3ARESH_AUFNR = afsa.dd_SalesDocNo
 and j.J_3ARESH_POSNR  = afsa.dd_SalesItemNo
 and j.J_3ARESH_ETENR = afsa.dd_ScheduleNo
 and afsa.dim_plantid = pl.dim_plantid
 AND dt.companycode = pl.companycode
 AND dt.datevalue = j.J_3ARESH_TERMN
 AND j.J_3ARESH_TERMN IS NOT NULL;

UPDATE fact_afsallocation_temp afsa
 SET afsa.Dim_DateIdReshPOSchedLineAvailability = 1
 WHERE afsa.Dim_DateIdReshPOSchedLineAvailability IS  NULL;

UPDATE fact_afsallocation_temp afsa
 FROM J_3ABDBS j, dim_plant pl, dim_date dt
 SET afsa.Dim_DateIdPOSchedLineAvailability = dt.dim_Dateid
 WHERE j.J_3ABDBS_AUFNR = afsa.dd_SalesDocNo
 and j.J_3ABDBS_POSNR  = afsa.dd_SalesItemNo
 and j.J_3ABDBS_ETENR = afsa.dd_ScheduleNo
 and afsa.dim_plantid = pl.dim_plantid
 AND dt.companycode = pl.companycode
 AND dt.datevalue = j.J_3ABDBS_TERMN
 AND j.J_3ABDBS_TERMN IS NOT NULL;
 
UPDATE fact_afsallocation_temp afsa
 SET afsa.Dim_DateIdPOSchedLineAvailability = afsa.Dim_DateIdReshPOSchedLineAvailability
 WHERE
 (NOT EXISTS ( SELECT 1 FROM J_3ABDBS j
		        WHERE j.J_3ABDBS_AUFNR = afsa.dd_SalesDocNo
                  AND j.J_3ABDBS_POSNR  = afsa.dd_SalesItemNo
                  AND j.J_3ABDBS_ETENR = afsa.dd_ScheduleNo
                )
OR afsa.Dim_DateIdPOSchedLineAvailability = 1);

UPDATE fact_afsallocation_temp afsa
 SET afsa.Dim_DateIdPOSchedLineAvailability = 1
 WHERE afsa.Dim_DateIdPOSchedLineAvailability IS  NULL;
 
UPDATE fact_afsallocation_temp afsa
 FROM J_3ABDBS j, dim_plant pl, dim_date dt
 SET afsa.Dim_DateIdDemandMatAvailability = dt.dim_Dateid
 WHERE j.J_3ABDBS_AUFNR = afsa.dd_SalesDocNo
 and j.J_3ABDBS_POSNR  = afsa.dd_SalesItemNo
 and j.J_3ABDBS_ETENR = afsa.dd_ScheduleNo
 and j.J_3ABDBS_J_3ASTAT = afsa.dd_AfsMrpStatus
 AND j.J_3ABDBS_J_3ABSKZ = afsa.dd_AfsStockType
 and afsa.dim_plantid = pl.dim_plantid
 AND dt.companycode = pl.companycode
 AND dt.datevalue = j.J_3ABDBS_MBDAT
 AND j.J_3ABDBS_MBDAT IS NOT NULL;

UPDATE fact_afsallocation_temp afsa
 SET afsa.Dim_DateIdDemandMatAvailability = 1
 WHERE afsa.Dim_DateIdDemandMatAvailability IS  NULL;

 UPDATE fact_afsallocation_temp afsa
 FROM dim_allocationreleaserule arr, knvv k, dim_customer cm,dim_distributionchannel dcm,dim_salesdivision sd
 SET afsa.Dim_AfsReleaseRuleId = arr.dim_allocationreleaseruleid
 WHERE afsa.Dim_CustomerIdsoldto = cm.dim_CustomerId
 AND afsa.dim_Distributionchannelid = dcm.dim_Distributionchannelid
 AND afsa.dim_SalesDivisionId = sd.dim_Salesdivisionid
 AND k.KNVV_KUNNR =   cm.customernumber
 AND k.KNVV_VTWEG = dcm.Distributionchannelcode
 AND k.KNVV_J_3ARESGY = arr.ReleaseRule
 AND k.KNVV_J_3ARESGY IS NOT NULL;

UPDATE fact_afsallocation_temp afsa
 SET afsa.Dim_AfsReleaseRuleId = 1
 WHERE afsa.Dim_AfsReleaseRuleId IS  NULL;

UPDATE fact_afsallocation_temp afsa
 FROM J_3ARESH j 
 SET afsa.dd_ExpSupplyPONumber = ifnull(J_3ARESH_J_3ABSNR,'Not Set')
 WHERE j.J_3ARESH_AUFNR = afsa.dd_SalesDocNo
 and j.J_3ARESH_POSNR  = afsa.dd_SalesItemNo
 and j.J_3ARESH_ETENR = afsa.dd_ScheduleNo
 and j.J_3ARESH_J_3ASTAT = afsa.dd_AfsReshMrpStatus
 AND j.J_3ARESH_J_3ABSKZ = afsa.dd_ArunStockType
 AND j.J_3ARESH_J_3ABSNR IS NOT NULL;

UPDATE fact_afsallocation_temp afsa
 SET afsa.dd_ExpSupplyPONumber = 'Not Set'
 WHERE afsa.dd_ExpSupplyPONumber IS  NULL;

UPDATE fact_afsallocation_temp afsa
 FROM J_3ABDBS j 
 SET afsa.dd_SupplyPONumber = ifnull(J_3ABDBS_J_3ABSNR,'Not Set')
 WHERE j.J_3ABDBS_AUFNR = afsa.dd_SalesDocNo
 and j.J_3ABDBS_POSNR  = afsa.dd_SalesItemNo
 and j.J_3ABDBS_ETENR = afsa.dd_ScheduleNo
 and j.J_3ABDBS_J_3ASTAT = afsa.dd_AfsMrpStatus
 AND j.J_3ABDBS_J_3ABSKZ = afsa.dd_AfsStockType
 AND j.J_3ABDBS_J_3ABSNR IS NOT NULL;

UPDATE fact_afsallocation_temp afsa
 SET afsa.dd_SupplyPONumber = 'Not Set'
 WHERE afsa.dd_SupplyPONumber IS  NULL;

call vectorwise(combine 'fact_afsallocation_temp');
 
 DROP TABLE IF EXISTS tmp_AfsRescheduleAllocation;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE fact_afsallocation_temp afsa
SET dd_ProjectedCategory = 'Not Set',
Dim_DateIdProjectedDate = 1,
ct_ProjectedQty = 0; 

UPDATE fact_afsallocation_temp afsa
SET Dim_DateIdProjectedDate = Dim_DateidActualGI_Original
WHERE Dim_DateidActualGI_Original > 1;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE  fact_afsallocation_temp
SET Dim_DateIdProjectedDate = Dim_DateidPlannedGoodsIssue
WHERE Dim_DateidActualGI_Original = 1
AND dd_SalesDlvrDocNo <> 'Not Set'
AND Dim_DateidPlannedGoodsIssue > 1;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdAfsReqDelivery
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND afsa.ct_CalculatedOpenQty > 0
AND (afsa.dd_AfsStockType IN ('C') OR afsa.dd_ArunStockType IN ('C'))
AND cg.DocumentCategory NOT in ('G','g','B','b')
AND afsa.dd_AllocationStatus in ('Allocated Order')
AND ansidate(dt.DateValue) >= current_Date;

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdAfsReqDelivery
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND (afsa.dd_AfsStockType IN ('C') OR afsa.dd_ArunStockType IN ('C'))
AND afsa.ct_CalculatedOpenQty > 0
AND cg.DocumentCategory in ('G','g','B','b')
AND afsa.dd_AllocationStatus in ('Allocated Contract')
AND ansidate(dt.DateValue) >= current_Date;

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt2
SET Dim_DateIdProjectedDate = dt2.dim_DateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND (afsa.dd_AfsStockType IN ('C') OR afsa.dd_ArunStockType IN ('C'))
AND afsa.ct_CalculatedOpenQty > 0
AND cg.DocumentCategory in ('G','g','B','b')
AND afsa.dd_AllocationStatus in ('Allocated Contract')
AND ansidate(dt2.datevalue) = current_date
and dt2.companycode = dt.companycode
AND ansidate(dt.DateValue) < ansidate(dt2.datevalue);

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt2
SET Dim_DateIdProjectedDate = dt2.dim_DateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND afsa.ct_CalculatedOpenQty > 0
AND (afsa.dd_AfsStockType IN ('C') OR afsa.dd_ArunStockType IN ('C'))
AND cg.DocumentCategory NOT in ('G','g','B','b')
AND afsa.dd_AllocationStatus in ('Allocated Order')
AND ansidate(dt2.datevalue) = current_date
and dt2.companycode = dt.companycode
AND ansidate(dt.DateValue) < ansidate(dt2.datevalue);

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt2
SET Dim_DateIdProjectedDate = dt2.dim_DateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
and dt2.dim_dateid = afsa.Dim_DateIdPOSchedLineAvailability
AND afsa.ct_CalculatedOpenQty > 0
AND (afsa.dd_AfsStockType IN ('L','B') OR afsa.dd_ArunStockType IN ('L','B'))
AND afsa.dd_AllocationStatus in ('Allocated Order')
AND cg.DocumentCategory NOT IN ('G','g','B','b')
AND (dt2.DateValue >= dt.DateValue AND dt2.DateValue >= current_date);

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt2
SET Dim_DateIdProjectedDate = dt2.dim_DateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
and dt2.dim_dateid = afsa.Dim_DateIdPOSchedLineAvailability
AND afsa.ct_CalculatedOpenQty > 0
AND (afsa.dd_AfsStockType IN ('L','B') OR afsa.dd_ArunStockType IN ('L','B'))
AND afsa.dd_AllocationStatus in ('Allocated Contract')
AND cg.DocumentCategory in ('G','g','B','b')
AND (dt2.DateValue >= dt.DateValue AND dt2.DateValue >= current_date);

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt2
SET Dim_DateIdProjectedDate = dt.dim_DateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
and dt2.dim_dateid = afsa.Dim_DateIdPOSchedLineAvailability
AND afsa.ct_CalculatedOpenQty > 0
AND (afsa.dd_AfsStockType IN ('L','B') OR afsa.dd_ArunStockType IN ('L','B'))
AND afsa.dd_AllocationStatus in ('Allocated Contract')
AND cg.DocumentCategory in ('G','g','B','b')
AND (dt.DateValue >= dt2.DateValue AND dt.DateValue >= current_date);

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt2
SET Dim_DateIdProjectedDate = dt.dim_DateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
and dt2.dim_dateid = afsa.Dim_DateIdPOSchedLineAvailability
AND afsa.ct_CalculatedOpenQty > 0
AND (afsa.dd_AfsStockType IN ('L','B') OR afsa.dd_ArunStockType IN ('L','B'))
AND afsa.dd_AllocationStatus in ('Allocated Order')
AND cg.DocumentCategory not in ('G','g','B','b')
AND (dt.DateValue >= dt2.DateValue AND dt.DateValue >= current_date);

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt2,dim_Date dt1
SET Dim_DateIdProjectedDate = dt1.dim_DateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
and dt2.dim_dateid = afsa.Dim_DateIdPOSchedLineAvailability
AND afsa.ct_CalculatedOpenQty > 0
AND (afsa.dd_AfsStockType IN ('L','B') OR afsa.dd_ArunStockType IN ('L','B'))
AND afsa.dd_AllocationStatus in ('Allocated Contract')
AND cg.DocumentCategory in ('G','g','B','b')
AND dt1.Datevalue = current_Date and dt1.companycode = dt.companycode
AND (dt1.DateValue >= dt2.DateValue AND dt1.DateValue >= dt.datevalue);

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt2,dim_Date dt1
SET Dim_DateIdProjectedDate = dt1.dim_DateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
and dt2.dim_dateid = afsa.Dim_DateIdPOSchedLineAvailability
AND afsa.ct_CalculatedOpenQty > 0
AND (afsa.dd_AfsStockType IN ('L','B') OR afsa.dd_ArunStockType IN ('L','B'))
AND afsa.dd_AllocationStatus in ('Allocated Order')
AND cg.DocumentCategory not in ('G','g','B','b')
AND dt1.Datevalue = current_Date and dt1.companycode = dt.companycode
AND (dt1.DateValue >= dt2.DateValue AND dt1.DateValue >= dt.datevalue);

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdAfsReqDelivery
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND afsa.ct_CalculatedOpenQty > 0
AND cg.DocumentCategory NOT in ('G','g','B','b')
AND afsa.dd_AllocationStatus in ('Unallocated Order')
AND dt.DateValue >= current_Date;

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt1
SET Dim_DateIdProjectedDate = dt1.dim_dateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND afsa.ct_CalculatedOpenQty > 0
AND cg.DocumentCategory NOT in ('G','g','B','b')
AND afsa.dd_AllocationStatus in ('Unallocated Order')
and dt1.datevalue = current_Date and dt1.companycode = dt.companycode
AND dt.DateValue < current_Date;

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdAfsReqDelivery
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND cg.DocumentCategory   in ('G','g','B','b')
AND afsa.ct_CalculatedOpenQty > 0
AND afsa.dd_AllocationStatus in ('Unallocated Contract')
AND dt.DateValue >= current_Date;

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg, dim_Date dt1
SET Dim_DateIdProjectedDate = dt1.dim_dateId
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND afsa.ct_CalculatedOpenQty > 0
AND cg.DocumentCategory   in ('G','g','B','b')
AND afsa.dd_AllocationStatus in ('Unallocated Contract')
and dt1.datevalue = current_Date and dt1.companycode = dt.companycode
AND dt.DateValue < current_Date;

call vectorwise(combine 'fact_afsallocation_temp');


UPDATE  fact_afsallocation_temp afsa
  FROM dim_Date dt, dim_date dt2
SET afsa.Dim_DateIdProjectedDate = afsa.Dim_DateIdAfsReqDelivery
where afsa.Dim_AfsRejectionReasonId <> 1
and afsa.Dim_DateIdRejection <> 1
AND afsa.Dim_DateIdAfsReqDelivery <> 1
AND afsa.Dim_DateIdRejection = dt.dim_Dateid
AND afsa.Dim_DateIdAfsReqDelivery = dt2.dim_dateid
AND dt.datevalue < dt2.datevalue;

UPDATE  fact_afsallocation_temp afsa
  FROM dim_Date dt, dim_date dt2
SET afsa.Dim_DateIdProjectedDate = afsa.Dim_DateIdRejection
where afsa.Dim_AfsRejectionReasonId <> 1
and afsa.Dim_DateIdRejection <> 1
AND afsa.Dim_DateIdAfsReqDelivery <> 1
AND afsa.Dim_DateIdRejection = dt.dim_Dateid
AND afsa.Dim_DateIdAfsReqDelivery = dt2.dim_dateid
AND dt.datevalue >= dt2.datevalue;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE fact_afsallocation_temp afsa
SET dd_ProjectedCategory = 'REJECTED',
ct_ProjectedQty = afsa.ct_ScheduleQtySalesUnit 
WHERE afsa.Dim_AfsRejectionReasonId <> 1;

UPDATE fact_afsallocation_temp afsa
SET dd_ProjectedCategory = 'SHIPPED',
ct_ProjectedQty = afsa.ct_AFSBilledQty
WHERE Dim_DateidActualGI_Original > 1 
AND afsa.Dim_AfsRejectionReasonId = 1
AND ct_AFSBilledQty > 0
AND afsa.dd_AfsMrpStatus = 'Not Set'
AND afsa.dd_AfsReshMrpStatus = 'Not Set';

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc
SET dd_ProjectedCategory = 'QUOTATION' ,
ct_ProjectedQty = (afsa.ct_ConfirmedQty - afsa.ct_AfsTotalDrawn)
WHERE dc.dim_Documentcategoryid = afsa.dim_Documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND dc.DocumentCategory IN ('B','b');

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc
SET dd_ProjectedCategory = 'ALLOCATED CONTRACT' ,
ct_ProjectedQty = (CASE WHEN afsa.ct_ExpectedReservedQty = 0 THEN afsa.ct_ReservedQty ELSE afsa.ct_ExpectedReservedQty END)
WHERE dc.dim_Documentcategoryid = afsa.dim_Documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND dc.DocumentCategory IN ('G','g')
AND afsa.ct_AfsOpenQty > 0
AND (afsa.dd_AfsReshMrpStatus = 'R' OR afsa.dd_AfsMrpStatus = 'R');

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc
SET dd_ProjectedCategory = 'UNALLOCATED CONTRACT',
ct_ProjectedQty = (afsa.ct_AfsOpenQty - afsa.ct_AfsAllocatedQty)
WHERE dc.dim_Documentcategoryid = afsa.dim_Documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND dc.DocumentCategory IN ('G','g')
AND afsa.dd_AfsReshMrpStatus = 'Not Set'
AND afsa.dd_AfsMrpStatus = 'Not Set'
AND afsa.ct_AfsOpenQty > 0;

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc
SET dd_ProjectedCategory = 'UNALLOCATED ORDER' ,
ct_ProjectedQty = (afsa.ct_AfsOpenQty - afsa.ct_AfsAllocatedQty)
WHERE dc.dim_Documentcategoryid = afsa.dim_Documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND afsa.dd_AfsReshMrpStatus = 'Not Set'
AND afsa.dd_AfsMrpStatus = 'Not Set'
AND afsa.Dim_DateidActualGI_Original = 1
AND (afsa.ct_AfsOpenQty-afsa.ct_QtyDelivered) > 0
AND dc.DocumentCategory NOT IN ('B','b','G','g');

UPDATE fact_afsallocation_temp afsa
FROM  dim_Documentcategory dc
SET dd_ProjectedCategory = 'DELIVERY BLOCK',
ct_ProjectedQty = (CASE WHEN afsa.ct_ExpectedFixedQty = 0 THEN afsa.ct_FixedQty ELSE afsa.ct_ExpectedFixedQty END)
WHERE dc.dim_documentcategoryid = afsa.dim_documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND dc.DocumentCategory NOT IN ('B','b','G','g')
AND (afsa.dd_AfsReshMrpStatus = 'F' OR afsa.dd_AfsMrpStatus = 'F')
AND afsa.Dim_DeliveryBlockid > 1;

UPDATE fact_afsallocation_temp afsa
FROM  dim_Documentcategory dc,
      dim_overallstatusforcreditcheck ocs
SET dd_ProjectedCategory = 'CREDIT BLOCK',
ct_ProjectedQty = (CASE WHEN afsa.ct_ExpectedFixedQty = 0 THEN afsa.ct_FixedQty ELSE afsa.ct_ExpectedFixedQty END)
WHERE dc.dim_documentcategoryid = afsa.dim_documentcategoryid
AND dc.DocumentCategory NOT IN ('B','b','G','g')
AND ocs.dim_overallstatusforcreditcheckID = 
                afsa.Dim_OverallStatusCreditCheckId 
AND afsa.Dim_AfsRejectionReasonId = 1
AND  ( afsa.dd_AfsMrpStatus = 'F' OR afsa.dd_AfsReshMrpStatus = 'F')
AND afsa.Dim_DeliveryBlockid = 1
AND ocs.OverallStatusforCreditCheck IN ('B');

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc,
      dim_overallstatusforcreditcheck ocs
SET dd_ProjectedCategory = 'PROJECTED TO SHIP' ,
ct_ProjectedQty = (CASE WHEN afsa.ct_ExpectedFixedQty = 0 THEN afsa.ct_FixedQty ELSE afsa.ct_ExpectedFixedQty END)
WHERE dc.dim_documentcategoryid = afsa.dim_documentcategoryid
AND dc.DocumentCategory NOT IN ('B','b','G','g')
AND ocs.dim_overallstatusforcreditcheckID = 
                afsa.Dim_OverallStatusCreditCheckId 
AND afsa.Dim_AfsRejectionReasonId = 1
AND (afsa.dd_AfsMrpStatus = 'F' OR afsa.dd_AfsReshMrpStatus = 'F')
AND afsa.Dim_DeliveryBlockid = 1
AND ocs.OverallStatusforCreditCheck NOT IN ('B');

UPDATE fact_afsallocation_temp afsa
SET dd_ProjectedCategory = 'ON DELIVERY',
ct_ProjectedQty = afsa.ct_AfsOnDeliveryQty
WHERE afsa.dd_AfsMrpStatus = 'D'
AND afsa.dd_AfsReshMrpStatus = 'Not Set'
AND afsa.ct_AfsOnDeliveryQty > 0;

UPDATE fact_afsallocation_temp afsa
SET ct_AllocatedPercent = 100*(afsa.ct_ReservedQty+afsa.ct_FixedQty)/(CASE WHEN ct_CalculatedOpenQty <> 0 THEN ct_CalculatedOpenQty ELSE 1 END)
WHERE afsa.ct_CalculatedOpenQty <> 0;

/* Andra 23 Jan : Logic for dd_clearedblockedsts */
update fact_afsallocation_temp 
set dd_clearedblockedsts = 'Cleared'
where (dd_clearedblockedsts <>'Cleared' OR dd_clearedblockedsts IS NULL);

UPDATE fact_afsallocation_temp afsa
FROM dim_salesorderheaderstatus sohs,dim_salesorderitemstatus sois
SET afsa.dd_clearedblockedsts = 'Blocked'
WHERE 
(sohs.GeneralIncompleteStatusItem in ('Not yet processed','Partially processed') 
OR sohs.OverallBlkdStatus in ('Partially processed','Completely processed') 
OR sois.GeneralIncompletionStatus in ('Not yet processed','Partially processed'))
AND afsa.dim_salesorderheaderstatusid = sohs.dim_salesorderheaderstatusid
AND afsa.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid; 

UPDATE fact_afsallocation_temp afsa
FROM   dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_customer cm,
       dim_distributionchannel dc
SET afsa.dim_Customermastersalesid = cms.dim_Customermastersalesid
 WHERE afsa.dim_salesorgid = sorg.dim_salesorgid
       AND cm.dim_customerid = afsa.dim_customeridsoldto
       and dc.dim_distributionchannelid = afsa.dim_distributionchannelid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND cm.customernumber = cms.CustomerNumber
       and dc.distributionchannelcode = cms.distributionchannel;

update fact_afsallocation_temp set dim_Customermastersalesid = 1 where dim_Customermastersalesid is NULL;

/* Andra 29 Apr : Logic for Logic for Cancel,CDD,RDD Dates versus Material Availability Date */

UPDATE fact_afsallocation_temp f
FROM dim_date soicd, dim_date scladt
SET f.dd_CancelvsMatAvailBucket = 
    (CASE
	     WHEN (soicd.DateValue - scladt.DateValue) = 0
		 THEN 'On Cancel'
		 WHEN (soicd.DateValue - scladt.DateValue) > 0
		 THEN 'Before Cancel'
		 WHEN (soicd.DateValue - scladt.DateValue) < 0
		 THEN 'Past Cancel'
		 ELSE 'Not Set'
    END)
WHERE f.Dim_DateIdPOSchedLineAvailability = scladt.dim_dateid
  AND f.Dim_DateIdAfsCancelDate = soicd.dim_dateid
  AND f.Dim_DateIdPOSchedLineAvailability > 1
  AND f.Dim_DateIdAfsCancelDate > 1;
  
UPDATE fact_afsallocation_temp f
FROM dim_date pdd, dim_date scladt
SET f.dd_CDDvsMatAvailBucket = 
    (CASE
	     WHEN (pdd.DateValue - scladt.DateValue) = 0
		 THEN 'On CDD'
		 WHEN (pdd.DateValue - scladt.DateValue) > 0
		 THEN 'Before CDD'
		 WHEN (pdd.DateValue - scladt.DateValue) < 0
		 THEN 'Past CDD'
		 ELSE 'Not Set'
    END)
WHERE f.Dim_DateIdPOSchedLineAvailability = scladt.dim_dateid
  AND f.dim_dateidscheddelivery = pdd.dim_dateid
  AND f.Dim_DateIdPOSchedLineAvailability > 1
  AND f.dim_dateidscheddelivery > 1;
  
UPDATE fact_afsallocation_temp f
FROM dim_date ardd, dim_date scladt
SET f.dd_RDDvsMatAvailBucket = 
    (CASE
	     WHEN (ardd.DateValue - scladt.DateValue) = 0
		 THEN 'On RDD'
		 WHEN (ardd.DateValue - scladt.DateValue) > 0
		 THEN 'Before RDD'
		 WHEN (ardd.DateValue - scladt.DateValue) < 0
		 THEN 'Past RDD'
		 ELSE 'Not Set'
    END)
WHERE f.Dim_DateIdPOSchedLineAvailability = scladt.dim_dateid
  AND f.dim_dateidafsreqdelivery = ardd.dim_dateid
  AND f.Dim_DateIdPOSchedLineAvailability > 1
  AND f.dim_dateidafsreqdelivery > 1;
  
UPDATE fact_afsallocation_temp f
SET f.dd_CancelvsMatAvailBucket = 'Not Set'
WHERE f.dd_CancelvsMatAvailBucket is null;

UPDATE fact_afsallocation_temp f
SET f.dd_CDDvsMatAvailBucket = 'Not Set'
WHERE f.dd_CDDvsMatAvailBucket is null;

UPDATE fact_afsallocation_temp f
SET f.dd_RDDvsMatAvailBucket = 'Not Set'
WHERE f.dd_RDDvsMatAvailBucket is null;

/* Andra 29 Apr : End of logic for Cancel,CDD,RDD Dates versus Material Availability Date */

/* Andra 1st of May Update dd_AFSExpectStockCat */

UPDATE fact_afsallocation_temp afsa
FROM     J_3ABDBS j
SET afsa.dd_AFSExpectStockCat = ifnull(j.J_3ABDBS_J_4KSCAT,'Not Set')
WHERE afsa.dd_SalesDocNo = j.J_3ABDBS_AUFNR
  AND afsa.dd_salesItemNo = j.J_3ABDBS_POSNR
  AND afsa.dd_ScheduleNo = j.J_3ABDBS_ETENR
  AND afsa.dd_AFSExpectStockCat <> ifnull(j.J_3ABDBS_J_4KSCAT,'Not Set');
  
UPDATE fact_afsallocation_temp afsa
FROM     J_3ARESH j
SET afsa.dd_AFSExpectStockCat = ifnull(j.J_3ARESH_J_4KSCAT,'Not Set')
WHERE afsa.dd_SalesDocNo = j.J_3ARESH_AUFNR
  AND afsa.dd_salesItemNo = j.J_3ARESH_POSNR
  AND afsa.dd_ScheduleNo = j.J_3ARESH_ETENR
  AND 
  (NOT EXISTS ( SELECT 1 FROM J_3ABDBS j
		        WHERE j.J_3ABDBS_AUFNR = afsa.dd_SalesDocNo
                  AND j.J_3ABDBS_POSNR  = afsa.dd_SalesItemNo
                  AND j.J_3ABDBS_ETENR = afsa.dd_ScheduleNo
                )
OR afsa.dd_AFSExpectStockCat = 'Not Set');
  
UPDATE fact_afsallocation_temp afsa
SET afsa.dd_AFSExpectStockCat = 'Not Set'
WHERE afsa.dd_AFSExpectStockCat is null;
 
/* Andra End of Update dd_AFSExpectStockCat */

/* Andra 1st of May Update Dim_MaterialPriceGroup2Id, Dim_UnitOfMeasureId from fact_salesorder */

UPDATE  fact_afsallocation_temp afsa
FROM  fact_salesorder so
SET afsa.Dim_MaterialPriceGroup2Id = so.Dim_MaterialPriceGroup2Id
Where afsa.dd_SalesDocNo = so.dd_SalesDocNo
  AND afsa.dd_salesItemNo = so.dd_SalesItemNo
  AND afsa.dd_ScheduleNo = so.dd_ScheduleNo
  AND afsa.Dim_MaterialPriceGroup2Id <> ifnull( so.Dim_MaterialPriceGroup2Id,1);

UPDATE fact_afsallocation_temp afsa
SET afsa.Dim_MaterialPriceGroup2Id = 1
WHERE afsa.Dim_MaterialPriceGroup2Id is null;

UPDATE  fact_afsallocation_temp afsa
FROM  fact_salesorder so
SET afsa.Dim_UnitOfMeasureId = so.Dim_BaseUoMid
Where afsa.dd_SalesDocNo = so.dd_SalesDocNo
  AND afsa.dd_salesItemNo = so.dd_SalesItemNo
  AND afsa.dd_ScheduleNo = so.dd_ScheduleNo
  AND afsa.Dim_UnitOfMeasureId <> ifnull( so.Dim_BaseUoMid,1);

UPDATE fact_afsallocation_temp afsa
SET afsa.Dim_UnitOfMeasureId = 1
WHERE afsa.Dim_UnitOfMeasureId is null;

/* Andra 1st of May Update Dim_DeliveryPriorityId */

UPDATE  fact_afsallocation_temp afsa
FROM  fact_salesorderdelivery sod
SET afsa.Dim_DeliveryPriorityId = sod.Dim_DeliveryPriorityId
Where afsa.dd_SalesDocNo = sod.dd_SalesDocNo
  AND afsa.dd_salesItemNo = sod.dd_SalesItemNo
  AND afsa.dd_ScheduleNo = sod.dd_ScheduleNo
  AND afsa.Dim_DeliveryPriorityId <> ifnull( sod.Dim_DeliveryPriorityId,1);

UPDATE fact_afsallocation_temp afsa
SET afsa.Dim_DeliveryPriorityId = 1
WHERE afsa.Dim_DeliveryPriorityId is null;

/* Andra: Logic for the Allocated Percent Bucket */

UPDATE fact_afsallocation_temp f
SET f.dd_allocatedBucket = 
    (CASE
	     WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) = 0)
		 THEN '0%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) > 0 
		       AND 
			   ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 10.49)
		 THEN '1-10%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 10.50 
		       AND 
			   ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 20.49)
		 THEN '11-20%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 20.50 
		       AND 
		        ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 30.49)
		 THEN '21-30%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 30.50 
		       AND 
			   ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 40.49)
		 THEN '31-40%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 40.50 
		       AND 
		       ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 50.49)
		 THEN '41-50%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 50.50 
		      AND 
			  ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 60.49)
		 THEN '51-60%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 60.50 
		      AND 
			  ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 70.49)
		 THEN '61-70%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 70.50 
		      AND 
			  ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 80.49)
		 THEN '71-80%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 80.50 
		      AND 
			  ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) <= 90.49)
		 THEN '81-90%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) >= 90.50 
		       AND 
		       ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) < 100)
		 THEN '91-99%'
		 WHEN (ROUND(ifnull(100*(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN (f.ct_CalculatedOpenQty) <> 0 THEN (f.ct_CalculatedOpenQty) ELSE 1 END),0),2) = 100)
		 THEN '100%'
		 ELSE 'Not Set'
    END);

/* Andra: Logic for the Allocated Percent Bucket at the Customer PO Level*/

DROP TABLE IF EXISTS tmp_AllocatedPercentBucket;

CREATE TABLE tmp_AllocatedPercentBucket AS
select f.dd_CustomerPONo,
ROUND(ifnull(100*SUM(f.ct_ReservedQty+f.ct_FixedQty)/(CASE WHEN SUM(f.ct_CalculatedOpenQty) <> 0 THEN SUM(f.ct_CalculatedOpenQty) ELSE 1 END),0),2) allocated_percent
from fact_afsallocation_temp f
GROUP BY f.dd_CustomerPONo;

UPDATE fact_afsallocation_temp f
SET f.dd_CustPOallocatedBucket = 'Not Set'
where f.dd_CustPOallocatedBucket <> 'Not Set';

UPDATE fact_afsallocation_temp f
FROM tmp_AllocatedPercentBucket t
SET f.dd_CustPOallocatedBucket = 
    (CASE
	     WHEN (t.allocated_percent = 0)
		 THEN '0%'
		 WHEN (t.allocated_percent > 0 AND t.allocated_percent <= 10.49)
		 THEN '1-10%'
		 WHEN (t.allocated_percent >= 10.50 AND t.allocated_percent <= 20.49)
		 THEN '11-20%'
		 WHEN (t.allocated_percent >= 20.50 AND t.allocated_percent <= 30.49)
		 THEN '21-30%'
		 WHEN (t.allocated_percent >= 30.50 AND t.allocated_percent <= 40.49)
		 THEN '31-40%'
		 WHEN (t.allocated_percent >= 40.50 AND t.allocated_percent <= 50.49)
		 THEN '41-50%'
		 WHEN (t.allocated_percent >= 50.50 AND t.allocated_percent <= 60.49)
		 THEN '51-60%'
		 WHEN (t.allocated_percent >= 60.50 AND t.allocated_percent <= 70.49)
		 THEN '61-70%'
		 WHEN (t.allocated_percent >= 70.50 AND t.allocated_percent <= 80.49)
		 THEN '71-80%'
		 WHEN (t.allocated_percent >= 80.50 AND t.allocated_percent <= 90.49)
		 THEN '81-90%'
		 WHEN (t.allocated_percent >= 90.50 AND t.allocated_percent < 100)
		 THEN '91-99%'
		 WHEN (t.allocated_percent = 100)
		 THEN '100%'
		 ELSE 'Not Set'
    END)
WHERE f.dd_CustomerPONo = t.dd_CustomerPONo;
  
DROP TABLE IF EXISTS tmp_AllocatedPercentBucket;

call vectorwise(combine 'fact_afsallocation_temp');
call vectorwise(combine 'fact_afsallocation - fact_afsallocation');
call vectorwise(combine 'fact_afsallocation');
call vectorwise(combine 'fact_afsallocation + fact_afsallocation_temp'); 

select 'Process Complete';
select current_time;

select 'END OF PROC bi_populate_afs_allocation_fact',TIMESTAMP(LOCAL_TIMESTAMP);