CREATE TABLE fact_planorder_bkp_bfr_curr_chgs_30sep
AS
SELECT * FROM fact_planorder;

DROP TABLE IF EXISTS fact_planorder;

create table fact_planorder(
	fact_planorderid integer not null,
	dim_companyid integer not null,
	dim_partid integer not null default '0',
	dim_mpnid integer default NULL,
	dim_storagelocationid integer not null,
	dim_plantid integer not null,
	dim_unitofmeasureid integer not null,
	dim_purchaseorgid integer not null,
	dim_vendorid integer not null default '0',
	dim_fixedvendorid integer default NULL,
	dim_specialprocurementid integer default NULL,
	dim_consumptiontypeid integer default NULL,
	dim_accountcategoryid integer default NULL,
	dim_specialstockid integer default NULL,
	ct_qtytotal decimal(18,4) not null default '0.0000',
	ct_completed integer not null default '0',
	amt_extendedprice decimal(18,5) not null default '0.00000',		--Amt column
	dd_planorderno varchar(50) default NULL collate ucs_basic,
	ct_qtyreduced decimal(18,4) default NULL,
	dirtyrow integer default NULL,
	dim_bomstatusid integer default NULL,
	dim_bomusageid integer default NULL,
	dim_objecttypeid integer default NULL,
	dim_productionschedulerid integer default NULL,
	dim_schedulingerrorid integer default NULL,
	dim_tasklisttypeid integer default NULL,
	dim_ordertypeid integer default NULL,
	dim_dateidstart integer default NULL,
	dim_dateidfinish integer default NULL,
	dim_dateidopening integer default NULL,
	dd_bomexplosionno varchar(8) default NULL collate ucs_basic,
	dim_actionstateid integer default NULL,
	dim_dateidconversiondate integer default NULL,
	dim_planorderstatusid integer default NULL,
	dim_planordermiscid integer default NULL,
	dim_dateidproductionstart integer default NULL,
	dim_dateidproductionfinish integer default NULL,
	dim_dateidexplosion integer default NULL,
	dim_dateidaction integer default NULL,
	dim_scheduletypeid integer default NULL,
	dim_availabilityconfirmationid integer default NULL,
	dd_productionorderno varchar(10) default NULL collate ucs_basic,
	dd_salesorderitemno integer default NULL,
	dd_salesorderscheduleno integer default NULL,
	dd_salesorderno varchar(10) default NULL collate ucs_basic,
	ct_grprocessingtime integer default NULL,
	ct_qtyissued decimal(18,4) default NULL,
	ct_qtycommitted decimal(18,4) default NULL,
	dim_currencyid integer default NULL,
	dim_customergroup1id integer default '1',
	dim_customerid integer default '1',
	dim_documentcategoryid integer default '1',
	dim_salesdocumenttypeid integer default '1',
	amt_extendedprice_gbl decimal(18,5) default '0.00000',--Amt column
	dd_sequenceno varchar(14) default 'Not Set' collate ucs_basic,
	dd_plannscenario varchar(10) default 'Not Set' collate ucs_basic,
	amt_stdunitprice decimal(18,5) not null default '0.00000',--Amt column
	dd_documentno varchar(50) default NULL collate ucs_basic,
	dim_dateidmrpsched integer not null default '1',
	dim_procurementid integer not null default '1',
	dim_dateidweekending integer not null default '1',
	dim_dateidmrpstartdate integer not null default '1',
	dim_mrpcontrollerid integer not null default '1',
	dd_agreementno varchar(10) default NULL collate ucs_basic,
	dd_agreementitemno integer default NULL,
	dim_dateidmrpdock integer not null default '1',
	dim_dateidscheduledstart integer not null default '1',
	dim_dateidscheduledfinish integer not null default '1',
	dim_profitcenterid smallint,
	amt_exchangerate decimal(18,4) default 1,		--New column
	amt_exchangerate_GBL decimal(18,4) default 1,		--New column
	Dim_Currencyid_TRA smallint not null default 1,		--New column
	Dim_Currencyid_GBL smallint not null default 1		--New column	
)
with structure=vectorwise,
location = (ii_database)
\p\g
