 /**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Lokesh */
/*   Created On     : 19 Aug 2013 */
/*   Description    : Stored Proc process_S031 migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   19 Aug 2013      Lokesh    1.0               Existing code migrated to Vectorwise.                                       */
/******************************************************************************************************************/

  drop table if exists tmp_S031_0;
  create table tmp_S031_0 as
  select *
  from S031
  where wgvbr <> 0;

  delete from S031
  where exists (select 1 from tmp_S031_0 b 
		where S031.matnr = b.matnr and S031.werks = b.werks and S031.spmon = b.spmon)
	and wgvbr = 0;

  update S031
  set SPMON_YEAR = substring(SPMON,1,4), SPMON_MONTH = substring(SPMON,5,2)
  where SPMON <> 0;

        update S031 x
        FROM dim_plant pl, tmp_funct_fiscal_year z
        set SPMON_YEAR = Year(cast(substr(z.pReturn, 12, 10) AS date)   - int(floor(((cast(substr(z.pReturn, 12, 10) AS date)   - cast(substr(z.pReturn, 1, 10) AS date)))/2))),
                SPMON_MONTH = Month(cast(substr(z.pReturn, 12, 10) AS date)  - int(floor(((cast(substr(z.pReturn, 12, 10) AS date)   - cast(substr(z.pReturn, 1, 10) AS date)))/2)))
        where x.SPMON = 0 and x.SPBUP <> 0 and x.WERKS = pl.PlantCode
        and z.pCompanyCode = pl.CompanyCode
        and z.FiscalYear =substring(SPBUP,1,4)
        and z.Period = substring(SPBUP,5,2)
        and z.fact_script_name = 'process_S031';

  update S031
  from dim_plant pl, dim_date dt
  set SPMON_YEAR = dt.CalendarYear,
      SPMON_MONTH = dt.CalendarMonthNumber
  where SPMON = 0 and SPBUP = 0 and S031_SPWOC <> 0
      and WERKS = pl.PlantCode and dt.CalendarWeekID = S031_SPWOC and dt.WeekDayNumber = 4 and dt.CompanyCode = pl.CompanyCode;

  update S031
  set SPMON_YEAR = Year(S031_SPTAG), SPMON_MONTH = Month(S031_SPTAG)
  where SPMON = 0 and SPBUP = 0 and S031_SPWOC = 0 and S031_SPTAG is not null;

  delete from S031 where SPMON_YEAR is null and SPMON_MONTH is null;

