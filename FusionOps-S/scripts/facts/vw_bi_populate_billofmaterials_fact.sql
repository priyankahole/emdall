/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 1 Jun 2013 */
/*   Description    : Stored Proc bi_populate_billofmaterials_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   17 Sep 2013     Lokesh    1.1 				 Currency and Exchange Rate changes								 */
/*   1 Jun 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_pGlobalCurrency_bom;
CREATE TABLE tmp_pGlobalCurrency_bom ( pGlobalCurrency CHAR(3) NULL);

INSERT INTO tmp_pGlobalCurrency_bom VALUES ( 'USD' );

update tmp_pGlobalCurrency_bom 
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

/* MODIFY fact_bom_tmp_populate TO TRUNCATED */

DROP TABLE IF EXISTS fact_bom_tmp_populate;
CREATE TABLE fact_bom_tmp_populate
AS
SELECT * FROM fact_bom
where 1=2;

ALTER TABLE fact_bom_tmp_populate
add  PRIMARY KEY (fact_bomid);

/* Lokesh : Not converting this update. The table is just truncated before this update. Confirm this with Rahul/Shanthi. 
	Either the truncate or the update should be removed  */
/*	
UPDATE fact_bom_tmp_populate bom, STKO hdr, STPO item, dim_bomstatus bs,dim_bomcategory bc
   SET Dim_CreatedOnDateid =
          ifnull(
             (SELECT dr.dim_dateid
                FROM dim_date dr
               WHERE dr.DateValue = STPO_ANDAT AND 'Not Set' = dr.CompanyCode),
             1),
       Dim_ChangedOnDateid =
          ifnull(
             (SELECT be.dim_dateid
                FROM dim_date be
               WHERE be.DateValue = STPO_AEDAT AND 'Not Set' = be.CompanyCode),
             1),
       Dim_ValidFromDateid =
          ifnull(
             (SELECT ls.dim_dateid
                FROM dim_date ls
               WHERE ls.DateValue = STPO_DATUV AND 'Not Set' = ls.CompanyCode),
             1),
       Dim_BOMComponentId =
          ifnull(
             (SELECT pitem.Dim_Partid
                FROM dim_part pitem
               WHERE     pitem.PartNumber = STPO_IDNRK
                     AND pitem.Plant = ifnull(STPO_PSWRK,STKO_WRKAN)
                     AND pitem.RowIsCurrent = 1),
             1),
       bom.Dim_bomstatusid = bs.Dim_bomstatusid,
       Dim_BaseUOMId =
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = STKO_BMEIN AND uom.RowIsCurrent = 1),
                 1),
       Dim_ComponentUOMId =
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = STPO_MEINS AND uom.RowIsCurrent = 1),
                 1),
       Dim_StorageLocationid =
          ifnull(
             (SELECT sl.Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE     sl.LocationCode = STPO_LGORT

                     AND sl.RowIsCurrent = 1 LIMIT 1),
             1),
       bom.Dim_IssuingPlantId =
          ifnull((SELECT dp.Dim_PlantId
                    FROM dim_plant dp
                   WHERE dp.PlantCode = STPO_PSWRK AND dp.RowIsCurrent = 1),
                 1),
       bom.Dim_PurchasingOrgId =
          ifnull(
             (SELECT porg.Dim_PurchaseOrgId
                FROM Dim_PurchaseOrg porg
               WHERE     porg.PurchaseOrgCode = STPO_EKORG

                     AND porg.RowIsCurrent = 1 LIMIT 1),
             1),
       bom.Dim_Currencyid =
          ifnull((SELECT cur.Dim_Currencyid
                    FROM Dim_Currency cur
                   WHERE cur.CurrencyCode = STPO_WAERS),
                 1),
       Dim_PurchasingGroupId =
          ifnull((SELECT pg.Dim_PurchaseGroupId
                    FROM Dim_PurchaseGroup pg
                   WHERE pg.PurchaseGroup = STPO_EKGRP AND pg.RowIsCurrent = 1),
                  1),
       bom.dd_BomItemNo = ifnull(item.stpo_posnr,'Not Set'),
       dd_BOMPredecessorNodeNo = ifnull(STPO_VGKNT, 0),
       dd_CreatedBy = ifnull(STPO_ANNAM, 'Not Set'),
       dd_ChangedBy = ifnull(STPO_AENAM, 'Not Set'),
       ct_ComponentScrapinPercent = STPO_AUSCH,
       ct_AvgMatPurityinPercent = STPO_CSSTR,
       ct_LeadTimeOffset = STPO_NLFZT,
       ct_BaseQty = STKO_BMENG,
       ct_ComponentQty = STPO_MENGE,
       amt_Price = STPO_PREIS,
       amt_PriceUnit = ifnull(STPO_PEINH,1),
       dd_PartNumber = 'Not Set',
       dd_ComponentNumber = ifnull(STPO_IDNRK, 'Not Set'),
       bom.Dim_BomItemCategoryId = ifnull(( SELECT bic.Dim_BomItemCategoryId FROM dim_bomitemcategory bic
                                           WHERE bic.ItemCategory = STPO_POSTP
                                           AND bic.RowIsCurrent = 1), 1),
       bom.Dim_VendorId = ifnull(( SELECT dv.Dim_VendorId FROM dim_Vendor dv
                                           WHERE dv.VendorNumber = STPO_LIFNR
                                           AND dv.RowIsCurrent = 1), 1),
       bom.dd_BomItemCounter = STPO_STPOZ
 WHERE     bom.dd_BomNumber = hdr.STKO_STLNR
       AND dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
       AND bc.Category = STPO_STLTY
       AND bc.RowIsCurrent = 1
       AND bom.dd_Alternative = hdr.STKO_STLAL
       AND hdr.STKO_STLNR = item.STPO_STLNR
       AND hdr.STKO_STLTY = item.STPO_STLTY
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1*/

/* Insert 1 */


delete from NUMBER_FOUNTAIN where table_name = 'fact_bom_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_bom_tmp_populate',ifnull(max(fact_bomid),0)
FROM fact_bom_tmp_populate;

	   
INSERT INTO fact_bom_tmp_populate(ct_AvgMatPurityinPercent,
                                 ct_BaseQty,
                                 ct_ComponentQty,
                                 ct_ComponentScrapinPercent,
                                 ct_LeadTimeOffset,
                                 amt_Price,
                                 amt_PriceUnit,
                                 dd_BomItemNo,
                                 dd_BOMItemNodeNo,
                                 dd_BomNumber,
                                 Dim_BOMCategoryId,
                                 dd_BOMPredecessorNodeNo,
                                 dd_ChangedBy,
                                 dd_CreatedBy,
                                 dd_PartNumber,
                                 dd_ComponentNumber,
                                 dd_Alternative,
                                 Dim_BaseUOMId,
                                 Dim_ComponentUOMId,
                                 Dim_BOMComponentId,
                                 Dim_ChangedOnDateid,
                                 Dim_CreatedOnDateid,
                                 Dim_CurrencyId,
                                 Dim_IssuingPlantId,
                                 Dim_PurchasingOrgId,
                                 Dim_StorageLocationId,
                                 Dim_BomStatusId,
                                 Dim_PurchasingGroupId,
                                 Dim_MaterialGroupId,
                                 Dim_ValidFromDateid,
                                 Dim_BomItemCategoryId,
                                 Dim_VendorId,
                                 dd_BomItemCounter,fact_bomid,
								 Dim_Currencyid_GBL,
								 amt_ExchangeRate_GBL
								 )
   SELECT STPO_CSSTR ct_AvgMatPurityinPercent,
          STKO_BMENG ct_BaseQty,
          STPO_MENGE ct_ComponentQty,
          STPO_AUSCH ct_ComponentScrapinPercent,
          STPO_NLFZT ct_LeadTimeOffset,
          STPO_PREIS amt_Price,
          ifnull(STPO_PEINH,1) amt_PriceUnit,
          ifnull(stpo_posnr,'Not Set') dd_BomItemNo,
          ifnull(STPO_STLKN, 0) dd_BOMItemNodeNo,
          STPO_STLNR dd_BomNumber,
          ifnull(bc.Dim_BomCategoryId, 1) ,
          ifnull(STPO_VGKNT, 0) dd_BOMPredecessorNodeNo,
          ifnull(STPO_AENAM, 'Not Set') dd_ChangedBy,
          ifnull(STPO_ANNAM, 'Not Set') dd_CreatedBy,
          'Not Set' dd_PartNumber,
          ifnull(STPO_IDNRK, 'Not Set') dd_ComponentNumber,
          ifnull(STKO_STLAL,'Not Set') dd_Alternative,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = STKO_BMEIN AND uom.RowIsCurrent = 1),
                 1)
             Dim_BaseUOMId,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = STPO_MEINS AND uom.RowIsCurrent = 1),
                 1)
             Dim_ComponentUOMId,
          ifnull(
             (SELECT pitem.Dim_Partid
                FROM dim_part pitem
               WHERE     pitem.PartNumber = STPO_IDNRK
                     AND pitem.Plant = ifnull(STPO_PSWRK,STKO_WRKAN)
                     AND pitem.RowIsCurrent = 1),
             1)
             Dim_BOMComponentId,
          ifnull(
             (SELECT be.dim_dateid
                FROM dim_date be
               WHERE be.DateValue = STPO_AEDAT AND 'Not Set' = be.CompanyCode),
             1)
             Dim_ChangedOnDateid,
          ifnull(
             (SELECT dr.dim_dateid
                FROM dim_date dr
               WHERE dr.DateValue = STPO_ANDAT AND 'Not Set' = dr.CompanyCode),
             1)
             Dim_CreatedOnDateid,
          ifnull((SELECT Dim_Currencyid
                    FROM dim_currency cur
                   WHERE cur.CurrencyCode = STPO_WAERS),
                 1)
             Dim_Currencyid_TRA,
          ifnull((SELECT dp.Dim_PlantId
                    FROM dim_plant dp
                   WHERE dp.PlantCode = STPO_PSWRK AND dp.RowIsCurrent = 1),
                 1)
             Dim_IssuingPlantId,
          ifnull(
             (SELECT porg.Dim_PurchaseOrgId
                FROM Dim_PurchaseOrg porg
               WHERE     porg.PurchaseOrgCode = STPO_EKORG
				AND porg.RowIsCurrent = 1 ),1)	 /*AND porg.RowIsCurrent = 1 LIMIT 1),*/
             Dim_PurchasingOrgId,
          ifnull(
             (SELECT sl.Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE     sl.LocationCode = STPO_LGORT
						AND sl.RowIsCurrent = 1 ), 1)	/*AND sl.RowIsCurrent = 1 LIMIT 1),*/
             Dim_StorageLocationid,
          bs.Dim_bomstatusid Dim_bomstatusid,
          ifnull(
             (SELECT pg.Dim_PurchaseGroupId
                FROM Dim_PurchaseGroup pg
               WHERE pg.PurchaseGroup = STPO_EKGRP AND pg.RowIsCurrent = 1),
             1)
             Dim_PurchasingGroupId,
          ifnull(
             (SELECT mg.Dim_MaterialGroupid
                FROM Dim_MaterialGroup mg
               WHERE mg.MaterialGroupCode = STPO_MATKL
                     AND mg.RowIsCurrent = 1),
             1)
             Dim_MaterialGroupid,
          ifnull(
             (SELECT ls.dim_dateid
                FROM dim_date ls
               WHERE ls.DateValue = STPO_DATUV AND 'Not Set' = ls.CompanyCode),
             1)
             Dim_ValidFromDateid,
          ifnull(( SELECT bic.Dim_BomItemCategoryId FROM dim_bomitemcategory bic
                                           WHERE bic.ItemCategory = STPO_POSTP
                                           AND bic.RowIsCurrent = 1), 1) Dim_BomItemCategoryId,
          ifnull(( SELECT dv.Dim_VendorId FROM dim_Vendor dv
                                           WHERE dv.VendorNumber = STPO_LIFNR
                                           AND dv.RowIsCurrent = 1), 1) Dim_VendorId,
          STPO_STPOZ dd_BomItemCounter,
		   (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_bom_tmp_populate') + row_number() over (),
		   
		     ifnull((SELECT Dim_Currencyid
                    FROM dim_currency cur
                   WHERE cur.CurrencyCode = pGlobalCurrency),
                 1) Dim_Currencyid_GBL,  
		   ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
              where z.pFromCurrency  = STPO_WAERS and z.fact_script_name = 'bi_populate_billofmaterials_fact'  
			  and z.pToCurrency = pGlobalCurrency AND z.pDate = ANSIDATE(LOCAL_TIMESTAMP) ),1) amt_ExchangeRate_GBL
		   
     FROM STKO hdr
          INNER JOIN STPO item
             ON hdr.STKO_STLNR = item.STPO_STLNR
                AND hdr.STKO_STLTY = item.STPO_STLTY
          INNER JOIN dim_bomstatus bs
             ON bs.BOMStatusCode = ifnull(STKO_STLST, 'Not Set')
                AND bs.RowIsCurrent = 1
          INNER JOIN dim_bomcategory bc
             ON bc.Category = STKO_STLTY AND bc.RowIsCurrent = 1,
			tmp_pGlobalCurrency_bom
    WHERE 1<> ifnull(
                 (SELECT 1
                    FROM fact_bom_tmp_populate bom
                   WHERE bom.dd_BomNumber = hdr.STKO_STLNR
                         AND dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
                         AND bc.Category = STPO_STLTY
                         AND STPO_STLTY = STKO_STLTY),0);
						 
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_bomid),0) from fact_bom_tmp_populate)
where table_name = 'fact_bom_tmp_populate';			


/* Update local curr. Tran/Global curr already populated in insert */
/* Dim_PlantID not populated above */
/* Use Dim_IssuingPlantId. Later override with STKO_WRKAN (Dim_PlantID) to get local currency where applicable*/

UPDATE fact_bom_tmp_populate bom
FROM STKO hdr,STPO item,dim_bomstatus bs,dim_bomcategory bc,dim_plant dp,dim_company c,dim_currency cur
SET 
	 Dim_Currencyid = cur.Dim_Currencyid
WHERE 
	 hdr.STKO_STLNR = item.STPO_STLNR
AND hdr.STKO_STLTY = item.STPO_STLTY
AND bs.BOMStatusCode = ifnull(STKO_STLST, 'Not Set') AND bs.RowIsCurrent = 1
AND bc.Category = STKO_STLTY AND bc.RowIsCurrent = 1
AND bom.dd_BomNumber = hdr.STKO_STLNR
AND dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
AND bc.Category = STPO_STLTY
AND STPO_STLTY = STKO_STLTY
AND bom.Dim_IssuingPlantId = dp.Dim_PlantId
AND dp.companycode = c.companycode
AND  c.currency = cur.CurrencyCode;


/* Update local exchange rate. GBL exchange rate already populated in insert */
UPDATE fact_bom_tmp_populate bom
FROM STKO hdr,STPO item,dim_bomstatus bs,dim_bomcategory bc,dim_plant dp,dim_company c,tmp_getExchangeRate1 z
SET 
	 amt_ExchangeRate = z.exchangeRate
WHERE 
	 hdr.STKO_STLNR = item.STPO_STLNR
AND hdr.STKO_STLTY = item.STPO_STLTY
AND bs.BOMStatusCode = ifnull(STKO_STLST, 'Not Set') AND bs.RowIsCurrent = 1
AND bc.Category = STKO_STLTY AND bc.RowIsCurrent = 1
AND bom.dd_BomNumber = hdr.STKO_STLNR
AND dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
AND bc.Category = STPO_STLTY
AND STPO_STLTY = STKO_STLTY
AND bom.Dim_IssuingPlantId = dp.Dim_PlantId
AND dp.companycode = c.companycode
AND z.pFromCurrency  = STPO_WAERS AND z.fact_script_name = 'bi_populate_billofmaterials_fact'  
AND z.pToCurrency = c.currency AND z.pDate = STPO_AEDAT;


/* Update 2*/			

DROP TABLE IF EXISTS TMP1_STKO;
CREATE TABLE TMP1_STKO
as
SELECT * FROM STKO;

DROP TABLE IF EXISTS TMP2_STKO;
CREATE TABLE TMP2_STKO
as
SELECT DISTINCT hdr.*
FROM STKO hdr,stpo,dim_Bomcategory bomc
WHERE stpo.STPO_STLTY = hdr.STKO_STLTY
AND bomc.Category = STPO_STLTY
AND bomc.RowIsCurrent = 1		
AND stpo.STPO_STLNR =  hdr.STKO_STLNR;

CALL VECTORWISE(COMBINE 'TMP1_STKO - TMP2_STKO');
	 
	 
/* Update column Dim_CreatedOnDateid */

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_CreatedOnDateid = 1
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_CreatedOnDateid,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
 ,dim_date dr
SET bom.Dim_CreatedOnDateid = dr.dim_dateid
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
  AND dr.DateValue = STKO_ANDAT AND 'Not Set' = dr.CompanyCode
AND IFNULL(bom.Dim_CreatedOnDateid,-1) <> IFNULL(dr.dim_dateid,-2);


/* Update column Dim_ChangedOnDateid */

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_ChangedOnDateid = 1
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_ChangedOnDateid,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
 ,dim_date be
SET bom.Dim_ChangedOnDateid = be.dim_dateid
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
  AND be.DateValue = STKO_AEDAT AND 'Not Set' = be.CompanyCode
AND IFNULL(bom.Dim_ChangedOnDateid,-1) <> IFNULL(be.dim_dateid,-2);


/* Update column Dim_ValidFromDateid */

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_ValidFromDateid = 1
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_ValidFromDateid,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
 ,dim_date ls
SET bom.Dim_ValidFromDateid = ls.dim_dateid
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
  AND ls.DateValue = STKO_DATUV AND 'Not Set' = ls.CompanyCode
AND IFNULL(bom.Dim_ValidFromDateid,-1) <> IFNULL(ls.dim_dateid,-2);
	 
	 
UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET Dim_BOMComponentId = 1	 
WHERE     bom.dd_BomNumber = STKO_STLNR
AND bom.dd_Alternative = STKO_STLAL
AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
AND bc.category = STKO_STLTY
AND bc.RowIsCurrent = 1
AND bs.BOMStatusCode = STKO_STLST
AND bs.RowIsCurrent = 1
AND IFNULL(bom.dim_BOMComponentId,-1) <> 1	 ;
	 
	 
/* Update column Dim_BaseUOMId */

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET Dim_BaseUOMId = 1
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.dim_BaseUOMId,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
 ,dim_unitofmeasure uom
SET Dim_BaseUOMId = uom.Dim_UnitOfMeasureid
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
 AND uom.UOM = STKO_BMEIN AND uom.RowIsCurrent = 1
AND IFNULL(bom.Dim_BaseUOMId,-1) <> IFNULL(uom.Dim_UnitOfMeasureid,-2);


/* Update column bom.Dim_PlantId */

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_PlantId = 1
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_PlantId,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
 ,dim_plant dp
SET bom.Dim_PlantId = dp.Dim_PlantId
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
 AND dp.PlantCode = STKO_WRKAN AND dp.RowIsCurrent = 1
AND IFNULL(bom.Dim_PlantId,-1) <> IFNULL(dp.Dim_PlantId,-2);


/* Update local curr and exchg rate as previous query updated plantid */

/* Use STKO_WRKAN (plant id) to get local currency */

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr,dim_bomstatus bs,dim_bomcategory bc,dim_plant dp,dim_company c,dim_currency cur
SET 
	 Dim_Currencyid = cur.Dim_Currencyid
WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
 AND dp.PlantCode = STKO_WRKAN AND dp.RowIsCurrent = 1
 AND dp.companycode = c.companycode AND  c.currency = cur.CurrencyCode;


/* Update local exchange rate.  */
/* Use Dim_CurrencyId_TRA(from curr),  plant currency (to curr : retrieved using STKO_WRKAN ),STKO_AEDAT(pDate) */

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr,dim_bomstatus bs,dim_bomcategory bc,dim_plant dp,dim_company c,tmp_getExchangeRate1 z
SET 
	 amt_ExchangeRate = z.exchangeRate
	 
WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND  dp.PlantCode = STKO_WRKAN AND dp.RowIsCurrent = 1
AND  dp.companycode = c.companycode
AND  z.pFromCurrency  = ( SELECT CurrencyCode from dim_currency cur where bom.Dim_CurrencyId_TRA = cur.Dim_CurrencyId )
AND z.fact_script_name = 'bi_populate_billofmaterials_fact'  
AND  z.pToCurrency = c.currency AND z.pDate = STKO_AEDAT;


/* Update remaining columns. These did not have inner sub-queries in mysql */
	 
UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_bomstatusid = bs.Dim_bomstatusid
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_bomstatusid ,-1) <> IFNULL( bs.Dim_bomstatusid,-2);

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET Dim_ComponentUOMId = 1
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND Dim_ComponentUOMId  <> 1;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET Dim_StorageLocationid = 1
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND bom.Dim_StorageLocationid <> 1;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_IssuingPlantId = 1
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_IssuingPlantId ,-1) <> 1;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_PurchasingOrgId = 1
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND bom.Dim_PurchasingOrgId <> 1;

/* LK: Commented out as this is populated above */
/*
UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_Currencyid = 1
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND bom.Dim_Currencyid <> 1 */

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET dd_BOMItemNodeNo = 0
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(dd_BOMItemNodeNo ,-1) <> 0;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET
dd_BomItemNo = 'Not Set'
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND ifnull(bom.dd_BomItemNo,'xx') <> 'Not Set';

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET dd_BOMPredecessorNodeNo = 0
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_BOMPredecessorNodeNo ,-1) <> 0;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET
dd_CreatedBy = ifnull(STKO_ANNAM, 'Not Set')
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_CreatedBy ,'xx') <> ifnull(STKO_ANNAM, 'Not Set');

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET
dd_ChangedBy = ifnull(STKO_AENAM, 'Not Set')
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_ChangedBy ,'xx') <> ifnull(STKO_AENAM, 'Not Set');

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET ct_AvgMatPurityinPercent = 0.00
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.ct_AvgMatPurityinPercent ,-1) <> 0.00;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET
ct_LeadTimeOffset = 0.00
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.ct_LeadTimeOffset ,-1) <> 0.00;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET ct_BaseQty = STKO_BMENG
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.ct_BaseQty ,-1) <> IFNULL( STKO_BMENG,-2);

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET ct_ComponentQty = 0.00
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.ct_ComponentQty ,-1) <> 0.00;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET amt_Price = 0.00
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.amt_Price ,-1) <> 0.00;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET amt_PriceUnit = 1.00
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.amt_PriceUnit ,-1) <> 1.00;

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET
dd_PartNumber = 'Not Set'
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_PartNumber ,'xx') <> 'Not Set';

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET
dd_ComponentNumber = 'Not Set'
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_ComponentNumber ,'xx') <> 'Not Set';

UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_VendorId = 1
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_VendorId ,-1) <> 1;
	 
/* End of Update 2 */	 
						 
/* Insert 2*/
						 
INSERT INTO fact_bom_tmp_populate(ct_AvgMatPurityinPercent,
                                 ct_BaseQty,
                                 ct_ComponentQty,
                                 ct_ComponentScrapinPercent,
                                 ct_LeadTimeOffset,
                                 amt_Price,
                                 amt_PriceUnit,
                                 dd_BomItemNo,
                                 dd_BOMItemNodeNo,
                                 dd_BomNumber,
                                 Dim_BomCategoryId,
                                 dd_Alternative,
                                 dd_BOMPredecessorNodeNo,
                                 dd_ChangedBy,
                                 dd_CreatedBy,
                                 dd_PartNumber,
                                 dd_ComponentNumber,
                                 Dim_PlantId,
                                 Dim_BaseUOMId,
                                 Dim_ComponentUOMId,
                                 Dim_BOMComponentId,
                                 Dim_ChangedOnDateid,
                                 Dim_CreatedOnDateid,
                                 Dim_CurrencyId,
                                 Dim_IssuingPlantId,
                                 Dim_PurchasingOrgId,
                                 Dim_StorageLocationId,
                                 Dim_BomStatusId,
                                 Dim_PurchasingGroupId,
                                 Dim_MaterialGroupId,
                                 Dim_ValidFromDateid,
                                 Dim_BomItemCategoryId,
                                 Dim_VendorId,fact_bomid,
								 Dim_Currencyid_TRA,
								 Dim_Currencyid_GBL)
   SELECT 0.00 ct_AvgMatPurityinPercent,
          STKO_BMENG ct_BaseQty,	/* decimal - ok */
          0.00 ct_ComponentQty,
          0.00 ct_ComponentScrapinPercent,
          0 ct_LeadTimeOffset,
          0.00 amt_Price,
          1.00 amt_PriceUnit,
		  0 dd_BomItemNo,			/* 'Not Set' dd_BomItemNo, in mysql	*/
          0 dd_BOMItemNodeNo,
          STKO_STLNR dd_BomNumber,	/* varchar - ok */
          bc.Dim_BomCategoryId Dim_BomCategoryId,	/* int - ok */
          STKO_STLAL dd_Alternative,	/* varchar - ok */
          0 dd_BOMPredecessorNodeNo,
          ifnull(STKO_AENAM, 'Not Set') dd_ChangedBy,
          ifnull(STKO_ANNAM, 'Not Set') dd_CreatedBy,
          'Not Set' dd_PartNumber,
          'Not Set' dd_ComponentNumber,
          ifnull((SELECT dp.Dim_PlantId
                    FROM dim_plant dp
                   WHERE dp.PlantCode = STKO_WRKAN AND dp.RowIsCurrent = 1),
                 1)
             Dim_PlantId,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = STKO_BMEIN AND uom.RowIsCurrent = 1),
                 1)
             Dim_BaseUOMId,
          1 Dim_ComponentUOMId,
          1 Dim_BOMComponentId,
          ifnull(
             (SELECT be.dim_dateid
                FROM dim_date be
               WHERE be.DateValue = STKO_AEDAT AND 'Not Set' = be.CompanyCode),
             1)
             Dim_ChangedOnDateid,--int
          ifnull(
             (SELECT dr.dim_dateid
                FROM dim_date dr
               WHERE dr.DateValue = STKO_ANDAT AND 'Not Set' = dr.CompanyCode),
             1)
             Dim_CreatedOnDateid,--int
		  			ifnull((SELECT cur.Dim_Currencyid
					FROM dim_plant dp,dim_company c,dim_currency cur
					WHERE dp.PlantCode = STKO_WRKAN AND dp.RowIsCurrent = 1
					AND dp.companycode = c.companycode
					AND c.currency = cur.CurrencyCode ),1) Dim_Currencyid,	/* Local currency */
          1 Dim_IssuingPlantId,
          1 Dim_PurchasingOrgId,
          1 Dim_StorageLocationid,
          bs.Dim_bomstatusid Dim_bomstatusid,
          1 Dim_PurchasingGroupId,
          1 Dim_MaterialGroupid,
          ifnull(
             (SELECT ls.dim_dateid
                FROM dim_date ls
               WHERE ls.DateValue = STKO_DATUV AND 'Not Set' = ls.CompanyCode),
             1)
             Dim_ValidFromDateid,--int
          1 Dim_BomItemCategoryId,
          1 Dim_VendorId,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_bom_tmp_populate') + row_number() over (),
		  1 Dim_Currencyid_TRA,		  	/* Keep default value */
			ifnull((SELECT Dim_Currencyid
					FROM dim_currency cur
					WHERE cur.CurrencyCode = pGlobalCurrency),1) Dim_Currencyid_GBL
					
     FROM    TMP1_STKO hdr
          INNER JOIN
             dim_bomstatus bs
          ON bs.BOMStatusCode = ifnull(STKO_STLST, 0)
             AND bs.RowIsCurrent = 1
          INNER JOIN dim_bomcategory bc
             ON bc.Category = STKO_STLTY AND bc.RowIsCurrent = 1,
			 tmp_pGlobalCurrency_bom
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_bom_tmp_populate bom
                   WHERE     bom.dd_BomNumber = hdr.STKO_STLNR
                         AND bom.dd_Alternative = hdr.STKO_STLAL
                         AND bc.Category = hdr.STKO_STLTY
                         AND bc.RowIsCurrent = 1);
									
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_bomid),0) from fact_bom_tmp_populate)
where table_name = 'fact_bom_tmp_populate';											


/* Delete 1 */

DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_1;
CREATE TABLE TMP_DEL_fact_bom_tmp_populate_1
AS
SELECT bom.*
FROM fact_bom_tmp_populate bom,STKO k , STPO p
WHERE     k.STKO_STLNR = p.STPO_STLNR
AND k.STKO_STLTY = p.STPO_STLTY
AND k.STKO_STLAL = bom.dd_Alternative
AND k.STKO_STLNR = bom.dd_BomNumber
AND p.STPO_STLKN = bom.dd_BomItemNodeNo
AND p.STPO_LKENZ = 'X';

CALL VECTORWISE(COMBINE 'fact_bom_tmp_populate - TMP_DEL_fact_bom_tmp_populate_1');


DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_2;
CREATE TABLE TMP_DEL_fact_bom_tmp_populate_2
AS
SELECT bom.*
FROM fact_bom_tmp_populate bom,STKO k
WHERE     k.STKO_STLNR = dd_BomNumber
AND k.STKO_STLAL = dd_Alternative
AND k.STKO_LOEKZ = 'X';

CALL VECTORWISE(COMBINE 'fact_bom_tmp_populate - TMP_DEL_fact_bom_tmp_populate_2');

/* Update column Dim_MaterialPlantId */

UPDATE fact_bom_tmp_populate bom
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc
SET Dim_MaterialPlantId = 1
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
AND bom.Dim_MaterialPlantId <> 1;


UPDATE fact_bom_tmp_populate bom
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc
 ,dim_plant p
SET Dim_MaterialPlantId = p.Dim_PlantId
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
 AND p.PlantCode = MAST_WERKS AND p.RowIsCurrent = 1
AND bom.Dim_MaterialPlantId <> p.Dim_PlantId;


/* Update column Dim_PartId */

UPDATE fact_bom_tmp_populate bom
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc
SET Dim_PartId = 1
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
AND bom.Dim_PartId <> 1;


UPDATE fact_bom_tmp_populate bom
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc
 ,dim_part pt
SET Dim_PartId = pt.Dim_PartId
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
 AND pt.PartNumber = m.MAST_MATNR AND pt.Plant = m.MAST_WERKS AND pt.RowIsCurrent = 1
AND bom.Dim_PartId <> pt.Dim_PartId;

UPDATE fact_bom_tmp_populate bom
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc
SET  bom.dd_PartNumber = ifnull(m.MAST_MATNR,'Not Set')
WHERE m.MAST_STLNR = bom.dd_BomNumber
 AND m.MAST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'M'
AND IFNULL( bom.dd_PartNumber ,'xx') <> ifnull(m.MAST_MATNR,'Not Set');

UPDATE fact_bom_tmp_populate bom
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc
SET bom.Dim_BomUsageId = bu.Dim_bomusageid
WHERE m.MAST_STLNR = bom.dd_BomNumber
 AND m.MAST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'M'
AND bom.Dim_BomUsageId <> bu.Dim_bomusageid;

/* Done till here */

/* Update from PRST : Category = P */


/* Update Dim_MaterialPlantId */
UPDATE    fact_bom_tmp_populate bom 
FROM PRST p,dim_bomusage bu,dim_bomcategory bc
SET bom.Dim_MaterialPlantId  = 1
WHERE p.PRST_STLNR = bom.dd_BomNumber
          AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P';	

UPDATE    fact_bom_tmp_populate bom 
FROM PRST p,dim_bomusage bu,dim_bomcategory bc
,dim_plant pl
SET bom.Dim_MaterialPlantId  = pl.Dim_PlantId
WHERE p.PRST_STLNR = bom.dd_BomNumber
          AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P'
AND pl.PlantCode = p.PRST_WERKS
AND pl.RowIsCurrent = 1
AND bom.Dim_MaterialPlantId  <> pl.Dim_PlantId;	


/* Update Dim_PartId */
UPDATE    fact_bom_tmp_populate bom 
FROM PRST p,dim_bomusage bu,dim_bomcategory bc
SET bom.Dim_PartId  =  1
WHERE p.PRST_STLNR = bom.dd_BomNumber
AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P';	

UPDATE    fact_bom_tmp_populate bom 
FROM PRST p,dim_bomusage bu,dim_bomcategory bc
,dim_part pt
SET bom.Dim_PartId  = pt.Dim_PartId
WHERE p.PRST_STLNR = bom.dd_BomNumber
AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P'
AND pt.PartNumber = p.PRST_MATNR
AND pt.Plant = p.PRST_WERKS
AND pt.RowIsCurrent = 1
AND bom.Dim_PartId  <> pt.Dim_PartId;	



UPDATE    fact_bom_tmp_populate bom
FROM PRST p,dim_bomusage bu,dim_bomcategory bc
SET  bom.dd_PartNumber = ifnull(p.PRST_MATNR,'Not Set')
WHERE p.PRST_STLNR = bom.dd_BomNumber
 AND p.PRST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'P'
AND IFNULL( bom.dd_PartNumber ,'xx') <> ifnull(p.PRST_MATNR,'Not Set');

UPDATE    fact_bom_tmp_populate bom
FROM PRST p,dim_bomusage bu,dim_bomcategory bc
SET bom.Dim_BomUsageId = bu.Dim_bomusageid
WHERE p.PRST_STLNR = bom.dd_BomNumber
 AND p.PRST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'P'
AND bom.Dim_BomUsageId <> bu.Dim_bomusageid;

UPDATE    fact_bom_tmp_populate bom
FROM PRST p,dim_bomusage bu,dim_bomcategory bc
SET bom.dd_WBSElement = ifnull(p.PRST_PSPNR,0)
WHERE p.PRST_STLNR = bom.dd_BomNumber
 AND p.PRST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'P'
AND IFNULL(bom.dd_WBSElement ,-1) <> ifnull(p.PRST_PSPNR,0);

/* End of Update from PRST : Category = P */



UPDATE       fact_bom_tmp_populate bom
FROM  dim_bomcategory bc,STPO p, Dim_BillOfMaterialMisc bomi
SET bom.Dim_BillOfMaterialMiscid = bomi.Dim_BillOfMaterialMiscid
WHERE bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND   bom.dd_BomNumber = p.STPO_STLNR
AND bom.dd_BomItemNodeNo = p.STPO_STLKN
AND bc.Category = p.STPO_STLTY
AND  bomi.FixedQuantity = ifnull(p.STPO_FMENG, 'Not Set')
AND bomi.Net = ifnull(p.STPO_NETAU, 'Not Set')
AND bomi.BulkMaterial = ifnull(p.STPO_SCHGT, 'Not Set')
AND bomi.MaterialProvision = ifnull(p.STPO_BEIKZ, 'Not Set')
AND bomi.ItemRelevantToSales = ifnull(p.STPO_RVREL, 'Not Set')
AND bomi.ItemRelevantToProduction = ifnull(p.STPO_SANFE, 'Not Set')
AND bomi.ItemRelevantForPlantMaint = ifnull(p.STPO_SANIN, 'Not Set')
AND bomi.ItemRelevantToEngg = ifnull(p.STPO_SANKA, 'Not Set')
AND bomi.BOMIsRecursive = ifnull(p.STPO_REKRI, 'Not Set')
AND bomi.RecursivenessAllowed = ifnull(p.STPO_REKRS, 'Not Set')
AND bomi.CAD = ifnull(p.STPO_CADPO, 'Not Set')
AND   bom.Dim_BillOfMaterialMiscid <> bomi.Dim_BillOfMaterialMiscid ;

DROP TABLE IF EXISTS tmp_FactBOMValidToDate;

CREATE TABLE tmp_FactBOMValidToDate
AS
SELECT   distinct dim_bomcategoryid, dd_BomNumber,dd_BomItemNo,dt.dateValue as ValidFromDate,'1 Jan 9999' as ValidToDate
FROM fact_bom_tmp_populate inner join dim_date dt on dt.dim_dateid = dim_validfromdateid 
where dim_validfromdateid <> 1;

DROP TABLE IF EXISTS tmp_FactBOMValidToDate_1;

CREATE TABLE tmp_FactBOMValidToDate_1 AS
SELECT distinct b1.dim_bomcategoryid, b1.dd_BomNumber,b1.dd_BomItemNo ,b2.validfromdate, (dt.datevalue - INTERVAL '1' DAY) ValidToDate FROM 
tmp_FactBOMValidToDate b2, fact_bom_tmp_populate b1,dim_date dt
WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid 
and b1.dd_BomNumber = b2.dd_BomNumber 
and b1.dd_BomItemNo = b2.dd_BomItemNo
AND dt.dim_dateid = b1.dim_validfromdateid
AND b2.validfromdate < dt.datevalue;

UPDATE fact_bom_tmp_populate b1
FROM	tmp_FactBOMValidToDate_1 b2, dim_date dt,dim_Date dfrd
SET b1.dim_dateidvalidto = dt.dim_dateid
WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid
AND b1.dd_BomNumber = b2.dd_BomNumber
AND b1.dd_BomItemNo = b2.dd_BomItemNo
AND dfrd.dim_dateid = b1.dim_validfromdateid
AND dfrd.datevalue = b2.validfromdate
AND dfrd.companycode = 'Not Set'
AND dt.datevalue = b2.ValidToDate
AND dt.companycode = 'Not Set'
AND b1.dim_dateidvalidto <> dt.dim_dateid;

UPDATE fact_bom_tmp_populate b1
  FROM dim_Date dt
SET b1.dim_dateidvalidto = dt.Dim_DateId
WHERE (b1.dim_dateidvalidto = 1 OR b1.dim_dateidvalidto IS NULL)
  AND dt.DateValue = '1 Jan 9999'
  AND dt.CompanyCode = 'Not Set';

 UPDATE fact_bom_tmp_populate b from dim_producthierarchy dph, dim_part dpr
    SET dim_compprodhierarchyid = dim_producthierarchyid
  WHERE dph.ProductHierarchy = dpr.ProductHierarchy
        AND dpr.Dim_Partid = b.Dim_BOMComponentId;

 UPDATE fact_bom_tmp_populate
    SET dim_compprodhierarchyid = 1
  WHERE dim_compprodhierarchyid IS NULL;

 UPDATE fact_bom_tmp_populate b from dim_producthierarchy dph, dim_part dpr
    SET dim_parentpartprodhierarchyid = dim_producthierarchyid
  WHERE dph.ProductHierarchy = dpr.ProductHierarchy
        AND dpr.Dim_Partid = b.Dim_partId;

 UPDATE fact_bom_tmp_populate
    SET dim_parentpartprodhierarchyid = 1
  WHERE dim_parentpartprodhierarchyid IS NULL;


DROP TABLE IF EXISTS tmp_FactBOMValidToDate_1;

DROP TABLE IF EXISTS tmp_FactBOMValidToDate;


/* Drop original table and rename staging table to orig table name */
drop table if exists fact_bom;
rename table fact_bom_tmp_populate to fact_bom;

DROP TABLE IF EXISTS tmp_FactBOMValidToDate;

/* These 2 procs are called after this */
/* CALL bi_populate_bom_level() */
/* CALL bi_process_bom_fact() */

DROP TABLE IF EXISTS tmp_pGlobalCurrency_bom;
DROP TABLE IF EXISTS TMP1_STKO;
DROP TABLE IF EXISTS TMP2_STKO;
DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_1;
DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_2;
