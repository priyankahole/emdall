
/* ##################################################################################################################*/
/*   Change History */
/*   Date            By        Version           Desc */
/*   9 Sep  2013     Lokesh	 1.34		 Currency and exchange rate changes */
/*   25 Aug 2013     Lokesh      1.33            Performance changes */
/*   29 Aug 2013     Shanthi     1.36            Fixed the amt_Salessubtotal3unitprice, fixed dim_materialpricegroup4id,dim_materialpricegroup5id             */
/*   7 Sep  2013     Shanthi     1.37            Fixed the null values for dim_materialpricegroup4id,dim_materialpricegroup5id   */
/*                                                 and the delivery type column  */
/*	 26 Sep 2013     Issam       1.42            Added fields dd_SDCreateTime dd_DeliveryTime, dd_PickingTime, 
												 dd_GITime, dd_SDLineCreateTime											*/
/*   12 Feb 2014     George      1.43            Added Dim_ScheduleDeliveryBlockid                                   */
/*   14 Feb 2014     George      1.44            Added Dim_CustomerGroup4id                                        */
/* #################################################################################################################### */




select 'START OF PROC VW_bi_populate_afs_salesorderdelivery_fact',TIMESTAMP(LOCAL_TIMESTAMP);

/*##########PART 1 Starts###############################	 */

Drop table if exists cursor_tbl1_721;
Drop table if exists cursor_tbl2_721;
Drop table if exists variable_tbl_721;

Create table variable_tbl_721( pDeltaChangesFlag varchar(10) null);

Create table cursor_tbl1_721 (
v_SalesDocNo      varchar(50) null,
v_SalesItemNo     integer null,
v_TotConfQty     decimal(18,4) null,
v_TotSchedQty     decimal(18,4) null,
v_GIDateMax       date null,
v_SchedQty        decimal(18,4) null,
v_ConfQty        decimal(18,4) null,
v_SchedNo         integer null,
v_Count           integer null,
v_MaxFactId       integer null,
iflag integer default 0);

Create table cursor_tbl2_721 (
v_SalesDocNo      varchar(50) null,
v_SalesItemNo     integer null,
v_TotConfQty     decimal(18,4) null,
v_TotSchedQty     decimal(18,4) null,
v_GIDateMax       date null,
v_SchedQty        decimal(18,4) null,
v_ConfQty        decimal(18,4) null,
v_SchedNo         integer null,
v_Count           integer null,
v_MaxFactId       integer null,
iflag integer default 0);


 /*If pDeltaChangesFlag <> 'true', truncate all rows in fact_salesorderdelivery */
 drop table if exists tmp_delete_fact_salesorderdelivery;
 create table tmp_delete_fact_salesorderdelivery
 as
 select f.*
 from fact_salesorderdelivery f, systemproperty s
 where s.property = 'process.delta.salesorderdelivery'
 and s.property_value <> 'true';

 call vectorwise(combine 'fact_salesorderdelivery-tmp_delete_fact_salesorderdelivery');

 drop table tmp_delete_fact_salesorderdelivery;

/* LK 30 Mar - This delete would run in any case. If delta.salesorderdelivery was false, f_sod would anyway have 0 rows, otherwise this SQL 
would actually delete rows */
DELETE FROM fact_salesorderdelivery
WHERE exists (select 1 from CDPOS_LIKP a where a.CDPOS_OBJECTID = dd_SalesDlvrDocNo AND a.CDPOS_CHNGIND = 'D' AND a.CDPOS_TABNAME = 'LIKP');

call vectorwise(combine 'fact_salesorderdelivery');


INSERT INTO variable_tbl_721( pDeltaChangesFlag )
SELECT ifnull(property_value,'true') from systemproperty
Where  property = 'process.delta.salesorderdelivery';

call vectorwise(combine 'variable_tbl_721');
 select 'A1',TIMESTAMP(LOCAL_TIMESTAMP);
/*call bi_populate_sod_headerstatus_dim()	--This is not called in the latest version of proc in mysql ( LK : 31 Mar ) */
/*call bi_populate_sod_itemstatus_dim() 	--This is not called in the latest version of proc in mysql ( LK : 31 Mar ) */

drop table if exists delete_staging_721;

/* LK 26 Aug: Slow query. Tuned now. */

DROP TABLE IF EXISTS tmp_delstg721_fact_salesorderdelivery_ins;
CREATE TABLE tmp_delstg721_fact_salesorderdelivery_ins
AS
SELECT DISTINCT dd_SalesDlvrDocNo,dd_SalesDlvrItemNo
FROM fact_salesorderdelivery sod,LIKP_LIPS 
WHERE dd_SalesDlvrDocNo = LIKP_VBELN;

DROP TABLE IF EXISTS tmp_delstg721_fact_salesorderdelivery_del;
CREATE TABLE tmp_delstg721_fact_salesorderdelivery_del
AS
select DISTINCT LIKP_VBELN dd_SalesDlvrDocNo,LIPS_POSNR dd_SalesDlvrItemNo
FROM LIKP_LIPS;


CALL VECTORWISE(COMBINE 'tmp_delstg721_fact_salesorderdelivery_ins-tmp_delstg721_fact_salesorderdelivery_del');


Create table delete_staging_721 as
Select distinct sod.* from fact_salesorderdelivery sod,variable_tbl_721,tmp_delstg721_fact_salesorderdelivery_ins ins
WHERE sod.dd_SalesDlvrDocNo = ins.dd_SalesDlvrDocNo 
AND sod.dd_SalesDlvrItemNo = ins.dd_SalesDlvrItemNo
AND pDeltaChangesFlag='true';
call vectorwise(combine 'delete_staging_721');

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');
drop table if exists delete_staging_721;

 select 'A',TIMESTAMP(LOCAL_TIMESTAMP);


Create table delete_staging_721 as
Select distinct fact_salesorderdelivery.* from fact_salesorderdelivery,variable_tbl_721,LIKP_LIPS a
WHERE dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrItemNo = LIPS_POSNR
AND  pDeltaChangesFlag='true';
call vectorwise(combine 'delete_staging_721');

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');
drop table if exists delete_staging_721;



DROP TABLE IF EXISTS tmp_delstg721_fso_ins;
CREATE TABLE tmp_delstg721_fso_ins
AS
SELECT DISTINCT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
FROM fact_salesorderdelivery;

DROP TABLE IF EXISTS tmp_delstg721_fso_del;
CREATE TABLE tmp_delstg721_fso_del
AS
SELECT DISTINCT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
FROM fact_salesorder;

CALL VECTORWISE(COMBINE 'tmp_delstg721_fso_ins-tmp_delstg721_fso_del');

Create table delete_staging_721 as
Select distinct fact_salesorderdelivery.* from fact_salesorderdelivery,variable_tbl_721,tmp_delstg721_fso_ins f
WHERE fact_salesorderdelivery.dd_SalesDocNo = f.dd_SalesDocNo 
and  fact_salesorderdelivery.dd_SalesItemNo = f.dd_SalesItemNo
and  fact_salesorderdelivery.dd_ScheduleNo = f.dd_ScheduleNo
AND   pDeltaChangesFlag='true';
							

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');
drop table if exists delete_staging_721;

Create table delete_staging_721 as
Select fact_salesorderdelivery.* from fact_salesorderdelivery,variable_tbl_721
WHERE dd_SalesDlvrDocNo = 'Not Set'
AND  pDeltaChangesFlag='true';
call vectorwise(combine 'delete_staging_721');

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');
drop table if exists delete_staging_721;
 
Create table delete_staging_721 as
Select fact_salesorderdelivery.* from fact_salesorderdelivery,variable_tbl_721
where pDeltaChangesFlag<>'true';

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');

drop table if exists delete_staging_721;

Drop table if exists max_holder_721;
Create table max_holder_721(maxid)
as
Select ifnull(max(fact_salesorderdeliveryid),0)
from fact_salesorderdelivery;
call vectorwise(combine 'max_holder_721');
call vectorwise(combine 'likp_lips');
select 'Insert 1 on fact_salesorderdelivery',TIMESTAMP(LOCAL_TIMESTAMP);

drop table if exists LIKP_LIPS_sub00;
Create table LIKP_LIPS_sub00 as Select * from  LIKP_LIPS where LIPS_LFIMG  > 0 ;

\i /db/schema_migration/bin/wrapper_optimizedb.sh LIKP_LIPS_sub00;
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorderdelivery;

Drop table if exists fact_salesorderdelivery_sub001;
Create table fact_salesorderdelivery_sub001 as 
Select *,
varchar(null,20) companycode_upd,
ansidate(null) likp_erdat_upd,
varchar(null,4) likp_fkarv_upd,
ansidate(null) likp_fkdat_upd,
ansidate(null) likp_kodat_upd,
varchar(null,10) likp_kunag_upd,
varchar(null,10) likp_kunnr_upd,
ansidate(null) likp_lddat_upd,
ansidate(null) likp_lfdat_upd,
varchar('',10) likp_vbeln_upd,
varchar(null,2) likp_vtwiv_upd,
ansidate(null) likp_wadat_upd,
ansidate(null) likp_wadat_ist_upd,
varchar(null,4) lips_kokrs_upd,
varchar(null,4) lips_lgort_upd,
varchar(null,18) lips_matnr_upd,
ansidate(null) lips_mbdat_upd,
integer(null) lips_posnr_upd,
varchar(null,18) lips_prodh_upd,
varchar(null,4) lips_werks_upd
from fact_salesorderdelivery where 1=2;

INSERT INTO fact_salesorderdelivery_sub001(
fact_salesorderdeliveryid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdSODocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    Dim_DateIdRejection,
				    dd_CustomerMaterial,
					companycode_upd,
					likp_erdat_upd,
					likp_fkarv_upd,
					likp_fkdat_upd,
					likp_kodat_upd,
					likp_kunag_upd,
					likp_kunnr_upd,
					likp_lddat_upd,
					likp_lfdat_upd,
					likp_vbeln_upd,
					likp_vtwiv_upd,
					likp_wadat_upd,
					likp_wadat_ist_upd,
					lips_kokrs_upd,
					lips_lgort_upd,
					lips_matnr_upd,
					lips_mbdat_upd,
					lips_posnr_upd,
					lips_prodh_upd,
					lips_werks_upd,
					/* LK Changes 9 Sep */
					dim_Currencyid_TRA,
					dim_Currencyid_GBL,
					dim_currencyid_STAT,
					amt_exchangerate_STAT,
					dd_BillOfLading,
					Dim_PartSalesId,
					Dim_AvailabilityCheckId,
					Dim_DeliveryPriorityId,
					Dim_IncoTermId,
					dd_InternaltionArticleNo,
					dd_ReleaseRule,
					dd_ArunNumber,Dim_salesofficeid
					)
	   SELECT maxid + row_number() over(order by LIPS_VGBEL,LIPS_VGPOS,LIPS_J_3AETENR) fact_salesorderdeliveryid,
	  LIPS_VGBEL dd_SalesDocNo,
          LIPS_VGPOS dd_SalesItemNo,
          LIPS_J_3AETENR dd_ScheduleNo,
          LIKP_VBELN dd_SalesDlvrDocNo,
          LIPS_POSNR dd_SalesDlvrItemNo,
          ifnull(lips_bwart, 'Not Set') dd_MovementType,
          f.dd_AfsStockCategory,
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          LIPS_LFIMG ct_QtyDelivered,
          LIPS_WAVWR amt_Cost_DocCurr,
          Decimal((LIPS_WAVWR
           * Decimal((CASE
                 WHEN f.amt_ExchangeRate < 0
                 THEN
                    (1 / (case when (-1 * f.amt_ExchangeRate)= 0 then null else (-1 * f.amt_ExchangeRate) end))
                 ELSE
                    f.amt_ExchangeRate
              END),18,4)),18,4)
             amt_Cost,
          f.amt_SubTotal3,
          f.amt_SubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          0 ct_AfsOpenQty,
          lips_vbeaf ct_FixedProcessDays,
          lips_vbeav ct_ShipProcessDays,
          (CASE WHEN (f.ct_ScheduleQtySalesUnit - LIPS_LFIMG >= 0 ) THEN  LIPS_LFIMG ELSE f.ct_ScheduleQtySalesUnit END )ct_ScheduleQtySalesUnit,
          (CASE WHEN (f.ct_ConfirmedQty - LIPS_LFIMG >= 0 ) THEN  LIPS_LFIMG ELSE f.ct_ConfirmedQty END ) ct_ConfirmedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
             1 Dim_DateidPlannedGoodsIssue,
             1 Dim_DateidActualGoodsIssue,
             1 Dim_DateidDeliveryDate,
             1 Dim_DateidLoadingDate,
             1 Dim_DateidPickingDate,
             1 Dim_DateidDlvrDocCreated,
             1 Dim_DateidMatlAvail,
	 f.Dim_CustomerID Dim_CustomeridSoldTo,
                 1 Dim_CustomeridShipTo,
             1 Dim_Partid,
          pl.Dim_Plantid,
             1 Dim_StorageLocationid,
                 1 Dim_ProductHierarchyid,
                 1 Dim_DeliveryHeaderStatusid,
             1 Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
          f.Dim_DateidMtrlAvail,
          f.Dim_Currencyid,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
                 1 Dim_ControllingAreaId,
             f.Dim_BillingDateId Dim_DateidBillingDate,
                 1 Dim_BillingDocumentTypeid,
             f.Dim_DistributionChannelId Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
             1 Dim_DateidActualGI_Original,
          LIPS_J_3ADEPM dd_AfsDepartment,
          f.ct_AfsAllocationRQty ct_ReservedQty,
          f.ct_AfsAllocationFQty ct_FixedQty,
          (CASE
              WHEN f.ct_ConfirmedQty > 0
              THEN
                 Decimal((f.amt_SubTotal3 / (case when f.ct_ConfirmedQty = 0 then null else f.ct_ConfirmedQty end)),18,4)
              ELSE
                 0
           END)
             amt_SalesSubTotal3UnitPrice,
	    f.Dim_DateIdSODocument Dim_DateIdSODocument,
	    f.Dim_DateIdSOCreated Dim_DateIdSOCreated,
	    f.dd_CreatedBy dd_SDCreatedBy,
	    f.Dim_DeliveryblockId,
	    f.ct_AfsUnallocatedQty,
	    ifnull(f.Dim_DateIdSOItemChangedOn,1),
            f.Dim_ShippingConditionId,
	    f.dd_AfsAllocationGroupNo,
	    f.Dim_SalesOrderHeaderStatusId,
	    f.Dim_SalesOrderItemStatusId,
	    f.ct_AfsTotalDrawn,
	    ifnull(f.Dim_DateidSchedDelivery,1),
	    ifnull(f.Dim_DateIdRejection,1),
	    ifnull(f.dd_CustomerMaterialNo,'Not Set'),
	   companycode,
	likp_erdat,
	likp_fkarv,
	likp_fkdat,
	likp_kodat,
	likp_kunag,
	likp_kunnr,
	likp_lddat,
	likp_lfdat,
	likp_vbeln,
	likp_vtwiv,
	likp_wadat,
	likp_wadat_ist,
	lips_kokrs,
	lips_lgort,
	lips_matnr,
	lips_mbdat,
	lips_posnr,
	lips_prodh,
	lips_werks,
	/* LK Changes 9 Sep */
	f.dim_Currencyid_TRA,
	f.dim_Currencyid_GBL,
	f.dim_currencyid_STAT,
	f.amt_exchangerate_STAT,
	ifnull(likp_bolnr,'Not Set'),
	f.Dim_PartSalesId,
	f.Dim_AvailabilityCheckId,
	f.Dim_DeliveryPriorityId,
	f.Dim_IncoTermId,
	f.dd_InternationalArticleNo,
	f.dd_ReleaseRule,
	f.dd_ArunNumber,f.Dim_salesofficeid
     FROM max_holder_721,LIKP_LIPS_sub00
          INNER JOIN fact_salesorder f
             ON     f.dd_SalesDocNo = LIPS_VGBEL
                AND f.dd_SalesItemNo = LIPS_VGPOS
                AND f.dd_ScheduleNo = LIPS_J_3AETENR
          INNER JOIN Dim_Plant pl
             ON LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
    WHERE f.dd_ItemRelForDelv = 'X' ;
 select 'B',TIMESTAMP(LOCAL_TIMESTAMP);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorderdelivery_sub001;

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidPlannedGoodsIssue = ifnull( (SELECT Dim_Dateid
					FROM Dim_Date dd
				       WHERE dd.DateValue = fss.likp_wadat_upd
					     AND dd.CompanyCode = fss.CompanyCode_upd),
				     1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidActualGoodsIssue = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_wadat_ist_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidDeliveryDate = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_lfdat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidLoadingDate = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_lddat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidPickingDate = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_kodat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set  Dim_DateidDlvrDocCreated = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_erdat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidMatlAvail = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.lips_mbdat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_CustomeridSoldTo = ifnull((SELECT Dim_CustomerID
                    FROM Dim_Customer cust
                   WHERE cust.CustomerNumber = fss.likp_kunag_upd),
                 fss.Dim_CustomeridSoldTo);


Update fact_salesorderdelivery_sub001 fss
Set Dim_CustomeridShipTo = ifnull((SELECT Dim_CustomerID
                    FROM Dim_Customer cust
                   WHERE cust.CustomerNumber = fss.likp_kunnr_upd),
                 1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_Partid = ifnull(
             (SELECT dim_partid
                FROM dim_part dp
               WHERE dp.PartNumber = fss.lips_matnr_upd AND dp.Plant = fss.lips_werks_upd),
             1);


Update fact_salesorderdelivery_sub001 fss
Set Dim_StorageLocationid = ifnull(
             (SELECT Dim_StorageLocationid
                FROM Dim_StorageLocation sl
               WHERE sl.LocationCode = fss.lips_lgort_upd AND sl.plant = fss.lips_werks_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_ProductHierarchyid = ifnull((SELECT Dim_ProductHierarchyid
                    FROM Dim_ProductHierarchy ph
                   WHERE ph.ProductHierarchy = fss.lips_prodh_upd),
                 1);



Update fact_salesorderdelivery_sub001 fss
Set Dim_DeliveryHeaderStatusid = ifnull((SELECT Dim_SalesOrderHeaderStatusid
                    FROM Dim_SalesOrderHeaderStatus sohs
                   WHERE sohs.SalesDocumentNumber = fss.LIKP_VBELN_upd),
                 1);


Update fact_salesorderdelivery_sub001 fss
Set Dim_DeliveryItemStatusid = ifnull(
             (SELECT Dim_SalesOrderItemStatusid
                FROM Dim_SalesOrderItemStatus sois
               WHERE sois.SalesDocumentNumber = fss.LIKP_VBELN_upd
                     AND sois.SalesItemNumber = fss.LIPS_POSNR_upd), 1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_ControllingAreaId = ifnull((SELECT ca.Dim_ControllingAreaid
                    FROM Dim_ControllingArea ca
                   WHERE ca.ControllingAreaCode = fss.LIPS_KOKRS_upd),
                 1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_BillingDocumentTypeid = ifnull((SELECT dim_billingdocumenttypeid
                    FROM dim_billingdocumenttype bdt
                   WHERE bdt.Type = fss.LIKP_FKARV_upd AND bdt.RowIsCurrent = 1),
                 1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DistributionChannelId = ifnull(
             (SELECT Dim_DistributionChannelId
                FROM dim_distributionchannel
               WHERE DistributionChannelCode = fss.LIKP_VTWIV_upd
                     AND RowIsCurrent = 1),
             fss.Dim_DistributionChannelId) ;

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidActualGI_Original = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_wadat_ist_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorderdelivery_sub001;
 select 'C',TIMESTAMP(LOCAL_TIMESTAMP);

/* Delete all rows from  fact_salesorderdelivery where keys match in fact_salesorderdelivery_sub001*/
DELETE FROM fact_salesorderdelivery f
WHERE EXISTS ( SELECT 1 FROM fact_salesorderdelivery_sub001 s where s.dd_salesdocno = f.dd_salesdocno and s.dd_salesitemno = f.dd_salesitemno and s.dd_scheduleno = f.dd_scheduleno and s.dd_salesdlvrdocno = f.dd_salesdlvrdocno and s.dd_salesdlvritemno = f.dd_salesdlvritemno );

INSERT INTO fact_salesorderdelivery(
					fact_salesorderdeliveryid,
					dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
				 Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
					 Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
                                    Dim_DateIdSODocument,
                                    Dim_DateIdSOCreated,
                                    dd_SDCreatedBy,
                                    Dim_DeliveryblockId,
                                    ct_AfsUnallocatedQty,
                                    Dim_DateIdSOItemChangedOn,
                                    Dim_ShippingConditionId,
                                    dd_AfsAllocationGroupNo,
                                    Dim_SalesOrderHeaderStatusId,
                                    Dim_SalesOrderItemStatusId,
                                    ct_AfsTotalDrawn,
                                    Dim_DateidSchedDelivery,
                                    Dim_DateIdRejection,
                                    dd_CustomerMaterial,
			    	    dim_materialpricegroup4id,
				    dim_materialpricegroup5id,
					dim_Currencyid_TRA,
					dim_Currencyid_GBL,
					dim_currencyid_STAT,
					amt_exchangerate_STAT,
					dd_BillOfLading,
					Dim_PartSalesId,
					Dim_AvailabilityCheckId,
					Dim_DeliveryPriorityId,
					Dim_IncoTermId,
					dd_InternaltionArticleNo,
					dd_ReleaseRule,dd_ArunNumber,Dim_salesofficeid
					)
Select fact_salesorderdeliveryid,
					dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
				 Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
					 Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
                                    Dim_DateIdSODocument,
                                    Dim_DateIdSOCreated,
                                    dd_SDCreatedBy,
                                    Dim_DeliveryblockId,
                                    ct_AfsUnallocatedQty,
                                    Dim_DateIdSOItemChangedOn,
                                    Dim_ShippingConditionId,
                                    dd_AfsAllocationGroupNo,
                                    Dim_SalesOrderHeaderStatusId,
                                    Dim_SalesOrderItemStatusId,
                                    ct_AfsTotalDrawn,
                                    Dim_DateidSchedDelivery,
                                    Dim_DateIdRejection,
                                    dd_CustomerMaterial,
			    	    dim_materialpricegroup4id,
				    dim_materialpricegroup5id,
					dim_Currencyid_TRA,
					dim_Currencyid_GBL,
					dim_currencyid_STAT,
					amt_exchangerate_STAT,
					dd_BillOfLading,
					Dim_PartSalesId,
					Dim_AvailabilityCheckId,
					Dim_DeliveryPriorityId,
					Dim_IncoTermId,
					dd_InternaltionArticleNo,
					dd_ReleaseRule,dd_ArunNumber,Dim_salesofficeid
From fact_salesorderdelivery_sub001 ;
 select 'D',TIMESTAMP(LOCAL_TIMESTAMP);

call vectorwise(combine 'fact_salesorderdelivery');
drop table if exists fact_salesorderdelivery_sub001;

drop table if exists LIKP_LIPS_sub00;
drop table if exists delete_staging_721;
drop table if exists Dim_ProfitCenter_721;
drop table if exists Dim_ProfitCenter_upd_721;

Create table Dim_ProfitCenter_721 as select first 0 * from Dim_ProfitCenter ORDER BY ValidTo ASC;
call vectorwise(combine 'Dim_ProfitCenter_721');

Create table Dim_ProfitCenter_upd_721 as Select 
maxid+row_number() over(order by LIPS_VGBEL,LIPS_VGPOS,LIPS_J_3AETENR) iid,LIPS_VGBEL,LIPS_VGPOS,LIPS_J_3AETENR,LIPS_PRCTR,LIPS_KOKRS,LIKP_WADAT_IST,LIKP_WADAT,1 Dim_ProfitCenterid
 FROM max_holder_721,LIKP_LIPS
          INNER JOIN fact_salesorder f
             ON     f.dd_SalesDocNo = LIPS_VGBEL
                AND f.dd_SalesItemNo = LIPS_VGPOS
                AND f.dd_ScheduleNo = LIPS_J_3AETENR
          INNER JOIN Dim_Plant pl
             ON LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
    WHERE f.dd_ItemRelForDelv = 'X' AND LIPS_LFIMG > 0;
call vectorwise(combine 'Dim_ProfitCenter_upd_721');
 select 'C2',TIMESTAMP(LOCAL_TIMESTAMP);


Update Dim_ProfitCenter_upd_721
set Dim_ProfitCenterid= ifnull(
             (SELECT pc.Dim_ProfitCenterid
                FROM Dim_ProfitCenter_721 pc
               WHERE     pc.ProfitCenterCode = LIPS_PRCTR
                     AND pc.ControllingArea = LIPS_KOKRS
                     AND pc.ValidTo >= ifnull(LIKP_WADAT_IST, LIKP_WADAT)
                     AND pc.RowIsCurrent = 1),
             1);
call vectorwise(combine 'Dim_ProfitCenter_upd_721');

Update fact_salesorderdelivery f
from Dim_ProfitCenter_upd_721 a
Set f.Dim_ProfitCenterid=a.Dim_ProfitCenterid
where f.fact_salesorderdeliveryid=iid
AND f.Dim_ProfitCenterid <> a.Dim_ProfitCenterid;

call vectorwise(combine 'fact_salesorderdelivery');

/*##########PART 1 Ends###############################	 */

/*# LK : Working perfectly till here in VW	 */

/*##########PART 2 Starts###############################	 */

/*Moved the cursor tables here as this should be happening after the true/false logic deletes data from f_sod */

DROP TABLE IF EXISTS tmp_perf_cursor_tbl1_721;
CREATE TABLE tmp_perf_cursor_tbl1_721
AS
SELECT sod.dd_SalesDocNo ,sod.dd_SalesItemNo, sod.dd_ScheduleNo,
	   SUM(sod.ct_confirmedqty) sum_ct_confirmedqty
FROM fact_salesorderdelivery sod
GROUP BY  sod.dd_SalesDocNo ,sod.dd_SalesItemNo, sod.dd_ScheduleNo;
	   

Insert into cursor_tbl1_721(v_SalesDocNo,v_SalesItemNo,v_SchedNo)
select  first 0 so.dd_Salesdocno, 
	so.dd_Salesitemno, 
	so.dd_scheduleno 
from fact_salesorder so,tmp_perf_cursor_tbl1_721 sod
where sod.dd_SalesDocNo = so.dd_salesdocno and sod.dd_SalesItemNo = so.dd_salesitemno and sod.dd_ScheduleNo = so.dd_Scheduleno
AND so.ct_ConfirmedQty > sod.sum_ct_confirmedqty
ORDER by so.dd_SalesDocNo, so.dd_SalesItemNo, so.dd_ScheduleNo;

call vectorwise(combine 'cursor_tbl1_721');
 select 'D2',TIMESTAMP(LOCAL_TIMESTAMP);
 
update cursor_tbl1_721                    
set v_ConfQty = ifnull((select f.ct_ConfirmedQty
                              from fact_salesorder f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo 
                                    and f.dd_ItemRelForDelv = 'X'),0);
call vectorwise(combine 'cursor_tbl1_721');
                                   
update cursor_tbl1_721 
set v_GIDateMax = ifnull((select max(dt.DateValue) from fact_salesorderdelivery fd 
                                          INNER JOIN dim_Date dt ON fd.Dim_DateidPlannedGoodsIssue = dt.dim_dateid
                            where fd.dd_SalesDocNo = v_SalesDocNo and fd.dd_SalesItemNo = v_SalesItemNo and fd.dd_ScheduleNo = v_SchedNo), ANSIDATE(current_date));
update cursor_tbl1_721 
set v_MaxFactId = ifnull((select max(fact_salesorderdeliveryid) from fact_salesorderdelivery fd 
                                          INNER JOIN dim_Date dt ON fd.Dim_DateidPlannedGoodsIssue = dt.dim_dateid
                            where fd.dd_SalesDocNo = v_SalesDocNo and fd.dd_SalesItemNo = v_SalesItemNo and fd.dd_ScheduleNo = v_SchedNo and dt.datevalue = ANSIDATE(v_GIDateMax)), -1);

update cursor_tbl1_721
set v_TotConfQty =   ifnull((select SUM(f.ct_ConfirmedQty)
                              from fact_salesorderdelivery f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo),0);

call vectorwise(combine 'cursor_tbl1_721');
Update cursor_tbl1_721
Set v_ConfQty = v_ConfQty - v_TotConfQty
Where (v_ConfQty - v_TotConfQty) > 0
AND v_ConfQty <> (v_ConfQty - v_TotConfQty);

call vectorwise(combine 'cursor_tbl1_721');

  UPDATE fact_salesorderdelivery sod
From cursor_tbl1_721, dim_date dt 
    SET sod.ct_ConfirmedQty =  sod.ct_ConfirmedQty + v_ConfQty
  WHERE sod.dd_SalesDocNo = v_SalesDocNo and sod.dd_SalesItemNo = v_SalesItemNo and sod.dd_ScheduleNo = v_SchedNo
  AND dt.Datevalue = v_GIDateMax AND dt.dim_Dateid = sod.dim_DateIdPlannedGoodsIssue
AND  sod.ct_ConfirmedQty <> ( sod.ct_ConfirmedQty + v_ConfQty)
AND v_MaxFactId = sod.fact_salesorderdeliveryid;
call vectorwise(combine 'fact_salesorderdelivery');

delete from cursor_tbl1_721;

Insert into cursor_tbl1_721(v_SalesDocNo,v_SalesItemNo,v_SchedNo)
select  first 0 so.dd_Salesdocno, 
	so.dd_Salesitemno, 
	so.dd_scheduleno 
from fact_salesorder so,tmp_perf_cursor_tbl1_721 sod
where sod.dd_SalesDocNo = so.dd_salesdocno and sod.dd_SalesItemNo = so.dd_salesitemno and sod.dd_ScheduleNo = so.dd_Scheduleno
AND so.ct_ConfirmedQty < sod.sum_ct_confirmedqty
ORDER by so.dd_SalesDocNo, so.dd_SalesItemNo, so.dd_ScheduleNo;

call vectorwise(combine 'cursor_tbl1_721');
 select 'D2',TIMESTAMP(LOCAL_TIMESTAMP);
 
update cursor_tbl1_721                    
set v_ConfQty = ifnull((select f.ct_ConfirmedQty
                              from fact_salesorder f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo 
                                    and f.dd_ItemRelForDelv = 'X'),0);
call vectorwise(combine 'cursor_tbl1_721');
                                   
update cursor_tbl1_721 
set v_GIDateMax = ifnull((select max(dt.DateValue) from fact_salesorderdelivery fd 
                                          INNER JOIN dim_Date dt ON fd.Dim_DateidPlannedGoodsIssue = dt.dim_dateid
                            where fd.dd_SalesDocNo = v_SalesDocNo and fd.dd_SalesItemNo = v_SalesItemNo and fd.dd_ScheduleNo = v_SchedNo), ANSIDATE(current_date));
update cursor_tbl1_721 
set v_MaxFactId = ifnull((select max(fact_salesorderdeliveryid) from fact_salesorderdelivery fd 
                                          INNER JOIN dim_Date dt ON fd.Dim_DateidPlannedGoodsIssue = dt.dim_dateid
                            where fd.dd_SalesDocNo = v_SalesDocNo and fd.dd_SalesItemNo = v_SalesItemNo and fd.dd_ScheduleNo = v_SchedNo and dt.datevalue = ANSIDATE(v_GIDateMax)), -1);

update cursor_tbl1_721
set v_TotConfQty =   ifnull((select SUM(f.ct_ConfirmedQty)
                              from fact_salesorderdelivery f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo),0);

call vectorwise(combine 'cursor_tbl1_721');
Update cursor_tbl1_721
Set v_ConfQty = v_TotConfQty - v_ConfQty
Where (v_ConfQty - v_TotConfQty) < 0
AND v_ConfQty <> (v_TotConfQty - v_ConfQty);

call vectorwise(combine 'cursor_tbl1_721');

  UPDATE fact_salesorderdelivery sod
From cursor_tbl1_721, dim_date dt 
    SET sod.ct_ConfirmedQty =  sod.ct_ConfirmedQty - v_ConfQty
  WHERE sod.dd_SalesDocNo = v_SalesDocNo and sod.dd_SalesItemNo = v_SalesItemNo and sod.dd_ScheduleNo = v_SchedNo
  AND dt.Datevalue = v_GIDateMax AND dt.dim_Dateid = sod.dim_DateIdPlannedGoodsIssue
AND  sod.ct_ConfirmedQty - v_ConfQty >= 0 
AND  sod.ct_ConfirmedQty <> ( sod.ct_ConfirmedQty - v_ConfQty)
AND v_MaxFactId = sod.fact_salesorderdeliveryid;
call vectorwise(combine 'fact_salesorderdelivery');

 select 'E',TIMESTAMP(LOCAL_TIMESTAMP);
/*##########PART 2 Ends###############################	 */

/*##########PART 3 Starts###############################	 */

   DROP TABLE IF EXISTS tmp_perf_cursor_tbl2_721_confqty;
CREATE TABLE tmp_perf_cursor_tbl2_721_confqty
AS
SELECT sod.dd_SalesDocNo ,sod.dd_SalesItemNo, sod.dd_ScheduleNo,
	   SUM(sod.ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit
FROM fact_salesorderdelivery sod
GROUP BY  sod.dd_SalesDocNo ,sod.dd_SalesItemNo, sod.dd_ScheduleNo;

Insert into cursor_tbl2_721(v_SalesDocNo,v_SalesItemNo,v_SchedNo)
select first 0 so.dd_Salesdocno, 
	so.dd_Salesitemno,
	so.dd_scheduleno 
from fact_salesorder so, tmp_perf_cursor_tbl2_721_confqty sod
WHERE sod.dd_SalesDocNo = so.dd_salesdocno and sod.dd_SalesItemNo = so.dd_salesitemno and sod.dd_ScheduleNo = so.dd_Scheduleno
AND so.ct_ScheduleQtySalesUnit > sod.sum_ct_ScheduleQtySalesUnit
ORDER by so.dd_SalesDocNo, so.dd_SalesItemNo, so.dd_ScheduleNo;
call vectorwise(combine 'cursor_tbl2_721');	   

Update cursor_tbl2_721
    set v_SchedQty = ifnull((select f.ct_ScheduleQtySalesUnit
                              from fact_salesorder f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo 
                                    and f.dd_ItemRelForDelv = 'X'),0);

call vectorwise(combine 'cursor_tbl2_721');

Update cursor_tbl2_721
    set v_GIDateMax = ifnull((select max(dt.DateValue) from fact_salesorderdelivery fd 
                                          INNER JOIN dim_Date dt ON fd.Dim_DateidPlannedGoodsIssue = dt.dim_dateid
                            where fd.dd_SalesDocNo = v_SalesDocNo and fd.dd_SalesItemNo = v_SalesItemNo and fd.dd_ScheduleNo = v_SchedNo), ANSIDATE(current_date));

call vectorwise(combine 'cursor_tbl2_721');
Update cursor_tbl2_721
set v_TotSchedQty =   ifnull((select SUM(f.ct_ScheduleQtySalesUnit)
                              from fact_salesorderdelivery f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo),0);

call vectorwise(combine 'cursor_tbl2_721');

Update cursor_tbl2_721
Set v_SchedQty = v_SchedQty - v_TotSchedQty
where (v_SchedQty - v_TotSchedQty) > 0
AND  v_SchedQty <> (v_SchedQty - v_TotSchedQty);

call vectorwise(combine 'cursor_tbl2_721');

  UPDATE fact_salesorderdelivery sod
From cursor_tbl2_721, dim_date dt 
    SET sod.ct_ScheduleQtySalesUnit =  sod.ct_ScheduleQtySalesUnit + v_SchedQty
  WHERE sod.dd_SalesDocNo = v_SalesDocNo and sod.dd_SalesItemNo = v_SalesItemNo and sod.dd_ScheduleNo = v_SchedNo
  AND dt.Datevalue = v_GIDateMax AND
  dt.dim_Dateid = sod.dim_DateIdPlannedGoodsIssue
AND  sod.ct_ScheduleQtySalesUnit <> (sod.ct_ScheduleQtySalesUnit + v_SchedQty);
  
call vectorwise(combine 'fact_salesorderdelivery');

/*##########PART 3 Ends###############################	 */

/*##########PART 4 Starts###############################	 */

UPDATE fact_salesorderdelivery sod
From  VBFA v
  SET sod.ct_QtyDelivered = CASE WHEN (sod.ct_QtyDelivered - v.vbfa_rfmng) < 0 THEN 0 ELSE (sod.ct_QtyDelivered - v.vbfa_rfmng) END
  WHERE sod.dd_SalesDlvrDocNo = v.vbfa_vbelv AND sod.dd_SalesDlvrItemNo = v.vbfa_posnv
      AND v.vbfa_bwart = '602' AND sod.ct_QtyDelivered > 0;
call vectorwise(combine 'fact_salesorderdelivery');

/* ##################################################################################################################*/
/* */
/*   Script         : bi_populate_so_shipment */
/*   Author         : Ashu */
/*   Created On     : 22 Jan 2013 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_so_shipment from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*   22 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise */
/*   20 Mar 2013     Lokesh	   1.1               Minor changes in logging, comments etc. Removed join with likp_lips  */
/*						 	to sync up with the current version on prod ( changes done by shanthi )  */
/* #################################################################################################################### */

select current_time;

DROP TABLE IF EXISTS tmp_ct_QtyDeliveredsummax;
CREATE TABLE tmp_ct_QtyDeliveredsummax
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,sum(f.ct_QtyDelivered) sum_ct_QtyDelivered,max(f.Dim_DateidDeliveryDate)  max_Dim_DateidDeliveryDate,
 max(f.Dim_DateidActualGoodsIssue) max_Dim_DateidActualGoodsIssue,
  min(f.Dim_DateidDlvrDocCreated) min_Dim_DateidDlvrDocCreated
FROM fact_salesorderdelivery f, dim_salesorderitemstatus sois 
where f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
and sois.GoodsMovementStatus <> 'Not yet processed'
GROUP BY dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo;

UPDATE fact_salesorder
SET ct_DeliveredQty = 0
WHERE ct_DeliveredQty <> 0;

UPDATE fact_salesorder so
FROM tmp_ct_QtyDeliveredsummax f
 SET ct_DeliveredQty = sum_ct_QtyDelivered
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
and f.dd_SalesItemNo = so.dd_SalesItemNo
and f.dd_ScheduleNo = so.dd_ScheduleNo;

call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder
SET Dim_DateidShipmentDelivery = 1
WHERE Dim_DateidShipmentDelivery <> 1;

UPDATE fact_salesorder so
FROM tmp_ct_QtyDeliveredsummax f
 SET Dim_DateidShipmentDelivery = max_Dim_DateidDeliveryDate
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
and f.dd_SalesItemNo = so.dd_SalesItemNo
and f.dd_ScheduleNo = so.dd_ScheduleNo
AND so.Dim_DateidShipmentDelivery <> f.max_Dim_DateidDeliveryDate;


call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder
SET Dim_DateidActualGI = 1
WHERE Dim_DateidActualGI <> 1;

UPDATE fact_salesorder so
FROM tmp_ct_QtyDeliveredsummax f
 SET Dim_DateidActualGI = max_Dim_DateidActualGoodsIssue
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
and f.dd_SalesItemNo = so.dd_SalesItemNo
and f.dd_ScheduleNo = so.dd_ScheduleNo
AND so.Dim_DateidActualGI <> f.max_Dim_DateidActualGoodsIssue;


call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder so
SET Dim_CustomeridShipTo = ifnull(( select Dim_CustomeridShipto from fact_Salesorderdelivery f,
                                   dim_salesorderitemstatus sois 
                           where  f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid AND f.dd_SalesDocNo = so.dd_SalesDocNo
                                and f.dd_SalesItemNo = so.dd_SalesItemNo
                                and f.dd_ScheduleNo = so.dd_ScheduleNo
                                and sois.GoodsMovementStatus <> 'Not yet processed' ),so.Dim_CustomerID)
WHERE so.Dim_CustomeridShipTo = 1;

call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder so
SET so.ct_AfsOnDeliveryQty = 0
WHERE so.ct_AfsOnDeliveryQty <> 0;

drop table if exists ct_group_update;
create table ct_group_update as
Select dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo, SUM(ct_QtyDelivered) ct_QtyDelivered
From fact_salesorderdelivery
where dd_ScheduleNo <> 0 AND Dim_DateidActualGI_Original = 1
Group by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo;

call vectorwise(combine 'ct_group_update');

UPDATE fact_salesorder so
  FROM ct_group_update l
SET so.ct_AfsOnDeliveryQty = l.ct_QtyDelivered
WHERE     so.dd_SalesDocNo = l.dd_SalesDocNo
AND so.dd_SalesItemNo = l.dd_SalesItemNo
AND so.dd_Scheduleno = l.dd_ScheduleNo;  
             
UPDATE  fact_salesorder so
SET  so.ct_AfsOnDeliveryQty = 0
WHERE  so.ct_AfsOnDeliveryQty IS NULL;

drop table if exists ct_group_update;

call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder
SET Dim_DateidDlvrDocCreated = 1
WHERE Dim_DateidDlvrDocCreated <> 1;

UPDATE fact_salesorder so
FROM tmp_ct_QtyDeliveredsummax f
 SET Dim_DateidDlvrDocCreated = min_Dim_DateidDlvrDocCreated
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
and f.dd_SalesItemNo = so.dd_SalesItemNo
and f.dd_ScheduleNo = so.dd_ScheduleNo;

call vectorwise(combine 'fact_salesorder');

DROP TABLE IF EXISTS tmp1_soshipmt;
CREATE TABLE tmp1_soshipmt
AS
SELECT dd_SalesDocNo,dd_SalesItemNo
FROM fact_salesorderdelivery sod inner join VBFA v on sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
WHERE v.vbfa_bwart = '602'
and not exists (select 1 from VBFA v1 where v1 .vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                                      and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat);

call vectorwise(combine 'tmp1_soshipmt');

 select 'F',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE fact_salesorder so
FROM dim_salesorderitemstatus s
SET ct_DeliveredQty = so.ct_ConfirmedQty
WHERE so.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      AND so.dd_ItemRelForDelv = 'X'
      AND s.OverallDeliveryStatus = 'Completely processed'
      AND so.ct_DeliveredQty < so.ct_ConfirmedQty
      AND not exists (select 1 from tmp1_soshipmt sod 
                      where sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo)
AND  ct_DeliveredQty <> so.ct_ConfirmedQty;
call vectorwise(combine 'fact_salesorder');

 select 'G',TIMESTAMP(LOCAL_TIMESTAMP);
                                     
DROP TABLE IF EXISTS tmp1_soshipmt;

DROP TABLE IF EXISTS tmp_CustomerPOAGIDate;

CREATE TABLE tmp_CustomerPOAGIDate as 
SELECT so.dd_CustomerPONo, max(dt.DateValue) AS LatestCustomPOAGI FROM fact_Salesorder so
INNER JOIN dim_Date dt ON dt.dim_dateid = so.Dim_DateidActualGI
WHERE so.dd_CustomerPONo <> 'Not Set' AND so.Dim_DateidActualGI <> 1 GROUP BY so.dd_CustomerPONo;


UPDATE fact_Salesorder so
	FROM dim_Date edt,
          dim_plant pl,
          tmp_CustomerPOAGIDate po
      SET so.Dim_DateIdLatestCustPOAGI = edt.Dim_Dateid
    WHERE     pl.Dim_Plantid = so.Dim_Plantid
          AND edt.companycode = pl.companycode
          AND edt.DateValue = po.LatestCustomPOAGI
          AND so.dd_CustomerPONo = po.dd_CustomerPONo
	  AND so.dd_CustomerPONo <> 'Not Set'
AND  so.Dim_DateIdLatestCustPOAGI<> edt.Dim_Dateid;


DROP TABLE IF EXISTS tmp_CustomerPOAGIDate;

Select 'process complete';
Select current_time;

/*   End of Script         : bi_populate_so_shipment */
/* ##################################################################################################################*/

Drop table if exists max_holder_721;
Create table max_holder_721(maxid)
as
Select ifnull(max(fact_salesorderdeliveryid),0)
from fact_salesorderdelivery;
call vectorwise(combine 'max_holder_721');
 select 'E2',TIMESTAMP(LOCAL_TIMESTAMP);


select 'Insert 2 on fact_salesorderdelivery',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS tmp_ins2_fs_ins;
CREATE TABLE tmp_ins2_fs_ins
AS
SELECT f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo
FROM fact_salesorder f;

DROP TABLE IF EXISTS tmp_ins2_fs_del;
CREATE TABLE tmp_ins2_fs_del
AS
SELECT f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo
FROM fact_salesorderdelivery f;


call vectorwise(combine 'tmp_ins2_fs_ins-tmp_ins2_fs_del');



INSERT INTO fact_salesorderdelivery(fact_salesorderdeliveryid,
					dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateIdBillingDate,
                                    Dim_BillingDocumentTypeId,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdSODocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    Dim_DateIdRejection,
				    dd_CustomerMaterial,
			    	    dim_materialpricegroup4id,
				    dim_materialpricegroup5id,
					dim_Currencyid_TRA,
					dim_Currencyid_GBL,
					dim_currencyid_STAT,
					amt_exchangerate_STAT,
					dd_BillOfLading,
					Dim_PartSalesId,
					Dim_AvailabilityCheckId,
					Dim_DeliveryPriorityId,
					Dim_IncoTermId,
					dd_InternaltionArticleNo,
					dd_ReleaseRule,dd_ArunNumber,Dim_salesofficeid
					)
   SELECT maxid + row_number() over(),f.dd_SalesDocNo,
          f.dd_SalesItemNo,
          f.dd_ScheduleNo,
          'Not Set' dd_SalesDlvrDocNo,
          0 dd_SalesDlvrItemNo,
          'Not Set' dd_MovementType,
          f.dd_AfsStockCategory,
          f.dd_CustomerPONo,
          'Not Set',
          0 ct_QtyDelivered,
          0 amt_Cost_DocCurr,
          0 amt_Cost,
          f.amt_SubTotal3 amt_SalesSubTotal3,
          f.amt_SubTotal4 amt_SalesSubTotal4,
          f.amt_AfsNetPrice amt_AfsNetPrice,
          f.amt_AfsNetValue amt_AfsNetValue,
          f.amt_BasePrice amt_BasePrice,
          IFNULL(f.amt_CustomerExpectedPrice,0) amt_CustomerExpectedPrice,
          IFNULL(f.amt_DicountAccrualNetPrice,0) amt_DicountAccrualNetPrice,
          f.ct_AfsOpenQty ct_AfsOpenQty,
          0 ct_FixedProcessDays,
          0 ct_ShipProcessDays,
          f.ct_ScheduleQtySalesUnit,
          f.ct_ConfirmedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidGoodsIssue,
          1,
          f.Dim_DateidSchedDelivery,
          f.Dim_DateidLoading,
          1 Dim_DateidPickingDate,
          1 Dim_DateidDlvrDocCreated,
          f.Dim_DateidMtrlAvail,
          f.Dim_CustomerID Dim_CustomeridSoldTo,
          f.Dim_CustomerID Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          1 Dim_DeliveryHeaderStatusid,
          1 Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
          f.Dim_DateidMtrlAvail,
          f.Dim_Currencyid,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1 Dim_ProfitCenterid,
          1 Dim_ControllingAreaId,
          f.Dim_BillingDateId Dim_DateIdBillingDate,
          1 Dim_BillingDocumentTypeId,
          f.Dim_DistributionChannelId Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1 Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2 Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId Dim_AFSSeasonId,
          f.Dim_AfsRejectionReasonId Dim_AfsRejectionReasonId,
          f.Dim_SalesOrderRejectReasonid Dim_SalesOrderRejectReasonid,
          1 Dim_DateidActualGI_Original,
          f.dd_AfsDepartment dd_AfsDepartment,
          f.ct_AfsAllocationRQty ct_ReservedQty,
          f.ct_AfsAllocationFQty ct_FixedQty,
          (CASE
              WHEN f.ct_ConfirmedQty > 0
              THEN
                 Decimal((f.amt_SubTotal3 / (case when f.ct_ConfirmedQty=0 then null else f.ct_ConfirmedQty end)),18,4)
              ELSE
                 0
           END)
             amt_SalesSubTotal3UnitPrice,
	   f.Dim_DateIdSODocument Dim_DateIdSODocument,
	   f.Dim_DateIdSOCreated Dim_DateIdSOCreated,
	   f.dd_CreatedBy dd_SDCreatedBy,
           f.Dim_DeliveryblockId,
	   f.ct_AfsUnallocatedQty,
	   ifnull(f.Dim_DateIdSOItemChangedOn,1),
	   f.Dim_ShippingConditionId,
	   f.dd_AfsAllocationGroupNo,
	   f.Dim_SalesOrderHeaderStatusId,
	   f.Dim_SalesOrderItemStatusId,
	   ct_AfsTotalDrawn,
	    ifnull(f.Dim_DateidSchedDelivery,1),
	    ifnull(f.Dim_DateIdRejection,1),
	    ifnull(dd_CustomerMaterialNo,'Not Set'),
	    dim_materialpricegroup4id,
	    dim_materialpricegroup5id,
		dim_Currencyid_TRA,
		dim_Currencyid_GBL,
		dim_currencyid_STAT,
		amt_exchangerate_STAT,
					'Not Set',
					f.Dim_PartSalesId,
					f.Dim_AvailabilityCheckId,
					1 Dim_DeliveryPriorityId,
					f.Dim_IncoTermId,
					f.dd_InternationalArticleNo,
					f.dd_ReleaseRule,f.dd_ArunNumber,f.Dim_salesofficeid
     FROM fact_salesorder f,max_holder_721,tmp_ins2_fs_ins f1
    WHERE f1.dd_SalesDocNo = f.dd_SalesDocNo
	AND  f1.dd_SalesItemNo = f.dd_SalesItemNo
	AND  f1.dd_ScheduleNo = f.dd_ScheduleNo;
call vectorwise(combine 'fact_salesorderdelivery');

 select 'H',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE       fact_salesorderdelivery sod
From likp_lips_vbuk v,dim_overallstatusforcreditcheck oscc
SET sod.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckID
Where sod.dd_SalesDlvrDocNo = v.VBUK_VBELN
AND  oscc.overallstatusforcreditcheck = v.VBUK_CMGST
AND  oscc.RowIsCurrent = 1
AND sod.Dim_OverallStatusCreditCheckId <> oscc.dim_overallstatusforcreditcheckID;
call vectorwise(combine 'fact_salesorderdelivery');
 select 'I',TIMESTAMP(LOCAL_TIMESTAMP);
 
 

 select 'J',TIMESTAMP(LOCAL_TIMESTAMP);

/* AFS Rescheduling fields */

call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorderdelivery sod
  FROM
       fact_salesorder so
 set sod.ct_AfsAllocatedQty = so.ct_AfsAllocatedQty
 WHERE sod.dd_Salesdocno = so.dd_SalesDocNo
 and sod.dd_SalesItemNo = so.dd_SalesItemNo
 and sod.dd_ScheduleNo = so.dd_ScheduleNo
 and sod.ct_AfsAllocatedQty <> so.ct_AfsAllocatedQty;

call vectorwise(combine 'fact_salesorderdelivery');
UPDATE fact_salesorderdelivery sod
  FROM dim_part dp,
  	   dim_afssize asiz,  	
       MEAN mn
SET sod.dd_UniversalProductCode = ifnull(mn.MEAN_EAN11,'Not Set')
WHERE sod.dim_partid = dp.dim_partid
AND sod.dim_Afssizeid = asiz.dim_AfsSizeId
AND dp.PartNumber = mn.MEAN_MATNR
AND mn.MEAN_EAN11 IS NOT NULL
AND mn.MEAN_J_3AKORDX = asiz.Size
AND dp.RowIsCurrent = 1
AND sod.dim_partid > 1;
UPDATE fact_salesorderdelivery sod
SET sod.dd_UniversalProductCode = 'Not Set'
WHERE sod.dd_UniversalProductCode IS NULL;
call vectorwise (combine 'fact_salesorderdelivery');
 select 'K',TIMESTAMP(LOCAL_TIMESTAMP);

 UPDATE fact_salesorderdelivery sod 
    FROM LIKP_LIPS lp, Dim_DeliveryType dt
 SET sod.Dim_DeliveryTypeId = dt.Dim_DeliveryTypeId
 WHERE sod.dd_SalesDlvrDocNo = lp.LIKP_VBELN
   AND sod.dd_SalesDlvrItemNo = lp.LIPS_POSNR
   AND dt.DeliveryType = lp.LIKP_LFART
   AND dt.RowIsCurrent = 1
   AND lp.LIKP_LFART IS NOT NULL;

UPDATE fact_salesorderdelivery sod
 SET sod.Dim_DeliveryTypeId = 1
WHERE sod.Dim_DeliveryTypeId IS NULL;

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery sod
  FROM VBAK_VBAP_VBEP lp, Dim_DeliveryPriority dp
SET sod.Dim_DeliveryPriorityId = dp.Dim_DeliveryPriorityId
WHERE sod.dd_SalesDocNo = lp.VBAK_VBELN
   AND sod.dd_SalesItemNo = lp.VBAP_POSNR
   AND sod.dd_ScheduleNo = lp.VBEP_ETENR
   AND dp.DeliveryPriority = lp.VBAP_LPRIO
   AND dp.RowIsCurrent = 1
   AND lp.vbap_LPRIO IS NOT NULL;
   
UPDATE fact_salesorderdelivery sod
  FROM VBAK_VBAP  lp, Dim_DeliveryPriority dp
SET sod.Dim_DeliveryPriorityId = dp.Dim_DeliveryPriorityId
WHERE sod.dd_SalesDocNo = lp.VBAP_VBELN
   AND sod.dd_SalesItemNo = lp.VBAP_POSNR
   AND sod.dd_ScheduleNo = 0
   AND dp.DeliveryPriority = lp.VBAP_LPRIO
   AND dp.RowIsCurrent = 1
   AND lp.vbap_LPRIO IS NOT NULL;   

UPDATE fact_salesorderdelivery sod
SET sod.Dim_DeliveryPriorityId = 1
WHERE sod.Dim_DeliveryPriorityId IS NULL; 

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery sod
SET sod.dim_MaterialPriceGroup4Id = 1
WHERE sod.dim_MaterialPriceGroup4Id IS NULL;

UPDATE fact_salesorderdelivery sod
SET sod.dim_MaterialPriceGroup5Id = 1
WHERE sod.dim_MaterialPriceGroup5Id IS NULL;

UPDATE fact_salesorderdelivery sod
SET sod.dim_DateIdAFSReqDelivery = 1
WHERE sod.dim_DateIdAFSReqDelivery IS NULL;

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.dd_AfsStockType = so.dd_AfsStockType
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.dd_AfsStockType,'Not Set') <> so.dd_AfsStockType; 

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.dim_MaterialPriceGroup4Id = so.dim_MaterialPriceGroup4Id
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.dim_MaterialPriceGroup4Id,1) <> so.dim_MaterialPriceGroup4Id; 

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.dim_MaterialPriceGroup5Id = so.dim_MaterialPriceGroup5Id
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.dim_MaterialPriceGroup5Id,1) <> so.dim_MaterialPriceGroup5Id; 

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.ct_AfsAllocatedQty = so.ct_AfsAllocatedQty
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.ct_AfsAllocatedQty,0.0000) <> so.ct_AfsAllocatedQty; 

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.ct_AfsTotalDrawn = f.ct_AfsTotalDrawn
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.ct_AfsTotalDrawn <> f.ct_AfsTotalDrawn;

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE  fact_salesorderdelivery sod
 SET sod.ct_AfsOpenQty = 0
WHERE sod.ct_AfsOpenQty <> 0;
 
UPDATE  fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.ct_AfsOpenQty = (CASE WHEN so.ct_AfsOpenQty > sod.ct_QtyDelivered THEN sod.ct_QtyDelivered ELSE so.ct_AfsOpenQty END)
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND sod.ct_ConfirmedQty = sod.ct_QtyDelivered
AND sod.dim_DateIdActualGI_Original = 1
AND sod.dd_SalesDlvrDocNo <> 'Not Set' 
AND ifnull(sod.ct_AfsOpenQty,-1) <> ifnull((CASE WHEN so.ct_AfsOpenQty > sod.ct_QtyDelivered THEN sod.ct_QtyDelivered ELSE so.ct_AfsOpenQty END),-2); 

UPDATE  fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.ct_AfsOpenQty = (CASE WHEN so.ct_AfsOpenQty > sod.ct_Confirmedqty THEN sod.ct_Confirmedqty ELSE so.ct_AfsOpenQty END)
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND sod.ct_ConfirmedQty > sod.ct_QtyDelivered
AND sod.dim_DateIdActualGI_Original = 1
AND sod.dd_SalesDlvrDocNo <> 'Not Set' 
AND ifnull(sod.ct_AfsOpenQty,-1) <> ifnull((CASE WHEN so.ct_AfsOpenQty > sod.ct_Confirmedqty THEN sod.ct_Confirmedqty ELSE so.ct_AfsOpenQty END),-2); 

UPDATE  fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.ct_AfsOpenQty = so.ct_AfsOpenQty
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND sod.ct_ConfirmedQty < sod.ct_QtyDelivered
AND sod.dim_DateIdActualGI_Original = 1
AND sod.dd_SalesDlvrDocNo <> 'Not Set' 
AND ifnull(sod.ct_AfsOpenQty,-1) <> ifnull(so.ct_AfsOpenQty,-2); 

UPDATE  fact_salesorderdelivery sod
FROM fact_salesorder so, dim_salesorderitemstatus sois
 SET sod.ct_AfsOpenQty = (sod.ct_ConfirmedQty - sod.ct_QtyDelivered)
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
and sod.dim_Deliveryitemstatusid = sois.dim_salesorderitemstatusid
AND sod.dim_DateIdActualGI_Original <> 1
AND (sod.ct_ConfirmedQty - sod.ct_QtyDelivered) <= so.ct_AfsOpenQty
AND sod.dd_SalesDlvrDocNo <> 'Not Set' 
AND ifnull(sod.ct_AfsOpenQty,-1) <> ifnull(so.ct_AfsOpenQty,-2); 
 
UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.ct_AfsOpenQty = so.ct_AfsOpenQty
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND sod.dd_SalesDlvrDocNo = 'Not Set'
AND ifnull(sod.ct_AfsOpenQty,0.0000) <> ifnull(so.ct_AfsOpenQty,-2); 

call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder t
SET t.ct_AfsCalculatedOpenQty  = 0
WHERE t.ct_AfsCalculatedOpenQty <> 0;

UPDATE fact_salesorder t
 FROM dim_documentcategory dc
SET t.ct_AfsCalculatedOpenQty  = (t.ct_ConfirmedQty - t.ct_AfsTotalDrawn)
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory in ('G','g','B','b') 
AND t.Dim_AfsRejectionReasonId = 1 
AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0);

UPDATE fact_salesorder t
 FROM dim_documentcategory dc
SET t.ct_AfsCalculatedOpenQty  = (t.ct_AfsOpenQty - t.ct_AfsOnDeliveryQty)
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory NOT IN ('G','g','B','b') 
AND t.Dim_AfsRejectionReasonId = 1 
AND t.ct_AfsOnDeliveryQty >= 0
AND ((t.ct_AfsOpenQty - t.ct_AfsOnDeliveryQty) >= 0);

UPDATE fact_salesorder t
 FROM dim_documentcategory dc
SET t.ct_AfsCalculatedOpenQty  = t.ct_AfsOpenQty
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory NOT IN ('G','g','B','b') 
AND t.Dim_AfsRejectionReasonId = 1 
AND t.Dim_DateIdActualGI = 1
AND t.ct_AfsOnDeliveryQty = 0;

UPDATE fact_salesorder t
SET t.ct_AfsCalculatedOpenQty  = 0
WHERE t.ct_AfsCalculatedOpenQty IS NULL;

call vectorwise(combine 'fact_salesorder');   

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery t
SET t.ct_AfsCalculatedOpenQty  = 0
WHERE t.ct_AfsCalculatedOpenQty  <> 0;

UPDATE fact_salesorderdelivery t
 FROM dim_documentcategory dc
SET t.ct_AfsCalculatedOpenQty  = (t.ct_ConfirmedQty - t.ct_AfsTotalDrawn)
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory in ('G','g','B','b') 
AND t.Dim_AfsRejectionReasonId = 1 
AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0);

UPDATE fact_salesorderdelivery t
 FROM dim_documentcategory dc
SET t.ct_AfsCalculatedOpenQty  = (t.ct_AfsOpenQty - t.ct_QtyDelivered)
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory NOT IN ('G','g','B','b') 
AND t.Dim_DateidActualGI_Original = 1 
AND t.Dim_AfsRejectionReasonId = 1 
AND ((t.ct_AfsOpenQty - t.ct_QtyDelivered) >= 0);

UPDATE fact_salesorderdelivery t
 FROM dim_documentcategory dc
SET t.ct_AfsCalculatedOpenQty  = t.ct_AfsOpenQty
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
AND dc.DocumentCategory NOT IN ('G','g','B','b') 
AND t.Dim_DateidActualGI_Original <> 1 
AND t.Dim_AfsRejectionReasonId = 1 
AND ((t.ct_AfsOpenQty - (t.ct_ConfirmedQty - t.ct_QtyDelivered)) >= 0);

UPDATE fact_salesorderdelivery t
SET t.ct_AfsCalculatedOpenQty  = 0
WHERE t.ct_AfsCalculatedOpenQty IS NULL;

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
 SET sod.ct_AfsUnallocatedQty = so.ct_AfsUnallocatedQty
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.ct_AfsUnallocatedQty,0.0000) <> so.ct_AfsUnallocatedQty; 

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.amt_SalesSubTotal3UnitPrice = Decimal((so.amt_SubTotal3 / (case when so.ct_ConfirmedQty = 0 then null else so.ct_ConfirmedQty end)),18,4)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.ct_ConfirmedQty > 0
AND ifnull(sod.amt_SalesSubTotal3UnitPrice,1) <> ifnull(Decimal((so.amt_SubTotal3 / (case when so.ct_ConfirmedQty = 0 then null else so.ct_ConfirmedQty end)),18,4),-1);

/* UPDATE fact_salesorderdelivery sod
 FROM EDIDS ed
SET dd_IDocStatus = ifnull(EDIDS_STATUS,'Not Set')
WHERE sod.dd_SalesDocNo = ed.EDIDS_STAPA2
  AND ed.EDIDS_STAMNO = 311 */

UPDATE fact_salesorderdelivery sod
SET dd_IDocStatus =  'Not Set'
WHERE dd_IDocStatus IS NULL;

/* UPDATE fact_salesorderdelivery sod
 FROM VEKP_VEPO ed
SET sod.dd_TrackingNo = ifnull(ed.VEKP_INHALT, 'Not Set')
WHERE sod.dd_SalesDlvrDocNo = ed.VEPO_VBELN */


UPDATE fact_salesorderdelivery sod
SET dd_TrackingNo =  'Not Set'
WHERE dd_TrackingNo IS NULL;

call vectorwise(combine 'fact_salesorderdelivery');

drop table if exists ct_group_update;
create table ct_group_update as select J_3ABDBS_AUFNR,J_3ABDBS_POSNR,J_3ABDBS_ETENR,J_3ABDBS_J_3ASTAT,sum(J_3ABDBS_MENGE) J_3ABDBS_MENGE
from j_3abdbs
WHERE J_3ABDBS_J_3ASTAT = 'D'
group by  J_3ABDBS_AUFNR,J_3ABDBS_POSNR,J_3ABDBS_ETENR,J_3ABDBS_J_3ASTAT;

UPDATE fact_salesorderdelivery so
  FROM ct_group_update ast
SET so.ct_AfsDeliveredQty = ast.J_3ABDBS_MENGE  
WHERE     ast.J_3ABDBS_AUFNR = so.dd_SalesDocNo
AND ast.J_3ABDBS_POSNR = so.dd_SalesItemNo
AND ast.J_3ABDBS_ETENR = so.dd_ScheduleNo;

drop table if exists ct_group_update;

call vectorwise(combine 'fact_salesorderdelivery');

select 'L',TIMESTAMP(LOCAL_TIMESTAMP);

/*##########PART 4 Ends###############################	 */

/*##########PART 5 Starts###############################	 */
/*
select 'Calling VW_bi_populate_so_shippedAgainstOrderQty.sql',TIMESTAMP(LOCAL_TIMESTAMP)

\i /home/fusionops/ispring/db/scripts/VW_bi_populate_so_shippedAgainstOrderQty.sql

select 'Calling VW_bi_populate_salesorder_coveredqty.sql',TIMESTAMP(LOCAL_TIMESTAMP)

\i /home/fusionops/ispring/db/scripts/VW_bi_populate_salesorder_coveredqty.sql

select 'Calling VW_bi_populate_salesorder_fillrate.sql',TIMESTAMP(LOCAL_TIMESTAMP)

\i /home/fusionops/ispring/db/scripts/VW_bi_populate_salesorder_fillrate.sql
*/
/*##########PART 5 Ends###############################	 */

/*##########PART 6 Starts###############################	 */

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery sod
From  fact_billing fb, dim_documentcategory bdt, dim_billingmisc bm
    SET sod.dd_billing_no = fb.dd_billing_no,
        sod.dd_Billingitem_no = fb.dd_billing_item_no 
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
	AND fb.dim_billingmiscid = bm.dim_billingmiscid
	AND bm.CancelFlag <> 'X'
	AND fb.dim_documentcategoryid = bdt.dim_documentcategoryid
	AND  bdt.DocumentCategory IN ('M');

 DROP TABLE IF EXISTS tmp_ct_BilledQtyAtSchedule_sum;
 CREATE TABLE tmp_ct_BilledQtyAtSchedule_sum
 AS
 SELECT dd_SalesDlvrDocNo,dd_SalesDlvrItemNo,dd_salesdocno,dd_salesitemno,dd_AfsScheduleNo,SUM(bil.ct_BilledQtyAtSchedule) sum_ct_BilledQtyAtSchedule,SUM(bil.amt_CustomerConfigSubtotal4+bil.amt_CustomerConfigSubtotal3) AS Subtotal34
 ,SUM(bil.amt_CustomerConfigSubtotal3) AS Subtotal3
 FROM fact_billing bil
INNER JOIN dim_DocumentCategory bdt ON bdt.dim_DocumentCategoryid = bil.dim_DocumentCategoryid
INNER JOIN dim_billingmisc bm ON bm.Dim_BillingMiscId = bil.Dim_BillingMiscId
WHERE bdt.documentCategory = 'M'
AND bm.CancelFlag <> 'X'
GROUP BY dd_SalesDlvrDocNo,dd_SalesDlvrItemNo,dd_salesdocno,dd_salesitemno,dd_AfsScheduleNo;

DROP TABLE IF EXISTS tmp_ItemUpdateShippedOrBilledQty;

CREATE TABLE tmp_ItemUpdateShippedOrBilledQty AS 
SELECT dd_SalesDocNo, dd_SalesItemNo, sum(fb.ct_BilledQtyAtSchedule) as BilledQty,SUM(fb.amt_CustomerConfigSubtotal4+amt_CustomerConfigSubtotal3) AS Subtotal34,SUM(amt_CustomerConfigSubtotal3) AS Subtotal3
FROM fact_billing fb, Dim_BillingMisc bmisc, dim_documentcategory bdt
    WHERE     fb.dim_documentcategoryid = bdt.dim_documentcategoryid
          AND fb.Dim_BillingMiscId = bmisc.Dim_BillingMiscId
          AND bmisc.CancelFlag = 'Not Set'
          AND bdt.documentcategory in ('M')
          group by dd_SalesDocNo, dd_SalesItemNo;


 /* Slow query - Tuned LK 25 Aug */
 UPDATE fact_salesorderdelivery sod
 SET ct_AfsBilledQty = 0
 WHERE ct_AfsBilledQty <> 0;
   
 UPDATE fact_salesorderdelivery sod
  FROM tmp_ct_BilledQtyAtSchedule_sum bil,tmp_ItemUpdateShippedOrBilledQty ti
   SET sod.ct_AfsBilledQty = bil.sum_ct_BilledQtyAtSchedule
WHERE  sod.dd_SalesDlvrDocNo = bil.dd_SalesDlvrDocNo
AND sod.dd_SalesDlvrItemNo = bil.dd_SalesDlvrItemNo
AND sod.dd_salesdocno = bil.dd_salesdocno
AND sod.dd_SalesItemNo = bil.dd_salesitemno
AND sod.dd_ScheduleNo = bil.dd_AfsScheduleNo
AND sod.dd_salesdocno = ti.dd_salesdocno
AND sod.dd_SalesItemNo = ti.dd_salesitemno
AND ct_AfsBilledQty <> sum_ct_BilledQtyAtSchedule;

  UPDATE fact_salesorderdelivery sod
	FROM tmp_ct_BilledQtyAtSchedule_sum bil,tmp_ItemUpdateShippedOrBilledQty ti
      SET sod.amt_BillingCustomerConfigSubtotal4  = ti.Subtotal34/(CASE WHEN ti.BilledQty <> 0 THEN ti.BilledQty else null end)
WHERE 	sod.dd_SalesDlvrDocNo = bil.dd_SalesDlvrDocNo
AND sod.dd_SalesDlvrItemNo = bil.dd_SalesDlvrItemNo
AND sod.dd_salesdocno = bil.dd_salesdocno
AND sod.dd_SalesItemNo = bil.dd_salesitemno
AND sod.dd_ScheduleNo = bil.dd_AfsScheduleNo
AND sod.dd_salesdocno = ti.dd_salesdocno
AND sod.dd_SalesItemNo = ti.dd_salesitemno
AND sod.amt_BillingCustomerConfigSubtotal4 <> ti.Subtotal34/(CASE WHEN ti.BilledQty <> 0 THEN ti.BilledQty else null end);
	
UPDATE fact_salesorderdelivery sod
	FROM tmp_ct_BilledQtyAtSchedule_sum bil,tmp_ItemUpdateShippedOrBilledQty ti
      SET sod.amt_BillingCustConfigSubtotal3UnitPrice  = ifnull(ti.Subtotal3/(CASE WHEN   ti.BilledQty <> 0 THEN  ti.BilledQty else null end),0)
WHERE 	sod.dd_SalesDlvrDocNo = bil.dd_SalesDlvrDocNo
AND sod.dd_SalesDlvrItemNo = bil.dd_SalesDlvrItemNo
AND sod.dd_salesdocno = bil.dd_salesdocno
AND sod.dd_SalesItemNo = bil.dd_salesitemno
AND sod.dd_ScheduleNo = bil.dd_AfsScheduleNo
AND sod.dd_salesdocno = ti.dd_salesdocno
AND sod.dd_SalesItemNo = ti.dd_salesitemno
AND sod.amt_BillingCustConfigSubtotal3UnitPrice <> ifnull(ti.Subtotal3/(CASE WHEN   ti.BilledQty <> 0 THEN  ti.BilledQty else null end),0);


DROP TABLE IF EXISTS tmp_ItemUpdateShippedOrBilledQty;
 DROP TABLE IF EXISTS tmp_ct_BilledQtyAtSchedule_sum;	
call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
	SET sod.Dim_SalesDistrictId = so.Dim_SalesDistrictId
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo and sod.dd_ScheduleNo = so.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
	SET sod.dd_ReferenceDocNo = ifnull(so.dd_ReferenceDocumentNo,'Not Set')
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo and sod.dd_ScheduleNo = so.dd_ScheduleNo
 AND ifnull(sod.dd_ReferenceDocNo,'X') <> ifnull(so.dd_ReferenceDocumentNo,'Not Set');

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
	SET sod.dd_ReferenceDocItemNo = ifnull(so.dd_ReferenceDocItemNo,0)
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo and sod.dd_ScheduleNo = so.dd_ScheduleNo
 AND ifnull(sod.dd_ReferenceDocItemNo,-1) = ifnull(so.dd_ReferenceDocItemNo,-2);

UPDATE       fact_salesorderdelivery sod
FROM fact_salesorder so
set sod.Dim_AccountAssignmentGroupId = so.Dim_AccountAssignmentGroupId
Where sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo and sod.dd_ScheduleNo = so.dd_ScheduleNo
and ifnull(sod.Dim_AccountAssignmentGroupId,-1) <> ifnull(so.Dim_AccountAssignmentGroupId,1) ;

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
	SET sod.dd_CustomerPONo = ifnull(so.dd_CustomerPONo,'Not Set')
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo and sod.dd_ScheduleNo = so.dd_ScheduleNo
 AND ifnull(sod.dd_CustomerPONo,'X') <> ifnull(so.dd_CustomerPONo,'Not Set');

UPDATE       fact_salesorderdelivery sod
FROM fact_salesorder so
set sod.Dim_AfsRejectionReasonId = so.Dim_AfsRejectionReasonId
Where sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo and sod.dd_ScheduleNo = so.dd_ScheduleNo
and ifnull(sod.Dim_AfsRejectionReasonId,-1) <> ifnull(so.Dim_AfsRejectionReasonId,1) ;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.amt_Cost =
            Decimal((sd.amt_Cost_DocCurr),18,4)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
	and sd.amt_Cost <>
            Decimal((sd.amt_Cost_DocCurr),18,4);


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET  sd.ct_PriceUnit = f.ct_PriceUnit
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.ct_PriceUnit<>  f.ct_PriceUnit;


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET 	sd.amt_UnitPrice = f.amt_UnitPrice, sd.amt_UnitPriceUoM = f.amt_UnitPriceUoM
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  ifnull(sd.amt_UnitPrice,-1) <> ifnull(f.amt_UnitPrice,-2);


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET   sd.amt_ExchangeRate = f.amt_ExchangeRate
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.amt_ExchangeRate <> f.amt_ExchangeRate;
        

  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET 	sd.amt_ExchangeRate_GBL = ifnull(f.amt_ExchangeRate_GBL,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.amt_ExchangeRate_GBL <>  ifnull(f.amt_ExchangeRate_GBL,1);

/* LK: 8 Sep 2013 : Curr and exchg rate chgs */
  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET         sd.dim_Currencyid_TRA = ifnull(f.dim_Currencyid_TRA,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.dim_Currencyid_TRA <>  ifnull(f.dim_Currencyid_TRA,1);


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET         sd.dim_Currencyid_GBL = ifnull(f.dim_Currencyid_GBL,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.dim_Currencyid_GBL <>  ifnull(f.dim_Currencyid_GBL,1);

 UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET         sd.dim_Currencyid = ifnull(f.dim_Currencyid,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.dim_Currencyid <>  ifnull(f.dim_Currencyid,1);


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET         sd.dim_currencyid_STAT = ifnull(f.dim_currencyid_STAT,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.dim_currencyid_STAT <>  ifnull(f.dim_currencyid_STAT,1);


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET         sd.amt_exchangerate_STAT = ifnull(f.amt_exchangerate_STAT,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.amt_exchangerate_STAT <>  ifnull(f.amt_exchangerate_STAT,1);

/* LK: 8 Sep 2013 : End of curr and exchg rate changes */

  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET  sd.Dim_DateidSalesOrderCreated = ifnull(f.Dim_DateidSalesOrderCreated,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_DateidSalesOrderCreated,-2) <>  ifnull(f.Dim_DateidSalesOrderCreated ,-1);


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET         sd.Dim_DateidSchedDeliveryReq = f.Dim_DateidSchedDeliveryReq
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  ifnull(sd.Dim_DateidSchedDeliveryReq,-1) <> ifnull(f.Dim_DateidSchedDeliveryReq,1);


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_DateidSchedDlvrReqPrev = f.Dim_DateidSchedDlvrReqPrev
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  ifnull(sd.Dim_DateidSchedDlvrReqPrev,-1) <> ifnull(f.Dim_DateidSchedDlvrReqPrev,-2);


          UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_DateidMatlAvailOriginal = f.Dim_DateidMtrlAvail
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_DateidMatlAvailOriginal,-1) <> ifnull(f.Dim_DateidMtrlAvail,-2);


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_DateidFirstDate = f.Dim_DateidFirstDate
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_DateidFirstDate,-1) <> ifnull(f.Dim_DateidFirstDate,-2);


  UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_Currencyid = f.Dim_Currencyid
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_Currencyid,-1) <> ifnull(f.Dim_Currencyid,-2);


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_Companyid = f.Dim_Companyid
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_Companyid,-1) <> ifnull(f.Dim_Companyid,-2);


          UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_SalesDivisionid = f.Dim_SalesDivisionid
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_SalesDivisionid,-1) <> ifnull(f.Dim_SalesDivisionid,-2);


          UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_ShipReceivePointid = f.Dim_ShipReceivePointid
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_ShipReceivePointid,-1) <> ifnull(f.Dim_ShipReceivePointid,-2);


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_DocumentCategoryid,-1) <> ifnull(f.Dim_DocumentCategoryid,-2);

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET  sd.Dim_SalesDocumentTypeid = ifnull(f.Dim_SalesDocumentTypeid,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_SalesDocumentTypeid,-1) <> ifnull(f.Dim_SalesDocumentTypeid,-2);


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_SalesOrgid = f.Dim_SalesOrgid
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_SalesOrgid <> f.Dim_SalesOrgid ;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_SalesGroupid = ifnull(f.Dim_SalesGroupid,1)
  WHERE     sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_SalesGroupid,-1) <> ifnull(f.Dim_SalesGroupid,-2);

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_CostCenterid = ifnull(f.Dim_CostCenterid,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  ifnull(sd.Dim_CostCenterid,-1) <> ifnull(f.Dim_CostCenterid,-2);


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_BillingBlockid = ifnull(f.Dim_BillingBlockid,1)
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND ifnull(sd.Dim_BillingBlockid,-1) <>  ifnull(f.Dim_BillingBlockid,-1);


          UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_TransactionGroupid = ifnull(f.Dim_TransactionGroupid,1)
 WHERE  sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  ifnull(sd.Dim_TransactionGroupid,-1) <> ifnull(f.Dim_TransactionGroupid,-2);

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET  sd.Dim_CustomeridSoldTo =
            CASE sd.Dim_CustomeridSoldTo
              WHEN 1 THEN f.Dim_CustomerID
              ELSE sd.Dim_CustomeridSoldTo
            END
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_CustomerGroup1id = f.Dim_CustomerGroup1id
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.Dim_CustomerGroup1id <>  f.Dim_CustomerGroup1id;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET  sd.Dim_CustomerGroup2id = f.Dim_CustomerGroup2id
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_CustomerGroup2id <> f.Dim_CustomerGroup2id;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.dim_salesorderitemcategoryid = f.dim_salesorderitemcategoryid
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND sd.dim_salesorderitemcategoryid  <> f.dim_salesorderitemcategoryid;


UPDATE fact_salesorderdelivery sd
From  fact_salesorder f
    SET sd.Dim_ScheduleLineCategoryId = f.Dim_ScheduleLineCategoryId
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.Dim_ScheduleLineCategoryId <> f.Dim_ScheduleLineCategoryId;

UPDATE fact_salesorderdelivery sd
From  fact_salesorder f, VBAK_VBAP_VBEP v
    SET sd.dd_ReferenceDocNo = f.dd_ReferenceDocumentNo
  WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
AND  sd.dd_ReferenceDocNo <> f.dd_ReferenceDocumentNo;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_AfsAllocationGroupNo = so.dd_AfsAllocationGroupNo
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.dd_AfsAllocationGroupNo <> so.dd_AfsAllocationGroupNo;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_ShippingConditionId = ifnull(so.Dim_ShippingConditionId,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_ShippingConditionId, -1) <> ifnull(so.Dim_ShippingConditionId, -2);

call vectorwise  (combine 'fact_salesorder');
call vectorwise  (combine 'fact_salesorderdelivery');

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_DateIdSOItemChangedOn = ifnull(so.Dim_DateIdSOItemChangedOn,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.Dim_DateIdSOItemChangedOn <> ifnull(so.Dim_DateIdSOItemChangedOn,1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_SalesOrderItemStatusId = ifnull(so.Dim_SalesOrderItemStatusId,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_SalesOrderItemStatusId,1) <> ifnull(so.Dim_SalesOrderItemStatusId,1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_SalesOrderHeaderStatusId = ifnull(so.Dim_SalesOrderHeaderStatusId,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_SalesOrderHeaderStatusId,1) <> ifnull(so.Dim_SalesOrderHeaderStatusId,1);
 
update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_CustomerMaterial = ifnull(so.dd_CustomerMaterialNo,'Not Set')
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.dd_CustomerMaterial <> ifnull(so.dd_CustomerMaterialNo,'Not Set');

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_DeliveryBlockId = ifnull(so.Dim_DeliveryBlockId,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_DeliveryBlockId,-1) <> ifnull(so.Dim_DeliveryBlockId,-2);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_OverallStatusCreditCheckId = ifnull(so.Dim_OverallStatusCreditCheckId,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND sod.Dim_OverallStatusCreditCheckId <> so.Dim_OverallStatusCreditCheckId;

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_MaterialPriceGroup4Id = ifnull(so.Dim_MaterialPriceGroup4Id,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup4Id,1) <> ifnull(so.Dim_MaterialPriceGroup4Id,-2);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_MaterialPriceGroup5Id = ifnull(so.Dim_MaterialPriceGroup5Id,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup5Id,1) <> ifnull(so.Dim_MaterialPriceGroup5Id,-2);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_SalesDistrictId = so.Dim_SalesDistrictId
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_SalesDistrictId,1) <> ifnull(so.Dim_SalesDistrictId,-1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_PartSalesId = so.Dim_PartSalesId
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_PartSalesId,1) <> ifnull(so.Dim_PartSalesId,-1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_AvailabilityCheckId = so.Dim_AvailabilityCheckId
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_AvailabilityCheckId,1) <> ifnull(so.Dim_AvailabilityCheckId,-1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_IncoTermId = so.Dim_IncoTermId
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_IncoTermId,1) <> ifnull(so.Dim_IncoTermId,-1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_InternaltionArticleNo = so.dd_InternationalArticleNo
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.dd_InternaltionArticleNo,'Not Set') <> ifnull(so.dd_InternationalArticleNo,'X');

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_ReleaseRule = so.dd_ReleaseRule
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.dd_ReleaseRule,'Not Set') <> ifnull(so.dd_ReleaseRule,'X');
					
update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_ArunNumber = ifnull(so.dd_ArunNumber,'Not Set')
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.dd_ArunNumber,'Not Set') <> ifnull(so.dd_ArunNumber,'X');

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_AfsStockCategory = ifnull(so.dd_AfsStockCategory,'Not Set')
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.dd_AfsStockCategory,'Not Set') <> ifnull(so.dd_AfsStockCategory,'X');

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.dd_AfsStockType = ifnull(so.dd_AfsStockType,'Not Set')
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.dd_AfsStockType,'Not Set') <> ifnull(so.dd_AfsStockType,'X');


update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_DateIdAfsReqDelivery = ifnull(so.Dim_DateIdAfsReqDelivery,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_DateIdAfsReqDelivery,1) <> ifnull(so.Dim_DateIdAfsReqDelivery,-1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.Dim_salesofficeid = ifnull(so.Dim_salesofficeid,1)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_salesofficeid,1) <> ifnull(so.Dim_salesofficeid,-1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.amt_SalesSubtotal3 = ifnull(so.amt_Subtotal3,0)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.amt_SalesSubtotal3,1) <> ifnull(so.amt_Subtotal3,-1);

update fact_salesorderdelivery sod
 from fact_salesorder so
SET sod.amt_SalesSubtotal4 = ifnull(so.amt_Subtotal4,0)
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.amt_SalesSubtotal4,1) <> ifnull(so.amt_Subtotal4,-1);

call vectorwise(combine 'fact_salesorderdelivery');
/* ##########PART 6 Ends. Proc Ends ###############################	 */


/* changes 26 Sep 2013 */	
/*##########PART 7 Starts###############################	 */
 UPDATE fact_salesorderdelivery sod 
    FROM LIKP_LIPS lp
 SET dd_SDCreateTime = ifnull(LIKP_ERZET, '000000'),
	dd_DeliveryTime = ifnull(LIKP_LFUHR, '000000'),
	dd_PickingTime = ifnull(LIKP_KOUHR, '000000'),
	dd_GITime = ifnull(LIKP_WAUHR, '000000'),
	dd_SDLineCreateTime	= ifnull(LIPS_ERZET, '000000')	
 WHERE sod.dd_SalesDlvrDocNo = lp.LIKP_VBELN
   AND sod.dd_SalesDlvrItemNo = lp.LIPS_POSNR;	

 UPDATE fact_salesorderdelivery sod
 SET dd_SDCreateTime = '000000',
	dd_DeliveryTime = '000000',
	dd_PickingTime = '000000',
	dd_GITime = '000000',
	dd_SDLineCreateTime	= '000000'
WHERE dd_SDCreateTime IS NULL OR dd_DeliveryTime IS NULL
OR dd_PickingTime IS NULL OR dd_GITime IS NULL OR dd_SDLineCreateTime IS NULL;
	
call vectorwise(combine 'fact_salesorderdelivery');
/* ##########PART 7 Ends. Proc Ends ###############################	 */

/* Start Changes 12 Feb 2014 */

UPDATE fact_salesorderdelivery sod
FROM  fact_salesorder so
 set sod.dim_scheduledeliveryblockid = so.dim_scheduledeliveryblockid
 WHERE sod.dd_Salesdocno = so.dd_SalesDocNo
 and sod.dd_SalesItemNo = so.dd_SalesItemNo
 and sod.dd_ScheduleNo = so.dd_ScheduleNo
 and ifnull(sod.dim_scheduledeliveryblockid,1) <> so.dim_scheduledeliveryblockid;

UPDATE fact_salesorderdelivery sod SET sod.dim_scheduledeliveryblockid = 1 WHERE sod.dim_scheduledeliveryblockid is NULL;

/* End Changes 12 Feb 2014 */

/* Start Changes 14 Feb 2014 */

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
SET sod.Dim_CustomerGroup4id = ifnull(so.Dim_CustomerGroup4id,1)
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.Dim_CustomerGroup4id,1) <> so.Dim_CustomerGroup4id;
update fact_salesorderdelivery set Dim_CustomerGroup4id = 1 where Dim_CustomerGroup4id is NULL;

/* END Changes 14 Feb 2014 */

 select 'M',TIMESTAMP(LOCAL_TIMESTAMP);

select 'Process Complete';
select current_time;

DROP TABLE IF EXISTS tmp_perf_cursor_tbl1_721;
DROP TABLE IF EXISTS tmp_perf_cursor_tbl2_721_confqty;


DROP TABLE IF EXISTS tmp_delstg721_fact_salesorderdelivery_ins;
DROP TABLE IF EXISTS tmp_delstg721_fact_salesorderdelivery_del;
DROP TABLE IF EXISTS tmp_delstg721_fso_ins;
DROP TABLE IF EXISTS tmp_delstg721_fso_del;
DROP TABLE IF EXISTS tmp_perf_cursor_tbl1_721;
 DROP TABLE IF EXISTS tmp_perf_cursor_tbl2_721_confqty;
DROP TABLE IF EXISTS tmp_ct_QtyDeliveredsummax;
DROP TABLE IF EXISTS tmp_ins2_fs_ins;
DROP TABLE IF EXISTS tmp_ins2_fs_del;
 DROP TABLE IF EXISTS tmp_ct_BilledQtyAtSchedule_sum;
DROP TABLE IF EXISTS tmp_perf_cursor_tbl2_721_confqty;

 select 'END OF PROC VW_bi_populate_afs_salesorderdelivery_fact',TIMESTAMP(LOCAL_TIMESTAMP);
