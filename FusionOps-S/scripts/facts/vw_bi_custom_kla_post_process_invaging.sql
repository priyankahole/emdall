/*********************************************************************
Last update date : July 26, 2012
Last updated by : Hiten
/*  15 Sep 2013   1.1        Lokesh	 Currency changes				*/
**********************************************************************/

select 'A - START OF PROC bi_custom_kla_post_process_invaging',TIMESTAMP(LOCAL_TIMESTAMP);

Update NUMBER_FOUNTAIN set max_id = ifnull((select ifnull(max(processinglogid),0) + 1 from processinglog),1)
where table_name = 'processinglog';

INSERT INTO processinglog ( processinglogid, referencename, startdate,description)
SELECT max_id + 1 , 'bi_custom_kla_post_process_invaging', date(LOCAL_TIMESTAMP), 'Start of proc'  
FROM NUMBER_FOUNTAIN WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN SET max_id = max_id + 1 WHERE table_name = 'processinglog';

  
DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

/* Don't insert into processinglog for now - serial no issue */
/* INSERT INTO processinglog (referencename, startDate, description) VALUES ('bi_custom_kla_post_process_invaging', NOW(), 'bi_custom_kla_post_process_invaging START') */

/* # Last transaction date for storage location */

/* Only store data that has max and min DateValue, in 2 separate tables */

DROP TABLE IF EXISTS tmp_custom_kla_postproc_dim_date;
CREATE TABLE tmp_custom_kla_postproc_dim_date AS
select mm.Dim_Partid, mm.Dim_StorageLocationid, mmdt.Dim_Dateid,
	row_number() over(partition by mm.Dim_Partid, mm.Dim_StorageLocationid order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate;

UPDATE fact_inventoryaging f
SET f.dim_dateidtransaction = IFNULL((select mmdt.Dim_Dateid
                                          from tmp_custom_kla_postproc_dim_date mmdt
					  where mmdt.Dim_Partid = f.dim_Partid and mmdt.Dim_StorageLocationid = f.dim_StorageLocationid
						and mmdt.RowSeqNo = 1),1)
WHERE f.dim_stockcategoryid in (2,3);

call vectorwise(combine 'fact_inventoryaging');


DELETE FROM MARC WHERE MARC_UMLMC = 0 and MARC_TRAME = 0;
DELETE FROM MARC WHERE MARC_LVORM = 'X';

/* Code removed related to MSLB is in main procedure */
/* Removing this stock category as it is custom for kla but has been inserted in main procedure */

DELETE FROM fact_inventoryaging iag
WHERE iag.dim_stockcategoryid IN (select stkc.dim_stockcategoryid from dim_stockcategory stkc
				  where stkc.categorycode in ('MARC','STO','WIP'));

select 'A1 - Preparing tmp2_MBEW_NO_BWTAR', TIMESTAMP(LOCAL_TIMESTAMP);		  

/* Intermediate tables -- replace MBEW_NO_BWTAR  */
drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR AS
SELECT DISTINCT m.*,((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = decimal((b.STPRS / b.PEINH), 18,5)
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.VERPR / b.PEINH), 18, 5)
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

drop table if exists tmp_MBEW_NO_BWTAR;

call vectorwise(combine 'tmp2_MBEW_NO_BWTAR');

select 'B - Before Insert 1',TIMESTAMP(LOCAL_TIMESTAMP);
  
/*Insert 1   */

 insert into processinglog ( processinglogid, referencename, startdate,description)
 SELECT max_id + 1 , 'bi_custom_kla_post_process_invaging', date(LOCAL_TIMESTAMP), 'Insert 1'  
 FROM NUMBER_FOUNTAIN WHERE table_name = 'processinglog';

 update NUMBER_FOUNTAIN set max_id = max_id + 1 where table_name = 'processinglog';

DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'fact_inventoryaging';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryaging',ifnull(max(fact_inventoryagingid),0)
FROM fact_inventoryaging;

DROP TABLE IF EXISTS tmp_custom_kla_postproc_dim_date;
CREATE TABLE tmp_custom_kla_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm 
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = 641;

 /* # INTRA stock analytics */

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (fp.ct_QtyIssued - fp.ct_ReceivedQty) ct_StockInTransit,
          fp.dd_DocumentNo,
          fp.dd_DocumentItemNo,
          fp.Dim_Partid,
          fp.dim_currencyid,
		  fp.Dim_currencyid Dim_CurrencyId_TRA,		/* inventoryaging should have tran curr same as local curr */
		  fp.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,	
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          fp.Dim_PlantidSupplying, 
		  (fp.amt_ExchangeRate_GBL/fp.amt_ExchangeRate) amt_ExchangeRate_GBL,
		  1 amt_ExchangeRate
     FROM fact_purchase fp
          INNER JOIN dim_purchasemisc pm ON fp.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
          INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = fp.Dim_Currencyid
     WHERE fp.ct_ReceivedQty < fp.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and fp.Dim_PlantidSupplying <> 1 and fp.Dim_Vendorid = 1;
 
  INSERT INTO fact_inventoryaging (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
   SELECT fp.ct_StockInTransit,
          ( b.multiplier * fp.ct_StockInTransit) amt_StockInTransitAmt,
          (b.multiplier * fp.ct_StockInTransit) * fp.amt_ExchangeRate_GBL amt_StockInTransitAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          b.multiplier * fp.amt_ExchangeRate_GBL amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          fp.dd_DocumentNo dd_DocumentNo,
          fp.dd_DocumentItemNo dd_DocumentItemNo,
          ifnull((select mmdt.Dim_Dateid
                  from tmp_custom_kla_postproc_dim_date mmdt
                  where mmdt.dd_DocumentNo = fp.dd_DocumentNo and mmdt.dd_DocumentItemNo = fp.dd_DocumentItemNo
			and mmdt.RowSeqNo = 1),1)  StorageLocEntryDate,
          1 LastReceivedDate,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          fp.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          ifnull((select stkc.dim_stockcategoryid from dim_stockcategory stkc
                  where stkc.categorycode = 'MARC'),1) dim_StockCategoryid,
          fp.Dim_PlantidSupplying, 
	  (select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging' ) + row_number() over (),
	  fp.amt_ExchangeRate_GBL,
	  fp.amt_ExchangeRate, fp.dim_currencyid_TRA, fp.dim_currencyid_GBL
     FROM dim_plant p
          INNER JOIN MARC a
             ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN tmp_fact_fp_001 fp
             ON fp.Dim_Partid = dp.Dim_Partid
          INNER JOIN dim_Currency dc
             ON dc.Dim_Currencyid = fp.Dim_Currencyid;


 select 'C - After Insert 1',TIMESTAMP(LOCAL_TIMESTAMP);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;

update NUMBER_FOUNTAIN
set max_id = ifnull((select max( fact_inventoryagingid ) from fact_inventoryaging),0)
where table_name = 'fact_inventoryaging';


UPDATE processinglog
SET enddate = date(LOCAL_TIMESTAMP)
where referencename = 'bi_custom_kla_post_process_invaging'
and enddate IS NULL
and processinglogid = ( select max_id  FROM NUMBER_FOUNTAIN WHERE table_name = 'processinglog');

 insert into processinglog ( processinglogid, referencename, startdate,description)
 SELECT max_id + 1 , 'bi_custom_kla_post_process_invaging', date(LOCAL_TIMESTAMP), 'Insert 2'
 FROM NUMBER_FOUNTAIN WHERE table_name = 'processinglog';

 update NUMBER_FOUNTAIN set max_id = max_id + 1 where table_name = 'processinglog';

 
/*Insert 2		    */
  /*# INTER stock analytics */

 
DROP TABLE IF EXISTS tmp_custom_kla_postproc_dim_date;
CREATE TABLE tmp_custom_kla_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm 
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = 643;

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (a.ct_QtyIssued - a.ct_ReceivedQty) ct_StockInTransit,
          a.amt_StdUnitPrice,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
          a.Dim_Partid,
          a.dim_plantidordering,
          a.dim_currencyid,
		  a.Dim_currencyid Dim_CurrencyId_TRA,			
		  a.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,	
		  1 amt_ExchangeRate,	 
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          a.Dim_PlantIdSupplying, 
	  (a.amt_ExchangeRate_GBL/a.amt_ExchangeRate) amt_ExchangeRate_GBL
     FROM fact_purchase a
          INNER JOIN dim_purchasemisc pm ON a.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
     WHERE a.ct_ReceivedQty < a.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and a.Dim_PlantidSupplying <> 1
           and not exists (select 1 from fact_inventoryaging fi inner join dim_stockcategory sc on fi.dim_stockcategoryid = sc.Dim_StockCategoryid
                          where fi.dim_Partid = a.dim_partid and fi.dd_DocumentNo = a.dd_DocumentNo and sc.CategoryCode = 'MARC');

  INSERT INTO fact_inventoryaging (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
   SELECT a.ct_StockInTransit,
          (a.amt_StdUnitPrice * a.ct_StockInTransit) amt_StockInTransitAmt,
          (a.amt_StdUnitPrice * a.amt_ExchangeRate_GBL) amt_StockInTransitAmt_GBL,
          a.amt_StdUnitPrice amt_StdUnitPrice,
          (a.amt_StdUnitPrice * a.amt_ExchangeRate_GBL)  amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
          ifnull((select mmdt.Dim_Dateid
                  from tmp_custom_kla_postproc_dim_date mmdt
                  where mmdt.dd_DocumentNo = a.dd_DocumentNo and mmdt.dd_DocumentItemNo = a.dd_DocumentItemNo
			and mmdt.RowSeqNo = 1),1) StorageLocEntryDate,
          1 LastReceivedDate,
          a.Dim_Partid,
          a.dim_plantidordering,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          a.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          ifnull((select stkc.dim_stockcategoryid from dim_stockcategory stkc
                  where stkc.categorycode = 'STO'),1) dim_StockCategoryid,
          a.Dim_PlantIdSupplying, (select ifnull(max_id, 0) FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging' ) + row_number() over (),
	  a.amt_ExchangeRate_GBL,
	  a.amt_ExchangeRate, a.dim_Currencyid_TRA,a.dim_Currencyid_GBL
     FROM tmp_fact_fp_001 a
          INNER JOIN dim_plant p
             ON a.dim_plantidordering = p.dim_plantid
          INNER JOIN dim_part dp
             ON dp.dim_partid = a.dim_partid
          INNER JOIN dim_Currency dc
             ON dc.Dim_Currencyid = a.Dim_Currencyid
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1;


 select 'D - After Insert 2',TIMESTAMP(LOCAL_TIMESTAMP);						  
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;

UPDATE processinglog
SET enddate = date(LOCAL_TIMESTAMP)
where referencename = 'bi_custom_kla_post_process_invaging'
and enddate IS NULL
and processinglogid = ( select max_id  FROM NUMBER_FOUNTAIN WHERE table_name = 'processinglog');

update NUMBER_FOUNTAIN
set max_id = (select max( fact_inventoryagingid ) from fact_inventoryaging)
where table_name = 'fact_inventoryaging';
						  
  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
   SET ia.Dim_ConsumptionTypeid = fp.Dim_ConsumptionTypeid
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND ia.Dim_ConsumptionTypeid <> fp.Dim_ConsumptionTypeid;

  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
  SET     ia.Dim_DocumentStatusid = fp.Dim_DocumentStatusid
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND ia.Dim_DocumentStatusid <> fp.Dim_DocumentStatusid;

  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
  SET     ia.Dim_DocumentTypeid = fp.Dim_DocumentTypeid
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND ia.Dim_DocumentTypeid <> fp.Dim_DocumentTypeid;


  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
  SET     ia.Dim_IncoTermid = fp.Dim_IncoTerm1id
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND ia.Dim_IncoTermid  <> fp.Dim_IncoTerm1id;


  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
  SET     ia.Dim_ItemCategoryid = fp.Dim_ItemCategoryid
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND ia.Dim_ItemCategoryid <> fp.Dim_ItemCategoryid;

  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
  SET     ia.Dim_ItemStatusid = fp.Dim_ItemStatusid
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND  ia.Dim_ItemStatusid  <> fp.Dim_ItemStatusid;

  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
  SET     ia.Dim_Termid = fp.Dim_Termid
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND ia.Dim_Termid <> fp.Dim_Termid;

  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
   SET    ia.Dim_PurchaseMiscid = fp.Dim_PurchaseMiscid
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND ia.Dim_PurchaseMiscid  <> fp.Dim_PurchaseMiscid;

  UPDATE fact_inventoryaging ia
  FROM fact_purchase fp
   SET    ia.Dim_SupplyingPlantId = fp.Dim_PlantidSupplying
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	AND ia.Dim_SupplyingPlantId <> fp.Dim_PlantidSupplying;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;
     
 
select 'E - After Insert 3',TIMESTAMP(LOCAL_TIMESTAMP);	
update NUMBER_FOUNTAIN
set max_id = (select max( fact_inventoryagingid ) from fact_inventoryaging)
where table_name = 'fact_inventoryaging';
			 

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp2_MBEW_NO_BWTAR; 

			 
  UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set amt_CommericalPrice1 = ifnull(MBEW_BWPRH,0)
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY
AND  amt_CommericalPrice1 <>  ifnull(MBEW_BWPRH,0);


  UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set       amt_CurPlannedPrice = ifnull(MBEW_LPLPR,0)
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY
AND amt_CurPlannedPrice <> ifnull(MBEW_LPLPR,0);


        UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set amt_MovingAvgPrice = ifnull(MBEW_VMVER,0)
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY
AND amt_MovingAvgPrice <> ifnull(MBEW_VMVER,0);


        UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set amt_PlannedPrice1 = ifnull(MBEW_ZPLP1,0)
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY
AND amt_PlannedPrice1  <> ifnull(MBEW_ZPLP1,0);


        UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set amt_PreviousPrice = ifnull(MBEW_STPRV,0)
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY
AND amt_PreviousPrice  <> ifnull(MBEW_STPRV,0);


        UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set amt_PrevPlannedPrice = ifnull(MBEW_VPLPR,0)
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;

        UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set Dim_DateIdLastChangedPrice = ifnull((SELECT dt.Dim_Dateid from dim_Date dt
                                            WHERE dt.DateValue = MBEW_LAEPR AND dt.CompanyCode = pl.CompanyCode ),1)
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY;


        UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set Dim_DateIdPlannedPrice2 = ifnull((SELECT dt.Dim_Dateid from dim_Date dt
                                            WHERE dt.DateValue = MBEW_ZPLD2 AND dt.CompanyCode = pl.CompanyCode ),1)
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY;


        UPDATE fact_inventoryaging ia
  FROM tmp2_MBEW_NO_BWTAR m,
        dim_plant pl,
        dim_part prt
  set amt_MtlDlvrCost = m.multiplier
  WHERE ia.dim_partid = prt.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY
AND amt_MtlDlvrCost  <>  m.multiplier;


\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;
call vectorwise(combine 'fact_inventoryaging');

										  
/*Update 2										   */
										  
  UPDATE fact_inventoryaging ia
	FROM  dim_part p, dim_plant pl, tmp2_MBEW_NO_BWTAR m, keph k, dim_date dt
   SET ia.amt_MtlDlvrCost = k.KEPH_KST001
 WHERE ia.dim_Partid = p.Dim_Partid and ia.dim_plantid = pl.dim_plantid
    and p.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
    and k.KEPH_KALNR = m.MBEW_KALN1 and k.KEPH_BWVAR = m.MBEW_BWVA2
    and k.KEPH_KADKY = dt.DateValue and pl.CompanyCode = dt.CompanyCode
    and ((dt.FinancialYear * 100) + dt.FinancialMonthNumber) >= ((m.MBEW_PDATL * 100) + m.MBEW_PPRDL)
    and (k.KEPH_KST003 + k.KEPH_KST004 + k.KEPH_KST005 + k.KEPH_KST006 + k.KEPH_KST007 + k.KEPH_KST008
                        + k.KEPH_KST009 + k.KEPH_KST010 + k.KEPH_KST011 + k.KEPH_KST012 + k.KEPH_KST013 + k.KEPH_KST014
                        + k.KEPH_KST015 + k.KEPH_KST016 + k.KEPH_KST017 + k.KEPH_KST018 + k.KEPH_KST019 + k.KEPH_KST020
                        + k.KEPH_KST021 + k.KEPH_KST022 + k.KEPH_KST023 + k.KEPH_KST024 + k.KEPH_KST025 + k.KEPH_KST026
                        + k.KEPH_KST027 + k.KEPH_KST001 + k.KEPH_KST002)
                        = m.multiplier;



  UPDATE fact_inventoryaging ia
        FROM  dim_part p, dim_plant pl, tmp2_MBEW_NO_BWTAR m, keph k, dim_date dt
   SET ia.amt_OverheadCost = k.KEPH_KST002
 WHERE ia.dim_Partid = p.Dim_Partid and ia.dim_plantid = pl.dim_plantid
    and p.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
    and k.KEPH_KALNR = m.MBEW_KALN1 and k.KEPH_BWVAR = m.MBEW_BWVA2
    and k.KEPH_KADKY = dt.DateValue and pl.CompanyCode = dt.CompanyCode
    and ((dt.FinancialYear * 100) + dt.FinancialMonthNumber) >= ((m.MBEW_PDATL * 100) + m.MBEW_PPRDL)
    and (k.KEPH_KST003 + k.KEPH_KST004 + k.KEPH_KST005 + k.KEPH_KST006 + k.KEPH_KST007 + k.KEPH_KST008
                        + k.KEPH_KST009 + k.KEPH_KST010 + k.KEPH_KST011 + k.KEPH_KST012 + k.KEPH_KST013 + k.KEPH_KST014
                        + k.KEPH_KST015 + k.KEPH_KST016 + k.KEPH_KST017 + k.KEPH_KST018 + k.KEPH_KST019 + k.KEPH_KST020
                        + k.KEPH_KST021 + k.KEPH_KST022 + k.KEPH_KST023 + k.KEPH_KST024 + k.KEPH_KST025 + k.KEPH_KST026
                        + k.KEPH_KST027 + k.KEPH_KST001 + k.KEPH_KST002)
                        = m.multiplier;

         UPDATE fact_inventoryaging ia
        FROM  dim_part p, dim_plant pl, tmp2_MBEW_NO_BWTAR m, keph k, dim_date dt
   SET ia.amt_OtherCost = k.KEPH_KST003 + k.KEPH_KST004 + k.KEPH_KST005 + k.KEPH_KST006 + k.KEPH_KST007 + k.KEPH_KST008
                        + k.KEPH_KST009 + k.KEPH_KST010 + k.KEPH_KST011 + k.KEPH_KST012 + k.KEPH_KST013 + k.KEPH_KST014
                        + k.KEPH_KST015 + k.KEPH_KST016 + k.KEPH_KST017 + k.KEPH_KST018 + k.KEPH_KST019 + k.KEPH_KST020
                        + k.KEPH_KST021 + k.KEPH_KST022 + k.KEPH_KST023 + k.KEPH_KST024 + k.KEPH_KST025 + k.KEPH_KST026
                        + k.KEPH_KST027
  WHERE ia.dim_Partid = p.Dim_Partid and ia.dim_plantid = pl.dim_plantid
    and p.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
    and k.KEPH_KALNR = m.MBEW_KALN1 and k.KEPH_BWVAR = m.MBEW_BWVA2
    and k.KEPH_KADKY = dt.DateValue and pl.CompanyCode = dt.CompanyCode
    and ((dt.FinancialYear * 100) + dt.FinancialMonthNumber) >= ((m.MBEW_PDATL * 100) + m.MBEW_PPRDL)
    and (k.KEPH_KST003 + k.KEPH_KST004 + k.KEPH_KST005 + k.KEPH_KST006 + k.KEPH_KST007 + k.KEPH_KST008
                        + k.KEPH_KST009 + k.KEPH_KST010 + k.KEPH_KST011 + k.KEPH_KST012 + k.KEPH_KST013 + k.KEPH_KST014
                        + k.KEPH_KST015 + k.KEPH_KST016 + k.KEPH_KST017 + k.KEPH_KST018 + k.KEPH_KST019 + k.KEPH_KST020
                        + k.KEPH_KST021 + k.KEPH_KST022 + k.KEPH_KST023 + k.KEPH_KST024 + k.KEPH_KST025 + k.KEPH_KST026
                        + k.KEPH_KST027 + k.KEPH_KST001 + k.KEPH_KST002)
                        = m.multiplier;

										
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;
/*Insert 4										 */
										
 /* # WIP stock analytics */
  INSERT INTO fact_inventoryaging(ct_StockQty,
                               amt_StockValueAmt,
                               ct_LastReceivedQty,
                               dim_StorageLocEntryDateid,
                               dim_LastReceivedDateid,
                               dim_Partid,
                               dim_Plantid,
                               dim_StorageLocationid,
                               dim_Companyid,
                               dim_Vendorid,
                               dim_Currencyid,
							   dim_Currencyid_TRA,
							   dim_Currencyid_GBL,
                               dim_StockTypeid,
                               dim_specialstockid,
                               Dim_PurchaseOrgid,
                               Dim_PurchaseGroupid,
                               dim_producthierarchyid,
                               Dim_UnitOfMeasureid,
                               Dim_MovementIndicatorid,
                               Dim_ConsumptionTypeid,
                               dim_costcenterid,
                               Dim_DocumentStatusid,
                               Dim_DocumentTypeid,
                               Dim_IncoTermid,
                               Dim_ItemCategoryid,
                               Dim_ItemStatusid,
                               Dim_Termid,
                               Dim_PurchaseMiscid,
                               amt_StockValueAmt_GBL,
                               ct_TotalRestrictedStock,
                               ct_StockInQInsp,
                               ct_BlockedStock,
                               ct_StockInTransfer,
                               ct_UnrestrictedConsgnStock,
                               ct_RestrictedConsgnStock,
                               ct_BlockedConsgnStock,
                               ct_ConsgnStockInQInsp,
                               ct_BlockedStockReturns,
                               amt_BlockedStockAmt,
                               amt_BlockedStockAmt_GBL,
                               amt_StockInQInspAmt,
                               amt_StockInQInspAmt_GBL,
                               amt_StockInTransferAmt,
                               amt_StockInTransferAmt_GBL,
                               amt_UnrestrictedConsgnStockAmt,
                               amt_UnrestrictedConsgnStockAmt_GBL,
                               amt_StdUnitPrice,
                               amt_StdUnitPrice_GBL,
                               dim_stockcategoryid,
                               ct_StockInTransit,
                               amt_StockInTransitAmt,
                               amt_StockInTransitAmt_GBL,
                               Dim_SupplyingPlantId,
                               amt_MtlDlvrCost,
                               amt_OverheadCost,
                               amt_OtherCost,
                               amt_WIPBalance,
                               ct_WIPAging,
                               amt_MovingAvgPrice,
                               amt_PreviousPrice,
                               amt_CommericalPrice1,
                               amt_PlannedPrice1,
                               amt_PrevPlannedPrice,
                               amt_CurPlannedPrice,
                               Dim_DateIdPlannedPrice2,
                               Dim_DateIdLastChangedPrice,
                               Dim_DateidActualRelease,
                               Dim_DateidActualStart,
                               dd_ProdOrdernumber,
                               dd_ProdOrderitemno,
                               dd_Plannedorderno,
                               dim_productionorderstatusid,
                               Dim_productionordertypeid,
			       fact_inventoryagingid,
			       amt_ExchangeRate_GBL,amt_ExchangeRate)
  SELECT 0 ct_StockQty,
        0 amt_StockValueAmt,
        0 ct_LastReceivedQty,
        1 dim_StorageLocEntryDateid,
        1 dim_LastReceivedDateid,
        po.Dim_PartidItem dim_Partid,
        po.Dim_Plantid dim_Plantid,
        po.Dim_StorageLocationid dim_StorageLocationid,
        po.Dim_Companyid dim_Companyid,
        1 dim_Vendorid,
        po.Dim_Currencyid dim_Currencyid,
		po.Dim_Currencyid dim_Currencyid_TRA,
		po.Dim_Currencyid_GBL dim_Currencyid_GBL,
        po.Dim_StockTypeid dim_StockTypeid,
        po.dim_specialstockid dim_specialstockid,
        po.Dim_PurchaseOrgid Dim_PurchaseOrgid,
        1 Dim_PurchaseGroupid,
        1 dim_producthierarchyid,
        po.Dim_UnitOfMeasureid Dim_UnitOfMeasureid,
        1 Dim_MovementIndicatorid,
        po.Dim_ConsumptionTypeid Dim_ConsumptionTypeid,
        1 dim_costcenterid,
        1 Dim_DocumentStatusid,
        1 Dim_DocumentTypeid,
        1 Dim_IncoTermid,
        1 Dim_ItemCategoryid,
        1 Dim_ItemStatusid,
        1 Dim_Termid,
        1 Dim_PurchaseMiscid,
        0 amt_StockValueAmt_GBL,
        0 ct_TotalRestrictedStock,
        0 ct_StockInQInsp,
        0 ct_BlockedStock,
        0 ct_StockInTransfer,
        0 ct_UnrestrictedConsgnStock,
        0 ct_RestrictedConsgnStock,
        0 ct_BlockedConsgnStock,
        0 ct_ConsgnStockInQInsp,
        0 ct_BlockedStockReturns,
        0 amt_BlockedStockAmt,
        0 amt_BlockedStockAmt_GBL,
        0 amt_StockInQInspAmt,
        0 amt_StockInQInspAmt_GBL,
        0 amt_StockInTransferAmt,
        0 amt_StockInTransferAmt_GBL,
        0 amt_UnrestrictedConsgnStockAmt,
        0 amt_UnrestrictedConsgnStockAmt_GBL,
        0 amt_StdUnitPrice,
        0 amt_StdUnitPrice_GBL,
        9 dim_stockcategoryid,
        0 ct_StockInTransit,
        0 amt_StockInTransitAmt,
        0 amt_StockInTransitAmt_GBL,
        1 Dim_SupplyingPlantId,
        0 amt_MtlDlvrCost,
        0 amt_OverheadCost,
        0 amt_OtherCost,
        po.amt_WIPBalance * amt_ExchangeRate amt_WIPBalance,
	ifnull((ANSIDATE(LOCAL_TIMESTAMP) - (select pard.DateValue from dim_date pard
						where pard.Dim_Dateid = po.Dim_DateidActualRelease and po.Dim_DateidActualRelease > 1)),0)  ct_WIPAging,
        0 amt_MovingAvgPrice,
        0 amt_PreviousPrice,
        0 amt_CommericalPrice1,
        0 amt_PlannedPrice1,
        0 amt_PrevPlannedPrice,
        0 amt_CurPlannedPrice,
        1 Dim_DateIdPlannedPrice2,
        1 Dim_DateIdLastChangedPrice,
        po.Dim_DateidActualRelease Dim_DateidActualRelease,
        po.Dim_DateidActualStart Dim_DateidActualStart,
        po.dd_ordernumber dd_ProdOrdernumber,
        po.dd_orderitemno dd_ProdOrderitemno,
        po.dd_plannedorderno dd_Plannedorderno,
        po.dim_productionorderstatusid dim_productionorderstatusid,
        po.Dim_ordertypeid Dim_productionordertypeid,
	(select ifnull(max_id, 0) FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging' ) + row_number() over (),
	ifnull((select z.exchangeRate from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_custom_kla_post_process_invaging' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0),
		1 amt_ExchangeRate	
  FROM fact_productionorder po 
	INNER JOIN dim_productionorderstatus ost ON po.dim_productionorderstatusid = ost.dim_productionorderstatusid
        INNER JOIN dim_part p ON po.Dim_PartidItem = p.dim_partid
        INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = po.Dim_Currencyid, tmp_GlobalCurr_001 gbl
  WHERE ost.Closed = 'Not Set' and ost.Delivered = 'Not Set'
      and ost.Created = 'Not Set' and ost.TechnicallyCompleted = 'Not Set'
      and p.ProfitCenterGroup_KLA <> 'Not Set'
      and po.ct_OrderItemQty - po.ct_GRQty > 0;
	  
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;
 insert into processinglog ( processinglogid, referencename, startdate,description)
 SELECT max_id + 1 , 'bi_custom_kla_post_process_invaging', date(LOCAL_TIMESTAMP), 'End of proc'  
 FROM NUMBER_FOUNTAIN WHERE table_name = 'processinglog';

 update NUMBER_FOUNTAIN set max_id = max_id + 1 where table_name = 'processinglog';

update NUMBER_FOUNTAIN
set max_id = (select max( fact_inventoryagingid ) from fact_inventoryaging)
where table_name = 'fact_inventoryaging';

select 'F - End of Proc',TIMESTAMP(LOCAL_TIMESTAMP);
