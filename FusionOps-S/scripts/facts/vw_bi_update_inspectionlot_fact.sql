
DROP TABLE IF EXISTS tmp_dim_profitcenter;
CREATE TABLE tmp_dim_profitcenter 
AS	  
SELECT  dim_profitcenterid,
			  ControllingArea,
			  ProfitCenterCode,
			  MIN(ValidTo) as min_validto
             FROM dim_profitcenter,
			 qals
            WHERE  ControllingArea = QALS_KOKRS
               AND ProfitCenterCode = QALS_PRCTR
              AND ValidTo >= QALS_ERSTELDAT
              AND RowIsCurrent = 1
              GROUP BY  dim_profitcenterid,
			  ControllingArea,
			  ProfitCenterCode ;
	
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa	   
   SET Dim_AccountCategoryid =
          (SELECT dim_accountcategoryid
             FROM dim_accountcategory ac
            WHERE ac.Category = ifnull(QALS_KNTTP, 'Not Set')
                  AND ac.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   and ifnull(Dim_AccountCategoryid,1) <>ifnull((SELECT dim_accountcategoryid FROM dim_accountcategory ac WHERE ac.Category = ifnull(QALS_KNTTP, 'Not Set') AND ac.RowIsCurrent = 1),2);
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_costcenterid = IFNULL(
          (SELECT dim_costcenterid
             FROM dim_costcenter cc
            WHERE     cc.Code = ifnull(QALS_KOSTL, 'Not Set')
                  AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set')
                  AND cc.RowIsCurrent = 1),1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_costcenterid,1)<> ifnull((SELECT dim_costcenterid FROM dim_costcenter cc WHERE cc.Code = ifnull(QALS_KOSTL, 'Not Set')  AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set') AND cc.RowIsCurrent = 1),2);
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET        dim_dateidinspectionend = ifnull((SELECT dim_dateid
                   FROM dim_date ie
                  WHERE ie.DateValue = QALS_PAENDTERM
                        AND ie.CompanyCode = dp.CompanyCode
						AND  QALS_PAENDTERM IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidinspectionend,1) <> ifnull((SELECT dim_dateid FROM dim_date ie  WHERE ie.DateValue = QALS_PAENDTERM  AND ie.CompanyCode = dp.CompanyCode AND  QALS_PAENDTERM IS NOT NULL), 2);
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_dateidinspectionstart = ifnull((SELECT dim_dateid
                   FROM dim_date dis
                  WHERE dis.DateValue = QALS_PASTRTERM
                        AND dis.CompanyCode = dp.CompanyCode
						AND QALS_PASTRTERM IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidinspectionstart,1) <> ifnull((SELECT dim_dateid  FROM dim_date dis  WHERE dis.DateValue = QALS_PASTRTERM   AND dis.CompanyCode = dp.CompanyCode AND QALS_PASTRTERM IS NOT NULL), 2);
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa						
    SET  dim_dateidkeydate = ifnull((SELECT dim_dateid
                   FROM dim_date kd
                  WHERE kd.DateValue = QALS_GUELTIGAB
                        AND kd.CompanyCode = dp.CompanyCode
						AND QALS_GUELTIGAB IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidkeydate,1) <> ifnull((SELECT dim_dateid FROM dim_date kd WHERE kd.DateValue = QALS_GUELTIGAB  AND kd.CompanyCode = dp.CompanyCode	AND QALS_GUELTIGAB IS NOT NULL), 2);
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_dateidlotcreated = ifnull(
                   (SELECT dim_dateid
                      FROM dim_date lcd
                     WHERE lcd.DateValue = QALS_ENSTEHDAT
                           AND lcd.CompanyCode = dp.CompanyCode
						   AND QALS_ENSTEHDAT IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidlotcreated,1) <> ifnull((SELECT dim_dateid FROM dim_date lcd WHERE lcd.DateValue = QALS_ENSTEHDAT  AND lcd.CompanyCode = dp.CompanyCode  AND QALS_ENSTEHDAT IS NOT NULL), 2);
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_dateidposting = ifnull((SELECT dim_dateid
                   FROM dim_date pd
                  WHERE pd.DateValue = QALS_BUDAT
                        AND pd.companycode = dp.CompanyCode
						AND QALS_BUDAT IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidposting,1) = ifnull((SELECT dim_dateid  FROM dim_date pd WHERE pd.DateValue = QALS_BUDAT  AND pd.companycode = dp.CompanyCode	AND QALS_BUDAT IS NOT NULL), 2);
	   
	    -- QALS_AENDERDAT dim_dateidrecordchanged,
       -- QALS_ERSTELDAT dim_dateidrecordcreated,
	   
call vectorwise(combine 'fact_inspectionlot');
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
     SET  dim_documenttypetextid =
          (SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.Type = ifnull(QALS_BLART, 'Not Set')
                  AND dtt.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_inspectionlotmiscid =
          (SELECT dim_inspectionlotmiscid
             FROM dim_inspectionlotmisc
            WHERE     AutomaticInspectionLot = ifnull(QALS_STAT01, 'Not Set')
                  AND BatchManagementRequired = ifnull(QALS_XCHPF, 'Not Set')
                  AND CompleteInspection = ifnull(QALS_HPZ, 'Not Set')
                  AND DocumentationRequired = ifnull(QALS_STAT19, 'Not Set')
                  AND InspectionPlanRequired = ifnull(QALS_STAT20, 'Not Set')
                  AND InspectionStockQuantity = ifnull(QALS_INSMK, 'Not Set')
                  AND ShotTermInspectionComplete =
                        ifnull(QALS_STAT14, 'Not Set')
                  AND StockPostingsComplete = ifnull(QALS_STAT34, 'Not Set')
                  AND UsageDecisionMade = ifnull(QALS_STAT35, 'Not Set')
                  AND LotSkipped = ifnull(QALS_KZSKIPLOT, 'Not Set'))
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
   SET dim_inspectionlotoriginid =
          (SELECT dim_inspectionlotoriginid
             FROM dim_inspectionlotorigin ilo
            WHERE ilo.InspectionLotOriginCode =
                     ifnull(QALS_HERKUNFT, 'Not Set')
                  AND ilo.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_inspectionlotstoragelocationid = ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation isl
                  WHERE     isl.LocationCode = QALS_LAGORTVORG
                        AND isl.RowIsCurrent = 1
                        AND isl.Plant = dp.PlantCode
						AND QALS_LAGORTVORG IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_inspectionsamplestatusid =
          (SELECT dim_inspectionsamplestatusid
             FROM dim_inspectionsamplestatus iss
            WHERE iss.statuscode = ifnull(QALS_LVS_STIKZ, 'Not Set')
                  AND iss.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_inspectionseverityid =
          (SELECT dim_inspectionseverityid
             FROM dim_inspectionseverity isvr
            WHERE isvr.InspectionSeverityCode =
                     ifnull(QALS_PRSCHAERFE, 0)
                  AND isvr.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;

call vectorwise(combine 'fact_inspectionlot');
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_inspectionstageid =
          (SELECT dim_inspectionstageid
             FROM dim_inspectionstage istg
            WHERE istg.InspectionStageCode = QALS_PRSTUFE
                  AND istg.InspectionStageRuleCode =
                        ifnull(QALS_DYNREGEL, 'Not Set')
                  AND istg.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_inspectiontypeid =
          (SELECT dim_inspectiontypeid
             FROM dim_inspectiontype ityp
            WHERE ityp.InspectionTypeCode = ifnull(QALS_ART, 'Not Set')
                  AND ityp.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_itemcategoryid =
          (SELECT dim_itemcategoryid
             FROM dim_itemcategory icc
            WHERE icc.CategoryCode = ifnull(QALS_PSTYP, 'Not Set')
                  AND icc.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_lotunitofmeasureid =
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure luom
            WHERE luom.UOM = ifnull(QALS_MENGENEINH, 'Not Set')
                  AND luom.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_manufacturerid =
          (SELECT dim_vendorid
             FROM dim_vendor dv
            WHERE dv.VendorNumber = ifnull(QALS_HERSTELLER, 'Not Set')
                  AND dv.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_ObjectCategoryid =
          (SELECT dim_ObjectCategoryid
             FROM dim_ObjectCategory oc
            WHERE oc.ObjectCategoryCode = ifnull(QALS_OBTYP, 'Not Set')
                  AND oc.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_purchaseorgid =
          (SELECT dim_purchaseorgid
             FROM dim_purchaseorg po
            WHERE po.PurchaseOrgCode = ifnull(QALS_EKORG, 'Not Set')
                  AND po.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_routeid =
          (SELECT dim_routeid
             FROM dim_route r
            WHERE r.RouteCode = ifnull(QALS_LS_ROUTE, 'Not Set')
                  AND r.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_sampleunitofmeasureid =
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure uom
            WHERE uom.UOM = ifnull(QALS_EINHPROBE, 'Not Set')
                  AND uom.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_specialstockid =
          (SELECT dim_specialstockid
             FROM dim_specialstock ss
            WHERE ss.specialstockindicator = ifnull(QALS_SOBKZ, 'Not Set')
                  AND ss.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_statusprofileid =
          (SELECT dim_statusprofileid
             FROM dim_statusprofile sp
            WHERE sp.StatusProfileCode = ifnull(QALS_STSMA, 'Not Set')
                  AND sp.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_storagelocationid = ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation sl
                  WHERE sl.LocationCode = ifnull(QALS_LAGORTCHRG, 'Not Set')
                        AND sl.Plant = dp.PlantCode
                        AND sl.RowIsCurrent = 1
						AND QALS_LAGORTCHRG IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_tasklisttypeid =
          (SELECT dim_tasklisttypeid
             FROM dim_tasklisttype tlt
            WHERE tlt.TaskListTypeCode = ifnull(QALS_PLNTY, 'Not Set')
                  AND tlt.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;

call vectorwise(combine 'fact_inspectionlot');
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_tasklistusageid =
          (SELECT dim_tasklistusageid
             FROM dim_tasklistusage tlu
            WHERE tlu.UsageCode = ifnull(QALS_PPLVERW, 'Not Set')
                  AND tlu.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_vendorid =
          (SELECT dim_vendorid
             FROM dim_vendor dv1
            WHERE dv1.VendorNumber = ifnull(QALS_LIFNR, 'Not Set')
                  AND dv1.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_warehousenumberid =
          (SELECT dim_warehouseid
             FROM dim_warehousenumber wn
            WHERE wn.WarehouseCode = ifnull(QALS_LGNUM, 'Not Set')
                  AND wn.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_controllingareaid =
          ifnull((SELECT ca.dim_controllingareaid
	     FROM dim_controllingarea ca
            WHERE ca.ControllingAreaCode = QALS_KOKRS),1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_profitcenterid =
          ifnull((SELECT  pc.dim_profitcenterid
             FROM dim_profitcenter pc,
				tmp_dim_profitcenter tmp_pc
            WHERE  pc.ControllingArea = QALS_KOKRS
               AND pc.ProfitCenterCode = QALS_PRCTR
              AND pc.ValidTo >= QALS_ERSTELDAT
              AND pc.RowIsCurrent = 1
			  AND tmp_pc.ControllingArea = QALS_KOKRS
              AND tmp_pc.ProfitCenterCode = QALS_PRCTR 
			  AND pc.ValidTo = tmp_pc.min_validto), 1),
	dd_ObjectNumber = ifnull(QALS_OBJNR,'Not Set')
 WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS;
DROP TABLE IF EXISTS tmp_dim_profitcenter;

call vectorwise(combine 'fact_inspectionlot');

UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_actualinspectedqty = ifnull(QALS_LMENGEPR,0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_actualinspectedqty,-1) <> ifnull(QALS_LMENGEPR,0);

UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_actuallotqty = ifnull(QALS_LMENGEIST,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_actuallotqty,-1) <> ifnull(QALS_LMENGEIST,0);

UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_blockedqty = ifnull(QALS_LMENGE04,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_blockedqty,-1) <> ifnull(QALS_LMENGE04,0);

UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_inspectionlotqty = ifnull(QALS_LOSMENGE,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_inspectionlotqty,-1) <> ifnull(QALS_LOSMENGE,0);

UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_postedqty = ifnull(QALS_LMENGEZUB,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_postedqty,-1) <> ifnull(QALS_LMENGEZUB,0);

UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_qtydefective = ifnull(QALS_LMENGESCH,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_qtydefective,-1) <> ifnull(QALS_LMENGESCH,0);

UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_qtyreturned = ifnull(QALS_LMENGE07,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_qtyreturned,-1) <> ifnull(QALS_LMENGE07,0);            
   
 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_reserveqty = ifnull(QALS_LMENGE05,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_reserveqty,-1) <> ifnull(QALS_LMENGE05,0);       

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_sampleqty = ifnull(QALS_LMENGE03,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_sampleqty,-1) <> ifnull(QALS_LMENGE03,0);     

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_samplesizeqty = ifnull(QALS_GESSTICHPR,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_samplesizeqty,-1) <> ifnull(QALS_GESSTICHPR,0);      

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_scrapqty = ifnull(QALS_LMENGE02,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_scrapqty,-1) <> ifnull(QALS_LMENGE02,0);   
 
 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_unrestrictedqty = ifnull(QALS_LMENGE01,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_unrestrictedqty,-1) <> ifnull(QALS_LMENGE01,0);   

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_batchno = ifnull(QALS_CHARG, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_batchno,'-1') <> ifnull(QALS_CHARG, 'Not Set');   

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_changedby = ifnull(QALS_AENDERER, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_changedby,'-1') <> ifnull(QALS_AENDERER, 'Not Set'); 

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_createdby = ifnull(QALS_ERSTELLER, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_createdby,'-1') <> ifnull(QALS_ERSTELLER, 'Not Set'); 

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_documentitemno = ifnull(QALS_EBELP, 0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_documentitemno,-1) <> ifnull(QALS_EBELP, 0); 

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_documentno = ifnull(QALS_EBELN, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_documentno,'-1') <> ifnull(QALS_EBELN, 'Not Set'); 

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_MaterialDocItemNo = ifnull(QALS_ZEILE, 0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_MaterialDocItemNo,-1) <> ifnull(QALS_ZEILE, 0);
           
 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_MaterialDocNo = ifnull(QALS_MBLNR, 'Not Set') 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_MaterialDocNo,'-1') <> ifnull(QALS_MBLNR, 'Not Set') ;         
          
 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_MaterialDocYear = ifnull(QALS_MJAHR, 0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_MaterialDocYear,-1) <> ifnull(QALS_MJAHR, 0);

 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_orderno = ifnull(QALS_AUFNR, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_orderno,'-1') <> ifnull(QALS_AUFNR, 'Not Set');  
 
 UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_ScheduleNo = ifnull(QALS_ETENR, 0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_ScheduleNo,-1) <> ifnull(QALS_ETENR, 0);

call vectorwise(combine 'fact_inspectionlot');
