INSERT INTO VTTK_VTTP(VTTP_VBELN,
                      VTTP_TPNUM,
                      VTTP_ERDAT,
                      VTTK_TPBEZ,
                      VTTK_TKNUM,
                      VTTK_TEXT4,
                      VTTK_TEXT3,
                      VTTK_TDLNR,
                      VTTK_SIGNI,
                      VTTK_SHTYP,
                      VTTK_ROUTE,
                      VTTK_HANDLE,
                      VTTK_EXTI2,
                      VTTK_ERZET,
                      VTTK_ERDAT,
                      VTTK_DPTEN,
                      VTTK_DATBG,
                      VTTK_AEDAT,
                      VTTK_ADD02,
                      VTTK_ADD01,
                      VTTK_TEXT1)
   SELECT DISTINCT VTTP_VBELN,
                   VTTP_TPNUM,
                   VTTP_ERDAT,
                   VTTK_TPBEZ,
                   VTTK_TKNUM,
                   VTTK_TEXT4,
                   VTTK_TEXT3,
                   VTTK_TDLNR,
                   VTTK_SIGNI,
                   VTTK_SHTYP,
                   VTTK_ROUTE,
                   VTTK_HANDLE,
                   VTTK_EXTI2,
                   VTTK_ERZET,
                   VTTK_ERDAT,
                   VTTK_DPTEN,
                   VTTK_DATBG,
                   VTTK_AEDAT,
                   VTTK_ADD02,
                   VTTK_ADD01,
                   VTTK_TEXT1
     FROM DELTA_VTTK_VTTP v1
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM VTTK_VTTP v2
                   WHERE v1.VTTK_TKNUM = v2.VTTK_TKNUM
                         AND v1.VTTP_TPNUM = v2.VTTP_TPNUM);
						 
CALL VECTORWISE(COMBINE 'VTTK_VTTP');