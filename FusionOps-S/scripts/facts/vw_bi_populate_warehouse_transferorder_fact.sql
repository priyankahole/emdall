/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : unknown */
/*   Created On     : 23 June 2015  */
/*   Description    : Brought from AM interface to SCRIPT model  */

/* Eliminate duplicates <ltak_lgnum, ltap_tanum, ltap_tapos> from LTAK_LTAP */
/* When we have duplicates on the key <ltak_lgnum, ltap_tanum, ltap_tapos>, we remove the ones that have NULL values for user ("ltap_qname" column)*/
drop table if exists ltak_ltap_duplicates;

create table ltak_ltap_duplicates as
select st.ltak_lgnum, st.ltap_tanum, st.ltap_tapos
from ltak_ltap st
group by st.ltak_lgnum, st.ltap_tanum, st.ltap_tapos
having count(*) > 1;

delete from ltak_ltap st
where exists (select 1 from ltak_ltap_duplicates z where st.ltak_lgnum = z.ltak_lgnum and st.ltap_tanum = z.ltap_tanum and st.ltap_tapos = z.ltap_tapos)
      and ltap_qname is null;
/* Eliminate duplicates <ltak_lgnum, ltap_tanum, ltap_tapos> from LTAK_LTAP */
drop table if exists fact_wmtransferorder_tmp;

CREATE TABLE fact_wmtransferorder_tmp 
AS 
select * from fact_wmtransferorder where 1 = 2;

/*initialize NUMBER_FOUNTAIN*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_wmtransferorder';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_wmtransferorder',ifnull(max(f.fact_wmtransferorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) 
FROM fact_wmtransferorder f;



INSERT INTO fact_wmtransferorder_tmp(fact_wmtransferorderid,
								dim_warehousenumberid,
								dd_transferorderno,
								dd_transferorderitem,								
								dim_movementtypeid,
								dim_dateidtransferordercreated ,
								dd_materialdocno,
								dim_partid,
								dim_plantid,
								dd_username,
								ct_sourcetargetqty,
								dim_sourcestoragebinid,
								dim_sourcestoragetypeid,
								dim_destinationstoragebinid,
								dim_destinationstoragetypeid,
								dd_sddocumentno,
								dim_dateidconfirmation,
								dim_dateidplannedexecution,
								dim_dateidstartto,
								dim_dateidendto,
								dim_rfqueueid,
								dim_wmmovementtypeid,
								dim_dateiditemconfirmation,
								dim_dateidgoodsreceipt,
								dim_dateidpickconfirmation,
								dd_conversionnum,
								dd_conversiondenom,
								dim_wmstorageunittypeid,
								dim_inventoryindicatorid,
								dd_materialdocitemno,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dd_sourcequant,
								dd_destinationquant,
								dd_batchnumber,
								dim_baseunitofmeasureid,
								dim_alternativeunitofmeasureid,
								dd_timeofconfirmation,
								ct_itemgrossweight,
								dd_transferprocedure,
								dim_wmstockcategoryid,
								dim_specialstockid,
								ct_sourceactualqty,
								ct_sourcedifferenceqty,
								ct_sourcetargetalternateqty,
								ct_sourceactualalternateqty,
								ct_sourcedifferencealternateqty,
								ct_destinationtargetqty,
								ct_destinationactualqty,
								ct_destinationdifferenceqty,
								ct_destinationtargetalternateqty,
								ct_destinationactualalternateqty,
								ct_destinationdifferencealternateqty,
								dim_wmtransferordermiscellaneousid,
								dim_wmtransferorderitemtypeid)			
								
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN 
   WHERE table_name = 'fact_wmtransferorder') 
   + row_number() over () ) fact_wmtransferorderid,
            wn.dim_warehousenumberid,
			ifnull(st.LTAP_TANUM,'Not Set'),
			ifnull(st.LTAP_TAPOS,0),
			ifnull((SELECT mt.dim_movementtypeid FROM dim_movementtype mt
                                WHERE mt.movementtype = st.LTAK_BWART), 1),
			ifnull((SELECT tocdt.dim_dateid FROM dim_date tocdt
                    WHERE tocdt.DateValue = st.LTAK_BDATU
                    AND tocdt.CompanyCode = pl.CompanyCode), 1),
			ifnull(st.LTAK_MBLNR,'Not Set'),
			ifnull((SELECT dim_partid FROM dim_part dp
                    WHERE dp.PartNumber = st.LTAP_MATNR AND dp.plant = st.LTAP_WERKS), 1),
			pl.Dim_Plantid,
			ifnull(st.LTAP_QNAME,'Not Set'),
			ifnull(st.LTAP_VSOLM, 0),
			ifnull((SELECT src.dim_storagebinid
                    FROM dim_storagebin src
                    WHERE src.storagebin = st.LTAP_VLPLA
                           AND src.storagetype = st.LTAP_VLTYP
						   AND src.WarehouseNumber = st.LTAK_LGNUM), 1),
			ifnull((SELECT srctype.dim_wmstoragetypeid
                    FROM dim_wmstoragetype srctype
                    WHERE srctype.storagetype = st.LTAP_VLTYP
					AND srctype.WarehouseNumber = st.LTAK_LGNUM), 1),                   
			ifnull((SELECT dest.dim_storagebinid
                    FROM dim_storagebin dest
                    WHERE dest.storagebin = st.LTAP_NLPLA
                           AND dest.storagetype = st.LTAP_NLTYP
						   AND dest.WarehouseNumber = st.LTAK_LGNUM), 1),
			ifnull((SELECT desttype.dim_wmstoragetypeid
                    FROM dim_wmstoragetype desttype
                    WHERE desttype.storagetype = st.LTAP_NLTYP
					       AND desttype.WarehouseNumber = st.LTAK_LGNUM),  1),
			ifnull(st.LTAK_VBELN,'Not Set'),
			ifnull((SELECT confdt.dim_dateid  FROM dim_date confdt
					WHERE confdt.DateValue = st.LTAK_QDATU
						   AND confdt.CompanyCode = pl.CompanyCode),  1),
			ifnull((SELECT pedt.dim_dateid FROM dim_date pedt
					WHERE pedt.DateValue = st.LTAK_PLDAT
							AND pedt.CompanyCode = pl.CompanyCode),  1),
			ifnull((SELECT stdt.dim_dateid FROM dim_date stdt
					WHERE stdt.DateValue = st.LTAK_STDAT
							AND stdt.CompanyCode = pl.CompanyCode), 1),
			ifnull((SELECT endt.dim_dateid FROM dim_date endt
					WHERE endt.DateValue = st.LTAK_ENDAT
							AND endt.CompanyCode = pl.CompanyCode), 1),
			ifnull((SELECT rfq.dim_rfqueueid FROM dim_rfqueue rfq
					WHERE rfq.Queue = st.LTAK_QUEUE
					        AND rfq.WarehouseNumber = st.LTAK_LGNUM),  1),
			ifnull((SELECT wmmt.dim_wmmovementtypeid FROM dim_wmmovementtype wmmt
					WHERE wmmt.MovementType = st.LTAK_BWLVS
					        AND wmmt.WarehouseNumber = st.LTAK_LGNUM),  1),
			ifnull((SELECT iconfdt.dim_dateid FROM dim_date iconfdt
					WHERE iconfdt.DateValue = st.LTAP_QDATU 
					        AND iconfdt.CompanyCode = pl.CompanyCode), 1),
			ifnull((SELECT grdt.dim_dateid FROM dim_date grdt
					WHERE grdt.DateValue = st.LTAP_WDATU
							AND grdt.CompanyCode = pl.CompanyCode), 1),
			ifnull((SELECT pcdt.dim_dateid FROM dim_date pcdt
					WHERE pcdt.DateValue = st.LTAP_EDATU
							AND pcdt.CompanyCode = pl.CompanyCode), 1),
			ifnull(st.LTAP_UMREZ, 0),
			ifnull(st.LTAP_UMREN, 0),
			ifnull((SELECT wmsut.dim_wmstorageunittypeid FROM dim_wmstorageunittype wmsut
					WHERE wmsut.StorageUnitType= st.LTAP_LETYP
					      AND wmsut.WarehouseNumber = st.LTAK_LGNUM), 1),
			ifnull((SELECT ii.dim_inventoryindicatorid FROM dim_inventoryindicator ii
					WHERE ii.InventoryIndicator = st.LTAP_KZINV), 1),
			ifnull(st.LTAP_MBPOS,0),
			ifnull(st.LTAP_WENUM,'Not Set'),
			ifnull(st.LTAP_WEPOS,0),
			ifnull(st.LTAP_VLQNR,'Not Set'),
			ifnull(st.LTAP_NLQNR,'Not Set'),
			ifnull(st.LTAP_CHARG,'Not Set'),
			ifnull((SELECT um.dim_unitofmeasureid FROM dim_unitofmeasure um
					WHERE um.uom = st.LTAP_MEINS), 1),
			ifnull((SELECT aum.dim_unitofmeasureid FROM dim_unitofmeasure aum
					WHERE aum.uom = st.LTAP_ALTME), 1),
		    TO_TIME(st.LTAP_QZEIT,'HH24MISS') as dd_timeofconfirmation,
			ifnull(st.LTAP_BRGEW, 0),
			ifnull(st.LTAP_VORGA,'Not Set'),
			ifnull((SELECT wmsc.dim_wmstockcategoryid FROM dim_wmstockcategory wmsc
					WHERE wmsc.WMStockCategory = LTAP_BESTQ),  1),
			ifnull((SELECT ss.dim_specialstockid FROM dim_specialstock ss
					WHERE ss.specialstockindicator = st.LTAP_SOBKZ),  1),
			ifnull(st.LTAP_VISTM, 0),
			ifnull(st.LTAP_VDIFM, 0),
			ifnull(st.LTAP_VSOLA, 0),
			ifnull(st.LTAP_VISTA, 0),
			ifnull(st.LTAP_VDIFA, 0),
			ifnull(st.LTAP_NSOLM, 0),
			ifnull(st.LTAP_NISTM, 0),
			ifnull(st.LTAP_NDIFM, 0),
			ifnull(st.LTAP_NSOLA, 0),
			ifnull(st.LTAP_NISTA, 0),
			ifnull(st.LTAP_NDIFA, 0),
			ifnull ((SELECT wmtomisc.dim_wmtransferordermiscellaneousid from dim_wmtransferordermiscellaneous wmtomisc
			         WHERE wmtomisc.Confirmation = ifnull(st.LTAK_KQUIT, 'Not Set')
		                   AND wmtomisc.NoRealStorage = ifnull(st.LTAK_MINWM, 'Not Set')
		                   AND wmtomisc.ConfirmationRequired = ifnull(st.LTAP_KZQUI, 'Not Set')
		                   AND wmtomisc.ConfirmationComplete = ifnull(st.LTAP_PQUIT, 'Not Set')	
					),1),	   
			ifnull((SELECT wmtoit.dim_wmtransferorderitemtypeid FROM dim_wmtransferorderitemtype wmtoit
		            WHERE wmtoit.WMTransferOrderItemType = st.LTAP_POSTY), 1)		   
     FROM LTAK_LTAP st,
          dim_plant pl,
		  dim_warehousenumber wn		 
    WHERE st.LTAP_WERKS = pl.PlantCode
		 AND wn.warehousecode = st.LTAK_LGNUM
         AND NOT EXISTS (SELECT 1 FROM fact_wmtransferorder fto
                         WHERE wn.dim_warehousenumberid = fto.dim_warehousenumberid
                               AND  st.LTAP_TANUM = fto.dd_transferorderno
                               AND  st.LTAP_TAPOS = fto.dd_transferorderitem);
	
/*   UPDATE Movement TYPE   */


drop table if exists tmp_dim_movementtype;

CREATE TABLE tmp_dim_movementtype AS
select mt.*,
       row_number() over (PARTITION BY movementtype ORDER BY dim_movementtypeid) rowno
from dim_movementtype mt;

UPDATE fact_wmtransferorder_tmp fto
FROM tmp_dim_movementtype mt, 
     LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dim_movementtypeid = ifnull(mt.dim_movementtypeid, 1)
WHERE mt.movementtype = st.LTAK_BWART
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND mt.rowno = 1
AND fto.dim_movementtypeid <> ifnull(mt.dim_movementtypeid, 1);

drop table if exists tmp_dim_movementtype;

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date tocdt
SET fto.dim_dateidtransferordercreated = ifnull(tocdt.dim_dateid, 1)
WHERE tocdt.DateValue = st.LTAK_BDATU
AND tocdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidtransferordercreated <> ifnull(tocdt.dim_dateid, 1);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date tocdt
SET fto.dd_materialdocno = ifnull(st.LTAK_MBLNR,'Not Set')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_materialdocno <> ifnull(st.LTAK_MBLNR,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dim_plantid = ifnull(pl.Dim_Plantid,1)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_plantid <> ifnull(pl.Dim_Plantid,1);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_username = ifnull(st.LTAP_QNAME,'Not Set')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_username <> ifnull(st.LTAP_QNAME,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_sourcetargetqty = ifnull(st.LTAP_VSOLM,0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND ct_sourcetargetqty <> ifnull(st.LTAP_VSOLM,0);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_storagebin src
SET fto.dim_sourcestoragebinid = ifnull(src.dim_storagebinid ,1)
WHERE src.storagebin = st.LTAP_VLPLA
AND src.storagetype = st.LTAP_VLTYP
AND src.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_sourcestoragebinid <> ifnull(src.dim_storagebinid ,1);	


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmstoragetype srctype
SET fto.dim_sourcestoragetypeid = ifnull(srctype.dim_wmstoragetypeid ,1)
WHERE srctype.storagetype = st.LTAP_VLTYP
AND srctype.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_sourcestoragetypeid <> ifnull(srctype.dim_wmstoragetypeid ,1);


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_storagebin dest
SET fto.dim_destinationstoragebinid = ifnull(dest.dim_storagebinid ,1)
WHERE dest.storagebin = st.LTAP_NLPLA
AND dest.storagetype = st.LTAP_NLTYP
AND dest.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_destinationstoragebinid <> ifnull(dest.dim_storagebinid ,1);


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmstoragetype desttype
SET fto.dim_destinationstoragetypeid = ifnull(desttype.dim_wmstoragetypeid ,1)
WHERE desttype.storagetype = st.LTAP_NLTYP
AND desttype.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_destinationstoragetypeid <> ifnull(desttype.dim_wmstoragetypeid ,1);


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_sddocumentno = ifnull(st.LTAK_VBELN,'Not Set')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_sddocumentno <> ifnull(st.LTAK_VBELN,'Not Set');


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date confdt
SET fto.dim_dateidconfirmation = ifnull(confdt.dim_dateid,1)
WHERE confdt.DateValue = st.LTAK_QDATU
AND confdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidconfirmation <> ifnull(confdt.dim_dateid,1);
	
UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date pedt
SET fto.dim_dateidplannedexecution = ifnull(pedt.dim_dateid,1)
WHERE pedt.DateValue = st.LTAK_PLDAT
AND pedt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidplannedexecution <> ifnull(pedt.dim_dateid,1);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date stdt
SET fto.dim_dateidstartto = ifnull( stdt.dim_dateid ,1)
WHERE stdt.DateValue = st.LTAK_STDAT 
AND stdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidstartto <> ifnull( stdt.dim_dateid ,1);

	
UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date endt
SET fto.dim_dateidendto = ifnull( endt.dim_dateid ,1)
WHERE endt.DateValue = st.LTAK_ENDAT
AND endt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidendto <> ifnull( endt.dim_dateid ,1);	


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_rfqueue rfq
SET fto.dim_rfqueueid = ifnull( rfq.dim_rfqueueid ,1)
WHERE  rfq.Queue = st.LTAK_QUEUE
AND rfq.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_rfqueueid <> ifnull( rfq.dim_rfqueueid ,1);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmmovementtype wmmt
SET fto.dim_wmmovementtypeid = ifnull(wmmt.dim_wmmovementtypeid ,1)
WHERE wmmt.MovementType = st.LTAK_BWLVS
AND wmmt.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_wmmovementtypeid <> ifnull(wmmt.dim_wmmovementtypeid ,1);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date iconfdt
SET fto.dim_dateiditemconfirmation = ifnull(iconfdt.dim_dateid ,1)
WHERE iconfdt.DateValue = st.LTAP_QDATU
AND iconfdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateiditemconfirmation <>ifnull(iconfdt.dim_dateid ,1);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date iconfdt
SET fto.dim_dateiditemconfirmation = ifnull(iconfdt.dim_dateid ,1)
WHERE iconfdt.DateValue = st.LTAP_QDATU
AND iconfdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateiditemconfirmation <>ifnull(iconfdt.dim_dateid ,1);	


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date pcdt
SET fto.dim_dateidpickconfirmation = ifnull(pcdt.dim_dateid ,1)
WHERE pcdt.DateValue = st.LTAP_EDATU
AND pcdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidpickconfirmation <> ifnull(pcdt.dim_dateid ,1);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_conversionnum  = ifnull(st.LTAP_UMREZ, '0')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_conversionnum <> ifnull(st.LTAP_UMREZ, '0');	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_conversiondenom  = ifnull(st.LTAP_UMREN, '0')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_conversiondenom <> ifnull(st.LTAP_UMREN, '0');	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmstorageunittype wmsut
SET fto.dim_wmstorageunittypeid  = ifnull( wmsut.dim_wmstorageunittypeid, 1)
WHERE wmsut.StorageUnitType= st.LTAP_LETYP
AND wmsut.WarehouseNumber = st.LTAK_LGNUM 
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_wmstorageunittypeid <> ifnull( wmsut.dim_wmstorageunittypeid, 1);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_inventoryindicator ii
SET fto.dim_inventoryindicatorid  = ifnull(ii.dim_inventoryindicatorid, 1)
WHERE ii.InventoryIndicator = st.LTAP_KZINV
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_inventoryindicatorid <> ifnull(ii.dim_inventoryindicatorid, 1);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_materialdocitemno  = ifnull(st.LTAP_MBPOS,0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_materialdocitemno <> ifnull(st.LTAP_MBPOS,0);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_goodsreceiptno =  ifnull(st.LTAP_WENUM,'Not Set')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_goodsreceiptno <> ifnull(st.LTAP_WENUM,'Not Set');	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_goodsreceiptitemno =  ifnull(st.LTAP_WEPOS,0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_goodsreceiptitemno <> ifnull(st.LTAP_WEPOS,0);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_sourcequant  = ifnull(st.LTAP_VLQNR,'Not Set')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_sourcequant <> ifnull(st.LTAP_VLQNR,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_destinationquant  = ifnull(st.LTAP_NLQNR,'Not Set')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_destinationquant <> ifnull(st.LTAP_NLQNR,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_batchnumber  = ifnull(st.LTAP_CHARG,'Not Set')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_batchnumber <> ifnull(st.LTAP_CHARG,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_unitofmeasure um
SET fto.dim_baseunitofmeasureid  = ifnull(um.dim_unitofmeasureid , 1)
WHERE um.uom = st.LTAP_MEINS
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_baseunitofmeasureid <> ifnull(um.dim_unitofmeasureid , 1);



UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_unitofmeasure aum
SET fto.dim_alternativeunitofmeasureid  = ifnull(aum.dim_unitofmeasureid  , 1)
WHERE aum.uom = st.LTAP_ALTME
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_alternativeunitofmeasureid <> ifnull(aum.dim_unitofmeasureid  , 1);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_timeofconfirmation  = TO_TIME(st.LTAP_QZEIT,'HH24MISS')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_timeofconfirmation <> TO_TIME(st.LTAP_QZEIT,'HH24MISS');


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_itemgrossweight  = ifnull(st.LTAP_BRGEW ,1)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_itemgrossweight <> ifnull(st.LTAP_BRGEW ,1);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.dd_transferprocedure  = ifnull(st.LTAP_VORGA,'Not Set')
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_transferprocedure <> ifnull(st.LTAP_VORGA,'Not Set');
	
UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmstockcategory wmsc
SET fto.dim_wmstockcategoryid  = ifnull(wmsc.dim_wmstockcategoryid , 1)
WHERE wmsc.WMStockCategory = LTAP_BESTQ
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_wmstockcategoryid  <> ifnull(wmsc.dim_wmstockcategoryid , 1);


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_specialstock ss
SET fto.dim_specialstockid  = ifnull(ss.dim_specialstockid , 1)
WHERE ss.specialstockindicator = st.LTAP_SOBKZ
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_specialstockid <> ifnull(ss.dim_specialstockid , 1);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_sourceactualqty = ifnull(st.LTAP_VISTM, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourceactualqty <> ifnull(st.LTAP_VISTM, 0);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_sourcedifferenceqty = ifnull(st.LTAP_VDIFM, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourcedifferenceqty <> ifnull(st.LTAP_VDIFM, 0);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_sourcetargetalternateqty = ifnull(st.LTAP_VSOLA, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourcetargetalternateqty <> ifnull(st.LTAP_VSOLA, 0);	

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_sourceactualalternateqty = ifnull(st.LTAP_VISTA, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourceactualalternateqty <> ifnull(st.LTAP_VISTA, 0);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_sourcedifferencealternateqty = ifnull(st.LTAP_VDIFA, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourcedifferencealternateqty <> ifnull(st.LTAP_VDIFA, 0);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_destinationtargetqty  = 	ifnull(st.LTAP_NSOLM, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationtargetqty  <>	ifnull(st.LTAP_NSOLM, 0);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_destinationactualqty = ifnull(st.LTAP_NISTM, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 	
AND fto.ct_destinationactualqty  <>	ifnull(st.LTAP_NISTM, 0);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_destinationdifferenceqty  = 	ifnull(st.LTAP_NDIFM, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationdifferenceqty <> ifnull(st.LTAP_NDIFM, 0);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_destinationtargetalternateqty  = ifnull(st.LTAP_NSOLA, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationtargetalternateqty <> ifnull(st.LTAP_NSOLA, 0);


UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_destinationactualalternateqty  = ifnull(st.LTAP_NISTA, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationactualalternateqty <> ifnull(st.LTAP_NISTA, 0);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn
SET fto.ct_destinationdifferencealternateqty  = ifnull(st.LTAP_NDIFA, 0)
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationdifferencealternateqty  <> ifnull(st.LTAP_NDIFA, 0);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmtransferordermiscellaneous wmtomisc
SET fto.dim_wmtransferordermiscellaneousid = ifnull(wmtomisc.dim_wmtransferordermiscellaneousid, 1)	
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND wmtomisc.Confirmation = ifnull(st.LTAK_KQUIT, 'Not Set')
AND wmtomisc.NoRealStorage = ifnull(st.LTAK_MINWM, 'Not Set')
AND wmtomisc.ConfirmationRequired = ifnull(st.LTAP_KZQUI, 'Not Set')
AND wmtomisc.ConfirmationComplete = ifnull(st.LTAP_PQUIT, 'Not Set')	
AND fto.dim_wmtransferordermiscellaneousid <> ifnull(wmtomisc.dim_wmtransferordermiscellaneousid, 1);

UPDATE fact_wmtransferorder_tmp fto
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmtransferorderitemtype wmtoit
SET fto.dim_wmtransferorderitemtypeid = ifnull(wmtoit.dim_wmtransferorderitemtypeid, 1)
WHERE wmtoit.WMTransferOrderItemType = st.LTAP_POSTY
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehouseid = fto.dim_warehouseid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_wmtransferorderitemtypeid <> ifnull(wmtoit.dim_wmtransferorderitemtypeid, 1);



call vectorwise(combine 'fact_wmtransferorder_tmp');
call vectorwise(combine 'fact_wmtransferorder');
call vectorwise(combine 'fact_wmtransferorder + fact_wmtransferorder_tmp');

drop table if exists fact_wmtransferorder_tmp;

