

/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 9 Sep 2013 */
/*   Description    : Stored Proc bi_populate_materialmovement_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   21 May 2014      Cornelia	1.0  		 Add dim_uomunitofentryid */
/*   19 May 2014      Cornelia	1.0  		 Add dim_unitofmeasureorderunitid */
/*   21 Sep 2013      Lokesh	1.2 		Performance changes - use hash function and use combine */
/*   09 Sep 2013      Lokesh	1.1           Exchange rate changes	  */
/*											  Note that amts are in local curr e.g MSEG_WAERS is same as dc.currency */
/* 											  We will take MSEG_WAERS as TRA and dim_company.currency as local curr */
/*   06 Sep 2013      Lokesh	1.0  		  Existing version migrated from CVS */
/******************************************************************************************************************/

SELECT 'START',timestamp(local_timestamp);

drop table if exists pGlobalCurrency_tbl;
create table pGlobalCurrency_tbl ( pGlobalCurrency varchar(3));
drop table if exists dim_profitcenter_789;
create table dim_profitcenter_789 as select first 0 * from dim_profitcenter order by ValidTo;

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'bi_populate_materialmovement_fact START');

Insert into pGlobalCurrency_tbl values('USD');

Update pGlobalCurrency_tbl
SET pGlobalCurrency =
        ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');
                
INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms');

UPDATE MKPF_MSEG 
SET MSEG_BUKRS = ifnull(MSEG_BUKRS,'Not Set')
where MSEG_BUKRS <>  ifnull(MSEG_BUKRS,'Not Set');

UPDATE MKPF_MSEG1 
SET MSEG1_BUKRS = ifnull(MSEG1_BUKRS,'Not Set') 
where  MSEG1_BUKRS <>  ifnull(MSEG1_BUKRS,'Not Set');

/* delete existing movement from fact */

drop table if exists tmp_fact_materialmovement_del;
create table tmp_fact_materialmovement_del 
as
select DISTINCT ms.fact_materialmovementid
FROM fact_materialmovement ms, MKPF_MSEG m,dim_plant dp,dim_company dc
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

ALTER TABLE tmp_fact_materialmovement_del
add  PRIMARY KEY (fact_materialmovementid);

insert into tmp_fact_materialmovement_del
select DISTINCT ms.fact_materialmovementid
FROM fact_materialmovement ms, MKPF_MSEG1 m,dim_plant dp,dim_company dc
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;


call vectorwise(combine 'fact_materialmovement-tmp_fact_materialmovement_del');


/* LK:  Used a tmp table to convert similar updates into insert-combine */

DROP TABLE IF EXISTS tmp_fact_materialmovement_perf1;
CREATE TABLE tmp_fact_materialmovement_perf1
AS
SELECT * FROM fact_materialmovement where 1=2;

ALTER TABLE tmp_fact_materialmovement_perf1
add  PRIMARY KEY (fact_materialmovementid);



  INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
				dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
/* These columns are not actually being updated. But need to keep the values. So they are needed in combine */
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
dd_consignmentflag,
dd_documentscheduleno,
dd_incoterms2,
dd_vendorspendflag,
dim_accountcategoryid,
dim_afssizeid,
dim_customergroup1id,
dim_dateidcosting,
dim_dateiddelivery,
dim_dateiddoccreation,
dim_dateidordered,
dim_dateidstatdelivery,
dim_documentcategoryid,
dim_documenttypeid,
dim_grstatusid,
dim_incotermid,
dim_inspusagedecisionid,
dim_itemcategoryid,
dim_itemstatusid,
dim_poissustoragelocid,
dim_poplantidordering,
dim_poplantidsupplying,
dim_postoragelocid,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_purchaseorgid,
dim_salesdocumenttypeid,
dim_salesgroupid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_salesorderrejectreasonid,
dim_salesorgid,
dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
dd_intorder,
ct_lotnotthruqm,
lotsreceivedflag)

  SELECT 	DISTINCT	ms.fact_materialmovementid,
				/*hash(concat(dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear)) fact_materialmovementid,*/
				ms.dd_MaterialDocNo,
                               ms.dd_MaterialDocItemNo,
                               ms.dd_MaterialDocYear,
				ifnull(m.MSEG_EBELN,'Not Set') dd_DocumentNo,
				m.MSEG_EBELP dd_DocumentItemNo,
				ifnull(m.MSEG_KDAUF,'Not Set') dd_SalesOrderNo ,
				m.MSEG_KDPOS dd_SalesOrderItemNo,
				ifnull(m.MSEG_KDEIN,0) dd_SalesOrderDlvrNo,
				dd_GLAccountNo = ifnull(m.MSEG_SAKTO,'Not Set'),
				dd_ReferenceDocNo = ifnull(m.MSEG_LFBNR,'Not Set'),
				dd_ReferenceDocItem = ifnull(m.MSEG_LFPOS,0),
				dd_debitcreditid = CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END,
				dd_productionordernumber = ifnull(m.MSEG_AUFNR,'Not Set'),
  				dd_productionorderitemno = m.MSEG_AUFPS ,
  				dd_GoodsMoveReason = m.MSEG_GRUND ,
 				dd_BatchNumber = ifnull(m.MSEG_CHARG, 'Not Set'),
  				dd_ValuationType = m.MSEG_BWTAR ,
                               ms.amt_LocalCurrAmt,
                               ms.amt_StdUnitPrice,
			amt_DeliveryCost = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR ,
			amt_AltPriceControl = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT ,
			amt_OnHand_ValStock = m.MSEG_SALK3 ,
        	ifnull((select CASE WHEN MSEG_WAERS = dc.Currency THEN 1.00 ELSE  z.exchangeRate end
		from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = MSEG_WAERS and z.fact_script_name = 'bi_populate_materialmovement_fact' 
		and z.pToCurrency = dc.Currency and z.pDate = MKPF_BUDAT),1) amt_ExchangeRate,
        	ifnull(( Select CASE WHEN MSEG_WAERS = pGlobalCurrency THEN 1.00 ELSE z.exchangeRate end 
		from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = MSEG_WAERS and z.fact_script_name = 'bi_populate_materialmovement_fact' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
				ct_Quantity = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE ,
				ct_QtyEntryUOM = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG ,
				ct_QtyGROrdUnit = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG ,
  				ct_QtyOnHand_ValStock = m.MSEG_LBKUM ,
				ct_QtyOrdPriceUnit = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG ,
                               ms.Dim_MovementTypeid,
                               ms.Dim_Companyid,
	Dim_CurrencyId = ifnull(( SELECT dcr.Dim_Currencyid
				FROM  dim_currency dcr
				WHERE  dcr.CurrencyCode = dc.currency),1), 
	Dim_Partid = ifnull(( SELECT dpr.Dim_Partid
				FROM dim_part dpr
				WHERE  dpr.PartNumber = m.MSEG_MATNR  AND dpr.Plant = m.MSEG_WERKS),1), 
                               ms.Dim_Plantid,
			Dim_StorageLocationid = ifnull((SELECT Dim_StorageLocationid
                		FROM dim_storagelocation dsl
                		WHERE     dsl.LocationCode = m.MSEG_LGORT AND dsl.Plant = m.MSEG_WERKS), 1),
                               ms.Dim_Vendorid,
	dim_DateIDMaterialDocDate = ifnull(( SELECT mdd.Dim_Dateid 
				FROM dim_date mdd  
				WHERE  mdd.CompanyCode = m.MSEG_BUKRS AND m.MKPF_BLDAT = mdd.DateValue),1), 
	dim_DateIDPostingDate =  ifnull(( SELECT pd.Dim_Dateid
				FROM dim_date pd
				WHERE  pd.DateValue = m.MKPF_BUDAT AND pd.CompanyCode = m.MSEG_BUKRS),1),
                               ms.Dim_producthierarchyid,
        Dim_MovementIndicatorid =  ifnull(( SELECT dmi.dim_MovementIndicatorid
                                FROM dim_movementindicator dmi
                                WHERE  dmi.TypeCode = m.MSEG_KZBEW),1),
                               ms.Dim_Customerid,
                               ms.Dim_CostCenterid,
        Dim_specialstockid =  ifnull(( SELECT spt.dim_specialstockid
                                FROM dim_specialstock spt
                                WHERE  spt.specialstockindicator = m.MSEG_SOBKZ),1),
        Dim_unitofmeasureid =  ifnull(( SELECT uom.Dim_UnitOfMeasureid
                                FROM dim_unitofmeasure uom
                                WHERE  uom.UOM = m.MSEG_MEINS),1),
        Dim_controllingareaid =  ifnull(( SELECT ca.dim_controllingareaid
                                FROM dim_controllingarea ca
                                WHERE  ca.ControllingAreaCode = m.MSEG_KOKRS),1),
                          	ms.Dim_profitcenterid,
        Dim_consumptiontypeid =  ifnull(( SELECT  ct.Dim_ConsumptionTypeid
                                FROM dim_consumptiontype ct
                                WHERE  ct.ConsumptionCode = m.MSEG_KZVBR),1),
        Dim_stocktypeid =  ifnull(( SELECT  st.Dim_StockTypeid
                                FROM dim_stocktype st
                                WHERE  st.TypeCode = m.MSEG_INSMK),1),
	Dim_PurchaseGroupid =  ifnull((SELECT pg.Dim_PurchaseGroupid
				FROM dim_part dpr inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup
                               	WHERE     dpr.PartNumber = m.MSEG_MATNR AND dpr.Plant = m.MSEG_WERKS), 1),
	Dim_materialgroupid = ifnull((SELECT mg.Dim_materialgroupid
 			          FROM dim_materialgroup mg, dim_part dpr
          			  WHERE mg.MaterialGroupCode = dpr.MaterialGroup AND dpr.PartNumber = m.MSEG_MATNR AND dpr.Plant = m.MSEG_WERKS), 1),
        Dim_ReceivingPlantId =  ifnull(( SELECT  pl.dim_plantid
                                FROM   dim_plant pl
                                WHERE  pl.PlantCode = m.MSEG_UMWRK),1),
        Dim_ReceivingPartId =  ifnull(( SELECT  prt.Dim_Partid
                                FROM   dim_part prt
                                WHERE  prt.Plant = MSEG_UMWRK AND prt.PartNumber = m.MSEG_UMMAT),1),
        Dim_RecvIssuStorLocid =  ifnull(( SELECT  dsl.Dim_StorageLocationid
                                FROM   dim_storagelocation dsl
                                WHERE  dsl.LocationCode = m.MSEG_UMLGO AND dsl.Plant = m.MSEG_UMWRK),1),
	Dim_CurrencyId_TRA = ifnull(( SELECT dcr.Dim_Currencyid
				FROM  dim_currency dcr
				WHERE  dcr.CurrencyCode = m.MSEG_WAERS),1), 
	Dim_CurrencyId_GBL = ifnull(( SELECT dcr.Dim_Currencyid
				FROM  dim_currency dcr
				WHERE  dcr.CurrencyCode = pGlobalCurrency),1) ,
ms.amt_plannedprice,
ms.amt_plannedprice1,
ms.amt_pounitprice,
ms.amt_vendorspend,
ms.ct_orderquantity,
ms.dd_consignmentflag,
ms.dd_documentscheduleno,
ms.dd_incoterms2,
ms.dd_vendorspendflag,
ms.dim_accountcategoryid,
ms.dim_afssizeid,
ms.dim_customergroup1id,
ms.dim_dateidcosting,
ms.dim_dateiddelivery,
ms.dim_dateiddoccreation,
ms.dim_dateidordered,
ms.dim_dateidstatdelivery,
ms.dim_documentcategoryid,
ms.dim_documenttypeid,
ms.dim_grstatusid,
ms.dim_incotermid,
ms.dim_inspusagedecisionid,
ms.dim_itemcategoryid,
ms.dim_itemstatusid,
ms.dim_poissustoragelocid,
ms.dim_poplantidordering,
ms.dim_poplantidsupplying,
ms.dim_postoragelocid,
ms.dim_productionorderstatusid,
ms.dim_productionordertypeid,
ms.dim_purchaseorgid,
ms.dim_salesdocumenttypeid,
ms.dim_salesgroupid,
ms.dim_salesorderheaderstatusid,
ms.dim_salesorderitemstatusid,
ms.dim_salesorderrejectreasonid,
ms.dim_salesorgid,
ms.dim_termid,
ms.dirtyrow,
ms.lotsacceptedflag,
ms.lotsawaitinginspectionflag,
ms.lotsinspectedflag,
ms.lotsrejectedflag,
ms.lotsskippedflag,
ms.percentlotsacceptedflag,
ms.percentlotsinspectedflag,
ms.percentlotsrejectedflag,
ms.qtyrejectedexternalamt,
ms.qtyrejectedinternalamt,
				ms.dd_intorder,
				ms.ct_lotnotthruqm,
				ms.lotsreceivedflag
FROM fact_materialmovement ms, MKPF_MSEG m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_fact_materialmovement_perf1;	

call vectorwise(combine 'fact_materialmovement+tmp_fact_materialmovement_perf1');
 


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_profitcenterid = ifnull((SELECT  pc.Dim_ProfitCenterid
                    FROM dim_profitcenter_789 pc
                   WHERE  pc.ControllingArea = MSEG_KOKRS
                      AND pc.ProfitCenterCode = MSEG_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT ),1)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;



         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_LocalCurrAmt = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR * ms.amt_ExchangeRate
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND amt_LocalCurrAmt <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR * ms.amt_ExchangeRate;

         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     amt_StdUnitPrice = m.MSEG_DMBTR / (case when m.MSEG_ERFMG=0 then null else m.MSEG_ERFMG end)	--MSEG_ERFMG is qty. So, tran curr amt / qty. 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND amt_StdUnitPrice <> ( m.MSEG_DMBTR / (case when m.MSEG_ERFMG=0 then null else m.MSEG_ERFMG end));







 UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     Dim_Vendorid = 1
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_LIFNR is null;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_vendor dv
  SET     ms.Dim_Vendorid = dv.Dim_Vendorid
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  dv.VendorNumber = m.MSEG_LIFNR
	AND ms.Dim_Vendorid <> dv.Dim_Vendorid;


         UPDATE fact_materialmovement ms
        from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc
  SET     dim_producthierarchyid = ifnull((SELECT dim_producthierarchyid
                                            FROM dim_producthierarchy dph, dim_part dpr
                                            WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                                  AND dpr.PartNumber = m.MSEG_MATNR
                                                  AND dpr.Plant = m.MSEG_WERKS), 1)
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

		  
/* Updates from MSEG1 */		  

call vectorwise(combine 'tmp_fact_materialmovement_perf1-tmp_fact_materialmovement_perf1');


/* Create denorm table for insert */
DROP TABLE IF EXISTS tmp_denorm_fact_mm_mseg1;
create table tmp_denorm_fact_mm_mseg1
as
select distinct ms.*,m.*,dc.Currency dc_currency,pGlobalCurrency
 FROM fact_materialmovement ms, MKPF_MSEG1 m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
 AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

call vectorwise(combine 'tmp_denorm_fact_mm_mseg1');


  INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
				dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
dd_consignmentflag,
dd_documentscheduleno,
dd_incoterms2,
dd_vendorspendflag,
dim_accountcategoryid,
dim_afssizeid,
dim_customergroup1id,
dim_dateidcosting,
dim_dateiddelivery,
dim_dateiddoccreation,
dim_dateidordered,
dim_dateidstatdelivery,
dim_documentcategoryid,
dim_documenttypeid,
dim_grstatusid,
dim_incotermid,
dim_inspusagedecisionid,
dim_itemcategoryid,
dim_itemstatusid,
dim_poissustoragelocid,
dim_poplantidordering,
dim_poplantidsupplying,
dim_postoragelocid,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_purchaseorgid,
dim_salesdocumenttypeid,
dim_salesgroupid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_salesorderrejectreasonid,
dim_salesorgid,
dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
				dd_intorder,
				ct_lotnotthruqm,
lotsreceivedflag)

  SELECT 			ms.fact_materialmovementid,
			    /*hash(concat(dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear)) fact_materialmovementid,*/
				ms.dd_MaterialDocNo,
                               ms.dd_MaterialDocItemNo,
                               ms.dd_MaterialDocYear,
				ifnull(ms.MSEG1_EBELN,'Not Set') dd_DocumentNo,
				ms.MSEG1_EBELP dd_DocumentItemNo,
				ifnull(ms.MSEG1_KDAUF,'Not Set') dd_SalesOrderNo ,
				ms.MSEG1_KDPOS dd_SalesOrderItemNo,
				ifnull(ms.MSEG1_KDEIN,0) dd_SalesOrderDlvrNo,
				dd_GLAccountNo = ifnull(ms.MSEG1_SAKTO,'Not Set'),
				dd_ReferenceDocNo = ifnull(ms.MSEG1_LFBNR,'Not Set'),
				dd_ReferenceDocItem = ifnull(ms.MSEG1_LFPOS,0),
				dd_debitcreditid = CASE WHEN ms.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END,
				dd_productionordernumber = ifnull(ms.MSEG1_AUFNR,'Not Set'),
  				dd_productionorderitemno = ms.MSEG1_AUFPS ,
  				dd_GoodsMoveReason = ms.MSEG1_GRUND ,
 				dd_BatchNumber = ifnull(ms.MSEG1_CHARG, 'Not Set'),
  				dd_ValuationType = ms.MSEG1_BWTAR ,
               ms.amt_LocalCurrAmt,
	   		   amt_StdUnitPrice = ms.MSEG1_DMBTR / (case when ms.MSEG1_ERFMG=0 then null else ms.MSEG1_ERFMG end),
     			amt_DeliveryCost = (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BNBTR ,
			amt_AltPriceControl = (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BUALT ,
			amt_OnHand_ValStock = ms.MSEG1_SALK3 ,
        	ifnull((select CASE WHEN MSEG1_WAERS = dc_Currency THEN 1.00 ELSE  z.exchangeRate end
		from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = MSEG1_WAERS and z.fact_script_name = 'bi_populate_materialmovement_fact' 
		and z.pToCurrency = dc_Currency and z.pDate = MKPF_BUDAT),1) amt_ExchangeRate,
        	ifnull(( Select CASE WHEN MSEG1_WAERS = pGlobalCurrency THEN 1.00 ELSE z.exchangeRate end 
		from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = MSEG1_WAERS and z.fact_script_name = 'bi_populate_materialmovement_fact' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL,
				ct_Quantity = (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_MENGE ,
				ct_QtyEntryUOM = (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_ERFMG ,
				ct_QtyGROrdUnit = (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BSTMG ,
  				ct_QtyOnHand_ValStock = ms.MSEG1_LBKUM ,
				ct_QtyOrdPriceUnit = (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BPMNG ,
                               ms.Dim_MovementTypeid,
                               ms.Dim_Companyid,
	Dim_CurrencyId = ifnull(( SELECT dcr.Dim_Currencyid
				FROM  dim_currency dcr
				WHERE  dcr.CurrencyCode = dc_currency),1), 
	Dim_Partid = ifnull(( SELECT dpr.Dim_Partid
				FROM dim_part dpr
				WHERE  dpr.PartNumber = ms.MSEG1_MATNR  AND dpr.Plant = ms.MSEG1_WERKS),1), 
                               ms.Dim_Plantid,
			Dim_StorageLocationid = ifnull((SELECT Dim_StorageLocationid
                		FROM dim_storagelocation dsl
                		WHERE     dsl.LocationCode = ms.MSEG1_LGORT AND dsl.Plant = ms.MSEG1_WERKS), 1),
		Dim_Vendorid = ifnull((SELECT dv.Dim_Vendorid
                		FROM dim_vendor dv
                		WHERE     dv.VendorNumber = ms.MSEG1_LIFNR), 1),						                               
	dim_DateIDMaterialDocDate = ifnull(( SELECT mdd.Dim_Dateid 
				FROM dim_date mdd  
				WHERE  mdd.CompanyCode = ms.MSEG1_BUKRS AND ms.MKPF_BLDAT = mdd.DateValue),1), 
	dim_DateIDPostingDate =  ifnull(( SELECT pd.Dim_Dateid
				FROM dim_date pd
				WHERE  pd.DateValue = ms.MKPF_BUDAT AND pd.CompanyCode = ms.MSEG1_BUKRS),1),
                               ms.Dim_producthierarchyid,
        Dim_MovementIndicatorid =  ifnull(( SELECT dmi.dim_MovementIndicatorid
                                FROM dim_movementindicator dmi
                                WHERE  dmi.TypeCode = ms.MSEG1_KZBEW),1),
                               ms.Dim_Customerid,
                               ms.Dim_CostCenterid,
        Dim_specialstockid =  ifnull(( SELECT spt.dim_specialstockid
                                FROM dim_specialstock spt
                                WHERE  spt.specialstockindicator = ms.MSEG1_SOBKZ),1),
        Dim_unitofmeasureid =  ifnull(( SELECT uom.Dim_UnitOfMeasureid
                                FROM dim_unitofmeasure uom
                                WHERE  uom.UOM = ms.MSEG1_MEINS),1),
        Dim_controllingareaid =  ifnull(( SELECT ca.dim_controllingareaid
                                FROM dim_controllingarea ca
                                WHERE  ca.ControllingAreaCode = ms.MSEG1_KOKRS),1),
                          	ms.Dim_profitcenterid,
        Dim_consumptiontypeid =  ifnull(( SELECT  ct.Dim_ConsumptionTypeid
                                FROM dim_consumptiontype ct
                                WHERE  ct.ConsumptionCode = ms.MSEG1_KZVBR),1),
        Dim_stocktypeid =  ifnull(( SELECT  st.Dim_StockTypeid
                                FROM dim_stocktype st
                                WHERE  st.TypeCode = ms.MSEG1_INSMK),1),
	Dim_PurchaseGroupid =  ifnull((SELECT pg.Dim_PurchaseGroupid
				FROM dim_part dpr inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup
                               	WHERE     dpr.PartNumber = ms.MSEG1_MATNR AND dpr.Plant = ms.MSEG1_WERKS), 1),
	Dim_materialgroupid = ifnull((SELECT mg.Dim_materialgroupid
 			          FROM dim_materialgroup mg, dim_part dpr
          			  WHERE mg.MaterialGroupCode = dpr.MaterialGroup AND dpr.PartNumber = ms.MSEG1_MATNR AND dpr.Plant = ms.MSEG1_WERKS), 1),
        Dim_ReceivingPlantId =  ifnull(( SELECT  pl.dim_plantid
                                FROM   dim_plant pl
                                WHERE  pl.PlantCode = ms.MSEG1_UMWRK),1),
        Dim_ReceivingPartId =  ifnull(( SELECT  prt.Dim_Partid
                                FROM   dim_part prt
                                WHERE  prt.Plant = MSEG1_UMWRK AND prt.PartNumber = ms.MSEG1_UMMAT),1),
        Dim_RecvIssuStorLocid =  ifnull(( SELECT  dsl.Dim_StorageLocationid
                                FROM   dim_storagelocation dsl
                                WHERE  dsl.LocationCode = ms.MSEG1_UMLGO AND dsl.Plant = ms.MSEG1_UMWRK),1),
	Dim_CurrencyId_TRA = ifnull(( SELECT dcr.Dim_Currencyid
				FROM  dim_currency dcr
				WHERE  dcr.CurrencyCode = ms.MSEG1_WAERS),1), 
	Dim_CurrencyId_GBL = ifnull(( SELECT dcr.Dim_Currencyid
				FROM  dim_currency dcr
				WHERE  dcr.CurrencyCode = pGlobalCurrency),1) ,
ms.amt_plannedprice,
ms.amt_plannedprice1,
ms.amt_pounitprice,
ms.amt_vendorspend,
ms.ct_orderquantity,
ms.dd_consignmentflag,
ms.dd_documentscheduleno,
ms.dd_incoterms2,
ms.dd_vendorspendflag,
ms.dim_accountcategoryid,
ms.dim_afssizeid,
ms.dim_customergroup1id,
ms.dim_dateidcosting,
ms.dim_dateiddelivery,
ms.dim_dateiddoccreation,
ms.dim_dateidordered,
ms.dim_dateidstatdelivery,
ms.dim_documentcategoryid,
ms.dim_documenttypeid,
ms.dim_grstatusid,
ms.dim_incotermid,
ms.dim_inspusagedecisionid,
ms.dim_itemcategoryid,
ms.dim_itemstatusid,
ms.dim_poissustoragelocid,
ms.dim_poplantidordering,
ms.dim_poplantidsupplying,
ms.dim_postoragelocid,
ms.dim_productionorderstatusid,
ms.dim_productionordertypeid,
ms.dim_purchaseorgid,
ms.dim_salesdocumenttypeid,
ms.dim_salesgroupid,
ms.dim_salesorderheaderstatusid,
ms.dim_salesorderitemstatusid,
ms.dim_salesorderrejectreasonid,
ms.dim_salesorgid,
ms.dim_termid,
ms.dirtyrow,
ms.lotsacceptedflag,
ms.lotsawaitinginspectionflag,
ms.lotsinspectedflag,
ms.lotsrejectedflag,
ms.lotsskippedflag,
ms.percentlotsacceptedflag,
ms.percentlotsinspectedflag,
ms.percentlotsrejectedflag,
ms.qtyrejectedexternalamt,
ms.qtyrejectedinternalamt,
				ms.dd_intorder,
				ms.ct_lotnotthruqm,
				ms.lotsreceivedflag

FROM tmp_denorm_fact_mm_mseg1 ms;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_fact_materialmovement_perf1;	

call vectorwise(combine 'fact_materialmovement+tmp_fact_materialmovement_perf1');


call vectorwise(combine 'tmp_fact_materialmovement_perf1-tmp_fact_materialmovement_perf1');
call vectorwise(combine 'tmp_fact_materialmovement_del-tmp_fact_materialmovement_del');

 		  
      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     amt_LocalCurrAmt = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR * ms.amt_exchangerate
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_profitcenterid = ifnull((SELECT  pc.Dim_ProfitCenterid
                    FROM dim_profitcenter_789 pc
                   WHERE  pc.ControllingArea = MSEG1_KOKRS
                      AND pc.ProfitCenterCode = MSEG1_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT),1)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;

		  
      UPDATE fact_materialmovement ms
FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc
  SET     dim_producthierarchyid = ifnull((SELECT dim_producthierarchyid
                                            FROM dim_producthierarchy dph, dim_part dpr
                                            WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                                  AND dpr.PartNumber = m.MSEG1_MATNR
                                                  AND dpr.Plant = m.MSEG1_WERKS), 1)
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;          


INSERT INTO processinglog (processinglogid, referencename, enddate, description)
 VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');


Drop table if exists max_holder_789;
create table max_holder_789(maxid)
as
Select ifnull(max(fact_materialmovementid),0)
from fact_materialmovement;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG;
CREATE TABLE tmp_mm_MKPF_MSEG as
select MSEG_MBLNR dd_MaterialDocNo, MSEG_ZEILE dd_MaterialDocItemNo, MSEG_MJAHR dd_MaterialDocYear
from MKPF_MSEG;


DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_del;
CREATE TABLE tmp_mm_MKPF_MSEG_del
as 
select * from tmp_mm_MKPF_MSEG where 1=2;
insert into tmp_mm_MKPF_MSEG_del
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
from fact_materialmovement ;

call vectorwise(combine 'tmp_mm_MKPF_MSEG-tmp_mm_MKPF_MSEG_del');

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_allcols;
CREATE TABLE tmp_mm_MKPF_MSEG_allcols as
SELECT distinct m.*
FROM MKPF_MSEG m,tmp_mm_MKPF_MSEG m1
where m.MSEG_MBLNR = m1.dd_MaterialDocNo
and m.MSEG_ZEILE = m1.dd_MaterialDocItemNo
and m.MSEG_MJAHR = m1.dd_MaterialDocYear;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_mm_MKPF_MSEG_allcols;


INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,                   
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
							   dim_Currencyid_TRA,
							   dim_Currencyid_GBL,
lotsreceivedflag)
   select max_holder_789.maxid + row_number() over(),
		  /*hash(concat(m.MSEG_MBLNR,m.MSEG_ZEILE,m.MSEG_MJAHR)) fact_materialmovementid,*/
		  bb.* 
		  from max_holder_789,(SELECT DISTINCT
          m.MSEG_MBLNR dd_MaterialDocNo,
          m.MSEG_ZEILE dd_MaterialDocItemNo,
          m.MSEG_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG_AUFNR,'Not Set') dd_productionordernumber, 
          ifnull(m.MSEG_AUFPS,1) dd_productionorderitemno,
          m.MSEG_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG_CHARG, 'Not Set') dd_BatchNumber,
          ifnull(m.MSEG_BWTAR,'Not Set') dd_ValuationType,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR amt_LocalCurrAmt,
          ifnull(m.MSEG_DMBTR,0) / (case when m.MSEG_ERFMG =0 then 1 else  m.MSEG_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT amt_AltPriceControl,
          ifnull(m.MSEG_SALK3,0) amt_OnHand_ValStock,
	  1 amt_ExchangeRate,
	  1 amt_ExchangeRate_GBL,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE ct_Quantity,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG ct_QtyGROrdUnit,
          ifnull(m.MSEG_LBKUM,0) ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG ct_QtyOrdPriceUnit,
          1 Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          1 Dim_Currencyid,	/* Local curr */
	 1 Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
	  1 Dim_StorageLocationid,
          ifnull((SELECT dv.Dim_Vendorid
                    FROM dim_vendor dv
                   WHERE dv.VendorNumber = m.MSEG_LIFNR),1) Dim_Vendorid,
          ifnull((SELECT mdd.Dim_Dateid
                    FROM dim_date mdd
                   WHERE mdd.CompanyCode = m.MSEG_BUKRS AND m.MKPF_BLDAT = mdd.DateValue),1) dim_MaterialDocDate,
          ifnull((SELECT pd.Dim_Dateid 
                    FROM dim_date pd
                   WHERE pd.DateValue = m.MKPF_BUDAT AND pd.CompanyCode = m.MSEG_BUKRS),1) dim_DateIDPostingDate,
	  ifnull((select (case WHEN m.MSEG_MATNR IS NULL then 1 else dim_producthierarchyid end)
                          FROM dim_producthierarchy dph, dim_part dpr
                          WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                AND dpr.PartNumber = m.MSEG_MATNR
                                AND dpr.Plant = m.MSEG_WERKS),1) dim_producthierarchyid,
          ifnull((SELECT dmi.dim_MovementIndicatorid
                    FROM dim_movementindicator dmi
                   WHERE dmi.TypeCode = m.MSEG_KZBEW),1) dim_MovementIndicatorid,
          ifnull(( SELECT dcu.dim_Customerid
                     FROM dim_customer dcu
                    WHERE dcu.CustomerNumber = m.MSEG_KUNNR),1) dim_Customerid,
		1 dim_CostCenterid,
          ifnull((SELECT spt.dim_specialstockid
                    FROM dim_specialstock spt
                  WHERE spt.specialstockindicator = MSEG_SOBKZ), 1) dim_specialstockid,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = MSEG_MEINS), 1) dim_unitofmeasureid,
          ifnull((SELECT ca.dim_controllingareaid
                    FROM dim_controllingarea ca
                   WHERE ca.ControllingAreaCode = MSEG_KOKRS),1) dim_controllingareaid,
                   1 Dim_ProfitCenterId,
          ifnull((SELECT ct.Dim_ConsumptionTypeid
                    FROM dim_consumptiontype ct
                   WHERE ct.ConsumptionCode = MSEG_KZVBR), 1) dim_consumptiontypeid,
          ifnull((SELECT st.Dim_StockTypeid
                    FROM dim_stocktype st
                   WHERE st.TypeCode = MSEG_INSMK), 1) dim_stocktypeid,
	ifnull(( Select (case when  m.MSEG_MATNR IS NULL then 1 else  pg.Dim_PurchaseGroupid end )
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG_MATNR
                                      AND dpr.Plant = m.MSEG_WERKS), 1) Dim_PurchaseGroupid,
          ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup
                        AND dpr.PartNumber = m.MSEG_MATNR
                        AND dpr.Plant = m.MSEG_WERKS), 1) Dim_materialgroupid,
          ifnull((SELECT pl.dim_plantid
                    FROM dim_plant pl
                   WHERE pl.PlantCode = MSEG_UMWRK), 1) Dim_ReceivingPlantId,
          ifnull((SELECT prt.Dim_Partid
                    FROM dim_part prt
                   WHERE prt.Plant = MSEG_UMWRK AND prt.PartNumber = MSEG_UMMAT), 1) Dim_ReceivingPartId,
	ifnull((select (case when  m.MSEG_UMLGO IS NULL then 1 else Dim_StorageLocationid end)
		FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG_UMLGO
                                AND dsl.Plant = m.MSEG_UMWRK), 1) Dim_RecvIssuStorLocid,
          ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = m.MSEG_WAERS),1) Dim_Currencyid_TRA,								
          ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = pGlobalCurrency),1) Dim_Currencyid_GBL,
				'N'
      FROM pGlobalCurrency_tbl, tmp_mm_MKPF_MSEG_allcols m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode ) bb;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM tmp_mm_MKPF_MSEG_allcols m, dim_plant dp, dim_company dc
SET ms.amt_StdUnitPrice = 0
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG_ERFMG = 0
AND ms.amt_StdUnitPrice <> 0;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM tmp_mm_MKPF_MSEG_allcols m, dim_plant dp, dim_company dc, dim_currency dcr
SET ms.dim_currencyid = dcr.dim_currencyid
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dcr.CurrencyCode = dc.currency
AND ms.dim_currencyid <> dcr.dim_currencyid;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM tmp_mm_MKPF_MSEG_allcols m, dim_plant dp, dim_company dc,dim_part dpr
SET ms.dim_partid = dpr.dim_partid
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dpr.PartNumber = m.MSEG_MATNR
AND dpr.Plant = m.MSEG_WERKS
AND ms.dim_partid <> dpr.dim_partid;



UPDATE tmp_fact_materialmovement_perf1 ms
FROM tmp_mm_MKPF_MSEG_allcols m, dim_plant dp, dim_company dc,dim_storagelocation dsl
SET ms.dim_storagelocationid = dsl.dim_storagelocationid
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dsl.LocationCode = m.MSEG_LGORT AND dsl.Plant = m.MSEG_WERKS
AND ms.dim_storagelocationid <> dsl.dim_storagelocationid;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM dim_currency tra, dim_currency lcl,dim_date d, tmp_getExchangeRate1 z
SET ms.amt_exchangerate = z.exchangerate
WHERE ms.dim_currencyid_tra = tra.dim_currencyid
AND ms.dim_currencyid = lcl.dim_currencyid
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = lcl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact' 
AND ms.amt_exchangerate <> z.exchangerate;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM dim_currency tra, dim_currency gbl,dim_date d, tmp_getExchangeRate1 z
SET ms.amt_exchangerate_gbl = z.exchangerate
WHERE ms.dim_currencyid_tra = tra.dim_currencyid
AND ms.dim_currencyid_gbl = gbl.dim_currencyid
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = gbl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate_gbl <> z.exchangerate;


UPDATE tmp_fact_materialmovement_perf1 ms
FROM dim_movementtype mt, tmp_mm_MKPF_MSEG_allcols m, dim_plant dp, dim_company dc
SET Dim_MovementTypeid = mt.Dim_MovementTypeid
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG_WERKS = dp.PlantCode
AND  m.MSEG_BUKRS = dc.CompanyCode
AND mt.MovementType = m.MSEG_BWART 
AND mt.ConsumptionIndicator = ifnull(m.MSEG_KZVBR, 'Not Set') 
AND mt.MovementIndicator = ifnull(m.MSEG_KZBEW, 'Not Set') 
AND mt.ReceiptIndicator = ifnull(m.MSEG_KZZUG, 'Not Set') 
AND mt.SpecialStockIndicator = ifnull(m.MSEG_SOBKZ, 'Not Set')
AND ms.Dim_MovementTypeid <> mt.Dim_MovementTypeid;


			 
/* Replaced NOT EXISTS with combine-insert*/		

		 
 /*   WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)) bb */

drop table if exists max_holder_789;

Update tmp_fact_materialmovement_perf1 fmt
From pGlobalCurrency_tbl, MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
set Dim_ProfitCenterId =  ifnull((SELECT  pc.Dim_ProfitCenterid
		    FROM dim_profitcenter_789 pc
		   WHERE  pc.ControllingArea = MSEG_KOKRS
		      AND pc.ProfitCenterCode = MSEG_PRCTR
		      AND pc.ValidTo >= m.MKPF_BUDAT
		   ),1) 
    WHERE fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR;


Update tmp_fact_materialmovement_perf1 fmt
From pGlobalCurrency_tbl, MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
set dim_CostCenterid =   ifnull((select  (case  WHEN m.MSEG_KOSTL IS NULL then 1 else dim_CostCenterid end)
                          FROM dim_costcenter dcc
                          WHERE     dcc.Code = m.MSEG_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG_KOKRS = dcc.ControllingArea), 1)
    WHERE fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR;

call vectorwise(combine 'fact_materialmovement+tmp_fact_materialmovement_perf1');	

/*Truncate perf table to be reused again*/
call vectorwise(combine 'tmp_fact_materialmovement_perf1-tmp_fact_materialmovement_perf1');

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');

Drop table if exists max_holder_789;
create table max_holder_789(maxid)
as
Select ifnull(max(fact_materialmovementid),0)
from fact_materialmovement;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1;
CREATE TABLE tmp_mm_MKPF_MSEG1 as
select MSEG1_MBLNR dd_MaterialDocNo, MSEG1_ZEILE dd_MaterialDocItemNo, MSEG1_MJAHR dd_MaterialDocYear
from MKPF_MSEG1;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_del;
CREATE TABLE tmp_mm_MKPF_MSEG1_del
as 
select * from tmp_mm_MKPF_MSEG1 where 1=2;
insert into tmp_mm_MKPF_MSEG1_del
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
From fact_materialmovement;

call vectorwise(combine 'tmp_mm_MKPF_MSEG1-tmp_mm_MKPF_MSEG1_del');

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_allcols;
CREATE TABLE tmp_mm_MKPF_MSEG1_allcols as
SELECT distinct m.*
FROM MKPF_MSEG1 m,tmp_mm_MKPF_MSEG1 m1
where m.MSEG1_MBLNR = m1.dd_MaterialDocNo
and m.MSEG1_ZEILE = m1.dd_MaterialDocItemNo
and m.MSEG1_MJAHR = m1.dd_MaterialDocYear;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_mm_MKPF_MSEG1_allcols;


  INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,Dim_Currencyid_TRA, Dim_Currencyid_GBL,
lotsreceivedflag)
   Select max_holder_789.maxid + row_number() over(), 
		/*hash(concat(m.MSEG1_MBLNR,m.MSEG1_ZEILE,m.MSEG1_MJAHR)) fact_materialmovementid,*/
		  bb.* 
		 from max_holder_789,(SELECT DISTINCT
          m.MSEG1_MBLNR dd_MaterialDocNo,
          m.MSEG1_ZEILE dd_MaterialDocItemNo,
          m.MSEG1_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG1_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG1_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG1_KDAUF,'Not Set') dd_SalesOrderNo,
          ifnull(m.MSEG1_KDPOS,1) dd_SalesOrderItemNo,
          ifnull(m.MSEG1_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG1_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG1_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG1_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG1_AUFNR,'Not Set') dd_productionordernumber, 
       		ifnull( m.MSEG1_AUFPS,1) dd_productionorderitemno,
          ifnull(m.MSEG1_GRUND,'Not Set') dd_GoodsMoveReason,
          ifnull(m.MSEG1_CHARG, 'Not Set') dd_BatchNumber,
          ifnull(m.MSEG1_BWTAR,'Not Set') dd_ValuationType,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR amt_LocalCurrAmt,
          ifnull(m.MSEG1_DMBTR,0) / (case when m.MSEG1_ERFMG =0 then 1 else  m.MSEG1_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BUALT amt_AltPriceControl,
          ifnull(m.MSEG1_SALK3,0) amt_OnHand_ValStock,
	1 amt_ExchangeRate,
	1 amt_ExchangeRate_GBL,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_MENGE ct_Quantity,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BSTMG ct_QtyGROrdUnit,
          m.MSEG1_LBKUM ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BPMNG ct_QtyOrdPriceUnit,
                        1 Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
                   1 Dim_Currencyid,
                   1  Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
          1 Dim_StorageLocationid,
          ifnull((SELECT dv.Dim_Vendorid
                    FROM dim_vendor dv
                   WHERE dv.VendorNumber = m.MSEG1_LIFNR),1) Dim_Vendorid,
          ifnull((SELECT mdd.Dim_Dateid
                    FROM dim_date mdd
                   WHERE mdd.CompanyCode = m.MSEG1_BUKRS AND MKPF_BLDAT = mdd.DateValue),1) dim_MaterialDocDate,
          ifnull((SELECT pd.Dim_Dateid 
                    FROM dim_date pd
                   WHERE pd.DateValue = MKPF_BUDAT AND pd.CompanyCode = m.MSEG1_BUKRS),1) dim_PostingDate,
          ifnull((SELECT dim_producthierarchyid
                          FROM dim_producthierarchy dph, dim_part dpr
                          WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                AND dpr.PartNumber = m.MSEG1_MATNR
                                AND dpr.Plant = m.MSEG1_WERKS), 1) dim_producthierarchyid,
          ifnull((SELECT dmi.dim_MovementIndicatorid
                    FROM dim_movementindicator dmi
                   WHERE dmi.TypeCode = m.MSEG1_KZBEW),1) dim_MovementIndicatorid,
          ifnull(( SELECT dcu.dim_Customerid
                     FROM dim_customer dcu
                    WHERE dcu.CustomerNumber = m.MSEG1_KUNNR ),1) dim_Customerid,
                   1 dim_CostCenterid,
          ifnull((SELECT spt.dim_specialstockid
                    FROM dim_specialstock spt
                  WHERE spt.specialstockindicator = MSEG1_SOBKZ), 1) dim_specialstockid,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = MSEG1_MEINS), 1) dim_unitofmeasureid,
          ifnull((SELECT ca.dim_controllingareaid
                    FROM dim_controllingarea ca
                   WHERE ca.ControllingAreaCode = MSEG1_KOKRS),1) dim_controllingareaid,
                   1 dim_profitcenterid,
          ifnull((SELECT ct.Dim_ConsumptionTypeid
                    FROM dim_consumptiontype ct
                   WHERE ct.ConsumptionCode = MSEG1_KZVBR), 1) dim_consumptiontypeid,
          ifnull((SELECT st.Dim_StockTypeid
                    FROM dim_stocktype st
                   WHERE st.TypeCode = MSEG1_INSMK), 1) dim_stocktypeid,
	 ifnull(( Select (case when  m.MSEG1_MATNR IS NULL then 1 else  pg.Dim_PurchaseGroupid end )
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG1_MATNR
                                      AND dpr.Plant = m.MSEG1_WERKS), 1) Dim_PurchaseGroupid,
          ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup
                        AND dpr.PartNumber = m.MSEG1_MATNR
                        AND dpr.Plant = m.MSEG1_WERKS), 1) Dim_materialgroupid,
          ifnull((SELECT pl.dim_plantid
                    FROM dim_plant pl
                   WHERE pl.PlantCode = MSEG1_UMWRK), 1) Dim_ReceivingPlantId,
          ifnull((SELECT prt.Dim_Partid
                    FROM dim_part prt
                   WHERE prt.Plant = MSEG1_UMWRK AND prt.PartNumber = MSEG1_UMMAT), 1) Dim_ReceivingPartId,
	 ifnull((select (case when  m.MSEG1_UMLGO IS NULL then 1 else Dim_StorageLocationid end)
                FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG1_UMLGO
                                AND dsl.Plant = m.MSEG1_UMWRK), 1) Dim_RecvIssuStorLocid,
   ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = m.MSEG1_WAERS),1) Dim_Currencyid_TRA,
   ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = pGlobalCurrency ),1) Dim_Currencyid_GBL,
				'N'
     FROM pGlobalCurrency_tbl, tmp_mm_MKPF_MSEG1_allcols m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
                 ) bb;
				 
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_fact_materialmovement_perf1;				 

UPDATE tmp_fact_materialmovement_perf1 ms
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc
SET ms.amt_StdUnitPrice = 0
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_ERFMG = 0
AND ms.amt_StdUnitPrice <> 0;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc, dim_currency dcr
SET ms.dim_currencyid = dcr.dim_currencyid
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dcr.CurrencyCode = dc.currency
AND ms.dim_currencyid <> dcr.dim_currencyid;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc,dim_part dpr
SET ms.dim_partid = dpr.dim_partid
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dpr.PartNumber = m.MSEG1_MATNR
AND dpr.Plant = m.MSEG1_WERKS
AND ms.dim_partid <> dpr.dim_partid;



UPDATE tmp_fact_materialmovement_perf1 ms
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc,dim_storagelocation dsl
SET ms.dim_storagelocationid = dsl.dim_storagelocationid
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dsl.LocationCode = m.MSEG1_LGORT AND dsl.Plant = m.MSEG1_WERKS
AND ms.dim_storagelocationid <> dsl.dim_storagelocationid;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM dim_currency tra, dim_currency lcl,dim_date d, tmp_getExchangeRate1 z
SET ms.amt_exchangerate = z.exchangerate
WHERE ms.dim_currencyid_tra = tra.dim_currencyid
AND ms.dim_currencyid = lcl.dim_currencyid
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = lcl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate <> z.exchangerate;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM dim_currency tra, dim_currency gbl,dim_date d, tmp_getExchangeRate1 z
SET ms.amt_exchangerate_gbl = z.exchangerate
WHERE ms.dim_currencyid_tra = tra.dim_currencyid
AND ms.dim_currencyid_gbl = gbl.dim_currencyid
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = gbl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate_gbl <> z.exchangerate;

UPDATE tmp_fact_materialmovement_perf1 ms
FROM dim_movementtype mt, tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc
SET Dim_MovementTypeid = mt.Dim_MovementTypeid
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_WERKS = dp.PlantCode
AND  m.MSEG1_BUKRS = dc.CompanyCode
AND mt.MovementType = m.MSEG1_BWART
AND mt.ConsumptionIndicator = ifnull(m.MSEG1_KZVBR, 'Not Set')
AND mt.MovementIndicator = ifnull(m.MSEG1_KZBEW, 'Not Set')
AND mt.ReceiptIndicator = ifnull(m.MSEG1_KZZUG, 'Not Set')
AND mt.SpecialStockIndicator = ifnull(m.MSEG1_SOBKZ, 'Not Set')
AND ms.Dim_MovementTypeid <> mt.Dim_MovementTypeid;


drop table if exists max_holder_789;

Update tmp_fact_materialmovement_perf1 fmt
  FROM pGlobalCurrency_tbl, MKPF_MSEG1 m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
set Dim_ProfitCenterId =  ifnull((SELECT  pc.Dim_ProfitCenterid
                    FROM dim_profitcenter_789 pc
                   WHERE  pc.ControllingArea = MSEG1_KOKRS
                      AND pc.ProfitCenterCode = MSEG1_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT
                   ),1)
 WHERE fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear;


Update tmp_fact_materialmovement_perf1 fmt
  FROM pGlobalCurrency_tbl, MKPF_MSEG1 m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
set dim_CostCenterid =  ifnull((select  (case  WHEN m.MSEG1_KOSTL IS NULL then 1 else dim_CostCenterid end)
                          FROM dim_costcenter dcc
                          WHERE     dcc.Code = m.MSEG1_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG1_KOKRS = dcc.ControllingArea), 1)
 WHERE fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear;


/*Populate mm with data in tmp table. Note that this won't need any not exists as its already handled in combine before */
call vectorwise(combine 'fact_materialmovement+tmp_fact_materialmovement_perf1');	

call vectorwise(combine 'tmp_fact_materialmovement_perf1-tmp_fact_materialmovement_perf1');	

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');                         

/*call vectorwise (combine 'fact_materialmovement')*/

UPDATE fact_materialmovement mm 
 FROM dim_movementtype mt
SET  mm.dd_VendorSpendFlag = 1
WHERE mm.dim_movementtypeid = mt.dim_movementtypeid
AND ( mt.movementtype IN (101,102,105,106,122,123,161,162,501,502,521,522) OR mm.dd_ConsignmentFlag = 1)
AND mm.dd_VendorSpendFlag <> 1;

UPDATE fact_materialmovement mm
SET mm.amt_VendorSpend = ( CASE WHEN mm.amt_AltPriceControl = 0 THEN mm.amt_LocalCurrAmt ELSE mm.amt_AltPriceControl END) + amt_DeliveryCost
WHERE mm.dd_VendorSpendFlag = 1;

call vectorwise (combine 'fact_materialmovement');

     INSERT INTO processinglog (processinglogid, referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms,fact_purchase po');

DROP TABLE IF EXISTS tmp_upd_po_to_mm;
CREATE TABLE tmp_upd_po_to_mm
AS
SELECT po.*
FROM fact_purchase po,
(SELECT fp.dd_DocumentNo,fp.dd_DocumentItemNo,min(dd_scheduleno) dd_scheduleno
FROM fact_purchase fp GROUP BY fp.dd_DocumentNo,fp.dd_DocumentItemNo) t
WHERE po.dd_DocumentNo = t.dd_DocumentNo
AND po.dd_DocumentItemNo = t.dd_DocumentItemNo
AND po.dd_scheduleno = t.dd_scheduleno;
     
  UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.dim_purchasegroupid =  po.dim_purchasegroupid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  po.dim_purchasegroupid <> 1
	AND  ms.dim_purchasegroupid <>  po.dim_purchasegroupid;

 

  UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET        ms.dim_purchaseorgid = po.dim_purchaseorgid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_purchaseorgid <> po.dim_purchaseorgid;

     UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET      ms.dim_itemcategoryid = po.dim_itemcategoryid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_itemcategoryid <> po.dim_itemcategoryid;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.dim_Termid = po.dim_Termid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_Termid <> po.dim_Termid;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.dim_documenttypeid = po.dim_documenttypeid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_documenttypeid <> po.dim_documenttypeid;


          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.dim_accountcategoryid = po.dim_accountcategoryid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_accountcategoryid <>  po.dim_accountcategoryid;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.dim_itemstatusid = po.dim_itemstatusid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.dim_itemstatusid <> po.dim_itemstatusid;


          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateIDDocCreation = po.Dim_DateidCreate
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDDocCreation <> po.Dim_DateidCreate;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateIDOrdered = po.Dim_DateidOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDOrdered <> po.Dim_DateidOrder;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_MaterialGroupid = po.Dim_MaterialGroupid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_MaterialGroupid <>  po.Dim_MaterialGroupid;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.amt_POUnitPrice = po.amt_UnitPrice * po.amt_ExchangeRate
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.amt_POUnitPrice <>  (po.amt_UnitPrice * po.amt_ExchangeRate);

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.amt_StdUnitPrice = po.amt_StdUnitPrice * po.amt_ExchangeRate
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.amt_StdUnitPrice <> (po.amt_StdUnitPrice * po.amt_ExchangeRate);

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateidCosting = po.Dim_DateidCosting
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateidCosting <> po.Dim_DateidCosting;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_IncoTermid = po.Dim_IncoTerm1id
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_IncoTermid <> po.Dim_IncoTerm1id;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.dd_incoterms2 = po.dd_incoterms2
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dd_incoterms2 <>  po.dd_incoterms2;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.dd_IntOrder = po.dd_IntOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  ms.dd_IntOrder <> po.dd_IntOrder;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.dd_IntOrder = po.dd_IntOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
        AND  ms.dd_IntOrder is null 
	AND po.dd_IntOrder is not null;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_POPlantidOrdering = po.Dim_PlantidOrdering
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POPlantidOrdering <> po.Dim_PlantidOrdering;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_POPlantidSupplying = po.Dim_PlantidSupplying
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POPlantidSupplying <> po.Dim_PlantidSupplying;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_POStorageLocid = po.Dim_StorageLocationid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POStorageLocid <>  po.Dim_StorageLocationid;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_POIssuStorageLocid = po.Dim_IssuStorageLocid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_POIssuStorageLocid <>  po.Dim_IssuStorageLocid;

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_ReceivingPlantId = (case when ms.Dim_ReceivingPlantId = 1 and ms.Dim_Plantid <> po.Dim_PlantidOrdering 
                                              and (ms.Dim_Plantid = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId end)
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  ms.Dim_ReceivingPlantId <> (case when ms.Dim_ReceivingPlantId = 1 and ms.Dim_Plantid <> po.Dim_PlantidOrdering 
                                              and (ms.Dim_Plantid = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId end);

          UPDATE fact_materialmovement ms
From tmp_upd_po_to_mm po, dim_vendor v, dim_plant pl
    SET ms.Dim_RecvIssuStorLocid = (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid end)
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  ms.Dim_RecvIssuStorLocid <> (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid end);
        
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms,fact_purchase po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement m,fact_salesorder so');

DROP TABLE IF EXISTS tmp_upd_so_to_mm;
CREATE TABLE tmp_upd_so_to_mm
AS
SELECT po.*
FROM fact_salesorder po,
(SELECT fp.dd_SalesDocNo,fp.dd_SalesItemNo,min(dd_scheduleno) dd_scheduleno
FROM fact_salesorder fp GROUP BY fp.dd_SalesDocNo,fp.dd_SalesItemNo) t
WHERE po.dd_SalesDocNo = t.dd_SalesDocNo
AND po.dd_SalesItemNo = t.dd_SalesItemNo
AND po.dd_scheduleno = t.dd_scheduleno;
  
  UPDATE fact_materialmovement ms
	FROM tmp_upd_so_to_mm so
    SET ms.dim_salesorderheaderstatusid = so.dim_salesorderheaderstatusid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderheaderstatusid <> so.dim_salesorderheaderstatusid;

  UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET         ms.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderitemstatusid <>  so.dim_salesorderitemstatusid;

          UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET ms.dim_salesdocumenttypeid = so.dim_salesdocumenttypeid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesdocumenttypeid <> so.dim_salesdocumenttypeid;

          UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET ms.dim_salesorderrejectreasonid = so.dim_salesorderrejectreasonid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderrejectreasonid <> so.dim_salesorderrejectreasonid;

          UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET ms.dim_salesgroupid = so.dim_salesgroupid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesgroupid <>  so.dim_salesgroupid;

          UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET ms.dim_salesorgid = so.dim_salesorgid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorgid <> so.dim_salesorgid;

          UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET ms.dim_customergroup1id = so.dim_customergroup1id
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_customergroup1id <>  so.dim_customergroup1id;

          UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET ms.dim_documentcategoryid = so.dim_documentcategoryid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_documentcategoryid <> so.dim_documentcategoryid;

          UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET ms.Dim_DateIDDocCreation = so.Dim_DateidSalesOrderCreated
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDDocCreation <>  so.Dim_DateidSalesOrderCreated;

          UPDATE fact_materialmovement ms
        FROM tmp_upd_so_to_mm so
    SET ms.Dim_DateIDOrdered = so.Dim_DateidSalesOrderCreated
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDOrdered <> so.Dim_DateidSalesOrderCreated;
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement m,fact_salesorder so');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_productionorder po');

  UPDATE fact_materialmovement ms
FROM fact_productionorder po
    SET ms.dim_productionorderstatusid = po.dim_productionorderstatusid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionorderstatusid <>  po.dim_productionorderstatusid;

UPDATE fact_materialmovement ms
FROM fact_productionorder po
    SET  ms.dim_productionordertypeid = po.Dim_ordertypeid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionordertypeid <> po.Dim_ordertypeid;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_productionorder po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

  UPDATE fact_materialmovement ms
FROM fact_inspectionlot fil
    SET ms.dim_inspusagedecisionid = fil.dim_inspusagedecisionid
  WHERE     ms.dd_MaterialDocItemNo = fil.dd_MaterialDocItemNo
        AND ms.dd_MaterialDocNo = fil.dd_MaterialDocNo
        AND ms.dd_MaterialDocYear = fil.dd_MaterialDocYear
AND  ms.dim_inspusagedecisionid <> fil.dim_inspusagedecisionid;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'bi_populate_materialmovement_fact END');

drop table if exists bwtarupd_tmp_100;

create table bwtarupd_tmp_100 as
Select  x.MATNR, x.BWKEY, max((x.LFGJA * 100) + x.LFMON) updcol 
from mbew_no_bwtar x
Group by  x.MATNR, x.BWKEY;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement;

UPDATE fact_materialmovement ia
FROM  mbew_no_bwtar m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt
SET amt_PlannedPrice1 = ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0)
 WHERE ia.dim_partid = prt.Dim_partid
  and ia.dim_plantid = pl.dim_plantid
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol 
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and amt_PlannedPrice1 <> ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0);

UPDATE fact_materialmovement ia
FROM  mbew_no_bwtar m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt
SET amt_PlannedPrice = ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0)
 WHERE ia.dim_partid = prt.Dim_partid
  and ia.dim_plantid = pl.dim_plantid
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol 
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and  amt_PlannedPrice <> ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0);

/*call bi_purchase_matmovement_dlvr_link()*/

UPDATE fact_materialmovement ms
  FROM dim_unitofmeasure uom,MKPF_MSEG m,dim_plant dp,dim_company dc
  SET ms.dim_unitofmeasureorderunitid = uom.dim_unitofmeasureid
  WHERE uom.uom = m.MSEG_BSTME
AND m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_unitofmeasureorderunitid,-1) <> ifnull(uom.dim_unitofmeasureid,-2);

UPDATE fact_materialmovement ms
  FROM dim_unitofmeasure uom,MKPF_MSEG1 m,dim_plant dp,dim_company dc
  SET ms.dim_unitofmeasureorderunitid = uom.dim_unitofmeasureid
  WHERE uom.uom = m.MSEG1_BSTME
AND m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_unitofmeasureorderunitid,-1) <> ifnull(uom.dim_unitofmeasureid,-2);
  
UPDATE fact_materialmovement ms
set ms.dim_unitofmeasureorderunitid = 1
WHERE  ms.dim_unitofmeasureorderunitid IS NULL;
  
 UPDATE fact_materialmovement ms
  FROM dim_unitofmeasure uom,MKPF_MSEG m,dim_plant dp,dim_company dc
  SET ms.dim_uomunitofentryid = uom.dim_unitofmeasureid
  WHERE uom.uom = m.MSEG_ERFME
AND m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_uomunitofentryid,-1) <> ifnull(uom.dim_unitofmeasureid,-2); 
  
 UPDATE fact_materialmovement ms
  FROM dim_unitofmeasure uom,MKPF_MSEG1 m,dim_plant dp,dim_company dc
  SET ms.dim_uomunitofentryid = uom.dim_unitofmeasureid
  WHERE uom.uom = m.MSEG1_ERFME
AND m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_uomunitofentryid,-1) <> ifnull(uom.dim_unitofmeasureid,-2); 

UPDATE fact_materialmovement ms
set ms.dim_uomunitofentryid = 1
WHERE  ms.dim_uomunitofentryid IS NULL;

drop table if exists dim_profitcenter_789;
drop table if exists bwtarupd_tmp_100;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_allcols;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_del;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_allcols;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_del;
drop table if exists tmp_fact_materialmovement_perf1;
drop table if exists tmp_fact_materialmovement_del;
DROP TABLE IF EXISTS tmp_denorm_fact_mm_mseg1;
DROP TABLE IF EXISTS tmp_upd_po_to_mm;
DROP TABLE IF EXISTS tmp_upd_so_to_mm;

SELECT 'END',timestamp(local_timestamp);

