/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 20 May 2013 */
/*   Description    : Stored Proc bi_populate_billing_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   31 May 2013      Lokesh    1.2       Converted multi-column updates to single-column 						  */
/*   23 May 2013      Lokesh	1.1		  Merged Shanthi's changes from vw_bi_populate_billing_sales_shipment_attributes.sql  */
/*   20 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/



/* ALTER TABLE fact_billing DISABLE KEYS */
/* SET FOREIGN_KEY_CHECKS = 0 */


DROP TABLE IF EXISTS tmp_pGlobalCurrency_fact_billing;
CREATE TABLE tmp_pGlobalCurrency_fact_billing ( pGlobalCurrency CHAR(3) NULL);

INSERT INTO tmp_pGlobalCurrency_fact_billing VALUES ( 'USD' );

update tmp_pGlobalCurrency_fact_billing 
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');


/* Update 1 - Part 1 - Need to handle getExchangeRate */


/* Update 1 column 1 - ct_BillingQtySalesUOM */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET 
ct_BillingQtySalesUOM = VBRP_FKLMG * (VBRP_UMVKN / VBRP_UMVKZ)
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode;

/* Update 1 column 2 - ct_BillingQtyStockUOM - NULLABLE column ( hence the ifnull with <> condition ) */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET 
       ct_BillingQtyStockUOM = VBRP_FKLMG
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(ct_BillingQtyStockUOM,-1) <> IFNULL(VBRP_FKLMG,-2);


/* Update 1 column 3 - amt_ExchangeRate */

/* Update the default value first */
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET amt_ExchangeRate = 1			  
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


/* Now update the value where a match is found */
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
tmp_getExchangeRate1 z
SET amt_ExchangeRate = z.exchangeRate
		   /* getExchangeRate(VBRK_WAERK, pGlobalCurrency,VBRK_KURRF,   VBRK_FKDAT), */			  
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND z.pFromCurrency  = VBRK_WAERK and z.pToCurrency = t_exch.pGlobalCurrency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT
and z.fact_script_name = 'bi_populate_billing_fact';



/* Update 1 column 4 - amt_ExchangeRate_GBL */

/* Update the default value first */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET
 amt_ExchangeRate_GBL = 1

WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
	 tmp_getExchangeRate1 z
SET
 amt_ExchangeRate_GBL = z.exchangeRate   
        /*  getExchangeRate(dc.currency,pGlobalCurrency,VBRK_KURRF,VBRK_FKDAT),	*/
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND z.pFromCurrency  = dc.currency and z.pToCurrency = t_exch.pGlobalCurrency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT 
and z.fact_script_name = 'bi_populate_billing_fact';



/* Update 1 column 5 - amt_CashDiscount */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
set
   amt_CashDiscount = VBRP_SKFBP
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND amt_CashDiscount <> VBRP_SKFBP;



/* Update 1 column 6 - dd_CancelledDocumentNo */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET
  dd_CancelledDocumentNo = ifnull(VBRK_SFAKN,'Not Set')
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(dd_CancelledDocumentNo,'xx') <> ifnull(VBRK_SFAKN,'yy');


/* Update 1 column 7 - dd_SalesDlvrItemNo */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET
 dd_SalesDlvrItemNo = VBRP_VGPOS
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(dd_SalesDlvrItemNo,-1) = IFNULL(VBRP_VGPOS,-2 );

/* Update 1 column 8 - dd_SalesDlvrDocNo */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET
 dd_SalesDlvrDocNo = ifnull(VBRP_VGBEL, 'Not Set')
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(dd_SalesDlvrDocNo,'xx') <> ifnull(VBRP_VGBEL,'yy');

/* Update 1 column 9 - Dim_DateidBilling */

/* Update default value first - e.g for rows which don't have a match for dt.DateValue = VBRK_FKDAT AND dt.CompanyCode = VBRK_BUKRS */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET
Dim_DateidBilling = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND Dim_DateidBilling <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
dim_date dt
SET
Dim_DateidBilling = dt.dim_dateid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND dt.DateValue = VBRK_FKDAT AND dt.CompanyCode = VBRK_BUKRS
AND Dim_DateidBilling <> dt.dim_dateid;

/* Update 1 column 10 - Dim_DateidCreated */

/* Update the default value first - for rows which don't match for the join condition on dt */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET
       Dim_DateidCreated = 1

WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND  Dim_DateidCreated <> 1;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
dim_date dt
SET
       Dim_DateidCreated = dt.dim_dateid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND dt.DateValue = VBRK_ERDAT AND dt.CompanyCode = VBRK_BUKRS
AND Dim_DateidCreated <> dt.dim_dateid;


/* Update 1 column 11 - Dim_DistributionChannelId */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch
SET
	   Dim_DistributionChannelId = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND  Dim_DistributionChannelId <> 1;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
dim_distributionchannel ddc
SET
	   Dim_DistributionChannelId = ddc.Dim_DistributionChannelid

WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND ddc.DistributionChannelCode = VBRK_VTWEG AND ddc.RowIsCurrent = 1
AND fb.Dim_DistributionChannelId <> ddc.Dim_DistributionChannelid;



/* Update 1 - Part 2 column 12 onwards - Need to handle WHERE binary dc1.DocumentCategory = VBRK_VBTYP
/* Also need to handle - ORDER BY pc.ValidTo ASC LIMIT 1 */


/* 2a */	

drop table if exists dim_profitcenter_fact_billing;
create table dim_profitcenter_fact_billing as Select first 0 * from dim_profitcenter  ORDER BY ValidTo ASC;


/* Default values first - for rows not matching join with dc1. Those that match, would be updated by the next query */

/* Update 1 - column 12 */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET        
       Dim_DocumentCategoryid = 1

WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND  Dim_DocumentCategoryid <> 1;	

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,dim_documentcategory dc1
SET        
       Dim_DocumentCategoryid = dc1.Dim_documentcategoryid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND ltrim(rtrim(dc1.DocumentCategory )) = ltrim(rtrim(VBRK_VBTYP)) AND dc1.RowIsCurrent = 1
and fb.Dim_DocumentCategoryid <> dc1.Dim_documentcategoryid ;

	
/* 2b */	

/* Update 1 - column 13 */	

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET  	
       Dim_DocumentTypeid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND fb.Dim_DocumentTypeid <> 1;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
dim_billingdocumenttype bdt
SET  	
       Dim_DocumentTypeid = bdt.dim_billingdocumenttypeid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND bdt.Type = VBRK_FKART AND bdt.RowIsCurrent = 1
AND Dim_DocumentTypeid <> bdt.dim_billingdocumenttypeid;

/* Update 1 - column 14 */	
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET  	
       Dim_IncoTermid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_IncoTermid,-1) <> 1;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	dim_incoterm it
SET  	
       Dim_IncoTermid = it.dim_IncoTermId
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND it.IncoTermCode = VBRK_INCO1 AND it.RowIsCurrent = 1
AND IFNULL(fb.Dim_IncoTermid,-1) <> IFNULL( it.dim_IncoTermId,-2) ;

/* Update 1 - column 15 */	
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET  	
       Dim_MaterialGroupid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_MaterialGroupid,-1) <> 1;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	dim_materialgroup mg
SET  	
       Dim_MaterialGroupid = mg.dim_materialgroupid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND mg.MaterialGroupCode = VBRP_MATKL AND mg.RowIsCurrent = 1
AND IFNULL(fb.Dim_MaterialGroupid,-1) <> IFNULL(mg.dim_materialgroupid,-2) ;

/* Update 1 - column 16 */	
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET  	
       fb.Dim_Partid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_Partid,-1) <> 1;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	dim_part p
SET  	
       fb.Dim_Partid = p.dim_partid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND p.PartNumber = VBRP_MATNR AND p.Plant = VBRP_WERKS AND p.RowIsCurrent = 1
AND IFNULL(fb.Dim_Partid,-1) <> IFNULL(p.dim_partid,-2);

/* Update 1 - column 17 */	
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET  	
 fb.Dim_Plantid = dp.dim_plantid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL( fb.Dim_Plantid,-1 ) <> IFNULL(dp.dim_plantid,-2);

/* Update 1 - column 18 */	
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET  	
	   Dim_ProductHierarchyid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_ProductHierarchyid,-1) <> 1 ;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	dim_producthierarchy ph
SET  	
	   Dim_ProductHierarchyid = ph.dim_producthierarchyid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND ph.ProductHierarchy = VBRP_PRODH AND ph.RowIsCurrent = 1
AND IFNULL(fb.Dim_ProductHierarchyid,-1) <> IFNULL(ph.dim_producthierarchyid,-2);


	

/* 2c */	
	
/* Update 1 - column 19 */		
	
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET  	
       Dim_ProfitCenterid =
          ifnull((SELECT pc.dim_profitcenterid
                      FROM dim_profitcenter_fact_billing pc
                      WHERE   pc.ProfitCenterCode = VBRP_PRCTR
                          AND pc.ControllingArea = VBRP_KOKRS
                          AND pc.ValidTo >= VBRK_FKDAT
                          AND pc.RowIsCurrent = 1 ),1)
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode;

/* End of Update 1 - Part 2 */

/* Update 1 - Part 3 : Tested */

/* Update 1 column 20 */

UPDATE fact_billing fb
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_SalesDivisionid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_SalesDivisionid,-1) <> 1;

UPDATE fact_billing fb
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_salesdivision sd
SET Dim_SalesDivisionid = sd.dim_salesdivisionid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND sd.DivisionCode = VBRK_SPART   
AND IFNULL(fb.Dim_SalesDivisionid,-1) <> IFNULL(sd.dim_salesdivisionid,-2);

/* Update 1 column 21 - Dim_SalesGroupid */

UPDATE fact_billing fb
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET Dim_SalesGroupid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_SalesGroupid,-1) <> 1;


UPDATE fact_billing fb
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_salesgroup sg
SET Dim_SalesGroupid = sg.dim_salesgroupid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND sg.SalesGroupCode = VBRP_VKGRP 
AND IFNULL(fb.Dim_SalesGroupid,-1) <> IFNULL(sg.dim_salesgroupid,-2);


/* Update 1 column 22 - Dim_SalesOfficeid */

UPDATE fact_billing fb
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET Dim_SalesOfficeid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_SalesOfficeid,-1) <> 1;


UPDATE fact_billing fb
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_salesoffice so1
SET Dim_SalesOfficeid = so1.dim_salesofficeid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND so1.SalesOfficeCode = VBRP_VKBUR 
AND IFNULL(fb.Dim_SalesOfficeid,-1) <> IFNULL(so1.dim_salesofficeid,-2);


/* Update 1 column 23 - Dim_SalesOrgid */
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_SalesOrgid = ifnull(so.dim_salesorgid,1)
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
 and IFNULL(fb.Dim_SalesOrgid,-1) <> ifnull(so.dim_salesorgid,1);
 
 
/* Update 1 column 24 - Dim_ControllingAreaid - NOT NULL column so IFNULL not used with the <> condition */ 
 
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_ControllingAreaid = ca.dim_controllingareaid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND fb.Dim_ControllingAreaid <>  ca.dim_controllingareaid;


/* Update 1 column 25 - Dim_StorageLocationid */ 

/* Update column Dim_StorageLocationid */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET Dim_StorageLocationid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_StorageLocationid,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_storagelocation sl
SET Dim_StorageLocationid = sl.dim_storagelocationid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND  sl.LocationCode = VBRP_LGORT
                     AND sl.plant = VBRP_WERKS
                     AND sl.RowIsCurrent = 1
AND IFNULL(fb.Dim_StorageLocationid,-1) <> IFNULL(sl.dim_storagelocationid,-2);


/* Update column Dim_AccountingTransferStatusid */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET Dim_AccountingTransferStatusid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_AccountingTransferStatusid,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_accountingtransferstatus ats
SET Dim_AccountingTransferStatusid = ats.dim_accountingtransferstatusid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND ats.AccountingTransferStatusCode = VBRK_RFBSK
                     AND ats.RowIsCurrent = 1
AND IFNULL(fb.Dim_AccountingTransferStatusid,-1) <> IFNULL(ats.dim_accountingtransferstatusid,-2);


/* Update column fb.Dim_ConditionProcedureid */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_ConditionProcedureid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_ConditionProcedureid,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_conditionprocedure dc1
SET fb.Dim_ConditionProcedureid = dc1.dim_conditionprocedureid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND dc1.Dim_ConditionProcedureid = VBRK_KALSM
                     AND dc1.RowIsCurrent = 1
AND IFNULL(fb.Dim_ConditionProcedureid,-1) <> IFNULL(dc1.dim_conditionprocedureid,-2);


/* Update column Dim_CreditControlAreaid */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET Dim_CreditControlAreaid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_CreditControlAreaid,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_creditcontrolarea cca
SET Dim_CreditControlAreaid = cca.dim_creditcontrolareaid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND cca.CreditControlAreaName = VBRP_KOKRS
                     AND cca.RowIsCurrent = 1
AND IFNULL(fb.Dim_CreditControlAreaid,-1) <> IFNULL(cca.dim_creditcontrolareaid,-2);


/* Update column Dim_CustomerID */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET Dim_CustomerID = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_CustomerID,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_customer c
SET Dim_CustomerID = c.dim_customerid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND c.CustomerNumber = VBRK_KUNAG AND c.RowIsCurrent = 1
AND IFNULL(fb.Dim_CustomerID,-1) <> IFNULL(c.dim_customerid,-2);


/* Update column Dim_CustomerGroup1id */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET Dim_CustomerGroup1id = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_CustomerGroup1id,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_customergroup1 cg1
SET Dim_CustomerGroup1id = cg1.dim_customergroup1id
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND cg1.CustomerGroup = VBRK_KDGRP
AND IFNULL(fb.Dim_CustomerGroup1id,-1) <> IFNULL(cg1.dim_customergroup1id,-2);

/* Update column Dim_CustomerPaymentTermsid */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET Dim_CustomerPaymentTermsid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_CustomerPaymentTermsid,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_customerpaymentterms cpt
SET Dim_CustomerPaymentTermsid = cpt.dim_customerpaymenttermsid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND cpt.PaymentTermCode = VBRK_ZTERM
AND cpt.RowIsCurrent = 1
AND IFNULL(fb.Dim_CustomerPaymentTermsid,-1) <> IFNULL(cpt.dim_customerpaymenttermsid,-2);


/* Update column fb.Dim_ShipReceivePointid */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_ShipReceivePointid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_ShipReceivePointid,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_shipreceivepoint srp
SET fb.Dim_ShipReceivePointid = srp.dim_shipreceivepointid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND srp.ShipReceivePointCode = VBRP_VSTEL
AND IFNULL(fb.Dim_ShipReceivePointid,-1) <> IFNULL(srp.dim_shipreceivepointid,-2);


/* Update column fb.Dim_UnitOfMeasureId */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_UnitOfMeasureId = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_UnitOfMeasureId,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_unitofmeasure um
SET fb.Dim_UnitOfMeasureId = um.dim_unitofmeasureid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND um.UOM = VBRP_MEINS
AND IFNULL(fb.Dim_UnitOfMeasureId,-1) <> IFNULL(um.dim_unitofmeasureid,-2);


/* Update 1 - Part 4 : tested */
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET
  fb.Dim_Companyid = dc.Dim_Companyid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND   fb.Dim_Companyid <> dc.Dim_Companyid;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.dd_fiscalyear = VBRK_GJAHR
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.dd_fiscalyear,-1) <> ifnull(VBRK_GJAHR,-2);

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.dd_postingperiod = VBRK_POPER
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.dd_postingperiod,-1) <> ifnull(VBRK_POPER,-2);

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.dd_salesitemno = VBRP_AUPOS
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.dd_salesitemno,-1) <> ifnull(VBRP_AUPOS,-2);

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET amt_cost_InDocCurrency = VBRP_WAVWR
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.amt_cost_InDocCurrency,-1) <> ifnull(VBRP_WAVWR,-2);

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET amt_NetValueHeader_InDocCurrency = VBRK_NETWR
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.amt_NetValueHeader_InDocCurrency,-1) <> ifnull(VBRK_NETWR,-2);
 
UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.dd_salesdocno = ifnull(VBRP_AUBEL, 'Not Set')
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;	   
 
 
/* Update column fb.dim_salesorderitemcategoryid */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.dim_salesorderitemcategoryid = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.dim_salesorderitemcategoryid,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_salesorderitemcategory soic
SET fb.dim_salesorderitemcategoryid = soic.dim_salesorderitemcategoryid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
 AND soic.SalesOrderItemCategory = vkp.VBRP_PSTYV AND soic.RowIsCurrent = 1
AND IFNULL(fb.dim_salesorderitemcategoryid,-1) <> IFNULL(soic.dim_salesorderitemcategoryid,-2);


/* Update column fb.Dim_BillingCategoryId */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_BillingCategoryId = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_BillingCategoryId,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_billingcategory bc
SET fb.Dim_BillingCategoryId = bc.dim_billingcategoryid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
 AND bc.Category = ifnull(VBRK_FKTYP,'Not Set')
                   AND bc.RowIsCurrent = 1
AND IFNULL(fb.Dim_BillingCategoryId,-1) <> IFNULL(bc.dim_billingcategoryid,-2);


/* Update column fb.Dim_RevenueRecognitionCategoryId */

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_RevenueRecognitionCategoryId = 1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_RevenueRecognitionCategoryId,-1) <> 1;


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_RevenueRecognitioncategory rrc
SET fb.Dim_RevenueRecognitionCategoryId = rrc.dim_RevenueRecognitioncategoryid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND rrc.Category = ifnull(VBRP_RRREL,'Not Set')
                   AND rrc.RowIsCurrent = 1
AND IFNULL(fb.Dim_RevenueRecognitionCategoryId,-1) <> IFNULL(rrc.dim_RevenueRecognitioncategoryid,-2);
 




/* Update 1 - Part 5 : tested*/

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET
        amt_NetValueItem_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_NETWR
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal1_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal2_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal3_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal4_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal5_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal6_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET
dd_CustomerPONumber = ifnull(VBRK_BSTNK_VF,'Not Set')
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
 AND ifnull(fb.dd_CustomerPONumber,'xx') <> ifnull(VBRK_BSTNK_VF,'Not Set');

UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
SET
dd_CreatedBy = ifnull(VBRK_ERNAM,'Not Set')
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
  AND ifnull(fb.dd_CreatedBy,'xx') <> ifnull(VBRK_ERNAM,'Not Set');

/* End of Update 1 */


/* Insert 1 */

DROP TABLE IF EXISTS tmp_db_fb1 ;
CREATE TABLE tmp_db_fb1
AS
SELECT pc.ProfitCenterCode,pc.ControllingArea,VBRK_FKDAT,min(pc.ValidTo) as min_ValidTo
FROM dim_profitcenter pc,VBRK_VBRP v
WHERE   pc.ProfitCenterCode = VBRP_PRCTR
AND pc.ControllingArea = VBRP_KOKRS
AND pc.ValidTo >= VBRK_FKDAT
AND pc.RowIsCurrent = 1
GROUP BY pc.ProfitCenterCode,pc.ControllingArea,VBRK_FKDAT;


DROP TABLE IF EXISTS tmp_db_fb2;
CREATE TABLE tmp_db_fb2
AS
SELECT pc.*,t.VBRK_FKDAT,t.min_ValidTo
FROM dim_profitcenter_fact_billing pc,tmp_db_fb1 t
WHERE pc.ProfitCenterCode = t.ProfitCenterCode
AND  pc.ControllingArea = t.ControllingArea
AND pc.ValidTo = t.min_ValidTo;


delete from NUMBER_FOUNTAIN where table_name = 'fact_billing';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_billing',ifnull(max(fact_billingid),0)
FROM fact_billing;



DROP TABLE IF EXISTS tmp_ins_fact_billing;
CREATE TABLE tmp_ins_fact_billing
AS
SELECT VBRK_VBELN as dd_billing_no, VBRP_POSNR as dd_billing_item_no
     FROM    VBRK_VBRP
         INNER JOIN dim_plant dp ON dp.PlantCode = VBRP_WERKS AND dp.RowIsCurrent = 1
     INNER JOIN dim_company dc ON dc.CompanyCode = VBRK_BUKRS;

		  
/* Need exactly the same table structure for combine to work */		  
DROP TABLE IF EXISTS tmp_del_fact_billing;
CREATE TABLE tmp_del_fact_billing
AS
SELECT * FROM tmp_ins_fact_billing where 1 = 2;

INSERT INTO tmp_del_fact_billing
SELECT b.dd_billing_no, b.dd_billing_item_no 	
FROM fact_billing b;

call vectorwise(combine 'tmp_ins_fact_billing - tmp_del_fact_billing');


INSERT INTO fact_billing(ct_BillingQtySalesUOM,
                         amt_ExchangeRate,
                         amt_ExchangeRate_GBL,
                         amt_CashDiscount,
                         ct_BillingQtyStockUOM,
                         dd_billing_no,
                         dd_billing_item_no,
                         dd_CancelledDocumentNo,
                         dd_SalesDlvrItemNo,
                         dd_SalesDlvrDocNo,
                         dd_fiscalyear,
                         dd_postingperiod,
                         Dim_AccountingTransferStatusid,
                         Dim_Companyid,
                         Dim_ConditionProcedureid,
                         Dim_ControllingAreaid,
                         Dim_CreditControlAreaid,
                         Dim_Currencyid,
                         Dim_CustomerGroup1id,
                         Dim_CustomerID,
                         Dim_CustomerPaymentTermsid,
                         Dim_DateidBilling,
                         Dim_DateidCreated,
                         Dim_DistributionChannelId,
                         Dim_DocumentCategoryid,
                         Dim_DocumentTypeid,
                         Dim_IncoTermid,
                         Dim_MaterialGroupid,
                         Dim_Partid,
                         Dim_Plantid,
                         Dim_ProductHierarchyid,
                         Dim_ProfitCenterid,
                         Dim_SalesDivisionid,
                         Dim_SalesGroupid,
                         Dim_SalesOfficeid,
                         Dim_SalesOrgid,
                         Dim_ShipReceivePointid,
                         Dim_StorageLocationid,
                         Dim_UnitOfMeasureId,
                         Dim_SalesOrderItemCategoryid,
                         dd_salesdocno,
                         dd_salesitemno,
                         Dim_BillingCategoryId,
                         Dim_RevenueRecognitionCategoryId,
                         amt_cost_InDocCurrency,
                         amt_NetValueHeader_InDocCurrency,
                         amt_NetValueItem_InDocCurrency,
                         amt_CustomerConfigSubtotal1_InDocCurrency,
                         amt_CustomerConfigSubtotal2_InDocCurrency,
                         amt_CustomerConfigSubtotal3_InDocCurrency,
                         amt_CustomerConfigSubtotal4_InDocCurrency,
                         amt_CustomerConfigSubtotal5_InDocCurrency,
                         amt_CustomerConfigSubtotal6_InDocCurrency,
                         dd_CustomerPONumber,
                         dd_CreatedBy,fact_billingid)
   SELECT VBRP_FKLMG * VBRP_UMVKN / VBRP_UMVKZ,
         ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
              where z.pFromCurrency  = VBRK_WAERK and z.fact_script_name = 'bi_populate_billing_fact' and z.pToCurrency = dc.Currency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT ),1)
			  amt_ExchangeRate ,
   
       /*   getExchangeRate(VBRK_WAERK,
                          pGlobalCurrency,
                          VBRK_KURRF,
                          VBRK_FKDAT)
          amt_ExchangeRate,	*/
		  
		  ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
		  where z.pFromCurrency  = dc.Currency and z.fact_script_name = 'bi_populate_billing_fact' and z.pToCurrency = pGlobalCurrency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT ),1)
		  amt_ExchangeRate_GBL ,
		/*
          getExchangeRate(dc.Currency,
                          pGlobalCurrency,
                          VBRK_KURRF,
                          VBRK_FKDAT)
             amt_ExchangeRate_GBL,	*/
          VBRP_SKFBP amt_CashDiscount,
          VBRP_FKLMG ct_BillingQtyStockUOM,
          VBRK_VBELN dd_billing_no,
          VBRP_POSNR dd_billing_item_no,
          ifnull(VBRK_SFAKN,'Not Set') dd_CancelledDocumentNo,
          VBRP_VGPOS dd_SalesDlvrItemNo,
          ifnull(VBRP_VGBEL, 'Not Set') dd_SalesDlvrDocNo,
          VBRK_GJAHR dd_fiscalyear,
          VBRK_POPER dd_postingperiod,
          ifnull(
             (SELECT dim_accountingtransferstatusid
                FROM dim_accountingtransferstatus ats
               WHERE ats.AccountingTransferStatusCode =
                        ifnull(VBRK_RFBSK, 'Not Set')
                     AND ats.RowIsCurrent = 1),
             1)
             Dim_AccountingTransferStatusid,
          ifnull((SELECT dim_companyid
             FROM dim_company c
            WHERE c.companycode = VBRK_BUKRS
                  AND c.RowIsCurrent = 1),1)
             Dim_Companyid,
          ifnull(
             (SELECT Dim_ConditionProcedureid
                FROM dim_conditionprocedure dc
               WHERE dc.Dim_ConditionProcedureid =
                        ifnull(VBRK_KALSM, 'Not Set')
                     AND dc.RowIsCurrent = 1),
             1)
             Dim_ConditionProcedureid,
          ifnull(
             (SELECT Dim_ControllingAreaid
                FROM dim_controllingarea ca
               WHERE ca.Dim_ControllingAreaid = ifnull(VBRP_KOKRS, 'Not Set')),
             1)
             Dim_ControllingAreaid,
          ifnull(
             (SELECT Dim_CreditControlAreaid
                FROM dim_creditcontrolarea cca
               WHERE cca.CreditControlAreaName = VBRP_KOKRS
                     AND cca.RowIsCurrent = 1),
             1)
             Dim_CreditControlAreaid,
             ifnull( ( SELECT dim_currencyid FROM dim_currency c
                       WHERE c.CurrencyCode = dc.Currency), 1)
             Dim_Currencyid,
          ifnull((SELECT dim_customergroup1id
                    FROM dim_customergroup1 cg1
                   WHERE cg1.CustomerGroup = ifnull(VBRK_KDGRP, 'Not Set')),
                 1)
             Dim_CustomerGroup1id,
          ifnull(
             (SELECT dim_customerid
                FROM dim_customer c
               WHERE c.CustomerNumber = ifnull(VBRK_KUNAG, 'Not Set')
                     AND c.RowIsCurrent = 1),
             1)
             Dim_CustomerID,
          ifnull(
             (SELECT Dim_CustomerPaymentTermsid
                FROM dim_customerpaymentterms cpt
               WHERE cpt.PaymentTermCode = ifnull(VBRK_ZTERM, 'Not Set')
                     AND cpt.RowIsCurrent = 1),
             1)
             Dim_CustomerPaymentTermsid,
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = ifnull(VBRK_FKDAT, '31 Dec 9999')
                     AND dt.CompanyCode = ifnull(VBRK_BUKRS, 'Not Set')),
             1)
             Dim_DateidBilling,
          ifnull(
             (SELECT dim_dateid
                FROM dim_date dt
               WHERE dt.DateValue = VBRK_ERDAT AND dt.CompanyCode = VBRK_BUKRS),1) Dim_DateIdCreated,
          ifnull(
             (SELECT Dim_DistributionChannelid
                FROM dim_distributionchannel dc
               WHERE dc.DistributionChannelCode =
                        ifnull(VBRK_VTWEG, 'Not Set')
                     AND dc.RowIsCurrent = 1),
             1)
             Dim_DistributionChannelId,
          ifnull(
             (SELECT Dim_documentcategoryid
                FROM dim_documentcategory dc1
               WHERE ltrim(rtrim(dc1.DocumentCategory)) = ltrim(rtrim(ifnull(VBRK_VBTYP, 'Not Set')))
                     AND dc1.RowIsCurrent = 1),
             1)
             Dim_DocumentCategoryid,
          ifnull((SELECT dim_billingdocumenttypeid
             FROM dim_billingdocumenttype bdt
            WHERE bdt.Type = VBRK_FKART
                  AND bdt.RowIsCurrent = 1),1)
             Dim_DocumentTypeid,
          ifnull((SELECT dim_IncoTermId
             FROM dim_incoterm it
            WHERE it.IncoTermCode = VBRK_INCO1
                  AND it.RowIsCurrent = 1),1)
             Dim_IncoTermid,
          ifnull((SELECT dim_materialgroupid
             FROM dim_materialgroup mg
            WHERE mg.MaterialGroupCode = VBRP_MATKL
                  AND mg.RowIsCurrent = 1),1)
             Dim_MaterialGroupid,
          ifnull(
             (SELECT dim_partid
                FROM dim_part p
               WHERE     p.PartNumber = VBRP_MATNR
                     AND p.Plant = VBRP_WERKS
                     AND p.RowIsCurrent = 1),
             1)
             Dim_Partid,
          ifnull(dim_Plantid,1) dim_Plantid,
          ifnull((SELECT dim_producthierarchyid
             FROM dim_producthierarchy ph
            WHERE ph.ProductHierarchy = VBRP_PRODH
                  AND ph.RowIsCurrent = 1),1)
             dim_producthierarchyid,
            ifnull((SELECT pc.dim_profitcenterid
                      FROM tmp_db_fb2 pc
                      WHERE   pc.ProfitCenterCode = VBRP_PRCTR
                          AND pc.ControllingArea = VBRP_KOKRS
                          AND pc.VBRK_FKDAT = VBRK_FKDAT
                          AND pc.RowIsCurrent = 1 ),1) Dim_ProfitCenterid,
          ifnull((SELECT dim_salesdivisionid
             FROM dim_salesdivision sd
            WHERE sd.DivisionCode = VBRK_SPART),1)
             Dim_SalesDivisionid,
          ifnull((SELECT dim_salesgroupid
             FROM dim_salesgroup sg
            WHERE sg.SalesGroupCode = VBRP_VKGRP),1)
             Dim_SalesGroupid,
          ifnull((SELECT dim_salesofficeid
             FROM dim_salesoffice so
            WHERE so.SalesOfficeCode = VBRP_VKBUR),1)
             Dim_SalesOfficeid,
          ifnull((SELECT dim_salesorgid
             FROM dim_salesorg so
            WHERE so.SalesOrgCode = VBRK_VKORG),1)
             Dim_SalesOrgid,
          ifnull((SELECT dim_shipreceivepointid
             FROM dim_shipreceivepoint srp
            WHERE srp.ShipReceivePointCode = VBRP_VSTEL),1)
             Dim_ShipReceivePointid,
          ifnull(
             (SELECT dim_storagelocationid
                FROM dim_storagelocation sl
               WHERE     sl.LocationCode = VBRP_LGORT
                     AND sl.plant = VBRP_WERKS
                     AND sl.RowIsCurrent = 1),
             1)
             Dim_StorageLocationid,
          ifnull((SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure um
            WHERE um.UOM = VBRP_MEINS
                  AND um.RowIsCurrent = 1),1)
             Dim_UnitOfMeasureId,
          ifnull((SELECT soic.Dim_SalesOrderItemCategoryid
             FROM dim_salesorderitemcategory soic
            WHERE soic.SalesOrderItemCategory = VBRP_PSTYV
                  AND soic.RowIsCurrent = 1),1),
          ifnull(VBRP_AUBEL, 'Not Set'),
          VBRP_AUPOS,
          ifnull((SELECT Dim_BillingCategoryId
                    FROM dim_billingcategory bc
                   WHERE bc.Category = VBRK_FKTYP
                   AND bc.RowIsCurrent = 1 ),
                 1) Dim_BillingCategoryId ,
          ifnull((SELECT Dim_RevenueRecognitionCategoryId
                    FROM dim_RevenueRecognitioncategory rrc
                   WHERE rrc.Category = VBRP_RRREL
                   AND rrc.RowIsCurrent = 1 ),
                 1) Dim_RevenueRecognitionCategoryId,
       VBRP_WAVWR,
       VBRK_NETWR,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_NETWR,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6,
       ifnull(VBRK_BSTNK_VF,'Not Set'),
       ifnull(VBRK_ERNAM,'Not Set') dd_CreatedBy,
	    (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_billing') + row_number() over ()
     FROM    VBRK_VBRP 
	 INNER JOIN dim_plant dp ON dp.PlantCode = VBRP_WERKS AND dp.RowIsCurrent = 1
     INNER JOIN dim_company dc ON dc.CompanyCode = VBRK_BUKRS, 
	 tmp_pGlobalCurrency_fact_billing,
	 tmp_ins_fact_billing b
	WHERE b.dd_billing_no = VBRK_VBELN
	AND b.dd_billing_item_no = VBRP_POSNR;
	
	/*
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_billing b
                   WHERE b.dd_billing_no = VBRK_VBELN
                         AND b.dd_billing_item_no = VBRP_POSNR)	*/
						 
						 
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_billingid),0) from fact_billing)
where table_name = 'fact_billing';						 


/* Update 2  - RECURSIVE not required to be removed */

/* Update 3 */ 

UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET
 amt_cost = VBRP_WAVWR * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_NetValueHeader = VBRK_NETWR * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_NetValueItem = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_NETWR * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal1 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1 * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal2 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2 * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal3 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3 * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal4 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4 * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal5 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5 * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal6 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6 * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_Billing fb
  FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm
   SET fb.dim_Customermastersalesid = cms.dim_Customermastersalesid
 WHERE     fb.dim_salesorgid = sorg.dim_salesorgid
       AND fb.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = fb.dim_salesdivisionid
       AND cm.dim_customerid = fb.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber;

UPDATE fact_Billing fb
SET fb.dim_Customermastersalesid = 1
WHERE fb.dim_Customermastersalesid IS NULL;

 
/* Update 4 */

/* Update 4 - Part 1 */   
/* Shanthi's queries from here */


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
   SET fb.Dim_SalesDocumentItemStatusId = ifnull(fso.dim_salesorderitemstatusid,1)
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND IFNULL(fb.Dim_SalesDocumentItemStatusId,-1) <> ifnull(fso.dim_salesorderitemstatusid,1);

UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
  SET  fb.Dim_so_CostCenterid = fso.Dim_CostCenterid
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND IFNULL(fb.Dim_so_CostCenterid,-1) <> IFNULL(fso.Dim_CostCenterid,-2);

UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
  set  fb.Dim_so_SalesDocumentTypeid = fso.Dim_SalesDocumentTypeid
   WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND IFNULL(fb.Dim_so_SalesDocumentTypeid,-1) <> IFNULL(fso.Dim_SalesDocumentTypeid,-2);


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_so_DateidSchedDelivery = fso.Dim_DateidSchedDelivery
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND IFNULL(fb.Dim_so_DateidSchedDelivery,-1) = IFNULL(fso.Dim_DateidSchedDelivery,-2);


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
 SET   fb.Dim_so_SalesOrderCreatedId = fso.Dim_DateidSalesOrderCreated
  WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND fb.Dim_so_SalesOrderCreatedId <> fso.Dim_DateidSalesOrderCreated;

	   
UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId = fso.Dim_CustomPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND fb.Dim_CustomPartnerFunctionId <> fso.Dim_CustomPartnerFunctionId;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_PayerPartnerFunctionId = fso.Dim_PayerPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND fb.Dim_PayerPartnerFunctionId <> fso.Dim_PayerPartnerFunctionId;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_BillToPartyPartnerFunctionId = fso.Dim_BillToPartyPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND fb.Dim_BillToPartyPartnerFunctionId <> fso.Dim_BillToPartyPartnerFunctionId;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomerGroupId = fso.Dim_CustomerGroupId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId1 = fso.Dim_CustomPartnerFunctionId1
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId2 = fso.Dim_CustomPartnerFunctionId2
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomeridShipTo = fso.Dim_CustomeridShipTo
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET    fb.ct_so_ConfirmedQty = ifnull((SELECT SUM(f_so.ct_ConfirmedQty) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno),0)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
        vbrk_vbrp vkp
SET    fb.amt_so_UnitPrice = fso.amt_UnitPrice
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

   
   
UPDATE fact_billing fb FROM
       fact_salesorder fso,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET       fb.ct_so_DeliveredQty = ifnull(( SELECT SUM(f_so.ct_DeliveredQty) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno) ,0)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

UPDATE fact_billing fb FROM
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET       fb.amt_so_Returned =
                ifnull((SELECT SUM(f_so.amt_ScheduleTotal) FROM fact_salesorder f_so,
                                                 Dim_documentcategory dc
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno
                                     AND dc.Dim_DocumentCategoryid = f_so.Dim_DocumentCategoryid
                                     AND dc.DocumentCategory IN ('H', 'K')
                                     AND f_so.Dim_SalesOrderRejectReasonid <> 1),0)

 WHERE     fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

   
DROP TABLE IF EXISTS tmpbilling_fact_salesorder;
CREATE TABLE tmpbilling_fact_salesorder
AS
SELECT f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,
		SUM(f_so.ct_ScheduleQtySalesUnit) - SUM(f_so.ct_DeliveredQty ) as diff_sched_dlvrqty
FROM fact_salesorder f_so,fact_billing fb
WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
AND f_so.dd_SalesItemNo = fb.dd_salesitemno
GROUP BY f_so.dd_SalesDocNo,f_so.dd_SalesItemNo;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       dim_billingmisc bm,
       Dim_documentcategory dc,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
 SET      fb.ct_so_OpenOrderQty =
                    ifnull((SELECT diff_sched_dlvrqty FROM tmpbilling_fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno),0)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

DROP TABLE IF EXISTS tmpbilling_fact_salesorder;   

UPDATE fact_billing fb FROM
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET    fb.ct_so_ReturnedQty =
                ifnull((SELECT SUM(f_so.ct_ScheduleQtySalesUnit) FROM fact_salesorder f_so,
                                    Dim_documentcategory dc
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno
                                     AND dc.Dim_DocumentCategoryid = f_so.Dim_DocumentCategoryid
                                     AND dc.DocumentCategory IN ('H', 'K')
                                                         AND f_so.Dim_SalesOrderRejectReasonid <> 1
                                     ),0)
 WHERE     fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       Dim_documentcategory dc,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET    fb.amt_revenue =
          (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
      WHEN '5' THEN 0
      WHEN '6' THEN 0
      WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
      WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
              ELSE fb.amt_NetValueItem
           END)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       Dim_documentcategory dc,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp

SET    fb.amt_Revenue_InDocCurrency = (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
      WHEN '5' THEN 0
      WHEN '6' THEN 0
      WHEN 'N' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
      WHEN 'O' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
              ELSE fb.amt_NetValueItem_InDocCurrency
           END)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       Dim_documentcategory dc,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
 SET   fb.amt_so_confirmed =
          ifnull((SELECT SUM(
                       f_so.ct_ConfirmedQty
                     * (f_so.amt_UnitPrice
                     / (CASE WHEN f_so.ct_PriceUnit = 0 THEN NULL ELSE f_so.ct_PriceUnit END)))
             FROM fact_salesorder f_so
            WHERE f_so.dd_SalesDocNo = fb.dd_SalesDocNo
                  AND f_so.dd_salesitemno = fb.dd_salesitemno),0)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');



DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;

CREATE TABLE tmp_openorder_from_Sales_to_billing AS
SELECT f_so.dd_SalesDocNo, f_so.dd_salesitemno,SUM(f_so.ct_ScheduleQtySalesUnit) as orderqty,SUM(f_so.ct_DeliveredQty) as DlvrQty, AVG(f_so.amt_UnitPrice) as TotalUnitPrice, ifnull(avg(f_so.ct_PriceUnit),0) as PriceUnit from fact_salesorder f_so
group by f_so.dd_SalesDocNo,f_so.dd_salesitemno;

UPDATE fact_billing fb FROM
           tmp_openorder_from_Sales_to_billing t,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm,
       vbrk_vbrp vkp
SET       fb.amt_so_openorder =
          ((t.orderqty - t.DlvrQty)* t.TotalUnitPrice /t.PriceUnit)
 WHERE t.dd_salesDocNo = fb.dd_Salesdocno
       AND t.dd_SalesItemNo = fb.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;

UPDATE fact_billing fb
 from VBRK_VBRP vkp,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt
SET fb.amt_sd_Cost = ifnull(( SELECT SUM(f_sod.amt_Cost) FROM fact_salesorderdelivery f_sod
                                   WHERE f_sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
                                     AND f_sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo),0)
  WHERE     fb.dd_billing_no = vkp.VBRK_VBELN
        AND fb.dd_billing_item_no = vkp.VBRP_POSNR
        AND fb.dim_billingmiscid = bm.dim_billingmiscid
        and bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

 UPDATE fact_billing fb
 from VBRK_VBRP vkp,
           fact_salesorderdelivery sod,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt
 SET fb.Dim_sd_DateidActualGoodsIssue = sod.Dim_DateidActualGoodsIssue
  WHERE     fb.dd_billing_no = vkp.VBRK_VBELN
        AND fb.dd_billing_item_no = vkp.VBRP_POSNR
        AND sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
        AND sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo
                AND fb.dim_billingmiscid = bm.dim_billingmiscid
                and bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;

CREATE TABLE tmp_openorder_from_Shipment_to_billing AS
SELECT f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo,SUM(f_sod.ct_QtyDelivered) as dlvrqty,AVG(f_sod.amt_UnitPrice) as AmtUnitPrice,ifnull(avg(f_sod.ct_PriceUnit),0) as PriceUnit from fact_salesorderdelivery f_sod
group by f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo;


  UPDATE fact_billing fb
  from VBRK_VBRP vkp,
           tmp_openorder_from_Shipment_to_billing t,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm
    SET fb.amt_sd_Shipped = (t.dlvrqty * t.AmtUnitPrice/(case when t.PriceUnit = 0 then null else t.PriceUnit end ))
  WHERE     fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo
        AND fb.dd_billing_no = vkp.VBRK_VBELN
        AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');
   
   
  UPDATE fact_billing fb
  from VBRK_VBRP vkp,
           tmp_openorder_from_Shipment_to_billing t,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm
    SET 
        fb.ct_sd_QtyDelivered = t.dlvrqty
  WHERE     fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo
        AND fb.dd_billing_no = vkp.VBRK_VBELN
        AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');   

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;

DROP table if exists tmp_SalesOrderAmtForBilling;


  CREATE TABLE tmp_SalesOrderAmtForBilling
  AS
  SELECT dd_billing_no,dd_billing_item_no,dt.datevalue as billingdate,so.dd_SalesDocNo,so.dd_salesItemno,AVG(fb.ct_sd_QtyDelivered) as sd_QtyDelivered,AVG(fb.amt_sd_Shipped) as sd_shipped,sum(so.ct_ScheduleQtySalesUnit) as totalorderqty,sum(so.amt_ScheduleTotal) as totalorderamt,000000000000000000.0000 as acttotalqty, 000000000000000000.0000 as acttotalamt
  FROM fact_billing fb, fact_salesorder so,dim_date dt, dim_billingdocumenttype bdt,
         dim_billingmisc bm
   WHERE fb.dd_SalesDocNo = so.dd_salesdocno
  and fb.dd_salesitemno = so.dd_SalesItemNo
  and fb.Dim_DateidBilling = dt.dim_dateid
  and fb.dd_Salesdocno <> 'Not Set'
  AND fb.dim_billingmiscid = bm.dim_billingmiscid
  AND bm.CancelFlag = 'Not Set'
  AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
  AND bdt.type IN ('F1','F2','ZF2','ZF2C')
  group by dd_billing_no,dd_billing_item_no,dt.datevalue,so.dd_SalesDocNo,so.dd_salesItemno;

  UPDATE tmp_SalesOrderAmtForBilling
  set acttotalamt =  sd_Shipped,
    acttotalqty =  sd_QtyDelivered
  where totalorderamt > sd_Shipped;

  DROP table if exists tmp_SalesOrderAmtForBilling_100;

  CREATE TABLE tmp_SalesOrderAmtForBilling_100
  AS SELECT dd_salesDocNo,dd_salesitemno, max(billingdate) as billdate,sum(acttotalamt) as ActualAmt,sum(acttotalqty) as ActualQty from
  tmp_SalesOrderAmtForBilling group by dd_salesDocNo,dd_salesitemno;

  UPDATE tmp_SalesOrderAmtForBilling t1 FROM
    tmp_SalesOrderAmtForBilling_100 t2
  set t1.acttotalamt = t1.acttotalamt + (t1.TotalOrderamt - t2.ActualAmt),
      t1.acttotalqty = t1.acttotalqty + (t1.Totalorderqty - t2.actualqty)
  where t1.dd_Salesdocno = t2.dd_salesdocno
    AND t1.dd_salesitemno = t2.dd_Salesitemno
    and t1.billingdate = t2.billdate
    and t1.totalorderamt > t2.ActualAmt;

  DROP table if exists tmp_SalesOrderAmtForBilling_100;

  UPDATE fact_billing fb
  from tmp_SalesOrderAmtForBilling tmp
  set fb.amt_so_ScheduleTotal = tmp.acttotalamt
  WHERE fb.dd_billing_no = tmp.dd_billing_no
  and fb.dd_billing_item_no = tmp.dd_billing_item_no
  AND fb.amt_so_ScheduleTotal <> tmp.acttotalamt
  AND IFNULL(fb.amt_so_ScheduleTotal,-2) = IFNULL(tmp.acttotalamt,-2);
  
  UPDATE fact_billing fb
  from tmp_SalesOrderAmtForBilling tmp
  set 
      fb.ct_so_ScheduleQtySalesUnit = tmp.acttotalqty
  WHERE fb.dd_billing_no = tmp.dd_billing_no
  and fb.dd_billing_item_no = tmp.dd_billing_item_no
  AND IFNULL(fb.ct_so_ScheduleQtySalesUnit,-1) <> IFNULL(tmp.acttotalqty,-2);  

  DROP table if exists tmp_SalesOrderAmtForBilling;
  
  UPDATE fact_salesorderdelivery sod
  FROM fact_billing fb,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm
    SET sod.dd_billing_no = fb.dd_billing_no
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
        AND bdt.type IN ('F1','F2','ZF2','ZF2C')
AND bm.dim_billingmiscid = fb.dim_billingmiscid
AND bm.CancelFlag = 'Not Set'
AND sod.dd_billing_no <> fb.dd_billing_no;  
  
  UPDATE fact_salesorderdelivery sod
  FROM fact_billing fb,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm
    SET sod.dd_billingitem_no = fb.dd_billing_item_no
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
        AND bdt.type IN ('F1','F2','ZF2','ZF2C')
AND bm.dim_billingmiscid = fb.dim_billingmiscid
AND bm.CancelFlag = 'Not Set'
AND sod.dd_billingitem_no <> fb.dd_billing_item_no;

select 'END OF PROC bi_populate_billing_fact',TIMESTAMP(LOCAL_TIMESTAMP);
  
	   
