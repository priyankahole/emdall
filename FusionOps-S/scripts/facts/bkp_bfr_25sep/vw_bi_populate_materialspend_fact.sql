
/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 19 May 2013 */
/*   Description    : Stored Proc bi_populate_materialspend_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   17-Jul-2013	  Lokesh    1.1               Remove all updates, truncate and insert fact_materialspend	  */
/*   19 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/



DROP TABLE IF EXISTS fact_materialspend_tmp_populate;
CREATE TABLE fact_materialspend_tmp_populate
AS
SELECT * FROM fact_materialspend
where 1=2;

ALTER TABLE fact_materialspend_tmp_populate
add  PRIMARY KEY (fact_materialspendid);


delete from NUMBER_FOUNTAIN where table_name = 'fact_materialspend_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_materialspend_tmp_populate',ifnull(max(fact_materialspendid),0)
FROM fact_materialspend_tmp_populate;

/* Insert 1 */

/* Split the insert to remove not exists and use a combine and a join instead */

DROP TABLE IF EXISTS tmp_ins_fact_materialspend;
CREATE TABLE tmp_ins_fact_materialspend
AS
SELECT distinct dd_MaterialDocNo,f.dd_MaterialDocItemNo,f.dd_MaterialDocYear
FROM fact_materialmovement f INNER JOIN dim_movementtype mt
          ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
    WHERE mt.MovementType IN (101,102,105,106,122,123,161,162,501,502,521,522)
          AND dd_ConsignmentFlag = 0;
		  

  INSERT INTO fact_materialspend_tmp_populate(dd_MaterialDocNo,
                              dd_MaterialDocItemNo,
                              dd_MaterialDocYear,
                              dd_DocumentNo,
                              dd_DocumentItemNo,
                              dd_SalesOrderNo,
                              dd_SalesOrderItemNo,
                              dd_SalesOrderDlvrNo,
                              dd_GLAccountNo,
                              amt_ExchangeRate,
                              amt_ExchangeRate_GBL,
                              amt_LocalCurrAmt,
                              amt_DeliveryCost,
                              amt_AltPriceControl,
                              amt_GRValue,
                              amt_StdGRAmt,
                              ct_Quantity,
                              Dim_MovementTypeid,
                              dim_Companyid,
                              Dim_Currencyid,
                              Dim_Partid,
                              Dim_Plantid,
                              Dim_StorageLocationid,
                              Dim_Vendorid,
                              dim_DateIDMaterialDocDate,
                              dim_DateIDPostingDate,
                              Dim_DateIDDocCreation,
                              Dim_DateIDOrdered,
                              dim_producthierarchyid,
                              dim_MovementIndicatorid,
                              dirtyrow,
                              dim_Customerid,
                              dim_CostCenterid,
                              dd_debitcreditid,
                              --#dim_movementreasonid,
                              dd_GoodsMoveReason,
                              Dim_AccountCategoryid,
                              Dim_ConsumptionTypeid,
                              Dim_ControllingAreaid,
                              Dim_ProfitCenterId,
                              Dim_CustomerGroup1id,
                              Dim_DocumentCategoryid,
                              Dim_DocumentTypeid,
                              Dim_ItemCategoryid,
                              Dim_Termid,
                              Dim_ItemStatusid,
                              dim_productionorderstatusid,
                              Dim_productionordertypeid,
                              Dim_PurchaseGroupid,
                              Dim_PurchaseOrgid,
                              Dim_SalesDocumentTypeid,
                              Dim_SalesGroupid,
                              Dim_SalesOrderHeaderStatusid,
                              Dim_SalesOrderItemStatusid,
                              Dim_SalesOrderRejectReasonid,
                              Dim_SalesOrgid,
                              dim_specialstockid,
                              Dim_StockTypeid,
                              Dim_UnitOfMeasureid,
                              dd_productionordernumber,
                              dd_productionorderitemno,
                              Dim_MaterialGroupid,
                              dd_ConsignmentFlag,fact_materialspendid)
    SELECT f.dd_MaterialDocNo,
          f.dd_MaterialDocItemNo,
          f.dd_MaterialDocYear,
          ifnull(dd_DocumentNo,'Not Set'),
          dd_DocumentItemNo,
          ifnull(dd_SalesOrderNo,'Not Set'),
          dd_SalesOrderItemNo,
          ifnull(dd_SalesOrderDlvrNo,'Not Set'),
          ifnull(dd_GLAccountNo,'Not Set'),
          amt_ExchangeRate,
          amt_ExchangeRate_GBL,
          amt_LocalCurrAmt,
          amt_DeliveryCost,
          amt_AltPriceControl,
          (f.amt_POUnitPrice * f.ct_QtyEntryUOM) amt_GRValue,
          ifnull((f.amt_StdUnitPrice * f.ct_QtyEntryUOM), 0) amt_StdGRAmt,
          ct_QtyEntryUOM,
          f.Dim_MovementTypeid,
          dim_Companyid,
          Dim_Currencyid,
          Dim_Partid,
          Dim_Plantid,
          Dim_StorageLocationid,
          Dim_Vendorid,
          dim_DateIDMaterialDocDate,
          dim_DateIDPostingDate,
          Dim_DateIDDocCreation,
          Dim_DateIDOrdered,
          dim_producthierarchyid,
          dim_MovementIndicatorid,
          dirtyrow,
          dim_Customerid,
          dim_CostCenterid,
          dd_debitcreditid,
          --#dim_movementreasonid,
          dd_GoodsMoveReason,
          Dim_AccountCategoryid,
          Dim_ConsumptionTypeid,
          Dim_ControllingAreaid,
          Dim_ProfitCenterId,
          Dim_CustomerGroup1id,
          Dim_DocumentCategoryid,
          Dim_DocumentTypeid,
          Dim_ItemCategoryid,
          Dim_Termid,
          Dim_ItemStatusid,
          dim_productionorderstatusid,
          Dim_productionordertypeid,
          Dim_PurchaseGroupid,
          Dim_PurchaseOrgid,
          Dim_SalesDocumentTypeid,
          Dim_SalesGroupid,
          Dim_SalesOrderHeaderStatusid,
          Dim_SalesOrderItemStatusid,
          Dim_SalesOrderRejectReasonid,
          Dim_SalesOrgid,
          dim_specialstockid,
          Dim_StockTypeid,
          Dim_UnitOfMeasureid,
          dd_productionordernumber,
          dd_productionorderitemno,
          Dim_MaterialGroupid,
          f.dd_ConsignmentFlag, (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_materialspend_tmp_populate') + row_number() over()
    FROM fact_materialmovement f 
	  INNER JOIN dim_movementtype mt ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
	  INNER JOIN tmp_ins_fact_materialspend ms ON ms.dd_MaterialDocNo = f.dd_MaterialDocNo
							 AND ms.dd_MaterialDocItemNo = f.dd_MaterialDocItemNo
							 AND ms.dd_MaterialDocYear = f.dd_MaterialDocYear
    WHERE mt.MovementType IN (101,102,105,106,122,123,161,162,501,502,521,522)
          AND dd_ConsignmentFlag = 0;
		  
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_materialspendid),0) from fact_materialspend_tmp_populate)
where table_name = 'fact_materialspend_tmp_populate';	  		  
						 
						 
/* Insert 2 */		

call vectorwise (combine 'tmp_ins_fact_materialspend - tmp_ins_fact_materialspend');				 

INSERT INTO tmp_ins_fact_materialspend
SELECT distinct dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
FROM fact_materialmovement f INNER JOIN dim_movementtype mt
		ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
WHERE dd_ConsignmentFlag = 1;


  INSERT INTO fact_materialspend_tmp_populate(dd_MaterialDocNo,
                              dd_MaterialDocItemNo,
                              dd_MaterialDocYear,
                              dd_DocumentNo,
                              dd_DocumentItemNo,
                              dd_SalesOrderNo,
                              dd_SalesOrderItemNo,
                              dd_SalesOrderDlvrNo,
                              dd_GLAccountNo,
                              amt_ExchangeRate,
                              amt_ExchangeRate_GBL,
                              amt_LocalCurrAmt,
                              amt_DeliveryCost,
                              amt_AltPriceControl,
                              amt_GRValue,
                              amt_StdGRAmt,
                              ct_Quantity,
                              Dim_MovementTypeid,
                              dim_Companyid,
                              Dim_Currencyid,
                              Dim_Partid,
                              Dim_Plantid,
                              Dim_StorageLocationid,
                              Dim_Vendorid,
                              dim_DateIDMaterialDocDate,
                              dim_DateIDPostingDate,
                              Dim_DateIDDocCreation,
                              Dim_DateIDOrdered,
                              dim_producthierarchyid,
                              dim_MovementIndicatorid,
                              dirtyrow,
                              dim_Customerid,
                              dim_CostCenterid,
                              dd_debitcreditid,
                              --#dim_movementreasonid,
                              dd_GoodsMoveReason,
                              Dim_AccountCategoryid,
                              Dim_ConsumptionTypeid,
                              Dim_ControllingAreaid,
                              Dim_ProfitCenterId,
                              Dim_CustomerGroup1id,
                              Dim_DocumentCategoryid,
                              Dim_DocumentTypeid,
                              Dim_ItemCategoryid,
                              Dim_Termid,
                              Dim_ItemStatusid,
                              dim_productionorderstatusid,
                              Dim_productionordertypeid,
                              Dim_PurchaseGroupid,
                              Dim_PurchaseOrgid,
                              Dim_SalesDocumentTypeid,
                              Dim_SalesGroupid,
                              Dim_SalesOrderHeaderStatusid,
                              Dim_SalesOrderItemStatusid,
                              Dim_SalesOrderRejectReasonid,
                              Dim_SalesOrgid,
                              dim_specialstockid,
                              Dim_StockTypeid,
                              Dim_UnitOfMeasureid,
                              dd_productionordernumber,
                              dd_productionorderitemno,
                              Dim_MaterialGroupid,
                              dd_ConsignmentFlag,fact_materialspendid)
    SELECT f.dd_MaterialDocNo,
          f.dd_MaterialDocItemNo,
          f.dd_MaterialDocYear,
          ifnull(dd_DocumentNo,'Not Set'),
          dd_DocumentItemNo,
          ifnull(dd_SalesOrderNo,'Not Set'),
          dd_SalesOrderItemNo,
          ifnull(dd_SalesOrderDlvrNo,'Not Set'),
          ifnull(dd_GLAccountNo,'Not Set'),
          amt_ExchangeRate,
          amt_ExchangeRate_GBL,
          amt_LocalCurrAmt,
          amt_DeliveryCost,
          amt_AltPriceControl,
          (f.amt_POUnitPrice * f.ct_QtyEntryUOM) amt_GRValue,
          ifnull((f.amt_StdUnitPrice * f.ct_QtyEntryUOM), 0) amt_StdGRAmt,
          ct_QtyEntryUOM,
          f.Dim_MovementTypeid,
          dim_Companyid,
          Dim_Currencyid,
          Dim_Partid,
          Dim_Plantid,
          Dim_StorageLocationid,
          Dim_Vendorid,
          dim_DateIDMaterialDocDate,
          dim_DateIDPostingDate,
          Dim_DateIDDocCreation,
          Dim_DateIDOrdered,
          dim_producthierarchyid,
          dim_MovementIndicatorid,
          dirtyrow,
          dim_Customerid,
          dim_CostCenterid,
          dd_debitcreditid,
          --#dim_movementreasonid,
          dd_GoodsMoveReason,
          Dim_AccountCategoryid,
          Dim_ConsumptionTypeid,
          Dim_ControllingAreaid,
          Dim_ProfitCenterId,
          Dim_CustomerGroup1id,
          Dim_DocumentCategoryid,
          Dim_DocumentTypeid,
          Dim_ItemCategoryid,
          Dim_Termid,
          Dim_ItemStatusid,
          dim_productionorderstatusid,
          Dim_productionordertypeid,
          Dim_PurchaseGroupid,
          Dim_PurchaseOrgid,
          Dim_SalesDocumentTypeid,
          Dim_SalesGroupid,
          Dim_SalesOrderHeaderStatusid,
          Dim_SalesOrderItemStatusid,
          Dim_SalesOrderRejectReasonid,
          Dim_SalesOrgid,
          dim_specialstockid,
          Dim_StockTypeid,
          Dim_UnitOfMeasureid,
          dd_productionordernumber,
          dd_productionorderitemno,
          Dim_MaterialGroupid,
          f.dd_ConsignmentFlag,(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_materialspend_tmp_populate') + row_number() over()
    FROM fact_materialmovement f 
	 INNER JOIN dim_movementtype mt ON f.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 INNER JOIN tmp_ins_fact_materialspend ms ON ms.dd_MaterialDocNo = f.dd_MaterialDocNo
						 AND ms.dd_MaterialDocItemNo = f.dd_MaterialDocItemNo
						 AND ms.dd_MaterialDocYear = f.dd_MaterialDocYear
    WHERE dd_ConsignmentFlag = 1;
	 
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_materialspendid),0) from fact_materialspend_tmp_populate)
where table_name = 'fact_materialspend_tmp_populate';	  

/* Drop original table and rename staging table to orig table name */
drop table if exists fact_materialspend;
rename table fact_materialspend_tmp_populate to fact_materialspend;	

CALL VECTORWISE(combine 'fact_materialspend');

DROP TABLE IF EXISTS tmp_ins_fact_materialspend;

