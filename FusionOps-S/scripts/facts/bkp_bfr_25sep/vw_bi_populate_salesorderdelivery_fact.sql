/* ################################################################################################################## */
/* */
/*   Script         : bi_populate_salesorderdelivery_fact */
/*   Author         : Ashu */
/*   Created On     : 18 Feb 2013 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_salesorderdelivery_fact from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*   13 Aug 2013     Issam     1.9               Added Sales District */
/*   29 Apr 2013     Hiten     1.2               Revised population logic */
/*   24 Feb 2013     Lokesh    1.1		 Add part 2 + while loop logic  */
/*   18 Feb 2013     Ashu      1.0               Existing code migrated to Vectorwise */
/* #################################################################################################################### */

/*Refresh the required tables from corresponding mysql db first ( for testing ) */
/*cd /home/fusionops/ispring/db/schema_migration/bin */
/*./refresh_vw_from_mysql_sameserver.sh_lk albea albea dim_billingdocumenttype dim_controllingarea dim_customer dim_date dim_distributionchannel dim_part dim_plant dim_producthierarchy dim_salesorderheaderstatus dim_salesorderitemstatus dim_storagelocation fact_salesorder fact_salesorderdelivery likp_lips systemproperty */

select 'START OF PROC bi_populate_salesorderdelivery_fact',TIMESTAMP(LOCAL_TIMESTAMP);

select current_time;

Drop table if exists flag_holder_722;
Drop table if exists cursor_table1_722;


DROP TABLE IF EXISTS NUMBER_FOUNTAIN;
CREATE TABLE NUMBER_FOUNTAIN
(
table_name      varchar(40) NOT NULL,
max_id          int     NOT NULL
);


DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'fact_salesorderdelivery';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_salesorderdelivery',ifnull(max(fact_salesorderdeliveryid),0)
FROM fact_salesorderdelivery;

Create table cursor_table1_722(
v_iid		  Integer,
v_DlvrDocNo       VARCHAR(50) null,
v_plantcode       VARCHAR(50) null,
v_DlvrItemNo      INTEGER null,
v_SalesDocNo      VARCHAR(50) null,
v_SalesItemNo     INTEGER null,
v_DeliveryQty     DECIMAL(18,4) null,
v_AGI_Date        DATE null,
v_PGI_Date        DATE null,
v_GIDate          DATE null,
v_DlvrCost        DECIMAL(18,4) null,
v_SchedQty        DECIMAL(18,4) null,
v_SchedNo         INTEGER null,
v_ControllingArea VARCHAR(4) null,
v_ProfitCenter    VARCHAR(10) null,
v_Billingdate     Timestamp(0) null,
v_BillingType     VARCHAR(4) null,
v_DistChannel     VARCHAR(2) null,
v_DlvrRowNo	   INTEGER null,
v_DlvrRowNoMax	   INTEGER null,
v_DeliveryQtyCUMM  DECIMAL(18,4) null);

Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingDate,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select first 0 row_number() over(),
	LIKP_VBELN v_DlvrDocNo, 
	LIPS_POSNR v_DlvrItemNo,
	LIPS_VGBEL v_SalesDocNo,
	LIPS_VGPOS v_SalesItemNo,
	LIPS_LFIMG v_DeliveryQty,
	ifnull(LIKP_WADAT_IST,LIKP_WADAT) v_AGI_Date,
	LIKP_WADAT v_PGI_Date,
	LIPS_WAVWR v_DlvrCost,
	LIPS_WERKS v_plantcode,
	LIPS_KOKRS v_ControllingArea,
	LIPS_PRCTR v_ProfitCenter,
	LIKP_FKDAT v_BillingDate,
	LIKP_FKARV v_BillingType,
	LIKP_VTWIV v_DistChannel,
	 ROW_NUMBER() OVER (PARTITION BY LIPS_VGBEL,LIPS_VGPOS ORDER BY LIKP_WADAT_IST,LIKP_WADAT,LIKP_VBELN,LIPS_POSNR) AS v_DlvrRowNo,
	 COUNT(1) OVER (PARTITION BY LIPS_VGBEL,LIPS_VGPOS) v_DlvrRowNoMax
from LIKP_LIPS 
where exists (select 1 from fact_salesorder f1 
              where f1.dd_SalesDocNo = LIPS_VGBEL and f1.dd_SalesItemNo = LIPS_VGPOS and f1.dd_ItemRelForDelv = 'X')
      and LIPS_LFIMG > 0
order by LIKP_VBELN, v_AGI_Date, LIPS_POSNR;

Create table flag_holder_722 as
         SELECT ifnull(property_value,'true') pDeltaChangesFlag
                   FROM systemproperty
                  WHERE property = 'process.delta.salesorderdelivery';

call vectorwise(combine 'cursor_table1_722');

/*** Remove deleted lines ***/

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE exists (select 1 from LIKP_LIPS a where dd_SalesDlvrDocNo = LIKP_VBELN)
AND not exists ( SELECT 1 from LIKP_LIPS where dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrDocNo = LIPS_POSNR)
AND pDeltaChangesFlag = 'true';

call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722');
Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.* FROM fact_salesorderdelivery,flag_holder_722
WHERE exists (select 1 from LIKP_LIPS a where dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrDocNo = LIPS_POSNR)
AND pDeltaChangesFlag = 'true';

call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722');
Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE not exists (select 1 from fact_salesorder f 
                      where fact_salesorderdelivery.dd_SalesDocNo = f.dd_SalesDocNo 
                            and fact_salesorderdelivery.dd_SalesItemNo = f.dd_SalesItemNo
                            and fact_salesorderdelivery.dd_ScheduleNo = f.dd_ScheduleNo)
AND pDeltaChangesFlag = 'true';

call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722');
Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE dd_SalesDlvrDocNo = 'Not Set'
AND pDeltaChangesFlag = 'true';

call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722');
Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
where  pDeltaChangesFlag <> 'true';

call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722');
Drop table if exists delete_tbl_722;

/*** Update cummulative delivery qty ***/

Drop table if exists cursor_table1_723;
Create table cursor_table1_723 as
select  a.v_iid,
	a.v_DlvrDocNo,
	a.v_plantcode,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_DlvrCost,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_Billingdate,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_DeliveryQty) v_DeliveryQtyCUMM
from cursor_table1_722 a inner join cursor_table1_722 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
group by a.v_iid,
	a.v_DlvrDocNo,
	a.v_plantcode,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_DlvrCost,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_Billingdate,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;

Drop table if exists salesorder_table_101;
Create table salesorder_table_101 as
select f.dd_SalesDocNo v_SalesDocNo,
	f.dd_SalesItemNo v_SalesItemNo,
	f.dd_ScheduleNo v_SchedNo,
	gi.DateValue v_GIDate,
	f.ct_ConfirmedQty v_SchedQty,
	ROW_NUMBER() OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo ORDER BY gi.DateValue,f.dd_ScheduleNo) AS v_DlvrRowNo,
	COUNT(1) OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo) v_DlvrRowNoMax
From fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
where f.dd_ItemRelForDelv = 'X' and f.ct_ConfirmedQty > 0
	and exists (select 1 from cursor_table1_722 where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo);

Drop table if exists cursor_table1_724;
Create table cursor_table1_724 as
select a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_SchedQty) v_SchedQtyCUMM
from salesorder_table_101 a inner join salesorder_table_101 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
group by a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;
Drop table if exists salesorder_table_101;

/*** Get final values with schedule link ***/

Drop table if exists cursor_table1_722;
Create table cursor_table1_722 as
select a.v_iid,
	a.v_DlvrDocNo,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_plantcode,
	a.v_AGI_Date,
	a.v_PGI_Date,
	b.v_GIDate,
	a.v_DlvrCost,
	b.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_Billingdate,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	a.v_DeliveryQtyCUMM,
	case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM 
	    then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax and b.v_SchedQtyCUMM <= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then 0
	    		else
	    		case when b.v_SchedQty > a.v_DeliveryQty 
			      	then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty then a.v_DeliveryQty 
					  when b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty - (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM)
					  else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	else case when b.v_DlvrRowNo = b.v_DlvrRowNoMax 
			      	  		then b.v_SchedQty
			      	  		else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  				when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)  
			      	  					then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 		else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  				when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  			  	else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  		end
			 end
		 end
	    else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty 
		      	then case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then a.v_DeliveryQty + (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM) else a.v_DeliveryQty end
		      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then b.v_SchedQty 
		      	  	else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) 
		      	   end
		 end
	end v_SchedQty,
      case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM 
            then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax
            			then case when (b.v_SchedQtyCUMM - b.v_SchedQty) > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) 
            						then a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)
            				 	else a.v_DeliveryQty end
		      else
		    	case when b.v_SchedQty > a.v_DeliveryQty 
	                      then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty or b.v_DlvrRowNo = b.v_DlvrRowNoMax
	                                then a.v_DeliveryQty 
	                                else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty 
	                                          else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end 
	                           end
	                      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty 
	                      			else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  						when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)  
			      	  							then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 			else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
	                      					when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  		  				else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	   end
	                 end
	              end
            else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty then a.v_DeliveryQty
                      else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) 
                 end
      end v_DeliveryQty,
      ROW_NUMBER() OVER (PARTITION BY a.v_DlvrDocNo,a.v_DlvrItemNo ORDER BY b.v_DlvrRowNo) AS v_DlvrRowSeq
from cursor_table1_723 a
	inner join cursor_table1_724 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DeliveryQtyCUMM > (b.v_SchedQtyCUMM - b.v_SchedQty)	
	and ((b.v_DlvrRowNo < b.v_DlvrRowNoMax and b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)) or b.v_DlvrRowNo = b.v_DlvrRowNoMax);

Drop table if exists cursor_table1_723;
Drop table if exists cursor_table1_724;


/*****/

/* loop table */
drop table if exists loop_tbl_722;
create table loop_tbl_722 as 
select * from cursor_table1_722 where v_DlvrRowNoMax > 0 and v_DeliveryQty > 0;

/* This intermediate table is used to handle the order by asc that was used in the insert query ( in mysql proc ) */
 
DROP TABLE IF EXISTS tmp4a_fs_dimpc ;
CREATE TABLE  tmp4a_fs_dimpc
AS
select pc.ProfitCenterCode,pc.ControllingArea,v_AGI_Date,min(pc.ValidTo) as ValidTo
FROM loop_tbl_722, Dim_ProfitCenter pc
WHERE pc.ProfitCenterCode = v_ProfitCenter
AND pc.ControllingArea = v_ControllingArea
AND pc.ValidTo >= v_AGI_Date
AND pc.RowIsCurrent = 1 
GROUP BY pc.ProfitCenterCode,pc.ControllingArea,v_AGI_Date;


DROP TABLE IF EXISTS tmp4_fs_dimpc ;
CREATE TABLE  tmp4_fs_dimpc
AS
select a.ProfitCenterCode,a.ControllingArea,v_AGI_Date,a.ValidTo,pc.dim_profitcenterid
FROM tmp4a_fs_dimpc a , Dim_ProfitCenter pc
WHERE pc.ProfitCenterCode = a.ProfitCenterCode
AND pc.ControllingArea = a.ControllingArea
AND pc.RowIsCurrent = 1
AND pc.ValidTo = a.ValidTo;
\i /db/schema_migration/bin/wrapper_optimizedb.sh loop_tbl_722;
\i /db/schema_migration/bin/wrapper_optimizedb.sh LIKP_LIPS;
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorder;
\i /db/schema_migration/bin/wrapper_optimizedb.sh Dim_Plant;
 
   INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateidBillingDate,
              Dim_BillingDocumentTypeid,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,
	      fact_salesorderdeliveryid)
      SELECT LIPS_VGBEL dd_SalesDocNo,
              LIPS_VGPOS dd_SalesItemNo,
              f.dd_ScheduleNo,
              LIKP_VBELN dd_SalesDlvrDocNo,
              LIPS_POSNR dd_SalesDlvrItemNo,
              ifnull(lips_bwart ,'Not Set') dd_MovementType,
              IFNULL(lt.v_DeliveryQty,0) ct_QtyDelivered,
              case when v_DlvrRowSeq = 1 then lt.v_DlvrCost else 0 end amt_Cost_DocCurr,
              case when v_DlvrRowSeq = 1 then (lt.v_DlvrCost * (case when f.amt_ExchangeRate < 0 then (1/(-1 * f.amt_ExchangeRate)) else f.amt_ExchangeRate end)) else 0 end amt_Cost,
              lips_vbeaf ct_FixedProcessDays,
              lips_vbeav ct_ShipProcessDays,
              lt.v_SchedQty ct_ScheduleQtySalesUnit,
              lt.v_SchedQty ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = likp_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPlannedGoodsIssue,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGoodsIssue,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = likp_lfdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDeliveryDate,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = likp_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoadingDate,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = likp_kodat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPickingDate,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = likp_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDlvrDocCreated,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = lips_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMatlAvail,
              ifnull((SELECT Dim_CustomerID
                      FROM Dim_Customer cust
                      WHERE cust.CustomerNumber = likp_kunnr),f.Dim_CustomerID) Dim_CustomeridSoldTo,
              ifnull((SELECT Dim_CustomerID
                      FROM Dim_Customer cust
                      WHERE cust.CustomerNumber = likp_kunag),1) Dim_CustomeridShipTo,
              ifnull((SELECT dim_partid
                      FROM dim_part dp
                      WHERE dp.PartNumber = lips_matnr AND dp.Plant = lips_werks),1) Dim_Partid,
              pl.Dim_Plantid,
              ifnull((SELECT Dim_StorageLocationid
                      FROM Dim_StorageLocation sl
                      WHERE sl.LocationCode = lips_lgort and sl.plant = lips_werks),1) Dim_StorageLocationid,
              ifnull((SELECT Dim_ProductHierarchyid
                      FROM Dim_ProductHierarchy ph
                      WHERE ph.ProductHierarchy = lips_prodh),1) Dim_ProductHierarchyid,
              ifnull((select Dim_SalesOrderHeaderStatusid
                      from Dim_SalesOrderHeaderStatus sohs
                      where sohs.SalesDocumentNumber = LIKP_VBELN),1) Dim_DeliveryHeaderStatusid,
              ifnull((select Dim_SalesOrderItemStatusid
                      from Dim_SalesOrderItemStatus sois
                      where sois.SalesDocumentNumber = LIKP_VBELN and sois.SalesItemNumber = LIPS_POSNR),1) Dim_DeliveryItemStatusid,
              f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId,
              ifnull((select pc.Dim_ProfitCenterid
                      from tmp4_fs_dimpc pc
                      where  pc.ProfitCenterCode = v_ProfitCenter
                        AND pc.ControllingArea = v_ControllingArea
			AND pc.ValidTo >= v_AGI_Date
                      ),1) Dim_ProfitCenterid,
              ifnull((select ca.Dim_ControllingAreaid
                      from Dim_ControllingArea ca
                      where  ca.ControllingAreaCode = v_ControllingArea),1) Dim_ControllingAreaId,
              ifnull((SELECT dt.Dim_DateID
                      FROM dim_Date dt
                      WHERE dt.DateValue = v_BillingDate AND dt.CompanyCode = pl.CompanyCode ), 1),
              ifnull((SELECT dim_billingdocumenttypeid
                      FROM dim_billingdocumenttype bdt
                      WHERE bdt.Type = v_BillingType AND bdt.RowIsCurrent= 1), 1),
              ifnull((SELECT Dim_DistributionChannelId
                      FROM dim_distributionchannel dc
                      WHERE dc.DistributionChannelCode = v_DistChannel AND dc.RowIsCurrent = 1),f.Dim_DistributionChannelId),
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGI_Original,
		(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over ()
	FROM	loop_tbl_722 lt
		inner join LIKP_LIPS on LIKP_VBELN = lt.v_DlvrDocNo and LIPS_POSNR = lt.v_DlvrItemNo
		inner join fact_salesorder f on f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS and f.dd_ScheduleNo = lt.v_SchedNo
		inner join Dim_Plant pl on pl.plantcode = LIPS_WERKS;

call vectorwise(combine 'fact_salesorderdelivery');	    

	update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_salesorderdeliveryid),0) from fact_salesorderdelivery)
	where table_name = 'fact_salesorderdelivery';

       DROP TABLE IF EXISTS tmp_fact_sodf_dimpc_1;
	CREATE TABLE tmp_fact_sodf_dimpc_1
	AS
	SELECT pc.ProfitCenterCode,pc.ControllingArea,ifnull(LIKP_WADAT_IST,LIKP_WADAT) LIKP_WADAT_IST,min(pc.ValidTo) as min_ValidTo
	FROM LIKP_LIPS , Dim_ProfitCenter pc
	WHERE pc.ProfitCenterCode = LIPS_PRCTR
 	AND pc.ControllingArea = LIPS_KOKRS
       AND pc.ValidTo >= ifnull(LIKP_WADAT_IST,LIKP_WADAT)
	AND pc.RowIsCurrent = 1
	GROUP BY pc.ProfitCenterCode,pc.ControllingArea,ifnull(LIKP_WADAT_IST,LIKP_WADAT);

	DROP TABLE IF EXISTS tmp_fact_sodf_dimpc_2;
	CREATE TABLE tmp_fact_sodf_dimpc_2
	AS
	SELECT t.*,pc.Dim_ProfitCenterid
	from Dim_ProfitCenter pc, tmp_fact_sodf_dimpc_1 t
	WHERE pc.ProfitCenterCode = t.ProfitCenterCode 
	AND pc.ControllingArea = t.ControllingArea 
	AND pc.ValidTo = t.min_ValidTo;

	DROP TABLE IF EXISTS tmp_fact_sodf_minsched;
	CREATE TABLE tmp_fact_sodf_minsched
	AS
	select f1.dd_SalesDocNo,f1.dd_SalesItemNo,min(f1.dd_ScheduleNo) min_dd_ScheduleNo
	from fact_salesorder f1
	GROUP BY f1.dd_SalesDocNo,f1.dd_SalesItemNo;
	

	DROP TABLE IF EXISTS tmp_fact_sodf_LIKP_LIPS;
	CREATE TABLE tmp_fact_sodf_LIKP_LIPS
	AS
	SELECT l.*,f.min_dd_ScheduleNo 
	from LIKP_LIPS l inner join tmp_fact_sodf_minsched f on f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS
	where not exists (select 1 from fact_salesorder f1
                        where f1.dd_SalesDocNo = LIPS_VGBEL and f1.dd_SalesItemNo = LIPS_VGPOS and f1.dd_ItemRelForDelv = 'X')
		or not exists (select 1 from loop_tbl_722 lt where LIKP_VBELN = lt.v_DlvrDocNo and LIPS_POSNR = lt.v_DlvrItemNo);


      INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,fact_salesorderdeliveryid)
      SELECT ll.LIPS_VGBEL dd_SalesDocNo,
              ll.LIPS_VGPOS dd_SalesItemNo,
              0 dd_ScheduleNo,
              ll.LIKP_VBELN dd_SalesDlvrDocNo,
              ll.LIPS_POSNR dd_SalesDlvrItemNo,
              ifnull(ll.lips_bwart ,'Not Set') dd_MovementType,
              f.dd_ReferenceDocumentNo,
              ll.LIPS_LFIMG ct_QtyDelivered,
              ll.LIPS_WAVWR amt_Cost_DocCurr,
              (ll.LIPS_WAVWR * (case when f.amt_ExchangeRate < 0 then (1/(-1 * f.amt_ExchangeRate)) else f.amt_ExchangeRate end)) amt_Cost,
              ifnull(f.amt_SubTotal3,0.0000),
              ifnull(f.amt_SubTotal4,0.0000),
              ll.lips_vbeaf ct_FixedProcessDays,
              ll.lips_vbeav ct_ShipProcessDays,
              ll.LIPS_LFIMG ct_ScheduleQtySalesUnit,
              ll.LIPS_LFIMG ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = ll.likp_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPlannedGoodsIssue,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = ll.likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGoodsIssue,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = ll.likp_lfdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDeliveryDate,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = ll.likp_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoadingDate,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = ll.likp_kodat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPickingDate,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = ll.likp_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDlvrDocCreated,
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = ll.lips_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMatlAvail,
              ifnull((SELECT Dim_CustomerID
                      FROM Dim_Customer cust
                      WHERE cust.CustomerNumber = ll.likp_kunag),f.Dim_CustomerID) Dim_CustomeridSoldTo,
              ifnull((SELECT Dim_CustomerID
                      FROM Dim_Customer cust
                      WHERE cust.CustomerNumber = ll.likp_kunnr),1) Dim_CustomeridShipTo,
              ifnull((SELECT dim_partid
                      FROM dim_part dp
                      WHERE dp.PartNumber = ll.lips_matnr AND dp.Plant = ll.lips_werks),1) Dim_Partid,
              pl.Dim_Plantid,
              ifnull((SELECT Dim_StorageLocationid
                      FROM Dim_StorageLocation sl
                      WHERE sl.LocationCode = ll.lips_lgort and sl.plant = ll.lips_werks),1) Dim_StorageLocationid,
              ifnull((SELECT Dim_ProductHierarchyid
                      FROM Dim_ProductHierarchy ph
                      WHERE ph.ProductHierarchy = ll.lips_prodh),1) Dim_ProductHierarchyid,
              ifnull((select Dim_SalesOrderHeaderStatusid
                      from Dim_SalesOrderHeaderStatus sohs
                      where sohs.SalesDocumentNumber = ll.LIKP_VBELN),1) Dim_DeliveryHeaderStatusid,
              ifnull((select Dim_SalesOrderItemStatusid
                      from Dim_SalesOrderItemStatus sois
                      where sois.SalesDocumentNumber = ll.LIKP_VBELN and sois.SalesItemNumber = ll.LIPS_POSNR),1) Dim_DeliveryItemStatusid,
              f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId,
           ifnull((select pc.Dim_ProfitCenterid
                      from tmp_fact_sodf_dimpc_2 pc
                      where  pc.ProfitCenterCode = LIPS_PRCTR
                        AND pc.ControllingArea = LIPS_KOKRS
                        AND pc.LIKP_WADAT_IST = ifnull(ll.LIKP_WADAT_IST,ll.LIKP_WADAT)),1) Dim_ProfitCenterid,
              ifnull((select ca.Dim_ControllingAreaid
                      from Dim_ControllingArea ca
                      where  ca.ControllingAreaCode = ll.LIPS_KOKRS),1) Dim_ControllingAreaId,
              ifnull((SELECT dt.Dim_DateID
                      FROM dim_Date dt
                      WHERE dt.DateValue = ll.LIKP_FKDAT AND dt.CompanyCode = pl.CompanyCode ), 1),
              ifnull((SELECT dim_billingdocumenttypeid
                      FROM dim_billingdocumenttype bdt
                      WHERE bdt.Type = ll.LIKP_FKARV AND bdt.RowIsCurrent= 1), 1),
              ifnull((SELECT Dim_DistributionChannelId
                      FROM dim_distributionchannel dc
                      WHERE dc.DistributionChannelCode = ll.LIKP_VTWIV AND dc.RowIsCurrent = 1),f.Dim_DistributionChannelId),
              ifnull((SELECT Dim_Dateid
                      FROM Dim_Date dd
                      WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGI_Original,
		(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over ()
      FROM tmp_fact_sodf_LIKP_LIPS ll
          inner join fact_salesorder f on f.dd_SalesDocNo = ll.LIPS_VGBEL and f.dd_SalesItemNo = ll.LIPS_VGPOS
                                          and f.dd_ScheduleNo = ll.min_dd_ScheduleNo
          inner join Dim_Plant pl on pl.plantcode = ll.LIPS_WERKS;

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery sod
FROM VBFA v
  SET sod.ct_QtyDelivered = case when (sod.ct_QtyDelivered - v.vbfa_rfmng) < 0 then 0 else (sod.ct_QtyDelivered - v.vbfa_rfmng) end
  WHERE sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
      and v.vbfa_bwart = '602' and sod.ct_QtyDelivered > 0
      and not exists (select 1 from VBFA v1
                      where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                            and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat);

call vectorwise(combine 'fact_salesorderdelivery');

/* start CALL bi_populate_so_shipment() */

drop table if exists update_so_shipment_001;
create table update_so_shipment_001 as
select f.dd_SalesDocNo v_dd_SalesDocNo, f.dd_SalesItemNo v_dd_SalesItemNo, f.dd_ScheduleNo v_dd_ScheduleNo,
	ifnull(sum(f.ct_QtyDelivered), 0) v_ct_DeliveredQty,
	ifnull(max(f.Dim_DateidDeliveryDate), 1) v_Dim_DateidShipmentDelivery,
	ifnull(max(f.Dim_DateidActualGoodsIssue), 1) v_Dim_DateidActualGI,
	min(Dim_CustomeridShipto) v_Dim_CustomeridShipTo,
	ifnull(min(f.Dim_DateidDlvrDocCreated), 1) v_Dim_DateidDlvrDocCreated
from fact_salesorderdelivery f
	inner join dim_salesorderitemstatus sois on f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
	inner join LIKP_LIPS on LIKP_VBELN = f.dd_SalesDlvrDocNo and LIPS_POSNR = f.dd_SalesDlvrItemNo
where sois.GoodsMovementStatus <> 'Not yet processed'
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo;

UPDATE fact_salesorder so
  FROM update_so_shipment_001 u
   SET ct_DeliveredQty = v_ct_DeliveredQty
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

UPDATE fact_salesorder so
  FROM update_so_shipment_001 u
   SET Dim_DateidShipmentDelivery = v_Dim_DateidShipmentDelivery
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

UPDATE fact_salesorder so
  FROM update_so_shipment_001 u
   SET Dim_DateidActualGI = v_Dim_DateidActualGI
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

UPDATE fact_salesorder so
  FROM update_so_shipment_001 u
   SET Dim_CustomeridShipTo = ifnull(v_Dim_CustomeridShipTo, Dim_CustomeridShipTo)
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

UPDATE fact_salesorder so
  FROM update_so_shipment_001 u
   SET Dim_DateidDlvrDocCreated = v_Dim_DateidDlvrDocCreated
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;


  UPDATE fact_salesorder so
  fROM dim_salesorderitemstatus s
  SET ct_DeliveredQty = so.ct_ConfirmedQty
  WHERE so.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      AND so.dd_ItemRelForDelv = 'X'
      AND s.OverallDeliveryStatus = 'Completely processed'
      AND so.ct_DeliveredQty < so.ct_ConfirmedQty
      AND not exists (select 1 from fact_salesorderdelivery sod inner join VBFA v on sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
                      where v.vbfa_bwart = '602' and sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo
                      and not exists (select 1 from VBFA v1 where v1 .vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                                      and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat));

/* end update so_shipment */

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_salesorderdeliveryid),0) from fact_salesorderdelivery)
where table_name = 'fact_salesorderdelivery';

  INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,fact_salesorderdeliveryid)
      SELECT f.dd_SalesDocNo,
              f.dd_SalesItemNo,
              f.dd_ScheduleNo,
              'Not Set' dd_SalesDlvrDocNo,
              0 dd_SalesDlvrItemNo,
              'Not Set' dd_MovementType,
              f.dd_ReferenceDocumentNo,
              0 ct_QtyDelivered,
              0 amt_Cost_DocCurr,
              0 amt_Cost,
              ifnull(f.amt_SubTotal3,0.0000) amt_SalesSubTotal3,
              ifnull(f.amt_SubTotal4,0.0000) amt_SalesSubTotal4,
              0 ct_FixedProcessDays,
              0 ct_ShipProcessDays,
              f.ct_ScheduleQtySalesUnit,
              f.ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL,
              f.Dim_DateidGoodsIssue,
              1 Dim_DateidActualGoodsIssue,
              f.Dim_DateidSchedDelivery,
              f.Dim_DateidLoading,
              1 Dim_DateidPickingDate,
              1 Dim_DateidDlvrDocCreated,
              f.Dim_DateidMtrlAvail,
              f.Dim_CustomerID Dim_CustomeridSoldTo,
              f.Dim_CustomerID Dim_CustomeridShipTo,
              f.Dim_Partid,
              f.Dim_Plantid,
              f.Dim_StorageLocationid,
              f.Dim_ProductHierarchyid,
              1 Dim_DeliveryHeaderStatusid,
              1 Dim_DeliveryItemStatusid,
              f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId,
              1 Dim_ProfitCenterid,
              1 Dim_ControllingAreaId,
              1 Dim_DateIdBillingDate,
              1 Dim_BillingDocumentTypeId,
              f.Dim_DistributionChannelId Dim_DistributionChannelId,
	(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over ()
      FROM fact_salesorder f inner join dim_salesorderitemstatus s
                                    ON f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      where f.dd_ItemRelForDelv = 'X' and s.OverallDeliveryStatus <> 'Completely processed'
            and (ct_ConfirmedQty - ct_DeliveredQty) > 0
            and not exists (select 1 from fact_salesorderdelivery f1
                            where f1.dd_SalesDocNo = f.dd_SalesDocNo and f1.dd_SalesItemNo = f.dd_SalesItemNo and f1.dd_ScheduleNo = f.dd_ScheduleNo);

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_salesorderdelivery sod
  FROM LIKP_LIPS, Dim_Deliverytype dt
SET sod.Dim_deliveryTypeId = dt.Dim_deliveryTypeId
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
  AND LIKP_LFART IS NOT NULL
  AND dt.DeliveryType = LIKP_LFART
  AND dt.RowIsCurrent = 1;

UPDATE facT_salesorderdelivery
SET Dim_deliveryTypeId = 1
WHERE Dim_deliveryTypeId IS NULL;

call vectorwise(combine 'fact_salesorderdelivery');


UPDATE       fact_salesorderdelivery sod
FROM 		likp_lips_vbuk v, dim_overallstatusforcreditcheck oscc
SET sod.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckID
WHERE		 sod.dd_SalesDlvrDocNo = v.VBUK_VBELN
AND		 oscc.overallstatusforcreditcheck = ifnull(v.VBUK_CMGST, 'Not Set')
AND 		oscc.RowIsCurrent = 1;
   
UPDATE fact_salesorderdelivery sod
FROM fact_billing fb
    SET sod.dd_billing_no = fb.dd_billing_no
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo;

UPDATE fact_salesorderdelivery sod
FROM fact_salesorder so
	SET sod.Dim_SalesDistrictId = so.Dim_SalesDistrictId
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo and sod.dd_ScheduleNo = so.dd_ScheduleNo;

call vectorwise(combine 'fact_salesorderdelivery');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorderdelivery;

Drop table if exists flag_holder_722;
Drop table if exists cursor_table1_722;
Drop table if exists update_tbl_722;
Drop table if exists loop_tbl_722;
Drop table if exists update_so_shipment_001;
Drop table if exists tmp_fact_sodf_LIKP_LIPS;