  /* Start of part 2. This should run after the fiscal year scripts */

UPDATE tmp_inv_t_cursor t
FROM tmp_Funct_Fiscal_Year z
SET pPeriodDates = z.pReturn
WHERE z.pCompanyCode = t.pCompCode
and z.FiscalYear = t.pFiYear
and z.Period = t.pPeriodFrom
and z.fact_script_name = 'bi_inventoryturn_processing';


UPDATE tmp_inv_t_cursor
SET pPeriodFromDate = cast(substr(pPeriodDates, 1, 10) AS date);

UPDATE tmp_inv_t_cursor
SET pPeriodToDate = cast(substr(pPeriodDates, 12, 10) AS date);

UPDATE tmp_inv_t_cursor
SET pPeriodMidDate = pPeriodToDate - int(floor(((pPeriodToDate - pPeriodFromDate))/2));


  UPDATE tmp_inv_t_cursor
  set pFromYear = Year(pPeriodMidDate);

  UPDATE tmp_inv_t_cursor
  set pFromMonth = Month(pPeriodMidDate);

  UPDATE tmp_inv_t_cursor
  set pToYear = Year(pPeriodMidDate);

  UPDATE tmp_inv_t_cursor
  set pToMonth = Month(pPeriodMidDate);


  DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold;
  CREATE TABLE tmp_ivturn_CostOfGoodsSold
  AS
  SELECT MATNR,WERKS,SPMON_YEAR,SPMON_MONTH,sum(WGVBR) sum_WGVBR
  FROM S031
  GROUP BY MATNR,WERKS,SPMON_YEAR,SPMON_MONTH;

  UPDATE tmp_inv_t_cursor
  SET CostOfGoodsSold = 0;

    UPDATE tmp_inv_t_cursor
        FROM tmp_ivturn_CostOfGoodsSold a
        set CostOfGoodsSold = sum_WGVBR
        where a.MATNR = pMaterialNo and a.WERKS = pPlant
        and      a.SPMON_YEAR = pFromYear and a.SPMON_MONTH = pFromMonth;

/* Call exchange rate std function after the following combine */
CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');

