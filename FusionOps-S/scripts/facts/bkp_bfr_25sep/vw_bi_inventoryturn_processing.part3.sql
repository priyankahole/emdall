
    /* Part 3 */

    DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold_GBL;
        CREATE TABLE tmp_ivturn_CostOfGoodsSold_GBL
        AS
        select a.MATNR ,WERKS, SPMON_YEAR,SPMON_MONTH,
        a.WGVBR * ifnull((select z.exchangeRate from tmp_getExchangeRate1 z
                                where z.pFromCurrency =  pCurrency AND z.pToCurrency = pGlobalCurrency
                                AND z.pDate = S031_SPTAG AND z.fact_script_name = 'bi_inventoryturn_processing'),1) WGVBR_gbl
        FROM S031 a, tmp_inv_t_cursor
        WHERE a.MATNR = pMaterialNo and a.WERKS = pPlant
        and  a.SPMON_YEAR = pFromYear and a.SPMON_MONTH = pFromMonth;

        DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold_GBL_2;
        CREATE TABLE tmp_ivturn_CostOfGoodsSold_GBL_2
        AS
        SELECT a.MATNR ,WERKS, SPMON_YEAR,SPMON_MONTH,sum(WGVBR_gbl) sum_WGVBR
        from tmp_ivturn_CostOfGoodsSold_GBL a
        group by a.MATNR ,WERKS, SPMON_YEAR,SPMON_MONTH;

        UPDATE tmp_inv_t_cursor
        set CostOfGoodsSold_GBL = 0;

        UPDATE tmp_inv_t_cursor
        FROM tmp_ivturn_CostOfGoodsSold_GBL_2 a
        SET CostOfGoodsSold_GBL = ifnull(sum_WGVBR,0)
        WHERE a.MATNR = pMaterialNo and a.WERKS = pPlant
        and A.SPMON_YEAR = pFromYear and a.SPMON_MONTH = pFromMonth;


        Update fact_inventory
        FROM tmp_inv_t_cursor
        set
        amt_OpenStockValue = OpeningStock
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_OpenStockValue <> OpeningStock ;

        Update fact_inventory
        FROM tmp_inv_t_cursor
        set
        amt_CloseStockValue = ClosingStock
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_CloseStockValue <> ClosingStock ;

        Update fact_inventory
        FROM tmp_inv_t_cursor
        set
        amt_AvgInventoryValue = AverageInv
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_AvgInventoryValue <> AverageInv;

        Update fact_inventory
        FROM tmp_inv_t_cursor
        set
        amt_COGS = CostOfGoodsSold
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_COGS <> CostOfGoodsSold;

        Update fact_inventory
        FROM tmp_inv_t_cursor
        set
        amt_COGS_GBL = CostOfGoodsSold_GBL
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_COGS_GBL <> CostOfGoodsSold_GBL;

        Update fact_inventory
        FROM tmp_inv_t_cursor
        set
        ct_InventoryTurn = case ifnull(CostOfGoodsSold,0) when 0 then 0 else case ifnull(AverageInv,0) when 0 then 0 else (CostOfGoodsSold/AverageInv) end end
        where Fact_Inventoryid = pFact_Inventoryid;

  DELETE FROM fact_inventory_tmp;

  /* Drop all temporary tables created */

  DROP TABLE IF EXISTS tmp_ivturn_globalvars;
  DROP TABLE IF EXISTS tmp_inv_t_cursor;
        DROP TABLE IF EXISTS tmp_ivt_opcount0;
        DROP TABLE IF EXISTS tmp_ivt_opcount1;
        DROP TABLE IF EXISTS tmp_inventoryturn_mbewh_t1;
        DROP TABLE IF EXISTS tmp_inventoryturn_mbewh_t2;
        DROP TABLE IF EXISTS tmp_inventoryturn_mbew_t1;
        DROP TABLE IF EXISTS tmp_inventoryturn_mbew_t2;
        DROP TABLE IF EXISTS tmp_inv_t_cursor_innerwhile_OpCount_zero;
        DROP TABLE IF EXISTS min_pPer4_tmp;
 DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold;
    DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold_GBL;
        DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold_GBL_2;

