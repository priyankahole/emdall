UPDATE fact_planorder po
   SET po.Dim_ActionStateid = 3, po.ct_Completed = 1
 WHERE NOT EXISTS
          (SELECT 1
             FROM PLAF p
            WHERE p.PLAF_PLNUM = po.dd_PlanOrderNo);

DROP TABLE IF EXISTS tmp_fpo_mbew_no_bwtar;
CREATE TABLE tmp_mbew_no_bwtar AS SELECT ifnull((CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0) calcPrice,
                        sp.BWKEY,
                        sp.MATNR
                FROM mbew_no_bwtar sp
               WHERE  ((sp.LFGJA * 100) + sp.LFMON) =
                            (SELECT max((x.LFGJA * 100) + x.LFMON)
                               FROM mbew_no_bwtar x
                              WHERE x.MATNR = sp.MATNR AND x.BWKEY = sp.BWKEY
                                    AND ((x.VPRSV = 'S' AND x.STPRS > 0)
                                         OR (x.VPRSV = 'V' AND x.VERPR > 0)))
                     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0)
                          OR (sp.VPRSV = 'V' AND sp.VERPR > 0));
UPDATE fact_planorder po
   FROM plaf p,
       dim_plant pl,
       dim_company dc,
       dim_currency c,
       dim_part dp,
       dim_unitofmeasure uom
   SET po.Dim_Companyid = dc.Dim_Companyid,
       po.Dim_Partid = dp.Dim_Partid,
       po.Dim_mpnid =
          ifnull(
             (SELECT dim_partid
                FROM dim_part mpn
               WHERE     mpn.PartNumber = p.PLAF_EMATN
                     AND mpn.Plant = p.PLAF_PLWRK
                     AND mpn.RowIsCurrent = 1),
             1),
       po.Dim_StorageLocationid =
          ifnull(
             (SELECT Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE     sl.LocationCode = p.plaf_lgort
                     AND sl.Plant = p.PLAF_PLWRK
                     AND sl.RowIsCurrent = 1),
             1),
       po.Dim_Plantid = pl.Dim_Plantid,
       po.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid,
       po.Dim_PurchaseOrgid = (select pog.Dim_PurchaseOrgid from dim_purchaseorg pog                                 
        			where pog.PurchaseOrgCode = pl.PurchOrg),
       po.Dim_Vendorid =
          ifnull(
             (SELECT Dim_Vendorid
                FROM dim_vendor dv
               WHERE dv.VendorNumber = p.PLAF_EMLIF),
             1),
       po.Dim_FixedVendorid =
          ifnull(
             (SELECT Dim_Vendorid
                FROM dim_vendor fv
               WHERE fv.VendorNumber = p.PLAF_FLIEF),
             1),
       po.Dim_SpecialProcurementid =
          ifnull(
             (SELECT Dim_SpecialProcurementid
                FROM dim_specialprocurement sp
               WHERE sp.specialprocurement = p.PLAF_SOBES),
             1),
       po.Dim_ConsumptionTypeid =
          ifnull(
             (SELECT Dim_ConsumptionTypeid
                FROM dim_consumptiontype dcp
               WHERE dcp.ConsumptionCode = p.PLAF_KZVBR),
             1),
       po.Dim_AccountCategoryid =
          ifnull((SELECT Dim_AccountCategoryid
                    FROM dim_accountcategory ac
                   WHERE ac.Category = p.plaf_knttp),
                 1),
       po.Dim_SpecialStockid =
          ifnull(
             (SELECT Dim_SpecialStockid
                FROM dim_specialstock st
               WHERE st.specialstockindicator = p.PLAF_SOBKZ),
             1),
       po.dim_bomstatusid =
          ifnull(
             (SELECT dim_bomstatusid
                FROM dim_bomstatus bs
               WHERE bs.BOMStatusCode = p.plaf_ststa),
             1),
       po.dim_bomusageid =
          ifnull(
             (SELECT dim_bomusageid
                FROM dim_bomusage bu
               WHERE bu.BOMUsageCode = p.plaf_stlan),
             1),
       po.dim_objecttypeid =
          ifnull(
             (SELECT dim_objecttypeid
                FROM dim_objecttype ot
               WHERE ot.ObjectType = p.PLAF_OBART),
             1),
       po.dim_productionschedulerid =
          ifnull(
             (SELECT dim_productionschedulerid
                FROM dim_productionscheduler ps
               WHERE     ps.ProductionScheduler = p.PLAF_PLGRP
                     AND ps.Plant = p.PLAF_PLWRK),
             1),
       po.dim_schedulingerrorid =
          ifnull(
             (SELECT dim_schedulingerrorid
                FROM dim_schedulingerror se
               WHERE se.SchedulingErrorCode = p.PLAF_TRMER),
             1),
       po.dim_tasklisttypeid =
          ifnull(
             (SELECT dim_tasklisttypeid
                FROM dim_tasklisttype tst
               WHERE tst.TaskListTypeCode = p.PLAF_PLNTY),
             1),
       po.dim_ordertypeid =
          ifnull(
             (SELECT dim_ordertypeid
                FROM dim_ordertype ordt
               WHERE ordt.OrderTypeCode = p.PLAF_PAART),
             1),
       Dim_dateidStart = ifnull((SELECT ds.dim_dateid
                 		FROM dim_date ds
                		WHERE ds.DateValue = p.PLAF_PSTTR
                      			AND pl.CompanyCode = ds.CompanyCode), 1),
       Dim_dateidFinish = ifnull((SELECT df.dim_dateid
                    FROM dim_date df
                   WHERE df.DateValue = p.PLAF_PEDTR
                         AND pl.CompanyCode = df.CompanyCode
						 AND p.PLAF_PEDTR IS NOT NULL), 1),
       Dim_dateidOpening = ifnull((SELECT dop.dim_dateid
                    FROM dim_date dop
                   WHERE dop.DateValue = p.PLAF_PERTR
                         AND pl.CompanyCode = dop.CompanyCode
						 AND p.PLAF_PERTR IS NOT NULL), 1),
       ct_QtyTotal = p.PLAF_GSMNG,
       amt_ExtendedPrice =
          (ifnull((SELECT calcPrice 
				FROM tmp_fpo_mbew_no_bwtar sp
               WHERE sp.MATNR = dp.PartNumber AND sp.BWKEY = pl.ValuationArea),
             0)
           * p.PLAF_GSMNG),
       dd_bomexplosionno = p.PLAF_SERNR,
       ct_QtyReduced = p.plaf_ABMNG,
       Dim_DateidProductionStart =
          ifnull((SELECT df.dim_dateid
                       FROM dim_date df
                      WHERE df.DateValue = p.PLAF_TERST
                            AND pl.CompanyCode = df.CompanyCode),1),
       Dim_DateidProductionFinish =
          ifnull((SELECT df.dim_dateid
                       FROM dim_date df
                      WHERE df.DateValue = p.PLAF_TERED
                            AND pl.CompanyCode = df.CompanyCode),1),
       Dim_DateidExplosion =
          ifnull((SELECT df.dim_dateid
                       FROM dim_date df
                      WHERE df.DateValue = p.PLAF_PALTR
                            AND pl.CompanyCode = df.CompanyCode),1),
       Dim_DateidAction =
          ifnull((SELECT df.dim_dateid
                       FROM dim_date df
                      WHERE df.DateValue = p.PLAF_MDACD
                            AND pl.CompanyCode = df.CompanyCode),1),
       dim_scheduletypeid =
          ifnull((SELECT st.dim_scheduletypeid
                FROM dim_scheduletype st
               WHERE st.ScheduleTypeCode = p.PLAF_LVSCH),1),
       dim_availabilityconfirmationid =
          ifnull((SELECT acf.dim_availabilityconfirmationid
                FROM dim_availabilityconfirmation acf
               WHERE acf.AvailabilityConfirmationCode = p.PLAF_MDPBV),1),
       po.Dim_ProcurementId =
          ifnull((SELECT pc.dim_procurementid
                FROM Dim_Procurement pc
               WHERE pc.procurement = p.PLAF_BESKZ),1),
       po.Dim_Currencyid = c.Dim_Currencyid,
       dd_SalesOrderNo = p.PLAF_KDAUF,
       dd_SalesOrderItemNo = p.PLAF_KDPOS,
       dd_SalesOrderScheduleNo = p.PLAF_KDEIN,
       ct_GRProcessingTime = p.PLAF_WEBAZ,
       ct_QtyIssued = p.PLAF_WAMNG,
       ct_QtyCommitted = p.PLAF_VFMNG,
       dd_SequenceNo = ifnull(p.PLAF_SEQNR, 'Not Set'),
       dd_PlannScenario = ifnull(p.PLAF_PLSCN, 'Not Set'),
       amt_StdUnitPrice =
	             ifnull((SELECT calcPrice 
				FROM tmp_fpo_mbew_no_bwtar sp
               WHERE sp.MATNR = dp.PartNumber AND sp.BWKEY = pl.ValuationArea),
             0),
       Dim_MRPControllerId =
          ifnull(
             (SELECT mc.Dim_MRPControllerId
                FROM Dim_MRPController mc
               WHERE     mc.MRPController = PLAF_DISPO
                     AND mc.Plant = PLAF_PLWRK),
             1),
       dd_AgreementNo = ifnull(PLAF_KONNR,'Not Set'),
       dd_AgreementItemNo = ifnull(PLAF_KTPNR,0)
 WHERE     po.dd_PlanOrderNo = p.plaf_plnum
       AND po.Dim_ActionStateid = 2
       AND pl.PlantCode = p.PLAF_PLWRK
       AND dc.CompanyCode = pl.CompanyCode
       AND dp.PartNumber = p.PLAF_MATNR
       AND dp.Plant = p.PLAF_PLWRK
       AND uom.UOM = p.plaf_meins
       AND dc.Currency = c.CurrencyCode;
DROP TABLE tmp_fpo_mbew_no_bwtar;

INSERT INTO fact_planorder(Dim_ActionStateid,
                           Dim_Companyid,
                           Dim_Partid,
                           Dim_mpnid,
                           Dim_StorageLocationid,
                           Dim_Plantid,
                           Dim_UnitOfMeasureid,
                           Dim_PurchaseOrgid,
                           Dim_Vendorid,
                           Dim_FixedVendorid,
                           Dim_SpecialProcurementid,
                           Dim_ConsumptionTypeid,
                           Dim_AccountCategoryid,
                           Dim_SpecialStockid,
                           dim_bomstatusid,
                           dim_bomusageid,
                           dim_objecttypeid,
                           dim_productionschedulerid,
                           dim_schedulingerrorid,
                           dim_tasklisttypeid,
                           dim_ordertypeid,
                           Dim_dateidStart,
                           Dim_dateidFinish,
                           Dim_dateidOpening,
                           ct_QtyTotal,
                           ct_Completed,
                           amt_ExtendedPrice,
                           dd_PlanOrderNo,
                           dd_bomexplosionno,
                           ct_QtyReduced,
                           Dim_PlanOrderStatusid,
                           Dim_DateidConversionDate,
                           Dim_DateidProductionStart,
                           Dim_DateidProductionFinish,
                           Dim_DateidExplosion,
                           Dim_DateidAction,
                           dim_scheduletypeid,
                           dim_availabilityconfirmationid,
                           dim_currencyid,
                           Dim_ProcurementId,
                           dd_SalesOrderNo,
                           dd_SalesOrderItemNo,
                           dd_SalesOrderScheduleNo,
                           ct_GRProcessingTime,
                           ct_QtyIssued,
                           ct_QtyCommitted,
                           dd_SequenceNo,
                           dd_PlannScenario,
                           amt_StdUnitPrice,
                           Dim_MRPControllerId,
                           dd_AgreementNo,
                           dd_AgreementItemNo)
   SELECT 2 Dim_ActionStateid,
          Dim_Companyid,
          dp.Dim_Partid,
          ifnull(
             (SELECT dim_partid
                FROM dim_part mpn
               WHERE     mpn.PartNumber = p.PLAF_EMATN
                     AND mpn.Plant = p.PLAF_PLWRK),
             1)
             Dim_mpnid,
          ifnull(
             (SELECT Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE     sl.LocationCode = p.plaf_lgort
                     AND sl.Plant = p.PLAF_PLWRK),
             1)
             Dim_StorageLocationid,
          Dim_Plantid,
          Dim_UnitOfMeasureid,
          Dim_PurchaseOrgid,
          ifnull(
             (SELECT Dim_Vendorid
                FROM dim_vendor dv
               WHERE dv.VendorNumber = p.PLAF_EMLIF),
             1)
             Dim_Vendorid,
          ifnull(
             (SELECT Dim_Vendorid
                FROM dim_vendor fv
               WHERE fv.VendorNumber = p.PLAF_FLIEF),
             1)
             Dim_FixedVendorid,
          ifnull(
             (SELECT Dim_SpecialProcurementid
                FROM dim_specialprocurement sp
               WHERE sp.specialprocurement = p.PLAF_SOBES),
             1)
             Dim_SpecialProcurementid,
          ifnull(
             (SELECT Dim_ConsumptionTypeid
                FROM dim_consumptiontype dcp
               WHERE dcp.ConsumptionCode = p.PLAF_KZVBR),
             1)
             Dim_ConsumptionTypeid,
          ifnull((SELECT Dim_AccountCategoryid
                    FROM dim_accountcategory ac
                   WHERE ac.Category = p.plaf_knttp),
                 1)
             Dim_AccountCategoryid,
          ifnull(
             (SELECT Dim_SpecialStockid
                FROM dim_specialstock st
               WHERE st.specialstockindicator = p.PLAF_SOBKZ),
             1)
             Dim_SpecialStockid,
          ifnull(
             (SELECT dim_bomstatusid
                FROM dim_bomstatus bs
               WHERE bs.BOMStatusCode = p.plaf_ststa),
             1)
             dim_bomstatusid,
          ifnull(
             (SELECT dim_bomusageid
                FROM dim_bomusage bu
               WHERE bu.BOMUsageCode = p.plaf_stlan),
             1)
             dim_bomusageid,
          ifnull(
             (SELECT dim_objecttypeid
                FROM dim_objecttype ot
               WHERE ot.ObjectType = p.PLAF_OBART),
             1)
             dim_objecttypeid,
          ifnull(
             (SELECT dim_productionschedulerid
                FROM dim_productionscheduler ps
               WHERE     ps.ProductionScheduler = p.PLAF_PLGRP
                     AND ps.Plant = p.PLAF_PLWRK),
             1)
             dim_productionschedulerid,
          ifnull(
             (SELECT dim_schedulingerrorid
                FROM dim_schedulingerror se
               WHERE se.SchedulingErrorCode = p.PLAF_TRMER),
             1)
             dim_schedulingerrorid,
          ifnull(
             (SELECT dim_tasklisttypeid
                FROM dim_tasklisttype tst
               WHERE tst.TaskListTypeCode = p.PLAF_PLNTY),
             1)
             dim_tasklisttypeid,
          ifnull(
             (SELECT dim_ordertypeid
                FROM dim_ordertype ordt
               WHERE ordt.OrderTypeCode = p.PLAF_PAART),
             1)
             dim_ordertypeid,
          ds.dim_dateid Dim_dateidStart,
          CASE
             WHEN p.PLAF_PEDTR IS NULL
             THEN
                1
             ELSE
                ifnull(
                   (SELECT df.dim_dateid
                      FROM dim_date df
                     WHERE df.DateValue = p.PLAF_PEDTR
                           AND pl.CompanyCode = df.CompanyCode),
                   1)
          END
             Dim_dateidFinish,
          CASE
             WHEN p.PLAF_PERTR IS NULL
             THEN
                1
             ELSE
                ifnull(
                   (SELECT dop.dim_dateid
                      FROM dim_date dop
                     WHERE dop.DateValue = p.PLAF_PERTR
                           AND pl.CompanyCode = dop.CompanyCode),
                   1)
          END
             Dim_dateidOpening,
          p.PLAF_GSMNG ct_QtyTotal,
          0 ct_Completed,
          ifnull(
             getStdPrice(pl.CompanyCode,
                         pl.PlantCode,
                         PLAF_MATNR,
                         ds.CalendarYear,
                         ds.FinancialMonthNumber,
                         CASE WHEN PLAF_UMREZ = 0 THEN 1 ELSE PLAF_UMREZ END,
                         CASE WHEN PLAF_UMREN = 0 THEN 1 ELSE PLAF_UMREN END,
                         NULL,
                         0),
             0)
          * p.PLAF_GSMNG
             amt_ExtendedPrice,
          p.PLAF_PLNUM dd_PlanOrderNo,
          p.PLAF_SERNR dd_bomexplosionno,
          p.plaf_ABMNG ct_QtyReduced,
          1 Dim_PlanOrderStatusid,
          1 Dim_DateidConversionDate,
          (CASE
              WHEN p.PLAF_TERST IS NULL
              THEN
                 1
              ELSE
                 ifnull(
                    (SELECT df.dim_dateid
                       FROM dim_date df
                      WHERE df.DateValue = p.PLAF_TERST
                            AND pl.CompanyCode = df.CompanyCode),
                    1)
           END)
             Dim_DateidProductionStart,
          (CASE
              WHEN p.PLAF_TERED IS NULL
              THEN
                 1
              ELSE
                 ifnull(
                    (SELECT df.dim_dateid
                       FROM dim_date df
                      WHERE df.DateValue = p.PLAF_TERED
                            AND pl.CompanyCode = df.CompanyCode),
                    1)
           END)
             Dim_DateidProductionFinish,
          (CASE
              WHEN p.PLAF_PALTR IS NULL
              THEN
                 1
              ELSE
                 ifnull(
                    (SELECT df.dim_dateid
                       FROM dim_date df
                      WHERE df.DateValue = p.PLAF_PALTR
                            AND pl.CompanyCode = df.CompanyCode),
                    1)
           END)
             Dim_DateidExplosion,
          (CASE
              WHEN p.PLAF_MDACD IS NULL
              THEN
                 1
              ELSE
                 ifnull(
                    (SELECT df.dim_dateid
                       FROM dim_date df
                      WHERE df.DateValue = p.PLAF_MDACD
                            AND pl.CompanyCode = df.CompanyCode),
                    1)
           END)
             Dim_DateidAction,
          ifnull(
             (SELECT st.dim_scheduletypeid
                FROM dim_scheduletype st
               WHERE st.ScheduleTypeCode = p.PLAF_LVSCH),
             1)
             dim_scheduletypeid,
          ifnull(
             (SELECT acf.dim_availabilityconfirmationid
                FROM dim_availabilityconfirmation acf
               WHERE acf.AvailabilityConfirmationCode = p.PLAF_MDPBV),
             1)
             dim_availabilityconfirmationid,
          c.dim_currencyid,
          ifnull(
             (SELECT pc.dim_procurementid
                FROM Dim_Procurement pc
               WHERE pc.procurement = p.PLAF_BESKZ),
             1),
          p.PLAF_KDAUF dd_SalesOrderNo,
          p.PLAF_KDPOS dd_SalesOrderItemNo,
          p.PLAF_KDEIN dd_SalesOrderScheduleNo,
          p.PLAF_WEBAZ ct_GRProcessingTime,
          p.PLAF_WAMNG ct_QtyIssued,
          p.PLAF_VFMNG ct_QtyCommitted,
          ifnull(p.PLAF_SEQNR, 'Not Set'),
          ifnull(p.PLAF_PLSCN, 'Not Set'),
          ifnull(
             (SELECT ifnull(
                        (CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0)
                FROM mbew_no_bwtar sp
               WHERE sp.MATNR = dp.PartNumber AND sp.BWKEY = pl.ValuationArea
                     AND ((sp.LFGJA * 100) + sp.LFMON) =
                            (SELECT max((x.LFGJA * 100) + x.LFMON)
                               FROM mbew_no_bwtar x
                              WHERE x.MATNR = sp.MATNR AND x.BWKEY = sp.BWKEY
                                    AND ((x.VPRSV = 'S' AND x.STPRS > 0)
                                         OR (x.VPRSV = 'V' AND x.VERPR > 0)))
                     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0)
                          OR (sp.VPRSV = 'V' AND sp.VERPR > 0))
               LIMIT 1),
             0)
             amt_StdUnitPrice,
          ifnull(
             (SELECT mc.Dim_MRPControllerId
                FROM Dim_MRPController mc
               WHERE     mc.MRPController = PLAF_DISPO
                     AND mc.Plant = PLAF_PLWRK),
             1)
             Dim_MRPControllerId,
          ifnull(PLAF_KONNR,'Not Set') dd_AgreementNo,
          ifnull(PLAF_KTPNR,0) dd_AgreementItemNo
     FROM plaf p
          INNER JOIN dim_plant pl
             ON pl.PlantCode = p.PLAF_PLWRK
          INNER JOIN dim_company dc
             ON dc.CompanyCode = pl.CompanyCode
          INNER JOIN Dim_Currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date ds
             ON ds.DateValue = p.PLAF_PSTTR
                AND pl.CompanyCode = ds.CompanyCode
          INNER JOIN dim_part dp
             ON     dp.PartNumber = p.PLAF_MATNR
                AND dp.Plant = p.PLAF_PLWRK
          INNER JOIN dim_unitofmeasure uom
             ON uom.UOM = p.plaf_meins
          INNER JOIN dim_purchaseorg pog
             ON pog.PurchaseOrgCode = pl.PurchOrg
    WHERE NOT EXISTS
             (SELECT 1
                FROM fact_planorder po
               WHERE po.dd_PlanOrderNo = p.plaf_plnum);

UPDATE fact_planorder po
	FROM
       dim_planordermisc m,
       plaf p
   SET po.Dim_PlanOrderMiscid = m.Dim_PlanOrderMiscid
 WHERE     po.dd_PlanOrderNo = ifnull(p.PLAF_PLNUM, 'Not Set')
       AND ProductionDateScheduling = ifnull(p.PLAF_PRSCH, 'Not Set')
       AND PlanningWithoutFinalAssembly = ifnull(p.PLAF_VRPLA, 'Not Set')
       AND SubcontractingVendor = ifnull(p.PLAF_LBLKZ, 'Not Set')
       AND ConversionIndicator = ifnull(p.PLAF_UMSKZ, 'Not Set')
       AND FirmingIndicator = ifnull(p.PLAF_AUFFX, 'Not Set')
       AND FixingIndicator = ifnull(p.PLAF_STLFX, 'Not Set')
       AND SchedulingIndicator = ifnull(p.PLAF_TRMKZ, 'Not Set')
       AND AssemblyOrderProcedures = ifnull(p.PLAF_MONKZ, 'Not Set')
       AND LeadingOrder = ifnull(p.PLAF_PRNKZ, 'Not Set');
       
UPDATE    fact_planorder po
       FROM
          fact_productionorder pod
   SET amt_ExtendedPrice_GBL = amt_ExtendedPrice * amt_ExchangeRate_GBL
   WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
   and amt_ExtendedPrice_GBL <> amt_ExtendedPrice * amt_ExchangeRate_GBL;