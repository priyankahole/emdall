UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET
          dd_DocumentNo = m.MSEG_EBELN 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND dd_DocumentNo <>  m.MSEG_EBELN;

UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET  dd_DocumentItemNo = m.MSEG_EBELP 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND	  dd_DocumentItemNo<> m.MSEG_EBELP;

UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET   dd_SalesOrderNo = m.MSEG_KDAUF 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  dd_SalesOrderNo<>  m.MSEG_KDAUF;

UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET    dd_SalesOrderItemNo = m.MSEG_KDPOS 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_SalesOrderItemNo <>  m.MSEG_KDPOS;

UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET    dd_SalesOrderDlvrNo = m.MSEG_KDEIN 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_SalesOrderDlvrNo <>  m.MSEG_KDEIN;
      

UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dd_GLAccountNo = m.MSEG_SAKTO 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_GLAccountNo <> m.MSEG_SAKTO;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dd_debitcreditid = CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dd_productionordernumber = m.MSEG_AUFNR  
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_productionordernumber <>  m.MSEG_AUFNR;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dd_productionorderitemno = m.MSEG_AUFPS 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_productionorderitemno <>  m.MSEG_AUFPS;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dd_GoodsMoveReason = m.MSEG_GRUND 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_GoodsMoveReason <> m.MSEG_GRUND;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dd_BatchNumber = ifnull(m.MSEG_CHARG, 'Not Set')
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_BatchNumber <> ifnull(m.MSEG_CHARG, 'Not Set');

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dd_ValuationType = m.MSEG_BWTAR 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dd_ValuationType <>  m.MSEG_BWTAR;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       amt_LocalCurrAmt = (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_DMBTR 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       amt_StdUnitPrice = m.MSEG_DMBTR / (case when m.MSEG_ERFMG =0 then null else m.MSEG_ERFMG end)
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       amt_DeliveryCost = (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BNBTR 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       amt_AltPriceControl = (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BUALT 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       amt_OnHand_ValStock = m.MSEG_SALK3 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  amt_OnHand_ValStock <>  m.MSEG_SALK3;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       ct_Quantity = (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_MENGE 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       ct_QtyEntryUOM = (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_ERFMG 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       ct_QtyGROrdUnit = (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BSTMG 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       ct_QtyOnHand_ValStock = m.MSEG_LBKUM 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ct_QtyOnHand_ValStock <>  m.MSEG_LBKUM;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       ct_QtyOrdPriceUnit = (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BPMNG 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dim_DateIDMaterialDocDate = ifnull((SELECT mdd.Dim_Dateid
                    FROM dim_date mdd
                   WHERE mdd.CompanyCode = m.MSEG_BUKRS AND m.MKPF_BLDAT = mdd.DateValue),1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dim_DateIDPostingDate = ifnull((SELECT pd.Dim_Dateid 
                    FROM dim_date pd
                   WHERE pd.DateValue = m.MKPF_BUDAT AND pd.CompanyCode = m.MSEG_BUKRS),1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dim_MovementIndicatorid = ifnull((SELECT dmi.dim_MovementIndicatorid
                    FROM dim_movementindicator dmi
                   WHERE dmi.TypeCode = m.MSEG_KZBEW
                          AND dmi.RowIsCurrent = 1),1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dim_specialstockid = ifnull((SELECT spt.dim_specialstockid
                    FROM dim_specialstock spt
                  WHERE spt.specialstockindicator = MSEG_SOBKZ
                        AND spt.RowIsCurrent = 1), 1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dim_unitofmeasureid = ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = MSEG_MEINS AND uom.RowIsCurrent = 1), 1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dim_controllingareaid = ifnull((SELECT ca.dim_controllingareaid
                    FROM dim_controllingarea ca
                   WHERE ca.ControllingAreaCode = MSEG_KOKRS),1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dim_consumptiontypeid = ifnull((SELECT ct.Dim_ConsumptionTypeid
                    FROM dim_consumptiontype ct
                   WHERE ct.ConsumptionCode = MSEG_KZVBR AND ct.RowIsCurrent = 1), 1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dim_stocktypeid = ifnull((SELECT st.Dim_StockTypeid
                    FROM dim_stocktype st
                   WHERE st.TypeCode = MSEG_INSMK AND st.RowIsCurrent = 1), 1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


 UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       Dim_PurchaseGroupid = 1
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG_MATNR IS NULL;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       Dim_PurchaseGroupid = ifnull((SELECT pg.Dim_PurchaseGroupid
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG_MATNR
                                      AND dpr.Plant = m.MSEG_WERKS
                                      AND dpr.RowIsCurrent = 1), 1)
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  m.MSEG_MATNR IS NOT NULL;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       Dim_materialgroupid = ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup AND mg.RowIsCurrent = 1
                        AND dpr.PartNumber = m.MSEG_MATNR
                        AND dpr.Plant = m.MSEG_WERKS AND dpr.RowIsCurrent = 1), 1) 
 WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;


      UPDATE fact_materialmovement ms
From MKPF_MSEG_RKWA m, dim_plant dp
SET       dd_ConsignmentFlag = 1
    WHERE m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND  dd_ConsignmentFlag <> 1;

drop table if exists fact_materialmovement_789;
create table fact_materialmovement_789 as select * from fact_materialmovement where 1=2;

Drop table if exists max_holder_789;

create table max_holder_789(maxid)
as
Select ifnull(max(fact_materialmovementid),0)
from fact_materialmovement;

  INSERT INTO fact_materialmovement_789(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               dd_ConsignmentFlag)
 select max_holder_789.maxid + row_number() over(), bb.* from max_holder_789,
   (SELECT DISTINCT
          m.MSEG_MBLNR dd_MaterialDocNo,
          m.MSEG_ZEILE dd_MaterialDocItemNo,
          m.MSEG_MJAHR dd_MaterialDocYear,
          m.MSEG_EBELN dd_DocumentNo,
          m.MSEG_EBELP dd_DocumentItemNo,
          m.MSEG_KDAUF dd_SalesOrderNo,
          m.MSEG_KDPOS dd_SalesOrderItemNo,
          m.MSEG_KDEIN dd_SalesOrderDlvrNo,
          m.MSEG_SAKTO dd_GLAccountNo,
          CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          m.MSEG_AUFNR dd_productionordernumber, 
          m.MSEG_AUFPS dd_productionorderitemno,
          m.MSEG_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG_CHARG, 'Not Set') dd_BatchNumber,
          m.MSEG_BWTAR dd_ValuationType,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_DMBTR amt_LocalCurrAmt,
          m.MSEG_DMBTR / (case when m.MSEG_ERFMG =0 then null else  m.MSEG_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BUALT amt_AltPriceControl,
          m.MSEG_SALK3 amt_OnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_MENGE ct_Quantity,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BSTMG ct_QtyGROrdUnit,
          m.MSEG_LBKUM ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BPMNG ct_QtyOrdPriceUnit,
          ifnull((SELECT mt.Dim_MovementTypeid
                    FROM dim_movementtype mt
                   WHERE mt.MovementType = m.MSEG_BWART
                        AND mt.ConsumptionIndicator = ifnull(m.MSEG_KZVBR, 'Not Set')
                        AND mt.MovementIndicator = ifnull(m.MSEG_KZBEW, 'Not Set')
                        AND mt.ReceiptIndicator = ifnull(m.MSEG_KZZUG, 'Not Set')
                        AND mt.SpecialStockIndicator = ifnull(m.MSEG_SOBKZ, 'Not Set')
                        AND mt.RowIsCurrent = 1),1) Dim_MovementTypeid,
          ifnull((SELECT dc.dim_Companyid
                    FROM dim_company dc
                   WHERE dc.CompanyCode = m.MSEG_BUKRS AND dc.RowIsCurrent = 1),1) dim_Companyid,
          ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = m.MSEG_WAERS),1) Dim_Currencyid,
          ifnull((select ( case when m.MSEG_MATNR IS NULL then 1 else Dim_Partid end )
                          FROM dim_part dpr
                          WHERE     dpr.PartNumber = m.MSEG_MATNR
                                AND dpr.Plant = m.MSEG_WERKS
                                AND dpr.RowIsCurrent = 1)
                  , 1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
          ifnull((Select (case WHEN m.MSEG_LGORT IS NULL THEN 1 ELSE Dim_StorageLocationid end ) 
                          FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG_LGORT
                                AND dsl.Plant = m.MSEG_WERKS
                                AND dsl.RowIsCurrent = 1)
                  , 1) Dim_StorageLocationid,
          ifnull((SELECT dv.Dim_Vendorid
                    FROM dim_vendor dv
                   WHERE dv.VendorNumber = m.MSEG_LIFNR
                          AND dv.RowIsCurrent = 1),1) Dim_Vendorid,
          ifnull((SELECT mdd.Dim_Dateid
                    FROM dim_date mdd
                   WHERE mdd.CompanyCode = m.MSEG_BUKRS AND m.MKPF_BLDAT = mdd.DateValue),1) dim_MaterialDocDate,
          ifnull((SELECT pd.Dim_Dateid 
                    FROM dim_date pd
                   WHERE pd.DateValue = m.MKPF_BUDAT AND pd.CompanyCode = m.MSEG_BUKRS),1) dim_PostingDate,
          ifnull((Select (case WHEN m.MSEG_MATNR IS NULL THEN 1 ELSE  dim_producthierarchyid end) 
                          FROM dim_producthierarchy dph, dim_part dpr
                          WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                AND dph.RowIsCurrent = 1
                                AND dpr.RowIsCurrent = 1
                                AND dpr.PartNumber = m.MSEG_MATNR
                                AND dpr.Plant = m.MSEG_WERKS)
                  , 1) dim_producthierarchyid,
          ifnull((SELECT dmi.dim_MovementIndicatorid
                    FROM dim_movementindicator dmi
                   WHERE dmi.TypeCode = m.MSEG_KZBEW
                          AND dmi.RowIsCurrent = 1),1) dim_MovementIndicatorid,
          ifnull(( SELECT dcu.dim_Customerid
                     FROM dim_customer dcu
                    WHERE dcu.CustomerNumber = m.MSEG_KUNNR 
                          AND dcu.RowIsCurrent = 1),1) dim_Customerid,
                   1 dim_CostCenterid,
          ifnull((SELECT spt.dim_specialstockid
                    FROM dim_specialstock spt
                  WHERE spt.specialstockindicator = MSEG_SOBKZ
                        AND spt.RowIsCurrent = 1), 1) dim_specialstockid,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = MSEG_MEINS AND uom.RowIsCurrent = 1), 1) dim_unitofmeasureid,
          ifnull((SELECT ca.dim_controllingareaid
                    FROM dim_controllingarea ca
                   WHERE ca.ControllingAreaCode = MSEG_KOKRS),1) dim_controllingareaid,
          ifnull((SELECT ct.Dim_ConsumptionTypeid
                    FROM dim_consumptiontype ct
                   WHERE ct.ConsumptionCode = MSEG_KZVBR AND ct.RowIsCurrent = 1), 1) dim_consumptiontypeid,
          ifnull((SELECT st.Dim_StockTypeid
                    FROM dim_stocktype st
                   WHERE st.TypeCode = MSEG_INSMK AND st.RowIsCurrent = 1), 1) dim_stocktypeid,
	 ifnull((Select (case WHEN m.MSEG_MATNR IS NULL THEN 1 ELSE pg.Dim_PurchaseGroupid end)
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG_MATNR
                                      AND dpr.Plant = m.MSEG_WERKS
                                      AND dpr.RowIsCurrent = 1)
                  , 1) Dim_PurchaseGroupid,
          ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup AND mg.RowIsCurrent = 1
                        AND dpr.PartNumber = m.MSEG_MATNR
                        AND dpr.Plant = m.MSEG_WERKS AND dpr.RowIsCurrent = 1), 1) Dim_materialgroupid,
          1  dd_ConsignmentFlag                        
     FROM MKPF_MSEG_RKWA m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)) bb;
                    
drop table if exists max_holder_789;

drop table if exists dim_costcenter_789;
create table dim_costcenter_789 as select first 0 * from dim_costcenter;


Update fact_materialmovement_789 fmt
from MKPF_MSEG_RKWA m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
Set dim_CostCenterid= ifnull((Select (case WHEN m.MSEG_KOSTL IS NULL THEN 1 else  dim_CostCenterid end )
                          FROM dim_costcenter_789 dcc
                          WHERE     dcc.Code = m.MSEG_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG_KOKRS = dcc.ControllingArea
                                AND dcc.RowIsCurrent = 1
                          ),1)
Where  NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)
AND fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR;

drop table if exists dim_costcenter_789;

  INSERT INTO fact_materialmovement(fact_materialmovementid,
  			       dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               dd_ConsignmentFlag)
    select fact_materialmovementid,
      			       dd_MaterialDocNo,
                                   dd_MaterialDocItemNo,
                                   dd_MaterialDocYear,
                                   dd_DocumentNo,
                                   dd_DocumentItemNo,
                                   dd_SalesOrderNo,
                                   dd_SalesOrderItemNo,
                                   dd_SalesOrderDlvrNo,
                                   dd_GLAccountNo,
                                   dd_debitcreditid,
                                   dd_productionordernumber,
                                   dd_productionorderitemno,
                                   dd_GoodsMoveReason,
                                   dd_BatchNumber,
                                   dd_ValuationType,
                                   amt_LocalCurrAmt,
                                   amt_StdUnitPrice,
                                   amt_DeliveryCost,
                                   amt_AltPriceControl,
                                   amt_OnHand_ValStock,
                                   ct_Quantity,
                                   ct_QtyEntryUOM,
                                   ct_QtyGROrdUnit,
                                   ct_QtyOnHand_ValStock,
                                   ct_QtyOrdPriceUnit,
                                   Dim_MovementTypeid,
                                   dim_Companyid,
                                   Dim_Currencyid,
                                   Dim_Partid,
                                   Dim_Plantid,
                                   Dim_StorageLocationid,
                                   Dim_Vendorid,
                                   dim_DateIDMaterialDocDate,
                                   dim_DateIDPostingDate,
                                   dim_producthierarchyid,
                                   dim_MovementIndicatorid,
                                   dim_Customerid,
                                   dim_CostCenterid,
                                   dim_specialstockid,
                                   dim_unitofmeasureid,
                                   dim_controllingareaid,
                                   dim_consumptiontypeid,
                                   dim_stocktypeid,
                                   Dim_PurchaseGroupid,
                                   dim_materialgroupid,
                               dd_ConsignmentFlag
      from fact_materialmovement_789;
                               

drop table if exists fact_materialmovement_789;

  UPDATE fact_materialmovement ms
	FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.dim_purchasegroupid = po.dim_purchasegroupid 
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  po.dim_purchasegroupid <>1 
	AND  ms.dim_purchasegroupid <> po.dim_purchasegroupid;


   UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET       ms.dim_purchaseorgid = po.dim_purchaseorgid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_purchaseorgid <> po.dim_purchaseorgid;


         UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.dim_itemcategoryid = po.dim_itemcategoryid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.dim_itemcategoryid <> po.dim_itemcategoryid;



         UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.dim_Termid = po.dim_Termid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_Termid <> po.dim_Termid;


         UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.dim_documenttypeid = po.dim_documenttypeid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_documenttypeid <> po.dim_documenttypeid;

         UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.dim_accountcategoryid = po.dim_accountcategoryid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_accountcategoryid <>  po.dim_accountcategoryid;

         UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.dim_itemstatusid = po.dim_itemstatusid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_itemstatusid <>  po.dim_itemstatusid;

         UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.Dim_DateIDDocCreation = po.Dim_DateidCreate
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDDocCreation <> po.Dim_DateidCreate;

         UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.Dim_DateIDOrdered = po.Dim_DateidOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_DateIDOrdered <> po.Dim_DateidOrder;

         UPDATE fact_materialmovement ms
        FROM  fact_purchase po,dim_vendor v, dim_plant pl
    SET ms.Dim_MaterialGroupid = po.Dim_MaterialGroupid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_MaterialGroupid <> po.Dim_MaterialGroupid;
  
  UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.dim_salesorderheaderstatusid = so.dim_salesorderheaderstatusid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderheaderstatusid <> so.dim_salesorderheaderstatusid;



  UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET         ms.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderitemstatusid <> so.dim_salesorderitemstatusid;

          UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.dim_salesdocumenttypeid = so.dim_salesdocumenttypeid
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesdocumenttypeid <> so.dim_salesdocumenttypeid;

          UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.dim_salesorderrejectreasonid = so.dim_salesorderrejectreasonid
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderrejectreasonid <> so.dim_salesorderrejectreasonid;

          UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.dim_salesgroupid = so.dim_salesgroupid
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesgroupid <> so.dim_salesgroupid;

          UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.dim_salesorgid = so.dim_salesorgid
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorgid <> so.dim_salesorgid;

          UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.dim_customergroup1id = so.dim_customergroup1id
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo;

          UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.dim_documentcategoryid = so.dim_documentcategoryid
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND ms.dim_documentcategoryid <>  so.dim_documentcategoryid;

          UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.Dim_DateIDDocCreation = so.Dim_DateidSalesOrderCreated
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDDocCreation <> so.Dim_DateidSalesOrderCreated;

          UPDATE fact_materialmovement ms
   FROM  fact_salesorder so
    SET ms.Dim_DateIDOrdered = so.Dim_DateidSalesOrderCreated
WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND ms.Dim_DateIDOrdered <> so.Dim_DateidSalesOrderCreated;
  
  UPDATE fact_materialmovement ms
FROM  fact_productionorder po
    SET ms.dim_productionorderstatusid = po.dim_productionorderstatusid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionorderstatusid<>  po.dim_productionorderstatusid;

  UPDATE fact_materialmovement ms
FROM  fact_productionorder po
SET  ms.dim_productionordertypeid = po.Dim_ordertypeid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionordertypeid <> po.Dim_ordertypeid;

       
  UPDATE fact_materialmovement ms
FROM fact_productionorder po
        SET ms.dim_productionordertypeid = po.Dim_ordertypeid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND ms.dim_productionordertypeid <>  po.Dim_ordertypeid;


