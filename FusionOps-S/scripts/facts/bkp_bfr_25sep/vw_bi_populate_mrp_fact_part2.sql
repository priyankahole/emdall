UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.Dim_ItemCategoryid = p.Dim_ItemCategoryid
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET 
        m.Dim_Vendorid = p.Dim_Vendorid
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.Dim_DocumentTypeid = p.Dim_DocumentTypeid
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.ct_leadTimeVariance =
            ifnull((dt.DateValue - ifnull(MDTB_UMDAT, MDTB_DAT00)), 0)
            - ifnull((Select v_leadTime 
			from tmp_getLeadTime glt 
			where glt.pPart = MDKP_MATNR 
                              AND glt.pPlant =    MDKP_PLWRK
                              AND glt.pVendor =    v.vendorNumber
                              AND glt.DocumentNumber=    MDTB_DELNR
                              AND glt.DocumentLineNumber=    MDTB_DELPS
			      AND glt.fact_script_name = 'bi_populate_mrp_fact'
                              AND glt.DocumentType=    'PO'), 0)
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.amt_ExtendedPrice =
            ifnull((MDTB_MNG01 * p.amt_UnitPrice), 0)
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.amt_ExtendedPrice_GBL =
            ifnull((MDTB_MNG01 * p.amt_UnitPrice), 0) * p.amt_ExchangeRate_GBL
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.dim_consumptiontypeid = p.Dim_ConsumptionTypeid
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.amt_UnitStdPrice = p.amt_StdUnitPrice
  WHERE     tmpTotalCount > 0  and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.Dim_PurchaseGroupid = p.Dim_PurchaseGroupid
  WHERE       tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET Dim_DateidReschedule = ifnull((SELECT od.dim_dateid
                                      FROM dim_date od
                                      WHERE od.DateValue = t.MDTB_UMDAT
                                          AND od.CompanyCode = pl.CompanyCode), 1)
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;


        UPDATE fact_mrp m
From fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt,
	tmpvariable_00e
    SET m.Dim_DateidPOCreated = p.Dim_DateidCreate
  WHERE       tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;

  UPDATE fact_mrp m
From  fact_productionorder p,
        Dim_Company dc,
        mdkp k,
        mdtb t,
	tmpvariable_00e
    SET amt_ExtendedPrice = ifnull((m.ct_QtyMRP * (p.amt_estimatedTotalCost / (case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end))), 0)
         WHERE      tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = p.dd_ordernumber
        AND m.dd_DocumentItemNo = p.dd_orderitemno
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND p.Dim_Companyid = dc.Dim_Companyid;
 


  UPDATE fact_mrp m
From  fact_productionorder p,
        Dim_Company dc,
        mdkp k,
        mdtb t,
	tmpvariable_00e
    SET amt_ExtendedPrice_GBL = ifnull((m.ct_QtyMRP * (p.amt_estimatedTotalCost / (case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end))),0) * p.amt_ExchangeRate_GBL
  WHERE       tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = p.dd_ordernumber
        AND m.dd_DocumentItemNo = p.dd_orderitemno
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND p.Dim_Companyid = dc.Dim_Companyid;




          UPDATE fact_mrp m
From  fact_productionorder p,
        Dim_Company dc,
        mdkp k,
        mdtb t,
	tmpvariable_00e
    SET Dim_DateidReschedule = ifnull((SELECT od.dim_dateid
                                      FROM dim_date od
                                      WHERE od.DateValue = t.MDTB_UMDAT AND od.CompanyCode = dc.CompanyCode), 1)          
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = p.dd_ordernumber
        AND m.dd_DocumentItemNo = p.dd_orderitemno
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND p.Dim_Companyid = dc.Dim_Companyid;

