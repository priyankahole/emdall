
/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 20 Jul 2013 */
/*   Description    : Stored Proc bi_populate_purchasing_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   1 Aug 2013       Lokesh    1.11              Sync up with AFS script. Issue was found with amt_DeliveryTotal, amt_DeliveryPPV */
/*                                                Now ABS is used while retrieving data from tmp_getExchangeRate1 e.g z.pFromExchangeRate = abs(EKKO_WKURS) */
/*   29 Jul 2013      Lokesh    1.10              Reverted Shanthi's ct_QtyReduced changes as they are not going into prod   */
/*   28 Jul 2013      Lokesh    1.9               int overflow in aei when i.EINA_UMREN = 0                       */
/*                                                Note that mysql proc was written expecting this to be non-zero  */
/*   28 Jul 2013      Lokesh    1.8               dd_ConfirmEdi fix. Bug in mysql, caught in honda validation. int to char update.            */
/*							 Changed version numbering to match CVS. 					*/
/*   24 Jul 2013      Lokesh    1.2               Merge Mircea's changes for QtyConversion_EqualTo,QtyConversion_Denom	*/
/*							 PriceConversion_EqualTo and PriceConversion_Denom		  */
/*   24 Jul 2013      Lokesh    1.1               Merge Shanthi's changes for ct_qtyreduced                       */
/*   20 Jul 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/

/* Note that this needs to be split into part1 and part 2 ( divided at the point where it updates the stdunitprice and costing date */
/* Also note that some initial part is in the exchange rate script as it populates tables required by exch rate call */
/* Use columbia99(testVW) for testing and validation */

select 'START OF PROC vw_bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP);

delete from NUMBER_FOUNTAIN where table_name = 'fact_purchase';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_purchase',ifnull(max(fact_purchaseid),0)
FROM fact_purchase;

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
select 'processinglog',ifnull(max(processinglogid),0)
FROM processinglog;


DROP TABLE IF EXISTS tmp_var_purchasing_fact;

CREATE table tmp_var_purchasing_fact (
	pGlobalCurrency,
	pCustomPartnerFunction1,
	pCustomPartnerFunction2,
	pCustomPartnerFunction3,
	pCustomPartnerFunction4)
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD'),varchar(3)),
	CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'custom.vendor.partnerfunction.key1'),
              'Not Set'), varchar(10)),
	CAST( ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'custom.vendor.partnerfunction.key2'),
              'Not Set'),varchar(10)),
	CAST( ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'custom.vendor.partnerfunction.key3'),
              'Not Set'),varchar(10)),
	CAST( ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'custom.vendor.partnerfunction.key4'),
              'Not Set'),varchar(10));


INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'bi_populate_purchasing_fact START',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

  INSERT INTO ekko_ekpo_eket
    SELECT distinct * FROM ekko_ekpo_eket_mseg a
    WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);

 DELETE FROM ekko_ekpo_eket_mseg;

  INSERT INTO ekko_ekpo_eket
    SELECT distinct * FROM ekko_ekpo_eket_ekbe a
    WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);

  DELETE FROM ekko_ekpo_eket_ekbe;

UPDATE ekko_ekpo_eket SET EKPO_BWTAR = IFNULL(EKPO_BWTAR,'0'), EKPO_BUKRS = IFNULL(EKPO_BUKRS,EKKO_BUKRS);

INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;
  
INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE ekko_ekpo_eket, getExchangeRate',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 2 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';  


/* Update 1 - split for each column */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET
 dd_PurchaseReqNo = ifnull(ifnull(dm_di_ds.EKET_BANFN, dm_di_ds.EKPO_BANFN),'Not Set')
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND  ifnull(dd_PurchaseReqNo,'xx') <> ifnull(ifnull(dm_di_ds.EKET_BANFN, dm_di_ds.EKPO_BANFN),'Not Set');

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET dd_PurchaseReqItemNo = ifnull(ifnull(EKET_BNFPO, EKPO_BNFPO),0)
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_PurchaseReqItemNo,-1) <> ifnull(ifnull(EKET_BNFPO, EKPO_BNFPO),0);

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET dd_VendorMaterialNo = ifnull(EKPO_IDNLF, '')
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_VendorMaterialNo,'xx') <> ifnull(EKPO_IDNLF, '');

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET dd_inforecordno = EKPO_INFNR
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_inforecordno,'xx') <> ifnull(EKPO_INFNR,'yy');

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET dd_bomexplosionno = EKET_SERNR
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_bomexplosionno,'xx') <> ifnull(EKET_SERNR,'yy');

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET dd_packagingno = EKPO_PACKNO
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_packagingno,-1) <> ifnull(EKPO_PACKNO,-2);

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET
dd_incoterms2 = ifnull(ifnull(EKPO_INCO2,EKKO_INCO2),'Not Set')
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_incoterms2,'xx') <> ifnull(ifnull(EKPO_INCO2,EKKO_INCO2),'Not Set');

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET
dd_RevisionLevel = ifnull(EKPO_REVLV,'Not Set')
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_RevisionLevel,'xx') <> ifnull(EKPO_REVLV,'Not Set');

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_SalesTax = 0
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_SalesTax <> 0;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_invoice = 0
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_invoice <> 0;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_Paid = 0
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_Paid <> 0;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET	 ct_ExchangeRate = ifnull(( select z.exchangeRate 
								from tmp_getExchangeRate1 z
								 where z.pFromCurrency  = EKKO_WAERS 
								 AND z.pToCurrency = dc.Currency
								 AND z.pFromExchangeRate = EKKO_WKURS
								 AND z.pDate = EKKO_BEDAT
								 and z.fact_script_name = 'bi_populate_purchasing_fact'),1)	
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET	 ct_ExchangeRate = 1
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND EKKO_WAERS = dc.Currency
AND ct_ExchangeRate <> 1;
 

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,tmp_var_purchasing_fact
SET	 amt_ExchangeRate_GBL = ifnull(( select z.exchangeRate 
								from tmp_getExchangeRate1 z
								 where z.pFromCurrency  = dc.Currency 
								 AND z.pToCurrency = pGlobalCurrency
								 AND z.pFromExchangeRate = 0
								 AND z.pDate = EKKO_BEDAT
								 and z.fact_script_name = 'bi_populate_purchasing_fact'),1)	
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,tmp_var_purchasing_fact
SET	 amt_ExchangeRate_GBL = 1
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND pGlobalCurrency = dc.Currency
AND amt_ExchangeRate_GBL <> 1;
 
 
UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_DeliveryQty =  ifnull((CASE WHEN EKKO_AUTLF IS NOT NULL OR EKKO_LOEKZ IS NOT NULL OR EKPO_ELIKZ IS NOT NULL OR EKPO_LOEKZ IS NOT NULL THEN EKET_WEMNG ELSE EKET_MENGE END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_DeliveryQty <>  ifnull((CASE WHEN EKKO_AUTLF IS NOT NULL OR EKKO_LOEKZ IS NOT NULL OR EKPO_ELIKZ IS NOT NULL OR EKPO_LOEKZ IS NOT NULL THEN EKET_WEMNG ELSE EKET_MENGE END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_ReceivedQty = ifnull(EKET_WEMNG, 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_ReceivedQty <> ifnull(EKET_WEMNG, 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_ReturnQty = 0
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_ReturnQty <> 0;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_RejectQty = 0
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_RejectQty <> 0;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_GRProcessingTime = ifnull(EKPO_WEBAZ, 0)
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_GRProcessingTime <> ifnull(EKPO_WEBAZ, 0);

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_OverDeliveryTolerance = ifnull(EKPO_UEBTO, 0)
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_OverDeliveryTolerance <> ifnull(EKPO_UEBTO, 0);

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_UnderDeliveryTolerance = ifnull(EKPO_UNTTO, 0)
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_UnderDeliveryTolerance <> ifnull(EKPO_UNTTO, 0);
 
/* Sync Shanthi's changes  - 24 Jul */
UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET /*ct_BaseUOMQty = EKET_MENGE * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END*/
         ct_BaseUOMQty = ifnull((CASE
                                    WHEN    EKKO_AUTLF IS NOT NULL
                                        OR EKKO_LOEKZ IS NOT NULL
                                        OR EKPO_ELIKZ IS NOT NULL
                                        OR EKPO_LOEKZ IS NOT NULL
                                    THEN EKET_WEMNG
                                    ELSE EKET_MENGE
                                  END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
                        * (EKPO_UMREZ / case when EKPO_UMREN = 0 then 1 else EKPO_UMREN end)
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_BaseUOMQty <> EKET_MENGE * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,EINA i
SET ct_BaseUOMQty = ct_BaseUOMQty * ifnull(( i.EINA_UMREZ / case i.EINA_UMREN when 0 then 1 else i.EINA_UMREN end  ),1)
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND i.INFNR = EKPO_INFNR AND i.EINA_UMREN <> 0
AND ifnull(( i.EINA_UMREZ / case i.EINA_UMREN when 0 then 1 else i.EINA_UMREN end  ),1) <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_StorageLocationid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_StorageLocationid <> 1;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_StorageLocation sl
SET Dim_StorageLocationid = sl.Dim_StorageLocationid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND sl.LocationCode = EKPO_LGORT
 AND sl.Plant = EKPO_WERKS
AND fpr.Dim_StorageLocationid <> sl.Dim_StorageLocationid;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_ItemStatusid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ItemStatusid <> 1;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_ItemStatus its
SET Dim_ItemStatusid = its.Dim_ItemStatusid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND its.Status = EKPO_STATUS
AND fpr.Dim_ItemStatusid <> its.Dim_ItemStatusid;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DocumentStatusid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DocumentStatusid <> 1;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_DocumentStatus dst
SET Dim_DocumentStatusid = dst.Dim_DocumentStatusid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dst.Status = EKKO_STATU
AND fpr.Dim_DocumentStatusid <> dst.Dim_DocumentStatusid;



UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DocumentTypeid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DocumentTypeid <> 1;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_DocumentType dtp
SET Dim_DocumentTypeid = dtp.Dim_DocumentTypeid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dtp.Type = EKKO_BSART AND dtp.Category = EKKO_BSTYP
AND fpr.Dim_DocumentTypeid <> dtp.Dim_DocumentTypeid;


/* Update Dim_UnitOfMeasureid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_UnitOfMeasureid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_UnitOfMeasureid <> 1;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_UnitOfMeasure uom
SET Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND uom.UOM = EKPO_MEINS
AND fpr.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;


/* Update Dim_StockTypeid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_StockTypeid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_StockTypeid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_StockType st
SET Dim_StockTypeid = st.Dim_StockTypeid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND st.TypeCode = EKPO_INSMK
AND fpr.Dim_StockTypeid <> st.Dim_StockTypeid;

/* Update Dim_ConsumptionTypeid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_ConsumptionTypeid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ConsumptionTypeid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_ConsumptionType ct
SET Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND ct.ConsumptionCode = EKPO_KZVBR
AND fpr.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;

/* Update Dim_ItemCategoryid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_ItemCategoryid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ItemCategoryid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_ItemCategory ic
SET Dim_ItemCategoryid = ic.Dim_ItemCategoryid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND ic.CategoryCode = EKPO_PSTYP
AND fpr.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;

/* Update Dim_AccountCategoryid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_AccountCategoryid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_AccountCategoryid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_AccountCategory ac
SET Dim_AccountCategoryid = ac.Dim_AccountCategoryid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND ac.Category = EKPO_KNTTP
AND fpr.Dim_AccountCategoryid <> ac.Dim_AccountCategoryid;

/* Update Dim_IncoTerm1id */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_IncoTerm1id = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_IncoTerm1id <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_IncoTerm it
SET Dim_IncoTerm1id = it.Dim_IncoTermid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND it.IncoTermCode = ifnull(EKPO_INCO1,EKKO_INCO1)
AND fpr.Dim_IncoTerm1id <> it.Dim_IncoTermid;


/* Update Dim_Termid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_Termid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_Termid <> 1;

/* Note that this was using LIMIT 1 without order by in mysql. Updating it like other columns here. It will update the first value of dtm.Dim_Termid it finds in Dim_Term */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Term dtm
SET Dim_Termid = dtm.Dim_Termid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dtm.TermCode = EKKO_ZTERM
AND fpr.Dim_Termid <> dtm.Dim_Termid;

/* Update Dim_DateidDelivery */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidDelivery = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidDelivery <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dsd
SET Dim_DateidDelivery = dsd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dsd.DateValue = EKET_SLFDT AND dsd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidDelivery <> dsd.Dim_Dateid;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd
SET Dim_DateidDelivery = dd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = EKET_EINDT AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidDelivery <> dd.Dim_Dateid;


/* Update Dim_DateIdValidityEnd */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateIdValidityEnd = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateIdValidityEnd <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd
SET Dim_DateIdValidityEnd = dd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = EKKO_KDATE AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateIdValidityEnd <> dd.Dim_Dateid;

/* Update Dim_DateIdValidityStart */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateIdValidityStart = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateIdValidityStart <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd
SET Dim_DateIdValidityStart = dd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = EKKO_KDATB AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateIdValidityStart <> dd.Dim_Dateid;


/* Update Dim_DateidShip */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidShip = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidShip <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dsd
SET Dim_DateidShip = dsd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dsd.DateValue = ( EKET_SLFDT - int(EKPO_PLIFZ))  AND dsd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidShip <> dsd.Dim_Dateid;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd
SET Dim_DateidShip = dd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = (EKET_EINDT - int(EKPO_PLIFZ)) AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidShip <> dd.Dim_Dateid;



/* Update Dim_DateidStatDelivery */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidStatDelivery = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidStatDelivery <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dsd
SET Dim_DateidStatDelivery = dsd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dsd.DateValue = EKET_EINDT AND dsd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidStatDelivery <> dsd.Dim_Dateid;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd
SET Dim_DateidStatDelivery = dd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = EKET_SLFDT AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidStatDelivery <> dd.Dim_Dateid;

/* Update Dim_DateidInvoice */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidInvoice = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidInvoice <> 1;

/* Update Dim_DateidPaid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidPaid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidPaid <> 1;


/* Update Dim_DateidPrice */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidPrice = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidPrice <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dprd
SET Dim_DateidPrice = dprd.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dprd.DateValue = EKPO_PRDAT AND dprd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidPrice <> dprd.Dim_Dateid;

/* Update Dim_PurchaseGroupid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_PurchaseGroupid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_PurchaseGroupid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_PurchaseGroup pg
SET Dim_PurchaseGroupid = pg.Dim_PurchaseGroupid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND pg.PurchaseGroup = EKKO_EKGRP
AND fpr.Dim_PurchaseGroupid <> pg.Dim_PurchaseGroupid;

/* Update Dim_PurchaseOrgid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_PurchaseOrgid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_PurchaseOrgid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_PurchaseOrg po
SET Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND po.PurchaseOrgCode = EKKO_EKORG
AND fpr.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;




/* Update Dim_PlantidOrdering */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_PlantidOrdering = Dim_Plantid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_PlantidOrdering <> Dim_Plantid;


/* Update Dim_PlantidSupplying */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_PlantidSupplying = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_PlantidSupplying <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Plant dps
SET Dim_PlantidSupplying = dps.Dim_Plantid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dps.PlantCode = EKKO_RESWK
AND fpr.Dim_PlantidSupplying <> dps.Dim_Plantid;

/* Update Dim_Currencyid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_Currencyid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_Currencyid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Currency dcr
SET Dim_Currencyid = dcr.Dim_Currencyid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dcr.CurrencyCode = dc.currency
AND fpr.Dim_Currencyid <> dcr.Dim_Currencyid;

/* Update Dim_POCurrencyid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_POCurrencyid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_POCurrencyid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Currency dcr
SET Dim_POCurrencyid = dcr.Dim_Currencyid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dcr.CurrencyCode = EKKO_WAERS
AND fpr.Dim_POCurrencyid <> dcr.Dim_Currencyid;

/* Update Dim_ShipToAddressid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_ShipToAddressid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ShipToAddressid <> 1;

/* LK: This used LIMIT 1 without order by in mysql */
UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_Address sta
SET Dim_ShipToAddressid = sta.Dim_Addressid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  sta.AddressNumber = EKPO_ADRNR                     
AND fpr.Dim_ShipToAddressid <> sta.Dim_Addressid;

/* Update Dim_DelivAddressId */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DelivAddressId = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DelivAddressId <> 1;

/* LIMIT 1 */
UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_Address sta
SET Dim_DelivAddressId = sta.Dim_Addressid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  sta.AddressNumber = EKPO_ADRN2                      
AND fpr.Dim_DelivAddressId <> sta.Dim_Addressid;

/* Update Dim_Partid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_Partid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_Partid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Part dp
SET Dim_Partid = dp.Dim_Partid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dp.PartNumber = EKPO_MATNR AND dp.Plant = EKPO_WERKS
AND fpr.Dim_Partid <> dp.Dim_Partid;

/* Update dim_producthierarchyid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET dim_producthierarchyid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND dim_producthierarchyid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_producthierarchy dph, Dim_Part p
SET dim_producthierarchyid = dph.dim_producthierarchyid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND      dph.ProductHierarchy = p.ProductHierarchy AND p.PartNumber = EKPO_MATNR AND p.Plant = EKPO_WERKS
AND fpr.dim_producthierarchyid <> dph.dim_producthierarchyid;

/* Update Dim_MaterialGroupid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_MaterialGroupid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_MaterialGroupid <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_MaterialGroup mg
SET Dim_MaterialGroupid = mg.Dim_MaterialGroupid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  mg.MaterialGroupCode = EKPO_MATKL
AND fpr.Dim_MaterialGroupid <> mg.Dim_MaterialGroupid;

/* Update Dim_DateIdSchedOrder */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateIdSchedOrder = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateIdSchedOrder <> 1;


UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dc0
SET Dim_DateIdSchedOrder = dc0.Dim_Dateid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dc0.DateValue = EKET_BEDAT AND dc0.CompanyCode = EKPO_BUKRS
AND fpr.Dim_DateIdSchedOrder <> dc0.Dim_Dateid;


/* Update dd_ConfirmEdi */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET dd_ConfirmEdi = ifnull(cast(EKKO_ABSGR as varchar(10)),'Not Set')
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_ConfirmEdi <> ifnull(cast(EKKO_ABSGR as varchar(10)),'Not Set');

/* Update ct_FirmZone */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_FirmZone = ifnull(EKPO_ETFZ1,0)
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_FirmZone <> ifnull(EKPO_ETFZ1,0);


/* Update Dim_MSZoneId */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_MSZoneId = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_MSZoneId <> 1;

/* LIMIT 1 */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_MaterialStagingZone ms
SET Dim_MSZoneId = ms.Dim_MaterialStagingZoneId
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  ms.StagingArea = EKPO_LGBZO
AND fpr.Dim_MSZoneId <> ms.Dim_MaterialStagingZoneId;


/* Update dd_ReferenceNo */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET dd_ReferenceNo = ifnull(EKKO_IHREZ,'Not Set')
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_ReferenceNo <> ifnull(EKKO_IHREZ,'Not Set');

/* Update ct_FirmZone */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_QtyIssued = ifnull(EKET_WAMNG,0)
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_QtyIssued <> ifnull(EKET_WAMNG,0);

/* Update Dim_ProfitCenterid */
/* Note that ifnull and subquery are required in this query. As it involves updating the first record from tmp table can't use that in join */

drop table if exists tmp_dim_pc_purchasing_fact;
create table tmp_dim_pc_purchasing_fact 
as select first 0 * from dim_profitcenter order by ValidTo desc;

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET     dim_profitcenterid = ifnull((SELECT pc.Dim_ProfitCenterid
								FROM tmp_dim_pc_purchasing_fact pc
								WHERE  pc.ProfitCenterCode = EKPO_KO_PRCTR ),1)	
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR;


/* Update Dim_IssuStorageLocid */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_IssuStorageLocid = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_IssuStorageLocid <> 1;


UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_StorageLocation sl
SET Dim_IssuStorageLocid = sl.Dim_StorageLocationid
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND sl.LocationCode = EKPO_RESLO AND sl.Plant = EKKO_RESWK
AND fpr.Dim_IssuStorageLocid <> sl.Dim_StorageLocationid;

/* Update dd_ShortText */

UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET
 dd_ShortText = ifnull(EKPO_TXZ01,'Not Set')
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND  dd_ShortText <> ifnull(EKPO_TXZ01,'Not Set');

/* Update Dim_DateidCreate */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidCreate = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidCreate <> 1;


UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dc0
SET Dim_DateidCreate = dc0.Dim_Dateid
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dc0.DateValue = EKKO_AEDAT AND dc0.CompanyCode = EKPO_BUKRS
AND fpr.Dim_DateidCreate <> dc0.Dim_Dateid;

/* Update Dim_DateidOrder */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidOrder = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidOrder <> 1;


UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dc0
SET Dim_DateidOrder = dc0.Dim_Dateid
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dc0.DateValue = EKKO_BEDAT AND dc0.CompanyCode = EKPO_BUKRS
AND fpr.Dim_DateidOrder <> dc0.Dim_Dateid;

/* Update Dim_DateidItemCreate */

UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET Dim_DateidItemCreate = 1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidItemCreate <> 1;


UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dcrd
SET Dim_DateidItemCreate = dcrd.Dim_Dateid
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dcrd.DateValue = EKPO_AEDAT AND dcrd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidItemCreate <> dcrd.Dim_Dateid;


/* Update amt_GrossOrderValue */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_GrossOrderValue = EKPO_BRTWR
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_GrossOrderValue <> EKPO_BRTWR;

/* Update ct_ItemQty */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET ct_ItemQty = EKPO_MENGE
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_ItemQty <> EKPO_MENGE;

/* Update amt_ItemNetValue */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_ItemNetValue = EKPO_NETWR
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_ItemNetValue <> EKPO_NETWR;

/* Update amt_PricingSbt1 */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_PricingSbt1 = EKPO_KZWI1
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt1 <> EKPO_KZWI1;

/* Update amt_PricingSbt2 */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_PricingSbt2 = EKPO_KZWI2
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt2 <> EKPO_KZWI2;

/* Update amt_PricingSbt3 */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_PricingSbt3 = EKPO_KZWI3
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt3 <> EKPO_KZWI3;

/* Update amt_PricingSbt4 */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_PricingSbt4 = EKPO_KZWI4
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt4 <> EKPO_KZWI4;

/* Update amt_PricingSbt5 */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_PricingSbt5 = EKPO_KZWI5
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt5 <> EKPO_KZWI5;

/* Update amt_PricingSbt6 */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET amt_PricingSbt6 = EKPO_KZWI6
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt6 <> EKPO_KZWI6;

/* Update fpr.Dim_CompanyId */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET fpr.Dim_CompanyId = dc.dim_companyid
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND fpr.Dim_CompanyId <> dc.dim_companyid;


/* Update fpr.QtyConversion_EqualTo and 3 other fields - Mircea changes 24 Jul */
UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET fpr.QtyConversion_EqualTo = EKPO_BPUMZ
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(QtyConversion_EqualTo,-1) <> EKPO_BPUMZ;

UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET fpr.QtyConversion_Denom = EKPO_BPUMN
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(QtyConversion_Denom,-1) <> EKPO_BPUMN;

UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET fpr.PriceConversion_EqualTo = EKPO_UMREZ
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(PriceConversion_EqualTo,-1) <> EKPO_UMREZ;

UPDATE fact_purchase fpr
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET fpr.PriceConversion_Denom = EKPO_UMREN
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(PriceConversion_Denom,-1) <> EKPO_UMREN;

/* Update 1 - Ends */

INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE ekko_ekpo_eket, getExchangeRate',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'INSERT INTO fact_purchase',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';
  
  
/* Insert 1 - Starts */  
  
  INSERT INTO fact_purchase(dd_DocumentNo,
                            dd_DocumentItemNo,
                            dd_ScheduleNo,
                            dd_PurchaseReqNo,
                            dd_PurchaseReqItemNo,
                            dd_VendorMaterialNo,
                            dd_inforecordno,
                            dd_ConfirmEdi,
                            dd_incoterms2,
                            amt_SalesTax,
                            amt_invoice,
                            amt_Paid,
                            ct_ExchangeRate,
                            amt_ExchangeRate_GBL,
                            ct_DeliveryQty,
                            ct_ReceivedQty,
                            ct_ReturnQty,
                            ct_RejectQty,
                            ct_GRProcessingTime,
                            ct_OverDeliveryTolerance,
                            ct_UnderDeliveryTolerance,
                            ct_leadtime,
                            ct_LeadTimeVariance,
                            ct_BaseUOMQty,
                            Dim_StorageLocationid,
                            Dim_ShipToAddressid,
                            Dim_DelivAddressId,
                            Dim_ItemStatusid,
                            Dim_DocumentStatusid,
                            Dim_DocumentTypeid,
                            Dim_UnitOfMeasureid,
                            Dim_StockTypeid,
                            Dim_ConsumptionTypeid,
                            Dim_ItemCategoryid,
                            Dim_AccountCategoryid,
                            Dim_IncoTerm1id,
                            Dim_Termid,
                            Dim_Vendorid,
                            Dim_DateidOrder,
                            Dim_DateidCreate,
                            Dim_DateidDelivery,
                            Dim_DateidStatDelivery,
                            Dim_DateidShip,
                            Dim_DateidItemCreate,
                            Dim_DateidInvoice,
                            Dim_DateidPaid,
                            Dim_DateidPrice,
                            Dim_DateidValidityEnd,
                            Dim_DateidValidityStart,
                            Dim_Partid,
                            Dim_PurchaseGroupid,
                            Dim_PurchaseOrgid,
                            Dim_PlantidOrdering,
                            Dim_PlantidSupplying,
                            Dim_Currencyid,
                            Dim_POCurrencyid,
                            Dim_PurchaseMiscid,
                            dim_producthierarchyid,
                            Dim_MaterialGroupid,
                            dd_bomexplosionno,
                            dd_packagingno,
                            Dim_DateIdSchedOrder,
                            ct_FirmZone,
                            Dim_MSZoneId,
                            ct_TotalLeadTime,
                            ct_TotalRLTime,
                            dd_RevisionLevel,
                            dd_ReferenceNo,
                            ct_QtyIssued,
                            Dim_ProfitCenterid,
                            Dim_IssuStorageLocid,
                            dd_ShortText,
                            amt_GrossOrderValue,
                            ct_ItemQty,
                            amt_ItemNetValue,
                            amt_PricingSbt1,
                            amt_PricingSbt2,
                            amt_PricingSbt3,
                            amt_PricingSbt4,
                            amt_PricingSbt5,
                            amt_PricingSbt6,
                            Dim_CompanyId,
							 QtyConversion_EqualTo,
							  QtyConversion_Denom,
							  PriceConversion_EqualTo,
							  PriceConversion_Denom,							
							fact_purchaseid)
     (SELECT EKPO_EBELN,
             EKPO_EBELP,
             EKET_ETENR,
             ifnull(ifnull(dm_di_ds.EKET_BANFN, dm_di_ds.EKPO_BANFN),'Not Set'),
             ifnull(ifnull(EKET_BNFPO, EKPO_BNFPO),0),
             ifnull(EKPO_IDNLF, ''),
             EKPO_INFNR,
             ifnull(cast(EKKO_ABSGR as varchar(10)),'Not Set'),
             ifnull(ifnull(EKPO_INCO2,EKKO_INCO2),'Not Set'),
             0,
             0,
             0,
            /* CASE WHEN EKKO_WAERS = dc.Currency THEN 1.00 ELSE getExchangeRate(EKKO_WAERS,
                                          dc.Currency,
                                          EKKO_WKURS,
                                          EKKO_BEDAT) END,
             CASE WHEN dc.Currency = pGlobalCurrency THEN 1.00 ELSE getExchangeRate(dc.Currency,
                            pGlobalCurrency,
                            0,
                            EKKO_BEDAT) END,	*/
			 CASE WHEN EKKO_WAERS = dc.Currency THEN 1.00 ELSE -5 END,
			 CASE WHEN dc.Currency = pGlobalCurrency THEN 1.00 ELSE -5 END,					
             (CASE
                WHEN    EKKO_AUTLF IS NOT NULL
                    OR EKKO_LOEKZ IS NOT NULL
                    OR EKPO_ELIKZ IS NOT NULL
                    OR EKPO_LOEKZ IS NOT NULL
                THEN EKET_WEMNG
                ELSE EKET_MENGE
              END) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END,
             EKET_WEMNG * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END,
             0,
             0,
             EKPO_WEBAZ,
             EKPO_UEBTO,
             EKPO_UNTTO,
             /*getLeadTime(EKPO_MATNR,
                         EKPO_WERKS,
                         EKKO_LIFNR,
                         EKPO_EBELN,
                         EKPO_EBELP,
                         'PO') leadTime,*/
			 -99999 leadTime,
             /*ifnull(datediff(EKET_EINDT, (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
                                          where x.EKPO_EBELN = dm_di_ds.EKPO_EBELN and x.EKPO_EBELP = dm_di_ds.EKPO_EBELP)), 0)
             - ifnull((getLeadTime(EKPO_MATNR,
                                   EKPO_WERKS,
                                   EKKO_LIFNR,
                                   EKPO_EBELN,
                                   EKPO_EBELP,
                                   'PO')), 0) leadTime_Variance,*/
			 -99999 leadTime_Variance,
              (CASE
                WHEN    EKKO_AUTLF IS NOT NULL
                    OR EKKO_LOEKZ IS NOT NULL
                    OR EKPO_ELIKZ IS NOT NULL
                    OR EKPO_LOEKZ IS NOT NULL
                THEN EKET_WEMNG
                ELSE EKET_MENGE
              END) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
             * ifnull((SELECT i.EINA_UMREZ / case i.EINA_UMREN when 0 then 1 else i.EINA_UMREN end  
                         FROM EINA i
                        WHERE i.INFNR = EKPO_INFNR), 1) ct_baseuomqty,
             ifnull((SELECT Dim_StorageLocationid
                       FROM dim_StorageLocation sl
                      WHERE sl.LocationCode = EKPO_LGORT AND sl.Plant = EKPO_WERKS),
                    1),
             ifnull((SELECT min(Dim_Addressid)
                       FROM Dim_Address sta
                      WHERE sta.AddressNumber = EKPO_ADRNR), 1)
                Dim_Addressid,
            ifnull((SELECT min(Dim_Addressid)
                      FROM dim_Address sta
                     WHERE sta.AddressNumber = EKPO_ADRN2), 1)  Dim_DelivAddressid,
             ifnull((SELECT Dim_ItemStatusid
                       FROM Dim_ItemStatus its
                      WHERE its.Status = EKPO_STATUS),
                    1),
             ifnull((SELECT Dim_DocumentStatusid
                       FROM Dim_DocumentStatus dst
                      WHERE dst.Status = EKKO_STATU),
                    1),
             ifnull(
                (SELECT Dim_DocumentTypeid
                   FROM Dim_DocumentType dtp
                  WHERE     dtp.Type = EKKO_BSART
                        AND dtp.Category = EKKO_BSTYP),
                1),
             ifnull((SELECT Dim_UnitOfMeasureid
                       FROM Dim_UnitOfMeasure uom
                      WHERE uom.UOM = EKPO_MEINS), 1),
             ifnull((SELECT Dim_StockTypeid
                       FROM Dim_StockType st
                      WHERE st.TypeCode = EKPO_INSMK), 1),
             ifnull(
                (SELECT Dim_ConsumptionTypeid
                   FROM Dim_ConsumptionType ct
                  WHERE ct.ConsumptionCode = EKPO_KZVBR),
                1),
             ifnull(
                (SELECT Dim_ItemCategoryid
                   FROM Dim_ItemCategory ic
                  WHERE ic.CategoryCode = EKPO_PSTYP),
                1),
             ifnull((SELECT Dim_AccountCategoryid
                       FROM Dim_AccountCategory ac
                      WHERE ac.Category = EKPO_KNTTP), 1),
             ifnull(
                (SELECT Dim_IncoTermid
                   FROM Dim_IncoTerm it
                  WHERE it.IncoTermCode = ifnull(EKPO_INCO1,EKKO_INCO1)),
                1),
             ifnull((SELECT min(Dim_Termid)
                       FROM Dim_Term dtm
                      WHERE dtm.TermCode = EKKO_ZTERM ), 1),
             ifnull((select v.Dim_Vendorid from dim_vendor v
                    where v.VendorNumber = EKKO_LIFNR),1) Dim_Vendorid,
             ifnull(
                (SELECT Dim_Dateid
                   FROM Dim_Date dcrd
                  WHERE dcrd.DateValue = EKKO_BEDAT AND dcrd.CompanyCode = pl.CompanyCode),
                1) Dim_DateidOrder,
             ifnull(
                  (SELECT Dim_Dateid
                      FROM Dim_Date dd
                    WHERE dd.DateValue = EKKO_AEDAT AND dd.CompanyCode = pl.CompanyCode),
                  1) Dim_DateidCreate,
             ifnull(
                (SELECT Dim_Dateid
                   FROM Dim_Date dd
                  WHERE dd.DateValue = EKET_EINDT AND dd.CompanyCode = pl.CompanyCode),
                ifnull(
                    (SELECT Dim_Dateid
                        FROM Dim_Date dsd
                      WHERE dsd.DateValue = EKET_SLFDT AND dsd.CompanyCode = pl.CompanyCode),
                    1))
                Dim_DateidDelivery,
             ifnull(
                (SELECT Dim_Dateid
                   FROM Dim_Date dsd
                  WHERE dsd.DateValue = EKET_SLFDT AND dsd.CompanyCode = pl.CompanyCode),
                ifnull(
                    (SELECT Dim_Dateid
                        FROM Dim_Date dsd
                      WHERE dsd.DateValue = EKET_EINDT AND dsd.CompanyCode = pl.CompanyCode),
                    1))
                Dim_DateidStatDelivery,
             ifnull(
               (SELECT Dim_Dateid
                  FROM Dim_Date dd
                 WHERE dd.DateValue =  ( EKET_EINDT - int(EKPO_PLIFZ) )  AND dd.CompanyCode = pl.CompanyCode),
               ifnull(
                  (SELECT Dim_Dateid
                      FROM Dim_Date dd
                    WHERE dd.DateValue = (EKET_SLFDT - int(EKPO_PLIFZ) ) AND dd.CompanyCode = pl.CompanyCode),
                  1))
               Dim_DateidShip,
             ifnull(
                (SELECT Dim_Dateid
                   FROM Dim_Date dcrd
                  WHERE dcrd.DateValue = EKPO_AEDAT AND dcrd.CompanyCode = pl.CompanyCode),
                1)
                Dim_DateidItemCreate,
             1 Dim_DateidInvoice,
             1 Dim_DateidPaid,
             ifnull(
                (SELECT Dim_Dateid
                   FROM Dim_Date dprd
                  WHERE dprd.DateValue = EKPO_PRDAT
                        AND dprd.CompanyCode = pl.CompanyCode),
                1)
                Dim_DateidPrice,
             ifnull(
               (SELECT Dim_Dateid
                  FROM Dim_Date dd
                 WHERE dd.DateValue = EKKO_KDATE AND dd.CompanyCode = pl.CompanyCode),
               1)
               Dim_DateIdValidityEnd,
             ifnull(
               (SELECT Dim_Dateid
                  FROM Dim_Date dd
                 WHERE dd.DateValue = EKKO_KDATB AND dd.CompanyCode = pl.CompanyCode),
               1) Dim_DateIdValidityStart,
             ifnull((SELECT dim_partid
                       FROM dim_part dp
                      WHERE dp.PartNumber = EKPO_MATNR AND dp.Plant = EKPO_WERKS),
                    1)
                    Dim_Partid,
             ifnull((SELECT Dim_PurchaseGroupid
                       FROM Dim_PurchaseGroup pg
                      WHERE pg.PurchaseGroup = EKKO_EKGRP),
                    1),
             ifnull(
                (SELECT Dim_PurchaseOrgid
                   FROM Dim_PurchaseOrg po
                  WHERE po.PurchaseOrgCode = EKKO_EKORG),
                1),
             Dim_Plantid Dim_PlantidOrdering,
             ifnull(
                (SELECT Dim_Plantid
                   FROM Dim_Plant dps
                  WHERE dps.PlantCode = EKKO_RESWK),
                1)
                Dim_PlantidSupplying,
             ifnull((SELECT Dim_Currencyid
                       FROM Dim_Currency dcr
                      WHERE dcr.CurrencyCode = dc.currency), 1) Dim_Currencyid,
             ifnull((SELECT Dim_Currencyid
                       FROM Dim_Currency dcr
                      WHERE dcr.CurrencyCode = EKKO_WAERS), 1) Dim_POCurrencyid,
             1 Dim_PurchaseMisc,
             ifnull(
                (SELECT dim_producthierarchyid
                   FROM dim_producthierarchy dph, Dim_Part p
                  WHERE     dph.ProductHierarchy = p.ProductHierarchy
                        AND p.PartNumber = EKPO_MATNR
                        AND p.Plant = EKPO_WERKS
                        AND dph.RowIsCurrent = 1),
                1)
                dim_producthierarchyid,
            ifnull((SELECT Dim_MaterialGroupid
                      FROM Dim_MaterialGroup mg
                     WHERE mg.MaterialGroupCode = EKPO_MATKL), 1) Dim_MaterialGroupid,
            EKET_SERNR dd_bomexplosionno,
            EKPO_PACKNO dd_packagingno,
            ifnull(
                (SELECT Dim_Dateid
                   FROM Dim_Date dcrd
                  WHERE dcrd.DateValue = EKET_BEDAT AND dcrd.CompanyCode = pl.CompanyCode),
                1) Dim_DateIdSchedOrder,
                ifnull(EKPO_ETFZ1,0),
            ifnull(( SELECT min(Dim_MaterialStagingZoneId)
                      FROM Dim_MaterialStagingZone ms
                      WHERE ms.StagingArea = EKPO_LGBZO),1),
            ifnull((SELECT ( LeadTime + EKPO_WEBAZ + ProcessingTime)
                    FROM dim_Part pt WHERE pt.PartNumber = dm_di_ds.EKPO_MATNR
                            AND pt.Plant = dm_di_ds.EKPO_WERKS),0),
            ifnull((SELECT TotalReplenishmentLeadTime
                    FROM dim_Part pt WHERE pt.PartNumber = dm_di_ds.EKPO_MATNR
                            AND pt.Plant = dm_di_ds.EKPO_WERKS),0),
            ifnull(EKPO_REVLV,'Not Set') dd_RevisionLevel,
            ifnull(EKKO_IHREZ,'Not Set') dd_ReferenceNo,
            ifnull(EKET_WAMNG,0) ct_QtyIssued,
            /*ifnull((select pc.Dim_ProfitCenterid from dim_profitcenter pc
                    where pc.ProfitCenterCode = EKPO_KO_PRCTR
                    order by pc.ValidTo desc limit 1),1) Dim_ProfitCenterid,*/
			-1 Dim_ProfitCenterid,
            ifnull((SELECT Dim_StorageLocationid
                      FROM dim_StorageLocation sl
                     WHERE sl.LocationCode = EKPO_RESLO
                          AND sl.Plant = EKKO_RESWK),1) Dim_IssuStorageLocid,
            ifnull(EKPO_TXZ01,'Not Set'),
            EKPO_BRTWR,
            EKPO_MENGE,
            EKPO_NETWR,
            EKPO_KZWI1,
            EKPO_KZWI2,
            EKPO_KZWI3,
            EKPO_KZWI4,
            EKPO_KZWI5,
            EKPO_KZWI6,
            dc.dim_companyid, 
			EKPO_BPUMZ,
				  EKPO_BPUMN,
				  EKPO_UMREZ,
				  EKPO_UMREN,
			 (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_purchase')+ row_number() over ()
        FROM tmp_var_purchasing_fact,
			 ekko_ekpo_eket dm_di_ds
             INNER JOIN dim_plant pl
                ON pl.PlantCode = EKPO_WERKS
             INNER JOIN dim_company dc
                ON dc.companycode = EKPO_BUKRS
       WHERE NOT EXISTS
                    (SELECT 1
                       FROM fact_purchase fp
                      WHERE     fp.dd_DocumentNo = dm_di_ds.EKPO_EBELN
                            AND fp.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
                            AND fp.dd_ScheduleNo = dm_di_ds.EKET_ETENR));

				
	 /* Update ct_ExchangeRate and amt_ExchangeRate_GBL */
	 
	 UPDATE fact_purchase fpr
	 FROM tmp_var_purchasing_fact,  ekko_ekpo_eket dm_di_ds ,dim_plant pl,dim_company dc
	 SET  fpr.ct_ExchangeRate =
	  ifnull(( select z.exchangeRate 
						from tmp_getExchangeRate1 z
						 where z.pFromCurrency  = EKKO_WAERS 
						 AND z.pToCurrency = dc.Currency
						 AND z.pFromExchangeRate = EKKO_WKURS
						 AND z.pDate = EKKO_BEDAT
						 and z.fact_script_name = 'bi_populate_purchasing_fact'),1)	              
	 WHERE pl.PlantCode = EKPO_WERKS
	 AND dc.companycode = EKPO_BUKRS
	 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
	 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
	 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
	 AND  fpr.ct_ExchangeRate = -5   ;
	 
	 UPDATE fact_purchase fpr
	 FROM tmp_var_purchasing_fact,  ekko_ekpo_eket dm_di_ds ,dim_plant pl,dim_company dc
	 SET  fpr.amt_ExchangeRate_GBL =
	  ifnull(( select z.exchangeRate 
						from tmp_getExchangeRate1 z
						 where z.pFromCurrency  = dc.Currency 
						 AND z.pToCurrency = pGlobalCurrency
						 AND z.pFromExchangeRate = 0
						 AND z.pDate = EKKO_BEDAT
						 and z.fact_script_name = 'bi_populate_purchasing_fact'),1)	              
	 WHERE pl.PlantCode = EKPO_WERKS
	 AND dc.companycode = EKPO_BUKRS
	 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
	 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
	 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR	 
	 AND  fpr.amt_ExchangeRate_GBL = -5   ;
	 
	 /* Update lead time and lead time variance */
             					
	 UPDATE fact_purchase fpr
	 FROM ekko_ekpo_eket dm_di_ds ,dim_plant pl,dim_company dc              
	 SET ct_leadtime =    ifnull((SELECT v_leadTime 
						  FROM tmp_getLeadTime 
						  WHERE pPart = EKPO_MATNR AND pPlant = EKPO_WERKS
						  AND pVendor = EKKO_LIFNR AND DocumentNumber = EKPO_EBELN
						  AND DocumentLineNumber =  EKPO_EBELP AND DocumentType =  'PO'
						  AND fact_script_name = 'bi_populate_purchasing_fact'  )                              
						  ,0)    
	 WHERE pl.PlantCode = EKPO_WERKS
	 AND dc.companycode = EKPO_BUKRS
	 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
	 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
	 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR	 
	 AND  fpr.ct_leadtime = -99999   ;				

	 UPDATE fact_purchase fpr
	 FROM ekko_ekpo_eket dm_di_ds ,dim_plant pl,dim_company dc   
	 SET ct_LeadTimeVariance
				= ifnull(EKET_EINDT - (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
                                          where x.EKPO_EBELN = dm_di_ds.EKPO_EBELN and x.EKPO_EBELP = dm_di_ds.EKPO_EBELP),0)
				  - ct_leadtime
	 WHERE pl.PlantCode = EKPO_WERKS
	 AND dc.companycode = EKPO_BUKRS
	 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
	 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
	 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR	 
	 AND  fpr.ct_LeadTimeVariance = -99999   ;		

	/* Update dim_profitcenterid */
		 
UPDATE fact_purchase fpr
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
SET     dim_profitcenterid = ifnull((SELECT pc.Dim_ProfitCenterid
								FROM tmp_dim_pc_purchasing_fact pc
								WHERE  pc.ProfitCenterCode = EKPO_KO_PRCTR ),1)	
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
 AND fpr.Dim_ProfitCenterid = -1;	 
	 
/* Insert 1 - Ends */  
							
 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_purchaseid) from fact_purchase), 0)
 where table_name = 'fact_purchase';							

 INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'INSERT INTO fact_purchase',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fp ekko_ekpo_eket dm_di_ds',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';
  
/* Update 2 */
	 /* Update lead time and lead time variance where leadtime is 0 */
             					
	 UPDATE fact_purchase fp
	 FROM ekko_ekpo_eket dm_di_ds, dim_date dt      
	 SET ct_leadtime =    ifnull((SELECT v_leadTime 
						  FROM tmp_getLeadTime 
						  WHERE pPart = EKPO_MATNR AND pPlant = EKPO_WERKS
						  AND pVendor = EKKO_LIFNR AND DocumentNumber = EKPO_EBELN
						  AND DocumentLineNumber =  EKPO_EBELP AND DocumentType =  'PO'
						  AND fact_script_name = 'bi_populate_purchasing_fact'  )                              
						  ,0)    
	 WHERE  fp.dd_DocumentNo = dm_di_ds.EKPO_EBELN
         AND fp.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
         AND fp.dd_ScheduleNo = dm_di_ds.EKET_ETENR
         AND fp.Dim_DateidDelivery = dt.Dim_Dateid
         AND EKET_EINDT IS NOT NULL
         AND dt.Dim_Dateid <> 1
         AND fp.ct_leadtime = 0;	


	 UPDATE fact_purchase fp
	 FROM ekko_ekpo_eket dm_di_ds, dim_date dt      
	 SET ct_LeadTimeVariance
				= ifnull(EKET_EINDT - (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
                                          where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo),0)
				  - ct_leadtime
	 WHERE  fp.dd_DocumentNo = dm_di_ds.EKPO_EBELN
         AND fp.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
         AND fp.dd_ScheduleNo = dm_di_ds.EKET_ETENR
         AND fp.Dim_DateidDelivery = dt.Dim_Dateid
         AND EKET_EINDT IS NOT NULL
         AND dt.Dim_Dateid <> 1
         AND fp.ct_leadtime = 0;			

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_purchase; 
		 
/* End of Update 2 */

 INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fp ekko_ekpo_eket dm_di_ds',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_purchase fp, ekko_ekpo_eket',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

call vectorwise(combine 'fact_purchase');
call vectorwise(combine 'NUMBER_FOUNTAIN');
call vectorwise(combine 'processinglog');
call vectorwise(combine 'ekko_ekpo_eket');
call vectorwise(combine 'ekko_ekpo_eket_mseg');
call vectorwise(combine 'tmp_var_purchasing_fact');
call vectorwise(combine 'ekko_ekpo_eket_ekbe');
