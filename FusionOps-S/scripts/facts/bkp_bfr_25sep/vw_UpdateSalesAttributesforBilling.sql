DROP TABLE IF EXISTS TMP_updfb_fact_salesorder;

CREATE TABLE TMP_updfb_fact_salesorder
AS
   SELECT f_so.dd_SalesDocNo,
          f_so.dd_SalesItemNo,
          SUM(f_so.ct_ConfirmedQty) AS sum_ct_ConfirmedQty,
          SUM(f_so.ct_DeliveredQty) AS sum_ct_DeliveredQty,
          SUM(f_so.amt_ScheduleTotal) AS sum_amt_ScheduleTotal,
          SUM(f_so.ct_ScheduleQtySalesUnit) AS sum_ct_ScheduleQtySalesUnit,
          00000000000000.0000 AS upd_ct_so_OpenOrderQty,
          SUM(f_so.ct_ConfirmedQty * f_so.amt_UnitPrice / (case when f_so.ct_PriceUnit=0 then 1 else f_so.ct_PriceUnit end))
             AS upd_amt_so_confirmed,
          00000000000000.0000 AS upd_amt_so_openorder
     FROM fact_salesorder f_so
   GROUP BY f_so.dd_SalesDocNo, f_so.dd_SalesItemNo;

UPDATE TMP_updfb_fact_salesorder
   SET upd_ct_so_OpenOrderQty =
          sum_ct_ScheduleQtySalesUnit - sum_ct_DeliveredQty;

UPDATE TMP_updfb_fact_salesorder
SET upd_amt_so_openorder = (sum_ct_ScheduleQtySalesUnit - sum_ct_DeliveredQty);

UPDATE fact_billing fb

FROM fact_salesorder fso, Dim_documentcategory dc, vbak_vbap_vbep vkp
set fb.ct_so_ConfirmedQty =
(select f_so.sum_ct_ConfirmedQty from TMP_updfb_fact_salesorder f_so where fb.dd_salesdocno = f_so.dd_SalesDocNo and fb.dd_salesitemno = f_so.dd_SalesItemNo ),
fb.ct_so_DeliveredQty =
(select f_so.sum_ct_DeliveredQty from TMP_updfb_fact_salesorder f_so where fb.dd_salesdocno = f_so.dd_SalesDocNo and fb.dd_salesitemno = f_so.dd_SalesItemNo ),
fb.ct_so_OpenOrderQty =
(select f_so.upd_ct_so_OpenOrderQty from TMP_updfb_fact_salesorder f_so where fb.dd_salesdocno = f_so.dd_SalesDocNo and fb.dd_salesitemno = f_so.dd_SalesItemNo),
fb.amt_so_ScheduleTotal =
(select f_so.sum_amt_ScheduleTotal from TMP_updfb_fact_salesorder f_so where fb.dd_salesdocno = f_so.dd_SalesDocNo and fb.dd_salesitemno = f_so.dd_SalesItemNo ),
fb.ct_so_ScheduleQtySalesUnit =
(select f_so.sum_ct_ScheduleQtySalesUnit from TMP_updfb_fact_salesorder f_so where fb.dd_salesdocno = f_so.dd_SalesDocNo and fb.dd_salesitemno = f_so.dd_SalesItemNo ),
fb.amt_so_openorder =
(select (f_so.upd_amt_so_openorder)*(fso.amt_UnitPrice/(case when fso.ct_PriceUnit=0 then 1 else fso.ct_PriceUnit end)) from TMP_updfb_fact_salesorder f_so where fb.dd_salesdocno = f_so.dd_SalesDocNo and fb.dd_salesitemno = f_so.dd_SalesItemNo ),
fb.amt_so_confirmed =
(select f_so.upd_amt_so_confirmed from TMP_updfb_fact_salesorder f_so where fb.dd_salesdocno = f_so.dd_SalesDocNo and fb.dd_salesitemno = f_so.dd_SalesItemNo )
WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
AND dc.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBAK_VBELN
AND fb.dd_billing_item_no = vkp.VBAP_POSNR;

UPDATE fact_billing fb

FROM fact_salesorder fso, Dim_documentcategory dc, vbak_vbap_vbep vkp,TMP_updfb_fact_salesorder f_so
SET fb.amt_so_Returned =
CASE
WHEN dc.DocumentCategory IN ('H', 'K')
AND fso.Dim_SalesOrderRejectReasonid <> 1
THEN f_so.sum_amt_ScheduleTotal
ELSE 0
END,
fb.ct_so_ReturnedQty =
CASE
WHEN dc.DocumentCategory IN ('H', 'K')
AND fso.Dim_SalesOrderRejectReasonid <> 1
THEN f_so.sum_ct_ScheduleQtySalesUnit
ELSE 0
END
WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND fso.dd_SalesDocNo = f_so.dd_SalesDocNo
AND fso.dd_SalesItemNo = f_so.dd_SalesItemNo
AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
AND dc.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBAK_VBELN
AND fb.dd_billing_item_no = vkp.VBAP_POSNR;

UPDATE fact_billing fb

FROM fact_salesorder fso, Dim_documentcategory dc, vbak_vbap_vbep vkp
SET fb.Dim_CustomerId = fso.Dim_CustomerID,
fb.Dim_SalesDocumentItemStatusId = fso.dim_salesorderitemstatusid,
fb.Dim_so_CostCenterid = fso.Dim_CostCenterid,
fb.Dim_so_SalesDocumentTypeid = fso.Dim_SalesDocumentTypeid,
fb.Dim_so_DateidSchedDelivery = fso.Dim_DateidSchedDelivery,
fb.Dim_so_SalesOrderCreatedId = fso.Dim_DateidSalesOrderCreated,
fb.amt_so_UnitPrice = fso.amt_UnitPrice,
/*-- Order Amount
-- Order qty */
fb.amt_revenue =
(CASE dc.DocumentCategory
WHEN 'U' THEN 0
WHEN '5' THEN 0
WHEN '6' THEN 0
WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
ELSE fb.amt_NetValueItem
END)
WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
AND dc.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBAK_VBELN
AND fb.dd_billing_item_no = vkp.VBAP_POSNR;

DROP TABLE IF EXISTS TMP_updfb_fact_salesorder;
call vectorwise(combine 'fact_billing');