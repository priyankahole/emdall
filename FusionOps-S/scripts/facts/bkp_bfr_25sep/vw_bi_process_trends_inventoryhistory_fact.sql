/**********************************************************************************/
/*  28 Aug 2013   1.56	      Shanthi    New Field WIP Qty			  */
/**********************************************************************************/

SELECT 'A - START OF PROC bi_process_trends_inventoryhistory_fact - latest version as of 6th Feb ( Shanthis changes as per 5th Feb merged )',
       TIMESTAMP(LOCAL_TIMESTAMP);

SELECT 'A1 - Populate fact_inventoryhistory ', TIMESTAMP(LOCAL_TIMESTAMP);

DELETE FROM fact_inventoryhistory
 WHERE SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP);

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_inventoryhistory';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_inventoryhistory', ifnull(max(fact_inventoryhistoryid), 0)
     FROM fact_inventoryhistory;

INSERT INTO fact_inventoryhistory(ct_POOpenQty, /* Issam change 24th Jun */
                                  ct_SOOpenQty, /* Issam change 24th Jun */
                                  amt_BlockedStockAmt,
                                  amt_BlockedStockAmt_GBL,
                                  amt_CommericalPrice1,
                                  amt_CurPlannedPrice,
                                  amt_MovingAvgPrice,
                                  amt_MtlDlvrCost,
                                  amt_OtherCost,
                                  amt_OverheadCost,
                                  amt_PlannedPrice1,
                                  amt_PreviousPrice,
                                  amt_PrevPlannedPrice,
                                  amt_StdUnitPrice,
                                  amt_StdUnitPrice_GBL,
                                  amt_StockInQInspAmt,
                                  amt_StockInQInspAmt_GBL,
                                  amt_StockInTransferAmt,
                                  amt_StockInTransferAmt_GBL,
                                  amt_StockInTransitAmt,
                                  amt_StockInTransitAmt_GBL,
                                  amt_StockValueAmt,
                                  amt_StockValueAmt_GBL,
                                  amt_UnrestrictedConsgnStockAmt,
                                  amt_UnrestrictedConsgnStockAmt_GBL,
                                  amt_WIPBalance,
				  ct_WIPQty,
                                  ct_BlockedConsgnStock,
                                  ct_BlockedStock,
                                  ct_BlockedStockReturns,
                                  ct_ConsgnStockInQInsp,
                                  ct_LastReceivedQty,
                                  ct_RestrictedConsgnStock,
                                  ct_StockInQInsp,
                                  ct_StockInTransfer,
                                  ct_StockInTransit,
                                  ct_StockQty,
                                  ct_TotalRestrictedStock,
                                  ct_UnrestrictedConsgnStock,
                                  ct_WIPAging,
                                  dd_BatchNo,
                                  dd_DocumentItemNo,
                                  dd_DocumentNo,
                                  dd_MovementType,
                                  dd_ValuationType,
                                  dim_Companyid,
                                  Dim_ConsumptionTypeid,
                                  dim_costcenterid,
                                  dim_Currencyid,
                                  Dim_DateIdLastChangedPrice,
                                  Dim_DateIdPlannedPrice2,
                                  Dim_DocumentStatusid,
                                  Dim_DocumentTypeid,
                                  Dim_IncoTermid,
                                  Dim_ItemCategoryid,
                                  Dim_ItemStatusid,
                                  dim_LastReceivedDateid,
                                  Dim_MovementIndicatorid,
                                  dim_Partid,
                                  dim_Plantid,
				  Dim_ProfitCenterId,
                                  dim_producthierarchyid,
                                  Dim_PurchaseGroupid,
                                  Dim_PurchaseMiscid,
                                  Dim_PurchaseOrgid,
                                  dim_specialstockid,
                                  dim_stockcategoryid,
                                  dim_StockTypeid,
                                  dim_StorageLocationid,
                                  dim_StorageLocEntryDateid,
                                  Dim_SupplyingPlantId,
                                  Dim_Termid,
                                  Dim_UnitOfMeasureid,
                                  dim_Vendorid,
                                  SnapshotDate,
                                  Dim_DateidSnapshot,
                                  fact_inventoryhistoryid,
				  amt_ExchangeRate_GBL)
   SELECT ifnull(ct_POOpenQty, 0),/* Issam change 24th Jun */
          ifnull(ct_SOOpenQty, 0), /* Issam change 24th Jun */
          amt_BlockedStockAmt,
          amt_BlockedStockAmt_GBL,
          amt_CommericalPrice1,
          amt_CurPlannedPrice,
          amt_MovingAvgPrice,
          amt_MtlDlvrCost,
          amt_OtherCost,
          amt_OverheadCost,
          amt_PlannedPrice1,
          amt_PreviousPrice,
          amt_PrevPlannedPrice,
          amt_StdUnitPrice,
          amt_StdUnitPrice_GBL,
          amt_StockInQInspAmt,
          amt_StockInQInspAmt_GBL,
          amt_StockInTransferAmt,
          amt_StockInTransferAmt_GBL,
          amt_StockInTransitAmt,
          amt_StockInTransitAmt_GBL,
          amt_StockValueAmt,
          amt_StockValueAmt_GBL,
          amt_UnrestrictedConsgnStockAmt,
          amt_UnrestrictedConsgnStockAmt_GBL,
          amt_WIPBalance,
	  ct_WIPQty,
          ct_BlockedConsgnStock,
          ct_BlockedStock,
          ct_BlockedStockReturns,
          ct_ConsgnStockInQInsp,
          ct_LastReceivedQty,
          ct_RestrictedConsgnStock,
          ct_StockInQInsp,
          ct_StockInTransfer,
          ct_StockInTransit,
          ct_StockQty,
          ct_TotalRestrictedStock,
          ct_UnrestrictedConsgnStock,
          ct_WIPAging,
          dd_BatchNo,
          dd_DocumentItemNo,
          dd_DocumentNo,
          dd_MovementType,
          dd_ValuationType,
          iag.dim_Companyid,
          Dim_ConsumptionTypeid,
          dim_costcenterid,
          dim_Currencyid,
          Dim_DateIdLastChangedPrice,
          Dim_DateIdPlannedPrice2,
          Dim_DocumentStatusid,
          Dim_DocumentTypeid,
          Dim_IncoTermid,
          Dim_ItemCategoryid,
          Dim_ItemStatusid,
          dim_LastReceivedDateid,
          Dim_MovementIndicatorid,
          dim_Partid,
          dim_Plantid,
	  Dim_ProfitCenterId,
          dim_producthierarchyid,
          Dim_PurchaseGroupid,
          Dim_PurchaseMiscid,
          Dim_PurchaseOrgid,
          dim_specialstockid,
          dim_stockcategoryid,
          dim_StockTypeid,
          dim_StorageLocationid,
          dim_StorageLocEntryDateid,
          Dim_SupplyingPlantId,
          Dim_Termid,
          Dim_UnitOfMeasureid,
          dim_Vendorid,
          ANSIDATE(LOCAL_TIMESTAMP) SnapshotDate,
          ifnull(
             (SELECT dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = ANSIDATE(LOCAL_TIMESTAMP)
                     AND dt.companycode = dc.companycode),
             1),
          (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryhistory') + row_number() over (),
	  amt_ExchangeRate_GBL
     FROM    facT_inventoryaging iag
          INNER JOIN
             dim_Company dc
          ON iag.dim_companyid = dc.dim_Companyid;

UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_inventoryhistoryid), 0) FROM fact_inventoryhistory)
 WHERE table_name = 'fact_inventoryhistory';

SELECT 'A2 - Done with populating fact_inventoryhistory. Now start with the updates',
       TIMESTAMP(LOCAL_TIMESTAMP);

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'processinglog', ifnull(max(processinglogid), 0) FROM processinglog;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          date(LOCAL_TIMESTAMP),
          'Start of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

delete from fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_StorageLocationid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_StorageLocationid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid IN (2, 3)
          AND SnapshotDate IN
                 (ANSIDATE(LOCAL_TIMESTAMP),
                  (ANSIDATE(LOCAL_TIMESTAMP) - 1),
                  (ANSIDATE(LOCAL_TIMESTAMP) - 7),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '1' MONTH),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_StorageLocationid,
            dim_stockcategoryid,
            SnapshotDate;

SELECT 'B - After Insert 1', TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE    fact_inventoryhistory_tmp ih1
FROM fact_inventoryhistory_tmp ih2
  SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock
WHERE ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
and ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
and ih1.SnapshotDate - (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND ih1.SnapshotDate - ( INTERVAL '7' DAY) = ih2.SnapshotDate;

call vectorwise(combine 'fact_inventoryhistory_tmp');
SELECT 'C - After Insert 2', TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE    fact_inventoryhistory_tmp ih1
FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1

FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;


call vectorwise (combine 'fact_inventoryhistory_tmp');


SELECT 'D - After Update 4', TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE    fact_inventoryhistory ih1

FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid in (2,3)
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP);

SELECT 'E - After Update 5', TIMESTAMP(LOCAL_TIMESTAMP);
call vectorwise (combine 'fact_inventoryhistory_tmp - fact_inventoryhistory_tmp');

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_Vendorid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_Vendorid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 4
          AND SnapshotDate IN
                 (ANSIDATE(LOCAL_TIMESTAMP),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '1' DAY),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '7' DAY),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '1' MONTH),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_Vendorid,
            dim_stockcategoryid,
            SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
FROM
          fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;

SELECT 'F - After Update 7', TIMESTAMP(LOCAL_TIMESTAMP);

call vectorwise (combine 'fact_inventoryhistory_tmp - fact_inventoryhistory_tmp');

UPDATE    fact_inventoryhistory_tmp ih1
FROM
          fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
FROM
          fact_inventoryhistory_tmp ih2
  SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
where  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
and ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
FROM
          fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid = 4
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP);

SELECT 'G - After Update 10', TIMESTAMP(LOCAL_TIMESTAMP);

call vectorwise (combine 'fact_inventoryhistory_tmp - fact_inventoryhistory_tmp');
INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)		  
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 5
          AND SnapshotDate IN
                 (ANSIDATE(LOCAL_TIMESTAMP),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '1' DAY),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '7' DAY),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '1' MONTH),
                  ANSIDATE(LOCAL_TIMESTAMP) - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_stockcategoryid,
            SnapshotDate;

SELECT 'G-1 - After Insert 3', TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE    fact_inventoryhistory_tmp ih1

FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
       AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;

SELECT 'G-2 - After Update 11', TIMESTAMP(LOCAL_TIMESTAMP);

 UPDATE    fact_inventoryhistory_tmp ih1

FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;

SELECT 'H - After Update 12', TIMESTAMP(LOCAL_TIMESTAMP);

call vectorwise (combine 'fact_inventoryhistory_tmp');
 UPDATE    fact_inventoryhistory_tmp ih1

FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1

FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
WHERE ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP)
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;

SELECT 'I - After Update 14', TIMESTAMP(LOCAL_TIMESTAMP);

call vectorwise (combine 'fact_inventoryhistory_tmp');
UPDATE    fact_inventoryhistory ih1

FROM fact_inventoryhistory_tmp ih2
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
WHERE ih1.dim_stockcategoryid = 5
AND ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.SnapshotDate = ANSIDATE(LOCAL_TIMESTAMP);

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          date(LOCAL_TIMESTAMP),
          'End of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

call vectorwise (combine 'fact_inventoryhistory_tmp - fact_inventoryhistory_tmp');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryhistory;

SELECT 'Z - END OF PROC bi_process_trends_inventoryhistory_fact - latest version as of 6th Feb ( Shanthis changes as per 5th Feb merged )',
       TIMESTAMP(LOCAL_TIMESTAMP);