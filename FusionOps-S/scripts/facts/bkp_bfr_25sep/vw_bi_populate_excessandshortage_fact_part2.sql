\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_excessandshortage;

drop table if exists mdtb_pi00;

Create table mdtb_pi00 as Select *,ifnull(MDTB_UMDAT, MDTB_DAT01) dateval_upd from mdtb
Where ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL ;

      UPDATE fact_excessandshortage m
FROM  plaf po,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_vendor fv,
       dim_consumptiontype ct,
       dim_part p,
       dim_plant pl,
       Dim_Date dd
   SET m.amt_UnitPrice =  ifnull((SELECT StandardPrice from tmp_getStdPrice z 
						where z.pCompanyCode = pl.CompanyCode
						AND z.pPlant = pl.PlantCode
						AND z.pMaterialNo =  MDKP_MATNR
						AND z.pFiYear = dd.CalendarYear
						AND z.pPeriod =  dd.FinancialMonthNumber
						AND z.fact_script_name = 'fact_excessandshortage'
						AND z.vUMREZ = PLAF_UMREZ
						AND z.vUMREN =   PLAF_UMREN
						AND z.PONumber IS NULL
						AND z.pUnitPrice =  0),0)
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND dd.Dim_Dateid <> 1
       AND m.dd_DocumentNo = po.PLAF_PLNUM
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND p.Dim_Partid = m.Dim_Partid
       AND pl.PlantCode = p.Plant
       AND v.VendorNumber = ifnull(PLAF_EMLIF, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(PLAF_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(po.PLAF_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND dd.DateValue = dateval_upd
       AND ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL
       AND dd.CompanyCode = pl.CompanyCode;

drop table if exists mdtb_pi00;

/* Do not split it single column updates */
UPDATE fact_excessandshortage m
FROM  fact_purchase p,
       dim_vendor v,
       dim_date dt
   SET m.Dim_ItemCategoryid = p.Dim_ItemCategoryid,
   m.Dim_Vendorid = p.Dim_Vendorid,
   m.Dim_DocumentTypeid = p.Dim_DocumentTypeid,
   m.amt_UnitPrice = ifnull((p.amt_DeliveryTotal / (case when p.ct_BaseUOMQty = 0 then null else p.ct_BaseUOMQty end )), 0),
   m.amt_UnitPrice_GBL = ifnull((p.amt_DeliveryTotal / (case when p.ct_BaseUOMQty = 0 then null else p.ct_BaseUOMQty end)), 0) * p.amt_ExchangeRate_GBL,
   ct_QtyOpenOrder = (ct_DeliveryQty - ct_ReceivedQty),
   m.dim_consumptiontypeid = p.Dim_ConsumptionTypeid
 WHERE     m.Dim_ActionStateid = 2
       AND dt.Dim_Dateid <> 1
       AND m.dd_DocumentNo = p.dd_DocumentNo
       AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
       AND v.Dim_Vendorid = p.Dim_Vendorid
       AND p.Dim_DateidDelivery = dt.Dim_Dateid;


UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET m.Dim_SalesDocumentTypeid = p.Dim_SalesDocumentTypeid
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_SalesDocumentTypeid <> p.Dim_SalesDocumentTypeid;



UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET      m.Dim_CustomerGroup1id = p.Dim_CustomerGroup1id
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_CustomerGroup1id <> p.Dim_CustomerGroup1id;



    UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET    m.Dim_CustomerID = p.Dim_CustomerID
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_CustomerID  <> p.Dim_CustomerID;



    UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET    m.Dim_SalesOrderRejectReasonid = p.Dim_SalesOrderRejectReasonid
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_SalesOrderRejectReasonid  <>  p.Dim_SalesOrderRejectReasonid;



    UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET    m.Dim_SalesOrderItemStatusid = p.Dim_SalesOrderItemStatusid
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_SalesOrderItemStatusid <> p.Dim_SalesOrderItemStatusid;



    UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET    m.Dim_SalesOrderHeaderStatusid = p.Dim_SalesOrderHeaderStatusid
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_SalesOrderHeaderStatusid <> p.Dim_SalesOrderHeaderStatusid;



    UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET    m.Dim_DocumentCategoryid = p.Dim_DocumentCategoryid
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_DocumentCategoryid <> p.Dim_DocumentCategoryid;



    UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET    m.amt_UnitPrice = ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end)
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.amt_UnitPrice  <> ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end);



    UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET    m.amt_UnitPrice_GBL = ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end) * p.amt_ExchangeRate_GBL
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.amt_UnitPrice_GBL <> ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end) * p.amt_ExchangeRate_GBL;



    UPDATE fact_excessandshortage m
FROM fact_salesorder p
SET    ct_QtyOpenOrder =
          CASE
             WHEN (ct_ScheduleQtySalesUnit - ct_DeliveredQty) < 0 THEN 0
             ELSE (ct_ScheduleQtySalesUnit - ct_DeliveredQty)
          END
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND ct_QtyOpenOrder <> CASE
             WHEN (ct_ScheduleQtySalesUnit - ct_DeliveredQty) < 0 THEN 0
             ELSE (ct_ScheduleQtySalesUnit - ct_DeliveredQty)
          END ;



UPDATE fact_excessandshortage m
FROM  fact_productionorder p
SET m.amt_UnitPrice = ifnull((p.amt_estimatedTotalCost/(case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0)
WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_ordernumber
       AND m.dd_DocumentItemNo = p.dd_orderitemno
	AND m.amt_UnitPrice  <> ifnull((p.amt_estimatedTotalCost/(case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0);
   

UPDATE fact_excessandshortage m
FROM  fact_productionorder p
SET m.amt_UnitPrice_GBL = ifnull((p.amt_estimatedTotalCost/(Case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0) * p.amt_ExchangeRate_GBL
WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_ordernumber
       AND m.dd_DocumentItemNo = p.dd_orderitemno
	AND m.amt_UnitPrice_GBL <>  ifnull((p.amt_estimatedTotalCost/(Case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0) * p.amt_ExchangeRate_GBL;


drop table if exists mdtb_pi00;

create table mdtb_pi00 as Select * from mdtb where ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL and MDTB_DAT02 IS NOT NULL;

UPDATE fact_excessandshortage m
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv
   SET m.Dim_ItemCategoryid = ic.Dim_ItemCategoryid
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.BANFN
       AND m.dd_DocumentItemNo = pr.BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.BSART, 'Not Set')
       AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND  m.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;
      

UPDATE fact_excessandshortage m
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv
   SET        m.Dim_Vendorid = v.Dim_Vendorid
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.BANFN
       AND m.dd_DocumentItemNo = pr.BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.BSART, 'Not Set')
       AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND m.Dim_Vendorid <> v.Dim_Vendorid;



       UPDATE fact_excessandshortage m
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv
   SET m.Dim_FixedVendorid = fv.Dim_Vendorid
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.BANFN
       AND m.dd_DocumentItemNo = pr.BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.BSART, 'Not Set')
       AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND  m.Dim_FixedVendorid  <> fv.Dim_Vendorid;



       UPDATE fact_excessandshortage m
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv
   SET m.Dim_DocumentTypeid = dt.Dim_DocumentTypeid
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.BANFN
       AND m.dd_DocumentItemNo = pr.BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.BSART, 'Not Set')
       AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND  m.Dim_DocumentTypeid <>  dt.Dim_DocumentTypeid;



       UPDATE fact_excessandshortage m
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv
   SET m.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.BANFN
       AND m.dd_DocumentItemNo = pr.BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.BSART, 'Not Set')
       AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND  m.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;



       UPDATE fact_excessandshortage m
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv
   SET m.amt_UnitPrice = ifnull(PREIS / ifnull((case when PEINH = 0 then null else PEINH end), 1), 0)
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.BANFN
       AND m.dd_DocumentItemNo = pr.BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.BSART, 'Not Set')
       AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND m.amt_UnitPrice  <> ifnull(PREIS / ifnull((case when PEINH = 0 then null else PEINH end), 1), 0);


       UPDATE fact_excessandshortage m
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv
   SET m.amt_UnitPrice_GBL = ifnull(PREIS / ifnull((case when PEINH = 0 then null else PEINH end), 1), 0) * 
(SELECT exchangeRate from tmp_getExchangeRate1
where pFromCurrency = pr.EBAN_WAERS
and pToCurrency = ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD' )
and pFromExchangeRate is null
and pDate = pr.BADAT
 and fact_script_name = 'bi_populate_excessandshortage_fact')

 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.BANFN
       AND m.dd_DocumentItemNo = pr.BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.BSART, 'Not Set')
       AND dt.Category = ifnull(pr.BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL;



UPDATE fact_excessandshortage m
From  mbew b,
       dim_plant p,
       dim_part dp
   SET m.amt_UnitPrice = (b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end))
 WHERE     m.Dim_ActionStateid = 2
       AND dp.Dim_Partid = m.Dim_Partid
       AND b.MATNR = dp.PartNumber
       AND p.Dim_Plantid = m.Dim_Plantid
       AND b.BWKEY = p.ValuationArea
       AND b.BWTAR IS NULL
       AND b.MBEW_LBKUM > 0
       AND ifnull(m.amt_UnitPrice, 0) = 0
	AND m.amt_UnitPrice <> (b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end));

Drop table if exists pGlobalCurrency_po_41;
Drop table if exists mdtb_pi00;
