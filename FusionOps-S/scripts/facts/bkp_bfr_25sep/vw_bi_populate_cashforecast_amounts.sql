
/**************************************************************************************************************/
/*   Script         : vw_bi_custom_cashforecast_amounts */
/*   Author         : Shanthi */
/*   Created On     : 14 Aug 2013 */
/*   Description    : Stored Proc vw_bi_custom_columbia_accountsreceivable migration from MySQL to Vectorwise syntax */
/*********************************************Change History*******************************************************/
/*   Date            By        Version           Desc 															  */
/*   14 Aug 2013      Shanthi    1.0               Existing code migrated to Vectorwise 							  */
/******************************************************************************************************************/

/*********************************************START****************************************************************/
call vectorwise(combine 'fact_exposure');

update fact_exposure f
set amt_DailyCashForecastBeyond = 0
WHERE amt_DailyCashForecastBeyond IS NULL
OR amt_DailyCashForecastBeyond <> 0;

update fact_exposure f
set amt_DailyCashForecastCurrMth = 0
WHERE amt_DailyCashForecastCurrMth IS NULL
OR amt_DailyCashForecastCurrMth <> 0;

update fact_exposure f
set amt_DailyCashForeCastNextMth = 0
WHERE amt_DailyCashForeCastNextMth IS NULL
OR amt_DailyCashForeCastNextMth <> 0;

update fact_exposure f
set amt_DailyCashForeCastPrevMth = 0
WHERE amt_DailyCashForeCastPrevMth IS NULL
OR amt_DailyCashForeCastPrevMth <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth9 = 0
WHERE amt_DailyCashForecastMonth9 IS NULL
OR amt_DailyCashForecastMonth9 <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth8 = 0
WHERE amt_DailyCashForecastMonth8 IS NULL
OR amt_DailyCashForecastMonth8 <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth7 = 0
WHERE amt_DailyCashForecastMonth7 IS NULL
OR amt_DailyCashForecastMonth7 <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth6 = 0
WHERE amt_DailyCashForecastMonth6 IS NULL
OR amt_DailyCashForecastMonth6 <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth5 = 0
WHERE amt_DailyCashForecastMonth5 IS NULL
OR amt_DailyCashForecastMonth5 <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth4 = 0
WHERE amt_DailyCashForecastMonth4 IS NULL
OR amt_DailyCashForecastMonth4 <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth12 = 0
WHERE amt_DailyCashForecastMonth12 IS NULL
OR amt_DailyCashForecastMonth12 <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth11 = 0
WHERE amt_DailyCashForecastMonth11 IS NULL
OR amt_DailyCashForecastMonth11 <> 0;

update fact_exposure f
set amt_DailyCashForecastMonth10 = 0
WHERE amt_DailyCashForecastMonth10 IS NULL
OR amt_DailyCashForecastMonth10 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek1 = 0
where f.amt_dailycashforecastweek1 is null
 OR f.amt_dailycashforecastweek1 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek2 = 0
where f.amt_dailycashforecastweek2 is null
 OR f.amt_dailycashforecastweek2 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek3 = 0
where f.amt_dailycashforecastweek3 is null
 OR f.amt_dailycashforecastweek3 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek4 = 0
where f.amt_dailycashforecastweek4 is null
 OR f.amt_dailycashforecastweek4 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek5 = 0
where f.amt_dailycashforecastweek5 is null
 OR f.amt_dailycashforecastweek5 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek6 = 0
where f.amt_dailycashforecastweek6 is null
 OR f.amt_dailycashforecastweek6 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek7 = 0
where f.amt_dailycashforecastweek7 is null
 OR f.amt_dailycashforecastweek7 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek8 = 0
where f.amt_dailycashforecastweek8 is null
 OR f.amt_dailycashforecastweek8 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek9 = 0
where f.amt_dailycashforecastweek9 is null
 OR f.amt_dailycashforecastweek9 <> 0;

update fact_exposure f
set f.amt_dailycashforecastweek10 = 0
where f.amt_dailycashforecastweek10 is null
 OR f.amt_dailycashforecastweek10 <> 0;

 update fact_exposure f
set f.amt_dailycashforecastweek11 = 0
where f.amt_dailycashforecastweek11 is null
 OR f.amt_dailycashforecastweek11 <> 0;

 update fact_exposure f
set f.amt_dailycashforecastweek12 = 0
where f.amt_dailycashforecastweek12 is null
 OR f.amt_dailycashforecastweek12 <> 0;

call vectorwise(combine 'fact_exposure');

 CREATE TABLE fact_ar_exposure_amt
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate,f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt 
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
    inner join dim_date agi on agi.dim_dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and (current_date - ndd.datevalue) between 0 and 29
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId,f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForeCastPrevMth = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;
  
call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;

  /* # Max exposure current month	*/

   CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between current_date and ( current_date + INTERVAL '28' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;  

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastCurrMth = t.ExpAmt
  WHERE f.Dim_CustomerId = t.Dim_CustomerId 
  and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;
  
  DROP TABLE fact_ar_exposure_amt;

   CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '29' DAY) and ( current_date + INTERVAL '56' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;
  
  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForeCastNextMth = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '57' DAY) and ( current_date + INTERVAL '84' DAY)  
 group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;
  
  update fact_exposure f
  FROM fact_ar_exposure_amt t 
    set f.amt_DailyCashForecastBeyond = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;


  DROP TABLE fact_ar_exposure_amt;

/*  # Max exposure beyond next month	*/
/* LK: Incorrect comment. This is 'max exposure within next week' */

CREATE TABLE fact_ar_exposure_amt
  AS	
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between current_date and (current_date + INTERVAL '6' DAY)
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;
  
  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek1 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '7' DAY) AND ( current_date + INTERVAL '13' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;  

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek2 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;
 
  DROP TABLE fact_ar_exposure_amt;

call vectorwise(combine 'fact_exposure');

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '14' DAY) AND ( current_date + INTERVAL '20' DAY) 
group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;    

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set  f.amt_dailycashforecastweek3 = t.ExpAmt 
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '21' DAY) AND ( current_date + INTERVAL '27' DAY) 
group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;    

  update fact_exposure f
  FROM  fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek4 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;
  
  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '28' DAY) AND ( current_date + INTERVAL '34' DAY) 
group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;   
  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek5 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;
  
 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '35' DAY) AND ( current_date + INTERVAL '41' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;  

  update fact_exposure f
  FROM  fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek6 = t.ExpAmt
  WHERE f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;
  
 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '42' DAY) AND ( current_date + INTERVAL '48' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;  

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek7 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;
  
call vectorwise(combine 'fact_exposure');

 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '49' DAY) AND ( current_date + INTERVAL '55' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;  

  update fact_exposure f
  FROM   fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek8 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;

 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '56' DAY) AND ( current_date + INTERVAL '62' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber, f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM  fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek9 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

call vectorwise(combine 'fact_exposure');
  
  DROP TABLE fact_ar_exposure_amt;

 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '63' DAY) AND ( current_date + INTERVAL '69' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek10 = t.ExpAmt
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;
  
 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '70' DAY) AND ( current_date + INTERVAL '76' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM  fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek11 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;
 
 CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '77' DAY) AND ( current_date + INTERVAL '83' DAY) 
   group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_dailycashforecastweek12 = t.ExpAmt  
  WHERE  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
   and ndd.DateValue between (current_date + INTERVAL '85' DAY) AND ( current_date + INTERVAL '113' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth4 = t.ExpAmt
  where  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;
 
  DROP TABLE fact_ar_exposure_amt;

/*   # Max exposure beyond next month	*/

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '114' DAY) AND ( current_date + INTERVAL '142' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth5 = t.ExpAmt
  where  f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;
  
  DROP TABLE fact_ar_exposure_amt;
  
/*   # Max exposure beyond next month */
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '143' DAY) AND ( current_date + INTERVAL '171' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth6 = t.ExpAmt
  where f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

call vectorwise(combine 'fact_exposure');
  
  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '172' DAY) AND ( current_date + INTERVAL '200' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth7 = t.ExpAmt
  where f.Dim_CustomerId = t.Dim_CustomerId 
  and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;
  
  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '201' DAY) AND ( current_date + INTERVAL '229' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth8 = t.ExpAmt
  where  f.Dim_CustomerId = t.Dim_CustomerId 
  and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;
  
  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '230' DAY) AND ( current_date + INTERVAL '258' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth9 = t.ExpAmt
  where f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;

  CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '259' DAY) AND ( current_date + INTERVAL '287' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth10 = t.ExpAmt
  where f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;

CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '288' DAY) AND ( current_date + INTERVAL '316' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth11 = t.ExpAmt
  WHERE f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

  DROP TABLE fact_ar_exposure_amt;

CREATE TABLE fact_ar_exposure_amt 
  AS
  select f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate, SUM(f_ar0.amt_InLocalCurrency) ExpAmt
  from fact_exposure f_ar0 inner join dim_date ndd on ndd.dim_dateid = f_ar0.Dim_DateIdNetDueDate
  inner join dim_date agi on agi.dim_Dateid = f_ar0.dim_DateidActualGIDate
  where f_ar0.Dim_DateIdNetDueDate <> 1
  and ndd.DateValue between (current_date + INTERVAL '317' DAY) AND ( current_date + INTERVAL '345' DAY) 
  group by f_ar0.dd_AccountingDocNo,f_ar0.dd_AccountingDocItemNo,f_ar0.dd_AssignmentNumber,f_ar0.Dim_CustomerId,f_ar0.Dim_CompanyId, f_ar0.dim_DateidActualGIDate, f_ar0.Dim_DateIdNetDueDate;

  update fact_exposure f
  FROM fact_ar_exposure_amt t 
  set f.amt_DailyCashForecastMonth12 = t.ExpAmt
  WHERE f.Dim_CustomerId = t.Dim_CustomerId and f.Dim_DateIdNetDueDate = t.Dim_DateIdNetDueDate
  AND f.dim_DateidActualGIDate = t.dim_DateidActualGIDate
  AND f.Dim_CompanyId = t.Dim_CompanyId
  AND f.dd_AccountingDocNo = t.dd_AccountingDocNo
  AND f.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
  AND f.dd_AssignmentNumber = t.dd_AssignmentNumber;

call vectorwise(combine 'fact_exposure');

  DROP TABLE fact_ar_exposure_amt;