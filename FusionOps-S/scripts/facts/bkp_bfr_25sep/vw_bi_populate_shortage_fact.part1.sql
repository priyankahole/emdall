

/* Start of vw_bi_populate_shortage_fact.part1 */
/* LK: This has been completely validated against kla on 17 Jun */

drop table if exists fact_shortage_temp;
create table fact_shortage_temp as select * from fact_shortage where 1 = 2;

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_shortage_temp';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_shortage_temp', ifnull(max(fact_shortageid), 0) FROM fact_shortage_temp;

INSERT INTO fact_shortage_temp(fact_shortageid,
ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_OrderItemNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateIdLatestReqDate,
                          Dim_Unitofmeasureid,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_ComponentId,
                          Dim_ComponentPlantId,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_CompSLocId,
                          Dim_SupplyAreaId,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateidRequirement,
                          Dim_DateIdReservRequirement,
                          dd_ReservationNo,
                          dd_ReservationItemNo,
                          Dim_ProductionOrderStatusId,
                          Dim_ProfitCenterId)
   SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over (),
   cast(0.00 as decimal(19,4)) ct_QtyActual,
       ifnull(RESB_BDMNG,0) ct_QtyRequired,
       cast(0.00 as decimal(19,4)) ct_QtyReservation,
       (CASE
           WHEN (r.RESB_BDMNG - r.RESB_ENMNG) > 0
           THEN
              r.RESB_BDMNG - r.RESB_ENMNG
           ELSE
              0
        END)
          ct_QtyShortage,
       ifnull(RESB_ENMNG,0) ct_QtyWithdrawn,
       ifnull((CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END),0) ct_QtyWithDrawnUOM ,
       ifnull((CASE WHEN (( r.RESB_ERFMG > (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) AND RESB_KZEAR IS NULL AND (r.RESB_KZKUP IS NULL OR pm.DeliveryComplete = 'Not Set'))
           THEN (r.RESB_ERFMG - (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) ELSE 0 END),0) ct_OpenQty,
       ifnull((CASE WHEN (r.RESB_VMENG = r.RESB_BDMNG) THEN r.RESB_ERFMG ELSE
          (r.RESB_VMENG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))) END),0) ct_ConfirmedQty ,
       ifnull(
          (SELECT (m.MARD_LABST + m.MARD_SPEME)
             FROM MARD m
            WHERE     m.MARD_WERKS = r.RESB_WERKS
                  AND m.MARD_MATNR = r.RESB_MATNR
                  AND m.MARD_LGORT = r.RESB_LGORT),
          0)
          ct_OnHandQty,
       ifnull(
          (SELECT (m.MARD_LABST + m.MARD_SPEME)
             FROM MARD m
            WHERE     m.MARD_WERKS = r.RESB_WERKS
                  AND m.MARD_MATNR = r.RESB_MATNR
                  AND m.MARD_LGORT = r.RESB_LGORT),
          0)
          ct_StockQty,
       po.dd_SequenceNo dd_ProductionSequenceNo,
       cast(0 as int) dd_DocumentItemNo,
       'Not Set' dd_DocumentNo,
       po.dd_OrderNumber dd_OrderNo,
       po.dd_OrderItemNo dd_OrderItemNo,
       cast(0 as int) dd_ScheduleNo,
       ifnull(r.RESB_BAUGR, 'Not Set') dd_PeggedRequirement ,
       ifnull(r.RESB_POSNR, 'Not Set') dd_BomItemNo,
       ifnull(
             (SELECT dt.Dim_DateId
                FROM dim_date dt, dim_plant pl
               WHERE dt.DateValue = RESB_SBTER
                     AND dt.CompanyCode = pl.CompanyCode
                     AND pl.PlantCode = RESB_WERKS
                     AND pl.RowIsCurrent = 1),
             1) Dim_DateIdLatestReqDate,
       ifnull((SELECT Dim_UnitOfMeasureid
                    FROM Dim_UnitOfMeasure uom
                   WHERE uom.UOM = r.RESB_MEINS), 1) Dim_UnitOfMeasureId,
       ifnull((SELECT Dim_EffectiveForMatPlanningId
                      FROM Dim_EffectiveForMatPlanning emp
                      WHERE emp.EffectiveForMatPlanning = r.RESB_NO_DISP),1) ,
       cast(1 as int) Dim_DocumentTypeid,
       ifnull((SELECT dim_bomitemcategoryid
                 FROM dim_bomitemcategory
                WHERE ItemCategory = RESB_POSTP AND RowIsCurrent = 1),
              1)
          Dim_ItemCategoryid,
       cast(1 as int) Dim_MrpControllerid,
       cast(1 as int) Dim_MRPElementid,
       ifnull(
             (SELECT dim_partid
                FROM dim_part
               WHERE     PartNumber = RESB_MATNR
                     AND Plant = RESB_WERKS
                     AND RowIsCurrent = 1),
             1) Dim_Componentid ,
       ifnull(
             (SELECT pl.dim_plantid
                FROM dim_plant pl
               WHERE    pl.PlantCode = RESB_WERKS
                     AND pl.RowIsCurrent = 1),
             1) Dim_ComponentPlantId,
       po.Dim_PartIdItem Dim_Partid,
       po.Dim_Plantid Dim_Plantid,
       po.Dim_StorageLocationId  Dim_StorageLocationid,
       ifnull(
          (SELECT dim_StorageLocationid
             FROM dim_StorageLocation
            WHERE     Plant = RESB_WERKS
                  AND LocationCode = RESB_LGORT
                  AND RowIsCurrent = 1),
          1)
          Dim_CompSLocId,
       ifnull(
          (SELECT Dim_SupplyAreaid
             FROM Dim_SupplyArea
            WHERE     Plant = RESB_WERKS
                  AND SupplyArea = RESB_PRVBE
                  AND RowIsCurrent = 1),
          1)
          Dim_SupplyAreaid,
       ifnull(
          (SELECT dv.dim_Vendorid
             FROM dim_Vendor dv, EORD
            WHERE     dv.vendornumber = EORD_LIFNR
                  AND dv.RowIsCurrent = 1
                  AND EORD_MATNR = r.RESB_MATNR
                  AND EORD_WERKS = r.RESB_WERKS
                  AND EORD_FLIFN = 'X'
                  AND EORD_BDATU > current_date),
          1)
          Dim_Vendorid,
       cast(0 as int) dd_ControlCycleItemNo,
       'Not Set' dd_ControlCycleNo,
       ifnull(
          (SELECT dim_movementtypeid
             FROM dim_movementtype mt
            WHERE mt.MovementType = r.RESB_BWART
                  AND mt.ConsumptionIndicator =
                         ifnull(r.RESB_KZVBR, 'Not Set')
                  AND mt.MovementIndicator = 'Not Set'
                  AND mt.ReceiptIndicator = 'Not Set'
                  AND mt.SpecialStockIndicator =
                         ifnull(r.RESB_SOBKZ, 'Not Set')
                  AND RowIsCurrent = 1),
          1)
          Dim_MovementTypeId,
       po.Dim_DateIdBasicStart Dim_DateIdRequirement,
       ifnull(
         (SELECT dt.Dim_DateId
            FROM dim_date dt, dim_plant pl
           WHERE dt.DateValue = RESB_BDTER
                 AND dt.CompanyCode = pl.CompanyCode
                 AND pl.PlantCode = RESB_WERKS
                 AND pl.RowIsCurrent = 1),
         1),
       r.RESB_RSNUM,
       r.RESB_RSPOS,
       po.Dim_ProductionOrderStatusId dim_productionorderstatusid,
       po.Dim_ProfitCenterId Dim_ProfitCenterId
  FROM fact_productionorder po,
       dim_productionordermisc pm,
       resb r
 WHERE     r.RESB_AUFNR = po.dd_OrderNumber
       AND po.Dim_ProductionOrderMiscId = pm.Dim_ProductionOrderMiscId
       AND r.RESB_POSTP = 'L'
       AND r.RESB_BWART = '261'
       AND r.RESB_XLOEK IS NULL
       AND NOT EXISTS
                  (SELECT 1
                     FROM fact_shortage_temp s
                    WHERE     s.dd_OrderNo = po.dd_OrderNumber
                          AND s.dd_OrderItemNo = po.dd_OrderItemNo
                          AND s.dd_ReservationNo = r.RESB_RSNUM
                          AND s.dd_ReservationItemNo = r.RESB_RSPOS);

UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_shortageid), 0) FROM fact_shortage_temp)
 WHERE table_name = 'fact_shortage_temp';


DROP TABLE IF EXISTS fact_shortage_temp_1;
CREATE TABLE fact_shortage_temp_1
AS
   SELECT PKPS_PKIMG ct_QtyActual,
          cast(0.00 as decimal(19,4)) ct_QtyRequired,
          PKPS_PKBMG ct_QtyReservation,
          (CASE WHEN PKPS_PKIMG = 0 THEN (PKPS_PKBMG - PKPS_PKIMG) ELSE 0 END) ct_QtyShortage,
          cast(0.00 as decimal(19,4)) ct_QtyWithdrawn,
          cast(0.00 as decimal(19,4)) ct_QtyWithdrawnUOM,
          cast(0.00 as decimal(19,4)) ct_OpenQty,
          cast(0.00 as decimal(19,4)) ct_ConfirmedQty,
          cast(0.00 as decimal(19,4)) ct_OnHandQty,
          cast(0.00 as decimal(19,4)) ct_StockQty,
          cast(0 as bigint) dd_ProductionSequenceNo,
          cast(0 as int) dd_DocumentItemNo,
          'Not Set' dd_DocumentNo,
          'Not Set' dd_OrderNo,
          cast(0 as int) dd_ScheduleNo,
          'Not Set' dd_PeggedRequirement,
          'Not Set' dd_BomItemNo,
          cast(1 as int) Dim_DateIdLatestReqDate,
          cast(1 as int) Dim_UnitOfMeasureId,
          cast(1 as int) Dim_EffectiveForMatPlanningId,
          cast(1 as int) Dim_DocumentTypeid,
          cast(1 as int) Dim_ItemCategoryid,
          cast(1 as int) Dim_MrpControllerid,
          cast(1 as int) Dim_MRPElementid,
          ifnull(
             (SELECT dim_partid
                FROM dim_part
               WHERE     PartNumber = PKHD_MATNR
                     AND Plant = PKHD_WERKS
                     AND RowIsCurrent = 1),
             1)
             Dim_Partid,
          p.Dim_Plantid,
          ifnull(
             (SELECT dim_StorageLocationid
                FROM dim_StorageLocation
               WHERE     Plant = PKHD_WERKS
                     AND LocationCode = PKHD_UMLGO
                     AND RowIsCurrent = 1),
             1)
             Dim_StorageLocationid,
          ifnull(
             (SELECT Dim_SupplyAreaid
                FROM Dim_SupplyArea
               WHERE     Plant = PKHD_WERKS
                     AND SupplyArea = PKHD_PRVBE
                     AND RowIsCurrent = 1),
             1) Dim_SupplyAreaid,
        cast(1 as int) Dim_Vendorid,
       PKPS_PKKEY dd_ControlCycleItemNo,
       PKHD_PKNUM dd_ControlCycleNo,
       cast(1 as int) Dim_MovementTypeId,
       cast(1 as int) Dim_DateIdRequirement
     FROM PKHD_PKPS dft, dim_plant p
 WHERE     dft.PKHD_WERKS = p.PlantCode
       AND p.RowIsCurrent = 1
   AND NOT EXISTS
                 (SELECT 1
                    FROM fact_shortage_temp st
                   WHERE st.dd_ControlCycleNo = dft.PKHD_PKNUM
                     AND st.dd_ControlCycleItemNo = dft.PKPS_PKKEY);

/* PKHD_PKSTF IS NULL */
UPDATE  fact_shortage_temp_1 t
FROM PKHD_PKPS dft, dim_plant p,dim_Vendor dv, EORD
SET Dim_Vendorid = dv.dim_Vendorid
WHERE t.dd_ControlCycleNo = dft.PKHD_PKNUM
AND t.dd_ControlCycleItemNo = dft.PKPS_PKKEY
AND PKHD_PKSTF IS NULL
AND      dv.vendornumber = EORD_LIFNR
AND dv.RowIsCurrent = 1
AND EORD_MATNR = dft.PKHD_MATNR
AND EORD_WERKS = dft.PKHD_WERKS
AND EORD_FLIFN = 'X'
AND EORD_BDATU > current_date;

/*  PKHD_PKSTF = '0002' */
UPDATE fact_shortage_temp_1 t
FROM PKHD_PKPS dft, dim_Vendor dv
SET Dim_Vendorid = dv.dim_Vendorid
WHERE t.dd_ControlCycleNo = dft.PKHD_PKNUM
AND t.dd_ControlCycleItemNo = dft.PKPS_PKKEY
AND PKHD_PKSTF = '0002'
AND dv.vendornumber = PKHD_LIFNR
AND dv.RowIsCurrent = 1;


INSERT INTO fact_shortage_temp(ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateIdLatestReqDate,
                          Dim_UnitOfMeasureId,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_SupplyAreaid,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateIdRequirement,fact_shortageid)
SELECT t.*,
(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over ()
FROM fact_shortage_temp_1 t;


UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_shortageid), 0) FROM fact_shortage_temp)
 WHERE table_name = 'fact_shortage_temp';

INSERT INTO fact_shortage_temp(ct_ScheduledQty,
                          ct_DeliveredQty,
                          ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateIdLatestReqDate,
                          Dim_UnitOfMeasureId,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateIdRequirement,fact_shortageid)
   SELECT EKET_MENGE ct_ScheduledQty,
          EKET_WEMNG ct_DeliveredQty,
          cast(0.00 as decimal(19,4)) ct_QtyActual,
          cast(0.00 as decimal(19,4)) ct_QtyRequired,
          cast(0.00 as decimal(19,4)) ct_QtyReservation,
          (CASE
              WHEN (EKET_MENGE - EKET_WEMNG) > 0
              THEN
                 EKET_MENGE - EKET_WEMNG
              ELSE
                 cast(0 as int)
           END) ct_QtyShortage,
          cast(0.00 as decimal(19,4)) ct_QtyWithdrawn,
          cast(0.00 as decimal(19,4)) ct_QtyWithdrawnUOM,
          cast(0.00 as decimal(19,4)) ct_OpenQty,
          cast(0.00 as decimal(19,4)) ct_ConfirmedQty,
          ifnull(
             (SELECT (m.MARD_LABST + m.MARD_SPEME)
                FROM MARD m
               WHERE m.MARD_MATNR = sto.EKPO_MATNR
                     AND m.MARD_WERKS = sto.EKPO_WERKS
                     AND m.MARD_LGORT = sto.EKPO_LGORT),
             0) ct_OnHandQty,
          ifnull(
             (SELECT m.MARD_LABST
                FROM MARD m
               WHERE m.MARD_MATNR = sto.EKPO_MATNR
                     AND m.MARD_WERKS = sto.EKPO_WERKS
                     AND m.MARD_LGORT = sto.EKPO_LGORT),
             0)  ct_StockQty,
          cast(0 as bigint) dd_ProductionSequenceNo,
          EKPO_EBELP dd_DocumentItemNo,
          EKPO_EBELN dd_DocumentNo,
          'Not Set' dd_OrderNo,
          EKET_ETENR dd_ScheduleNo,
          'Not Set' dd_PeggedRequirement,
          'Not Set' dd_BomItemNo,
          cast(1 as int) Dim_DateIdLatestReqDate,
          cast(1 as int) Dim_UnitOfMeasureId,
          cast(1 as int) Dim_EffectiveForMatPlanningId,
          ifnull(
             (SELECT Dim_DocumentTypeid
                FROM Dim_DocumentType dt
               WHERE     dt.Type = sto.EKKO_BSART
                     AND dt.Category = sto.EKKO_BSTYP
                     AND dt.RowIsCurrent = 1),
             1) Dim_DocumentTypeid,
          cast(1 as int) Dim_ItemCategoryid,
          cast(1 as int) Dim_MrpControllerid,
          cast(1 as int) Dim_MRPElementid,
          ifnull(
             (SELECT dim_partid
                FROM dim_part
               WHERE     PartNumber = EKPO_MATNR
                     AND Plant = EKPO_WERKS
                     AND RowIsCurrent = 1),
             1)
             Dim_Partid,
          p.Dim_Plantid,
          ifnull(
             (SELECT dim_StorageLocationid
                FROM dim_StorageLocation
               WHERE     Plant = EKPO_WERKS
                     AND LocationCode = EKPO_LGORT
                     AND RowIsCurrent = 1),
             1)
             Dim_StorageLocationid,
        ifnull(( SELECT dv.dim_Vendorid
                  FROM dim_Vendor dv, EORD
                 WHERE     dv.vendornumber = EORD_LIFNR
                       AND dv.RowIsCurrent = 1
                       AND EORD_MATNR = EKPO_MATNR
                       AND EORD_WERKS = EKPO_WERKS
                       AND EORD_FLIFN = 'X'
                       AND EORD_BDATU > current_date
                       AND EORD_EBELN = EKPO_EBELN
                       AND EORD_EBELP = EKPO_EBELP),1) Dim_Vendorid,
       cast(0 as int) dd_ControlCycleItemNo,
       'Not Set' dd_ControlCycleNo,
       cast(1 as int) Dim_MovementTypeId,
       cast(1 as int) Dim_DateIdRequirement,(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over ()
     FROM ekko_Ekpo_eket sto, Dim_Plant p
 WHERE     p.PlantCode = sto.EKPO_WERKS
       AND p.RowIsCurrent = 1
       AND sto.EKKO_BSART IN ('NB','UB','ZNB','ZUB','ZUBA')
       AND  NOT EXISTS
                 (SELECT 1
                    FROM fact_shortage_temp st
                   WHERE st.dd_DocumentNo = sto.EKPO_EBELN
                     AND st.dd_DocumentItemNo = sto.EKPO_EBELP
                     AND st.dd_ScheduleNo = sto.EKET_ETENR);

UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_shortageid), 0) FROM fact_shortage_temp)
 WHERE table_name = 'fact_shortage_temp';


INSERT INTO fact_shortage_temp(fact_shortageid,
ct_ScheduledQty,
                          ct_DeliveredQty,
                          ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_ReservationNo,
                          dd_ReservationItemNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateidLatestReqDate,
                          Dim_UnitOfMeasureId,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_SupplyAreaId,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateIdRequirement,
                          Dim_DateIdReservRequirement,
                          Dim_ComponentId,
                          Dim_ComponentPlantId,
                          Dim_CompSLocId
                          )
   SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over (),
   cast(0.00 as decimal(19,4)) ct_ScheduledQty,
          cast(0.00 as decimal(19,4)) ct_DeliveredQty,
          cast(0.00 as decimal(19,4)) ct_QtyActual,
          r.RESB_BDMNG ct_QtyRequired,
          cast(0.00 as decimal(19,4)) ct_QtyReservation,
          ( CASE WHEN (r.RESB_BDMNG - r.RESB_ENMNG) > 0 THEN (r.RESB_BDMNG - r.RESB_ENMNG) ELSE 0 END ) ct_QtyShortage,
          r.RESB_ENMNG ct_QtyWithdrawn,
                  ifnull((CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END),0) ct_QtyWithdrawnUOM,
          --0 ct_OpenQty,
                   ifnull((CASE WHEN (( r.RESB_ERFMG > (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) AND RESB_KZEAR IS NULL AND (r.RESB_KZKUP IS NULL))
           THEN (r.RESB_ERFMG - (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) ELSE 0 END), 0) ct_OpenQty,

          ifnull((CASE WHEN (r.RESB_VMENG = r.RESB_BDMNG) THEN r.RESB_ERFMG ELSE
          (r.RESB_VMENG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))) END),0) ct_ConfirmedQty,
          (m.MARD_LABST + m.MARD_SPEME) ct_OnHandQty,
          m.MARD_LABST  ct_StockQty,
          cast(0 as bigint) dd_ProductionSequenceNo,
          r.RESB_RSNUM dd_ReservationNo,
          r.RESB_RSPOS dd_ReservationItemNo,
          cast(0 as int) dd_DocumentItemNo,
          'Not Set' dd_DocumentNo,
          'Not Set' dd_OrderNo,
          cast(0 as int) dd_ScheduleNo,
          ifnull(r.RESB_BAUGR, 'Not Set') dd_PeggedRequirement,
          ifnull(r.RESB_POSNR, 'Not Set') dd_BomItemNo,
          ifnull(
             (SELECT dt.Dim_DateId
                FROM dim_date dt
               WHERE dt.DateValue = RESB_SBTER
                     AND dt.CompanyCode = p.CompanyCode),
             1) Dim_DateIdLatestReqDate,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM Dim_UnitOfMeasure uom
                   WHERE uom.UOM = r.RESB_MEINS), 1) Dim_UnitOfMeasureId,
          ifnull((SELECT emp.Dim_EffectiveForMatPlanningId
                      FROM Dim_EffectiveForMatPlanning emp
                      WHERE emp.EffectiveForMatPlanning = r.RESB_NO_DISP),1) Dim_EffectiveForMatPlanningId,
          cast(1 as int) Dim_DocumentTypeid,
          ifnull ((SELECT dim_bomitemcategoryid FROM dim_bomitemcategory WHERE ItemCategory = r.RESB_POSTP AND RowIsCurrent = 1), 1) Dim_ItemCategoryid,
          cast(1 as int) Dim_MrpControllerid,
          cast(1 as int) Dim_MRPElementid,
          ifnull(
             (SELECT dim_partid
                FROM dim_part
               WHERE     PartNumber = r.RESB_MATNR
                     AND Plant = r.RESB_WERKS
                     AND RowIsCurrent = 1),
             1)
             Dim_Partid,
          p.Dim_Plantid,
          ifnull(
             (SELECT dim_StorageLocationid
                FROM dim_StorageLocation
               WHERE     Plant = r.RESB_WERKS
                     AND LocationCode = r.RESB_LGORT
                     AND RowIsCurrent = 1),
             1)
             Dim_StorageLocationid,
          ifnull(
             (SELECT Dim_SupplyAreaid
                FROM Dim_SupplyArea
               WHERE     Plant = RESB_WERKS
                     AND SupplyArea = RESB_PRVBE
                     AND RowIsCurrent = 1),
             1) Dim_SupplyAreaid,
        ifnull(( SELECT dv.dim_Vendorid
                  FROM dim_Vendor dv, EORD
                 WHERE     dv.vendornumber = EORD_LIFNR
                       AND dv.RowIsCurrent = 1
                       AND EORD_MATNR = r.RESB_MATNR
                       AND EORD_WERKS = r.RESB_WERKS
                       AND EORD_FLIFN = 'X'
                       AND EORD_BDATU > current_date),1) Dim_Vendorid,
       cast(0 as int) dd_ControlCycleItemNo,
       'Not Set' dd_ControlCycleNo,
       ifnull((SELECT dim_movementtypeid
                    FROM dim_movementtype  mt
                   WHERE mt.MovementType = r.RESB_BWART
                        AND mt.ConsumptionIndicator = r.RESB_KZVBR
                        AND mt.MovementIndicator = 'Not Set'
                        AND mt.ReceiptIndicator = 'Not Set'
                        AND mt.SpecialStockIndicator = r.RESB_SOBKZ
                        AND RowIsCurrent = 1),
                 1) Dim_MovementTypeId,
       cast(1 as int) Dim_DateIdRequirement,
       ifnull(
         (SELECT dt.Dim_DateId
            FROM dim_date dt
           WHERE dt.DateValue = RESB_BDTER
                 AND dt.CompanyCode = p.CompanyCode),
         1) Dim_DateIdReservRequirement,
       cast(1 as int) Dim_ComponentId,
       cast(1 as int) Dim_ComponentPlantId,
       cast(1 as int) Dim_CompSLocId
     FROM RESB r, MARD m, dim_Plant p
 WHERE     r.RESB_XLOEK IS NULL
       AND r.RESB_BWART = '201'
       AND r.RESB_MATNR = m.MARD_MATNR
       AND r.RESB_WERKS = m.MARD_WERKS
       AND r.RESB_LGORT = m.MARD_LGORT
       AND p.PlantCode = m.MARD_WERKS
       AND p.RowIsCurrent = 1
          AND NOT EXISTS
                 (SELECT 1
                    FROM fact_shortage_temp st
                   WHERE st.dd_ReservationNo = r.RESB_RSNUM
                     AND st.dd_ReservationItemNo = r.RESB_RSPOS);

UPDATE fact_shortage_temp s from resb r, dim_shortagemisc sm
   SET s.dim_shortagemiscid = sm.dim_shortagemiscid
 WHERE     s.dd_ReservationNo <> 0
       AND s.dd_ReservationItemNo <> 0
       AND r.RESB_RSNUM = s.dd_ReservationNo
       AND r.RESB_RSPOS = s.dd_ReservationItemNo
       AND sm.BulkMaterial = ifnull(r.RESB_SCHGT, 'Not Set')
       AND sm.CoProduct = ifnull(r.RESB_KZKUP, 'Not Set')
       AND sm.FinalIssue = ifnull(r.RESB_KZEAR, 'Not Set')
       AND sm.MissingPart = ifnull(r.RESB_XFEHL, 'Not Set')
       AND sm.PhantomItem = ifnull(r.RESB_DUMPS, 'Not Set');


UPDATE fact_shortage_temp fs
   SET fs.amt_StdPrice = 0.0
 WHERE fs.dd_ControlCycleNo <> 'Not Set' AND fs.dd_ControlCycleNo IS NOT NULL;

