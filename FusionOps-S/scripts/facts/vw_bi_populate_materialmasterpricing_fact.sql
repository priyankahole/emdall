/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : George */
/*   Created On     : 30 Dec 2013 */
/*   Description    : vw_bi_populate_materialmasterpricing_fact Script VW syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version	 Desc                                                            */
/*                                          */

/******************************************************************************************************************/

DELETE FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_materialmasterpricing';

INSERT INTO NUMBER_FOUNTAIN(table_name,max_id)
SELECT 'fact_materialmasterpricing', ifnull(MAX(fact_materialmasterpricingid),1) FROM fact_materialmasterpricing; 

DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

drop table if exists tmp_KONH_KONP_A004_001;
create table tmp_KONH_KONP_A004_001 as 
select  KONP_KBETR,
        A004_MATNR,
		A004_VKORG,
		A004_VTWEG,
		KONH_KNUMH,
		A004_KSCHL,
		A004_KNUMH,
		A004_DATAB,
		A004_DATBI,
		KONP_KONWA
		/*,row_number() over(PARTITION BY A004_MATNR, A004_VKORG, A004_VTWEG ORDER BY A004_DATAB desc) A004_ROWNUM*/
from KONH_KONP inner join A004 on A004_KNUMH = KONH_KNUMH
WHERE A004_KSCHL IN ('ZMRP', 'Z3AP', 'ZMAP', 'ZEPR');

drop table if exists tmp_dp_sales_001;
create table tmp_dp_sales_001 as
select p.Dim_PartSalesid, 
	p.PartNumber, 
	n.A004_KSCHL dd_ConditionType,
	ifnull(n.KONP_KBETR,0) dd_ConditionAmt,
	ifnull(n.A004_KNUMH,'Not Set') dd_ConditionRecNo,
	n.A004_DATAB ConditionValidFrom,
	n.A004_DATBI ConditionValidTo,
	co.Dim_Companyid, 
	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = n.KONP_KONWA),1) Dim_CurrencyId,
	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = co.Currency),1) Dim_CurrencyId_TRA,
	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = pGlobalCurrency),1) Dim_CurrencyId_GBL,
        1 amt_Exchangerate,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = co.Currency and z.fact_script_name = 'bi_populate_materialmasterpricing_fact' 
		and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)
		amt_Exchangerate_GBL,
	so.CompanyCode
from dim_partsales p 
	inner join dim_salesorg so on p.SalesOrgCode = so.SalesOrgCode
	inner join dim_company co on so.CompanyCode = co.CompanyCode
	inner join tmp_KONH_KONP_A004_001 n on (p.PartNumber = n.A004_MATNR 
					and p.SalesOrgCode = n.A004_VKORG 
					and p.DistributionChannelCode = n.A004_VTWEG),
	tmp_GlobalCurr_001;
					
drop table if exists tmp_dp_sales_002;
create table tmp_dp_sales_002 as
select dp.Dim_Partid,
       pl.Dim_Plantid,
	   t.*
from tmp_dp_sales_001 t 
    inner join dim_part dp on t.PartNumber = dp.PartNumber
	inner join dim_plant pl on dp.plant = pl.PlantCode;

modify fact_materialmasterpricing to truncated;

INSERT INTO fact_materialmasterpricing		  (Dim_Partid,
												  Dim_PartSalesid,
												  Dim_CompanyId,
												  Dim_PlantId,
												  dd_ConditionType,
												  amt_ConditionAmt,
												  dd_ConditionAmt,
												  Dim_Dateid_CondValidFrom,
												  Dim_Dateid_CondValidTo,
												  dd_ConditionRecNo,
												  Dim_CurrencyId,
												  Dim_CurrencyId_GBL,
												  Dim_CurrencyId_TRA,
												  amt_ExchangeRate,
												  amt_ExchangeRate_gbl,
												  fact_materialmasterpricingid)
			  
SELECT ts.Dim_Partid,
       ts.Dim_PartSalesid,
	   ts.Dim_CompanyId,
	   ts.Dim_PlantId,
	   ifnull(ts.dd_ConditionType,'Not Set') dd_ConditionType,
	   0 amt_ConditionAmt,
	   ifnull(ts.dd_ConditionAmt,0) dd_ConditionAmt,
	   ifnull((select dt.Dim_Dateid from Dim_Date dt where dt.CompanyCode = ts.CompanyCode 
									   and dt.DateValue = ts.ConditionValidFrom),1) Dim_Dateid_CondValidFrom,
	   ifnull((select dt.Dim_Dateid from Dim_Date dt where dt.CompanyCode = ts.CompanyCode 
									   and dt.DateValue = ts.ConditionValidTo),1) Dim_Dateid_CondValidTo,
	   ifnull(ts.dd_ConditionRecNo,'Not Set') dd_ConditionRecNo,
	   ts.Dim_CurrencyId,
	   ts.Dim_CurrencyId_GBL,
	   ts.Dim_CurrencyId_TRA,
	   ts.amt_ExchangeRate,
	   ts.amt_ExchangeRate_gbl,
	   row_number() over()
	   /*(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_materialmasterpricingid') + row_number() over() */

from tmp_dp_sales_002 ts;
	
drop table if exists tmp_dp_sales_001;
drop table if exists tmp_dp_sales_002;
drop table if exists tmp_KONH_KONP_A004_001;

CALL VECTORWISE(combine 'fact_materialmasterpricing');