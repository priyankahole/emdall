
delete from number_fountain m where m.table_name = 'fact_replenishmenttracking';

insert into number_fountain
select 	'fact_replenishmenttracking_temp',
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)
from (select 1) t;

drop table if exists fact_replenishmenttracking_temp;
create table fact_replenishmenttracking_temp as			   
select (select ifnull(m.max_id, 0) from number_fountain m where m.table_name = 'fact_replenishmenttracking_temp')
          + row_number() over() as fact_replenishmenttrackingid,
          'Shipment' dd_recordtype,
          f.*
from fact_salesorderdelivery f
where dd_afsstockcategory in ('V1','V1R');

call vectorwise(combine 'fact_replenishmenttracking_temp');

alter table fact_replenishmenttracking_temp add column 	ct_resultantforecastqty decimal(18,4) not null default '0.0000';
alter table fact_replenishmenttracking_temp add column 	amt_netbaseprcprice decimal(18,4) not null default '0.0000';
alter table fact_replenishmenttracking_temp add column 	dd_brandalphacode varchar(10) not null default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_brandnumericcode varchar(10) not null default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_productlinecode varchar(10) not null default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_productlinedesc varchar(100) not null default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_materialfamily_columbia varchar(18) default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_afscolor varchar(7) default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_afsrddyear integer not null default 0;
alter table fact_replenishmenttracking_temp add column 	dd_afsrddmonth integer not null default 0;
alter table fact_replenishmenttracking_temp add column 	dd_afsrddmonthname varchar(20) not null default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_customerparent varchar(10) not null default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_customersoldto varchar(10) not null default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dim_materialfamilyid i8 not null default 1;
alter table fact_replenishmenttracking_temp add column 	dd_productlinerollup varchar(20) not null default 'Not Set' collate ucs_basic;
alter table fact_replenishmenttracking_temp add column 	dd_forecasttimestamp timestamp(6) without time zone not null default CURRENT_TIMESTAMP;
alter table fact_replenishmenttracking_temp add column  dim_customeridparent i8 not null default 1;
alter table fact_replenishmenttracking_temp add column  dim_materialtypeid i8 not null default 1;
alter table fact_replenishmenttracking_temp add column  dd_romanticcolordescription varchar(60) not null default 'Not Set' collate ucs_basic;

alter table fact_replenishmenttracking_temp rename column fact_salesorderdeliveryid to dd_factsalesorderdeliveryid ; 

call vectorwise(combine 'fact_replenishmenttracking_temp');

delete from number_fountain m where m.table_name = 'fact_replenishmenttracking_temp';

insert into number_fountain
select 	'fact_replenishmenttracking_temp',
	ifnull(max(f.fact_replenishmenttrackingid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_replenishmenttracking_temp f;

insert into fact_replenishmenttracking_temp
(
		fact_replenishmenttrackingid,
		dd_factsalesorderdeliveryid,
		dd_recordtype,
		dd_afsstockcategory,
		dd_seasoncode_columbia,
		dd_materialfamily_columbia,
		dd_afscolor,
		dd_customersoldto,
		dd_afsrddmonthname,
		dd_afsrddmonth,
		dd_afsrddyear,
		ct_resultantforecastqty,	
		amt_netbaseprcprice,
		dd_brandalphacode,
		dd_brandnumericcode,
		dd_productlinecode,
		dd_productlinedesc,
		dd_productlinerollup,
		dd_forecasttimestamp
)		   
select  (select ifnull(m.max_id, 0) from number_fountain m where m.table_name = 'fact_replenishmenttracking_temp')
          + row_number() over(),
	  	0 as dd_factsalesorderdeliveryid,
		'Forecast' as dd_recordtype,
		concat('V1',r.lgty_rgn_cd) as dd_afsstockcategory,
		r.seas_cd as dd_seasoncode_columbia,
		r.mtrl_fam_nbr as dd_materialfamily_columbia,
		r.colr_nbr as dd_afscolor,
		r.sold_to_cust_nbr as dd_customersoldto,
		Case 
			When r.fcst_mth_nbr = 1 then 'Jan' 
			When r.fcst_mth_nbr = 2 then 'Feb' 
			When r.fcst_mth_nbr = 3 then 'Mar' 
			When r.fcst_mth_nbr = 4 then 'Apr' 
			When r.fcst_mth_nbr = 5 then 'May' 
			When r.fcst_mth_nbr = 6 then 'Jun' 
			When r.fcst_mth_nbr = 7 then 'Jul'
			When r.fcst_mth_nbr = 8 then 'Aug'
			When r.fcst_mth_nbr = 9 then 'Sep'
			When r.fcst_mth_nbr = 10 then 'Oct'
			When r.fcst_mth_nbr = 11 then 'Nov' 
			When r.fcst_mth_nbr = 12 then 'Dec' 
			else 'Not Set' 
		End as dd_afsrddmonthname,
		r.fcst_mth_nbr as dd_afsrddmonth,
		r.fcst_yr_nbr as  dd_afsrddyear,
		r.rslt_fcst_qty as ct_resultantforecastqty,	
		r.net_base_prc_amt as amt_netbaseprcprice,
		r.brnd_alpha_cd as dd_brandalphacode,
		r.brnd_num_cd as dd_brandnumericcode,
		r.prod_lin_cd as dd_productlinecode,
		r.prod_lin_desc as dd_productlinedesc,
		CASE 
			WHEN r.prod_lin_desc = 'Not Set' THEN 'Not Set'
			WHEN r.prod_lin_desc = 'Footwear' THEN 'Footwear' 
			ELSE 'Apparel' 
		END as dd_productlinerollup,
		r.edw_crt_ts
from replenishment_forecast_logility r;

delete from number_fountain m where m.table_name = 'fact_replenishmenttracking';					

call vectorwise(combine 'fact_replenishmenttracking_temp');


/* Update dim_materialfamilyid for Both Shipment and Forecast */
update fact_replenishmenttracking_temp f
from  dim_materialfamily mf
set f.dim_materialfamilyid = mf.dim_materialfamilyid
where f.dd_recordtype = 'Forecast' and
      f.dd_materialfamily_columbia = mf.materialfamily and
      f.dim_materialfamilyid <> mf.dim_materialfamilyid;

update fact_replenishmenttracking_temp f
from  dim_materialtype mt
set f.dim_materialtypeid = mt.dim_materialtypeid
where f.dd_recordtype = 'Forecast' and
      mt.materialtype = 'ZFRT' and
      f.dim_materialtypeid <> mt.dim_materialtypeid;
	  
update fact_replenishmenttracking_temp f
from  dim_materialfamily mf, dim_part p
set f.dim_materialfamilyid = mf.dim_materialfamilyid
where f.dd_recordtype = 'Shipment' and
      f.dim_partid = p.dim_partid and
      p.materialfamily_columbia = mf.materialfamily and
      f.dim_materialfamilyid <> mf.dim_materialfamilyid;

update fact_replenishmenttracking_temp f
from  dim_materialtype mt, dim_part p
set f.dim_materialtypeid = mt.dim_materialtypeid
where f.dd_recordtype = 'Shipment' and
      f.dim_partid = p.dim_partid and
      p.parttype = mt.materialtype and
      f.dim_materialtypeid <> mt.dim_materialtypeid;
      
/* Update dim_customeridsoldto for Forecast */
update fact_replenishmenttracking_temp f
from dim_customer dc
set f.dim_customeridsoldto = dc.dim_customerid
where dd_recordtype = 'Forecast' 
and TRIM(LEADING '0' FROM dc.customernumber)  = f.dd_customersoldto
and f.dim_customeridsoldto <> dc.dim_customerid;



/* Update dd columns for Shipment */
update fact_replenishmenttracking_temp f
from dim_customer dc
set f.dd_customersoldto = TRIM(LEADING '0' FROM dc.customernumber)
where dd_recordtype = 'Shipment' 
and f.dim_customeridsoldto = dc.dim_customerid
and f.dd_customersoldto <> TRIM(LEADING '0' FROM dc.customernumber);

update fact_replenishmenttracking_temp f
from dim_date dt
set f.dd_afsrddyear = dt.calendaryear
where f.Dim_DateIdAfsReqDelivery = dt.dim_dateid 
and f.dd_recordtype = 'Shipment'
and f.dd_afsrddyear <> dt.calendaryear;

update fact_replenishmenttracking_temp f
from dim_date dt
set f.dd_afsrddmonth = dt.calendarmonthnumber
where f.Dim_DateIdAfsReqDelivery = dt.dim_dateid 
and f.dd_recordtype = 'Shipment'
and f.dd_afsrddmonth <> dt.calendarmonthnumber;

call vectorwise(combine 'fact_replenishmenttracking_temp');

update fact_replenishmenttracking_temp f
from dim_date dt
set f.dd_afsrddmonthname = dt.monthabbreviation
where f.Dim_DateIdAfsReqDelivery = dt.dim_dateid 
and f.dd_recordtype = 'Shipment'
and f.dd_afsrddmonthname <> dt.monthabbreviation;

update fact_replenishmenttracking_temp f
from dim_part p
set f.dd_materialfamily_columbia = p.materialfamily_columbia
where f.dim_partid = p.dim_partid 
and f.dd_recordtype = 'Shipment'
and f.dd_materialfamily_columbia <> p.materialfamily_columbia;

update fact_replenishmenttracking_temp f
from dim_part p
set f.dd_afscolor = p.afscolor
where f.dim_partid = p.dim_partid 
and f.dd_recordtype = 'Shipment'
and f.dd_afscolor <> p.afscolor;

/* Update dd_customerparent */
update fact_replenishmenttracking_temp f
from dim_customer dc, dim_customerpartnerfunctions pf
set f.dd_customerparent = case when TRIM(LEADING '0' FROM pf.customernumberbusinesspartner) is null 
				then TRIM(LEADING '0' FROM dc.customernumber)
				else TRIM(LEADING '0' FROM pf.customernumberbusinesspartner) end
where f.dim_customeridsoldto = dc.dim_customerid
and dc.customernumber = pf.customernumber1
and pf.partnerfunction = 'ZH'
and f.dd_customerparent <> case when TRIM(LEADING '0' FROM pf.customernumberbusinesspartner) is null then TRIM(LEADING '0' FROM dc.customernumber)
				else TRIM(LEADING '0' FROM pf.customernumberbusinesspartner) end;

update fact_replenishmenttracking_temp f
from dim_customer dc
set f.dd_customerparent = TRIM(LEADING '0' FROM dc.customernumber)
where f.dim_customeridsoldto = dc.dim_customerid
and not exists
(select 1 
from dim_customerpartnerfunctions pf 
where dc.customernumber = pf.customernumber1 and pf.partnerfunction = 'ZH')
and f.dd_customerparent <> TRIM(LEADING '0' FROM dc.customernumber);

/* Update dim_customeridparent */
update fact_replenishmenttracking_temp f
from dim_customer dc
set f.dim_customeridparent = dc.dim_customerid
where f.dd_customerparent = TRIM(LEADING '0' FROM dc.customernumber)
and f.dim_customeridparent <> dc.dim_customerid;


update fact_replenishmenttracking_temp f
from dim_part p
set f.dd_brandnumericcode = p.division
where f.dim_partid = p.dim_partid 
and f.dd_recordtype = 'Shipment'
and f.dd_brandnumericcode <> p.division;

update fact_replenishmenttracking_temp f
from dim_part p
set f.dd_brandalphacode = p.divisiondescription
where f.dim_partid = p.dim_partid 
and f.dd_recordtype = 'Shipment'
and f.dd_brandalphacode <> p.divisiondescription;


update fact_replenishmenttracking_temp f
from dim_part p, dim_producthierarchy ph
set f.dd_productlinecode = ph.level1code
where f.dim_partid = p.dim_partid 
and p.producthierarchy = ph.producthierarchy
and f.dd_recordtype = 'Shipment'
and f.dd_productlinecode <> ph.level1code;

update fact_replenishmenttracking_temp f
from dim_part p, dim_producthierarchy ph
set f.dd_productlinedesc = ph.level1desc
where f.dim_partid = p.dim_partid 
and p.producthierarchy = ph.producthierarchy
and f.dd_recordtype = 'Shipment'
and f.dd_productlinedesc <> ph.level1desc;

update fact_replenishmenttracking_temp f
from dim_part p, dim_producthierarchy ph
set f.dd_productlinerollup = CASE
WHEN ph.level1desc = 'Not Set' THEN 'Not Set'
WHEN ph.level1desc = 'Footwear' THEN 'Footwear' 
ELSE 'Apparel' 
END
where f.dim_partid = p.dim_partid 
and p.producthierarchy = ph.producthierarchy
and f.dd_recordtype = 'Shipment'
and f.dd_productlinerollup <> CASE 
WHEN ph.level1desc = 'Not Set' THEN 'Not Set'
WHEN ph.level1desc = 'Footwear' THEN 'Footwear' 
ELSE 'Apparel' 
END;

							 
/* For Exchange rate related fields*/
Update fact_replenishmenttracking_temp
set amt_exchangerate_stat = 1
where dd_recordtype = 'Forecast';

Update fact_replenishmenttracking_temp
set amt_ExchangeRate_GBL = 1
where dd_recordtype = 'Forecast';

Update fact_replenishmenttracking_temp
set amt_exchangerate = 1
where dd_recordtype = 'Forecast';

/* Explicitly Setting field values for Forecast Data*/
UPDATE fact_replenishmenttracking_temp f
FROM dim_salesdocumenttype d
SET f.dim_salesdocumenttypeid =  d.dim_salesdocumenttypeid
WHERE f.dd_Recordtype = 'Forecast' 
AND f.dim_salesdocumenttypeid = 1
AND lower(d.description) like '%replenishment order%';

/* Set Forecast Data to US Sales Org 1001 */
UPDATE fact_replenishmenttracking_temp f
FROM dim_salesorg d
SET f.dim_salesorgid = ifnull(d.dim_salesorgid,1)
WHERE f.dd_Recordtype = 'Forecast'
AND d.salesorgcode = '1001'
AND f.dim_salesorgid <> ifnull(d.dim_salesorgid,1);

UPDATE fact_replenishmenttracking_temp f
FROM   dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_customer cm
SET f.dim_Customermastersalesid = ifnull(cms.dim_Customermastersalesid,1)
WHERE f.dd_Recordtype = 'Forecast'
AND f.dim_salesorgid = sorg.dim_salesorgid
       AND cm.dim_customerid = f.dim_customeridsoldto
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND cm.customernumber = cms.CustomerNumber
AND f.dim_Customermastersalesid <> ifnull(cms.dim_Customermastersalesid,1);

UPDATE fact_replenishmenttracking_temp f
FROM dim_part d
SET f.dd_romanticcolordescription = ifnull(d.romanticcolordescription_columbia,'Not Set')
WHERE f.dd_Recordtype = 'Shipment'
AND f.dim_partid = d.dim_partid
AND d.afscolor = f.dd_afscolor
AND f.dd_romanticcolordescription <> ifnull(d.romanticcolordescription_columbia,'Not Set');

UPDATE fact_replenishmenttracking_temp f
FROM dim_part d
SET f.dd_romanticcolordescription = ifnull(d.romanticcolordescription_columbia,'Not Set')
WHERE f.dd_Recordtype = 'Forecast'
AND d.afscolor = f.dd_afscolor
AND f.dd_romanticcolordescription <> ifnull(d.romanticcolordescription_columbia,'Not Set');


call vectorwise(combine 'fact_replenishmenttracking_temp');

drop table if exists fact_replenishmenttracking;
rename table fact_replenishmenttracking_temp to fact_replenishmenttracking;

/* Create a copy of the Staging table*/
drop table if exists replenishment_forecast_logility_LatestSnapshot;
create table replenishment_forecast_logility_LatestSnapshot
as
select * From replenishment_forecast_logility;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_replenishmenttracking;