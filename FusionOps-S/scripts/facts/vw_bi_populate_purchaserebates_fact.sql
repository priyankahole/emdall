
/* 
#################################################################################################################
Script         : vw_bi_populate_purchaserebates_fact.sql
Author         : SVinayan 
Created On     : 22 Apr 2015 
Description    : 
*********************************************Change History**************************************************
Date		By		Version		Desc                      
22Apr2015	Suchithra	1.0		New Script Created 
11May2015	Suchithra	1.1		Removed fact_purchase from the Updates for PO Doc and Item Numebrs
14May2015	Suchithra	2.0		Main Insert Updated with outer joins
15May2015	Suchithra	3.0		Main Insert Updated to full outer join b/w Postings and DFL table
						New field : Latest Claim Number	
16May2015	Suchithra	3.1		New fields : KZWI1 & BUDAT from IPPRITM table	
28May2015	Suchithra	3.2		Excluded SA from the logic to calculate Rebate Amt Paid					
14Jun2015	Suchithra	3.3		New fields BUDATe, BPMNG
16Jun2015	Suchithra	3.4		Additional Update for rebate agreement dimension
18Jun2015	Suchithra	3.5		Update dim_rebateagreements ID for SalesRebate related DeploymentCodes
#################################################################################################################
*/

/* Update to existing Data */

/* IPPRHDR */
update fact_purchaserebates f
from irm_ipprhdr h
set	
	f.dd_priptype  = 	ifnull(h.irm_ipprhdr_iptyp,'Not Set'),
	f.dd_prvendor  = 	ifnull(h.irm_ipprhdr_recip,'Not Set'),
	f.dd_prcreatetimstamp = ifnull(h.irm_ipprhdr_erzet,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
where f.dd_pripdocno =  h.irm_ipprhdr_ipnum;

/* IPPRITM */
update fact_purchaserebates f
from irm_ippritm h
set
	amt_pramount  = 	ifnull(irm_ippritm_netwr,'0.0000'),
	dd_pragreementno  = 	ifnull(irm_ippritm_knuma_ag,'Not Set'),
	dd_part  = 	ifnull(irm_ippritm_matnr,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
where f.dd_prdocno  = 	irm_ippritm_ipnum
and f.dd_prdocitmno  = 	irm_ippritm_ipitm;	

/*14Jun2015 - New field */
UPDATE fact_purchaserebates f
from irm_ippritm 
SET f.ct_POPriceUnitQty = ifnull(irm_ippritm_BPMNG,'0.0000'),
	dw_update_date = CURRENT_TIMESTAMP
where f.dd_prdocno  =   irm_ippritm_ipnum
and f.dd_prdocitmno  =  irm_ippritm_ipitm
and f.ct_POPriceUnitQty <> ifnull(irm_ippritm_BPMNG,'0.0000'); 

/* IPPRDFL */
update fact_purchaserebates f
from irm_ipprdfl d
Set
	dd_precedingcategory  = 	ifnull(irm_ipprdfl_vbtyp_v,'Not Set')
	,dd_parkdocumentfiscalyear  = 	ifnull(irm_ipprdfl_gjahr,0000)
	,dd_referancetransaction  = 	ifnull(irm_ipprdfl_awtyp,'Not Set')
	,dd_parkdoctime = 	ifnull(irm_ipprdfl_erzet,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
where f.dd_pripdocno =  d.irm_ipprdfl_ipnum
and f.dd_prdocitmno	= d.irm_ipprdfl_ipitm
and f.dd_subsequentdoccategory  = 	d.irm_ipprdfl_vbtyp_n
and f.dd_parkdocumentno = d.irm_ipprdfl_vbeln;

/*GCRHDR*/
update fact_purchaserebates
from irm_gcrhdr
set
	dd_claimchangename  = 	ifnull(irm_gcrhdr_aenam,'Not Set')
	,dd_salesdoctype  = 	ifnull(irm_gcrhdr_auart,'Not Set')
	,dd_claimpostingstatus  = 	ifnull(irm_gcrhdr_bstat,'Not Set')
	,dd_cancellation  = 	ifnull(irm_gcrhdr_cancd,'Not Set')
	,dd_vendor  = 	ifnull(irm_gcrhdr_clmvn,'Not Set')
	,dd_postingblock  = 	ifnull(irm_gcrhdr_crpbk,'Not Set')
	,dd_claimheaderstage  = 	ifnull(irm_gcrhdr_crstage,'Not Set')
	,dd_claimtype  = 	ifnull(irm_gcrhdr_crtyp,'Not Set')
	,dd_claimcreatedby  = 	ifnull(irm_gcrhdr_ernam,'Not Set')
	-- ,dd_userstatus  = 	ifnull(irm_gcrhdr_frprd,'Not Set')
	,dd_paymentreference  = 	ifnull(irm_gcrhdr_kidno,'Not Set')
	,amt_submittedamount  = 	ifnull(irm_gcrhdr_subamt,'0.0000')
	,dd_submittedcurency  = 	ifnull(irm_gcrhdr_subcur,'Not Set')
	,dd_endvalidtimespan  = 	ifnull(irm_gcrhdr_toprd,0)
	,dd_claimtrackingno  = 	ifnull(irm_gcrhdr_tracknum,'Not Set')
	,dd_currentuserstatus  = 	ifnull(irm_gcrhdr_ustat,'Not Set')
	,dd_claimdocnoforrefdoc  = 	ifnull(irm_gcrhdr_vgbel,'Not Set')
	,dd_claimrefdocno  = 	ifnull(irm_gcrhdr_xblnr,'Not Set')
	,dd_claimtimestamp = 	ifnull(irm_gcrhdr_aezet,'Not Set')
	,dd_claimcreatetimestamp = 	ifnull(irm_gcrhdr_erzet,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
where dd_claimnumber  = 	irm_gcrhdr_vbeln;


/*IRM_IPPRAPS*/
update fact_purchaserebates 
from irm_ippraps
set
	dd_reforgunits  = 	ifnull(irm_ippraps_aworg,'Not Set')
	,dd_refdocno  = 	ifnull(irm_ippraps_awref,'Not Set')
	,dd_reftransactioncd  = 	ifnull(irm_ippraps_awtyp,'Not Set')
	,dd_companycode  = 	ifnull(irm_ippraps_bukrs,'Not Set')
	,dd_purchrebateevaluationfromperiod  = 	ifnull(irm_ippraps_efprd,'Not Set')
	,dd_purchrebatecreatedby  = 	ifnull(irm_ippraps_ernam,'Not Set')
	,dd_fiscalyear  = 	ifnull(irm_ippraps_gjahr,0000)
	,dd_lowlvlparticipantpost  = 	ifnull(irm_ippraps_lppost,'Not Set')
	,amt_netvalue  = 	ifnull(irm_ippraps_netwr,'0.0000')
	,dd_calculationrun  = 	ifnull(irm_ippraps_pcrnum,'Not Set')
	,dd_evaluationtoperiod  = 	ifnull(irm_ippraps_perid,'Not Set')
	,dd_reasoncodeforpayments  = 	ifnull(irm_ippraps_rstgr,'Not Set')
	,dd_settlementadjustmentcode  = 	ifnull(irm_ippraps_sacod,'Not Set')
	,dd_settlementpartner  = 	ifnull(irm_ippraps_sktonr,'Not Set')
	,dd_settlementpartnertype  = 	ifnull(irm_ippraps_snrart,'Not Set')
	,dd_currencykey  = 	ifnull(irm_ippraps_waers,'Not Set')
	,amt_purchaserebateamt  = 	ifnull(irm_ippraps_wrbtr,'0.0000')
	,dd_purchrebaterecordtimestamp = 	ifnull(irm_ippraps_erzet,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
where dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_agreementno = irm_ippraps_knuma_ag
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n;

/* Insert New Unique Rows into the Fact */
delete from NUMBER_FOUNTAIN where table_name = 'fact_purchaserebates';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_purchaserebates',ifnull(max(fact_purchaserebatesid),0)
FROM fact_purchaserebates;

/* Insert new fields into the fact table */
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_purchaserebates',ifnull(max(fact_purchaserebatesid),0)
FROM fact_purchaserebates;

insert into fact_purchaserebates(	
	fact_purchaserebatesid,
	dd_deploymentcode,
	dd_participationtype,
	dd_participationnumber,
	dd_accrualdocno,
	dd_subsequentdocumentcategory,
	dd_reforgunits ,
	dd_refdocno ,
	dd_reftransactioncd ,
	dd_companycode ,
	dd_purchrebateevaluationfromperiod ,
	dd_purchrebatecreatedby ,
	dd_fiscalyear ,
	dd_agreementno ,
	dd_lowlvlparticipantpost ,
	amt_netvalue ,
	dd_calculationrun ,
	dd_evaluationtoperiod ,
	dd_reasoncodeforpayments ,
	dd_settlementadjustmentcode ,
	dd_settlementpartner ,
	dd_settlementpartnertype ,
	dd_currencykey ,
	amt_purchaserebateamt ,
	dd_purchrebaterecordtimestamp,
	

	/*IRM_IPPRDFL*/
	dd_chargebackdocumentnumber,
	dd_parkdocumentno,
	dd_precedingcategory ,
	dd_subsequentdoccategory ,
	dd_parkdocumentfiscalyear ,
	dd_referancetransaction ,
	dd_parkdoctime,


	/*IRM_IPPRHDR*/
	dd_pripdocno,
	dd_priptype ,
	dd_prvendor ,
	dd_prcreatetimstamp,

	/*IRM_IPPRITM,*/
	dd_prdocno ,
	dd_prdocitmno ,
	amt_pramount ,
	dd_pragreementno ,
	dd_part ,
	amt_lineamount,
	ct_POPriceUnitQty,


	/*IRM_IPPRASP*/
	dim_rebateagreementsid,


	/*GCRHDR*/
	dd_claimnumber ,
	dd_claimchangename ,
	dd_salesdoctype ,
	dd_claimpostingstatus ,
	dd_cancellation ,
	dd_vendor ,
	dd_postingblock ,
	dd_claimheaderstage ,
	dd_claimtype ,
	dd_claimcreatedby ,
	-- dd_userstatus ,
	dd_paymentreference ,
	amt_submittedamount ,
	dd_submittedcurency ,
	dd_endvalidtimespan ,
	dd_claimtrackingno ,
	dd_currentuserstatus ,
	dd_claimdocnoforrefdoc ,
	dd_claimrefdocno ,
	dd_claimtimestamp,
	dd_claimcreatetimestamp

)
SELECT
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_purchaserebates') + row_number() over() as fact_purchaserebatesid,
/* IRM_IPPRAPS */
	ifnull(irm_ippraps_depcode,'Not Set'),
	ifnull(irm_ippraps_nrart,'Not Set'),
	ifnull(irm_ippraps_panum,'Not Set'),
	ifnull(irm_ippraps_vbeln,'Not Set'),
	ifnull(irm_ippraps_vbtyp_n,'Not Set'),
	ifnull(irm_ippraps_aworg,'Not Set'),
	ifnull(irm_ippraps_awref,'Not Set'),
	ifnull(irm_ippraps_awtyp,'Not Set'),
	ifnull(irm_ippraps_bukrs,'Not Set'),
	ifnull(irm_ippraps_efprd,'Not Set'),
	ifnull(irm_ippraps_ernam,'Not Set'),
	ifnull(irm_ippraps_gjahr,0),
	ifnull(irm_ippraps_knuma_ag,'Not Set'),
	ifnull(irm_ippraps_lppost,'Not Set'),
	ifnull(irm_ippraps_netwr,'0.0000'),
	ifnull(irm_ippraps_pcrnum,'Not Set'),
	ifnull(irm_ippraps_perid,'Not Set'),
	ifnull(irm_ippraps_rstgr,'Not Set'),
	ifnull(irm_ippraps_sacod,'Not Set'),
	ifnull(irm_ippraps_sktonr,'Not Set'),
	ifnull(irm_ippraps_snrart,'Not Set'),
	ifnull(irm_ippraps_waers,'Not Set'),
	ifnull(irm_ippraps_wrbtr,'0.0000'),
	ifnull(irm_ippraps_erzet,'Not Set'),
	 
/*IRM_IPPRDFL*/
	ifnull(irm_ipprdfl_ipnum,'Not Set'),
	ifnull(irm_ipprdfl_vbeln,'Not Set'),
	ifnull(irm_ipprdfl_vbtyp_v,'Not Set'),
	ifnull(irm_ipprdfl_vbtyp_n,'Not Set'),
	ifnull(irm_ipprdfl_gjahr,0),
	ifnull(irm_ipprdfl_awtyp,'Not Set'),
	ifnull(irm_ipprdfl_erzet,'Not Set'),
	
/*IRM_IPPRHDR*/
	ifnull(irm_ipprhdr_ipnum,'Not Set'),
	ifnull(irm_ipprhdr_iptyp,'Not Set'),
	ifnull(irm_ipprhdr_recip,'Not Set'),
	ifnull(irm_ipprhdr_erzet,'Not Set'),
	
/*IRM_IPPRITM*/
	ifnull(irm_ippritm_ipnum,'Not Set'),
	ifnull(irm_ippritm_ipitm,0),
	ifnull(irm_ippritm_netwr,'0.0000'),
	ifnull(irm_ippritm_knuma_ag,'Not Set'),
	ifnull(irm_ippritm_matnr,'Not Set'),
	ifnull(irm_ippritm_kzwi1,'0.0000'),
	ifnull(irm_ippritm_BPMNG,'0.0000'),

	
/*IRM_IPPRASP*/
	ifnull((select dim_rebateagreementsid from dim_rebateagreements d
			where i.irm_ippritm_knuma_ag = d.agreementnum
			and d.rebatebasis = 'PURC'),1),

	
	
/*IRM_GCRHDR*/
	ifnull(irm_gcrhdr_vbeln,'Not Set'),
	ifnull(irm_gcrhdr_aenam,'Not Set'),
	ifnull(irm_gcrhdr_auart,'Not Set'),
	ifnull(irm_gcrhdr_bstat,'Not Set'),
	ifnull(irm_gcrhdr_cancd,'Not Set'),
	ifnull(irm_gcrhdr_clmvn,'Not Set'),
	ifnull(irm_gcrhdr_crpbk,'Not Set'),
	ifnull(irm_gcrhdr_crstage,'Not Set'),
	ifnull(irm_gcrhdr_crtyp,'Not Set'),
	ifnull(irm_gcrhdr_ernam,'Not Set'),
	-- ifnull(irm_gcrhdr_frprd,'Not Set'),
	ifnull(irm_gcrhdr_kidno,'Not Set'),
	ifnull(irm_gcrhdr_subamt,'0.0000'),
	ifnull(irm_gcrhdr_subcur,'Not Set'),
	ifnull(irm_gcrhdr_toprd,0),
	ifnull(irm_gcrhdr_tracknum,'Not Set'),
	ifnull(irm_gcrhdr_ustat,'Not Set'),
	ifnull(irm_gcrhdr_vgbel,'Not Set'),
	ifnull(irm_gcrhdr_xblnr,'Not Set'),
	ifnull(irm_gcrhdr_aezet,'Not Set'),
	ifnull(irm_gcrhdr_erzet,'Not Set')
from irm_ippraps p
left join irm_ipprdfl d
	on irm_ipprdfl_vbeln = irm_ippraps_vbeln 
	and irm_ipprdfl_gjahr = irm_ippraps_gjahr
	and irm_ipprdfl_bukrs = irm_ippraps_bukrs /* ippraps, ipprdfl : Everything from APS evenif no matching recs in DFL */
	and irm_ipprdfl_vbtyp_v = 'IP'
full join irm_ippritm i /* ippritm, ipprdfl : Everything from PRITM even if no matching ipitm,ipnum combination in DFL */
	on irm_ippritm_ipnum  = irm_ipprdfl_ipnum  
	and irm_ippritm_ipitm = irm_ipprdfl_ipitm  
left join irm_ipprhdr h
	on irm_ipprhdr_ipnum = irm_ippritm_ipnum
left join irm_ipprasp a
	on irm_ipprasp_knuma_ag = irm_ippritm_knuma_ag
	and irm_ipprasp_objec = 'PURC'
left join irm_gcrhdr g
	on substr(irm_gcrhdr_tracknum,12) = irm_ippraps_panum
where  1 <> ifnull ((Select 1 from fact_purchaserebates 
			where  dd_participationnumber = ifnull(irm_ippraps_panum,'Not Set')
			and dd_accrualdocno = ifnull(irm_ippraps_vbeln,'Not Set')
			and dd_deploymentcode = ifnull(irm_ippraps_depcode,'Not Set')
			and dd_participationtype = ifnull(irm_ippraps_nrart,'Not Set')
			and dd_subsequentdocumentcategory = ifnull(irm_ippraps_vbtyp_n,'Not Set')
			and dd_agreementno = ifnull(irm_ippraps_knuma_ag,'Not Set')
			and dd_parkdocumentno = ifnull(irm_ipprdfl_vbeln,'Not Set')
			and dd_chargebackdocumentnumber = ifnull(irm_ipprdfl_ipnum,'Not Set')
			and dd_subsequentdoccategory = ifnull(irm_ipprdfl_vbtyp_n,'Not Set')
			and trim(dd_pripdocno)  =  ifnull(irm_ipprhdr_ipnum,'Not Set')
			and dd_prdocitmno = ifnull(irm_ippritm_ipitm,0)
			and dd_claimnumber = ifnull(irm_gcrhdr_vbeln,'Not Set')
			),0);
		
/*
from 
	irm_ippraps p 
	,irm_ipprdfl d
	,irm_ipprhdr h  
	,irm_ippritm i  
	,irm_gcrhdr g
	,irm_ipprasp a
where d.irm_ipprdfl_vbeln = p.irm_ippraps_vbeln 
and d.irm_ipprdfl_gjahr = p.irm_ippraps_gjahr
and d.irm_ipprdfl_bukrs = p.irm_ippraps_bukrs /* ippraps, ipprdfl 
and d.irm_ipprdfl_vbtyp_v = 'IP'
and d.irm_ipprdfl_ipnum = h.irm_ipprhdr_ipnum /* cbhdr				
and h.irm_ipprhdr_ipnum = i.irm_ippritm_ipnum 
and d.irm_ipprdfl_ipitm	= i.irm_ippritm_ipitm /* cbitm				
and p.irm_ippraps_panum = substr(g.irm_gcrhdr_tracknum,12) /* gcrhdr	
and i.irm_ippritm_knuma_ag = a.irm_ipprasp_knuma_ag 
and a.irm_ipprasp_objec = 'PURC'
and not exists (Select 1 from fact_purchaserebates 
					where  dd_participationnumber = irm_ippraps_panum
					and dd_accrualdocno = irm_ippraps_vbeln
					and dd_claimnumber = irm_gcrhdr_vbeln
					and dd_pripdocno =  irm_ipprdfl_ipnum
					and dd_prdocitmno	= irm_ipprdfl_ipitm
					and dd_deploymentcode = irm_ippraps_depcode
					and dd_participationtype = irm_ippraps_nrart
					and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n)
*/


/* Plant and Company Code */
UPDATE fact_purchaserebates f
FROM irm_ippritm i, dim_plant pl
SET f.dim_plantid = pl.dim_plantid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_prdocno = i.irm_ippritm_ipnum 
AND f.dd_prdocitmno = i.irm_ippritm_ipitm
AND i.irm_ippritm_werks = pl.plantcode
AND f.dim_plantid <> pl.dim_plantid;

/* Company code from ipprpaps table to be used to populate dimension id*/
UPDATE fact_purchaserebates f
FROM dim_company c
SET f.dim_companyid = c.dim_companyid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_companycode = c.companycode
and f.dim_companyid <> c.dim_companyid;

/* Update Purchase Doc and Line Item Nos */

/* iphdr ebelp is ekko ebeln and ipitm ebeln is ekpo ebeln 
Assumption - the populate script runs after purchasing is processed completely 
11May2015 Removing fact_purchase from updates as an issue was found of missing POs.*/


UPDATE fact_purchaserebates f
FROM irm_ipprhdr h, irm_ippritm i
SET dd_podocno = ifnull(h.irm_ipprhdr_ebeln,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_pripdocno = h.irm_ipprhdr_ipnum
and h.irm_ipprhdr_ipnum = i.irm_ippritm_ipnum
and f.dd_prdocitmno = i.irm_ippritm_ipitm
and ifnull(dd_podocno,'Not Set') <> ifnull(h.irm_ipprhdr_ebeln,'Not Set');

UPDATE fact_purchaserebates f
FROM irm_ipprhdr h, irm_ippritm i
SET dd_polineitem = ifnull(i.irm_ippritm_ebelp,0),
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_pripdocno = h.irm_ipprhdr_ipnum
and h.irm_ipprhdr_ipnum = i.irm_ippritm_ipnum
and f.dd_prdocitmno = i.irm_ippritm_ipitm
and ifnull(dd_polineitem,'Not Set') <> ifnull(i.irm_ippritm_ebelp,0);

UPDATE fact_purchaserebates f
FROM irm_ipprhdr h, irm_ippritm i, dim_part p
SET f.dim_partid = ifnull(p.dim_partid,1),
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_pripdocno = h.irm_ipprhdr_ipnum
and h.irm_ipprhdr_ipnum = i.irm_ippritm_ipnum
and f.dd_prdocitmno = i.irm_ippritm_ipitm
and i.irm_ippritm_matnr = f.dd_part
and dd_part = p.partnumber
and i.irm_ippritm_werks = p.plant
and f.dim_partid <> ifnull(p.dim_partid,1);

/* Vendor ID from Vendor Dimension */
update fact_purchaserebates f
from dim_vendor v
set f.dim_vendorid = v.dim_vendorid,
	dw_update_date = CURRENT_TIMESTAMP
where f.dd_vendor = v.vendornumber
and  f.dim_vendorid <> v.dim_vendorid;


-- Date fields

/*IRM_IPPRAPS*/
UPDATE fact_purchaserebates f
FROM irm_ippraps p, dim_date dt
SET	dim_dateidpurchrebateposting = dt.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
and p.irm_ippraps_budat =dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidpurchrebateposting <> dt.dim_dateid;


UPDATE fact_purchaserebates f
FROM irm_ippraps p, dim_date dt
SET	dim_dateidpurchrebateevalfrom = dt.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
and p.irm_ippraps_efdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidpurchrebateevalfrom <> dt.dim_dateid;


UPDATE fact_purchaserebates f
FROM irm_ippraps p, dim_date dt
SET dim_dateidpurchrebatecreate = dt.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
and p.irm_ippraps_erdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidpurchrebatecreate <> dt.dim_dateid;

UPDATE fact_purchaserebates f
FROM irm_ippraps p, dim_date dt
SET	dim_dateidpurchrebateevalto = dt.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
and p.irm_ippraps_etdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidpurchrebateevalto <> dt.dim_dateid;

UPDATE fact_purchaserebates f
FROM irm_ipprdfl p, dim_date dt
SET	dim_dateidparkdoccreate = dt.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_pripdocno = p.irm_ipprdfl_ipnum
and f.dd_chargebackdocumentnumber = p.irm_ipprdfl_ipnum
and f.dd_prdocitmno = p.irm_ipprdfl_ipitm
and dd_parkdocumentno = p.irm_ipprdfl_vbeln
and dd_companycode = p.irm_ipprdfl_bukrs
and irm_ipprdfl_erdat = dt.datevalue
and dt.companycode = p.irm_ipprdfl_bukrs
and dim_dateidparkdoccreate <> dt.dim_dateid;	

/*IRM_IPPRHDR*/
UPDATE fact_purchaserebates f
FROM	irm_ipprhdr h, dim_Date dt
SET
	dim_dateidprposting = dt.dim_Dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERe
f.dd_pripdocno = h.irm_ipprhdr_ipnum
and dt.datevalue = irm_ipprhdr_budat
and f.dd_companycode = dt.companycode
and dim_dateidprposting <> dt.dim_Dateid;

UPDATE fact_purchaserebates f
FROM	irm_ipprhdr h, dim_Date dt
SET
	dim_dateidprcreate = dt.dim_Dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERe
f.dd_pripdocno = h.irm_ipprhdr_ipnum
and dt.datevalue = irm_ipprhdr_erdat
and f.dd_companycode = dt.companycode
and dim_dateidprcreate <> dt.dim_Dateid;

/* 16Jun2015 - to Update ReabtesAgreement deatils incase dd_pragreementno is not set*/
UPDATE fact_purchaserebates f
from dim_rebateagreements d
set f.dim_rebateagreementsid = ifnull(d.dim_rebateagreementsid,1),
	dw_update_date = CURRENT_TIMESTAMP
where  d.agreementnum = case when f.dd_Agreementno = 'Not Set' and f.dd_pragreementno <> 'Not Set' then f.dd_pragreementno
							when f.dd_Agreementno <> 'Not Set' and f.dd_pragreementno = 'Not Set' then f.dd_agreementno
						end
and f.dim_rebateagreementsid = 1
and d.rebatebasis = 'PURC'	
and f.dim_rebateagreementsid <> ifnull(d.dim_rebateagreementsid,1);

/* 18Jun2015 - To Udpdate AgreementDetails for SalesRebates related Deployment codes as well 
UPDATE fact_purchaserebates f
FROM dim_rebateagreements d
SET f.dim_rebateagreementsid = d.dim_rebateagreementsid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE d.agreementnum = case when f.dd_Agreementno = 'Not Set' and f.dd_pragreementno <> 'Not Set' then f.dd_pragreementno
							when f.dd_Agreementno <> 'Not Set' and f.dd_pragreementno = 'Not Set' then f.dd_agreementno
						end
and f.dim_rebateagreementsid = 1
and d.rebatebasis = 'SALE' 
and f.dd_deploymentcode  in ('Z1PRD714','Z1PRD715','Z1PRD716','Z1PRD717','Z1PRD720','Z1PRD723')
and f.dim_rebateagreementsid <> d.dim_rebateagreementsid
*/	

/*14Jun2015 New field */
UPDATE fact_purchaserebates f
FROM    irm_ipprhdr h, dim_Date dt
SET f.dim_dateidPO = dt.dim_Dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_pripdocno = h.irm_ipprhdr_ipnum
and dt.datevalue = IRM_IPPRHDR_BEDAT
and f.dd_companycode = dt.companycode
and dim_dateidPO <> dt.dim_Dateid;


/* IRM_IPPRITM */
UPDATE fact_purchaserebates f
FROM irm_ippritm i, dim_date dt
SET	
	dim_dateiddocposting = ifnull(dt.dim_Dateid,1),
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_pripdocno = i.irm_ippritm_ipnum
and f.dd_prdocitmno = i.irm_ippritm_ipitm
and dt.datevalue = irm_ippritm_budat
and f.dd_companycode = dt.companycode
and dim_dateiddocposting <> ifnull(dt.dim_Dateid,1);



/* IRM_GCRHDR */
UPDATE fact_purchaserebates f
FROM irm_gcrhdr g, dim_Date dt
SET
	dim_dateidclaimchange = dt.dim_Dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE
f.dd_claimnumber = g.irm_gcrhdr_vbeln
and g.irm_gcrhdr_aedat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidclaimchange <> dt.dim_Dateid;

UPDATE fact_purchaserebates f
FROM irm_gcrhdr g, dim_Date dt
SET
	dim_dateidclaimcreate = dt.dim_Dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE
f.dd_claimnumber = g.irm_gcrhdr_vbeln
and g.irm_gcrhdr_erdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidclaimcreate <> dt.dim_Dateid;

UPDATE fact_purchaserebates f
FROM irm_gcrhdr g, dim_Date dt
SET
	dim_dateidclaimfrom = dt.dim_Dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE
f.dd_claimnumber = g.irm_gcrhdr_vbeln
and g.irm_gcrhdr_frdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidclaimfrom <> dt.dim_Dateid;

UPDATE fact_purchaserebates f
FROM irm_gcrhdr g, dim_Date dt
SET
	dim_dateidclaimto = dt.dim_Dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE
f.dd_claimnumber = g.irm_gcrhdr_vbeln
and g.irm_gcrhdr_todat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidclaimfrom <> dt.dim_Dateid;

call vectorwise(combine 'fact_purchaserebates');

/* Derived Fields */
/*
UPDATE fact_purchaserebates
SET amt_rebateamtpaid = ifnull((CASE WHEN dd_subsequentdocumentcategory in ('AR','SI','SE') THEN amt_purchaserebateamt*-1 ELSE amt_purchaserebateamt END),'0.0000'),
	dw_update_date = CURRENT_TIMESTAMP
WHERE  ifnull(amt_rebateamtpaid,'0.0000') <> ifnull((CASE WHEN dd_subsequentdocumentcategory in ('AR','SI','SE') THEN amt_purchaserebateamt*-1 ELSE amt_purchaserebateamt END),'0.0000')
*/
						
UPDATE fact_purchaserebates f
FROM dim_date fdt, dim_date tdt
SET dd_participationperiod = ifnull(tdt.datevalue - fdt.datevalue,0),
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dim_dateidpurchrebateevalto = tdt.dim_dateid
and f.dim_dateidpurchrebateevalfrom = fdt.dim_Dateid
and dd_participationperiod <> ifnull(tdt.datevalue - fdt.datevalue,0);

/* 15May2015 - Latest claim Number */
drop table if exists latestclaim_temp;
create table latestclaim_temp
as
select distinct dd_participationnumber, dd_prdocno,dd_prdocitmno,max(dd_claimnumber) as maxClaimNumber
from fact_purchaserebates
group by dd_participationnumber, dd_prdocno,dd_prdocitmno;

update fact_purchaserebates f
from latestclaim_temp t
set f.dd_latestClaimnumber = t.maxClaimNumber,
	dw_update_date = CURRENT_TIMESTAMP
where f.dd_participationnumber = t.dd_participationnumber
and f.dd_prdocno = t.dd_prdocno
and f.dd_prdocitmno = t.dd_prdocitmno
and f.dd_latestClaimnumber <> t.maxClaimNumber;

call vectorwise (combine 'fact_purchaserebates');
