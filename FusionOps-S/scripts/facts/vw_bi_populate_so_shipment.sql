/* ##################################################################################################################

   Script         : bi_populate_so_shipment
   Author         : Ashu
  Created On     : 22 Jan 2013


   Description    : Stored Proc bi_populate_so_shipment from MySQL to Vectorwise syntax

   Change History
   Date            By        Version           Desc
   22 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
   20 Mar 2013     Lokesh	   1.1               Minor changes in logging, comments etc. Removed join with likp_lips 
						 	to sync up with the current version on prod ( changes done by shanthi ) 
#################################################################################################################### */


UPDATE fact_salesorder so
  SET ct_DeliveredQty = ifnull((select sum(f.ct_QtyDelivered) from fact_salesorderdelivery f, dim_salesorderitemstatus sois 
                           where f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid and 
				f.dd_SalesDocNo = so.dd_SalesDocNo
                                and f.dd_SalesItemNo = so.dd_SalesItemNo
                                and f.dd_ScheduleNo = so.dd_ScheduleNo
                                and sois.GoodsMovementStatus <> 'Not yet processed'), 0);

UPDATE fact_salesorder so
Set Dim_DateidShipmentDelivery = ifnull((select max(f.Dim_DateidDeliveryDate) from fact_salesorderdelivery f,
                                  dim_salesorderitemstatus sois 
                           where  f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
				and f.dd_SalesDocNo = so.dd_SalesDocNo
                                and f.dd_SalesItemNo = so.dd_SalesItemNo
                                and f.dd_ScheduleNo = so.dd_ScheduleNo
                                and sois.GoodsMovementStatus <> 'Not yet processed'),1);

UPDATE fact_salesorder so
SET Dim_DateidActualGI = ifnull((select max(f.Dim_DateidActualGoodsIssue) from fact_salesorderdelivery f,
                                  dim_salesorderitemstatus sois
                           where  f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid AND f.dd_SalesDocNo = so.dd_SalesDocNo
                                and f.dd_SalesItemNo = so.dd_SalesItemNo
                                and f.dd_ScheduleNo = so.dd_ScheduleNo
                                and sois.GoodsMovementStatus <> 'Not yet processed'),1);


UPDATE fact_salesorder so
SET Dim_CustomeridShipTo = ifnull(( select Dim_CustomeridShipto from fact_Salesorderdelivery f,
                                   dim_salesorderitemstatus sois 
                           where  f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid AND f.dd_SalesDocNo = so.dd_SalesDocNo
                                and f.dd_SalesItemNo = so.dd_SalesItemNo
                                and f.dd_ScheduleNo = so.dd_ScheduleNo
                                and sois.GoodsMovementStatus <> 'Not yet processed' ),so.Dim_CustomerID);

UPDATE fact_salesorder so
SET Dim_DateidDlvrDocCreated = ifnull((select min(f.Dim_DateidDlvrDocCreated) from fact_salesorderdelivery f,
                                   dim_salesorderitemstatus sois 
                           where  f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid and f.dd_SalesDocNo = so.dd_SalesDocNo
                                and f.dd_SalesItemNo = so.dd_SalesItemNo
                                and f.dd_ScheduleNo = so.dd_ScheduleNo
                                and sois.GoodsMovementStatus <> 'Not yet processed'),1);

DROP TABLE IF EXISTS tmp1_soshipmt;
CREATE TABLE tmp1_soshipmt
AS
SELECT dd_SalesDocNo,dd_SalesItemNo
FROM fact_salesorderdelivery sod inner join VBFA v on sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
WHERE v.vbfa_bwart = '602'
and not exists (select 1 from VBFA v1 where v1 .vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                                      and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat);


UPDATE fact_salesorder so
FROM dim_salesorderitemstatus s
SET ct_DeliveredQty = so.ct_ConfirmedQty
WHERE so.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      AND so.dd_ItemRelForDelv = 'X'
      AND s.OverallDeliveryStatus = 'Completely processed'
      AND so.ct_DeliveredQty < so.ct_ConfirmedQty
      AND not exists (select 1 from tmp1_soshipmt sod 
                      where sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo)
AND  ct_DeliveredQty <> so.ct_ConfirmedQty;
                                     
DROP TABLE IF EXISTS tmp1_soshipmt;
