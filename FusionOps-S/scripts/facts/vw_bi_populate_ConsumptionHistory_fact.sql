/******************************************************************************************************************/
/*   Script         : vw_bi_populate_ConsumptionHistory_fact.sql	                                          */
/*   Author         : Simona                                                                                      */
/*   Created On     : 24 July 2014                                                                                */
/*   Description    : Populate script for fact_ConsumptionHistory                                                 */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/******************************************************************************************************************/

DROP TABLE IF EXISTS fact_ConsumptionHistory_tmp_populate;
CREATE TABLE fact_ConsumptionHistory_tmp_populate
AS
SELECT * FROM fact_ConsumptionHistory WHERE 1 = 2;

ALTER TABLE fact_ConsumptionHistory_tmp_populate  ADD  PRIMARY KEY (fact_ConsumptionHistoryId);

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_ConsumptionHistory_tmp_populate';
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ConsumptionHistory_tmp_populate', ifnull(max(fact_ConsumptionHistoryId), 0) FROM fact_ConsumptionHistory_tmp_populate;

DROP TABLE IF EXISTS tmp_ins_fact_ConsumptionHistory;
DROP TABLE IF EXISTS tmp_del_fact_ConsumptionHistory;

CREATE TABLE tmp_ins_fact_ConsumptionHistory
AS
SELECT DISTINCT fmm.dd_MaterialDocNo, fmm.dd_MaterialDocItemNo, fmm.dd_MaterialDocYear, fmm.dim_plantid, fmm.dim_companyid
 FROM fact_materialmovement fmm;

CREATE TABLE tmp_del_fact_ConsumptionHistory
AS
SELECT DISTINCT fch.dd_MaterialDocNo, fch.dd_MaterialDocItemNo, fch.dd_MaterialDocYear, fch.dim_plantid, fch.dim_companyid
 FROM fact_ConsumptionHistory fch;

CALL VECTORWISE(combine 'tmp_ins_fact_ConsumptionHistory - tmp_del_fact_ConsumptionHistory');

INSERT INTO fact_ConsumptionHistory_tmp_populate 
(fact_ConsumptionHistoryId,
 dd_materialdocno,
 dd_materialdocitemno,
 dd_materialdocyear,
 dd_documentno,
 dd_documentitemno,
 dd_salesorderno,
 dd_salesorderitemno,
 dd_salesorderdlvrno,
 dd_glaccountno,
 dd_productionordernumber,
 dd_productionorderitemno,
 dd_batchnumber,
 dd_valuationtype,
 dd_debitcreditid,
 dd_goodsmovereason,
 amt_localcurramt,
 amt_deliverycost,
 amt_altpricecontrol,
 amt_onhand_valstock,
 ct_quantity,
 ct_qtyentryuom,
 ct_qtygrordunit,
 ct_qtyonhand_valstock,
 ct_qtyordpriceunit,
 dim_movementtypeid,
 dim_companyid,
 dim_currencyid,
 dim_partid,
 dim_plantid,
 dim_storagelocationid,
 dim_vendorid,
 dim_dateidmaterialdocdate,
 dim_dateidpostingdate,
 dim_dateiddoccreation,
 dim_dateidordered,
 dim_producthierarchyid,
 dim_movementindicatorid,
 dim_customerid,
 dim_costcenterid,
 dim_accountcategoryid,
 dim_consumptiontypeid,
 dim_controllingareaid,
 dim_customergroup1id,
 dim_documentcategoryid,
 dim_documenttypeid,
 dim_itemcategoryid,
 dim_itemstatusid,
 dim_productionorderstatusid,
 dim_productionordertypeid,
 dim_purchasegroupid,
 dim_purchaseorgid,
 dim_salesdocumenttypeid,
 dim_salesgroupid,
 dim_salesorderheaderstatusid,
 dim_salesorderitemstatusid,
 dim_salesorderrejectreasonid,
 dim_salesorgid,
 dim_specialstockid,
 dim_stocktypeid,
 dim_unitofmeasureid,
 dirtyrow,
 dim_materialgroupid,
 amt_exchangerate_gbl,
 amt_exchangerate,
 dim_termid,
 dd_consignmentflag,
 dim_profitcenterid,
 dim_receivingplantid,
 dim_receivingpartid,
 amt_pounitprice,
 amt_stdunitprice,
 dim_dateidcosting,
 dim_incotermid,
 dim_dateiddelivery,
 dim_dateidstatdelivery,
 dim_grstatusid,
 dd_incoterms2,
 dd_intorder,
 amt_plannedprice,
 amt_plannedprice1,
 dd_documentscheduleno,
 dim_recvissustorlocid,
 ct_orderquantity,
 dim_poplantidordering, 
 dim_poplantidsupplying,
 dim_postoragelocid,
 dim_poissustoragelocid,
 dim_inspusagedecisionid,
 ct_lotnotthruqm,
 dd_referencedocno,
 dd_referencedocitem,
 lotsacceptedflag,
 lotsrejectedflag,
 percentlotsinspectedflag,
 percentlotsrejectedflag,
 percentlotsacceptedflag,
 lotsskippedflag,
 lotsawaitinginspectionflag,
 lotsinspectedflag,
 qtyrejectedexternalamt,
 qtyrejectedinternalamt,
 dim_afssizeid,
 dd_vendorspendflag,
 amt_vendorspend,
 dim_currencyid_tra,
 dim_currencyid_gbl,
 dd_inspectionstatusingrdoc,
 lotsreceivedflag,
 dim_unitofmeasureorderunitid,
 dim_uomunitofentryid,
 dd_Plannedconsumption,
 dd_UnPlannedconsumption,
 dd_quantitystring,
 dd_stocktypemodif,
 dd_consecutivecounter)
SELECT 
(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_ConsumptionHistory_tmp_populate') + row_number() over() fact_ConsumptionHistoryId,
 f.dd_materialdocno,
 f.dd_materialdocitemno,
 f.dd_materialdocyear,
 ifnull(f.dd_documentno,'Not Set'),
 ifnull(f.dd_documentitemno,0),
 ifnull(f.dd_salesorderno,'Not Set'),
 ifnull(f.dd_salesorderitemno,0),
 ifnull(f.dd_salesorderdlvrno,0),
 ifnull(f.dd_glaccountno,'Not Set'),
 ifnull(f.dd_productionordernumber,'Not Set'),
 ifnull(f.dd_productionorderitemno,0),
 ifnull(f.dd_batchnumber,'Not Set'),
 ifnull(f.dd_valuationtype,'Not Set'),
 f.dd_debitcreditid,
 f.dd_goodsmovereason,
 f.amt_localcurramt,
 f.amt_deliverycost,
 f.amt_altpricecontrol,
 f.amt_onhand_valstock,
 f.ct_quantity,
 f.ct_qtyentryuom,
 f.ct_qtygrordunit,
 f.ct_qtyonhand_valstock,
 f.ct_qtyordpriceunit,
 f.dim_movementtypeid,
 f.dim_companyid,
 f.dim_currencyid,
 f.dim_partid,
 f.dim_plantid,
 f.dim_storagelocationid,
 f.dim_vendorid,
 f.dim_dateidmaterialdocdate,
 f.dim_dateidpostingdate,
 f.dim_dateiddoccreation,
 f.dim_dateidordered,
 f.dim_producthierarchyid,
 f.dim_movementindicatorid,
 f.dim_customerid,
 f.dim_costcenterid,
 f.dim_accountcategoryid,
 f.dim_consumptiontypeid,
 f.dim_controllingareaid,
 f.dim_customergroup1id,
 f.dim_documentcategoryid,
 f.dim_documenttypeid,
 f.dim_itemcategoryid,
 f.dim_itemstatusid,
 ifnull(f.dim_productionorderstatusid,1),
 f.dim_productionordertypeid,
 f.dim_purchasegroupid,
 f.dim_purchaseorgid,
 f.dim_salesdocumenttypeid,
 f.dim_salesgroupid,
 f.dim_salesorderheaderstatusid,
 f.dim_salesorderitemstatusid,
 f.dim_salesorderrejectreasonid,
 f.dim_salesorgid,
 f.dim_specialstockid,
 f.dim_stocktypeid,
 f.dim_unitofmeasureid,
 f.dirtyrow,
 f.dim_materialgroupid,
 f.amt_exchangerate_gbl,
 f.amt_exchangerate,
 f.dim_termid,
 f.dd_consignmentflag,
 f.dim_profitcenterid,
 f.dim_receivingplantid,
 f.dim_receivingpartid,
 ifnull(f.amt_pounitprice,0),
 ifnull(f.amt_stdunitprice,0),
 f.dim_dateidcosting,
 f.dim_incotermid,
 f.dim_dateiddelivery,
 f.dim_dateidstatdelivery,
 ifnull(f.dim_grstatusid,1),
 ifnull(f.dd_incoterms2,'Not Set'),
 ifnull(f.dd_intorder,'Not Set'),
 ifnull(f.amt_plannedprice,0),
 ifnull(f.amt_plannedprice1,0),
 ifnull(f.dd_documentscheduleno,0),
 f.dim_recvissustorlocid,
 ifnull(f.ct_orderquantity,0),
 f.dim_poplantidordering, 
 f.dim_poplantidsupplying,
 f.dim_postoragelocid,
 f.dim_poissustoragelocid,
 f.dim_inspusagedecisionid,
 ifnull(f.ct_lotnotthruqm,0),
 f.dd_referencedocno,
 f.dd_referencedocitem,
 ifnull(f.lotsacceptedflag,'Not Set'),
 ifnull(f.lotsrejectedflag,'Not Set'),
 ifnull(f.percentlotsinspectedflag,'Not Set'),
 ifnull(f.percentlotsrejectedflag,'Not Set'),
 ifnull(f.percentlotsacceptedflag,'Not Set'),
 ifnull(f.lotsskippedflag,'Not Set'),
 ifnull(f.lotsawaitinginspectionflag,'Not Set'),
 ifnull(f.lotsinspectedflag,'Not Set'),
 ifnull(f.qtyrejectedexternalamt,0),
 ifnull(f.qtyrejectedinternalamt,0),
 f.dim_afssizeid,
 f.dd_vendorspendflag,
 ifnull(f.amt_vendorspend,0),
 f.dim_currencyid_tra,
 f.dim_currencyid_gbl,
 f.dd_inspectionstatusingrdoc,
 f.lotsreceivedflag,
 ifnull(f.dim_unitofmeasureorderunitid,1),
 ifnull(f.dim_uomunitofentryid,1),
 'Not Set' dd_Plannedconsumption,
 'Not Set' dd_UnPlannedconsumption,
 'Not Set' dd_quantitystring,
 'Not Set' dd_stocktypemodif,
 'Not Set' dd_consecutivecounter
FROM fact_materialmovement f
INNER JOIN tmp_ins_fact_ConsumptionHistory ins on f.dd_MaterialDocNo = ins.dd_MaterialDocNo and f.dd_MaterialDocItemNo = ins.dd_MaterialDocItemNo and f.dd_MaterialDocYear = ins.dd_MaterialDocYear 
       and f.dim_plantid = ins.dim_plantid and f.dim_companyid = ins.dim_companyid
INNER JOIN dim_movementtype mt on mt.Dim_MovementTypeid = f.Dim_MovementTypeid
INNER JOIN T156 t ON ifnull(t.T156_BWART , 'Not Set') = mt.MovementType
WHERE  t.T156_KZVBU IS NOT NULL ;

INSERT INTO fact_ConsumptionHistory
(fact_ConsumptionHistoryId,
 dd_materialdocno,
 dd_materialdocitemno,
 dd_materialdocyear,
 dd_documentno,
 dd_documentitemno,
 dd_salesorderno,
 dd_salesorderitemno,
 dd_salesorderdlvrno,
 dd_glaccountno,
 dd_productionordernumber,
 dd_productionorderitemno,
 dd_batchnumber,
 dd_valuationtype,
 dd_debitcreditid,
 dd_goodsmovereason,
 amt_localcurramt,
 amt_deliverycost,
 amt_altpricecontrol,
 amt_onhand_valstock,
 ct_quantity,
 ct_qtyentryuom,
 ct_qtygrordunit,
 ct_qtyonhand_valstock,
 ct_qtyordpriceunit,
 dim_movementtypeid,
 dim_companyid,
 dim_currencyid,
 dim_partid,
 dim_plantid,
 dim_storagelocationid,
 dim_vendorid,
 dim_dateidmaterialdocdate,
 dim_dateidpostingdate,
 dim_dateiddoccreation,
 dim_dateidordered,
 dim_producthierarchyid,
 dim_movementindicatorid,
 dim_customerid,
 dim_costcenterid,
 dim_accountcategoryid,
 dim_consumptiontypeid,
 dim_controllingareaid,
 dim_customergroup1id,
 dim_documentcategoryid,
 dim_documenttypeid,
 dim_itemcategoryid,
 dim_itemstatusid,
 dim_productionorderstatusid,
 dim_productionordertypeid,
 dim_purchasegroupid,
 dim_purchaseorgid,
 dim_salesdocumenttypeid,
 dim_salesgroupid,
 dim_salesorderheaderstatusid,
 dim_salesorderitemstatusid,
 dim_salesorderrejectreasonid,
 dim_salesorgid,
 dim_specialstockid,
 dim_stocktypeid,
 dim_unitofmeasureid,
 dirtyrow,
 dim_materialgroupid,
 amt_exchangerate_gbl,
 amt_exchangerate,
 dim_termid,
 dd_consignmentflag,
 dim_profitcenterid,
 dim_receivingplantid,
 dim_receivingpartid,
 amt_pounitprice,
 amt_stdunitprice,
 dim_dateidcosting,
 dim_incotermid,
 dim_dateiddelivery,
 dim_dateidstatdelivery,
 dim_grstatusid,
 dd_incoterms2,
 dd_intorder,
 amt_plannedprice,
 amt_plannedprice1,
 dd_documentscheduleno,
 dim_recvissustorlocid,
 ct_orderquantity,
 dim_poplantidordering, 
 dim_poplantidsupplying,
 dim_postoragelocid,
 dim_poissustoragelocid,
 dim_inspusagedecisionid,
 ct_lotnotthruqm,
 dd_referencedocno,
 dd_referencedocitem,
 lotsacceptedflag,
 lotsrejectedflag,
 percentlotsinspectedflag,
 percentlotsrejectedflag,
 percentlotsacceptedflag,
 lotsskippedflag,
 lotsawaitinginspectionflag,
 lotsinspectedflag,
 qtyrejectedexternalamt,
 qtyrejectedinternalamt,
 dim_afssizeid,
 dd_vendorspendflag,
 amt_vendorspend,
 dim_currencyid_tra,
 dim_currencyid_gbl,
 dd_inspectionstatusingrdoc,
 lotsreceivedflag,
 dim_unitofmeasureorderunitid,
 dim_uomunitofentryid,
 dd_Plannedconsumption,
 dd_UnPlannedconsumption,
 dd_quantitystring,
 dd_stocktypemodif,
 dd_consecutivecounter )
SELECT
(SELECT ifnull(max(ch.fact_ConsumptionHistoryId)+1, 1) id FROM fact_ConsumptionHistory ch) + row_number() over() fact_ConsumptionHistoryId,
 dd_materialdocno,
 dd_materialdocitemno,
 dd_materialdocyear,
 dd_documentno,
 dd_documentitemno,
 dd_salesorderno,
 dd_salesorderitemno,
 dd_salesorderdlvrno,
 dd_glaccountno,
 dd_productionordernumber,
 dd_productionorderitemno,
 dd_batchnumber,
 dd_valuationtype,
 dd_debitcreditid,
 dd_goodsmovereason,
 amt_localcurramt,
 amt_deliverycost,
 amt_altpricecontrol,
 amt_onhand_valstock,
 ct_quantity,
 ct_qtyentryuom,
 ct_qtygrordunit,
 ct_qtyonhand_valstock,
 ct_qtyordpriceunit,
 dim_movementtypeid,
 dim_companyid,
 dim_currencyid,
 dim_partid,
 dim_plantid,
 dim_storagelocationid,
 dim_vendorid,
 dim_dateidmaterialdocdate,
 dim_dateidpostingdate,
 dim_dateiddoccreation,
 dim_dateidordered,
 dim_producthierarchyid,
 dim_movementindicatorid,
 dim_customerid,
 dim_costcenterid,
 dim_accountcategoryid,
 dim_consumptiontypeid,
 dim_controllingareaid,
 dim_customergroup1id,
 dim_documentcategoryid,
 dim_documenttypeid,
 dim_itemcategoryid,
 dim_itemstatusid,
 dim_productionorderstatusid,
 dim_productionordertypeid,
 dim_purchasegroupid,
 dim_purchaseorgid,
 dim_salesdocumenttypeid,
 dim_salesgroupid,
 dim_salesorderheaderstatusid,
 dim_salesorderitemstatusid,
 dim_salesorderrejectreasonid,
 dim_salesorgid,
 dim_specialstockid,
 dim_stocktypeid,
 dim_unitofmeasureid,
 dirtyrow,
 dim_materialgroupid,
 amt_exchangerate_gbl,
 amt_exchangerate,
 dim_termid,
 dd_consignmentflag,
 dim_profitcenterid,
 dim_receivingplantid,
 dim_receivingpartid,
 amt_pounitprice,
 amt_stdunitprice,
 dim_dateidcosting,
 dim_incotermid,
 dim_dateiddelivery,
 dim_dateidstatdelivery,
 dim_grstatusid,
 dd_incoterms2,
 dd_intorder,
 amt_plannedprice,
 amt_plannedprice1,
 dd_documentscheduleno,
 dim_recvissustorlocid,
 ct_orderquantity,
 dim_poplantidordering, 
 dim_poplantidsupplying,
 dim_postoragelocid,
 dim_poissustoragelocid,
 dim_inspusagedecisionid,
 ct_lotnotthruqm,
 dd_referencedocno,
 dd_referencedocitem,
 lotsacceptedflag,
 lotsrejectedflag,
 percentlotsinspectedflag,
 percentlotsrejectedflag,
 percentlotsacceptedflag,
 lotsskippedflag,
 lotsawaitinginspectionflag,
 lotsinspectedflag,
 qtyrejectedexternalamt,
 qtyrejectedinternalamt,
 dim_afssizeid,
 dd_vendorspendflag,
 amt_vendorspend,
 dim_currencyid_tra,
 dim_currencyid_gbl,
 dd_inspectionstatusingrdoc,
 lotsreceivedflag,
 dim_unitofmeasureorderunitid,
 dim_uomunitofentryid,
 dd_Plannedconsumption,
 dd_UnPlannedconsumption,
 dd_quantitystring,
 dd_stocktypemodif,
 dd_consecutivecounter
FROM fact_ConsumptionHistory_tmp_populate ;

CALL VECTORWISE(combine 'fact_ConsumptionHistory');

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

/* Update dd_quantitystring field */
UPDATE fact_ConsumptionHistory fch
FROM MKPF_MSEG m, dim_plant dp, dim_company dc
SET dd_quantitystring = MSEG_BUSTM
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_quantitystring <> ifnull(m.MSEG_BUSTM , 'Not Set') ;

UPDATE fact_ConsumptionHistory fch
FROM MKPF_MSEG1 m, dim_plant dp, dim_company dc
SET dd_quantitystring = MSEG1_BUSTM
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_quantitystring <> ifnull(m.MSEG1_BUSTM , 'Not Set') ;

/* Update dd_stocktypemodif field */
UPDATE fact_ConsumptionHistory fch
FROM MKPF_MSEG m, dim_plant dp, dim_company dc
SET dd_stocktypemodif = ifnull(m.MSEG_ZUSTD , 'Not Set') 
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_stocktypemodif <> ifnull(m.MSEG_ZUSTD , 'Not Set') ;

UPDATE fact_ConsumptionHistory fch
FROM MKPF_MSEG1 m, dim_plant dp, dim_company dc
SET dd_stocktypemodif = ifnull(m.MSEG1_ZUSTD , 'Not Set')
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_stocktypemodif <> ifnull(m.MSEG1_ZUSTD , 'Not Set') ;

/* Update dd_consecutivecounter field */
UPDATE fact_ConsumptionHistory fch
FROM fact_materialmovement m, dim_plant dp, dim_company dc
SET dd_consecutivecounter = ifnull(m.dd_ItemAutomaticallyCreated , 'Not Set')
WHERE m.dim_plantid = dp.dim_plantid
AND m.dim_companyid  = dc.dim_companyid
AND fch.dd_MaterialDocNo = m.dd_Materialdocno
AND fch.dd_MaterialDocItemNo = m.dd_MaterialDocItemNo
AND m.dd_MaterialDocYear = fch.dd_MaterialDocYear
AND dd_consecutivecounter <> ifnull(m.dd_ItemAutomaticallyCreated , 'Not Set');

CALL VECTORWISE(combine 'fact_ConsumptionHistory');

/* The key to determining the value to use for T156M-CNT02 is MSEG-XAUTO. MSEG-XAUTO = blank use T156M-CNT02=1 MSEG-XAUTO = 'X' use T156M-CNT02=2 */
DROP TABLE IF EXISTS TMP_T156M ;
CREATE TABLE TMP_T156M AS
SELECT T156M_BUSTM, ifnull(T156M_ZUSTD,'Not Set') T156M_ZUSTD, DECODE(T156M_CNT02, 1,'Not Set', 2,'X') AS T156M_CNT02_X, T156M_KZVBL
FROM T156M ;

/* Keep all records where T156M_KZVBL is not blank */
DELETE FROM fact_ConsumptionHistory
WHERE EXISTS (SELECT 1 FROM TMP_T156M t 
              WHERE varchar(t.T156M_BUSTM,7) = dd_quantitystring AND varchar(t.T156M_ZUSTD,7) = dd_stocktypemodif  AND varchar(t.T156M_CNT02_X,7) = dd_consecutivecounter
			    AND ifnull(t.T156M_KZVBL,'BLANK') = 'BLANK');
				
CALL VECTORWISE(combine 'fact_ConsumptionHistory');

/* Update dd_Plannedconsumption, dd_UnPlannedconsumption based on MSEG and T156 */
/* T156_KZVBU = 'G' */
	UPDATE fact_ConsumptionHistory fch
	FROM T156 t, dim_movementtype mt
	SET dd_Plannedconsumption = 'Yes'
	WHERE t.T156_KZVBU = 'G'
	AND mt.Dim_MovementTypeid = fch.Dim_MovementTypeid
	AND ifnull(t.T156_BWART , 'Not Set') = mt.MovementType
	AND dd_Plannedconsumption <> 'Yes' ;

	UPDATE fact_ConsumptionHistory fch
	FROM T156 t, dim_movementtype mt
	SET dd_UnPlannedconsumption = 'No'
	WHERE t.T156_KZVBU = 'G'
	AND mt.Dim_MovementTypeid = fch.Dim_MovementTypeid
	AND ifnull(t.T156_BWART , 'Not Set') = mt.MovementType
	AND dd_UnPlannedconsumption <> 'No' ;

/* T156_KZVBU = 'U' */
	UPDATE fact_ConsumptionHistory fch
	FROM T156 t, dim_movementtype mt
	SET dd_Plannedconsumption = 'No'
	WHERE t.T156_KZVBU = 'U'
	AND mt.Dim_MovementTypeid = fch.Dim_MovementTypeid
	AND ifnull(t.T156_BWART , 'Not Set') = mt.MovementType
	AND dd_Plannedconsumption <> 'No'  ;
	
	UPDATE fact_ConsumptionHistory fch
	FROM T156 t, dim_movementtype mt
	SET dd_UnPlannedconsumption = 'Yes'
	WHERE t.T156_KZVBU = 'U'
	AND mt.Dim_MovementTypeid = fch.Dim_MovementTypeid
	AND ifnull(t.T156_BWART , 'Not Set') = mt.MovementType
	AND dd_UnPlannedconsumption <> 'Yes'  ;

/* T156_KZVBU =  'R' and MSEG_RSNUM <> 0 for dd_Plannedconsumption  */
	UPDATE fact_ConsumptionHistory fch
	FROM T156 t, fact_materialmovement m, dim_plant dp, dim_company dc, dim_movementtype mt
	SET dd_Plannedconsumption = 'Yes'
	WHERE t.T156_KZVBU =  'R' and m.dd_ReservationNumber <> 0
	AND dp.dim_plantid = m.dim_plantid
	AND dc.dim_companyid = m.dim_Companyid
	AND mt.Dim_MovementTypeid = fch.Dim_MovementTypeid
	AND fch.dd_MaterialDocNo = m.dd_MaterialDocNo
	AND fch.dd_MaterialDocItemNo = m.dd_MaterialDocItemNo
	AND fch.dd_MaterialDocYear = m.dd_MaterialDocYear
	AND ifnull(t.T156_BWART , 'Not Set') = mt.MovementType 
	AND dd_Plannedconsumption <> 'Yes' ;


/* T156_KZVBU =  'R' and MSEG_RSNUM = 0 for dd_Plannedconsumption  */
	UPDATE fact_ConsumptionHistory fch
	FROM T156 t, fact_materialmovement m, dim_plant dp, dim_company dc, dim_movementtype mt
	SET dd_Plannedconsumption = 'No'
	WHERE t.T156_KZVBU =  'R' and m.dd_ReservationNumber = 0
	AND dp.dim_plantid = m.dim_plantid
	AND dc.dim_companyid = m.dim_Companyid
	AND mt.Dim_MovementTypeid = fch.Dim_MovementTypeid
	AND fch.dd_MaterialDocNo = m.dd_MaterialDocNo
	AND fch.dd_MaterialDocItemNo = m.dd_MaterialDocItemNo
	AND fch.dd_MaterialDocYear = m.dd_MaterialDocYear
	AND ifnull(t.T156_BWART , 'Not Set') = mt.MovementType 
	AND dd_Plannedconsumption <> 'No' ;

/* T156_KZVBU =  'R' and MSEG_RSNUM <> 0 for dd_UnPlannedconsumption  */
	UPDATE fact_ConsumptionHistory fch
	FROM T156 t, fact_materialmovement m, dim_plant dp, dim_company dc, dim_movementtype mt
	SET dd_UnPlannedconsumption = 'No'
	WHERE t.T156_KZVBU =  'R' and m.dd_ReservationNumber <> 0
	AND dp.dim_plantid = m.dim_plantid
	AND dc.dim_companyid = m.dim_Companyid
	AND mt.Dim_MovementTypeid = fch.Dim_MovementTypeid
	AND fch.dd_MaterialDocNo = m.dd_MaterialDocNo
	AND fch.dd_MaterialDocItemNo = m.dd_MaterialDocItemNo
	AND fch.dd_MaterialDocYear = m.dd_MaterialDocYear
	AND ifnull(t.T156_BWART , 'Not Set') = mt.MovementType 
	AND dd_UnPlannedconsumption <> 'No' ;

/* T156_KZVBU =  'R' and MSEG_RSNUM = 0 for dd_UnPlannedconsumption  */
	UPDATE fact_ConsumptionHistory fch
	FROM T156 t, fact_materialmovement m, dim_plant dp, dim_company dc, dim_movementtype mt
	SET dd_UnPlannedconsumption = 'Yes'
	WHERE t.T156_KZVBU =  'R' and m.dd_ReservationNumber = 0
	AND dp.dim_plantid = m.dim_plantid
	AND dc.dim_companyid = m.dim_Companyid
	AND mt.Dim_MovementTypeid = fch.Dim_MovementTypeid
	AND fch.dd_MaterialDocNo = m.dd_MaterialDocNo
	AND fch.dd_MaterialDocItemNo = m.dd_MaterialDocItemNo
	AND fch.dd_MaterialDocYear = m.dd_MaterialDocYear
	AND ifnull(t.T156_BWART , 'Not Set') = mt.MovementType 
	AND dd_UnPlannedconsumption <> 'Yes' ;
	
/* dd_flag_lastmoved */

/* Store the movement types to be filtered out while calculating last movement date */
DROP TABLE IF EXISTS tmp_filter_movementtypes;
CREATE TABLE tmp_filter_movementtypes
AS
SELECT '101' movementtype
UNION
SELECT '102'
UNION
SELECT '122'
UNION
SELECT '123'
UNION
SELECT '161'
UNION
SELECT '162'
UNION
SELECT '202'
UNION
SELECT '262'
UNION
SELECT '301'
UNION
SELECT '309'
UNION
SELECT '602'
UNION
SELECT '631';

/* Get the last posting date for each part-plant combination */
DROP TABLE IF EXISTS tmp_f_ch_lastmovedflag_partplant;
CREATE TABLE tmp_f_ch_lastmovedflag_partplant
AS
SELECT dim_plantid,dim_partid,max(pd.datevalue) max_postingdate
FROM fact_consumptionhistory f_ch,dim_date pd,dim_movementtype dm
WHERE f_ch.dim_dateidpostingdate = pd.dim_dateid
AND f_ch.dim_movementtypeid = dm.dim_movementtypeid AND dm.movementtype not in ( SELECT movementtype from tmp_filter_movementtypes )
GROUP BY dim_plantid,dim_partid;


/* For the movements on the last day, get the max dd_materialdocno  */
DROP TABLE IF EXISTS tmp_f_ch_lastmovedflag_partplant_maxmatdoc;
CREATE TABLE tmp_f_ch_lastmovedflag_partplant_maxmatdoc
AS
SELECT f_ch.dim_plantid,f_ch.dim_partid,pd.datevalue max_postingdate,max(f_ch.dd_materialdocno) dd_materialdocno_max
FROM fact_consumptionhistory f_ch,dim_date pd,tmp_f_ch_lastmovedflag_partplant t,dim_movementtype dm
WHERE f_ch.dim_plantid = t.dim_plantid AND f_ch.dim_partid = t.dim_partid and f_ch.dim_dateidpostingdate = pd.dim_dateid
AND pd.datevalue = t.max_postingdate
AND f_ch.dim_movementtypeid = dm.dim_movementtypeid AND dm.movementtype not in ( SELECT movementtype from tmp_filter_movementtypes )
GROUP BY f_ch.dim_plantid,f_ch.dim_partid,pd.datevalue;

/* For the movements on the last day, get the max dd_materialdocno and the max item for max doc  */
DROP TABLE IF EXISTS tmp_f_ch_lastmovedflag_partplant_maxmatdocitem;
CREATE TABLE tmp_f_ch_lastmovedflag_partplant_maxmatdocitem
AS
SELECT f_ch.dim_plantid,f_ch.dim_partid,t.max_postingdate,dd_materialdocno_max,max(f_ch.dd_materialdocitemno) dd_materialdocitemno_max
FROM fact_consumptionhistory f_ch,dim_date pd,tmp_f_ch_lastmovedflag_partplant_maxmatdoc t,dim_movementtype dm
WHERE f_ch.dim_plantid = t.dim_plantid AND f_ch.dim_partid = t.dim_partid and f_ch.dim_dateidpostingdate = pd.dim_dateid
AND pd.datevalue = t.max_postingdate AND f_ch.dd_materialdocno = t.dd_materialdocno_max
AND f_ch.dim_movementtypeid = dm.dim_movementtypeid AND dm.movementtype not in ( SELECT movementtype from tmp_filter_movementtypes )
GROUP BY  f_ch.dim_plantid,f_ch.dim_partid,t.max_postingdate,dd_materialdocno_max;


/* So, tmp_f_ch_lastmovedflag_partplant_maxmatdocitem has the part-plant on latest day of movement. from amongst specified movement types
It has only the latest mat doc and item for that day */

UPDATE fact_consumptionhistory f_ch
SET dd_flaglastmoved = 'N'
WHERE dd_flaglastmoved <> 'N';

UPDATE fact_consumptionhistory f_ch
FROM tmp_f_ch_lastmovedflag_partplant_maxmatdocitem t,dim_date pd, dim_movementtype dm
SET dd_flaglastmoved = 'Y'
WHERE f_ch.dim_plantid = t.dim_plantid AND f_ch.dim_partid = t.dim_partid
AND f_ch.dim_dateidpostingdate = pd.dim_dateid AND pd.datevalue = t.max_postingdate
AND f_ch.dd_materialdocno = t.dd_materialdocno_max AND f_ch.dd_materialdocitemno = t.dd_materialdocitemno_max
AND f_ch.dim_movementtypeid = dm.dim_movementtypeid AND dm.movementtype not in ( SELECT movementtype from tmp_filter_movementtypes )
AND dd_flaglastmoved <> 'Y';

/* Populate Qty on Hand from Inventoryaging for Latest row */
UPDATE fact_consumptionhistory 
SET ct_onhandqty = 0;


DROP TABLE IF EXISTS tmp_sumonhand_fromivaging;
CREATE TABLE tmp_sumonhand_fromivaging
AS
SELECT dim_partid, dim_plantid,
sum(f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit 
+ f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) sum_onhandqty
FROM fact_inventoryaging f_invagng
group by dim_partid,dim_plantid;


UPDATE fact_consumptionhistory f_ch
FROM tmp_sumonhand_fromivaging f_ivaging
SET ct_onhandqty = f_ivaging.sum_onhandqty
WHERE f_ch.dim_partid = f_ivaging.dim_partid AND f_ch.dim_plantid = f_ivaging.dim_plantid 
AND f_ch.dd_flaglastmoved = 'Y' --AND f_ivaging.dim_storagelocentrydateid = f_ch.dim_dateidpostingdate /* No relation between iv storage loc entry date and mm posting date */
AND f_ch.ct_onhandqty <> f_ivaging.sum_onhandqty;

UPDATE fact_consumptionhistory f_ch
SET ct_daysfromlastmovement = 0 
WHERE dd_flaglastmoved = 'N'
AND ifnull(ct_daysfromlastmovement,-1) <> 0;

UPDATE fact_consumptionhistory f_ch
FROM dim_date pd
SET ct_daysfromlastmovement =  case when f_ch.dim_dateidpostingdate > 1 then ( current_date - pd.DateValue ) else 0.0000 end
WHERE f_ch.dim_dateidpostingdate = pd.dim_dateid
AND ct_daysfromlastmovement <> case when f_ch.dim_dateidpostingdate > 1 then ( current_date - pd.DateValue ) else 0.0000 end
AND dd_flaglastmoved = 'Y';


CALL VECTORWISE(combine 'fact_ConsumptionHistory');

DROP TABLE IF EXISTS fact_ConsumptionHistory_tmp_populate;
DROP TABLE IF EXISTS tmp_ins_fact_ConsumptionHistory;
DROP TABLE IF EXISTS tmp_del_fact_ConsumptionHistory;
DROP TABLE IF EXISTS TMP_T156M ;
