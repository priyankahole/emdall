DELETE FROM fact_masterdata;

delete from number_fountain m where m.table_name = 'fact_masterdata';

insert into number_fountain
select 	'fact_masterdata',
	ifnull(max(d.fact_masterdataid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_masterdata d;



INSERT INTO fact_masterdata(
		Fact_MasterDataID,
		Dim_Plantid, 
		Dim_Companyid,  
		Dim_SalesOrgid,
		Dim_CustomerID,  
		Dim_AdditionalCustomerMaterialid,  
		Dim_AdditionalMaterialId,
		Dim_PartID)
SELECT 	(select ifnull(m.max_id, 0) from number_fountain m where m.table_name = 'fact_masterdata')
          + row_number() over() as Fact_MasterDataID,
		ifnull(pl.Dim_Plantid,1) Dim_Plantid,  
		ifnull(co.Dim_Companyid,1) Dim_Companyid,  
		ifnull(so.Dim_SalesOrgid,1) Dim_SalesOrgid,
		ifnull(c.Dim_CustomerID,1) Dim_CustomerID,  
		ifnull(acm.Dim_AdditionalCustomerMaterialid,1) Dim_AdditionalCustomerMaterialid,  
		ifnull(am.Dim_AdditionalMaterialId,1) Dim_AdditionalMaterialId,
		ifnull(p.dim_partid,1) dim_partid
FROM dim_plant pl
		Left Outer Join dim_company co on pl.companycode = co.companycode
		Left Outer Join dim_salesorg so on pl.salesorg = so.salesorgcode
		Left Outer Join dim_additionalcustomermaterial acm on acm.salesorg = so.salesorgcode -- KOTD498
		Left Outer Join dim_customer c on c.customernumber = acm.customernumber
		Left Outer Join dim_additionalmaterial am on acm.conditionrecordnumber = am.conditionrecordno -- KONDDP
		Left Outer Join dim_part p on pl.plantcode = p.plant and am.material = p.partnumber;
	
	