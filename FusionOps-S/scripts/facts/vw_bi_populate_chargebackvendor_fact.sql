/*
***************************************************************************************************************************
   Script         : vw_bi_populate_chargebackvendor_fact	                                                  
   Author         : Octavian                                                                                    
   Created On     : 13 Apr 2015                                                                                 
*********************************************Change History ****************************************************************
Date		By	    Version	Desc
16Jun2015	Octavian	9.6		Updating the delete statement to only occur when dfl_flag is 'Set'. That means even if a change happens to any of the records in IRM_IPCBITM that had a correspondent that got bypassed in IRM_IPCBDFL(adhoc),
								it will not delete that row, since if it will delete it will not be picked up(we are not bypassing DFL in the main insert into the fact_chargebackvendor)
15Jun2015   Octavian	9.5		Deleting from VISTEX_BKP tables where a change in CBITM at the level of AEDAT occurred. Thus deleting from fact where changes have occurred at IRM_IPCBITM level, which will be inserted afterwards
								with proper values
14Jun2015   Octavian	9.0     Adding VISTEX_BKP tables as main tables. Doing deltas from staging tables to *_vistex_hist tables. 
								Changing each update to be applied based on is_today, defaulted to 0001-01-01, moving on set to current_date, limiting the output of each statement this way.
03Jun2015	Suchithra   8.2		New custom measure added	
29May2015	Suchithra   8.1		Removed the separate Update for Accounting Doc Amt to consider Dr-Cr Indicator	
								Added New custom measure amt_hdraccountingNet
28May2015	Suchithra   8.0		Update to use Billing fact tabe directly instead of VBRK_RP staing tale
								Converted inner jo in b/w Billing and KONV table to left join  	
27May2015	Suchithra   7.2		Added New field : Is Initial Accrual
25May2015   Octavian    7.1    	Removed fact_chargebackvendorwoupdate. As of now there is no delta process in place. In case of a delta process
								a combination of how the dw_update_date is applied now and a separate update should be used 
25May2015   Octavian    7.0    	Each update applied on multiple columns split into individual updates for better performance.
25May2015   Octavian    6.1  	Replaced konv_ONETIME with konv. Changed dim_DateidPriceExRate and dim_dateidservice update stmts.
21May2015	Suchithra   6.0		Temp Update of to replace konv with konv_ONETIME.		
19May2015  	Octavian    5.5		CB Buy Cost,CB Contract Cost,CB Line Amount added
18May2015  	Octavian    5.4    	Customer, Invoice Line Gross value, Invoice Line Contract # added 
18May2015   Octavian    5.3     PC Posting Status, PC Interim Settlement Status, Invoice Line Net value, Service Rendered Date added
15May2015   Octavian    5.2     CB Line Pricing Date, Park Date and Settlement Date added.
15May2015   Octavian    5.1     CB Vendor added.
15May2015   Octavian    5.0     Removed the dependency on Chargebacks SA. Now all updates are based on staging tables
15May2015   Octavian    4.5    	Fix Profit Center - coming from VBRK_VBRP instead of CB ITM table 
14May2015	Suchithra   4.4		Initial Settlement Doc Amt remapped to IPBKPF-NETWR field similar to Init Accr Doc Amt	  
13May2015	Suchithra   4.3		Modified logic for Park doc related fields Update
								Initial Accrual Doc Amt remapped to IPBKPF-NETWR field from IPBKPF-WRBTR
12May2015	Suchithra   4.2		Changes to consider the lowest Posting date instead of timestamp from BKPF table		
								for initial accrual and settlements related updates
11May201	Octavian    4.1     Chargeback Amount Expected added to backend  
11May2015	Suchithra   4.0     Changes to the main join to consider all the billing docs even though they may not have 
								a charge back associated with it 
09May2015	Suchithra   3.1     Changes to consider lowest among the Acctng Doc Number for Accrual/Settlement Updates
								Remove BELNR from the Update for Accrual/Settlement Updates 	
06May2015	Octavian    3.0     Changed the join condition for Partner Communication tables.  
06May2015	Octavian    2.7     Added IPITM and BELNR on each of the Accrual and Settlement updates also in their respective temp tables. 
								TIPOPCC issues fixed by adding filters on VBTYP='SP' and PARVW='LF' according to the specifications 
05May2015	Octavian    2.5     Adding new initial condition, Adding fact_chargebackvendorwoupdate and other temps renamed  
								IRM_IPBKPFINDX VBTYP join condition added and Changed to KPF instead of INDX on BELNR updates on KPF table  
29Apr2015	Suchithra   2.0		Updated to include IPCBDFL - vbeln, vbtyp_n in the initial insert 
								to the fact table as one IPNUM can have multiple VBELNs 	  
								Added clause for dim_plantid whereever requried in Update Stmts   
13Apr2015	Octavian    1.0     New script created                                                    

Note - A new field dfl_flag has been included in the fact table to specify if the Doc Flow table has been bypassed while
inserting records in the fact table. 'Not Set' value implies that the IPCBDFL table has been bypassed. The defualt value
of this field would be 'Set'
***************************************************************************************************************************
*/
INSERT INTO irm_ipbkpfindx_vistex_hist
SELECT * FROM  irm_ipbkpfindx t
    WHERE NOT EXISTS (SELECT 1
                      FROM irm_ipbkpfindx_vistex_hist bkp
                      WHERE ifnull(t.irm_ipbkpfindx_vbtyp,'Not Set') = ifnull(bkp.irm_ipbkpfindx_vbtyp,'Not Set')
                        AND ifnull(t.irm_ipbkpfindx_belnr,'Not Set') = ifnull(bkp.irm_ipbkpfindx_belnr,'Not Set')
                        AND ifnull(t.irm_ipbkpfindx_bukrs,'Not Set') = ifnull(bkp.irm_ipbkpfindx_bukrs,'Not Set')
                        AND ifnull(t.irm_ipbkpfindx_ipnum,'Not Set') = ifnull(bkp.irm_ipbkpfindx_ipnum,'Not Set')
                        AND ifnull(t.irm_ipbkpfindx_ipitm,0) = ifnull(bkp.irm_ipbkpfindx_ipitm,0));
CALL VECTORWISE(COMBINE 'irm_ipbkpfindx_vistex_hist');

INSERT INTO irm_ipbkpf_indx_vistex_hist
SELECT * FROM  irm_ipbkpf_indx t
    WHERE NOT EXISTS (SELECT 1
                      FROM irm_ipbkpf_indx_vistex_hist bkp
                      WHERE ifnull(t.irm_ipbkpfindx_ipnum,'Not Set') = ifnull(bkp.irm_ipbkpfindx_ipnum,'Not Set')
                        AND ifnull(t.irm_ipbkpfindx_ipitm,0) = ifnull(bkp.irm_ipbkpfindx_ipitm,0)
                        AND ifnull(t.irm_ipbkpf_belnr,'Not Set') = ifnull(bkp.irm_ipbkpf_belnr,'Not Set')
                        AND ifnull(t.irm_ipbkpf_bukrs,'Not Set') = ifnull(bkp.irm_ipbkpf_bukrs,'Not Set')
                        AND ifnull(t.irm_ipbkpf_vbtyp,'Not Set') = ifnull(bkp.irm_ipbkpf_vbtyp,'Not Set')
                        AND ifnull(t.irm_ipbkpf_gjahr,0) = ifnull(bkp.irm_ipbkpf_gjahr,0));
CALL VECTORWISE(COMBINE 'irm_ipbkpf_indx_vistex_hist');


INSERT INTO irm_ipcbdfl_vistex_hist
SELECT * FROM  irm_ipcbdfl t
    WHERE NOT EXISTS (SELECT 1
                      FROM irm_ipcbdfl_vistex_hist bkp
                      WHERE ifnull(t.irm_ipcbdfl_ipnum,'Not Set') = ifnull(bkp.irm_ipcbdfl_ipnum,'Not Set')
                        AND ifnull(t.irm_ipcbdfl_ipitm,0) = ifnull(bkp.irm_ipcbdfl_ipitm,0)
                        AND ifnull(t.irm_ipcbdfl_vbeln,'Not Set') = ifnull(bkp.irm_ipcbdfl_vbeln,'Not Set')
                        AND ifnull(t.irm_ipcbdfl_vbtyp_n,'Not Set') = ifnull(bkp.irm_ipcbdfl_vbtyp_n,'Not Set')
                        AND ifnull(t.irm_ipcbdfl_gjahr,0) = ifnull(bkp.irm_ipcbdfl_gjahr,0));
CALL VECTORWISE(COMBINE 'irm_ipcbdfl_vistex_hist');

/* IRM_IPCBHDR */
DROP TABLE IF EXISTS irm_ipcbhdr_inserts;
CREATE TABLE irm_ipcbhdr_inserts as 
SELECT * FROM irm_ipcbhdr t
WHERE EXISTS (SELECT 1 FROM IRM_IPCBHDR_vistex_hist bkp
			  WHERE ifnull(t.irm_ipcbhdr_ipnum,'Not Set') = ifnull(bkp.irm_ipcbhdr_ipnum,'Not Set')
                AND ifnull(t.irm_ipcbhdr_aedat,'0001-01-01') <> ifnull(bkp.irm_ipcbhdr_aedat,'0001-01-01')); 

DELETE FROM IRM_IPCBHDR_vistex_hist bkp
WHERE EXISTS (SELECT 1 FROM irm_ipcbhdr_inserts t
			  WHERE ifnull(t.irm_ipcbhdr_ipnum,'Not Set') = ifnull(bkp.irm_ipcbhdr_ipnum,'Not Set')
				AND ifnull(t.irm_ipcbhdr_aedat,'0001-01-01') <> ifnull(bkp.irm_ipcbhdr_aedat,'0001-01-01'));
				
INSERT INTO irm_ipcbhdr_vistex_hist
SELECT * FROM irm_ipcbhdr_inserts t
    WHERE NOT EXISTS (SELECT 1
                      FROM irm_ipcbhdr_vistex_hist bkp
                      WHERE ifnull(t.irm_ipcbhdr_ipnum,'Not Set') = ifnull(bkp.irm_ipcbhdr_ipnum,'Not Set')
						AND ifnull(t.irm_ipcbhdr_aedat,'0001-01-01') = ifnull(bkp.irm_ipcbhdr_aedat,'0001-01-01'));

INSERT INTO irm_ipcbhdr_vistex_hist
SELECT * FROM irm_ipcbhdr t
	 WHERE NOT EXISTS (SELECT 1
                      FROM irm_ipcbhdr_vistex_hist bkp
                      WHERE ifnull(t.irm_ipcbhdr_ipnum,'Not Set') = ifnull(bkp.irm_ipcbhdr_ipnum,'Not Set'));
CALL VECTORWISE(COMBINE 'irm_ipcbhdr_vistex_hist');

/* IRM_IPCBHDR */

/* IRM_IPCBITM */

DROP TABLE IF EXISTS irm_ipcbitm_inserts;
CREATE TABLE irm_ipcbitm_inserts as 
SELECT * FROM irm_ipcbitm t
WHERE EXISTS (SELECT 1 FROM irm_ipcbitm_vistex_hist bkp
			  WHERE ifnull(t.irm_ipcbitm_ipnum,'Not Set') = ifnull(bkp.irm_ipcbitm_ipnum,'Not Set')
				AND ifnull(t.irm_ipcbitm_ipitm,0) = ifnull(t.irm_ipcbitm_ipitm,0)
                AND ifnull(t.irm_ipcbitm_aedat,'0001-01-01') <> ifnull(bkp.irm_ipcbitm_aedat,'0001-01-01')); 

DELETE FROM irm_ipcbitm_vistex_hist bkp
WHERE EXISTS (SELECT 1 FROM irm_ipcbitm_inserts t
			  WHERE ifnull(t.irm_ipcbitm_ipnum,'Not Set') = ifnull(bkp.irm_ipcbitm_ipnum,'Not Set')
				AND ifnull(t.irm_ipcbitm_ipitm,0) = ifnull(bkp.irm_ipcbitm_ipitm,0)
				AND ifnull(t.irm_ipcbitm_aedat,'0001-01-01') <> ifnull(bkp.irm_ipcbitm_aedat,'0001-01-01'));
				
INSERT INTO irm_ipcbitm_vistex_hist
SELECT * FROM irm_ipcbitm_inserts t
    WHERE NOT EXISTS (SELECT 1
                      FROM irm_ipcbitm_vistex_hist bkp
                      WHERE ifnull(t.irm_ipcbitm_ipnum,'Not Set') = ifnull(bkp.irm_ipcbitm_ipnum,'Not Set')
						AND ifnull(t.irm_ipcbitm_ipitm,0) = ifnull(bkp.irm_ipcbitm_ipitm,0)
						AND ifnull(t.irm_ipcbitm_aedat,'0001-01-01') = ifnull(bkp.irm_ipcbitm_aedat,'0001-01-01'));

INSERT INTO irm_ipcbitm_vistex_hist
SELECT * FROM irm_ipcbitm t
	WHERE NOT EXISTS (SELECT 1
						FROM irm_ipcbitm_vistex_hist bkp
                      WHERE ifnull(t.irm_ipcbitm_ipnum,'Not Set') = ifnull(bkp.irm_ipcbitm_ipnum,'Not Set')
						AND ifnull(t.irm_ipcbitm_ipitm,0) = ifnull(bkp.irm_ipcbitm_ipitm,0));
CALL VECTORWISE(COMBINE 'irm_ipcbitm_vistex_hist');

/* IRM_IPCBITM */

/* IRM_IPPC */

DROP TABLE IF EXISTS irm_ippc_inserts;
CREATE TABLE irm_ippc_inserts as 
SELECT * FROM irm_ippc t
WHERE EXISTS (SELECT 1 FROM irm_ippc_vistex_hist bkp
			  WHERE ifnull(t.irm_ippchdr_pcnum,'Not Set') = ifnull(bkp.irm_ippchdr_pcnum,'Not Set')
				AND ifnull(t.irm_ippcitm_pcitm,0) = ifnull(t.irm_ippcitm_pcitm,0)
                AND ifnull(t.irm_ippchdr_aedat,'0001-01-01') <> ifnull(bkp.irm_ippchdr_aedat,'0001-01-01')); 

DELETE FROM irm_ippc_vistex_hist bkp
WHERE EXISTS (SELECT 1 FROM irm_ippc_inserts t
			  WHERE ifnull(t.irm_ippchdr_pcnum,'Not Set') = ifnull(bkp.irm_ippchdr_pcnum,'Not Set')
				AND ifnull(t.irm_ippcitm_pcitm,0) = ifnull(bkp.irm_ippcitm_pcitm,0)
				AND ifnull(t.irm_ippchdr_aedat,'0001-01-01') <> ifnull(bkp.irm_ippchdr_aedat,'0001-01-01'));
				
INSERT INTO irm_ippc_vistex_hist
SELECT * FROM irm_ippc_inserts t
    WHERE NOT EXISTS (SELECT 1
                      FROM irm_ippc_vistex_hist bkp
                      WHERE ifnull(t.irm_ippchdr_pcnum,'Not Set') = ifnull(bkp.irm_ippchdr_pcnum,'Not Set')
						AND ifnull(t.irm_ippcitm_pcitm,0) = ifnull(bkp.irm_ippcitm_pcitm,0)
						AND ifnull(t.irm_ippchdr_aedat,'0001-01-01') = ifnull(bkp.irm_ippchdr_aedat,'0001-01-01'));
						
INSERT INTO irm_ippc_vistex_hist
SELECT * FROM irm_ippc t
	 WHERE NOT EXISTS (SELECT 1
                      FROM irm_ippc_vistex_hist bkp
                      WHERE ifnull(t.irm_ippchdr_pcnum,'Not Set') = ifnull(bkp.irm_ippchdr_pcnum,'Not Set')
						AND ifnull(t.irm_ippcitm_pcitm,0) = ifnull(bkp.irm_ippcitm_pcitm,0));
CALL VECTORWISE(COMBINE 'irm_ippc_vistex_hist');

/* IRM_IPPC */

/* VBRK_VBRP, VBPA_BILL, konv */

DROP TABLE IF EXISTS vbpa_bill_inserts;
CREATE TABLE vbpa_bill_inserts as 
SELECT * FROM vbpa_bill t
WHERE EXISTS (SELECT 1 FROM vbpa_bill_vistex_hist bkp
			  WHERE ifnull(t.vbpa_vbeln,'Not Set') = ifnull(bkp.vbpa_vbeln,'Not Set')
				AND ifnull(t.vbpa_posnr,0) = ifnull(t.vbpa_posnr,0)
                AND ifnull(t.vbrk_aedat,'0001-01-01') <> ifnull(bkp.vbrk_aedat,'0001-01-01')); 

DELETE FROM vbpa_bill_vistex_hist bkp
WHERE EXISTS (SELECT 1 FROM vbpa_bill_inserts t
			  WHERE ifnull(t.vbpa_vbeln,'Not Set') = ifnull(bkp.vbpa_vbeln,'Not Set')
				AND ifnull(t.vbpa_posnr,0) = ifnull(bkp.vbpa_posnr,0)
				AND ifnull(t.vbrk_aedat,'0001-01-01') <> ifnull(bkp.vbrk_aedat,'0001-01-01'));
				
INSERT INTO vbpa_bill_vistex_hist
SELECT * FROM vbpa_bill_inserts t
    WHERE NOT EXISTS (SELECT 1
                      FROM vbpa_bill_vistex_hist bkp
                      WHERE ifnull(t.vbpa_vbeln,'Not Set') = ifnull(bkp.vbpa_vbeln,'Not Set')
						AND ifnull(t.vbpa_posnr,0) = ifnull(bkp.vbpa_posnr,0)
						AND ifnull(t.vbrk_aedat,'0001-01-01') = ifnull(bkp.vbrk_aedat,'0001-01-01'));

INSERT INTO vbpa_bill_vistex_hist
SELECT * FROM vbpa_bill t
	WHERE NOT EXISTS (SELECT 1
                      FROM vbpa_bill_vistex_hist bkp
                      WHERE ifnull(t.vbpa_vbeln,'Not Set') = ifnull(bkp.vbpa_vbeln,'Not Set')
						AND ifnull(t.vbpa_posnr,0) = ifnull(bkp.vbpa_posnr,0));
CALL VECTORWISE(COMBINE 'vbpa_bill_vistex_hist');


DROP TABLE IF EXISTS vbrk_vbrp_inserts;
CREATE TABLE vbrk_vbrp_inserts as 
SELECT VBRK_VBELN,VBRP_POSNR,VBRP_FKIMG,VBRP_VRKME,VBRP_KZWI6,VBRP_FBUDA,VBRP_PRSDT,VBRP_PRCTR,VBRP_NETWR,VBRP_SHKZG,VBRP_BRTWR,VBRK_KUNRG,VBRP_KNUMA_AG,VBRK_ERDAT,VBRK_AEDAT
FROM vbrk_vbrp t
WHERE EXISTS (SELECT 1 FROM VBRK_VBRP_vistex_hist bkp
			  WHERE ifnull(t.vbrk_vbeln,'Not Set') = ifnull(bkp.vbrk_vbeln,'Not Set')
				AND ifnull(t.vbrp_posnr,0) = ifnull(t.vbrp_posnr,0)
                AND ifnull(t.vbrk_aedat,'0001-01-01') <> ifnull(bkp.vbrk_aedat,'0001-01-01')); 

DELETE FROM vbrk_vbrp_vistex_hist bkp
WHERE EXISTS (SELECT 1 FROM vbrk_vbrp_inserts t
			  WHERE ifnull(t.vbrk_vbeln,'Not Set') = ifnull(bkp.vbrk_vbeln,'Not Set')
				AND ifnull(t.vbrp_posnr,0) = ifnull(bkp.vbrp_posnr,0)
				AND ifnull(t.vbrk_aedat,'0001-01-01') <> ifnull(bkp.vbrk_aedat,'0001-01-01'));
				
INSERT INTO vbrk_vbrp_vistex_hist
SELECT * FROM vbrk_vbrp_inserts t
    WHERE NOT EXISTS (SELECT 1
                      FROM vbrk_vbrp_vistex_hist bkp
                      WHERE ifnull(t.vbrk_vbeln,'Not Set') = ifnull(bkp.vbrk_vbeln,'Not Set')
						AND ifnull(t.vbrp_posnr,0) = ifnull(bkp.vbrp_posnr,0)
						AND ifnull(t.vbrk_aedat,'0001-01-01') = ifnull(bkp.vbrk_aedat,'0001-01-01'));
						
INSERT INTO vbrk_vbrp_vistex_hist
SELECT VBRK_VBELN,VBRP_POSNR,VBRP_FKIMG,VBRP_VRKME,VBRP_KZWI6,VBRP_FBUDA,VBRP_PRSDT,VBRP_PRCTR,VBRP_NETWR,VBRP_SHKZG,VBRP_BRTWR,VBRK_KUNRG,VBRP_KNUMA_AG,VBRK_ERDAT,VBRK_AEDAT
FROM vbrk_vbrp t
    WHERE NOT EXISTS (SELECT 1
                      FROM vbrk_vbrp_vistex_hist bkp
                      WHERE ifnull(t.vbrk_vbeln,'Not Set') = ifnull(bkp.vbrk_vbeln,'Not Set')
						AND ifnull(t.vbrp_posnr,0) = ifnull(bkp.vbrp_posnr,0));					
						
call vectorwise (combine 'vbrk_vbrp_vistex_hist');

INSERT INTO konv_vistex_hist (konv_knumv,konv_kposn,konv_kbetr,konv_kschl,konv_kwert,konv_kdatu)
SELECT konv_knumv,konv_kposn,konv_kbetr,konv_kschl,konv_kwert,konv_kdatu
FROM  konv t
    WHERE NOT EXISTS (SELECT 1
                      FROM konv_vistex_hist bkp
                      WHERE ifnull(t.konv_knumv,'Not Set') = ifnull(bkp.konv_knumv,'Not Set')
                      	AND ifnull(t.konv_kposn,0) = ifnull(bkp.konv_kposn,0)
                      	AND ifnull(t.konv_kschl,'Not Set') = ifnull(bkp.konv_kschl,'Not Set'))
    AND t.KONV_KSCHL in ('ZVCC','ZBCT');
CALL VECTORWISE(COMBINE 'konv_vistex_hist');


/* END OF DELTAS */



delete from NUMBER_FOUNTAIN where table_name = 'fact_chargebackvendor';


INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_chargebackvendor',ifnull(max(f.fact_chargebackvendorid ), 
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_chargebackvendor f;

DROP TABLE IF EXISTS tmp_var_chargebackvendor_fact;
CREATE table tmp_var_chargebackvendor_fact (
	pGlobalCurrency) AS
Select  CAST(ifnull((SELECT property_value
			FROM systemproperty
			WHERE property = 'customer.global.currency'),
		'USD'),varchar(3));
		
/* drop table if exists fact_chargebackvendorwoupdate*/
drop table if exists tmp_billing_chargebackbvendor;
drop table if exists tmp_billing_chargebackbvendor_filtered;
drop table if exists temp_konv_cbv;
drop table if exists temp_vbrk_vbrp_cbv;
drop table if exists temp_vbrk_konv_cbv;
drop table if exists tmp_billing_cbv_stg_filtered;

/*
create table tmp_billing_chargebackbvendor as
SELECT DISTINCT fb.dd_billing_no, fb.dd_billing_item_no
FROM fact_billing fb, dim_billingdocumenttype dtp
where fb.dim_documenttypeid = dtp.dim_billingdocumenttypeid
and dtp.dim_billingdocumenttypeid in (select dim_billingdocumenttypeid 
					From dim_billingdocumenttype 
					where type in ('YRST', 'ZFV', 'ZGCM', 'ZGRB', 'ZLRB', 'ZRAN', 'ZRDM', 'ZRE', 'ZRST') )
AND NOT EXISTS (SELECT fcv.dd_billing_no,fcv.dd_billing_item_no FROM fact_chargebackvendor fcv 
where fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no)

create table temp_konv_cbv as
select distinct KONV_KNUMV,KONV_KPOSN from konv where KONV_KSCHL in ('ZVCC','ZBCT')

create table temp_vbrk_vbrp_cbv as 
SELECT distinct vbrk_vbeln, vbrp_posnr,VBRK_KNUMV FROM vbrk_vbrp --where vbrp_kzwi6>0
where vbrk_fkart IN ('YRST', 'ZFV', 'ZGCM', 'ZGRB', 'ZLRB', 'ZRAN', 'ZRDM', 'ZRE', 'ZRST')
*/
/* 28May2015 - COnverting inner join to left join b/w Billing and KONV tables to bring the Billing Docs even if there is no correspoding Condition Types */

/*
create table temp_vbrk_konv_cbv as 
SELECT distinct vbrk_vbeln, vbrp_posnr 
FROM temp_vbrk_vbrp_cbv vbk,temp_konv_cbv k
WHERE vbk.VBRK_KNUMV = k.KONV_KNUMV AND vbk.VBRP_POSNR = k.KONV_KPOSN
FROM temp_vbrk_vbrp_cbv vbk LEFT JOIN temp_konv_cbv k
ON vbk.VBRK_KNUMV = k.KONV_KNUMV AND vbk.VBRP_POSNR = k.KONV_KPOSN

create table tmp_billing_chargebackbvendor_filtered as
SELECT DISTINCT fb.dd_billing_no, fb.dd_billing_item_no
FROM tmp_billing_chargebackbvendor fb,temp_vbrk_konv_cbv vbk
where vbk.vbrk_vbeln = fb.dd_billing_no and vbk.vbrp_posnr = fb.dd_billing_item_no
*/
/* Include vbeln and vbtyp_n fields from Doc Flow table */	   
/* Suchithra 04302015 - IPNUM in DFL table could be set to 0, including BKPFINDX table to not to missout on Accting Doc nums */
/* Suchithra 05112015 - Updated join to include billing docs even if there is no associated chargeback */


/*
DELETE FROM fact_chargebackvendor fcv WHERE EXISTS (SELECT 1
												FROM IRM_IPCBHDR_INSERTS t1
												WHERE   ifnull(t1.IRM_IPCBHDR_IPNUM,'Not Set') = ifnull(fcv.dd_ipnum,'Not Set'))
*/
												
DELETE FROM fact_chargebackvendor fcv WHERE EXISTS (SELECT 1
												FROM	IRM_IPCBITM_INSERTS t1
												WHERE   ifnull(t1.IRM_IPCBITM_IPNUM,'Not Set') = ifnull(fcv.dd_ipnum,'Not Set')
													AND ifnull(t1.IRM_IPCBITM_IPITM,0) = ifnull(fcv.dd_ipitem,0)
													);
										

/*													
DELETE FROM fact_chargebackvendor fcv WHERE EXISTS (SELECT 1
												FROM IRM_IPPC_INSERTS t1
												WHERE   ifnull(t1.IRM_IPPCHDR_PCNUM,'Not Set') = ifnull(fcv.dd_pcnum,'Not Set'))
												
DELETE FROM fact_chargebackvendor fcv WHERE EXISTS (SELECT 1
												FROM vbpa_bill_inserts t1
												WHERE   ifnull(t1.vbpa_vbeln,'Not Set') = ifnull(fcv.dd_ipnum,'Not Set')
													AND ifnull(t1.vbpa_posnr,0) = ifnull(fcv.dd_ipitem,0))
													
DELETE FROM fact_chargebackvendor fcv WHERE EXISTS (SELECT 1
												FROM vbrk_vbrp_inserts t1
												WHERE   ifnull(t1.vbrk_vbeln,'Not Set') = ifnull(fcv.dd_ipnum,'Not Set')
													AND ifnull(t1.vbrp_posnr,0) = ifnull(fcv.dd_ipitem,0))
*/


create table tmp_billing_cbv_stg_filtered as
SELECT 
	distinct fb.dd_billing_no, fb.dd_billing_item_no
	,stg.IRM_IPCBITM_IPNUM dd_ipnum, stg.IRM_IPCBITM_IPITM dd_ipitem
	,dfl.IRM_IPCBDFL_VBELN dd_docflowacctdocno, dfl.IRM_IPCBDFL_VBTYP_N dd_subsequentcategory
	,indx.irm_ipbkpfindx_belnr dd_AccountingDocNo
FROM 
	IRM_IPCBITM_vistex_hist stg 
INNER JOIN  IRM_IPCBDFL_vistex_hist dfl
	ON stg.irm_ipcbitm_ipnum = dfl.irm_ipcbdfl_ipnum 
INNER JOIN IRM_IPBKPFINDX_vistex_hist indx
	ON indx.irm_ipbkpfindx_ipnum = stg.irm_ipcbitm_ipnum 
	AND indx.irm_ipbkpfindx_ipitm = stg.irm_ipcbitm_ipitm
	AND indx.irm_ipbkpfindx_belnr = dfl.IRM_IPCBDFL_VBELN
	AND indx.irm_ipbkpfindx_gjahr = dfl.irm_ipcbdfl_gjahr
	AND indx.irm_ipbkpfindx_bukrs = dfl.irm_ipcbdfl_bukrs
	AND indx.irm_ipbkpfindx_vbtyp = dfl.irm_ipcbdfl_vbtyp_n
RIGHT JOIN fact_billing fb
	ON fb.dd_billing_no = stg.IRM_IPCBITM_ZZINV
	AND fb.dd_billing_item_no = stg.IRM_IPCBITM_ZZINV_ITM
INNER JOIN dim_billingdocumenttype dtp
	ON fb.dim_documenttypeid = dtp.dim_billingdocumenttypeid
LEFT JOIN konv_vistex_hist knv
	ON fb.dd_ConditionNo = knv.KONV_KNUMV AND fb.dd_billing_item_no = knv.KONV_KPOSN
WHERE 
	/* dtp.type in ('YRST', 'ZFV', 'ZGCM', 'ZGRB', 'ZLRB', 'ZRAN', 'ZRDM', 'ZRE', 'ZRST')
	AND knv.KONV_KSCHL in ('ZVCC','ZBCT') 
	AND */
	NOT EXISTS (SELECT 1 
				FROM fact_chargebackvendor fcv 
				where fcv.dd_zzinv = fb.dd_billing_no
				and fcv.dd_zzinv_itm = fb.dd_billing_item_no
				and fcv.dd_ipnum = stg.IRM_IPCBITM_IPNUM
				and fcv.dd_ipitem = stg.IRM_IPCBITM_IPITM
				and fcv.dd_AccountingDocNo = indx.irm_ipbkpfindx_belnr
				and fcv.dd_docflowacctdocno = dfl.IRM_IPCBDFL_VBELN
				and fcv.dd_subsequentcategory =  dfl.IRM_IPCBDFL_VBTYP_N
				 );
	   
INSERT INTO fact_chargebackvendor(fact_chargebackvendorid,
									dd_zzinv,
									dd_zzinv_itm,
									dd_ipnum,
									dd_ipitem
									,dd_docflowacctdocno
									,dd_subsequentcategory
									,dd_AccountingDocNo
									,is_today
									)
		SELECT (SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_chargebackvendor') + row_number() over() fact_chargebackvendorid,
									ifnull(dd_billing_no,'Not Set'),
									ifnull(dd_billing_item_no,0),
									ifnull(dd_ipnum,'Not Set'),
									ifnull(dd_ipitem,0)
									,ifnull(dd_docflowacctdocno,'Not Set')
									,ifnull(dd_subsequentcategory,'Not Set')
									,ifnull(dd_AccountingDocNo,'Not Set')
									,current_date
		FROM tmp_billing_cbv_stg_filtered;

	

call vectorwise(combine 'fact_chargebackvendor');

/*
create table fact_chargebackvendorwoupdate as select * from fact_chargebackvendor where 1=0
call vectorwise(combine 'fact_chargebackvendorwoupdate+fact_chargebackvendor')
*/

/* Billing and Vendor branch : CB HEADER and ITEM into VBRK,VBRP,KONV,VBPA,LFA1 */

drop table if exists temp_konv_cbv_agg;
create table temp_konv_cbv_agg as 
select 
KONV_KNUMV,KONV_KPOSN,KONV_KSCHL,
SUM(KONV_KBETR) as SUM_KBETR,
SUM(KONV_KWERT) as SUM_KWERT
from konv_vistex_hist k
where
exists(
select 1 from fact_billing fb, fact_chargebackvendor fcv
where
fb.dd_ConditionNo = k.KONV_KNUMV
and fb.dd_billing_item_no = k.KONV_KPOSN
AND  fb.dd_billing_no = fcv.dd_zzinv
	AND fb.dd_billing_item_no = fcv.dd_zzinv_itm	
	and fcv.is_today = current_date
)
group by KONV_KNUMV,KONV_KPOSN,KONV_KSCHL;	
	

UPDATE fact_chargebackvendor fcv
FROM   temp_konv_cbv_agg k, fact_billing fb
SET     
		 fcv.amt_rate = ifnull(k.SUM_KBETR,0),
		 dw_update_date = CURRENT_TIMESTAMP
WHERE 	
fb.dd_ConditionNo = k.KONV_KNUMV and fb.dd_billing_item_no = k.KONV_KPOSN
	AND fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
/*vbk.VBRK_KNUMV = k.KONV_KNUMV and vbk.VBRP_POSNR = k.KONV_KPOSN
	AND fcv.dd_zzinv = vbk.VBRK_VBELN and fcv.dd_zzinv_itm = vbk.VBRP_POSNR
	AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR */
	and k.KONV_KSCHL = 'ZBCT' and fcv.amt_rate <> ifnull(k.SUM_KBETR,0);
	
UPDATE fact_chargebackvendor fcv
FROM   temp_konv_cbv_agg k , fact_billing fb
/* , VBRK_VBRP_CEXTR vbk*/
SET     
		 fcv.amt_total = ifnull(k.SUM_KWERT,0),
		 dw_update_date = CURRENT_TIMESTAMP
WHERE
fb.dd_ConditionNo = k.KONV_KNUMV and fb.dd_billing_item_no = k.KONV_KPOSN
	AND fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no 	
/*vbk.VBRK_KNUMV = k.KONV_KNUMV and vbk.VBRP_POSNR = k.KONV_KPOSN
	AND fcv.dd_zzinv = vbk.VBRK_VBELN and fcv.dd_zzinv_itm = vbk.VBRP_POSNR
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR */
	and k.KONV_KSCHL = 'ZBCT' and fcv.amt_total <> ifnull(k.SUM_KWERT,0);
	
UPDATE fact_chargebackvendor fcv
FROM   temp_konv_cbv_agg k , fact_billing fb
/* , VBRK_VBRP_CEXTR vbk*/
SET     
		 fcv.amt_BuyCost = ifnull(k.SUM_KWERT,0),
		 dw_update_date = CURRENT_TIMESTAMP
WHERE 	fb.dd_ConditionNo = k.KONV_KNUMV and fb.dd_billing_item_no = k.KONV_KPOSN
	AND fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
/*vbk.VBRK_KNUMV = k.KONV_KNUMV and vbk.VBRP_POSNR = k.KONV_KPOSN
	AND fcv.dd_zzinv = vbk.VBRK_VBELN and fcv.dd_zzinv_itm = vbk.VBRP_POSNR
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR */
	and k.KONV_KSCHL = 'ZBCT' and fcv.amt_BuyCost <> ifnull(k.SUM_KWERT,0);
	

UPDATE fact_chargebackvendor fcv
FROM   temp_konv_cbv_agg k , fact_billing fb
/* , VBRK_VBRP_CEXTR vbk*/
SET     
		 fcv.amt_rate = ifnull(k.SUM_KBETR,0),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE
fb.dd_ConditionNo = k.KONV_KNUMV and fb.dd_billing_item_no = k.KONV_KPOSN
	AND fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no 	
/*vbk.VBRK_KNUMV = k.KONV_KNUMV and vbk.VBRP_POSNR = k.KONV_KPOSN
	AND fcv.dd_zzinv = vbk.VBRK_VBELN and fcv.dd_zzinv_itm = vbk.VBRP_POSNR
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR */
	and k.KONV_KSCHL = 'ZVCC' and fcv.amt_rate <> ifnull(k.SUM_KBETR,0)	;
	
UPDATE fact_chargebackvendor fcv
FROM   temp_konv_cbv_agg k , fact_billing fb
/* , VBRK_VBRP_CEXTR vbk*/
SET     
		 fcv.amt_total = ifnull(k.SUM_KWERT,0),
		 dw_update_date = CURRENT_TIMESTAMP							
WHERE 	
fb.dd_ConditionNo = k.KONV_KNUMV and fb.dd_billing_item_no = k.KONV_KPOSN
	AND fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/*vbk.VBRK_KNUMV = k.KONV_KNUMV and vbk.VBRP_POSNR = k.KONV_KPOSN
	AND fcv.dd_zzinv = vbk.VBRK_VBELN and fcv.dd_zzinv_itm = vbk.VBRP_POSNR
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR */
	and k.KONV_KSCHL = 'ZVCC' and fcv.amt_total <> ifnull(k.SUM_KWERT,0);
	
UPDATE fact_chargebackvendor fcv
FROM   temp_konv_cbv_agg k , fact_billing fb
/* , VBRK_VBRP_CEXTR vbk*/
SET     
		 fcv.amt_ContractPrice = ifnull(k.SUM_KWERT,0),
		 dw_update_date = CURRENT_TIMESTAMP							
WHERE 	
fb.dd_ConditionNo = k.KONV_KNUMV and fb.dd_billing_item_no = k.KONV_KPOSN
	AND fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/*vbk.VBRK_KNUMV = k.KONV_KNUMV and vbk.VBRP_POSNR = k.KONV_KPOSN
	AND fcv.dd_zzinv = vbk.VBRK_VBELN and fcv.dd_zzinv_itm = vbk.VBRP_POSNR
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR */
	and k.KONV_KSCHL = 'ZVCC' and fcv.amt_ContractPrice <> ifnull(k.SUM_KWERT,0)	;

UPDATE fact_chargebackvendor fcv
SET fcv.amt_cbexpected = fcv.amt_BuyCost - fcv.amt_ContractPrice,
	dw_update_date = CURRENT_TIMESTAMP
WHERE fcv.amt_cbexpected <> fcv.amt_BuyCost - fcv.amt_ContractPrice
AND fcv.is_today = current_date;

UPDATE fact_chargebackvendor fcv
SET fcv.amt_cbexpected = 0,
	dw_update_date = CURRENT_TIMESTAMP
WHERE fcv.amt_ContractPrice = 0 
AND fcv.amt_cbexpected <> 0
AND fcv.is_today = current_date;		 

	
drop table if exists temp_konv_cbv_agg;

UPDATE fact_chargebackvendor fcv
FROM   fact_billing fb 
/*, vbrk_vbrp vbk*/
SET     fcv.dd_billing_no = ifnull(fb.dd_billing_no,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/*AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR*/
	AND fcv.dd_billing_no <> ifnull(fb.dd_billing_no,'Not Set')
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   fact_billing fb /*, vbrk_vbrp vbk*/
SET     fcv.dd_billing_item_no = ifnull(fb.dd_billing_item_no,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR*/
	AND fcv.dd_billing_item_no <> ifnull(fb.dd_billing_item_no,0)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   fact_billing fb /*, vbrk_vbrp vbk*/
SET     fcv.dim_documenttypeid = ifnull(fb.dim_documenttypeid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR*/
	AND fcv.dim_documenttypeid <> ifnull(fb.dim_documenttypeid,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   fact_billing fb /*, vbrk_vbrp vbk*/
SET     fcv.dim_dateidbilling = ifnull(fb.dim_dateidbilling,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR*/
	AND fcv.dim_dateidbilling <> ifnull(fb.dim_dateidbilling,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   fact_billing fb /*, vbrk_vbrp vbk*/
SET     fcv.Dim_DateidCreated = ifnull(fb.Dim_DateidCreated,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR*/
	AND fcv.Dim_DateidCreated <> ifnull(fb.Dim_DateidCreated,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   fact_billing fb /*, vbrk_vbrp vbk*/
SET     fcv.dim_plantid = ifnull(fb.dim_plantid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR*/
	AND fcv.dim_plantid <> ifnull(fb.dim_plantid,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   fact_billing fb /*, vbrk_vbrp vbk*/
SET     fcv.Dim_Companyid = ifnull(fb.Dim_Companyid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR*/
	AND fcv.Dim_Companyid <> ifnull(fb.Dim_Companyid,1)
	AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM   fact_billing fb /*, vbrk_vbrp vbk*/
SET     fcv.dim_partid = ifnull(fb.dim_partid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_zzinv = fb.dd_billing_no and fcv.dd_zzinv_itm = fb.dd_billing_item_no
	/* AND fb.dd_billing_no = vbk.VBRK_VBELN AND fb.dd_billing_item_no = vbk.VBRP_POSNR*/
	AND fcv.dim_partid <> ifnull(fb.dim_partid,1)
	AND is_today = current_date;	


/* VBRK_VBRP updates */
UPDATE fact_chargebackvendor fcv
FROM   vbrk_vbrp_vistex_hist vkp
SET     fcv.ct_InvoicedQty = ifnull(vkp.VBRP_FKIMG,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   fcv.dd_zzinv = vkp.VBRK_VBELN and fcv.dd_zzinv_itm = vkp.VBRP_POSNR
	AND fcv.ct_InvoicedQty <> ifnull(vkp.VBRP_FKIMG,0)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   vbrk_vbrp_vistex_hist vkp
SET     fcv.dd_UoM = ifnull(vkp.VBRP_VRKME,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   fcv.dd_zzinv = vkp.VBRK_VBELN and fcv.dd_zzinv_itm = vkp.VBRP_POSNR
	AND fcv.dd_UoM <> ifnull(vkp.VBRP_VRKME,'Not Set')
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   vbrk_vbrp_vistex_hist vkp
SET     fcv.dd_cbbestcontractprice = ifnull(vkp.VBRP_KZWI6,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   fcv.dd_zzinv = vkp.VBRK_VBELN and fcv.dd_zzinv_itm = vkp.VBRP_POSNR
	AND fcv.dd_cbbestcontractprice <> ifnull(vkp.VBRP_KZWI6,0)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM  vbrk_vbrp_vistex_hist vkp, dim_date dt, dim_plant pl
SET   dim_dateidservice = ifnull(dt.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_zzinv = vkp.VBRK_VBELN and fcv.dd_zzinv_itm = vkp.VBRP_POSNR
	AND fcv.dim_plantid = pl.dim_plantid
	AND dt.DateValue = VBRP_FBUDA
	AND dt.CompanyCode = pl.CompanyCode
	AND dim_dateidservice <> ifnull(dt.dim_dateid,1)
	AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM  vbrk_vbrp_vistex_hist vkp, dim_date dt, dim_plant pl
SET  dim_DateidPriceExRate = ifnull(dt.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_zzinv = vkp.VBRK_VBELN and fcv.dd_zzinv_itm = vkp.VBRP_POSNR
	AND dt.DateValue = VBRP_PRSDT
	AND fcv.dim_plantid = pl.dim_plantid
	AND dt.CompanyCode = pl.CompanyCode
	AND dim_DateidPriceExRate <> ifnull(dt.dim_dateid,1)
	AND is_today = current_date;
	
update fact_chargebackvendor fcv
from VBPA_BILL_vistex_hist v, DIM_VENDOR dv
set fcv.dim_vendorid = ifnull(dv.dim_vendorid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
where     fcv.dd_zzinv = ifnull(v.VBPA_VBELN,'Not Set') and fcv.dd_zzinv_itm = ifnull(v.VBPA_POSNR,0)
	  and v.VBPA_LIFNR = dv.vendornumber
	  and fcv.dim_vendorid <> ifnull(dv.dim_vendorid,1)
	  AND is_today = current_date; 
	  
/* 15 May 2015 : Profit Center from VBRP_PRCTR instead of CB ITM table */
update fact_chargebackvendor fcv
from VBRK_VBRP_vistex_hist vkp, dim_profitcenter d
set fcv.dim_profitcenterid = ifnull(d.dim_profitcenterid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
where     fcv.dd_zzinv = ifnull(vkp.VBRK_VBELN,'Not Set') and fcv.dd_zzinv_itm = ifnull(vkp.VBRP_POSNR,0)
	  and ifnull(vkp.VBRP_PRCTR,'Not Set') = d.profitcentercode
	  and fcv.dim_profitcenterid <> ifnull(d.dim_profitcenterid,1)
	  AND is_today = current_date;
	  
/* 18 May 2015 : New field added */
UPDATE fact_chargebackvendor fcv
FROM vbrk_vbrp_vistex_hist vkp
SET amt_netvalueinvoice =  VBRP_NETWR * (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END),
		 dw_update_date = CURRENT_TIMESTAMP	
where fcv.dd_zzinv = ifnull(vkp.VBRK_VBELN,'Not Set') and fcv.dd_zzinv_itm = ifnull(vkp.VBRP_POSNR,0)
AND ifnull(amt_netvalueinvoice,-1) <> (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_NETWR
AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM vbrk_vbrp_vistex_hist vkp
SET amt_grossvalueinvoice = VBRP_BRTWR * (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END),
		 dw_update_date = CURRENT_TIMESTAMP	
where fcv.dd_zzinv = ifnull(vkp.VBRK_VBELN,'Not Set') and fcv.dd_zzinv_itm = ifnull(vkp.VBRP_POSNR,0)
AND ifnull(amt_grossvalueinvoice,-1) <> (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_BRTWR
AND is_today = current_date;


UPDATE fact_chargebackvendor fcv
FROM VBRK_VBRP_vistex_hist vkp, dim_customer d
SET fcv.dim_customerid = ifnull(d.dim_customerid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   fcv.dd_zzinv = ifnull(vkp.VBRK_VBELN,'Not Set') and fcv.dd_zzinv_itm = ifnull(vkp.VBRP_POSNR,0)
	AND d.customernumber = ifnull(vkp.VBRK_KUNRG,'Not Set')
	AND fcv.dim_customerid <> ifnull(d.dim_customerid,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM VBRK_VBRP_vistex_hist vkp, dim_agreements d
SET fcv.dim_invoicecontractid = ifnull(d.dim_agreementsid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_zzinv = ifnull(vkp.VBRK_VBELN,'Not Set') and fcv.dd_zzinv_itm = ifnull(vkp.VBRP_POSNR,0)
	AND d.Agreement = ifnull(vkp.VBRP_KNUMA_AG,'Not Set')
	AND fcv.dim_invoicecontractid <> ifnull(d.dim_agreementsid,1)
	AND is_today = current_date;
	  
/* END OF VBRK_VBRP updates */


/* Updates from /IRM/IPCBITM and /IRM/IPCBHDR (main tables) */ 

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPCBITM_vistex_hist itm
SET    fcv.amt_netvalueitem = ifnull(itm.IRM_IPCBITM_NETWR,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE  dd_ipnum = itm.IRM_IPCBITM_IPNUM AND dd_ipitem = itm.IRM_IPCBITM_IPITM
	AND fcv.amt_netvalueitem <> ifnull(IRM_IPCBITM_NETWR,0)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBITM_vistex_hist itm, dim_agreements d
SET fcv.dim_agreementsid = ifnull(d.dim_agreementsid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = IRM_IPCBITM_IPITM
	AND d.Agreement = ifnull(itm.IRM_IPCBITM_KNUMA_AG,'Not Set')
	AND fcv.dim_agreementsid <> ifnull(d.dim_agreementsid,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBHDR_vistex_hist a, dim_date d, dim_plant pl
SET fcv.dim_dateidDocDate = ifnull(d.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBHDR_AUDAT AND d.CompanyCode = IRM_IPCBHDR_BUKRS
	AND fcv.dim_plantid = pl.dim_plantid
	AND d.CompanyCode = pl.CompanyCode
	AND fcv.dim_dateidDocDate <> ifnull(d.dim_dateid,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBHDR_vistex_hist a, dim_date d, dim_plant pl
SET fcv.dim_dateidCBPostingDate = ifnull(d.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBHDR_BUDAT AND d.CompanyCode = a.IRM_IPCBHDR_BUKRS
	AND fcv.dim_plantid = pl.dim_plantid
	AND d.CompanyCode = pl.CompanyCode
	AND fcv.dim_dateidCBPostingDate <> ifnull(d.dim_dateid,1)
	AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBHDR_vistex_hist a, dim_date d, dim_plant pl
SET fcv.dim_DateidHdrCreated = ifnull(d.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBHDR_ERDAT AND d.CompanyCode = a.IRM_IPCBHDR_BUKRS
	AND fcv.dim_plantid = pl.dim_plantid
	AND d.CompanyCode = pl.CompanyCode
	AND fcv.dim_DateidHdrCreated <> ifnull(d.dim_dateid,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBHDR_vistex_hist a, dim_chargebacktypes d
SET fcv.dim_chargebacktypesid = ifnull(d.dim_chargebacktypesid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.Type = ifnull(a.IRM_IPCBHDR_IPTYP,'Not Set')
	AND fcv.dim_chargebacktypesid <> ifnull(d.dim_chargebacktypesid,1)
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBITM_vistex_hist itm, dim_chargebackcontract d
SET fcv.dim_chargebackcontractid = d.dim_chargebackcontractid,
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = IRM_IPCBITM_IPITM
	AND d.Agreement = ifnull(itm.IRM_IPCBITM_KNUMA_AG,'Not Set')
	AND fcv.dim_chargebackcontractid <> d.dim_chargebackcontractid
	AND is_today = current_date;

/* 15 May : Chargeback Vendor */
update fact_chargebackvendor fcv
from IRM_IPCBHDR_vistex_hist v, DIM_VENDOR dv
set fcv.dim_cbvendorid = ifnull(dv.dim_vendorid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
where     fcv.dd_ipnum = IRM_IPCBHDR_IPNUM
	  and v.IRM_IPCBHDR_PAYER = dv.vendornumber
	  and fcv.dim_cbvendorid <> ifnull(dv.dim_vendorid,1)
	  AND is_today = current_date; 
	  
/* 15 May : New request */	  
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBITM_vistex_hist a, IRM_IPCBHDR_vistex_hist b, dim_date d, dim_plant pl
SET fcv.dim_dateidcblineprice = ifnull(d.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = a.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_PRSDT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND fcv.dim_plantid = pl.dim_plantid
	AND d.CompanyCode = pl.CompanyCode
	AND fcv.dim_dateidcblineprice <> ifnull(d.dim_dateid,1)
	AND is_today = current_date;
	
/* 15 May : New request */
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBITM_vistex_hist a, IRM_IPCBHDR_vistex_hist b, dim_date d, dim_plant pl
SET fcv.dim_dateidpark = ifnull(d.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = a.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_PARKDT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND fcv.dim_plantid = pl.dim_plantid
	AND d.CompanyCode = pl.CompanyCode
	AND fcv.dim_dateidpark <> ifnull(d.dim_dateid,1)
	AND is_today = current_date;
	
/* 15 May : New request */
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBITM_vistex_hist a, IRM_IPCBHDR_vistex_hist b, dim_date d, dim_plant pl
SET fcv.dim_dateidsettlement = ifnull(d.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = a.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_SETLDT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND fcv.dim_plantid = pl.dim_plantid
	AND d.CompanyCode = pl.CompanyCode
	AND fcv.dim_dateidsettlement <> ifnull(d.dim_dateid,1)
	AND is_today = current_date;
	
/* 18 May : New request */
UPDATE fact_chargebackvendor fcv
FROM IRM_IPCBITM_vistex_hist a, IRM_IPCBHDR_vistex_hist b, dim_date d, dim_plant pl
SET fcv.dim_dateidservicerend = ifnull(d.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE fcv.dd_ipnum = a.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_FBUDA AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND fcv.dim_plantid = pl.dim_plantid
	AND d.CompanyCode = pl.CompanyCode
	AND fcv.dim_dateidservicerend <> ifnull(d.dim_dateid,1)
	AND is_today = current_date;
	
/* 19 May : CB Buy Cost, CB Contract Cost, CB Line Amount */
UPDATE 	fact_chargebackvendor fcv
FROM 	IRM_IPCBITM_vistex_hist a
SET  	fcv.amt_cbbuyc = ifnull(a.IRM_IPCBITM_KZWI1,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = a.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND fcv.amt_cbbuyc <> ifnull(a.IRM_IPCBITM_KZWI1,0)
	AND is_today = current_date;
	
UPDATE 	fact_chargebackvendor fcv
FROM 	IRM_IPCBITM_vistex_hist a
SET  	fcv.amt_cbcontractc = ifnull(a.IRM_IPCBITM_KZWI3,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = a.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND fcv.amt_cbcontractc <> ifnull(a.IRM_IPCBITM_KZWI3,0)
	AND is_today = current_date;
	
UPDATE 	fact_chargebackvendor fcv
FROM 	IRM_IPCBITM_vistex_hist a
SET  	fcv.amt_cblineamt = ifnull(a.IRM_IPCBITM_KZWI5,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = a.IRM_IPCBITM_IPNUM AND fcv.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND fcv.amt_cblineamt <> ifnull(a.IRM_IPCBITM_KZWI5,0)
	AND is_today = current_date;
	
call vectorwise(combine 'fact_chargebackvendor');

						
/* IRM_IPBKPFINDX (IPNUM,IPITM) */
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPFINDX_vistex_hist ndx
SET 	fcv.dd_dcindicator = ifnull(ndx.IRM_IPBKPFINDX_DCIND,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = ndx.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = ndx.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_dcindicator <> ifnull(ndx.IRM_IPBKPFINDX_DCIND,'Not Set')
	AND fcv.dd_AccountingDocNo = ndx.IRM_IPBKPFINDX_BELNR and fcv.dd_subsequentcategory = ndx.irm_ipbkpfindx_vbtyp
	AND is_today = current_date;	/* Added on 30Apr2015*/	/*vbtyp added*/	

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPFINDX_vistex_hist ndx
SET 	fcv.dd_accfiscalyear = ifnull(ndx.IRM_IPBKPFINDX_GJAHR,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = ndx.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = ndx.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_accfiscalyear <> ifnull(ndx.IRM_IPBKPFINDX_GJAHR,0)
	AND fcv.dd_AccountingDocNo = ndx.IRM_IPBKPFINDX_BELNR and fcv.dd_subsequentcategory = ndx.irm_ipbkpfindx_vbtyp
	AND is_today = current_date;	/* Added on 30Apr2015*/	/*vbtyp added*/	

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPFINDX_vistex_hist ndx
SET 	fcv.amt_netvalueaccount = ifnull(ndx.IRM_IPBKPFINDX_NETWR,0) * (CASE WHEN dd_dcindicator = 'H' THEN -1 ELSE 1 END),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = ndx.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = ndx.IRM_IPBKPFINDX_IPITM
	AND fcv.amt_netvalueaccount <> ifnull(ndx.IRM_IPBKPFINDX_NETWR,0) * (CASE WHEN dd_dcindicator = 'H' THEN -1 ELSE 1 END)
	AND fcv.dd_AccountingDocNo = ndx.IRM_IPBKPFINDX_BELNR and fcv.dd_subsequentcategory = ndx.irm_ipbkpfindx_vbtyp
	AND is_today = current_date;	/* Added on 30Apr2015*/	/*vbtyp added*/	

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPFINDX_vistex_hist ndx
SET 	fcv.dd_accountingdocumentcategory = ifnull(ndx.IRM_IPBKPFINDX_VBTYP,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = ndx.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = ndx.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_accountingdocumentcategory <> ifnull(ndx.IRM_IPBKPFINDX_VBTYP,'Not Set')
	AND fcv.dd_AccountingDocNo = ndx.IRM_IPBKPFINDX_BELNR and fcv.dd_subsequentcategory = ndx.irm_ipbkpfindx_vbtyp
	AND is_today = current_date;	/* Added on 30Apr2015*/	/*vbtyp added*/	

	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPFINDX_vistex_hist ndx, dim_currency dcr
SET    fcv.dim_currencyid = ifnull(dcr.dim_currencyid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   dcr.CurrencyCode = ifnull(ndx.IRM_IPBKPFINDX_WAERS,'Not Set')
	AND fcv.dd_ipnum = ndx.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = ndx.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_AccountingDocNo = ndx.IRM_IPBKPFINDX_BELNR and fcv.dd_subsequentcategory = ndx.irm_ipbkpfindx_vbtyp  /* Added on 30Apr2015*/ /*vbtyp added*/	
	AND fcv.dim_currencyid <> ifnull(dcr.dim_currencyid,1)
	AND is_today = current_date;
/* IRM_IPBKPFINDX (IPNUM,IPITM) */


/* IRM_IPBKPF (BUKRS,IPITM,GJAHR) from IRM_IPBKPFINDX  */
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_accountno = ifnull(kpf.IRM_IPBKPF_ACPTR,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_accountno <> ifnull(kpf.IRM_IPBKPF_ACPTR,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_participant = ifnull(kpf.IRM_IPBKPF_IPPTR,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_participant <> ifnull(kpf.IRM_IPBKPF_IPPTR,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_accounttype = ifnull(kpf.IRM_IPBKPF_KOART,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_accounttype <> ifnull(kpf.IRM_IPBKPF_KOART,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_fiscalperiod = ifnull(kpf.IRM_IPBKPF_MONAT,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_fiscalperiod <> ifnull(kpf.IRM_IPBKPF_MONAT,0)
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_partnercomm = ifnull(kpf.IRM_IPBKPF_PCNUM,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_partnercomm <> ifnull(kpf.IRM_IPBKPF_PCNUM,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_parkdocstatus = ifnull(kpf.IRM_IPBKPF_PDSTAT,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_parkdocstatus <> ifnull(kpf.IRM_IPBKPF_PDSTAT,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_reconstatus = ifnull(kpf.IRM_IPBKPF_RSTAT,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_reconstatus <> ifnull(kpf.IRM_IPBKPF_RSTAT,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_settlementmethod = ifnull(kpf.IRM_IPBKPF_SETLM,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_settlementmethod <> ifnull(kpf.IRM_IPBKPF_SETLM,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_debitcreditind = ifnull(kpf.IRM_IPBKPF_SHKZG,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_debitcreditind <> ifnull(kpf.IRM_IPBKPF_SHKZG,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_timestamp = ifnull(kpf.IRM_IPBKPF_TIMESTAMP,0),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_timestamp <> ifnull(kpf.IRM_IPBKPF_TIMESTAMP,0)
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.dd_documentcategory = ifnull(kpf.IRM_IPBKPF_VBTYP,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_documentcategory <> ifnull(kpf.IRM_IPBKPF_VBTYP,'Not Set')
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR
	AND is_today = current_date; /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET     fcv.amt_accountingdoc = ifnull(kpf.IRM_IPBKPF_WRBTR,0)* (CASE WHEN ifnull(irm_ipbkpf_shkzg,'Not Set') = 'H' THEN -1 ELSE 1 END),  /* 29May2015 - Added after removing a separate update statment to consider Dr/Cr indicatior ipbkpf_shkzg*/
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.amt_accountingdoc <> ifnull(kpf.IRM_IPBKPF_WRBTR,0) * (CASE WHEN ifnull(irm_ipbkpf_shkzg,'Not Set') = 'H' THEN -1 ELSE 1 END)
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR  /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */
	AND fcv.dd_accountingdocumentcategory = kpf.IRM_IPBKPF_VBTYP
	AND is_today = current_date; /* New clause. dd_accountingdocumentcategory poopulated with BKPFINDX_VBTYP */
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf, dim_date dt, dim_plant pl
SET    fcv.dim_dateidpostingdate = ifnull(dt.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   dt.DateValue = kpf.IRM_IPBKPF_BUDAT
	AND fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR  /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */
	AND fcv.dim_plantid = pl.dim_plantid
	AND dt.CompanyCode = pl.CompanyCode
	AND fcv.dim_dateidpostingdate <> ifnull(dt.dim_dateid,1)
	AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf, dim_chargebacktypes d
SET    fcv.dim_iptype = ifnull(d.dim_chargebacktypesid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   d.Type = ifnull(kpf.IRM_IPBKPF_IPTYP,'Not Set')
	AND fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
	AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR  /* Added on 30Apr2015*/	/* Changed to KPF instead of INDX as the column already existed */
	AND fcv.dim_iptype <> ifnull(d.dim_chargebacktypesid,1)
	AND is_today = current_date;	


/*UPDATE fact_chargebackvendor fcv
FROM   IRM_IPBKPF_INDX_vistex_hist kpf
SET    fcv.amt_accountingdoc = ifnull(irm_ipbkpf_wrbtr,0) * (CASE WHEN ifnull(irm_ipbkpf_shkzg,'Not Set') = 'H' THEN -1 ELSE 1 END)
WHERE  fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
AND fcv.dd_AccountingDocNo = kpf.IRM_IPBKPF_BELNR  /* Added on 30Apr2015	/* Changed to KPF instead of INDX as the column already existed 
	AND fcv.amt_accountingdoc <> ifnull(irm_ipbkpf_wrbtr,0) * (CASE WHEN ifnull(irm_ipbkpf_shkzg,'Not Set') = 'H' THEN -1 ELSE 1 END)	*/	

call vectorwise(combine 'fact_chargebackvendor');	
/* IRM_IPCBFL (IPNUM) */

/* Suchithra - 04292015 Modifications to the below Update 
New field added fcv.dd_docflowacctdocno
Restriction of value 'SP' to IRM_IPCBDFL_VBTYP_N to update Parking Doc No 
Correction in Subsequent Category field to update vbtyp_n and not vbtyp_v
*/
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPCBDFL_vistex_hist dfl
SET     fcv.dd_precedingcategory = ifnull(dfl.IRM_IPCBDFL_VBTYP_V,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE	fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM 
	and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_docflowacctdocno =  dfl.IRM_IPCBDFL_VBELN
	and fcv.dd_subsequentcategory	= dfl.IRM_IPCBDFL_VBTYP_N
	and fcv.dd_precedingcategory <> ifnull(dfl.IRM_IPCBDFL_VBTYP_V,'Not Set')
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPCBDFL_vistex_hist dfl
SET     fcv.dd_subsequentcategory = ifnull(dfl.IRM_IPCBDFL_VBTYP_N,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE	fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM 
	and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_docflowacctdocno =  dfl.IRM_IPCBDFL_VBELN
	and fcv.dd_subsequentcategory	= dfl.IRM_IPCBDFL_VBTYP_N
	and fcv.dd_subsequentcategory <> ifnull(dfl.IRM_IPCBDFL_VBTYP_N,'Not Set')
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPCBDFL_vistex_hist dfl
SET     fcv.dd_docflowacctdocno = ifnull(dfl.IRM_IPCBDFL_VBELN,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE	fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM 
	and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_docflowacctdocno =  dfl.IRM_IPCBDFL_VBELN
	and fcv.dd_subsequentcategory	= dfl.IRM_IPCBDFL_VBTYP_N
	and fcv.dd_docflowacctdocno <> ifnull(dfl.IRM_IPCBDFL_VBELN,'Not Set')
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPCBDFL_vistex_hist dfl
SET     fcv.dd_parkingdocno = ifnull(dfl.IRM_IPCBDFL_VBELN,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE	fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM 
	and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_docflowacctdocno =  dfl.IRM_IPCBDFL_VBELN
	and fcv.dd_subsequentcategory	= dfl.IRM_IPCBDFL_VBTYP_N
	and fcv.dd_parkingdocno <> ifnull(dfl.IRM_IPCBDFL_VBELN,'Not Set')
	and ifnull(dfl.IRM_IPCBDFL_VBTYP_N,'Not Set') = 'SP'
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPCBDFL_vistex_hist dfl
SET     fcv.dd_referencetransaction = ifnull(dfl.IRM_IPCBDFL_AWTYP,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE	fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM 
	and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_docflowacctdocno =  dfl.IRM_IPCBDFL_VBELN
	and fcv.dd_subsequentcategory	= dfl.IRM_IPCBDFL_VBTYP_N
	and fcv.dd_referencetransaction <> ifnull(dfl.IRM_IPCBDFL_AWTYP,'Not Set')
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPCBDFL_vistex_hist dfl, dim_date dt, dim_plant pl
SET    fcv.dim_dateidaccdocd = ifnull(dt.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   dt.DateValue = dfl.IRM_IPCBDFL_ERDAT
	AND fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_docflowacctdocno =  dfl.IRM_IPCBDFL_VBELN
    and fcv.dd_subsequentcategory	= dfl.IRM_IPCBDFL_VBTYP_N
	AND fcv.dim_plantid = pl.dim_plantid
	AND dt.CompanyCode = pl.CompanyCode
	AND fcv.dim_dateidaccdocd <> ifnull(dt.dim_dateid,1)
	AND is_today = current_date;

/* Suchithra -13May - Park Doc related fields to be populated with data of to SP category line item per IPNUM. Removed the fields from the above update */
drop table if exists parkDoc_tmp;
CREATE TABLE parkDoc_tmp as
SELECT distinct t1.irm_ipcbdfl_ipnum, t1.irm_ipcbdfl_ipitm
	,t1.IRM_IPCBDFL_VBELN
	,t1.IRM_IPCBDFL_GJAHR 
	,t1.IRM_IPCBDFL_ERZET 
	,t1.IRM_IPCBDFL_ERDAT
FROM irm_ipcbdfl_vistex_hist t1 
WHERE t1.irm_ipcbdfl_erdat = 
		(select max(t2.irm_ipcbdfl_erdat) from irm_ipcbdfl_vistex_hist t2
		WHERE t1.irm_ipcbdfl_ipnum = t2.irm_ipcbdfl_ipnum and t1.irm_ipcbdfl_vbtyp_n = t2.irm_ipcbdfl_vbtyp_n 
		group by t2.irm_ipcbdfl_ipnum, t2.irm_ipcbdfl_vbtyp_n) 
AND t1.irm_ipcbdfl_vbtyp_n = 'SP';

/* In case there are multiple entries for same ERDAT and different VBELNs, get the highest VBELN */
drop table if exists parkDoc_tmp_rmvDups;
create table parkDoc_tmp_rmvDups as 
select * 
FROM parkDoc_tmp t1 where t1.IRM_IPCBDFL_VBELN = 
    (select max(t2.IRM_IPCBDFL_VBELN) from parkDoc_tmp t2
    WHERE t1.irm_ipcbdfl_ipnum = t2.irm_ipcbdfl_ipnum and t1.irm_ipcbdfl_ipitm = t2.irm_ipcbdfl_ipitm);


UPDATE fact_chargebackvendor fcv
FROM   parkDoc_tmp_rmvDups dfl
SET     fcv.dd_parkingdocno = ifnull(dfl.IRM_IPCBDFL_VBELN,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM 
	and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_parkingdocno <> ifnull(dfl.IRM_IPCBDFL_VBELN,'Not Set')
	AND is_today = current_date;
	
UPDATE fact_chargebackvendor fcv
FROM   parkDoc_tmp_rmvDups dfl
SET     fcv.dd_parkfiscalyear = ifnull(dfl.IRM_IPCBDFL_GJAHR,0000),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM 
	and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_parkfiscalyear <> ifnull(dfl.IRM_IPCBDFL_GJAHR,0000)
	AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM   parkDoc_tmp_rmvDups dfl
SET     fcv.dd_parkdoctime = ifnull(dfl.IRM_IPCBDFL_ERZET,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM 
	and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	and fcv.dd_parkdoctime <> ifnull(dfl.IRM_IPCBDFL_ERZET,'Not Set')
	AND is_today = current_date;




/* - Old Update
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPCBDFL_vistex_hist dfl, dim_date dt, dim_plant pl
SET    fcv.dim_dateidParkDoc = ifnull(dt.dim_dateid,1)
WHERE   dt.DateValue = dfl.IRM_IPCBDFL_ERDAT
	AND fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	AND fcv.dim_plantid = pl.dim_plantid
	AND dt.CompanyCode = pl.CompanyCode
	and fcv.dd_docflowacctdocno =  dfl.IRM_IPCBDFL_VBELN
	and fcv.dd_subsequentcategory	= dfl.IRM_IPCBDFL_VBTYP_N
	AND fcv.dim_dateidpostingdate <> ifnull(dt.dim_dateid,1)
	*/

UPDATE fact_chargebackvendor fcv
FROM   parkDoc_tmp_rmvDups dfl, dim_date dt, dim_plant pl
SET    fcv.dim_dateidParkDoc = ifnull(dt.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   dt.DateValue = dfl.IRM_IPCBDFL_ERDAT
	AND fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
	AND fcv.dim_plantid = pl.dim_plantid
	AND dt.CompanyCode = pl.CompanyCode
	/*and fcv.dd_docflowacctdocno =  dfl.IRM_IPCBDFL_VBELN
	and fcv.dd_subsequentcategory	= dfl.IRM_IPCBDFL_VBTYP_N*/
	AND fcv.dim_dateidParkDoc <> ifnull(dt.dim_dateid,1)
	AND is_today = current_date;
	
/* Park Doc Status */
UPDATE fact_chargebackvendor fcv
FROM IRM_IPBKPF_INDX_vistex_hist kpf, parkDoc_tmp_rmvDups dfl
SET  fcv.dd_parkdocstatus = ifnull(kpf.IRM_IPBKPF_PDSTAT,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	 fcv.dd_ipnum = kpf.IRM_IPBKPFINDX_IPNUM 
AND fcv.dd_ipitem = kpf.IRM_IPBKPFINDX_IPITM
AND dfl.IRM_IPCBDFL_VBELN = kpf.IRM_IPBKPF_BELNR
AND fcv.dd_ipnum = dfl.IRM_IPCBDFL_IPNUM and fcv.dd_ipitem = dfl.IRM_IPCBDFL_IPITM
AND fcv.dd_parkdocstatus <> ifnull(kpf.IRM_IPBKPF_PDSTAT,'Not Set')
AND is_today = current_date;	
	
/* Suchithra -13May - End of Park Doc related changes */	

	

	

/* IRM_IPCBFL (IPNUM) */

call vectorwise(combine 'fact_chargebackvendor');

/* IRM_IPPC (PC Header and Item) */


		/* distinct PCNUM at the level of vendor and cc */
drop table if exists ct_accountline_chargebackvendor;
create table ct_accountline_chargebackvendor as
	select ippc.IRM_IPPCHDR_PCNUM pcnum,ippc.IRM_IPPCHDR_BUKRS,ippc.IRM_IPPCHDR_PARTNER, count(*) pcnumsum
	from IRM_IPPC_vistex_hist ippc 
	group by ippc.IRM_IPPCHDR_PCNUM,ippc.IRM_IPPCHDR_BUKRS,ippc.IRM_IPPCHDR_PARTNER;
		/* distinct PCNUM at the level of vendor and cc */
	
update  fact_chargebackvendor fcv
FROM    ct_accountline_chargebackvendor tmp
SET	    fcv.ct_accountline = tmp.pcnumsum,
		 dw_update_date = CURRENT_TIMESTAMP			
WHERE   fcv.dd_ipnum = tmp.pcnum
	AND fcv.ct_accountline <> tmp.pcnumsum
    AND is_today = current_date	;

	 
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.dd_pcnum = ifnull(ippc.IRM_IPPCHDR_PCNUM,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.dd_pcnum <> ifnull(ippc.IRM_IPPCHDR_PCNUM,'Not Set')
	 AND is_today = current_date;
	 
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.dd_pctype = ifnull(ippc.IRM_IPPCHDR_PCTYP,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.dd_pctype <> ifnull(ippc.IRM_IPPCHDR_PCTYP,'Not Set')
	 AND is_today = current_date;
	 
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.dd_partnerfiscalyear = ifnull(ippc.IRM_IPPCHDR_GJAHR,0),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.dd_partnerfiscalyear <> ifnull(ippc.IRM_IPPCHDR_GJAHR,0)
	 AND is_today = current_date;	

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.dd_partnerfunction = ifnull(ippc.IRM_IPPCHDR_PARVW,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP			
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.dd_partnerfunction <> ifnull(ippc.IRM_IPPCHDR_PARVW,'Not Set')
	 AND is_today = current_date;
	 
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.dd_participationtype = ifnull(ippc.IRM_IPPCHDR_NRART,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP			
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.dd_participationtype <> ifnull(ippc.IRM_IPPCHDR_NRART,'Not Set')
	 AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.dd_partnercommcreatedby = ifnull(ippc.IRM_IPPCITM_ERZET,000000),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.dd_partnercommcreatedby <> ifnull(ippc.IRM_IPPCITM_ERZET,000000)
	 AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.amt_settlement = ifnull(ippc.IRM_IPPCITM_PISAMT,0)	,
		 dw_update_date = CURRENT_TIMESTAMP					
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.amt_settlement <> ifnull(ippc.IRM_IPPCITM_PISAMT,0)
	 AND is_today = current_date;

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.ct_submitted = ifnull(ippc.IRM_IPPCITM_SUBQTY,0),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.ct_submitted <> ifnull(ippc.IRM_IPPCITM_SUBQTY,0)
	 AND is_today = current_date;	 
	 

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.amt_submitted = ifnull(ippc.IRM_IPPCITM_SUBAMT,0),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.amt_submitted <> ifnull(ippc.IRM_IPPCITM_SUBAMT,0)
	 AND is_today = current_date;	 

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.ct_adjusted = ifnull(ippc.IRM_IPPCITM_ADJQTY,0),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.ct_adjusted <> ifnull(ippc.IRM_IPPCITM_ADJQTY,0)
	 AND is_today = current_date;	 

UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.amt_adjusted = ifnull(ippc.IRM_IPPCITM_ADJAMT,0),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.amt_adjusted <> ifnull(ippc.IRM_IPPCITM_ADJAMT,0)
	 AND is_today = current_date;	 
	 
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET      fcv.amt_payeraccepted = ifnull(ippc.IRM_IPPCITM_ORSAMT,0),
		 dw_update_date = CURRENT_TIMESTAMP						
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND fcv.amt_payeraccepted <> ifnull(ippc.IRM_IPPCITM_ORSAMT,0)
	 AND is_today = current_date;	 


call vectorwise(combine 'fact_chargebackvendor');
drop table if exists tmp_dim_dateidPartnerComm;


create table tmp_dim_dateidPartnerComm as
select fact_chargebackvendorid, ifnull(dt.dim_dateid,1) as dim_dateidpartnercomm 
from fact_chargebackvendor fcv,IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm,dim_date dt, dim_company cc
WHERE   dt.DateValue = ippc.IRM_IPPCHDR_ERDAT
AND itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
AND dt.CompanyCode = cc.companycode
AND cc.dim_companyid = fcv.dim_companyid
AND fcv.dim_dateidPartnerComm <> ifnull(dt.dim_dateid,1)
AND is_today = current_date;

UPDATE  fact_chargebackvendor fcv
FROM    tmp_dim_dateidPartnerComm t
SET     fcv.dim_dateidPartnerComm = t.dim_dateidpartnercomm,
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   t.fact_chargebackvendorid = fcv.fact_chargebackvendorid
	AND fcv.dim_dateidPartnerComm <> t.dim_dateidpartnercomm;
	
UPDATE  fact_chargebackvendor fcv
FROM    IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm, Dim_Currency cur	
SET		fcv.dim_originalcurrencyid = ifnull(cur.dim_currencyid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   cur.CurrencyCode = ifnull(ippc.irm_ippcitm_orscur,'Not Set')
	AND itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	AND fcv.dim_originalcurrencyid <> ifnull(cur.dim_currencyid,1)
	AND is_today = current_date;
	
drop table if exists tmp_dim_dateidPartnerComm;
	
	
UPDATE  fact_chargebackvendor fcv
FROM    IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm, Dim_Currency cur	
SET		fcv.dim_payeracceptedcurrid = ifnull(cur.dim_currencyid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   cur.CurrencyCode = ifnull(ippc.irm_ippcitm_pacur,'Not Set')
	AND itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	AND fcv.dd_ipnum = ippc.IRM_IPPCHDR_PCNUM and fcv.dd_ipitem = ippc.IRM_IPPCITM_PCITM
	AND fcv.dim_payeracceptedcurrid <> ifnull(cur.dim_currencyid,1)
	AND is_today = current_date;

/* 18 May 2015: New fields added */
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET 
		dd_pcpostingstatus = ifnull(IRM_IPPCHDR_PSTAT,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND dd_pcpostingstatus <> ifnull(IRM_IPPCHDR_PSTAT,'Not Set')
	 AND is_today = current_date;
	 
UPDATE fact_chargebackvendor fcv
FROM   IRM_IPPC_vistex_hist ippc, IRM_IPCBITM_vistex_hist itm
SET 
		dd_pcintsetlsts = ifnull(IRM_IPPCHDR_ISSTAT,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	 itm.irm_ipcbitm_pcnum = ippc.irm_ippchdr_pcnum
	 AND itm.irm_ipcbitm_pcitm = ippc.irm_ippcitm_pcitm
	 AND fcv.dd_ipnum = itm.IRM_IPCBITM_IPNUM and fcv.dd_ipitem = itm.IRM_IPCBITM_IPITM
	 AND dd_pcintsetlsts <> ifnull(IRM_IPPCHDR_ISSTAT,'Not Set')
	 AND is_today = current_date;
/* IRM_IPPC (PC Header and Item) */	


/* IRM_TIPOPCC linked to IPCBHDR */
update  fact_chargebackvendor fcv
FROM    IRM_TIPOPCC pcc, IRM_IPCBHDR_vistex_hist hdr
SET     fcv.dd_CorrespondenceId = ifnull(pcc.irm_tipopcc_event,'Not Set'),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE   pcc.irm_tipopcc_ktonr = hdr.irm_ipcbhdr_payer 
and pcc.irm_tipopcc_bukrs = hdr.irm_ipcbhdr_bukrs 
and hdr.irm_ipcbhdr_ipnum = fcv.dd_ipnum
AND fcv.dd_CorrespondenceId <> ifnull(pcc.irm_tipopcc_event,'Not Set')
AND is_today = current_date;

/*
update  fact_chargebackvendor fcv
FROM    IRM_TIPOPCC pcc, IRM_IPCBHDR_vistex_hist hdr
SET     fcv.dd_flagElectronicComm = CASE WHEN pcc.irm_tipopcc_event = 'ZC001' THEN 'X' ELSE 'Not Set' END
WHERE   pcc.irm_tipopcc_ktonr = hdr.irm_ipcbhdr_payer 
	and pcc.irm_tipopcc_bukrs = hdr.irm_ipcbhdr_bukrs 
	and hdr.irm_ipcbhdr_ipnum = fcv.dd_ipnum
	AND is_today = current_date
	
*/

/* IRM_TIPOPCC linked to IPCBHDR */
	

/* Accrual and Settlement */

/* Accrual updates */
/* take the minimum date value at the level of IPNUM and VBTYP and those will be the value that will generate the Initial Doc No, Doc Date and Amount */
/* Suchithra - 12May2015 - Modified to consider min of posting date (budat) instead of timestamp in the BKPF table for initial doc no, date and amount for both accruals and settlements */
/* Suchithra - 13May2015 - Remap Initial Accrual Doc Amt to IPBKPFINDX-NETWR instead of WRBTR */
drop table if exists accsetlcbvendor;
CREATE TABLE accsetlcbvendor as
SELECT distinct t1.irm_ipbkpfindx_ipnum,t1.irm_ipbkpfindx_ipitm,t1.IRM_IPBKPF_BELNR,t1.irm_ipbkpf_vbtyp,t1.irm_ipbkpf_budat, /* 6th May : Added ipitm and belnr for join condition on update below */
	   t1.irm_ipbkpf_belnr docno,t1.irm_ipbkpf_budat docdate ,t1.irm_ipbkpf_wrbtr amount 
FROM irm_ipbkpf_indx_vistex_hist t1 
WHERE t1.irm_ipbkpf_budat = 
		(select min(t2.irm_ipbkpf_budat) from irm_ipbkpf_indx_vistex_hist t2 
		WHERE t1.irm_ipbkpfindx_ipnum = t2.irm_ipbkpfindx_ipnum and t1.irm_ipbkpf_vbtyp = t2.irm_ipbkpf_vbtyp group by t2.irm_ipbkpfindx_ipnum, t2.irm_ipbkpf_vbtyp) and t1.irm_ipbkpf_vbtyp = 'AC';
/* take the minimum date value at the level of IPNUM and VBTYP and those will be the value that will generate the Initial Doc No, Doc Date and Amount */

/* To remove duplicates by considering only the lowest among the Acctng doc Numbers - 09May2015 */
drop table if exists accsetlcbvendor_rm_dupl;
create table accsetlcbvendor_rm_dupl as 
select * 
FROM accsetlcbvendor t1 where t1.irm_ipbkpf_belnr = 
    (select min(t2.irm_ipbkpf_belnr) from accsetlcbvendor t2
    WHERE t1.irm_ipbkpfindx_ipnum = t2.irm_ipbkpfindx_ipnum and t1.irm_ipbkpfindx_ipitm = t2.irm_ipbkpfindx_ipitm);
 
 /* Ini Accr Doc Amt Updated separately - 13May2015 */
UPDATE fact_chargebackvendor fcv
FROM   accsetlcbvendor_rm_dupl acsb
SET 	
		 fcv.dd_iniaccrualdocno = ifnull(acsb.docno,'Not Set') ,
		 dw_update_date = CURRENT_TIMESTAMP			 
WHERE 	 fcv.dd_ipnum = acsb.IRM_IPBKPFINDX_IPNUM 
     AND fcv.dd_ipitem = acsb.IRM_IPBKPFINDX_IPITM
	 AND fcv.dd_iniaccrualdocno <> ifnull(acsb.docno,'Not Set')
	 AND is_today = current_date; /* 6th May : Added ipitm and belnr */
	/* AND fcv.dd_AccountingDocNo = acsb.IRM_IPBKPF_BELNR   6th May : Added ipitm and belnr */
	
/* Update for Initi Accr Doc Amt - 13May2015 */	
UPDATE fact_chargebackvendor fcv
FROM   accsetlcbvendor_rm_dupl acsb, irm_ipbkpfindx indx
SET  fcv.amt_iniaccrualdoc = ifnull(indx.irm_ipbkpfindx_netwr,0.0000),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE 	 fcv.dd_ipnum = acsb.IRM_IPBKPFINDX_IPNUM 
AND fcv.dd_ipitem = acsb.IRM_IPBKPFINDX_IPITM 
AND acsb.IRM_IPBKPFINDX_IPNUM  = indx.irm_ipbkpfindx_ipnum
AND acsb.IRM_IPBKPFINDX_IPITM = indx.irm_ipbkpfindx_ipitm
AND acsb.irm_ipbkpf_belnr = indx.irm_ipbkpfindx_belnr
AND fcv.amt_iniaccrualdoc <> ifnull(indx.irm_ipbkpfindx_netwr,0.0000)
AND is_today = current_date;
/* Suchithra - 13May2015 - End of changes */


UPDATE fact_chargebackvendor fcv
FROM   accsetlcbvendor_rm_dupl acsb, dim_date dt, dim_plant pl
SET    fcv.dim_iniaccrualdocdate = ifnull(dt.dim_dateid,1),
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE  dt.CompanyCode = pl.CompanyCode AND fcv.dim_plantid = pl.dim_plantid
   /*AND dt.DateValue = cast(substring(docdate,1,4)+'-'+substring(docdate,5,2)+'-'+substring(docdate,7,2) as date)*/
   AND dt.dateValue = docdate
   AND fcv.dd_ipnum = acsb.IRM_IPBKPFINDX_IPNUM and acsb.irm_ipbkpf_vbtyp = 'AC'
   AND fcv.dd_ipitem = acsb.IRM_IPBKPFINDX_IPITM 		/* 6th May : Added ipitm and belnr */
   AND fcv.dim_iniaccrualdocdate <> ifnull(dt.dim_dateid,1)
   AND is_today = current_date;
   /* AND fcv.dd_AccountingDocNo = acsb.IRM_IPBKPF_BELNR /* 6th May : Added ipitm and belnr */
   
drop table if exists accsetlcbvendor_rm_dupl;
   



/* This will need update with an exchangerate script for other customers. HDSMITH uses only USD */
update fact_chargebackvendor
set amt_exchangerate = 1
where amt_exchangerate <> 1
AND is_today = current_date;

update fact_chargebackvendor
set amt_exchangerate_gbl = 1
where amt_exchangerate_gbl <> 1
AND is_today = current_date;
/* This will need update with an exchangerate script for other customers. HDSMITH uses only USD */

/*
call vectorwise(combine 'fact_chargebackvendor')
call vectorwise(combine 'fact_chargebackvendorwoupdate-fact_chargebackvendor')

UPDATE fact_chargebackvendor fb
FROM fact_chargebackvendorwoupdate fbi
SET   fb.dw_update_date = CURRENT_TIMESTAMP
WHERE fb.fact_chargebackvendorid = fbi.fact_chargebackvendorid
*/

/* Suchithra 27 May 2015 - New field */
/* A boolean field to show True if the Accounting Doc Number is the Initial Accrual Doc Num */
UPDATE fact_chargebackvendor 
SET dd_isinitialaccrual = CASE WHEN dd_iniaccrualdocno = dd_accountingdocno THEN 'True' ELSE 'False' END,
		 dw_update_date = CURRENT_TIMESTAMP	
WHERE dd_isinitialaccrual <> CASE WHEN dd_iniaccrualdocno = dd_accountingdocno THEN 'True' ELSE 'False' END;
/* End of changes 27 May 2015 */

/*

UPDATE fact_chargebackvendor fcv
SET fcv.amt_hdraccountingNet = CASE WHEN fcv.dd_accountingdocumentcategory in ('SE','SR','SI','RI') THEN fcv.amt_accountingdoc * -1 ELSE fcv.amt_accountingdoc END
WHERE amt_hdraccountingNet <> CASE WHEN fcv.dd_accountingdocumentcategory in ('SE','SR','SI','RI') THEN fcv.amt_accountingdoc * -1 ELSE fcv.amt_accountingdoc END


UPDATE fact_chargebackvendor
set amt_acctDocAmtTest = amt_hdraccountingNet
WHERe amt_acctDocAmtTest <> amt_hdraccountingNet
*/

call vectorwise(combine 'fact_chargebackvendor');


drop table if exists ct_accountline_chargebackvendor;
drop table if exists tmp_IRM_IPPC_ctamt;
drop table if exists tmp_billing_chargebackbvendor;
drop table if exists tmp_billing_chargebackbvendor_filtered;
drop table if exists temp_konv_cbv;
drop table if exists temp_vbrk_vbrp_cbv;
drop table if exists temp_vbrk_konv_cbv;
drop table if exists tmp_billing_cbv_stg_filtered;

DROP TABLE IF EXISTS irm_ipcbhdr_inserts;
DROP TABLE IF EXISTS irm_ipcbitm_inserts;
DROP TABLE IF EXISTS irm_ippc_inserts;
DROP TABLE IF EXISTS vbpa_bill_inserts;
DROP TABLE IF EXISTS vbrk_vbrp_inserts;
