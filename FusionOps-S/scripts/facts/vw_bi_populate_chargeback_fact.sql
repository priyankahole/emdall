
/******************************************************************************************************************/
/*   Script         : vw_bi_populate_chargeback_fact	                                                          */
/*   Author         : Hiten                                                                                       */
/*   Created On     : 20 Feb 2014                                                                                 */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   22 Dec 2014      Alex D    1.1               Add Vendor Dimension                                            */
/*   20 Feb 2014      Hiten     1.0               New script created                                              */
/******************************************************************************************************************/


delete from NUMBER_FOUNTAIN where table_name = 'fact_chargeback';

INSERT INTO NUMBER_FOUNTAIN
select 	'fact_chargeback',
	ifnull(max(f.fact_chargebacksid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_chargebacks f;

DROP TABLE IF EXISTS tmp_var_chargeback_fact;
CREATE table tmp_var_chargeback_fact (
	pGlobalCurrency) AS
Select  CAST(ifnull((SELECT property_value
			FROM systemproperty
			WHERE property = 'customer.global.currency'),
		'USD'),varchar(3));

drop table if exists tmp_chargeback_new_no_001;
create table tmp_chargeback_new_no_001 as
select distinct IRM_IPCBITM_IPNUM NEW_IPNUM, IRM_IPCBITM_IPITM NEW_IPITM 
from IRM_IPCBITM
where not exists (select 1 from fact_chargebacks f where f.dd_IPNum = IRM_IPCBITM_IPNUM);

INSERT INTO fact_chargebacks(fact_chargebacksid,
                             amt_exchangerate,
                             amt_exchangerate_gbl,
                             amt_exchangerate_stat,
                             dd_application,
                             dd_debitcreditind,
                             dd_recipient,
                             dd_splitcriteria,
                             dd_package,
                             dd_salesbilldocno,
                             dd_groupkey,
                             dd_docconditionno,
                             dd_exratetype,
                             dd_billdoccancelledflag,
                             dd_cancelledbilldocno,
                             dd_accrualstatus,
                             dd_parkstatus,
                             dd_interimsettlementstatus,
                             dd_settlementstatus,
                             dd_deleteindheader,
                             dd_completionstatus,
                             dd_hdrcreatedby,
                             dd_ipnum,
                             dd_ipitem,
                             dd_uom,
                             dd_sddoccategory,
                             dd_salesdocno,
                             dd_salesdocitemno,
                             dd_refdocno,
                             dd_refdocitemno,
                             dd_profitsegmentno,
                             dd_wbselement,
                             dd_orderno,
                             dd_returnsitem,
                             dd_intntlarticleno,
                             dd_settlementblockcode,
                             dd_acceptedamtstatus,
                             dd_actioncode,
                             dd_resubmitreasoncode,
                             dd_extresubmitreasoncode,
                             dd_settlementdoc,
                             dd_settlementdocyr,
                             dd_postingtype,
                             dd_intsettlecount,
                             dd_settlementstatusitem,
                             dd_claimstatus,
                             dd_accrualstatusitem,
                             dd_itemtext,
                             dd_deleteinditem,
                             dd_itemcreatedby,
                             amt_exchangeratefipost,
                             amt_exchangeratepricedetr,
                             amt_netvalueheader,
                             ct_quantity,
                             amt_netvalueitem,
                             amt_unitprice,
                             amt_subtotal1,
                             amt_subtotal2,
                             amt_subtotal3,
                             amt_itemacceptedprice,
                             amt_originalclaim,
                             amt_payeraccepted,
                             amt_postsettlementadj,
                             ct_contributionperc,
                             amt_accrual,
                             amt_tax,
                             dim_chargebacktypesid,
                             dim_salseorgid,
                             dim_distributionchannelid,
                             dim_purchaseorgid,
                             dim_customergroupid,
                             dim_salesdistrictid,
                             dim_salesdoctypeid,
                             dim_billingdoctypeid,
                             dim_customeridsoldto,
                             dim_currencyid,
                             dim_currencyid_tra,
                             dim_currencyid_stat,
                             dim_dateiddocdate,
                             dim_dateidpostingdate,
                             dim_companyid,
                             dim_companyidclearing,
                             dim_dateidhdrcreated,
                             dim_dateidhdrlastchg,
                             dim_rebateagmttypesid,
                             dim_agreementsid,
                             dim_customeridshipto,
                             dim_customeridpayer,
                             dim_soitemcategoryid,
                             dim_materialgroupid,
                             dim_partid,
                             dim_partsalesid,
                             dim_producthierarchyid,
                             dim_salesdivisionid,
                             dim_salesofficeid,
                             dim_salesgroupid,
                             dim_dateidpriceexrate,
                             dim_dateidservice,
                             dim_profitcenterid,
                             dim_costcenterid,
                             dim_businessareaid,
                             dim_functionalareaid,
                             dim_controllingareaid,
                             dim_plantid,
                             dim_materialpricinggroupid,
                             dim_specialstockid,
                             dim_companyidsettlementdoc,
                             dim_dateidlastpark,
                             dim_dateidlastsettlement,
                             dim_dateidlastreversepark,
                             dim_dateidlastreversesettle,
                             dim_dateiditemcreated,
                             dim_dateiditemlastchg,
			     dim_vendorid)
SELECT (SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_chargeback') + row_number() over() fact_chargebacksid,
	     1 amt_exchangerate,
	     1 amt_exchangerate_gbl,
	     1 amt_exchangerate_stat,
	     'Not Set' dd_application,
	     'Not Set' dd_debitcreditind,
	     'Not Set' dd_recipient,
	     'Not Set' dd_splitcriteria,
	     'Not Set' dd_package,
	     'Not Set' dd_salesbilldocno,
	     'Not Set' dd_groupkey,
	     'Not Set' dd_docconditionno,
	     'Not Set' dd_exratetype,
	     'Not Set' dd_billdoccancelledflag,
	     'Not Set' dd_cancelledbilldocno,
	     'Not Set' dd_accrualstatus,
	     'Not Set' dd_parkstatus,
	     'Not Set' dd_interimsettlementstatus,
	     'Not Set' dd_settlementstatus,
	     'Not Set' dd_deleteindheader,
	     'Not Set' dd_completionstatus,
	     'Not Set' dd_hdrcreatedby,
	     NEW_IPNUM dd_ipnum,
	     NEW_IPITM dd_ipitem,
	     'Not Set' dd_uom,
	     'Not Set' dd_sddoccategory,
	     'Not Set' dd_salesdocno,
	     0 dd_salesdocitemno,
	     'Not Set' dd_refdocno,
	     0 dd_refdocitemno,
	     'Not Set' dd_profitsegmentno,
	     'Not Set' dd_wbselement,
	     'Not Set' dd_orderno,
	     'Not Set' dd_returnsitem,
	     'Not Set' dd_intntlarticleno,
	     'Not Set' dd_settlementblockcode,
	     'Not Set' dd_acceptedamtstatus,
	     'Not Set' dd_actioncode,
	     'Not Set' dd_resubmitreasoncode,
	     'Not Set' dd_extresubmitreasoncode,
	     'Not Set' dd_settlementdoc,
	     'Not Set' dd_settlementdocyr,
	     'Not Set' dd_postingtype,
	     'Not Set' dd_intsettlecount,
	     'Not Set' dd_settlementstatusitem,
	     'Not Set' dd_claimstatus,
	     'Not Set' dd_accrualstatusitem,
	     'Not Set' dd_itemtext,
	     'Not Set' dd_deleteinditem,
	     'Not Set' dd_itemcreatedby,
	     1 amt_exchangeratefipost,
	     1 amt_exchangeratepricedetr,
	     0 amt_netvalueheader,
	     0 ct_quantity,
	     0 amt_netvalueitem,
	     0 amt_unitprice,
	     0 amt_subtotal1,
	     0 amt_subtotal2,
	     0 amt_subtotal3,
	     0 amt_itemacceptedprice,
	     0 amt_originalclaim,
	     0 amt_payeraccepted,
	     0 amt_postsettlementadj,
	     0 ct_contributionperc,
	     0 amt_accrual,
	     0 amt_tax,
	     1 dim_chargebacktypesid,
	     1 dim_salseorgid,
	     1 dim_distributionchannelid,
	     1 dim_purchaseorgid,
	     1 dim_customergroupid,
	     1 dim_salesdistrictid,
	     1 dim_salesdoctypeid,
	     1 dim_billingdoctypeid,
	     1 dim_customeridsoldto,
	     1 dim_currencyid,
	     1 dim_currencyid_tra,
	     1 dim_currencyid_stat,
	     1 dim_dateiddocdate,
	     1 dim_dateidpostingdate,
	     1 dim_companyid,
	     1 dim_companyidclearing,
	     1 dim_dateidhdrcreated,
	     1 dim_dateidhdrlastchg,
	     1 dim_rebateagmttypesid,
	     1 dim_agreementsid,
	     1 dim_customeridshipto,
	     1 dim_customeridpayer,
	     1 dim_soitemcategoryid,
	     1 dim_materialgroupid,
	     1 dim_partid,
	     1 dim_partsalesid,
	     1 dim_producthierarchyid,
	     1 dim_salesdivisionid,
	     1 dim_salesofficeid,
	     1 dim_salesgroupid,
	     1 dim_dateidpriceexrate,
	     1 dim_dateidservice,
	     1 dim_profitcenterid,
	     1 dim_costcenterid,
	     1 dim_businessareaid,
	     1 dim_functionalareaid,
	     1 dim_controllingareaid,
	     1 dim_plantid,
	     1 dim_materialpricinggroupid,
	     1 dim_specialstockid,
	     1 dim_companyidsettlementdoc,
	     1 dim_dateidlastpark,
	     1 dim_dateidlastsettlement,
	     1 dim_dateidlastreversepark,
	     1 dim_dateidlastreversesettle,
	     1 dim_dateiditemcreated,
	     1 dim_dateiditemlastchg,
	     1 dim_vendorid
FROM tmp_chargeback_new_no_001;

call vectorwise(combine 'fact_chargebacks');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_chargebacks;
\i /db/schema_migration/bin/wrapper_optimizedb.sh IRM_IPCBHDR;
\i /db/schema_migration/bin/wrapper_optimizedb.sh IRM_IPCBITM;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_CLASS = 'Not Set' WHERE IRM_IPCBHDR_CLASS IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_application = IRM_IPCBHDR_CLASS
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_application <> IRM_IPCBHDR_CLASS;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_DCIND = CASE WHEN IRM_IPCBHDR_DCIND = 'H' THEN 'Credit' ELSE 'Debit' END;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_debitcreditind = IRM_IPCBHDR_DCIND
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_debitcreditind <> IRM_IPCBHDR_DCIND;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_PAYER = 'Not Set' WHERE IRM_IPCBHDR_PAYER IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_recipient = IRM_IPCBHDR_PAYER
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_recipient <> IRM_IPCBHDR_PAYER;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_ZUKRI = 'Not Set' WHERE IRM_IPCBHDR_ZUKRI IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_splitcriteria = IRM_IPCBHDR_ZUKRI
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_splitcriteria <> IRM_IPCBHDR_ZUKRI;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_PACKG = 'Not Set' WHERE IRM_IPCBHDR_PACKG IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_package = IRM_IPCBHDR_PACKG
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_package <> IRM_IPCBHDR_PACKG;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_VBELN = 'Not Set' WHERE IRM_IPCBHDR_VBELN IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_salesbilldocno = IRM_IPCBHDR_VBELN
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_salesbilldocno <> IRM_IPCBHDR_VBELN;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_KONZS = 'Not Set' WHERE IRM_IPCBHDR_KONZS IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_groupkey = IRM_IPCBHDR_KONZS
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_groupkey <> IRM_IPCBHDR_KONZS;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_KNUMV = 'Not Set' WHERE IRM_IPCBHDR_KNUMV IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_docconditionno = IRM_IPCBHDR_KNUMV
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_docconditionno <> IRM_IPCBHDR_KNUMV;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_KURST = 'Not Set' WHERE IRM_IPCBHDR_KURST IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_exratetype = IRM_IPCBHDR_KURST
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_exratetype <> IRM_IPCBHDR_KURST;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_FKSTO = 'Not Set' WHERE IRM_IPCBHDR_FKSTO IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_billdoccancelledflag = IRM_IPCBHDR_FKSTO
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_billdoccancelledflag <> IRM_IPCBHDR_FKSTO;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_SFAKN = 'Not Set' WHERE IRM_IPCBHDR_SFAKN IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_cancelledbilldocno = IRM_IPCBHDR_SFAKN
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_cancelledbilldocno <> IRM_IPCBHDR_SFAKN;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_ASTAT = 'Not Set' WHERE IRM_IPCBHDR_ASTAT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_accrualstatus = IRM_IPCBHDR_ASTAT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_accrualstatus <> IRM_IPCBHDR_ASTAT;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_PSTAT = 'Not Set' WHERE IRM_IPCBHDR_PSTAT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_parkstatus = IRM_IPCBHDR_PSTAT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_parkstatus <> IRM_IPCBHDR_PSTAT;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_SSTAT = 'Not Set' WHERE IRM_IPCBHDR_SSTAT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_settlementstatus = IRM_IPCBHDR_SSTAT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_settlementstatus <> IRM_IPCBHDR_SSTAT;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_DELETED = 'Not Set' WHERE IRM_IPCBHDR_DELETED IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_deleteindheader = IRM_IPCBHDR_DELETED
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_deleteindheader <> IRM_IPCBHDR_DELETED;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_OSTAT = 'Not Set' WHERE IRM_IPCBHDR_OSTAT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_completionstatus = IRM_IPCBHDR_OSTAT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_completionstatus <> IRM_IPCBHDR_OSTAT;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_ERNAM = 'Not Set' WHERE IRM_IPCBHDR_ERNAM IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET dd_hdrcreatedby = IRM_IPCBHDR_ERNAM
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ipnum = IRM_IPCBHDR_IPNUM
	AND dd_hdrcreatedby <> IRM_IPCBHDR_ERNAM;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_VRKME = 'Not Set' WHERE IRM_IPCBITM_VRKME IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_UoM = IRM_IPCBITM_VRKME
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_UoM <> IRM_IPCBITM_VRKME;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_AUTYP = 'Not Set' WHERE IRM_IPCBITM_AUTYP IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_sddoccategory = IRM_IPCBITM_AUTYP
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_sddoccategory <> IRM_IPCBITM_AUTYP;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_AUBEL = 'Not Set' WHERE IRM_IPCBITM_AUBEL IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_salesdocno = IRM_IPCBITM_AUBEL
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_salesdocno <> IRM_IPCBITM_AUBEL;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_AUPOS = 0 WHERE IRM_IPCBITM_AUPOS IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_salesdocitemno = IRM_IPCBITM_AUPOS
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_salesdocitemno <> IRM_IPCBITM_AUPOS;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_VGBEL = 'Not Set' WHERE IRM_IPCBITM_VGBEL IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_refdocno = IRM_IPCBITM_VGBEL
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_refdocno <> IRM_IPCBITM_VGBEL;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_VGPOS = 0 WHERE IRM_IPCBITM_VGPOS IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_refdocitemno = IRM_IPCBITM_VGPOS
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_refdocitemno <> IRM_IPCBITM_VGPOS;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_PAOBJNR = 0 WHERE IRM_IPCBITM_PAOBJNR IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_profitsegmentno = IRM_IPCBITM_PAOBJNR
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_profitsegmentno <> IRM_IPCBITM_PAOBJNR;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_PS_PSP_PNR = 0 WHERE IRM_IPCBITM_PS_PSP_PNR IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_wbselement = IRM_IPCBITM_PS_PSP_PNR
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_wbselement <> IRM_IPCBITM_PS_PSP_PNR;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_AUFNR = 'Not Set' WHERE IRM_IPCBITM_AUFNR IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_orderno = IRM_IPCBITM_AUFNR
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_orderno <> IRM_IPCBITM_AUFNR;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SHKZG = 'Not Set' WHERE IRM_IPCBITM_SHKZG IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_returnsitem = IRM_IPCBITM_SHKZG
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_returnsitem <> IRM_IPCBITM_SHKZG;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_EAN11 = 'Not Set' WHERE IRM_IPCBITM_EAN11 IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_intntlarticleno = IRM_IPCBITM_EAN11
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_intntlarticleno <> IRM_IPCBITM_EAN11;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SETLB = 'Not Set' WHERE IRM_IPCBITM_SETLB IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_settlementblockcode = IRM_IPCBITM_SETLB
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_settlementblockcode <> IRM_IPCBITM_SETLB;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_CEPOK = 'Not Set' WHERE IRM_IPCBITM_CEPOK IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_acceptedamtstatus = IRM_IPCBITM_CEPOK
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_acceptedamtstatus <> IRM_IPCBITM_CEPOK;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_RSRCD = 'Not Set' WHERE IRM_IPCBITM_RSRCD IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_resubmitreasoncode = IRM_IPCBITM_RSRCD
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_resubmitreasoncode <> IRM_IPCBITM_RSRCD;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_ERSRC = 'Not Set' WHERE IRM_IPCBITM_ERSRC IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_extresubmitreasoncode = IRM_IPCBITM_ERSRC
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_extresubmitreasoncode <> IRM_IPCBITM_ERSRC;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SGJAHR = 0 WHERE IRM_IPCBITM_SGJAHR IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_settlementdocyr = IRM_IPCBITM_SGJAHR
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_settlementdocyr <> IRM_IPCBITM_SGJAHR;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SPSTTY = 'Not Set' WHERE IRM_IPCBITM_SPSTTY IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_postingtype = IRM_IPCBITM_SPSTTY
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_postingtype <> IRM_IPCBITM_SPSTTY;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_ISCNT = 0 WHERE IRM_IPCBITM_ISCNT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_intsettlecount = IRM_IPCBITM_ISCNT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_intsettlecount <> IRM_IPCBITM_ISCNT;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SSTAT = 'Not Set' WHERE IRM_IPCBITM_SSTAT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_settlementstatusitem = IRM_IPCBITM_SSTAT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_settlementstatusitem <> IRM_IPCBITM_SSTAT;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_CSTAT = 'Not Set' WHERE IRM_IPCBITM_CSTAT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_claimstatus = IRM_IPCBITM_CSTAT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_claimstatus <> IRM_IPCBITM_CSTAT;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_ASTAT = 'Not Set' WHERE IRM_IPCBITM_ASTAT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_accrualstatusitem = IRM_IPCBITM_ASTAT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_accrualstatusitem <> IRM_IPCBITM_ASTAT;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SGTXT = 'Not Set' WHERE IRM_IPCBITM_SGTXT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_itemtext = IRM_IPCBITM_SGTXT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_itemtext <> IRM_IPCBITM_SGTXT;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_DELETED = 'Not Set' WHERE IRM_IPCBITM_DELETED IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_DeleteIndItem  = IRM_IPCBITM_DELETED
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_DeleteIndItem <> IRM_IPCBITM_DELETED;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_ERNAM = 'Not Set' WHERE IRM_IPCBITM_ERNAM IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET dd_ItemCreatedBy = IRM_IPCBITM_ERNAM
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND dd_ItemCreatedBy <> IRM_IPCBITM_ERNAM;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_KURRF = 1 WHERE IRM_IPCBHDR_KURRF IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET amt_exchangeratefipost = IRM_IPCBHDR_KURRF
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBHDR_IPNUM
	AND amt_exchangeratefipost <> IRM_IPCBHDR_KURRF;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_KURSK = 1 WHERE IRM_IPCBHDR_KURSK IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET amt_exchangeratepricedetr = IRM_IPCBHDR_KURSK
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBHDR_IPNUM
	AND amt_exchangeratepricedetr <> IRM_IPCBHDR_KURSK;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_NETWR = 0 WHERE IRM_IPCBHDR_NETWR IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBHDR 
SET amt_netvalueheader = IRM_IPCBHDR_NETWR
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBHDR_IPNUM
	AND amt_netvalueheader <> IRM_IPCBHDR_NETWR;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KWMENG = 0 WHERE IRM_IPCBITM_KWMENG IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET ct_quantity = IRM_IPCBITM_KWMENG
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND ct_quantity <> IRM_IPCBITM_KWMENG;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_NETWR = 0 WHERE IRM_IPCBITM_NETWR IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_netvalueitem = IRM_IPCBITM_NETWR
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_netvalueitem <> IRM_IPCBITM_NETWR;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_NETPR = 0 WHERE IRM_IPCBITM_NETPR IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_unitprice = IRM_IPCBITM_NETPR
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_unitprice <> IRM_IPCBITM_NETPR;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KZWI1 = 0 WHERE IRM_IPCBITM_KZWI1 IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_subtotal1 = IRM_IPCBITM_KZWI1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_subtotal1 <> IRM_IPCBITM_KZWI1;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KZWI2 = 0 WHERE IRM_IPCBITM_KZWI2 IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_subtotal2 = IRM_IPCBITM_KZWI2
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_subtotal2 <> IRM_IPCBITM_KZWI2;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KZWI3 = 0 WHERE IRM_IPCBITM_KZWI3 IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_subtotal3 = IRM_IPCBITM_KZWI3
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_subtotal3 <> IRM_IPCBITM_KZWI3;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_CMPRE = 0 WHERE IRM_IPCBITM_CMPRE IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_itemacceptedprice = IRM_IPCBITM_CMPRE
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_itemacceptedprice <> IRM_IPCBITM_CMPRE;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_WAVWR = 0 WHERE IRM_IPCBITM_WAVWR IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_originalclaim = IRM_IPCBITM_WAVWR
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_originalclaim <> IRM_IPCBITM_WAVWR;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_PAAMT = 0 WHERE IRM_IPCBITM_PAAMT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_payeraccepted = IRM_IPCBITM_PAAMT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_payeraccepted <> IRM_IPCBITM_PAAMT;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_PSAAMT = 0 WHERE IRM_IPCBITM_PSAAMT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_postsettlementadj = IRM_IPCBITM_PSAAMT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_postsettlementadj <> IRM_IPCBITM_PSAAMT;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_CONTB = 0 WHERE IRM_IPCBITM_CONTB IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET ct_contributionperc = IRM_IPCBITM_CONTB
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND ct_contributionperc <> IRM_IPCBITM_CONTB;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_ACAMT = 0 WHERE IRM_IPCBITM_ACAMT IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_accrual = IRM_IPCBITM_ACAMT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_accrual <> IRM_IPCBITM_ACAMT;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_MWSBP = 0 WHERE IRM_IPCBITM_MWSBP IS NULL;

UPDATE fact_chargebacks
FROM IRM_IPCBITM 
SET amt_tax = IRM_IPCBITM_MWSBP
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = IRM_IPCBITM_IPNUM AND dd_IPItem = IRM_IPCBITM_IPITM
	AND amt_tax <> IRM_IPCBITM_MWSBP;

drop table if exists tmp_chargeback_new_no_001;
create table tmp_chargeback_new_no_001 as
select distinct IRM_IPCBITM_IPNUM NEW_IPNUM, 
		IRM_IPCBITM_IPITM NEW_IPITM,
		ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
			where z.pFromCurrency  = IRM_IPCBHDR_WAERS and z.fact_script_name = 'bi_populate_chargeback_fact' 
				and z.pDate = ifnull(IRM_IPCBITM_PRSDT,IRM_IPCBHDR_ERDAT) AND z.pToCurrency = co.Currency),1) ExRate,
		ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
			where z.pFromCurrency  = IRM_IPCBHDR_WAERS and z.fact_script_name = 'bi_populate_chargeback_fact' 
				and z.pDate = ifnull(IRM_IPCBITM_PRSDT,IRM_IPCBHDR_ERDAT) AND z.pToCurrency = pGlobalCurrency),1) ExRate_GBL,
		ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
			where z.pFromCurrency  = IRM_IPCBHDR_WAERS and z.fact_script_name = 'bi_populate_chargeback_fact' 
				and z.pDate = ifnull(IRM_IPCBITM_PRSDT,IRM_IPCBHDR_ERDAT) AND z.pToCurrency = IRM_IPCBHDR_STWAE and z.pFromExchangeRate = IRM_IPCBHDR_STCUR),1) ExRate_STAT
from IRM_IPCBHDR a inner join IRM_IPCBITM b on IRM_IPCBHDR_IPNUM = IRM_IPCBITM_IPNUM
	inner join Dim_Company co on co.CompanyCode = IRM_IPCBHDR_BUKRS,
	tmp_var_chargeback_fact;

UPDATE fact_chargebacks
FROM tmp_chargeback_new_no_001 
SET amt_ExchangeRate = ExRate
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = NEW_IPNUM AND dd_IPItem = NEW_IPITM
	AND amt_ExchangeRate <> ExRate;

UPDATE fact_chargebacks
FROM tmp_chargeback_new_no_001 
SET amt_exchangerate_gbl = ExRate_GBL
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = NEW_IPNUM AND dd_IPItem = NEW_IPITM
	AND amt_exchangerate_gbl <> ExRate_GBL;

UPDATE fact_chargebacks
FROM tmp_chargeback_new_no_001 
SET amt_ExchangeRate_Stat = ExRate_STAT
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_IPNum = NEW_IPNUM AND dd_IPItem = NEW_IPITM
	AND amt_ExchangeRate_Stat <> ExRate_STAT;


UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_IPTYP = 'Not Set' WHERE IRM_IPCBHDR_IPTYP IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_chargebacktypes d
SET f.dim_chargebacktypesid = d.dim_chargebacktypesid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.Type = a.IRM_IPCBHDR_IPTYP
	AND f.dim_chargebacktypesid <> d.dim_chargebacktypesid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_VKORG = 'Not Set' WHERE IRM_IPCBHDR_VKORG IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_salesorg d
SET f.dim_salseorgid = d.dim_salesorgid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.salesorgcode = a.IRM_IPCBHDR_VKORG
	AND f.dim_salseorgid <> d.dim_salesorgid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_VTWEG = 'Not Set' WHERE IRM_IPCBHDR_VTWEG IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_distributionchannel d
SET f.dim_distributionchannelid = d.dim_distributionchannelid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.distributionchannelcode = a.IRM_IPCBHDR_VTWEG
	AND f.dim_distributionchannelid <> d.dim_distributionchannelid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_EKORG = 'Not Set' WHERE IRM_IPCBHDR_EKORG IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_purchaseorg d
SET f.dim_purchaseorgid = d.dim_purchaseorgid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.purchaseorgcode = a.IRM_IPCBHDR_EKORG
	AND f.dim_purchaseorgid <> d.dim_purchaseorgid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_KDGRP = 'Not Set' WHERE IRM_IPCBHDR_KDGRP IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_customergroup d
SET f.dim_customergroupid = d.dim_customergroupid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.customergroup = a.IRM_IPCBHDR_KDGRP
	AND f.dim_customergroupid <> d.dim_customergroupid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_BZIRK = 'Not Set' WHERE IRM_IPCBHDR_BZIRK IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_salesdistrict d
SET f.dim_salesdistrictid = d.dim_salesdistrictid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.salesdistrict = a.IRM_IPCBHDR_BZIRK
	AND f.dim_salesdistrictid <> d.dim_salesdistrictid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_AUART = 'Not Set' WHERE IRM_IPCBHDR_AUART IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_salesdocumenttype d
SET f.dim_salesdoctypeid = d.dim_salesdocumenttypeid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.documenttype = a.IRM_IPCBHDR_AUART
	AND f.dim_salesdoctypeid <> d.dim_salesdocumenttypeid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_FKART = 'Not Set' WHERE IRM_IPCBHDR_FKART IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_billingdocumenttype d
SET f.dim_billingdoctypeid = d.dim_billingdocumenttypeid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.type = a.IRM_IPCBHDR_FKART
	AND f.dim_billingdoctypeid <> d.dim_billingdocumenttypeid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_KUNAG = 'Not Set' WHERE IRM_IPCBHDR_KUNAG IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_customer d
SET f.dim_customeridSoldTo = d.dim_customerid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.customernumber = a.IRM_IPCBHDR_KUNAG
	AND f.dim_customeridSoldTo <> d.dim_customerid;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, Dim_Company co, dim_Currency d
SET f.dim_Currencyid = d.dim_Currencyid 
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND co.CompanyCode = IRM_IPCBHDR_BUKRS
	AND co.Currency = d.CurrencyCode
	AND f.dim_Currencyid <> d.dim_Currencyid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_WAERS = 'Not Set' WHERE IRM_IPCBHDR_WAERS IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_Currency d
SET f.dim_Currencyid_tra = d.dim_Currencyid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.CurrencyCode = a.IRM_IPCBHDR_WAERS
	AND f.dim_Currencyid_tra <> d.dim_Currencyid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_STWAE = 'Not Set' WHERE IRM_IPCBHDR_STWAE IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_Currency d
SET f.dim_Currencyid_stat = d.dim_Currencyid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.CurrencyCode = a.IRM_IPCBHDR_STWAE
	AND f.dim_Currencyid_stat <> d.dim_Currencyid;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_date d
SET f.dim_dateidDocDate = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBHDR_AUDAT AND d.CompanyCode = IRM_IPCBHDR_BUKRS
	AND f.dim_dateidDocDate <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_date d
SET f.dim_dateidPostingDate = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBHDR_BUDAT AND d.CompanyCode = IRM_IPCBHDR_BUKRS
	AND f.dim_dateidPostingDate <> d.dim_dateid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_BUKRS = 'Not Set' WHERE IRM_IPCBHDR_BUKRS IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_Company d
SET f.dim_Companyid = d.dim_Companyid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.CompanyCode = a.IRM_IPCBHDR_BUKRS
	AND f.dim_Companyid <> d.dim_Companyid;

UPDATE IRM_IPCBHDR SET IRM_IPCBHDR_PARBU = 'Not Set' WHERE IRM_IPCBHDR_PARBU IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_Company d
SET f.dim_CompanyidClearing = d.dim_Companyid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.CompanyCode = a.IRM_IPCBHDR_PARBU
	AND f.dim_CompanyidClearing <> d.dim_Companyid;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_date d
SET f.dim_DateidHdrCreated = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBHDR_ERDAT AND d.CompanyCode = IRM_IPCBHDR_BUKRS
	AND f.dim_DateidHdrCreated <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBHDR a, dim_date d
SET f.dim_DateidHdrLastChg = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBHDR_AEDAT AND d.CompanyCode = IRM_IPCBHDR_BUKRS
	AND f.dim_DateidHdrLastChg <> d.dim_dateid;
	

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KNUMA_AG = 'Not Set' WHERE IRM_IPCBITM_KNUMA_AG IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_agreements d
SET f.dim_agreementsid = d.dim_agreementsid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = IRM_IPCBITM_IPITM
	AND d.Agreement = a.IRM_IPCBITM_KNUMA_AG
	AND f.dim_agreementsid <> d.dim_agreementsid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KUNWE = 'Not Set' WHERE IRM_IPCBITM_KUNWE IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_customer d
SET f.dim_customeridShipTo = d.dim_customerid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = IRM_IPCBITM_IPITM
	AND d.customernumber = a.IRM_IPCBITM_KUNWE
	AND f.dim_customeridShipTo <> d.dim_customerid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KUNRG = 'Not Set' WHERE IRM_IPCBITM_KUNRG IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_customer d
SET f.dim_customeridPayer = d.dim_customerid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = IRM_IPCBITM_IPITM
	AND d.customernumber = a.IRM_IPCBITM_KUNRG
	AND f.dim_customeridPayer <> d.dim_customerid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_PSTYV = 'Not Set' WHERE IRM_IPCBITM_PSTYV IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_salesorderitemcategory d
SET f.dim_soitemcategoryid = d.dim_salesorderitemcategoryid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.salesorderitemcategory = a.IRM_IPCBITM_PSTYV
	AND f.dim_soitemcategoryid <> d.dim_salesorderitemcategoryid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_MATKL = 'Not Set' WHERE IRM_IPCBITM_MATKL IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_materialgroup d
SET f.dim_materialgroupid = d.dim_materialgroupid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.materialgroupcode = a.IRM_IPCBITM_MATKL
	AND f.dim_materialgroupid <> d.dim_materialgroupid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_MATNR = 'Not Set' WHERE IRM_IPCBITM_MATNR IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_part d
SET f.dim_partid = d.dim_partid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.PartNumber = a.IRM_IPCBITM_MATNR AND d.plant = a.IRM_IPCBITM_WERKS
	AND f.dim_partid <> d.dim_partid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_partsales d
SET f.dim_partsalesid = d.dim_partsalesid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.PartNumber = a.IRM_IPCBITM_MATNR AND d.salesorgcode = b.IRM_IPCBHDR_VKORG
	AND f.dim_partsalesid <> d.dim_partsalesid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_PRODH = 'Not Set' WHERE IRM_IPCBITM_PRODH IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_producthierarchy d
SET f.dim_producthierarchyid = d.dim_producthierarchyid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.producthierarchy = a.IRM_IPCBITM_PRODH
	AND f.dim_producthierarchyid <> d.dim_producthierarchyid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SPART = 'Not Set' WHERE IRM_IPCBITM_SPART IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_salesdivision d
SET f.dim_salesdivisionid = d.dim_salesdivisionid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.divisioncode = a.IRM_IPCBITM_SPART
	AND f.dim_salesdivisionid <> d.dim_salesdivisionid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_VKBUR = 'Not Set' WHERE IRM_IPCBITM_VKBUR IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_salesoffice d
SET f.dim_salesofficeid = d.dim_salesofficeid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.salesofficecode = a.IRM_IPCBITM_VKBUR
	AND f.dim_salesofficeid <> d.dim_salesofficeid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_VKGRP = 'Not Set' WHERE IRM_IPCBITM_VKGRP IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_salesgroup d
SET f.dim_salesgroupid = d.dim_salesgroupid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.salesgroupcode = a.IRM_IPCBITM_VKGRP
	AND f.dim_salesgroupid <> d.dim_salesgroupid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_PRCTR = 'Not Set' WHERE IRM_IPCBITM_PRCTR IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_profitcenter d
SET f.dim_profitcenterid = d.dim_profitcenterid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.profitcentercode = a.IRM_IPCBITM_PRCTR
	AND f.dim_profitcenterid <> d.dim_profitcenterid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KOSTL = 'Not Set' WHERE IRM_IPCBITM_KOSTL IS NULL;
UPDATE IRM_IPCBITM SET IRM_IPCBITM_KOKRS = 'Not Set' WHERE IRM_IPCBITM_KOKRS IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_costcenter d
SET f.dim_costcenterid = d.dim_costcenterid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.code = a.IRM_IPCBITM_KOSTL AND d.controllingarea = IRM_IPCBITM_KOKRS
	AND f.dim_costcenterid <> d.dim_costcenterid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_controllingarea d
SET f.dim_controllingareaid = d.dim_controllingareaid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.controllingareacode = a.IRM_IPCBITM_KOKRS
	AND f.dim_controllingareaid <> d.dim_controllingareaid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_GSBER = 'Not Set' WHERE IRM_IPCBITM_GSBER IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_businessarea d
SET f.dim_businessareaid = d.dim_businessareaid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.businessarea = a.IRM_IPCBITM_GSBER
	AND f.dim_businessareaid <> d.dim_businessareaid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_FKBER = 'Not Set' WHERE IRM_IPCBITM_FKBER IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_functionalarea d
SET f.dim_functionalareaid = d.dim_functionalareaid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.functionalarea = a.IRM_IPCBITM_FKBER
	AND f.dim_functionalareaid <> d.dim_functionalareaid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_WERKS = 'Not Set' WHERE IRM_IPCBITM_WERKS IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_plant d
SET f.dim_plantid = d.dim_plantid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.plantcode = a.IRM_IPCBITM_WERKS
	AND f.dim_plantid <> d.dim_plantid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_KONDM = 'Not Set' WHERE IRM_IPCBITM_KONDM IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_materialpricinggroup d
SET f.dim_materialpricinggroupid = d.dim_materialpricinggroupid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.materialpricinggroupcode = a.IRM_IPCBITM_KONDM
	AND f.dim_materialpricinggroupid <> d.dim_materialpricinggroupid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SOBKZ = 'Not Set' WHERE IRM_IPCBITM_SOBKZ IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_specialstock d
SET f.dim_specialstockid = d.dim_specialstockid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.specialstockindicator = a.IRM_IPCBITM_SOBKZ
	AND f.dim_specialstockid <> d.dim_specialstockid;

UPDATE IRM_IPCBITM SET IRM_IPCBITM_SBUKRS = 'Not Set' WHERE IRM_IPCBITM_SBUKRS IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_company d
SET f.dim_companyidSettlementDoc = d.dim_companyid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND d.CompanyCode = a.IRM_IPCBITM_SBUKRS
	AND f.dim_companyidSettlementDoc <> d.dim_companyid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_date d
SET f.dim_DateidPriceExRate = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_PRSDT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_DateidPriceExRate <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_date d
SET f.dim_DateidService = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_FBUDA AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_DateidService <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_date d
SET f.dim_DateidLastPark = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_PARKDT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_DateidLastPark <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_date d
SET f.dim_DateidLastSettlement = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_SETLDT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_DateidLastSettlement <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_date d
SET f.dim_DateidLastReversePark = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_RPDAT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_DateidLastReversePark <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_date d
SET f.dim_DateidLastREverseSettle = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_RSDAT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_DateidLastREverseSettle <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_date d
SET f.dim_DateidItemCreated = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_ERDAT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_DateidItemCreated <> d.dim_dateid;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, IRM_IPCBHDR b, dim_date d
SET f.dim_DateidItemLastChg = d.dim_dateid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND d.DateValue = a.IRM_IPCBITM_AEDAT AND d.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_DateidItemLastChg <> d.dim_dateid;


UPDATE IRM_IPCBASP SET IRM_IPCBASP_BOART_AG = 'Not Set' WHERE IRM_IPCBASP_BOART_AG IS NULL;

UPDATE fact_chargebacks f
FROM IRM_IPCBITM a, dim_chargebackcontract d
SET f.dim_chargebackcontractid = d.dim_chargebackcontractid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dd_ipnum = a.IRM_IPCBITM_IPNUM AND f.dd_ipitem = IRM_IPCBITM_IPITM
	AND d.Agreement = a.IRM_IPCBITM_KNUMA_AG
	AND f.dim_chargebackcontractid <> d.dim_chargebackcontractid;

UPDATE fact_chargebacks f
FROM dim_chargebackcontract b, dim_rebateagreementtypes d
SET f.dim_rebateagmttypesid = d.dim_rebateagreementtypesid
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE f.dim_chargebackcontractid = b.dim_chargebackcontractid
	AND d.AgreementType = b.AgreementType
	AND f.dim_rebateagmttypesid <> d.dim_rebateagreementtypesid;
	


/* 22.12.2014
   Add Vendor Dimension 
*/  
update fact_chargebacks fc
from IRM_IPCBHDR i, LFA1 l, DIM_VENDOR v
set fc.dim_vendorid = v.dim_vendorid
	,fc.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where     fc.dd_ipnum = i.IRM_IPCBHDR_IPNUM
      and i.IRM_IPCBHDR_PAYER = l.LIFNR
	  and l.LIFNR = v.vendornumber
	  and fc.dim_vendorid <> v.dim_vendorid;  
  

call vectorwise(combine 'fact_chargebacks');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_chargebacks;

drop table if exists tmp_chargeback_new_no_001;
