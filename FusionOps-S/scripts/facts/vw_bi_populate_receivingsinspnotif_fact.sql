
/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 19 May 2013 */
/*   Description    : Stored Proc bi_populate_receivingsinspnotif_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   19 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/


/* Truncate table */

Modify fact_receivingsinspnotif to truncated;

delete from NUMBER_FOUNTAIN where table_name = 'fact_receivingsinspnotif';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_receivingsinspnotif',ifnull(max(fact_receivingsinspnotifid),0)
FROM fact_receivingsinspnotif;

/* Insert 1 - fact_materialmovement */

INSERT INTO fact_receivingsinspnotif(ct_ActualInspectedQty,
                                     ct_ActualLotQty,
                                     ct_complaintQty,
                                     ct_ExternalDefectiveQty,
                                     ct_InternalDefectiveQty,
                                     ct_LotRejected,
                                     ct_QtyRejected,
                                     ct_Quantity,
                                     ct_returndeliveryqty,
                                     dd_batchno,
                                     dd_Coordinator,
                                     dd_InspectionLotNo,
                                     dd_LotType,
                                     dd_MaterialDocItemNo,
                                     dd_MaterialDocNo,
                                     dd_MaterialDocYear,
                                     dd_notificationo,
                                     dd_referencenotificationo,
                                     dd_taskcode,
                                     dd_ZmrbFlag,
                                     Dim_ActionStateid,
                                     dim_batchstoragelocationid,
                                     dim_catalogprofileid,
                                     dim_currencyid,
                                     dim_customerid,
                                     dim_dateidnotificationcompletion,
                                     dim_dateidnotificationdate,
                                     dim_DateIDPostingDate,
                                     Dim_dateidrecordcreated,
                                     dim_dateidreturndate,
                                     dim_dateidtaskcompleted,
                                     dim_dateidtaskcreated,
                                     dim_defectreporttypeid,
                                     dim_distributionchannelid,
                                     dim_inspectioncatalogtypeid,
                                     Dim_inspectionlotstatusid,
                                     dim_inspectionlotstoragelocationid,
                                     Dim_InspUsageDecisionId,
                                     dim_MaterialGroupid,
                                     Dim_MovementTypeid,
                                     dim_notificationoriginid,
                                     dim_notificationphaseid,
                                     Dim_NotificationPriorityid,
                                     dim_notificationstatusid,
                                     dim_notificationtypeid,
                                     dim_partid,
                                     dim_plantid,
                                     dim_producthierarchyid,
                                     dim_purchasegroupid,
                                     dim_purchaseorgid,
                                     dim_qualitynotificationmiscid,
                                     Dim_SalesDivisionid,
                                     dim_SalesGroupid,
                                     dim_SalesOfficeid,
                                     dim_SalesOrgid,
                                     Dim_UnitOfMeasureid,
                                     dim_vendorid,
                                     Dim_DateIdCustomDate,
                                     Dim_DateIdCustomDate1,fact_receivingsinspnotifid)
   SELECT 0 ct_ActualInspectedQty,
          0 ct_ActualLotQty,
          0 ct_complaintQty,
          0 ct_ExternalDefectiveQty,
          0 ct_InternalDefectiveQty,
          0 ct_LotRejected,
          0 ct_QtyRejected,
          ct_Quantity,
          0 ct_returndeliveryqty,
          'Not Set' dd_batchno,
          'Not Set' dd_Coordinator,
          'Not Set' dd_InspectionLotNo,
          'Receivings' dd_LotType,
          dd_MaterialDocItemNo,
          dd_MaterialDocNo,
          dd_MaterialDocYear,
          'Not Set' dd_notificationo,
          'Not Set' dd_referencenotificationo,
          'Not Set' dd_taskcode,
          'Not Set' dd_ZmrbFlag,
          2 Dim_ActionStateid,
          1 dim_batchstoragelocationid,
          1 dim_catalogprofileid,
          dim_currencyid,
          dim_customerid,
          1 dim_dateidnotificationcompletion,
          1 dim_dateidnotificationdate,
          dim_DateIDPostingDate,
          1 Dim_dateidrecordcreated,
          1 dim_dateidreturndate,
          1 dim_dateidtaskcompleted,
          1 dim_dateidtaskcreated,
          1 dim_defectreporttypeid,
          1 dim_distributionchannelid,
          1 dim_inspectioncatalogtypeid,
          1 Dim_inspectionlotstatusid,
          1 dim_inspectionlotstoragelocationid,
          Dim_InspUsageDecisionId,
          dim_MaterialGroupid,
          mm.Dim_MovementTypeid Dim_MovementTypeid,
          1 dim_notificationoriginid,
          1 dim_notificationphaseid,
          1 Dim_NotificationPriorityid,
          1 dim_notificationstatusid,
          1 dim_notificationtypeid,
          dim_partid,
          dim_plantid,
          dim_producthierarchyid,
          dim_purchasegroupid,
          dim_purchaseorgid,
          1 dim_qualitynotificationmiscid,
          1 Dim_SalesDivisionid,
          dim_SalesGroupid,
          1 dim_SalesOfficeid,
          dim_SalesOrgid,
          Dim_UnitOfMeasureid,
          dim_vendorid,
          dim_DateIDPostingDate CustomDate,
          dim_DateIDPostingDate CustomDate1,(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_receivingsinspnotif') + row_number() over()
     FROM    fact_materialmovement mm
          INNER JOIN
             dim_movementtype mt
          ON mt.dim_movementtypeid = mm.dim_movementtypeid
    WHERE mt.MovementType IN (101, 102);
	
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_receivingsinspnotifid),0) from fact_receivingsinspnotif)
where table_name = 'fact_receivingsinspnotif';	  		
	
/* Update 1 */	

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dd_inspectionlotno = il.dd_inspectionlotno
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND ifnull(rin.dd_inspectionlotno,'xx') <> ifnull(il.dd_inspectionlotno,'yy');

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.ct_actualinspectedqty = il.ct_ActualInspectedQty
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND ifnull(rin.ct_ActualInspectedQty,-1) <> ifnull(il.ct_ActualInspectedQty,-2);

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.ct_actuallotqty = il.ct_ActualLotQty
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND ifnull(rin.ct_ActualLotQty,-1) <> ifnull(il.ct_ActualLotQty,-2);

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dim_dateidinsppostingdate = il.dim_dateidposting
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND rin.dim_DateIDInspPostingDate <> il.dim_dateidposting;

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dim_dateidrecordcreated = il.Dim_dateidrecordcreated
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND rin.Dim_dateidrecordcreated <> il.Dim_dateidrecordcreated;

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dd_lottype = 'Automatic Inspections'
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND rin.dd_LotType <> 'Automatic Inspections';

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dim_inspusagedecisionid = il.Dim_InspUsageDecisionId
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND rin.Dim_InspUsageDecisionId <> il.Dim_InspUsageDecisionId;

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dim_inspectionlotstatusid = il.Dim_inspectionlotstatusid
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND rin.Dim_inspectionlotstatusid <> il.Dim_inspectionlotstatusid;

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dim_dateidinspectionend = il.dim_dateidinspectionend
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND rin.dim_dateidinspectionend <> il.dim_dateidinspectionend;

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dim_dateidinspectionstart = il.dim_dateidinspectionstart
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND rin.dim_dateidinspectionstart <> il.dim_dateidinspectionstart;

UPDATE    fact_receivingsinspnotif rin
FROM  fact_inspectionlot il
SET rin.dim_dateidlotcreated = il.dim_dateidlotcreated
WHERE rin.dd_LotType in ('Receivings','Automatic Inspections')
AND rin.dd_MaterialDocNo = il.dd_MaterialDocNo
 AND rin.dd_MaterialDocItemNo = il.dd_MaterialDocItemNo
AND rin.dd_MaterialDocYear = il.dd_MaterialDocYear
AND rin.dim_dateidlotcreated <> il.dim_dateidlotcreated;


/* End of Update 1 */	


/* Insert 2 - fact_inspectionlot */

    INSERT INTO fact_receivingsinspnotif(ct_ActualInspectedQty,
                                     ct_ActualLotQty,
                                     ct_complaintQty,
                                     ct_ExternalDefectiveQty,
                                     ct_InternalDefectiveQty,
                                     ct_LotRejected,
                                     ct_QtyRejected,
                                     ct_Quantity,
                                     ct_returndeliveryqty,
                                     dd_batchno,
                                     dd_Coordinator,
                                     dd_InspectionLotNo,
                                     dd_LotType,
                                     dd_MaterialDocItemNo,
                                     dd_MaterialDocNo,
                                     dd_MaterialDocYear,
                                     dd_notificationo,
                                     dd_referencenotificationo,
                                     dd_taskcode,
                                     dd_ZmrbFlag,
                                     Dim_ActionStateid,
                                     dim_batchstoragelocationid,
                                     dim_catalogprofileid,
                                     dim_currencyid,
                                     dim_customerid,
                                     dim_dateidnotificationcompletion,
                                     dim_dateidnotificationdate,
                                     dim_DateIDInspPostingDate,
                                     Dim_dateidrecordcreated,
                                     dim_dateidreturndate,
                                     dim_dateidtaskcompleted,
                                     dim_dateidtaskcreated,
                                     dim_defectreporttypeid,
                                     dim_distributionchannelid,
                                     dim_inspectioncatalogtypeid,
                                     Dim_inspectionlotstatusid,
                                     dim_inspectionlotstoragelocationid,
                                     Dim_InspUsageDecisionId,
                                     dim_MaterialGroupid,
                                     Dim_MovementTypeid,
                                     dim_notificationoriginid,
                                     dim_notificationphaseid,
                                     Dim_NotificationPriorityid,
                                     dim_notificationstatusid,
                                     dim_notificationtypeid,
                                     dim_partid,
                                     dim_plantid,
                                     dim_producthierarchyid,
                                     dim_purchasegroupid,
                                     dim_purchaseorgid,
                                     dim_qualitynotificationmiscid,
                                     Dim_SalesDivisionid,
                                     dim_SalesGroupid,
                                     dim_SalesOfficeid,
                                     dim_SalesOrgid,
                                     Dim_UnitOfMeasureid,
                                     dim_vendorid,
                                     dim_dateidlotcreated,fact_receivingsinspnotifid)
   SELECT il.ct_ActualInspectedQty ct_ActualInspectedQty,
          il.ct_ActualLotQty ct_ActualLotQty,
          0 ct_complaintQty,
          0 ct_ExternalDefectiveQty,
          0 ct_InternalDefectiveQty,
          0 ct_LotRejected,
          0 ct_QtyRejected,
          0 ct_Quantity,
          0 ct_returndeliveryqty,
          dd_batchno,
          'Not Set' dd_Coordinator,
          dd_InspectionLotNo,
          'Manual Inspections' dd_LotType,
          dd_MaterialDocItemNo,
          dd_MaterialDocNo,
          dd_MaterialDocYear,
          'Not Set' dd_notificationo,
          'Not Set' dd_referencenotificationo,
          'Not Set' dd_taskcode,
          'Not Set' dd_ZmrbFlag,
          2 Dim_ActionStateid,
          1 dim_batchstoragelocationid,
          1 dim_catalogprofileid,
          1 dim_currencyid,
          1 dim_customerid,
          1 dim_dateidnotificationcompletion,
          1 dim_dateidnotificationdate,
          dim_dateidposting dim_DateIDPostingDate,
          Dim_dateidrecordcreated,
          1 dim_dateidreturndate,
          1 dim_dateidtaskcompleted,
          1 dim_dateidtaskcreated,
          1 dim_defectreporttypeid,
          1 dim_distributionchannelid,
          1 dim_inspectioncatalogtypeid,
          Dim_inspectionlotstatusid,
          dim_inspectionlotstoragelocationid,
          Dim_InspUsageDecisionId,
          1 dim_MaterialGroupid,
          1 Dim_MovementTypeid,
          1 dim_notificationoriginid,
          1 dim_notificationphaseid,
          1 Dim_NotificationPriorityid,
          1 dim_notificationstatusid,
          1 dim_notificationtypeid,
          dim_partid,
          dim_plantid,
          1 dim_producthierarchyid,
          1 dim_purchasegroupid,
          dim_purchaseorgid,
          1 dim_qualitynotificationmiscid,
          1 Dim_SalesDivisionid,
          1 dim_SalesGroupid,
          1 dim_SalesOfficeid,
          1 dim_SalesOrgid,
          1 Dim_UnitOfMeasureid,
          il.dim_vendorid dim_vendorid,
          il.dim_dateidlotcreated dim_dateidlotcreated,(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_receivingsinspnotif') + row_number() over()
     FROM    fact_inspectionlot il
    WHERE il.dd_MaterialDocNo = 'Not Set';
	
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_receivingsinspnotifid),0) from fact_receivingsinspnotif)
where table_name = 'fact_receivingsinspnotif';	  			


/* Insert 3 - fact_qualitynotification */

 INSERT INTO fact_receivingsinspnotif(ct_ActualInspectedQty,
                                     ct_ActualLotQty,
                                     ct_complaintQty,
                                     ct_ExternalDefectiveQty,
                                     ct_InternalDefectiveQty,
                                     ct_LotRejected,
                                     ct_QtyRejected,
                                     ct_Quantity,
                                     ct_returndeliveryqty,
                                     dd_batchno,
                                     dd_Coordinator,
                                     dd_InspectionLotNo,
                                     dd_LotType,
                                     dd_MaterialDocItemNo,
                                     dd_MaterialDocNo,
                                     dd_MaterialDocYear,
                                     dd_notificationo,
                                     dd_referencenotificationo,
                                     dd_taskcode,
                                     dd_ZmrbFlag,
                                     Dim_ActionStateid,
                                     dim_batchstoragelocationid,
                                     dim_catalogprofileid,
                                     dim_currencyid,
                                     dim_customerid,
                                     dim_dateidnotificationcompletion,
                                     dim_dateidnotificationdate,
                                     dim_DateIDPostingDate,
                                     Dim_dateidrecordcreated,
                                     dim_dateidreturndate,
                                     dim_dateidtaskcompleted,
                                     dim_dateidtaskcreated,
                                     dim_defectreporttypeid,
                                     dim_distributionchannelid,
                                     dim_inspectioncatalogtypeid,
                                     Dim_inspectionlotstatusid,
                                     dim_inspectionlotstoragelocationid,
                                     Dim_InspUsageDecisionId,
                                     dim_MaterialGroupid,
                                     Dim_MovementTypeid,
                                     dim_notificationoriginid,
                                     dim_notificationphaseid,
                                     Dim_NotificationPriorityid,
                                     dim_notificationstatusid,
                                     dim_notificationtypeid,
                                     dim_partid,
                                     dim_plantid,
                                     dim_producthierarchyid,
                                     dim_purchasegroupid,
                                     dim_purchaseorgid,
                                     dim_qualitynotificationmiscid,
                                     Dim_SalesDivisionid,
                                     dim_SalesGroupid,
                                     dim_SalesOfficeid,
                                     dim_SalesOrgid,
                                     Dim_UnitOfMeasureid,
                                     dim_vendorid,
                                     Dim_DateIdCustomDate,
                                     Dim_DateIdCustomDate1,fact_receivingsinspnotifid)
    SELECT 0 ct_ActualInspectedQty,
                                     0 ct_ActualLotQty,
                                     ct_complaintQty,
                                     ct_ExternalDefectiveQty,
                                     ct_InternalDefectiveQty,
                                     ct_LotRejected,
                                     ct_QtyRejected,
                                     0 ct_Quantity,
                                     ct_returndeliveryqty,
                                     dd_batchno,
                                     dd_Coordinator,
                                     dd_InspectionLotNo,
                                     (CASE WHEN dd_referencenotificationo = 'Not Set' THEN 'Notifications' ELSE 'Referenced Notifications' END) dd_LotType,
                                     dd_MaterialDocItemNo,
                                     dd_MaterialDocNo,
                                     dd_MaterialDocYear,
                                     dd_notificationo,
                                     dd_referencenotificationo,
                                     dd_taskcode,
                                     dd_ZmrbFlag,
                                     Dim_ActionStateid,
                                     dim_batchstoragelocationid,
                                     dim_catalogprofileid,
                                     dim_currencyid,
                                     dim_customerid,
                                     dim_dateidnotificationcompletion,
                                     dim_dateidnotificationdate,
                                     1 dim_DateIDPostingDate,
                                     Dim_dateidrecordcreated,
                                     dim_dateidreturndate,
                                     dim_dateidtaskcompleted,
                                     dim_dateidtaskcreated,
                                     dim_defectreporttypeid,
                                     dim_distributionchannelid,
                                     dim_inspectioncatalogtypeid,
                                     1 Dim_inspectionlotstatusid,
                                     dim_inspectionlotstoragelocationid,
                                     1 Dim_InspUsageDecisionId,
                                     dim_MaterialGroupid,
                                     1 Dim_MovementTypeid,
                                     dim_notificationoriginid,
                                     dim_notificationphaseid,
                                     Dim_NotificationPriorityid,
                                     dim_notificationstatusid,
                                     dim_notificationtypeid,
                                     dim_partid,
                                     dim_plantid,
                                     dim_producthierarchyid,
                                     dim_purchasegroupid,
                                     dim_purchaseorgid,
                                     dim_qualitynotificationmiscid,
                                     Dim_SalesDivisionid,
                                     dim_SalesGroupid,
                                     dim_SalesOfficeid,
                                     dim_SalesOrgid,
                                     Dim_UnitOfMeasureid,
                                     dim_vendorid,
                                     Dim_dateidrecordcreated,
                                     dim_dateidnotificationcompletion,
									 (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_receivingsinspnotif') + row_number() over()
FROM fact_qualitynotification qn
WHERE qn.dd_MaterialDocNo = 'Not Set';

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_receivingsinspnotifid),0) from fact_receivingsinspnotif)
where table_name = 'fact_receivingsinspnotif';	  	


/* Insert 4 - fact_qualitynotification */


INSERT INTO fact_receivingsinspnotif(ct_ActualInspectedQty,
                                     ct_ActualLotQty,
                                     ct_complaintQty,
                                     ct_ExternalDefectiveQty,
                                     ct_InternalDefectiveQty,
                                     ct_LotRejected,
                                     ct_QtyRejected,
                                     ct_Quantity,
                                     ct_returndeliveryqty,
                                     dd_batchno,
                                     dd_Coordinator,
                                     dd_InspectionLotNo,
                                     dd_LotType,
                                     dd_MaterialDocItemNo,
                                     dd_MaterialDocNo,
                                     dd_MaterialDocYear,
                                     dd_notificationo,
                                     dd_referencenotificationo,
                                     dd_taskcode,
                                     dd_ZmrbFlag,
                                     Dim_ActionStateid,
                                     dim_batchstoragelocationid,
                                     dim_catalogprofileid,
                                     dim_currencyid,
                                     dim_customerid,
                                     dim_dateidnotificationcompletion,
                                     dim_dateidnotificationdate,
                                     dim_DateIDPostingDate,
                                     Dim_dateidrecordcreated,
                                     dim_dateidreturndate,
                                     dim_dateidtaskcompleted,
                                     dim_dateidtaskcreated,
                                     dim_defectreporttypeid,
                                     dim_distributionchannelid,
                                     dim_inspectioncatalogtypeid,
                                     Dim_inspectionlotstatusid,
                                     dim_inspectionlotstoragelocationid,
                                     Dim_InspUsageDecisionId,
                                     dim_MaterialGroupid,
                                     Dim_MovementTypeid,
                                     dim_notificationoriginid,
                                     dim_notificationphaseid,
                                     Dim_NotificationPriorityid,
                                     dim_notificationstatusid,
                                     dim_notificationtypeid,
                                     dim_partid,
                                     dim_plantid,
                                     dim_producthierarchyid,
                                     dim_purchasegroupid,
                                     dim_purchaseorgid,
                                     dim_qualitynotificationmiscid,
                                     Dim_SalesDivisionid,
                                     dim_SalesGroupid,
                                     dim_SalesOfficeid,
                                     dim_SalesOrgid,
                                     Dim_UnitOfMeasureid,
                                     dim_vendorid,
                                     Dim_DateIdCustomDate,
                                     Dim_DateIdCustomDate1,fact_receivingsinspnotifid)
    SELECT 0 ct_ActualInspectedQty,
                                     0 ct_ActualLotQty,
                                     ct_complaintQty,
                                     ct_ExternalDefectiveQty,
                                     ct_InternalDefectiveQty,
                                     ct_LotRejected,
                                     ct_QtyRejected,
                                     0 ct_Quantity,
                                     ct_returndeliveryqty,
                                     dd_batchno,
                                     dd_Coordinator,
                                     dd_InspectionLotNo,
                                     (CASE WHEN dd_referencenotificationo = 'Not Set' THEN 'Notifications' ELSE 'Referenced Notifications' END) dd_LotType,
                                     dd_MaterialDocItemNo,
                                     dd_MaterialDocNo,
                                     dd_MaterialDocYear,
                                     dd_notificationo,
                                     dd_referencenotificationo,
                                     dd_taskcode,
                                     dd_ZmrbFlag,
                                     Dim_ActionStateid,
                                     dim_batchstoragelocationid,
                                     dim_catalogprofileid,
                                     dim_currencyid,
                                     dim_customerid,
                                     dim_dateidnotificationcompletion,
                                     dim_dateidnotificationdate,
                                     1 dim_DateIDPostingDate,
                                     Dim_dateidrecordcreated,
                                     dim_dateidreturndate,
                                     dim_dateidtaskcompleted,
                                     dim_dateidtaskcreated,
                                     dim_defectreporttypeid,
                                     dim_distributionchannelid,
                                     dim_inspectioncatalogtypeid,
                                     1 Dim_inspectionlotstatusid,
                                     dim_inspectionlotstoragelocationid,
                                     1 Dim_InspUsageDecisionId,
                                     dim_MaterialGroupid,
                                     1 Dim_MovementTypeid,
                                     dim_notificationoriginid,
                                     dim_notificationphaseid,
                                     Dim_NotificationPriorityid,
                                     dim_notificationstatusid,
                                     dim_notificationtypeid,
                                     dim_partid,
                                     dim_plantid,
                                     dim_producthierarchyid,
                                     dim_purchasegroupid,
                                     dim_purchaseorgid,
                                     dim_qualitynotificationmiscid,
                                     Dim_SalesDivisionid,
                                     dim_SalesGroupid,
                                     dim_SalesOfficeid,
                                     dim_SalesOrgid,
                                     Dim_UnitOfMeasureid,
                                     dim_vendorid,
                                     Dim_dateidrecordcreated,
                                     dim_dateidnotificationcompletion,
									  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_receivingsinspnotif') + row_number() over()
FROM fact_qualitynotification qn
WHERE qn.dd_MaterialDocNo <> 'Not Set';

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_receivingsinspnotifid),0) from fact_receivingsinspnotif)
where table_name = 'fact_receivingsinspnotif';	  	


