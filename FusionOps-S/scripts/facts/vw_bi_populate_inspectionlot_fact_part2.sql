/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc  */
/*   22  Dec  2014    Amar    	1.1               Update Material Movement for Inspection Lots */
/******************************************************************************************************************/

/*Step converted to script from multi-statement in App Manager */

/* Step - Update Usage Decision in Material Movement */
UPDATE fact_materialmovement ms
	FROM
         fact_inspectionlot fil
    SET ms.dim_inspusagedecisionid = fil.dim_inspusagedecisionid,
			ms.dw_update_date = current_timestamp
  WHERE     ms.dd_MaterialDocItemNo = fil.dd_MaterialDocItemNo
        AND ms.dd_MaterialDocNo = fil.dd_MaterialDocNo
        AND ms.dd_MaterialDocYear = fil.dd_MaterialDocYear;
		
/*Step - Update Lots Not in QM */

drop table if exists ttt_00;

create table ttt_00 as select il.dd_MaterialDocYear,il.dd_MaterialDocItemNo, il.dd_MAterialDocNo
FROM facT_inspectionlot il, dim_inspectionlotstatus ils
WHERE il.Dim_InspectionLotStatusId = ils.dim_inspectionlotstatusid AND ils.LotCancelled <> 'X'
GROUP by il.dd_MaterialDocYear,il.dd_MaterialDocItemNo, il.dd_MAterialDocNo;

Update fact_materialmovement mm
FROM dim_movementtype mt
SET mm.ct_LotNotThruQM =1,
			mm.dw_update_date = current_timestamp
WHERE mm.Dim_MovementTypeid = mt.Dim_MovementTypeid;

Update fact_materialmovement mm
FROM dim_movementtype mt,ttt_00 tt
SET mm.ct_LotNotThruQM =0,
			mm.dw_update_date = current_timestamp
WHERE mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
AND  mm.dd_MaterialDocYear = tt.dd_MaterialDocYear
AND  mm.dd_MaterialDocItemNo = tt.dd_MaterialDocItemNo
AND mm.dd_MaterialDocNo = tt.dd_MAterialDocNo
AND  mt.MovementType = '101'
AND  mm.ct_LotNotThruQM <> 0;

Update fact_materialmovement mm
FROM dim_movementtype mt
SET mm.ct_LotNotThruQM =0,
			mm.dw_update_date = current_timestamp
WHERE mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
AND  mt.MovementType <> '101'
AND  mm.ct_LotNotThruQM <> 0;

drop table if exists ttt_00;

--Step -Update Inspection Lot Flags in Material Movement

CALL vectorwise(combine 'fact_materialmovement');
call vectorwise(combine 'fact_materialmovement_forLots_tmp-fact_materialmovement_forLots_tmp');

UPDATE fact_materialmovement
SET LotsAcceptedFlag = 'N',
    dw_update_date = current_timestamp
WHERE LotsAcceptedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsAwaitingInspectionFlag = 'N',
    dw_update_date = current_timestamp
WHERE LotsAwaitingInspectionFlag = 'Y';

UPDATE fact_materialmovement
SET LotsInspectedFlag = 'N',
    dw_update_date = current_timestamp
WHERE LotsInspectedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsSkippedFlag = 'N',
    dw_update_date = current_timestamp
WHERE LotsSkippedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsAcceptedFlag = 'N',
    dw_update_date = current_timestamp
WHERE PercentLotsAcceptedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsInspectedFlag = 'N',
    dw_update_date = current_timestamp
WHERE PercentLotsInspectedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsRejectedFlag = 'N',
    dw_update_date = current_timestamp
WHERE PercentLotsRejectedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsReceivedFlag = 'N',
    dw_update_date = current_timestamp
WHERE LotsReceivedFlag = 'Y';

CALL vectorwise(combine 'fact_materialmovement');

INSERT INTO fact_materialmovement_forLots_tmp(fact_materialmovementid,
                                              dd_MaterialDocNo,
                                              dd_MaterialDocItemNo,
                                              dd_MaterialDocYear,
                                              dim_movementtypeid,
                                              Rank1,
                                              LotsAcceptedFlag,
                                              LotsAwaitingInspectionFlag,
                                              LotsInspectedFlag,
                                              LotsRejectedFlag,
                                              LotsSkippedFlag,
                                              PercentLotsAcceptedFlag,
                                              PercentLotsInspectedFlag,
                                              PercentLotsRejectedFlag,LotsReceivedFlag)
   SELECT fact_materialmovementid,
          dd_MaterialDocNo,
          dd_MaterialDocItemNo,
          dd_MaterialDocYear,
          mm.dim_movementtypeid,
          RANK() OVER (PARTITION BY dd_MaterialDocNo, dd_MaterialDocItemNo, dd_MaterialDocYear,mm.dim_movementtypeid ORDER BY fact_materialmovementid ASC) as Rank1,
'N','N','N','N','N','N','N','N','N'
from fact_materialmovement mm, dim_movementtype mt
where mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = mm.dim_movementtypeid;


DELETE FROM fact_materialmovement_forLots_tmp
 WHERE Rank1 <> 1;

CALL vectorwise(combine 'fact_materialmovement_forLots_tmp');

UPDATE    fact_materialmovement_forLots_tmp f_mm
        FROM
          dim_movementtype mt
   SET f_mm.LotsAcceptedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils,
                              dim_CodeText ud
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND ud.dim_codetextid =
                                     il.dim_inspusagedecisionid
                              AND ud.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACC',
                                      'ACCI',
                                      'ACUS',
                                      'ACCP',
                                      'ACCN','A50','AX')), 'N')
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

UPDATE    fact_materialmovement_forLots_tmp f_mm
        FROM
          dim_movementtype mt
   SET        f_mm.LotsAwaitingInspectionFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_inspectionlotmisc ilm,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dim_inspectionlotmiscid =
                                     ilm.dim_inspectionlotmiscid
                              AND ilm.LotSkipped <> 'X' AND il.dim_inspusagedecisionid = 1), 'N')
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

CALL vectorwise(combine 'fact_materialmovement_forLots_tmp');

UPDATE    fact_materialmovement_forLots_tmp f_mm
        FROM
          dim_movementtype mt
   SET        f_mm.LotsInspectedFlag = ifnull((SELECT 'Y' FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dim_inspusagedecisionid <> 1), 'N')
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

UPDATE    fact_materialmovement_forLots_tmp f_mm
        FROM
          dim_movementtype mt
   SET        f_mm.LotsRejectedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid <> 1
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND udc.Code NOT IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCP',
                                      'ACCI',
                                      'ACCN',
                                      'ACUS',
                                      'ACC','A50','AX')), 'N')
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

UPDATE    fact_materialmovement_forLots_tmp f_mm
        FROM
          dim_movementtype mt
   SET        LotsSkippedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_inspectionlotmisc ilm,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dim_inspectionlotmiscid =
                                     ilm.dim_inspectionlotmiscid
                              AND ilm.LotSkipped = 'X'), 'N')
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

CALL vectorwise(combine 'fact_materialmovement_forLots_tmp');

UPDATE    fact_materialmovement_forLots_tmp f_mm
     FROM
      dim_movementtype mt
   SET PercentLotsAcceptedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND udc.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCP',
                                      'ACCI',
                                      'ACCN',
                                      'ACUS',
                                      'ACC','A50','AX')), 'N')
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

CALL vectorwise(combine 'fact_materialmovement_forLots_tmp');

UPDATE    fact_materialmovement_forLots_tmp f_mm
        FROM
          dim_movementtype mt
   SET        PercentLotsInspectedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils,
                              dim_codetext ct
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dim_inspusagedecisionid =
                                     ct.dim_Codetextid
                              AND ct.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCI',
                                      'ACCP',
                                      'QUAR',
                                      'QURP',
                                      'REJO',
                                      'REJR',
                                      'REJS',
                                      'REJV','A50','AX','R','R1','R2','R3','R4','R40','RQ','X')), 'N')
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

CALL vectorwise(combine 'fact_materialmovement_forLots_tmp');

UPDATE    fact_materialmovement_forLots_tmp f_mm
        FROM
          dim_movementtype mt
   SET        PercentLotsRejectedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND il.dim_inspusagedecisionid <> 1
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND udc.Code IN
                                     ('QUAR',
                                      'QURP',
                                      'REJO',
                                      'REJR',
                                      'REJS',
                                      'REJV','R','R1','R2','R3','R4','R40','RQ','X')), 'N')
 WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

UPDATE    fact_materialmovement_forLots_tmp f_mm
        FROM
          dim_movementtype mt
   SET        LotsReceivedFlag = 'Y' 
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

CALL vectorwise(combine 'fact_materialmovement_forLots_tmp');


UPDATE fact_materialmovement f_mm
  FROM fact_materialmovement_forLots_tmp t
   SET f_mm.LotsAcceptedFlag = t.LotsAcceptedFlag,
       f_mm.LotsAwaitingInspectionFlag = t.LotsAwaitingInspectionFlag,
       f_mm.LotsInspectedFlag = t.LotsInspectedFlag,
       f_mm.LotsRejectedFlag = t.LotsRejectedFlag,
       f_mm.LotsSkippedFlag = t.LotsSkippedFlag,
       f_mm.PercentLotsAcceptedFlag = t.PercentLotsAcceptedFlag,
       f_mm.PercentLotsInspectedFlag = t.PercentLotsInspectedFlag,
       f_mm.PercentLotsRejectedFlag = t.PercentLotsRejectedFlag,
       f_mm.LotsReceivedflag = t.LotsReceivedFlag,
       f_mm.dw_update_date = current_timestamp
 WHERE     f_mm.fact_materialmovementid = t.fact_materialmovementid
       AND f_mm.dd_MaterialDocNo = t.dd_MaterialDocNo
       AND f_mm.dd_MaterialDocItemNo = t.dd_MaterialDocItemNo
       AND f_mm.dd_MaterialDocYear = t.dd_MaterialDocYear
       AND f_mm.dim_movementtypeid = t.dim_movementtypeid;
	   
update fact_inspectionlot f_il
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit
set f_il.ct_AverageInspectionDuration = ifnull(qm.QMAT_MPDAU,0)
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.ct_AverageInspectionDuration <> ifnull(qm.QMAT_MPDAU,0);

/*BC merck to base*/
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa	   
   SET dd_movementtype = ifnull(QALS_BWART, 'Not Set')
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   and dd_movementtype <> ifnull(QALS_BWART, 'Not Set');
	   

update fact_inspectionlot f_il
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit
set f_il.dd_posttoinspstock = ifnull(qm.QMAT_INSMK,'Not Set')
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_posttoinspstock <> ifnull(qm.QMAT_INSMK,'Not Set');

update fact_inspectionlot f_il
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit
set f_il.dd_skipsallowed = ifnull(qm.QMAT_DYN,'Not Set')
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_skipsallowed <> ifnull(qm.QMAT_DYN,'Not Set');


call vectorwise(combine 'fact_materialmovement_forLots_tmp- fact_materialmovement_forLots_tmp');

CALL vectorwise(combine 'fact_materialmovement');
		
