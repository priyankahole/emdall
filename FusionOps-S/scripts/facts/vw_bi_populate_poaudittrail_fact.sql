select current_time;

select 'START OF PROC VW_bi_populate_poaudittrail_fact',TIMESTAMP(LOCAL_TIMESTAMP);

drop table if exists fact_purchase_temp;
create table fact_purchase_temp as select distinct dd_DocumentNo from fact_purchase;

delete from NUMBER_FOUNTAIN where table_name = 'fact_poaudittrail';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_poaudittrail',ifnull(max(fact_poaudittrailid),0)
FROM fact_poaudittrail;

/* Get ItemNo and ScheduleNo before inserting in the fact table because VW does not support functions in join condition*/
update CDPOS_PO set CDPOS_ItemNo = substr(CDPOS_TABKEY,14,5);

update CDPOS_PO set CDPOS_ScheduleNo = substr(CDPOS_TABKEY,19,4);

call vectorwise (combine 'CDPOS_PO');

insert into fact_poaudittrail
(
  fact_POAuditTrailid,
  dd_ChangeDocNo,
  dd_UserName,
  dim_DateidChange,
  dd_TimeChange,
  dd_PLAN_ChangeNo,
  dd_ACT_ChangeNo,
  dd_Planned,
  dd_DocumentNo,
  dd_ItemNo,
  dd_ScheduleNo,
  dd_TableName,
  dd_FieldName,
  dd_ChangeType,
  dd_TextCase,
  dim_UnitOfMeasureidOld,
  dim_UnitofmeasureidNew,
  dim_CurrencyidOld,
  dim_CurrencyidNew,
  dim_DateidItemDeliveryOld,
  dim_DateidItemDeliveryNew,
  dim_DateidStatRelDeliveryOld,
  dim_DateidStatRelDeliveryNew,
  dd_ReleaseIndicatorOld,
  dd_ReleaseIndicatorNew,
  dd_ReleasedOld,
  dd_ReleasedNew,
  dd_ProcessStatusOld,
  dd_ProcessStatusNew,
  amt_ReleaseTotalValOld,
  amt_ReleaseTotalValNew,
  dim_DateidItemCreationOld,
  dim_DateidItemCreationNew,
  amt_GrossValueOld,
  amt_GrossValueNew,
  amt_EffectiveValueOld,
  amt_EffectiveValueNew,
  dd_FinalInvoiceIndOld,
  dd_FinalInvoiceIndNew,
  amt_TaxAmountOld,
  amt_TaxAmountNew,
  amt_NetOrderPriceOld,
  amt_NetOrderPriceNew,
  amt_ItemNetPriceOld,
  amt_ItemNetPriceNew,
  dd_ItemEstimatedPriceOld,
  dd_ItemEstimatedPriceNew,
  dd_ItemDeletionIndicatorOld,
  dd_ItemDeletionIndicatorNew,
  ct_TargetQuantityOld,
  ct_TargetQuantityNew,
  ct_OrderQuantityOld,
  ct_OrderQuantityNew,
  ct_ScheduledQuantityOld,
  ct_ScheduledQuantityNew,
  Dim_CompanyId,
  Dim_Currencyid,	
  Dim_DateidCreate,
  Dim_DateidItemCreate,
  Dim_DocumentTypeid,
  Dim_PlantidOrdering,
  Dim_POCurrencyid,
  Dim_Producthierarchyid,
  Dim_PurchaseGroupid,
  Dim_PurchaseOrgid,
  Dim_StorageLocationid,
  Dim_Vendorid,
  dim_Currencyid_TRA,
  dim_Currencyid_GBL,
  amt_ExchangeRate,
  amt_ExchangeRate_GBL
)
select
  ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_poaudittrail') + row_number() over ()) fact_poaudittrailid,
  CDHDR_CHANGENR	dd_ChangeDocNo,
  CDHDR_USERNAME	dd_UserName,
  dim_Dateid		dim_DateidChange,
  CDHDR_UTIME		dd_TimeChange,
  ifnull(CDHDR_PLANCHNGNR, 'Not Set')	dd_PLAN_ChangeNo,
  ifnull(CDHDR_ACT_CHNGNO, 'Not Set')  dd_ACT_ChangeNo,
  ifnull(CDHDR_WAS_PLANND, 'Not Set')  dd_Planned,
  ifnull(CDHDR_OBJECTID, 'Not Set')	dd_DocumentNo,
  CDPOS_ItemNo		dd_ItemNo,
  CDPOS_ScheduleNo  dd_ScheduleNo,
  ifnull(CDPOS_TABNAME, 'Not Set')		dd_TableName,
  ifnull(CDPOS_FNAME, 'Not Set')		dd_FieldName,
  ifnull(CDPOS_CHNGIND, 'Not Set')		dd_ChangeType,
  ifnull(CDPOS_TEXT_CASE, 'Not Set')	dd_TextCase,
  ifnull((select Dim_UnitOfMeasureid from dim_unitofmeasure where UOM = CDPOS_UNIT_OLD), 1)	dim_UnitOfMeasureidOld,
  ifnull((select Dim_UnitOfMeasureid from dim_unitofmeasure where UOM = CDPOS_UNIT_New), 1)	dim_UnitOfMeasureidNew,
  ifnull((select Dim_Currencyid from dim_currency where CurrencyCode = CDPOS_CUKY_OLD), 1) 	dim_CurrencyidOld,
  ifnull((select Dim_Currencyid from dim_currency where CurrencyCode = CDPOS_CUKY_NEW), 1) 	dim_CurrencyidNew,
  1 dim_DateidItemDeliveryOld,
  1 dim_DateidItemDeliveryNew,
  1 dim_DateidStatRelDeliveryOld,
  1 dim_DateidStatRelDeliveryNew,
  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'FRGKE' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
  	   else 'Not Set' end dd_ReleaseIndicatorOld,
  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'FRGKE' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
  	   else 'Not Set' end dd_ReleaseIndicatorNew,
  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'FRGZU' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
  	   else 'Not Set' end dd_ReleasedOld,
  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'FRGZU' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
  	   else 'Not Set' end dd_ReleasedNew,
  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'PROCSTAT' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
  	   else 'Not Set' end dd_ProcessStatusOld,
  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'PROCSTAT' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
  	   else 'Not Set' end dd_ProcessStatusNew,
  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'RLWRT' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
  	   else 0.00 end amt_ReleaseTotalValOld,
  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'RLWRT' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
  	   else 0.00 end amt_ReleaseTotalValNew,  
  1 dim_DateidItemCreationOld,
  1 dim_DateidItemCreationNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'BRTWR' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
  	   else 0.00 end amt_GrossValueOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'BRTWR' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
  	   else 0.00 end amt_GrossValueNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'EFFWR' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
  	   else 0.00 end amt_EffectiveValueOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'EFFWR' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
  	   else 0.00 end amt_EffectiveValueNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'EREKZ' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
  	   else 'Not Set' end dd_FinalInvoiceIndOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'EREKZ' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
  	   else 'Not Set' end dd_FinalInvoiceIndNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NAVNW' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
  	   else 0.00 end amt_TaxAmountOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NAVNW' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
  	   else 0.00 end amt_TaxAmountNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NETPR' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
  	   else 0.00 end amt_NetOrderPriceOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NETPR' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
  	   else 0.00 end amt_NetOrderPriceNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NETWR' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
  	   else 0.00 end amt_ItemNetPriceOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NETWR' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
  	   else 0.00 end amt_ItemNetPriceNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'SCHPR' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
  	   else 'Not Set' end dd_ItemEstimatedPriceOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'SCHPR' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
  	   else 'Not Set' end dd_ItemEstimatedPriceNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'LOEKZ' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
  	   else 'Not Set' end dd_ItemDeletionIndicatorOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'LOEKZ' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
  	   else 'Not Set' end dd_ItemDeletionIndicatorNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'KTMNG' then ifnull(decimal(CDPOS_VALUE_OLD,18,3), 0)
  	   else 0.000 end ct_TargetQuantityOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'KTMNG' then ifnull(decimal(CDPOS_VALUE_NEW,18,3), 0)
  	   else 0.000 end ct_TargetQuantityNew,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'MENGE' then ifnull(decimal(CDPOS_VALUE_OLD,18,3), 0)
  	   else 0.000 end ct_OrderQuantityOld,
  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'MENGE' then ifnull(decimal(CDPOS_VALUE_NEW,18,3), 0)
  	   else 0.000 end ct_OrderQuantityNew,
  case when CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'MENGE' then ifnull(decimal(CDPOS_VALUE_OLD,18,3), 0)
  	   else 0.000 end ct_ScheduledQuantityOld,
  case when CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'MENGE' then ifnull(decimal(CDPOS_VALUE_NEW,18,3), 0)
  	   else 0.000 end ct_ScheduledQuantityNew,
  ifnull((select distinct Dim_CompanyId from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_CompanyId, -- header
  ifnull((select distinct Dim_Currencyid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_Currencyid, -- header
  ifnull((select distinct Dim_DateidCreate from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_DateidCreate, -- header
  ifnull((select distinct Dim_DateidItemCreate from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID and dd_DocumentItemNo = CDPOS_ItemNo),1) Dim_DateidItemCreate, -- item
  ifnull((select distinct Dim_DocumentTypeid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_DocumentTypeid, -- header
  ifnull((select distinct Dim_PlantidOrdering from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID and dd_DocumentItemNo = CDPOS_ItemNo),1) Dim_PlantidOrdering, -- item
  ifnull((select distinct Dim_POCurrencyid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_POCurrencyid, -- header
  ifnull((select distinct Dim_Producthierarchyid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID and dd_DocumentItemNo = CDPOS_ItemNo),1) Dim_Producthierarchyid, -- item
  ifnull((select distinct Dim_PurchaseGroupid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_PurchaseGroupid, -- header
  ifnull((select distinct Dim_PurchaseOrgid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_PurchaseOrgid, -- header
  ifnull((select distinct Dim_StorageLocationid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID and dd_DocumentItemNo = CDPOS_ItemNo),1) Dim_StorageLocationid, -- item
  ifnull((select distinct Dim_Vendorid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_Vendorid, -- header
  ifnull((select distinct Dim_Currencyid_TRA from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_Currencyid_TRA,
  ifnull((select distinct Dim_Currencyid_GBL from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_Currencyid_GBL,
  ifnull((select distinct amt_ExchangeRate from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) amt_ExchangeRate,
  ifnull((select distinct amt_ExchangeRate_GBL from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) amt_ExchangeRate_GBL
from CDHDR_PO inner join CDPOS_PO on CDHDR_OBJECTID = CDPOS_OBJECTID and CDHDR_CHANGENR = CDPOS_CHANGENR
			  inner join fact_purchase_temp fp on CDPOS_OBJECTID = fp.dd_DocumentNo
  			  inner join dim_date dd on CDHDR_UDATE = dd.DateValue and dd.CompanyCode = 'Not Set'
where not exists (select 1 from fact_poaudittrail where dd_ChangeDocNo = CDPOS_CHANGENR and dd_DocumentNo = CDPOS_OBJECTID and dd_TableName = CDPOS_TABNAME  and dd_FieldName = CDPOS_FNAME and dd_ChangeType = CDPOS_CHNGIND and dd_ItemNo = CDPOS_ItemNo and dd_ScheduleNo = CDPOS_ScheduleNo);
			
/* Update date dim ids because VW does not support select in a case conditional expression*/			
update fact_poaudittrail
from CDPOS_PO, dim_date dd
set dim_DateidItemDeliveryOld = dd.Dim_Dateid
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'EINDT' and
	  STR_TO_DATE(CDPOS_VALUE_OLD,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
from CDPOS_PO, dim_date dd
set dim_DateidItemDeliveryNew = dd.Dim_Dateid
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'EINDT' and
	  STR_TO_DATE(CDPOS_VALUE_NEW,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
from CDPOS_PO, dim_date dd
set dim_DateidStatRelDeliveryOld = dd.Dim_Dateid
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'SLFDT' and
	  STR_TO_DATE(CDPOS_VALUE_OLD,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
from CDPOS_PO, dim_date dd
set dim_DateidStatRelDeliveryNew = dd.Dim_Dateid
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'SLFDT' and
	  STR_TO_DATE(CDPOS_VALUE_NEW,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
from CDPOS_PO, dim_date dd
set dim_DateidItemCreationOld = dd.Dim_Dateid
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'AEDAT' and
	  STR_TO_DATE(CDPOS_VALUE_OLD,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
from CDPOS_PO, dim_date dd
set dim_DateidItemCreationNew = dd.Dim_Dateid
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'AEDAT' and
	  STR_TO_DATE(CDPOS_VALUE_NEW,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
call vectorwise (combine 'fact_poaudittrail');

drop table if exists fact_purchase_temp;