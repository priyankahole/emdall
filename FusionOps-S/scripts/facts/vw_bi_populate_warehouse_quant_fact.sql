

drop table if exists fact_wmquant_tmp ;

CREATE TABLE fact_wmquant_tmp 
AS 
select * from fact_wmquant where 1 = 2;

/*initialize NUMBER_FOUNTAIN*/

delete from NUMBER_FOUNTAIN f where f.table_name = 'fact_wmquant';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_wmquant',ifnull(max(f.fact_wmquantid) , ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) 
FROM fact_wmquant f;



INSERT INTO fact_wmquant_tmp (fact_wmquantid,
								dim_warehousenumberid,
								dd_quantno,
								dim_partid,
								dim_plantid,
								dim_storagetypeid,
								dim_storagebinid,
								dim_storagelocationid,
								dim_unitofmeasureid,
								ct_availablestock,
								dd_batchnumber,
								dim_wmstockcategoryid,
								dim_specialstockid,
								dd_specialstockno,
								dim_wmblockingreasonid,
								dim_dateidlastmovement,
								dd_lastchangetodocno,
								dd_lastchangetodocitemno,
								dim_dateidlaststockplacement,
								dim_dateidlaststockremoval,
								dim_dateidlaststockaddition,
								dim_dateidgoodsreceipt,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dim_wmstorageunittypeid,
								ct_totalqty,
								ct_putawayqty,
								ct_removeqty,
								ct_materialweight,
								dim_weightunitid,
								dd_transferreqno,
								dd_inventorydocitemno,
								dim_wmrequirementtypeid,
								dd_requirementno,
								dd_storageunitno,
								dd_inspectionlotno,
								dim_dateidbestbefore,
								ct_capacityusage,
								dim_wmpickingareaid,
								ct_opentransferqty,
								dd_deliveryno,
								dd_deliveryitemno,
								dim_dateidlastinventory,
								dim_wmquantmiscellaneousid)
   SELECT 	((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN  WHERE table_name = 'fact_wmquant') 
            + row_number() over () )  fact_wmquantid,
			wn.dim_warehousenumberid as dim_warehousenumberid,
			ifnull(st.LQUA_LQNUM, 'Not Set') as dd_quantno,
			ifnull((SELECT dim_partid
                       FROM dim_part dp
                      WHERE dp.PartNumber = st.LQUA_MATNR AND dp.plant = st.LQUA_WERKS),
                    1) as dim_partid,
			pl.Dim_Plantid,
			ifnull((SELECT sty.dim_wmstoragetypeid
                      FROM dim_wmstoragetype sty
                     WHERE sty.storagetype = st.LQUA_LGTYP
					 AND sty.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')),
                   1) as dim_storagetypeid,  				   
			ifnull((SELECT sb.dim_storagebinid
                      FROM dim_storagebin sb
                     WHERE sb.storagebin = st.LQUA_LGPLA
                           AND sb.storagetype = st.LQUA_LGTYP
						   AND sb.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')),
                   1) as dim_storagebinid,                 
			ifnull((SELECT sl.dim_storagelocationid
                      FROM dim_storagelocation sl
                     WHERE sl.plant = st.LQUA_WERKS
                           AND sl.locationcode = st.LQUA_LGORT),
                   1) as dim_storagelocationid,
			ifnull((SELECT um.dim_unitofmeasureid
                      FROM dim_unitofmeasure um
                     WHERE um.uom = st.LQUA_MEINS),
                   1) as dim_unitofmeasureid,
			ifnull(st.LQUA_VERME, 0) as ct_availablestock,
			ifnull(st.LQUA_CHARG,'Not Set') as dd_batchnumber,
			ifnull((SELECT wmsc.dim_wmstockcategoryid
					  FROM dim_wmstockcategory wmsc
					 WHERE wmsc.WMStockCategory = st.LQUA_BESTQ),
					  1) as dim_wmstockcategoryid,
			ifnull((SELECT ss.dim_specialstockid
					  FROM dim_specialstock ss
					 WHERE ss.specialstockindicator = st.LQUA_SOBKZ),
					  1) as dim_specialstockid,
			ifnull(st.LQUA_SONUM,'Not Set') as dd_specialstockno,
			ifnull((SELECT wmbr.dim_wmblockingreasonid
					  FROM dim_wmblockingreason wmbr
					 WHERE wmbr.WMBlockingReason = st.LQUA_SPGRU
			               AND wmbr.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')),
					  1) as dim_wmblockingreasonid,
			ifnull((SELECT lmd.dim_dateid
								  FROM dim_date lmd
								 WHERE lmd.DateValue = st.LQUA_BDATU
									   AND lmd.CompanyCode = pl.CompanyCode), 1) as dim_dateidlastmovement,
			ifnull(st.LQUA_BTANR,'Not Set') as dd_lastchangetodocno,
			ifnull(st.LQUA_BTAPS,0) as dd_lastchangetodocitemno,
			ifnull((SELECT lspd.dim_dateid
								  FROM dim_date lspd
								 WHERE lspd.DateValue = st.LQUA_EDATU
									   AND lspd.CompanyCode = pl.CompanyCode), 1) as dim_dateidlaststockplacement,
			ifnull((SELECT lsrd.dim_dateid
								  FROM dim_date lsrd
								 WHERE lsrd.DateValue = st.LQUA_ADATU
									   AND lsrd.CompanyCode = pl.CompanyCode), 1) as dim_dateidlaststockremoval,
			ifnull((SELECT lsad.dim_dateid
								  FROM dim_date lsad
								 WHERE lsad.DateValue = st.LQUA_ZDATU
									   AND lsad.CompanyCode = pl.CompanyCode), 1) as dim_dateidlaststockaddition,
			ifnull((SELECT grd.dim_dateid
								  FROM dim_date grd
								 WHERE grd.DateValue = st.LQUA_WDATU
									   AND grd.CompanyCode = pl.CompanyCode), 1) as dim_dateidgoodsreceipt,
			ifnull(st.LQUA_WENUM,'Not Set') as dd_goodsreceiptno,
			ifnull(st.LQUA_WEPOS,0) as dd_goodsreceiptitemno,
			ifnull((SELECT  wmsut.dim_wmstorageunittypeid
					  FROM dim_wmstorageunittype wmsut
					 WHERE  wmsut.StorageUnitType = st.LQUA_LETYP
			AND  wmsut.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')),
					  1) as dim_wmstorageunittypeid,
			ifnull(st.LQUA_GESME,0) as ct_totalqty,
			ifnull(st.LQUA_EINME,0) as ct_putawayqty,
			ifnull(st.LQUA_AUSME,0) as ct_removeqty,
			ifnull(st.LQUA_MGEWI,0) as ct_materialweight,
			ifnull((SELECT wum.dim_unitofmeasureid
					  FROM dim_unitofmeasure wum
					 WHERE wum.uom = st.LQUA_GEWEI),
					  1) as dim_weightunitid,
			ifnull(st.LQUA_TBNUM,'Not Set') as dd_transferreqno,
			ifnull(st.LQUA_IVPOS,0) as dd_inventorydocitemno,
			ifnull((SELECT wmrt.dim_wmrequirementtypeid
					  FROM dim_wmrequirementtype wmrt
					 WHERE wmrt.RequirementType = st.LQUA_BETYP
			AND wmrt.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')),
					  1) as dim_wmrequirementtypeid,
			ifnull(st.LQUA_BENUM,'Not Set') as dd_requirementno,
			ifnull(st.LQUA_LENUM,'Not Set') as dd_storageunitno,
			ifnull(st.LQUA_QPLOS,'Not Set') as dd_inspectionlotno,
			ifnull((SELECT bbd.dim_dateid
								  FROM dim_date bbd
								 WHERE bbd.DateValue = st.LQUA_VFDAT
									   AND bbd.CompanyCode = pl.CompanyCode), 1) as dim_dateidbestbefore,
			ifnull(st.LQUA_QKAPV,0) as ct_capacityusage,
			ifnull((SELECT wmpa.dim_wmpickingareaid
					  FROM dim_wmpickingarea wmpa
					  WHERE wmpa.StorageType = st.LQUA_LGTYP
			               AND wmpa.PickingArea = st.LQUA_KOBER
			               AND wmpa.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')),
					  1) as dim_wmpickingareaid,
			ifnull(st.LQUA_TRAME, 0) as ct_opentransferqty,
			ifnull(st.LQUA_VBELN,'Not Set') as dd_deliveryno,
			ifnull(st.LQUA_POSNR,0) as dd_deliveryitemno,
			ifnull((SELECT lid.dim_dateid
								  FROM dim_date lid
								 WHERE lid.DateValue = st.LQUA_IDATU
									   AND lid.CompanyCode = pl.CompanyCode), 1) as dim_dateidlastinventory,
			ifnull((select wmqmisc.dim_wmquantmiscellaneousid 
					    from dim_wmquantmiscellaneous wmqmisc
					    where     wmqmisc.userputawayblocked  = ifnull(st.LQUA_SKZUE,'Not Set')
							  AND wmqmisc.userremovalblocked  = ifnull(st.LQUA_SKZUA,'Not Set')
							  AND wmqmisc.systcurrentplacementblocked = ifnull(st.LQUA_SKZSE,'Not Set')
							  AND wmqmisc.systremovalblocked  = ifnull(st.LQUA_SKZSA,'Not Set')
							  AND wmqmisc.systInventoryblocked  = ifnull(st.LQUA_SKZSI,'Not Set')
							  AND wmqmisc.nogrdata = ifnull(st.LQUA_VIRGO,'Not Set')
							  AND wmqmisc.OnHandlingUnit = ifnull(st.LQUA_KZHUQ,'Not Set')),1) as dim_wmquantmiscellaneousid				   
     FROM LQUA st,
          dim_plant pl,
		  dim_warehousenumber wn
    WHERE st.LQUA_WERKS = pl.PlantCode
		 AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')			
          AND NOT EXISTS
                (SELECT 1
                   FROM fact_wmquant fq
                  WHERE     wn.dim_warehousenumberid = fq.dim_warehousenumberid
                        AND  st.LQUA_LQNUM = fq.dd_quantno)	;
						
						
UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
       dim_part dp,
	   dim_warehousenumber wn
SET fwmq.dim_partid = ifnull(dp.dim_partid ,1),
    dw_update_date = current_timestamp
where dp.PartNumber = st.LQUA_MATNR 
AND dp.plant = st.LQUA_WERKS  
AND  st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND  st.LQUA_LQNUM = fwmq.dd_quantno
AND  fwmq.dim_partid <> ifnull(dp.dim_partid ,1);


UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
       dim_part dp,
	   dim_warehousenumber wn
SET fwmq.dim_plantid = ifnull(pl.Dim_Plantid ,1),
    dw_update_date = current_timestamp
where  st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND  st.LQUA_LQNUM = fwmq.dd_quantno
AND  fwmq.dim_plantid <> ifnull(pl.Dim_Plantid ,1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn, 
	   dim_wmstoragetype sty
SET fwmq.dim_storagetypeid = ifnull(sty.dim_wmstoragetypeid,1),
    dw_update_date = current_timestamp
where  sty.storagetype = st.LQUA_LGTYP
AND sty.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_storagetypeid <> ifnull(sty.dim_wmstoragetypeid,1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn, 
	   dim_storagebin sb
SET fwmq.dim_storagebinid = ifnull(sb.dim_storagebinid,1),
    dw_update_date = current_timestamp
where  sb.storagebin = st.LQUA_LGPLA
AND sb.storagetype = st.LQUA_LGTYP
AND sb.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_storagebinid <> ifnull(sb.dim_storagebinid,1);


UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn, 
	   dim_storagelocation sl
SET fwmq.dim_storagelocationid = ifnull(sl.dim_storagelocationid,1),
    dw_update_date = current_timestamp
where  sl.plant = st.LQUA_WERKS
AND sl.locationcode = st.LQUA_LGORT
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_storagelocationid <> ifnull(sl.dim_storagelocationid,1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn, 
	  dim_unitofmeasure um
SET fwmq.dim_unitofmeasureid = ifnull( um.dim_unitofmeasureid,1),
    dw_update_date = current_timestamp
where  um.uom = st.LQUA_MEINS
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_unitofmeasureid <> ifnull( um.dim_unitofmeasureid,1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.ct_availablestock = ifnull(st.LQUA_VERME, 0),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND  fwmq.ct_availablestock <> ifnull(st.LQUA_VERME, 0);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_batchnumber  = ifnull(st.LQUA_CHARG,'Not Set'),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_batchnumber  <> ifnull(st.LQUA_CHARG,'Not Set');


UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmstockcategory wmsc
SET fwmq.dim_wmstockcategoryid  = ifnull(wmsc.dim_wmstockcategoryid,1),
    dw_update_date = current_timestamp
where wmsc.WMStockCategory = st.LQUA_BESTQ
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmstockcategoryid  <> ifnull(wmsc.dim_wmstockcategoryid,1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_specialstock ss
SET fwmq.dim_specialstockid  = ifnull(ss.dim_specialstockid,1),
    dw_update_date = current_timestamp
where ss.specialstockindicator = st.LQUA_SOBKZ
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_specialstockid  <> ifnull(ss.dim_specialstockid,1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_specialstockno  = ifnull(st.LQUA_SONUM,'Not Set'),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_specialstockno  <>  ifnull(st.LQUA_SONUM,'Not Set');

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmblockingreason wmbr
SET fwmq.dim_wmblockingreasonid  = ifnull(wmbr.dim_wmblockingreasonid,1),
    dw_update_date = current_timestamp
where wmbr.WMBlockingReason = st.LQUA_SPGRU
AND wmbr.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmblockingreasonid  <> ifnull(wmbr.dim_wmblockingreasonid,1);		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lmd
SET fwmq.dim_dateidlastmovement = ifnull(lmd.dim_dateid, 1),
    dw_update_date = current_timestamp
where lmd.DateValue = st.LQUA_BDATU
AND lmd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlastmovement <> ifnull(lmd.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lmd
SET fwmq.dd_lastchangetodocno  = ifnull(st.LQUA_BTANR,'Not Set'),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_lastchangetodocno <> ifnull(st.LQUA_BTANR,'Not Set');	


UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_lastchangetodocitemno  = ifnull(st.LQUA_BTAPS,0),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_lastchangetodocitemno  <> ifnull(st.LQUA_BTAPS,0);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lspd
SET fwmq.dim_dateidlaststockplacement = ifnull( lspd.dim_dateid, 1),
    dw_update_date = current_timestamp
where lspd.DateValue = st.LQUA_EDATU
AND lspd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlaststockplacement <> ifnull( lspd.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lsrd
SET fwmq.dim_dateidlaststockremoval = ifnull( lsrd.dim_dateid, 1),
   dw_update_date = current_timestamp
where lsrd.DateValue = st.LQUA_ADATU
AND lsrd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlaststockremoval <> ifnull( lsrd.dim_dateid, 1);	

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lsad
SET fwmq.dim_dateidlaststockaddition = ifnull( lsad.dim_dateid, 1),
    dw_update_date = current_timestamp
where lsad.DateValue = st.LQUA_ZDATU
AND lsad.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlaststockaddition <> ifnull( lsad.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date grd
SET fwmq.dim_dateidgoodsreceipt = ifnull( grd.dim_dateid, 1),
    dw_update_date = current_timestamp
where grd.DateValue = st.LQUA_WDATU
AND grd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidgoodsreceipt <> ifnull( grd.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
FROM   LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date grd
SET fwmq.dim_dateidgoodsreceipt = ifnull( grd.dim_dateid, 1),
    dw_update_date = current_timestamp
where grd.DateValue = st.LQUA_WDATU
AND grd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidgoodsreceipt <> ifnull( grd.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date grd
SET fwmq.dd_goodsreceiptno  = ifnull(st.LQUA_WENUM,'Not Set'),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_goodsreceiptno <> ifnull(st.LQUA_WENUM,'Not Set');		


UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date grd
SET fwmq.dd_goodsreceiptitemno  = ifnull(st.LQUA_WEPOS,0),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_goodsreceiptitemno  <> ifnull(st.LQUA_WEPOS,0);		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmstorageunittype wmsut
SET fwmq.dim_wmstorageunittypeid  =  ifnull(wmsut.dim_wmstorageunittypeid,1),
    dw_update_date = current_timestamp
where wmsut.StorageUnitType = st.LQUA_LETYP
AND wmsut.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmstorageunittypeid <> ifnull(wmsut.dim_wmstorageunittypeid,1);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.ct_totalqty  = ifnull(st.LQUA_GESME,0),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_totalqty <> ifnull(st.LQUA_GESME,0);	

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.ct_putawayqty  = ifnull(st.LQUA_EINME,0), 
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_putawayqty <> ifnull(st.LQUA_EINME,0);	

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.ct_removeqty  = ifnull(st.LQUA_AUSME,0),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_removeqty <> ifnull(st.LQUA_AUSME,0);	

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.ct_materialweight  = ifnull( st.LQUA_MGEWI,0),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_materialweight <> ifnull( st.LQUA_MGEWI,0);

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_unitofmeasure wum
SET fwmq.dim_weightunitid  = ifnull(wum.dim_unitofmeasureid,1),
    dw_update_date = current_timestamp
where wum.uom = st.LQUA_GEWEI
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_weightunitid  <> ifnull(wum.dim_unitofmeasureid,1);

	
UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_transferreqno  = ifnull(st.LQUA_TBNUM,'Not Set'),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_transferreqno <> ifnull(st.LQUA_TBNUM,'Not Set');	

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_inventorydocitemno  = ifnull(st.LQUA_IVPOS,0),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_inventorydocitemno <> ifnull(st.LQUA_IVPOS,0);	

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmrequirementtype wmrt
SET fwmq.dim_wmrequirementtypeid  =  ifnull(wmrt.dim_wmrequirementtypeid,1),
    dw_update_date = current_timestamp
where wmrt.RequirementType = st.LQUA_BETYP
AND wmrt.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmrequirementtypeid <> ifnull(wmrt.dim_wmrequirementtypeid,1);		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_requirementno  = ifnull(st.LQUA_BENUM,'Not Set'),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_requirementno <> ifnull(st.LQUA_BENUM,'Not Set');		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_storageunitno  = ifnull(st.LQUA_LENUM,'Not Set'), 
    dw_update_date = current_timestamp 
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_storageunitno <> ifnull(st.LQUA_LENUM,'Not Set');	


UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_inspectionlotno  = ifnull(st.LQUA_QPLOS,'Not Set'),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_inspectionlotno <> ifnull(st.LQUA_QPLOS,'Not Set');		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date bbd
SET fwmq.dim_dateidbestbefore = ifnull( bbd.dim_dateid,1),
    dw_update_date = current_timestamp
where bbd.DateValue = st.LQUA_VFDAT
AND bbd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidbestbefore <> ifnull( bbd.dim_dateid,1);		

		
UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.ct_capacityusage  = ifnull(st.LQUA_QKAPV,0),
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_capacityusage <> ifnull(st.LQUA_QKAPV,0);		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmpickingarea wmpa
SET fwmq.dim_wmpickingareaid  = ifnull(wmpa.dim_wmpickingareaid,1),
    dw_update_date = current_timestamp
where wmpa.StorageType = st.LQUA_LGTYP
AND wmpa.PickingArea = st.LQUA_KOBER
AND wmpa.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set') 
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmpickingareaid <> ifnull(wmpa.dim_wmpickingareaid,1);		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.ct_opentransferqty  = ifnull(st.LQUA_TRAME, 0) ,
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_opentransferqty  <> ifnull(st.LQUA_TRAME, 0) ;	

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_deliveryno  = ifnull(st.LQUA_VBELN,'Not Set'), 
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_deliveryno <> ifnull(st.LQUA_VBELN,'Not Set');	

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
SET fwmq.dd_deliveryitemno  = ifnull(st.LQUA_POSNR,0), 
    dw_update_date = current_timestamp
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_deliveryitemno  <> ifnull(st.LQUA_POSNR,0);		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lid
SET fwmq.dim_dateidlastinventory = ifnull( lid.dim_dateid, 1),
    dw_update_date = current_timestamp
where lid.DateValue = st.LQUA_IDATU
AND lid.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlastinventory <> ifnull( lid.dim_dateid, 1);		

UPDATE fact_wmquant_tmp fwmq
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmquantmiscellaneous wmqmisc
SET fwmq.dim_wmquantmiscellaneousid = ifnull ( wmqmisc.dim_wmquantmiscellaneousid,1),
    dw_update_date = current_timestamp  
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND wmqmisc.userputawayblocked  = ifnull(st.LQUA_SKZUE,'Not Set')
AND wmqmisc.userremovalblocked  = ifnull(st.LQUA_SKZUA,'Not Set')
AND wmqmisc.systcurrentplacementblocked = ifnull(st.LQUA_SKZSE,'Not Set')
AND wmqmisc.systremovalblocked  = ifnull(st.LQUA_SKZSA,'Not Set')
AND wmqmisc.systInventoryblocked  = ifnull(st.LQUA_SKZSI,'Not Set')
AND wmqmisc.OnHandlingUnit = ifnull(st.LQUA_KZHUQ,'Not Set')
AND fwmq.dim_wmquantmiscellaneousid <> ifnull ( wmqmisc.dim_wmquantmiscellaneousid,1);

			
call vectorwise(combine 'fact_wmquant_tmp');
call vectorwise(combine 'fact_wmquant');
call vectorwise(combine 'fact_wmquant + fact_wmquant_tmp');

drop table if exists fact_wmtransferorder_tmp;