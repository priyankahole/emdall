

drop table if exists fact_salesorderinvoices_tmp;

create table fact_salesorderinvoices_tmp as
select row_number() over () fact_salesorderinvoicesid,
	   ar.dim_companyid,
	   ar.dim_customerid,
	   ar.dd_CreditRep,
	   ar.amt_InLocalCurrency,
	   ar.dd_CreditLimit,
	   ar.Dim_DocumentTypeId,
	   ar.dd_AccountingDocNo,
	   ar.Dim_DateIdCreated,
	   so.dd_SalesDocNo,
	   so.Dim_SalesDocumentTypeId,
	   so.Dim_DateIdSOCreated,
	   so.Dim_PaymentTermsId,
	   so.dd_salesitemno,
	   so.dd_scheduleno,
	   so.dd_customerpono,
	   so.Dim_SalesOrgId,
	   so.Dim_distributionchannelid,
	   ar.Dim_RiskClassId,
	   so.Dim_CustomerPaymentTermsId,	   
	   so.amt_ScheduleTotal,
	   so.amt_Tax,
	   so.Dim_DateidNextDate,
	   so.amt_AfsNetValue,
	   so.Dim_CurrencyId,
	   ar.amt_OriginalAmtDisputed,
	   ar.amt_DisputedAmt,
	   ar.amt_DisputeAmtPaid,
	   ar.amt_DisputeAmtCredited,
	   ar.amt_DisputeAmtCleared,
	   ar.amt_DisputeAmtWrittenOff,
	   ar.dd_DisputeCaseId,
	   ar.Dim_ChartOfAccountsId,
	   ar.Dim_DateIdPosting,
	   ar.dd_FiscalYear,
	   ar.Dim_PostingKeyId,
	   ar.dd_ARAge,
	   so.amt_UnitPrice,
	   so.amt_afsnetprice,
	   ar.Dim_NetDueDateId,
	   ar.Dim_DateIdClearing,
	   so.Dim_AfsSeasonId,
	   ar.dd_CreditMgr,
		so.amt_ExchangeRate,
		so.amt_ExchangeRate_gbl,
		so.dim_Currencyid_TRA,
		so.dim_Currencyid_GBL,
		current_timestamp	as dw_insert_date /* Fields added to fix integration error 13Jan2015 */
,current_timestamp	as dw_update_date
,1 as dim_projectsourceid
from  fact_accountsreceivable ar inner join fact_billing b on ar.dd_accountingdocno = b.dd_billing_no
 inner join fact_salesorder so on b.dd_salesdocno = so.dd_salesdocno and b.dd_salesitemno = so.dd_salesitemno and b.dd_AfsScheduleNo = so.dd_scheduleNo;



CALL vectorwise(combine 'fact_salesorderinvoices - fact_salesorderinvoices');

insert into fact_salesorderinvoices
select * from  fact_salesorderinvoices_tmp;

CALL vectorwise(combine 'fact_salesorderinvoices');
