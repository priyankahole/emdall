UPDATE    dim_schedulelinecategory slc
       FROM
          TVEPT t
   SET slc.Description = TVEPT_VTEXT
 WHERE slc.RowIsCurrent = 1
 AND t.TVEPT_SPRAS = 'E' AND t.TVEPT_ETTYP = slc.ScheduleLineCategory
;
