INSERT INTO dim_salesorderitemcategory(Dim_SalesOrderItemCategoryid,
							SalesOrderItemCategory,
							Description,
							ScheduleLinesAllowed,
                             RowStartDate,
                             RowEndDate,
                             RowIsCurrent,
                             RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          'Not Set',		  
          current_timestamp,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorderitemcategory
               WHERE dim_salesorderitemcategoryid = 1);

UPDATE    Dim_SalesOrderItemCategory ic
       FROM
          TVAPT t
   SET ic.Description = TVAPT_VTEXT, ic.ScheduleLinesAllowed = ifnull(TVAP_ETERL,'Not Set'),
			ic.dw_update_date = current_timestamp
	WHERE t.TVAPT_SPRAS = 'E' AND t.TVAPT_PSTYV = ic.SalesOrderItemCategory  ;

delete from number_fountain m where m.table_name = 'dim_salesorderitemcategory';

insert into number_fountain
select 	'dim_salesorderitemcategory',
	ifnull(max(d.Dim_SalesOrderItemCategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemcategory d
where d.Dim_SalesOrderItemCategoryid <> 1; 
	
INSERT INTO dim_salesorderitemcategory(
	Dim_SalesOrderItemCategoryid,
	SalesOrderItemCategory,
	Description,
        ScheduleLinesAllowed,
 	RowStartDate,
	RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemcategory') 
          + row_number() over(),
			  ifnull(TVAPT_PSTYV, 'Not Set'),
            TVAPT_VTEXT,
	    ifnull(TVAP_ETERL,'Not Set'),
            current_timestamp,
            1
       FROM TVAPT t
      WHERE t.TVAPT_SPRAS = 'E'
	   AND NOT EXISTS
                  (SELECT 1
                     FROM dim_salesorderitemcategory ic
                    WHERE TVAPT_PSTYV = ic.SalesOrderItemCategory)
   ORDER BY 2  OFFSET 0;