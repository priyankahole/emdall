UPDATE    dim_distributionchannel dc
       FROM
          TVTWT t
   SET dc.DistributionChannelName = t.TVTWT_VTEXT
 WHERE dc.RowIsCurrent = 1
 AND t.TVTWT_VTWEG = dc.DistributionChannelCode;
