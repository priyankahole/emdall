UPDATE dim_MRPController FROM T024D
   SET Description = ifnull(T024D_DSNAM, 'Not Set'),
       TelephoneNo = ifnull(T024D_DSTEL,'Not Set')
 WHERE     MRPController = T024D_DISPO
       AND Plant = T024D_WERKS
       AND RowIsCurrent = 1
;