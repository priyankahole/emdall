UPDATE dim_riskclass
   FROM ukm_risk_cl0t t
   SET RiskClassText = ifnull(t.RISK_CLASS_TXT, 'Not Set')
 WHERE RiskClass = t.RISK_CLASS AND RowIsCurrent = 1
;
