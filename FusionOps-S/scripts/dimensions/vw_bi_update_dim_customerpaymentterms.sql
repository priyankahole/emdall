UPDATE    dim_customerpaymentterms cpt
       FROM
          TVZBT t      
   SET cpt.PaymentTermName = ifnull(TVZBT_VTEXT, 'Not Set')
	WHERE t.TVZBT_ZTERM = cpt.PaymentTermCode   ;
