UPDATE    dim_reasoncode drc
       FROM
          ReasonCode rc
   SET drc.ReasonCode = ifnull(rc.Reason_mnemonic,'Not Set'),
       drc.Description = ifnull(rc.Description,'Not Set'),
       drc.CodeType = ifnull(rc.Code_type,'Not Set')
	WHERE drc.ReasonCodeId = rc.ID
	  AND drc.RowIsCurrent = 1
;
