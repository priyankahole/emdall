
INSERT INTO dim_classoftradedescription(dim_classoftradedescriptionid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_classoftradedescription
               WHERE dim_classoftradedescriptionid = 1);
			   
UPDATE 	dim_classoftradedescription d
from 	IRM_TGMLGRP1T s
SET 	d.description = s.IRM_TGMLGRP1T_DESCR,
		d.dw_update_date = current_timestamp
WHERE	d.membergroup1 = s.IRM_TGMLGRP1T_MBGRP1
		AND d.RowIsCurrent = 1;
		
delete from number_fountain m where m.table_name = 'dim_classoftradedescription';

insert into number_fountain
select 	'dim_classoftradedescription',
	ifnull(max(d.dim_classoftradedescriptionid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_classoftradedescription d
where d.dim_classoftradedescriptionid <> 1;
			   
INSERT INTO dim_classoftradedescription(dim_classoftradedescriptionid,
								  description,
                                  membergroup1,
                                  RowStartDate,
                                  RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_classoftradedescription') 
          + row_number() over(),
		  IRM_TGMLGRP1T_DESCR,
		  IRM_TGMLGRP1T_MBGRP1,
          current_timestamp,
          1
     FROM IRM_TGMLGRP1T
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM dim_classoftradedescription
                   WHERE     description = IRM_TGMLGRP1T_DESCR
                         AND membergroup1 = IRM_TGMLGRP1T_MBGRP1
			 AND RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_classoftradedescription';
			 