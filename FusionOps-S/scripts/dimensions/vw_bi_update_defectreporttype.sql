
UPDATE    dim_defectreporttype drt
       FROM
          TQ86T t
   SET drt.DefectReportTypeName = t.TQ86T_KURZTEXT
 WHERE drt.RowIsCurrent = 1
 AND t.TQ86T_FEART = drt.DefectReportTypeCode;
