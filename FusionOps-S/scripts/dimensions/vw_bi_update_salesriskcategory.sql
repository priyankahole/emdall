UPDATE    dim_salesriskcategory src
       FROM
          T691T t
   SET src.SalesRiskCategoryDescription = ifnull(t.T691T_RTEXT, 'Not Set')
 WHERE src.RowIsCurrent = 1
 AND src.SalesRiskCategory = t.T691T_CTLPC
 AND src.CreditControlArea = t.T691T_KKBER
;
