UPDATE    dim_inspectionsamplestatus s
       FROM
          DD07T t
   SET s.description = t.DD07T_DDTEXT
 WHERE s.RowIsCurrent = 1
       AND    t.DD07T_DOMNAME = 'LVS_STIKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND s.statuscode = t.DD07T_DOMVALUE;
