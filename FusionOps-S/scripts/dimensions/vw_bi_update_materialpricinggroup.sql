UPDATE dim_MaterialPricingGroup a FROM T178T 
   SET a.MaterialPricingGroupName = ifnull(T178T_VTEXT, 'Not Set')
 WHERE a.MaterialPricingGroupCode = T178T_KONDM AND RowIsCurrent = 1;
