
UPDATE    dim_customergroup cg
       FROM
          T151T t
   SET cg.Description = ifnull(T151T_KTEXT, 'Not Set')
 WHERE cg.RowIsCurrent = 1
      AND t.T151T_KDGRP = cg.CustomerGroup AND t.T151T_SPRAS = 'E';
