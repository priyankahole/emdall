set session authorization ariat;


INSERT INTO dim_afsseason(Dim_AfsSeasonId,SeasonIndicator,Collection,Theme,Material,Size,RowStartDate
,RowEndDate,RowIsCurrent,RowChangeReason)
SELECT 1,'Not Set','Not Set','Not Set','Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_afsseason where Dim_AfsSeasonId = 1);

/*
UPDATE	dim_afsseason ase
FROM 	J_3ASEANT t
SET 	ase.Description = ifnull(t.J_3ASEANT_TEXT, 'Not Set'),
		ase.dw_update_date = current_timestamp
WHERE 	ase.RowIsCurrent = 1
		AND ase.SeasonIndicator = t.J_3ASEANT_J_3ASEAN
		AND ase.Collection = t.J_3ASEANT_AFS_COLLECTION
		AND ase.Theme = t.J_3ASEANT_AFS_THEME

call vectorwise(combine 'dim_afsseason')
*/		

 
delete from number_fountain m where m.table_name = 'dim_afsseason';

insert into number_fountain
select 	'dim_afsseason',
	ifnull(max(d.Dim_AfsSeasonId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_afsseason d
where d.Dim_AfsSeasonId <> 1; 
 
INSERT INTO dim_afsseason(Dim_AfsSeasonId,
			  SeasonIndicator,
			  Collection,
			  Theme,
			  Material,
			  Size,
              RowStartDate,
              RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_afsseason') 
          + row_number() over(),
			ifnull(J_3AMSEA_J_3ASEAN,'Not Set'),
	        ifnull(J_3AMSEA_AFSCOLLECTION,'Not Set'),
	        ifnull(J_3AMSEA_AFSTHEME,'Not Set'),
			ifnull(J_3AMSEA_MATNR,'Not Set'),
			ifnull(J_3AMSEA_J_3ASIZE,'Not Set'),
            current_timestamp,
            1
       FROM J_3AMSEA
      WHERE J_3AMSEA_J_3ASEAN IS NOT NULL
	    AND NOT EXISTS
                  (SELECT 1
                     FROM dim_afsseason
                    WHERE SeasonIndicator = ifnull(J_3AMSEA_J_3ASEAN,'Not Set')
                     AND Collection = ifnull(J_3AMSEA_AFSCOLLECTION,'Not Set')
                     AND Theme = ifnull(J_3AMSEA_AFSTHEME,'Not Set')
					 AND Material = ifnull(J_3AMSEA_MATNR,'Not Set')
					 );
					 
call vectorwise(combine 'dim_afsseason');					 
delete from number_fountain m where m.table_name = 'dim_afsseason';	
