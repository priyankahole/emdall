UPDATE dim_MaterialPriceGroup1 a FROM TVM1T
   SET a.Description = ifnull(TVM1T_BEZEI, 'Not Set')
 WHERE a.MaterialPriceGroup1 = TVM1T_MVGR1 AND RowIsCurrent = 1;
