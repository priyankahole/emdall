UPDATE    dim_MRPProcedure mrpp
       FROM
          DD07T t
   SET mrpp.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE mrpp.RowIsCurrent = 1
       AND t.DD07T_DOMNAME = 'DISVF'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND mrpp.MRPProcedure = t.DD07T_DOMVALUE 
;