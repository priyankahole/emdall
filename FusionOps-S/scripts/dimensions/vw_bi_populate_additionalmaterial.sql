INSERT INTO Dim_AdditionalMaterial
	(Dim_AdditionalMaterialId,
	ConditionRecordNo,
	Material,
	SequenceNo,
	RequirementCategory,
	ASize,
	RowStartDate,
	RowEndDate,
	RowIsCurrent,
	RowChangeReason )
SELECT 1,'Not Set','Not Set','Not Set','Not Set','Not Set',CURRENT_TIMESTAMP,NULL,1,NULL
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM Dim_AdditionalMaterial WHERE Dim_AdditionalMaterialId = 1);


delete from number_fountain m where m.table_name = 'Dim_AdditionalMaterial';

insert into number_fountain
select 	'Dim_AdditionalMaterial',
	ifnull(max(d.Dim_AdditionalMaterialid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_AdditionalMaterial d
where d.Dim_AdditionalMaterialid <> 1;


INSERT INTO Dim_AdditionalMaterial
	(	Dim_AdditionalMaterialId,
		ConditionRecordNo,
		Material,
		SequenceNo,
                RequirementCategory,
		ASize,
                RowStartDate,
                RowIsCurrent)
   SELECT 
   	  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_AdditionalMaterial')
          + row_number() over() as Dim_AdditionalMaterialId,
	  IFNULL(KONDDP_KNUMH, 'Not Set'),
          IFNULL(KONDDP_SMATN, 'Not Set'),
          IFNULL(KONDDP_KPOSN, 'Not Set'),
          IFNULL(KONDDP_J_4KRCAT, 'Not Set'),
	  IFNULL(KONDDP_J_3asize, 'Not Set'),
          current_timestamp,
          1
     FROM  KONDDP
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM Dim_AdditionalMaterial
                   WHERE     ConditionRecordNo = IFNULL(KONDDP_KNUMH, 'Not Set')
                         AND Material = IFNULL(KONDDP_SMATN, 'Not Set')
                         AND RequirementCategory = IFNULL(KONDDP_J_4KRCAT, 'Not Set')
						 AND Asize = IFNULL(KONDDP_J_3asize, 'Not Set'));
						 

UPDATE NUMBER_FOUNTAIN 
SET max_id = ifnull((select 	ifnull(max(d.Dim_AdditionalMaterialid), 
				ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
		     from Dim_AdditionalMaterial d
		     where d.Dim_AdditionalMaterialid <> 1), 1)
WHERE table_name = 'Dim_AdditionalMaterial';




INSERT INTO Dim_AdditionalMaterial
		(	Dim_AdditionalMaterialId,
			ConditionRecordNo,
			Material,
                        SequenceNo,
                        RequirementCategory,
			ASize,
                        RowStartDate,
                        RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_AdditionalMaterial')
          + row_number() over() as Dim_AdditionalMaterialId,
	  IFNULL(KONDD_KNUMH, 'Not Set'),
          IFNULL(KONDD_SMATN, 'Not Set'),
          'Not Set',
          IFNULL(KONDD_J_4KRCAT, 'Not Set'),
	  IFNULL(KONDD_J_3asize, 'Not Set'),
          current_timestamp,
          1
     FROM  KONDD
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM Dim_AdditionalMaterial
                   WHERE     ConditionRecordNo = IFNULL(KONDD_KNUMH, 'Not Set')
                         AND Material = IFNULL(KONDD_SMATN, 'Not Set')
                         AND RequirementCategory = IFNULL(KONDD_J_4KRCAT, 'Not Set')
						 AND Asize = IFNULL(KONDD_J_3asize, 'Not Set'));
