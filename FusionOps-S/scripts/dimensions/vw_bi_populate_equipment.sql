/* ##################################################################################################################*/
/*	Script         : vw_bi_populate_equipment																		*/
/*     Created By     : Amar															*/
/*     Created On     : 12 March 2015													*/
/*     Change History																	*/
/*     Date            By        Version           Desc									*/
/*	 3/12/2015		 Amar	   1				 Multi-Statement changed to Script		*/
/*#################################################################################################################### */


INSERT INTO dim_equipment(Dim_equipmentid,
				Equipment,
			       TechnicalDescription,
                               Language,
			       EquipmentDescription,
                               RowStartDate,
                               RowIsCurrent)
SELECT 			1,
				'Not Set',
				'Not Set',
				'EN',
				'Not Set',
				current_timestamp,
				1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_equipment WHERE dim_equipmentId = 1);

							   
DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_equipment';

INSERT INTO NUMBER_FOUNTAIN
select 	'dim_equipment',
		ifnull(max(d.dim_equipmentId), 
		ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_equipment d
where d.dim_equipmentId <> 1;

INSERT INTO dim_equipment(Dim_equipmentid,
				Equipment,
			       TechnicalDescription,
                               Language,
			       EquipmentDescription,
                               RowStartDate,
                               RowIsCurrent)
   SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_equipment') 
          + row_number() over() ,
	    EQKT_EQUNR,
	    ifnull(EQKT_EQKTX, 'Not Set'),
            EQKT_SPRAS,
	    ifnull(EQKT_EQKTU, 'Not Set'),
            current_timestamp,
            1			 
       FROM EQKT
      WHERE EQKT_SPRAS = 'E'
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_equipment
                    WHERE equipment = EQKT_EQUNR AND language = EQKT_SPRAS);
					
					

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_equipment';
