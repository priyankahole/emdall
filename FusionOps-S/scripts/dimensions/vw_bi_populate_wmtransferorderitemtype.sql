/* 	Server: QA
	Process Name: WM Transfer Order Item Type Transfer 
	Interface No: 2
*/
INSERT INTO dim_wmtransferorderitemtype
(dim_wmtransferorderitemtypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmtransferorderitemtype where dim_wmtransferorderitemtypeid = 1);

UPDATE    dim_wmtransferorderitemtype wmtoit
       FROM
          DD07T dt
	SET wmtoit.Description = ifnull(dt.DD07T_DDTEXT, 'Not Set'),
			wmtoit.dw_update_date = current_timestamp
 WHERE wmtoit.WMTransferOrderItemType = dt.DD07T_DOMVALUE AND wmtoit.RowIsCurrent = 1
 AND dt.DD07T_DOMNAME = 'LTAP_POSTY' AND dt.DD07T_DOMNAME IS NOT NULL;
 
delete from number_fountain m where m.table_name = 'dim_wmtransferorderitemtype';

insert into number_fountain				   
select 	'dim_wmtransferorderitemtype',
	ifnull(max(d.dim_wmtransferorderitemtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmtransferorderitemtype d
where d.dim_wmtransferorderitemtypeid <> 1;

INSERT INTO dim_wmtransferorderitemtype(dim_wmtransferorderitemtypeid,
								Description,
                               WMTransferOrderItemType,
                               RowStartDate,
                               RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmtransferorderitemtype') 
          + row_number() over() ,
			ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'LTAP_POSTY' 
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_wmtransferorderitemtype
                    WHERE WMTransferOrderItemType = DD07T_DOMVALUE AND DD07T_DOMNAME = 'LTAP_POSTY')
   ORDER BY 2 OFFSET 0;
