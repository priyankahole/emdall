UPDATE    dim_objectcategory oc
       FROM
          TJ03T t
   SET oc.ObjectCategoryName = t.TJ03T_TXT
 WHERE oc.RowIsCurrent = 1
      AND oc.ObjectCategoryCode = t.TJ03T_OBTYP
;