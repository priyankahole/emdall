UPDATE    dim_chartofaccounts coa
       FROM
          SKAT s      
   SET coa.ShortText = s.SKAT_TXT20, coa.Description = s.SKAT_TXT50
 WHERE coa.RowIsCurrent = 1
 AND  coa.CharOfAccounts = s.SKAT_KTOPL
          AND coa.GLAccountNumber = s.SKAT_SAKNR
          AND s.SKAT_SPRAS = 'E'
;