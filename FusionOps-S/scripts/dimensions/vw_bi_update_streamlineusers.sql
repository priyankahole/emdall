UPDATE dim_streamlineusers su FROM users u
   SET su.LoginId = u.Loginid,
       UserName = ifnull(User_name,'Not Set'),
       UserRole = User_role,
       BuyerFlag = Buyer_flag,
       PlannerFlag = Planner_Flag,
       AdminFlag = Admin_flag,
       su.Email = u.Email,
       VendorId = ifnull(Vendor_Id,'Not Set')
 WHERE u.Id = UserId AND su.RowIsCurrent = 1
;