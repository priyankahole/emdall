UPDATE    dim_conditionprocedure cp
       FROM
          T683U t
   SET cp.ProcedureDescription = t.T683U_VTEXT,
       cp.ProcedureApplication = t.T683U_KAPPL,
       cp.UsageOfConditionTable = t.T683U_KVEWE
 WHERE cp.RowIsCurrent = 1
 AND cp.ProcedureCode = t.T683U_KALSM
 ;