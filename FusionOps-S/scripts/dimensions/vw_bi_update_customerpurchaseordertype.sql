
UPDATE dim_customerpurchaseordertype cpo FROM T176T t
   SET Description = ifnull(t.T176T_VTEXT, 'Not Set')
 WHERE cpo.CustomerPOType = t.T176T_BSARK AND cpo.RowIsCurrent = 1;
