INSERT INTO dim_customergroup1
(dim_customergroup1id
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_customergroup1 where dim_customergroup1id = 1);

UPDATE    dim_customergroup1 cg1
       FROM
          TVV1T t
   SET cg1.Description = ifnull(TVV1T_BEZEI, 'Not Set'),
			cg1.dw_update_date = current_timestamp
   WHERE t.TVV1T_KVGR1 = cg1.CustomerGroup AND t.TVV1T_SPRAS = 'E';

delete from number_fountain m where m.table_name = 'dim_customergroup1';

insert into number_fountain
select 	'dim_customergroup1',
	ifnull(max(d.dim_customergroup1id), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customergroup1 d
where d.dim_customergroup1id <> 1;

INSERT
  INTO dim_customergroup1(Dim_CustomerGroup1id,
                                                LanguageKey,
                          CustomerGroup,
                          Description)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customergroup1')
          + row_number() over(),
                        TVV1T_SPRAS,
        TVV1T_KVGR1,
        ifnull(TVV1T_BEZEI,'Not Set')
FROM TVV1T
WHERE not exists (select 1 from dim_customergroup1 a where a.CustomerGroup = TVV1T_KVGR1)
        and TVV1T_SPRAS = 'E';

delete from number_fountain m where m.table_name = 'dim_customergroup1';		