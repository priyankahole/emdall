UPDATE dim_mrpexception mrpx FROM T458B t
   SET mrpx.Description = ifnull(T458B_AUSLT, 'Not Set'),
	mrpx.exceptionnumber = ifnull((select a.T458A_AUSKT from T458A a where a.T458A_AUSSL = t.T458B_AUSSL),'Not Set')
 WHERE mrpx.RowIsCurrent = 1
 AND mrpx.exceptionkey = t.T458B_AUSSL
;