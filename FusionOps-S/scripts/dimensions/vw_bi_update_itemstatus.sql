
UPDATE    dim_itemstatus s
       FROM
          DD07T t
   SET s.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE s.RowIsCurrent = 1
       AND     t.DD07T_DOMNAME = 'EPSTATU'
          AND DD07T_DOMVALUE IS NOT NULL
          AND s.status = t.DD07T_DOMVALUE;
