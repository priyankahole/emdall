UPDATE    dim_notificationphase s
       FROM
          DD07T t
   SET s.NotificationPhaseName = t.DD07T_DDTEXT,
			s.dw_update_date = current_timestamp
 WHERE s.RowIsCurrent = 1
    AND   t.DD07T_DOMNAME = 'QM_PHASE'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND s.NotificationPhaseCode = t.DD07T_DOMVALUE
;

INSERT INTO dim_notificationphase(dim_notificationphaseId, RowIsCurrent,rowstartdate)
SELECT 1, 1,current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_notificationphase
               WHERE dim_notificationphaseId = 1);


delete from number_fountain m where m.table_name = 'dim_notificationphase';
   
insert into number_fountain
select 	'dim_notificationphase',
	ifnull(max(d.Dim_NotificationPhaseid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_notificationphase d
where d.Dim_NotificationPhaseid <> 1; 

INSERT INTO dim_notificationphase(Dim_NotificationPhaseid,
                                       NotificationPhaseCode,
                                       NotificationPhaseName,
                                       RowStartDate,
                                       RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_notificationphase') 
          + row_number() over() ,
			 t.DD07T_DOMVALUE,
          t.DD07T_DDTEXT,
          current_timestamp,
          1
     FROM DD07T t
    WHERE t.DD07T_DOMNAME = 'QM_PHASE' AND t.DD07T_DOMVALUE IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_notificationphase s
                  WHERE     s.NotificationPhaseCode = t.DD07T_DOMVALUE
                        AND t.DD07T_DOMNAME = 'QM_PHASE'
                        AND s.RowIsCurrent = 1)
;
