UPDATE    dim_ProductionOrderType pot
       FROM
          T003P tp
   SET pot.Description = ifnull(T003P_TXT, 'Not Set')
 WHERE pot.RowIsCurrent = 1
 AND tp.T003P_AUART = pot.TypeCode;

UPDATE    dim_ProductionOrderType pot
       FROM
          T003P tp
   SET pot.Description = ifnull(T003P_TXT, 'Not Set')
 WHERE pot.RowIsCurrent = 1
 AND tp.T003P_AUART = pot.TypeCode
;
