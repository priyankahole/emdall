UPDATE    dim_Address da
       FROM
          adrc a, t005t
   SET da.Address1 = ifnull(adrc_name1, 'Not Set'),
       da.Address2 = ifnull(adrc_name2, 'Not Set'),
       da.City = ifnull(adrc_city1, 'Not Set'),
       da.State = ifnull(adrc_region, 'Not Set'),
       da.Zip = ifnull(adrc_post_Code1, 'Not Set'),
       da.Country = ifnull(adrc_country, 'Not Set'),
       da.CountryName = ifnull(t005t_landx, 'Not Set')
   WHERE   a.adrc_addrnumber = da.AddressNumber
          AND a.adrc_addrnumber IS NOT NULL
          AND a.adrc_country = t005t_land1;
