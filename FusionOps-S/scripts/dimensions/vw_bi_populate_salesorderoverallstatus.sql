INSERT INTO dim_salesorderoverallstatus(Dim_SalesOrderStatusid,
							Description,
							Status,
                             RowStartDate,
                             RowEndDate,
                             RowIsCurrent,
                             RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',		  
          current_timestamp,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorderoverallstatus
               WHERE Dim_SalesOrderStatusid = 1);

UPDATE    dim_salesorderoverallstatus s
       FROM
          DD07T t
	SET s.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			s.dw_update_date = current_timestamp
    WHERE t.DD07T_DOMNAME = 'STATV' AND s.Status = t.DD07T_DOMVALUE;

delete from number_fountain m where m.table_name = 'dim_salesorderoverallstatus';

insert into number_fountain
select 	'dim_salesorderoverallstatus',
	ifnull(max(d.Dim_SalesOrderStatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderoverallstatus d
where d.Dim_SalesOrderStatusid <> 1; 
	
INSERT INTO dim_salesorderoverallstatus(Dim_SalesOrderStatusid,
                               Description,
                               Status,
                               RowStartDate,
                               RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderoverallstatus') 
          + row_number() over(),
			  ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'STATV' 
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_salesorderoverallstatus
                    WHERE Status = DD07T_DOMVALUE AND DD07T_DOMNAME = 'STATV')
   ORDER BY 2 OFFSET 0;	