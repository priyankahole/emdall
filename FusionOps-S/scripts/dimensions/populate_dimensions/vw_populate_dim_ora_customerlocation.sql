/*insert default row*/
INSERT INTO dim_ora_customerlocation
(
dim_ora_customerlocationid,
party_name,party_id,cust_account_id,account_number,account_name,site_use_code,address1,address2,
address3,address4,city,state,postal_code,county,territory_short_name,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_customerlocationid,
'Not Set' PARTY_NAME,
0 PARTY_ID,
0 CUST_ACCOUNT_ID,
'Not Set' ACCOUNT_NUMBER,
'Not Set' ACCOUNT_NAME,
'Not Set' SITE_USE_CODE,
'Not Set' ADDRESS1,
'Not Set' ADDRESS2,
'Not Set' ADDRESS3,
'Not Set' ADDRESS4,
'Not Set' CITY,
'Not Set' STATE,
'Not Set' POSTAL_CODE,
'Not Set' COUNTY,
'Not Set' TERRITORY_SHORT_NAME,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_customerlocation D
ON T.dim_ora_customerlocationid = D.dim_ora_customerlocationid
WHERE T.dim_ora_customerlocationid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_customerlocation';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_customerlocation',IFNULL(MAX(dim_ora_customerlocationid),0)
FROM dim_ora_customerlocation;

/*update dimension rows*/
UPDATE dim_ora_customerlocation T
FROM ORA_HZ_CUSTLOCATION S
SET 
T.PARTY_NAME = ifnull(S.PARTY_NAME, 'Not Set'),
T.PARTY_ID = ifnull(S.PARTY_ID, 0),
T.CUST_ACCOUNT_ID = ifnull(S.CUST_ACCOUNT_ID, 0),
T.ACCOUNT_NUMBER = ifnull(S.ACCOUNT_NUMBER, 'Not Set'),
T.ACCOUNT_NAME = ifnull(S.ACCOUNT_NAME, 'Not Set'),
T.SITE_USE_CODE = ifnull(S.SITE_USE_CODE, 'Not Set'),
T.ADDRESS1 = ifnull(S.ADDRESS1, 'Not Set'),
T.ADDRESS2 = ifnull(S.ADDRESS2, 'Not Set'),
T.ADDRESS3 = ifnull(S.ADDRESS3, 'Not Set'),
T.ADDRESS4 = ifnull(S.ADDRESS4, 'Not Set'),
T.CITY = ifnull(S.CITY, 'Not Set'),
T.STATE = ifnull(S.STATE, 'Not Set'),
T.POSTAL_CODE = ifnull(S.POSTAL_CODE, 'Not Set'),
T.COUNTY = ifnull(S.COUNTY, 'Not Set'),
T.TERRITORY_SHORT_NAME = ifnull(S.TERRITORY_SHORT_NAME, 'Not Set'),
T.DW_UPDATE_DATE = current_timestamp,
T.rowiscurrent = 1,
T.SOURCE_ID ='ORA 12.1'
WHERE S.SITE_USE_ID  = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_customerlocation
(
dim_ora_customerlocationid,
party_name,party_id,cust_account_id,account_number,account_name,site_use_code,address1,address2,
address3,address4,city,state,postal_code,county,territory_short_name,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_customerlocation' ),0) + ROW_NUMBER() OVER() dim_ora_customerlocationid,
ifnull(S.PARTY_NAME, 'Not Set') PARTY_NAME,
ifnull(S.PARTY_ID, 0) PARTY_ID,
ifnull(S.CUST_ACCOUNT_ID, 0) CUST_ACCOUNT_ID,
ifnull(S.ACCOUNT_NUMBER, 'Not Set') ACCOUNT_NUMBER,
ifnull(S.ACCOUNT_NAME, 'Not Set') ACCOUNT_NAME,
ifnull(S.SITE_USE_CODE, 'Not Set') SITE_USE_CODE,
ifnull(S.ADDRESS1, 'Not Set') ADDRESS1,
ifnull(S.ADDRESS2, 'Not Set') ADDRESS2,
ifnull(S.ADDRESS3, 'Not Set') ADDRESS3,
ifnull(S.ADDRESS4, 'Not Set') ADDRESS4,
ifnull(S.CITY, 'Not Set') CITY,
ifnull(S.STATE, 'Not Set') STATE,
ifnull(S.POSTAL_CODE, 'Not Set') POSTAL_CODE,
ifnull(S.COUNTY, 'Not Set') COUNTY,
ifnull(S.TERRITORY_SHORT_NAME, 'Not Set') TERRITORY_SHORT_NAME,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.SITE_USE_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_HZ_CUSTLOCATION S LEFT JOIN dim_ora_customerlocation D ON
D.KEY_ID = S.SITE_USE_ID
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_customerlocation');