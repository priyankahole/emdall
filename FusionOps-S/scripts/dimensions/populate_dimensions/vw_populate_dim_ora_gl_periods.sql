/*insert default row*/
INSERT INTO dim_ora_gl_periods
(
dim_ora_gl_periodsid,
PERIOD_SET_NAME,PERIOD_NAME,LAST_UPDATE_DATE,LAST_UPDATED_BY,START_DATE,END_DATE,
PERIOD_TYPE,PERIOD_YEAR,PERIOD_NUM,QUARTER_NUM,ENTERED_PERIOD_NAME,ADJUSTMENT_PERIOD_FLAG,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,
DESCRIPTION,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3,ATTRIBUTE4,ATTRIBUTE5,ATTRIBUTE6,ATTRIBUTE7,ATTRIBUTE8,
CONTEXT,YEAR_START_DATE,QUARTER_START_DATE,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT T.* FROM 
(SELECT
1 dim_ora_gl_periodsid,
'Not Set' PERIOD_SET_NAME,
'Not Set' PERIOD_NAME,
timestamp '1970-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
'1970-01-01' START_DATE,
'1970-01-01' END_DATE,
'Not Set' PERIOD_TYPE,
0 PERIOD_YEAR,
0 PERIOD_NUM,
0 QUARTER_NUM,
'Not Set' ENTERED_PERIOD_NAME,
'Not Set' ADJUSTMENT_PERIOD_FLAG,
timestamp '1970-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' DESCRIPTION,
'Not Set' ATTRIBUTE1,
'Not Set' ATTRIBUTE2,
'Not Set' ATTRIBUTE3,
'Not Set' ATTRIBUTE4,
'Not Set' ATTRIBUTE5,
'Not Set' ATTRIBUTE6,
'Not Set' ATTRIBUTE7,
'Not Set' ATTRIBUTE8,
'Not Set' CONTEXT,
'1970-01-01' YEAR_START_DATE,
'1970-01-01' QUARTER_START_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
'Not Set' KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_gl_periods D 
ON T.dim_ora_gl_periodsid = D.dim_ora_gl_periodsid 
WHERE D.dim_ora_gl_periodsid IS NULL;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_gl_periods';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_gl_periods',IFNULL(MAX(dim_ora_gl_periodsid),0)
FROM dim_ora_gl_periods;

/*update dimension rows*/
UPDATE dim_ora_gl_periods T	
FROM ora_gl_periods S
SET 
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.START_DATE = ifnull(S.START_DATE, '1970-01-01'),
T.END_DATE = ifnull(S.END_DATE, '1970-01-01'),
T.PERIOD_TYPE = ifnull(S.PERIOD_TYPE, 'Not Set'),
T.PERIOD_YEAR = ifnull(S.PERIOD_YEAR, 0),
T.PERIOD_NUM = ifnull(S.PERIOD_NUM, 0),
T.QUARTER_NUM = ifnull(S.QUARTER_NUM, 0),
T.ENTERED_PERIOD_NAME = ifnull(S.ENTERED_PERIOD_NAME, 'Not Set'),
T.ADJUSTMENT_PERIOD_FLAG = ifnull(S.ADJUSTMENT_PERIOD_FLAG, 'Not Set'),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.ATTRIBUTE1 = ifnull(S.ATTRIBUTE1, 'Not Set'),
T.ATTRIBUTE2 = ifnull(S.ATTRIBUTE2, 'Not Set'),
T.ATTRIBUTE3 = ifnull(S.ATTRIBUTE3, 'Not Set'),
T.ATTRIBUTE4 = ifnull(S.ATTRIBUTE4, 'Not Set'),
T.ATTRIBUTE5 = ifnull(S.ATTRIBUTE5, 'Not Set'),
T.ATTRIBUTE6 = ifnull(S.ATTRIBUTE6, 'Not Set'),
T.ATTRIBUTE7 = ifnull(S.ATTRIBUTE7, 'Not Set'),
T.ATTRIBUTE8 = ifnull(S.ATTRIBUTE8, 'Not Set'),
T.CONTEXT = ifnull(S.CONTEXT, 'Not Set'),
T.YEAR_START_DATE = ifnull(S.YEAR_START_DATE, '1970-01-01'),
T.QUARTER_START_DATE = ifnull(S.QUARTER_START_DATE, '1970-01-01'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
T.KEY_ID = VARCHAR(ifnull(S.PERIOD_SET_NAME,'Not Set'), 200) + '~' + VARCHAR(ifnull(S.PERIOD_NAME,'Not Set'), 200); 

/*insert new rows*/
INSERT INTO dim_ora_gl_periods
(
dim_ora_gl_periodsid,
PERIOD_SET_NAME,PERIOD_NAME,LAST_UPDATE_DATE,LAST_UPDATED_BY,START_DATE,END_DATE,
PERIOD_TYPE,PERIOD_YEAR,PERIOD_NUM,QUARTER_NUM,ENTERED_PERIOD_NAME,ADJUSTMENT_PERIOD_FLAG,CREATION_DATE,CREATED_BY,LAST_UPDATE_LOGIN,
DESCRIPTION,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3,ATTRIBUTE4,ATTRIBUTE5,ATTRIBUTE6,ATTRIBUTE7,ATTRIBUTE8,
CONTEXT,YEAR_START_DATE,QUARTER_START_DATE,
DW_INSERT_DATE,DW_UPDATE_DATE,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
KEY_ID,SOURCE_ID
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_gl_periods' ),0) + ROW_NUMBER() OVER() dim_ora_gl_periodsid,
S.PERIOD_SET_NAME PERIOD_SET_NAME,
S.PERIOD_NAME PERIOD_NAME,  
ifnull(S.LAST_UPDATE_DATE, timestamp '1970-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.START_DATE, '1970-01-01') START_DATE,
ifnull(S.END_DATE, '1970-01-01') END_DATE,
ifnull(S.PERIOD_TYPE, 'Not Set') PERIOD_TYPE,
ifnull(S.PERIOD_YEAR, 0) PERIOD_YEAR,
ifnull(S.PERIOD_NUM, 0) PERIOD_NUM,
ifnull(S.QUARTER_NUM, 0) QUARTER_NUM,
ifnull(S.ENTERED_PERIOD_NAME, 'Not Set') ENTERED_PERIOD_NAME,
ifnull(S.ADJUSTMENT_PERIOD_FLAG, 'Not Set') ADJUSTMENT_PERIOD_FLAG,
ifnull(S.CREATION_DATE, timestamp '1970-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.ATTRIBUTE1, 'Not Set') ATTRIBUTE1,
ifnull(S.ATTRIBUTE2, 'Not Set') ATTRIBUTE2,
ifnull(S.ATTRIBUTE3, 'Not Set') ATTRIBUTE3,
ifnull(S.ATTRIBUTE4, 'Not Set') ATTRIBUTE4,
ifnull(S.ATTRIBUTE5, 'Not Set') ATTRIBUTE5,
ifnull(S.ATTRIBUTE6, 'Not Set') ATTRIBUTE6,
ifnull(S.ATTRIBUTE7, 'Not Set') ATTRIBUTE7,
ifnull(S.ATTRIBUTE8, 'Not Set') ATTRIBUTE8,
ifnull(S.CONTEXT, 'Not Set') CONTEXT,
ifnull(S.YEAR_START_DATE, '1970-01-01') YEAR_START_DATE,
ifnull(S.QUARTER_START_DATE, '1970-01-01') QUARTER_START_DATE,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
VARCHAR(ifnull(S.PERIOD_SET_NAME,'Not Set'), 200) + '~' + VARCHAR(ifnull(S.PERIOD_NAME,'Not Set'), 200) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ora_gl_periods S LEFT JOIN dim_ora_gl_periods D
ON D.KEY_ID = VARCHAR(ifnull(S.PERIOD_SET_NAME,'Not Set'), 200) + '~' + VARCHAR(ifnull(S.PERIOD_NAME,'Not Set'), 200)
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_gl_periods');

/*GL_PERIODS - dim_date update*/
DROP IF EXISTS dim_ora_gl_periods_tmp;

create table dim_ora_gl_periods_tmp as 
select dim_dateid, datevalue, 
glp.start_date,
glp.end_date,
glp.period_year,
glp.quarter_num,
glp.period_num,
DENSE_RANK() over(order by period_year asc) RANK_year,
DENSE_RANK() over(order by period_year asc, quarter_num asc) RANK_quarter,
DENSE_RANK() over(order by period_year asc, period_num asc) RANK_month
from 
dim_date d join dim_ora_gl_periods glp
on d.datevalue between glp.start_date and glp.end_date
where glp.period_set_name in (select property_value period_set_name from systemproperty where property ='GL.period_set_name')
and glp.start_date <> glp.end_date;

UPDATE dim_Date dt set financialyear=0;
UPDATE dim_Date dt set financialquarter=0;

UPDATE dim_Date dt set financialyear=(SELECT IFNULL(period_year,0) from dim_ora_gl_periods prd
WHERE
dt.datevalue between prd.start_date and prd.end_date and period_set_name  in (select property_value period_set_name from systemproperty where property ='GL.period_set_name'));

UPDATE dim_Date dt set financialquarter=(SELECT IFNULL(quarter_num,0) from dim_ora_gl_periods prd
WHERE
dt.datevalue between prd.start_date and prd.end_date and period_set_name in (select property_value period_set_name from systemproperty where property ='GL.period_set_name'));

UPDATE dim_Date dt set financialquartername=(SELECT concat(IFNULL(period_year,0) || ' Q ' || IFNULL(quarter_num,0)) from dim_ora_gl_periods prd
WHERE
dt.datevalue between prd.start_date and prd.end_date and period_set_name in (select property_value period_set_name from systemproperty where property ='GL.period_set_name'));

UPDATE dim_Date dt set financialquarterid=(SELECT concat(ifnull(period_year,0) || IFNULL(quarter_num,0)) from dim_ora_gl_periods prd
WHERE
dt.datevalue between prd.start_date and prd.end_date and period_set_name  in (select property_value period_set_name from systemproperty where property ='GL.period_set_name'));

UPDATE dim_Date dt set financialmonthnumber=(SELECT  IFNULL(period_num,0) from dim_ora_gl_periods prd
WHERE
dt.datevalue between prd.start_date and prd.end_date and period_set_name  in (select property_value period_set_name from systemproperty where property ='GL.period_set_name'));

/*year flag*/
UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set yearflag='Current'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.rank_year = (select distinct rank_year from dim_ora_gl_periods_tmp where current_date = datevalue);

UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set yearflag='Previous'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.rank_year+1 = (select distinct rank_year from dim_ora_gl_periods_tmp where current_date = datevalue);

UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set yearflag='Next'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.rank_year-1 = (select distinct rank_year from dim_ora_gl_periods_tmp where current_date = datevalue);

/*quarter flag*/
UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set quarterflag='Current'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.RANK_quarter = (select distinct RANK_quarter from dim_ora_gl_periods_tmp where current_date = datevalue);

UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set quarterflag='Previous'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.RANK_quarter+1 = (select distinct RANK_quarter from dim_ora_gl_periods_tmp where current_date = datevalue);

UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set quarterflag='Next'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.RANK_quarter-1 = (select distinct RANK_quarter from dim_ora_gl_periods_tmp where current_date = datevalue);

/*month flag*/ 
UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set monthflag='Current'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.RANK_month = (select distinct RANK_month from dim_ora_gl_periods_tmp where current_date = datevalue);

UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set monthflag='Previous'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.RANK_month+1 = (select distinct RANK_month from dim_ora_gl_periods_tmp where current_date = datevalue);

UPDATE dim_Date dt 
from dim_ora_gl_periods_tmp tmp 
set monthflag='Next'
WHERE
dt.datevalue = tmp.datevalue 
and tmp.RANK_month-1 = (select distinct RANK_month from dim_ora_gl_periods_tmp where current_date = datevalue);

DROP IF EXISTS dim_ora_gl_periods_tmp;

CALL VECTORWISE( COMBINE 'dim_date');