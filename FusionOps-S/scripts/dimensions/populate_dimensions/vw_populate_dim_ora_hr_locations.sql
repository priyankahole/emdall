/*insert default row*/
INSERT INTO dim_ora_hr_locations
(
dim_ora_hr_locationsid,
address_line_3,address_line_2,address_line_1,style,inactive_date,tax_name,inventory_organization_id,
designated_receiver_id,office_site_flag,in_organization_flag,bill_to_site_flag,receiving_site_flag,ship_to_site_flag,ship_to_location_id,
description,business_group_name,location_use,location_code,last_update_login,last_updated_by,last_update_date,
loc_information20,loc_information19,loc_information18,loc_information17,loc_information16,loc_information15,loc_information14,
loc_information13,telephone_number_3,telephone_number_2,telephone_number_1,region_3,region_2,region_1,postal_code,country,town_or_city,
timezone_code,legal_address_flag,object_version_number,ece_tp_location_code,tp_header_id,entered_by,creation_date,created_by,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_hr_locationsid,
'Not Set' ADDRESS_LINE_3,
'Not Set' ADDRESS_LINE_2,
'Not Set' ADDRESS_LINE_1,
'Not Set' STYLE,
timestamp '0001-01-01 00:00:00.000000' INACTIVE_DATE,
'Not Set' TAX_NAME,
0 INVENTORY_ORGANIZATION_ID,
0 DESIGNATED_RECEIVER_ID,
'Not Set' OFFICE_SITE_FLAG,
'Not Set' IN_ORGANIZATION_FLAG,
'Not Set' BILL_TO_SITE_FLAG,
'Not Set' RECEIVING_SITE_FLAG,
'Not Set' SHIP_TO_SITE_FLAG,
0 SHIP_TO_LOCATION_ID,
'Not Set' DESCRIPTION,
0 BUSINESS_GROUP_NAME,
'Not Set' LOCATION_USE,
'Not Set' LOCATION_CODE,
0 LAST_UPDATE_LOGIN,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
'Not Set' LOC_INFORMATION20,
'Not Set' LOC_INFORMATION19,
'Not Set' LOC_INFORMATION18,
'Not Set' LOC_INFORMATION17,
'Not Set' LOC_INFORMATION16,
'Not Set' LOC_INFORMATION15,
'Not Set' LOC_INFORMATION14,
'Not Set' LOC_INFORMATION13,
'Not Set' TELEPHONE_NUMBER_3,
'Not Set' TELEPHONE_NUMBER_2,
'Not Set' TELEPHONE_NUMBER_1,
'Not Set' REGION_3,
'Not Set' REGION_2,
'Not Set' REGION_1,
'Not Set' POSTAL_CODE,
'Not Set' COUNTRY,
'Not Set' TOWN_OR_CITY,
'Not Set' TIMEZONE_CODE,
'Not Set' LEGAL_ADDRESS_FLAG,
0 OBJECT_VERSION_NUMBER,
'Not Set' ECE_TP_LOCATION_CODE,
0 TP_HEADER_ID,
0 ENTERED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_hr_locations D
ON T.dim_ora_hr_locationsid = D.dim_ora_hr_locationsid
WHERE D.dim_ora_hr_locationsid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_hr_locations';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_hr_locations',IFNULL(MAX(dim_ora_hr_locationsid),0)
FROM dim_ora_hr_locations;

/*update dimension rows*/
UPDATE dim_ora_hr_locations T
FROM dim_ora_hr_locations D JOIN ORA_HR_LOCATIONS S
ON D.KEY_ID = S.LOCATION_ID
SET 
T.ADDRESS_LINE_3 = ifnull(S.ADDRESS_LINE_3, 'Not Set'),
T.ADDRESS_LINE_2 = ifnull(S.ADDRESS_LINE_2, 'Not Set'),
T.ADDRESS_LINE_1 = ifnull(S.ADDRESS_LINE_1, 'Not Set'),
T.STYLE = ifnull(S.STYLE, 'Not Set'),
T.INACTIVE_DATE = ifnull(S.INACTIVE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.TAX_NAME = ifnull(S.TAX_NAME, 'Not Set'),
T.INVENTORY_ORGANIZATION_ID = ifnull(S.INVENTORY_ORGANIZATION_ID, 0),
T.DESIGNATED_RECEIVER_ID = ifnull(S.DESIGNATED_RECEIVER_ID, 0),
T.OFFICE_SITE_FLAG = ifnull(S.OFFICE_SITE_FLAG, 'Not Set'),
T.IN_ORGANIZATION_FLAG = ifnull(S.IN_ORGANIZATION_FLAG, 'Not Set'),
T.BILL_TO_SITE_FLAG = ifnull(S.BILL_TO_SITE_FLAG, 'Not Set'),
T.RECEIVING_SITE_FLAG = ifnull(S.RECEIVING_SITE_FLAG, 'Not Set'),
T.SHIP_TO_SITE_FLAG = ifnull(S.SHIP_TO_SITE_FLAG, 'Not Set'),
T.SHIP_TO_LOCATION_ID = ifnull(S.SHIP_TO_LOCATION_ID, 0),
T.DESCRIPTION = ifnull(S.DESCRIPTION, 'Not Set'),
T.BUSINESS_GROUP_NAME = ifnull(S.BUSINESS_GROUP_NAME, 'Not Set'),
T.LOCATION_USE = ifnull(S.LOCATION_USE, 'Not Set'),
T.LOCATION_CODE = ifnull(S.LOCATION_CODE, 'Not Set'),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LOC_INFORMATION20 = ifnull(S.LOC_INFORMATION20, 'Not Set'),
T.LOC_INFORMATION19 = ifnull(S.LOC_INFORMATION19, 'Not Set'),
T.LOC_INFORMATION18 = ifnull(S.LOC_INFORMATION18, 'Not Set'),
T.LOC_INFORMATION17 = ifnull(S.LOC_INFORMATION17, 'Not Set'),
T.LOC_INFORMATION16 = ifnull(S.LOC_INFORMATION16, 'Not Set'),
T.LOC_INFORMATION15 = ifnull(S.LOC_INFORMATION15, 'Not Set'),
T.LOC_INFORMATION14 = ifnull(S.LOC_INFORMATION14, 'Not Set'),
T.LOC_INFORMATION13 = ifnull(S.LOC_INFORMATION13, 'Not Set'),
T.TELEPHONE_NUMBER_3 = ifnull(S.TELEPHONE_NUMBER_3, 'Not Set'),
T.TELEPHONE_NUMBER_2 = ifnull(S.TELEPHONE_NUMBER_2, 'Not Set'),
T.TELEPHONE_NUMBER_1 = ifnull(S.TELEPHONE_NUMBER_1, 'Not Set'),
T.REGION_3 = ifnull(S.REGION_3, 'Not Set'),
T.REGION_2 = ifnull(S.REGION_2, 'Not Set'),
T.REGION_1 = ifnull(S.REGION_1, 'Not Set'),
T.POSTAL_CODE = ifnull(S.POSTAL_CODE, 'Not Set'),
T.COUNTRY = ifnull(S.COUNTRY, 'Not Set'),
T.TOWN_OR_CITY = ifnull(S.TOWN_OR_CITY, 'Not Set'),
T.TIMEZONE_CODE = ifnull(S.TIMEZONE_CODE, 'Not Set'),
T.LEGAL_ADDRESS_FLAG = ifnull(S.LEGAL_ADDRESS_FLAG, 'Not Set'),
T.OBJECT_VERSION_NUMBER = ifnull(S.OBJECT_VERSION_NUMBER, 0),
T.ECE_TP_LOCATION_CODE = ifnull(S.ECE_TP_LOCATION_CODE, 'Not Set'),
T.TP_HEADER_ID = ifnull(S.TP_HEADER_ID, 0),
T.ENTERED_BY = ifnull(S.ENTERED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
S.LOCATION_ID = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_hr_locations
(
dim_ora_hr_locationsid,
address_line_3,address_line_2,address_line_1,style,inactive_date,tax_name,inventory_organization_id,
designated_receiver_id,office_site_flag,in_organization_flag,bill_to_site_flag,receiving_site_flag,ship_to_site_flag,ship_to_location_id,
description,business_group_name,location_use,location_code,last_update_login,last_updated_by,last_update_date,
loc_information20,loc_information19,loc_information18,loc_information17,loc_information16,loc_information15,loc_information14,
loc_information13,telephone_number_3,telephone_number_2,telephone_number_1,region_3,region_2,region_1,postal_code,country,town_or_city,
timezone_code,legal_address_flag,object_version_number,ece_tp_location_code,tp_header_id,entered_by,creation_date,created_by,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_hr_locations' ),0) + ROW_NUMBER() OVER() dim_ora_hr_locationsid,
ifnull(S.ADDRESS_LINE_3, 'Not Set') ADDRESS_LINE_3,
ifnull(S.ADDRESS_LINE_2, 'Not Set') ADDRESS_LINE_2,
ifnull(S.ADDRESS_LINE_1, 'Not Set') ADDRESS_LINE_1,
ifnull(S.STYLE, 'Not Set') STYLE,
ifnull(S.INACTIVE_DATE, timestamp '0001-01-01 00:00:00.000000') INACTIVE_DATE,
ifnull(S.TAX_NAME, 'Not Set') TAX_NAME,
ifnull(S.INVENTORY_ORGANIZATION_ID, 0) INVENTORY_ORGANIZATION_ID,
ifnull(S.DESIGNATED_RECEIVER_ID, 0) DESIGNATED_RECEIVER_ID,
ifnull(S.OFFICE_SITE_FLAG, 'Not Set') OFFICE_SITE_FLAG,
ifnull(S.IN_ORGANIZATION_FLAG, 'Not Set') IN_ORGANIZATION_FLAG,
ifnull(S.BILL_TO_SITE_FLAG, 'Not Set') BILL_TO_SITE_FLAG,
ifnull(S.RECEIVING_SITE_FLAG, 'Not Set') RECEIVING_SITE_FLAG,
ifnull(S.SHIP_TO_SITE_FLAG, 'Not Set') SHIP_TO_SITE_FLAG,
ifnull(S.SHIP_TO_LOCATION_ID, 0) SHIP_TO_LOCATION_ID,
ifnull(S.DESCRIPTION, 'Not Set') DESCRIPTION,
ifnull(S.BUSINESS_GROUP_NAME, 'Not Set') BUSINESS_GROUP_NAME,
ifnull(S.LOCATION_USE, 'Not Set') LOCATION_USE,
ifnull(S.LOCATION_CODE, 'Not Set') LOCATION_CODE,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LOC_INFORMATION20, 'Not Set') LOC_INFORMATION20,
ifnull(S.LOC_INFORMATION19, 'Not Set') LOC_INFORMATION19,
ifnull(S.LOC_INFORMATION18, 'Not Set') LOC_INFORMATION18,
ifnull(S.LOC_INFORMATION17, 'Not Set') LOC_INFORMATION17,
ifnull(S.LOC_INFORMATION16, 'Not Set') LOC_INFORMATION16,
ifnull(S.LOC_INFORMATION15, 'Not Set') LOC_INFORMATION15,
ifnull(S.LOC_INFORMATION14, 'Not Set') LOC_INFORMATION14,
ifnull(S.LOC_INFORMATION13, 'Not Set') LOC_INFORMATION13,
ifnull(S.TELEPHONE_NUMBER_3, 'Not Set') TELEPHONE_NUMBER_3,
ifnull(S.TELEPHONE_NUMBER_2, 'Not Set') TELEPHONE_NUMBER_2,
ifnull(S.TELEPHONE_NUMBER_1, 'Not Set') TELEPHONE_NUMBER_1,
ifnull(S.REGION_3, 'Not Set') REGION_3,
ifnull(S.REGION_2, 'Not Set') REGION_2,
ifnull(S.REGION_1, 'Not Set') REGION_1,
ifnull(S.POSTAL_CODE, 'Not Set') POSTAL_CODE,
ifnull(S.COUNTRY, 'Not Set') COUNTRY,
ifnull(S.TOWN_OR_CITY, 'Not Set') TOWN_OR_CITY,
ifnull(S.TIMEZONE_CODE, 'Not Set') TIMEZONE_CODE,
ifnull(S.LEGAL_ADDRESS_FLAG, 'Not Set') LEGAL_ADDRESS_FLAG,
ifnull(S.OBJECT_VERSION_NUMBER, 0) OBJECT_VERSION_NUMBER,
ifnull(S.ECE_TP_LOCATION_CODE, 'Not Set') ECE_TP_LOCATION_CODE,
ifnull(S.TP_HEADER_ID, 0) TP_HEADER_ID,
ifnull(S.ENTERED_BY, 0) ENTERED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
S.LOCATION_ID KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_HR_LOCATIONS S LEFT JOIN dim_ora_hr_locations D 
ON D.KEY_ID = S.LOCATION_ID
WHERE D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_hr_locations');