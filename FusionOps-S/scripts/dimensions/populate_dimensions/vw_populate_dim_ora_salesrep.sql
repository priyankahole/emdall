/*Add 2 new columns*/
/*insert default row*/
INSERT INTO dim_ora_salesrep
(
dim_ora_salesrepid,
security_group_id,resource_id,last_update_date,last_updated_by,creation_date,created_by,last_update_login,sales_credit_type_name,
repname,status,start_date_active,end_date_active,gl_id_rev,gl_id_freight,gl_id_rec,set_of_books_id,salesrep_number,email_address,
wh_update_date,person_id,sales_tax_geocode,sales_tax_inside_city_limits,object_version_number,attribute_category,area,region,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT T.* FROM
(SELECT
1 dim_ora_salesrepid,
0 SECURITY_GROUP_ID,
0 RESOURCE_ID,
timestamp '0001-01-01 00:00:00.000000' LAST_UPDATE_DATE,
0 LAST_UPDATED_BY,
timestamp '0001-01-01 00:00:00.000000' CREATION_DATE,
0 CREATED_BY,
0 LAST_UPDATE_LOGIN,
'Not Set' SALES_CREDIT_TYPE_NAME,
'Not Set' REPNAME,
'Not Set' STATUS,
timestamp '0001-01-01 00:00:00.000000' START_DATE_ACTIVE,
timestamp '0001-01-01 00:00:00.000000' END_DATE_ACTIVE,
0 GL_ID_REV,
0 GL_ID_FREIGHT,
0 GL_ID_REC,
0 SET_OF_BOOKS_ID,
'Not Set' SALESREP_NUMBER,
'Not Set' EMAIL_ADDRESS,
timestamp '0001-01-01 00:00:00.000000' WH_UPDATE_DATE,
0 PERSON_ID,
'Not Set' SALES_TAX_GEOCODE,
'Not Set' SALES_TAX_INSIDE_CITY_LIMITS,
0 OBJECT_VERSION_NUMBER,
'Not Set' ATTRIBUTE_CATEGORY,
'Not Set' area,
'Not Set' region,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'ORA 12.1' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_ora_salesrep D
ON T.dim_ora_salesrepid = D.dim_ora_salesrepid
WHERE D.dim_ora_salesrepid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_ora_salesrep';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_ora_salesrep',IFNULL(MAX(dim_ora_salesrepid),0)
FROM dim_ora_salesrep;

/*update dimension rows*/
UPDATE dim_ora_salesrep T
FROM dim_ora_salesrep D JOIN ORA_JTF_SALESREP S
ON D.KEY_ID = VARCHAR(ifnull(S.SALESREP_ID,0), 200) +'~' + VARCHAR(ifnull(S.ORG_ID,0), 200)
SET
T.SECURITY_GROUP_ID = ifnull(S.SECURITY_GROUP_ID, 0),
T.RESOURCE_ID = ifnull(S.RESOURCE_ID, 0),
T.LAST_UPDATE_DATE = ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.LAST_UPDATED_BY = ifnull(S.LAST_UPDATED_BY, 0),
T.CREATION_DATE = ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.CREATED_BY = ifnull(S.CREATED_BY, 0),
T.LAST_UPDATE_LOGIN = ifnull(S.LAST_UPDATE_LOGIN, 0),
T.SALES_CREDIT_TYPE_NAME = ifnull(S.SALES_CREDIT_TYPE_NAME, 'Not Set'),
T.REPNAME = ifnull(S.REPNAME, 'Not Set'),
T.STATUS = ifnull(S.STATUS, 'Not Set'),
T.START_DATE_ACTIVE = ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.END_DATE_ACTIVE = ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000'),
T.GL_ID_REV = ifnull(S.GL_ID_REV, 0),
T.GL_ID_FREIGHT = ifnull(S.GL_ID_FREIGHT, 0),
T.GL_ID_REC = ifnull(S.GL_ID_REC, 0),
T.SET_OF_BOOKS_ID = ifnull(S.SET_OF_BOOKS_ID, 0),
T.SALESREP_NUMBER = ifnull(S.SALESREP_NUMBER, 'Not Set'),
T.EMAIL_ADDRESS = ifnull(S.EMAIL_ADDRESS, 'Not Set'),
T.WH_UPDATE_DATE = ifnull(S.WH_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000'),
T.PERSON_ID = ifnull(S.PERSON_ID, 0),
T.SALES_TAX_GEOCODE = ifnull(S.SALES_TAX_GEOCODE, 'Not Set'),
T.SALES_TAX_INSIDE_CITY_LIMITS = ifnull(S.SALES_TAX_INSIDE_CITY_LIMITS, 'Not Set'),
T.OBJECT_VERSION_NUMBER = ifnull(S.OBJECT_VERSION_NUMBER, 0),
T.ATTRIBUTE_CATEGORY = ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set'),
T.area = ifnull(S.area, 'Not Set'),
T.region = ifnull(S.region, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='ORA 12.1'
WHERE
VARCHAR(ifnull(S.SALESREP_ID,0), 200) +'~' + VARCHAR(ifnull(S.ORG_ID,0), 200)  = T.KEY_ID;

/*insert new rows*/
INSERT INTO dim_ora_salesrep
(
dim_ora_salesrepid,
security_group_id,resource_id,last_update_date,last_updated_by,creation_date,created_by,last_update_login,sales_credit_type_name,
repname,status,start_date_active,end_date_active,gl_id_rev,gl_id_freight,gl_id_rec,set_of_books_id,salesrep_number,email_address,
wh_update_date,person_id,sales_tax_geocode,sales_tax_inside_city_limits,object_version_number,attribute_category,area,region,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,
key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_ora_salesrep' ),0) + ROW_NUMBER() OVER() dim_ora_salesrepid,
ifnull(S.SECURITY_GROUP_ID, 0) SECURITY_GROUP_ID,
ifnull(S.RESOURCE_ID, 0) RESOURCE_ID,
ifnull(S.LAST_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') LAST_UPDATE_DATE,
ifnull(S.LAST_UPDATED_BY, 0) LAST_UPDATED_BY,
ifnull(S.CREATION_DATE, timestamp '0001-01-01 00:00:00.000000') CREATION_DATE,
ifnull(S.CREATED_BY, 0) CREATED_BY,
ifnull(S.LAST_UPDATE_LOGIN, 0) LAST_UPDATE_LOGIN,
ifnull(S.SALES_CREDIT_TYPE_NAME, 'Not Set') SALES_CREDIT_TYPE_NAME,
ifnull(S.REPNAME, 'Not Set') REPNAME,
ifnull(S.STATUS, 'Not Set') STATUS,
ifnull(S.START_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') START_DATE_ACTIVE,
ifnull(S.END_DATE_ACTIVE, timestamp '0001-01-01 00:00:00.000000') END_DATE_ACTIVE,
ifnull(S.GL_ID_REV, 0) GL_ID_REV,
ifnull(S.GL_ID_FREIGHT, 0) GL_ID_FREIGHT,
ifnull(S.GL_ID_REC, 0) GL_ID_REC,
ifnull(S.SET_OF_BOOKS_ID, 0) SET_OF_BOOKS_ID,
ifnull(S.SALESREP_NUMBER, 'Not Set') SALESREP_NUMBER,
ifnull(S.EMAIL_ADDRESS, 'Not Set') EMAIL_ADDRESS,
ifnull(S.WH_UPDATE_DATE, timestamp '0001-01-01 00:00:00.000000') WH_UPDATE_DATE,
ifnull(S.PERSON_ID, 0) PERSON_ID,
ifnull(S.SALES_TAX_GEOCODE, 'Not Set') SALES_TAX_GEOCODE,
ifnull(S.SALES_TAX_INSIDE_CITY_LIMITS, 'Not Set') SALES_TAX_INSIDE_CITY_LIMITS,
ifnull(S.OBJECT_VERSION_NUMBER, 0) OBJECT_VERSION_NUMBER,
ifnull(S.ATTRIBUTE_CATEGORY, 'Not Set') ATTRIBUTE_CATEGORY,
ifnull(S.area, 'Not Set') area,
ifnull(S.region, 'Not Set') region,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
VARCHAR(ifnull(S.SALESREP_ID,0), 200) +'~' + VARCHAR(ifnull(S.ORG_ID,0), 200) KEY_ID,
'ORA 12.1' SOURCE_ID
FROM ORA_JTF_SALESREP S LEFT JOIN dim_ora_salesrep D ON
D.KEY_ID = VARCHAR(ifnull(S.SALESREP_ID,0), 200) +'~' + VARCHAR(ifnull(S.ORG_ID,0), 200)
WHERE
D.KEY_ID is null;

/*CALL VECTORWISE COMBINE*/
CALL VECTORWISE( COMBINE 'dim_ora_salesrep');