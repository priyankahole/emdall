INSERT INTO dim_materialsale
(dim_materialsaleid)
select 1
from (select 1) a
where not exists ( select 'x' from dim_materialsale where dim_materialsaleid = 1);

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET CommissionGroup = ifnull(mv.MVKE_PROVG, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.CommissionGroup <> ifnull(mv.MVKE_PROVG, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET CrossSDMaterialStatus = ifnull(mv.MARA_MSTAV, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.CrossSDMaterialStatus <> ifnull(Mv.MARA_MSTAV, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET CSDMaterialStatus = ifnull((SELECT TVMST_VMSTB FROM TVMST WHERE TVMST_VMSTA = mv.MVKE_VMSTA), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.CSDMaterialStatus <> ifnull((SELECT TVMST_VMSTB FROM TVMST WHERE TVMST_VMSTA = mv.MVKE_VMSTA), 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET CSDMaterialStatusCode = ifnull(MVKE_VMSTA, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.CSDMaterialStatusCode <> ifnull(MVKE_VMSTA, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DChainValidFromDt = mv.MVKE_VMSTD,
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.DChainValidFromDt <> mv.MVKE_VMSTD;

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DeliveryPlant = ifnull(mv.MVKE_DWERK, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.DeliveryPlant <> ifnull(mv.MVKE_DWERK, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DeliveryUnits = ifnull(mv.MVKE_SCMNG,0),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
	AND p.SalesOrgCode = mv.MVKE_VKORG
	AND p.DistributionChannelCode = mv.MVKE_VTWEG
	AND mv.MAKT_SPRAS = 'E'
	AND p.DeliveryUnits <> ifnull(mv.MVKE_SCMNG,0);

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DistributionChannel = ifnull((SELECT TVTWT_VTEXT FROM TVTWT WHERE TVTWT_VTWEG = mv.MVKE_VTWEG), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.DistributionChannel <>  ifnull((SELECT TVTWT_VTEXT FROM TVTWT WHERE TVTWT_VTWEG = mv.MVKE_VTWEG), 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DistributionChannelCode = ifnull(MVKE_VTWEG, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.DistributionChannelCode <> ifnull(MVKE_VTWEG, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET GridValue = ifnull(AFSMVKE_J_3ASIZE , 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.GridValue <> ifnull(AFSMVKE_J_3ASIZE , 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup1 = ifnull(mv.MVKE_MVGR1, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialGroup1  <> ifnull(mv.MVKE_MVGR1, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup2 = ifnull(mv.MVKE_MVGR2, 'Not Set'),
			p.dw_update_date = current_timestamp 
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.MaterialGroup2 <> ifnull(mv.MVKE_MVGR2, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup3 = ifnull(mv.MVKE_MVGR3, 'Not Set'),
			p.dw_update_date = current_timestamp 
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.MaterialGroup3 <> ifnull(mv.MVKE_MVGR3, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup4 = ifnull(mv.MVKE_MVGR4, 'Not Set'),
			p.dw_update_date = current_timestamp              
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialGroup4 <> ifnull(mv.MVKE_MVGR4, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup5 = ifnull(mv.MVKE_MVGR5, 'Not Set'),
			p.dw_update_date = current_timestamp 
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialGroup5 <> ifnull(mv.MVKE_MVGR5, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup1Description = ifnull((SELECT TVM1T_BEZEI FROM TVM1T t1 WHERE t1.TVM1T_MVGR1 =  mv.MVKE_MVGR1),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup1Description <> ifnull((SELECT TVM1T_BEZEI FROM TVM1T t1 WHERE t1.TVM1T_MVGR1 =  mv.MVKE_MVGR1),'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup2Description = ifnull((SELECT TVM2T_BEZEI FROM TVM2T t2 WHERE t2.TVM2T_MVGR2 =  mv.MVKE_MVGR2),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup2Description <> ifnull((SELECT TVM2T_BEZEI FROM TVM2T t2 WHERE t2.TVM2T_MVGR2 =  mv.MVKE_MVGR2),'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup3Description = ifnull((SELECT TVM3T_BEZEI FROM TVM3T t3 WHERE t3.TVM3T_MVGR3 =  mv.MVKE_MVGR3),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup3Description <> ifnull((SELECT TVM3T_BEZEI FROM TVM3T t3 WHERE t3.TVM3T_MVGR3 =  mv.MVKE_MVGR3),'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup4Description = ifnull((SELECT TVM4T_BEZEI FROM TVM4T t4 WHERE t4.TVM4T_MVGR4 =  mv.MVKE_MVGR4),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup4Description <> ifnull((SELECT TVM4T_BEZEI FROM TVM4T t4 WHERE t4.TVM4T_MVGR4 =  mv.MVKE_MVGR4),'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup5Description = ifnull((SELECT TVM5T_BEZEI FROM TVM5T t5 WHERE t5.TVM5T_MVGR5 =  mv.MVKE_MVGR5),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup5Description  <>  ifnull((SELECT TVM5T_BEZEI FROM TVM5T t5 WHERE t5.TVM5T_MVGR5 =  mv.MVKE_MVGR5),'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialPricingGroup = ifnull(mv.MVKE_KONDM, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialPricingGroup  <> ifnull(mv.MVKE_KONDM, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET PackQuantity = ifnull(AFSMVKE_J_3APQTY,0.000),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.PackQuantity <> ifnull(AFSMVKE_J_3APQTY, 0.000);

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
	  INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET PartNumber = mv.MARA_MATNR,
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E' 
	AND p.PartNumber <> mv.MARA_MATNR ;

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET PrcIndicatorMaterialMaster = ifnull(MVKE_J_3APIND, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.PrcIndicatorMaterialMaster <> ifnull(MVKE_J_3APIND, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET RefValCatSpecialValuesGEND = ifnull(AFSMVKE_AFS_REFGEND, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.RefValCatSpecialValuesGEND <> ifnull(AFSMVKE_AFS_REFGEND, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET RefValCatSpecialValuesPQ = ifnull(AFSMVKE_AFS_REFPQ, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.RefValCatSpecialValuesPQ <> ifnull(AFSMVKE_AFS_REFPQ, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET RefValCatSpecialValuesVMS = ifnull(AFSMVKE_AFS_REFVMS, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.RefValCatSpecialValuesVMS <> ifnull(AFSMVKE_AFS_REFVMS, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET RequirementCategory = ifnull(AFSMVKE_J_4KRCAT, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.RequirementCategory <> ifnull(AFSMVKE_J_4KRCAT, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET RoundingProfle = ifnull(mv.MVKE_RDPRF, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.RoundingProfle <> ifnull(mv.MVKE_RDPRF, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET SalesGridNumber = ifnull((SELECT J_3AGRHD_J_3APGNR
          FROM KOTJ010, J_3AGRHD
         WHERE KOTJ010_VTWEG = mv.MVKE_VTWEG
		   AND KOTJ010_VKORG = mv.MVKE_VKORG
		   AND KOTJ010_MATNR = mv.MARA_MATNR
		   AND J_3AGRHD_KNUMH = KOTJ010_KNUMH),
       'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.SalesGridNumber <> ifnull((SELECT J_3AGRHD_J_3APGNR
          FROM KOTJ010, J_3AGRHD
         WHERE KOTJ010_VTWEG = mv.MVKE_VTWEG
                   AND KOTJ010_VKORG = mv.MVKE_VKORG
                   AND KOTJ010_MATNR = mv.MARA_MATNR
                   AND J_3AGRHD_KNUMH = KOTJ010_KNUMH),
       'Not Set') ;

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET SalesOrg = ifnull((SELECT TVKOT_VTEXT FROM TVKOT WHERE TVKOT_VKORG = mv.MVKE_VKORG), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.SalesOrg  <>  ifnull((SELECT TVKOT_VTEXT FROM TVKOT WHERE TVKOT_VKORG = mv.MVKE_VKORG), 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET SalesOrgCode = ifnull(MVKE_VKORG, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.SalesOrgCode <> ifnull(MVKE_VKORG, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET StandardCaseType = ifnull(AFSMVKE_J_3ACASE, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.StandardCaseType <> ifnull(AFSMVKE_J_3ACASE, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET TargetGroup = ifnull(AFSMVKE_J_3AGEND, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.TargetGroup <> ifnull(AFSMVKE_J_3AGEND, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET UnitOfPackQuantity = ifnull(AFSMVKE_J_3APQUN, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.UnitOfPackQuantity <> ifnull(AFSMVKE_J_3APQUN, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET ValueAddSrvMaterialGroup = ifnull(MVKE_J_3AVASG, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.ValueAddSrvMaterialGroup <> ifnull(MVKE_J_3AVASG, 'Not Set');

UPDATE dim_materialsale p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET VolumeRebateGroup = ifnull(MVKE_BONUS, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.VolumeRebateGroup <> ifnull(MVKE_BONUS, 'Not Set');


insert into dim_materialsale (dim_materialsaleid,rowstartdate,rowiscurrent,partnumber_noleadzero)
select 1,current_timestamp,1,'Not Set' from (select 1) as src
where
not exists (select 1 from dim_materialsale where dim_materialsaleid=1);

call vectorwise (combine 'tmp_MARAMVKEMAKT_ins_mat-tmp_MARAMVKEMAKT_ins_mat');
call vectorwise (combine 'tmp_MARAMVKEMAKT_del_mat-tmp_MARAMVKEMAKT_del_mat');

insert into tmp_MARAMVKEMAKT_ins_mat
select mv.MARA_MATNR, mv.MVKE_VKORG, mv.MVKE_VTWEG
FROM    MARA_MVKE_MAKT mv;

insert into tmp_MARAMVKEMAKT_del_mat
select p.PartNumber, p.SalesOrgCode,  p.DistributionChannelCode
from dim_materialsale p;

call vectorwise (combine 'tmp_MARAMVKEMAKT_ins_mat-tmp_MARAMVKEMAKT_del_mat');

delete from number_fountain m where m.table_name = 'dim_materialsale';

insert into number_fountain				   
select 	'dim_materialsale',
	ifnull(max(d.dim_materialsaleid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_materialsale d
where d.dim_materialsaleid <> 1;

INSERT INTO dim_materialsale(
	dim_materialsaleid,
	RowStartDate,
	RowIsCurrent,
	CommissionGroup,
	CrossSDMaterialStatus,
	CSDMaterialStatus,
	CSDMaterialStatusCode,
	DChainValidFromDt,
	DeliveryPlant,
	DeliveryUnits,
	DistributionChannel,
	DistributionChannelCode,
	GridValue,
	MaterialGroup1,
	MaterialGroup2,
	MaterialGroup3,
	MaterialGroup4,
	MaterialGroup5,
	MaterialPriceGroup1Description,
	MaterialPriceGroup2Description,
	MaterialPriceGroup3Description,
	MaterialPriceGroup4Description,
	MaterialPriceGroup5Description,
	MaterialPricingGroup,
	PackQuantity,
	PartNumber,
	PrcIndicatorMaterialMaster,
	RefValCatSpecialValuesGEND,
	RefValCatSpecialValuesPQ,
	RefValCatSpecialValuesVMS,
	RequirementCategory,
	RoundingProfle,
	SalesGridNumber,
	SalesOrg,
	SalesOrgCode,
	StandardCaseType,
	TargetGroup,
	UnitOfPackQuantity,
	ValueAddSrvMaterialGroup,
	VolumeRebateGroup)
Select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_materialsale') + row_number() over(),
	   tp.*
From (SELECT DISTINCT 
	current_timestamp,
	1,
	ifnull(mv.MVKE_PROVG, 'Not Set'),
	ifnull(mv.MARA_MSTAV, 'Not Set'),
	ifnull((SELECT TVMST_VMSTB FROM TVMST WHERE TVMST_VMSTA = mv.MVKE_VMSTA), 'Not Set'),
	ifnull(MVKE_VMSTA, 'Not Set'),
	mv.MVKE_VMSTD,
	ifnull(mv.MVKE_DWERK, 'Not Set'),
	ifnull(mv.MVKE_SCMNG, 0.000),
	ifnull((SELECT TVTWT_VTEXT FROM TVTWT WHERE TVTWT_VTWEG = mv.MVKE_VTWEG), 'Not Set'),
	ifnull(mv.MVKE_VTWEG, 'Not Set'),
	ifnull(mv.AFSMVKE_J_3ASIZE,  'Not Set'),
	ifnull(mv.MVKE_MVGR1, 'Not Set'),
	ifnull(mv.MVKE_MVGR2, 'Not Set'),
	ifnull(mv.MVKE_MVGR3, 'Not Set'),
	ifnull(mv.MVKE_MVGR4, 'Not Set'),
	ifnull(mv.MVKE_MVGR5, 'Not Set'),
	ifnull((SELECT TVM1T_BEZEI FROM TVM1T t1 WHERE t1.TVM1T_MVGR1 =  mv.MVKE_MVGR1),'Not Set'),
	ifnull((SELECT TVM2T_BEZEI FROM TVM2T t2 WHERE t2.TVM2T_MVGR2 =  mv.MVKE_MVGR2),'Not Set'),
	ifnull((SELECT TVM3T_BEZEI FROM TVM3T t3 WHERE t3.TVM3T_MVGR3 =  mv.MVKE_MVGR3),'Not Set'),
	ifnull((SELECT TVM4T_BEZEI FROM TVM4T t4 WHERE t4.TVM4T_MVGR4 =  mv.MVKE_MVGR4),'Not Set'),
	ifnull((SELECT TVM5T_BEZEI FROM TVM5T t5 WHERE t5.TVM5T_MVGR5 =  mv.MVKE_MVGR5),'Not Set'),
	ifnull(mv.MVKE_KONDM, 'Not Set'),
	ifnull(mv.AFSMVKE_J_3APQTY,0.000),
	mv.MARA_MATNR,
	ifnull(mv.MVKE_J_3APIND, 'Not Set'),
	ifnull(mv.AFSMVKE_AFS_REFGEND, 'Not Set'),
	ifnull(mv.AFSMVKE_AFS_REFPQ, 'Not Set'),
	ifnull(mv.AFSMVKE_AFS_REFVMS, 'Not Set'),
	ifnull(mv.AFSMVKE_J_4KRCAT, 'Not Set'),
	ifnull(mv.MVKE_RDPRF, 'Not Set'),
	ifnull((SELECT J_3AGRHD_J_3APGNR FROM KOTJ010, J_3AGRHD WHERE KOTJ010_VTWEG = mv.MVKE_VTWEG AND KOTJ010_VKORG = mv.MVKE_VKORG AND KOTJ010_MATNR = mv.MARA_MATNR AND J_3AGRHD_KNUMH = KOTJ010_KNUMH),'Not Set'),
	ifnull((SELECT TVKOT_VTEXT FROM TVKOT  WHERE TVKOT_VKORG = mv.MVKE_VKORG), 'Not Set'),
	ifnull(mv.MVKE_VKORG, 'Not Set'),
	ifnull(mv.AFSMVKE_J_3ACASE, 'Not Set'),
	ifnull(mv.AFSMVKE_J_3AGEND, 'Not Set'),
	ifnull(mv.AFSMVKE_J_3APQUN, 'Not Set'),
	ifnull(mv.MVKE_J_3AVASG, 'Not Set'),
	ifnull(mv.MVKE_BONUS, 'Not Set')
FROM    MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
	INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG, tmp_MARAMVKEMAKT_ins_mat p
WHERE mv.MARA_MATNR = p.MARA_MATNR
      AND mv.MVKE_VKORG = p.MVKE_VKORG
      AND mv.MVKE_VTWEG = p.MVKE_VTWEG
      AND mv.MAKT_SPRAS = 'E') tp;

UPDATE dim_materialsale p
	SET  
p.PartNumber_NoLeadZero= ifnull(case when length(p.partnumber)=18 and p.partnumber is integer then trim(leading '0' from p.partnumber) else p.partnumber end,'Not Set');

call vectorwise (combine 'tmp_MARAMVKEMAKT_ins_mat-tmp_MARAMVKEMAKT_ins_mat');
call vectorwise (combine 'tmp_MARAMVKEMAKT_del_mat-tmp_MARAMVKEMAKT_del_mat');
call vectorwise (combine 'dim_materialsale')