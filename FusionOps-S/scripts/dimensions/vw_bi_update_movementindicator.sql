UPDATE    dim_movementindicator mi
       FROM
          DD07T t
   SET mi.Description = ifnull(t.DD07T_DDTEXT, 'Not Set')
 WHERE mi.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'KZBEW'
          AND mi.TypeCode = ifnull(t.DD07T_DOMVALUE, 'Not Set')
;