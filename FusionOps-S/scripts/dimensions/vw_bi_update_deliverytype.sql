UPDATE dim_deliverytype a FROM TVLKT
   SET Description = ifnull(TVLKT_VTEXT, 'Not Set')
 WHERE a.deliverytype = TVLKT_LFART AND RowIsCurrent = 1;

