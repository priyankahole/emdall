

UPDATE    dim_OrderType o
       FROM
          DD07T t
   SET o.Description = ifnull(t.DD07T_DDTEXT, 'Not Set')
 WHERE o.RowIsCurrent = 1
  AND t.DD07T_DOMNAME = 'PAART'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND o.OrderTypeCode = t.DD07T_DOMVALUE;
