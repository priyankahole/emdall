UPDATE    dim_costcenter cc
       FROM
          CSKT ck      
   SET cc.Name = ifnull(CSKT_KTEXT, 'Not Set')
 WHERE cc.RowIsCurrent = 1   
 AND  ck.CSKT_KOSTL = cc.Code
          AND cc.ControllingArea = ifnull(CSKT_KOKRS, 'Not Set')
          AND cc.ValidTo = CSKT_DATBI
;