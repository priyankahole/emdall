UPDATE    dim_consumptiontype ct
       FROM
          DD07T dt
   SET ct.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE ct.RowIsCurrent = 1
 AND   DD07T_DOMNAME = 'KZVBR'
          AND DD07T_DOMVALUE IS NOT NULL
          AND ct.ConsumptionCode = dt.DD07T_DOMVALUE
 ;