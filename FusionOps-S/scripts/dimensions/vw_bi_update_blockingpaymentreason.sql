UPDATE    dim_blockingpaymentreason bpr
       FROM
          T008T t
   SET bpr.ExplainReasonForPaymentBlock = ifnull(T008T_TEXTL, 'Not Set')		  
       WHERE t.T008T_ZAHLS = bpr.BlockingKeyPayment AND bpr.RowIsCurrent = 1;