
UPDATE dim_MaterialPriceGroup4 a FROM TVM4T
   SET a.Description = ifnull(TVM4T_BEZEI, 'Not Set')
 WHERE a.MaterialPriceGroup4 = TVM4T_MVGR4 AND RowIsCurrent = 1;
