
INSERT INTO dim_deliverypriority(dim_deliverypriorityid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_deliverypriority
               WHERE dim_deliverypriorityid = 1);

UPDATE dim_DeliveryPriority dp
  FROM tprit t
SET dp.Description = ifnull(t.BEZEI,'Not Set'),
			dp.dw_update_date = current_timestamp
WHERE dp.DeliveryPriority = t.LPRIO
 AND dp.RowIsCurrent = 1;

drop table if exists tmpTPRIT_ins;
create table tmpTPRIT_ins as 
select cast(LPRIO as integer) as LPRIO from TPRIT;

drop table if exists tmpTPRIT_del;
create table tmpTPRIT_del as 
select a.deliverypriority as LPRIO from dim_deliverypriority a;

call vectorwise (combine 'tmpTPRIT_ins-tmpTPRIT_del');

delete from number_fountain m where m.table_name = 'dim_deliverypriority';

insert into number_fountain
select 	'dim_deliverypriority',
	ifnull(max(d.dim_deliverypriorityid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_deliverypriority d
where d.dim_deliverypriorityid <> 1;

INSERT INTO dim_deliverypriority(dim_deliverypriorityId,
                              deliverypriority,
                              Description,
                              RowStartDate,
                              RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_deliverypriority')
          + row_number() over() ,
                        t.LPRIO,
          ifnull(BEZEI, 'Not Set'),
          current_timestamp,
          1
     FROM TPRIT t, tmpTPRIT_ins i
    WHERE SPRAS = 'E' AND t.LPRIO = i.LPRIO;

delete from number_fountain m where m.table_name = 'dim_deliverypriority';

call vectorwise(combine 'tmpTPRIT_ins-tmpTPRIT_ins');
call vectorwise(combine 'tmpTPRIT_del-tmpTPRIT_del');
