/* ##################################################################################################################

   Script         : bi_populate_so_itemstatus_dim
   Author         : Ashu
   Created On     : 17 Jan 2013


   Description    : Stored Proc bi_populate_so_itemstatus_dim from MySQL to Vectorwise syntax

   Change History
   Date            By        Version           Desc
   17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
   7 July 2014     George    1.1               Added GeneralIncompletionStatusCode
   7 Aug  2015     Cristina  1.2               Added the population script of dimension from Shipments process
#################################################################################################################### */

INSERT INTO dim_salesorderitemstatus(
          dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
		  ItemDeliveryBlockStatus
          )
		  
  SELECT 1,
          'Not Set',
          0,
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set'
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorderitemstatus
               WHERE dim_salesorderitemstatusid = 1);		  

 DELETE FROM VBUP
 WHERE NOT EXISTS
              (SELECT 1
                 FROM VBAK_VBAP
                WHERE VBUP_VBELN = VBAP_VBELN AND VBUP_POSNR = VBAP_POSNR)
       AND NOT EXISTS
                  (SELECT 1
                     FROM fact_salesorderlife
                    WHERE dd_SalesDocNo = VBUP_VBELN
                          AND dd_SalesItemNo = VBUP_POSNR);

    
UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET OverallReferenceStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA
AND OverallReferenceStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET ConfirmationStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA
AND  ConfirmationStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET DeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA
AND DeliveryStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET OverallDeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA
AND OverallDeliveryStatus <>  ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET BillingStatusDeliveryRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA
AND BillingStatusDeliveryRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET BillingStatusOrderRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA
AND  BillingStatusOrderRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET ItemRejectionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA
AND ItemRejectionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET OverallProcessingStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA
AND OverallProcessingStatus <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET GeneralIncompletionStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL
AND  GeneralIncompletionStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET DelayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA
AND DelayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET PODStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA
AND  PODStatus  <> ifnull( t.DD07T_DDTEXT ,'Not Set') ;

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET GoodsMovementStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA
AND  GoodsMovementStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
SET PickingPutawayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA
AND PickingPutawayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus from VBUP,dd07t t
Set ItemDeliveryBlockStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
Where SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LSSTA
AND  ItemDeliveryBlockStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus FROM VBUP, DD07T t
   SET    ItemDataforDeliv = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK
  AND ItemDataforDeliv <> ifnull(t.DD07T_DDTEXT,'Not Set');
  
UPDATE dim_salesorderitemstatus FROM VBUP, DD07T t
   SET    GeneralIncompletionStatusCode = ifnull(t.DD07T_DOMVALUE,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL
  AND  GeneralIncompletionStatusCode <> ifnull(t.DD07T_DOMVALUE ,'Not Set');
  
UPDATE dim_salesorderitemstatus FROM VBUP, TVBST t
   SET    GeneralIncompletionProcessingStatusOfItem = ifnull(t.TVBST_BEZEI,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND varchar(VBUP_UVALL,1) = varchar(t.TVBST_STATU,1) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'
  AND  GeneralIncompletionProcessingStatusOfItem <> ifnull(t.TVBST_BEZEI ,'Not Set');

  
delete from number_fountain m where m.table_name = 'dim_salesorderitemstatus';

insert into number_fountain
select 	'dim_salesorderitemstatus',
	ifnull(max(d.dim_salesorderitemstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemstatus d
where d.dim_salesorderitemstatusid <> 1; 
  
INSERT INTO dim_salesorderitemstatus(
          dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
	  ItemDeliveryBlockStatus,
	  ItemDataforDeliv,
	  GeneralIncompletionStatusCode,
	  GeneralIncompletionProcessingStatusOfItem
          )
  SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by dtbl1.VBUP_VBELN,dtbl1.VBUP_POSNR)),dtbl1.*
 FROM (SELECT  distinct
          VBUP_VBELN,
          VBUP_POSNR,
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LSSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK),'Not Set'),
	  ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
	  ifnull((select t.TVBST_BEZEI from TVBST t where varchar(VBUP_UVALL,1) = varchar(t.TVBST_STATU,1) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'),'Not Set')
FROM VBUP inner join VBAK_VBAP_VBEP on VBAK_VBELN = VBUP_VBELN and VBAP_POSNR = VBUP_POSNR
WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR)) dtbl1;

delete from number_fountain m where m.table_name = 'dim_salesorderitemstatus';

insert into number_fountain
select 	'dim_salesorderitemstatus',
	ifnull(max(d.dim_salesorderitemstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemstatus d
where d.dim_salesorderitemstatusid <> 1; 

      
  INSERT
  INTO dim_salesorderitemstatus(
	  dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
	  ItemDataforDeliv,
	  GeneralIncompletionStatusCode,
	  GeneralIncompletionProcessingStatusOfItem
          )
  SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by dtbl1.VBUP_VBELN,dtbl1.VBUP_POSNR)),dtbl1.*
 FROM (SELECT  distinct
          VBUP_VBELN,
          VBUP_POSNR,
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LSSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK),'Not Set'),
	  ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
	  ifnull((select t.TVBST_BEZEI from TVBST t where varchar(VBUP_UVALL,1) = varchar(t.TVBST_STATU,1) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'),'Not Set')
  FROM VBUP inner join VBAK_VBAP on VBAP_VBELN = VBUP_VBELN and VBAP_POSNR = VBUP_POSNR
  WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR)) dtbl1;

  drop table if exists tmp_del1_LIKP_LIPS_VBUK;
create table tmp_del1_LIKP_LIPS_VBUK
as
select distinct LIKP_VBELN,LIPS_POSNR from LIKP_LIPS
union
select distinct dd_SalesDlvrDocNo,dd_SalesDlvrItemNo from fact_salesorderlife;

  delete from LIKP_LIPS_VBUP
  where not exists (select 1 from tmp_del1_LIKP_LIPS_VBUK where VBUP_VBELN = LIKP_VBELN and VBUP_POSNR = LIPS_POSNR);

  UPDATE dim_salesorderitemstatus
	FROM LIKP_LIPS_VBUP, dd07t t
   SET OverallReferenceStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA
  AND  OverallReferenceStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

  UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, dd07t t
   SET ConfirmationStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA
  AND ConfirmationStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

    UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, dd07t t 
   SET DeliveryStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA
  AND  DeliveryStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

  UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, dd07t t
   SET OverallDeliveryStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA
  AND  OverallDeliveryStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

    UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07t t
   SET      BillingStatusDeliveryRelated = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA
  AND BillingStatusDeliveryRelated <> ifnull(t.DD07T_DDTEXT,'Not Set');

       UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET   BillingStatusOrderRelated = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA
  AND BillingStatusOrderRelated <> ifnull(t.DD07T_DDTEXT,'Not Set');

      UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET    ItemRejectionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA
  AND ItemRejectionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

      UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET    OverallProcessingStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA
  AND OverallProcessingStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

      UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET    GeneralIncompletionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL
  AND GeneralIncompletionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

       UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET   DelayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA
  AND DelayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

       UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET   PODStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA
  AND PODStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

      UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET    GoodsMovementStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA
  AND GoodsMovementStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

      UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET    PickingPutawayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA
  AND PickingPutawayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');
  
  UPDATE dim_salesorderitemstatus
        FROM LIKP_LIPS_VBUP, DD07T t
   SET    ItemDataforDeliv = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK
  AND ItemDataforDeliv <> ifnull(t.DD07T_DDTEXT,'Not Set');

delete from number_fountain m where m.table_name = 'dim_salesorderitemstatus';

insert into number_fountain
select 	'dim_salesorderitemstatus',
	ifnull(max(d.dim_salesorderitemstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemstatus d
where d.dim_salesorderitemstatusid <> 1; 

  INSERT
  INTO dim_salesorderitemstatus(
	  dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
		  ItemDataforDeliv
          )
   SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by dtbl1.VBUP_VBELN,dtbl1.VBUP_POSNR)),dtbl1.*
 FROM (SELECT  distinct
          VBUP_VBELN,
          VBUP_POSNR,
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LSSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK),'Not Set')
  FROM LIKP_LIPS_VBUP inner join LIKP_LIPS on LIKP_VBELN = VBUP_VBELN and LIPS_POSNR = VBUP_POSNR
  WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR)) dtbl1;