

insert into dim_afssize(Dim_AfsSizeId,Size,Material,EAN_UPC,EAN_category, MAIN_EAN,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason) 
SELECT 1,'Not Set','Not Set','Not Set','Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) as a
where not exists(select 1 from dim_afssize where dim_afssizeId = 1);


delete from number_fountain m where m.table_name = 'dim_afssize';

insert into number_fountain
select 	'dim_afssize',
	ifnull(max(d.dim_afssizeId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_afssize d
where d.dim_afssizeId <> 1; 
 
INSERT INTO dim_afssize(dim_afssizeId,
			  Size,
			  Material,
			  EAN_UPC,
			  EAN_category,
			  MAIN_EAN,
              RowStartDate,
              RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_afssize') 
          + row_number() over(),
			MEAN_J_3AKORDX,
			MEAN_MATNR,
			MEAN_EAN11,
			ifnull(MEAN_EANTP,'Not Set'),
			ifnull(MEAN_HPEAN,'Not Set'),
			current_timestamp,
			1
       FROM MEAN
      WHERE MEAN_J_3AKORDX IS NOT NULL
	    AND NOT EXISTS
                  (SELECT 1
                     FROM dim_afssize
                    WHERE Size = MEAN_J_3AKORDX
					 AND Material = MEAN_MATNR
					 AND EAN_UPC = MEAN_EAN11
                     AND EAN_category = MEAN_EANTP
					 AND MAIN_EAN = MEAN_HPEAN);


CALL vectorwise(combine 'dim_AfsSize');

delete from number_fountain m where m.table_name = 'dim_afssize';
