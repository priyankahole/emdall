insert into dim_processingmessagestatus
(dim_processingmessagestatusid, processingmessage, description, rowstartdate, rowenddate, rowiscurrent, rowchangereason) 
SELECT 1,'Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_processingmessagestatus where dim_processingmessagestatusid = 1);

UPDATE    dim_processingmessagestatus pm
       FROM
          DD07T t
	SET pm.Description = t.DD07T_DDTEXT,
			pm.dw_update_date = current_timestamp
 WHERE pm.RowIsCurrent = 1
 AND  t.DD07T_DOMNAME = 'NA_VSTAT'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND pm.ProcessingMessage = t.DD07T_DOMVALUE;

delete from number_fountain m where m.table_name = 'dim_processingmessagestatus';

insert into number_fountain
select 	'dim_processingmessagestatus',
	ifnull(max(d.dim_processingmessagestatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_processingmessagestatus d
where d.dim_processingmessagestatusid <> 1;

INSERT INTO dim_processingmessagestatus(Dim_ProcessingMessageStatusid,
							ProcessingMessage,
                            Description,
                            RowStartDate,
                            RowIsCurrent)
   SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_processingmessagestatus')) 
          + row_number() over() ,
			t.DD07T_DOMVALUE,
          t.DD07T_DDTEXT,
          current_timestamp,
          1
     FROM DD07T t
    WHERE t.DD07T_DOMNAME = 'NA_VSTAT' AND t.DD07T_DOMVALUE IS NOT NULL
          AND NOT EXISTS
                     (SELECT 1
                        FROM dim_processingmessagestatus s
                       WHERE     s.ProcessingMessage = t.DD07T_DOMVALUE
                             AND t.DD07T_DOMNAME = 'NA_VSTAT'
                             AND s.RowIsCurrent = 1);

CALL vectorwise(combine 'dim_processingmessagestatus');

delete from number_fountain m where m.table_name = 'dim_processingmessagestatus';
