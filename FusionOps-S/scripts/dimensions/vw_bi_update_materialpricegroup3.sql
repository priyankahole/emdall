
UPDATE dim_MaterialPriceGroup3 a FROM TVM3T
   SET a.Description = ifnull(TVM3T_BEZEI, 'Not Set')
 WHERE a.MaterialPriceGroup3 = TVM3T_MVGR3 AND RowIsCurrent = 1;
