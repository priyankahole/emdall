UPDATE   dim_allocationreleaserule d1
 FROM j_3aresg_t t
SET d1.Description = ifnull(t.J_3ARESG_T_BEZEI,'Not Set')
WHERE d1.RowIsCurrent = 1 AND d1.ReleaseRule  = t.J_3ARESG_T_J_3ARESGY;