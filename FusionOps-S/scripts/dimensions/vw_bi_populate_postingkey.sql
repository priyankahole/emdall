UPDATE    dim_postingkey pk
       FROM
          TBSLT t
   SET pk.Name = t.TBSLT_LTEXT,
			pk.dw_update_date = current_timestamp 
 WHERE pk.RowIsCurrent = 1
   AND t.TBSLT_SPRAS = 'E'
          AND pk.PostingKey = t.TBSLT_BSCHL
          AND pk.SpecialGLIndicator = ifnull(t.TBSLT_UMSKZ, 'Not Set')   
;

INSERT INTO dim_postingkey(dim_postingkeyId, RowIsCurrent,rowstartdate,NAME)
SELECT 1, 1,current_timestamp,'Not Set'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_postingkey
               WHERE dim_postingkeyId = 1);

delete from number_fountain m where m.table_name = 'dim_postingkey';
   
insert into number_fountain
select 	'dim_postingkey',
	ifnull(max(d.Dim_PostingKeyId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_postingkey d
where d.Dim_PostingKeyId <> 1; 

INSERT INTO dim_postingkey(Dim_PostingKeyId,
							PostingKey,
                           SpecialGLIndicator,
                           Name,
                           RowStartDate,
                           RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_postingkey')
         + row_number() over(),
			  TBSLT_BSCHL,
          ifnull(TBSLT_UMSKZ,'Not Set'),
          ifnull(TBSLT_LTEXT,'Not Set'),
          current_timestamp,
          1
     FROM TBSLT t
    WHERE t.TBSLT_SPRAS = 'E'
          AND NOT EXISTS
                     (SELECT 1
                        FROM dim_postingkey pk
                       WHERE     pk.PostingKey = t.TBSLT_BSCHL
                             AND pk.SpecialGLIndicator = ifnull(t.TBSLT_UMSKZ,'Not Set')
                             AND pk.RowIsCurrent = 1) 
;

