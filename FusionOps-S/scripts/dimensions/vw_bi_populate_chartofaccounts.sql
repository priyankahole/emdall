
INSERT INTO dim_chartofaccounts(dim_chartofaccountsId, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_chartofaccounts
               WHERE dim_chartofaccountsId = 1);
			   
UPDATE    dim_chartofaccounts coa
       FROM
          SKAT s      
   SET coa.ShortText = s.SKAT_TXT20, coa.Description = s.SKAT_TXT50,
			coa.dw_update_date = current_timestamp
 WHERE coa.RowIsCurrent = 1
 AND  coa.CharOfAccounts = s.SKAT_KTOPL
          AND coa.GLAccountNumber = s.SKAT_SAKNR
          AND s.SKAT_SPRAS = 'E';
		  
delete from number_fountain m where m.table_name = 'dim_chartofaccounts';

insert into number_fountain
select 	'dim_chartofaccounts',
	ifnull(max(d.dim_chartofaccountsId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_chartofaccounts d
where d.dim_chartofaccountsId <> 1;


INSERT INTO dim_chartofaccounts(Dim_ChartOfAccountsId,
								CharOfAccounts,
                                GLAccountNumber,
                                ShortText,
                                Description,
                                RowStartDate,
                                RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_chartofaccounts') 
          + row_number() over(),
			SKAT_KTOPL,
          SKAT_SAKNR,
          SKAT_TXT20,
          SKAT_TXT50,
          current_timestamp,
          1
     FROM SKAT s
    WHERE s.SKAT_SPRAS = 'E'
          AND NOT EXISTS
                     (SELECT 1
                        FROM dim_chartofaccounts ca
                       WHERE     ca.CharOfAccounts = s.SKAT_KTOPL
                             AND ca.GLAccountNumber = s.SKAT_SAKNR
                             AND ca.RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_chartofaccounts';
							 