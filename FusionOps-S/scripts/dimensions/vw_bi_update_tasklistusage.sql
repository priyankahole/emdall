UPDATE    dim_tasklistusage u
       FROM
          T411T t
   SET u.UsageName = t.T411T_TXT
 WHERE u.RowIsCurrent = 1
 AND  u.UsageCode = t.T411T_VERWE
;