UPDATE    dim_bomitemcategory bic
       FROM
          T418T t
       SET bic.ItemCatText = ifnull(t.T418T_PTEXT, 'Not Set')
	   WHERE bic.ItemCategory = t.T418T_POSTP AND bic.RowIsCurrent = 1;
