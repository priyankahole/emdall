
INSERT INTO Dim_Currency(Dim_CurrencyId )
SELECT 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM Dim_Currency
               WHERE Dim_CurrencyId = 1);
			   
UPDATE    Dim_Currency dc
       FROM
          tcurt c
   SET dc.Currency = ifnull(ifnull(c.KTEXT, c.ltext), 'Not Set'),
			dc.dw_update_date = current_timestamp
   WHERE c.WAERS = dc.CurrencyCode   ;
   
delete from number_fountain m where m.table_name = 'Dim_Currency';

insert into number_fountain
select 	'Dim_Currency',
	ifnull(max(d.Dim_CurrencyId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_Currency d
where d.Dim_CurrencyId <> 1;

INSERT INTO Dim_Currency(Dim_Currencyid,CurrencyCode, Currency)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_Currency') 
          + row_number() over(),
			waers, ifnull(ifnull(c.KTEXT, c.ltext), 'Not Set')
     FROM tcurt c
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_currency dc
                       WHERE dc.CurrencyCode = c.waers);

delete from number_fountain m where m.table_name = 'Dim_Currency';
					   