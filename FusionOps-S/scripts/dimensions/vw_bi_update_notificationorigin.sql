UPDATE    dim_notificationorigin no
       FROM
          DD07T t
   SET no.NotificationOriginName = t.DD07T_DDTEXT
 WHERE no.RowIsCurrent = 1
       AND t.DD07T_DOMNAME = 'HERKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND no.NotificationOriginCode = t.DD07T_DOMVALUE 
;