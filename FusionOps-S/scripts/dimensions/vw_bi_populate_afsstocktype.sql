insert into dim_afsstocktype(Dim_AfsStockTypeId,StockType,StockTypeDescription,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason) 
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from dim_afsstocktype where dim_afsstocktypeid = 1);
               
UPDATE    dim_afsstocktype ast
       FROM
          DD07T dt
   SET ast.StockTypeDescription = ifnull(DD07T_DDTEXT, 'Not Set'),
		ast.dw_update_date = current_timestamp
 WHERE ast.RowIsCurrent = 1
 AND  dt.DD07T_DOMNAME = 'J_3ABSKZ'
          AND dt.DD07T_DOMVALUE IS NOT NULL
          AND ast.StockType = dt.DD07T_DOMVALUE;

delete from number_fountain m where m.table_name = 'dim_afsstocktype';

insert into number_fountain
select 	'dim_afsstocktype',
	ifnull(max(d.Dim_AfsStockTypeId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_afsstocktype d
where d.Dim_AfsStockTypeId <> 1;
		  
INSERT INTO dim_AfsStockType(Dim_AfsStockTypeid,
							  StockType,
                                                          StockTypeDescription,
                                                          RowStartDate,
                                                          RowIsCurrent)
   SELECT distinct (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_afsstocktype') 
          + row_number() over() ,
			DD07T_DOMVALUE,
	ifnull(DD07T_DDTEXT, 'Not Set'),
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'J_3ABSKZ' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM Dim_AfsStockType
                    WHERE StockType = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'J_3ABSKZ');

delete from number_fountain m where m.table_name = 'dim_afsstocktype';						  