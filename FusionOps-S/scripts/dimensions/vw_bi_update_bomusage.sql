
UPDATE    dim_bomusage bu
       FROM
          T416T t
   SET bu.Description = ifnull(t.T416T_ANTXT, 'Not Set')
 WHERE bu.RowIsCurrent = 1
 AND bu.BOMUsageCode = t.T416T_STLAN;
