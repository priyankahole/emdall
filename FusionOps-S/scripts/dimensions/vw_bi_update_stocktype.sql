UPDATE    dim_stocktype s
       FROM
          DD07T t
   SET s.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE s.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'INSMK'
          AND s.TypeCode = ifnull(t.DD07T_DOMVALUE, 'Not Set')
;
