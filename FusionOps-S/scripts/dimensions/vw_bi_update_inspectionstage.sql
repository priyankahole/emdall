
UPDATE    dim_inspectionstage s
       FROM
          QDPST q
   SET s.InspectionStageName = q.QDPST_KURZTEXT
 WHERE s.RowIsCurrent = 1
      AND s.InspectionStageCode = q.QDPST_PRSTUFE
          AND s.InspectionStageRuleCode = q.QDPST_DYNREGEL;
