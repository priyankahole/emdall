UPDATE    Dim_Currency dc
       FROM
          tcurt c
   SET dc.Currency = ifnull(ifnull(c.KTEXT, c.ltext), 'Not Set')
   WHERE c.WAERS = dc.CurrencyCode   
;