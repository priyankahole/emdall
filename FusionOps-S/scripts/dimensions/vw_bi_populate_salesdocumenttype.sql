INSERT INTO dim_Salesdocumenttype(dim_Salesdocumenttypeid,
                             DocumentType,
                             Description)
   SELECT 1,
          'Not Set',
          'Not Set'		  
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_Salesdocumenttype
               WHERE dim_Salesdocumenttypeid = 1);

UPDATE    dim_Salesdocumenttype a
       FROM
          TVAKT t
   SET a.Description = ifnull(TVAKT_BEZEI, 'Not Set'),
			a.dw_update_date = current_timestamp
WHERE t.TVAKT_AUART IS NOT NULL AND a.DocumentType = t.TVAKT_AUART;

delete from number_fountain m where m.table_name = 'dim_salesdocumenttype';

insert into number_fountain
select 	'dim_salesdocumenttype',
	ifnull(max(d.Dim_SalesDocumentTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesdocumenttype d
where d.Dim_SalesDocumentTypeid <> 1; 

insert into dim_salesdocumenttype
(	Dim_SalesDocumentTypeid,
	DocumentType,
	Description
)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesdocumenttype') 
          + row_number() over(),
			  TVAKT_AUART,
	ifnull(TVAKT_BEZEI,'Not Set')
from TVAKT
where TVAKT_AUART is not null
	and not exists (select 1 from dim_salesdocumenttype a where a.DocumentType = TVAKT_AUART);
