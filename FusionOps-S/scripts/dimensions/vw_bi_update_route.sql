UPDATE    dim_route r
       FROM
          TVROT t
   SET r.RouteName = t.TVROT_BEZEI,
       r.TransitTime = t.TVRO_TRAZT,
       r.TransitTimeInCalendarDays = t.TVRO_TRAZTD,
       r.TransportLeadTimeInCalendarDays = t.TVRO_TDVZTD
 WHERE r.RowIsCurrent = 1
 AND r.RouteCode = t.TVROT_ROUTE
;
