UPDATE    dim_SpecialStock st
       FROM
          t148t t
   SET st.Description = t.T148T_SOTXT,
			st.dw_update_date = current_timestamp
 WHERE st.RowIsCurrent = 1
  AND st.specialstockindicator = t.T148T_SOBKZ
;

INSERT INTO dim_specialstock(dim_specialstockId, RowIsCurrent, Description)
SELECT 1, 1 , 'Not Set'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_specialstock
               WHERE dim_specialstockId = 1);

delete from number_fountain m where m.table_name = 'dim_specialstock';

insert into number_fountain
select 	'dim_specialstock',
	ifnull(max(d.dim_specialstockid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_specialstock d
where d.dim_specialstockid <> 1;

INSERT INTO dim_specialstock(dim_specialstockid,
                             specialstockindicator,
                             description,
                             RowStartDate,
                             RowIsCurrent)
        SELECT distinct (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_specialstock') 
          + row_number() over(),
			  t.T148T_SOBKZ,
          t.T148T_SOTXT,
          current_timestamp,
          1
     FROM t148t t
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_specialstock st
                       WHERE st.specialstockindicator = t.T148T_SOBKZ)
;

