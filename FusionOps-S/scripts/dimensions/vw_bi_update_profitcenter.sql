UPDATE    dim_profitcenter pc
       FROM
          CEPCT c
   SET pc.ProfitCenterName = ifnull(CEPCT_KTEXT, 'Not Set'),
       pc.ControllingArea = ifnull(CEPCT_KOKRS, 'Not Set'),
       pc.ValidTo = CEPCT_DATBI
 WHERE pc.RowIsCurrent = 1
 AND c.CEPCT_PRCTR = pc.ProfitCenterCode
;
