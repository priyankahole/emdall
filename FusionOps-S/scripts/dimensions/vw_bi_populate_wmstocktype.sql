/* 	Server: QA
	Process Name: WM Stock Type Transfer
	Interface No: 2
*/
INSERT INTO dim_wmstocktype
(dim_wmstocktypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmstocktype where dim_wmstocktypeid = 1);

UPDATE    dim_wmstocktype wmst
       FROM
          T064B t
   SET  wmst.StockTypeDescription = t.T064B_BTEXT,
			wmst.dw_update_date = current_timestamp
 WHERE wmst.RowIsCurrent = 1
     AND wmst.StockType = t.T064B_BSTAR;
 
delete from number_fountain m where m.table_name = 'dim_wmstocktype';

insert into number_fountain				   
select 	'dim_wmstocktype',
	ifnull(max(d.dim_wmstocktypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmstocktype d
where d.dim_wmstocktypeid <> 1;

INSERT INTO dim_wmstocktype(dim_wmstocktypeid,
							StockType,
                            StockTypeDescription,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmstocktype') + row_number() over(),
		  t.T064B_BSTAR,
		  t.T064B_BTEXT,
          current_timestamp,
          1
     FROM T064B t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_wmstocktype s
                  WHERE    s.StockType = t.T064B_BSTAR
					     AND s.RowIsCurrent = 1) ;
