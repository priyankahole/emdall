/* ##################################################################################################################*/
/*	Script         : vw_bi_populate_ordercategory																		*/
/*     Created By     : Amar															*/
/*     Created On     : 12 March 2015													*/
/*     Change History																	*/
/*     Date            By        Version           Desc									*/
/*	 3/12/2015		 Amar	   1				 Multi-Statement changed to Script		*/
/*#################################################################################################################### */


INSERT INTO dim_ordercategory(Dim_ordercategoryid,
								Description,
                               ordercategory,
                               RowStartDate,
                               RowIsCurrent)

SELECT 			1,
				'Not Set',
				'Not Set',
				current_timestamp,
				1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_ordercategory WHERE dim_ordercategoryId = 1);

							   
DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_ordercategory';

INSERT INTO NUMBER_FOUNTAIN
select 	'dim_ordercategory',
		ifnull(max(d.dim_ordercategoryId), 
		ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ordercategory d
where d.dim_ordercategoryId <> 1;


									
INSERT INTO dim_ordercategory(Dim_ordercategoryid,
								Description,
                               ordercategory,
                               RowStartDate,
                               RowIsCurrent)
   SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_ordercategory') 
          + row_number() over() ,
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'AUFTYP' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_ordercategory
                    WHERE ordercategory = DD07T_DOMVALUE AND DD07T_DOMNAME = 'AUFTYP')
   ORDER BY 2 OFFSET 0;
   
   DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_ordercategory';


