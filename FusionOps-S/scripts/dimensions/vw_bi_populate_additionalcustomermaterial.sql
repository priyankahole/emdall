INSERT INTO Dim_AdditionalCustomerMaterial(Dim_AdditionalCustomerMaterialId,DistributionChannel,SalesOrg, ConditionRecordNumber,ValidityEndDate,
ValidityStartDate,CustomerNumber,Application,MaterialDetType,MaterialBelongingToCustomer,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason )
SELECT 1,'Not Set','Not Set','Not Set','1900-01-01','1900-01-01','Not Set','Not Set','Not Set','Not Set',CURRENT_TIMESTAMP,NULL,1,NULL
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM Dim_AdditionalCustomerMaterial WHERE Dim_AdditionalCustomerMaterialId = 1);
   

delete from number_fountain m where m.table_name = 'Dim_AdditionalCustomerMaterial';

insert into number_fountain
select 	'Dim_AdditionalCustomerMaterial',
	ifnull(max(d.Dim_AdditionalCustomerMaterialid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_AdditionalCustomerMaterial d
where d.Dim_AdditionalCustomerMaterialid <> 1;

INSERT INTO Dim_AdditionalCustomerMaterial(
			Dim_AdditionalCustomerMaterialId,
			DistributionChannel,
                        SalesOrg,
                        ConditionRecordNumber,
                        ValidityEndDate,
                        ValidityStartDate,
			CustomerNumber,
			Application,
			MaterialDetType,
			MaterialBelongingToCustomer,
                        RowStartDate,
                        RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_AdditionalCustomerMaterial') + row_number() over() as Dim_AdditionalCustomerMaterialId,
   		KOTD498_VTWEG,
          	KOTD498_VKORG,
          	KOTD498_KNUMH,
          	CAST(KOTD498_DATBI AS DATE),
          	CAST(KOTD498_DATAB AS DATE),
		KOTD498_KUNNR,
		KOTD498_KAPPL,
		KOTD498_KSCHL,
		KOTD498_KDMAT,
          	current_timestamp,
          	1
   FROM KOTD498 K
         WHERE NOT EXISTS
                 (SELECT 1
                    FROM Dim_AdditionalCustomerMaterial
                    	WHERE DistributionChannel = KOTD498_VTWEG
                         AND SalesOrg = KOTD498_VKORG
                         AND ConditionRecordNumber = KOTD498_KNUMH
                         AND CustomerNumber = KOTD498_KUNNR
                         AND Application = KOTD498_KAPPL
			 AND MaterialDetType = KOTD498_KSCHL
                         AND MaterialBelongingToCustomer = KOTD498_KDMAT
                         AND RowIsCurrent = 1);

                         
                         