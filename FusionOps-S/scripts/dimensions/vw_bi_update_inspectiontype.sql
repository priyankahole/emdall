
UPDATE    dim_inspectiontype it
       FROM
          TQ30T t
   SET it.InspectionTypeName = t.TQ30T_LTXA1,
       it.InspectionTypeText = t.TQ30T_KURZTEXT
 WHERE it.RowIsCurrent = 1
     AND it.InspectionTypeCode = t.TQ30T_ART;
