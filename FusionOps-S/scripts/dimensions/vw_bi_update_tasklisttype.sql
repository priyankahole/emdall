UPDATE    dim_tasklisttype y
       FROM
          TCA02 t
   SET y.Description = ifnull(TCA02_TXT, 'Not Set')
 WHERE y.RowIsCurrent = 1
 AND y.TaskListTypeCode = t.TCA02_PLNTY
;