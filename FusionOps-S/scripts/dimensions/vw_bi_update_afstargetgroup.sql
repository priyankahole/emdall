UPDATE dim_AfsTargetGroup a FROM J_3AGENDRT 
   SET a.AFSTargetGroupDescription = ifnull(J_3AGENDRT_TEXT, 'Not Set')
 WHERE a.AFSTargetGroup = J_3AGENDRT_J_3AGEND AND RowIsCurrent = 1;