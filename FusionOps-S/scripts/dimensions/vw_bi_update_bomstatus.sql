

UPDATE    dim_bomstatus bs
       FROM
          T415T t
   SET bs.Description = ifnull(T415T_STTXT, 'Not Set')
 WHERE bs.RowIsCurrent = 1
 AND bs.bomstatuscode = T415T_STLST;
