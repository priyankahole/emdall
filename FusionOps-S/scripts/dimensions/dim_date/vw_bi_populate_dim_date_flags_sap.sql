/* ##################################################################################################################

   Script         : vw_bi_populate_dim_date_flags_sap.sql 
   Author         : Victor Criclivii
   Created On     : 22 12 2015


   Description    : Dim Date Flags

   Change History
   Date            By        	Version           Desc
   22 12 2015     Victor         1.0              Dim Date Flags
     
#################################################################################################################### */

/*currentperiodname*/
UPDATE dim_date SET currentperiodname = 'Not Set';

UPDATE dim_date d
SET d.currentperiodname = TO_CHAR(current_Date,'MON-YY')
WHERE TO_CHAR(d.datevalue,'MON-YY') =  TO_CHAR(current_Date,'MON-YY');


/*fiscalyearstartflag*/
/*SELECT COUNT(*),companycode FROM dim_date WHERE fiscalyearstartflag ='Y' GROUP BY companycode*/
UPDATE dim_date SET fiscalyearstartflag ='N';

DROP TABLE IF EXISTS dim_date_fiscalyearstartflag_tmp;

CREATE TABLE dim_date_fiscalyearstartflag_tmp AS
SELECT MAX(d.datevalue) datevalue, d.financialweek, d.monthyear, d.financialyear, d.companycode
FROM dim_date d, (SELECT MIN(t.financialweek) financialweek, t.companycode FROM dim_date t GROUP BY t.companycode ) t
WHERE d.financialweek = t.financialweek
  AND t.companycode = d.companycode
GROUP BY d.financialweek, d.monthyear, d.financialyear, d.companycode;


UPDATE dim_date d
SET d.fiscalyearstartflag = 'Y'
FROM dim_date d, dim_date_fiscalyearstartflag_tmp tmp 
WHERE d.datevalue = tmp.datevalue
  AND d.companycode = tmp.companycode;

DROP TABLE IF EXISTS dim_date_fiscalyearstartflag_tmp;

/*fiscalyearendflag*/
/*SELECT COUNT(*),companycode FROM dim_date WHERE fiscalyearendflag ='Y'  GROUP BY companycode*/

UPDATE dim_date SET fiscalyearendflag ='N';

DROP TABLE IF EXISTS dim_date_fiscalyearendflag_tmp;

CREATE TABLE dim_date_fiscalyearendflag_tmp AS
SELECT MAX(d.datevalue) datevalue, d.financialweek, d.monthyear, d.financialyear, d.companycode
FROM dim_date d, (SELECT MAX(t.financialweek) financialweek, t.companycode FROM dim_date t GROUP BY t.companycode ) t
WHERE d.financialweek = t.financialweek
  AND t.companycode = d.companycode
GROUP BY d.financialweek, d.monthyear, d.financialyear, d.companycode;

		          
UPDATE dim_date d
SET d.fiscalyearendflag = 'Y'
FROM dim_date d, dim_date_fiscalyearendflag_tmp tmp 
WHERE d.datevalue = tmp.datevalue
 AND d.companycode = tmp.companycode;

DROP TABLE IF EXISTS dim_date_fiscalyearendflag_tmp;

/*dim_date_tmp*/
DROP TABLE IF EXISTS dim_date_tmp;

CREATE TABLE dim_date_tmp AS 
SELECT dim_dateid, datevalue, financialyear, financialquarter, monthyear, financialweek ,companycode,
FIRST_VALUE(financialweek ) OVER(PARTITION BY financialyear, financialquarter,monthyear 
ORDER BY financialweek DESC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) financialweek_m
FROM dim_date
ORDER BY datevalue,financialweek;

/*dim_date_rank_tmp*/
DROP TABLE IF EXISTS dim_date_rank_tmp;

CREATE TABLE dim_date_rank_tmp AS
SELECT 
dim_dateid, datevalue,
financialyear, financialquarter, monthyear, financialweek ,financialweek_m,companycode,
DENSE_RANK() over(ORDER BY financialyear ASC ) RANK_YEAR,
DENSE_RANK() over(ORDER BY financialyear ASC, financialquarter ASC) RANK_quarter,
DENSE_RANK() over(ORDER BY financialyear ASC, financialweek_m ASC) RANK_month,
DENSE_RANK() over(ORDER BY financialyear ASC, financialweek ASC) RANK_week
FROM dim_date_tmp
ORDER BY datevalue;



/*yearflag*/
/*SELECT DISTINCT yearflag  FROM dim_date WHERE yearflag in ('Current','Previous','Next')*/
UPDATE dim_date SET yearflag ='Not Set';

UPDATE dim_date d
SET d.yearflag = 'Current'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_YEAR, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND d.companycode = t1.companycode
AND t1.RANK_YEAR = t2.RANK_YEAR
AND t1.companycode = t2.companycode;

UPDATE dim_date d
SET d.yearflag = 'Previous'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_YEAR, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND d.companycode = t1.companycode
AND t1.RANK_YEAR + 1 = t2.RANK_YEAR
AND t1.companycode = t2.companycode;

UPDATE dim_date d
SET d.yearflag = 'Next'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_YEAR, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND d.companycode = t1.companycode
AND t1.RANK_YEAR - 1 = t2.RANK_YEAR
AND t1.companycode = t2.companycode;
 
/*quarterflag*/
/*SELECT DISTINCT quarterflag  FROM dim_date*/
UPDATE dim_date
SET quarterflag = 'Not Set';

UPDATE dim_date d
SET d.quarterflag = 'Current'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_quarter, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_quarter = t2.RANK_quarter
AND d.companycode = t1.companycode
AND t1.companycode = t2.companycode;

UPDATE dim_date d
SET d.quarterflag = 'Previous'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_quarter, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_quarter + 1 = t2.RANK_quarter
AND d.companycode = t1.companycode
AND t1.companycode = t2.companycode;

UPDATE dim_date d
SET d.quarterflag = 'Next'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_quarter, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_quarter - 1 = t2.RANK_quarter
AND d.companycode = t1.companycode
AND t1.companycode = t2.companycode;


/*monthflag*/
/*SELECT DISTINCT monthflag  FROM dim_date*/
UPDATE dim_date SET monthflag ='Not Set';

UPDATE dim_date d
SET d.monthflag = 'Current'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_month, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_month = t2.RANK_month
AND t1.companycode = t2.companycode
AND d.companycode = t1.companycode;

UPDATE dim_date d
SET d.monthflag = 'Previous'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_month, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_month + 1 = t2.RANK_month
AND t1.companycode = t2.companycode
AND d.companycode = t1.companycode;

UPDATE dim_date d
SET d.monthflag = 'Next'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_month, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_month - 1 = t2.RANK_month
AND t1.companycode = t2.companycode
AND d.companycode = t1.companycode;
 

/*weekflag*/
/*SELECT  weekflag,companycode,COUNT(*)  FROM dim_date GROUP BY weekflag,companycode */
UPDATE dim_date SET weekflag ='Not Set';

UPDATE dim_date d
SET d.weekflag = 'Current'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_week, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
  AND t1.RANK_week = t2.Rank_week
  AND t1.companycode = t2.companycode
  AND d.companycode = t1.companycode;

UPDATE dim_date d
SET d.weekflag = 'Previous'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_week, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
  AND t1.RANK_week + 1 = t2.Rank_week
  AND t1.companycode = t2.companycode
  AND d.companycode = t1.companycode;


UPDATE dim_date d
SET d.weekflag = 'Next'
FROM dim_date d, dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_week, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2
WHERE d.datevalue = t1.datevalue 
  AND t1.RANK_week - 1 = t2.Rank_week
  AND t1.companycode = t2.companycode
  AND d.companycode = t1.companycode;

DROP TABLE IF EXISTS dim_date_tmp;
DROP TABLE IF EXISTS dim_date_rank_tmp;

/*weekstartflag and weekendflag*/
/*SELECT DISTINCT weekstartflag  FROM dim_date*/

UPDATE dim_date SET weekstartflag = 'N';
UPDATE dim_date SET weekendflag = 'N';

UPDATE dim_date SET weekstartflag = 'Y' WHERE weekstartdate = datevalue;
UPDATE dim_date SET weekendflag = 'Y' WHERE weekenddate = datevalue;


/*dayflag*/
/*SELECT DISTINCT dayflag  FROM dim_date*/
UPDATE dim_date SET dayflag = 'Not Set';

UPDATE dim_date
SET dayflag = 'Current'
WHERE CURRENT_DATE = datevalue;

UPDATE dim_date
SET dayflag = 'Previous'
WHERE CURRENT_DATE - 1 = datevalue;

UPDATE dim_date
SET dayflag = 'Next'
WHERE CURRENT_DATE + 1 = datevalue;