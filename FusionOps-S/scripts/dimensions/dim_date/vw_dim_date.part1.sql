Drop table if exists parameters_d00;

Create table parameters_d00(StartYear integer, NumYears integer);

Insert into parameters_d00 values(1990,60);

Drop table if exists counttbl_d00;

Create table counttbl_d00 (counter integer);

Insert into counttbl_d00 values 
(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),
(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),
(21),(22),(23),(24),(25),(26),(27),(28),(29),(30),
(31),(32),(33),(34),(35),(36),(37),(38),(39),(40),
(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),
(51),(52),(53),(54),(55),(56),(57),(58),(59),(60),
(61),(62),(63),(64),(65),(66),(67),(68),(69),(70),
(71),(72),(73),(74),(75),(76),(77),(78),(79),(80),
(81),(82),(83),(84),(85),(86),(87),(88),(89),(90),
(91),(92),(93),(94),(95),(96),(97),(98),(99),(100);

Drop table if exists companymaster_d00;

Create table companymaster_d00(
StartYear integer null,
NumYears integer null,
pCompanyCode varchar(20) null,
DT timestamp(0) null,
MAXDT timestamp(0) null,
MAXDT_EXISTS INTeger null,
MAXDT_NAME VARCHAR(10) null,
JDATE INTeger null,
CALYEAR INTeger null,
FINYEAR INTeger null,
CALQUARTER INTeger null,
CALQTRID INTeger null,
CALQTRNAME CHAR(8) null,
FINQUARTER INTeger null,
FINQTRID INTeger null,
FINQTRNAME CHAR(13) null,
SEASON CHAR(6) null,
CALMONTHID INTeger null,
FINMONTHID INTeger null,
CALMONTH  INTeger null,
vMONTHNAME  VARCHAR(12) null,
MONTHABBR  VARCHAR(3) null,
FINMONTH  INTeger null,
CALWEEKID  INTeger null,
CALWEEK  INTeger null,
FINWEEKID  INTeger null,
FINWEEK  INTeger null,
FINMONTHSTART INTeger null,
FINWEEKSTART INTeger null,
DAYOFCALYEAR INTeger null,
DAYOFFINYEAR INTeger null,
vDAYOFMONTH INTeger null,
vDAYOFWEEK INTeger null,
DAYOFWEEKNAME VARCHAR(10) null,
DAYOFWEEKABBR VARCHAR(3) null,
ISWEEKENDDAY integer null,
ISLEAPYEAR integer null,
pPeriodDates Char(53) null,
pPeriodDates2 Char(53) null,
pPeriodDates3 Char(53) null,
pPeriodDates4 Char(53) null,
pPeriodDates5 Char(53) null,
pPeriodDates6 Char(53) null,
pPeriodFromDate timestamp(0) null,
pPeriodToDate timestamp(0) null,
DAYSINCALYEAR INTeger null,
DAYSINFINYEAR INTeger null,
WEEKDAYSCALYEARSOFAR INTeger null,
WEEKDAYSFINYEARSOFAR INTeger null,
WORKDAYSCALYEARSOFAR INTeger null,
WORKDAYSFINYEARSOFAR INTeger null,
DAYSINMONTH INTeger null,
WEEKDAYSMONTHSOFAR INTeger null,
DateExists integer null);


Insert into companymaster_d00(
StartYear,
NumYears,
pCompanyCode )
Select 
(Select StartYear from parameters_d00),
(Select NumYears from parameters_d00),companycode from dim_company;
/*companycode from dim_company*/

Update companymaster_d00
SET WEEKDAYSFINYEARSOFAR = 0,
WEEKDAYSCALYEARSOFAR = 0,
WEEKDAYSMONTHSOFAR = 0,
MAXDT = STR_TO_DATE('01/01/9999','%m/%d/%Y'),
MAXDT_NAME = 'DATE_MAX',
DT  = CAST(concat(cast(StartYear AS CHAR(4)), '-1-1') AS timestamp(0)) - (interval '1' year);

Update companymaster_d00
set pPeriodToDate =  DT - (INTERVAL '1' DAY);


/*ashu - need to grant select access on fusionops_model_schema.dim_date to fusionops/all users */
INSERT INTO dim_date
(Dim_Dateid,DateValue,calendarquartername,financialquartername,season,monthname,monthabbreviation,weekdayname,weekdayabbreviation,companycode,businessdaysseqno)
SELECT 1, '0001-01-01','0001 Q 0','0001 Q 0','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_date
               WHERE dim_dateid = 1);

drop table if exists mh_i01;
Create table mh_i01(maxid)
asselect 	ifnull(max(d.dim_dateid), 	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))from dim_date dwhere d.dim_dateid <> 1;

INSERT INTO dim_date
(Dim_Dateid,DateValue,JulianDate,CalendarYear,FinancialYear,CalendarQuarter,
CalendarQuarterID, CalendarQuarterName,
FinancialQuarter, FinancialQuarterID, FinancialQuarterName, Season, CalendarMonthID, CalendarMonthNumber, MonthName, MonthAbbreviation,
FinancialMonthID, FinancialMonthNumber, CalendarWeekID, CalendarWeek, FinancialWeekID, FinancialWeek, DayofCalendarYear, DayofFinancialYear,
DayofMonth, WeekDayNumber, WeekDayName, WeekDayAbbreviation, IsaWeekendday, IsaPublicHoliday, IsaLeapYear, IsaSpecialday,
DaysInCalendarYear, DaysInFinancialYear, WeekdaysInCalendarYear, WeekdaysInFinancialYear, WorkdaysInCalendarYear,
WorkdaysInFinancialYear, DaysInCalendarYearSofar, DaysInFinancialYearSofar,
WeekdaysInCalendarYearSofar, WeekdaysInFinancialYearSofar, WorkdaysInCalendarYearSofar, WorkdaysInFinancialYearSofar, DaysInMonth,
WeekdaysInMonth, WorkdaysInMonth, DaysInMonthSofar,
WeekdaysInMonthSofar, WorkdaysInMonthSofar, CompanyCode, DateName, MonthYear, WeekStartDate, WeekEndDate, MonthStartDate,
MonthEndDate, FinancialMonthStartDate, FinancialMonthEndDate, CalendarWeekYr, FinancialMonthYear, BusinessDaysSeqNo, FinancialYearStartDate, FinancialYearEndDate)SELECT mh_i01.maxid + row_number() over () , '1900-01-01',0,1900,1900,1,19001,'1900 Q 1',1,19001,'1900 Q 1','Not Set',190001,1,'January','Jan',190001,1,190001,1,190001,1,1,1,1,2,'Monday','Mon',0,0,0,0,365,1,260,260,260,260,0,0,0,0,0,0,0,21,21,1,0,0,d1.companycode,'01/01/1900','Jan 1900','1899-12-31','1900-01-06','1900-01-01','1900-01-02','1900-01-01','1900-01-02','1900-01','1900-01',1,'1900-01-01','1900-01-02'     FROM dim_company d1,mh_i01WHERE NOT EXISTS
             (SELECT 1
                FROM dim_date d2
               WHERE datevalue = '1900-01-01'                AND d2.companycode = d1.companycode);





/*SELECT mh_i01.maxid + row_number() over () , '1900-01-01','0001 Q 0','0001 Q 0','Not Set','Not Set','Not Set','Not Set','Not Set',d1.companycode,1
     FROM dim_company d1,mh_i01
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_date d2
               WHERE datevalue = '1900-01-01'
		AND d2.companycode = d1.companycode)*/

drop table if exists mh_i01;
Create table mh_i01(maxid)
as
select 	ifnull(max(d.dim_dateid), 	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))from dim_date dwhere d.dim_dateid <> 1;

/*INSERT INTO dim_date
(Dim_Dateid,DateValue,calendarquartername,financialquartername,season,monthname,monthabbreviation,weekdayname,weekdayabbreviation,companycode,businessdaysseqno)
SELECT mh_i01.maxid + row_number() over () ,'1900-01-02','0001 Q 0','0001 Q 0','Not Set','Not Set','Not Set','Not Set','Not Set',d1.companycode,2
     FROM dim_company d1,mh_i01
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_date d2
               WHERE datevalue = '1900-01-02'
                AND d2.companycode = d1.companycode)*/

INSERT INTO dim_date
(Dim_Dateid,DateValue,JulianDate,CalendarYear,FinancialYear,CalendarQuarter,
CalendarQuarterID, CalendarQuarterName,
FinancialQuarter, FinancialQuarterID, FinancialQuarterName, Season, CalendarMonthID, CalendarMonthNumber, MonthName, MonthAbbreviation,
FinancialMonthID, FinancialMonthNumber, CalendarWeekID, CalendarWeek, FinancialWeekID, FinancialWeek, DayofCalendarYear, DayofFinancialYear,
DayofMonth, WeekDayNumber, WeekDayName, WeekDayAbbreviation, IsaWeekendday, IsaPublicHoliday, IsaLeapYear, IsaSpecialday,
DaysInCalendarYear, DaysInFinancialYear, WeekdaysInCalendarYear, WeekdaysInFinancialYear, WorkdaysInCalendarYear,
WorkdaysInFinancialYear, DaysInCalendarYearSofar, DaysInFinancialYearSofar,
WeekdaysInCalendarYearSofar, WeekdaysInFinancialYearSofar, WorkdaysInCalendarYearSofar, WorkdaysInFinancialYearSofar, DaysInMonth,
WeekdaysInMonth, WorkdaysInMonth, DaysInMonthSofar,
WeekdaysInMonthSofar, WorkdaysInMonthSofar, CompanyCode, DateName, MonthYear, WeekStartDate, WeekEndDate, MonthStartDate,
MonthEndDate, FinancialMonthStartDate, FinancialMonthEndDate, CalendarWeekYr, FinancialMonthYear, BusinessDaysSeqNo, FinancialYearStartDate, FinancialYearEndDate)SELECT mh_i01.maxid + row_number() over () , '1900-01-02',0,1900,1900,1,19001,'1900 Q 1',1,19001,'1900 Q 1','Not Set',190001,1,'January','Jan',190001,1,190001,1,190001,1,2,2,2,3,'Tuesday','Tue',0,0,0,0,365,2,260,260,260,260,1,0,0,0,0,0,0,21,21,2,0,0,d1.companycode,'01/02/1900','Jan 1900','1899-12-31','1900-01-06','1900-01-01','1900-01-02','1900-01-01','1900-01-02','1900-01','1900-01',2,'1900-01-01','1900-01-02'     FROM dim_company d1,mh_i01WHERE NOT EXISTS
             (SELECT 1
                FROM dim_date d2
               WHERE datevalue = '1900-01-02'                AND d2.companycode = d1.companycode);


drop table if exists mh_i01;


Drop table if exists alldays_d00;
Create table alldays_d00 as 
Select ansidate(concat(year,'-',month,'-',counter)) date from (Select year,month,fday,day(last_day(ansidate(concat(year,'-',month,'-1')))) lday from 
(Select aa.year,bb.month,1 fday  from (Select (StartYear-1)+(counter-1) year from parameters_d00,counttbl_d00 where counter<=(Select NumYears +2 from parameters_d00)) aa, (select counter month from counttbl_d00 where counter<=12) bb) cc) dd,counttbl_d00 where counter<=lday;


Update companymaster_d00
Set FINMONTHSTART = 1,
    FINWEEKSTART = 1
where pCompanyCode = 'Not Set';

Drop table if exists companydetailed_d00;

Create table companydetailed_d00 as
Select row_number() over() iid,c.*,d.*,
Integer(0) pflag1,
Integer(0) pflag2,
Integer(0) pflag3,
Integer(0) loop2,
Integer(0) p1,
Integer(0) p2,
Integer(0) p3,
Integer(0) p4
from companymaster_d00 c,alldays_d00 d ;

Update companydetailed_d00 set DT=date;

Update companydetailed_d00
Set FINMONTHSTART = 1,
    FINWEEKSTART = 1
where pCompanyCode = 'Not Set';

Update companydetailed_d00
Set pflag1=1
where pPeriodDates is null 
and pCompanyCode <> 'Not Set';

