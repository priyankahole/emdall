Update companydetailed_d00 
set pPeriodDates = (Select pReturn from fiscalprd_i00 where fiscalprd_i00.pCompanyCode=companydetailed_d00.pCompanyCode and fiscalprd_i00.CalDate=companydetailed_d00.DT)
Where pflag1=1
AND pCompanyCode <>  'Not Set';

Update companydetailed_d00
set pflag2=1
where  pflag1=1 and pCompanyCode <>  'Not Set' and pPeriodDates is not null;

select pCompanyCode,min(dt) from companydetailed_d00 where pflag2=1 group by pCompanyCode;

Update companydetailed_d00
Set FINYEAR=substr(pPeriodDates,1,4)
Where  pflag2=1;

