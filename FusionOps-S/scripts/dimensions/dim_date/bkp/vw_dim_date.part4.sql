Update companymaster_d00 set MAXDT_EXISTS = 0;

Update companymaster_d00
set MAXDT_EXISTS = IFNULL((SELECT 1 FROM dim_date
                               WHERE DateValue = MAXDT AND companycode = pCompanyCode ),0);
                               
Drop table if exists mh_i01;
Create table mh_i01(maxid)
as
Select ifnull(max(dim_dateid),0)
from dim_date;
 
      INSERT INTO dim_date(dim_dateid,DateValue,
                          DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          select mh_i01.maxid + row_number() over(), MAXDT,
                  MAXDT_NAME,
                  0,
                  YEAR(MAXDT),
                  0,
                  QUARTER(MAXDT),
                  0,
                  concat(YEAR(MAXDT), ' Q ',QUARTER(MAXDT)),
                  0,
                  0,
                  'Not Set',
                  'Not Set',
                  0,
                  MONTH(MAXDT),
                  date_format(MAXDT,'%M'),
                  LEFT(date_format(MAXDT,'%M'),3),
                  0,
                  0,
                  0,
                  WEEK(MAXDT,4),
                  0,
                  0,
                  DAY(MAXDT),
                  0,
                  DAYOFMONTH(MAXDT),
                  DAYOFWEEK(MAXDT),
                  date_format(MAXDT,'%W'),
                  LEFT(date_format(MAXDT,'%W'),3),
                  (CASE WHEN DAYOFWEEK(MAXDT) IN (1,7) THEN 1 ELSE 0 END),
                  (CASE WHEN ((mod(YEAR(MAXDT),4) = 0)  AND (mod(YEAR(MAXDT) , 100) != 0 OR mod(YEAR(MAXDT) ,400) = 0)) THEN 1 ELSE 0 END),
                  DAYOFYEAR(CAST(concat('9999-01-01 00:00:00') AS timestamp(0))),
                  DAYOFYEAR(CAST(concat('9999-01-01 00:00:00') AS timestamp(0))),
                  DAYOFMONTH(MAXDT) - 1,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  DAYOFMONTH(MAXDT),
                  0,
                  0,
                  pCompanyCode,
                  concat(YEAR(MAXDT),'-',lpad(cast(WEEK(MAXDT,4) as char(2)),2,'0'))
		  from companymaster_d00,mh_i01 where MAXDT_EXISTS = 0;
                 
drop table if exists mh_i01;
 
Update companymaster_d00 
SET MAXDT = STR_TO_DATE('12/31/9999','%m/%d/%Y'), MAXDT_NAME = '31 Dec 9999';
 
Update companymaster_d00 
set MAXDT_EXISTS = IFNULL((SELECT 1 FROM dim_date
                               WHERE DateValue = MAXDT AND companycode = pCompanyCode ),0);
                               

drop table if exists mh_i01;
Create table mh_i01(maxid)
as
Select ifnull(max(dim_dateid),0)
from dim_date;
 
      INSERT INTO dim_date(dim_dateid,DateValue,
                          DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          select mh_i01.maxid + row_number() over(),MAXDT,
                  MAXDT_NAME,
                  0,
                  YEAR(MAXDT),
                  0,
                  QUARTER(MAXDT),
                  0,
                  concat(YEAR(MAXDT), ' Q ',QUARTER(MAXDT)),
                  0,
                  0,
                  'Not Set',
                  'Not Set',
                  0,
                  MONTH(MAXDT),
                  date_format(MAXDT,'%M'),
                  LEFT(date_format(MAXDT,'%M'),3),
                  0,
                  0,
                  0,
                  WEEK(MAXDT,4),
                  0,
                  0,
                  DAY(MAXDT),
                  0,
                  DAYOFMONTH(MAXDT),
                  DAYOFWEEK(MAXDT),
                  date_format(MAXDT,'%W'),
                  LEFT(date_format(MAXDT,'%W'),3),
                  (CASE WHEN DAYOFWEEK(MAXDT) IN (1,7) THEN 1 ELSE 0 END),
                  (CASE WHEN ((mod(YEAR(MAXDT) , 4) = 0)  AND (mod(YEAR(MAXDT) , 100) != 0 OR mod(YEAR(MAXDT) , 400) = 0)) THEN 1 ELSE 0 END),
                  ifnull(DAYOFYEAR(CAST(concat('9999-12-31 00:00:00') AS timestamp(0))),365),
                  ifnull(DAYOFYEAR(CAST(concat('9999-12-31 00:00:00') AS timestamp(0))),365),
                  DAYOFMONTH(MAXDT) - 1,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  DAYOFMONTH(MAXDT),
                  0,
                  0,
                  pCompanyCode,
                  concat(YEAR(MAXDT),'-',lpad(cast(WEEK(MAXDT,4) as char(2)),2,'0'))
		from companymaster_d00,mh_i01
		where MAXDT_EXISTS=0;
                 
drop table if exists mh_i01; 
  
  insert into dim_monthyear
  (Dim_MonthYearid, MonthYear, MonthYearAlt, MonthStartDate, MonthEndDate, DaysInMonth, CalYear, WorkdaysInMonth)
  select distinct case Dim_Dateid when 1 then 1 else CalendarMonthID end CalendarMonthID, 
          case Dim_Dateid when 1 then 'Jan 0001' else MonthYear end MonthYear, 
          case Dim_Dateid when 1 then '0001-01' else concat(CalendarYear,'-',lpad(CalendarMonthnumber,2,0)) end MonthYearAlt, 
          MonthStartDate, 
          MonthEndDate, 
          DaysInMonth, 
          CalendarYear CalYear, 
          case Dim_Dateid when 1 then 0 else WorkdaysInMonth end WorkdaysInMonth
  from dim_date dt
  where (Dim_Dateid = 1 or CalendarMonthID > 0) and companycode = 'Not Set'
      and not exists (select 1 from dim_monthyear my 
                      where my.Dim_MonthYearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarMonthID end);

\i /db/schema_migration/bin/wrapper_optimizedb.sh dim_year;
\i /db/schema_migration/bin/wrapper_optimizedb.sh dim_date;

DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins1;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_del;

CREATE TABLE tmp_dimdate_dim_year_ins 
AS
select distinct case Dim_Dateid when 1 then 1 else CalendarYear end Dim_Yearid,
CalendarYear CalYear,
case CalendarYear when 9999 then 365 else DaysInCalendarYear end DaysInCalendarYear
from dim_date dt
where (Dim_Dateid = 1 or CalendarYear > 0) and companycode = 'Not Set';

CREATE TABLE tmp_dimdate_dim_year_ins1
AS
SELECT DISTINCT Dim_Yearid
FROM tmp_dimdate_dim_year_ins;

CREATE TABLE tmp_dimdate_dim_year_del
AS
SELECT * FROM tmp_dimdate_dim_year_ins1 where 1=2;

INSERT INTO tmp_dimdate_dim_year_del
SELECT DISTINCT y.Dim_Yearid
FROM dim_year y, dim_date dt
where y.Dim_Yearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarYear end;

call vectorwise(combine 'tmp_dimdate_dim_year_ins1-tmp_dimdate_dim_year_del');


insert into dim_year
select DISTINCT dt.Dim_Yearid, 
dt.CalYear,
dt.DaysInCalendarYear
from tmp_dimdate_dim_year_ins dt, tmp_dimdate_dim_year_ins1 i
WHERE i.Dim_Yearid = dt.Dim_Yearid;
/*where (Dim_Dateid = 1 or CalendarYear > 0) and companycode = 'Not Set'
and not exists (select 1 from dim_year y 
      where y.Dim_Yearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarYear end)*/

DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins1;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_del;

/* business days sequence calculation */
  
drop table if exists dimdateseqtemp;
create table dimdateseqtemp (
companycode varchar(10) null,
dim_dateid integer null,
datevalue timestamp(0) null,
dateseqno integer null);

Insert into dimdateseqtemp(companycode,dim_dateid,datevalue,dateseqno)
select companycode,dim_dateid, datevalue, row_number() over(partition by companycode order by DateValue)  DtSeqNo
from dim_date dt
where dt.IsaPublicHoliday = 0 and dt.IsaWeekendday = 0 ;


/* Update BusinessDaysSeqNo for business days */
update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where  dt.dim_dateid = s.dim_dateid
and dt.companycode = s.companycode ;


/* Update BusinessDaysSeqNo for holidays/weekends where previous day is a business day */
/*update dim_date dt 
from dimdateseqtemp s 
  set dt.BusinessDaysSeqNo = s.dateseqno
where  dt.datevalue - 1 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1 */

/* Update BusinessDaysSeqNo for holidays/weekends where previous day is also a non-business day */

/*update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 2 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 2 days are non-business days */
/*update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 3 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 3 days are non-business days */
/*update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 4 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 4 days are non-business days */
/*update dim_date dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 5 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */


update dim_date dt
set dt.BusinessDaysSeqNo = (select max(s.dateseqno) from dimdateseqtemp s where s.DateValue <= dt.DateValue and s.companycode=dt.companycode)
where (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1);


/* Handle the case where the first date or first few days were non-business days */
UPDATE dim_date
SET BusinessDaysSeqNo = 0
WHERE BusinessDaysSeqNo IS NULL;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycount;
CREATE TABLE tmp_dim_date_weekdaycount
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,CalendarYear,
row_number() over( PARTITION BY CompanyCode,CalendarYear order by d2.datevalue) WeekdaysInCalendarYearSofar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycountfinyr;
CREATE TABLE tmp_dim_date_weekdaycountfinyr
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,FinancialYear,
row_number() over( PARTITION BY CompanyCode,FinancialYear order by d2.datevalue) WeekdaysInFinancialYearSofar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycountmonth;
CREATE TABLE tmp_dim_date_weekdaycountmonth
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,CalendarYear,MonthName,
row_number() over( PARTITION BY CompanyCode,CalendarYear,MonthName order by d2.datevalue) WeekdaysInMonthSoFar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


UPDATE tmp_dim_date_weekdaycount
SET date_value_next = date_value_next + 1;

UPDATE tmp_dim_date_weekdaycount
SET date_value_next_2 = date_value_next_2 + 2;

UPDATE tmp_dim_date_weekdaycountfinyr
SET date_value_next = date_value_next + 1;

UPDATE tmp_dim_date_weekdaycountfinyr
SET date_value_next_2 = date_value_next_2 + 2;

UPDATE tmp_dim_date_weekdaycountmonth
SET date_value_next = date_value_next + 1;

UPDATE tmp_dim_date_weekdaycountmonth
SET date_value_next_2 = date_value_next_2 + 2;


DROP TABLE IF EXISTS tmp_weekenddays;
CREATE TABLE tmp_weekenddays
AS
SELECT datevalue
FROM dim_date 
WHERE IsaWeekendday = 1;

/* Update WeekdaysInCalendarYearSofar*/
UPDATE dim_date d
FROM tmp_dim_date_weekdaycount w
SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
WHERE d.datevalue = w.datevalue
AND d.CompanyCode = w.CompanyCode;

UPDATE dim_date d
FROM tmp_dim_date_weekdaycount w, tmp_weekenddays e
SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
WHERE d.datevalue = w.date_value_next
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue;

UPDATE dim_date d
FROM tmp_dim_date_weekdaycount w,tmp_weekenddays e
SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
WHERE d.datevalue = w.date_value_next_2
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  ;/* Check that the previous day was also a weekend day */

/* Update weekdaysinfinancialyearsofar*/

UPDATE dim_date d
FROM tmp_dim_date_weekdaycountfinyr w
SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
WHERE d.datevalue = w.datevalue
AND d.CompanyCode = w.CompanyCode ;


UPDATE dim_date d
FROM tmp_dim_date_weekdaycountfinyr w, tmp_weekenddays e
SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
WHERE d.datevalue = w.date_value_next
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue;

UPDATE dim_date d
FROM tmp_dim_date_weekdaycountfinyr w,tmp_weekenddays e
SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
WHERE d.datevalue = w.date_value_next_2
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  ;/* Check that the previous day was also a weekend day */

/* Reset to 0 where its the 1st month of a financial year and 1st is Saturday */

UPDATE dim_date d
FROM tmp_weekenddays e
SET d.weekdaysinfinancialyearsofar = 0
WHERE d.datevalue = e.datevalue
AND substring(FinancialMonthID,5,2) = '01'
AND date_format(d.datevalue,'%e') = 1;

/* Reset to 0 where its the 1st month of a financial year and 1st or 2nd is Sunday */

UPDATE dim_date d
FROM tmp_weekenddays e
SET d.weekdaysinfinancialyearsofar = 0
WHERE d.datevalue = e.datevalue
AND substring(FinancialMonthID,5,2) = '01'
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
AND date_format(d.datevalue,'%e') IN ( 1,2);



/* Update weekdaysinmonthsofar */

UPDATE dim_date d
FROM tmp_dim_date_weekdaycountmonth w
SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
WHERE d.datevalue = w.datevalue 
AND d.CompanyCode = w.CompanyCode;

/* Update for Saturdays */
UPDATE dim_date d
FROM tmp_dim_date_weekdaycountmonth w, tmp_weekenddays e
SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
WHERE d.datevalue = w.date_value_next
AND d.CompanyCode = w.CompanyCode
AND d.datevalue = e.datevalue;


/* Update for Sundays */
UPDATE dim_date d
FROM tmp_dim_date_weekdaycountmonth w,tmp_weekenddays e
SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
WHERE d.datevalue = w.date_value_next_2
AND d.datevalue = e.datevalue
AND d.CompanyCode = w.CompanyCode
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  ;/* Check that the previous day was also a weekend day */

/* Reset to 0 where its a Sat and the 1st of a month */
UPDATE dim_date d
FROM tmp_weekenddays e
SET d.weekdaysinmonthsofar = 0
WHERE d.datevalue = e.datevalue
AND date_format(d.datevalue,'%e') = 1;

/* Reset to 0 where its a Sunday and 1st or 2nd of a month */
UPDATE dim_date d
FROM tmp_weekenddays e
SET d.weekdaysinmonthsofar = 0
WHERE d.datevalue = e.datevalue
AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
AND date_format(d.datevalue,'%e') IN ( 1,2);


/* workdaysincalendaryearsofar eqivalent to weekdaysincalendaryearsofar 
workdaysinfinancialyearsofar equivalent to weekdaysinfinancialyearsofar */

UPDATE dim_date
SET workdaysincalendaryearsofar = WeekdaysInCalendarYearSofar;

UPDATE dim_date
SET workdaysinfinancialyearsofar = weekdaysinfinancialyearsofar;


UPDATE dim_date
SET workdaysinmonthsofar = weekdaysinmonthsofar;

/* Update values for default dates */
UPDATE dim_date
SET weekdaysincalendaryearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET weekdaysinfinancialyearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET workdaysincalendaryearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET workdaysinfinancialyearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET weekdaysinmonthsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET workdaysinmonthsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

