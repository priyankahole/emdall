drop table if exists tffy_i10;

create table tffy_i10 as 
select *,cast(pFromDate as timestamp(0)) "f_date",
cast(pToDate as timestamp(0)) "t_date" 
from tmp_funct_fiscal_year;

Update tffy_i10 
set pReturn = cast(f_date as char(20)) || '|' || cast(t_date as char(20));


Update companydetailed_d00 c
Set pPeriodDates = (Select pReturn from tffy_i10 s 
			where s.pCompanyCode=c.pCompanyCode 
				and s.FiscalYear=c.FINYEAR 
				and s.Period=1 
				and s.fact_script_name='dim_date')
where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1;

Update companydetailed_d00
Set pflag3=1
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and pPeriodDates is null;

Update companydetailed_d00
Set pflag3=2
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and pPeriodDates is not null;


Update companydetailed_d00 c
set pPeriodDates = (Select pReturn from tffy_i10 s
                        where s.pCompanyCode=c.pCompanyCode 
				and s.FiscalYear=c.FINYEAR 
				and s.Period=12 
				and s.fact_script_name='dim_date')
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;

drop table if exists tffy_i10;

Update companydetailed_d00
Set pPeriodFromDate = (cast(substr(pPeriodDates,1,19) as date))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;

Update companydetailed_d00
Set pPeriodToDate = (cast(substr(pPeriodDates,21,19) as date))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;

Update companydetailed_d00
Set FINMONTHSTART = MONTH(pPeriodFromDate + (( INTERVAL '1' DAY) * ceiling((ansidate(pPeriodToDate) - ansidate(pPeriodFromDate))/2) ))+1
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;

Update companydetailed_d00
Set FINWEEKSTART = WEEK(pPeriodFromDate,4)+1
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;


Update companydetailed_d00
set pPeriodFromDate = (cast(substr(pPeriodDates,1,19) as ansidate))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=2;

Update companydetailed_d00
set pPeriodToDate = (cast(substr(pPeriodDates,21,19) as ansidate))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=2;

Update companydetailed_d00
set FINMONTHSTART = MONTH(pPeriodFromDate + (( INTERVAL '1' DAY) * ceiling((ansidate(pPeriodToDate) - ansidate(pPeriodFromDate))/2)))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=2;

Update companydetailed_d00
set FINWEEKSTART = WEEK(pPeriodFromDate,4)
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=2;



Update companydetailed_d00
SET FINMONTH = FINMONTHSTART
Where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 ;


Update companydetailed_d00
SET FINQUARTER=case when FINMONTH in (1,2,3) then 1
		    when FINMONTH in (4,5,6) then 2
		    when FINMONTH in (7,8,9) then 3
		    when FINMONTH in (10,11,12) then 4
		end
Where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1 ;
 
              
Update companydetailed_d00
SET FINQTRID = (FINYEAR * 10) + FINQUARTER
Where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1 ;

Update companydetailed_d00
SET FINQTRNAME = concat(FINYEAR,' Q ',CAST(FINQUARTER AS CHAR (1)))
Where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1 ;

Update companydetailed_d00
SET FINMONTHID = (FINYEAR * 100) + FINMONTH
Where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1 ;



drop table if exists git_i10;
create table git_i10 as 
Select pCompanyCode,
min(dt) "dt",
Integer(0) "FINMONTHSTART",
Integer(0) "FINWEEKSTART",
timestamp(null,0) "pPeriodToDate",
timestamp(null,0) "pPeriodFromDate",
char(null,53) "pPeriodDates",
Integer(0) "FINYEAR",
Integer(0) "FINMONTH",
Integer(0) "FINQUARTER",
Integer(0) "FINQTRID"
from companydetailed_d00 
where pflag2=1 
group by pCompanyCode;

insert into git_i10 
select pCompanyCode,min(dt) "dt",1,1,null,null,null,0,0,0,0 
from companydetailed_d00 
where pCompanyCode='Not Set' 
group by pCompanyCode;

select * from git_i10;

Update companydetailed_d00
set loop2=1
where DT >=(select dt from git_i10 where git_i10.pCompanyCode=companydetailed_d00.pCompanyCode );

Update git_i10 
set FINMONTHSTART=ifnull((Select FINMONTHSTART 
				from companydetailed_d00 
				where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode 
					and companydetailed_d00.dt=git_i10.dt),0)
Where pCompanyCode <> 'Not Set';

Update git_i10
set FINWEEKSTART=ifnull((Select FINWEEKSTART
                                from companydetailed_d00
                                where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode
                                        and companydetailed_d00.dt=git_i10.dt),0)
where pCompanyCode <> 'Not Set';

Update git_i10
set pPeriodToDate=(Select pPeriodToDate
                                from companydetailed_d00
                                where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode
                                        and companydetailed_d00.dt=git_i10.dt);

Update git_i10
set pPeriodFromDate =(Select pPeriodFromDate
                                from companydetailed_d00
                                where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode
                                        and companydetailed_d00.dt=git_i10.dt)
Where pCompanyCode <> 'Not Set';

Update git_i10
set pPeriodDates=(Select pPeriodDates
                                from companydetailed_d00
                                where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode
                                        and companydetailed_d00.dt=git_i10.dt)
Where pCompanyCode <> 'Not Set';

Update companydetailed_d00
set FINMONTHSTART=ifnull((select FINMONTHSTART 
				from git_i10 
				where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode ),0)
Where loop2=1;

Update companydetailed_d00
set FINWEEKSTART =ifnull((select FINWEEKSTART 
				from git_i10 
				where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode ),0)
Where loop2=1;

Update companydetailed_d00
set pPeriodToDate = (select pPeriodToDate 
				from git_i10 
				where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode )
Where loop2=1;

Update companydetailed_d00
set pPeriodFromDate = (select pPeriodFromDate 
				from git_i10 
				where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode )
Where loop2=1;

Update companydetailed_d00
set pPeriodDates = (select pPeriodDates 
				from git_i10 
				where companydetailed_d00.pCompanyCode=git_i10.pCompanyCode )
Where loop2=1;

drop table if exists git_i10;

Update companydetailed_d00
set p1=1
where loop2=1 and DT > pPeriodToDate;

Update companydetailed_d00
set p1=2
where loop2=1 and DT <= pPeriodToDate;


Update companydetailed_d00 
set pPeriodToDate = LAST_DAY(DT) 
where p1=1 and pCompanyCode = 'Not Set';

Update companydetailed_d00 
set FINYEAR = YEAR(DT) 
where p1=1 and pCompanyCode = 'Not Set';        

Update companydetailed_d00 
set FINMONTH = MONTH(DT) 
where p1=1 and pCompanyCode = 'Not Set';
            
Update companydetailed_d00
Set FINQUARTER=( case when FINMONTH in (1,2,3) then 1
		when FINMONTH in (4,5,6) then 2
		when FINMONTH in (7,8,9) then 3
		when FINMONTH in (10,11,12) then 4
end)
where p1=1 and pCompanyCode = 'Not Set';

Update companydetailed_d00 
SET FINQTRID = (FINYEAR * 10) + FINQUARTER 
where p1=1 and pCompanyCode = 'Not Set';

Update companydetailed_d00 
SET FINQTRNAME = concat(FINYEAR,' Q ',CAST(FINQUARTER AS CHAR (1))) 
where p1=1 and pCompanyCode = 'Not Set';

Update companydetailed_d00 
SET FINMONTHID = (FINYEAR * 100) + FINMONTH 
where p1=1 and pCompanyCode = 'Not Set';

Update companydetailed_d00 
set pPeriodDates = (Select pReturn from fiscalprd_i00 
			where fiscalprd_i00.pCompanyCode=companydetailed_d00.pCompanyCode 
			and fiscalprd_i00.CalDate=companydetailed_d00.DT) 
where p1=1 and pCompanyCode <> 'Not Set';

Update companydetailed_d00 
set p2=1 
where p1=1 and pCompanyCode <> 'Not Set' and pPeriodDates is not null;


Update companydetailed_d00 
set pPeriodFromDate = (cast(substr(pPeriodDates,9,19) as ansidate)) 
where p2=1;

Update companydetailed_d00 
set pPeriodToDate = (cast(substr(pPeriodDates,29,19) as ansidate)) 
where p2=1;

Update companydetailed_d00 
set FINMONTH = (substr(pPeriodDates,6,2)) 
where p2=1;

Update companydetailed_d00 
SET FINYEAR = (substr(pPeriodDates,1,4)) 
where p2=1;
         
Update companydetailed_d00
Set FINQUARTER =(case when FINMONTH in (1,2,3) then 1
		 when FINMONTH in (4,5,6) then 2
		 when FINMONTH in (7,8,9) then 3
		 when FINMONTH in (10,11,12) then 4
	end )
where p2=1;
      
Update companydetailed_d00 
SET FINQTRID = (FINYEAR * 10) + FINQUARTER 
where p2=1;

Update companydetailed_d00 
SET FINQTRNAME = concat(FINYEAR,' Q ',CAST(FINQUARTER AS CHAR (1))) 
where p2=1;

Update companydetailed_d00 
SET FINMONTHID = (FINYEAR * 100) + FINMONTH 
where p2=1;

update companydetailed_d00 set p1=0,p2=0;

Update companydetailed_d00 
set p1=1 
where loop2=1 and (pPeriodDates is not null or pCompanyCode = 'Not Set');
  
Update companydetailed_d00 SET ISWEEKENDDAY =0 where p1=1;    
Update companydetailed_d00 SET CALYEAR = YEAR(DT)  where p1=1;    
Update companydetailed_d00 SET CALQUARTER = QUARTER(DT) where p1=1;    
Update companydetailed_d00 SET CALMONTH = MONTH(DT) where p1=1;    
Update companydetailed_d00 SET vMONTHNAME = date_format(DT,'%M') where p1=1;    
Update companydetailed_d00 SET CALWEEK = WEEK(DT,4) where p1=1;    
Update companydetailed_d00 SET DAYOFCALYEAR = DAYOFMONTH(DT) where p1=1;    
Update companydetailed_d00 SET vDAYOFMONTH = DAYOFMONTH(DT) where p1=1;    
Update companydetailed_d00 SET vDAYOFWEEK = DAYOFWEEK(DT) where p1=1;    
Update companydetailed_d00 SET DAYOFWEEKNAME = date_format(DT,'%W') where p1=1;  
Update companydetailed_d00 SET JDATE = (CALYEAR * 1000) + DAYOFCALYEAR where p1=1;
Update companydetailed_d00 SET CALQTRID = (CALYEAR * 10) + CALQUARTER where p1=1;
Update companydetailed_d00 SET CALQTRNAME = concat(CALYEAR, ' Q ',CALQUARTER)  where p1=1;
Update companydetailed_d00 SET CALWEEKID = (CALYEAR * 100) + CALWEEK where p1=1;
Update companydetailed_d00 set ISWEEKENDDAY =1 where p1=1 and (vDAYOFWEEK = 1 OR  vDAYOFWEEK = 7);


Update companydetailed_d00 
set ISLEAPYEAR=(case when ((mod(CALYEAR,4) = 0)  AND (mod(CALYEAR,100) != 0 OR mod(CALYEAR,400) = 0)) then 1 else 0 end)
where p1=1;


Update companydetailed_d00 
set SEASON=(case when CALMONTH IN(9,10,11) then 'Spring'
				when CALMONTH IN(12,1,2) then 'Summer'
				when CALMONTH IN(3,4,5) then 'Autumn'
				when CALMONTH IN(6,7,8) then 'Winter'
			end)
where p1=1;

call vectorwise(combine 'companydetailed_d00');

Update companydetailed_d00 
SET CALMONTHID = (CALYEAR * 100) + CALMONTH 
where p1=1;

Update companydetailed_d00 
SET MONTHABBR = LEFT(vMONTHNAME, 3) 
where p1=1;

Update companydetailed_d00 
set FINWEEK= ( case when CALWEEK > FINWEEKSTART then CALWEEK - (FINWEEKSTART - 1)
						else CALWEEK + (FINWEEKSTART - 1) end)
where p1=1;
			
Update companydetailed_d00 
SET FINWEEKID = (FINYEAR * 100) + FINWEEK 
where p1=1;

Update companydetailed_d00 
SET DAYOFFINYEAR = (case when (CALMONTH > (FINMONTHSTART - 1)) 
				then DAYOFYEAR(DT) - (cast(concat(FINYEAR, '-1-1') as ansidate) - cast(concat(FINYEAR, '-', FINMONTHSTART, '-1') as ansidate))
				else DAYOFYEAR(DT) - (cast(concat(FINYEAR, '-1-1') as ansidate) - cast(concat(FINYEAR, '-', FINMONTHSTART, '-1') as ansidate))
				end)
where p1=1;

Update companydetailed_d00 
SET DAYOFWEEKABBR = LEFT(DAYOFWEEKNAME, 3) 
where p1=1;

Update companydetailed_d00 
SET DAYSINCALYEAR = DAYOFYEAR(CAST(concat(CALYEAR, '-12-31') AS timestamp(0))) 
where p1=1;

Update companydetailed_d00 
SET DAYSINFINYEAR = (CAST(concat(FINYEAR, '-', FINMONTHSTART, '-1') AS ansidate) -  CAST(concat(FINYEAR, '-', FINMONTHSTART, '-1') AS ansidate)) 
where p1=1;

Update companydetailed_d00 
SET DAYSINMONTH = DAY(( CAST(concat(CALYEAR, '-', LEFT(MONTH((DT + ( INTERVAL '1' MONTH))),3), '-1') AS DATE) - ( INTERVAL '1' DAY))) 
where p1=1;

Update companydetailed_d00 
set WEEKDAYSCALYEARSOFAR=0
 where p1=1 and DAYOFCALYEAR = 1;

Update companydetailed_d00 
set WEEKDAYSFINYEARSOFAR=0 
where p1=1 and CALMONTH = FINMONTHSTART AND vDAYOFMONTH = 1;


Update companydetailed_d00 
set WEEKDAYSMONTHSOFAR = 0 
where p1=1 and vDAYOFMONTH = 1;

Update companydetailed_d00 
SET WEEKDAYSCALYEARSOFAR= WEEKDAYSCALYEARSOFAR + 1 
Where p1=1 and ISWEEKENDDAY = 0;

Update companydetailed_d00 
SET WEEKDAYSFINYEARSOFAR = WEEKDAYSFINYEARSOFAR + 1 
Where p1=1 and ISWEEKENDDAY = 0;

Update companydetailed_d00 
SET WEEKDAYSMONTHSOFAR = WEEKDAYSMONTHSOFAR + 1 
Where p1=1 and ISWEEKENDDAY = 0;    

Update companydetailed_d00 
set DateExists = ifnull((select 1 from dim_date a where a.DateValue = DT and a.CompanyCode = pCompanyCode),0) 
where p1=1;

drop table if exists mh_i01;
Create table mh_i01(maxid)
as
Select ifnull(max(dim_dateid),0)
from dim_date;

INSERT INTO dim_date(dim_dateid,DateValue,
	  DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          Select mh_i01.maxid + row_number() over () ,DT,
                  date_format(DT, '%d %b %Y'),
                  JDATE,
                  CALYEAR,
                  FINYEAR,
                  CALQUARTER,
                  CALQTRID,
                  CALQTRNAME,
                  FINQUARTER,
                  FINQTRID,
                  FINQTRNAME,
                  SEASON,
                  CALMONTHID,
                  CALMONTH,
                  vMONTHNAME,
                  MONTHABBR,
                  ifnull(FINMONTHID, 0),
                  ifnull(FINMONTH, 0),
                  CALWEEKID,
                  CALWEEK,
                  FINWEEKID,
                  FINWEEK,
                  DAYOFCALYEAR,
                  ifnull(DAYOFFINYEAR, 0),
                  vDAYOFMONTH,
                  vDAYOFWEEK,
                  DAYOFWEEKNAME,
                  DAYOFWEEKABBR,
                  ISWEEKENDDAY,
                  ISLEAPYEAR,
                  DAYSINCALYEAR,
                  ifnull(DAYSINFINYEAR, 0),
                  DAYOFCALYEAR - 1,
                  ifnull(DAYOFFINYEAR - 1, 0),
                  WEEKDAYSCALYEARSOFAR,
                  ifnull(WEEKDAYSFINYEARSOFAR, 0),
                  WEEKDAYSCALYEARSOFAR,
                  ifnull(WEEKDAYSFINYEARSOFAR, 0),
                  DAYSINMONTH,
                  vDAYOFMONTH,
                  WEEKDAYSMONTHSOFAR,
                  WEEKDAYSMONTHSOFAR,
                  pCompanyCode,
                  concat(CALYEAR,'-',lpad(cast(CALWEEK as char(2)),2,'0'))
		From companydetailed_d00 ,mh_i01 where DateExists = 0 and p1=1;
     
    /*  UPDATE  dim_date dd
	from companydetailed_d00 c
        SET   dd.FinancialYear = c.FINYEAR,
              dd.CalendarQuarter = c.CALQUARTER,
              dd.CalendarQuarterID = c.CALQTRID,
              dd.CalendarQuarterName = c.CALQTRNAME,
              dd.FinancialQuarter = c.FINQUARTER,
              dd.FinancialQuarterID = c.FINQTRID,
              dd.FinancialQuarterName = c.FINQTRNAME,
              dd.Season = c.SEASON,
              dd.CalendarMonthID = c.CALMONTHID,
              dd.CalendarMonthNumber = c.CALMONTH,
              dd.MonthName = c.vMONTHNAME,
              dd.MonthAbbreviation = c.MONTHABBR,
              dd.FinancialMonthID = ifnull(c.FINMONTHID, 0),
              dd.FinancialMonthNumber = ifnull(c.FINMONTH, 0),
              dd.CalendarWeekID = c.CALWEEKID,
              dd.CalendarWeek = c.CALWEEK,
              dd.FinancialWeekID = c.FINWEEKID,
              dd.FinancialWeek = c.FINWEEK,
              dd.DayofCalendarYear = c.DAYOFCALYEAR,
              dd.DayofFinancialYear = ifnull(c.DAYOFFINYEAR, 0),
              dd.DayofMonth = c.vDAYOFMONTH,
              dd.WeekDayNumber = c.vDAYOFWEEK,
              dd.WeekDayName = c.DAYOFWEEKNAME,
              dd.WeekDayAbbreviation = c.DAYOFWEEKABBR,
              dd.IsaWeekendday = c.ISWEEKENDDAY,
              dd.IsaLeapYear = c.ISLEAPYEAR,
              dd.DaysInCalendarYear = c.DAYSINCALYEAR,
              dd.DaysInFinancialYear = ifnull(c.DAYSINFINYEAR, 0),
              dd.DaysInCalendarYearSofar = c.DAYOFCALYEAR - 1,
              dd.DaysInFinancialYearSofar = ifnull(c.DAYOFFINYEAR - 1, 0),
              dd.WeekdaysInCalendarYearSofar = c.WEEKDAYSCALYEARSOFAR,
              dd.WeekdaysInFinancialYearSofar = ifnull(c.WEEKDAYSFINYEARSOFAR, 0),
              dd.WorkdaysInCalendarYearSofar = c.WEEKDAYSCALYEARSOFAR,
              dd.WorkdaysInFinancialYearSofar = ifnull(c.WEEKDAYSFINYEARSOFAR, 0),
              dd.DaysInMonth = c.DAYSINMONTH,
              dd.DaysInMonthSofar = c.vDAYOFMONTH,
              dd.WeekdaysInMonthSofar = c.WEEKDAYSMONTHSOFAR,
              dd.WorkdaysInMonthSofar = c.WEEKDAYSMONTHSOFAR,
              dd.CalendarWeekYr = concat(CALYEAR,'-',lpad(cast(c.CALWEEK as char(2)),2,'0'))
      WHERE DateValue = DT and CompanyCode = pCompanyCode
	    and DateExists <>  0 and p1=1 */

 UPDATE  dim_date dd
from companydetailed_d00 c
SET
   dd.FinancialYear = c.FINYEAR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND    (dd.FinancialYear <> c.FINYEAR or dd.FinancialYear is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.CalendarQuarter = c.CALQUARTER
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.CalendarQuarter <> c.CALQUARTER or dd.CalendarQuarter is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.CalendarQuarterID = c.CALQTRID
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.CalendarQuarterID <> c.CALQTRID or dd.CalendarQuarterID is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.CalendarQuarterName = c.CALQTRNAME
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.CalendarQuarterName <> c.CALQTRNAME or dd.CalendarQuarterName is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.FinancialQuarter = c.FINQUARTER
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.FinancialQuarter <> c.FINQUARTER or dd.FinancialQuarter is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.FinancialQuarterID = c.FINQTRID
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.FinancialQuarterID <> c.FINQTRID or dd.FinancialQuarterID is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.FinancialQuarterName = c.FINQTRNAME
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.FinancialQuarterName <> c.FINQTRNAME or dd.FinancialQuarterName is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.Season = c.SEASON
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.Season <> c.SEASON or dd.Season is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.CalendarMonthID = c.CALMONTHID
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.CalendarMonthID <> c.CALMONTHID or dd.CalendarMonthID is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.CalendarMonthNumber = c.CALMONTH
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.CalendarMonthNumber <> c.CALMONTH or dd.CalendarMonthNumber is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.MonthName = c.vMONTHNAME
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.MonthName <> c.vMONTHNAME or dd.MonthName is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.MonthAbbreviation = c.MONTHABBR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.MonthAbbreviation <> c.MONTHABBR or dd.MonthAbbreviation is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.FinancialMonthID = ifnull(c.FINMONTHID, 0)
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.FinancialMonthID <> ifnull(c.FINMONTHID, 0) or dd.FinancialMonthID is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.FinancialMonthNumber = ifnull(c.FINMONTH, 0)
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.FinancialMonthNumber <> ifnull(c.FINMONTH, 0) or dd.FinancialMonthNumber is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.CalendarWeekID = c.CALWEEKID
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.CalendarWeekID <> c.CALWEEKID or dd.CalendarWeekID is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.CalendarWeek = c.CALWEEK
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.CalendarWeek <> c.CALWEEK or dd.CalendarWeek is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.FinancialWeekID = c.FINWEEKID
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.FinancialWeekID <> c.FINWEEKID or dd.FinancialWeekID is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.FinancialWeek = c.FINWEEK
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.FinancialWeek <> c.FINWEEK or dd.FinancialWeek is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DayofCalendarYear = c.DAYOFCALYEAR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DayofCalendarYear <> c.DAYOFCALYEAR or dd.DayofCalendarYear is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DayofFinancialYear = ifnull(c.DAYOFFINYEAR, 0)
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DayofFinancialYear <> ifnull(c.DAYOFFINYEAR, 0) or dd.DayofFinancialYear is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DayofMonth = c.vDAYOFMONTH
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DayofMonth <> c.vDAYOFMONTH or dd.DayofMonth is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WeekDayNumber = c.vDAYOFWEEK
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WeekDayNumber <> c.vDAYOFWEEK or dd.WeekDayNumber is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WeekDayName = c.DAYOFWEEKNAME
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WeekDayName <> c.DAYOFWEEKNAME or dd.WeekDayName is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WeekDayAbbreviation = c.DAYOFWEEKABBR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WeekDayAbbreviation <> c.DAYOFWEEKABBR or dd.WeekDayAbbreviation is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.IsaWeekendday = c.ISWEEKENDDAY
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.IsaWeekendday <> c.ISWEEKENDDAY or dd.IsaWeekendday is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.IsaLeapYear = c.ISLEAPYEAR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.IsaLeapYear <> c.ISLEAPYEAR or dd.IsaLeapYear is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DaysInCalendarYear = c.DAYSINCALYEAR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DaysInCalendarYear <> c.DAYSINCALYEAR or dd.DaysInCalendarYear is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DaysInFinancialYear = ifnull(c.DAYSINFINYEAR, 0)
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DaysInFinancialYear <> ifnull(c.DAYSINFINYEAR, 0) or dd.DaysInFinancialYear is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DaysInCalendarYearSofar = c.DAYOFCALYEAR - 1
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DaysInCalendarYearSofar <> c.DAYOFCALYEAR - 1 or dd.DaysInCalendarYearSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DaysInFinancialYearSofar = ifnull(c.DAYOFFINYEAR - 1, 0)
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DaysInFinancialYearSofar <> ifnull(c.DAYOFFINYEAR - 1, 0) or dd.DaysInFinancialYearSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WeekdaysInCalendarYearSofar = c.WEEKDAYSCALYEARSOFAR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WeekdaysInCalendarYearSofar <> c.WEEKDAYSCALYEARSOFAR or dd.WeekdaysInCalendarYearSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WeekdaysInFinancialYearSofar = ifnull(c.WEEKDAYSFINYEARSOFAR, 0)
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WeekdaysInFinancialYearSofar <> ifnull(c.WEEKDAYSFINYEARSOFAR, 0) or dd.WeekdaysInFinancialYearSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WorkdaysInCalendarYearSofar = c.WEEKDAYSCALYEARSOFAR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WorkdaysInCalendarYearSofar <> c.WEEKDAYSCALYEARSOFAR or dd.WorkdaysInCalendarYearSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WorkdaysInFinancialYearSofar = ifnull(c.WEEKDAYSFINYEARSOFAR, 0)
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WorkdaysInFinancialYearSofar <> ifnull(c.WEEKDAYSFINYEARSOFAR, 0) or dd.WorkdaysInFinancialYearSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DaysInMonth = c.DAYSINMONTH
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DaysInMonth <> c.DAYSINMONTH or dd.DaysInMonth is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.DaysInMonthSofar = c.vDAYOFMONTH
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.DaysInMonthSofar <> c.vDAYOFMONTH or dd.DaysInMonthSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WeekdaysInMonthSofar = c.WEEKDAYSMONTHSOFAR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WeekdaysInMonthSofar <> c.WEEKDAYSMONTHSOFAR or dd.WeekdaysInMonthSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.WorkdaysInMonthSofar = c.WEEKDAYSMONTHSOFAR
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.WorkdaysInMonthSofar <> c.WEEKDAYSMONTHSOFAR or dd.WorkdaysInMonthSofar is null);

 UPDATE  dim_date dd
from companydetailed_d00 c
SET dd.CalendarWeekYr = concat(CALYEAR,'-',lpad(cast(c.CALWEEK as char(2)),2,'0'))
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1
AND (dd.CalendarWeekYr <> concat(CALYEAR,'-',lpad(cast(c.CALWEEK as char(2)),2,'0')) or dd.CalendarWeekYr is null);


      
drop table if exists CWeekends_i01;
drop table if exists mh_i01;

create table CWeekends_i01 as 
SELECT CompanyCode,CalendarYear AS CYear,COUNT(IsaWeekendday) AS Num_Weekend_Days 
FROM dim_date 
WHERE IsaWeekendday = 1 
GROUP BY CompanyCode,CalendarYear;

drop table if exists CWeekdays_i01;

create table CWeekdays_i01 as
Select CompanyCode,CalendarYear AS CYear,MAX(DaysInCalendarYear) AS Days_In_Calendar_Year
From dim_date
GROUP BY CompanyCode,CalendarYear;

UPDATE dim_date dd
from CWeekends_i01, CWeekdays_i01
SET WeekdaysInCalendarYear = CWeekdays_i01.Days_In_Calendar_Year - CWeekends_i01.Num_Weekend_Days
WHERE dd.CalendarYear = CWeekends_i01.CYear and dd.CalendarYear = CWeekdays_i01.CYear
      and dd.CompanyCode = CWeekends_i01.CompanyCode
	and dd.CompanyCode = CWeekdays_i01.CompanyCode
AND (WeekdaysInCalendarYear <> CWeekdays_i01.Days_In_Calendar_Year - CWeekends_i01.Num_Weekend_Days or WeekdaysInCalendarYear is null);

drop table if exists CWeekends_i01;
drop table if exists CWeekdays_i01;

drop table if exists FWeekends_i01;
create table FWeekends_i01 as
Select CompanyCode,FinancialYear AS FYear,COUNT(IsaWeekendday) AS Num_Weekend_Days
FROM dim_date
WHERE IsaWeekendday = 1
GROUP BY CompanyCode,FinancialYear;


drop table if exists FWeekdays_i01;
create table FWeekdays_i01 as
Select CompanyCode,FinancialYear AS FYear,MAX(DaysInFinancialYear) AS Days_In_Financial_Year
From dim_date
GROUP BY CompanyCode,FinancialYear;

UPDATE dim_date dd
From FWeekends_i01, FWeekdays_i01
SET WeekdaysInFinancialYear = FWeekdays_i01.Days_In_Financial_Year - FWeekends_i01.Num_Weekend_Days
 WHERE dd.FinancialYear = FWeekends_i01.FYear and dd.FinancialYear = FWeekdays_i01.FYear
      and dd.CompanyCode = FWeekends_i01.CompanyCode
      and dd.CompanyCode = FWeekdays_i01.CompanyCode
AND (WeekdaysInFinancialYear <> FWeekdays_i01.Days_In_Financial_Year - FWeekends_i01.Num_Weekend_Days or WeekdaysInFinancialYear is null);

drop table if exists FWeekends_i01;
drop table if exists FWeekdays_i01;


UPDATE dim_date
   SET WorkdaysInCalendarYear = WeekdaysInCalendarYear
WHERE (WorkdaysInCalendarYear <> WeekdaysInCalendarYear or WorkdaysInCalendarYear is null);

UPDATE dim_date
SET WorkdaysInFinancialYear = WeekdaysInFinancialYear
WHERE (WorkdaysInFinancialYear <> WeekdaysInFinancialYear or WorkdaysInFinancialYear is null);

UPDATE dim_date
SET WorkdaysInMonth = WeekdaysInMonth
WHERE (WorkdaysInMonth <> WeekdaysInMonth or WorkdaysInMonth is null);

UPDATE dim_date
SET MonthYear = (case when CalendarYear = 0 then  'Not Set' else CONCAT(MonthAbbreviation, ' ', CalendarYear) end)
WHERE (MonthYear <> (case when CalendarYear = 0 then  'Not Set' else CONCAT(MonthAbbreviation, ' ', CalendarYear) end) or MonthYear is null);

UPDATE dim_date
SET FinancialMonthYear = ( case when FinancialYear = 0 then 'Not Set' else  CONCAT(FinancialYear, '-', LPAD(FinancialMonthNumber,2,'0')) end)
WHERE (FinancialMonthYear <> ( case when FinancialYear = 0 then 'Not Set' else  CONCAT(FinancialYear, '-', LPAD(FinancialMonthNumber,2,'0')) end) or FinancialMonthYear is null);
       
--ashu - need to re-check
drop table if exists StartEnd_i01;
create table StartEnd_i01 as
Select  min(datevalue + ( ( INTERVAL '1' day) - DAYOFWEEK(datevalue)))  Week_Start,
	max(datevalue +( ( INTERVAL '7' day) - DAYOFWEEK(datevalue))) Week_End,
	CalendarWeekID,
	CompanyCode
from dim_date 
group by CompanyCode,CalendarWeekID;

 UPDATE dim_date dd
from StartEnd_i01 StartEnd
    SET WeekStartDate = StartEnd.Week_Start
 WHERE dd.CompanyCode = StartEnd.CompanyCode
      and dd.CalendarWeekID = StartEnd.CalendarWeekID
AND (WeekStartDate <> StartEnd.Week_Start or WeekStartDate is null);


 UPDATE dim_date dd
from StartEnd_i01 StartEnd
SET WeekEndDate = StartEnd.Week_End
 WHERE dd.CompanyCode = StartEnd.CompanyCode
      and dd.CalendarWeekID = StartEnd.CalendarWeekID
AND (WeekEndDate <> StartEnd.Week_End or WeekEndDate is null);

     
drop table if exists StartEnd_i01;

create table StartEnd_i01 as 
Select min(datevalue) Month_Start, max(datevalue) Month_End, CalendarMonthID ,CompanyCode
From  dim_date 
group by CompanyCode,CalendarMonthID;
	
 UPDATE dim_date dd
From StartEnd_i01
SET MonthStartDate = StartEnd_i01.Month_Start
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.CalendarMonthID = StartEnd_i01.CalendarMonthID
AND (MonthStartDate <> StartEnd_i01.Month_Start or MonthStartDate is null);


 UPDATE dim_date dd
From StartEnd_i01
SET MonthEndDate = StartEnd_i01.Month_End
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.CalendarMonthID = StartEnd_i01.CalendarMonthID
AND (MonthEndDate <> StartEnd_i01.Month_End or MonthEndDate is null);

     
drop table if exists StartEnd_i01;
 
drop table if exists StartEnd_i01;
create table StartEnd_i01 as 
Select min(datevalue) Month_Start, max(datevalue) Month_End, FinancialMonthID,CompanyCode
         from dim_date 
         group by CompanyCode,FinancialMonthID;

 UPDATE dim_date dd
From StartEnd_i01
SET FinancialMonthStartDate = StartEnd_i01.Month_Start
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.FinancialMonthID = StartEnd_i01.FinancialMonthID
AND (FinancialMonthStartDate <> StartEnd_i01.Month_Start or FinancialMonthStartDate is null);


 UPDATE dim_date dd
From StartEnd_i01
SET  FinancialMonthEndDate = StartEnd_i01.Month_End
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.FinancialMonthID = StartEnd_i01.FinancialMonthID
AND (FinancialMonthEndDate <> StartEnd_i01.Month_End or FinancialMonthEndDate is null);

drop table if exists StartEnd_i01;
      

drop table if exists StartEnd_i01;
Create table StartEnd_i01 as
select min(datevalue) FinYr_Start, max(datevalue) FinYr_End,FinancialYear,CompanyCode
         from dim_date 
         group by CompanyCode,FinancialYear;

 UPDATE dim_date dd
FROM StartEnd_i01
    SET FinancialYearStartDate = StartEnd_i01.FinYr_Start,
        FinancialYearEndDate = StartEnd_i01.FinYr_End
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.FinancialYear = StartEnd_i01.FinancialYear;
      
