/* Pre Process For DeltaForSalesDocumentHeaderStatus */
INSERT INTO VBUK(VBUK_ABSTK,
                 VBUK_AEDAT,
                 VBUK_BESTK,
                 VBUK_BUCHK,
                 VBUK_CMGST,
                 VBUK_FKIVK,
                 VBUK_FKSAK,
                 VBUK_FKSTK,
                 VBUK_GBSTK,
                 VBUK_KOQUK,
                 VBUK_KOSTK,
                 VBUK_LFGSK,
                 VBUK_LFSTK,
                 VBUK_RELIK,
                 VBUK_RFGSK,
                 VBUK_RFSTK,
                 VBUK_TRSTA,
                 VBUK_UVALL,
                 VBUK_UVALS,
                 VBUK_VBELN,
                 VBUK_WBSTK,VBUK_UVVLK,VBUK_SPSTG,VBUK_LSSTK)
   SELECT  DISTINCT VBUK_ABSTK,
                   VBUK_AEDAT,
                   VBUK_BESTK,
                   VBUK_BUCHK,
                   VBUK_CMGST,
                   VBUK_FKIVK,
                   VBUK_FKSAK,
                   VBUK_FKSTK,
                   VBUK_GBSTK,
                   VBUK_KOQUK,
                   VBUK_KOSTK,
                   VBUK_LFGSK,
                   VBUK_LFSTK,
                   VBUK_RELIK,
                   VBUK_RFGSK,
                   VBUK_RFSTK,
                   VBUK_TRSTA,
                   VBUK_UVALL,
                   VBUK_UVALS,
                   VBUK_VBELN,
                   VBUK_WBSTK,VBUK_UVVLK,VBUK_SPSTG,VBUK_LSSTK
     FROM DELTA_VBUK dv
    WHERE NOT EXISTS
             (SELECT v.VBUK_VBELN
                FROM VBUK v
               WHERE dv.VBUK_VBELN = v.VBUK_VBELN);
CALL VECTORWISE(COMBINE 'VBUK');