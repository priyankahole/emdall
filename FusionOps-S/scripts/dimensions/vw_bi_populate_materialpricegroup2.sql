UPDATE dim_MaterialPriceGroup2 a FROM TVM2T
   SET a.Description = ifnull(TVM2T_BEZEI, 'Not Set'),
			a.dw_update_date = current_timestamp
 WHERE a.MaterialPriceGroup2 = TVM2T_MVGR2 AND RowIsCurrent = 1;
 
INSERT INTO dim_MaterialPriceGroup2(dim_MaterialPriceGroup2id, MaterialPriceGroup2,Description, RowIsCurrent, RowStartDate)
SELECT 1, 'Not Set', 'Not Set', 1, current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup2
               WHERE dim_MaterialPriceGroup2id = 1);

delete from number_fountain m where m.table_name = 'dim_MaterialPriceGroup2';

insert into number_fountain				   
select 	'dim_MaterialPriceGroup2',
	ifnull(max(d.dim_MaterialPriceGroup2id), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MaterialPriceGroup2 d
where d.dim_MaterialPriceGroup2id <> 1;

INSERT INTO dim_MaterialPriceGroup2(dim_materialpricegroup2Id,
                                    MaterialPriceGroup2,
                                    Description,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MaterialPriceGroup2')
          + row_number() over() ,
                         t.TVM2T_MVGR2,
          ifnull(TVM2T_BEZEI, 'Not Set'),
          current_timestamp,
          1
     FROM TVM2T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup2 a
               WHERE a.MaterialPriceGroup2 = TVM2T_MVGR2);		   
