insert into dim_part (dim_partid,rowstartdate,rowiscurrent,partnumber_noleadzero)
select 1,current_timestamp,1,'Not Set' from (select 1) as src
where
not exists (select 1 from dim_part where dim_partid=1);

insert into tmp_mara_marc_makt_spras_ins
select  a.MARA_MATNR,a.MARC_WERKS
FROM    mara_marc_makt_spras a;


insert into tmp_mara_marc_makt_spras_del
select b.MARA_MATNR,b.MARC_WERKS
from mara_marc_makt b;

call vectorwise (combine 'tmp_mara_marc_makt_spras_ins-tmp_mara_marc_makt_spras_del');

INSERT INTO mara_marc_makt(
   MARA_MATNR
  ,MARA_MTART
  ,MARA_MATKL
  ,MARA_PRDHA
  ,MARA_TRAGR
  ,MARA_SPART
  ,MARA_MTPOS
  ,MARA_KZREV
  ,MARA_MEINS
  ,MARA_MFRPN
  ,MARA_MSTAE
  ,MARA_LAEDA
  ,MARA_ERSDA
  ,MARC_WERKS
  ,MARC_MMSTA
  ,MARC_MAABC
  ,MARC_KZKRI
  ,MARC_EKGRP
  ,MARC_DISPO
  ,MARC_PLIFZ
  ,MARC_BESKZ
  ,MARC_MINBE
  ,MARC_EISBE
  ,MARC_MABST
  ,MARC_USEQU
  ,MARC_KAUTB
  ,MARC_STAWN
  ,MARC_LGPRO
  ,MARC_SOBSL
  ,MARC_INSMK
  ,MARC_XCHPF
  ,MARC_LVORM
  ,MARC_BSTRF
  ,MARC_DISMM
  ,MARC_DISLS
  ,MARC_BSTMI
  ,MARC_WEBAZ
  ,MARC_LGFSB
  ,MARC_HERKL
  ,MARC_BWSCL
  ,MARC_STRGR
  ,MARC_DISPR
  ,MARC_DISGR
  ,MARC_SCHGT
  ,MAKT_MAKTX
  ,MARC_DZEIT
  ,MAKT_SPRAS
  ,MARC_PRCTR
  ,MARA_EXTWG
  ,MARC_KZAUS
  ,MARC_BEARZ
  ,MARC_WZEIT
  ,MARA_J_3AGEND
  ,MARA_BISMT
  ,MARA_MSTAV
  ,MARA_VOLUM
  ,MARA_LABOR
  ,MARA_J_3APGNR
  ,MARA_J_3ACOL
  ,MARA_AFS_SCHNITT
  ,MARC_NCOST
  ,MARC_SFEPR
  ,MARC_FXHOR
  ,MARA_WRKST
  ,MARA_EAN11
  ,MARA_MFRNR
  ,MARA_GROES
  ,MARC_FHORI
  ,MARA_RAUBE
  ,MARA_STOFF
  ,MARC_RWPRO,MARC_SHFLG,MARC_SHZET,MARC_BSTMA,MARC_BSTFE,
       MARA_J_3AMIND,
       MARA_J_3AMSTAT,
       MARA_J_3AKALSV,
       MARA_J_3AKALSM,
       MARA_J_3ASMAT,
       MARA_J_3APROFP,
       MARA_J_3APROFC,
       MARA_J_3APROFG,
       MARA_J_3APROFT,
       MARA_J_3AGMID,
       MARA_J_3ANOGRID,
       MARA_J_3AFCC,
       MARA_J_4KCSGR,
       MARA_AFS_SCLAS,
       MARA_J_4KCOVSA,
       MARC_J_3AMSTATC,
       MARC_J_3AMSTAT,
       MARC_J_4KCOVS,
       MARC_J_3ADISPKZ,
       MARC_J_3AHORIZ,
       MARC_J_3APRFC,
       MARC_J_3APRFG,
       MARC_J_3APRFT,
       MARC_J_3ASGRID,
       MARC_J_3AVERPR,
       MARC_J_3AVERMR,
       MARC_J_3AVERSS,
       MARC_J_3AVERPU,
       MARC_J_3AVERSD,
       MARC_J_3AEKORG,
       MARC_J_3APRCM,
       MARC_J_3ABUND,
       MARC_J_3ACHINT,
       MARC_J_3ABAINT,
       MARC_J_3ARISK,
       MARC_J_3ADEFSI,
       MARC_J_4KDEFSC,
       MARC_J_4AATPSI,
       MARC_J_3ASLHMOD,
       MARC_J_3AMROUND,
       MARC_J_3AMRDMENG,
       MARC_J_3AMIND
) SELECT a.MARA_MATNR, MARA_MTART, MARA_MATKL, MARA_PRDHA, MARA_TRAGR, MARA_SPART, MARA_MTPOS,
 MARA_KZREV, MARA_MEINS, MARA_MFRPN, MARA_MSTAE, MARA_LAEDA, MARA_ERSDA, a.MARC_WERKS, MARC_MMSTA,
 MARC_MAABC, MARC_KZKRI, MARC_EKGRP, MARC_DISPO, MARC_PLIFZ, MARC_BESKZ, MARC_MINBE, MARC_EISBE,
 MARC_MABST, MARC_USEQU, MARC_KAUTB, MARC_STAWN, MARC_LGPRO, MARC_SOBSL, MARC_INSMK, MARC_XCHPF,
 MARC_LVORM, MARC_BSTRF, MARC_DISMM, MARC_DISLS, MARC_BSTMI, MARC_WEBAZ, MARC_LGFSB, MARC_HERKL,
 MARC_BWSCL, MARC_STRGR, MARC_DISPR, MARC_DISGR, MARC_SCHGT, MAKT_MAKTX, MARC_DZEIT, MAKT_SPRAS,
 MARC_PRCTR, MARA_EXTWG, MARC_KZAUS, MARC_BEARZ, MARC_WZEIT, MARA_J_3AGEND, MARA_BISMT, MARA_MSTAV,
 MARA_VOLUM, MARA_LABOR, MARA_J_3APGNR, MARA_J_3ACOL, MARA_AFS_SCHNITT, MARC_NCOST, MARC_SFEPR, MARC_FXHOR, MARA_WRKST, MARA_EAN11, MARA_MFRNR,  
 MARA_GROES,MARC_FHORI,MARA_RAUBE,MARA_STOFF,MARC_RWPRO,MARC_SHFLG,MARC_SHZET,MARC_BSTMA,MARC_BSTFE,
       MARA_J_3AMIND,
       MARA_J_3AMSTAT,
       MARA_J_3AKALSV,
       MARA_J_3AKALSM,
       MARA_J_3ASMAT,
       MARA_J_3APROFP,
       MARA_J_3APROFC,
       MARA_J_3APROFG,
       MARA_J_3APROFT,
       MARA_J_3AGMID,
       MARA_J_3ANOGRID,
       MARA_J_3AFCC,
       MARA_J_4KCSGR,
       MARA_AFS_SCLAS,
       MARA_J_4KCOVSA,
       MARC_J_3AMSTATC,
       MARC_J_3AMSTAT,
       MARC_J_4KCOVS,
       MARC_J_3ADISPKZ,
       MARC_J_3AHORIZ,
       MARC_J_3APRFC,
       MARC_J_3APRFG,
       MARC_J_3APRFT,
       MARC_J_3ASGRID,
       MARC_J_3AVERPR,
       MARC_J_3AVERMR,
       MARC_J_3AVERSS,
       MARC_J_3AVERPU,
       MARC_J_3AVERSD,
       MARC_J_3AEKORG,
       MARC_J_3APRCM,
       MARC_J_3ABUND,
       MARC_J_3ACHINT,
       MARC_J_3ABAINT,
       MARC_J_3ARISK,
       MARC_J_3ADEFSI,
       MARC_J_4KDEFSC,
       MARC_J_4AATPSI,
       MARC_J_3ASLHMOD,
       MARC_J_3AMROUND,
       MARC_J_3AMRDMENG,
       MARC_J_3AMIND
FROM mara_marc_makt_spras a,  tmp_mara_marc_makt_spras_ins b
WHERE a.MARA_MATNR = b.MARA_MATNR and a.MARC_WERKS = b.MARC_WERKS;


call vectorwise (combine 'tmp_mara_marc_makt_spras_ins-tmp_mara_marc_makt_spras_ins');
call vectorwise (combine 'tmp_mara_marc_makt_spras_del-tmp_mara_marc_makt_spras_del');

/*

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
       Revision = ifnull(MARA_KZREV, 'Not Set'),
       UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),
       CommodityCode = ifnull(MARC_STAWN, 'Not Set'),
       PartType = ifnull(MARA_MTART, 'Not Set'),
       LeadTime = ifnull(MARC_PLIFZ, 0),
       StockLocation = ifnull(marc_lgpro, 'Not Set'),
       PurchaseGroupCode = ifnull(marc_ekgrp, 'Not Set'),
       PurchaseGroupDescription =
          ifnull((SELECT t.T024_EKNAM
                    FROM t024 t
                   WHERE t.T024_EKGRP = MARC_EKGRP),
                 'Not Set'),
       MRPController = ifnull(marc_dispo, 'Not Set'),
       MaterialGroup = ifnull(mara_matkl, 'Not Set'),
     --  Plant = ifnull(mck.marc_werks, 'Not Set'),
       ABCIndicator = ifnull(marc_maabc, 'Not Set'),
       ProcurementType = ifnull(marc_beskz, 'Not Set'),
       StorageLocation = ifnull(MARC_LGFSB, 'Not Set'),
       CriticalPart = ifnull(MARC_KZKRI, 'Not Set'),
       MRPType = ifnull(MARC_DISMM, 'Not Set'),
       SupplySource = ifnull(MARC_BWSCL, 'Not Set'),
       StrategyGroup = ifnull(MARC_STRGR, 'Not Set'),
       TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),
       Division = ifnull(MARA_SPART, 'Not Set'),
       DivisionDescription =
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),
                 'Not Set'),
       GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
       DeletionFlag = ifnull(MARC_LVORM, 'Not Set'),
       MaterialStatus = ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
       ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
       MPN = ifnull(MARA_MFRPN, 'Not Set'),
       BulkMaterial = ifnull(MARC_SCHGT, 'Not Set'),
       ProductHierarchyDescription =
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set'),
       MRPProfile = ifnull(mck.MARC_DISPR, 'Not Set'),
       MRPProfileDescription =
          ifnull((SELECT p.T401T_KTEXT
                    FROM t401t p
                   WHERE p.T401T_DISPR = mck.MARC_DISPR),
                 'Not Set'),
       PartTypeDescription =
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set'),
       MaterialGroupDescription =
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set'),
       MRPTypeDescription =
          ifnull((SELECT mt.T438T_DIBEZ
                    FROM T438T mt
                   WHERE mt.T438T_DISMM = mck.MARC_DISMM),
                 'Not Set'),
       ProcurementTypeDescription =
          ifnull(
             (SELECT dd.DD07T_DDTEXT
                FROM DD07T dd
               WHERE dd.DD07T_DOMNAME = 'BESKZ'
                     AND dd.DD07T_DOMVALUE = MARC_BESKZ),
             'Not Set'),
       MRPControllerDescription =
          ifnull(
             (SELECT mc.T024D_DSNAM
                FROM T024D mc
               WHERE mc.T024D_DISPO = mck.MARC_DISPO
                     AND mc.T024D_WERKS = mck.MARC_WERKS),
             'Not Set'),
       MRPGroup = ifnull(MARC_DISGR, 'Not Set'),
       MRPGroupDescription =
          ifnull(
             (SELECT mpg.T438X_TEXT40
                FROM T438X mpg
               WHERE mpg.T438X_DISGR = mck.MARC_DISGR
                     AND mpg.T438X_WERKS = mck.MARC_WERKS),
             'Not Set'),
       MRPLotSize = ifnull(mck.MARC_DISLS, 'Not Set'),
       MRPLotSizeDescription =
          ifnull((SELECT ml.T439T_LOSLT
                    FROM T439T ml
                   WHERE ml.T439T_DISLS = mck.MARC_DISLS),
                 'Not Set'),
       materialstatusdescription =
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set'),
       dp.SpecialProcurement = ifnull(mck.MARC_SOBSL, 'Not Set'),
       dp.SpecialProcurementDescription =
          ifnull(
             (SELECT sp.T460T_LTEXT
                FROM t460t sp
               WHERE sp.T460T_SOBSL = mck.marc_sobsl
                     AND sp.T460T_WERKS = mck.MARC_WERKS),
             'Not Set'),
       dp.InhouseProductionTime = mck.MARC_DZEIT,
       dp.ProfitCenterCode = ifnull(mck.MARC_PRCTR, 'Not Set'),
       dp.ProfitCenterName =
          ifnull(
             (SELECT c.CEPCT_KTEXT
                FROM cepct c
               WHERE c.CEPCT_PRCTR = mck.MARC_PRCTR
                     AND c.CEPCT_DATBI > current_date),
             'Not Set'),
       dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
       dp.ExternalMaterialGroupDescription =
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set'),
       dp.MaterialDiscontinuationFlag = ifnull(MARC_KZAUS, 'Not Set'),
       dp.MaterialDiscontinuationFlagDescription =
          ifnull(
             (SELECT DD07T_DDTEXT
                FROM DD07T
               WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS),
             'Not Set'),
       dp.ProcessingTime = MARC_BEARZ,
       dp.TotalReplenishmentLeadTime = MARC_WZEIT,
       dp.GRProcessingTime = MARC_WEBAZ,
       dp.OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
       dp.AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
       dp.AFSColorDescription =
          ifnull((SELECT J_3ACOLRT_TEXT
                    FROM J_3ACOLRT
                   WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL),
                 'Not Set'),
       dp.SDMaterialStatusDescription =
          ifnull((SELECT TVMST_VMSTB
                    FROM tvmst
                   WHERE TVMST_VMSTA = MARA_MSTAV),
                 'Not Set'),
       dp.Volume = MARA_VOLUM,
       dp.AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),
       dp.AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
       dp.DoNotCost = ifnull(MARC_NCOST, 'Not Set'),
       dp.PlantMaterialStatus = ifnull(mck.MARC_MMSTA, 'Not Set'),
       dp.PlantMaterialStatusDescription =
          ifnull((SELECT t141t_MTSTB
                    FROM t141t mst
                   WHERE mst.T141T_MMSTA = mck.MARC_MMSTA),
                 'Not Set'),
       dp.REMProfile = ifnull(MARC_SFEPR, 'Not Set'),
       dp.PlanningTimeFence = ifnull(MARC_FXHOR, 0),
       dp.SafetyStock = ifnull(MARC_EISBE, 0.0000),
       dp.RoundingValue = ifnull(MARC_BSTRF, 0.0000),
       dp.ReorderPoint = ifnull(MARC_MINBE, 0.0000),
       dp.MinimumLotSize = ifnull(MARC_BSTMI,0.0000),
       dp.ValidFrom = (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and  j1.J_3AMAD_WERKS = mck.MARC_WERKS),
       dp.MRPStatus = ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and  j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
       dp.CheckingGroupCode = ifnull(mck.MARC_MTVFP, 'Not Set'),
       dp.CheckingGroup = ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
       dp.UPCNumber=ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set'),
       dp.MaximumStockLevel = ifnull(mck.MARC_MABST, 0.0000),
       dp.ConsumptionMode = ifnull(mck.MARC_VRMOD, 'Not Set'),
       dp.ConsumptionModeDescription = ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
       dp.ConsumptionPeriodBackward = ifnull(mck.MARC_VINT1, 0),
       dp.ConsumptionPeriodForward = ifnull(mck.MARC_VINT2, 0),
       dp.PartLongDesc = ifnull(mck.MAKT_MAKTG,'Not Set'),
       dp.ManfacturerNumber = ifnull(mck.MARA_MFRNR,'Not Set'),
       dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
       dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
	   dp.SchedMarginKey = ifnull(mck.MARC_FHORI,'Not Set'),
       dp.MRPControllerTelephone = ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set'),
			dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   
	   */

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PartDescription <> ifnull(MAKT_MAKTX, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET Revision = ifnull(MARA_KZREV, 'Not Set'),	
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and Revision <> ifnull(MARA_KZREV, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),	
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and UnitOfMeasure <> ifnull(MARA_MEINS, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p       
   SET CommodityCode = ifnull(MARC_STAWN, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and CommodityCode <> ifnull(MARC_STAWN, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p       
   SET PartType = ifnull(MARA_MTART, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and PartType <> ifnull(MARA_MTART, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p       
   SET LeadTime = ifnull(MARC_PLIFZ, 0),
	   	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and LeadTime <> ifnull(MARC_PLIFZ, 0);	   
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p       
   SET  StockLocation = ifnull(marc_lgpro, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and StockLocation <> ifnull(marc_lgpro, 'Not Set');	   

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p       
   SET  PurchaseGroupCode = ifnull(marc_ekgrp, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and PurchaseGroupCode <> ifnull(marc_ekgrp, 'Not Set');	 

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  PurchaseGroupDescription =
          ifnull((SELECT t.T024_EKNAM
                    FROM t024 t
                   WHERE t.T024_EKGRP = MARC_EKGRP),
                 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
and PurchaseGroupDescription <>
          ifnull((SELECT t.T024_EKNAM
                    FROM t024 t
                   WHERE t.T024_EKGRP = MARC_EKGRP),
                 'Not Set');	   
				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  MRPController = ifnull(marc_dispo, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and MRPController <> ifnull(marc_dispo, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET   MaterialGroup = ifnull(mara_matkl, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and MaterialGroup <> ifnull(mara_matkl, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  ABCIndicator = ifnull(marc_maabc, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and ABCIndicator <> ifnull(marc_maabc, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  ProcurementType = ifnull(marc_beskz, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and ProcurementType <> ifnull(marc_beskz, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  StorageLocation = ifnull(MARC_LGFSB, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and StorageLocation <> ifnull(MARC_LGFSB, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET CriticalPart = ifnull(MARC_KZKRI, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and CriticalPart <> ifnull(MARC_KZKRI, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET MRPType = ifnull(MARC_DISMM, 'Not Set'),
       dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and MRPType <> ifnull(MARC_DISMM, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  SupplySource = ifnull(MARC_BWSCL, 'Not Set'),
dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  SupplySource <> ifnull(MARC_BWSCL, 'Not Set');
 	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET   StrategyGroup = ifnull(MARC_STRGR, 'Not Set'),  
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  StrategyGroup <> ifnull(MARC_STRGR, 'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),	   
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and TransportationGroup <> ifnull(MARA_TRAGR, 'Not Set');
	
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  Division = ifnull(MARA_SPART, 'Not Set'),
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and Division <> ifnull(MARA_SPART, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  DivisionDescription =
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),
                 'Not Set'),
				   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and DivisionDescription <>
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),
                 'Not Set');
			
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
   			   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  GeneralItemCategory <> ifnull(MARA_MTPOS, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET DeletionFlag = ifnull(MARC_LVORM, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and DeletionFlag <> ifnull(MARC_LVORM, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  MaterialStatus = ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MaterialStatus <> ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ProductHierarchy <> ifnull(MARA_PRDHA, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET MPN = ifnull(MARA_MFRPN, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MPN <> ifnull(MARA_MFRPN, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET BulkMaterial = ifnull(MARC_SCHGT, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and BulkMaterial <> ifnull(MARC_SCHGT, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET ProductHierarchyDescription =
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ProductHierarchyDescription <>
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set');
				
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  MRPProfile = ifnull(mck.MARC_DISPR, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPProfile <> ifnull(mck.MARC_DISPR, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET   MRPProfileDescription =
          ifnull((SELECT p.T401T_KTEXT
                    FROM t401t p
                   WHERE p.T401T_DISPR = mck.MARC_DISPR),
                 'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPProfileDescription <>
          ifnull((SELECT p.T401T_KTEXT
                    FROM t401t p
                   WHERE p.T401T_DISPR = mck.MARC_DISPR),
                 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET PartTypeDescription =
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and PartTypeDescription <>
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set');
				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  MaterialGroupDescription =
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set'),
dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MaterialGroupDescription <>
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set');
				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET   MRPTypeDescription =
          ifnull((SELECT mt.T438T_DIBEZ
                    FROM T438T mt
                   WHERE mt.T438T_DISMM = mck.MARC_DISMM),
                 'Not Set'),
		dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and        MRPTypeDescription <>
          ifnull((SELECT mt.T438T_DIBEZ
                    FROM T438T mt
                   WHERE mt.T438T_DISMM = mck.MARC_DISMM),
                 'Not Set');
				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET ProcurementTypeDescription =
          ifnull(
             (SELECT dd.DD07T_DDTEXT
                FROM DD07T dd
               WHERE dd.DD07T_DOMNAME = 'BESKZ'
                     AND dd.DD07T_DOMVALUE = MARC_BESKZ),
             'Not Set'),
dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ProcurementTypeDescription <>
          ifnull(
             (SELECT dd.DD07T_DDTEXT
                FROM DD07T dd
               WHERE dd.DD07T_DOMNAME = 'BESKZ'
                     AND dd.DD07T_DOMVALUE = MARC_BESKZ),
             'Not Set');
			 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET MRPControllerDescription =
          ifnull(
             (SELECT mc.T024D_DSNAM
                FROM T024D mc
               WHERE mc.T024D_DISPO = mck.MARC_DISPO
                     AND mc.T024D_WERKS = mck.MARC_WERKS),
             'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPControllerDescription <>
          ifnull(
             (SELECT mc.T024D_DSNAM
                FROM T024D mc
               WHERE mc.T024D_DISPO = mck.MARC_DISPO
                     AND mc.T024D_WERKS = mck.MARC_WERKS),
             'Not Set');
		
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET MRPGroup = ifnull(MARC_DISGR, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPGroup <> ifnull(MARC_DISGR, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET MRPGroupDescription =
          ifnull(
             (SELECT mpg.T438X_TEXT40
                FROM T438X mpg
               WHERE mpg.T438X_DISGR = mck.MARC_DISGR
                     AND mpg.T438X_WERKS = mck.MARC_WERKS),
             'Not Set'),
 dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPGroupDescription <>
          ifnull(
             (SELECT mpg.T438X_TEXT40
                FROM T438X mpg
               WHERE mpg.T438X_DISGR = mck.MARC_DISGR
                     AND mpg.T438X_WERKS = mck.MARC_WERKS),
             'Not Set');
			 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET MRPLotSize = ifnull(mck.MARC_DISLS, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPLotSize <> ifnull(mck.MARC_DISLS, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET MRPLotSizeDescription =
          ifnull((SELECT ml.T439T_LOSLT
                    FROM T439T ml
                   WHERE ml.T439T_DISLS = mck.MARC_DISLS),
                 'Not Set'),
	  dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPLotSizeDescription <>
          ifnull((SELECT ml.T439T_LOSLT
                    FROM T439T ml
                   WHERE ml.T439T_DISLS = mck.MARC_DISLS),
                 'Not Set');
	
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET materialstatusdescription =
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set'),
	  dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and materialstatusdescription <>
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set');
			 
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.SpecialProcurement = ifnull(mck.MARC_SOBSL, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SpecialProcurement <> ifnull(mck.MARC_SOBSL, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.SpecialProcurementDescription =
          ifnull(
             (SELECT sp.T460T_LTEXT
                FROM t460t sp
               WHERE sp.T460T_SOBSL = mck.marc_sobsl
                     AND sp.T460T_WERKS = mck.MARC_WERKS),
             'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SpecialProcurementDescription <>
          ifnull(
             (SELECT sp.T460T_LTEXT
                FROM t460t sp
               WHERE sp.T460T_SOBSL = mck.marc_sobsl
                     AND sp.T460T_WERKS = mck.MARC_WERKS),
             'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.InhouseProductionTime = mck.MARC_DZEIT,
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.InhouseProductionTime <> mck.MARC_DZEIT;
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ProfitCenterCode = ifnull(mck.MARC_PRCTR, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ProfitCenterCode <> ifnull(mck.MARC_PRCTR, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ProfitCenterName =
          ifnull(
             (SELECT c.CEPCT_KTEXT
                FROM cepct c
               WHERE c.CEPCT_PRCTR = mck.MARC_PRCTR
                     AND c.CEPCT_DATBI > current_date),
             'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ProfitCenterName <>
          ifnull(
             (SELECT c.CEPCT_KTEXT
                FROM cepct c
               WHERE c.CEPCT_PRCTR = mck.MARC_PRCTR
                     AND c.CEPCT_DATBI > current_date),
             'Not Set');
			 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ExternalMaterialGroupCode <> ifnull(MARA_EXTWG, 'Not Set');
	   				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ExternalMaterialGroupDescription =
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set'),
  dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ExternalMaterialGroupDescription <>
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set');
				 

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.MaterialDiscontinuationFlag = ifnull(MARC_KZAUS, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MaterialDiscontinuationFlag <> ifnull(MARC_KZAUS, 'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.MaterialDiscontinuationFlagDescription =
          ifnull(
             (SELECT DD07T_DDTEXT
                FROM DD07T
               WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS),
             'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MaterialDiscontinuationFlagDescription <>
          ifnull(
             (SELECT DD07T_DDTEXT
                FROM DD07T
               WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS),
             'Not Set');
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ProcessingTime = MARC_BEARZ,
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks;

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.TotalReplenishmentLeadTime = MARC_WZEIT,
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.TotalReplenishmentLeadTime <> MARC_WZEIT;
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.GRProcessingTime = MARC_WEBAZ,
       dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.GRProcessingTime <> MARC_WEBAZ;
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.OldPartNumber <> ifnull(MARA_BISMT, 'Not Set');
	   
   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.AFSColor <> ifnull(MARA_J_3ACOL, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.AFSColorDescription =
          ifnull((SELECT J_3ACOLRT_TEXT
                    FROM J_3ACOLRT
                   WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL),
                 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.AFSColorDescription <>
          ifnull((SELECT J_3ACOLRT_TEXT
                    FROM J_3ACOLRT
                   WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL),
                 'Not Set');
   
 UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.SDMaterialStatusDescription =
          ifnull((SELECT TVMST_VMSTB
                    FROM tvmst
                   WHERE TVMST_VMSTA = MARA_MSTAV),
                 'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SDMaterialStatusDescription <>
          ifnull((SELECT TVMST_VMSTB
                    FROM tvmst
                   WHERE TVMST_VMSTA = MARA_MSTAV),
                 'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.Volume = MARA_VOLUM,	
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
and dp.Volume <> MARA_VOLUM;	 

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),  
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.AFSMasterGrid <> ifnull(MARA_J_3APGNR, 'Not Set');
	
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
   	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.AFSPattern <> ifnull(MARA_AFS_SCHNITT, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  dp.DoNotCost = ifnull(MARC_NCOST, 'Not Set'),
    	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.DoNotCost <> ifnull(MARC_NCOST, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.PlantMaterialStatus = ifnull(mck.MARC_MMSTA, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.PlantMaterialStatus <> ifnull(mck.MARC_MMSTA, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.PlantMaterialStatusDescription =
          ifnull((SELECT t141t_MTSTB
                    FROM t141t mst
                   WHERE mst.T141T_MMSTA = mck.MARC_MMSTA),
                 'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.PlantMaterialStatusDescription <>
          ifnull((SELECT t141t_MTSTB
                    FROM t141t mst
                   WHERE mst.T141T_MMSTA = mck.MARC_MMSTA),
                 'Not Set');
				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.REMProfile = ifnull(MARC_SFEPR, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.REMProfile <> ifnull(MARC_SFEPR, 'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.PlanningTimeFence = ifnull(MARC_FXHOR, 0),
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.PlanningTimeFence <> ifnull(MARC_FXHOR, 0);
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.SafetyStock = ifnull(MARC_EISBE, 0.0000),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SafetyStock <> ifnull(MARC_EISBE, 0.0000);
	   
   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.RoundingValue = ifnull(MARC_BSTRF, 0.0000),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  dp.RoundingValue <> ifnull(MARC_BSTRF, 0.0000);
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ReorderPoint = ifnull(MARC_MINBE, 0.0000),
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ReorderPoint <> ifnull(MARC_MINBE, 0.0000);
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.MinimumLotSize = ifnull(MARC_BSTMI,0.0000),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MinimumLotSize <> ifnull(MARC_BSTMI,0.0000);
	   

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ValidFrom = (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and  j1.J_3AMAD_WERKS = mck.MARC_WERKS),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks;
       
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.MRPStatus = ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and  j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks        
   and  dp.MRPStatus <> ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and  j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.CheckingGroupCode = ifnull(mck.MARC_MTVFP, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks        
   and dp.CheckingGroupCode <> ifnull(mck.MARC_MTVFP, 'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.CheckingGroup = ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
  dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks        
and    dp.CheckingGroup <> ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set');
     
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.UPCNumber=ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set'),
  dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks   
and dp.UPCNumber <>ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.MaximumStockLevel = ifnull(mck.MARC_MABST, 0.0000), 
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks;
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ConsumptionMode = ifnull(mck.MARC_VRMOD, 'Not Set'),
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.ConsumptionMode <> ifnull(mck.MARC_VRMOD, 'Not Set');
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ConsumptionModeDescription = ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks   
and  dp.ConsumptionModeDescription <> ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ConsumptionPeriodBackward = ifnull(mck.MARC_VINT1, 0),    	 
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks;
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ConsumptionPeriodForward = ifnull(mck.MARC_VINT2, 0),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks;
   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.PartLongDesc = ifnull(mck.MAKT_MAKTG,'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks  
	   and dp.PartLongDesc <> ifnull(mck.MAKT_MAKTG,'Not Set');
   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ManfacturerNumber = ifnull(mck.MARA_MFRNR,'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks  
	   and dp.ManfacturerNumber <> ifnull(mck.MARA_MFRNR,'Not Set');

	      
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
      dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.UPCCode <> ifnull(mck.MARA_EAN11,'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	  and dp.NDCCode <> ifnull(mck.MARA_GROES,'Not Set');
	  
   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.SchedMarginKey = ifnull(mck.MARC_FHORI,'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.SchedMarginKey <> ifnull(mck.MARC_FHORI,'Not Set');
   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.MRPControllerTelephone = ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set'),
			dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MRPControllerTelephone <> ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set');
	   
       

 UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.storageconditioncode = ifnull(mck.MARA_RAUBE,'Not Set'),
	   dp.hazardousmaterialnumber = ifnull(mck.MARA_STOFF,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks;
       

/*17 feb 2015 new MARC columns */	   

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.rangeofcoverage=ifnull(mck.MARC_RWPRO,'Not Set')
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.rangeofcoverage<>ifnull(mck.MARC_RWPRO,'Not Set');
	   
 UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.safetytimeind = ifnull(mck.MARC_SHFLG,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.safetytimeind <> ifnull(mck.MARC_SHFLG,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.safetytime = ifnull(mck.MARC_SHZET,0)
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.safetytime <> ifnull(mck.MARC_SHZET,0);
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.maxlotsize  = ifnull(mck.MARC_BSTMA,0)
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.maxlotsize  <> ifnull(mck.MARC_BSTMA,0);
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.fixedlotsize = ifnull(mck.MARC_BSTFE,0)
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.fixedlotsize <> ifnull(mck.MARC_BSTFE,0);
 
 /* end 17 feb 2015 new MARC columns */
 

  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_materialmasterind = ifnull(mck.MARA_J_3AMIND,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_materialmasterind <> ifnull(mck.MARA_J_3AMIND,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_materialmasterstat = ifnull(mck.MARA_J_3AMSTAT,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_materialmasterstat <> ifnull(mck.MARA_J_3AMSTAT,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_griddetprocsd = ifnull(mck.MARA_J_3AKALSV,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_griddetprocsd <> ifnull(mck.MARA_J_3AKALSV,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_griddetprocmm = ifnull(mck.MARA_J_3AKALSM,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_griddetprocmm <> ifnull(mck.MARA_J_3AKALSM,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_flagforsetmaterial = ifnull(mck.MARA_J_3ASMAT,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_flagforsetmaterial <> ifnull(mck.MARA_J_3ASMAT,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_plantprofile = ifnull(mck.MARA_J_3APROFP,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_plantprofile <> ifnull(mck.MARA_J_3APROFP,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_categprofile = ifnull(mck.MARA_J_3APROFC,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_categprofile <> ifnull(mck.MARA_J_3APROFC,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_gridprofile = ifnull(mck.MARA_J_3APROFG,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_gridprofile <> ifnull(mck.MARA_J_3APROFG,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_periodprofile = ifnull(mck.MARA_J_3APROFT,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_periodprofile <> ifnull(mck.MARA_J_3APROFT,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_materialconvidfordvc = ifnull(mck.MARA_J_3AGMID,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_materialconvidfordvc <> ifnull(mck.MARA_J_3AGMID,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_materialdummygrid = ifnull(mck.MARA_J_3ANOGRID,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_materialdummygrid <> ifnull(mck.MARA_J_3ANOGRID,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_fabriccontentcode = ifnull(mck.MARA_J_3AFCC,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_fabriccontentcode <> ifnull(mck.MARA_J_3AFCC,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_categstructure = ifnull(mck.MARA_J_4KCSGR,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_categstructure <> ifnull(mck.MARA_J_4KCSGR,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_specialprocurementclass = ifnull(mck.MARA_AFS_SCLAS,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_specialprocurementclass <> ifnull(mck.MARA_AFS_SCLAS,'Not Set');
 
  UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.afs_plantindepcoveragestrat = ifnull(mck.MARA_J_4KCOVSA,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.afs_plantindepcoveragestrat <> ifnull(mck.MARA_J_4KCOVSA,'Not Set');

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_MaterialMasterStatusMRP_MARC = ifnull(mck.MARC_J_3AMSTATC,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_MaterialMasterStatusMRP_MARC <> ifnull(mck.MARC_J_3AMSTATC,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_MaterialMasterStatus_MARC = ifnull(mck.MARC_J_3AMSTAT,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_MaterialMasterStatus_MARC <> ifnull(mck.MARC_J_3AMSTAT,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_CoverageStrategy = ifnull(mck.MARC_J_4KCOVS,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_CoverageStrategy <> ifnull(mck.MARC_J_4KCOVS,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_MRPIndicator = ifnull(mck.MARC_J_3ADISPKZ,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_MRPIndicator <> ifnull(mck.MARC_J_3ADISPKZ,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_SKULevelHorizon = ifnull(mck.MARC_J_3AHORIZ,0)
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_SKULevelHorizon <> ifnull(mck.MARC_J_3AHORIZ,0);
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_CategoryProfile = ifnull(mck.MARC_J_3APRFC,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_CategoryProfile <> ifnull(mck.MARC_J_3APRFC,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_GridProfile_MARC = ifnull(mck.MARC_J_3APRFG,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_GridProfile_MARC <> ifnull(mck.MARC_J_3APRFG,'Not Set');
 
 UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_PeriodProfile_MARC = ifnull(mck.MARC_J_3APRFT,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_PeriodProfile_MARC <> ifnull(mck.MARC_J_3APRFT,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_SingleGridValue = ifnull(mck.MARC_J_3ASGRID,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_SingleGridValue <> ifnull(mck.MARC_J_3ASGRID,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DefVersAFSPlannedIndReq = ifnull(mck.MARC_J_3AVERPR,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DefVersAFSPlannedIndReq <> ifnull(mck.MARC_J_3AVERPR,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DefVersMRP = ifnull(mck.MARC_J_3AVERMR,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DefVersMRP <> ifnull(mck.MARC_J_3AVERMR,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DefVersMRPsafetyStock = ifnull(mck.MARC_J_3AVERSS,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DefVersMRPsafetyStock <> ifnull(mck.MARC_J_3AVERSS,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DefVersPurchasing = ifnull(mck.MARC_J_3AVERPU,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DefVersPurchasing <> ifnull(mck.MARC_J_3AVERPU,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DefVersSD = ifnull(mck.MARC_J_3AVERSD,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DefVersSD <> ifnull(mck.MARC_J_3AVERSD,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DefPurchaseOrgSourceAlloc = ifnull(mck.MARC_J_3AEKORG,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DefPurchaseOrgSourceAlloc <> ifnull(mck.MARC_J_3AEKORG,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_ConsumptionPriority = ifnull(mck.MARC_J_3APRCM,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_ConsumptionPriority <> ifnull(mck.MARC_J_3APRCM,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_MatProducedUsingBundles = ifnull(mck.MARC_J_3ABUND,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_MatProducedUsingBundles <> ifnull(mck.MARC_J_3ABUND,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DiscreteBatchNumber = ifnull(mck.MARC_J_3ACHINT,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DiscreteBatchNumber <> ifnull(mck.MARC_J_3ACHINT,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_UseGridValueBatchNoFormats = ifnull(mck.MARC_J_3ABAINT,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_UseGridValueBatchNoFormats <> ifnull(mck.MARC_J_3ABAINT,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_RiskFactor = ifnull(mck.MARC_J_3ARISK,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_RiskFactor <> ifnull(mck.MARC_J_3ARISK,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DefaultGridValue = ifnull(mck.MARC_J_3ADEFSI,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DefaultGridValue <> ifnull(mck.MARC_J_3ADEFSI,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_DefaultStockCategoryValue = ifnull(mck.MARC_J_4KDEFSC,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_DefaultStockCategoryValue <> ifnull(mck.MARC_J_4KDEFSC,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_MRPCheckMethod = ifnull(mck.MARC_J_4AATPSI,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_MRPCheckMethod <> ifnull(mck.MARC_J_4AATPSI,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_ActivateSKULvlHorizonMethod2 = ifnull(mck.MARC_J_3ASLHMOD,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_ActivateSKULvlHorizonMethod2 <> ifnull(mck.MARC_J_3ASLHMOD,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_MRPTwoLevelRoundIndicator = ifnull(mck.MARC_J_3AMROUND,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_MRPTwoLevelRoundIndicator <> ifnull(mck.MARC_J_3AMROUND,'Not Set');
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_SKURoundQuantity = ifnull(mck.MARC_J_3AMRDMENG,0.000)
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_SKURoundQuantity <> ifnull(mck.MARC_J_3AMRDMENG,0.000);
 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.AFS_MaterialMasterIndicatorMRP = ifnull(mck.MARC_J_3AMIND,'Not Set')
   , dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.AFS_MaterialMasterIndicatorMRP <> ifnull(mck.MARC_J_3AMIND,'Not Set');
	   
insert into tmp_dim_MARA_MARC_MAKT_ins
select mck.MARA_MATNR,mck.MARC_WERKS
FROM    MARA_MARC_MAKT mck;

insert into tmp_dim_MARA_MARC_MAKT_del
select p.PartNumber,p.Plant
from dim_part p;

call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_ins-tmp_dim_MARA_MARC_MAKT_del');


delete from number_fountain m where m.table_name = 'dim_part';
   
insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1; 

INSERT INTO dim_part(dim_partid,
                     PartNumber,
                     PartDescription,
                     Revision,
                     UnitOfMeasure,
                     RowStartDate,
                     RowIsCurrent,
                     CommodityCode,
                     PartType,
                     LeadTime,
                     StockLocation,
                     PurchaseGroupCode,
                     PurchaseGroupDescription,
                     MRPController,
                     MaterialGroup,
                     Plant,
                     ABCIndicator,
                     ProcurementType,
                     StorageLocation,
                     CriticalPart,
                     MRPType,
                     SupplySource,
                     StrategyGroup,
                     TransportationGroup,
                     Division,
                     DivisionDescription,
                     GeneralItemCategory,
                     DeletionFlag,
                     MaterialStatus,
                     ProductHierarchy,
                     MPN,
                     ProductHierarchyDescription,
                     MRPProfile,
                     MRPProfileDescription,
                     PartTypeDescription,
                     MaterialGroupDescription,
                     MRPTypeDescription,
                     ProcurementTypeDescription,
                     MRPControllerDescription,
                     MRPGroup,
                     MRPGroupDescription,
                     MinimumLotSize,
                     MRPLotSize,
                     MRPLotSizeDescription,
                     MaterialStatusDescription,
                     SpecialProcurement,
                     SpecialProcurementDescription,
                     BulkMaterial,
                     InhouseProductionTime,
                     ProfitCenterCode,
                     ProfitCenterName,
                     ExternalMaterialGroupCode,
                     ExternalMaterialGroupDescription,
                     MaterialDiscontinuationFlag,
                     MaterialDiscontinuationFlagDescription,
                     ProcessingTime,
                     TotalReplenishmentLeadTime,
                     GRProcessingTime,
                     OldPartNumber,
                     AFSColor,
                     AFSColorDescription,
                     SDMaterialStatusDescription,
                     Volume,
                     AFSMasterGrid,
                     AFSPattern,
                     Laboratory,
                     DoNotCost,
                     PlantMaterialStatus,
                     PlantMaterialStatusDescription,
                     REMProfile,
                     PlanningTimeFence,
                     SafetyStock,
                     RoundingValue,
                     ReorderPoint,
                     ValidFrom,
                     MRPStatus,
                     CheckingGroupCode,
                     CheckingGroup,
                        UPCNumber,
                        MaximumStockLevel,
                        ConsumptionMode,
                        ConsumptionModeDescription,
                        ConsumptionPeriodBackward,
                        ConsumptionPeriodForward,
                        PartLongDesc,
			ManfacturerNumber,
                        UPCCode,
                        NDCCode,
						SchedMarginKey,
						storageconditioncode,
						hazardousmaterialnumber,
						rangeofcoverage,
						safetytimeind,
						safetytime,
						maxlotsize,
						fixedlotsize,
			MRPControllerTelephone,
			AFS_MaterialMasterInd,
			AFS_MaterialMasterStat,
			AFS_GridDetProcSD,
			AFS_GridDetProcMM,
			AFS_FlagForSetmaterial,
			AFS_PlantProfile,
			AFS_CategProfile,
			AFS_GridProfile,
			AFS_PeriodProfile,
			AFS_MaterialConvIDforDVC,
			AFS_MaterialDummyGrid,
			AFS_FabricContentCode,
			AFS_CategStructure,
			AFS_SpecialProcurementClass,
			AFS_PlantIndepCoverageStrat,
			AFS_MaterialMasterStatusMRP_MARC,
			AFS_MaterialMasterStatus_MARC,
			AFS_CoverageStrategy,
			AFS_MRPIndicator,
			AFS_SKULevelHorizon,
			AFS_CategoryProfile,
			AFS_GridProfile_MARC,
			AFS_PeriodProfile_MARC,
			AFS_SingleGridValue,
			AFS_DefVersAFSPlannedIndReq,
			AFS_DefVersMRP,
			AFS_DefVersMRPsafetyStock,
			AFS_DefVersPurchasing,
			AFS_DefVersSD,
			AFS_DefPurchaseOrgSourceAlloc,
			AFS_ConsumptionPriority,
			AFS_MatProducedUsingBundles,
			AFS_DiscreteBatchNumber,
			AFS_UseGridValueBatchNoFormats,
			AFS_RiskFactor,
			AFS_DefaultGridValue,
			AFS_DefaultStockCategoryValue,
			AFS_MRPCheckMethod,
			AFS_ActivateSKULvlHorizonMethod2,
			AFS_MRPTwoLevelRoundIndicator,
			AFS_SKURoundQuantity,
			AFS_MaterialMasterIndicatorMRP
			)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part')
          + row_number()
             over(), t.* from (SELECT DISTINCT
          mck.MARA_MATNR,
          ifnull(MAKT_MAKTX, 'Not Set'),
          ifnull(MARA_KZREV, 'Not Set'),
          ifnull(MARA_MEINS, 'Not Set'),
          current_timestamp,
          1,
          ifnull(MARC_STAWN, 'Not Set'),
          ifnull(MARA_MTART, 'Not Set'),
          ifnull(MARC_PLIFZ, 0),
          ifnull(MARC_LGPRO, 'Not Set'),
          ifnull(MARC_EKGRP, 'Not Set'),
          ifnull((SELECT t.T024_EKNAM
                    FROM t024 t
                   WHERE t.T024_EKGRP = MARC_EKGRP),
                 'Not Set')
             PurchaseGroupDescription,
          ifnull(MARC_DISPO, 'Not Set'),
          ifnull(MARA_MATKL, 'Not Set'),
          mck.MARC_WERKS,
          ifnull(MARC_MAABC, 'Not Set'),
          ifnull(MARC_BESKZ, 'Not Set'),
          ifnull(MARC_LGFSB, 'Not Set'),
          ifnull(MARC_KZKRI, 'Not Set'),
          ifnull(MARC_DISMM, 'Not Set'),
          ifnull(MARC_BWSCL, 'Not Set'),
          ifnull(MARC_STRGR, 'Not Set'),
          ifnull(MARA_TRAGR, 'Not Set'),
          ifnull(MARA_SPART, 'Not Set'),
   ifnull((SELECT TSPAT_VTEXT FROM TSPAT
    WHERE TSPAT_SPART = MARA_SPART
                    AND TSPAT_SPRAS = 'E'),'Not Set'),
          ifnull(MARA_MTPOS, 'Not Set'),
          ifnull(MARC_LVORM, 'Not Set'),
          ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
          ifnull(MARA_PRDHA, 'Not Set'),
          ifnull(MARA_MFRPN, 'Not Set'),
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set')
             ProductHierarchyDescription,
          ifnull(mck.MARC_DISPR, 'Not Set') MRPProfile,
          ifnull((SELECT p.T401T_KTEXT
                    FROM t401t p
                   WHERE p.T401T_DISPR = mck.MARC_DISPR),
                 'Not Set')
             MRPProfileDescription,
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set')
             PartTypeDescription,
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set')
             MaterialGroupDescription,
          ifnull((SELECT mt.T438T_DIBEZ
                    FROM T438T mt
                   WHERE mt.T438T_DISMM = mck.MARC_DISMM),
                 'Not Set')
             MRPTypeDescription,
          ifnull(
             (SELECT dd.DD07T_DDTEXT
                FROM DD07T dd
               WHERE dd.DD07T_DOMNAME = 'BESKZ'
                     AND dd.DD07T_DOMVALUE = MARC_BESKZ),
             'Not Set')
             ProcurementTypeDescription,
          ifnull(
             (SELECT mc.T024D_DSNAM
                FROM T024D mc
               WHERE mc.T024D_DISPO = mck.MARC_DISPO
                     AND mc.T024D_WERKS = mck.MARC_WERKS),
             'Not Set')
             MRPControllerDescription,
          ifnull(MARC_DISGR, 'Not Set') MRPGroup,
          ifnull(
             (SELECT mpg.T438X_TEXT40
                FROM T438X mpg
               WHERE mpg.T438X_DISGR = mck.MARC_DISGR
                     AND mpg.T438X_WERKS = mck.MARC_WERKS),
             'Not Set')
             MRPGroupDescription,
          ifnull(mck.MARC_BSTMI,0.0000) MinimumLotSize,
          ifnull(mck.MARC_DISLS, 'Not Set') MRPLotSize,
          ifnull((SELECT ml.T439T_LOSLT
                    FROM T439T ml
                   WHERE ml.T439T_DISLS = mck.MARC_DISLS),
                 'Not Set')
             MRPLotSizeDescription,
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set')
             materialstatusdescription,
          ifnull(mck.MARC_SOBSL, 'Not Set'),
          ifnull(
             (SELECT sp.T460T_LTEXT
                FROM t460t sp
               WHERE sp.T460T_SOBSL = mck.MARC_SOBSL
                     AND sp.T460T_WERKS = mck.MARC_WERKS),
             'Not Set'),
          ifnull(MARC_SCHGT, 'Not Set'),
          mck.MARC_DZEIT,
          ifnull(MARC_PRCTR, 'Not Set'),
          ifnull(
             (SELECT c.CEPCT_KTEXT
                FROM cepct c
               WHERE c.CEPCT_PRCTR = MARC_PRCTR AND c.CEPCT_DATBI > current_date),
             'Not Set'),
          ifnull(MARA_EXTWG, 'Not Set'),
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set'),
          ifnull(MARC_KZAUS, 'Not Set'),
          ifnull(
             (SELECT DD07T_DDTEXT
                FROM DD07T
               WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS),
             'Not Set'),
          mck.MARC_BEARZ,
          mck.MARC_WZEIT,
   mck.MARC_WEBAZ,
   ifnull(mck.MARA_BISMT, 'Not Set'),
   ifnull(mck.MARA_J_3ACOL, 'Not Set'),
   ifnull((SELECT J_3ACOLRT_TEXT
                FROM J_3ACOLRT
               WHERE J_3ACOLRT_J_3ACOL = mck.MARA_J_3ACOL), 'Not Set'),
  ifnull((SELECT TVMST_VMSTB
     FROM tvmst
       WHERE TVMST_VMSTA = MARA_MSTAV), 'Not Set'),
  MARA_VOLUM,
  ifnull(MARA_J_3APGNR, 'Not Set'),
  ifnull(MARA_AFS_SCHNITT, 'Not Set'),
  ifnull(MARA_LABOR, 'Not Set'),
  ifnull(MARC_NCOST,'Not Set'),
  ifnull(mck.MARC_MMSTA,'Not Set'),
     ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = mck.MARC_MMSTA), 'Not Set'),
  ifnull(MARC_SFEPR,'Not Set'),
  ifnull(MARC_FXHOR,0),
  ifnull(MARC_EISBE,0.0000),
     ifnull(MARC_BSTRF,0.0000),
     ifnull(MARC_MINBE,0.0000),
  (select  ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and  j1.J_3AMAD_WERKS = mck.MARC_WERKS),
  ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and  j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
  ifnull(mck.MARC_MTVFP, 'Not Set'),
  ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
  ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set'),
  ifnull(mck.MARC_MABST, 0.0000),
  ifnull(mck.MARC_VRMOD, 'Not Set'),
  ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
  ifnull(mck.MARC_VINT1, 0),
  ifnull(mck.MARC_VINT2, 0),
  ifnull(mck.MAKT_MAKTG,'Not Set'),
  ifnull(mck.MARA_MFRNR,'Not Set'),
  ifnull(mck.MARA_EAN11,'Not Set'),
  ifnull(mck.MARA_GROES,'Not Set'),
  ifnull(mck.MARC_FHORI,'Not Set'),
  ifnull(mck.MARA_RAUBE,'Not Set'),
  ifnull(mck.MARA_STOFF,'Not Set'),
  ifnull(mck.MARC_RWPRO,'Not Set'),
  ifnull(mck.MARC_SHFLG,'Not Set'),
  ifnull(mck.MARC_SHZET,0),
  ifnull(mck.MARC_BSTMA,0),
  ifnull(mck.MARC_BSTFE,0),
  ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set') MRPControllerTelephone,
  ifnull(mck.MARA_J_3AMIND,'Not Set'),
  ifnull(mck.MARA_J_3AMSTAT,'Not Set'),
  ifnull(mck.MARA_J_3AKALSV,'Not Set'),
  ifnull(mck.MARA_J_3AKALSM,'Not Set'),
  ifnull(mck.MARA_J_3ASMAT,'Not Set'),
  ifnull(mck.MARA_J_3APROFP,'Not Set'),
  ifnull(mck.MARA_J_3APROFC,'Not Set'),
  ifnull(mck.MARA_J_3APROFG,'Not Set'),
  ifnull(mck.MARA_J_3APROFT,'Not Set'),
  ifnull(mck.MARA_J_3AGMID,'Not Set'),
  ifnull(mck.MARA_J_3ANOGRID,'Not Set'),
  ifnull(mck.MARA_J_3AFCC,'Not Set'),
  ifnull(mck.MARA_J_4KCSGR,'Not Set'),
  ifnull(mck.MARA_AFS_SCLAS,'Not Set'),
  ifnull(mck.MARA_J_4KCOVSA,'Not Set'),
  ifnull(mck.MARC_J_3AMSTATC,'NotSet'),
  ifnull(mck.MARC_J_3AMSTAT,'NotSet'),
  ifnull(mck.MARC_J_4KCOVS,'NotSet'),
  ifnull(mck.MARC_J_3ADISPKZ,'NotSet'),
  ifnull(mck.MARC_J_3AHORIZ,0),
  ifnull(mck.MARC_J_3APRFC,'NotSet'),
  ifnull(mck.MARC_J_3APRFG,'NotSet'),
  ifnull(mck.MARC_J_3APRFT,'NotSet'),
  ifnull(mck.MARC_J_3ASGRID,'NotSet'),
  ifnull(mck.MARC_J_3AVERPR,'NotSet'),
  ifnull(mck.MARC_J_3AVERMR,'NotSet'),
  ifnull(mck.MARC_J_3AVERSS,'NotSet'),
  ifnull(mck.MARC_J_3AVERPU,'NotSet'),
  ifnull(mck.MARC_J_3AVERSD,'NotSet'),
  ifnull(mck.MARC_J_3AEKORG,'NotSet'),
  ifnull(mck.MARC_J_3APRCM,'NotSet'),
  ifnull(mck.MARC_J_3ABUND,'NotSet'),
  ifnull(mck.MARC_J_3ACHINT,'NotSet'),
  ifnull(mck.MARC_J_3ABAINT,'NotSet'),
  ifnull(mck.MARC_J_3ARISK,'NotSet'),
  ifnull(mck.MARC_J_3ADEFSI,'NotSet'),
  ifnull(mck.MARC_J_4KDEFSC,'NotSet'),
  ifnull(mck.MARC_J_4AATPSI,'NotSet'),
  ifnull(mck.MARC_J_3ASLHMOD,'NotSet'),
  ifnull(mck.MARC_J_3AMROUND,'NotSet'),
  ifnull(mck.MARC_J_3AMRDMENG,0.000),
  ifnull(mck.MARC_J_3AMIND,'NotSet')
     FROM    MARA_MARC_MAKT mck
          INNER JOIN
             dim_plant dp
          ON dp.plantcode = mck.marc_werks, -- AND dp.languagekey = makt_spras
          tmp_dim_MARA_MARC_MAKT_ins p
    WHERE ifnull(mck.MARC_WERKS,'NULL') <> 'NULL'
    AND mck.MARA_MATNR = p.MARA_MATNR
    AND mck.MARC_WERKS = p.MARC_WERKS) t;
    

	
UPDATE dim_part dp 
	SET  
dp.PartNumber_NoLeadZero= ifnull(case when length(dp.partnumber)=18 and dp.partnumber is integer then trim(leading '0' from dp.partnumber) else dp.partnumber end,'Not Set'),
			dp.dw_update_date = current_timestamp;	


update dim_part dp
from marc ds
set dp.loadinggroup = ifnull(ds.marc_ladgr,'Not Set'),
	dp.dw_update_date = current_timestamp
where     dp.partnumber = ds.marc_matnr
	  and dp.plant = marc_werks
	  and dp.loadinggroup <> ifnull(ds.marc_ladgr,'Not Set');


call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_ins-tmp_dim_MARA_MARC_MAKT_ins');
call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_del-tmp_dim_MARA_MARC_MAKT_del');


UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET VERPR = MBEW_VERPR 
WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND VERPR <> MBEW_VERPR;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET STPRS = MBEW_STPRS
WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON 
       AND STPRS <> MBEW_STPRS;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET SALK3 = MBEW_SALK3 
 WHERE     m.MATNR = mmm.MBEW_MATNR
        AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND SALK3 <> MBEW_SALK3;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET PEINH = MBEW_PEINH 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND PEINH <> MBEW_PEINH;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_LBKUM = mmm.MBEW_LBKUM 
       WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_LBKUM <> mmm.MBEW_LBKUM;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET LFMON = MBEW_LFMON 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND LFMON <> MBEW_LFMON;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_VMSTP = mmm.MBEW_VMSTP
       WHERE m.MATNR = mmm.MBEW_MATNR
	AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.MBEW_VMSTP <> mmm.MBEW_VMSTP
       AND m.LFMON = mmm.MBEW_LFMON;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET VPRSV = MBEW_VPRSV 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND VPRSV <> MBEW_VPRSV 
       AND m.LFMON = mmm.MBEW_LFMON;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_VJSTP = mmm.MBEW_VJSTP 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_VJSTP <> mmm.MBEW_VJSTP;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_VMPEI = mmm.MBEW_VMPEI 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND  m.MBEW_VMPEI <> mmm.MBEW_VMPEI;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_VJPEI = mmm.MBEW_VJPEI 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND  m.MBEW_VJPEI <> mmm.MBEW_VJPEI;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET LVORM = MBEW_LVORM 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND LVORM <> MBEW_LVORM;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET        m.MBEW_ZPLD1 = mmm.MBEW_ZPLD1 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_ZPLD1 <> mmm.MBEW_ZPLD1;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET        m.MBEW_KALN1 = mmm.MBEW_KALN1 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON;
CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET        m.MBEW_BWPRH = mmm.MBEW_BWPRH
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_BWPRH <> mmm.MBEW_BWPRH;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET        m.MBEW_LPLPR = mmm.MBEW_LPLPR 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON;
CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_VMVER = mmm.MBEW_VMVER 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_VMVER <> mmm.MBEW_VMVER;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_ZPLP1 = mmm.MBEW_ZPLP1 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON;

CALL VECTORWISE (COMBINE 'MBEW');
       
UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_STPRV = mmm.MBEW_STPRV 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_STPRV <> mmm.MBEW_STPRV;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_VPLPR = mmm.MBEW_VPLPR 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON;
CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_LAEPR = mmm.MBEW_LAEPR 
       WHERE     m.MATNR = mmm.MBEW_MATNR 
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND  m.MBEW_LAEPR <> mmm.MBEW_LAEPR;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_ZPLD2 = mmm.MBEW_ZPLD2 
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_ZPLD2 <> mmm.MBEW_ZPLD2;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET m.MBEW_BWVA2 = mmm.MBEW_BWVA2
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_BWVA2 <> mmm.MBEW_BWVA2;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET 	m.MBEW_KALNR = mmm.MBEW_KALNR 
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_KALNR <> mmm.MBEW_KALNR;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET 	m.MARA_LAEDA = mmm.MARA_LAEDA
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MARA_LAEDA <> mmm.MARA_LAEDA;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET 	m.MBEW_ZPLPR = mmm.MBEW_ZPLPR
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND 	m.MBEW_ZPLPR <> mmm.MBEW_ZPLPR;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET 	m.MBEW_PDATL = mmm.MBEW_PDATL 
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_PDATL <> mmm.MBEW_PDATL;

CALL VECTORWISE (COMBINE 'MBEW');

UPDATE MBEW m FROM MARA_MARC_MBEW mmm
   SET 	m.MBEW_PPRDL = mmm.MBEW_PPRDL
 WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_PPRDL <> mmm.MBEW_PPRDL;

CALL VECTORWISE (COMBINE 'MBEW');

DROP TABLE IF EXISTS MBEW_000;

CREATE TABLE MBEW_000
AS
   SELECT MATNR,
          BWKEY,
          LFGJA,
          ifnull(mb.BWTAR, 'Not Set') BWTAR,
          LFMON
     FROM MBEW mb;

INSERT INTO MBEW(VERPR,
                 STPRS,
                 SALK3,
                 PEINH,
                 MBEW_LBKUM,
                 LFMON,
                 LFGJA,
                 MBEW_VMSTP,
                 VPRSV,
                 MBEW_VJSTP,
                 MBEW_VMPEI,
                 MBEW_VJPEI,
                 LVORM,
                 MATNR,
                 BWTAR,
                 BWKEY,
                 MBEW_ZPLD1,
                 MBEW_KALN1,
                 MBEW_BWPRH,
                 MBEW_LPLPR,
                 MBEW_VMVER,
                 MBEW_ZPLP1,
                 MBEW_STPRV,
                 MBEW_VPLPR,
                 MBEW_LAEPR,
                 MBEW_ZPLD2,
                 MBEW_BWVA2,
                 MBEW_KALNR,
                 MARA_LAEDA,
                 MBEW_ZPLPR,
                 MBEW_PDATL,
                 MBEW_PPRDL)
   SELECT DISTINCT MBEW_VERPR,
                   MBEW_STPRS,
                   MBEW_SALK3,
                   MBEW_PEINH,
                   MBEW_LBKUM,
                   MBEW_LFMON,
                   MBEW_LFGJA,
                   MBEW_VMSTP,
                   MBEW_VPRSV,
                   MBEW_VJSTP,
                   MBEW_VMPEI,
                   MBEW_VJPEI,
                   MBEW_LVORM,
                   MBEW_MATNR,
                   MBEW_BWTAR,
                   MBEW_BWKEY,
                   MBEW_ZPLD1,
                   MBEW_KALN1,
                   MBEW_BWPRH,
                   MBEW_LPLPR,
                   MBEW_VMVER,
                   MBEW_ZPLP1,
                   MBEW_STPRV,
                   MBEW_VPLPR,
                   MBEW_LAEPR,
                   MBEW_ZPLD2,
                   MBEW_BWVA2,
                   MBEW_KALNR,
                   MARA_LAEDA,
                   MBEW_ZPLPR,
                   MBEW_PDATL,
                   MBEW_PPRDL
     FROM MARA_MARC_MBEW mmm
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM MBEW_000 mb
                   WHERE     mb.MATNR = mmm.MBEW_MATNR
                         AND mb.BWKEY = mmm.MBEW_BWKEY
                         AND mb.LFGJA = mmm.MBEW_LFGJA
                         AND BWTAR = ifnull(mmm.MBEW_BWTAR, 'Not Set')
                         AND mb.LFMON = mmm.MBEW_LFMON);

DROP TABLE IF EXISTS MBEW_000;

CALL vectorwise(combine 'mbew_no_bwtar-mbew_no_bwtar');

DROP TABLE IF EXISTS SALK3_000;

CREATE TABLE SALK3_000
AS
   SELECT a.MATNR,
          a.BWKEY,
          ((a.LFGJA * 100) + a.LFMON) LFGJA_LFMON,
          max(a.SALK3) MAXSALKS,
          max(a.MBEW_KALNR) MAXMBEW_KALNR
     FROM mbew a
    WHERE a.BWTAR IS NULL
   GROUP BY a.MATNR, a.BWKEY, ((a.LFGJA * 100) + a.LFMON);

INSERT INTO mbew_no_bwtar(BWKEY,
                          MATNR,
                          LVORM,
                          VPRSV,
                          VERPR,
                          STPRS,
                          LFGJA,
                          LFMON,
                          PEINH,
                          SALK3,
                          BWTAR,
                          MBEW_LBKUM,
                          MBEW_VMPEI,
                          MBEW_VMSTP,
                          MBEW_VJSTP,
                          MBEW_VJPEI,
                          MBEW_ZPLD1,
                          MBEW_KALN1,
                          MBEW_VMVER,
                          MBEW_STPRV,
                          MBEW_LAEPR,
                          MBEW_BWPRH,
                          MBEW_ZPLP1,
                          MBEW_ZPLD2,
                          MBEW_VPLPR,
                          MBEW_LPLPR,
                          MBEW_BWVA2,
                          MBEW_KALNR,
                          MARA_LAEDA,
                          MBEW_ZPLPR,
                          MBEW_PDATL,
                          MBEW_PPRDL)
   SELECT DISTINCT BWKEY,
                   MATNR,
                   LVORM,
                   VPRSV,
                   VERPR,
                   STPRS,
                   LFGJA,
                   LFMON,
                   PEINH,
                   SALK3,
                   BWTAR,
                   MBEW_LBKUM,
                   MBEW_VMPEI,
                   MBEW_VMSTP,
                   MBEW_VJSTP,
                   MBEW_VJPEI,
                   NULL MBEW_ZPLD1,
                   MBEW_KALN1,
                   MBEW_VMVER,
                   MBEW_STPRV,
                   NULL MBEW_LAEPR,
                   MBEW_BWPRH,
                   MBEW_ZPLP1,
                   NULL MBEW_ZPLD2,
                   MBEW_VPLPR,
                   MBEW_LPLPR,
                   MBEW_BWVA2,
                   MBEW_KALNR,
                   NULL MARA_LAEDA,
                   MBEW_ZPLPR,
                   MBEW_PDATL,
                   MBEW_PPRDL
     FROM mbew m
    WHERE BWTAR IS NULL
          AND SALK3 IN
                 (SELECT MAXSALKS
                    FROM SALK3_000 a
                   WHERE     a.MATNR = m.MATNR
                         AND a.BWKEY = m.BWKEY
                         AND LFGJA_LFMON = ((m.LFGJA * 100) + m.LFMON))
          AND MBEW_KALNR IN
                 (SELECT MAXMBEW_KALNR
                    FROM SALK3_000 a
                   WHERE     a.MATNR = m.MATNR
                         AND a.BWKEY = m.BWKEY
                         AND LFGJA_LFMON = ((m.LFGJA * 100) + m.LFMON));

DROP TABLE IF EXISTS SALK3_000;

CALL vectorwise(combine 'MBEW');
CALL vectorwise(combine 'mbew_no_bwtar');


UPDATE dim_part dpt from mara_marc_makt mck, dim_plant p
   SET dpt.AFSTargetGroup = ifnull(MARA_J_3AGEND, 'Not Set'),
       dpt.AFSTargetGroupDescription =
          ifnull((SELECT atg.J_3AGENDRT_TEXT
                    FROM j_3agendrt atg
                   WHERE atg.J_3AGENDRT_J_3AGEND = MARA_J_3AGEND),
                 'Not Set'),
			dpt.dw_update_date = current_timestamp
 WHERE     dpt.PartNumber = mck.MARA_MATNR
       AND dpt.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
       AND p.languagekey = makt_spras;

UPDATE dim_part dpt from 
 dim_Vendor dv
 SET dpt.ExtManfacturerCode = dv.VendorMfgCode,
     dpt.ExtManfacturerDesc = dv.VendorName,
			dpt.dw_update_date = current_timestamp
WHERE dpt.ManfacturerNumber = dv.VendorNumber;     

update dim_part  
set 	ValidFrom ='0001-01-01',
	dw_update_date = current_timestamp 
where ValidFrom is null;

/* 17 feb 2015 */
update  dim_part pt from t142t t
set storageconditiondescription = ifnull(rbtxt,'Not Set')
where storageconditioncode = raube
and storageconditioncode <> ifnull(rbtxt,'Not Set');

/* 19 feb 2015 */
update dim_part pt
from T439A
set lotsizeindicator = ifnull(T439A.LOSKZ,'Not Set')
where T439A.DISLS = pt.MRPLotSize
and lotsizeindicator <> ifnull(T439A.LOSKZ,'Not Set');

update dim_part pt
from T439A
set numberofperiods = ifnull(T439A.PERAZ,0)
where T439A.DISLS = pt.MRPLotSize
and numberofperiods <> ifnull(T439A.PERAZ,0);

update dim_part pt
from T438R
set periodindicator = ifnull(T438R.RWPER,'Not Set')
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage
and periodindicator <> ifnull(T438R.RWPER,'Not Set');


update dim_part pt
from T438R
set targetcoverage1 = ifnull(T438R.RW1TG,0)
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage
and targetcoverage1 <> ifnull(T438R.RW1TG,0);

update dim_part pt
from T438R
set typeperiodlength = ifnull(T438R.RWART,'Not Set')
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage
and typeperiodlength <> ifnull(T438R.RWART,'Not Set');

/* end 19 feb 2015 */

delete from MBEWH
where exists  (select 1 from MARA_MARC_MBEWH
		where MBEWH_MATNR = MATNR and MBEWH_BWKEY = BWKEY and MBEWH_LFGJA = LFGJA and MBEWH_LFMON = LFMON
			and ifnull(MBEWH_BWTAR, 'Not Set') = ifnull(BWTAR, 'Not Set'));

INSERT INTO MBEWH(VERPR,
                  STPRS,
                  SALK3,
                  PEINH,
                  MBEWH_LBKUM,
                  LFMON,
                  LFGJA,
                  VPRSV,
                  MATNR,
                  BWTAR,
                  BWKEY)
   SELECT distinct mmh.MBEWH_VERPR,
          mmh.MBEWH_STPRS,
          mmh.MBEWH_SALK3,
          mmh.MBEWH_PEINH,
          mmh.MBEWH_LBKUM,
          mmh.MBEWH_LFMON,
          mmh.MBEWH_LFGJA,
          mmh.MBEWH_VPRSV,
          mmh.MBEWH_MATNR,
          mmh.MBEWH_BWTAR,
          mmh.MBEWH_BWKEY
     FROM MARA_MARC_MBEWH mmh;

delete from mbewh_no_bwtar;

INSERT INTO mbewh_no_bwtar(MATNR,
                           BWKEY,
                           LFGJA,
                           LFMON,
                           VPRSV,
                           VERPR,
                           STPRS,
                           PEINH,
                           SALK3,
                           BWTAR,
                           MBEWH_LBKUM)
   SELECT distinct MATNR,
          BWKEY,
          LFGJA,
          LFMON,
          VPRSV,
          VERPR,
          STPRS,
          PEINH,
          SALK3,
          BWTAR,
          MBEWH_LBKUM
     FROM mbewh
     WHERE ifnull(BWTAR,'null') <> 'null';

call vectorwise(combine 'mbewh');
call vectorwise(combine 'mbewh_no_bwtar');
	   