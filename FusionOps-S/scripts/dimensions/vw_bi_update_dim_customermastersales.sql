UPDATE dim_customermastersales FROM KNVV
   SET ABCClassification = ifnull(KNVV_KLABC, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

UPDATE dim_customermastersales FROM KNVV
   SET AccountAssignmentGroup = ifnull(KNVV_KTGRD, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

UPDATE dim_customermastersales FROM KNVV, TVKTT
   SET AccountAssignmentGroupDesciption = ifnull(TVKTT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND tvktt.TVKTT_KTGRD = KNVV_KTGRD;

UPDATE dim_customermastersales FROM KNVV
   SET SalesOffice = ifnull(knvv_VKBUR, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

UPDATE dim_customermastersales FROM KNVV, TVKBT
   SET SalesOfficeDescription = ifnull(TVKBT_BEZEI, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_VKBUR = TVKBT_VKBUR;

UPDATE dim_customermastersales FROM KNVV
   SET SalesDistrict = ifnull(KNVV_BZIRK, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

UPDATE dim_customermastersales FROM KNVV, T171T
   SET SalesDistrictDescription = ifnull(T171T_BZTXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_BZIRK = T171T_BZIRK;

UPDATE dim_customermastersales FROM KNVV
   SET CustomerGroup = ifnull(KNVV_KDGRP, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

UPDATE dim_customermastersales FROM KNVV, T151T
   SET CustomerGroupDescription = ifnull(T151T_KTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND knvv.knvv_KDGRP = T151T_KDGRP;

UPDATE dim_customermastersales FROM KNVV
   SET PaymentTerms = ifnull(KNVV_ZTERM, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

UPDATE dim_customermastersales FROM KNVV, KNA1 k
   SET CustomerName = ifnull(k.KNA1_NAME1, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNA1_KUNNR = knvv.knvv_KUNNR;

UPDATE dim_customermastersales FROM KNVV, TVTWT
   SET DistributionChannelDescription = ifnull(TVTWT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVTWT_VTWEG = knvv.KNVV_VTWEG;

UPDATE dim_customermastersales FROM KNVV, TSPAT
   SET Division = ifnull(TSPAT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TSPAT_SPART = knvv.KNVV_SPART;

UPDATE dim_customermastersales FROM KNVV, TVKOT
   SET SalesOrgDescription = ifnull(TVKOT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVKOT_VKORG = knvv.KNVV_VKORG;


UPDATE dim_customermastersales FROM KNVV
   SET Customergroup3 = ifnull(KNVV_KVGR3, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND ifnull(Customergroup3,'X') <> ifnull(KNVV_KVGR3, 'Not Set');
       

       
 /* Start New fields added as a part of Columbia Report 856 */ 
UPDATE dim_customermastersales FROM KNVV
   SET pricingProcedure = ifnull(KNVV_KALKS, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND pricingProcedure <> ifnull(KNVV_KALKS, 'Not Set'); 
	   
UPDATE dim_customermastersales FROM KNVV
   SET deliveryPriority = ifnull(KNVV_LPRIO,1.0000),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND deliveryPriority <> ifnull(KNVV_LPRIO,1.0000);   

UPDATE dim_customermastersales FROM KNVV
   SET incoTerm2 = ifnull(KNVV_INCO2, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND incoTerm2 <> ifnull(KNVV_INCO2, 'Not Set');   
	   
UPDATE dim_customermastersales FROM KNVV
   SET maxPartialDelivery = ifnull(KNVV_ANTLF,1.0000),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND maxPartialDelivery <> ifnull(KNVV_ANTLF,1.0000);
	   
UPDATE dim_customermastersales FROM KNVV
   SET storeLocator = ifnull(KNVV_AGREL, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND storeLocator <> ifnull(KNVV_AGREL, 'Not Set');

UPDATE dim_customermastersales FROM KNVV
   SET customergroup5 = ifnull(KNVV_KVGR5, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup5 <> ifnull(KNVV_KVGR5, 'Not Set');	
	   
UPDATE dim_customermastersales FROM KNVV
   SET customergroup2 = ifnull(KNVV_KVGR2, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup2 <> ifnull(KNVV_KVGR2, 'Not Set');	
	   
UPDATE dim_customermastersales FROM KNVV
   SET customergroup1 = ifnull(KNVV_KVGR1, 'Not Set'),
			dw_update_date = current_timestamp
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup1 <> ifnull(KNVV_KVGR1, 'Not Set');	   