INSERT INTO dim_salesoffice(Dim_SalesOfficeid,
                             SalesOfficeCode,
                             SalesOfficeName,
                             RowStartDate,
                             RowEndDate,
                             RowIsCurrent,
                             RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          current_timestamp,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesoffice
               WHERE Dim_SalesOfficeid = 1);

UPDATE    dim_salesoffice so
       FROM
          TVKBT t
   SET so.SalesOfficeName = t.TVKBT_BEZEI,
			so.dw_update_date = current_timestamp
   WHERE so.SalesOfficeCode = TVKBT_VKBUR;

delete from number_fountain m where m.table_name = 'dim_salesoffice';

insert into number_fountain
select 	'dim_salesoffice',
	ifnull(max(d.Dim_SalesOfficeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesoffice d
where d.Dim_SalesOfficeid <> 1;    
   
INSERT INTO dim_salesoffice(Dim_SalesOfficeid,
                            SalesOfficeCode,
                            SalesOfficeName,
                            RowStartDate,
                            RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesoffice') 
          + row_number() over(),
		 TVKBT_VKBUR,
          TVKBT_BEZEI,
          current_timestamp,
          1
     FROM TVKBT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesoffice so
               WHERE so.SalesOfficeCode = TVKBT_VKBUR);