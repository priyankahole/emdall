
INSERT INTO dim_accountsreceivabledocstatus(dim_accountsreceivabledocstatusid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_accountsreceivabledocstatus
               WHERE dim_accountsreceivabledocstatusid = 1);

/*New Update Statement*/
UPDATE	dim_accountsreceivabledocstatus ards
FROM	DD07T d
SET 	ards.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
		ards.dw_update_date = current_timestamp
WHERE	ards.Status = d.DD07T_DOMVALUE
        AND d.DD07T_DOMNAME = 'BSTAT'
        AND d.DD07T_DOMVALUE IS NOT NULL
        AND ards.RowIsCurrent = 1;
				
delete from number_fountain m where m.table_name = 'dim_accountsreceivabledocstatus';

insert into number_fountain				   
select 	'dim_accountsreceivabledocstatus',
	ifnull(max(d.dim_accountsreceivabledocstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_accountsreceivabledocstatus d
where d.dim_accountsreceivabledocstatusid <> 1;
			   
INSERT INTO dim_accountsreceivabledocstatus(Dim_AccountsReceivableDocStatusid,
                               Description,
                               Status,
                               RowStartDate,
                               RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_accountsreceivabledocstatus')
          + row_number() over() ,
                        ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'BSTAT' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_accountsreceivabledocstatus
                    WHERE Status = DD07T_DOMVALUE AND DD07T_DOMNAME = 'BSTAT')
   ORDER BY 2 OFFSET 0;

delete from number_fountain m where m.table_name = 'dim_accountsreceivabledocstatus';
   