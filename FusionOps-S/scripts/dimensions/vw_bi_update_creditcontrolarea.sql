UPDATE    dim_creditcontrolarea cca
       FROM
          T014T t
   SET cca.CreditControlAreaName = t.T014T_KKBTX
 WHERE cca.RowIsCurrent = 1
 AND t.T014T_KKBER = cca.CreditControlAreaCode
;