insert into tmp_dim_MARA_MARC_MAKT_ins
select mck.MARA_MATNR,mck.MARC_WERKS
FROM    MARA_MARC_MAKT mck;

insert into tmp_dim_MARA_MARC_MAKT_del
select p.PartNumber,p.Plant
from dim_part p;

call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_ins-tmp_dim_MARA_MARC_MAKT_del');


DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'dim_part';

INSERT INTO NUMBER_FOUNTAIN
select 	'dim_part',
	ifnull(max(d.dim_partid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1;

INSERT INTO dim_part(dim_partid,
                     PartNumber,
                     PartDescription,
                     Revision,
                     UnitOfMeasure,
                     RowStartDate,
                     RowIsCurrent,
                     CommodityCode,
                     PartType,
                     LeadTime,
                     StockLocation,
                     PurchaseGroupCode,
                     PurchaseGroupDescription,
                     MRPController,
                     MaterialGroup,
                     Plant,
                     ABCIndicator,
                     ProcurementType,
                     StorageLocation,
                     CriticalPart,
                     MRPType,
                     SupplySource,
                     StrategyGroup,
                     TransportationGroup,
                     Division,
                     DivisionDescription,
                     GeneralItemCategory,
                     DeletionFlag,
                     MaterialStatus,
                     ProductHierarchy,
                     MPN,
                     ProductHierarchyDescription,
                     MRPProfile,
                     MRPProfileDescription,
                     PartTypeDescription,
                     MaterialGroupDescription,
                     MRPTypeDescription,
                     ProcurementTypeDescription,
                     MRPControllerDescription,
                     MRPGroup,
                     MRPGroupDescription,
                     MinimumLotSize,
                     MRPLotSize,
                     MRPLotSizeDescription,
                     MaterialStatusDescription,
                     SpecialProcurement,
                     SpecialProcurementDescription,
                     BulkMaterial,
                     InhouseProductionTime,
                     ProfitCenterCode,
                     ProfitCenterName,
                     ExternalMaterialGroupCode,
                     ExternalMaterialGroupDescription,
                     MaterialDiscontinuationFlag,
                     MaterialDiscontinuationFlagDescription,
                     ProcessingTime,
                     TotalReplenishmentLeadTime,
                     GRProcessingTime,
                     OldPartNumber,
                     AFSColor,
                     AFSColorDescription,
                     SDMaterialStatusDescription,
                     Volume,
                     AFSMasterGrid,
                     AFSPattern,
                     Laboratory,
                     DoNotCost,
                     PlantMaterialStatus,
                     PlantMaterialStatusDescription,
                     REMProfile,
                     PlanningTimeFence,
                     SafetyStock,
                     RoundingValue,
                     ReorderPoint,
                     ValidFrom,
                     MRPStatus,
                     CheckingGroupCode,
                     CheckingGroup,
                        UPCNumber,
                        MaximumStockLevel,
                        ConsumptionMode,
                        ConsumptionModeDescription,
                        ConsumptionPeriodBackward,
                        ConsumptionPeriodForward,
                        PartLongDesc,
                        UPCCode,
                        NDCCode,
						MRPControllerTelephone)
   SELECT (SELECT max_id
             FROM NUMBER_FOUNTAIN
            WHERE table_name = 'dim_part')
          + row_number()
             over(), t.* from (SELECT DISTINCT
          mck.MARA_MATNR,
          ifnull(MAKT_MAKTX, 'Not Set'),
          ifnull(MARA_KZREV, 'Not Set'),
          ifnull(MARA_MEINS, 'Not Set'),
          current_timestamp,
          1,
          ifnull(MARC_STAWN, 'Not Set'),
          ifnull(MARA_MTART, 'Not Set'),
          ifnull(MARC_PLIFZ, 0),
          ifnull(MARC_LGPRO, 'Not Set'),
          ifnull(MARC_EKGRP, 'Not Set'),
          ifnull((SELECT t.T024_EKNAM
                    FROM t024 t
                   WHERE t.T024_EKGRP = MARC_EKGRP),
                 'Not Set')
             PurchaseGroupDescription,
          ifnull(MARC_DISPO, 'Not Set'),
          ifnull(MARA_MATKL, 'Not Set'),
          mck.MARC_WERKS,
          ifnull(MARC_MAABC, 'Not Set'),
          ifnull(MARC_BESKZ, 'Not Set'),
          ifnull(MARC_LGFSB, 'Not Set'),
          ifnull(MARC_KZKRI, 'Not Set'),
          ifnull(MARC_DISMM, 'Not Set'),
          ifnull(MARC_BWSCL, 'Not Set'),
          ifnull(MARC_STRGR, 'Not Set'),
          ifnull(MARA_TRAGR, 'Not Set'),
          ifnull(MARA_SPART, 'Not Set'),
   ifnull((SELECT TSPAT_VTEXT FROM TSPAT
    WHERE TSPAT_SPART = MARA_SPART
                    AND TSPAT_SPRAS = 'E'),'Not Set'),
          ifnull(MARA_MTPOS, 'Not Set'),
          ifnull(MARC_LVORM, 'Not Set'),
          ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
          ifnull(MARA_PRDHA, 'Not Set'),
          ifnull(MARA_MFRPN, 'Not Set'),
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set')
             ProductHierarchyDescription,
          ifnull(mck.MARC_DISPR, 'Not Set') MRPProfile,
          ifnull((SELECT p.T401T_KTEXT
                    FROM t401t p
                   WHERE p.T401T_DISPR = mck.MARC_DISPR),
                 'Not Set')
             MRPProfileDescription,
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set')
             PartTypeDescription,
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set')
             MaterialGroupDescription,
          ifnull((SELECT mt.T438T_DIBEZ
                    FROM T438T mt
                   WHERE mt.T438T_DISMM = mck.MARC_DISMM),
                 'Not Set')
             MRPTypeDescription,
          ifnull(
             (SELECT dd.DD07T_DDTEXT
                FROM DD07T dd
               WHERE dd.DD07T_DOMNAME = 'BESKZ'
                     AND dd.DD07T_DOMVALUE = MARC_BESKZ),
             'Not Set')
             ProcurementTypeDescription,
          ifnull(
             (SELECT mc.T024D_DSNAM
                FROM T024D mc
               WHERE mc.T024D_DISPO = mck.MARC_DISPO
                     AND mc.T024D_WERKS = mck.MARC_WERKS),
             'Not Set')
             MRPControllerDescription,
          ifnull(MARC_DISGR, 'Not Set') MRPGroup,
          ifnull(
             (SELECT mpg.T438X_TEXT40
                FROM T438X mpg
               WHERE mpg.T438X_DISGR = mck.MARC_DISGR
                     AND mpg.T438X_WERKS = mck.MARC_WERKS),
             'Not Set')
             MRPGroupDescription,
          ifnull(mck.MARC_BSTMI,0.0000) MinimumLotSize,
          ifnull(mck.MARC_DISLS, 'Not Set') MRPLotSize,
          ifnull((SELECT ml.T439T_LOSLT
                    FROM T439T ml
                   WHERE ml.T439T_DISLS = mck.MARC_DISLS),
                 'Not Set')
             MRPLotSizeDescription,
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set')
             materialstatusdescription,
          ifnull(mck.MARC_SOBSL, 'Not Set'),
          ifnull(
             (SELECT sp.T460T_LTEXT
                FROM t460t sp
               WHERE sp.T460T_SOBSL = mck.MARC_SOBSL
                     AND sp.T460T_WERKS = mck.MARC_WERKS),
             'Not Set'),
          ifnull(MARC_SCHGT, 'Not Set'),
          mck.MARC_DZEIT,
          ifnull(MARC_PRCTR, 'Not Set'),
          ifnull(
             (SELECT c.CEPCT_KTEXT
                FROM cepct c
               WHERE c.CEPCT_PRCTR = MARC_PRCTR AND c.CEPCT_DATBI > current_date),
             'Not Set'),
          ifnull(MARA_EXTWG, 'Not Set'),
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set'),
          ifnull(MARC_KZAUS, 'Not Set'),
          ifnull(
             (SELECT DD07T_DDTEXT
                FROM DD07T
               WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS),
             'Not Set'),
          mck.MARC_BEARZ,
          mck.MARC_WZEIT,
   mck.MARC_WEBAZ,
   ifnull(mck.MARA_BISMT, 'Not Set'),
   ifnull(mck.MARA_J_3ACOL, 'Not Set'),
   ifnull((SELECT J_3ACOLRT_TEXT
                FROM J_3ACOLRT
               WHERE J_3ACOLRT_J_3ACOL = mck.MARA_J_3ACOL), 'Not Set'),
  ifnull((SELECT TVMST_VMSTB
     FROM tvmst
       WHERE TVMST_VMSTA = MARA_MSTAV), 'Not Set'),
  MARA_VOLUM,
  ifnull(MARA_J_3APGNR, 'Not Set'),
  ifnull(MARA_AFS_SCHNITT, 'Not Set'),
  ifnull(MARA_LABOR, 'Not Set'),
  ifnull(MARC_NCOST,'Not Set'),
  ifnull(mck.MARC_MMSTA,'Not Set'),
     ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = mck.MARC_MMSTA), 'Not Set'),
  ifnull(MARC_SFEPR,'Not Set'),
  ifnull(MARC_FXHOR,0),
  ifnull(MARC_EISBE,0.0000),
     ifnull(MARC_BSTRF,0.0000),
     ifnull(MARC_MINBE,0.0000),
  (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and j1.J_3AMAD_WERKS = mck.MARC_WERKS),
  ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
  ifnull(mck.MARC_MTVFP, 'Not Set'),
  ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
  ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set'),
  ifnull(mck.MARC_MABST, 0.0000),
  ifnull(mck.MARC_VRMOD, 'Not Set'),
  ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
  ifnull(mck.MARC_VINT1, 0),
  ifnull(mck.MARC_VINT2, 0),
  ifnull(mck.MAKT_MAKTG,'Not Set'),
  ifnull(mck.MARA_EAN11,'Not Set'),
  ifnull(mck.MARA_GROES,'Not Set'),
  ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set') MRPControllerTelephone
     FROM    MARA_MARC_MAKT mck
          INNER JOIN
             dim_plant dp
          ON dp.plantcode = mck.marc_werks, -- AND dp.languagekey = makt_spras
          tmp_dim_MARA_MARC_MAKT_ins p
    WHERE ifnull(mck.MARC_WERKS,'NULL') <> 'NULL'
    AND mck.MARA_MATNR = p.MARA_MATNR
    AND mck.MARC_WERKS = p.MARC_WERKS) t;

call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_ins-tmp_dim_MARA_MARC_MAKT_ins');
call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_del-tmp_dim_MARA_MARC_MAKT_del');