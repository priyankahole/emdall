UPDATE    dim_inspectionseverity isv
       FROM
          QDEPT q
   SET isv.InspectionSeverityName = q.QDEPT_KURZTEXT
 WHERE isv.RowIsCurrent = 1
 AND isv.InspectionSeverityCode = q.QDEPT_PRSCHAERFE;
