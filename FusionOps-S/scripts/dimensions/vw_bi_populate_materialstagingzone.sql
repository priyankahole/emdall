UPDATE    Dim_MaterialStagingZone ms
       FROM
          T30CT t
   SET ms.MSAreaTxt = ifnull(t.T30CT_LBZOT, 'Not Set'),
			ms.dw_update_date = current_timestamp
 WHERE ms.RowIsCurrent = 1
 AND ms.WarehouseCode = t.T30CT_LGNUM AND ms.StagingArea = t.T30CT_LGBZO;

INSERT INTO Dim_MaterialStagingZone(Dim_MaterialStagingZoneId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM Dim_MaterialStagingZone
               WHERE Dim_MaterialStagingZoneId = 1);

delete from number_fountain m where m.table_name = 'Dim_MaterialStagingZone';
   
insert into number_fountain
select 	'Dim_MaterialStagingZone',
	ifnull(max(d.Dim_MaterialStagingZoneId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_MaterialStagingZone d
where d.Dim_MaterialStagingZoneId <> 1; 

INSERT INTO Dim_MaterialStagingZone(Dim_MaterialStagingZoneId,
									WarehouseCode,
                                    StagingArea,
                                    MSAreaTxt,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_MaterialStagingZone') 
          + row_number() over() ,
			 t.T30CT_LGNUM,
          t.T30CT_LGBZO,
          t.T30CT_LBZOT,
          current_timestamp,
          1
     FROM T30CT t
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM Dim_MaterialStagingZone ms
                   WHERE     ms.WarehouseCode = t.T30CT_LGNUM
                         AND ms.StagingArea = t.T30CT_LGBZO
                         AND ms.RowIsCurrent = 1);
