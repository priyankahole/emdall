
UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
	  INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET PartNumber = mv.MARA_MATNR,
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E' 
	AND p.PartNumber <> mv.MARA_MATNR ;



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.PartDescription <> ifnull(MAKT_MAKTX, 'Not Set') ;


UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET Revision = ifnull(MARA_KZREV, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.Revision <> ifnull(MARA_KZREV, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.UnitOfMeasure <> ifnull(MARA_MEINS, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET RowStartDate = current_timestamp,
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E';



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET RowIsCurrent =  1,
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.RowIsCurrent <> 1;



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup = ifnull(MARA_MATKL, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialGroup<> ifnull(MARA_MATKL, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.TransportationGroup <> ifnull(MARA_TRAGR, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET Division = ifnull(MARA_SPART, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.Division <> ifnull(MARA_SPART, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.GeneralItemCategory <> ifnull(MARA_MTPOS, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialStatus = ifnull(MARA_MSTAE, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.MaterialStatus <>  ifnull(MARA_MSTAE, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.ProductHierarchy <> ifnull(MARA_PRDHA, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MPN = ifnull(MARA_MFRPN, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MPN <> ifnull(MARA_MFRPN, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET ProductHierarchyDescription = ifnull((SELECT t.VTEXT FROM t179t t WHERE t.PRODH = mv.MARA_PRDHA), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.ProductHierarchyDescription <>  ifnull((SELECT t.VTEXT FROM t179t t WHERE t.PRODH = mv.MARA_PRDHA), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET PartType = ifnull(MARA_MTART, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.PartType <> ifnull(MARA_MTART, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET PartTypeDescription = ifnull((SELECT pt.T134T_MTBEZ FROM T134T pt  WHERE pt.T134T_MTART = mv.MARA_MTART), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.PartTypeDescription <> ifnull((SELECT pt.T134T_MTBEZ FROM T134T pt  WHERE pt.T134T_MTART = mv.MARA_MTART), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroupDescription = ifnull((SELECT mg.T023T_WGBEZ FROM T023T mg WHERE mg.T023T_MATKL = mv.MARA_MATKL), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialGroupDescription <> ifnull((SELECT mg.T023T_WGBEZ FROM T023T mg WHERE mg.T023T_MATKL = mv.MARA_MATKL), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialStatusDescription = ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA =  mv.MARA_MSTAE), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialStatusDescription <> ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA =  mv.MARA_MSTAE), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.ExternalMaterialGroupCode <> ifnull(MARA_EXTWG, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET ExternalMaterialGroupDescription = ifnull((SELECT t.TWEWT_EWBEZ FROM twewt t WHERE t.TWEWT_EXTWG = MARA_EXTWG), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.ExternalMaterialGroupDescription  <> ifnull((SELECT t.TWEWT_EWBEZ FROM twewt t WHERE t.TWEWT_EXTWG = MARA_EXTWG), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET OldPartNumber = ifnull(mv.MARA_BISMT, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.OldPartNumber <> ifnull(mv.MARA_BISMT, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.AFSColor <> ifnull(MARA_J_3ACOL, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET AFSColorDescription = ifnull((SELECT J_3ACOLRT_TEXT FROM J_3ACOLRT WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.AFSColorDescription  <> ifnull((SELECT J_3ACOLRT_TEXT FROM J_3ACOLRT WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET SDMaterialStatusDescription = ifnull((SELECT TVMST_VMSTB FROM tvmst WHERE TVMST_VMSTA = MARA_MSTAV), 'Not Set'),
			p.dw_update_date = current_timestamp 
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.SDMaterialStatusDescription <> ifnull((SELECT TVMST_VMSTB FROM tvmst WHERE TVMST_VMSTA = MARA_MSTAV), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET Volume = MARA_VOLUM,
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.Volume <>  MARA_VOLUM;



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.AFSMasterGrid <> ifnull(MARA_J_3APGNR, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.AFSPattern  <> ifnull(MARA_AFS_SCHNITT, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET SalesOrgCode = ifnull(MVKE_VKORG, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.SalesOrgCode <> ifnull(MVKE_VKORG, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET SalesOrg = ifnull((SELECT TVKOT_VTEXT FROM TVKOT WHERE TVKOT_VKORG = mv.MVKE_VKORG), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.SalesOrg  <>  ifnull((SELECT TVKOT_VTEXT FROM TVKOT WHERE TVKOT_VKORG = mv.MVKE_VKORG), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DistributionChannelCode = ifnull(MVKE_VTWEG, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.DistributionChannelCode <> ifnull(MVKE_VTWEG, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DistributionChannel = ifnull((SELECT TVTWT_VTEXT FROM TVTWT WHERE TVTWT_VTWEG = mv.MVKE_VTWEG), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.DistributionChannel <>  ifnull((SELECT TVTWT_VTEXT FROM TVTWT WHERE TVTWT_VTWEG = mv.MVKE_VTWEG), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET CSDMaterialStatusCode = ifnull(MVKE_VMSTA, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.CSDMaterialStatusCode <> ifnull(MVKE_VMSTA, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET CSDMaterialStatus = ifnull((SELECT TVMST_VMSTB FROM TVMST WHERE TVMST_VMSTA = mv.MVKE_VMSTA), 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.CSDMaterialStatus <> ifnull((SELECT TVMST_VMSTB FROM TVMST WHERE TVMST_VMSTA = mv.MVKE_VMSTA), 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET SalesGridNumber = ifnull((SELECT J_3AGRHD_J_3APGNR
          FROM KOTJ010, J_3AGRHD
         WHERE KOTJ010_VTWEG = mv.MVKE_VTWEG
		   AND KOTJ010_VKORG = mv.MVKE_VKORG
		   AND KOTJ010_MATNR = mv.MARA_MATNR
		   AND J_3AGRHD_KNUMH = KOTJ010_KNUMH),
       'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.SalesGridNumber <> ifnull((SELECT J_3AGRHD_J_3APGNR
          FROM KOTJ010, J_3AGRHD
         WHERE KOTJ010_VTWEG = mv.MVKE_VTWEG
                   AND KOTJ010_VKORG = mv.MVKE_VKORG
                   AND KOTJ010_MATNR = mv.MARA_MATNR
                   AND J_3AGRHD_KNUMH = KOTJ010_KNUMH),
       'Not Set') ;



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup1 = ifnull(mv.MVKE_MVGR1, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialGroup1  <> ifnull(mv.MVKE_MVGR1, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup2 = ifnull(mv.MVKE_MVGR2, 'Not Set'),
			p.dw_update_date = current_timestamp 
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.MaterialGroup2 <> ifnull(mv.MVKE_MVGR2, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup3 = ifnull(mv.MVKE_MVGR3, 'Not Set'),
			p.dw_update_date = current_timestamp 
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND  p.MaterialGroup3 <> ifnull(mv.MVKE_MVGR3, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup4 = ifnull(mv.MVKE_MVGR4, 'Not Set'),
			p.dw_update_date = current_timestamp              
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialGroup4 <> ifnull(mv.MVKE_MVGR4, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialGroup5 = ifnull(mv.MVKE_MVGR5, 'Not Set'),
			p.dw_update_date = current_timestamp 
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialGroup5 <> ifnull(mv.MVKE_MVGR5, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DChainValidFromDt = mv.MVKE_VMSTD,
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.DChainValidFromDt <> mv.MVKE_VMSTD;



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DeliveryPlant = ifnull(mv.MVKE_DWERK, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.DeliveryPlant <> ifnull(mv.MVKE_DWERK, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialPricingGroup = ifnull(mv.MVKE_KONDM, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialPricingGroup  <> ifnull(mv.MVKE_KONDM, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET RoundingProfle = ifnull(mv.MVKE_RDPRF, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.RoundingProfle <> ifnull(mv.MVKE_RDPRF, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET CommissionGroup = ifnull(mv.MVKE_PROVG, 'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.CommissionGroup <> ifnull(mv.MVKE_PROVG, 'Not Set');



UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET DeliveryUnits = ifnull(mv.MVKE_SCMNG,0),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
	AND p.SalesOrgCode = mv.MVKE_VKORG
	AND p.DistributionChannelCode = mv.MVKE_VTWEG
	AND mv.MAKT_SPRAS = 'E'
	AND p.DeliveryUnits <> ifnull(mv.MVKE_SCMNG,0);


UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup1Description = ifnull((SELECT TVM1T_BEZEI FROM TVM1T t1 WHERE t1.TVM1T_MVGR1 =  mv.MVKE_MVGR1),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup1Description <> ifnull((SELECT TVM1T_BEZEI FROM TVM1T t1 WHERE t1.TVM1T_MVGR1 =  mv.MVKE_MVGR1),'Not Set');

UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup2Description = ifnull((SELECT TVM2T_BEZEI FROM TVM2T t2 WHERE t2.TVM2T_MVGR2 =  mv.MVKE_MVGR2),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup2Description <> ifnull((SELECT TVM2T_BEZEI FROM TVM2T t2 WHERE t2.TVM2T_MVGR2 =  mv.MVKE_MVGR2),'Not Set');

UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup3Description = ifnull((SELECT TVM3T_BEZEI FROM TVM3T t3 WHERE t3.TVM3T_MVGR3 =  mv.MVKE_MVGR3),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup3Description <> ifnull((SELECT TVM3T_BEZEI FROM TVM3T t3 WHERE t3.TVM3T_MVGR3 =  mv.MVKE_MVGR3),'Not Set');

UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup4Description = ifnull((SELECT TVM4T_BEZEI FROM TVM4T t4 WHERE t4.TVM4T_MVGR4 =  mv.MVKE_MVGR4),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup4Description <> ifnull((SELECT TVM4T_BEZEI FROM TVM4T t4 WHERE t4.TVM4T_MVGR4 =  mv.MVKE_MVGR4),'Not Set');

UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
Set MaterialPriceGroup5Description = ifnull((SELECT TVM5T_BEZEI FROM TVM5T t5 WHERE t5.TVM5T_MVGR5 =  mv.MVKE_MVGR5),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND MaterialPriceGroup5Description  <>  ifnull((SELECT TVM5T_BEZEI FROM TVM5T t5 WHERE t5.TVM5T_MVGR5 =  mv.MVKE_MVGR5),'Not Set');


insert into dim_partsales (dim_partsalesid,rowstartdate,rowiscurrent,partnumber_noleadzero)
select 1,current_timestamp,1,'Not Set' from (select 1) as src
where
not exists (select 1 from dim_partsales where dim_partsalesid=1);		


drop table if exists subtbl_t001;
Create table subtbl_t001 as Select distinct PartNumber,SalesOrgCode,DistributionChannelCode from dim_partsales;

insert into tmp_MARAMVKEMAKT_ins
select mv.MARA_MATNR, mv.MVKE_VKORG, mv.MVKE_VTWEG
FROM    MARA_MVKE_MAKT mv;


insert into tmp_MARAMVKEMAKT_del
select p.PartNumber, p.SalesOrgCode,  p.DistributionChannelCode
from dim_partsales p;

call vectorwise (combine 'tmp_MARAMVKEMAKT_ins-tmp_MARAMVKEMAKT_del');

delete from number_fountain m where m.table_name = 'dim_partsales';

insert into number_fountain				   
select 	'dim_partsales',
	ifnull(max(d.dim_partsalesid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_partsales d
where d.dim_partsalesid <> 1;

INSERT INTO dim_partsales(dim_partsalesid,
			PartNumber,
			PartDescription,
			Revision,
			UnitOfMeasure,
			RowStartDate,
			RowIsCurrent,
			MaterialGroup,
			TransportationGroup,
			Division,
			GeneralItemCategory,
			MaterialStatus,
			ProductHierarchy,
			MPN,
			ProductHierarchyDescription,
			PartType,
			PartTypeDescription,
			MaterialGroupDescription,
			MaterialStatusDescription,
			ExternalMaterialGroupCode,
			ExternalMaterialGroupDescription,
			OldPartNumber,
			AFSColor,
			AFSColorDescription,
			SDMaterialStatusDescription,
			Volume,
			AFSMasterGrid,
			AFSPattern,
			SalesOrgCode,
			SalesOrg,
			DistributionChannelCode,
			DistributionChannel,
			CSDMaterialStatusCode,
			CSDMaterialStatus,
			SalesGridNumber,
			MaterialGroup1,
			MaterialGroup2,
			MaterialGroup3,
			MaterialGroup4,
			MaterialGroup5,
			DChainValidFromDt,
			DeliveryPlant,  
			MaterialPricingGroup,  
			RoundingProfle,
			CommissionGroup,
			DeliveryUnits,
			 MaterialPriceGroup1Description,
			 MaterialPriceGroup2Description,
			 MaterialPriceGroup3Description,
			 MaterialPriceGroup4Description,
			 MaterialPriceGroup5Description)
Select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_partsales') + row_number() over(),
	   tp.*
From (SELECT DISTINCT mv.MARA_MATNR,
			ifnull(MAKT_MAKTX, 'Not Set'),
			ifnull(MARA_KZREV, 'Not Set'),
			ifnull(MARA_MEINS, 'Not Set'),
			current_timestamp,
			1,
			ifnull(MARA_MATKL, 'Not Set'),
			ifnull(MARA_TRAGR, 'Not Set'),
			ifnull(MARA_SPART, 'Not Set'),
			ifnull(MARA_MTPOS, 'Not Set'),
			ifnull(MARA_MSTAE, 'Not Set'),
			ifnull(MARA_PRDHA, 'Not Set'),
			ifnull(MARA_MFRPN, 'Not Set'),
			ifnull((SELECT t.VTEXT
				FROM t179t t
				WHERE t.PRODH = mv.MARA_PRDHA),
				'Not Set') ProductHierarchyDescription,
			ifnull(MARA_MTART, 'Not Set') PartType,
			ifnull((SELECT pt.T134T_MTBEZ
				FROM T134T pt
				WHERE pt.T134T_MTART = mv.MARA_MTART),
				'Not Set') PartTypeDescription,
			ifnull((SELECT mg.T023T_WGBEZ
				FROM T023T mg
				WHERE mg.T023T_MATKL = mv.MARA_MATKL),
				'Not Set') MaterialGroupDescription,
			ifnull(
			(SELECT t141t_MTSTB
				FROM t141t mst
				WHERE mst.T141T_MMSTA =  mv.MARA_MSTAE),
				'Not Set') materialstatusdescription,
			ifnull(MARA_EXTWG, 'Not Set'),
			ifnull((SELECT t.TWEWT_EWBEZ
				FROM twewt t
				WHERE t.TWEWT_EXTWG = MARA_EXTWG
				),
				'Not Set'),
			ifnull(mv.MARA_BISMT, 'Not Set'),
			ifnull(MARA_J_3ACOL, 'Not Set'),
			ifnull((SELECT J_3ACOLRT_TEXT
				FROM J_3ACOLRT
				WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL), 'Not Set'),
			ifnull((SELECT TVMST_VMSTB
				FROM tvmst
				WHERE TVMST_VMSTA = MARA_MSTAV), 'Not Set'),
			MARA_VOLUM,
			ifnull(MARA_J_3APGNR, 'Not Set'),
			ifnull(MARA_AFS_SCHNITT, 'Not Set'),
			ifnull(mv.MVKE_VKORG, 'Not Set'),
			ifnull((SELECT TVKOT_VTEXT
				FROM TVKOT 
				WHERE TVKOT_VKORG = mv.MVKE_VKORG),
				'Not Set'),
			ifnull(mv.MVKE_VTWEG, 'Not Set'),				 
			ifnull((SELECT TVTWT_VTEXT
				FROM TVTWT
				WHERE TVTWT_VTWEG = mv.MVKE_VTWEG),
				'Not Set'),
			ifnull(MVKE_VMSTA, 'Not Set'),
			ifnull((SELECT TVMST_VMSTB
				FROM TVMST
				WHERE TVMST_VMSTA = mv.MVKE_VMSTA),
				'Not Set'),
			ifnull((SELECT J_3AGRHD_J_3APGNR
				FROM KOTJ010, J_3AGRHD
				WHERE KOTJ010_VTWEG = mv.MVKE_VTWEG
				AND KOTJ010_VKORG = mv.MVKE_VKORG
				AND KOTJ010_MATNR = mv.MARA_MATNR
				AND J_3AGRHD_KNUMH = KOTJ010_KNUMH),
				'Not Set'),
			ifnull(mv.MVKE_MVGR1, 'Not Set'),
			ifnull(mv.MVKE_MVGR2, 'Not Set'),
			ifnull(mv.MVKE_MVGR3, 'Not Set'),
			ifnull(mv.MVKE_MVGR4, 'Not Set'),
			ifnull(mv.MVKE_MVGR5, 'Not Set'),
			mv.MVKE_VMSTD,
			ifnull(mv.MVKE_DWERK, 'Not Set'),  
			ifnull(mv.MVKE_KONDM, 'Not Set'),  
			ifnull(mv.MVKE_RDPRF, 'Not Set'),
			ifnull(mv.MVKE_PROVG, 'Not Set'),
			ifnull(mv.MVKE_SCMNG,0),
			ifnull((SELECT TVM1T_BEZEI FROM TVM1T t1 WHERE t1.TVM1T_MVGR1 =  mv.MVKE_MVGR1),'Not Set'),
			ifnull((SELECT TVM2T_BEZEI FROM TVM2T t2 WHERE t2.TVM2T_MVGR2 =  mv.MVKE_MVGR2),'Not Set'),
			ifnull((SELECT TVM3T_BEZEI FROM TVM3T t3 WHERE t3.TVM3T_MVGR3 =  mv.MVKE_MVGR3),'Not Set'),
			ifnull((SELECT TVM4T_BEZEI FROM TVM4T t4 WHERE t4.TVM4T_MVGR4 =  mv.MVKE_MVGR4),'Not Set'),
			ifnull((SELECT TVM5T_BEZEI FROM TVM5T t5 WHERE t5.TVM5T_MVGR5 =  mv.MVKE_MVGR5),'Not Set')
FROM    MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
	INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG, tmp_MARAMVKEMAKT_ins p
WHERE mv.MARA_MATNR = p.MARA_MATNR
      AND mv.MVKE_VKORG = p.MVKE_VKORG
      AND mv.MVKE_VTWEG = p.MVKE_VTWEG
      AND mv.MAKT_SPRAS = 'E') tp;
	  
UPDATE dim_partsales p
	SET  
p.PartNumber_NoLeadZero= ifnull(case when length(p.partnumber)=18 and p.partnumber is integer then trim(leading '0' from p.partnumber) else p.partnumber end,'Not Set'),
p.dw_update_date = current_timestamp;	  

/* 29Jan2014 Added by Victor - MaterialPricingGroupDescription*/
UPDATE dim_partsales p
FROM MARA_MVKE_MAKT mv INNER JOIN dim_salesorg so
          ON so.SalesOrgCode = mv.MVKE_VKORG
          INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
SET MaterialPricingGroupDescription = IFNULL((SELECT T178T_VTEXT FROM T178T t1 WHERE t1.T178T_KONDM =  mv.MVKE_KONDM),'Not Set'),
			p.dw_update_date = current_timestamp
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	AND p.MaterialPricingGroupDescription  <> IFNULL((SELECT T178T_VTEXT FROM T178T t1 WHERE t1.T178T_KONDM =  mv.MVKE_KONDM),'Not Set');  
/* End Modification 29Jan2014 */

call vectorwise (combine 'tmp_MARAMVKEMAKT_ins-tmp_MARAMVKEMAKT_ins');
call vectorwise (combine 'tmp_MARAMVKEMAKT_del-tmp_MARAMVKEMAKT_del');


drop table if exists subtbl_t001;
