UPDATE    dim_MRPDiscontinuationIndicator mrpi
       FROM
          DD07T t
   SET mrpi.Description = ifnull(DD07T_DDTEXT, 'Not Set')
 WHERE mrpi.RowIsCurrent = 1
        AND   t.DD07T_DOMNAME = 'KZAUS'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND mrpi.Indicator = t.DD07T_DOMVALUE
;