insert into Dim_BillingBlock(Dim_BillingBlockid,BillingBlockCode,Description)
select 1,'Not Set','Not Set'
FROM (SELECT 1) a
where not exists (select 1 from Dim_BillingBlock where Dim_BillingBlockid = 1);

UPDATE    dim_billingblock bb
       FROM
          TVFST tv
	   SET Description = ifnull(TVFST_VTEXT, 'Not Set'),
			bb.dw_update_date = current_timestamp
       WHERE bb.BillingBlockCode = tv.TVFST_FAKSP AND tv.TVFST_FAKSP IS NOT NULL;               

delete from number_fountain m where m.table_name = 'Dim_BillingBlock';

insert into number_fountain
select 	'Dim_BillingBlock',
	ifnull(max(d.Dim_BillingBlockid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_BillingBlock d
where d.Dim_BillingBlockid <> 1;
	   
insert into dim_billingblock
(	Dim_BillingBlockid,
	BillingBlockCode,
	Description
)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_BillingBlock') 
          + row_number() over(),
		  TVFST_FAKSP,
	ifnull(TVFST_VTEXT,'Not Set')
from TVFST
where TVFST_FAKSP is not null
	and not exists (select 1 from dim_billingblock a where a.BillingBlockCode = TVFST_FAKSP);
	
delete from number_fountain m where m.table_name = 'Dim_BillingBlock';
