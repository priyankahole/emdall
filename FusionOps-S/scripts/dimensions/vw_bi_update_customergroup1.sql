
UPDATE    dim_customergroup1 cg1
       FROM
          TVV1T t
   SET cg1.Description = ifnull(TVV1T_BEZEI, 'Not Set')
   WHERE t.TVV1T_KVGR1 = cg1.CustomerGroup AND t.TVV1T_SPRAS = 'E';
