UPDATE    dim_billingblock bb
       FROM
          TVFST tv
	   SET Description = ifnull(TVFST_VTEXT, 'Not Set')
       WHERE bb.BillingBlockCode = tv.TVFST_FAKSP AND tv.TVFST_FAKSP IS NOT NULL;