
DROP TABLE IF EXISTS tmp_tcurr;
CREATE TABLE tmp_tcurr as SELECT t.*, concat(substr((99999999 - tcurr_gdatu),0,4),'-',substr((99999999 - tcurr_gdatu),5,2),'-',substr((99999999 - tcurr_gdatu),7,2)) tcurr_gdatu_date  FROM TCURR t;

UPDATE  dim_exchangerate er
FROM    tmp_tcurr tc
                SET     er.FromCurrency = ifnull(tc.tcurr_fcurr, 'Not Set'),
                        er.FromCurrencyUnitsRatio = ifnull(tc.tcurr_ffact, 0),
                        er.EffectiveFrom = ifnull(tc.tcurr_gdatu_date, '0001-01-01'),
                        er.ExchangeRateType = ifnull(tc.tcurr_kurst, 'Not Set'),
                        er.ToCurrency = ifnull(tc.tcurr_tcurr, 'Not Set'),
                        er.ToCurrencyUnitsRatio = ifnull(tc.tcurr_tfact, 0),
                        er.ExchangeRate = ifnull(tc.tcurr_ukurs, 0)
WHERE er.RowIsCurrent = 1
AND tc.tcurr_kurst = er.ExchangeRateType
AND tc.tcurr_tcurr = er.ToCurrency
AND tc.tcurr_fcurr = er.FromCurrency
AND tc.tcurr_gdatu_date = er.EffectiveFrom;


--Update EffectiveTo
DROP TABLE IF EXISTS EffectiveTo_tmp;

CREATE TABLE EffectiveTo_tmp
AS
SELECT distinct tc.tcurr_kurst,tc.tcurr_tcurr,tc.tcurr_fcurr,tc.tcurr_gdatu_date,Min(er.EffectiveFrom - 1) as EffectiveTo
FROM tmp_tcurr tc,dim_exchangerate er
WHERE   tc.tcurr_kurst = er.ExchangeRateType
                AND tc.tcurr_tcurr = er.ToCurrency
                AND tc.tcurr_fcurr = er.FromCurrency
                AND tc.tcurr_gdatu_date < er.EffectiveFrom
GROUP BY tcurr_kurst,tcurr_tcurr,tcurr_fcurr,tc.tcurr_gdatu_date;


UPDATE dim_exchangerate er
FROM EffectiveTo_tmp tmp
SET er.EffectiveTo = tmp.EffectiveTo
WHERE tmp.tcurr_kurst = er.ExchangeRateType
        AND tmp.tcurr_tcurr = er.ToCurrency
        AND tmp.tcurr_fcurr = er.FromCurrency
        AND tmp.tcurr_gdatu_date = er.EffectiveFrom;

UPDATE dim_exchangerate er SET er.EffectiveTo = '9999-12-31'
WHERE EffectiveTo = '0001-01-01'
AND dim_exchangerateid <> 1;

DROP TABLE IF EXISTS EffectiveTo_tmp;
DROP TABLE IF EXISTS tmp_tcurr;

