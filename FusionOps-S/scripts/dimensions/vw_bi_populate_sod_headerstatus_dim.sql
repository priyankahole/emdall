/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   5 Feb 2015      Liviu Ionescu              Add Combine for dim_salesorderheaderstatus */
/******************************************************************************************************************/

drop table if exists tmp_del_LIKP_LIPS_VBUK;
create table tmp_del_LIKP_LIPS_VBUK
as
select distinct LIKP_VBELN from LIKP_LIPS
union
select distinct dd_SalesDlvrDocNo from fact_salesorderdelivery;

delete from LIKP_LIPS_VBUK
  where not exists (select 1 from tmp_del_LIKP_LIPS_VBUK where VBUK_VBELN = LIKP_VBELN);

  UPDATE dim_salesorderheaderstatus
	FROM LIKP_LIPS_VBUK
   SET RefStatusAllItems = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
       ConfirmationStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
       DeliveryStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
       OverallDlvrStatusOfItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
       BillingStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
       BillingStatusOfOrdRelBillDoc = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
       RejectionStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
       OverallProcessStatusItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
       TotalIncompleteStatusAllItems = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
       GeneralIncompleteStatusItem = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
       OverallPickingStatus = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
       HeaderIncompletionStautusConcernDlvry = ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set'),
			dw_update_date = current_timestamp
  WHERE dim_salesorderheaderstatus.SalesDocumentNumber = VBUK_VBELN;

delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 

  INSERT
    INTO dim_salesorderheaderstatus(
	  dim_salesorderheaderstatusid,
          SalesDocumentNumber,
          RefStatusAllItems,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDlvrStatusOfItem,
          BillingStatus,
          BillingStatusOfOrdRelBillDoc,
          RejectionStatus,
          OverallProcessStatusItem,
          TotalIncompleteStatusAllItems,
          GeneralIncompleteStatusItem,
          OverallPickingStatus,
	  HeaderIncompletionStautusConcernDlvry
          )
 SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_RFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_BESTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_LFGSK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_FKSAK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_ABSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_GBSTK),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALS),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVALL),'Not Set'),
        ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_KOSTK),'Not Set'),
	ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUK_UVVLK),'Not Set')
FROM LIKP_LIPS_VBUK inner join LIKP_LIPS on LIKP_VBELN = VBUK_VBELN
  WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = VBUK_VBELN)) dtbl1;

/* LI Start Changes 5 Feb 2015 */
call vectorwise(combine 'dim_salesorderheaderstatus');
/* End Changes 5 Feb 2015 */

drop table if exists tmp_del_LIKP_LIPS_VBUK;
