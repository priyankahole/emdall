
/* Get distinct list of customer number and address number */
DROP TABLE IF EXISTS tmp_distinct_cust_addrno;
CREATE TABLE tmp_distinct_cust_addrno
AS
SELECT DISTINCT VBPA_KUNNR,VBPA_ADRNR
FROM vbpa
WHERE VBPA_ADRNR like '9%';


/* For all such pairs, insert a row in dim_customershipto */
DROP TABLE IF EXISTS tmp_insert_dim_customershipto;
CREATE TABLE tmp_insert_dim_customershipto AS
SELECT DISTINCT
map.VBPA_KUNNR CustomerNumber,
ifnull(adrc_country,'Not Set') Country,
ifnull(adrc_name1,'Not Set') Name,
ifnull(adrc_name2,'Not Set') Name2,
ifnull(adrc_city1,'Not Set') City,
ifnull(adrc_post_code1,'Not Set') PostalCode,
ifnull(adrc_region,'Not Set') Region,
ifnull(adrc_street,'Not Set') Address,
'Y' source_ADRC,
ifnull(adrc_date_from,'1-Jan-0001') date_from,
ifnull(adrc_nation,'Not Set'),
ifnull(adrc_date_to,'31-Dec-9999') date_to,
ifnull(adrc_addrnumber,'Not Set') addressnumber,
ifnull(adrc_city2,'Not Set') city2
FROM vbpa_adrc_custshipto t,tmp_distinct_cust_addrno map
WHERE adrc_addrnumber LIKE '9%'
AND t.adrc_addrnumber = map.VBPA_ADRNR
AND NOT EXISTS ( SELECT 1
                FROM dim_customershipto ship
                where ship.CustomerNumber = map.VBPA_KUNNR
                AND ship.addressnumber = ifnull(adrc_addrnumber,'Not Set')
                AND ship.date_from = ifnull(adrc_date_from,'1-Jan-0001')
                AND ship.nation = ifnull(adrc_nation,'Not Set')
                );

INSERT INTO dim_customershipto
(dim_customershiptoid)
select 1
from (select 1) a
where not exists ( select 'x' from dim_customershipto where dim_customershiptoid = 1);

INSERT INTO dim_customershipto
SELECT *,'Not Set' source_ADRC,'01-Jan-0001' date_from,'31-Dec-9999' date_to,'Not Set' nation,'Not Set' city2
FROM dim_customer dc
WHERE NOT EXISTS ( SELECT 1 FROM dim_customershipto ship WHERE ship.CustomerNumber = dc.CustomerNumber AND ship.addressnumber = dc.addressnumber ) ;

INSERT INTO dim_customershipto
(
dim_customershiptoid,
CustomerNumber,
Country,
Name,
Name2,
City,
PostalCode,
Region,
Address,
source_ADRC,
date_from,
nation,
date_to,
addressnumber,
city2
)
SELECT
(select ifnull(max(c.dim_customershiptoid)+1, 1) id
             FROM dim_customershipto c ) + row_number() over() dim_customershiptoid,
*
FROM tmp_insert_dim_customershipto;

DROP TABLE IF EXISTS tmp_distinct_cust_addrno;
DROP TABLE IF EXISTS tmp_insert_dim_customershipto;
