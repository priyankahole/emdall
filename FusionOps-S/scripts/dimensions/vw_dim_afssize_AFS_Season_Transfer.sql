/*Default row*/
insert into dim_afssize(Dim_AfsSizeId,Size,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason) values (1,'Not Set','2012-10-15 00:00:00',null,1,null) 
where not exists(select 1 from dim_afssize where dim_afssizeId = 1);

INSERT INTO dim_afsseason(Dim_AfsSeasonId,
			SeasonIndicator,
			  Collection,
			  Theme,
			  Description,
              RowStartDate,
              RowIsCurrent)
     SELECT (SELECT ifnull(max(ase.Dim_AfsSeasonId)+1, 1) id
             FROM dim_afsseason ase) 
          + row_number() over(),
			J_3ASEANT_J_3ASEAN,
	    J_3ASEANT_AFS_COLLECTION,
	    J_3ASEANT_AFS_THEME,
	    J_3ASEANT_TEXT,
        current_timestamp,
        1
       FROM J_3ASEANT
      WHERE J_3ASEANT_J_3ASEAN IS NOT NULL
	    AND NOT EXISTS
                  (SELECT 1
                     FROM dim_afsseason
                    WHERE SeasonIndicator = J_3ASEANT_J_3ASEAN
                     AND Collection = J_3ASEANT_AFS_COLLECTION
                     AND Theme = J_3ASEANT_AFS_THEME);