insert into dim_globalpart (dim_globalpartid,rowstartdate,rowiscurrent,partnumber_noleadzero)
select 1,current_timestamp,1,'Not Set' from (select 1) as src
where
not exists (select 1 from dim_globalpart where dim_globalpartid=1);

drop table if exists tmp_global_mara_marc_makt_spras_ins;
drop table if exists tmp_global_mara_marc_makt_spras_del;

create table tmp_global_mara_marc_makt_spras_ins as 
select  a.MARA_MATNR,a.MARC_WERKS
FROM    mara_marc_makt_spras a;

create table tmp_global_mara_marc_makt_spras_del as 
select b.MARA_MATNR,b.MARC_WERKS
from mara_marc_makt b;

call vectorwise (combine 'tmp_global_mara_marc_makt_spras_ins-tmp_global_mara_marc_makt_spras_del');

INSERT INTO mara_marc_makt(
   MARA_MATNR
  ,MARA_MTART
  ,MARA_MATKL
  ,MARA_PRDHA
  ,MARA_TRAGR
  ,MARA_SPART
  ,MARA_MTPOS
  ,MARA_KZREV
  ,MARA_MEINS
  ,MARA_MFRPN
  ,MARA_MSTAE
  ,MARA_LAEDA
  ,MARA_ERSDA
  ,MARC_WERKS
  ,MARC_MMSTA
  ,MARC_MAABC
  ,MARC_KZKRI
  ,MARC_EKGRP
  ,MARC_DISPO
  ,MARC_PLIFZ
  ,MARC_BESKZ
  ,MARC_MINBE
  ,MARC_EISBE
  ,MARC_MABST
  ,MARC_USEQU
  ,MARC_KAUTB
  ,MARC_STAWN
  ,MARC_LGPRO
  ,MARC_SOBSL
  ,MARC_INSMK
  ,MARC_XCHPF
  ,MARC_LVORM
  ,MARC_BSTRF
  ,MARC_DISMM
  ,MARC_DISLS
  ,MARC_BSTMI
  ,MARC_WEBAZ
  ,MARC_LGFSB
  ,MARC_HERKL
  ,MARC_BWSCL
  ,MARC_STRGR
  ,MARC_DISPR
  ,MARC_DISGR
  ,MARC_SCHGT
  ,MAKT_MAKTX
  ,MARC_DZEIT
  ,MAKT_SPRAS
  ,MARC_PRCTR
  ,MARA_EXTWG
  ,MARC_KZAUS
  ,MARC_BEARZ
  ,MARC_WZEIT
  ,MARA_J_3AGEND
  ,MARA_BISMT
  ,MARA_MSTAV
  ,MARA_VOLUM
  ,MARA_LABOR
  ,MARA_J_3APGNR
  ,MARA_J_3ACOL
  ,MARA_AFS_SCHNITT
  ,MARC_NCOST
  ,MARC_SFEPR
  ,MARC_FXHOR
  ,MARA_WRKST
  ,MARA_EAN11
  ,MARA_MFRNR
  ,MARA_GROES
  ,MARC_FHORI
  ,MARA_RAUBE
  ,MARA_STOFF
  ,MARC_RWPRO,MARC_SHFLG,MARC_SHZET,MARC_BSTMA,MARC_BSTFE,
       MARA_J_3AMIND,
       MARA_J_3AMSTAT,
       MARA_J_3AKALSV,
       MARA_J_3AKALSM,
       MARA_J_3ASMAT,
       MARA_J_3APROFP,
       MARA_J_3APROFC,
       MARA_J_3APROFG,
       MARA_J_3APROFT,
       MARA_J_3AGMID,
       MARA_J_3ANOGRID,
       MARA_J_3AFCC,
       MARA_J_4KCSGR,
       MARA_AFS_SCLAS,
       MARA_J_4KCOVSA,
       MARC_J_3AMSTATC,
       MARC_J_3AMSTAT,
       MARC_J_4KCOVS,
       MARC_J_3ADISPKZ,
       MARC_J_3AHORIZ,
       MARC_J_3APRFC,
       MARC_J_3APRFG,
       MARC_J_3APRFT,
       MARC_J_3ASGRID,
       MARC_J_3AVERPR,
       MARC_J_3AVERMR,
       MARC_J_3AVERSS,
       MARC_J_3AVERPU,
       MARC_J_3AVERSD,
       MARC_J_3AEKORG,
       MARC_J_3APRCM,
       MARC_J_3ABUND,
       MARC_J_3ACHINT,
       MARC_J_3ABAINT,
       MARC_J_3ARISK,
       MARC_J_3ADEFSI,
       MARC_J_4KDEFSC,
       MARC_J_4AATPSI,
       MARC_J_3ASLHMOD,
       MARC_J_3AMROUND,
       MARC_J_3AMRDMENG,
       MARC_J_3AMIND
) SELECT a.MARA_MATNR, MARA_MTART, MARA_MATKL, MARA_PRDHA, MARA_TRAGR, MARA_SPART, MARA_MTPOS,
 MARA_KZREV, MARA_MEINS, MARA_MFRPN, MARA_MSTAE, MARA_LAEDA, MARA_ERSDA, a.MARC_WERKS, MARC_MMSTA,
 MARC_MAABC, MARC_KZKRI, MARC_EKGRP, MARC_DISPO, MARC_PLIFZ, MARC_BESKZ, MARC_MINBE, MARC_EISBE,
 MARC_MABST, MARC_USEQU, MARC_KAUTB, MARC_STAWN, MARC_LGPRO, MARC_SOBSL, MARC_INSMK, MARC_XCHPF,
 MARC_LVORM, MARC_BSTRF, MARC_DISMM, MARC_DISLS, MARC_BSTMI, MARC_WEBAZ, MARC_LGFSB, MARC_HERKL,
 MARC_BWSCL, MARC_STRGR, MARC_DISPR, MARC_DISGR, MARC_SCHGT, MAKT_MAKTX, MARC_DZEIT, MAKT_SPRAS,
 MARC_PRCTR, MARA_EXTWG, MARC_KZAUS, MARC_BEARZ, MARC_WZEIT, MARA_J_3AGEND, MARA_BISMT, MARA_MSTAV,
 MARA_VOLUM, MARA_LABOR, MARA_J_3APGNR, MARA_J_3ACOL, MARA_AFS_SCHNITT, MARC_NCOST, MARC_SFEPR, MARC_FXHOR, MARA_WRKST, MARA_EAN11, MARA_MFRNR,  
 MARA_GROES,MARC_FHORI,MARA_RAUBE,MARA_STOFF,MARC_RWPRO,MARC_SHFLG,MARC_SHZET,MARC_BSTMA,MARC_BSTFE,
       MARA_J_3AMIND,
       MARA_J_3AMSTAT,
       MARA_J_3AKALSV,
       MARA_J_3AKALSM,
       MARA_J_3ASMAT,
       MARA_J_3APROFP,
       MARA_J_3APROFC,
       MARA_J_3APROFG,
       MARA_J_3APROFT,
       MARA_J_3AGMID,
       MARA_J_3ANOGRID,
       MARA_J_3AFCC,
       MARA_J_4KCSGR,
       MARA_AFS_SCLAS,
       MARA_J_4KCOVSA,
       MARC_J_3AMSTATC,
       MARC_J_3AMSTAT,
       MARC_J_4KCOVS,
       MARC_J_3ADISPKZ,
       MARC_J_3AHORIZ,
       MARC_J_3APRFC,
       MARC_J_3APRFG,
       MARC_J_3APRFT,
       MARC_J_3ASGRID,
       MARC_J_3AVERPR,
       MARC_J_3AVERMR,
       MARC_J_3AVERSS,
       MARC_J_3AVERPU,
       MARC_J_3AVERSD,
       MARC_J_3AEKORG,
       MARC_J_3APRCM,
       MARC_J_3ABUND,
       MARC_J_3ACHINT,
       MARC_J_3ABAINT,
       MARC_J_3ARISK,
       MARC_J_3ADEFSI,
       MARC_J_4KDEFSC,
       MARC_J_4AATPSI,
       MARC_J_3ASLHMOD,
       MARC_J_3AMROUND,
       MARC_J_3AMRDMENG,
       MARC_J_3AMIND
FROM mara_marc_makt_spras a,  tmp_global_mara_marc_makt_spras_ins b
WHERE a.MARA_MATNR = b.MARA_MATNR and a.MARC_WERKS = b.MARC_WERKS;

call vectorwise (combine 'tmp_global_mara_marc_makt_spras_ins-tmp_global_mara_marc_makt_spras_ins');
call vectorwise (combine 'tmp_global_mara_marc_makt_spras_del-tmp_global_mara_marc_makt_spras_del');


UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and AFSColor <> ifnull(MARA_J_3ACOL, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p  
   SET  AFSColorDescription = ifnull((SELECT act.J_3ACOLRT_TEXT FROM J_3ACOLRT act WHERE act.J_3ACOLRT_J_3ACOL = MARA_J_3ACOL), 'Not Set'),
	dp.dw_update_date = current_timestamp
	WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	and AFSColorDescription <> ifnull((SELECT act.J_3ACOLRT_TEXT FROM J_3ACOLRT act WHERE act.J_3ACOLRT_J_3ACOL = MARA_J_3ACOL), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and AFSMasterGrid <> ifnull(MARA_J_3APGNR, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and AFSPattern <> ifnull(MARA_AFS_SCHNITT, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET AFSTargetGroup = ifnull(MARA_J_3AGEND, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and AFSTargetGroup <> ifnull(MARA_J_3AGEND, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET AFSTargetGroupDescription = ifnull((SELECT atg.J_3AGENDRT_TEXT FROM j_3agendrt atg WHERE atg.J_3AGENDRT_J_3AGEND = MARA_J_3AGEND), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and AFSTargetGroupDescription <> ifnull((SELECT atg.J_3AGENDRT_TEXT FROM j_3agendrt atg WHERE atg.J_3AGENDRT_J_3AGEND = MARA_J_3AGEND), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET CategProfile = ifnull(MARA_J_3APROFC,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and CategProfile <> ifnull(MARA_J_3APROFC,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET CategStructure = ifnull(MARA_J_4KCSGR,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and CategStructure <> ifnull(MARA_J_4KCSGR,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET CheckingGroup = ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and CheckingGroup <> ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where marc_mtvfp = tm.tmvft_mtvfp), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET Concentration = ifnull((SELECT a.AUSP_ATFLV FROM AUSP a WHERE a.AUSP_ATINN = 6 AND  a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and Concentration <> ifnull((SELECT a.AUSP_ATFLV FROM AUSP a WHERE a.AUSP_ATINN = 6 AND  a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ConcentrationUOM = ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 7 AND  a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ConcentrationUOM <> ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 7 AND  a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ConsumptionModeDescription = ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ConsumptionModeDescription <> ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET Contents = ifnull((SELECT a.AUSP_ATFLV FROM AUSP a WHERE a.AUSP_ATINN = 8 AND  a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and Contents <> ifnull((SELECT a.AUSP_ATFLV FROM AUSP a WHERE a.AUSP_ATINN = 8 AND  a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ContentsUOM = ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 9 AND  a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ContentsUOM <> ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 9 AND  a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET CrossSDMaterialStatus = ifnull(MARA_MSTAV, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and CrossSDMaterialStatus <> ifnull(MARA_MSTAV, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET CrossSDMaterialStatusDescription = ifnull((SELECT TVMST_vmstb FROM TVMST mst WHERE mst.TVMST_vmsta = MARA_MSTAV), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and CrossSDMaterialStatusDescription <> ifnull((SELECT TVMST_vmstb FROM TVMST mst WHERE mst.TVMST_vmsta = MARA_MSTAV), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET Division = ifnull(MARA_SPART, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and Division <> ifnull(MARA_SPART, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET DivisionDescription = ifnull((SELECT tsp.TSPAT_VTEXT FROM TSPAT tsp WHERE tsp.TSPAT_SPART = MARA_SPART AND tsp.TSPAT_SPRAS = 'E'),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and DivisionDescription <> ifnull((SELECT tsp.TSPAT_VTEXT FROM TSPAT tsp WHERE tsp.TSPAT_SPART = MARA_SPART AND tsp.TSPAT_SPRAS = 'E'),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET DominantSpecies = ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 128 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and DominantSpecies <> ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 128 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ExternalMaterialGroupCode <> ifnull(MARA_EXTWG, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ExternalMaterialGroupDescription = ifnull((SELECT t.TWEWT_EWBEZ FROM twewt t WHERE t.TWEWT_EXTWG = MARA_EXTWG), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ExternalMaterialGroupDescription <> ifnull((SELECT t.TWEWT_EWBEZ FROM twewt t WHERE t.TWEWT_EXTWG = MARA_EXTWG), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ExtManfacturerCode = ifnull((SELECT dv.VendorMfgCode FROM dim_Vendor dv WHERE MARA_MFRNR = dv.VendorNumber),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ExtManfacturerCode <> ifnull((SELECT dv.VendorMfgCode FROM dim_Vendor dv WHERE MARA_MFRNR = dv.VendorNumber),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ExtManfacturerDesc = ifnull((SELECT dv.VendorName FROM dim_Vendor dv WHERE MARA_MFRNR = dv.VendorNumber),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ExtManfacturerDesc <> ifnull((SELECT dv.VendorName FROM dim_Vendor dv WHERE MARA_MFRNR = dv.VendorNumber),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET FabricContentCode = ifnull(MARA_J_3AFCC,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and FabricContentCode <> ifnull(MARA_J_3AFCC,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET FixedLotSize = ifnull(MARC_BSTFE,0),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and FixedLotSize <> ifnull(MARC_BSTFE,0);

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and GeneralItemCategory <> ifnull(MARA_MTPOS, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET GridDetProcMM = ifnull(MARA_J_3AKALSM,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and GridDetProcMM <> ifnull(MARA_J_3AKALSM,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET GridDetProcSD = ifnull(MARA_J_3AKALSV,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and GridDetProcSD <> ifnull(MARA_J_3AKALSV,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET GridProfile = ifnull(MARA_J_3APROFG,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and GridProfile <> ifnull(MARA_J_3APROFG,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ItemSubType = ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 13 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ItemSubType <> ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 13 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ItemSubTypeDescription = ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 13 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ItemSubTypeDescription <> ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 13 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ItemType = ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 14 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ItemType <> ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 14 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ItemTypeDescription = ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 14 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ItemTypeDescription <> ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 14 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET Laboratory = ifnull(MARA_LABOR, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and Laboratory <> ifnull(MARA_LABOR, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MainUPC = ifnull((select me.MEAN_EAN11 FROM MEAN me WHERE me.MEAN_MATNR =mck.MARA_MATNR), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MainUPC <> ifnull((select me.MEAN_EAN11 FROM MEAN me WHERE me.MEAN_MATNR =mck.MARA_MATNR), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ManfacturerNumber = ifnull(MARA_MFRNR,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ManfacturerNumber <> ifnull(MARA_MFRNR,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialConvIDforDVC = ifnull(MARA_J_3AGMID,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialConvIDforDVC <> ifnull(MARA_J_3AGMID,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialDummyGrid = ifnull(MARA_J_3ANOGRID,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialDummyGrid <> ifnull(MARA_J_3ANOGRID,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialFamilyColor = ifnull((select concat(SUBSTRING(TRIM(LEADING '0' FROM PartNumber),1,6),' ',afscolor) from dim_globalpart),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialFamilyColor <> ifnull((select concat(SUBSTRING(TRIM(LEADING '0' FROM PartNumber),1,6),' ',afscolor) from dim_globalpart),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialGroup = ifnull(MARA_MATKL, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialGroup <> ifnull(MARA_MATKL, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialGroupDescription = ifnull((SELECT mg.T023T_WGBEZ FROM T023T mg WHERE mg.T023T_MATKL = MARA_MATKL), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialGroupDescription <> ifnull((SELECT mg.T023T_WGBEZ FROM T023T mg WHERE mg.T023T_MATKL = MARA_MATKL), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialMasterInd = ifnull(MARA_J_3AMIND,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialMasterInd <> ifnull(MARA_J_3AMIND,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialMasterStat = ifnull(MARA_J_3AMSTAT,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialMasterStat <> ifnull(MARA_J_3AMSTAT,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialStatus = ifnull(MARA_MSTAE,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialStatus <> ifnull(MARA_MSTAE,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MaterialStatusDescription = ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = MARA_MSTAE), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MaterialStatusDescription <> ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = MARA_MSTAE), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MinimumLotSize = ifnull(MARC_BSTMI,0.0000),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MinimumLotSize <> ifnull(MARC_BSTMI,0.0000);

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MRPController = ifnull(marc_dispo, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MRPController <> ifnull(marc_dispo, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MRPControllerTelephone = ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MRPControllerTelephone <> ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MRPGroup = ifnull(MARC_DISGR, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MRPGroup <> ifnull(MARC_DISGR, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MRPLotSize = ifnull(MARC_DISLS, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MRPLotSize <> ifnull(MARC_DISLS, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MRPProfile = ifnull(MARC_DISPR, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MRPProfile <> ifnull(MARC_DISPR, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MRPStatus = ifnull((SELECT j2.J_3AMAD_J_4ASTAT FROM J_3AMAD j2 WHERE mck.MARA_MATNR = j2.j_3amad_matnr and j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MRPStatus <> ifnull((SELECT j2.J_3AMAD_J_4ASTAT FROM J_3AMAD j2 WHERE mck.MARA_MATNR = j2.j_3amad_matnr and j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET MRPType = ifnull(MARC_DISMM, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and MRPType <> ifnull(MARC_DISMM, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET NDCCode = ifnull(MARA_GROES,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and NDCCode <> ifnull(MARA_GROES,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and OldPartNumber <> ifnull(MARA_BISMT, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PartDescription <> ifnull(MAKT_MAKTX, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PartLongDesc = ifnull(MAKT_MAKTG,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PartLongDesc <> ifnull(MAKT_MAKTG,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PartType = ifnull(MARA_MTART, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PartType <> ifnull(MARA_MTART, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PartTypeDescription = ifnull((SELECT pt.T134T_MTBEZ FROM T134T pt WHERE pt.T134T_MTART = MARA_MTART), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PartTypeDescription <> ifnull((SELECT pt.T134T_MTBEZ FROM T134T pt WHERE pt.T134T_MTART = MARA_MTART), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PeriodProfile = ifnull(MARA_J_3APROFT,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PeriodProfile <> ifnull(MARA_J_3APROFT,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PlantIndepCoverageStrat = ifnull(MARA_J_4KCOVSA,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PlantIndepCoverageStrat <> ifnull(MARA_J_4KCOVSA,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PlantMaterialStatus = ifnull(MARC_MMSTA,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PlantMaterialStatus <> ifnull(MARC_MMSTA,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PlantProfile = ifnull(MARA_J_3APROFP,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PlantProfile <> ifnull(MARA_J_3APROFP,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PrimarySite = ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 438 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PrimarySite <> ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 438 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ProductGroup = ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 129 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ProductGroup <> ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 129 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ProductHierarchy <> ifnull(MARA_PRDHA, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ProductHierarchyDescription = ifnull((SELECT t.VTEXT FROM t179t t WHERE t.PRODH = MARA_PRDHA), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ProductHierarchyDescription <> ifnull((SELECT t.VTEXT FROM t179t t WHERE t.PRODH = MARA_PRDHA), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ProductType = ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 127 AND a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ProductType <> ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 127 AND a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ProductTypeDescription = ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 127 AND a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ProductTypeDescription <> ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 127 AND a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ProfitCenterCode = ifnull(MARC_PRCTR, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ProfitCenterCode <> ifnull(MARC_PRCTR, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ProfitCenterName = ifnull((SELECT c.CEPCT_KTEXT FROM cepct c WHERE c.CEPCT_PRCTR = MARC_PRCTR AND c.CEPCT_DATBI > current_date), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ProfitCenterName <> ifnull((SELECT c.CEPCT_KTEXT FROM cepct c WHERE c.CEPCT_PRCTR = MARC_PRCTR AND c.CEPCT_DATBI > current_date), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PurchaseGroup = ifnull(marc_ekgrp, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PurchaseGroup <> ifnull(marc_ekgrp, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET PurchaseGroupDescription = ifnull((SELECT t.T024_EKNAM FROM t024 t WHERE t.T024_EKGRP = MARC_EKGRP), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PurchaseGroupDescription <> ifnull((SELECT t.T024_EKNAM FROM t024 t WHERE t.T024_EKGRP = MARC_EKGRP), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET Revision = ifnull(MARA_KZREV, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and Revision <> ifnull(MARA_KZREV, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET SpecialProcurement  =  ifnull(MARC_SOBSL, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and SpecialProcurement  <>  ifnull(MARC_SOBSL, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET storagecondition = ifnull((SELECT t.rbtxt FROM t142t t WHERE t.RAUBE = MARA_RAUBE), 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and storagecondition <> ifnull((SELECT t.rbtxt FROM t142t t WHERE t.RAUBE = MARA_RAUBE), 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET StorageConditionCode = ifnull(MARA_RAUBE,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and StorageConditionCode <> ifnull(MARA_RAUBE,'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and TransportationGroup <> ifnull(MARA_TRAGR, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and UnitOfMeasure <> ifnull(MARA_MEINS, 'Not Set');

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET ValidFrom = (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.MARA_MATNR = j1.j_3amad_matnr and j1.J_3AMAD_WERKS = mck.MARC_WERKS),
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and ValidFrom <> (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.MARA_MATNR = j1.j_3amad_matnr and j1.J_3AMAD_WERKS = mck.MARC_WERKS);

UPDATE dim_globalpart dp from mara_marc_makt mck, dim_plant p
   SET Volume = MARA_VOLUM,
	   	dp.dw_update_date = current_timestamp
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and Volume <> MARA_VOLUM;


delete from number_fountain m where m.table_name = 'dim_globalpart';
   
insert into number_fountain
select 	'dim_globalpart',
	ifnull(max(d.dim_globalpartid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_globalpart d
where d.dim_globalpartid <> 1; 

call vectorwise (combine 'tmp_dim_globalpart_MARA_MARC_MAKT_ins-tmp_dim_globalpart_MARA_MARC_MAKT_ins');
call vectorwise (combine 'tmp_dim_globalpart_MARA_MARC_MAKT_del-tmp_dim_globalpart_MARA_MARC_MAKT_del');

insert into tmp_dim_globalpart_MARA_MARC_MAKT_ins
select mck.MARA_MATNR,mck.MARC_WERKS
FROM    MARA_MARC_MAKT mck;

insert into tmp_dim_globalpart_MARA_MARC_MAKT_del
select p.PartNumber,p.Plant
from dim_globalpart p;

call vectorwise (combine 'tmp_dim_globalpart_MARA_MARC_MAKT_ins-tmp_dim_globalpart_MARA_MARC_MAKT_del');

INSERT INTO dim_globalpart(dim_globalpartid,
	RowIsCurrent,
	RowStartDate,
AFSColor,
AFSColorDescription,
AFSMasterGrid,
AFSPattern,
AFSTargetGroup,
AFSTargetGroupDescription,
CategProfile,
CategStructure,
CheckingGroup,
Concentration,
ConcentrationUOM,
ConsumptionModeDescription,
Contents,
ContentsUOM,
CrossSDMaterialStatus,
CrossSDMaterialStatusDescription,
Division,
DivisionDescription,
DominantSpecies,
ExternalMaterialGroupCode,
ExternalMaterialGroupDescription,
ExtManfacturerCode,
ExtManfacturerDesc,
FabricContentCode,
FixedLotSize,
GeneralItemCategory,
GridDetProcMM,
GridDetProcSD,
GridProfile,
ItemSubType,
ItemSubTypeDescription,
ItemType,
ItemTypeDescription,
Laboratory,
MainUPC,
ManfacturerNumber,
MaterialConvIDforDVC,
MaterialDummyGrid,
MaterialFamilyColor,
MaterialGroup,
MaterialGroupDescription,
MaterialMasterInd,
MaterialMasterStat,
MaterialStatus,
MaterialStatusDescription,
MinimumLotSize,
MRPController,
MRPControllerTelephone,
MRPGroup,
MRPLotSize,
MRPProfile,
MRPStatus,
MRPType,
NDCCode,
OldPartNumber,
PartDescription,
PartLongDesc,
PartNumber,
PartType,
PartTypeDescription,
PeriodProfile,
Plant,
PlantIndepCoverageStrat,
PlantMaterialStatus,
PlantProfile,
PrimarySite,
ProductGroup,
ProductHierarchy,
ProductHierarchyDescription,
ProductType,
ProductTypeDescription,
ProfitCenterCode,
ProfitCenterName,
PurchaseGroup,
PurchaseGroupDescription,
Revision,
SpecialProcurement, 
/* SpecialProcurementClass, */
StorageCondition,
StorageConditionCode,
TransportationGroup,
UnitOfMeasure,
ValidFrom,
Volume)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_globalpart')
          + row_number()
             over(), t.* from (
SELECT DISTINCT
	1,
	current_timestamp,
ifnull(MARA_J_3ACOL, 'Not Set'),
ifnull((SELECT act.J_3ACOLRT_TEXT FROM J_3ACOLRT act WHERE act.J_3ACOLRT_J_3ACOL = MARA_J_3ACOL), 'Not Set'),
ifnull(MARA_J_3APGNR, 'Not Set'),
ifnull(MARA_AFS_SCHNITT, 'Not Set'),
ifnull(MARA_J_3AGEND, 'Not Set'),
ifnull((SELECT atg.J_3AGENDRT_TEXT FROM j_3agendrt atg WHERE atg.J_3AGENDRT_J_3AGEND = MARA_J_3AGEND), 'Not Set'),
ifnull(MARA_J_3APROFC,'Not Set'),
ifnull(MARA_J_4KCSGR,'Not Set'),
ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
ifnull((SELECT a.AUSP_ATFLV FROM AUSP a WHERE a.AUSP_ATINN = 6 AND  a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 7 AND  a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
ifnull((SELECT a.AUSP_ATFLV FROM AUSP a WHERE a.AUSP_ATINN = 8 AND  a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set'),
ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 9 AND  a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set'),
ifnull(MARA_MSTAV, 'Not Set'),
ifnull((SELECT TVMST_vmstb FROM TVMST mst WHERE mst.TVMST_vmsta = MARA_MSTAV), 'Not Set'),
ifnull(MARA_SPART, 'Not Set'),
ifnull((SELECT tsp.TSPAT_VTEXT FROM TSPAT tsp WHERE tsp.TSPAT_SPART = MARA_SPART AND tsp.TSPAT_SPRAS = 'E'),'Not Set'),
ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 128 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull(MARA_EXTWG, 'Not Set'),
ifnull((SELECT t.TWEWT_EWBEZ FROM twewt t WHERE t.TWEWT_EXTWG = MARA_EXTWG), 'Not Set'),
ifnull((SELECT dv.VendorMfgCode FROM dim_Vendor dv WHERE MARA_MFRNR = dv.VendorNumber),'Not Set'),
ifnull((SELECT dv.VendorName FROM dim_Vendor dv WHERE MARA_MFRNR = dv.VendorNumber),'Not Set'),
ifnull(MARA_J_3AFCC,'Not Set'),
ifnull(MARC_BSTFE,0),
ifnull(MARA_MTPOS, 'Not Set'),
ifnull(MARA_J_3AKALSM,'Not Set'),
ifnull(MARA_J_3AKALSV,'Not Set'),
ifnull(MARA_J_3APROFG,'Not Set'),
ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 13 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 13 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 14 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 14 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull(MARA_LABOR, 'Not Set'),
ifnull((select me.MEAN_EAN11 FROM MEAN me WHERE me.MEAN_MATNR =mck.MARA_MATNR), 'Not Set'),
ifnull(MARA_MFRNR,'Not Set'),
ifnull(MARA_J_3AGMID,'Not Set'),
ifnull(MARA_J_3ANOGRID,'Not Set'),
ifnull((select concat(SUBSTRING(TRIM(LEADING '0' FROM PartNumber),1,6),' ',afscolor) from dim_globalpart),'Not Set'),
ifnull(MARA_MATKL, 'Not Set'),
ifnull((SELECT mg.T023T_WGBEZ FROM T023T mg WHERE mg.T023T_MATKL = MARA_MATKL), 'Not Set'),
ifnull(MARA_J_3AMIND,'Not Set'),
ifnull(MARA_J_3AMSTAT,'Not Set'),
ifnull(MARA_MSTAE, 'Not Set'),
ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = MARA_MSTAE), 'Not Set'),
ifnull(MARC_BSTMI,0.0000),
ifnull(marc_dispo, 'Not Set'),
ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set'),
ifnull(MARC_DISGR, 'Not Set') MRPGroup,
ifnull(MARC_DISLS, 'Not Set') MRPLotSize,
ifnull(MARC_DISPR, 'Not Set') MRPProfile,
ifnull((SELECT j2.J_3AMAD_J_4ASTAT FROM J_3AMAD j2 WHERE mck.MARA_MATNR = j2.j_3amad_matnr and j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
ifnull(MARC_DISMM, 'Not Set'),
ifnull(MARA_GROES,'Not Set'),
ifnull(MARA_BISMT, 'Not Set'),
ifnull(MAKT_MAKTX, 'Not Set'),
ifnull(MAKT_MAKTG,'Not Set'),
mck.MARA_MATNR,
ifnull(MARA_MTART, 'Not Set'),
ifnull((SELECT pt.T134T_MTBEZ FROM T134T pt WHERE pt.T134T_MTART = MARA_MTART), 'Not Set'),
ifnull(MARA_J_3APROFT,'Not Set'),
mck.MARC_WERKS,
ifnull(MARA_J_4KCOVSA,'Not Set'),
ifnull(MARC_MMSTA,'Not Set'),
ifnull(MARA_J_3APROFP,'Not Set'),
ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 438 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 129 AND a.AUSP_OBJEK = mck.MARA_MATNR),'Not Set'),
ifnull(MARA_PRDHA, 'Not Set'),
ifnull((SELECT t.VTEXT FROM t179t t WHERE t.PRODH = MARA_PRDHA), 'Not Set'),
ifnull((SELECT a.AUSP_ATWRT FROM AUSP a WHERE a.AUSP_ATINN = 127 AND a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set'),
ifnull((SELECT c.CAWNT_ATWTB FROM CAWNT c, AUSP a WHERE a.AUSP_ATINN = c.CAWN_ATINN AND a.AUSP_ATWRT = c.CAWN_ATWRT AND a.AUSP_ATINN = 127 AND a.AUSP_OBJEK =mck.MARA_MATNR),'Not Set'),
ifnull(MARC_PRCTR, 'Not Set'),
ifnull((SELECT c.CEPCT_KTEXT FROM cepct c WHERE c.CEPCT_PRCTR = MARC_PRCTR AND c.CEPCT_DATBI > current_date), 'Not Set'),
ifnull(marc_ekgrp, 'Not Set'),
ifnull((SELECT t.T024_EKNAM FROM t024 t WHERE t.T024_EKGRP = MARC_EKGRP), 'Not Set'),
ifnull(MARA_KZREV, 'Not Set'),
 ifnull(MARC_SOBSL, 'Not Set'), 
/* ifnull(MARA_AFS_SCLAS,'Not Set'), */
ifnull((SELECT t.rbtxt FROM t142t t WHERE t.RAUBE = MARA_RAUBE), 'Not Set'),
ifnull(MARA_RAUBE,'Not Set'),
ifnull(MARA_TRAGR, 'Not Set'),
ifnull(MARA_MEINS, 'Not Set'),
	(select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.MARA_MATNR = j1.j_3amad_matnr and j1.J_3AMAD_WERKS = mck.MARC_WERKS),
	MARA_VOLUM
     FROM MARA_MARC_MAKT mck
          INNER JOIN
             dim_plant dp
          ON dp.plantcode = mck.marc_werks, -- AND dp.languagekey = makt_spras
          tmp_dim_globalpart_MARA_MARC_MAKT_ins p
    WHERE ifnull(mck.MARC_WERKS,'NULL') <> 'NULL'
    AND mck.MARA_MATNR = p.MARA_MATNR
    AND mck.MARC_WERKS = p.MARC_WERKS) t;

UPDATE dim_globalpart dp 
	SET  
dp.PartNumber_NoLeadZero= ifnull(case when length(dp.partnumber)=18 and dp.partnumber is integer then trim(leading '0' from dp.partnumber) else dp.partnumber end,'Not Set'),
			dp.dw_update_date = current_timestamp;

call vectorwise (combine 'tmp_dim_globalpart_MARA_MARC_MAKT_ins-tmp_dim_globalpart_MARA_MARC_MAKT_ins');
call vectorwise (combine 'tmp_dim_globalpart_MARA_MARC_MAKT_del-tmp_dim_globalpart_MARA_MARC_MAKT_del');
call vectorwise (combine 'dim_globalpart');