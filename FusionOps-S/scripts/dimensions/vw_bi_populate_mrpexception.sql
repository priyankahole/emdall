UPDATE dim_mrpexception mrpx FROM T458B t
   SET mrpx.Description = ifnull(T458B_AUSLT, 'Not Set'),
	mrpx.exceptionnumber = ifnull((select a.T458A_AUSKT
					from T458A a 
					where a.T458A_AUSSL = t.T458B_AUSSL),'Not Set'),
			mrpx.dw_update_date = current_timestamp 
 WHERE mrpx.RowIsCurrent = 1
 AND mrpx.exceptionkey = t.T458B_AUSSL
;

INSERT INTO dim_mrpexception(dim_mrpexceptionId, RowIsCurrent, description, RowStartDate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_mrpexception
               WHERE dim_mrpexceptionId = 1);

delete from number_fountain m where m.table_name = 'dim_mrpexception';
   
insert into number_fountain
select 	'dim_mrpexception',
	ifnull(max(d.dim_mrpexceptionID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_mrpexception d
where d.dim_mrpexceptionID <> 1; 

INSERT INTO dim_mrpexception(dim_mrpexceptionID,
                                Description,
                                exceptionkey,
                                RowStartDate,
                                RowIsCurrent,
				exceptionnumber)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_mrpexception') 
          + row_number() over(),
			 ifnull(T458B_AUSLT, 'Not Set'),
            ifnull(T458B_AUSSL, 'Not Set'),
            current_timestamp,
            1,
	    ifnull((select a.T458A_AUSKT from T458A a where a.T458A_AUSSL = T458B_AUSSL),'Not Set')
       FROM T458B
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_mrpexception
                    WHERE exceptionkey = T458B_AUSSL)
   ORDER BY 2 OFFSET 0
;
