UPDATE dim_partseason pse
FROM J_3AMSEA t,J_3ASEANT
SET pse.Description = ifnull(J_3ASEANT_TEXT,'Not Set'),
			pse.dw_update_date = current_timestamp
 WHERE pse.RowIsCurrent = 1
 AND pse.SeasonIndicator = ifnull(J_3AMSEA_J_3ASEAN, 'Not Set')
 AND pse.Collection = ifnull(J_3AMSEA_AFSCOLLECTION, 'Not Set')
 AND pse.Theme = ifnull(J_3AMSEA_AFSTHEME, 'Not Set')
 AND pse.GridValue = ifnull(J_3AMSEA_J_3ASIZE,'Not Set')
 AND pse.CategoryValue = ifnull(J_3AMSEA_J_4KRCAT, 'Not Set')
 AND pse.PartNumber = ifnull(J_3AMSEA_MATNR,'Not Set')
AND J_3ASEANT_J_3ASEAN = J_3AMSEA_J_3ASEAN
AND J_3ASEANT_AFS_COLLECTION = J_3AMSEA_AFSCOLLECTION
AND J_3ASEANT_AFS_THEME = J_3AMSEA_AFSTHEME
AND pse.Description <> ifnull(J_3ASEANT_TEXT,'Not Set');

UPDATE    dim_partseason pse
from J_3AMSEA t, MEAN
SET pse.InternationalPartNumber = ifnull(MEAN_EAN11,'Not Set'),
			pse.dw_update_date = current_timestamp
 WHERE pse.RowIsCurrent = 1
 AND pse.SeasonIndicator = ifnull(J_3AMSEA_J_3ASEAN, 'Not Set')
 AND pse.Collection = ifnull(J_3AMSEA_AFSCOLLECTION, 'Not Set')
 AND pse.Theme = ifnull(J_3AMSEA_AFSTHEME, 'Not Set')
 AND pse.GridValue = ifnull(J_3AMSEA_J_3ASIZE,'Not Set')
 AND pse.CategoryValue = ifnull(J_3AMSEA_J_4KRCAT, 'Not Set')
 AND pse.PartNumber = ifnull(J_3AMSEA_MATNR,'Not Set')
AND MEAN_J_3AKORDX = J_3AMSEA_J_3ASIZE
AND MEAN_MATNR = J_3AMSEA_MATNR
AND pse.InternationalPartNumber <> ifnull(MEAN_EAN11,'Not Set');


INSERT INTO dim_partseason(dim_partseasonId,rowstartdate,RowIsCurrent,partnumber_noleadzero)
SELECT 1,current_timestamp,1,'Not Set'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_partseason
               WHERE dim_partseasonId = 1);

delete from number_fountain m where m.table_name = 'dim_partseason';
   
insert into number_fountain
select 	'dim_partseason',
	ifnull(max(d.dim_partseasonId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_partseason d
where d.dim_partseasonId <> 1; 

INSERT INTO dim_partseason(dim_partseasonId,
			PartNumber,
			SeasonIndicator,
			  Collection,
			  Theme,
			  GridValue, 
			  CategoryValue,
			  Description,
			  InternationalPartNumber,
              RowStartDate,
              RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_partseason') 
          + row_number() over(),
		ifnull(J_3AMSEA_MATNR, 'Not Set'), 
		ifnull(J_3AMSEA_J_3ASEAN, 'Not Set'),
		ifnull(J_3AMSEA_AFSCOLLECTION, 'Not Set'),
	    ifnull(J_3AMSEA_AFSTHEME, 'Not Set'),
		ifnull(J_3AMSEA_J_3ASIZE, 'Not Set'),
		ifnull(J_3AMSEA_J_4KRCAT, 'Not Set'),
	    ifnull((SELECT J_3ASEANT_TEXT FROM J_3ASEANT WHERE
				    J_3ASEANT_J_3ASEAN = J_3AMSEA_J_3ASEAN
                     AND J_3ASEANT_AFS_COLLECTION = J_3AMSEA_AFSCOLLECTION
                     AND J_3ASEANT_AFS_THEME = J_3AMSEA_AFSTHEME), 'Not Set'),
		ifnull((SELECT MEAN_EAN11 FROM MEAN WHERE
				    MEAN_J_3AKORDX = J_3AMSEA_J_3ASIZE
                     AND MEAN_MATNR = J_3AMSEA_MATNR), 'Not Set'),
        current_timestamp,
        1
       FROM J_3AMSEA
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_partseason
                    WHERE SeasonIndicator = ifnull(J_3AMSEA_J_3ASEAN, 'Not Set')
                     AND Collection = ifnull(J_3AMSEA_AFSCOLLECTION, 'Not Set')
                     AND Theme = ifnull(J_3AMSEA_AFSTHEME, 'Not Set')
		     AND GridValue = ifnull(J_3AMSEA_J_3ASIZE,'Not Set')
		     AND CategoryValue = ifnull(J_3AMSEA_J_4KRCAT, 'Not Set')
		     AND PartNumber = ifnull(J_3AMSEA_MATNR,'Not Set'));
			 
			 
			 
UPDATE dim_partseason pse 
	SET  
pse.PartNumber_NoLeadZero= ifnull(case when length(pse.partnumber)=18 and pse.partnumber is integer then trim(leading '0' from pse.partnumber) else pse.partnumber end,'Not Set'),
			pse.dw_update_date = current_timestamp;			 