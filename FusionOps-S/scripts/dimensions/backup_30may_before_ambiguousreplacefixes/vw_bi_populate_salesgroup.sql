INSERT INTO dim_salesgroup(dim_salesgroupid,
                             SalesGroupCode,
                             SalesGroupName)
   SELECT 1,
          'Not Set',
          'Not Set'
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesgroup
               WHERE dim_salesgroupid = 1);

UPDATE       dim_salesgroup a
          FROM
             TVBVK t
       INNER JOIN
          TVGRT t1
       ON t.TVBVK_VKGRP = t1.TVGRT_VKGRP AND t1.TVGRT_SPRAS = 'E'
   SET a.SalesGroupName = ifnull(TVGRT_BEZEI, 'Not Set'),
			a.dw_update_date = current_timestamp
   WHERE a.SalesGroupCode = TVBVK_VKGRP;

delete from number_fountain m where m.table_name = 'dim_salesgroup';

insert into number_fountain
select 	'dim_salesgroup',
	ifnull(max(d.Dim_SalesGroupid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesgroup d
where d.Dim_SalesGroupid <> 1; 
   
   insert into dim_salesgroup
(	Dim_SalesGroupid,
	SalesGroupCode,
	SalesGroupName
)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesgroup') 
          + row_number() over(),
			  TVBVK_VKGRP,
		ifnull(TVGRT_BEZEI,'Not Set')
from TVBVK inner join TVGRT on TVBVK_VKGRP = TVGRT_VKGRP and TVGRT_SPRAS = 'E'
where not exists (select 1 from dim_salesgroup a where a.SalesGroupCode = TVBVK_VKGRP);