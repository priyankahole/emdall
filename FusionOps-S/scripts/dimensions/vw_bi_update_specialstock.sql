UPDATE    dim_SpecialStock st
       FROM
          t148t t
   SET st.Description = t.T148T_SOTXT
 WHERE st.RowIsCurrent = 1
  AND st.specialstockindicator = t.T148T_SOBKZ
;
