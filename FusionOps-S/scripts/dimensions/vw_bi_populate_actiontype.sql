
INSERT INTO dim_ActionType(dim_ActionTypeid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_ActionType
               WHERE dim_ActionTypeid = 1);

UPDATE 	dim_ActionType dat 
from 	actiontype aty
SET 	dat.ActionMnemonic = ifnull(aty.ACTION_MNEMONIC,'Not Set'),
		dat.Description = ifnull(aty.DESCRIPTION,'Not Set'),
		dat.DueByPeriod = aty.DUE_BY_PERIOD,
		dat.MatchingEnabled = aty.Matching_enabled,
		dat.dw_update_date = current_timestamp
WHERE dat.ActionTypeId = aty.ACTION_TYPE AND dat.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_ActionType';

insert into number_fountain
select 	'dim_ActionType',
	ifnull(max(d.dim_ActionTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ActionType d
where d.dim_ActionTypeid <> 1;
			   
INSERT INTO dim_ActionType(Dim_ActionTypeId,
                           ActionTypeId,
                           ActionMnemonic,
                           Description,
                           DueByPeriod,
                           MatchingEnabled,
                           RowIsCurrent,
                           RowStartDate)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_ActionType')
          + row_number() over(),
                  ACTION_TYPE,
          ifnull(ACTION_MNEMONIC,'Not Set'),
          ifnull(DESCRIPTION,'Not Set'),
          DUE_BY_PERIOD,
          Matching_enabled,
          1,
          current_timestamp
     FROM actiontype aty
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_actiontype
               WHERE ifnull(ActionTypeId,0) = aty.ACTION_TYPE AND RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_ActionType';
			   