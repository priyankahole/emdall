/* #########################################################################################################################################	*/
/* */
/*   Script         : vw_getLeadTime_std.sql */
/*   Author         : Lokesh */
/*   Created On     : 22 Jul 2013 */
/*  */
/*  */
/*   Description    : getLeadTime function migrated from MySQL  */
/* */
/*   Change History */
/*   Date            By        Version            Desc 		
/*   29-Jul-2013     Lokesh      1.1              																									*/
/*   22-Jul-2013     Lokesh      1.0              Standard function for getLeadTime in VW ( Created from Ashu's migrated script vw_getLeadTime) */
/*                                                Run this after running the fact-specific script that populates tmp_getLeadTime 	*/
/* ##########################################################################################################################################					*/

UPDATE tmp_getLeadTime
set processed_flag = 'Y'
WHERE processed_flag IS NULL;       /* Only pick up the rows which are not processed already */ 
 


Update tmp_getLeadTime 
SET v_leadTime = 0,
    idone=0
WHERE processed_flag = 'Y'	;

Update tmp_getLeadTime
Set v_leadTime = 0,
    idone=1
where pPart IS NULL OR pPlant IS NULL
AND processed_flag = 'Y';


Update tmp_getLeadTime
Set v_leadTime = ifnull(( Select ekt.EKPO_PLIFZ 
		   FROM EKKO_EKPO_EKET ekt
		   WHERE ekt.EKPO_EBELN = DocumentNumber
		   AND ekt.EKPO_EBELP = DocumentLineNumber),0) 
Where DocumentNumber IS NOT NULL AND DocumentLineNumber > 0
      AND DocumentType = 'PO'
      AND idone = 0
	  AND processed_flag = 'Y';

Drop table if exists EINE_00e;
Create table EINE_00e as Select first 0 * from EINE  ORDER BY EINE_PRDAT DESC, APLFZ DESC;

Update tmp_getLeadTime
Set v_leadTime = ifnull(( Select e.APLFZ
                           FROM EINE_00e e
                          WHERE     e.EBELN = DocumentNumber
                                AND e.EBELP = DocumentLineNumber
                                AND ifnull(e.EINE_ESOKZ, '0') = '0'),0)
Where DocumentNumber IS NOT NULL AND DocumentLineNumber > 0
      AND DocumentType = 'PO'
      AND idone = 0
      AND v_leadTime = 0
	  AND processed_flag = 'Y';


DROP TABLE IF EXISTS tmp_EINE_00e_EINA;
CREATE TABLE tmp_EINE_00e_EINA
AS
SELECT e.APLFZ,b.EKPO_EBELN,b.EKPO_EBELP,e.EBELN,e.EINE_ERDAT,ifnull(b.EKPO_AEDAT, b.EKKO_BEDAT) EKPO_AEDAT,
e.EINE_PRDAT, ifnull(e.EINE_ESOKZ, '0') EINE_ESOKZ
FROM EINE_00e e
  INNER JOIN EINA ea
		  ON e.INFNR = ea.INFNR
	   INNER JOIN EKKO_EKPO_EKET b
		  ON ea.MATNR = b.EKPO_MATNR
			 AND e.WERKS = b.EKPO_WERKS
			 AND ea.LIFNR = b.EKKO_LIFNR
ORDER BY EINE_PRDAT DESC, APLFZ DESC offset 0;

Update tmp_getLeadTime
Set v_leadTime =  IFNULL(( SELECT e.APLFZ
                             FROM tmp_EINE_00e_EINA e
                             WHERE     EKPO_EBELN = DocumentNumber
                                   AND EKPO_EBELP = DocumentLineNumber
                                   AND EBELN IS NULL
                                   AND APLFZ <> 999
                                   AND EINE_ERDAT <= EKPO_AEDAT                                    
                                   AND e.EINE_PRDAT >= EKPO_AEDAT
                                   AND EINE_ESOKZ = '0'),0)
Where DocumentNumber IS NOT NULL AND DocumentLineNumber > 0
      AND DocumentType = 'PO'
      AND idone = 0
      AND v_leadTime = 0 
	  AND processed_flag = 'Y';


Update tmp_getLeadTime
Set v_leadTime =  ifnull( (SELECT pr.EBAN_PLIFZ
                            FROM eban pr
                           WHERE pr.EBAN_BANFN = DocumentNumber
                                 AND pr.EBAN_BNFPO = DocumentLineNumber), 0)
Where DocumentType = 'PR'
      AND idone = 0
      AND v_leadTime = 0
	  AND processed_flag = 'Y';


Update tmp_getLeadTime
Set v_leadTime =   ifnull( (SELECT e.aplfz
                            FROM eine e , eban pr
                           WHERE e.INFNR = pr.EBAN_INFNR
                                 AND pr.EBAN_BANFN = DocumentNumber
                                 AND pr.EBAN_BNFPO = DocumentLineNumber
                                 AND e.APLFZ <> 999 ), 0)
Where DocumentType = 'PR'
      AND idone = 0
      AND v_leadTime = 0
	  AND processed_flag = 'Y';
 

Update tmp_getLeadTime glt
Set v_leadTime =    ifnull((  SELECT e.APLFZ
                                  FROM eina a, eine e,eban pr
                                  Where a.INFNR = e.INFNR
                                             AND a.MATNR = glt.pPart
                                             AND e.WERKS = glt.pPlant
                                          AND    pr.EBAN_MATNR = a.MATNR
                                             AND pr.EBAN_WERKS = e.WERKS
                                             AND a.LIFNR = glt.pVendor
                                             AND glt.pVendor IS NOT NULL
                                             AND e.APLFZ <> 999
                                             AND((pr.EBAN_LIFNR = glt.pVendor
                                                  AND pr.eban_lifnr IS NOT NULL)
                                                 OR(pr.EBAN_FLIEF = glt.pVendor
                                                    AND pr.EBAN_FLIEF IS NOT NULL))
                                             AND e.EINE_PRDAT >= pr.EBAN_BADAT
                                             AND e.EINE_ERDAT <= pr.EBAN_BADAT
                                             AND ifnull(e.EINE_ESOKZ, '0') = '0'),0)
Where DocumentType = 'PR'
      AND idone = 0
      AND v_leadTime = 0
	  AND processed_flag = 'Y';


Update tmp_getLeadTime
Set v_leadTime = ifnull((SELECT e.APLFZ
                               FROM eina a , eine e,plaf p
                               WHERE a.INFNR = e.INFNR
                                          AND a.MATNR = pPart
                                          AND e.WERKS = pPlant
                                          AND a.LIFNR = pVendor
                                          AND pVendor IS NOT NULL
                                          AND e.APLFZ <> 999
                                          AND ifnull(e.EINE_ESOKZ, '0') = '0'
                                       AND     p.PLAF_MATNR = a.MATNR
                                          AND p.PLAF_PLWRK = e.WERKS
                                          AND e.EINE_ERDAT <= p.PLAF_PERTR
                                          AND e.EINE_PRDAT >= p.PLAF_PERTR),0)
Where DocumentType = 'PLAN_ORDER'
      AND idone = 0
      AND v_leadTime = 0
	  AND processed_flag = 'Y';


Update tmp_getLeadTime
Set v_leadTime =  IFNULL((SELECT p.MARC_PLIFZ
                          FROM MARA_MARC_MAKT p
                         WHERE p.MARA_MATNR = pPart AND p.MARC_WERKS = pPlant),0)
Where  idone = 0
      AND v_leadTime = 0
	  AND processed_flag = 'Y';


Update tmp_getLeadTime
Set v_leadTime =  IFNULL((SELECT m.PLIFZ
                          FROM    LFM1 m,dim_plant dp
                               where  m.LIFNR = pVendor
                                  AND pVendor IS NOT NULL
                                  AND m.EKORG = dp.PurchOrg
                                  AND m.PLIFZ <> 999
                                  AND dp.PlantCode = pPlant),0)
Where  idone = 0
      AND v_leadTime = 0
	  AND processed_flag = 'Y';

Drop table if exists EINE_00e;
DROP TABLE IF EXISTS tmp_EINE_00e_EINA;

CALL VECTORWISE( COMBINE 'tmp_getLeadTime');
