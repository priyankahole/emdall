/* START OF CODE BLOCKS - tmp_getLeadTime would have the final the data    */

DELETE FROM tmp_getLeadTime
WHERE fact_script_name = 'bi_populate_mrp_fact';

INSERT INTO tmp_getLeadTime
(pPart,
pPlant,             
pVendor,            
DocumentNumber,     
DocumentLineNumber, 
DocumentType,
fact_script_name
)       
Select distinct k.MDKP_MATNR,
		k.MDKP_PLWRK,
		ifnull(EBAN_FLIEF, EBAN_LIFNR),
		eban_BANFN,
		eban_BNFPO,
		'PR','bi_populate_mrp_fact'
From   fact_mrp m,
	eban pr,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_BANFN
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL
        AND MDTB_DAT02 IS NOT NULL
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN;

	 

CALL VECTORWISE( COMBINE 'tmp_getLeadTime');

