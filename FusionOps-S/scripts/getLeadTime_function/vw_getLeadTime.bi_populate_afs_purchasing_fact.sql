/* START OF CODE BLOCKS - tmp_getLeadTime would have the final the data    */

DELETE FROM tmp_getLeadTime
WHERE fact_script_name = 'bi_populate_afs_purchasing_fact';

INSERT INTO tmp_getLeadTime
(pPart,
pPlant,             
pVendor,            
DocumentNumber,     
DocumentLineNumber, 
DocumentType,
fact_script_name
)       
SELECT DISTINCT EKPO_MATNR pPart, EKPO_WERKS pPlant, EKKO_LIFNR pVendor, EKPO_EBELN DocumentNumber, EKPO_EBELP DocumentLineNumber, 'PO' DocumentType,
	 'bi_populate_afs_purchasing_fact' fact_script_name
FROM ekko_ekpo_eket;	 

CALL VECTORWISE( COMBINE 'tmp_getLeadTime');

