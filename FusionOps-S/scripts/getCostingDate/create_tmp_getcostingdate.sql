/* #########################################################################################################################################	*/
/* */
/*   Script         : create_tmp_getcostingdate.sql */
/*   Author         : Lokesh */
/*   Created On     : 22 Jul 2013 */
/*  */
/*  */
/*   Description    : Create table tmp_getcostingdate  */
/* */
/*   Change History */
/*   Date            By        Version            Desc 																											*/
/*   22-Jul-2013     Lokesh      1.0               Create table definition for tmp_getcostingdate. To be used by VW scripts corresponding to getcostingdate mysql function 	*/
/* ##########################################################################################################################################					*/

DROP TABLE IF EXISTS tmp_getcostingdate;

CREATE TABLE tmp_getcostingdate  
(
        pCompanyCode    varchar(4),
	 pPlant          varchar(4),
	 pMaterialNo     varchar(18),
	 pFiYear         int,
	 pPeriod         int,
	 costingDate_Id  bigint,
	 rPPVPrice       decimal(22, 5),
	 pprevFiYear     INT,
	 pprevPeriod     INT,
	 pUMREZ_UMREN    decimal(22, 5),
	 pprevDates      Char(45),
	 pprevFromDate   ANSIDATE,
	 pprevToDate     ANSIDATE,	 
	 fact_script_name varchar(60),
	 processed_flag char(1),
	 upd_flag char(4)
);




