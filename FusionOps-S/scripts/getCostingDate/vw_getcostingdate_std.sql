/* #########################################################################################################################################	*/
/* */
/*   Script         : vw_getcostingdate_std.sql */
/*   Author         : Lokesh */
/*   Created On     : 22 Jul 2013 */
/*  */
/*  */
/*   Description    : getcostingdate function migrated from MySQL  */
/* */
/*   Change History */
/*   Date            By        Version            Desc 																											*/
/*   22-Jul-2013     Lokesh      1.0              Standard function for getcostingdate in VW ( Created from Ashu's migrated script vw_getLeadTime) */
/*                                                Run this after running the fact-specific script that populates tmp_getcostingdate	*/
/* ##########################################################################################################################################					*/

UPDATE tmp_getcostingdate
set processed_flag = 'Y',upd_flag = 'X'
WHERE processed_flag IS NULL;       /* Only pick up the rows which are not processed already */ 

 
update tmp_getcostingdate
SET pprevFiYear = pFiYear - 1,pprevPeriod = 12
WHERE pPeriod = 1
AND processed_flag = 'Y';

update tmp_getcostingdate
SET pprevFiYear = pFiYear,pprevPeriod = pPeriod - 1
WHERE ifnull(pPeriod,-1) <> 1
AND processed_flag = 'Y';

DROP TABLE IF EXISTS tmp_costingdate_mbew_no_bwtar;
CREATE TABLE tmp_costingdate_mbew_no_bwtar
AS 
SELECT a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,max(MBEW_ZPLD1) max_MBEW_ZPLD1
FROM MBEW_NO_BWTAR a
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;


update tmp_getcostingdate
      SET upd_flag = 'A',costingDate_Id =
                    (SELECT dt.Dim_DateId
                      FROM tmp_costingdate_mbew_no_bwtar a , Dim_Date dt
                      WHERE     a.MATNR = pMaterialNo
                            AND a.LFGJA = pFiYear
                            AND a.LFMON = pPeriod
                            AND a.BWKEY = pPlant
                            AND a.max_MBEW_ZPLD1 = dt.DateValue
                            AND dt.CompanyCode = pCompanyCode )
WHERE processed_flag = 'Y' AND upd_flag = 'X';


DROP TABLE IF EXISTS tmp_costingdate_mbew;
CREATE TABLE tmp_costingdate_mbew
AS 
SELECT a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,max(MBEW_ZPLD1) max_MBEW_ZPLD1
FROM MBEW a
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

update tmp_getcostingdate
      SET upd_flag = 'B',costingDate_Id =
                    (SELECT dt.Dim_DateId
                      FROM tmp_costingdate_mbew a , Dim_Date dt
                      WHERE     a.MATNR = pMaterialNo
                            AND a.LFGJA = pFiYear
                            AND a.LFMON = pPeriod
                            AND a.BWKEY = pPlant
                            AND a.max_MBEW_ZPLD1 = dt.DateValue
                            AND dt.CompanyCode = pCompanyCode )
WHERE processed_flag = 'Y' AND upd_flag = 'X';




update tmp_getcostingdate
      SET upd_flag = 'C',costingDate_Id =
                    (SELECT dt.Dim_DateId
                      FROM tmp_costingdate_mbew_no_bwtar a , Dim_Date dt
                      WHERE     a.MATNR = pMaterialNo
                            AND a.LFGJA = pprevFiYear
                            AND a.LFMON = pprevPeriod
                            AND a.BWKEY = pPlant
                            AND a.max_MBEW_ZPLD1 = dt.DateValue
                            AND dt.CompanyCode = pCompanyCode )
WHERE processed_flag = 'Y' AND upd_flag = 'X';


update tmp_getcostingdate
      SET upd_flag = 'D',costingDate_Id =
                    (SELECT dt.Dim_DateId
                      FROM tmp_costingdate_mbew a , Dim_Date dt
                      WHERE     a.MATNR = pMaterialNo
                            AND a.LFGJA = pprevFiYear
                            AND a.LFMON = pprevPeriod
                            AND a.BWKEY = pPlant
                            AND a.max_MBEW_ZPLD1 = dt.DateValue
                            AND dt.CompanyCode = pCompanyCode )
WHERE processed_flag = 'Y' AND upd_flag = 'X';


update tmp_getcostingdate
      SET upd_flag = 'E',costingDate_Id =
                    (SELECT dt.Dim_DateId
                      FROM tmp_costingdate_mbew_no_bwtar a , Dim_Date dt
                      WHERE     a.MATNR = pMaterialNo
                            AND a.BWKEY = pPlant
                            AND a.max_MBEW_ZPLD1 = dt.DateValue
                            AND dt.CompanyCode = pCompanyCode )
WHERE processed_flag = 'Y' AND upd_flag = 'X';


update tmp_getcostingdate
      SET upd_flag = 'F',costingDate_Id =
                    (SELECT dt.Dim_DateId
                      FROM tmp_costingdate_mbew a , Dim_Date dt
                      WHERE     a.MATNR = pMaterialNo
                            AND a.BWKEY = pPlant
                            AND a.max_MBEW_ZPLD1 = dt.DateValue
                            AND dt.CompanyCode = pCompanyCode )
WHERE processed_flag = 'Y' AND upd_flag = 'X';

update tmp_getcostingdate
SET costingDate_Id = 1
WHERE processed_flag = 'Y' AND upd_flag = 'X';


CALL VECTORWISE( COMBINE 'tmp_getcostingdate');

DROP TABLE IF EXISTS tmp_costingdate_mbew_no_bwtar;
DROP TABLE IF EXISTS tmp_costingdate_mbew;

