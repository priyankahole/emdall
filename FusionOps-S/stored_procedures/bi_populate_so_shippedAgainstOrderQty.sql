DROP PROCEDURE IF EXISTS fusionops_model_schema.bi_populate_so_shippedAgainstOrderQty;
CREATE PROCEDURE fusionops_model_schema.`bi_populate_so_shippedAgainstOrderQty`()
BEGIN

DECLARE done              INT DEFAULT 0;
DECLARE v_DlvrDocNo       VARCHAR(50);
DECLARE v_plantcode       VARCHAR(50);
DECLARE v_DlvrItemNo      INT;
DECLARE v_SalesDocNo      VARCHAR(50);
DECLARE v_SalesItemNo     INT;
DECLARE v_DeliveryQty     DECIMAL(18,4);
DECLARE v_TotalCmlRcvdQty DECIMAL(18,4);
DECLARE v_CmlRcvdQty      DECIMAL(18,4);
DECLARE v_AGI_Date        DATE;
DECLARE v_PGI_Date        DATE;
DECLARE v_GIDate          DATE;
DECLARE v_GIDate_tmp      DATE;
DECLARE v_GIDateMax       DATE;
DECLARE v_DlvrCost        DECIMAL(18,4);  
DECLARE v_SchedQty        DECIMAL(18,4);
DECLARE v_DlvrQty         DECIMAL(18,4);
DECLARE v_SchedNo         INT DEFAULT 0;
DECLARE v_SchduleNo       INT;
DECLARE v_SchedNoMax      INT;
DECLARE v_TotGRQty        DECIMAL(18,4);  
DECLARE v_Count           INT;

DECLARE cur1 CURSOR FOR
select distinct f.dd_SalesDocNo,
      f.dd_SalesItemNo,
      p.PlantCode
from fact_Salesorder f
inner join fact_SalesOrderDelivery sd on f.dd_SalesDocNo = sd.dd_SalesDocNo and f.dd_SalesItemNo = sd.dd_SalesItemNo
inner join likp_lips l on l.likp_vbeln = sd.dd_SalesDlvrDocNo and l.lips_posnr = sd.dd_Salesdlvritemno
inner join dim_salesorderitemstatus sois on sd.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
inner join dim_Date AGI on AGI.Dim_DateId = f.Dim_DateidActualGI
inner join dim_Date PGI on PGI.Dim_DateId = f.Dim_DateidGoodsIssue
INNER JOIN Dim_Plant p on p.Dim_Plantid = f.Dim_Plantid
where f.ct_ScheduleQtySalesUnit > 0 and f.dd_ItemRelForDelv = 'X' and sois.GoodsMovementStatus <> 'Not yet processed'
order by f.dd_SalesDocNo, f.dd_SalesItemNo;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

UPDATE fact_salesorder f set f.ct_ShippedAgnstOrderQty = 0, ct_CmlQtyReceived = 0;

set v_Count = 1;

OPEN cur1;
FETCH cur1 INTO 
      v_SalesDocNo,
      v_SalesItemNo,
      v_plantcode;
      
WHILE done = 0 DO  
  
  set v_GIDateMax = (select max(gi.DateValue)
                      from fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
                      where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                            and f.dd_ItemRelForDelv = 'X' and f.ct_ScheduleQtySalesUnit > 0); 
                            
  set v_DeliveryQty = ifnull((select sum(fd.ct_QtyDelivered) from fact_salesorderdelivery fd 
                              where fd.dd_SalesDocNo = v_SalesDocNo and fd.dd_SalesItemNo = v_SalesItemNo ),0);
                            
  set v_SchedNoMax = ifnull((select max(f.dd_ScheduleNo)
                      from fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
                      where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                            and f.dd_ItemRelForDelv = 'X' and gi.DateValue >= v_GIDateMax and f.ct_ScheduleQtySalesUnit > 0),0); 

  set v_TotalCmlRcvdQty = ifnull((select max(VBLB_ABEFZ)
                           from VBLB where VBLB_VBELN = v_SalesDocNo and VBLB_POSNR = v_SalesItemNo),0); 
                            

  IF (v_SchedNoMax > 0) THEN
  WHILE (v_DeliveryQty > 0) DO
  
    set v_TotGRQty = ifnull((select sum(f.ct_ShippedAgnstOrderQty) from fact_salesorder f 
                            where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo),0); 

    if ( v_TotGRQty > 0 ) then                  
        set v_GIDate = ifnull((select min(gi.DateValue) 
                                from fact_salesorder f
                                      inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
                                where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo and f.ct_ShippedAgnstOrderQty = 0
                                and f.ct_ScheduleQtySalesUnit > 0
                                and f.dd_ItemRelForDelv = 'X'),now());
        set v_SchedNo = ifnull((select min(f.dd_ScheduleNo) from 
                                 fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
                                where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo and
                                gi.DateValue = v_GIDate and f.dd_ScheduleNo > v_SchedNo and f.dd_ItemRelForDelv = 'X'
                                and f.ct_ScheduleQtySalesUnit > 0),0); 
                                
        set v_SchedQty = ifnull((select sum(f.ct_ScheduleQtySalesUnit) 
                                from fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
                                where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                      and (gi.DateValue < v_GIDate or (gi.DateValue = v_GIDate and f.dd_ScheduleNo <= v_SchedNo)) 
                                      and f.dd_ItemRelForDelv = 'X'),0);
      else
        set v_GIDate = (select min(gi.DateValue)
                        from fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
                        where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                              and f.dd_ItemRelForDelv = 'X' and f.ct_ScheduleQtySalesUnit > 0 and f.ct_ShippedAgnstOrderQty = 0);
        set v_SchedNo = 0;
        set v_SchedQty = 0; 
    end if;      
    
    if ((v_SchedQty - v_TotGRQty) > 0) then             
      set v_SchedQty = v_SchedQty - v_TotGRQty; 
    else
       set v_GIDate_tmp = (select min(gi.DateValue)
                        from fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
                        where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                 and ((gi.DateValue = v_GIDate and f.dd_ScheduleNo > v_SchedNo) or gi.DateValue > v_GIDate)                         
                              and f.dd_ItemRelForDelv = 'X' and f.ct_ScheduleQtySalesUnit > 0 and f.ct_ShippedAgnstOrderQty = 0);
        set v_SchedNo = ifnull((select min(f.dd_ScheduleNo)
                                from fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
                                where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo 
                                    and gi.DateValue = v_GIDate_tmp and ((gi.DateValue = v_GIDate and f.dd_ScheduleNo > v_SchedNo) or gi.DateValue > v_GIDate)
                                    and f.ct_ScheduleQtySalesUnit > 0 and f.dd_ItemRelForDelv = 'X'),0);
        set v_SchedQty = ifnull((select f.ct_ScheduleQtySalesUnit
                                from fact_salesorder f 
                                where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo 
                                and f.dd_ScheduleNo = v_SchedNo),0); 
        set v_GIDate = v_GIDate_tmp;
    end if;
    
    if (v_SchedNo = 0) then 
      set v_SchedQty = 0;
      set v_SchedNo = v_SchedNoMax;
    end if;
    
    if (v_SchedQty > v_DeliveryQty) then
      set v_SchedQty = v_DeliveryQty;
      set v_DeliveryQty = 0;
    else
      set v_DeliveryQty = v_DeliveryQty - v_SchedQty;
    end if;
    
    set v_DlvrQty = v_SchedQty;
    if(v_SchedNo = v_SchedNoMax) then
      set v_DlvrQty = v_SchedQty + v_DeliveryQty;
      set v_DeliveryQty = 0;
    end if;    

    if ( v_TotalCmlRcvdQty > v_DlvrQty) then
      set v_CmlRcvdQty = v_DlvrQty;
    else
      set v_CmlRcvdQty = v_TotalCmlRcvdQty;
    end if;
     
    set v_TotalCmlRcvdQty = v_TotalCmlRcvdQty - v_CmlRcvdQty;
    
    UPDATE fact_Salesorder f
      SET f.ct_ShippedAgnstOrderQty = v_DlvrQty
          #,f.ct_CmlQtyReceived = v_CmlRcvdQty
    WHERE     f.dd_SalesDocNo = v_SalesDocNo
          AND f.dd_SalesItemNo = v_SalesItemNo
          AND f.dd_ScheduleNo = v_SchedNo;
          
  END WHILE;
  END IF;

set v_Count = v_Count + 1;

FETCH cur1 INTO 
       v_SalesDocNo,
      v_SalesItemNo,
      v_plantcode;
END WHILE;
CLOSE cur1;


END;
