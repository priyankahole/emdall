DROP PROCEDURE IF EXISTS fusionops_model_schema.bi_populate_stocksummary_fact;
CREATE PROCEDURE fusionops_model_schema.`bi_populate_stocksummary_fact`()
BEGIN

DECLARE vRunID varchar(64);
DECLARE pGlobalCurrency varchar(3);

SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');
SET vRunID = newid();

ALTER TABLE fact_stocksummary DISABLE KEYS;
SET FOREIGN_KEY_CHECKS = 0;

DELETE FROM fact_stocksummary
 WHERE Dim_ActionStateid = 2
       AND EXISTS
              (SELECT 1
                 FROM dim_date dd
                WHERE dd.Dim_Dateid = Dim_DateidRecorded
                      AND week(current_date) = dd.CalendarWeek);

DELETE FROM fact_stocksummary
 WHERE EXISTS
          (SELECT 1
             FROM dim_date d
            WHERE d.Dim_Dateid = Dim_DateidRecorded
                  AND d.DateValue < date_sub(current_date, INTERVAL 3 MONTH))
       AND ct_Completed = 1;


UPDATE fact_stocksummary sm,
       dim_date d,
       dim_company dc
   SET sm.ct_Completed = 1,
       sm.Dim_ActionStateid = 3,
       sm.Dim_DateidRecordedEnd = d.Dim_Dateid
 WHERE     d.CompanyCode = dc.CompanyCode
       AND d.DateValue = current_date
       AND sm.Dim_Companyid = dc.Dim_Companyid
       AND dc.RowIsCurrent = 1
       AND sm.Dim_ActionStateid = 2;



UPDATE fact_stocksummary sm
   SET sm.ct_QtyCancelRequestTotal =
          (SELECT ifnull(sum(m.ct_QtyMRP), 0)
             FROM fact_mrp m
                  INNER JOIN dim_mrpexception ex
                     ON m.dim_mrpexceptionID1 = ex.dim_mrpexceptionID
                        AND ex.exceptionkey = 'U3'
                  INNER JOIN dim_mrpelement me
                     ON me.Dim_MRPElementID = m.Dim_MRPElementid
                        AND me.MRPElement IN ('BE', 'LA', 'LE')
                  INNER JOIN dim_company dc
                     ON dc.Dim_Companyid = m.Dim_Companyid
            WHERE     m.Dim_ActionStateid = 2
                  AND sm.Dim_Plantid = m.Dim_Plantid
                  AND sm.Dim_Partid = m.Dim_Partid),
       sm.amt_CancelRequestTotal =
          (SELECT ifnull(sum(m.amt_ExtendedPrice),0)
             FROM fact_mrp m
                  INNER JOIN dim_mrpexception ex
                     ON m.dim_mrpexceptionID1 = ex.dim_mrpexceptionID
                        AND ex.exceptionkey = 'U3'
                  INNER JOIN dim_mrpelement me
                     ON me.Dim_MRPElementID = m.Dim_MRPElementid
                        AND me.MRPElement IN ('BE', 'LA', 'LE')
                  INNER JOIN dim_company dc
                     ON dc.Dim_Companyid = m.Dim_Companyid
            WHERE     m.Dim_ActionStateid = 2
                  AND sm.Dim_Plantid = m.Dim_Plantid
                  AND sm.Dim_Partid = m.Dim_Partid),
       sm.amt_CancelRequestTotal_GBL =
          (SELECT ifnull(sum(m.amt_ExtendedPrice_GBL),0)
             FROM fact_mrp m
                  INNER JOIN dim_mrpexception ex
                     ON m.dim_mrpexceptionID1 = ex.dim_mrpexceptionID
                        AND ex.exceptionkey = 'U3'
                  INNER JOIN dim_mrpelement me
                     ON me.Dim_MRPElementID = m.Dim_MRPElementid
                        AND me.MRPElement IN ('BE', 'LA', 'LE')
                  INNER JOIN dim_company dc
                     ON dc.Dim_Companyid = m.Dim_Companyid
            WHERE     m.Dim_ActionStateid = 2
                  AND sm.Dim_Plantid = m.Dim_Plantid
                  AND sm.Dim_Partid = m.Dim_Partid);



INSERT INTO fact_stocksummary(Dim_Companyid,
                              Dim_ActionStateid,
                              Dim_DateidRecorded,
                              Dim_DateidRecordedEnd,
                              Dim_Plantid,
                              Dim_Partid,
                              Dim_Currencyid,
                              dd_runid,
                              ct_QtyCancelRequestTotal,
                              amt_CancelRequestTotal,
                              amt_CancelRequestTotal_GBL,
                              ct_Completed)
SELECT m.Dim_Companyid,
       m.Dim_ActionStateid,
       rd.Dim_Dateid,
       1,
       m.Dim_Plantid,
       m.Dim_Partid,
       m.Dim_Currencyid,
       vRunID,
       sum(m.ct_QtyMRP) ct_QtyCancelRequestTotal,
       sum(m.amt_ExtendedPrice) amt_CancelRequestTotal,
       sum(m.amt_ExtendedPrice_GBL ) amt_CancelRequestTotal_GBL,
       0
  FROM fact_mrp m
       INNER JOIN dim_mrpexception ex
          ON m.dim_mrpexceptionID1 = ex.dim_mrpexceptionID
             AND ex.exceptionkey = 'U3'
       INNER JOIN dim_mrpelement me
          ON me.Dim_MRPElementID = m.Dim_MRPElementid
             AND me.MRPElement IN ('BE', 'LA', 'LE')
       INNER JOIN dim_company dc
          ON dc.Dim_Companyid = m.Dim_Companyid
       INNER JOIN dim_date rd
          ON rd.CompanyCode = dc.CompanyCode AND rd.DateValue = current_date
 WHERE m.Dim_ActionStateid = 2
       AND NOT EXISTS
                  (SELECT 1
                     FROM fact_stocksummary sm
                    WHERE     sm.Dim_ActionStateid = 2
                          AND sm.Dim_Plantid = m.Dim_Plantid
                          AND sm.Dim_Partid = m.Dim_Partid)
GROUP BY m.Dim_Plantid, m.Dim_Partid;

   
   
UPDATE fact_stocksummary sm, dim_company dc, dim_part dp
   SET ct_QtyRequisition30day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              m.MENGE
                           ELSE
                              0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       ct_QtyRequisition60day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              m.MENGE
                           ELSE
                              0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       ct_QtyRequisition90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              m.MENGE
                           ELSE
                              0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       ct_QtyRequisitionOver90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) > 90 THEN m.MENGE
                           ELSE 0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       ct_QtyRequisitionPastDue =
          ifnull(
             (SELECT SUM(CASE WHEN nd.DateValue < now() THEN m.MENGE ELSE 0 END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       ct_QtyRequisitionTotal =
          ifnull(
             (SELECT SUM(m.MENGE)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_Requisition30day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_Requisition30day_GBL =
          ifnull(
             (SELECT SUM(
                         ( CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END ) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_Requisition60day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_Requisition60day_GBL =
          ifnull(
             (SELECT SUM(
                        ( CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_Requisition90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_Requisition90day_GBL =
          ifnull(
             (SELECT SUM(
                       ( CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),

       amt_RequisitionOver90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_RequisitionOver90day_GBL =
          ifnull(
             (SELECT SUM(
                        (CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0) ,
             0),
       amt_RequisitionPastDue =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN nd.DateValue < now()
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_RequisitionPastDue_GBL =
          ifnull(
             (SELECT SUM(
                        ( CASE
                           WHEN nd.DateValue < now()
                           THEN
                              ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                           ELSE
                              0
                        END) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_RequisitionTotal =
          ifnull(
             (SELECT SUM(ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE)
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0),
       amt_RequisitionTotal_GBL =
          ifnull(
             (SELECT SUM( (ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
                FROM eban m, dim_date nd
               WHERE     nd.DateValue = m.LFDAT
                     AND nd.CompanyCode = dc.CompanyCode
                     AND m.MATNR = dp.PartNumber
                     AND m.WERKS = dp.Plant
                     AND EBAKZ IS NULL
                     AND LOEKZ IS NULL
                     AND STATU = 'N'
                     AND MENGE > 0),
             0)
 WHERE     sm.Dim_ActionStateid = 2
       AND dp.Dim_Partid = sm.Dim_Partid
       AND dc.Dim_Companyid = sm.Dim_Companyid;
   


INSERT INTO fact_stocksummary(Dim_Companyid,
                              Dim_ActionStateid,
                              Dim_DateidRecorded,
                              Dim_DateidRecordedEnd,
                              Dim_Plantid,
                              Dim_Partid,
                              Dim_Currencyid,
                              dd_runid,
                              ct_QtyRequisition30day,
                              ct_QtyRequisition60day,
                              ct_QtyRequisition90day,
                              ct_QtyRequisitionOver90day,
                              ct_QtyRequisitionPastDue,
                              ct_QtyRequisitionTotal,
                              amt_Requisition30day,
                              amt_Requisition60day,
                              amt_Requisition90day,
                              amt_RequisitionOver90day,
                              amt_RequisitionPastDue,
                              amt_RequisitionTotal,
                              amt_Requisition30day_GBL,
                              amt_Requisition60day_GBL,
                              amt_Requisition90day_GBL,
                              amt_RequisitionOver90day_GBL,
                              amt_RequisitionPastDue_GBL,
                              amt_RequisitionTotal_GBL,
                              ct_Completed)
   SELECT dc.Dim_Companyid,
          2,
          rd.Dim_Dateid,
          1,
          dpl.Dim_Plantid,
          dp.Dim_Partid,
          dcur.Dim_Currencyid,
          vRunID,
          SUM(
             CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30 THEN m.MENGE
                ELSE 0
             END)
             ct_QtyRequisition30day,
          SUM(
             CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60 THEN m.MENGE
                ELSE 0
             END)
             ct_QtyRequisition60day,
          SUM(
             CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90 THEN m.MENGE
                ELSE 0
             END)
             ct_QtyRequisition90day,
          SUM(
             CASE WHEN datediff(nd.DateValue, now()) > 90 THEN m.MENGE ELSE 0 END)
             ct_QtyRequisitionOver90day,
          SUM(
             CASE WHEN nd.DateValue < now() THEN m.MENGE ELSE 0 END)
             ct_QtyRequisitionPastDue,  
          sum(m.MENGE) ct_QtyRequisitionTotal,
          SUM(
             CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END)
             amt_Requisition30day,
          SUM(
             CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END)
             amt_Requisition60day,
          SUM(
             CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END)
             amt_Requisition90day,
          SUM(
             CASE
                WHEN datediff(nd.DateValue, now()) > 90
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END)
             amt_RequisitionOver90day,
             SUM(
             CASE
                WHEN nd.DateValue < now()
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END)
             amt_RequisitionPastDue,
          sum(ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE)
             amt_RequisitionTotal,
          SUM(
             ( CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
             amt_Requisition30day_GBL,
          SUM(
              ( CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END ) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
             amt_Requisition60day_GBL,
          SUM(
             ( CASE
                WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
             amt_Requisition90day_GBL,
          SUM(
             ( CASE
                WHEN datediff(nd.DateValue, now()) > 90
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
             amt_RequisitionOver90day_GBL,
             SUM(
               ( CASE
                WHEN nd.DateValue < now()
                THEN
                   ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE
                ELSE
                   0
             END) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
             amt_RequisitionPastDue_GBL,
          sum( (ifnull(PREIS / ifnull(PEINH, 1), 0) * m.MENGE) * getExchangeRate( dc.Currency, pGlobalCurrency,null,m.EBAN_FRGDT) )
             amt_RequisitionTotal_GBL,
          0
     FROM             EBAN m
                   INNER JOIN
                      dim_part dp
                   ON     dp.PartNumber = m.MATNR
                      AND dp.Plant = m.WERKS
                      AND dp.RowIsCurrent = 1
                INNER JOIN
                   dim_plant dpl
                ON dpl.PlantCode = m.WERKS AND dpl.RowIsCurrent = 1
             INNER JOIN
                dim_company dc
             ON dc.CompanyCode = dpl.CompanyCode AND dc.RowIsCurrent = 1
          INNER JOIN
             dim_currency dcur
          ON dc.Currency = dcur.CurrencyCode,
          dim_date rd,
          dim_date nd
    WHERE dp.Dim_Partid <> 1
          AND nd.CompanyCode = dc.CompanyCode
          AND nd.DateValue = m.LFDAT
          AND EBAKZ IS NULL
          AND LOEKZ IS NULL
          AND STATU = 'N'
          AND MENGE > 0
          AND rd.CompanyCode = dc.CompanyCode
          AND rd.DateValue = current_date
          AND NOT EXISTS
                     (SELECT 1
                        FROM fact_stocksummary sm
                       WHERE Dim_ActionStateid = 2
                             AND sm.Dim_Plantid = dpl.Dim_Plantid
                             AND sm.Dim_Partid = dp.Dim_Partid)
   GROUP BY dpl.Dim_Plantid, dp.Dim_Partid;
   



UPDATE fact_stocksummary sm
   SET ct_QtyPurchasingDelivery30day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              ct_DeliveryQty - ct_ReceivedQty
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyPurchasingDelivery60day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              ct_DeliveryQty - ct_ReceivedQty
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyPurchasingDelivery90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              ct_DeliveryQty - ct_ReceivedQty
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyPurchasingDeliveryOver90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              ct_DeliveryQty - ct_ReceivedQty
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyPurchasingDeliveryPastDue =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN nd.DateValue < now()
                           THEN
                              ct_DeliveryQty - ct_ReceivedQty
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyPurchasingDeliveryTotal =
          ifnull((SELECT SUM(ct_DeliveryQty - ct_ReceivedQty)
                    FROM fact_Purchase m
                         INNER JOIN dim_purchasemisc pmsc
                            ON m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                               AND pmsc.ItemDeliveryComplete = 'Not Set'
                               AND pmsc.DeliveryComplete = 'Not Set'
                               AND pmsc.ItemReject = 'Not Set'
                               AND pmsc.ItemReturn = 'Not Set'
                         INNER JOIN dim_date nd
                            ON m.Dim_DateidDelivery = nd.Dim_Dateid
                   WHERE m.Dim_Partid = sm.Dim_Partid),
                 0),
       amt_PurchasingDelivery30day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDelivery30day_GBL =
          ifnull(
             (SELECT SUM(
                       ( CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END) * m.amt_ExchangeRate_GBL )
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDelivery60day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDelivery60day_GBL =
          ifnull(
             (SELECT SUM(
                        ( CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END) * m.amt_ExchangeRate_GBL )
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDelivery90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDelivery90day_GBL =
          ifnull(
             (SELECT SUM(
                        ( CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END) * m.amt_ExchangeRate_GBL )
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),       
       amt_PurchasingDeliveryOver90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDeliveryOver90day_GBL =
          ifnull(
             (SELECT SUM(
                        ( CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END) * m.amt_ExchangeRate_GBL )
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDeliveryPastDue =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN nd.DateValue < now()
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END)
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDeliveryPastDue_GBL =
          ifnull(
             (SELECT SUM(
                        ( CASE
                           WHEN nd.DateValue < now()
                           THEN
                              (ct_DeliveryQty - ct_ReceivedQty)
                              * (amt_DeliveryTotal / ct_DeliveryQty)
                           ELSE
                              0
                        END) * m.amt_ExchangeRate_GBL )
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDeliveryTotal =
          ifnull(
             (SELECT SUM(
                        (ct_DeliveryQty - ct_ReceivedQty)
                        * (amt_DeliveryTotal / ct_DeliveryQty))
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_PurchasingDeliveryTotal_GBL =
          ifnull(
             (SELECT SUM(
                        (ct_DeliveryQty - ct_ReceivedQty)
                        * (amt_DeliveryTotal / ct_DeliveryQty) * m.amt_ExchangeRate_GBL )
                FROM fact_Purchase m
                     INNER JOIN dim_purchasemisc pmsc
                        ON     m.Dim_PurchaseMiscid = pmsc.Dim_PurchaseMiscid
                           AND pmsc.ItemDeliveryComplete = 'Not Set'
                           AND pmsc.DeliveryComplete = 'Not Set'
                           AND pmsc.ItemReject = 'Not Set'
                           AND pmsc.ItemReturn = 'Not Set'
                     INNER JOIN dim_date nd
                        ON m.Dim_DateidDelivery = nd.Dim_Dateid
               WHERE m.Dim_Partid = sm.Dim_Partid),
             0)
 WHERE sm.Dim_ActionStateid = 2;
 


INSERT INTO fact_stocksummary(Dim_Companyid,
                              Dim_ActionStateid,
                              Dim_DateidRecorded,
                              Dim_DateidRecordedEnd,
                              Dim_Plantid,
                              Dim_Partid,
                              Dim_Currencyid,
                              dd_runid,
                              ct_Completed,
                              ct_QtyPurchasingDelivery30day,
                              ct_QtyPurchasingDelivery60day,
                              ct_QtyPurchasingDelivery90day,
                              ct_QtyPurchasingDeliveryOver90day,
                              ct_QtyPurchasingDeliveryPastDue,
                              ct_QtyPurchasingDeliveryTotal,
                              amt_PurchasingDelivery30day,
                              amt_PurchasingDelivery60day,
                              amt_PurchasingDelivery90day,
                              amt_PurchasingDeliveryOver90day,
                              amt_PurchasingDeliveryPastDue,
                              amt_PurchasingDeliveryTotal,
                              amt_PurchasingDelivery30day_GBL,
                              amt_PurchasingDelivery60day_GBL,
                              amt_PurchasingDelivery90day_GBL,
                              amt_PurchasingDeliveryOver90day_GBL,
                              amt_PurchasingDeliveryPastDue_GBL,
                              amt_PurchasingDeliveryTotal_GBL)


SELECT Dim_Companyid,
       2 Dim_ActionStateid,
       rd.Dim_Dateid Dim_DateidRecorded,
       1 Dim_DateidRecordedEnd,
       Dim_Plantid,
       Dim_Partid,
       Dim_Currencyid,
       VRunID,
       0 ct_Completed,
       SUM(
          CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
             THEN
                (ct_DeliveryQty - ct_ReceivedQty)
             ELSE
                0
          END)
          ct_QtyPurchasingDelivery30day,
       SUM(
          CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
             THEN
                (ct_DeliveryQty - ct_ReceivedQty)
             ELSE
                0
          END)
          ct_QtyPurchasingDelivery60day,
       SUM(
          CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
             THEN
                (ct_DeliveryQty - ct_ReceivedQty)
             ELSE
                0
          END)
          ct_QtyPurchasingDelivery90day,
       SUM(
          CASE
             WHEN datediff(dd.DateValue, now()) > 90
             THEN
                (ct_DeliveryQty - ct_ReceivedQty)
             ELSE
                0
          END)
          ct_QtyPurchasingDeliveryOver90day,
       SUM(
          CASE
             WHEN dd.DateValue < now() THEN (ct_DeliveryQty - ct_ReceivedQty)
             ELSE 0
          END)
          ct_QtyPurchasingDeliveryPastDue,
       sum(ct_DeliveryQty - ct_ReceivedQty) ct_QtyPurchasingDeliveryTotal,
       SUM(
          CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END)
          amt_PurchasingDelivery30day,
       SUM(
          CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END)
          amt_PurchasingDelivery60day,
       SUM(
          CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END)
          amt_PurchasingDelivery90day,
       SUM(
          CASE
             WHEN datediff(dd.DateValue, now()) > 90
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END)
          amt_PurchasingDeliveryOver90day,
       SUM(
          CASE
             WHEN dd.DateValue < now()
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END)
          amt_PurchasingDeliveryPastDue,
       sum(
          (amt_DeliveryTotal / ct_DeliveryQty)
          * (ct_DeliveryQty - ct_ReceivedQty))
          amt_PurchasingDeliveryTotal,
       SUM(
          ( CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END) * amt_ExchangeRate_GBL) 
          amt_PurchasingDelivery30day_GBL,
       SUM(
          ( CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END) * amt_ExchangeRate_GBL) 
          amt_PurchasingDelivery60day_GBL,
       SUM(
          ( CASE
             WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END) * amt_ExchangeRate_GBL) 
          amt_PurchasingDelivery90day_GBL,
       SUM(
          (CASE
             WHEN datediff(dd.DateValue, now()) > 90
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty))
             ELSE
                0
          END) * amt_ExchangeRate_GBL) 
          amt_PurchasingDeliveryOver90day_GBL,
       SUM(
          CASE
             WHEN dd.DateValue < now()
             THEN
                ((amt_DeliveryTotal / ct_DeliveryQty)
                 * (ct_DeliveryQty - ct_ReceivedQty) * amt_ExchangeRate_GBL)
             ELSE
                0
          END)
          amt_PurchasingDeliveryPastDue_GBL,
       sum(
          (amt_DeliveryTotal / ct_DeliveryQty)
          * (ct_DeliveryQty - ct_ReceivedQty) * amt_ExchangeRate_GBL)
          amt_PurchasingDeliveryTotal_GBL
  FROM fact_purchase m
       INNER JOIN dim_purchasemisc pmsc
          ON     pmsc.ItemDeliveryComplete = 'Not Set'
             AND pmsc.DeliveryComplete = 'Not Set'
             AND pmsc.ItemReject = 'Not Set'
             AND pmsc.ItemReturn = 'Not Set'
       INNER JOIN dim_plant dp
          ON m.Dim_PlantidOrdering = dp.Dim_Plantid
       INNER JOIN dim_company dc
          ON dp.CompanyCode = dc.CompanyCode
       INNER JOIN dim_date dd
          ON m.Dim_DateidDelivery = dd.Dim_Dateid
       INNER JOIN dim_date rd
          ON rd.CompanyCode = dc.CompanyCode AND rd.DateValue = current_date
 WHERE m.Dim_Partid <> 1 AND (ct_DeliveryQty - ct_ReceivedQty) > 0
       AND NOT EXISTS
                  (SELECT 1
                     FROM fact_stocksummary sm
                    WHERE     sm.Dim_ActionStateid = 2
                          AND sm.Dim_Plantid = m.Dim_PlantidOrdering
                          AND sm.Dim_Partid = m.Dim_Partid)
GROUP BY m.Dim_PlantidOrdering, m.Dim_Partid;



UPDATE fact_stocksummary fs
   SET ct_QtyStock =
          ifnull(
             (SELECT sum(fi.ct_OpenStock)
                FROM fact_inventory fi
               WHERE fi.Dim_Plantid = fs.Dim_Plantid
                     AND fi.Dim_Partid = fs.Dim_Partid),
             0),
       amt_Stock =
          ifnull(
             (SELECT sum(fi.amt_COGS)
                FROM fact_inventory fi
               WHERE fi.Dim_Plantid = fs.Dim_Plantid
                     AND fi.Dim_Partid = fs.Dim_Partid),
             0),
       amt_Stock_GBL =
          ifnull(
             (SELECT sum(fi.amt_COGS_GBL)
                FROM fact_inventory fi
               WHERE fi.Dim_Plantid = fs.Dim_Plantid
                     AND fi.Dim_Partid = fs.Dim_Partid),
             0)
 WHERE fs.Dim_ActionStateid = 2;



INSERT INTO fact_stocksummary(Dim_Companyid,
                              Dim_ActionStateid,
                              Dim_DateidRecorded,
                              Dim_DateidRecordedEnd,
                              Dim_Plantid,
                              Dim_Partid,
                              Dim_Currencyid,
                              dd_runid,
                              ct_Completed,
                              ct_QtyStock,
                              amt_Stock,
                              amt_Stock_GBL)
SELECT dc.Dim_Companyid,
       2 Dim_ActionStateid,
       rd.Dim_Dateid,
       1 Dim_DateidRecordedEnd,
       Dim_Plantid,
       Dim_Partid,
       Dim_Currencyid,
       vRunID,
       0 ct_Completed,
       sum(fi.ct_OpenStock),
       sum(fi.amt_COGS),
       sum(fi.amt_COGS_GBL )
  FROM fact_inventory fi,
             dim_company dc
          INNER JOIN
             dim_date rd
          ON rd.CompanyCode = dc.CompanyCode AND rd.DateValue = current_date
       INNER JOIN
          dim_currency dcr
       ON dc.Currency = dcr.CurrencyCode
 WHERE fi.ct_OpenStock > 0 AND fi.Dim_Companyid = dc.Dim_Companyid
       AND NOT EXISTS
                  (SELECT 1
                     FROM fact_stocksummary sm
                    WHERE     sm.Dim_ActionStateid = 2
                          AND sm.Dim_Partid = fi.Dim_Partid
                          AND sm.Dim_Plantid = fi.Dim_Plantid)
GROUP BY fi.Dim_Plantid, fi.Dim_Partid;
   
   
   
   UPDATE fact_stocksummary sm
   SET ct_QtyForecast30day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              m.ct_QtyTotal
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyForecast60day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              m.ct_QtyTotal
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyForecast90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              m.ct_QtyTotal
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyForecastOver90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              m.ct_QtyTotal
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyForecastPastDue =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN nd.DateValue < now() THEN m.ct_QtyTotal
                           ELSE 0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtyForecastTotal =
          ifnull(
             (SELECT SUM(m.ct_QtyTotal)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_Forecast30day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              m.amt_ExtendedPrice
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_Forecast60day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              m.amt_ExtendedPrice
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_Forecast90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              m.amt_ExtendedPrice
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_ForecastOver90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              m.amt_ExtendedPrice
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_ForecastPastDue =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN nd.DateValue < now() THEN m.amt_ExtendedPrice
                           ELSE 0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_ForecastTotal =
          ifnull(
             (SELECT SUM(m.amt_ExtendedPrice)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_Forecast30day_GBL =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              m.amt_ExtendedPrice_GBL
                           ELSE
                              0
                        END)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_Forecast60day_GBL =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              m.amt_ExtendedPrice_GBL
                           ELSE
                              0
                        END) 
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_Forecast90day_GBL =
          ifnull(
             (SELECT SUM(
                         CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              m.amt_ExtendedPrice_GBL
                           ELSE
                              0
                        END ) 
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_ForecastOver90day_GBL =
          ifnull(
             (SELECT SUM(
                         CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              m.amt_ExtendedPrice_GBL
                           ELSE
                              0
                        END) 
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_ForecastPastDue_GBL =
          ifnull(
             (SELECT SUM(
                         CASE
                           WHEN nd.DateValue < now() THEN m.amt_ExtendedPrice
                           ELSE 0
                        END) 
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_ForecastTotal_GBL =
          ifnull(
             (SELECT SUM(m.amt_ExtendedPrice_GBL)
                FROM fact_planorder m, dim_date nd
               WHERE m.Dim_dateidFinish = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0);
   
   
INSERT INTO fact_stocksummary(Dim_Companyid,
                              Dim_ActionStateid,
                              Dim_DateidRecorded,
                              Dim_DateidRecordedEnd,
                              Dim_Plantid,
                              Dim_Partid,
                              Dim_Currencyid,
                              dd_runid,
                              ct_Completed,
                              ct_QtyForecast30day,
                              ct_QtyForecast60day,
                              ct_QtyForecast90day,
                              ct_QtyForecastOver90day,
                              ct_QtyForecastPastDue,
                              ct_QtyForecastTotal,
                              amt_Forecast30day,
                              amt_Forecast60day,
                              amt_Forecast90day,
                              amt_ForecastOver90day,
                              amt_ForecastPastDue,
                              amt_ForecastTotal,
                              amt_Forecast30day_GBL,
                              amt_Forecast60day_GBL,
                              amt_Forecast90day_GBL,
                              amt_ForecastOver90day_GBL,
                              amt_ForecastPastDue_GBL,
                              amt_ForecastTotal_GBL)
   SELECT dc.Dim_Companyid,
          2 Dim_ActionStateid,
          rd.Dim_Dateid Dim_DateidRecorded,
          1 Dim_DateidRecordedEnd,
          Dim_Plantid,
          Dim_Partid,
          Dim_Currencyid,
          VRunID,
          0 ct_Completed,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
                THEN
                   m.ct_QtyTotal
                ELSE
                   0
             END)
             ct_QtyForecast30day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
                THEN
                   m.ct_QtyTotal
                ELSE
                   0
             END)
             ct_QtyForecast60day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
                THEN
                   m.ct_QtyTotal
                ELSE
                   0
             END)
             ct_QtyForecast90day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) > 90 THEN m.ct_QtyTotal
                ELSE 0
             END)
             ct_QtyForecastOver90day,
          SUM(CASE WHEN dd.DateValue < now() THEN m.ct_QtyTotal ELSE 0 END)
             ct_QtyForecastPastDue,
          sum(ct_QtyTotal) ct_QtyForecastTotal,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
                THEN
                   m.amt_ExtendedPrice
                ELSE
                   0
             END)
             amt_Forecast30day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
                THEN
                   m.amt_ExtendedPrice
                ELSE
                   0
             END)
             amt_Forecast60day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
                THEN
                   m.amt_ExtendedPrice
                ELSE
                   0
             END)
             amt_Forecast90day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) > 90 THEN m.amt_ExtendedPrice
                ELSE 0
             END)
             amt_ForecastOver90day,
          SUM(CASE WHEN dd.DateValue < now() THEN m.amt_ExtendedPrice ELSE 0 END)
             amt_ForecastPastDue,
          sum(m.amt_ExtendedPrice) amt_ForecastTotal,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
                THEN
                   m.amt_ExtendedPrice_GBL
                ELSE
                   0
             END)
             amt_Forecast30day_GBL,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
                THEN
                   m.amt_ExtendedPrice_GBL
                ELSE
                   0
             END)
             amt_Forecast60day_GBL,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
                THEN
                   m.amt_ExtendedPrice_GBL
                ELSE
                   0
             END)
             amt_Forecast90day_GBL,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) > 90 THEN m.amt_ExtendedPrice_GBL
                ELSE 0
             END)
             amt_ForecastOver90day_GBL,
          SUM(CASE WHEN dd.DateValue < now() THEN m.amt_ExtendedPrice_GBL ELSE 0 END)
             amt_ForecastPastDue_GBL,
          sum(m.amt_ExtendedPrice_GBL) amt_ForecastTotal_GBL
     FROM fact_planorder m
          INNER JOIN dim_company dc
             ON m.Dim_Companyid = dc.Dim_Companyid
          INNER JOIN dim_date dd
             ON m.Dim_dateidFinish = dd.Dim_Dateid
          INNER JOIN dim_date rd
             ON rd.CompanyCode = dc.CompanyCode AND rd.DateValue = current_date
    WHERE m.Dim_Partid <> 1
          AND m.ct_QtyTotal > 0
          AND NOT EXISTS
                     (SELECT 1
                        FROM fact_stocksummary sm
                       WHERE     sm.Dim_ActionStateid = 2
                             AND sm.Dim_Plantid = m.Dim_Plantid
                             AND sm.Dim_Partid = m.Dim_Partid)
   GROUP BY m.Dim_Plantid, m.Dim_Partid;
   
   
   
UPDATE fact_stocksummary sm
   SET ct_QtyShipmentTotal =
          ifnull(
             (SELECT SUM(ct_ConfirmedQty - ct_QtyDelivered)
                FROM fact_salesorderdelivery m, dim_date nd
               WHERE m.Dim_DateidDeliveryDate = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_ShipmentTotal =
          ifnull(
             (SELECT SUM(
                        (ct_ConfirmedQty - ct_QtyDelivered) * m.amt_UnitPrice)
                FROM fact_salesorderdelivery m, dim_date nd
               WHERE m.Dim_DateidDeliveryDate = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_ShipmentTotal_GBL =
          ifnull(
             (SELECT SUM(
                        (ct_ConfirmedQty - ct_QtyDelivered) * m.amt_UnitPrice * m.amt_ExchangeRate_GBL)
                FROM fact_salesorderdelivery m, dim_date nd
               WHERE m.Dim_DateidDeliveryDate = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0)
 WHERE sm.Dim_ActionStateid = 2;
   
   
   
   INSERT INTO fact_stocksummary(Dim_Companyid,
                              Dim_ActionStateid,
                              Dim_DateidRecorded,
                              Dim_DateidRecordedEnd,
                              Dim_Plantid,
                              Dim_Partid,
                              Dim_Currencyid,
                              dd_runid,
                              ct_Completed,
                              ct_QtyShipmentTotal,
                              amt_ShipmentTotal,
                              amt_ShipmentTotal_GBL)
   SELECT dc.Dim_Companyid,
          2 Dim_ActionStateid,
          rd.Dim_Dateid Dim_DateidRecorded,
          1 Dim_DateidRecordedEnd,
          Dim_Plantid,
          Dim_Partid,
          Dim_Currencyid,
          VRunID,
          0 ct_Completed,
          sum(ct_ConfirmedQty - ct_QtyDelivered) ct_QtyShipmentTotal,
          sum((ct_ConfirmedQty - ct_QtyDelivered) * amt_UnitPrice)
             amt_ShipmentTotal,
          sum((ct_ConfirmedQty - ct_QtyDelivered) * amt_UnitPrice * amt_ExchangeRate_GBL)
             amt_ShipmentTotal
             FROM fact_salesorderdelivery m
          INNER JOIN dim_company dc
             ON m.Dim_Companyid = dc.Dim_Companyid
          INNER JOIN dim_date dd
             ON m.Dim_DateidDeliveryDate = dd.Dim_Dateid
          INNER JOIN dim_date rd
             ON rd.CompanyCode = dc.CompanyCode AND rd.DateValue = current_date
    WHERE m.Dim_Partid <> 1 AND (ct_ConfirmedQty - ct_QtyDelivered) > 0
          AND NOT EXISTS
                     (SELECT 1
                        FROM fact_stocksummary sm
                       WHERE     sm.Dim_ActionStateid = 2
                             AND sm.Dim_Plantid = m.Dim_Plantid
                             AND sm.Dim_Partid = m.Dim_Partid)
   GROUP BY m.Dim_Plantid, m.Dim_Partid;
   
   
   
   UPDATE fact_stocksummary sm
   SET ct_QtySalesOrder30day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              ct_ConfirmedQty - ct_DeliveredQty
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtySalesOrder60day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              ct_ConfirmedQty - ct_DeliveredQty
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtySalesOrder90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              ct_ConfirmedQty - ct_DeliveredQty
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtySalesOrderOver90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              ct_ConfirmedQty - ct_DeliveredQty
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtySalesOrderPastDue =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN nd.DateValue < now()
                           THEN
                              ct_ConfirmedQty - ct_DeliveredQty
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       ct_QtySalesOrderTotal =
          ifnull(
             (SELECT SUM(ct_ConfirmedQty - ct_DeliveredQty)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrder30day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrder60day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrder90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrderOver90day =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrderPastDue =
          ifnull(
             (SELECT SUM(
                        CASE
                           WHEN nd.DateValue < now()
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrderTotal =
          ifnull(
             (SELECT SUM(
                        (ct_ConfirmedQty - ct_DeliveredQty) * m.amt_UnitPrice)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrder30day_GBL =
          ifnull(
             (SELECT SUM(
                        (CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 0 AND 30
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END )* amt_ExchangeRate_GBL )
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrder60day_GBL =
          ifnull(
             (SELECT SUM(
                        ( CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 31 AND 60
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END) * amt_ExchangeRate_GBL)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrder90day_GBL =
          ifnull(
             (SELECT SUM(
                        ( CASE
                           WHEN datediff(nd.DateValue, now()) BETWEEN 61 AND 90
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END) * amt_ExchangeRate_GBL)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrderOver90day_GBL =
          ifnull(
             (SELECT SUM(
                        (CASE
                           WHEN datediff(nd.DateValue, now()) > 90
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END) * amt_ExchangeRate_GBL)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrderPastDue_GBL =
          ifnull(
             (SELECT SUM(
                        (CASE
                           WHEN nd.DateValue < now()
                           THEN
                              (ct_ConfirmedQty - ct_DeliveredQty)
                              * m.amt_UnitPrice
                           ELSE
                              0
                        END) * amt_ExchangeRate_GBL)
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0),
       amt_SalesOrderTotal_GBL =
          ifnull(
             (SELECT SUM(
                        (ct_ConfirmedQty - ct_DeliveredQty) * m.amt_UnitPrice * m.amt_ExchangeRate_GBL )
                FROM fact_salesorder m, dim_date nd
               WHERE m.Dim_DateidSchedDelivery = nd.Dim_Dateid
                     AND m.Dim_Partid = sm.Dim_Partid),
             0)
 WHERE sm.Dim_ActionStateid = 2;
   
   
   
   INSERT INTO fact_stocksummary(Dim_Companyid,
                              Dim_ActionStateid,
                              Dim_DateidRecorded,
                              Dim_DateidRecordedEnd,
                              Dim_Plantid,
                              Dim_Partid,
                              Dim_Currencyid,
                              dd_runid,
                              ct_Completed,
                              ct_QtySalesOrder30day,
                              ct_QtySalesOrder60day,
                              ct_QtySalesOrder90day,
                              ct_QtySalesOrderOver90day,
                              ct_QtySalesOrderPastDue,
                              ct_QtySalesOrderTotal,
                              amt_SalesOrder30day,
                              amt_SalesOrder60day,
                              amt_SalesOrder90day,
                              amt_SalesOrderOver90day,
                              amt_SalesOrderPastDue,
                              amt_SalesOrderTotal,
                              amt_SalesOrder30day_GBL,
                              amt_SalesOrder60day_GBL,
                              amt_SalesOrder90day_GBL,
                              amt_SalesOrderOver90day_GBL,
                              amt_SalesOrderPastDue_GBL,
                              amt_SalesOrderTotal_GBL)
   SELECT dc.Dim_Companyid,
          2 Dim_ActionStateid,
          rd.Dim_Dateid Dim_DateidRecorded,
          1 Dim_DateidRecordedEnd,
          Dim_Plantid,
          Dim_Partid,
          Dim_Currencyid,
          VRunID,
          0 ct_Completed,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
                THEN
                   (ct_ConfirmedQty - ct_DeliveredQty)
                ELSE
                   0
             END)
             ct_QtySalesOrder30day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
                THEN
                   (ct_ConfirmedQty - ct_DeliveredQty)
                ELSE
                   0
             END)
             ct_QtySalesOrder60day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
                THEN
                   (ct_ConfirmedQty - ct_DeliveredQty)
                ELSE
                   0
             END)
             ct_QtySalesOrder90day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) > 90
                THEN
                   (ct_ConfirmedQty - ct_DeliveredQty)
                ELSE
                   0
             END)
             ct_QtySalesOrderOver90day,
          SUM(
             CASE
                WHEN dd.DateValue < now() THEN (ct_ConfirmedQty - ct_DeliveredQty)
                ELSE 0
             END)
             ct_QtySalesOrderPastDue,
          sum(ct_ConfirmedQty - ct_DeliveredQty) ct_QtySalesOrderTotal,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrder30day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrder60day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrder90day,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) > 90
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrderOver90day,
          SUM(
             CASE
                WHEN dd.DateValue < now()
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrderPastDue,
          ifnull(sum((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice/ct_PriceUnit), 0)
             amt_SalesOrderTotal,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 0 AND 30
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice * amt_ExchangeRate_GBL/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrder30day_GBL,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 31 AND 60
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice * amt_ExchangeRate_GBL/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrder60day_GBL,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) BETWEEN 61 AND 90
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice * amt_ExchangeRate_GBL/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrder90day_GBL,
          SUM(
             CASE
                WHEN datediff(dd.DateValue, now()) > 90
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice* amt_ExchangeRate_GBL/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrderOver90day_GBL,
          SUM(
             CASE
                WHEN dd.DateValue < now()
                THEN
                   ((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice* amt_ExchangeRate_GBL/ct_PriceUnit)
                ELSE
                   0
             END)
             amt_SalesOrderPastDue_GBL,
          ifnull(sum((ct_ConfirmedQty - ct_DeliveredQty) * amt_UnitPrice* amt_ExchangeRate_GBL/ct_PriceUnit), 0)
             amt_SalesOrderTotal_GBL
     FROM fact_salesorder m
          INNER JOIN dim_company dc
             ON m.Dim_Companyid = dc.Dim_Companyid
          INNER JOIN dim_date dd
             ON m.Dim_DateidSchedDelivery = dd.Dim_Dateid
          INNER JOIN dim_date rd
             ON rd.CompanyCode = dc.CompanyCode AND rd.DateValue = current_date
    WHERE m.Dim_Partid <> 1
          AND (ct_ConfirmedQty - ct_DeliveredQty) > 0
          AND NOT EXISTS
                     (SELECT 1
                        FROM fact_stocksummary sm
                       WHERE     sm.Dim_ActionStateid = 2
                             AND sm.Dim_Plantid = m.Dim_Plantid
                             AND sm.Dim_Partid = m.Dim_Partid)
   GROUP BY m.Dim_Plantid, m.Dim_Partid;
   
SET FOREIGN_KEY_CHECKS = 1;

ALTER TABLE fact_stocksummary ENABLE KEYS;

OPTIMIZE TABLE fact_stocksummary;

END;
