DROP PROCEDURE IF EXISTS fusionops_model_schema.bi_populate_salesorder_fillrate;
CREATE PROCEDURE fusionops_model_schema.`bi_populate_salesorder_fillrate`()
BEGIN

  DECLARE done INT DEFAULT 0;
  DECLARE v_Fact_SalesOrderid   INT;
  DECLARE v_dd_SalesDocNo       VARCHAR(50);
  DECLARE v_dd_SalesItemNo      INT;
  DECLARE v_dd_ScheduleNo       INT;
  DECLARE v_dim_doccategoryid   INT;
  DECLARE v_dim_part_id         INT;
  DECLARE v_goods_issue_date    DATE;
  DECLARE v_ct_FillQty          DECIMAL(18,4);
  DECLARE v_ct_OnHandQty        DECIMAL(18,4);
  DECLARE v_TotalQty            DECIMAL(18,4);

  DECLARE cur1 CURSOR FOR
  SELECT Fact_SalesOrderid,
         dd_SalesDocNo,
         dd_SalesItemNo,
         dd_ScheduleNo,
         dim_partid,
         dt.DateValue,
         ct_FillQty,
         ct_OnHandQty,
         ct_ConfirmedQty total_qty
  FROM fact_salesorder f
      INNER JOIN dim_date dt ON dt.dim_dateid = f.Dim_DateidGoodsIssue
      INNER JOIN dim_date adt ON adt.dim_dateid = f.Dim_DateidActualGI
      inner join dim_documentcategory dg on dg.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
      inner join Dim_ScheduleLineCategory slc on f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
  WHERE (dt.DateValue = curdate() OR adt.DateValue = curdate() 
            OR ((dt.DateValue < curdate() OR adt.DateValue < curdate()) AND ct_FillQty = 0 AND ct_OnHandQty = 0))
        AND f.Dim_SalesOrderRejectReasonid = 1 and dg.DocumentCategory <> 'H' 
        AND f.dd_ItemRelForDelv = 'X'
  ORDER BY dt.DateValue DESC, dd_SalesDocNo, dd_SalesItemNo;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  SET v_dim_doccategoryid = (select dg.Dim_DocumentCategoryid from dim_documentcategory dg where BINARY dg.DocumentCategory = 'H');

OPEN cur1;
FETCH cur1 INTO
       v_Fact_SalesOrderid,
       v_dd_SalesDocNo,
       v_dd_SalesItemNo,
       v_dd_ScheduleNo,
       v_dim_part_id,
       v_goods_issue_date,
       v_ct_FillQty,
       v_ct_OnHandQty,
       v_TotalQty;

WHILE done = 0 DO

 IF ( v_ct_FillQty = 0 )
 THEN
    UPDATE fact_salesorder f, dim_salesorderitemstatus s
        SET   ct_FillQty =
              case when s.OverallDeliveryStatus = 'Completely processed'
                  and exists (SELECT 1 FROM fact_salesorderdelivery f_sod
                                              INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                                              INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                                              inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                                        WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                                              AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                                              AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
                                              and sois.GoodsMovementStatus <> 'Not yet processed'
                                              AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date)))
              then 
                f.ct_ConfirmedQty - ifnull((SELECT sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END)
                                            FROM    fact_salesorderdelivery f_sod
                                              INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                                              INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                                              inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                                            WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                                                  AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                                                  AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
                                                  and sois.GoodsMovementStatus <> 'Not yet processed'
                                                  AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue > v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue > v_goods_issue_date))),0)
              else
                ifnull((SELECT sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END)
                        FROM    fact_salesorderdelivery f_sod
                              INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                              INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                              inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                        WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                              AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                              AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
                              and sois.GoodsMovementStatus <> 'Not yet processed'
                              AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))),0)
              end,
              Dim_DateidShipDlvrFill =
              ifnull((SELECT max(f_sod.Dim_DateidDeliveryDate)
                      FROM    fact_salesorderdelivery f_sod
                            INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                            INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                            inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                      WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                            AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                            AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
                            and sois.GoodsMovementStatus <> 'Not yet processed'
                            AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))),1),
              Dim_DateidActualGIFill =
              ifnull((SELECT max(f_sod.Dim_DateidActualGoodsIssue)
                      FROM    fact_salesorderdelivery f_sod
                            INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                            INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                            inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                      WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                            AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                            AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
                            and sois.GoodsMovementStatus <> 'Not yet processed'
                            AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))),1),
              Dim_DateidLoadingFill =
              ifnull((SELECT max(f_sod.Dim_DateidLoadingDate)
                      FROM    fact_salesorderdelivery f_sod
                            INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                            INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                            inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                      WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                            AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                            AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
                            and sois.GoodsMovementStatus <> 'Not yet processed'
                            AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))),1)
    WHERE f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
        AND Fact_SalesOrderid = v_Fact_SalesOrderid;
  END IF;

  IF (v_ct_OnHandQty = 0 )
  THEN
    IF (v_goods_issue_date < curdate())
    THEN

      UPDATE fact_salesorder
      SET   ct_OnHandQty = ct_FillQty
      WHERE Fact_SalesOrderid = v_Fact_SalesOrderid;

    ELSE

      SET v_ct_OnHandQty = ifnull((SELECT sum(f_inv_sq_001.ct_StockQty + f_inv_sq_001.ct_BlockedStock 
                                          + f_inv_sq_001.ct_StockInQInsp + f_inv_sq_001.ct_StockInTransfer
                                          + f_inv_sq_001.ct_TotalRestrictedStock)
                                FROM fact_inventoryaging f_inv_sq_001
                                WHERE f_inv_sq_001.dim_Partid = v_dim_part_id),0)
                        - ifnull((SELECT sum(CASE WHEN (f_so_sq_001.ct_ConfirmedQty  - f_so_sq_001.ct_DeliveredQty) < 0 THEN 0
                                                  ELSE (f_so_sq_001.ct_ConfirmedQty - f_so_sq_001.ct_DeliveredQty)
                                              END)
                                  FROM fact_salesorder f_so_sq_001 INNER JOIN dim_date d_GoodsIssue_sq_001
                                        ON f_so_sq_001.Dim_DateidGoodsIssue = d_GoodsIssue_sq_001.Dim_Dateid
                                  WHERE f_so_sq_001.Dim_Partid = v_dim_part_id 
                                        AND f_so_sq_001.Dim_SalesOrderRejectReasonid = 1 
                                        and f_so_sq_001.Dim_DocumentCategoryid <> v_dim_doccategoryid 
                                        and f_so_sq_001.dd_ItemRelForDelv = 'X'
                                        AND (d_GoodsIssue_sq_001.DateValue < v_goods_issue_date
                                            OR (d_GoodsIssue_sq_001.DateValue = v_goods_issue_date
                                                AND (f_so_sq_001.dd_SalesDocNo < v_dd_SalesDocNo
                                                      OR (f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                                          AND (f_so_sq_001.dd_SalesItemNo < v_dd_SalesItemNo
                                                              OR (f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                                                  AND f_so_sq_001.dd_ScheduleNo < v_dd_ScheduleNo))))))),0);

      IF(v_ct_OnHandQty < 0) THEN
        SET v_ct_OnHandQty = 0;
      ELSEIF(v_ct_OnHandQty > v_TotalQty) THEN
        SET v_ct_OnHandQty = v_TotalQty;
      END IF;

      UPDATE fact_salesorder
        SET   ct_OnHandQty = v_ct_OnHandQty
        WHERE Fact_SalesOrderid = v_Fact_SalesOrderid;

    END IF;

  END IF;

FETCH cur1 INTO
       v_Fact_SalesOrderid,
       v_dd_SalesDocNo,
       v_dd_SalesItemNo,
       v_dd_ScheduleNo,
       v_dim_part_id,
       v_goods_issue_date,
       v_ct_FillQty,
       v_ct_OnHandQty,
       v_TotalQty;

END WHILE;
CLOSE cur1;

END;
