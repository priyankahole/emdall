DROP PROCEDURE IF EXISTS fusionops_model_schema.bi_populate_salesorder_coveredqty;
CREATE PROCEDURE fusionops_model_schema.`bi_populate_salesorder_coveredqty`()
BEGIN

  DECLARE done INT DEFAULT 0;
  DECLARE v_Fact_SalesOrderid   INT;
  DECLARE v_Pre_SalesOrderid    INT;
  DECLARE v_dd_SalesDocNo       VARCHAR(50);
  DECLARE v_dd_SalesItemNo      INT;
  DECLARE v_dd_ScheduleNo       INT;
  DECLARE v_dim_doccategoryid   INT;
  DECLARE v_dim_schcategoryid   INT;
  DECLARE v_dim_part_id         INT;
  DECLARE v_Pre_part_id         INT;
  DECLARE v_GIDate              DATE;
  DECLARE v_CoveredQty          DECIMAL(18,4);
  DECLARE v_Pre_CoveredQty      DECIMAL(18,4);
  DECLARE v_OpenQty             DECIMAL(18,4);
  DECLARE v_ConfQty             DECIMAL(18,4);
  DECLARE v_OrdQty              DECIMAL(18,4);
  DECLARE v_DlvrQty             DECIMAL(18,4);
  DECLARE v_DlvrQtyOrd          DECIMAL(18,4);
  DECLARE v_nxt_ConfQty         DECIMAL(18,4);
  DECLARE v_nxt_OrdQty          DECIMAL(18,4);
  DECLARE v_ItmRelDlvr          VARCHAR(10);
  DECLARE v_CmlRcvdQty          DECIMAL(18,4);

  DECLARE cur1 CURSOR FOR
  SELECT Fact_SalesOrderid,
         dd_SalesDocNo,
         dd_SalesItemNo,
         dd_ScheduleNo,
         mad.DateValue GIDate,
         dim_partid,
         f.ct_ConfirmedQty,
         f.dd_ItemRelForDelv,
         f.ct_DeliveredQty,
         f.ct_ScheduleQtySalesUnit
  FROM fact_salesorder f INNER JOIN Dim_Date mad ON f.Dim_DateidGoodsIssue = mad.Dim_Dateid
      inner join dim_documentcategory dg on dg.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
      inner join Dim_ScheduleLineCategory slc on f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
  WHERE f.ct_ConfirmedQty > 0 
        AND f.Dim_SalesOrderRejectReasonid = 1
        AND dg.DocumentCategory <> 'H'
        AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
  ORDER BY dim_partid, mad.DateValue, dd_SalesDocNo, dd_SalesItemNo, dd_ScheduleNo;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
  
  UPDATE fact_salesorder f
  SET ct_OnHandCoveredQty = 0;
  
  UPDATE fact_salesorder f, Dim_ScheduleLineCategory slc
  SET ct_DeliveredQty = 0
  WHERE f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
        AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
        AND f.dd_ItemRelForDelv = 'Not Set';
  
  SET v_dim_part_id = 0;

  OPEN cur1;
  FETCH cur1 INTO
         v_Fact_SalesOrderid,
         v_dd_SalesDocNo,
         v_dd_SalesItemNo,
         v_dd_ScheduleNo,
         v_GIDate,
         v_dim_part_id,
         v_ConfQty,
         v_ItmRelDlvr,
         v_DlvrQty,
         v_OrdQty;
  WHILE done = 0 DO

    IF v_Pre_part_id > 0 and v_Pre_part_id <> v_dim_part_id THEN
      UPDATE fact_salesorder
      SET ct_OnHandCoveredQty = v_Pre_CoveredQty
      WHERE Fact_SalesOrderid = v_Pre_SalesOrderid;
    END IF;
    
    if (v_ItmRelDlvr = 'Not Set') then
      SET v_DlvrQty = ifnull((SELECT sum(f_dlvr.ct_QtyDelivered)
                              FROM fact_salesorderdelivery f_dlvr inner join dim_salesorderitemstatus sois on f_dlvr.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                              WHERE f_dlvr.dd_SalesDocNo = v_dd_SalesDocNo 
                                    and f_dlvr.dd_SalesItemNo = v_dd_SalesItemNo
                                    and sois.GoodsMovementStatus <> 'Not yet processed'),0)
                          - ifnull((SELECT sum(f_so_sq_001.ct_DeliveredQty)
                                    FROM fact_salesorder f_so_sq_001 
                                          INNER JOIN dim_date d_GI_sq_001 
                                                  ON f_so_sq_001.Dim_DateidGoodsIssue = d_GI_sq_001.Dim_Dateid
                                          inner join Dim_ScheduleLineCategory slc 
                                                  on f_so_sq_001.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
                                    WHERE f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                          AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (d_GI_sq_001.DateValue < v_GIDate
                                              OR (d_GI_sq_001.DateValue = v_GIDate
                                                  AND f_so_sq_001.dd_ScheduleNo < v_dd_ScheduleNo))),0);
      
      set v_DlvrQtyOrd = ifnull((SELECT sum(f_dlvr.ct_QtyDelivered)
                              FROM fact_salesorderdelivery f_dlvr inner join dim_salesorderitemstatus sois on f_dlvr.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                              WHERE f_dlvr.dd_SalesDocNo = v_dd_SalesDocNo 
                                    and f_dlvr.dd_SalesItemNo = v_dd_SalesItemNo
                                    and sois.GoodsMovementStatus <> 'Not yet processed'),0)
                          - ifnull((SELECT sum(f_so_sq_001.ct_ShippedAgnstOrderQty)
                                    FROM fact_salesorder f_so_sq_001 
                                          INNER JOIN dim_date d_GI_sq_001 
                                                  ON f_so_sq_001.Dim_DateidGoodsIssue = d_GI_sq_001.Dim_Dateid
                                          inner join Dim_ScheduleLineCategory slc 
                                                  on f_so_sq_001.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
                                    WHERE f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                          AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (d_GI_sq_001.DateValue < v_GIDate
                                              OR (d_GI_sq_001.DateValue = v_GIDate
                                                  AND f_so_sq_001.dd_ScheduleNo < v_dd_ScheduleNo))),0);
      
      SET v_nxt_ConfQty = ifnull((SELECT sum(f_so_sq_001.ct_ConfirmedQty)
                                    FROM fact_salesorder f_so_sq_001 
                                          INNER JOIN dim_date d_GI_sq_001 
                                                  ON f_so_sq_001.Dim_DateidGoodsIssue = d_GI_sq_001.Dim_Dateid
                                          inner join Dim_ScheduleLineCategory slc 
                                                  on f_so_sq_001.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
                                    WHERE f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                          AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (d_GI_sq_001.DateValue > v_GIDate
                                              OR (d_GI_sq_001.DateValue = v_GIDate
                                                  AND f_so_sq_001.dd_ScheduleNo > v_dd_ScheduleNo))),0);
                                                  
      set v_nxt_OrdQty  = ifnull((SELECT sum(f_so_sq_001.ct_ScheduleQtySalesUnit)
                                    FROM fact_salesorder f_so_sq_001 
                                          INNER JOIN dim_date d_GI_sq_001 
                                                  ON f_so_sq_001.Dim_DateidGoodsIssue = d_GI_sq_001.Dim_Dateid
                                          inner join Dim_ScheduleLineCategory slc 
                                                  on f_so_sq_001.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
                                    WHERE f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                          AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (d_GI_sq_001.DateValue > v_GIDate
                                              OR (d_GI_sq_001.DateValue = v_GIDate
                                                  AND f_so_sq_001.dd_ScheduleNo > v_dd_ScheduleNo))),0);

      set v_CmlRcvdQty = ifnull((select max(VBLB_ABEFZ)
                                from VBLB where VBLB_VBELN = v_dd_SalesDocNo and VBLB_POSNR = v_dd_SalesItemNo),0)
                          - ifnull((SELECT sum(f_so_sq_001.ct_CmlQtyReceived)
                                    FROM fact_salesorder f_so_sq_001 
                                          INNER JOIN dim_date d_GI_sq_001 
                                                  ON f_so_sq_001.Dim_DateidGoodsIssue = d_GI_sq_001.Dim_Dateid
                                          inner join Dim_ScheduleLineCategory slc 
                                                  on f_so_sq_001.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
                                    WHERE f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                          AND f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                          AND slc.ScheduleLineCategory in ('BP','BV','CP','CV','E1')
                                          AND f_so_sq_001.dd_ItemRelForDelv = 'Not Set'
                                          AND (d_GI_sq_001.DateValue < v_GIDate
                                              OR (d_GI_sq_001.DateValue = v_GIDate
                                                  AND f_so_sq_001.dd_ScheduleNo < v_dd_ScheduleNo))),0); 
      
      if (v_nxt_ConfQty > 0 and v_DlvrQty > v_ConfQty) then
        SET v_DlvrQty = v_ConfQty;
      end if;
      
      if (v_nxt_OrdQty > 0 and v_DlvrQtyOrd > v_OrdQty) then
        SET v_DlvrQtyOrd = v_OrdQty;
      end if;
      
      if (v_nxt_OrdQty > 0 and v_CmlRcvdQty > v_OrdQty) then
        SET v_CmlRcvdQty = v_OrdQty;
      end if;
    end if;
    
    SET v_OpenQty = v_ConfQty - v_DlvrQty;
    if (v_OpenQty < 0) then
      SET v_OpenQty = 0;
    end if;
    
    SET v_CoveredQty = ifnull((SELECT sum(f_inv_sq_001.ct_StockQty + f_inv_sq_001.ct_BlockedStock 
                                          + f_inv_sq_001.ct_StockInQInsp + f_inv_sq_001.ct_StockInTransfer
                                          + f_inv_sq_001.ct_TotalRestrictedStock)
                                FROM fact_inventoryaging f_inv_sq_001
                                WHERE f_inv_sq_001.dim_Partid = v_dim_part_id),0)
                        - ifnull((SELECT sum(f_so_sq_001.ct_OnHandCoveredQty)
                                  FROM fact_salesorder f_so_sq_001 INNER JOIN dim_date d_GI_sq_001
                                        ON f_so_sq_001.Dim_DateidGoodsIssue = d_GI_sq_001.Dim_Dateid
                                  WHERE f_so_sq_001.Dim_Partid = v_dim_part_id
                                        AND (d_GI_sq_001.DateValue < v_GIDate
                                            OR (d_GI_sq_001.DateValue = v_GIDate
                                                AND (f_so_sq_001.dd_SalesDocNo < v_dd_SalesDocNo
                                                      OR (f_so_sq_001.dd_SalesDocNo = v_dd_SalesDocNo
                                                          AND (f_so_sq_001.dd_SalesItemNo < v_dd_SalesItemNo
                                                              OR (f_so_sq_001.dd_SalesItemNo = v_dd_SalesItemNo
                                                                  AND f_so_sq_001.dd_ScheduleNo < v_dd_ScheduleNo))))))),0);
                        

    SET v_Pre_CoveredQty = v_CoveredQty;
    SET v_Pre_SalesOrderid = v_Fact_SalesOrderid;
    SET v_Pre_part_id = v_dim_part_id;
    
    IF(v_CoveredQty < 0) THEN
      SET v_CoveredQty = 0;
    ELSEIF(v_CoveredQty > v_OpenQty) THEN
      SET v_CoveredQty = v_OpenQty;
    END IF;

    UPDATE fact_salesorder
    SET ct_OnHandCoveredQty = v_CoveredQty,
        ct_DeliveredQty = v_DlvrQty,
        ct_ShippedAgnstOrderQty = v_DlvrQtyOrd,
        ct_CmlQtyReceived = v_CmlRcvdQty
    WHERE Fact_SalesOrderid = v_Fact_SalesOrderid;
    
  FETCH cur1 INTO
         v_Fact_SalesOrderid,
         v_dd_SalesDocNo,
         v_dd_SalesItemNo,
         v_dd_ScheduleNo,
         v_GIDate,
         v_dim_part_id,
         v_ConfQty,
         v_ItmRelDlvr,
         v_DlvrQty,
         v_OrdQty;
  END WHILE;
  CLOSE cur1;

END;
