DROP PROCEDURE IF EXISTS fusionops_model_schema.bi_process_trends_inventoryhistory_fact;
CREATE PROCEDURE fusionops_model_schema.`bi_process_trends_inventoryhistory_fact`()
BEGIN

DELETE FROM fact_inventoryhistory
 WHERE SnapshotDate = curDate();

INSERT INTO fact_inventoryhistory(amt_BlockedStockAmt,
                                  amt_BlockedStockAmt_GBL,
                                  amt_CommericalPrice1,
                                  amt_CurPlannedPrice,
                                  amt_MovingAvgPrice,
                                  amt_MtlDlvrCost,
                                  amt_OtherCost,
                                  amt_OverheadCost,
                                  amt_PlannedPrice1,
                                  amt_PreviousPrice,
                                  amt_PrevPlannedPrice,
                                  amt_StdUnitPrice,
                                  amt_StdUnitPrice_GBL,
                                  amt_StockInQInspAmt,
                                  amt_StockInQInspAmt_GBL,
                                  amt_StockInTransferAmt,
                                  amt_StockInTransferAmt_GBL,
                                  amt_StockInTransitAmt,
                                  amt_StockInTransitAmt_GBL,
                                  amt_StockValueAmt,
                                  amt_StockValueAmt_GBL,
                                  amt_UnrestrictedConsgnStockAmt,
                                  amt_UnrestrictedConsgnStockAmt_GBL,
                                  amt_WIPBalance,
                                  ct_BlockedConsgnStock,
                                  ct_BlockedStock,
                                  ct_BlockedStockReturns,
                                  ct_ConsgnStockInQInsp,
                                  ct_LastReceivedQty,
                                  ct_RestrictedConsgnStock,
                                  ct_StockInQInsp,
                                  ct_StockInTransfer,
                                  ct_StockInTransit,
                                  ct_StockQty,
                                  ct_TotalRestrictedStock,
                                  ct_UnrestrictedConsgnStock,
                                  ct_WIPAging,
                                  dd_BatchNo,
                                  dd_DocumentItemNo,
                                  dd_DocumentNo,
                                  dd_MovementType,
                                  dd_ValuationType,
                                  dim_Companyid,
                                  Dim_ConsumptionTypeid,
                                  dim_costcenterid,
                                  dim_Currencyid,
                                  Dim_DateIdLastChangedPrice,
                                  Dim_DateIdPlannedPrice2,
                                  Dim_DocumentStatusid,
                                  Dim_DocumentTypeid,
                                  Dim_IncoTermid,
                                  Dim_ItemCategoryid,
                                  Dim_ItemStatusid,
                                  dim_LastReceivedDateid,
                                  Dim_MovementIndicatorid,
                                  dim_Partid,
                                  dim_Plantid,
                                  dim_producthierarchyid,
                                  Dim_PurchaseGroupid,
                                  Dim_PurchaseMiscid,
                                  Dim_PurchaseOrgid,
                                  dim_specialstockid,
                                  dim_stockcategoryid,
                                  dim_StockTypeid,
                                  dim_StorageLocationid,
                                  dim_StorageLocEntryDateid,
                                  Dim_SupplyingPlantId,
                                  Dim_Termid,
                                  Dim_UnitOfMeasureid,
                                  dim_Vendorid,
                                  SnapshotDate,
                                  Dim_DateidSnapshot)
   SELECT amt_BlockedStockAmt,
          amt_BlockedStockAmt_GBL,
          amt_CommericalPrice1,
          amt_CurPlannedPrice,
          amt_MovingAvgPrice,
          amt_MtlDlvrCost,
          amt_OtherCost,
          amt_OverheadCost,
          amt_PlannedPrice1,
          amt_PreviousPrice,
          amt_PrevPlannedPrice,
          amt_StdUnitPrice,
          amt_StdUnitPrice_GBL,
          amt_StockInQInspAmt,
          amt_StockInQInspAmt_GBL,
          amt_StockInTransferAmt,
          amt_StockInTransferAmt_GBL,
          amt_StockInTransitAmt,
          amt_StockInTransitAmt_GBL,
          amt_StockValueAmt,
          amt_StockValueAmt_GBL,
          amt_UnrestrictedConsgnStockAmt,
          amt_UnrestrictedConsgnStockAmt_GBL,
          amt_WIPBalance,
          ct_BlockedConsgnStock,
          ct_BlockedStock,
          ct_BlockedStockReturns,
          ct_ConsgnStockInQInsp,
          ct_LastReceivedQty,
          ct_RestrictedConsgnStock,
          ct_StockInQInsp,
          ct_StockInTransfer,
          ct_StockInTransit,
          ct_StockQty,
          ct_TotalRestrictedStock,
          ct_UnrestrictedConsgnStock,
          ct_WIPAging,
          dd_BatchNo,
          dd_DocumentItemNo,
          dd_DocumentNo,
          dd_MovementType,
          dd_ValuationType,
          iag.dim_Companyid,
          Dim_ConsumptionTypeid,
          dim_costcenterid,
          dim_Currencyid,
          Dim_DateIdLastChangedPrice,
          Dim_DateIdPlannedPrice2,
          Dim_DocumentStatusid,
          Dim_DocumentTypeid,
          Dim_IncoTermid,
          Dim_ItemCategoryid,
          Dim_ItemStatusid,
          dim_LastReceivedDateid,
          Dim_MovementIndicatorid,
          dim_Partid,
          dim_Plantid,
          dim_producthierarchyid,
          Dim_PurchaseGroupid,
          Dim_PurchaseMiscid,
          Dim_PurchaseOrgid,
          dim_specialstockid,
          dim_stockcategoryid,
          dim_StockTypeid,
          dim_StorageLocationid,
          dim_StorageLocEntryDateid,
          Dim_SupplyingPlantId,
          Dim_Termid,
          Dim_UnitOfMeasureid,
          dim_Vendorid,
          curDate() SnapshotDate,
          ifnull(
             (SELECT dim_Dateid
                FROM dim_Date dt
               WHERE dt.DateValue = curDate()
                     AND dt.companycode = dc.companycode),
             1)
     FROM    facT_inventoryaging iag
          INNER JOIN
             dim_Company dc
          ON iag.dim_companyid = dc.dim_Companyid;

TRUNCATE TABLE fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp
(dim_Partid,dim_Plantid,dim_StorageLocationid,dim_stockcategoryid,SnapshotDate,amt_BlockedStockAmt,amt_StockInQInspAmt,amt_StockInTransferAmt,
amt_StockValueAmt,ct_BlockedStock,ct_StockInQInsp,ct_StockInTransfer,ct_StockInTransit,ct_StockQty,
ct_TotalRestrictedStock)
select dim_Partid,dim_Plantid,dim_StorageLocationid,dim_stockcategoryid,SnapshotDate,SUM(amt_BlockedStockAmt),SUM(amt_StockInQInspAmt),SUM(amt_StockInTransferAmt),
SUM(amt_StockValueAmt),SUM(ct_BlockedStock),SUM(ct_StockInQInsp),SUM(ct_StockInTransfer),SUM(ct_StockInTransit),SUM(ct_StockQty),
SUM(ct_TotalRestrictedStock) FROM fact_inventoryhistory 
WHERE dim_stockcategoryid in (2,3) 
and SnapshotDate in ( curdate(), DATE_SUB(curdate(), INTERVAL 1 DAY), DATE_SUB(curdate(), INTERVAL 1 WEEK),DATE_SUB(curdate(), INTERVAL 1 MONTH),DATE_SUB(curdate(), INTERVAL 1 QUARTER))
group by dim_Partid,dim_Plantid,dim_StorageLocationid,dim_stockcategoryid,SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
       AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 DAY) = ih2.SnapshotDate;

 UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
 AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 WEEK) = ih2.SnapshotDate;


 UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
 AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 MONTH) = ih2.SnapshotDate;
       

UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
       AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 QUARTER) = ih2.SnapshotDate;
       
UPDATE    fact_inventoryhistory ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
          AND ih1.SnapshotDate = ih2.Snapshotdate
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
   WHERE ih1.dim_stockcategoryid in (2,3)
   AND ih1.SnapshotDate = curdate();
       
       
TRUNCATE TABLE fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp
(dim_Partid,dim_Plantid,dim_Vendorid,dim_stockcategoryid,SnapshotDate,amt_BlockedStockAmt,amt_StockInQInspAmt,amt_StockInTransferAmt,
amt_StockValueAmt,ct_BlockedStock,ct_StockInQInsp,ct_StockInTransfer,ct_StockInTransit,ct_StockQty,
ct_TotalRestrictedStock)
select dim_Partid,dim_Plantid,dim_Vendorid,dim_stockcategoryid,SnapshotDate,SUM(amt_BlockedStockAmt),SUM(amt_StockInQInspAmt),SUM(amt_StockInTransferAmt),
SUM(amt_StockValueAmt),SUM(ct_BlockedStock),SUM(ct_StockInQInsp),SUM(ct_StockInTransfer),SUM(ct_StockInTransit),SUM(ct_StockQty),
SUM(ct_TotalRestrictedStock) FROM fact_inventoryhistory 
WHERE dim_stockcategoryid = 4
and SnapshotDate in ( curdate(), DATE_SUB(curdate(), INTERVAL 1 DAY), DATE_SUB(curdate(), INTERVAL 1 WEEK),DATE_SUB(curdate(), INTERVAL 1 MONTH),DATE_SUB(curdate(), INTERVAL 1 QUARTER))
group by dim_Partid,dim_Plantid,dim_Vendorid,dim_stockcategoryid,SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Vendorid = ih2.dim_Vendorid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
       AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 DAY) = ih2.SnapshotDate;

 UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Vendorid = ih2.dim_Vendorid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
 AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 WEEK) = ih2.SnapshotDate;


 UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Vendorid = ih2.dim_Vendorid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
 AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 MONTH) = ih2.SnapshotDate;
       

UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Vendorid = ih2.dim_Vendorid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
       AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 QUARTER) = ih2.SnapshotDate;
       
UPDATE    fact_inventoryhistory ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_Vendorid = ih2.dim_Vendorid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
          AND ih1.SnapshotDate = ih2.Snapshotdate
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
   WHERE ih1.dim_stockcategoryid = 4
   AND ih1.SnapshotDate = curdate();       
       
TRUNCATE TABLE fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp
(dim_Partid,dim_Plantid,dim_stockcategoryid,SnapshotDate,amt_BlockedStockAmt,amt_StockInQInspAmt,amt_StockInTransferAmt,
amt_StockValueAmt,ct_BlockedStock,ct_StockInQInsp,ct_StockInTransfer,ct_StockInTransit,ct_StockQty,
ct_TotalRestrictedStock)
select dim_Partid,dim_Plantid,dim_stockcategoryid,SnapshotDate,SUM(amt_BlockedStockAmt),SUM(amt_StockInQInspAmt),SUM(amt_StockInTransferAmt),
SUM(amt_StockValueAmt),SUM(ct_BlockedStock),SUM(ct_StockInQInsp),SUM(ct_StockInTransfer),SUM(ct_StockInTransit),SUM(ct_StockQty),
SUM(ct_TotalRestrictedStock) FROM fact_inventoryhistory 
WHERE dim_stockcategoryid = 5
and SnapshotDate in ( curdate(), DATE_SUB(curdate(), INTERVAL 1 DAY), DATE_SUB(curdate(), INTERVAL 1 WEEK),DATE_SUB(curdate(), INTERVAL 1 MONTH),DATE_SUB(curdate(), INTERVAL 1 QUARTER))
group by dim_Partid,dim_Plantid,dim_stockcategoryid,SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
       AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 DAY) = ih2.SnapshotDate;

 UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
 AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 WEEK) = ih2.SnapshotDate;


 UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
 AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 MONTH) = ih2.SnapshotDate;
       

UPDATE    fact_inventoryhistory_tmp ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
   SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
 WHERE ih1.SnapshotDate = curdate()
       AND DATE_SUB(ih1.SnapshotDate, INTERVAL 1 QUARTER) = ih2.SnapshotDate;
       
UPDATE    fact_inventoryhistory ih1
       INNER JOIN
          fact_inventoryhistory_tmp ih2
       ON     ih1.dim_partid = ih2.dim_partid
          AND ih1.dim_plantid = ih2.dim_plantid
          AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
          AND ih1.SnapshotDate = ih2.Snapshotdate
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
   WHERE ih1.dim_stockcategoryid = 5
   AND ih1.SnapshotDate = curdate();       
       
TRUNCATE TABLE fact_inventoryhistory_tmp;
       
END;
