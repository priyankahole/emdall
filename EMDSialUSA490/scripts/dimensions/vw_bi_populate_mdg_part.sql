
TRUNCATE TABLE MDG_MARA;
INSERT INTO MDG_MARA(
MATNR,
YYD_GLMTY,
YYD_YSBU,
YYD_ABCCL,
BISMT,
MEINS,
MHDHB,
MHDRZ,
MSTAE,
MTART,
MTPOS_MARA,
PRDHA,
XCHPF,
YYD_APORL,
YYD_GLOPH,
YYD_SCPPI,
YYD_SCPST,
YYD_COGSI,
YYD_COMCO,
YYD_PPUNI,
MARA_NTGEW,
MARA_GEWEI,
YYD_YGART,
YYD_YKZLP,
MARA_YYD_AIDOS,
MARA_YYD_AIUNI,
MARA_YYD_RIMIB,
MARA_YYD_IMCOD
)
SELECT MATNR,
       YYD_GLMTY,
       YYD_YSBU,
       YYD_ABCCL,
       BISMT,
       MEINS,
       MHDHB,
       MHDRZ,
       MSTAE,
       MTART,
       MTPOS_MARA,
       PRDHA,
       XCHPF,
       YYD_APORL,
       YYD_GLOPH,
       YYD_SCPPI,
       YYD_SCPST,
       YYD_COGSI,
       YYD_COMCO,
       YYD_PPUNI,
       MARA_NTGEW,
       MARA_GEWEI,
	   YYD_YGART,
	   YYD_YKZLP,
	   MARA_YYD_AIDOS,
           MARA_YYD_AIUNI,
      MARA_YYD_RIMIB,
      MARA_YYD_IMCOD
FROM EMDTempoCC4.MDG_MARA;

TRUNCATE TABLE MDG_YYD_V_MAPPING;
INSERT INTO MDG_YYD_V_MAPPING(
MANDT,
USMDKMMMATERIAL,
USMDTMMMATERIAL,
MD_MMMATERIAL
)
SELECT MANDT,
       USMDKMMMATERIAL,
       USMDTMMMATERIAL,
       MD_MMMATERIAL
FROM EMDTempoCC4.MDG_YYD_V_MAPPING;

TRUNCATE TABLE MDG_T9D519T;
INSERT INTO MDG_T9D519T(
YYD_YORGA,
SPRAS,
LTEXT
)
SELECT YYD_YORGA,
       SPRAS,
       LTEXT
FROM EMDTempoCC4.MDG_T9D519T;

TRUNCATE TABLE MDG_T134T;
INSERT INTO MDG_T134T(
T134T_MANDT,
T134T_SPRAS,
T134T_MTART,
T134T_MTBEZ
)
SELECT T134T_MANDT,
       T134T_SPRAS,
       T134T_MTART,
       T134T_MTBEZ
FROM EMDTempoCC4.MDG_T134T;

TRUNCATE TABLE MDG_T9D241T;
INSERT INTO MDG_T9D241T(
YYD_GLMTY,
YYD_TEXT
)
SELECT YYD_GLMTY,
       YYD_TEXT
FROM EMDTempoCC4.MDG_T9D241T;

TRUNCATE TABLE MDG_MAKT;
INSERT INTO MDG_MAKT(
MAKT_MATNR,
MAKT_MAKTX
)
SELECT MAKT_MATNR,
       MAKT_MAKTX
FROM EMDTempoCC4.MDG_MAKT;

TRUNCATE TABLE MDG_YYD_V_YORG;
INSERT INTO MDG_YYD_V_YORG(
MD_MMYYD_ORGA,
MD_MMYYD_YORGA,
MANDT,
USMDKMMMATERIAL,
USMD_ACTIVE,
USMD_O_YORG
)
SELECT MD_MMYYD_ORGA,
       MD_MMYYD_YORGA,
       MANDT,
       USMDKMMMATERIAL,
       USMD_ACTIVE,
       USMD_O_YORG
FROM EMDTempoCC4.MDG_YYD_V_YORG;

TRUNCATE TABLE MDG_T9DRIMIBT;
INSERT INTO MDG_T9DRIMIBT(
T9DRIMIBT_YYD_RIMIB ,
T9DRIMIBT_LTEXT
)
SELECT T9DRIMIBT_YYD_RIMIB,
       T9DRIMIBT_LTEXT
FROM EMDTempoCC4.MDG_T9DRIMIBT;

TRUNCATE TABLE MDG_T9D343T;
INSERT INTO MDG_T9D343T(
MDG_T9D343T_YYD_IMCOD ,
MDG_T9D343T_YYD_TEXT
)
SELECT MDG_T9D343T_YYD_IMCOD,
       MDG_T9D343T_YYD_TEXT
FROM EMDTempoCC4.MDG_T9D343T;


drop table if exists tmp_part;
create table tmp_part as
select distinct
PartNumber  as partnumber,
PartDescription  as PARTDESCRIPTION,
PartType as materialtype,
PartTypeDescription as materialtypedescr,
SIGMA_PRIMARY_MANUF as primary_production_location,
SIGMA_PRIMARY_MANUF as primary_production_location_name,
Laboratory,
productgroupsbu,
row_number() over (partition by PartNumber  order by '' ) rno
from dim_part
where PartNumber <>  'Not Set';


DELETE FROM number_fountain m
WHERE m.table_name = 'dim_mdg_part';

INSERT INTO number_fountain
SELECT 'dim_mdg_part',
       ifnull(MAX(d.dim_mdg_partid ), ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_mdg_part d
WHERE d.dim_mdg_partid <> 1;


insert into dim_mdg_part
(
dim_mdg_partid,
partnumber,
PARTDESCRIPTION,
materialtype,
materialtypedescr,
primary_production_location,
primary_production_location_name,
mattypeforglbrep,
mattypeforglbrepdescr,
glbproductgroup
)
SELECT
 (SELECT ifnull(m.max_id, 1)
        FROM number_fountain m
        WHERE m.table_name = 'dim_mdg_part') + ROW_NUMBER() OVER(ORDER BY '') as dim_mdg_partid,
partnumber,
PARTDESCRIPTION,
materialtype,
materialtypedescr,
primary_production_location,
primary_production_location_name,
materialtype as mattypeforglbrep,
materialtypedescr as mattypeforglbrepdescr,
productgroupsbu as glbproductgroup
FROM tmp_part m
WHERE rno = 1 AND NOT EXISTS (SELECT 'x'
                  FROM dim_mdg_part dmp
                  WHERE dmp.partnumber = m.partnumber
                     AND milliporeflag ='Not Set');


UPDATE  dim_mdg_part m
SET primary_production_location = Primarymanufacturinglocationcode,
primary_production_location_name = ClusterPrimarymanufacturingsite
FROM dim_mdg_part m, tmp_part t, EMDSIALEMEA8DF.mapping_clusterprimary cl
WHERE
m.partnumber = t.PartNumber
AND cl.LabOfficetext = t.Laboratory
AND rno = 1
AND milliporeflag ='Not Set';

update dim_mdg_part
set IndicatorSalesCatalogue = ifnull(YYD_YKZLP,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(IndicatorSalesCatalogue,'a') <> ifnull(YYD_YKZLP,'Not Set');

update dim_mdg_part dmp
set dmp.IndicatorSalesCatalogueDesc = ifnull(cd.description,'Not Set')
from dim_mdg_part dmp,  catalogue_desc cd
where dmp.IndicatorSalesCatalogue = cd.code
	and ifnull(IndicatorSalesCatalogueDesc,'a') <> ifnull(cd.description,'Not Set');
UPDATE  dim_mdg_part m
SET m.mattypeforglbrep = t.materialtype,
m.mattypeforglbrepdescr = t.materialtypedescr,
m.glbproductgroup = t.productgroupsbu,
m.PARTDESCRIPTION = t.PARTDESCRIPTION,
m.materialtype  =   t.materialtype,
m.materialtypedescr = t.materialtypedescr
FROM dim_mdg_part m, tmp_part t
WHERE  m.partnumber = t.PartNumber
    and rno = 1
    and milliporeflag ='Not Set';

/* 21 March 2017 Cornelia add PRM and PRP */
drop table if exists tmp_aditionam_mdm_atributes;
create table tmp_aditionam_mdm_atributes
as
select a.md_mmmaterial,a.usmdkmmmaterial,b.usmd_active,b.usmd_o_yorg,b.md_mmyyd_orga,b.md_mmyyd_yorga
from MDG_YYD_V_MAPPING a
	inner join MDG_YYD_V_YORG b on a.usmdkmmmaterial = b.usmdkmmmaterial and usmd_active = 1 and ifnull(usmd_o_yorg,'Y') <> 'X';

update dim_mdg_part d
set d.PRM_Product_manager_code = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PRM'
	and ifnull(d.PRM_Product_manager_code,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.PRM_Product_manager_desc = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.PRM_Product_manager_code = t.YYD_YORGA and d.PRM_Product_manager_desc <> ifnull(t.LTEXT,'Not Set');


update dim_mdg_part d
set d.PRP_Price_responsible_code = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PRP'
	and ifnull(d.PRP_Price_responsible_code,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.PRP_Price_responsible_desc = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.PRP_Price_responsible_code = t.YYD_YORGA and d.PRP_Price_responsible_desc <> ifnull(t.LTEXT,'Not Set');

drop table if exists tmp_part;
create table tmp_part as
select distinct
PartNumber  as partnumber,
Laboratory,
row_number() over (partition by PartNumber  order by '' ) rno
from dim_part
where PartNumber <>  'Not Set';


update dim_mdg_part mdg
set mdg.primary_production_location=cm.primary_manufacturing_site
,mdg.primary_production_location_name=cm.primary_manufacturing_location
from dim_mdg_part mdg,new_cluster_mapping cm,tmp_part tmp
where mdg.partnumber=tmp.partnumber
and tmp.laboratory=cm.laboratory_code
and rno=1
and milliporeflag ='Not Set';


/*Bring Global Material numbers just for Millipore (milliporeflag ='Y')  */




update dim_mdg_part
set MaterialType = ifnull(MTART,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
    and milliporeflag = 'Y'
	and ifnull(MaterialType,'a') <> ifnull(MTART,'Not Set');



update dim_mdg_part
set MaterialTypeDescr = ifnull(t134t_mtbez,'Not Set')
from dim_mdg_part, MDG_T134T
where MaterialType = t134t_mtart
    and milliporeflag = 'Y'
	and ifnull(MaterialTypeDescr,'a') <> ifnull(t134t_mtbez,'Not Set');

 update dim_mdg_part d
  set d.partdescription = ifnull(t.MAKT_MAKTX,'Not Set')
  from dim_mdg_part d
    inner join MDG_MAKT t on d.partnumber = t.MAKT_MATNR
  where      milliporeflag = 'Y'
            and d.partdescription <> ifnull(t.MAKT_MAKTX,'Not Set');

 update dim_mdg_part
set MatTypeforGlbRep = ifnull(YYD_GLMTY,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
    and milliporeflag = 'Y'
	and ifnull(MatTypeforGlbRep,'a') <> ifnull(YYD_GLMTY,'Not Set');

	update dim_mdg_part
set MatTypeforGlbRepDescr = ifnull(yyd_text,'Not Set')
from dim_mdg_part, MDG_T9D241T
where MatTypeforGlbRep = yyd_glmty
    and milliporeflag = 'Y'
	and ifnull(MatTypeforGlbRepDescr,'a') <> ifnull(yyd_text,'Not Set');

	update dim_mdg_part
set GlbProductGroup = ifnull(YYD_YSBU,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
    and milliporeflag = 'Y'
	and ifnull(GlbProductGroup,'a') <> ifnull(YYD_YSBU,'Not Set');

/* APP-7108: Oana 04Aug2017 */
update dim_mdg_part d
set globalbrand = ifnull(MARA_YYD_RIMIB, 'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
     and globalbrand <> ifnull(MARA_YYD_RIMIB,'Not Set');

update dim_mdg_part d
  set IMCCode = ifnull(MARA_YYD_IMCOD, 'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
     and IMCCode <> ifnull(MARA_YYD_IMCOD,'Not Set');




/* INSERT NEW ROWS */
DELETE FROM number_fountain m
WHERE m.table_name = 'dim_mdg_part';

INSERT INTO number_fountain
SELECT 'dim_mdg_part',
       ifnull(MAX(d.dim_mdg_partid ), ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_mdg_part d
WHERE d.dim_mdg_partid <> 1;

INSERT INTO dim_mdg_part
(
  dim_mdg_partid,
  partnumber,
  MatTypeforGlbRep,
  GlbProductGroup,
  GlbABCIndicator,
  Oldmatnumber,
  BaseUnitofMeasure,
  TotalShelfLife,
  MinRemainingShelfLife,
  CrossPlantMatStatus,
  MaterialType,
  Generalitemcatgroup,
  Producthierarchy,
  Batchmanreqind,
  APORelevance,
  global_operational_hierarchy,
  MatTypeforGlbRepDescr,
  CrossPlantMatStatusDescr,
  MaterialTypeDescr,
  GeneralitemcatgroupDescr,
  ProducthierarchyDescr,
  APORelevanceDesc,
  global_operational_hierarchy_description,
  COGSICMIndicator,
  UnitOfCompleteContentOfThePackage,
  CompleteContentOfThePackage,
  ppu,
  ppu_units,
  act_conv,
  unit2,
  gaussuom,
  gaussuomdesc,
  articlenumber,
  supplychainplaningppi,
  Dosage,
  UnitofDosage,
  milliporeflag,
  GlobalBrand,
  IMCCode
)
SELECT (SELECT ifnull(m.max_id, 1)
        FROM number_fountain m
        WHERE m.table_name = 'dim_mdg_part') + ROW_NUMBER() OVER(ORDER BY '') as dim_mdg_partid,
       m.MATNR as partnumber,
	   ifnull(YYD_GLMTY,'Not Set'),
	   ifnull(YYD_YSBU,'Not Set'),
	   ifnull(YYD_ABCCL,'Not Set'),
	   ifnull(BISMT,'Not Set'),
	   ifnull(MEINS,'Not Set'),
	   ifnull(MHDHB,0),
	   ifnull(MHDRZ,0),
	   ifnull(MSTAE,'Not Set'),
	   ifnull(MTART,'Not Set'),
	   ifnull(MTPOS_MARA,'Not Set'),
	   ifnull(PRDHA,'Not Set'),
	   ifnull(XCHPF,'Not Set'),
	   ifnull(YYD_APORL,'Not Set'),
	   ifnull(YYD_GLOPH, 'Not Set'),
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   ifnull(YYD_COGSI,'Not Set'),
	   ifnull(YYD_PPUNI,'Not Set'),
	   ifnull(YYD_COMCO,0),
	   0,
	   'Not Set',
	   0,
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   ifnull(YYD_YGART, 'Not Set'),
	   ifnull(YYD_SCPPI,'Not Set'),
	   ifnull(MARA_YYD_AIDOS, 0 ) as Dosage,
           ifnull(MARA_YYD_AIUNI,'Not Set') as UnitofDosage,
        'Y' as milliporeflag,
      ifnull(MARA_YYD_RIMIB,'Not Set') as GlobalBrand,
      ifnull(MARA_YYD_IMCOD, 'Not Set') as IMCCode
FROM MDG_MARA m
WHERE NOT EXISTS (SELECT 'x'
                  FROM dim_mdg_part dmp
                  WHERE dmp.partnumber = m.MATNR
                        and milliporeflag = 'Y');




drop table if exists tmp_aditionam_mdm_atributes;
create table tmp_aditionam_mdm_atributes
as
select distinct a.md_mmmaterial,b.usmd_active,b.usmd_o_yorg,b.md_mmyyd_orga,b.md_mmyyd_yorga,row_number() over (partition by md_mmmaterial,b.md_mmyyd_orga  order by '' ) rono
from MDG_YYD_V_MAPPING a
	inner join MDG_YYD_V_YORG b on a.usmdkmmmaterial = b.usmdkmmmaterial and usmd_active = 1 and ifnull(usmd_o_yorg,'Y') <> 'X';

update dim_mdg_part d
set d.primary_production_location = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PPL'
    and milliporeflag = 'Y'
    and rono = 1
	and ifnull(d.primary_production_location,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.primary_production_location_name = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.primary_production_location = t.YYD_YORGA
    and milliporeflag = 'Y'
    and d.primary_production_location_name <> ifnull(t.LTEXT,'Not Set');

/* APP-7108 - Oana 21Aug2017 */
update dim_mdg_part d
  set globalbranddescr = ifnull(T9DRIMIBT_LTEXT, 'Not Set')
from dim_mdg_part, MDG_T9DRIMIBT
where globalbrand = T9DRIMIBT_YYD_RIMIB
      and globalbranddescr <> ifnull(T9DRIMIBT_LTEXT,'Not Set');

update dim_mdg_part d
  set IMCCodeDescr = ifnull(MDG_T9D343T_YYD_TEXT, 'Not Set')
from dim_mdg_part, MDG_T9D343T
where IMCCode = MDG_T9D343T_YYD_IMCOD
     and IMCCodeDescr <> ifnull(MDG_T9D343T_YYD_TEXT,'Not Set');

update dim_mdg_part
  set IMCCODE_and_Descr = IMCCode || ' ' || IMCCodeDescr
where IMCCODE_and_Descr <> IMCCode || ' ' || IMCCodeDescr
  and IMCCODE <> 'Not Set' ;

/* Expression test  */

update dim_mdg_part
set PRODUCT_HIERARCHY_DESCRIPTION2 = decode(producthierarchy,'Not Set',producthierarchy,left(producthierarchy,3))
where PRODUCT_HIERARCHY_DESCRIPTION2 <> decode(producthierarchy,'Not Set',producthierarchy,left(producthierarchy,3));

update dim_mdg_part
set PrimaryProductionLocation = primary_production_location || ' - ' || primary_production_location_name
where PrimaryProductionLocation <> primary_production_location || ' - ' || primary_production_location_name;

/* 7 august 2017 - Cornelia add new attribute MaterialCategory */
update dim_mdg_part 
set materialcategory ='Finished goods'
where mattypeforglbrep in (
'MER',
'FERT',
'FIN',
'ZCSM',
'ZFRT',
'ZVFT',
'ZVAR',
'KMAT',
'LEER',
'NLAG',
'UNBW',
'ZLIT',
'ZRCH',
'ZCTO',
'Not Set'
);


update dim_mdg_part 
set materialcategory ='Raw Material'
where mattypeforglbrep in (
'DIEN',
'DUM',
'HIBE',
'RAW',
'ROH',
'VERP',
'ZUNB',
'ZVRP',
'PROC',
'ZHLB',
'ZIMU',
'ABF'
);

update dim_mdg_part 
set materialcategory ='Unfinished Goods'
where mattypeforglbrep in ('HALB','UFG');

UPDATE dim_mdg_part dp 
SET  dp.MDGPartNumber_NoLeadZero = ifnull(case when length(dp.partnumber) = 18 and dp.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from dp.partnumber) else dp.partnumber end,'Not Set');

