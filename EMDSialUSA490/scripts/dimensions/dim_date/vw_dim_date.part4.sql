Update companymaster_d00 set MAXDT_EXISTS = 0;
Update companymaster_d00 t1
set MAXDT_EXISTS = IFNULL(dd.exst, 0)
from companymaster_d00 t1
		left join (select 1 as exst, x.* from dim_date x) dd on dd.DateValue = t1.MAXDT AND dd.companycode = t1.pCompanyCode;
                               
Drop table if exists mh_i01;
Create table mh_i01
as Select ifnull(max(dim_dateid),0) maxid
from dim_date;
 
      INSERT INTO dim_date(dim_dateid,DateValue,
                          DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                         DaysInMonth,
                         DaysInMonthSofar,
                         WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          select mh_i01.maxid + row_number() over( order by ''), MAXDT,
                  MAXDT_NAME,
                  0,
                  YEAR(MAXDT),
                  0,
                  case when MONTH(MAXDT) in (1,2,3) then 1		/* VW original: QUARTER(MAXDT) */ 
					   when MONTH(MAXDT) in (4,5,6) then 2
					   when MONTH(MAXDT) in (7,8,9) then 3
					   when MONTH(MAXDT) in (10,11,12) then 4 end,
                  0,
                  concat(YEAR(MAXDT), ' Q ',case when MONTH(MAXDT) in (1,2,3) then 1		
												 when MONTH(MAXDT) in (4,5,6) then 2
												 when MONTH(MAXDT) in (7,8,9) then 3
												 when MONTH(MAXDT) in (10,11,12) then 4 end),
                  0,
                  0,
                  'Not Set',
                  'Not Set',
                  0,
                  MONTH(MAXDT),
                  to_char(MAXDT, 'Month') ,					/* VW original: date_format(MAXDT,'%M') */ 
                  LEFT(to_char(MAXDT, 'Month'),3),			/* VW original: date_format(MAXDT,'%M') */
                  0,
                  0,
                  0,
                  CASE 
					WHEN TO_CHAR(TRUNC(MAXDT,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(MAXDT,'UW') + 1 
					ELSE TO_CHAR(MAXDT,'UW') 
				END /*WEEK(MAXDT) */,		/* VW original: WEEK(MAXDT,4) */
                  0,
                  0,
                  DAY(MAXDT),
                  0,
                  cast(to_char(MAXDT,'DD') as integer), 															/* VW original: DAYOFMONTH(MAXDT) */
                  cast(to_char(MAXDT,'D') as integer),																/* VW original: DAYOFWEEK(MAXDT) */
                  to_char(MAXDT, 'Day'),																			/* VW original: date_format(MAXDT,'%W') */
                  LEFT(to_char(MAXDT, 'Day'),3),																	/* VW original: LEFT(date_format(MAXDT,'%W'),3) */
                  (CASE WHEN cast(to_char(MAXDT,'D') as integer) IN (1,7) THEN 1 ELSE 0 END),						/* VW original: DAYOFWEEK(MAXDT) */
                  (CASE WHEN ((mod(YEAR(MAXDT),4) = 0)  AND (mod(YEAR(MAXDT) , 100) != 0 OR mod(YEAR(MAXDT) ,400) = 0)) THEN 1 ELSE 0 END),
                  cast(to_char(CAST(concat('9999-01-01 00:00:00') AS timestamp),'DDD') as integer),					/* VW original: DAYOFYEAR(CAST(concat('9999-01-01 00:00:00') AS timestamp)) */
                  cast(to_char(CAST(concat('9999-01-01 00:00:00') AS timestamp),'DDD') as integer),					/* VW original: DAYOFYEAR(CAST(concat('9999-01-01 00:00:00') AS timestamp)) */
                  cast(to_char(MAXDT,'DD') as integer) - 1,															/* VW original: DAYOFMONTH(MAXDT) */
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  cast(to_char(MAXDT,'DD') as integer),																/* VW original: DAYOFMONTH(MAXDT) */					
                  0,
                  0,
                  pCompanyCode,
                  concat(YEAR(MAXDT),'-',lpad(convert(varchar (2),CASE 
					WHEN TO_CHAR(TRUNC(MAXDT,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(MAXDT,'UW') + 1 
					ELSE TO_CHAR(MAXDT,'UW') 
				END /*WEEK(MAXDT) */),2,'0'))		/* VW original: WEEK(MAXDT,4) */
		  from companymaster_d00,mh_i01 where MAXDT_EXISTS = 0;
                
drop table if exists mh_i01;
 
Update companymaster_d00 
SET MAXDT = TO_DATE('12/31/9999','MM/DD/YYYY'), MAXDT_NAME = '31 Dec 9999';
 
Update companymaster_d00 t1
set MAXDT_EXISTS = IFNULL(dd.exst, 0)
from companymaster_d00 t1
		left join (select 1 as exst, x.* from dim_date x) dd on dd.DateValue = t1.MAXDT AND dd.companycode = t1.pCompanyCode;
                               
drop table if exists mh_i01;
Create table mh_i01
as
select 	ifnull(max(d.dim_dateid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from dim_date d
where d.dim_dateid <> 1;

 
      INSERT INTO dim_date(dim_dateid,DateValue,
                          DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          select mh_i01.maxid + row_number() over(order by ''),MAXDT,
                  MAXDT_NAME,
                  0,
                  YEAR(MAXDT),
                  0,
				  case when MONTH(MAXDT) in (1,2,3) then 1		/* VW original: QUARTER(MAXDT) */ 
					   when MONTH(MAXDT) in (4,5,6) then 2
					   when MONTH(MAXDT) in (7,8,9) then 3
					   when MONTH(MAXDT) in (10,11,12) then 4 end,
                  0,
                  concat(YEAR(MAXDT), ' Q ',case when MONTH(MAXDT) in (1,2,3) then 1		
												 when MONTH(MAXDT) in (4,5,6) then 2
												 when MONTH(MAXDT) in (7,8,9) then 3
												 when MONTH(MAXDT) in (10,11,12) then 4 end),
                  0,
                  0,
                  'Not Set',
                  'Not Set',
                  0,
                  MONTH(MAXDT),
                  to_char(MAXDT, 'Month') ,					/* VW original: date_format(MAXDT,'%M') */ 
                  LEFT(to_char(MAXDT, 'Month'),3),			/* VW original: date_format(MAXDT,'%M') */
                  0,
                  0,
                  0,
                  CASE 
					WHEN TO_CHAR(TRUNC(MAXDT,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(MAXDT,'UW') + 1 
					ELSE TO_CHAR(MAXDT,'UW') 
				END /* WEEK(MAXDT) */,		/* VW original: WEEK(MAXDT,4) */
                  0,
                  0,
                  DAY(MAXDT),
                  0,
                  cast(to_char(MAXDT,'DD') as integer), 															/* VW original: DAYOFMONTH(MAXDT) */
                  cast(to_char(MAXDT,'D') as integer),																/* VW original: DAYOFWEEK(MAXDT) */
                  to_char(MAXDT, 'Day'),																			/* VW original: date_format(MAXDT,'%W') */
                  LEFT(to_char(MAXDT, 'Day'),3),																	/* VW original: LEFT(date_format(MAXDT,'%W'),3) */
                  (CASE WHEN cast(to_char(MAXDT,'D') as integer) IN (1,7) THEN 1 ELSE 0 END),						/* VW original: DAYOFWEEK(MAXDT) */
                  (CASE WHEN ((mod(YEAR(MAXDT) , 4) = 0)  AND (mod(YEAR(MAXDT) , 100) != 0 OR mod(YEAR(MAXDT) , 400) = 0)) THEN 1 ELSE 0 END),
                  cast(to_char(CAST(concat('9999-01-01 00:00:00') AS timestamp),'DDD') as integer),					/* VW original: ifnull(DAYOFYEAR(CAST(concat('9999-12-31 00:00:00') AS timestamp(0))),365) */
                  cast(to_char(CAST(concat('9999-01-01 00:00:00') AS timestamp),'DDD') as integer),					/* VW original: ifnull(DAYOFYEAR(CAST(concat('9999-12-31 00:00:00') AS timestamp(0))),365) */
                  cast(to_char(MAXDT,'DD') as integer) - 1,															/* VW original: DAYOFMONTH(MAXDT) */
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  cast(to_char(MAXDT,'DD') as integer),																/* VW original: DAYOFMONTH(MAXDT) */					
                  0,
                  0,
                  pCompanyCode,
                  concat(YEAR(MAXDT),'-',lpad(convert(varchar(2), CASE 
					WHEN TO_CHAR(TRUNC(MAXDT,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(MAXDT,'UW') + 1 
					ELSE TO_CHAR(MAXDT,'UW') 
				END /* WEEK(MAXDT)*/ ),2,'0'))							/* VW original: WEEK(MAXDT,4) */
		from companymaster_d00,mh_i01
		where MAXDT_EXISTS=0;
                 
drop table if exists mh_i01; 
  
  insert into dim_monthyear
  (Dim_MonthYearid, MonthYear, MonthYearAlt, MonthStartDate, MonthEndDate, DaysInMonth, CalYear, WorkdaysInMonth)
  select distinct case Dim_Dateid when 1 then 1 else CalendarMonthID end CalendarMonthID, 
          case Dim_Dateid when 1 then 'Jan 0001' else MonthYear end MonthYear, 
          case Dim_Dateid when 1 then '0001-01' else concat(CalendarYear,'-',lpad(CalendarMonthnumber,2,0)) end MonthYearAlt, 
          MonthStartDate, 
          MonthEndDate, 
          DaysInMonth, 
          CalendarYear CalYear, 
          case Dim_Dateid when 1 then 0 else WorkdaysInMonth end WorkdaysInMonth
  from dim_date dt
  where (Dim_Dateid = 1 or CalendarMonthID > 0) and companycode = 'Not Set'
      and not exists (select 1 from dim_monthyear my 
                      where my.Dim_MonthYearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarMonthID end);

DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins1;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_del;

CREATE TABLE tmp_dimdate_dim_year_ins 
AS
select distinct case Dim_Dateid when 1 then 1 else CalendarYear end Dim_Yearid,
CalendarYear CalYear,
case CalendarYear when 9999 then 365 else DaysInCalendarYear end DaysInCalendarYear
from dim_date dt
where (Dim_Dateid = 1 or CalendarYear > 0) and companycode = 'Not Set';

CREATE TABLE tmp_dimdate_dim_year_ins1
AS
SELECT DISTINCT Dim_Yearid
FROM tmp_dimdate_dim_year_ins;

CREATE TABLE tmp_dimdate_dim_year_del
AS
SELECT * FROM tmp_dimdate_dim_year_ins1 where 1=2;

INSERT INTO tmp_dimdate_dim_year_del
SELECT DISTINCT y.Dim_Yearid
FROM dim_year y, dim_date dt
where y.Dim_Yearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarYear end;

merge into tmp_dimdate_dim_year_ins1 t1
using tmp_dimdate_dim_year_del t2 on t1.Dim_Yearid = t2.Dim_Yearid
when matched then delete;
					/* VW original: call vectorwise(combine 'tmp_dimdate_dim_year_ins1-tmp_dimdate_dim_year_del') */

insert into dim_year(
dim_yearid,calyear,daysincalendaryear)
select DISTINCT dt.Dim_Yearid, 
dt.CalYear,
dt.DaysInCalendarYear
from tmp_dimdate_dim_year_ins dt, tmp_dimdate_dim_year_ins1 i
WHERE i.Dim_Yearid = dt.Dim_Yearid;
/*where (Dim_Dateid = 1 or CalendarYear > 0) and companycode = 'Not Set'
and not exists (select 1 from dim_year y 
      where y.Dim_Yearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarYear end)*/

DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins1;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_del;

/* business days sequence calculation */
  
drop table if exists dimdateseqtemp;
create table dimdateseqtemp (
companycode varchar(10) null,
dim_dateid bigint null,
datevalue timestamp null,
dateseqno integer null);

Insert into dimdateseqtemp(companycode,dim_dateid,datevalue,dateseqno)
select companycode,dim_dateid, datevalue, row_number() over(partition by companycode order by DateValue)  DtSeqNo
from dim_date dt
where dt.IsaPublicHoliday = 0 and dt.IsaWeekendday = 0 ;

update dim_date dt
set dt.BusinessDaysSeqNo = 0;

/* Update BusinessDaysSeqNo for business days */
update dim_date dt
  set dt.BusinessDaysSeqNo = s.dateseqno
from dimdateseqtemp s, dim_date dt
where  dt.dim_dateid = s.dim_dateid
and dt.companycode = s.companycode 
and dt.BusinessDaysSeqNo <> s.dateseqno;




/* Update BusinessDaysSeqNo for holidays/weekends where previous day is a business day */
update dim_date dt 
 set dt.BusinessDaysSeqNo = s.dateseqno
from dimdateseqtemp s ,dim_date dt 
where  dt.datevalue + 1 = s.datevalue
and dt.companycode = s.companycode
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1) ;

/* Update BusinessDaysSeqNo for holidays/weekends where previous day is also a non-business day */
update dim_date dt 
 set dt.BusinessDaysSeqNo = s.dateseqno
from dimdateseqtemp s ,dim_date dt 
where  dt.BusinessDaysSeqNo = 0
AND dt.datevalue + 2 = s.datevalue
and dt.companycode = s.companycode
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1) ;

update dim_date dt 
 set dt.BusinessDaysSeqNo = s.dateseqno
from dimdateseqtemp s ,dim_date dt 
where  dt.BusinessDaysSeqNo = 0
AND dt.datevalue + 3 = s.datevalue
and dt.companycode = s.companycode
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1) ;


update dim_date dt 
 set dt.BusinessDaysSeqNo = s.dateseqno
from dimdateseqtemp s ,dim_date dt 
where  dt.BusinessDaysSeqNo = 0
AND dt.datevalue + 4 = s.datevalue
and dt.companycode = s.companycode
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1) ;

update dim_date dt 
 set dt.BusinessDaysSeqNo = s.dateseqno
from dimdateseqtemp s ,dim_date dt 
where  dt.BusinessDaysSeqNo = 0
AND dt.datevalue + 5 = s.datevalue
and dt.companycode = s.companycode
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1) ;


/* Handle the case where the first date or first few days were non-business days */
UPDATE dim_date
SET BusinessDaysSeqNo = 0
WHERE BusinessDaysSeqNo IS NULL;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycount;
CREATE TABLE tmp_dim_date_weekdaycount
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,CalendarYear,
row_number() over( PARTITION BY CompanyCode,CalendarYear order by d2.datevalue) WeekdaysInCalendarYearSofar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycountfinyr;
CREATE TABLE tmp_dim_date_weekdaycountfinyr
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,FinancialYear,
row_number() over( PARTITION BY CompanyCode,FinancialYear order by d2.datevalue) WeekdaysInFinancialYearSofar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycountmonth;
CREATE TABLE tmp_dim_date_weekdaycountmonth
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,CalendarYear,MonthName,
row_number() over( PARTITION BY CompanyCode,CalendarYear,MonthName order by d2.datevalue) WeekdaysInMonthSoFar
from dim_date d2
WHERE d2.IsaWeekendday = 0;


UPDATE tmp_dim_date_weekdaycount
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycount
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountfinyr
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountfinyr
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountmonth
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountmonth
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';
DROP TABLE IF EXISTS tmp_weekenddays;
CREATE TABLE tmp_weekenddays
AS
SELECT datevalue
FROM dim_date 
WHERE IsaWeekendday = 1;

/* Update WeekdaysInCalendarYearSofar*/
UPDATE dim_date d
SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
FROM tmp_dim_date_weekdaycount w, dim_date d
WHERE d.datevalue = w.datevalue
AND d.CompanyCode = w.CompanyCode
AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar;

merge into dim_date dim
using (select distinct d.dim_dateid, w.WeekdaysInCalendarYearSofar
       from dim_date d
				inner join tmp_dim_date_weekdaycount w on d.datevalue = w.date_value_next AND d.CompanyCode = w.CompanyCode
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.WeekdaysInCalendarYearSofar = src.WeekdaysInCalendarYearSofar
where dim.WeekdaysInCalendarYearSofar <> src.WeekdaysInCalendarYearSofar;
			/* VW Original:
				UPDATE dim_date d
				SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
				FROM tmp_dim_date_weekdaycount w, tmp_weekenddays e, dim_date d
				WHERE d.datevalue = w.date_value_next
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar*/
			
merge into dim_date dim
using (select distinct d.dim_dateid, w.WeekdaysInCalendarYearSofar
       from dim_date d
				inner join tmp_dim_date_weekdaycount w on d.datevalue = w.date_value_next_2 AND d.CompanyCode = w.CompanyCode
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.WeekdaysInCalendarYearSofar = src.WeekdaysInCalendarYearSofar
where dim.WeekdaysInCalendarYearSofar <> src.WeekdaysInCalendarYearSofar;

			/* VW Original:
				UPDATE dim_date d
				SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
				FROM tmp_dim_date_weekdaycount w,tmp_weekenddays e, dim_date d
				WHERE d.datevalue = w.date_value_next_2
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
				AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar */ /* Check that the previous day was also a weekend day */

/* Update weekdaysinfinancialyearsofar*/

UPDATE dim_date d
SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
FROM tmp_dim_date_weekdaycountfinyr w, dim_date d
WHERE d.datevalue = w.datevalue
AND d.CompanyCode = w.CompanyCode 
AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar;

merge into dim_date dim
using (select distinct d.dim_dateid, w.weekdaysinfinancialyearsofar
       from dim_date d
				inner join tmp_dim_date_weekdaycountfinyr w on d.datevalue = w.date_value_next AND d.CompanyCode = w.CompanyCode
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinfinancialyearsofar = src.weekdaysinfinancialyearsofar
where dim.weekdaysinfinancialyearsofar <> src.weekdaysinfinancialyearsofar;
			/* VW Original:
				UPDATE dim_date d
				SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
				FROM tmp_dim_date_weekdaycountfinyr w, tmp_weekenddays e, dim_date d
				WHERE d.datevalue = w.date_value_next
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar */

merge into dim_date dim
using (select distinct d.dim_dateid, w.weekdaysinfinancialyearsofar
       from dim_date d
				inner join tmp_dim_date_weekdaycountfinyr w on d.datevalue = w.date_value_next_2 AND d.CompanyCode = w.CompanyCode
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinfinancialyearsofar = src.weekdaysinfinancialyearsofar
where dim.weekdaysinfinancialyearsofar <> src.weekdaysinfinancialyearsofar;
			/* VW Original:
				UPDATE dim_date d
				SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
				FROM tmp_dim_date_weekdaycountfinyr w,tmp_weekenddays e, dim_date d
				WHERE d.datevalue = w.date_value_next_2
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
				AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar */ /* Check that the previous day was also a weekend day */

/* Reset to 0 where its the 1st month of a financial year and 1st is Saturday */

merge into dim_date dim
using (select distinct d.dim_dateid
       from dim_date d
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where    substring(d.FinancialMonthID,5,2) = '01'
		    AND DAY(d.datevalue) = 1
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinfinancialyearsofar = 0
where dim.weekdaysinfinancialyearsofar <> 0;
			/* VW Original:
				UPDATE dim_date d
				SET d.weekdaysinfinancialyearsofar = 0
				FROM tmp_weekenddays e, dim_date d
				WHERE d.datevalue = e.datevalue
				AND substring(d.FinancialMonthID,5,2) = '01'
				AND DAY(d.datevalue) = 1
				AND d.weekdaysinfinancialyearsofar <> 0 */

/* Reset to 0 where its the 1st month of a financial year and 1st or 2nd is Sunday */
merge into dim_date dim
using (select distinct d.dim_dateid
       from dim_date d
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where    substring(d.FinancialMonthID,5,2) = '01'
		    AND DAY(d.datevalue) IN ( 1,2)
			AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinfinancialyearsofar = 0
where dim.weekdaysinfinancialyearsofar <> 0;
			/* VW Original:
				UPDATE dim_date d
				SET d.weekdaysinfinancialyearsofar = 0
				FROM tmp_weekenddays e, dim_date d
				WHERE d.datevalue = e.datevalue
				AND substring(FinancialMonthID,5,2) = '01'
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
				AND date_format(d.datevalue,'%e') IN ( 1,2)
				AND d.weekdaysinfinancialyearsofar <> 0 */

/* Update weekdaysinmonthsofar */

UPDATE dim_date d
SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
FROM tmp_dim_date_weekdaycountmonth w, dim_date d
WHERE d.datevalue = w.datevalue 
AND d.CompanyCode = w.CompanyCode
AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar;

/* Update for Saturdays */
merge into dim_date dim
using (select distinct d.dim_dateid, w.weekdaysinmonthsofar
       from dim_date d
				inner join tmp_dim_date_weekdaycountmonth w on d.datevalue = w.date_value_next AND d.CompanyCode = w.CompanyCode
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = src.weekdaysinmonthsofar
where dim.weekdaysinmonthsofar <> src.weekdaysinmonthsofar;
			/* VW Original:
				UPDATE dim_date d
				SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
				FROM tmp_dim_date_weekdaycountmonth w, tmp_weekenddays e, dim_date d
				WHERE d.datevalue = w.date_value_next
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar*/

/* Update for Sundays */
merge into dim_date dim
using (select distinct d.dim_dateid, w.weekdaysinmonthsofar
       from dim_date d
				inner join tmp_dim_date_weekdaycountmonth w on d.datevalue = w.date_value_next_2 AND d.CompanyCode = w.CompanyCode
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 ) 
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = src.weekdaysinmonthsofar
where dim.weekdaysinmonthsofar <> src.weekdaysinmonthsofar;
			/* VW Original:
				UPDATE dim_date d
				SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
				FROM tmp_dim_date_weekdaycountmonth w,tmp_weekenddays e, dim_date d
				WHERE d.datevalue = w.date_value_next_2
				AND d.datevalue = e.datevalue
				AND d.CompanyCode = w.CompanyCode
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
				AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar */ /* Check that the previous day was also a weekend day */

/* Reset to 0 where its a Sat and the 1st of a month */

merge into dim_date dim
using (select distinct d.dim_dateid
       from dim_date d
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where    DAY(d.datevalue) = 1
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = 0
where dim.weekdaysinmonthsofar <> 0;
			/* VW Original:
				UPDATE dim_date d
				SET d.weekdaysinmonthsofar = 0
				FROM tmp_weekenddays e, dim_date d
				WHERE d.datevalue = e.datevalue
				AND date_format(d.datevalue,'%e') = 1
				AND d.weekdaysinmonthsofar <> 0 */ 

/* Reset to 0 where its a Sunday and 1st or 2nd of a month */
merge into dim_date dim
using (select distinct d.dim_dateid
       from dim_date d
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where    DAY(d.datevalue) IN ( 1,2)
		    AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 ) 
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = 0
where dim.weekdaysinmonthsofar <> 0;
			/* VW Original:
				UPDATE dim_date d
				SET d.weekdaysinmonthsofar = 0
				FROM tmp_weekenddays e, dim_date d
				WHERE d.datevalue = e.datevalue
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
				AND date_format(d.datevalue,'%e') IN ( 1,2)
				AND d.weekdaysinmonthsofar <> 0 */


/* workdaysincalendaryearsofar eqivalent to weekdaysincalendaryearsofar 
workdaysinfinancialyearsofar equivalent to weekdaysinfinancialyearsofar */

UPDATE dim_date
SET workdaysincalendaryearsofar = WeekdaysInCalendarYearSofar;

UPDATE dim_date
SET workdaysinfinancialyearsofar = weekdaysinfinancialyearsofar;


UPDATE dim_date
SET workdaysinmonthsofar = weekdaysinmonthsofar;

/* Update values for default dates */
UPDATE dim_date
SET weekdaysincalendaryearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET weekdaysinfinancialyearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET workdaysincalendaryearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET workdaysinfinancialyearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET weekdaysinmonthsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET workdaysinmonthsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET WeekStartDate = '0001-01-01' 
WHERE dim_dateid = 1;

UPDATE dim_date
SET WeekEndDate = '0001-01-06' 
WHERE dim_dateid = 1;

UPDATE dim_date
SET MonthStartDate = '0001-01-01' 
WHERE dim_dateid = 1;


UPDATE dim_date
SET MonthEndDate = '9999-01-01' 
WHERE dim_dateid = 1;

UPDATE dim_date
SET FinancialMonthStartDate = '0001-01-01' 
WHERE dim_dateid = 1;

UPDATE dim_date
SET FinancialMonthEndDate = '9999-01-01' 
WHERE dim_dateid = 1;

UPDATE dim_date
SET WeekStartDate = '9998-12-27' 
WHERE datevalue IN ( '9999-01-01','9999-12-31');

UPDATE dim_date
SET WeekEndDate = '9999-01-02' 
WHERE datevalue IN ( '9999-01-01','9999-12-31');

UPDATE dim_date
SET MonthStartDate = '9999-01-01' 
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET MonthEndDate = '9999-12-31' 
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET FinancialMonthStartDate = '9999-01-01' 
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date
SET FinancialMonthEndDate = '9999-12-31' 
WHERE datevalue IN ( '9999-01-01','9999-12-31');


UPDATE dim_date
SET DaysInFinancialYear = financialyearenddate - financialyearstartdate + 1
where DaysInFinancialYear <> financialyearenddate - financialyearstartdate + 1;

drop table if exists tmp_finworkdays_001;
create table tmp_finworkdays_001 as
select companycode, financialyear, count(*) workdaysfinyr 
from dim_date 
where isaweekendday = 0 and isapublicholiday = 0
group by companycode, financialyear;

update dim_date dt
set dt.weekdaysinfinancialyear = a.workdaysfinyr
from tmp_finworkdays_001 a, dim_date dt
where dt.companycode = a.companycode and dt.financialyear = a.financialyear
and dt.weekdaysinfinancialyear <> a.workdaysfinyr;

update dim_date dt
set dt.workdaysinfinancialyear = a.workdaysfinyr
from tmp_finworkdays_001 a, dim_date dt
where dt.companycode = a.companycode and dt.financialyear = a.financialyear
and dt.workdaysinfinancialyear <> a.workdaysfinyr;

/* Fix daysinfinancialyearsofar */
UPDATE dim_date
SET daysinfinancialyearsofar = current_date - financialyearstartdate + 1
WHERE daysinfinancialyearsofar <> current_date - financialyearstartdate + 1;

/* financialweek should be the weekno from the start of 0th weekstart of that financial year. Week starts from 0 */
/* For example, if the first week starts on 30th March 2014,1 to 5 Apr will be finweek 0 and 6th April 2014 will be in financialweek 1 */
UPDATE dim_date a
SET financialweek = FLOOR((a.datevalue - b.weekstartdate)/7)
FROM dim_date b, dim_date a
WHERE b.companycode = a.companycode AND a.financialyear = b.financialyear AND b.datevalue = b.financialyearstartdate
AND a.financialweek <> FLOOR((a.datevalue - b.weekstartdate)/7);

UPDATE dim_date
SET financialweekid = financialyear * 100 + financialweek
WHERE financialweekid <> financialyear * 100 + financialweek;


UPDATE dim_date
SET DayofFinancialYear = daysinfinancialyearsofar
WHERE DayofFinancialYear <> daysinfinancialyearsofar;


/* Update Financial Half Data */

/* By default financialhalf = 1. Set it to 2 where financialmonthstartdate is atleast 6 months more than financialyearstartdate */
UPDATE dim_date dt
SET financialhalf = 2 
where months_between(financialmonthstartdate,financialyearstartdate) >= 6
AND financialhalf <> 2;


Update dim_date dt
SET financialhalfid = (financialyear * 10) + financialhalf
Where  financialhalfid <> (financialyear * 10) + financialhalf;

Update dim_date dt
SET financialhalfname = concat(financialyear,' H ',CAST(financialhalf AS CHAR (1)))
WHERE financialhalfname <> concat(financialyear,' H ',CAST(financialhalf AS CHAR (1)));

UPDATE dim_date dt
SET financialhalfstartdate = financialyearstartdate
WHERE financialhalf = 1
AND ifnull(financialhalfstartdate,to_date('1902-01-01','YYYY-MM-DD')) <> ifnull(financialyearstartdate,to_date('1903-01-01','YYYY-MM-DD') );

UPDATE dim_date dt
SET financialhalfstartdate = financialyearstartdate + interval '6' MONTH
WHERE financialhalf = 2
AND ifnull(financialhalfstartdate,to_date('1902-01-01','YYYY-MM-DD')) <> ifnull(financialyearstartdate + interval '6' MONTH,to_date('1903-01-01','YYYY-MM-DD'))
AND dt.datevalue <> to_date('9999-12-31','YYYY-MM-DD');

UPDATE dim_date dt
SET financialhalfenddate = financialhalfstartdate + interval '6' MONTH - interval '1' day
WHERE dt.datevalue <> to_date('9999-12-31','YYYY-MM-DD')
      and ifnull(financialhalfenddate,to_date('1902-01-01','YYYY-MM-DD')) <>  ifnull(financialhalfstartdate + interval '6' MONTH - interval '1' day,to_date('1903-01-01','YYYY-MM-DD'));


UPDATE dim_date dt
SET financialhalfenddate = to_date('9999-12-31','YYYY-MM-DD')
WHERE dt.datevalue = to_date('9999-12-31','YYYY-MM-DD')
AND financialhalfenddate <> to_date('9999-12-31','YYYY-MM-DD');

UPDATE dim_date dt
SET financialhalfstartdate = to_date('9999-12-31','YYYY-MM-DD')
WHERE dt.datevalue = to_date('9999-12-31','YYYY-MM-DD')
AND financialhalfstartdate <> to_date('9999-12-31','YYYY-MM-DD');


update dim_date 
set CalendarWeekYr = REPLACE(CalendarWeekYr,'-','')
where CalendarWeekYr <> REPLACE(CalendarWeekYr,'-','');

/* Expression test  */

update dim_date
set IsaPublicHoliday2 = case when IsaPublicHoliday= 1 then 'Yes' else 'No' end
where IsaPublicHoliday2 <> case when IsaPublicHoliday= 1 then 'Yes' else 'No' end;

update dim_date
set IsaLeapYear2 = case when IsaLeapYear= 1 then 'Yes' else 'No' end
where IsaLeapYear2 <> case when IsaLeapYear= 1 then 'Yes' else 'No' end; 

update Dim_Date
set IsaWeekendday2 = case when IsaWeekendday= 1 then 'Yes' else 'No' end
where IsaWeekendday2 <> case when IsaWeekendday= 1 then 'Yes' else 'No' end;   

update Dim_Date
set calendarmonthid2 = concat (left(calendarmonthid,4),'-',right(calendarmonthid,2))
where calendarmonthid2 <> concat (left(calendarmonthid,4),'-',right(calendarmonthid,2));

update Dim_Date
set CalendarWeekYr2 = to_char(CalendarWeekYr)
where CalendarWeekYr2 <> to_char(CalendarWeekYr); 

/* Marius Update dw_update_date when dim_date is processed */
UPDATE dim_date dt
set dt.dw_update_date = current_timestamp;
drop table if exists tmp_finworkdays_001;
drop TABLE if exists dimdateseqtemp ;
drop TABLE if exists tmp_dimdate_dim_year_del;
drop TABLE if exists tmp_dimdate_dim_year_ins;
drop TABLE if exists tmp_dimdate_dim_year_ins1;
drop TABLE if exists tmp_dim_date_weekdaycount;
drop TABLE if exists tmp_dim_date_weekdaycountfinyr;
drop TABLE if exists tmp_dim_date_weekdaycountmonth;
drop TABLE if exists tmp_weekenddays;
drop TABLE if exists tmp_ffyr_minbutag;
drop TABLE if exists TMP_FFY_T009B_1;
drop TABLE if exists TMP_FFY_T009B_2;
drop TABLE if exists TMP_T009B_1;
drop TABLE if exists TMP_T009B_2;
drop TABLE if exists TMP_T009B_2a;
drop TABLE if exists TMP_T009B_3A;
drop TABLE if exists TMP_T009B_3A_TO;
drop TABLE if exists TMP_T009B_3B;
drop TABLE if exists TMP_T009B_3B_TO;
drop TABLE if exists TMP_T009B_4A;
drop TABLE if exists TMP_T009B_4A_TO;
drop TABLE if exists TMP_T009B_4B;
drop TABLE if exists TMP_T009B_4B_TO;
drop TABLE if exists TMP_TO_3;
drop TABLE if exists TMP_TO_3B;
drop TABLE if exists TMP_TO_4;
drop TABLE if exists TMP_TO_4B;
DROP TABLE IF EXISTS tmp_testing_dimdateseqtemp;
