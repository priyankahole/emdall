
INSERT INTO dim_deliveryblock(dim_deliveryblockid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_deliveryblock
               WHERE dim_deliveryblockid = 1);
			   
UPDATE dim_deliveryblock a
   SET a.Description = ifnull(t.TVLST_VTEXT, 'Not Set'),
			a.dw_update_date = current_timestamp
  FROM dim_deliveryblock a, TVLST t
 WHERE a.DeliveryBlock = t.TVLST_LIFSP 
 AND a.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_deliveryblock';

insert into number_fountain
select 	'dim_deliveryblock',
	ifnull(max(d.dim_deliveryblockid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_deliveryblock d
where d.dim_deliveryblockid <> 1;

INSERT INTO dim_deliveryblock(dim_deliveryblockId,
                              DeliveryBlock,
                              Description,
                              RowStartDate,
                              RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_deliveryblock')
          + row_number() over(order by '') ,
                        t.TVLST_LIFSP,
          ifnull(TVLST_VTEXT, 'Not Set'),
          current_timestamp,
          1
     FROM TVLST t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_deliveryblock a
               WHERE a.DeliveryBlock = TVLST_LIFSP);

delete from number_fountain m where m.table_name = 'dim_deliveryblock';

update dim_deliveryblock d
set d.CUST_INTERNAL = ifnull(t.CUST_INTERNAL,'Not Set')
from dim_deliveryblock d, csv_blocks_and_holds t
where d.DeliveryBlock = t.CODE and d.CUST_INTERNAL <> ifnull(t.CUST_INTERNAL,'Not Set');

update dim_deliveryblock d
set d.BLK_HLD_GRP = ifnull(t.BLK_HLD_GRP,'Not Set')
from dim_deliveryblock d, csv_blocks_and_holds t
where d.DeliveryBlock = t.CODE and d.BLK_HLD_GRP <> ifnull(t.BLK_HLD_GRP,'Not Set');