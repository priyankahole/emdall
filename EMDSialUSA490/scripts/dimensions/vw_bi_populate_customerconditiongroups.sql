INSERT INTO dim_CustomerConditionGroups(dim_CustomerConditionGroupsid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1 ) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_CustomerConditionGroups
               WHERE dim_CustomerConditionGroupsid = 1);

UPDATE 
dim_CustomerConditionGroups a 
SET a.Description = ifnull(TVKGGT_VTEXT, 'Not Set'),
	a.dw_update_date = current_timestamp
FROM 
dim_CustomerConditionGroups a,
TVKGGT
WHERE a.CustomerCondGrp = TVKGGT_KDKGR AND RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_CustomerConditionGroups';

insert into number_fountain
select 	'dim_CustomerConditionGroups',
	ifnull(max(d.dim_CustomerConditionGroupsid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_CustomerConditionGroups d
where d.dim_CustomerConditionGroupsid <> 1;

INSERT INTO dim_CustomerConditionGroups(dim_CustomerConditionGroupsId,
                                    CustomerCondGrp,
                                    Description,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_CustomerConditionGroups')
			+ row_number() over(order by '') ,
          t.TVKGGT_KDKGR,
          ifnull(TVKGGT_VTEXT, 'Not Set'),
          current_timestamp,
          1
     FROM TVKGGT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM  dim_CustomerConditionGroups a
               WHERE a.CustomerCondGrp = TVKGGT_KDKGR);

delete from number_fountain m where m.table_name = 'dim_CustomerConditionGroups';