UPDATE    
dim_transactioneventtype et
SET et.TransactionEventTypeDescription = w.T158W_LTEXT,
    et.dw_update_date = current_timestamp
FROM
dim_transactioneventtype et,
t158w w
WHERE et.RowIsCurrent = 1
AND et.TransactionEventTypeCode = w.T158W_VGART
;

INSERT INTO dim_transactioneventtype(dim_transactioneventtypeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_transactioneventtype
               WHERE dim_transactioneventtypeId = 1);

delete from number_fountain m where m.table_name = 'dim_transactioneventtype';

insert into number_fountain
select 	'dim_transactioneventtype',
	ifnull(max(d.Dim_TransactionEventTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_transactioneventtype d
where d.Dim_TransactionEventTypeid <> 1;	

INSERT INTO dim_transactioneventtype(Dim_TransactionEventTypeid,
                             TransactionEventTypeCode,
                             TransactionEventTypeDescription,
                             RowStartDate,
                             RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_transactioneventtype') 
          + row_number() over(order by ''),
			 T158W_VGART,
          T158W_LTEXT,
          current_timestamp,
          1
     FROM t158w
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_transactioneventtype
                       WHERE TransactionEventTypeCode = T158W_VGART)
;
