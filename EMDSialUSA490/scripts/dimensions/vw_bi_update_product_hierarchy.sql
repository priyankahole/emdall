merge into dim_producthierarchy dim
using (select t1.DIM_PRODUCTHIERARCHYID,
			  ifnull(t2.VTEXT, 'Not Set') as Level1Desc,
			  ifnull(t3.VTEXT, 'Not Set') as Level2Desc,
			  ifnull(t4.VTEXT, 'Not Set') as Level3Desc,
			  ifnull(t5.VTEXT, 'Not Set') as Level4Desc,
			  ifnull(t6.VTEXT, 'Not Set') as Level5Desc,
				'Not Set' as Level6Desc,
			  /*ifnull(t7.VTEXT, 'Not Set') as Level6Desc,*/
			  (CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 2
					THEN substring(t1.ProductHierarchy, 1, 2)
				ELSE 'Not Set' END) as Level1Code,
			  right((CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 6
					THEN substring(t1.ProductHierarchy, 1, 6)
				ELSE 'Not Set' END),4) as Level2Code,
			  right((CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 10
					THEN substring(t1.ProductHierarchy, 1, 10)
				ELSE 'Not Set' END),4) as Level3Code,
			  right((CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 14
					THEN substring(t1.ProductHierarchy, 1, 14)
				ELSE 'Not Set' END),4) as Level4Code,
			  right((CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 18
					THEN substring(t1.ProductHierarchy, 1, 18)
				ELSE 'Not Set' END),4) as Level5Code,
				'Not Set' as Level6Code/*,
			  (CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 18
					THEN substring(t1.ProductHierarchy, 1, 18)
				ELSE 'Not Set' END) as Level6Code*/
	   from DIM_PRODUCTHIERARCHY t1
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 2)
						   where length(ph.ProductHierarchy) >= 2) t2
						on t1.DIM_PRODUCTHIERARCHYID = t2.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 6)
						   where length(ph.ProductHierarchy) >= 6) t3
						on t1.DIM_PRODUCTHIERARCHYID = t3.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 10)
						   where length(ph.ProductHierarchy) >= 10) t4
						on t1.DIM_PRODUCTHIERARCHYID = t4.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 14)
						   where length(ph.ProductHierarchy) >= 14) t5
						on t1.DIM_PRODUCTHIERARCHYID = t5.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 18)
						   where length(ph.ProductHierarchy) >= 18) t6
						on t1.DIM_PRODUCTHIERARCHYID = t6.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 18)
						   where length(ph.ProductHierarchy) >= 18) t7
						on t1.DIM_PRODUCTHIERARCHYID = t7.DIM_PRODUCTHIERARCHYID
	  ) src on dim.DIM_PRODUCTHIERARCHYID = src.DIM_PRODUCTHIERARCHYID
when matched then update set dim.Level1Desc = src.Level1Desc,
							 dim.Level1Code = src.Level1Code,
							 dim.Level2Desc = src.Level2Desc,
							 dim.Level2Code = src.Level2Code,
							 dim.Level3Desc = src.Level3Desc,
							 dim.Level3Code = src.Level3Code,
							 dim.Level4Desc = src.Level4Desc,
							 dim.Level4Code = src.Level4Code,
							 dim.Level5Desc = src.Level5Desc,
							 dim.Level5Code = src.Level5Code,
							 dim.Level6Desc = src.Level6Desc,
							 dim.Level6Code = src.Level6Code,
							 dim.dw_update_date = current_timestamp;
