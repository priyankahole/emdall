insert into dim_cluster(dim_clusterid)
select 1 from (select 1) a where not exists (select 1 from dim_cluster where dim_clusterid = 1);

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'dim_cluster';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'dim_cluster', ifnull(max(dim_clusterid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM dim_cluster where dim_clusterid <> 1;

update dim_cluster dc
set dc.clust = clst.clust
from dim_cluster dc, csv_cluster_ls clst
where dc.primary_manufacturing_site = ifnull(clst.prim_man_site,'Not Set')
	and dc.clust <> clst.clust;

insert into dim_cluster(dim_clusterid,primary_manufacturing_site,clust)
select (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_cluster') + row_number() over (order by '') dim_clusterid
	,ifnull(clst.prim_man_site,'Not Set') primary_manufacturing_site
	,ifnull(clst.clust, 'Not Set') clust
from csv_cluster_ls clst
where not exists (select 1 from dim_cluster dc where primary_manufacturing_site = ifnull(clst.prim_man_site,'Not Set'));


update dim_cluster 
set clust = case when clust ='APAC' then 'Asia'
                 when clust='APIs & Excipients' then 'API & Excipient Manufacturing'
                 when clust='Biologics' then 'Biologics'
                 when clust='Divested' then 'Divested'
                 when clust='Inorganics, Organics & Solvents' then replace(clust,' ','') --'Inorganics,Organics&Solvents'
                 when clust='Performance materials' then 'Performance Materials'
                 when clust='Separations & Instruments' then 'Separations and Instrum Manuf'
else clust end
from dim_cluster;

update dim_cluster
set clust= case when labor in ('825'
,'826'
,'827'
,'829'
,'830'
,'831'
,'832'
,'833'
,'835'
,'836'
,'837'
,'838'
,'800'
,'801'
,'802'
,'803'
,'804'
,'805'
,'806'
,'807'
,'808'
,'809'
,'810'
,'811'
,'812'
,'813'
,'814'
,'815'
,'816'
,'817'
,'818'
,'819'
,'820'
,'821'
,'822'
,'823'
,'824'
,'179'
,'301'
,'302'
,'303'
,'304'
,'305'
,'306'
,'082'
,'352'
,'GIL'
,'ULT'
,'CAN'
,'CMQ'
,'CER'
,'010'
) then 'Others' 
when labor='SAJ' then 'Asia' 
else clust end 
from dim_cluster;