INSERT INTO dim_bwhierarchycountry08
(
dim_bwhierarchycountry08id,
internalhierarchyid,
objectversion,
internalidno,
infoobject,
nodehierarchynamelvl5,
nodehierarchynamelvl4,
nodehierarchynamelvl3,
nodehierarchynamelvl2,
nodehierarchynamelvl1,
nodehierarchylevel,
nodehierarchylinkind,
nodehierarchyparentid,
nodehierarchychildid,
nodehierarchynextid,
shortdescriptionlvl1,
shortdescriptionlvl2,
shortdescriptionlvl3,
shortdescriptionlvl4,
shortdescriptionlvl5
)
select 1, 'Not Set','Not Set',0,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',0,'Not Set',0,0,0,'Not Set','Not Set','Not Set','Not Set','Not Set'
FROM (SELECT 1) a 
WHERE NOT EXISTS ( SELECT 'x' FROM dim_bwhierarchycountry08 WHERE dim_bwhierarchycountry08id =1 );




delete from number_fountain m where m.table_name = 'dim_bwhierarchycountry08';
insert into number_fountain
select 'dim_bwhierarchycountry08',
 ifnull(max(d.dim_bwhierarchycountry08id ), 
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_bwhierarchycountry08 d
where d.dim_bwhierarchycountry08id <> 1;

/*INSERT INTO dim_bwhierarchycountry08
(
dim_bwhierarchycountry08id,
internalhierarchyid,
objectversion,
internalidno,
infoobject,
nodehierarchynamelvl5,
nodehierarchynamelvl4,
nodehierarchynamelvl3,
nodehierarchynamelvl2,
nodehierarchynamelvl1,
nodehierarchylevel,
nodehierarchylinkind,
nodehierarchyparentid,
nodehierarchychildid,
nodehierarchynextid,
shortdescriptionlvl1,
shortdescriptionlvl2,
shortdescriptionlvl3,
shortdescriptionlvl4,
shortdescriptionlvl5
)
SELECT 
(select ifnull(m.max_id, 1) 
        from number_fountain m 
        where m.table_name = 'dim_bwhierarchycountry08') + row_number() over() AS dim_bwhierarchycountry08id,
       IFNULL(t1.hieid,'Not Set') as internalhierarchyID,
       IFNULL(t1.objvers,'Not Set') as objectversion,
	   IFNULL(t1.nodeid, 0) as internalidno,
       IFNULL(t1.iobjnm,'Not Set') as infoobject,
       IFNULL(t1.nodename,'Not Set') as nodehierarchynamelvl5,
       IFNULL(t2.nodename,'Not Set') as nodehierarchynamelvl4,
	   IFNULL(t3.nodename,'Not Set') as nodehierarchynamelvl3,
	   IFNULL(t4.nodename,'Not Set') as nodehierarchynamelvl2,
	   IFNULL(t5.nodename,'Not Set') as nodehierarchynamelvl1,
	   IFNULL(t1.tlevel, 0) as nodehierarchylevel, 
	   IFNULL(t1.link,'Not Set') as nodehierarchylinkind,
	   IFNULL(t1.parentid, 0) as nodehierarchyparentid,
	   IFNULL(t1.childid, 0) as nodehierarchychildid,
	   IFNULL(t1.nextid, 0) as nodehierarchynextid,
	   'Not Set' as shortdescriptionlvl1,
       'Not Set' as shortdescriptionlvl2,
       'Not Set' as shortdescriptionlvl3,
       'Not Set' as shortdescriptionlvl4,
       'Not Set' as shortdescriptionlvl5

FROM BI0_HCOUNTRYFlat t1
inner join BI0_HCOUNTRYFlat t2 on     t1.hieid = t2.hieid 
                              and t1.parentid = t2.nodeid
inner join BI0_HCOUNTRYFlat t3 on     t2.hieid = t3.hieid 
                              and t2.parentid = t3.nodeid
inner join BI0_HCOUNTRYFlat t4 on     t3.hieid = t4.hieid 
                              and t3.parentid = t4.nodeid
inner join BI0_HCOUNTRYFlat t5 on     t4.hieid = t5.hieid 
                              and t4.parentid = t5.nodeid
where not exists (select 'x'
             from dim_bwhierarchycountry08 bw
             where    t1.hieid = bw.internalhierarchyID
			       and t1.nodeid = bw.internalidno
                   and t1.objvers = bw.objectversion)
and t1.hieid =	'54G8L55S3E80Q2SXZYJAELHKR'
*/
INSERT INTO dim_bwhierarchycountry08
(
dim_bwhierarchycountry08id,
internalhierarchyid,
objectversion,
internalidno,
infoobject,
nodehierarchynamelvl5,
nodehierarchynamelvl4,
nodehierarchynamelvl3,
nodehierarchynamelvl2,
nodehierarchynamelvl1,
nodehierarchylevel,
nodehierarchylinkind,
nodehierarchyparentid,
nodehierarchychildid,
nodehierarchynextid,
shortdescriptionlvl1,
shortdescriptionlvl2,
shortdescriptionlvl3,
shortdescriptionlvl4,
shortdescriptionlvl5
)
SELECT 
(select ifnull(m.max_id, 1) 
        from number_fountain m 
        where m.table_name = 'dim_bwhierarchycountry08') + row_number() over() AS dim_bwhierarchycountry08id,
       IFNULL(a.hieid,'Not Set') as internalhierarchyID,
       IFNULL(a.objvers,'Not Set') as objectversion,
	   IFNULL(a.nodeid, 0) as internalidno,
       IFNULL(a.iobjnm,'Not Set') as infoobject,
       CASE 
         WHEN a.tlevel = 5 THEN a.nodename 
         WHEN b.tlevel = 5 THEN b.nodename 
         WHEN c.tlevel = 5 THEN c.nodename 
         WHEN d.tlevel = 5 THEN d.nodename 
         WHEN e.tlevel = 5 THEN e.nodename 
         ELSE 'Not Set' 
       END nodehierarchynamelvl5, 
       CASE 
         WHEN a.tlevel = 4 THEN a.nodename 
         WHEN b.tlevel = 4 THEN b.nodename 
         WHEN c.tlevel = 4 THEN c.nodename 
         WHEN d.tlevel = 4 THEN d.nodename 
         WHEN e.tlevel = 4 THEN e.nodename 
         ELSE 'Not Set' 
       END nodehierarchynamelvl4, 
       CASE 
         WHEN a.tlevel = 3 THEN a.nodename 
         WHEN b.tlevel = 3 THEN b.nodename 
         WHEN c.tlevel = 3 THEN c.nodename 
         WHEN d.tlevel = 3 THEN d.nodename 
         WHEN e.tlevel = 3 THEN e.nodename 
         ELSE 'Not Set' 
       END nodehierarchynamelvl3, 
       CASE 
         WHEN a.tlevel = 2 THEN a.nodename 
         WHEN b.tlevel = 2 THEN b.nodename 
         WHEN c.tlevel = 2 THEN c.nodename 
         WHEN d.tlevel = 2 THEN d.nodename 
         WHEN e.tlevel = 2 THEN e.nodename 
         ELSE 'Not Set' 
       END nodehierarchynamelvl2, 
       CASE 
         WHEN a.tlevel = 1 THEN a.nodename 
         WHEN b.tlevel = 1 THEN b.nodename 
         WHEN c.tlevel = 1 THEN c.nodename 
         WHEN d.tlevel = 1 THEN d.nodename 
         WHEN e.tlevel = 1 THEN e.nodename 
         ELSE 'Not Set' 
       END nodehierarchynamelvl1,
       IFNULL(a.tlevel, 0) as nodehierarchylevel, 
	   IFNULL(a.link,'Not Set') as nodehierarchylinkind,
	   IFNULL(a.parentid, 0) as nodehierarchyparentid,
	   IFNULL(a.childid, 0) as nodehierarchychildid,
	   IFNULL(a.nextid, 0) as nodehierarchynextid,
	   'Not Set' as shortdescriptionlvl1,
       'Not Set' as shortdescriptionlvl2,
       'Not Set' as shortdescriptionlvl3,
       'Not Set' as shortdescriptionlvl4,
       'Not Set' as shortdescriptionlvl5
FROM   bi0_hcountryflat a 
       LEFT JOIN bi0_hcountryflat b 
              ON a.hieid = b.hieid 
                 AND a.parentid = b.nodeid 
       LEFT JOIN bi0_hcountryflat c 
              ON b.hieid = c.hieid 
                 AND b.parentid = c.nodeid 
       LEFT JOIN bi0_hcountryflat d 
              ON c.hieid = d.hieid 
                 AND c.parentid = d.nodeid 
       LEFT JOIN bi0_hcountryflat e 
              ON d.hieid = e.hieid 
                 AND d.parentid = e.nodeid 
where not exists (select 'x'
             from dim_bwhierarchycountry08 bw
             where    a.hieid = bw.internalhierarchyID
			       and a.nodeid = bw.internalidno
                   and a.objvers = bw.objectversion)
AND  a.hieid = '54G8L55S3E80Q2SXZYJAELHKR' 
AND  a.childid = 0; 			   

UPDATE dim_bwhierarchycountry08 dim
FROM country_mapping m
SET shortdescriptionlvl5 =  m.shortdescription
WHERE  dim.nodehierarchynamelvl5 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;				   
	   
UPDATE dim_bwhierarchycountry08 dim
FROM country_mapping m
SET shortdescriptionlvl4 =  m.shortdescription
WHERE  dim.nodehierarchynamelvl4 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;
	 
UPDATE dim_bwhierarchycountry08 dim
FROM country_mapping m
SET shortdescriptionlvl3 =  m.shortdescription
WHERE  dim.nodehierarchynamelvl3 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;
	 
UPDATE dim_bwhierarchycountry08 dim
FROM country_mapping m
SET shortdescriptionlvl2 =  m.shortdescription
WHERE  dim.nodehierarchynamelvl2 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;
	 
UPDATE dim_bwhierarchycountry08 dim
FROM country_mapping m
SET shortdescriptionlvl1 =  m.shortdescription
WHERE  dim.nodehierarchynamelvl1 = m.hiernode
     AND dim.internalhierarchyid = m.internalhierarchyid;



CALL VECTORWISE(combine 'dim_bwhierarchycountry08');