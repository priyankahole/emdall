


insert into dim_commercialview
	(dim_commercialviewid, UpperProdHier,SoldToCountry, CommercialView, Description, CommercialRegions, CommercialRegDesc, CommSubRegion, CommSubRegionDesc)
	select 1,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set'
	from (select 1) a
	where not exists (select 'x' from dim_commercialview  where dim_commercialviewid=1)
;

delete from number_fountain m where m.table_name = 'dim_commercialview';
insert into number_fountain
select 'dim_commercialview',
            ifnull(max(d.dim_commercialviewid ), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_commercialview d
where dim_commercialviewid <> 1;


insert into dim_commercialview 
	(dim_commercialviewid, UpperProdHier,SoldToCountry, CommercialView, Description, CommercialRegions, CommercialRegDesc, CommSubRegion, CommSubRegionDesc,dw_insert_date,dw_update_date)
	select 
		(select ifnull(m.max_id,1) from number_fountain m where m.table_name = 'dim_commercialview')  + row_number() over(order by '') as dim_commercialviewid,
		ifnull(ds.UpperProdHier,'Not Set') as UpperProdHier,
		ifnull(ds.SoldToCountry,'Not Set') as SoldToCountry,
		ifnull(ds.CommercialView,'Not Set') as CommercialView, 
		ifnull(ds.Description,'Not Set') as Description, 
		ifnull(ds.CommercialRegions,'Not Set') as CommercialRegions, 
		ifnull(ds.CommercialRegDesc,'Not Set')as CommercialRegDesc,
		ifnull(ds.CommSubRegion,'Not Set') as CommSubRegion, 
		ifnull(ds.CommSubRegionDesc,'Not Set') as CommSubRegionDesc,
		current_timestamp,
		current_timestamp
	from emdtempocc4.csv_commercialview ds
	where not exists
		(select 'x' from dim_commercialview dc 
			where dc.UpperProdHier=ds.UpperProdHier 
			and dc.SoldToCountry= ds.SoldToCountry
		)
;

update dim_commercialview ds
set ds.CommercialView  = dc.CommercialView	 
from dim_commercialview ds,emdtempocc4.csv_commercialview dc
where dc.UpperProdHier=ds.UpperProdHier 
  and dc.SoldToCountry= ds.SoldToCountry
  and  ds.CommercialView  <> dc.CommercialView;

update dim_commercialview ds
set  ds.Description  = dc.Description 
from dim_commercialview ds,emdtempocc4.csv_commercialview dc
where dc.UpperProdHier=ds.UpperProdHier 
  and dc.SoldToCountry= ds.SoldToCountry
  and ds.Description  <> dc.Description ;


update dim_commercialview ds
set  ds.CommercialRegions = dc.CommercialRegions
from dim_commercialview ds,emdtempocc4.csv_commercialview dc
where dc.UpperProdHier=ds.UpperProdHier 
  and dc.SoldToCountry= ds.SoldToCountry
  and ds.CommercialRegions <> dc.CommercialRegions ;

update dim_commercialview ds
set  ds.CommercialRegDesc = dc.CommercialRegDesc
from dim_commercialview ds,emdtempocc4.csv_commercialview dc
where dc.UpperProdHier=ds.UpperProdHier 
  and dc.SoldToCountry= ds.SoldToCountry
  and ds.CommercialRegDesc <> dc.CommercialRegDesc ;


update dim_commercialview ds
set  ds.CommSubRegion  = dc.CommSubRegion
from dim_commercialview ds,emdtempocc4.csv_commercialview dc
where dc.UpperProdHier=ds.UpperProdHier 
  and dc.SoldToCountry= ds.SoldToCountry
  and ds.CommSubRegion  <> dc.CommSubRegion ;

update dim_commercialview ds
set   ds.CommSubRegionDesc  = dc.CommSubRegionDesc
from dim_commercialview ds,emdtempocc4.csv_commercialview dc
where dc.UpperProdHier=ds.UpperProdHier 
  and dc.SoldToCountry= ds.SoldToCountry
  and  ds.CommSubRegionDesc  <> dc.CommSubRegionDesc;