/* ***********************************************************************************************************************************
   Script         - vw_bi_populate_metadata_fact.sql
   Author         - Alin Gheorghe
   Created ON     - May 2016
   Description    - Populate fact_metadata (APP-6215)

 ********************************************Change History**************************************************************************
  Date			    By			 Version		Desc
  16 May 2017		AlinGh		 1.0			Create the first version of the script
 *************************************************************************************************************************************/

drop table if exists tmp_fact_metadata;
create table tmp_fact_metadata
LIKE fact_metadata INCLUDING DEFAULTS INCLUDING IDENTITY;

/*Madalina 20170609 - bring the descriptions for the columns containing a formula, that do not already have a description coming from CustomMeasure */
update STG_FACT_METADATA 
set CUSTOMMEASUREWITHDESCS = FINALDESCRIPTION
from STG_FACT_METADATA stg
where (CUSTOMMEASUREWITHDESCS is null or lower(CUSTOMMEASUREWITHDESCS) like '%m:%')
and ifnull(CUSTOMMEASUREWITHDESCS,'Not Set') <> ifnull(FINALDESCRIPTION, 'Not Set');
/* Insert rows */

INSERT INTO tmp_fact_metadata(
fact_metadataid, 
dd_application, 
dd_subjectarea, 
dd_dimensionname,  
dd_relationshipname, 
dd_fieldname, 
dd_summary, 
dd_measuregroup, 
dd_measuretype, 
dd_businessdescription, 
dd_potentialusage, 
dd_example,
dd_sourceinformation,
dd_derivation,
dd_formulawithfopscolumns,
dd_type,
dd_displayformat,
dd_corporateorprivate,
dd_createdby,
dd_creationdate,
dd_lastupdateddate,
dd_lastupdatedby,
dd_custommeasure,
dd_CUSTOMMEASUREWITHDESCS,
DW_INSERT_DATE,
DW_UPDATE_DATE,
dd_flagbasefield,
dd_CONTAINSCUSTOM,
dd_LISTOFDEPENDENTFIELDS
-- ,
--dd_no_used_in_corporate_reports,
--dd_no_used_in_private_reports
)
SELECT
ifnull(FIELDID, 'Not Set'),
ifnull(APPLICATION, 'Not Set'),
ifnull(SUBJECTAREA, 'Not Set'),
ifnull(DIMENSIONNAME, 'Not Set'),
ifnull(RELATIONSHIPNAME, 'Not Set'),
ifnull(FIELDNAME, 'Not Set'),
ifnull(SUMMARY, 'Not Set'),
ifnull(MEASUREGROUP, 'Not Set'),
ifnull(MEASURETYPE, 'Not Set'),
ifnull(BUSINESSDESCRIPTION, 'Not Set'),
ifnull(POTENTIALUSAGE, 'Not Set'),
ifnull(EXAMPLE, 'Not Set'),
ifnull(SOURCEINFORMATION, 'Not Set'),
ifnull(DERIVATION, 'Not Set'),
ifnull(FORMULAWITHFOPSCOLUMNS,'Not Set'),
ifnull(TYPE, 'Not Set'),
ifnull(DISPLAYFORMAT, 'Not Set'),
ifnull(CORPORATE, 'Not Set'),
ifnull(CREATEDBY, 'Not Set'),
ifnull(CREATIONDATE, '0001-01-01 00:00:00'),
ifnull(LASTUPDATEDDATE, '0001-01-01 00:00:00'),
ifnull(LASTUPDATEDBY, 'Not Set'),
ifnull(CUSTOMMEASURE, 'Not Set'),
ifnull(CUSTOMMEASUREWITHDESCS, 'Not Set'),
current_timestamp,
current_timestamp,
ifnull(flagbasefield, 'Not Set'),
ifnull(CONTAINSCUSTOM, 'Not Set'),
ifnull(LISTOFDEPENDENTFIELDS, 'Not Set')
--,
--ifnull(dd_no_used_in_corporate_reports,'0'),
--ifnull(dd_no_used_in_private_reports,'0')
from STG_FACT_METADATA;

TRUNCATE TABLE fact_metadata;
INSERT INTO fact_metadata
SELECT * FROM tmp_fact_metadata;

DROP TABLE IF EXISTS tmp_fact_metadata;

merge into fact_metadata f_md
using
(
select distinct f_md.fact_metadataid, max(d.dim_dateid) as dim_dateid
from fact_metadata f_md, dim_date d
WHERE d.datevalue   = cast(substr(f_md.dd_creationdate,1,10) as date)
AND f_md.dim_dateidcreation <> d.dim_dateid
group by f_md.fact_metadataid
)t
on f_md.fact_metadataid = t.fact_metadataid
when matched then 
update set f_md.dim_dateidcreation = t.dim_dateid
where f_md.dim_dateidcreation <> t.dim_dateid;


merge into fact_metadata f_md
using
(
select distinct f_md.fact_metadataid, max(d.dim_dateid) as dim_dateid
from fact_metadata f_md, dim_date d
WHERE d.datevalue   = cast(substr(f_md.dd_lastupdateddate,1,10) as date)
AND f_md.dim_dateidlastupdated <> d.dim_dateid
group by f_md.fact_metadataid
)t
on f_md.fact_metadataid = t.fact_metadataid
when matched then 
update set f_md.dim_dateidlastupdated = t.dim_dateid
where f_md.dim_dateidlastupdated <> t.dim_dateid;

/*APP-9594 - Set Base/Merck requested field*/
update fact_metadata
set dd_flagbasefield = 'Customer requested' 
where dd_flagbasefield = 'Not Set';

/*APP-9734 - mark if a field is a custom measure*/
update fact_metadata
set DD_ISCUSTOMMEASURE = (case when instr(dd_formulawithfopscolumns, '*') <> 0 or 
			 instr(dd_formulawithfopscolumns, '/') <> 0 or  
			instr(dd_formulawithfopscolumns,'+') <> 0 or  
			instr(dd_formulawithfopscolumns,'-') <> 0 or
			instr(dd_formulawithfopscolumns,' ') <> 0 or
			instr(dd_formulawithfopscolumns,'(') <> 0 
			then 'Y' else 'N' end);

