--Create table for Lead Time Data calculation (RLT_DAYS,RLT_MONTHS,STDDEV_RLT_MONTH)

DROP TABLE IF EXISTS lead_time_data;

CREATE TABLE LEAD_TIME_DATA
(MATERIAL VARCHAR(20),
PLANT_EMD VARCHAR(100),
RLT_DAYS DECIMAL(18,4),
RLT_MONTHS DECIMAL(18,4),
STDDEV_RLT_MONTH DECIMAL(18,4));

INSERT INTO lead_time_data
(Material,
PLANT_EMD,
RLT_DAYS,
RLT_MONTHS,
STDDEV_RLT_MONTH)
  SELECT
  A.Global_material as Material,
  A.plant_emd,
  round(AVG(a.Lead_Time)) as rlt_days,
  round((AVG(a.Lead_Time))* 12/365,2)as RLT_MONTHS ,
   round(stddev_pop(a.Lead_Time_Months),3) as STDDEV_RLT_MONTH
  FROM
    (SELECT MDGPartNumber_NoLeadZero Global_material,
          pl.plant_emd Plant_EMD,
          lpd.DateValue - POC.DateValue Lead_Time,
          POC.DateValue PO_Create_Date,
          lpd.DateValue Last_Posting_Date,
   			  ROUND((lpd.DateValue - POC.DateValue)*12/365,1) as Lead_Time_Months
    FROM fact_s_curve AS f_s_curve
    INNER JOIN dim_mdg_part AS mdg_part ON f_s_curve.dim_mdg_partid = mdg_part.dim_mdg_partid
    INNER JOIN Dim_Plant AS pl ON f_s_curve.Dim_Plantid = pl.Dim_Plantid
    AND (lower(pl.plant_emd) = lower('6401 - Atsugi'))
    INNER JOIN dim_bwproducthierarchy AS bwuph ON f_s_curve.DIM_BWPRODUCTHIERARCHYID = bwuph.dim_bwproducthierarchyid
    AND (lower(bwuph.businesssectordesc) = loweR('Life Science'))
    INNER JOIN Dim_Part AS pt ON f_s_curve.Dim_Partid = pt.Dim_Partid
    AND ((pt.PACKINGSITE) IN (('Darmstadt'),('Temecula'),('MP_Molsheim')))
    INNER JOIN Dim_Date AS POC ON f_s_curve.DIM_PODATEIDCREATE = POC.Dim_Dateid
    INNER JOIN Dim_Date AS lpd ON f_s_curve.DIM_DATEIDLASTPOSTINGDATE = lpd.Dim_Dateid
    WHERE (lpd.DateValue - POC.DateValue >= 0
      AND lpd.DateValue - POC.DateValue <= 9000)
      AND (lower(f_s_curve.DD_INTERCOMPANYFLAG) = lower('Internal Vendor'))
      AND (lower(f_s_curve.DD_SHIPMENTSTATUS_FLAG) = lower('Received'))
      GROUP BY MDGPartNumber_NoLeadZero,
               pl.plant_emd,
               lpd.DateValue - POC.DateValue,
               TO_DATE(POC.DateValue, 'YYYY-MM-DD'),
               POC.DateValue,
               TO_DATE(lpd.DateValue, 'YYYY-MM-DD'),
               lpd.DateValue ) a
  WHERE LOWER(A.PLANT_EMD) = LOWER('6401 - ATSUGI')
  GROUP BY 1,2;


--Create table for Demand Data calculation (AVG_DEMAND_QTY_12M,AVG_DEMAND_OVER_LT,STANDARD_DEVIATION_DEMAND)

DROP TABLE IF EXISTS TMP_DEMAND_DATA ;

CREATE TABLE TMP_DEMAND_DATA AS
(SELECT MDGPartNumber_NoLeadZero Global_Material_Number,
        pl.plant_emd Plant_EMD,
        mlsrd.MonthYear Month_year,
        prt.PACKINGSITE Packing_site,
        prt.ABCDESC ABC_Description,
        prt.ABCIndicator ABC_Indicator,
        round(prt.SafetyStock) Safety_stock,
	      mdg_part.partdescription PartDescription,
        ROUND(SUM(f_so.ct_ScheduleQtySalesUnit), 2) Order_qty,
		prt.MTOMTS as mtomts
    FROM fact_salesorder AS f_so
    INNER JOIN dim_mdg_part AS mdg_part ON f_so.dim_mdg_partid = mdg_part.dim_mdg_partid
    INNER JOIN Dim_Plant AS pl ON f_so.Dim_Plantid = pl.Dim_Plantid
    AND (lower(pl.plant_emd) = lower('6401 - Atsugi'))
    INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
    AND ((prt.PACKINGSITE) IN (('Temecula'),('MP_Molsheim'),('Darmstadt')))
    INNER JOIN dim_bwproducthierarchy AS bw ON f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
    AND (lower(bw.businesssectordesc) = lower('LIFE SCIENCE'))
    INNER JOIN dim_date_factory_calendar AS mlsrd ON f_so.dim_mercklsconforreqdateid = mlsrd.dim_date_factory_calendarid
    AND (mlsrd.CalendarYear = 2018)
    INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
    AND (lower(sorr.Description) = lower('Not Set'))
    INNER JOIN dim_accountassignmentgroup AS aag ON f_so.Dim_AccountAssignmentGroupId = aag.dim_accountassignmentgroupid
    AND ((aag.AccountAssignmentGroup) NOT IN (('03'),('04'),('08')))
    INNER JOIN dim_Customer AS stpss ON f_so.DIM_CUSTOMERORGSOURCEID = stpss.dim_Customerid
    AND (lower(stpss.TradingPartner) = lower('Not Set'))
    INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
    AND ((dc.DocumentCategory) IN (('C'),('I'),('Physical Shipment of'),('Physical Receipt of '),('Billing / Adjustment'),('Billing/Adj with no '),('Billing/Adjustment L'),('Billing/Adjustment R'),('K'),('L')))
    INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
    INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
    WHERE ((f_so.dd_OrderStatus) IN (('CLOSED'),('OPEN')))
    GROUP BY MDGPartNumber_NoLeadZero,
             pl.plant_emd,
             mlsrd.MonthYear,
             prt.PACKINGSITE,
             prt.ABCDESC,
             prt.ABCIndicator,
             prt.SafetyStock,
	           mdg_part.partdescription,
             prt.MTOMTS);


DROP TABLE IF EXISTS DEMAND_DATA;

CREATE TABLE DEMAND_DATA AS
( SELECT
      x.Global_Material_Number as Material,
      x.plant_emd,
      x.partdescription,
      X.Packing_site,
      X.ABC_Description,
      X.ABC_Indicator,
      round(X.Safety_stock) as Safety_Stock,
      round(avg_order_qty,1) as AVG_DEMAND_QTY_12M,
      round((avg_order_qty * round(y.RLT_MONTHS,2)),1) as AVG_DEMAND_OVER_LT,
      round(x.stddev_order_qty,7) as STANDARD_DEVIATION_DEMAND,
	    x.MTOMTS

FROM
(select t.Global_Material_Number,t.plant_emd,t.partdescription,t.Packing_site, t.ABC_Description, t.ABC_Indicator,round(t.Safety_stock) as Safety_Stock,
        round(avg(t.Order_Qty),1) avg_order_qty, round(stddev_pop(Order_Qty),7) stddev_order_qty,t.MTOMTS from tmp_demand_data t, lead_time_data l
WHERE t.Global_Material_Number = l.material
and t.plant_emd = l.plant_emd
GROUP BY t.Global_Material_Number,
         t.plant_emd,
         t.partdescription,
         t.Packing_site,
         t.ABC_Description,
         t.ABC_Indicator,
         t.Safety_stock,
         t.MTOMTS
           )  x INNER JOIN lead_time_data y
on x.Global_Material_Number = y.material and x.plant_emd = y.plant_emd);


--create table for ABC/Volatility

DROP TABLE IF EXISTS fill_rate;

CREATE TABLE fill_rate (ABC varchar(60),
                        target_fill_rate decimal(18,4),
                        service_factor decimal(18,4));

INSERT INTO  fill_rate(ABC) values
('1 order'),('2 orders'),('3-5 orders'),('6-10 orders'),('>=11 orders'),('>=18 orders'),('>=32 orders'),('>=60 orders'),
('A Significance'),('AX'),('AY'),('AZ'),('B Significance'),('BX'),('BY'),('BZ'),('C Significance'),('CX'),('CY'),('CZ'),
('D Significance'),('Discontinued'),('E Significance'),('F Significance'),('Fairly important material according to the ABC analysis'),
('G Significance'),('H Significance'),('I Significance'),('J Significance'),('K Significance'),('L Significance'),('LS >=100 orders'),
('LS >=180 orders'),('LS >=32 orders'),('LS >=360 orders'),('LS >=60 orders'),('Less important material according to the ABC analysis'),
('Low Signific /LS >=100 orders'),('Manual Stocking policy'),('Medium Signific/LS >=180 order'),('N Significance'),('No sales'),
('No stock regardless of sales'),('Not Set'),('Significant / LS >=360 orders'),('U Significance'),('V Significance'),
('Very important material according to the ABC analysis'),('X Significance'),('Y Significance'),('Z Significance');

update fill_rate set target_fill_rate ='90' where abc = '2 orders';
update fill_rate set target_fill_rate ='90' where abc = '3-5 orders';
update fill_rate set target_fill_rate ='90' where abc = '6-10 orders';
update fill_rate set target_fill_rate ='90' where abc = '>=11 orders';
update fill_rate set target_fill_rate ='90' where abc = '>=18 orders';
update fill_rate set target_fill_rate ='90' where abc = '>=32 orders';
update fill_rate set target_fill_rate ='90' where abc = '>=60 orders';
update fill_rate set target_fill_rate ='97' where abc = 'A Significance';
update fill_rate set target_fill_rate ='97' where abc = 'AY';
update fill_rate set target_fill_rate ='90' where abc = 'AZ';
update fill_rate set target_fill_rate ='95' where abc = 'B Significance';
update fill_rate set target_fill_rate ='97' where abc = 'BX';
update fill_rate set target_fill_rate ='97' where abc = 'BY';
update fill_rate set target_fill_rate ='90' where abc = 'BZ';
update fill_rate set target_fill_rate ='90' where abc = 'C Significance';
update fill_rate set target_fill_rate ='90' where abc = 'CX';
update fill_rate set target_fill_rate ='90' where abc = 'CY';
update fill_rate set target_fill_rate ='90' where abc = 'CZ';
update fill_rate set target_fill_rate ='90' where abc = 'D Significance';
update fill_rate set target_fill_rate ='90' where abc = 'Discontinued';
update fill_rate set target_fill_rate ='90' where abc = 'E Significance';
update fill_rate set target_fill_rate ='90' where abc = 'F Significance';
update fill_rate set target_fill_rate ='95' where abc = 'Fairly important material according to the ABC analysis';
update fill_rate set target_fill_rate ='90' where abc = 'G Significance';
update fill_rate set target_fill_rate ='90' where abc = 'H Significance';
update fill_rate set target_fill_rate ='90' where abc = 'I Significance';
update fill_rate set target_fill_rate ='90' where abc = 'J Significance';
update fill_rate set target_fill_rate ='90' where abc = 'K Significance';
update fill_rate set target_fill_rate ='90' where abc = 'L Significance';
update fill_rate set target_fill_rate ='95' where abc = 'LS >=100 orders';
update fill_rate set target_fill_rate ='95' where abc = 'LS >=180 orders';
update fill_rate set target_fill_rate ='90' where abc = 'LS >=32 orders';
update fill_rate set target_fill_rate ='95' where abc = 'LS >=360 orders';
update fill_rate set target_fill_rate ='95' where abc = 'LS >=60 orders';
update fill_rate set target_fill_rate ='90' where abc = 'Less important material according to the ABC analysis';
update fill_rate set target_fill_rate ='90' where abc = 'Low Signific /LS >=100 orders';
update fill_rate set target_fill_rate ='90' where abc = 'Manual Stocking policy';
update fill_rate set target_fill_rate ='95' where abc = 'Medium Signific/LS >=180 order';
update fill_rate set target_fill_rate ='90' where abc =  'N Significance';
update fill_rate set target_fill_rate ='90' where abc =  'No sales';
update fill_rate set target_fill_rate ='90' where abc =  'No stock regardless of sales';
update fill_rate set target_fill_rate ='90' where abc =  'Not Set';
update fill_rate set target_fill_rate ='97' where abc =  'Significant / LS >=360 orders';
update fill_rate set target_fill_rate ='90' where abc =  'U Significance';
update fill_rate set target_fill_rate ='90' where abc =  'V Significance';
update fill_rate set target_fill_rate ='97' where abc =  'Very important material according to the ABC analysis';
update fill_rate set target_fill_rate ='97' where abc =  'X Significance';
update fill_rate set target_fill_rate ='95' where abc =  'Y Significance';
update fill_rate set target_fill_rate ='90' where abc =  'Z Significance';
update fill_rate set target_fill_rate ='90' where abc =  '1 order';
update fill_rate set target_fill_rate ='99' where abc =  'AX';
update fill_rate set service_factor = '1.28' where target_fill_rate ='90';
update fill_rate set service_factor = '2.33' where target_fill_rate ='99';
update fill_rate set service_factor = '1.88' where target_fill_rate ='97';
update fill_rate set service_factor = '1.64' where target_fill_rate ='95';


--Create table for safety stock in units

DROP TABLE IF EXISTS safety_stock_indicators;

CREATE TABLE safety_stock_indicators as
(select
  x.A_Indicator,
  x.B_indicator,
  x.material,
  x.plant_emd,
 (f.service_factor*sqrt(x.A_Indicator+x.B_Indicator)) as SAFETYSTOCK_IN_UNITS
FROM
( SELECT d.abc_description,l.material as material, l.plant_emd as plant_emd,
    round((l.RLT_MONTHS *power(d.STANDARD_DEVIATION_DEMAND,2)),2) as A_Indicator,
    round((power(l.STDDEV_RLT_MONTH*d.AVG_DEMAND_OVER_LT,2)),2) as B_indicator
    FROM
    lead_time_data l,
    demand_data d
    WHERE l.material=d.material) x,
fill_rate f
WHERE x.abc_description= f.abc
GROUP BY
x.A_Indicator,
x.B_indicator,
x.material,
x.plant_emd,
f.service_factor);

--Create table for standard unit price

DROP TABLE IF EXISTS StdUnitPrice;

CREATE TABLE StdUnitPrice AS SELECT * FROM
    (SELECT MDGPartNumber_NoLeadZero  Global_Material_Number,
            pl.plant_emd Plant_EMD,
			      pl.Dim_Plantid Plantid,
			      mdg_part.dim_mdg_partid Mdg_partid,
            ROUND(AVG(f_invagng.amt_StdUnitPrice), 5) AVG_STD_UnitPrice
     FROM fact_inventoryaging AS f_invagng
     INNER JOIN Dim_Plant AS pl ON f_invagng.Dim_Plantid = pl.Dim_Plantid
     AND (lower(pl.plant_emd) = lower('6401 - Atsugi'))
     INNER JOIN dim_mdg_part AS mdg_part ON f_invagng.dim_mdg_partid = mdg_part.dim_mdg_partid
     GROUP BY MDGPartNumber_NoLeadZero,
              pl.plant_emd,
			        pl.Dim_Plantid,
			        mdg_part.dim_mdg_partid );


-- Create table ITEM_QUANTITY for the shipping type description

DROP TABLE IF EXISTS ITEM_QUANTITY;

CREATE TABLE ITEM_QUANTITY AS
      (SELECT MDGPartNumber_NoLeadZero,
                   mdg_part.PARTDESCRIPTION,
                   pl.plant_emd,
                   pt.PACKINGSITE,
                   f_s_curve.DD_SHIPPINGTYPEDESCRIPTION as SHIPPINGTYPEDESCRIPTION,
                   ROUND(avg((lpd.DateValue) - (POC.DateValue)), 0) Lead_time,
                   ROUND(AVG(ct_ItemQty), 2) ItemnQty
            FROM fact_s_curve AS f_s_curve
            INNER JOIN Dim_Plant AS pl ON f_s_curve.Dim_Plantid = pl.Dim_Plantid
            AND (lower(pl.plant_emd) = lower('6401 - Atsugi'))
            INNER JOIN Dim_Part AS pt ON f_s_curve.Dim_Partid = pt.Dim_Partid
            AND ((pt.PACKINGSITE) IN (('Darmstadt'),('Temecula'),('MP_Molsheim')))
            INNER JOIN dim_mdg_part AS mdg_part ON f_s_curve.dim_mdg_partid = mdg_part.dim_mdg_partid
            INNER JOIN Dim_Date AS POC ON f_s_curve.DIM_PODATEIDCREATE = POC.Dim_Dateid
            INNER JOIN Dim_Date AS lpd ON f_s_curve.DIM_DATEIDLASTPOSTINGDATE = lpd.Dim_Dateid
            GROUP BY MDGPartNumber_NoLeadZero,
                     mdg_part.PARTDESCRIPTION,
                     pl.plant_emd,
                     pt.PACKINGSITE,
                     f_s_curve.DD_SHIPPINGTYPEDESCRIPTION);

--create table MERCK_RATE for Merck_LS_Fill_Rate
DROP TABLE IF EXISTS MERCK_RATE;

create table merck_rate as (SELECT MDGPartNumber_NoLeadZero Material,
                   mdg_part.PARTDESCRIPTION,
                   pl.plant_emd,
                   prt.PACKINGSITE,
                   prt.ABCDESC,
                   ROUND(CASE
                             WHEN (SUM(CASE
                                           WHEN DD_OPENQTYCVRDBYPLANT IN ('X') THEN 1
                                           ELSE 0
                                       END) > COUNT(DISTINCT dd_salesdocno || dd_salesitemno)) THEN 100
                             ELSE SUM(CASE
                                          WHEN DD_OPENQTYCVRDBYPLANT IN ('X') THEN 1
                                          ELSE 0
                                      END) / COUNT(DISTINCT dd_salesdocno || dd_salesitemno) * 100
                         END, 4) Merck_LS_Fill_Rate
            FROM fact_fillrate_snapshot AS ffs
            INNER JOIN dim_mdg_part AS mdg_part ON ffs.dim_mdg_partid = mdg_part.dim_mdg_partid
            INNER JOIN Dim_Plant AS pl ON ffs.Dim_Plantid = pl.Dim_Plantid
            AND (lower(pl.plant_emd) = lower('6401 - Atsugi'))
            INNER JOIN Dim_Part AS prt ON ffs.Dim_Partid = prt.Dim_Partid
            GROUP BY MDGPartNumber_NoLeadZero,
                     mdg_part.PARTDESCRIPTION,
                     pl.plant_emd,
                     prt.PACKINGSITE,
                     prt.ABCDESC);

--Create final table for safety stock calculation

DROP TABLE IF EXISTS safety_stock_calculation ;

CREATE TABLE safety_stock_calculation as
  (SELECT
  	     distinct l.material,
         l.plant_emd,
         round(l.RLT_DAYS,2) as RLT_DAYS,
  	     round(l.RLT_MONTHS,2) as RLT_MONTHS,
         round(l.STDDEV_RLT_MONTH,2) as STDDEV_RLT_MONTH,
  	     round(d.AVG_DEMAND_QTY_12M,2) AS AVG_DEMAND_QTY_12M,
         round(d.AVG_DEMAND_OVER_LT,2) as AVG_DEMAND_OVER_LT,
         round(d.STANDARD_DEVIATION_DEMAND,2) as STANDARD_DEVIATION_DEMAND,
  	     d.packing_site,
  	     d.partdescription,
  	     f.ABC,
         round(f.target_fill_rate,2) as target_fill_rate,
         round(f.service_Factor,2) as service_Factor,
         round(x.A_Indicator,2) as A_Indicator,
         round(x.B_indicator,2) as B_indicator,
         round(x.SAFETYSTOCK_IN_UNITS,2) as SAFETYSTOCK_IN_UNITS,
         d.safety_stock as current_safety_stock,
         round((case when d.safety_stock <> 0 then (x.SAFETYSTOCK_IN_UNITS-d.safety_stock)/d.safety_stock*100 else null end),2)  as variation,
         round(s.AVG_STD_UnitPrice,2) as AVG_STD_UnitPrice,
         round((s.AVG_STD_UnitPrice*(x.SAFETYSTOCK_IN_UNITS-d.safety_stock))) as inv_impact,
         case when x.safetystock_in_units > 1.2*d.safety_stock then 'T' else 'F' end as safety_stock_flag,
         d.MTOMTS
   FROM lead_time_data l
   INNER JOIN  demand_data d
   on l.material = d.material
   and l.plant_emd =d.plant_emd
   INNER JOIN fill_rate f
   ON d.abc_description = f.ABC
   INNER JOIN safety_stock_indicators x
   ON  l.material=x.material
   INNER JOIN StdUnitPrice s
   ON l.material=s.global_material_number
   GROUP BY l.material,
           l.plant_emd,
           l.RLT_DAYS,
  	       l.RLT_MONTHS,
           l.STDDEV_RLT_MONTH,
  	       d.AVG_DEMAND_QTY_12M,
           d.AVG_DEMAND_OVER_LT,
           d.STANDARD_DEVIATION_DEMAND,
  	       d.packing_site,
  	       d.partdescription,
  	       f.ABC,
           f.target_fill_rate,
           f.service_Factor,
           x.A_Indicator,
           x.B_indicator,
           x.SAFETYSTOCK_IN_UNITS,
           d.safety_stock,
  	       s.AVG_STD_UnitPrice,
           d.MTOMTS);

ALTER TABLE safety_stock_calculation ADD COLUMN sales_distribution VARCHAR(30);
ALTER TABLE safety_stock_calculation ADD COLUMN leadtime_distribution VARCHAR(30);


UPDATE  safety_stock_calculation
SET sales_distribution  = f.dd_sales_distribution
FROM  FACT_SAFETYSTOCK_LS_OUTPUTFROMDS f INNER JOIN safety_stock_calculation s ON
f.dd_productapo = s.material AND f.dd_locationapo = s.PLANT_EMD;

update  safety_stock_calculation
set leadtime_distribution  = f.dd_leadtime_distribution
from  FACT_SAFETYSTOCK_LS_OUTPUTFROMDS f inner join safety_stock_calculation s on
f.dd_productapo = s.material and f.dd_locationapo = s.PLANT_EMD;

ALTER TABLE safety_stock_calculation ADD COLUMN merck_ls_fill_rate  DECIMAL(18,4);

UPDATE safety_stock_calculation
SET merck_ls_fill_rate  = m.Merck_LS_Fill_Rate
FROM  MERCK_RATE m LEFT JOIN safety_stock_calculation s ON
m.MATERIAL = s.material AND m.PLANT_EMD = s.PLANT_EMD;

UPDATE  safety_stock_calculation
SET merck_ls_fill_rate  = ifnull( merck_ls_fill_rate,99999999999)
WHERE  merck_ls_fill_rate IS NULL;

ALTER TABLE  safety_stock_calculation add column mode_of_transport VARCHAR(30);

MERGE INTO safety_stock_calculation s USING
( SELECT DISTINCT material,PLANT_EMD,SHIPPINGTYPEDESCRIPTION FROM
(SELECT s.material,i.PLANT_EMD,i.MDGPartNumber_NoLeadZero, i.SHIPPINGTYPEDESCRIPTION,
row_number() over ( PARTITION BY i.PLANT_EMD,i.MDGPartNumber_NoLeadZero ORDER BY itemnqty DESC) AS row_id FROM
item_quantity i INNER JOIN safety_stock_calculation s
ON
ifnull(i.MDGPartNumber_NoLeadZero,'Not Set') = ifnull(s.material,'Not Set')
AND ifnull(i.PLANT_EMD,'Not Set') = ifnull(s.PLANT_EMD ,'Not Set')
)t WHERE t.row_id = 1)x ON x.material = s.material AND x.plant_emd = s.plant_emd WHEN matched THEN
UPDATE SET mode_of_transport  = x.SHIPPINGTYPEDESCRIPTION;

UPDATE safety_stock_calculation
SET safetystock_in_units = m.ct_safetystock
FROM safety_stock_calculation s
 INNER JOIN FACT_SAFETYSTOCK_LS_OUTPUTFROMDS m
ON  s.material = m.dd_productapo AND s.plant_emd = m.dd_locationapo WHERE s.sales_distribution <> 'norm' AND s.leadtime_distribution <> 'norm' ;

UPDATE safety_stock_calculation
SET inv_impact = round((s.AVG_STD_UnitPrice*(s.SAFETYSTOCK_IN_UNITS-s.current_safety_stock)))
FROM safety_stock_calculation s
WHERE inv_impact <> round((s.AVG_STD_UnitPrice*(s.SAFETYSTOCK_IN_UNITS-s.current_safety_stock)),0)
AND s.sales_distribution <> 'norm' AND s.leadtime_distribution <> 'norm' ;
