
--create table backorder_tranding_monthly for Snapshot_Date_Month_Year and Total_Open_Order_AmtLS

DROP TABLE IF EXISTS backorder_tranding_monthly;

CREATE TABLE backorder_tranding_monthly AS
(SELECT snpsd.MonthYear  Snapshot_Date_Month_Year,
        pl.plant_emd ,
        MDGPartNumber_NoLeadZero Material,
        ROUND(SUM(f_sosnp.amt_ExchangeRate_GBL * ((CASE
                                                  WHEN (bw.businessdesc) = 'Applied Solutions' THEN (CASE
                                                                                                         WHEN f_sosnp.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
                                                                                                         WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC')
                                                                                                              AND f_sosnp.dd_ItemRelForDelv = 'X' THEN (CASE
                                                                                                                                                            WHEN (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived)) < 0 THEN 0.0000
                                                                                                                                                            ELSE (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived))
                                                                                                                                                        END * f_sosnp.amt_UnitPriceUoM/(CASE
                                                                                                                                                                                            WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit
                                                                                                                                                                                            ELSE 1
                                                                                                                                                                                        END))
                                                                                                         ELSE (CASE
                                                                                                                   WHEN (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
                                                                                                                   ELSE (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty)
                                                                                                               END * f_sosnp.amt_UnitPriceUoM/(CASE
                                                                                                                                                   WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit
                                                                                                                                                   ELSE 1
                                                                                                                                               END))
                                                                                                     END)
                                                  ELSE 0
                                              END) + (CASE
                                                          WHEN (bw.businessdesc) = 'Research Solutions' THEN (CASE
                                                                                                                  WHEN f_sosnp.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
                                                                                                                  WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC')
                                                                                                                       AND f_sosnp.dd_ItemRelForDelv = 'X' THEN (CASE
                                                                                                                                                                     WHEN (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived)) < 0 THEN 0.0000
                                                                                                                                                                     ELSE (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived))
                                                                                                                                                                 END * f_sosnp.amt_UnitPriceUoM/(CASE
                                                                                                                                                                                                     WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit
                                                                                                                                                                                                     ELSE 1
                                                                                                                                                                                                 END))
                                                                                                                  ELSE (CASE
                                                                                                                            WHEN (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
                                                                                                                            ELSE (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty)
                                                                                                                        END * f_sosnp.amt_UnitPriceUoM/(CASE
                                                                                                                                                            WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit
                                                                                                                                                            ELSE 1
                                                                                                                                                        END))
                                                                                                              END)
                                                          ELSE 0
                                                      END) + (CASE
                                                                  WHEN (bw.businessdesc) = 'Process Solutions' THEN (CASE
                                                                                                                         WHEN f_sosnp.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
                                                                                                                         WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC')
                                                                                                                              AND f_sosnp.dd_ItemRelForDelv = 'X' THEN (CASE
                                                                                                                                                                            WHEN (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived)) < 0 THEN 0.0000
                                                                                                                                                                            ELSE (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived))
                                                                                                                                                                        END * f_sosnp.amt_UnitPriceUoM/(CASE
                                                                                                                                                                                                            WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit
                                                                                                                                                                                                            ELSE 1
                                                                                                                                                                                                        END))
                                                                                                                         ELSE (CASE
                                                                                                                                   WHEN (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
                                                                                                                                   ELSE (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty)
                                                                                                                               END * f_sosnp.amt_UnitPriceUoM/(CASE
                                                                                                                                                                   WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit
                                                                                                                                                                   ELSE 1
                                                                                                                                                               END))
                                                                                                                     END)
                                                                  ELSE 0
                                                              END))), 0) Total_Open_Order_AmtLS
        FROM fact_salesorder_snapshot AS f_sosnp
        INNER JOIN Dim_Date AS snpsd ON f_sosnp.DIM_DATEIDSNAPSHOT = snpsd.Dim_Dateid
        AND ((snpsd.DateValue) BETWEEN (LAST_DAY(to_date('30 May 2019',  'dd Mon YYYY') - INTERVAL '13' MONTH) + INTERVAL '1' DAY) AND (LAST_DAY(to_date('30 May 2019', 'dd Mon YYYY'))- INTERVAL '1' MONTH))
        AND (snpsd.DateValue = snpsd.MonthEndDate)
        INNER JOIN Dim_Plant AS pl ON f_sosnp.Dim_Plantid = pl.Dim_Plantid
        AND (lower(pl.plant_emd) = lower('6401 - Atsugi'))
        INNER JOIN dim_mdg_part AS mdg_part ON f_sosnp.dim_mdg_partid = mdg_part.dim_mdg_partid
        INNER JOIN dim_bwproducthierarchy AS bw ON f_sosnp.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
        AND (lower(bw.businessdesc) != lower('CO Life Science'))
        AND (lower(bw.businesssectordesc) = lower('LIFE SCIENCE'))
        INNER JOIN dim_salesdocumenttype AS sdt ON f_sosnp.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
        WHERE (lower(f_sosnp.DD_INTERCOMPFLAG) = lower('3rd Party'))
          AND (lower(f_sosnp.DD_BIFILTERS_PRE_PRD) = lower('X'))
          AND (lower(f_sosnp.dd_ls_Blocked_BackOrder) = lower('BackOrder'))
        GROUP BY snpsd.MonthYear,
                 snpsd.CalendarMonthID,
                 pl.plant_emd,
                 MDGPartNumber_NoLeadZero);


--CREATE TABLE MERCK_FILL_RATE for Merck_LS_Fill_Rate

DROP TABLE IF EXISTS MERCK_FILL_RATE;

CREATE TABLE MERCK_FILL_RATE As (SELECT MDGPartNumber_NoLeadZero Material,
                   pl.plant_emd,
                   ds.MonthYear Snapshot_Date_Month_Year,
                   prt.ABCDESC,
                   ROUND(CASE
                             WHEN (SUM(CASE
                                           WHEN DD_OPENQTYCVRDBYPLANT IN ('X') THEN 1
                                           ELSE 0
                                       END) > COUNT(DISTINCT dd_salesdocno || dd_salesitemno)) THEN 100
                             ELSE SUM(CASE
                                          WHEN DD_OPENQTYCVRDBYPLANT IN ('X') THEN 1
                                          ELSE 0
                                      END) / COUNT(DISTINCT dd_salesdocno || dd_salesitemno) * 100
                         END, 4) Merck_LS_Fill_Rate,
                   ds.CalendarMonthID
            FROM fact_fillrate_snapshot AS ffs
            INNER JOIN Dim_Date AS ds ON ffs.DIM_DATEIDSNAPSHOT = ds.Dim_Dateid
            AND ((ds.DateValue) BETWEEN (LAST_DAY(to_date('11 Jun 2019', 'dd Mon YYYY') - INTERVAL '7' MONTH) + INTERVAL '1' DAY) AND (LAST_DAY(to_date('11 Jun 2019', 'dd Mon YYYY'))- INTERVAL '1' MONTH))
            INNER JOIN dim_mdg_part AS mdg_part ON ffs.dim_mdg_partid = mdg_part.dim_mdg_partid
            INNER JOIN Dim_Plant AS pl ON ffs.Dim_Plantid = pl.Dim_Plantid
            AND (lower(pl.plant_emd) = lower('6401 - Atsugi'))
            INNER JOIN dim_bwproducthierarchy AS bw ON ffs.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
            AND (lower(bw.businessdesc) != lower('CO Life Science'))
            AND (lower(bw.businesssectordesc) = lower('LIFE SCIENCE'))
            AND (lower(bw.BUSINESSNAME_PS) != lower('Not Set'))
            INNER JOIN Dim_Part AS prt ON ffs.Dim_Partid = prt.Dim_Partid
            WHERE (lower(ffs.DD_INTERCOMPFLAG) = lower('3rd Party'))
              AND (lower(ffs.DD_BIFILTERS_PRE_PRD_NEW) = lower('X'))
            GROUP BY MDGPartNumber_NoLeadZero,
                     pl.plant_emd,
                     ds.MonthYear,
                     ds.CalendarMonthID,
                     prt.ABCDESC);


--create final table backorder_tranding

DROP TABLE IF EXISTS backorder_tranding;

CREATE TABLE backorder_tranding as
   (select b.Material,
           b.plant_emd,
           b.Snapshot_Date_Month_Year,
				   b.Total_Open_Order_AmtLS,
 				   m.Merck_LS_Fill_Rate,
				   m.Abcdesc
   FROM backorder_tranding_monthly b
   INNER JOIN MERCK_FILL_RATE m on b.Material=m.Material
   AND  b.plant_emd= m.plant_emd
   AND  b.Snapshot_Date_Month_Year = m.Snapshot_Date_Month_Year);

ALTER TABLE backorder_tranding add column target_fill_rate DECIMAL(18,4);

UPDATE backorder_tranding b
SET b.target_fill_rate = f.target_fill_rate
FROM backorder_tranding b,  fill_rate f
WHERE b.Abcdesc = f.abc;
