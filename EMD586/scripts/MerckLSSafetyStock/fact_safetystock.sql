DROP TABLE IF EXISTS FACT_SAFETYSTOCK;

CREATE TABLE FACT_SAFETYSTOCK (
    FACT_SAFETYSTOCKID           DECIMAL(36,0) PRIMARY KEY,
    DD_MATERIAL                  VARCHAR(20) DEFAULT 'Not Set',
    DD_PLANT_EMD                 VARCHAR(100) DEFAULT 'Not Set',
    CT_RLT_DAYS                  DECIMAL(18,4)DEFAULT 0,
    CT_RLT_MONTHS                DECIMAL(18,4)DEFAULT 0,
    CT_STDDEV_RLT_MONTH          DECIMAL(18,4)DEFAULT 0,
    CT_AVG_DEMAND_QTY_12M        DECIMAL(18,4)DEFAULT 0,
    CT_AVG_DEMAND_OVER_LT        DECIMAL(18,4)DEFAULT 0,
    CT_STANDARD_DEVIATION_DEMAND DECIMAL(18,4)DEFAULT 0,
    DD_PACKING_SITE              VARCHAR(40) DEFAULT 'Not Set',
    DD_PARTDESCRIPTION           VARCHAR(50) DEFAULT 'Not Set',
    DD_ABC                       VARCHAR(60) DEFAULT 'Not Set',
    CT_TARGET_FILL_RATE          DECIMAL(18,4)DEFAULT 0,
    CT_SERVICE_FACTOR            DECIMAL(18,4) DEFAULT 0,
    CT_A_INDICATOR               DECIMAL(18,4) DEFAULT 0,
    CT_B_INDICATOR               DECIMAL(18,4) DEFAULT 0,
    CT_SAFETYSTOCK_IN_UNITS      DECIMAL(18,4) DEFAULT 0,
    CT_CURRENT_SAFETY_STOCK      DECIMAL(18,4) DEFAULT 0,
    CT_VARIATION                 DECIMAL(18,4) DEFAULT 0,
    CT_AVG_STD_UNITPRICE         DECIMAL(18,4) DEFAULT 0,
    CT_INV_IMPACT                DECIMAL(18,4) DEFAULT 0,
    DD_SAFETY_STOCK_FLAG         VARCHAR(7) DEFAULT 'Not Set',
    DD_MODE_OF_TRANSPORT         VARCHAR(20) DEFAULT 'Not Set',
    DD_MTOMTS                    VARCHAR(20) DEFAULT 'Not Set',
    DW_INSERT_DATE               TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    DW_UPDATE_DATE               TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

TRUNCATE TABLE fact_safetystock;

DROP TABLE IF EXISTS  number_fountain_safetystock;

CREATE TABLE number_fountain_safetystock (
    TABLE_NAME VARCHAR(40) NOT NULL ENABLE,
    MAX_ID     DECIMAL(36,0) DEFAULT 0 NOT NULL ENABLE
);

delete from number_fountain_safetystock s where s.table_name = 'FACT_SAFETYSTOCK';

insert into number_fountain_safetystock
select 'FACT_SAFETYSTOCK',
ifnull(max(f.fact_safetystockid ),1)
from fact_safetystock f;

INSERT INTO FACT_SAFETYSTOCK
(   FACT_SAFETYSTOCKID,
    DD_MATERIAL,
    DD_PLANT_EMD,
    CT_RLT_DAYS,
    CT_RLT_MONTHS,
    CT_STDDEV_RLT_MONTH,
    CT_AVG_DEMAND_QTY_12M,
    CT_AVG_DEMAND_OVER_LT,
    CT_STANDARD_DEVIATION_DEMAND,
    DD_PACKING_SITE,
    DD_PARTDESCRIPTION,
    DD_ABC,
    CT_TARGET_FILL_RATE,
    CT_SERVICE_FACTOR,
    CT_A_INDICATOR,
    CT_B_INDICATOR,
    CT_SAFETYSTOCK_IN_UNITS,
    CT_CURRENT_SAFETY_STOCK,
    CT_VARIATION,
    CT_AVG_STD_UNITPRICE,
    CT_INV_IMPACT,
    DD_SAFETY_STOCK_FLAG,
    DD_MODE_OF_TRANSPORT,
    DD_MTOMTS
)

SELECT
(SELECT max_id FROM number_fountain_safetystock
        WHERE table_name = 'FACT_SAFETYSTOCK') + row_number() over(order by '') FACT_SAFETYSTOCKID,
    IFNULL(SS.MATERIAL,'Not Set'),
    IFNULL(SS.PLANT_EMD,'Not Set'),
    IFNULL(SS.RLT_DAYS,0),
    IFNULL(SS.RLT_MONTHS,0),
    IFNULL(SS.STDDEV_RLT_MONTH,0),
    IFNULL(SS.AVG_DEMAND_QTY_12M,0),
    IFNULL(SS.AVG_DEMAND_OVER_LT,0),
    IFNULL(SS.STANDARD_DEVIATION_DEMAND,0),
    IFNULL(SS.PACKING_SITE,'Not Set'),
    IFNULL(SS.PARTDESCRIPTION,'Not Set'),
    IFNULL(SS.ABC,'Not Set'),
    IFNULL(SS.TARGET_FILL_RATE,0),
    IFNULL(SS.SERVICE_FACTOR,0),
    IFNULL(SS.A_INDICATOR,0),
    IFNULL(SS.B_INDICATOR,0),
    IFNULL(SS.SAFETYSTOCK_IN_UNITS,0),
    IFNULL(SS.CURRENT_SAFETY_STOCK,0),
    IFNULL(SS.VARIATION,0),
    IFNULL(SS.AVG_STD_UNITPRICE,0),
    IFNULL(SS.INV_IMPACT,0),
    IFNULL(SS.SAFETY_STOCK_FLAG,'Not Set'),
    IFNULL(SS.MODE_OF_TRANSPORT,'Not Set'),
    IFNULL(SS.MTOMTS,'Not Set')
FROM SAFETY_STOCK_CALCULATION SS;

alter table FACT_SAFETYSTOCK add column ct_merck_ls_fill_rate  DECIMAL(18,4);

update FACT_SAFETYSTOCK
set ct_merck_ls_fill_rate  = m.Merck_LS_Fill_Rate
from  MERCK_RATE m left join fact_safetystock s on
m.MATERIAL = s.dd_material and m.PLANT_EMD = s.dd_PLANT_EMD;

update FACT_SAFETYSTOCK
set ct_merck_ls_fill_rate  = ifnull( ct_merck_ls_fill_rate ,99999999999)
where  ct_merck_ls_fill_rate is null;

alter table fact_safetystock add column dd_sales_distribution varchar(30) default 'Not Set';
alter table fact_safetystock add column dd_leadtime_distribution varchar(30) default 'Not Set';


update  fact_safetystock
set dd_sales_distribution  = f.dd_sales_distribution
from  FACT_SAFETYSTOCK_LS_OUTPUTFROMDS f inner join fact_safetystock  s on
f.dd_productapo = s.dd_material and f.dd_locationapo = s.dd_PLANT_EMD;

update  fact_safetystock
set dd_leadtime_distribution  = f.dd_leadtime_distribution
from  FACT_SAFETYSTOCK_LS_OUTPUTFROMDS f inner join fact_safetystock  s on
f.dd_productapo = s.dd_material and f.dd_locationapo = s.dd_PLANT_EMD;
