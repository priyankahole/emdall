
# coding: utf-8

# In[16]:
import sys
sys.path.append('/opt/anaconda2/lib/python2.7/site-packages/')
import aera_utilities as aera_u
import pandas as pd
import re

# In[8]:

projectid = sys.argv[1] #'0797FF95_4733_4F4A_880B_F474204228A1'
mysqlUAT_host = sys.argv[2] # 'uat.culqhtllksrc.us-east-1.rds.amazonaws.com'
mysqlCWB_host = sys.argv[3] #'uat-cwb.culqhtllksrc.us-east-1.rds.amazonaws.com'
mysqlCWB_shema = sys.argv[4] #'reckitt7b8'
table_source = sys.argv[5]
table_destination = sys.argv[6]
col_Sync = sys.argv[7]
direction = sys.argv[8]
allCommonCols =  sys.argv[9]
load_Type = sys.argv[10]
insertDateColumn = sys.argv[11]

# In[114]:
             
 

aera_u.cwb_import_data(projectid = projectid, mysqlUAT_host = mysqlUAT_host, mysqlCWB_host= mysqlCWB_host ,mysqlCWB_shema = mysqlCWB_shema, col_Sync = col_Sync, 
                table_source = table_source, 
                table_destination =  table_destination , direction = direction, 
                allCommonCols = allCommonCols, load_Type = load_Type, insertDateColumn = insertDateColumn)



