
# coding: utf-8

# In[2]:

# import packages
import MySQLdb
import pandas as pd
import exasol as e
import numpy as np
import re
import sys
sys.path.append('/opt/anaconda2/lib/python2.7/site-packages/')
import aera_utilities as aera_u
import json


# In[3]:

# establish the connection with MySQL
# user_mysql = "support"
# passwd_mysql = "fusion12"
# host_mysql = "192.168.100.11"

#user_mysql = "octet"
#passwd_mysql = "insightqa"
#host_mysql = "uat.culqhtllksrc.us-east-1.rds.amazonaws.com"

# user_mysql = sys.argv[1]
# passwd_mysql = sys.argv[2]
# host_mysql = sys.argv[3]


# con_mysql = MySQLdb.connect(host=host_mysql,
#                     user=user_mysql,
#                     passwd=passwd_mysql,db = 'fusionops')


# In[4]:

# using DB class instead of classic connection
con_mysql = aera_u.DBConnection("MySQL") # Select Vendor
con_mysql.select_schema("uat-irl01-15mar.cygphomlu5zp.eu-west-1.rds.amazonaws.com","fusionops")

# set the projectid
#projectid = '"26369A04_4AD1_46DC_9E1F_730710ED4962"'
#projectid = '"' + sys.argv[4] + '"'
projectid = '"0B778AE1_48B6_46D9_A6AA_5BDE5693F7FC"'

# In[ ]:




# ### Connect to Exasol

# In[5]:

# con = e.connect(dsn='exasolution-uo2214lv2_64', autocommit = True)

# try:
#     con.execute('OPEN SCHEMA MERCK')
# except:
#     print "ERROR IN OPEN SCHEMA"

con = aera_u.DBConnection('Exasol')
con.select_schema('exasolution-uo2214lv2_64', 'EMD586')


# ## Load MySQL main data - unprocessed

# In[6]:

# select metadata info for both measures and attributes
metadata = pd.read_sql("select * from     ((select             \"Measure\" as Type,             f.`FactName` \"SubjectArea\",             ifnull(sa.name,'Other') as \"Application\",             'Not Set' as \"DimensionName\",                 'Not Set' as \"RelationshipName\",             fm.`DisplayName` \"FieldName\",             fm.Description \"Summary\",             fm.MeasureGroup    as \"MeasureGroup\",             fm.MeasureType      as \"MeasureType\",             fm.BusinessDescriptionLong as \"BusinessDescription\",             fm.PotentialUsage as \"PotentialUsage\",             fm.Examples as \"Example\",             fm.SourceReference as \"SourceInformation\",             fm.TechnicalDescription as \"Derivation\",             fm.ColumnName as \"FormulawithFOPScolumns\",             fm.displayformat as \"DisplayFormat\",             CASE WHEN fm.corporate = 'Y' THEN 'Corporate' ELSE 'Private' END as \"Corporate\",             (select concat(firstname,\" \",lastname) from usermain u where u.usermainid = fm.createdby) as \"CreatedBy\",             fm.`CreationDate` as \"CreationDate\",             fm.`LastUpdatedDate` as \"LastUpdatedDate\",             (select concat(firstname,\" \",lastname) from usermain u where u.usermainid = fm.`LastUpdatedBy`) as \"LastUpdatedBy\",             fm.`md_factmeasureid` as \"FieldID\",             fm.`CustomMeasure` as \"CustomMeasure\",             f.`md_factid` as \"FactId\",             'Not Set' as \"JoinAlias\",             'Not Set' as \"FactDimRelID\",             'Not Set' as \"FactColumnName\",             fm.columnname as \"ColumnName\",             f.isdeleted as \"factIsdeleted\",             fm.isdeleted as \"MeasureIsdeleted\",             0 as \"factDimRelIsdeleted\",             0 as \"DimIsdeleted\",             0 as \"AttrIsdeleted\"     from md_factmeasure fm inner join md_fact f on fm.`MD_Factid` = f.md_factid and fm.`Projectid` = f.`Projectid`     left join md_subjectareafactrelation sfr     on f.md_factid = sfr.md_factid and f.projectid = sfr.projectid     left join md_subjectarea sa     on sa.md_subjectareaid = sfr.md_subjectareaid and sa.`ProjectId` = sfr.`Projectid`     where f.projectid = %(projectidv)s     /* and f.isdeleted = 0 and fm.isdeleted = 0 */       )     union     (     select  \"Attribute\" as Type,             f.`FactName` \"SubjectArea\",             ifnull(sa.name,'Other') as \"Application\",             d.`DimensionName` as 'DimensionName' ,             fdr.`RelationshipName` as 'RelationShipName',             da.`DisplayName` as 'FieldName',             da.`Description` as \"Summary\",             'Not Set'     as     \"MeasureGroup\",             'Not Set'     as     \"MeasureType\",             'Not Set'     as     \"BusinessDescription\",             'Not Set'     as     \"PotentialUsage\",             'Not Set'     as     \"Example\",             concat(ifnull(fdr.SourceReference, ''), case when fdr.SourceReference is not null then ', ' else '' end, ifnull(da.SourceReference,'')) as \"SourceInformation\",             'Not Set' as \"Derivation\",             CASE WHEN da.VALUEFORMAT is null then da.columnname else da.VALUEFORMAT end as \"Formula\",             da.DisplayFormat as \"DisplayFormat\",             CASE WHEN da.corporate = 'Y' THEN \"Corporate\" ELSE \"Private\" END as \"Corporate\",             (select concat(firstname,\" \",lastname) from usermain u where u.usermainid = da.`CreatedBy`) as \"CreatedBy\",             da.`CreationDate` as \"CreationDate\",             da.`LastUpdateDate` as \"LastUpdatedDate\",             (select concat(firstname,\" \",lastname) from usermain u where u.usermainid = da.`LastUpdateBy`) as \"LastUpdatedBy\",             da.`md_dimensionattributeid` as \"FieldID\",             da.Expression as \"CustomMeasure\",             f.`md_factid` as \"FactId\",             fdr.`JoinAlias` as \"JoinAlias\",             fdr.`md_factdimensionrelationid` as \"FactDimRelID\",             fdr.`factColumnName` as \"FactColumnName\",             da.`columnname` as \"ColumnName\",             f.isdeleted as \"factIsdeleted\",             0 as \"MeasureIsdeleted\",             fdr.isdeleted as \"factDimRelIsdeleted\",             d.isdeleted as \"DimIsdeleted\",             da.isdeleted as \"AttrIsdeleted\"     from md_factdimensionrelation fdr inner join md_fact f on   f.`MD_Factid` = fdr.`MD_Factid` and f.projectid = fdr.projectid inner join md_dimension d on d.md_dimensionid = fdr.`MD_Dimensionid` and d.projectid = fdr.projectid inner join md_dimensionattribute da on da.`MD_Dimensionid` = d.`MD_Dimensionid`  AND da.projectid = d.projectid left join md_subjectareafactrelation sfr on f.md_factid = sfr.md_factid  and f.projectid = sfr.projectid left join md_subjectarea sa    on sa.md_subjectareaid = sfr.md_subjectareaid  and sa.`ProjectId` = sfr.`Projectid` where d.projectid = %(projectidv)s    )) t" %{'projectidv' : projectid},con = con_mysql.get_con())

# ## 1.Metadata - Custom Measure
#
#
#

# In[7]:

# first step: select a distinct list of distinct measures and attributes
lookup_CustomMeasure = pd.concat( [        metadata[(metadata.Type == 'Measure') | ((metadata.Type == 'Attribute') & (metadata.ColumnName.str[0:2] == 'dd'))][['FieldID','ColumnName','CustomMeasure','FieldName','FactId']].drop_duplicates(),        metadata[(metadata.Type == 'Attribute') & (metadata.ColumnName.str[0:2] <> 'dd')][['FieldID','ColumnName','CustomMeasure','FieldName']].drop_duplicates()],                                      axis = 0)


# In[8]:

# also create a list of distinct RelationshipNames from md_factdimensionrelation
lookup_FactDimRel = metadata[['FactDimRelID', 'RelationshipName','JoinAlias','FactId','FactColumnName']].drop_duplicates()


# In[9]:

# next, replace all the IDs contained by the CustomMeasure column with their related DisplayNames
# all the IDs for M: and D: will be found in either lookup_CustomMeasure-FieldID, either lookup_FactDimRel - FactDimRelID


# ### Functions

# In[10]:

# function that gets a string as parameter, and extracts the IDs from between [], and returns a list of IDs
def substractIDs(x):
    listOfIDs = []
    if x:
        for i in xrange(x.count('[')):
            listOfIDs.append(x.split('[')[i+1].split(']')[0])
    return listOfIDs



# In[11]:

# function that gets a list of IDs as IN parameter,
# and returns a dictionary that maps the measure/attribute IDs with their related DisplayName
def listIDs_to_dictIdDisplayName(xlist,name_df = lookup_CustomMeasure):
    listOfDesc_dict = {'ID':[], 'DESC':[]}
    for i in xlist:
        if i[:2] == 'M:':
            try:
                listOfDesc_dict['ID'].append(name_df[name_df.FieldID == i[2:]]['FieldID'].values[0])
                listOfDesc_dict['DESC'].append(name_df[name_df.FieldID == i[2:]]['FieldName'].values[0])
            except:
                listOfDesc_dict['ID'].append('')
                listOfDesc_dict['DESC'].append('')
        if i[:2] == 'D:':
#             the ID is formed by md_dimensionattributeid + md_factdimensionrelationid
            dimAttrID = i[2:].split('_')
            for j in dimAttrID:
            #  get the description of  md_factdimensionrelation
                try:
                    listOfDesc_dict['ID'].append(lookup_FactDimRel[lookup_FactDimRel.FactDimRelID == j]['FactDimRelID'].values[0])
                    listOfDesc_dict['DESC'].append(lookup_FactDimRel[lookup_FactDimRel.FactDimRelID == j]['RelationshipName'].values[0])
                except:
#                     pass
                    listOfDesc_dict['ID'].append('')
                    listOfDesc_dict['DESC'].append('')
            #  get the description of  md_dimensionattribute
                try:
                    listOfDesc_dict['ID'].append(name_df[name_df.FieldID == j]['FieldID'].values[0])
                    listOfDesc_dict['DESC'].append(name_df[name_df.FieldID == j]['FieldName'].values[0])
                except:
#                     pass
                    listOfDesc_dict['ID'].append('')
                    listOfDesc_dict['DESC'].append('')

    return listOfDesc_dict


# In[12]:

# create a new column in dataframe, for holding the dictionary that maps the IDs with the descriptions
lookup_CustomMeasure['DictIdDesc'] = lookup_CustomMeasure.CustomMeasure.map(lambda x: substractIDs(x)).map(lambda x: listIDs_to_dictIdDisplayName(x))


# In[13]:

# lookup_CustomMeasure[lookup_CustomMeasure.FieldID == '0D805D6E_FF02_4D21_BCC9_6F763C10AB57'].CustomMeasure.map(lambda x: substractIDs(x)).map(lambda x: listIDs_to_dictIdDisplayName(x))


# In[14]:

# function that gets as IN parameters: an ID of measure/attribute as parameter, the CustomMeasure and the dictionary
# and returns the CustomMeasure formed based on DisplayNames instead of IDs
def replaceDictInFormula(vObjectID, x, xdf):
#     x = ''

    try:
#         x = lookup_CustomMeasure[lookup_CustomMeasure.ObjectID == vObjectID]['CustomMeasure'].values[0]
        y = x
#         xdict = lookup_CustomMeasure[lookup_CustomMeasure.ObjectID == vObjectID]['DictIdDesc'].values[0]
#         xdf = pd.DataFrame(xdict)
#         iterate through the ID-s from CustomMeasure, find the related ID in dictionary, and replace with the DESC
        for i in xrange(y.count('[')):

            IDtoReplace = y.split('[')[i+1].split(']')[0]
            IDtoFind = IDtoReplace[2:]

            if IDtoReplace[:2] == 'M:':
                try:
                    x = x.replace('['+IDtoReplace+']', "'" + xdf[xdf.ID == IDtoFind].DESC.values[0] + "'")
                except:
                    pass

            if IDtoReplace[:2] == 'D:':
                try:
                    dimAttrID = IDtoReplace[2:].split('_')
                    dimAttrID_Attr = dimAttrID[0]
                    x = x.replace(dimAttrID_Attr, "'" + xdf[xdf.ID == dimAttrID[0]].DESC.values[0] + "'")
                except:
                    pass

                try:
                    dimAttrID_dimID = '_' + dimAttrID[1] + ']'
                    x = x.replace(dimAttrID_dimID, "'" + xdf[xdf.ID == dimAttrID[1]].DESC.values[0] + "'")
                except:
                    pass

                AttrDesc = "'" + xdf[xdf.ID == dimAttrID[0]].DESC.values[0] + "'"
                DimDesc = "'" + xdf[xdf.ID == dimAttrID[1]].DESC.values[0] + "'"
                x = x.replace(AttrDesc+DimDesc,DimDesc+'.'+AttrDesc)

        x = x.replace('[D:','')
    except:
        pass

    return x





# In[15]:

# lookup_CustomMeasure['CustomMeasureWithDescs'] = lookup_CustomMeasure.ObjectID.map(lambda x: replaceDictInFormula(x))


# In[16]:

# lookup_CustomMeasure will contain the CustomMeasure formula on CustomMeasureWithDescs column
lookup_CustomMeasure['CustomMeasureWithDescs'] = lookup_CustomMeasure.apply(lambda s:             replaceDictInFormula(s['FieldID'],s['CustomMeasure'],pd.DataFrame(s['DictIdDesc']) ), axis = 1)


# ### Check if Custom Measure Contains Custom Measure

# iterate through the IDs from the dictionary
# lookup for each dictionary ID into the original df - lookup_CustomMeasure, and verify its CustomMeasure
# if the CustomMeasure is not None, that means it contains another CustomMeasure
def CustomMeasureContainsCustomMeasure(dictID, df = lookup_CustomMeasure):
    containsCustom = 'N'
    for x in dictID['ID']:
        if x:
            customMeasureCheck = df[df['FieldID'] == x]['CustomMeasure']
            if customMeasureCheck.isnull().values.all():
                # only null values -> no CustomMeasure
                pass
            else:
                containsCustom = 'Y'
                break
    return containsCustom

# In[17]:

lookup_CustomMeasure['containsCustom'] = lookup_CustomMeasure['DictIdDesc'].apply(lambda s: CustomMeasureContainsCustomMeasure(s))

# In[18]:

def MeasureInOtherCustomMeasures (FieldID, df = lookup_CustomMeasure):
    return '; '.join(list(df[df.CustomMeasure.str.contains(FieldID) == True]['FieldName']))


# In[19]:

lookup_CustomMeasure['ListOfDependentFields'] =  lookup_CustomMeasure['FieldID'].apply(lambda s: MeasureInOtherCustomMeasures(s))
# ## 2.Metadata - ColumnName
#

# In[17]:

sql_keywords = pd.read_sql('select * from EXA_SQL_KEYWORDS',con.get_con())


# In[18]:

# keep some of the keywords as valid columnnames
ommited_key_words = ['TEXT', 'LENGTH']
sql_keywords.drop(sql_keywords.index[sql_keywords.KEYWORD.isin(ommited_key_words)], inplace=True)


# ## Functions

# In[20]:

# function to rename all ColumnNames with the DisplayName
lookup_CustomMeasure['ColumnNameUpper'] = lookup_CustomMeasure.ColumnName.apply(lambda s: s.upper() if s else False)
lookup_FactDimRel['JoinAliasUpper'] = lookup_FactDimRel.JoinAlias.apply(lambda s: s.upper() if s else False)
lookup_FactDimRel['FactColumnNameUpper'] = lookup_FactDimRel.FactColumnName.apply(lambda s: s.upper() if s else False)

def replaceAliasAndAttributeAndFactColumns_improved2(md_factmeasureid,pcolumnname,factid):
    replacedFieldsList = []
    pcolumnname_bkp = pcolumnname

#     work with a smaller set of data, for both fields(factColumns) and aliases(FactDimRel)
    factColumns = lookup_CustomMeasure[(lookup_CustomMeasure.FactId == factid) | (lookup_CustomMeasure.FactId.isnull() == True)]
    try:
        FactDimRel = lookup_FactDimRel[lookup_FactDimRel.FactId == factid]
    except:
        FactDimRel = []
    pattern = re.compile(r'[\w]+',re.IGNORECASE)
    listofwords = re.findall(pattern,pcolumnname_bkp)
    listofwords.sort(key = len, reverse = True)
    for fieldname in listofwords:

        fieldnameUpper = fieldname.upper()
#    if the current field name is not already replaced and is not a keyword, then try to find its DisplayName
        if fieldname not in replacedFieldsList and fieldnameUpper not in sql_keywords.KEYWORD.tolist():
#             try to replace the column name - for measures and attributes
#           only check the column length for measures and attributes -> the join aliases might have length = 1
            if len(fieldname) > 1:
	            try:
	                pcolumnname = pcolumnname.replace(fieldname,"'"+factColumns[factColumns.ColumnNameUpper == fieldnameUpper].FieldName.values[0]+"'")
	                replacedFieldsList.append(fieldname)

	            except:
	                try:
	                    pcolumnname = pcolumnname.replace(fieldname,"'"+factColumns[factColumns.ColumnName == fieldname].FieldName.values[0]+"'")
	                    replacedFieldsList.append(fieldname)
	                except:
	                    pass
#             try to replace the aliases
            try:
                pcolumnname = pcolumnname.replace(fieldname,"'"+FactDimRel[FactDimRel.JoinAliasUpper == fieldnameUpper].RelationshipName.values[0]+"'")
                replacedFieldsList.append(fieldname)
            except:
                try:
                    pcolumnname = pcolumnname.replace(fieldname,"'"+FactDimRel[FactDimRel.JoinAlias == fieldname].RelationshipName.values[0]+"'")
                    replacedFieldsList.append(fieldname)
                except:
                    pass
#             try to replace the dimensions
            try:
                pcolumnname = pcolumnname.replace(fieldname,"'"+FactDimRel[FactDimRel.FactColumnNameUpper == fieldnameUpper].RelationshipName.values[0]+"'")
                replacedFieldsList.append(fieldname)
            except:
                try:
                    pcolumnname = pcolumnname.replace(fieldname,"'"+FactDimRel[FactDimRel.FactColumnName == fieldname].RelationshipName.values[0]+"'")
                    replacedFieldsList.append(fieldname)
                except:
                    pass

#####################################################################################################

    return pcolumnname


# In[26]:
#apply the function for smaller slices
#lookup_CustomMeasure["FinalDescription"] = ''
#x = len(lookup_CustomMeasure)

#for i in range(0,(x/1000)+1):
#	if i == 2:
#	    j = (i + 1)* 1000
#	    if j > x:
#	        j = x
#
#	    lookup_CustomMeasure.iloc[i*1000:j+1]["FinalDescription"]=         lookup_CustomMeasure.iloc[i*1000:j+1].apply(lambda s:              replaceAliasAndAttributeAndFactColumns_improved2(s['FieldID'],s['ColumnName'],s['FactId']), axis = 1)

lookup_CustomMeasure["FinalDescription"]=         lookup_CustomMeasure.apply(lambda s:              replaceAliasAndAttributeAndFactColumns_improved2(s['FieldID'],s['ColumnName'],s['FactId']), axis = 1)


#lookup_CustomMeasure[1000:2000]["FinalDescription"]=         lookup_CustomMeasure[1000:2000].apply(lambda s:              replaceAliasAndAttributeAndFactColumns_improved2(s['FieldID'],s['ColumnName'],s['FactId']), axis = 1)
#lookup_CustomMeasure["FinalDescription"] = ''

# In[32]:

# %timeit lookup_CustomMeasure["FinalDescription"] = \
#         lookup_CustomMeasure.apply(lambda s: \
#              replaceAliasAndAttributeAndFactColumns_improved2(s['FieldID'],s['ColumnName'],s['FactId']), axis = 1)


# In[35]:

#lookup_CustomMeasure[['ColumnName','FinalDescription']].head()


# In[24]:

#lookup_CustomMeasure[lookup_CustomMeasure.FieldID == 'F7524CEA_15CD_4D40_B2C7_674A605FAEEA']


# ## Insert data from dataframe to Exasol

# ### Truncate and insert new data

# In[38]:

con.execute('delete from STG_FACT_METADATA;')


# In[39]:

# filter the active fields
m1 = metadata[(metadata.factIsdeleted == 0) & (metadata.MeasureIsdeleted == 0) & (metadata.factDimRelIsdeleted == 0)               & (metadata.DimIsdeleted == 0) & (metadata.AttrIsdeleted == 0)]
m1 = m1.drop(['factIsdeleted','MeasureIsdeleted','factDimRelIsdeleted','DimIsdeleted','AttrIsdeleted'], axis = 1)


# In[42]:

# add a new column in metadata - the CustomMeasure with the related descriptions from md_factmeasure/md_dimensionattribute/md_factdimensionrelation
metadata_descs = pd.DataFrame(data = lookup_CustomMeasure, columns = [ 'FieldID','CustomMeasureWithDescs', 'FinalDescription'])
# metadata = pd.merge(metadata, metadata_descs, how='left', on='FieldID')
x = pd.merge(m1, metadata_descs, how='left', on='FieldID')


# In[40]:



# import re

# x['FieldName'] = x['FieldName'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
# x['Summary'] = x['Summary'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
# x['SourceInformation'] = x['SourceInformation'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
# x['FormulawithFOPScolumns'] = x['FormulawithFOPScolumns'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
# x['CreatedBy'] = x['CreatedBy'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
# x['LastUpdatedBy'] = x['LastUpdatedBy'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
# x['CustomMeasure'] = x['CustomMeasure'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
# x['ColumnName'] = x['ColumnName'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
x['CustomMeasureWithDescs'] = x['CustomMeasureWithDescs'].fillna('')
# x['CustomMeasureWithDescs'] = x['CustomMeasureWithDescs'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')
x['FinalDescription'] = x['FinalDescription'].fillna('')
# x['FinalDescription'] = x['FinalDescription'].apply(lambda s: re.sub(r'[^\x00-\x7F]',' ', s) if s else '')


# In[37]:

# print x.CustomMeasureWithDescs.map(lambda x: len(x)).max()


# APP-9594 - Add Base/Requested field
packagesInfo = pd.read_sql("""
        SELECT e.entity_id, max(concat(p.app_package_name,' ( ',pc.app_package_version,' ) ')) as flagBaseField from app_package_details p
        INNER JOIN app_client_relation pc ON p.app_package_id = pc.app_package_id
        INNER JOIN app_entity_relation e ON p.app_package_id = e.app_package_id
        INNER JOIN md_factmeasure mf ON mf.md_factmeasureid = e.entity_id AND pc.project_id = mf.Projectid
        WHERE pc.project_id = %(projectid)s AND mf.isdeleted =0
        GROUP BY e.entity_id # in case a measure is part of multiple packages, take max
        UNION
        SELECT e.entity_id, max(concat(p.app_package_name,' ( ',pc.app_package_version,' ) ')) as flagBaseField from app_package_details p
        INNER JOIN app_client_relation pc ON p.app_package_id = pc.app_package_id
        INNER JOIN app_entity_relation e ON p.app_package_id = e.app_package_id
        INNER JOIN md_dimensionattribute md ON md.md_dimensionattributeid = e.entity_id AND pc.project_id = md.Projectid
        WHERE pc.project_id = %(projectid)s AND md.isdeleted =0
        GROUP BY e.entity_id; # in case an attribute is part of multiple packages, take max
        """ %{'projectid':projectid} ,con=con_mysql.get_con())

x = pd.merge(x, packagesInfo, how='left', left_on='FieldID',right_on = 'entity_id')

x = x.drop('entity_id', 1)
# In[41]:


# In[37]:

# in order to keep the same order of columns as the exasol table, merge the 'containsCustom' column at the end of the x dataframe
x = pd.merge(x, pd.DataFrame(lookup_CustomMeasure[['FieldID','containsCustom','ListOfDependentFields']].drop_duplicates()), how='left', on='FieldID')

# In[38]:

x['ListOfDependentFields'] = x['ListOfDependentFields'].fillna('')
# add these functions right before shipping to exasol
x = aera_u.df_replaceNonAscii(x)
x = aera_u.df_encoding(x)

# get the definition of stg table
get_exa_table_definition = pd.read_sql('desc STG_FACT_METADATA', con.get_con())
# get the varchar lengths 
get_exa_table_definition['length'] = get_exa_table_definition['SQL_TYPE'].apply(lambda s: s.split('(')[1].split(')')[0] if 'VARCHAR' in s else '')
# convert to lower strings
get_exa_table_definition['COLUMN_NAME'] = get_exa_table_definition['COLUMN_NAME'].apply(lambda s: s.lower())
# get all the columns of the df that will be written to exa
for col in x.columns:
    try:   
        # get the Exa declare length of the df column
        exa_length = get_exa_table_definition[get_exa_table_definition['COLUMN_NAME'] == col.lower() ]['length'].values[0]
        # if the Exa length is less then the df column length, then alter the stg table
        if exa_length <> '' and int(exa_length) < int(x[col].map(lambda x: len(x)).max()):
            alterTable = 'alter table STG_FACT_METADATA modify column '+ col.lower() + ' varchar (' + str(x[col].map(lambda x: len(x)).max() + 1)  + ')'
            con.get_con().execute (alterTable)
    except:
        pass

try:
    con.get_con().writeData(x, table = 'STG_FACT_METADATA',columns = x.columns)
except:
    print con.get_con().error
