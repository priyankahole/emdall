
/* Populate Relationship table fact_scn_product_sellingplant for supply chain navigation */


DROP TABLE IF EXISTS tmp_scn_fpspl_openordcount;
CREATE TABLE tmp_scn_fpspl_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_part p
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;

DROP TABLE IF EXISTS tmp_scn_product_sellingplant;
CREATE TABLE tmp_scn_product_sellingplant as
select DISTINCT f_so.dim_partid dim_partid_product, prt.partnumber dd_partnumber_product, prt.partdescription dd_partdescription_product, prt.plant dd_plant_product,
dp.dim_plantid dim_plantid_sellingplant, dp.plantcode dd_plantcode_sellingplant, dp.name dd_plantname_sellingplant, dp.city dd_plantcity_sellingplant, dp.country dd_plantcountry_sellingplant,

/* Related measures */
sum(f_so.ct_ConfirmedQty) ct_qtyshipped_productsellingplant,

sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_netvalue_productsellingplant,
count(distinct f_so.dim_customerid) ct_customercount_productsellingplant,
count(distinct f_so.dd_salesdocno) ct_socount_productsellingplant,
count(DISTINCT op.dd_salesdocno) ct_openordercount_productsellingplant,
(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0.0000 
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000 
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_productsellingplant

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_fpspl_openordcount op on f_so.dd_salesdocno = op.dd_salesdocno,
dim_part prt, dim_plant dp, dim_salesdocumenttype sdt, dim_date agidt, dim_date pgid
WHERE f_so.dim_partid = prt.dim_partid AND prt.scnenabled = 1 AND f_so.dim_plantid = dp.dim_plantid
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid
GROUP BY f_so.dim_partid, prt.partnumber, prt.partdescription, prt.plant, dp.dim_plantid, dp.plantcode, dp.name, dp.city, dp.country;



truncate TABLE fact_scn_product_sellingplant;

delete from number_fountain m where m.table_name = 'fact_scn_product_sellingplant';

insert into number_fountain
select  'fact_scn_product_sellingplant',
        ifnull(max(f.fact_scn_product_sellingplantid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s where s.dim_projectsourceid = 99),1))
from fact_scn_product_sellingplant f
where f.fact_scn_product_sellingplantid <> 1;


insert into fact_scn_product_sellingplant 
(	fact_scn_product_sellingplantid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	ct_qtyshipped_productsellingplant, 
	amt_netvalue_productsellingplant, 
	ct_customercount_productsellingplant, 
	ct_socount_productsellingplant, 
	ct_openordercount_productsellingplant, 
	ct_otif_productsellingplant
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_product_sellingplant') + 
			row_number() over(order by dim_partid_product, dim_plantid_sellingplant) fact_scn_product_sellingplantid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	ifnull(ct_qtyshipped_productsellingplant,0) ct_qtyshipped_productsellingplant, 
	ifnull(amt_netvalue_productsellingplant,0) amt_netvalue_productsellingplant, 
	ct_customercount_productsellingplant, 
	ct_socount_productsellingplant, 
	ct_openordercount_productsellingplant, 
	ct_otif_productsellingplant
from tmp_scn_product_sellingplant s;


/* Copy Data Into Mirror Table */

truncate table fact_scn_sellingplant_product;

insert into fact_scn_sellingplant_product
(	
	fact_scn_sellingplant_productid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	ct_qtyshipped_productsellingplant, 
	amt_netvalue_productsellingplant, 
	ct_customercount_productsellingplant, 
	ct_socount_productsellingplant, 
	ct_openordercount_productsellingplant, 
	ct_otif_productsellingplant
)
select 
	fact_scn_product_sellingplantid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	ct_qtyshipped_productsellingplant, 
	amt_netvalue_productsellingplant, 
	ct_customercount_productsellingplant, 
	ct_socount_productsellingplant, 
	ct_openordercount_productsellingplant, 
	ct_otif_productsellingplant
from fact_scn_product_sellingplant;





DROP TABLE IF EXISTS tmp_scn_fpspl_openordcount;

DROP TABLE IF EXISTS tmp_scn_product_sellingplant;







