
/* Populate Relationship table fact_scn_sellingplant_customer for supply chain navigation */

DROP TABLE IF EXISTS tmp_scn_splcus_openordcount;
CREATE TABLE tmp_scn_splcus_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_part p, dim_customer dc
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.dim_customerid = dc.dim_customerid AND dc.scnenabled = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;

DROP TABLE IF EXISTS tmp_scn_sellingplant_customer;
CREATE TABLE tmp_scn_sellingplant_customer as
select DISTINCT 
dp.dim_plantid dim_plantid_sellingplant, dp.plantcode dd_plantcode_sellingplant, dp.name dd_plantname_sellingplant, dp.city dd_plantcity_sellingplant, dp.country dd_plantcountry_sellingplant,
dc.dim_customerid dim_customerid_customer, dc.customernumber dd_customernumber_customer, dc.name dd_customername_customer, dc.city dd_customercity_customer, dc.country dd_customercountry_customer,
/* Related measures */
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_netvalue_sellingplantcustomer,
count(distinct dim_partid) ct_productcount_sellingplantcustomer,
COUNT(distinct f_so.dd_salesdocno) ct_socount_sellingplantcustomer,
COUNT(DISTINCT op.dd_salesdocno) ct_openordercount_sellingplantcustomer,
(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0.0000
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_sellingplantcustomer

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_splcus_openordcount op ON op.dd_salesdocno = f_so.dd_salesdocno,
dim_customer dc, dim_plant dp, dim_salesdocumenttype sdt, dim_date agidt, dim_date pgid
WHERE f_so.dim_customerid = dc.dim_customerid 
AND f_so.dim_plantid = dp.dim_plantid AND dc.scnenabled = 1
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid
GROUP BY dp.dim_plantid, dp.plantcode, dp.name, dp.city, dp.country, dc.dim_customerid, dc.customernumber, dc.name, dc.city, dc.country;



truncate TABLE fact_scn_sellingplant_customer;

delete from number_fountain m where m.table_name = 'fact_scn_sellingplant_customer';

insert into number_fountain
select  'fact_scn_sellingplant_customer',
        ifnull(max(f.fact_scn_sellingplant_customerid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s where s.dim_projectsourceid = 99),1))
from fact_scn_sellingplant_customer f
where f.fact_scn_sellingplant_customerid <> 1;


insert into fact_scn_sellingplant_customer 
(	fact_scn_sellingplant_customerid, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	amt_netvalue_sellingplantcustomer, 
	ct_productcount_sellingplantcustomer, 
	ct_socount_sellingplantcustomer, 
	ct_openordercount_sellingplantcustomer, 
	ct_otif_sellingplantcustomer
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_sellingplant_customer') + 
			row_number() over(order by dim_plantid_sellingplant, dim_customerid_customer) fact_scn_sellingplant_customerid, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ifnull(amt_netvalue_sellingplantcustomer,0) amt_netvalue_sellingplantcustomer, 
	ct_productcount_sellingplantcustomer, 
	ct_socount_sellingplantcustomer, 
	ct_openordercount_sellingplantcustomer, 
	ct_otif_sellingplantcustomer
from tmp_scn_sellingplant_customer s;


/* Copy To Mirror Table */

truncate table fact_scn_customer_sellingplant;

insert into fact_scn_customer_sellingplant
(	
	fact_scn_customer_sellingplantid, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	amt_netvalue_sellingplantcustomer, 
	ct_productcount_sellingplantcustomer, 
	ct_socount_sellingplantcustomer, 
	ct_openordercount_sellingplantcustomer, 
	ct_otif_sellingplantcustomer
)
select
	fact_scn_sellingplant_customerid, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	amt_netvalue_sellingplantcustomer, 
	ct_productcount_sellingplantcustomer, 
	ct_socount_sellingplantcustomer, 
	ct_openordercount_sellingplantcustomer, 
	ct_otif_sellingplantcustomer
from fact_scn_sellingplant_customer;




DROP TABLE IF EXISTS tmp_scn_splcus_openordcount;

DROP TABLE IF EXISTS tmp_scn_sellingplant_customer;







