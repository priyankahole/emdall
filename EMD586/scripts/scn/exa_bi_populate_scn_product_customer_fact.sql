
/* Populate Relationship table fact_scn_product_customer for supply chain navigation */


DROP TABLE IF EXISTS tmp_scn_fpcus_openordcount;
CREATE TABLE tmp_scn_fpcus_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_part p, dim_customer dc
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.dim_customerid = dc.dim_customerid AND dc.scnenabled = 1
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;

DROP TABLE IF EXISTS tmp_scn_fpcus_plantcount;
CREATE TABLE tmp_scn_fpcus_plantcount AS
SELECT partnumber, count(*) plantcount
FROM dim_part
GROUP BY partnumber;

DROP TABLE IF EXISTS tmp_scn_product_customer;
CREATE TABLE tmp_scn_product_customer as
select DISTINCT 
f_so.dim_partid dim_partid_product, prt.partnumber dd_partnumber_product, prt.partdescription dd_partdescription_product, prt.plant dd_plant_product,
dc.dim_customerid dim_customerid_customer, dc.customernumber dd_customernumber_customer, dc.name dd_customername_customer, dc.city dd_customercity_customer, dc.country dd_customercountry_customer,
/* Related measures */
sum(f_so.ct_ConfirmedQty) ct_qtyshipped_productcustomer,
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_netvalue_productcustomer,
sum(pc.plantcount) ct_plantcount_productcustomer,
COUNT(distinct f_so.dd_salesdocno) ct_socount_productcustomer,
COUNT(DISTINCT op.dd_salesdocno) ct_openordercount_productcustomer,
(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_productcustomer

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_fpcus_openordcount op ON op.dd_salesdocno = f_so.dd_salesdocno, 
dim_customer dc, dim_part prt, dim_salesdocumenttype sdt, dim_date agidt, dim_date pgid, tmp_scn_fpcus_plantcount pc
WHERE f_so.dim_customerid = dc.dim_customerid AND dc.scnenabled = 1
AND f_so.dim_partid = prt.dim_partid AND prt.scnenabled = 1 AND prt.partnumber = pc.partnumber
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid
GROUP BY f_so.dim_partid, prt.partnumber, prt.partdescription, prt.plant, dc.dim_customerid, dc.customernumber, dc.name, dc.city, dc.country;




truncate TABLE fact_scn_product_customer;

delete from number_fountain m where m.table_name = 'fact_scn_product_customer';

insert into number_fountain
select  'fact_scn_product_customer',
        ifnull(max(f.fact_scn_product_customerid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s where s.dim_projectsourceid = 99),1))
from fact_scn_product_customer f
where f.fact_scn_product_customerid <> 1;


insert into fact_scn_product_customer 
(	fact_scn_product_customerid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ct_qtyshipped_productcustomer, 
	amt_netvalue_productcustomer, 
	ct_plantcount_productcustomer, 
	ct_socount_productcustomer, 
	ct_openordercount_productcustomer, 
	ct_otif_productcustomer
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_product_customer') + 
			row_number() over(order by dim_partid_product, dim_customerid_customer) fact_scn_product_customerid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ifnull(ct_qtyshipped_productcustomer,0) ct_qtyshipped_productcustomer, 	
	ifnull(amt_netvalue_productcustomer,0) amt_netvalue_productcustomer, 
	ct_plantcount_productcustomer, 
	ct_socount_productcustomer, 
	ct_openordercount_productcustomer, 
	ct_otif_productcustomer
from tmp_scn_product_customer s;



/* Copy To Mirror Table */
truncate TABLE fact_scn_customer_product;


insert into fact_scn_customer_product
(
	fact_scn_customer_productid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ct_qtyshipped_productcustomer, 
	amt_netvalue_productcustomer, 
	ct_plantcount_productcustomer, 
	ct_socount_productcustomer, 
	ct_openordercount_productcustomer, 
	ct_otif_productcustomer
)	
select
	fact_scn_product_customerid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ct_qtyshipped_productcustomer, 
	amt_netvalue_productcustomer, 
	ct_plantcount_productcustomer, 
	ct_socount_productcustomer, 
	ct_openordercount_productcustomer, 
	ct_otif_productcustomer
from fact_scn_product_customer;



DROP TABLE IF EXISTS tmp_scn_fpcus_plantcount;

DROP TABLE IF EXISTS tmp_scn_fpcus_openordcount;

DROP TABLE IF EXISTS tmp_scn_product_customer;






