
/* Populate Relationship table fact_scn_supplier_rawmaterial for supply chain navigation */

/* Dimensions: Vendorid, PartID, Date */
/* Measures :   
Related measures: 		Material Qty, Material Amt, Supplier OTIF, Supplier Open Orders count

Related Measures(Derived):  	Supplier Performance Score

Entity measures(Internal): 	Supplier Total Material Count, Overall Supplier OTIF, Material Total Supplier Count, Overall Material OTIF, Total Supplier Open Orders count, 
				Overall supplier perf

Entity Measures(External): 	Supplier City
*/

/* Store date */

drop table if exists tmp_scn_supplier_rawmaterial_1;
create table tmp_scn_supplier_rawmaterial_1 as
select distinct f.dim_vendorid dim_vendorid_supplier, dv.vendornumber dd_vendornumber_supplier, 
dv.vendorname dd_vendorname_supplier, dv.city dd_vendorcity_supplier, dv.country dd_vendorcountry_supplier,
--dp.dim_partid dim_partid_rawmaterial, dp.partdescription partdescription_rawmaterial, dp.plant plant_rawmaterial,
dp.partnumber dd_partnumber_rawmaterial, 
dp.dim_partid,

/* Related measures */
sum(ct_BaseUOMQty) ct_receivedqty_supplierrawmaterial, 
sum(amt_ItemNetValue * amt_exchangerate_gbl) amt_itemnetvalue_supplierrawmaterial, 
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_supplierrawmaterial,
count(DISTINCT f.dd_DocumentNo) ct_pocount_supplierrawmaterial,
/*sum(case when dd_ontime_delv = 'Y' AND dd_qtyinfull = 'Y' THEN 1 ELSE 0 END) tmp_ct_otifcount_supplier_rawmaterial, 
sum(1) tmp_ct_overallcount_supplier_rawmaterial, */
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END) / 
	CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE 
			  SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_supplierrawmaterial,

/* Related Measures(Derived) */
cast(0 as decimal(36,2)) ct_supplierperformancescore_supplierrawmaterial

FROM fact_purchase f, dim_date d, dim_vendor dv, dim_part dp, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid AND f.dim_vendorid = dv.dim_vendorid AND f.dim_partid = dp.dim_partid and f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_vendorid <> 1 AND f.dim_partid <> 1 AND ct_DeliveryQty > 0	/* Some sanitization */
--AND dp.scnenabled = 1 and dv.scnenabled = 1 -- We can comment this as Supplier Raw Material Combination Is Not Having Much Data
GROUP BY f.dim_vendorid, dv.vendornumber, dv.vendorname, dv.city, dv.country, dp.partnumber,
--dp.dim_partid, dp.partdescription, dp.plant
dp.dim_partid;


DROP TABLE IF EXISTS tmp_scn_supplier_rawmaterial;
CREATE TABLE tmp_scn_supplier_rawmaterial
as
select s.*, dp.dim_partid dim_partid_rawmaterial, dp.partdescription dd_partdescription_rawmaterial, dp.plant dd_plant_rawmaterial, dpl.dim_plantid dim_plantid_rawmaterial
FROM tmp_scn_supplier_rawmaterial_1 s, dim_part dp, dim_plant dpl
WHERE s.dim_partid = dp.dim_partid and dp.plant = dpl.plantcode;




truncate TABLE fact_scn_supplier_rawmaterial;

delete from number_fountain m where m.table_name = 'fact_scn_supplier_rawmaterial';

insert into number_fountain
select  'fact_scn_supplier_rawmaterial',
        ifnull(max(f.fact_scn_supplier_rawmaterialId),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s where s.dim_projectsourceid = 99),1))
from fact_scn_supplier_rawmaterial f
where f.fact_scn_supplier_rawmaterialid <> 1;


insert into fact_scn_supplier_rawmaterial 
(	fact_scn_supplier_rawmaterialid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dd_partnumber_rawmaterial, 
	dim_partid, 
	ct_receivedqty_supplierrawmaterial, 
	amt_itemnetvalue_supplierrawmaterial, 
	ct_openordercount_supplierrawmaterial, 
	ct_pocount_supplierrawmaterial, 
	ct_otif_supplierrawmaterial, 
	ct_supplierperformancescore_supplierrawmaterial, 
	dim_partid_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial,
	dim_plantid_rawmaterial
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_supplier_rawmaterial') + 
			row_number() over(order by dim_vendorid_supplier, dim_partid_rawmaterial) fact_scn_supplier_rawmaterialid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dd_partnumber_rawmaterial, 
	dim_partid, 
	ct_receivedqty_supplierrawmaterial, 
	amt_itemnetvalue_supplierrawmaterial, 
	ct_openordercount_supplierrawmaterial, 
	ct_pocount_supplierrawmaterial, 
	ct_otif_supplierrawmaterial, 
	ct_supplierperformancescore_supplierrawmaterial, 
	dim_partid_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial,
	dim_plantid_rawmaterial
from tmp_scn_supplier_rawmaterial s;


/* Copy Data Into Mirror Table */

truncate table fact_scn_rawmaterial_supplier;

insert into fact_scn_rawmaterial_supplier
(	
	fact_scn_rawmaterial_supplierid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dd_partnumber_rawmaterial, 
	dim_partid, 
	ct_receivedqty_supplierrawmaterial, 
	amt_itemnetvalue_supplierrawmaterial, 
	ct_openordercount_supplierrawmaterial, 
	ct_pocount_supplierrawmaterial, 
	ct_otif_supplierrawmaterial, 
	ct_supplierperformancescore_supplierrawmaterial, 
	dim_partid_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_plantid_rawmaterial
)
select
	fact_scn_supplier_rawmaterialid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dd_partnumber_rawmaterial, 
	dim_partid, 
	ct_receivedqty_supplierrawmaterial, 
	amt_itemnetvalue_supplierrawmaterial, 
	ct_openordercount_supplierrawmaterial, 
	ct_pocount_supplierrawmaterial, 
	ct_otif_supplierrawmaterial, 
	ct_supplierperformancescore_supplierrawmaterial, 
	dim_partid_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial,
	dim_plantid_rawmaterial
from fact_scn_supplier_rawmaterial;



DROP TABLE IF EXISTS tmp_scn_supplier_rawmaterial_1;
DROP TABLE IF EXISTS tmp_scn_supplier_rawmaterial;





