
/*
The following script is used to select data for the supply chain navigation model. 
*/

/*
INSERT INTO EMD586.DIM_PROJECTSOURCE
(
	DIM_PROJECTSOURCEID, 
	PROJECTCODE, 
	PROJECTDESCRIPTION, 
	PROJECTID, 
	PARENTPROJECTID, 
	MULTIPLIER, 
	ROWSTARTDATE, 
	ROWENDDATE, 
	ROWISCURRENT, 
	ROWCHANGEREASON, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	LSINDICATOR
)
SELECT
99	DIM_PROJECTSOURCEID, 
'EMD586'	PROJECTCODE, 
'EMD Parent Instance'	PROJECTDESCRIPTION, 
'0B778AE1_48B6_46D9_A6AA_5BDE5693F7FC'	PROJECTID, 
'6EA48753_57DE_4B4E_A749_03C03908C860'	PARENTPROJECTID, 
10000000000	MULTIPLIER, 
Null	ROWSTARTDATE, 
Null	ROWENDDATE, 
Null	ROWISCURRENT, 
Null	ROWCHANGEREASON, 
Current_Timestamp	DW_INSERT_DATE, 
Current_Timestamp	DW_UPDATE_DATE, 
'Not Set'	LSINDICATOR
*/


/* SCN Enabled Column */
/*
ALTER TABLE DIM_PLANT ADD COLUMN scnenabled int DEFAULT 0
ALTER TABLE DIM_MDG_PART ADD COLUMN scnenabled int DEFAULT 0
ALTER TABLE DIM_PART ADD COLUMN scnenabled int DEFAULT 0
ALTER TABLE DIM_VENDOR ADD COLUMN scnenabled int DEFAULT 0
ALTER TABLE DIM_CUSTOMER ADD COLUMN scnenabled int DEFAULT 0
*/


-- 119854
/* Step 1 - Identify the Products with Active BOM */
/*
drop table if exists tmp_scn_productswithbom
create table tmp_scn_productswithbom as 
select distinct rp.dim_partid, rp.partnumber as toplevelpart, rp.plant as toplevelplant, rp.parttype as toplevelparttype, rp.parttypedescription as toplevelparttypedescription
from fact_bom f, dim_part cp, dim_part pp, dim_part rp, dim_date vf, dim_date vt,
	 dim_bomstatus 	boms, dim_bomcategory 	bc, dim_billofmaterialmisc 	bomis, dim_bomitemcategory bicat
where f.dim_rootpartid <> 1 and f.dim_bomcomponentid <> 1 
and f.dim_bomcomponentid = cp.dim_partid and f.dim_partid = pp.dim_partid 
and f.dim_rootpartid = rp.dim_partid and f.dim_validfromdateid = vf.dim_dateid
and f.dim_dateidvalidto = vt.dim_dateid and f.dim_bomstatusid = boms.dim_bomstatusid	
and f.dim_bomcategoryid = bc.dim_bomcategoryid
and f.dim_billofmaterialmiscid = bomis.dim_billofmaterialmiscid
and f.dim_bomitemcategoryid = bicat.dim_bomitemcategoryid
-- and boms.description = 'Active' -- and vt.datevalue > current_date
and f.dim_rootpartid in (select distinct dim_partid from fact_fosalesforecast)
*/


/* Compare Part/Plant for Part Type */
/*
drop table if exists tmp_ca_parttypeswithbom
create table tmp_ca_parttypeswithbom as 
select rp.parttype, rp.parttypedescription, count(*) as rowcount
-- cp.parttype, cp.parttypedescription, count(*) as rowcount
from fact_bom f, dim_part cp, dim_part pp, dim_part rp, dim_date vf, dim_date vt,
dim_bomstatus 	boms, dim_bomcategory 	bc, dim_billofmaterialmisc 	bomis, dim_bomitemcategory bicat
where f.dim_rootpartid <> 1 and f.dim_bomcomponentid <> 1 
and f.dim_bomcomponentid = cp.dim_partid and f.dim_partid = pp.dim_partid 
and f.dim_rootpartid = rp.dim_partid and f.dim_validfromdateid = vf.dim_dateid
and f.dim_dateidvalidto = vt.dim_dateid and f.dim_bomstatusid = boms.dim_bomstatusid	
and f.dim_bomcategoryid = bc.dim_bomcategoryid
and f.dim_billofmaterialmiscid = bomis.dim_billofmaterialmiscid
and f.dim_bomitemcategoryid = bicat.dim_bomitemcategoryid
and boms.description = 'Active' and vt.datevalue > current_date
group by rp.parttype, rp.parttypedescription
--group by cp.parttype, cp.parttypedescription
*/

-- 21702
/* Step 2 - Get Sales Order Amt for the Products with Active BOM */
/*
drop table if exists tmp_scn_fpbom_sales
create table tmp_scn_fpbom_sales as 
select t.*, sum(CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) orderamt
from tmp_scn_productswithbom t, fact_salesorder f, dim_date soc, dim_salesmisc dsi
where --parttype = 'FG' and 
t.dim_partid = f.dim_partid
and f.dim_dateidsalesordercreated = soc.dim_dateid
and f.dim_salesmiscid =	dsi.dim_salesmiscid
and soc.datevalue >= '2014-01-01'
group by t.*
having sum(CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) > 0
*/


-- 199759
/* Step 3 - Identify all components of the Products with Active BOM */
/*
drop table if exists tmp_scn_bomrc
create table tmp_scn_bomrc as 
select distinct f.dd_rootpart, f.dd_componentnumber, f.dd_level
from tmp_scn_fpbom_sales t, fact_bom f, dim_part cp, dim_part pp, dim_part rp, dim_date vf, dim_date vt,
	 dim_bomstatus 	boms, dim_bomcategory 	bc, dim_billofmaterialmisc 	bomis, dim_bomitemcategory bicat
where t.dim_partid = f.dim_rootpartid
and f.dim_bomcomponentid = cp.dim_partid and f.dim_partid = pp.dim_partid 
and f.dim_rootpartid = rp.dim_partid and f.dim_validfromdateid = vf.dim_dateid
and f.dim_dateidvalidto = vt.dim_dateid and f.dim_bomstatusid = boms.dim_bomstatusid	
and f.dim_bomcategoryid = bc.dim_bomcategoryid
and f.dim_billofmaterialmiscid = bomis.dim_billofmaterialmiscid
and f.dim_bomitemcategoryid = bicat.dim_bomitemcategoryid
and boms.description = 'Active' and vt.datevalue > current_date
*/


--286209
/* Step 4 - Identify Dim_Part Records for Update */
/*
drop table if exists tmp_scn_partselect;
create table tmp_scn_partselect as 
select distinct p.dim_partid, p.partnumber, p.partdescription, p.parttype, p.parttypedescription, p.plant  
from dim_part p inner join 
( select distinct partnumber
from (
select distinct dd_rootpart as partnumber from tmp_scn_bomrc union all
select  distinct dd_componentnumber as partnumber from tmp_scn_bomrc ) p
) f on p.partnumber = f.partnumber
*/



-- Check Data Queries
/*
select parttype, parttypedescription, count(*) selectcount
from tmp_scn_partselect
group by parttype, parttypedescription

select plant, count(*) selectcount
from tmp_scn_partselect
group by plant
*/


/* -------------------------------------------- */
-- Commenting all the above steps as we are enabling scn for Healthcare Forecasted Parts with BOM entries.

-- Option 2
/* Step 1 - Identify Dim_PartID Which Have BOM and Forecast */
drop table if exists tmp_dim_mdg_partid_all;
create table tmp_dim_mdg_partid_all as
select distinct pr.dim_partid, md.dim_mdg_partid
from dim_mdg_part md, dim_part pr
where md.projectsourceid = 1 and pr.projectsourceid = 1 and
right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18);


drop table if exists tmp_forecast_mdg_part_link;
create table tmp_forecast_mdg_part_link as
select * from tmp_dim_mdg_partid_all where dim_mdg_partid in (
select distinct dim_partid from emd586.fact_fosalesforecast where dim_partid <> 1);


drop table if exists tmp_scn_partselect_forecast;
create table tmp_scn_partselect_forecast as 
select distinct p.dim_partid, p.partnumber, p.partdescription, p.parttype, p.parttypedescription, p.plant, p.projectsourceid
from dim_part p inner join 
(
select distinct dim_partid 
from tmp_forecast_mdg_part_link 
where dim_partid in 
(
select distinct dim_partid from 
(
select distinct dim_rootpartid as dim_partid from fact_bom
union all
select distinct dim_bomcomponentid as dim_partid from fact_bom
union all
select distinct dim_partid as dim_partid from fact_bom 
) x
)
) f on p.dim_partid = f.dim_partid;


/* Step 2 - Identify Dim_PartID In All Levels - Root, Sub Assembly, Component in BOM  */
drop table if exists tmp_scn_partselect_bom;
create table tmp_scn_partselect_bom as 
select distinct dim_rootpartid, dim_bomcomponentid, dim_partid
from 
(
select distinct dim_rootpartid, dim_bomcomponentid, dim_partid 
from fact_bom 
where dim_rootpartid <> 1 and dim_bomcomponentid <> 1 and dim_partid <> 1 
and dim_rootpartid in (select dim_partid from tmp_scn_partselect_forecast)
union all
select distinct dim_rootpartid, dim_bomcomponentid, dim_partid 
from fact_bom 
where dim_rootpartid <> 1 and dim_bomcomponentid <> 1 and dim_partid <> 1 
and dim_bomcomponentid in (select dim_partid from tmp_scn_partselect_forecast)
union all
select distinct dim_rootpartid, dim_bomcomponentid, dim_partid 
from fact_bom 
where dim_rootpartid <> 1 and dim_bomcomponentid <> 1 and dim_partid <> 1 
and dim_partid in (select dim_partid from tmp_scn_partselect_forecast)
) x;


/* Step 3 - Identify Dim_Part Records for Update  */
drop table if exists tmp_scn_partselect;
create table tmp_scn_partselect as 
select distinct p.dim_partid, p.partnumber, p.partdescription, p.parttype, p.parttypedescription, p.plant, p.projectsourceid
from dim_part p inner join 
(
select distinct dim_partid
from 
(
select distinct dim_rootpartid as dim_partid from tmp_scn_partselect_bom
union all
select distinct dim_bomcomponentid as dim_partid from tmp_scn_partselect_bom
union all
select distinct dim_partid as dim_partid from tmp_scn_partselect_bom
) x
) f on p.dim_partid = f.dim_partid;


drop table if exists tmp_scn_mdgpartselect;
create table tmp_scn_mdgpartselect as 
select distinct dim_mdg_partid
from tmp_forecast_mdg_part_link where dim_partid in (select dim_partid from tmp_scn_partselect);



/* Final Step */


/* Update Dim_Part */

-- reset for new data
update dim_part p
set p.scnenabled = 0;

--286209
-- set for scn 
update dim_part p
set p.scnenabled = 1
from tmp_scn_partselect t, dim_part p
where p.dim_partid = t.dim_partid
and p.dim_partid <> 1; 


/* Update Dim_MDG_Part */

-- reset for new data
update dim_mdg_part p
set p.scnenabled = 0;

--286209
-- set for scn 
update dim_mdg_part p
set p.scnenabled = 1
from tmp_scn_mdgpartselect t, dim_mdg_part p
where p.dim_mdg_partid = t.dim_mdg_partid
and p.dim_mdg_partid <> 1; 


/* Update Dim_Vendor */

drop table if exists tmp_scn_vendor;
create table tmp_scn_vendor as 
select distinct f.dim_vendorid
from fact_purchase f, dim_part p, dim_vendor v
where f.dim_partid <> 1 and f.dim_partid = p.dim_partid and p.scnenabled = 1 and v.dim_vendorid = f.dim_vendorid;

-- reset for new data
update dim_vendor v
set v.scnenabled = 0;

-- set for scn
update dim_vendor v
set v.scnenabled = 1
from tmp_scn_vendor t, dim_vendor v
where v.dim_vendorid = t.dim_vendorid
and v.dim_vendorid <> 1;


/* Update Dim_Customer */

drop table if exists tmp_scn_customer;
create table tmp_scn_customer as 
select distinct f.dim_customerid
from fact_salesorder f, dim_part p, dim_customer c
where f.dim_partid <> 1 and f.dim_partid = p.dim_partid and p.scnenabled = 1 and c.dim_customerid = f.dim_customerid;

-- reset for new data
update dim_customer c
set c.scnenabled = 0;

-- set for scn
update dim_customer c
set c.scnenabled = 1
from tmp_scn_customer t, dim_customer c
where c.dim_customerid = t.dim_customerid
and c.dim_customerid <> 1;



/* Update Dim_Plant */
drop table if exists tmp_scn_plant;
create table tmp_scn_plant as 
select p.dim_plantid, p.plantcode, p.projectsourceid
from dim_plant p inner join 
(
select distinct dim_plantid
from
(
select distinct dim_plantidsupplying as dim_plantid from fact_purchase where dim_plantidsupplying <> 1 and dim_vendorid in (select dim_vendorid from dim_vendor where scnenabled = 1)
union all
select distinct dim_plantidsupplying as dim_plantid from fact_purchase where dim_plantidsupplying <> 1 and dim_vendorid in (select dim_vendorid from dim_vendor where scnenabled = 1)
union all
select distinct dim_plantid from fact_salesorder where dim_plantid <> 1 and dim_customerid in (select dim_customerid from dim_customer where scnenabled = 1)
union all
select dim_plantid from dim_plant where  dim_plantid <> 1 and plantcode in (select distinct plant from dim_part where scnenabled = 1)
) x
) f on p.dim_plantid = f.dim_plantid
and p.projectsourceid = 1 and  p.dim_plantid <> 1;

-- reset for new data
update dim_plant pl
set pl.scnenabled = 0;

-- set for scn
update dim_plant p
set p.scnenabled = 1
from tmp_scn_plant t, dim_plant p
where p.dim_plantid = t.dim_plantid
and p.dim_plantid <> 1;



/* Drop Temp Tables */
drop table if exists tmp_scn_productswithbom;
drop table if exists tmp_scn_fpbom_sales;
drop table if exists tmp_scn_bomrc;

drop table if exists tmp_dim_mdg_partid_all;
drop table if exists tmp_forecast_mdg_part_link;
drop table if exists tmp_scn_partselect_forecast;
drop table if exists tmp_scn_partselect_bom;
drop table if exists tmp_scn_partselect;
drop table if exists tmp_scn_mdgpartselect;

drop table if exists tmp_scn_vendor;
drop table if exists tmp_scn_customer;
drop table if exists tmp_scn_plant;













