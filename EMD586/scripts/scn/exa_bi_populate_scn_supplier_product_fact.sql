

/* Populate Relationship table fact_scn_supplier_product for supply chain navigation */

DROP TABLE IF EXISTS tmp_scn_sp_fact_purchase_part;
CREATE TABLE tmp_scn_sp_fact_purchase_part AS
SELECT 	distinct fp.dim_partid
from fact_purchase fp 
inner join dim_date d on fp.dim_dateidcreate = d.dim_dateid
inner join dim_vendor dv on fp.dim_vendorid = dv.dim_vendorid and dv.scnenabled = 1
inner join dim_part dp on fp.dim_partid = dp.dim_partid and dp.scnenabled = 1
WHERE d.datevalue > current_date-450;


DROP TABLE IF EXISTS tmp_scn_sp_fact_salesorder_part;
CREATE TABLE tmp_scn_sp_fact_salesorder_part AS
SELECT 	distinct f_so.dim_partid
from fact_salesorder f_so 
inner join dim_date agidt on f_so.dim_dateidactualgi = agidt.dim_dateid
inner join dim_customer dc on dc.dim_customerid = f_so.dim_customerid and dc.scnenabled = 1
inner join dim_part dp on dp.dim_partid = f_so.dim_partid and dp.scnenabled = 1
WHERE agidt.datevalue > current_date-450;

-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_sp_fmm_prodorder_issue;   
create table tmp_scn_sp_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, f.dd_productionordernumber, 'Issue' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_sp_fmm_prodorder_receive;   
create table tmp_scn_sp_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_sp_fmm_prodorder_link;   
create table tmp_scn_sp_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber
from tmp_scn_sp_fmm_prodorder_receive por, tmp_scn_sp_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;


-- Step 4 - Get BOM Root and Component
DROP TABLE IF EXISTS tmp_scn_sp_bom;
CREATE TABLE tmp_scn_sp_bom AS
SELECT DISTINCT f_bom.dd_ComponentNumber, f_bom.dd_RootPart
FROM fact_bom f_bom, dim_part cp, dim_part rp, tmp_scn_sp_fact_purchase_part fp, tmp_scn_sp_fact_salesorder_part fso
WHERE f_bom.dim_bomcomponentid = cp.dim_partid AND cp.scnenabled = 1
AND f_bom.dim_rootpartid = rp.dim_partid AND rp.scnenabled = 1
AND f_bom.dim_bomcomponentid = fp.dim_partid
AND f_bom.dim_rootpartid = fso.dim_partid;

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_sp_bom (dd_RootPart,dd_ComponentNumber) 
select dd_rootpart, dd_ComponentNumber
from tmp_scn_sp_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_sp_bom b
where b.dd_ComponentNumber = m.dd_ComponentNumber and b.dd_RootPart = m.dd_rootpart);


DROP TABLE IF EXISTS tmp_scn_sp_fact_purchase;
CREATE TABLE tmp_scn_sp_fact_purchase AS
SELECT 	DISTINCT 
		fp.dim_vendorid dim_vendorid_supplier, 
		dv.vendornumber dd_vendornumber_supplier, 
		dv.vendorname dd_vendorname_supplier, 
		dv.city dd_vendorcity_supplier,
		dv.country dd_vendorcountry_supplier,
		fp.dim_partid as dim_partid_supplierpart, 
		dp.partnumber dd_componentpartnumber,
		fp.ct_BaseUOMQty,
		fp.amt_ItemNetValue,
		fp.amt_exchangerate_gbl 
from fact_purchase fp 
inner join dim_date d on fp.dim_dateidcreate = d.dim_dateid
inner join dim_vendor dv on fp.dim_vendorid = dv.dim_vendorid and dv.scnenabled = 1
inner join dim_part dp on fp.dim_partid = dp.dim_partid and dp.scnenabled =1
WHERE -- d.datevalue > current_date-500 AND 
EXISTS ( SELECT 1 FROM tmp_scn_sp_bom t where t.DD_COMPONENTNUMBER = dp.partnumber);



DROP TABLE IF EXISTS tmp_scn_sp_fact_salesorder;
CREATE TABLE tmp_scn_sp_fact_salesorder AS
SELECT 	dc.dim_customerid dim_customerid_customer, 
		dc.customernumber dd_customernumber_customer, 
		dc.name dd_customername_customer, 
		dc.city dd_customercity_customer, 
		dc.country dd_customercountry_customer, 
		f_so.dim_partid as dim_partid_customerpart, 
		dp.partnumber as dd_rootpartnumber, 
		sum(f_so.ct_ConfirmedQty) ct_productqty_suppliercustomer,
		sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*f_so.amt_exchangerate_gbl) amt_netitemvalue_suppliercustomer
from fact_salesorder f_so 
inner join dim_date agidt on f_so.dim_dateidactualgi = agidt.dim_dateid
inner join dim_customer dc on dc.dim_customerid = f_so.dim_customerid and dc.scnenabled = 1
inner join dim_part dp on dp.dim_partid = f_so.dim_partid and dp.scnenabled = 1 
WHERE -- agidt.datevalue > current_date-400 AND 
EXISTS ( SELECT 1 FROM tmp_scn_sp_bom t where t.DD_ROOTPART = dp.partnumber)
GROUP BY dc.dim_customerid, dc.customernumber, dc.name, dc.city, dc.country, f_so.dim_partid, 
		 dp.partnumber; 



DROP TABLE IF EXISTS tmp_scn_supplier_product;
CREATE TABLE tmp_scn_supplier_product as
SELECT 
		fp.dim_vendorid_supplier, 
		fp.dd_vendornumber_supplier,
		fp.dd_vendorname_supplier, 
		fp.dd_vendorcity_supplier, 
		fp.dd_vendorcountry_supplier,
		pd.dim_partid dim_partid_product, 
		pd.partnumber dd_partnumber_product, 
		pd.partdescription dd_partdescription_product, 
		pd.plant dd_plant_product,
		count(distinct f_bom.DD_COMPONENTNUMBER) ct_rawmaterialcount_supplierproduct,
		sum(fp.ct_BaseUOMQty) ct_rawmaterialqty_supplierproduct,
		sum(fp.amt_ItemNetValue * fp.amt_exchangerate_gbl) amt_netitemvaluerawmaterial_supplierproduct
FROM tmp_scn_sp_fact_purchase fp, dim_part dpcomp, tmp_scn_sp_bom f_bom, dim_part pd
WHERE fp.dim_partid_supplierpart = dpcomp.dim_partid AND dpcomp.scnenabled = 1
AND dpcomp.partnumber = f_bom.DD_COMPONENTNUMBER 
AND f_bom.DD_ROOTPART = pd.partnumber AND pd.scnenabled = 1
GROUP BY fp.dim_vendorid_supplier, fp.dd_vendornumber_supplier, fp.dd_vendorname_supplier, fp.dd_vendorcity_supplier, fp.dd_vendorcountry_supplier,
pd.dim_partid, pd.partnumber, pd.partdescription, pd.plant;


/* Insert Into Fact Table */

truncate TABLE fact_scn_supplier_product;

delete from number_fountain m where m.table_name = 'fact_scn_supplier_product';

insert into number_fountain
select  'fact_scn_supplier_product',
        ifnull(max(f.fact_scn_supplier_productid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s where s.dim_projectsourceid = 99),1))
from fact_scn_supplier_product f
where f.fact_scn_supplier_productid <> 1;


insert into fact_scn_supplier_product 
( 
	fact_scn_supplier_productid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ct_rawmaterialcount_supplierproduct, 
	ct_rawmaterialqty_supplierproduct, 
	amt_netitemvaluerawmaterial_supplierproduct
)
select 
  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_supplier_product') + 
      row_number() over(order by dim_vendorid_supplier, dim_partid_product)  	fact_scn_supplier_productid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ct_rawmaterialcount_supplierproduct, 
	ct_rawmaterialqty_supplierproduct, 
	amt_netitemvaluerawmaterial_supplierproduct
from tmp_scn_supplier_product s;


/* Copy Data Into Mirror Table */

truncate table fact_scn_product_supplier;

insert into fact_scn_product_supplier
(	
	fact_scn_product_supplierid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ct_rawmaterialcount_supplierproduct, 
	ct_rawmaterialqty_supplierproduct, 
	amt_netitemvaluerawmaterial_supplierproduct
)
select
	fact_scn_supplier_productid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ct_rawmaterialcount_supplierproduct, 
	ct_rawmaterialqty_supplierproduct, 
	amt_netitemvaluerawmaterial_supplierproduct
from fact_scn_supplier_product;	



/* Drop Temp Tables */

DROP TABLE IF EXISTS tmp_scn_sp_bom;
DROP TABLE IF EXISTS tmp_scn_sp_fact_purchase;
DROP TABLE IF EXISTS tmp_scn_supplier_product;
DROP TABLE IF EXISTS tmp_scn_sp_fact_salesorder;
DROP TABLE IF EXISTS tmp_scn_sp_fact_purchase_part;
DROP TABLE IF EXISTS tmp_scn_sp_fact_salesorder_part;

DROP TABLE IF EXISTS tmp_scn_sp_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_sp_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_sp_fmm_prodorder_link;


