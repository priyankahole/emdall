
/* Populate Relationship table fact_scn_plant (po plant) for supply chain navigation */

/* Direct Measures */

DROP TABLE IF EXISTS tmp_scn_plant;
CREATE TABLE tmp_scn_plant as
SELECT dp.dim_Plantid dim_plantid_plant, dp.plantcode dd_plantcode_plant, dp.name dd_plantname_plant, dp.city dd_plantcity_plant, dp.country dd_plantcountry_plant,
count(distinct f.dim_vendorid) ct_suppliercount_plant,
count(distinct f.dim_partid) ct_materialcount_plant,
sum(amt_ItemNetValue * amt_exchangerate_gbl) amt_itemnetvalue_plant, 
count(DISTINCT f.dd_DocumentNo) ct_pocount_plant,
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_plant,
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END)/CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_plant,

/* Related Measures(Derived) */
cast(0 as decimal(36,2)) ct_supplierperformancescore_plant,

/* Indirect measures */
cast(0 as decimal(36,0)) ct_productcount_plant,
cast(0 as decimal(36,0)) ct_sellingplantcount_plant,
cast(0 as decimal(36,4)) amt_salesordernetvalue_plant,
cast(0 as decimal(36,0)) ct_customercount_plant

FROM fact_purchase f, dim_date d, dim_vendor dv, dim_plant dp, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid 
AND f.dim_vendorid = dv.dim_vendorid AND dv.scnenabled = 1
AND f.dim_plantidordering = dp.dim_plantid and f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_vendorid <> 1 AND f.dim_partid <> 1 AND ct_DeliveryQty > 0
GROUP BY dp.dim_Plantid, dp.plantcode, dp.name, dp.city, dp.country;


/* Add 1 row that corresponds to all plants */
DROP TABLE IF EXISTS tmp_scn_plant_all;
CREATE TABLE tmp_scn_plant_all as
SELECT
count(distinct f.dim_vendorid) ct_suppliercount_plant,
count(distinct f.dim_partid) ct_materialcount_plant,
sum(amt_ItemNetValue * amt_exchangerate_gbl) amt_itemnetvalue_plant,
count(DISTINCT f.dd_DocumentNo) ct_pocount_plant,
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_plant,
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END)/CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_plant

FROM fact_purchase f, dim_date d, dim_vendor dv, dim_plant dp, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid 
AND f.dim_vendorid = dv.dim_vendorid AND dv.scnenabled = 1
AND f.dim_plantidordering = dp.dim_plantid and f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_vendorid <> 1 AND f.dim_partid <> 1 AND ct_DeliveryQty > 0;


insert into tmp_scn_plant
(		dim_plantid_plant, 
		dd_plantcode_plant, 
		dd_plantname_plant, 
		dd_plantcity_plant, 
		dd_plantcountry_plant,
		ct_suppliercount_plant, 
		ct_materialcount_plant, 
		amt_itemnetvalue_plant, 
		ct_pocount_plant, 
		ct_openordercount_plant, 
		ct_otif_plant,
		ct_supplierperformancescore_plant,
		ct_productcount_plant,
		ct_sellingplantcount_plant,
		amt_salesordernetvalue_plant,
		ct_customercount_plant
)
SELECT 	0 dim_plantid_plant, 
		'ALL' dd_plantcode_plant, 
		'ALL' dd_plantname_plant, 
		'ALL' dd_plantcity_plant, 
		'ALL' dd_plantcountry_plant,
		ct_suppliercount_plant, 
		ct_materialcount_plant, 
		amt_itemnetvalue_plant, 
		ct_pocount_plant, 
		ct_openordercount_plant, 
		ct_otif_plant,
		0 ct_supplierperformancescore_plant,
		0 ct_productcount_plant,
		0 ct_sellingplantcount_plant,
		0 amt_salesordernetvalue_plant,
		0 ct_customercount_plant
FROM tmp_scn_plant_all;



/* In Direct Measures */

/*
DROP TABLE IF EXISTS tmp_scn_plant_indirect
CREATE TABLE tmp_scn_plant_indirect AS
SELECT pl.dim_plantid dim_plantid_plant,
count(distinct f_bom.dd_RootPart) ct_productcount_plant,
count(distinct f_so.dim_plantid) ct_sellingplantcount_plant,
count(distinct f_so.dim_customerid) ct_customercount_plant,
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))* f_so.amt_exchangerate_gbl) amt_salesordernetvalue_plant
FROM fact_bom f_bom, fact_salesorder f_so, dim_part dp, dim_part dpcomp, dim_plant pl, dim_customer c
WHERE f_bom.dd_RootPart = dp.partnumber AND dp.dim_partid = f_so.dim_partid AND dp.scnenabled = 1
AND c.dim_customerid = f_so.dim_customerid AND c.scnenabled = 1
AND f_bom.dd_ComponentNumber = dpcomp.partnumber AND dpcomp.scnenabled = 1
AND dp.plant = pl.plantcode
GROUP BY pl.dim_plantid
*/

-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_poplant_fmm_prodorder_issue;   
create table tmp_scn_poplant_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, dp.dim_partid dim_BOMcomponentid,dp.plant dd_componentplant, f.dd_productionordernumber, 
'Issue' as dd_activity, (-1 * sum(f.ct_quantity)) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber, dp.dim_partid, dp.plant
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_poplant_fmm_prodorder_receive;   
create table tmp_scn_poplant_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, dp.dim_partid dim_rootpartid, dp.plant dd_rootplant,pl.dim_plantid dim_rootplantid, 
f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber, dp.dim_partid, dp.plant, pl.dim_plantid
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_poplant_fmm_prodorder_link;   
create table tmp_scn_poplant_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber, por.dim_rootpartid, por.dim_rootplantid,
 por.dd_rootplant, poi.dim_BOMcomponentid, poi.dd_componentplant,
poi.ct_quantity ct_componentqty, 1 as dd_level
from tmp_scn_poplant_fmm_prodorder_receive por, tmp_scn_poplant_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;

-- Step 4 - Get BOM Root and Component
DROP TABLE IF EXISTS tmp_scn_poplant_bom;
CREATE TABLE tmp_scn_poplant_bom AS
SELECT distinct f_bom.dim_rootpartid, f_bom.dd_RootPart, pl.dim_plantid dim_plantid
FROM fact_bom f_bom, dim_part dp, dim_part dpcomp, dim_plant pl
WHERE f_bom.dim_rootpartid = dp.dim_partid AND f_bom.dd_RootPart = dp.partnumber AND dp.scnenabled = 1
AND f_bom.dim_bomcomponentid = dpcomp.dim_partid AND f_bom.dd_ComponentNumber = dpcomp.partnumber AND dpcomp.scnenabled = 1
AND dp.plant = pl.plantcode AND pl.scnenabled = 1;

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_poplant_bom (dd_RootPart,dim_rootpartid, dim_plantid) 
select dd_RootPart,dim_rootpartid, dim_rootplantid
from tmp_scn_poplant_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_poplant_bom b
where b.dim_rootpartid = m.dim_rootpartid);

DROP TABLE IF EXISTS tmp_scn_poplant_sales;
CREATE TABLE tmp_scn_poplant_sales AS
SELECT f_so.dim_partid, f_so.dim_plantid, f_so.dim_customerid,
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))* f_so.amt_exchangerate_gbl) amt_salesordernetvalue
FROM fact_salesorder f_so, dim_part dp, dim_plant pl, dim_customer c
WHERE f_so.dim_partid = dp.dim_partid AND dp.scnenabled = 1
AND dp.plant = pl.plantcode AND pl.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
group by f_so.dim_partid, f_so.dim_plantid, f_so.dim_customerid;

DROP TABLE IF EXISTS tmp_scn_plant_indirect;
CREATE TABLE tmp_scn_plant_indirect AS
SELECT f_bom.dim_plantid dim_plantid_plant,
count(distinct f_bom.dd_RootPart) ct_productcount_plant,
count(distinct f_so.dim_plantid) ct_sellingplantcount_plant,
count(distinct f_so.dim_customerid) ct_customercount_plant,
sum(amt_salesordernetvalue) amt_salesordernetvalue_plant
FROM tmp_scn_poplant_bom f_bom, tmp_scn_poplant_sales f_so
WHERE f_bom.dim_rootpartid = f_so.dim_partid 
GROUP BY f_bom.dim_plantid;


/*
INSERT INTO tmp_scn_plant_indirect
SELECT 0 dim_plantid_plant,
count(distinct f_bom.dd_RootPart) ct_productcount_plant,
count(distinct f_so.dim_plantid) ct_sellingplantcount_plant,
count(distinct f_so.dim_customerid) ct_customercount_plant,
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*f_so.amt_exchangerate_gbl) amt_salesordernetvalue_plant
FROM fact_bom f_bom, fact_salesorder f_so, dim_part dp, dim_part dpcomp, dim_plant pl, dim_customer c
WHERE f_bom.dd_RootPart = dp.partnumber AND dp.dim_partid = f_so.dim_partid AND dp.scnenabled = 1
AND c.dim_customerid = f_so.dim_customerid AND c.scnenabled = 1
AND f_bom.dd_ComponentNumber = dpcomp.partnumber AND dpcomp.scnenabled = 1
AND dp.plant = pl.plantcode
*/

INSERT INTO tmp_scn_plant_indirect
SELECT 0 dim_plantid_plant,
count(distinct f_bom.dd_RootPart) ct_productcount_plant,
count(distinct f_so.dim_plantid) ct_sellingplantcount_plant,
count(distinct f_so.dim_customerid) ct_customercount_plant,
sum(amt_salesordernetvalue) amt_salesordernetvalue_plant
FROM tmp_scn_poplant_bom f_bom, tmp_scn_poplant_sales f_so
WHERE f_bom.dim_rootpartid = f_so.dim_partid;



UPDATE tmp_scn_plant p
set p.ct_productcount_plant = t.ct_productcount_plant,
	p.ct_sellingplantcount_plant = t.ct_sellingplantcount_plant,
	p.amt_salesordernetvalue_plant = t.amt_salesordernetvalue_plant,
	p.ct_customercount_plant = t.ct_customercount_plant
FROM tmp_scn_plant_indirect t, tmp_scn_plant p
WHERE p.dim_plantid_plant = t.dim_plantid_plant;



/* Insert Into Fact Table */

truncate TABLE fact_scn_plant;

delete from number_fountain m where m.table_name = 'fact_scn_plant';

insert into number_fountain
select  'fact_scn_plant',
        ifnull(max(f.fact_scn_plantid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s where s.dim_projectsourceid = 99),1))
from fact_scn_plant f
where f.fact_scn_plantid <> 1;


insert into fact_scn_plant 
( 
	fact_scn_plantid, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	ct_suppliercount_plant, 
	ct_materialcount_plant, 
	amt_itemnetvalue_plant, 
	ct_pocount_plant, 
	ct_openordercount_plant, 
	ct_otif_plant, 
	ct_supplierperformancescore_plant, 
	ct_productcount_plant, 
	ct_sellingplantcount_plant, 
	amt_salesordernetvalue_plant, 
	ct_customercount_plant
)
select 
  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_plant') + 
      row_number() over(order by dim_plantid_plant) fact_scn_plantid, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	ct_suppliercount_plant, 
	ct_materialcount_plant, 
	amt_itemnetvalue_plant, 
	ct_pocount_plant, 
	ct_openordercount_plant, 
	ct_otif_plant, 
	ct_supplierperformancescore_plant, 
	ct_productcount_plant, 
	ct_sellingplantcount_plant, 
	amt_salesordernetvalue_plant, 
	ct_customercount_plant
from tmp_scn_plant s;



/* Drop Temp Tables */

DROP TABLE IF EXISTS tmp_scn_plant_indirect;
DROP TABLE IF EXISTS tmp_scn_plant_all;
DROP TABLE IF EXISTS tmp_scn_plant;

DROP TABLE IF EXISTS tmp_scn_pop_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_pop_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_pop_fmm_prodorder_link;
DROP TABLE IF EXISTS tmp_scn_pop_bom;


