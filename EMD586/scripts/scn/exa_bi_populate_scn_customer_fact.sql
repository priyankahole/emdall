
/* Populate Relationship table fact_scn_customer for supply chain navigation */

/* Direct Measures */

/* Store date */
/*
DROP TABLE IF EXISTS tmp_scn_recentdata
CREATE TABLE tmp_scn_recentdata AS
SELECT current_date-365 as dd_recentdate
*/

DROP TABLE IF EXISTS tmp_scn_ce_so_openordcount;
CREATE TABLE tmp_scn_ce_so_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_part p, dim_customer c
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;


DROP TABLE IF EXISTS tmp_scn_customer;
CREATE TABLE tmp_scn_customer as
select DISTINCT 
dc.dim_customerid dim_customerid_customer, dc.customernumber dd_customernumber_customer, dc.name dd_customername_customer, dc.city dd_customercity_customer, dc.country dd_customercountry_customer,
/* Related measures */
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_itemnetvalue_customer,
count(distinct prd.partnumber) ct_productcount_customer,
count(distinct f_so.dim_plantid) ct_plantcount_customer,
COUNT(distinct f_so.dd_salesdocno) ct_socount_customer,
COUNT(DISTINCT op.dd_salesdocno) ct_openordercount_customer,
(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_customer,
cast(0 as decimal(36,0)) ct_rawmaterialcount_customer,
cast(0 as decimal(36,0)) ct_poplantcount_customer,
cast(0 as decimal(36,0)) ct_suppliercount_customer

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_ce_so_openordcount op ON op.dd_salesdocno = f_so.dd_salesdocno, 
dim_customer dc, dim_plant dp, dim_salesdocumenttype sdt, dim_date agidt, dim_part prd, dim_date pgid --, tmp_lastyeardate_scn t
WHERE f_so.dim_customerid = dc.dim_customerid AND dc.scnenabled = 1
AND f_so.dim_plantid = dp.dim_plantid 
AND f_so.dim_partid = prd.dim_partid AND prd.scnenabled = 1
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid
-- AND agidt.datevalue  >= t.dt_prevyear
GROUP BY dc.dim_customerid, dc.customernumber, dc.name, dc.city, dc.country;


DROP TABLE IF EXISTS tmp_scn_customer_all;
CREATE TABLE tmp_scn_customer_all as
SELECT 
/* Related measures */
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_itemnetvalue_customer,
count(distinct prd.partnumber) ct_productcount_customer,
count(distinct f_so.dim_plantid) ct_plantcount_customer,
COUNT(distinct f_so.dd_salesdocno) ct_socount_customer,
COUNT(DISTINCT op.dd_salesdocno) ct_openordercount_customer,
(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_customer

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_ce_so_openordcount op ON op.dd_salesdocno = f_so.dd_salesdocno, 
dim_customer dc, dim_plant dp, dim_salesdocumenttype sdt, dim_date agidt, dim_part prd, dim_date pgid -- , tmp_lastyeardate_scn t
WHERE f_so.dim_customerid = dc.dim_customerid AND dc.scnenabled = 1
AND f_so.dim_plantid = dp.dim_plantid 
AND f_so.dim_partid = prd.dim_partid AND prd.scnenabled = 1
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid
-- AND agidt.datevalue  >= t.dt_prevyear
;


INSERT INTO tmp_scn_customer
(	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	amt_itemnetvalue_customer, 
	ct_productcount_customer, 
	ct_plantcount_customer, 
	ct_socount_customer, 
	ct_openordercount_customer, 
	ct_otif_customer, 
	ct_rawmaterialcount_customer, 
	ct_poplantcount_customer, 
	ct_suppliercount_customer
)
SELECT 	0 dim_customerid_customer, 
		'ALL' dd_customernumber_customer, 
		'ALL' dd_customername_customer, 
		'ALL' dd_customercity_customer, 
		'ALL' dd_customercountry_customer,
		amt_itemnetvalue_customer, 
		ct_productcount_customer, 
		ct_plantcount_customer, 
		ct_socount_customer, 
		ct_openordercount_customer, 
		ct_otif_customer,
		0 ct_rawmaterialcount_customer,
		0 ct_poplantcount_customer,
		0 ct_suppliercount_customer		
--		cast(0 as decimal(36,0)) ct_rawmaterialcount_customer,
--		cast(0 as decimal(36,0)) ct_poplantcount_customer,
--		cast(0 as decimal(36,0)) ct_suppliercount_customer
FROM tmp_scn_customer_all;


/* Update indirect measures */

-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_ce_fmm_prodorder_issue;   
create table tmp_scn_ce_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, f.dd_productionordernumber, 'Issue' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_ce_fmm_prodorder_receive;   
create table tmp_scn_ce_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_ce_fmm_prodorder_link;   
create table tmp_scn_ce_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber
from tmp_scn_ce_fmm_prodorder_receive por, tmp_scn_ce_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;

-- Step 4 - Get BOM Root and Component
DROP TABLE IF EXISTS tmp_scn_ce_bom;
CREATE TABLE tmp_scn_ce_bom AS
SELECT DISTINCT f_bom.dd_ComponentNumber, f_bom.dd_RootPart
FROM fact_bom f_bom, dim_part cp, dim_part rp
WHERE f_bom.dim_bomcomponentid = cp.dim_partid AND cp.scnenabled = 1
AND f_bom.dim_rootpartid = rp.dim_partid AND rp.scnenabled = 1;

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_ce_bom (dd_RootPart,dd_ComponentNumber) 
select dd_rootpart, dd_ComponentNumber
from tmp_scn_ce_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_ce_bom b
where b.dd_ComponentNumber = m.dd_ComponentNumber and b.dd_RootPart = m.dd_rootpart);

DROP TABLE IF EXISTS tmp_scn_ce_sales_bom;
CREATE TABLE tmp_scn_ce_sales_bom AS
SELECT distinct f_so.dim_customerid, f_so.dim_plantid, bom.dd_RootPart
FROM  fact_salesorder f_so 
      inner join dim_part dp on f_so.dim_partid = dp.dim_partid and dp.scnenabled = 1
      inner join dim_customer c on f_so.dim_customerid = c.dim_customerid and c.scnenabled = 1
      left outer join tmp_scn_ce_bom bom on dp.partnumber = bom.dd_RootPart;


DROP TABLE IF EXISTS tmp_scn_ce_purchase_bom;
CREATE TABLE tmp_scn_ce_purchase_bom AS
SELECT distinct f.dim_vendorid, f.dim_plantidordering, bom.dd_ComponentNumber
FROM  fact_purchase f 
      inner join dim_part dp on f.dim_partid = dp.dim_partid and dp.scnenabled = 1
      inner join dim_vendor v on f.dim_vendorid = v.dim_vendorid and v.scnenabled = 1
      left outer join tmp_scn_ce_bom bom on dp.partnumber = bom.dd_ComponentNumber;



DROP TABLE IF EXISTS tmp_scn_customer_indirect;
CREATE TABLE tmp_scn_customer_indirect AS
SELECT f_so.dim_customerid dim_customerid_customer,
count(distinct f_bom.dd_ComponentNumber) ct_rawmaterialcount_customer,
count(distinct f.dim_plantidordering) ct_poplantcount_customer,
count(distinct f.dim_vendorid) ct_suppliercount_customer
FROM tmp_scn_ce_bom f_bom, tmp_scn_ce_purchase_bom f, tmp_scn_ce_sales_bom f_so
WHERE f_bom.dd_RootPart = f_so.dd_RootPart
AND f_bom.dd_ComponentNumber = f.dd_ComponentNumber
GROUP BY f_so.dim_customerid;


INSERT INTO tmp_scn_customer_indirect
SELECT 0 dim_customerid_customer,
count(distinct f_bom.dd_ComponentNumber) ct_rawmaterialcount_customer,
count(distinct f.dim_plantidordering) ct_poplantcount_customer,
count(distinct f.dim_vendorid) ct_suppliercount_customer
FROM tmp_scn_ce_bom f_bom, tmp_scn_ce_purchase_bom f, tmp_scn_ce_sales_bom f_so
WHERE f_bom.dd_RootPart = f_so.dd_RootPart
AND f_bom.dd_ComponentNumber = f.dd_ComponentNumber;


UPDATE tmp_scn_customer s
SET s.ct_rawmaterialcount_customer = t.ct_rawmaterialcount_customer,
	s.ct_poplantcount_customer = t.ct_poplantcount_customer,
	s.ct_suppliercount_customer = t.ct_suppliercount_customer
FROM tmp_scn_customer_indirect t, tmp_scn_customer s
WHERE s.dim_customerid_customer = t.dim_customerid_customer;






/* Insert Into Fact Table */

truncate TABLE fact_scn_customer;

delete from number_fountain m where m.table_name = 'fact_scn_customer';

insert into number_fountain
select  'fact_scn_customer',
        ifnull(max(f.fact_scn_customerid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s where s.dim_projectsourceid = 99),1))
from fact_scn_customer f
where f.fact_scn_customerid <> 1;


insert into fact_scn_customer 
( 
	fact_scn_customerid, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	amt_itemnetvalue_customer, 
	ct_productcount_customer, 
	ct_plantcount_customer, 
	ct_socount_customer, 
	ct_openordercount_customer, 
	ct_otif_customer, 
	ct_rawmaterialcount_customer, 
	ct_poplantcount_customer, 
	ct_suppliercount_customer --, 
--	amt_openorder
)
select 
  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_customer') + 
      row_number() over(order by dim_customerid_customer) 	fact_scn_customerid, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ifnull(amt_itemnetvalue_customer,0) amt_itemnetvalue_customer, 
	ct_productcount_customer, 
	ct_plantcount_customer, 
	ct_socount_customer, 
	ct_openordercount_customer, 
	ct_otif_customer, 
	ct_rawmaterialcount_customer, 
	ct_poplantcount_customer, 
	ct_suppliercount_customer --, 
--	amt_openorder
from tmp_scn_customer s;




/* Drop Temp Tables */
DROP TABLE IF EXISTS tmp_scn_ce_purchase_bom;
DROP TABLE IF EXISTS tmp_scn_ce_sales_bom;
DROP TABLE IF EXISTS tmp_scn_ce_bom;
DROP TABLE IF EXISTS tmp_scn_ce_so_openordcount;

DROP TABLE IF EXISTS tmp_scn_ce_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_ce_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_ce_fmm_prodorder_link;

DROP TABLE IF EXISTS tmp_scn_customer_indirect;
DROP TABLE IF EXISTS tmp_scn_customer_all;
DROP TABLE IF EXISTS tmp_scn_customer;




