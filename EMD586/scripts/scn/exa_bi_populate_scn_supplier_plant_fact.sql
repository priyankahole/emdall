
/* Populate Relationship table fact_scn_supplier_plant for supply chain navigation */


/* Store date */

DROP TABLE IF EXISTS tmp_scn_supplier_plant;
CREATE TABLE tmp_scn_supplier_plant as
select DISTINCT f.dim_vendorid dim_vendorid_supplier, dv.vendornumber dd_vendornumber_supplier, dv.vendorname dd_vendorname_supplier, dv.city dd_vendorcity_supplier,
dv.country dd_vendorcountry_supplier,
dp.dim_Plantid dim_plantid_plant, dp.plantcode dd_plantcode_plant, dp.name dd_plantname_plant, dp.city dd_plantcity_plant, dp.country dd_plantcountry_plant,

/* Related measures */
count(distinct prt.partnumber) ct_materialcount_supplierplant,
sum(amt_ItemNetValue * f.amt_exchangerate_gbl) amt_itemnetvalue_supplierplant,
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_supplierplant,
count(DISTINCT f.dd_DocumentNo) ct_pocount_supplierplant,
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END)/CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_supplierplant,

/* Related Measures(Derived) */
cast(0 as decimal(18,2)) ct_supplierperformancescore_supplierplant

FROM fact_purchase f, dim_date d, dim_vendor dv, dim_plant dp, dim_part prt, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid AND f.dim_vendorid = dv.dim_vendorid AND f.dim_plantidordering = dp.dim_plantid AND f.dim_partid = prt.dim_partid
AND f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_vendorid <> 1 AND f.dim_plantidordering <> 1 AND f.dim_partid <> 1 AND ct_DeliveryQty > 0	/* Some sanitization */
-- AND prt.scnenabled = 1 AND dv.scnenabled = 1 -- Commenting this as scnenabled = 1 returining no data
GROUP BY f.dim_vendorid, dv.vendornumber, dv.vendorname, dv.city, dv.country,
dp.dim_Plantid, dp.plantcode, dp.name, dp.city, dp.country;


truncate TABLE fact_scn_supplier_plant;

delete from number_fountain m where m.table_name = 'fact_scn_supplier_plant';

insert into number_fountain
select  'fact_scn_supplier_plant',
        ifnull(max(f.fact_scn_supplier_plantid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s where s.dim_projectsourceid = 99),1))
from fact_scn_supplier_plant f
where f.fact_scn_supplier_plantid <> 1;


insert into fact_scn_supplier_plant 
(	fact_scn_supplier_plantid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	ct_materialcount_supplierplant, 
	amt_itemnetvalue_supplierplant, 
	ct_openordercount_supplierplant, 
	ct_pocount_supplierplant, 
	ct_otif_supplierplant, 
	ct_supplierperformancescore_supplierplant
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_supplier_plant') + 
			row_number() over(order by dim_vendorid_supplier, dim_plantid_plant) fact_scn_supplier_plantid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	ct_materialcount_supplierplant, 
	amt_itemnetvalue_supplierplant, 
	ct_openordercount_supplierplant, 
	ct_pocount_supplierplant, 
	ct_otif_supplierplant, 
	ct_supplierperformancescore_supplierplant
from tmp_scn_supplier_plant s;


/* Copy Data Into Mirror Table */
truncate table fact_scn_plant_supplier;

insert into fact_scn_plant_supplier
(	
	fact_scn_plant_supplierid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	ct_materialcount_supplierplant, 
	amt_itemnetvalue_supplierplant, 
	ct_openordercount_supplierplant, 
	ct_pocount_supplierplant, 
	ct_otif_supplierplant, 
	ct_supplierperformancescore_supplierplant
)
select
	fact_scn_supplier_plantid, 
	dim_vendorid_supplier, 
	dd_vendornumber_supplier, 
	dd_vendorname_supplier, 
	dd_vendorcity_supplier, 
	dd_vendorcountry_supplier, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	ct_materialcount_supplierplant, 
	amt_itemnetvalue_supplierplant, 
	ct_openordercount_supplierplant, 
	ct_pocount_supplierplant, 
	ct_otif_supplierplant, 
	ct_supplierperformancescore_supplierplant
from fact_scn_supplier_plant;




DROP TABLE IF EXISTS tmp_scn_supplier_plant;







