/*Copy and Adaptation to Exasol of /home/fusionops/ispring_clustered_storage/configurations/MerckDA5/scripts/dimensions/dim_date/vw_dim_date.part4.sql */

drop table if exists tmp_update_maxdt_exists;
create table tmp_update_maxdt_exists  as
select distinct companycode,DateValue, plantcode_factory,min(dim_dateid) dim_dateid from dim_date_factory_calendar
group by companycode,DateValue,plantcode_factory;

Update companymaster_d00 set MAXDT_EXISTS = 0;
Update companymaster_d00 t1
set MAXDT_EXISTS = IFNULL(dd.exst, 0)
from companymaster_d00 t1
		left join (select 1 as exst, x.* from tmp_update_maxdt_exists x where x.plantcode_factory = 'Not Set') dd on dd.DateValue = t1.MAXDT AND dd.companycode = t1.pCompanyCode;
                                
Drop table if exists mh_i01;
Create table mh_i01
as Select ifnull(max(dim_dateid),0) maxid
from dim_date_factory_calendar;
 
      INSERT INTO dim_date_factory_calendar(dim_dateid,DateValue,
                          DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          select mh_i01.maxid + row_number() over( order by ''), MAXDT,
                  MAXDT_NAME,
                  0,
                  YEAR(MAXDT),
                  0,
                  case when MONTH(MAXDT) in (1,2,3) then 1		/* VW original: QUARTER(MAXDT) */ 
					   when MONTH(MAXDT) in (4,5,6) then 2
					   when MONTH(MAXDT) in (7,8,9) then 3
					   when MONTH(MAXDT) in (10,11,12) then 4 end,
                  0,
                  concat(YEAR(MAXDT), ' Q ',case when MONTH(MAXDT) in (1,2,3) then 1		
												 when MONTH(MAXDT) in (4,5,6) then 2
												 when MONTH(MAXDT) in (7,8,9) then 3
												 when MONTH(MAXDT) in (10,11,12) then 4 end),
                  0,
                  0,
                  'Not Set',
                  'Not Set',
                  0,
                  MONTH(MAXDT),
                  to_char(MAXDT, 'Month') ,					/* VW original: date_format(MAXDT,'%M') */ 
                  LEFT(to_char(MAXDT, 'Month'),3),			/* VW original: date_format(MAXDT,'%M') */
                  0,
                  0,
                  0,
                  CASE 
					WHEN TO_CHAR(TRUNC(MAXDT,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(MAXDT,'UW') + 1 
					ELSE TO_CHAR(MAXDT,'UW') 
				END /*WEEK(MAXDT) */,		/* VW original: WEEK(MAXDT,4) */
                  0,
                  0,
                  DAY(MAXDT),
                  0,
                  cast(to_char(MAXDT,'DD') as integer), 															/* VW original: DAYOFMONTH(MAXDT) */
                  cast(to_char(MAXDT,'D') as integer),																/* VW original: DAYOFWEEK(MAXDT) */
                  to_char(MAXDT, 'Day'),																			/* VW original: date_format(MAXDT,'%W') */
                  LEFT(to_char(MAXDT, 'Day'),3),																	/* VW original: LEFT(date_format(MAXDT,'%W'),3) */
                  (CASE WHEN cast(to_char(MAXDT,'D') as integer) IN (1,7) THEN 1 ELSE 0 END),						/* VW original: DAYOFWEEK(MAXDT) */
                  (CASE WHEN ((mod(YEAR(MAXDT),4) = 0)  AND (mod(YEAR(MAXDT) , 100) != 0 OR mod(YEAR(MAXDT) ,400) = 0)) THEN 1 ELSE 0 END),
                  cast(to_char(CAST(concat('9999-01-01 00:00:00') AS timestamp),'DDD') as integer),					/* VW original: DAYOFYEAR(CAST(concat('9999-01-01 00:00:00') AS timestamp)) */
                  cast(to_char(CAST(concat('9999-01-01 00:00:00') AS timestamp),'DDD') as integer),					/* VW original: DAYOFYEAR(CAST(concat('9999-01-01 00:00:00') AS timestamp)) */
                  cast(to_char(MAXDT,'DD') as integer) - 1,															/* VW original: DAYOFMONTH(MAXDT) */
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  cast(to_char(MAXDT,'DD') as integer),																/* VW original: DAYOFMONTH(MAXDT) */					
                  0,
                  0,
                  pCompanyCode,
                  concat(YEAR(MAXDT),'-',lpad(convert(varchar (2),CASE 
					WHEN TO_CHAR(TRUNC(MAXDT,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(MAXDT,'UW') + 1 
					ELSE TO_CHAR(MAXDT,'UW') 
				END /*WEEK(MAXDT) */),2,'0'))		/* VW original: WEEK(MAXDT,4) */
		  from companymaster_d00,mh_i01 where MAXDT_EXISTS = 0;
                
drop table if exists mh_i01;
 
Update companymaster_d00 
SET MAXDT = TO_DATE('12/31/9999','MM/DD/YYYY'), MAXDT_NAME = '31 Dec 9999';
 

Update companymaster_d00 t1
set MAXDT_EXISTS = IFNULL(dd.exst, 0)
from companymaster_d00 t1
		left join (select 1 as exst, x.* from tmp_update_maxdt_exists x) dd on dd.DateValue = t1.MAXDT AND dd.companycode = t1.pCompanyCode;
		
                               
drop table if exists mh_i01;
Create table mh_i01
as
select 	ifnull(max(d.dim_dateid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from dim_date_factory_calendar d
where d.dim_dateid <> 1;

 
      INSERT INTO dim_date_factory_calendar(dim_dateid,DateValue,
                          DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          select mh_i01.maxid + row_number() over(order by ''),MAXDT,
                  MAXDT_NAME,
                  0,
                  YEAR(MAXDT),
                  0,
				  case when MONTH(MAXDT) in (1,2,3) then 1		/* VW original: QUARTER(MAXDT) */ 
					   when MONTH(MAXDT) in (4,5,6) then 2
					   when MONTH(MAXDT) in (7,8,9) then 3
					   when MONTH(MAXDT) in (10,11,12) then 4 end,
                  0,
                  concat(YEAR(MAXDT), ' Q ',case when MONTH(MAXDT) in (1,2,3) then 1		
												 when MONTH(MAXDT) in (4,5,6) then 2
												 when MONTH(MAXDT) in (7,8,9) then 3
												 when MONTH(MAXDT) in (10,11,12) then 4 end),
                  0,
                  0,
                  'Not Set',
                  'Not Set',
                  0,
                  MONTH(MAXDT),
                  to_char(MAXDT, 'Month') ,					/* VW original: date_format(MAXDT,'%M') */ 
                  LEFT(to_char(MAXDT, 'Month'),3),			/* VW original: date_format(MAXDT,'%M') */
                  0,
                  0,
                  0,
                  CASE 
					WHEN TO_CHAR(TRUNC(MAXDT,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(MAXDT,'UW') + 1 
					ELSE TO_CHAR(MAXDT,'UW') 
				END /* WEEK(MAXDT) */,		/* VW original: WEEK(MAXDT,4) */
                  0,
                  0,
                  DAY(MAXDT),
                  0,
                  cast(to_char(MAXDT,'DD') as integer), 															/* VW original: DAYOFMONTH(MAXDT) */
                  cast(to_char(MAXDT,'D') as integer),																/* VW original: DAYOFWEEK(MAXDT) */
                  to_char(MAXDT, 'Day'),																			/* VW original: date_format(MAXDT,'%W') */
                  LEFT(to_char(MAXDT, 'Day'),3),																	/* VW original: LEFT(date_format(MAXDT,'%W'),3) */
                  (CASE WHEN cast(to_char(MAXDT,'D') as integer) IN (1,7) THEN 1 ELSE 0 END),						/* VW original: DAYOFWEEK(MAXDT) */
                  (CASE WHEN ((mod(YEAR(MAXDT) , 4) = 0)  AND (mod(YEAR(MAXDT) , 100) != 0 OR mod(YEAR(MAXDT) , 400) = 0)) THEN 1 ELSE 0 END),
                  cast(to_char(CAST(concat('9999-01-01 00:00:00') AS timestamp),'DDD') as integer),					/* VW original: ifnull(DAYOFYEAR(CAST(concat('9999-12-31 00:00:00') AS timestamp(0))),365) */
                  cast(to_char(CAST(concat('9999-01-01 00:00:00') AS timestamp),'DDD') as integer),					/* VW original: ifnull(DAYOFYEAR(CAST(concat('9999-12-31 00:00:00') AS timestamp(0))),365) */
                  cast(to_char(MAXDT,'DD') as integer) - 1,															/* VW original: DAYOFMONTH(MAXDT) */
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  cast(to_char(MAXDT,'DD') as integer),																/* VW original: DAYOFMONTH(MAXDT) */					
                  0,
                  0,
                  pCompanyCode,
                  concat(YEAR(MAXDT),'-',lpad(convert(varchar(2), CASE 
					WHEN TO_CHAR(TRUNC(MAXDT,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(MAXDT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(MAXDT,'UW') + 1 
					ELSE TO_CHAR(MAXDT,'UW') 
				END /* WEEK(MAXDT)*/ ),2,'0'))							/* VW original: WEEK(MAXDT,4) */
		from companymaster_d00,mh_i01
		where MAXDT_EXISTS=0;
                 
drop table if exists mh_i01; 
  
  insert into dim_monthyear
  (Dim_MonthYearid, MonthYear, MonthYearAlt, MonthStartDate, MonthEndDate, DaysInMonth, CalYear, WorkdaysInMonth)
  select distinct case Dim_Dateid when 1 then 1 else CalendarMonthID end CalendarMonthID, 
          case Dim_Dateid when 1 then 'Jan 0001' else MonthYear end MonthYear, 
          case Dim_Dateid when 1 then '0001-01' else concat(CalendarYear,'-',lpad(CalendarMonthnumber,2,0)) end MonthYearAlt, 
          MonthStartDate, 
          MonthEndDate, 
          DaysInMonth, 
          CalendarYear CalYear, 
          case Dim_Dateid when 1 then 0 else WorkdaysInMonth end WorkdaysInMonth
  from dim_date_factory_calendar dt
  where (Dim_Dateid = 1 or CalendarMonthID > 0) and companycode = 'Not Set'
      and not exists (select 1 from dim_monthyear my 
                      where my.Dim_MonthYearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarMonthID end);

DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins1;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_del;

CREATE TABLE tmp_dimdate_dim_year_ins 
AS
select distinct case Dim_Dateid when 1 then 1 else CalendarYear end Dim_Yearid,
CalendarYear CalYear,
case CalendarYear when 9999 then 365 else DaysInCalendarYear end DaysInCalendarYear
from dim_date_factory_calendar dt
where (Dim_Dateid = 1 or CalendarYear > 0) and companycode = 'Not Set';

CREATE TABLE tmp_dimdate_dim_year_ins1
AS
SELECT DISTINCT Dim_Yearid
FROM tmp_dimdate_dim_year_ins;

CREATE TABLE tmp_dimdate_dim_year_del
AS
SELECT * FROM tmp_dimdate_dim_year_ins1 where 1=2;

INSERT INTO tmp_dimdate_dim_year_del
SELECT DISTINCT y.Dim_Yearid
FROM dim_year y, dim_date_factory_calendar dt
where y.Dim_Yearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarYear end;

merge into tmp_dimdate_dim_year_ins1 t1
using tmp_dimdate_dim_year_del t2 on t1.Dim_Yearid = t2.Dim_Yearid
when matched then delete;
					/* VW original: call vectorwise(combine 'tmp_dimdate_dim_year_ins1-tmp_dimdate_dim_year_del') */

insert into dim_year(
dim_yearid,calyear,daysincalendaryear)
select DISTINCT dt.Dim_Yearid, 
dt.CalYear,
dt.DaysInCalendarYear
from tmp_dimdate_dim_year_ins dt, tmp_dimdate_dim_year_ins1 i
WHERE i.Dim_Yearid = dt.Dim_Yearid;
/*where (Dim_Dateid = 1 or CalendarYear > 0) and companycode = 'Not Set'
and not exists (select 1 from dim_year y 
      where y.Dim_Yearid = case dt.Dim_Dateid when 1 then 1 else dt.CalendarYear end)*/

DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_ins1;
DROP TABLE IF EXISTS tmp_dimdate_dim_year_del;

/* business days sequence calculation */
  
drop table if exists dimdateseqtemp;
create table dimdateseqtemp (
companycode varchar(10) null,
dim_dateid bigint null,
datevalue timestamp null,
isapublicholiday integer null,
isaweekendday integer null,
dateseqno integer null,
plantcode_factory varchar(20) null);

Insert into dimdateseqtemp(companycode,dim_dateid,datevalue,isapublicholiday,isaweekendday,dateseqno,plantcode_factory)
select companycode,dim_dateid, datevalue,isapublicholiday,isaweekendday, row_number() over(partition by companycode,plantcode_factory order by DateValue)  DtSeqNo,plantcode_factory
from dim_date_factory_calendar dt
where dt.IsaPublicHoliday = 0 and dt.IsaWeekendday = 0 ;


/* Update BusinessDaysSeqNo for business days */
update dim_date_factory_calendar dt
  set dt.BusinessDaysSeqNo = s.dateseqno
from dimdateseqtemp s, dim_date_factory_calendar dt
where  dt.dim_dateid = s.dim_dateid
and dt.companycode = s.companycode 
and dt.plantcode_factory = s.plantcode_factory
and dt.BusinessDaysSeqNo <> s.dateseqno;

Insert into dimdateseqtemp(companycode,dim_dateid,datevalue,isapublicholiday,isaweekendday,dateseqno,plantcode_factory)
select companycode,dim_dateid, datevalue,isapublicholiday,isaweekendday,0 DtSeqNo,plantcode_factory
from dim_date_factory_calendar dt
where dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1 ;


/* Update BusinessDaysSeqNo for holidays/weekends where previous day is a business day */
/*update dim_date_factory_calendar dt 
from dimdateseqtemp s 
  set dt.BusinessDaysSeqNo = s.dateseqno
where  dt.datevalue - 1 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1 */

/* Update BusinessDaysSeqNo for holidays/weekends where previous day is also a non-business day */

/*update dim_date_factory_calendar dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 2 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 2 days are non-business days */
/*update dim_date_factory_calendar dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 3 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 3 days are non-business days */
/*update dim_date_factory_calendar dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 4 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */

/* Update BusinessDaysSeqNo for holidays/weekends where previous 4 days are non-business days */
/*update dim_date_factory_calendar dt
from dimdateseqtemp s
  set dt.BusinessDaysSeqNo = s.dateseqno
where dt.BusinessDaysSeqNo = 0
AND dt.datevalue - 5 = s.datevalue
and dt.companycode = s.companycode
AND dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1  */


/*
update dim_date_factory_calendar dt
set dt.BusinessDaysSeqNo = (select max(s.dateseqno) from dimdateseqtemp s where s.DateValue <= dt.DateValue and s.companycode=dt.companycode)
where (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1)*/

drop table if exists tmpseq_dimdateseqtemp;
CREATE TABLE tmpseq_dimdateseqtemp AS 
select a.companycode,a.datevalue,a.plantcode_factory,a.max_dateseqno from
(SELECT companycode, 
        datevalue, 
        plantcode_factory, 
		isapublicholiday,
		isaweekendday,
		Max(dateseqno) over (partition by companycode,plantcode_factory order by datevalue ROWS
		BETWEEN UNBOUNDED PRECEDING AND current row) max_dateseqno 
 FROM dimdateseqtemp 
 ) a
where a.isapublicholiday = 1 
      OR a.isaweekendday = 1;
 --@Catalin
UPDATE 	dim_date_factory_calendar dt
SET dt.BusinessDaysSeqNo = s.max_dateseqno
FROM tmpseq_dimdateseqtemp s, dim_date_factory_calendar dt
WHERE s.DateValue = dt.datevalue AND s.companycode=dt.companycode and dt.plantcode_factory = s.plantcode_factory
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1)
AND ifnull(dt.BusinessDaysSeqNo,-1) <> ifnull(s.max_dateseqno,-2);


/* Handle the case where the first date or first few days were non-business days */
UPDATE dim_date_factory_calendar
SET BusinessDaysSeqNo = 0
WHERE BusinessDaysSeqNo IS NULL;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycount;
CREATE TABLE tmp_dim_date_weekdaycount
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,CalendarYear,
row_number() over( PARTITION BY CompanyCode,CalendarYear order by d2.datevalue) WeekdaysInCalendarYearSofar ,plantcode_factory--@Catalin
from dim_date_factory_calendar d2
WHERE d2.IsaWeekendday = 0;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycountfinyr;
CREATE TABLE tmp_dim_date_weekdaycountfinyr
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,FinancialYear,
row_number() over( PARTITION BY CompanyCode,FinancialYear order by d2.datevalue) WeekdaysInFinancialYearSofar ,plantcode_factory--@Catalin
from dim_date_factory_calendar d2
WHERE d2.IsaWeekendday = 0;


DROP TABLE IF EXISTS tmp_dim_date_weekdaycountmonth;
CREATE TABLE tmp_dim_date_weekdaycountmonth
AS
SELECT DISTINCT d2.datevalue,d2.datevalue date_value_next, d2.datevalue date_value_next_2,
CompanyCode,CalendarYear,MonthName,
row_number() over( PARTITION BY CompanyCode,CalendarYear,MonthName order by d2.datevalue) WeekdaysInMonthSoFar ,plantcode_factory--@Catalin
from dim_date_factory_calendar d2
WHERE d2.IsaWeekendday = 0;


UPDATE tmp_dim_date_weekdaycount
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycount
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountfinyr
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountfinyr
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountmonth
SET date_value_next = date_value_next + 1
WHERE datevalue < '9999-12-31';

UPDATE tmp_dim_date_weekdaycountmonth
SET date_value_next_2 = date_value_next_2 + 2
WHERE datevalue < '9999-12-31';


DROP TABLE IF EXISTS tmp_weekenddays;
CREATE TABLE tmp_weekenddays
AS
SELECT distinct datevalue
FROM dim_date_factory_calendar 
WHERE IsaWeekendday = 1;

/* Update WeekdaysInCalendarYearSofar*/
merge into dim_date_factory_calendar dim
using (select  dim_dateid, max(w.WeekdaysInCalendarYearSofar) WeekdaysInCalendarYearSofar
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycount w on d.datevalue = w.datevalue AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory			
group by dim_dateid
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.WeekdaysInCalendarYearSofar = src.WeekdaysInCalendarYearSofar
where dim.WeekdaysInCalendarYearSofar <> src.WeekdaysInCalendarYearSofar;

/*UPDATE dim_date_factory_calendar d
SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
FROM tmp_dim_date_weekdaycount w, dim_date_factory_calendar d
WHERE d.datevalue = w.datevalue
AND d.CompanyCode = w.CompanyCode
and d.plantcode_factory = w.plantcode_factory 
AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar*/


merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid, w.WeekdaysInCalendarYearSofar
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycount w on d.datevalue = w.date_value_next AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.WeekdaysInCalendarYearSofar = src.WeekdaysInCalendarYearSofar
where dim.WeekdaysInCalendarYearSofar <> src.WeekdaysInCalendarYearSofar;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
				FROM tmp_dim_date_weekdaycount w, tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = w.date_value_next
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar*/
			
merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid, w.WeekdaysInCalendarYearSofar,d.plantcode_factory,d.companycode
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycount w on d.datevalue = w.date_value_next_2 AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )
	  ) src on dim.dim_dateid = src.dim_dateid and dim.plantcode_factory  =src.plantcode_factory and dim.companycode = src.companycode
when matched then update set dim.WeekdaysInCalendarYearSofar = src.WeekdaysInCalendarYearSofar
where dim.WeekdaysInCalendarYearSofar <> src.WeekdaysInCalendarYearSofar;

			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.WeekdaysInCalendarYearSofar = w.WeekdaysInCalendarYearSofar
				FROM tmp_dim_date_weekdaycount w,tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = w.date_value_next_2
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
				AND d.WeekdaysInCalendarYearSofar <> w.WeekdaysInCalendarYearSofar */ /* Check that the previous day was also a weekend day */

/* Update weekdaysinfinancialyearsofar*/

merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid, max(w.weekdaysinfinancialyearsofar) weekdaysinfinancialyearsofar,d.plantcode_factory,d.companycode
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycountfinyr w on d.datevalue = w.datevalue AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory
					group by d.dim_dateid,d.plantcode_factory,d.companycode 
 ) src on dim.dim_dateid = src.dim_dateid  and dim.plantcode_factory = src.plantcode_factory and dim.companycode = src.companycode
when matched then update set dim.weekdaysinfinancialyearsofar = src.weekdaysinfinancialyearsofar 
where dim.weekdaysinfinancialyearsofar <> src.weekdaysinfinancialyearsofar;

/*UPDATE dim_date_factory_calendar d
SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
FROM tmp_dim_date_weekdaycountfinyr w, dim_date_factory_calendar d
WHERE d.datevalue = w.datevalue
AND d.CompanyCode = w.CompanyCode 
and d.plantcode_factory = w.plantcode_factory 
AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar*/

merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid, w.weekdaysinfinancialyearsofar,d.plantcode_factory,d.companycode
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycountfinyr w on d.datevalue = w.date_value_next AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	  ) src on dim.dim_dateid = src.dim_dateid and dim.plantcode_factory = src.plantcode_factory and dim.companycode = src.companycode
when matched then update set dim.weekdaysinfinancialyearsofar = src.weekdaysinfinancialyearsofar 
where dim.weekdaysinfinancialyearsofar <> src.weekdaysinfinancialyearsofar;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
				FROM tmp_dim_date_weekdaycountfinyr w, tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = w.date_value_next
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				and d.plantcode_factory = w.plantcode_factory
				AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar */

merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid, w.weekdaysinfinancialyearsofar,d.plantcode_factory ,d.companycode
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycountfinyr w on d.datevalue = w.date_value_next_2 AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )
	  ) src on dim.dim_dateid = src.dim_dateid and dim.plantcode_factory = src.plantcode_factory and dim.companycode = src.companycode
when matched then update set dim.weekdaysinfinancialyearsofar = src.weekdaysinfinancialyearsofar
where dim.weekdaysinfinancialyearsofar <> src.weekdaysinfinancialyearsofar;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.weekdaysinfinancialyearsofar = w.weekdaysinfinancialyearsofar
				FROM tmp_dim_date_weekdaycountfinyr w,tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = w.date_value_next_2
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
				AND d.weekdaysinfinancialyearsofar <> w.weekdaysinfinancialyearsofar */ /* Check that the previous day was also a weekend day */

/* Reset to 0 where its the 1st month of a financial year and 1st is Saturday */

merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid
       from dim_date_factory_calendar d
				inner join tmp_weekenddays e on d.datevalue = e.datevalue 
	   where    substring(d.FinancialMonthID,5,2) = '01'
		    AND DAY(d.datevalue) = 1
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinfinancialyearsofar = 0
where dim.weekdaysinfinancialyearsofar <> 0;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.weekdaysinfinancialyearsofar = 0
				FROM tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = e.datevalue
				AND substring(d.FinancialMonthID,5,2) = '01'
				AND DAY(d.datevalue) = 1
				AND d.weekdaysinfinancialyearsofar <> 0 */

/* Reset to 0 where its the 1st month of a financial year and 1st or 2nd is Sunday */
merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid
       from dim_date_factory_calendar d
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where    substring(d.FinancialMonthID,5,2) = '01'
		    AND DAY(d.datevalue) IN ( 1,2)
			AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinfinancialyearsofar = 0
where dim.weekdaysinfinancialyearsofar <> 0;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.weekdaysinfinancialyearsofar = 0
				FROM tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = e.datevalue
				AND substring(FinancialMonthID,5,2) = '01'
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
				AND date_format(d.datevalue,'%e') IN ( 1,2)
				AND d.weekdaysinfinancialyearsofar <> 0 */

/* Update weekdaysinmonthsofar */

merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid, max(w.weekdaysinmonthsofar) weekdaysinmonthsofar
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycountmonth w on d.datevalue = w.datevalue AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory
					group by d.dim_dateid	
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = src.weekdaysinmonthsofar
where dim.weekdaysinmonthsofar <> src.weekdaysinmonthsofar;

/*UPDATE dim_date_factory_calendar d
SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
FROM tmp_dim_date_weekdaycountmonth w, dim_date_factory_calendar d
WHERE d.datevalue = w.datevalue 
AND d.CompanyCode = w.CompanyCode
and d.plantcode_factory = w.plantcode_factory 
AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar*/

/* Update for Saturdays */
merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid, w.weekdaysinmonthsofar
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycountmonth w on d.datevalue = w.date_value_next AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = src.weekdaysinmonthsofar
where dim.weekdaysinmonthsofar <> src.weekdaysinmonthsofar;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
				FROM tmp_dim_date_weekdaycountmonth w, tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = w.date_value_next
				AND d.CompanyCode = w.CompanyCode
				AND d.datevalue = e.datevalue
				AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar*/

/* Update for Sundays */
merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid, w.weekdaysinmonthsofar
       from dim_date_factory_calendar d
				inner join tmp_dim_date_weekdaycountmonth w on d.datevalue = w.date_value_next_2 AND d.CompanyCode = w.CompanyCode and d.plantcode_factory = w.plantcode_factory
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 ) 
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = src.weekdaysinmonthsofar
where dim.weekdaysinmonthsofar <> src.weekdaysinmonthsofar;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.weekdaysinmonthsofar = w.weekdaysinmonthsofar
				FROM tmp_dim_date_weekdaycountmonth w,tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = w.date_value_next_2
				AND d.datevalue = e.datevalue
				AND d.CompanyCode = w.CompanyCode
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 WHERE e2.datevalue = e.datevalue - 1 )  
				AND d.weekdaysinmonthsofar <> w.weekdaysinmonthsofar */ /* Check that the previous day was also a weekend day */

/* Reset to 0 where its a Sat and the 1st of a month */

merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid
       from dim_date_factory_calendar d
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where    DAY(d.datevalue) = 1
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = 0
where dim.weekdaysinmonthsofar <> 0;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.weekdaysinmonthsofar = 0
				FROM tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = e.datevalue
				AND date_format(d.datevalue,'%e') = 1
				AND d.weekdaysinmonthsofar <> 0 */ 

/* Reset to 0 where its a Sunday and 1st or 2nd of a month */
merge into dim_date_factory_calendar dim
using (select distinct d.dim_dateid
       from dim_date_factory_calendar d
				inner join tmp_weekenddays e on d.datevalue = e.datevalue
	   where    DAY(d.datevalue) IN ( 1,2)
		    AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 ) 
	  ) src on dim.dim_dateid = src.dim_dateid
when matched then update set dim.weekdaysinmonthsofar = 0
where dim.weekdaysinmonthsofar <> 0;
			/* VW Original:
				UPDATE dim_date_factory_calendar d
				SET d.weekdaysinmonthsofar = 0
				FROM tmp_weekenddays e, dim_date_factory_calendar d
				WHERE d.datevalue = e.datevalue
				AND EXISTS ( SELECT 1 from tmp_weekenddays e2 where e2.datevalue = d.datevalue - 1 )
				AND date_format(d.datevalue,'%e') IN ( 1,2)
				AND d.weekdaysinmonthsofar <> 0 */


/* workdaysincalendaryearsofar eqivalent to weekdaysincalendaryearsofar 
workdaysinfinancialyearsofar equivalent to weekdaysinfinancialyearsofar */

UPDATE dim_date_factory_calendar
SET workdaysincalendaryearsofar = WeekdaysInCalendarYearSofar;

UPDATE dim_date_factory_calendar
SET workdaysinfinancialyearsofar = weekdaysinfinancialyearsofar;


UPDATE dim_date_factory_calendar
SET workdaysinmonthsofar = weekdaysinmonthsofar;

/* Update values for default dates */
UPDATE dim_date_factory_calendar
SET weekdaysincalendaryearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date_factory_calendar
SET weekdaysinfinancialyearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date_factory_calendar
SET workdaysincalendaryearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date_factory_calendar
SET workdaysinfinancialyearsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date_factory_calendar
SET weekdaysinmonthsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date_factory_calendar
SET workdaysinmonthsofar = 0
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );


UPDATE dim_date_factory_calendar
SET WeekStartDate = '0001-01-01' 
WHERE dim_dateid = 1;

UPDATE dim_date_factory_calendar
SET WeekEndDate = '0001-01-06' 
WHERE dim_dateid = 1;

UPDATE dim_date_factory_calendar
SET MonthStartDate = '0001-01-01' 
WHERE dim_dateid = 1;


UPDATE dim_date_factory_calendar
SET MonthEndDate = '9999-01-01' 
WHERE dim_dateid = 1;

UPDATE dim_date_factory_calendar
SET FinancialMonthStartDate = '0001-01-01' 
WHERE dim_dateid = 1;

UPDATE dim_date_factory_calendar
SET FinancialMonthEndDate = '9999-01-01' 
WHERE dim_dateid = 1;

UPDATE dim_date_factory_calendar
SET WeekStartDate = '9998-12-27' 
WHERE datevalue IN ( '9999-01-01','9999-12-31');

UPDATE dim_date_factory_calendar
SET WeekEndDate = '9999-01-02' 
WHERE datevalue IN ( '9999-01-01','9999-12-31');

UPDATE dim_date_factory_calendar
SET MonthStartDate = '9999-01-01' 
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date_factory_calendar
SET MonthEndDate = '9999-12-31' 
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date_factory_calendar
SET FinancialMonthStartDate = '9999-01-01' 
WHERE datevalue IN ( '9999-01-01', '9999-12-31' );

UPDATE dim_date_factory_calendar
SET FinancialMonthEndDate = '9999-12-31' 
WHERE datevalue IN ( '9999-01-01','9999-12-31');


UPDATE dim_date_factory_calendar
SET DaysInFinancialYear = financialyearenddate - financialyearstartdate + 1
where DaysInFinancialYear <> financialyearenddate - financialyearstartdate + 1;

drop table if exists tmp_finworkdays_001;
create table tmp_finworkdays_001 as
select companycode, financialyear, count(*) workdaysfinyr 
from dim_date_factory_calendar 
where isaweekendday = 0 and isapublicholiday = 0
group by companycode, financialyear;

update dim_date_factory_calendar dt
set dt.weekdaysinfinancialyear = a.workdaysfinyr
from tmp_finworkdays_001 a, dim_date_factory_calendar dt
where dt.companycode = a.companycode and dt.financialyear = a.financialyear
and dt.weekdaysinfinancialyear <> a.workdaysfinyr;

update dim_date_factory_calendar dt
set dt.workdaysinfinancialyear = a.workdaysfinyr
from tmp_finworkdays_001 a, dim_date_factory_calendar dt
where dt.companycode = a.companycode and dt.financialyear = a.financialyear
and dt.workdaysinfinancialyear <> a.workdaysfinyr;

/* Fix daysinfinancialyearsofar */
UPDATE dim_date_factory_calendar
SET daysinfinancialyearsofar = current_date - financialyearstartdate + 1
WHERE daysinfinancialyearsofar <> current_date - financialyearstartdate + 1;

/* financialweek should be the weekno from the start of 0th weekstart of that financial year. Week starts from 0 */
/* For example, if the first week starts on 30th March 2014,1 to 5 Apr will be finweek 0 and 6th April 2014 will be in financialweek 1 */
UPDATE dim_date_factory_calendar a
SET financialweek = FLOOR((a.datevalue - b.weekstartdate)/7)
FROM dim_date_factory_calendar b, dim_date_factory_calendar a
WHERE b.companycode = a.companycode AND a.financialyear = b.financialyear AND b.datevalue = b.financialyearstartdate
AND a.PLANTCODE_FACTORY = b.PLANTCODE_FACTORY
AND a.financialweek <> FLOOR((a.datevalue - b.weekstartdate)/7);

UPDATE dim_date_factory_calendar
SET financialweekid = financialyear * 100 + financialweek
WHERE financialweekid <> financialyear * 100 + financialweek;


UPDATE dim_date_factory_calendar
SET DayofFinancialYear = daysinfinancialyearsofar
WHERE DayofFinancialYear <> daysinfinancialyearsofar;


/* Update Financial Half Data */

/* By default financialhalf = 1. Set it to 2 where financialmonthstartdate is atleast 6 months more than financialyearstartdate */
UPDATE dim_date_factory_calendar dt
SET financialhalf = 2 
where months_between(financialmonthstartdate,financialyearstartdate) >= 6
AND financialhalf <> 2;


Update dim_date_factory_calendar dt
SET financialhalfid = (financialyear * 10) + financialhalf
Where  financialhalfid <> (financialyear * 10) + financialhalf;

Update dim_date_factory_calendar dt
SET financialhalfname = concat(financialyear,' H ',CAST(financialhalf AS CHAR (1)))
WHERE financialhalfname <> concat(financialyear,' H ',CAST(financialhalf AS CHAR (1)));

UPDATE dim_date_factory_calendar dt
SET financialhalfstartdate = financialyearstartdate
WHERE financialhalf = 1
AND ifnull(financialhalfstartdate,to_date('1902-01-01','YYYY-MM-DD')) <> ifnull(financialyearstartdate,to_date('1903-01-01','YYYY-MM-DD') );

UPDATE dim_date_factory_calendar dt
SET financialhalfstartdate = financialyearstartdate + interval '6' MONTH
WHERE financialhalf = 2
AND ifnull(financialhalfstartdate,to_date('1902-01-01','YYYY-MM-DD')) <> ifnull(financialyearstartdate + interval '6' MONTH,to_date('1903-01-01','YYYY-MM-DD'))
AND dt.datevalue <> to_date('9999-12-31','YYYY-MM-DD');

UPDATE dim_date_factory_calendar dt
SET financialhalfenddate = financialhalfstartdate + interval '6' MONTH - interval '1' day
WHERE dt.datevalue <> to_date('9999-12-31','YYYY-MM-DD')
      and ifnull(financialhalfenddate,to_date('1902-01-01','YYYY-MM-DD')) <>  ifnull(financialhalfstartdate + interval '6' MONTH - interval '1' day,to_date('1903-01-01','YYYY-MM-DD'));


UPDATE dim_date_factory_calendar dt
SET financialhalfenddate = to_date('9999-12-31','YYYY-MM-DD')
WHERE dt.datevalue = to_date('9999-12-31','YYYY-MM-DD')
AND financialhalfenddate <> to_date('9999-12-31','YYYY-MM-DD');

UPDATE dim_date_factory_calendar dt
SET financialhalfstartdate = to_date('9999-12-31','YYYY-MM-DD')
WHERE dt.datevalue = to_date('9999-12-31','YYYY-MM-DD')
AND financialhalfstartdate <> to_date('9999-12-31','YYYY-MM-DD');


/* Marius Update dw_update_date when dim_date_factory_calendar is processed */
UPDATE dim_date_factory_calendar dt
set dt.dw_update_date = current_timestamp;
drop table if exists tmp_finworkdays_001;
drop TABLE if exists dimdateseqtemp ;
drop TABLE if exists tmp_dimdate_dim_year_del;
drop TABLE if exists tmp_dimdate_dim_year_ins;
drop TABLE if exists tmp_dimdate_dim_year_ins1;
drop TABLE if exists tmp_dim_date_weekdaycount;
drop TABLE if exists tmp_dim_date_weekdaycountfinyr;
drop TABLE if exists tmp_dim_date_weekdaycountmonth;
drop TABLE if exists tmp_weekenddays;
drop TABLE if exists tmp_ffyr_minbutag;
drop TABLE if exists TMP_FFY_T009B_1;
drop TABLE if exists TMP_FFY_T009B_2;
drop TABLE if exists TMP_T009B_1;
drop TABLE if exists TMP_T009B_2;
drop TABLE if exists TMP_T009B_2a;
drop TABLE if exists TMP_T009B_3A;
drop TABLE if exists TMP_T009B_3A_TO;
drop TABLE if exists TMP_T009B_3B;
drop TABLE if exists TMP_T009B_3B_TO;
drop TABLE if exists TMP_T009B_4A;
drop TABLE if exists TMP_T009B_4A_TO;
drop TABLE if exists TMP_T009B_4B;
drop TABLE if exists TMP_T009B_4B_TO;
drop TABLE if exists TMP_TO_3;
drop TABLE if exists TMP_TO_3B;
drop TABLE if exists TMP_TO_4;
drop TABLE if exists TMP_TO_4B;
DROP TABLE IF EXISTS tmpseq_dimdateseqtemp; --@Catalin


DROP TABLE IF EXISTS tmp_dim_date_factory;
CREATE TABLE tmp_dim_date_factory
AS
SELECT * FROM dim_date_factory_calendar WHERE 1=2;

/* New companies with 'Not Set' plant and previous rows with given plant are already populated in dim_date_factory_calendar */
/* Next - only those plants which are new in dim_plant and which have an associated companycode in dim_company need to be populated */

/* Get new plants */
DROP TABLE IF EXISTS tmp_existingplants_dim_date;
CREATE TABLE tmp_existingplants_dim_date
as
select distinct plantcode_factory ,companycode
from dim_date_factory_calendar;

DROP TABLE IF EXISTS tmp_newplants_dim_date;
CREATE TABLE tmp_newplants_dim_date
as
select plantcode,companycode 
from dim_plant pl
where not exists ( select 1 from tmp_existingplants_dim_date t where t.plantcode_factory = pl.plantcode and t.companycode = pl.companycode );

/* Get distinct rows where plantcode is 'Not Set' and companycode belongs to one of the new plantcodes */
/* Its assumed that plantcode-companycode mapping does not change. If it does, then both old and new plant-company combinations will exist in dim_date_factory_calendar */
/* Note: Plantcode + companycode is the key.So, If there's a plantcode in tmp_newplants_dim_date that already exists in dim_date_factory_calendar with different companycode , then new rows for plant+newcompany will be populated. Old rows cannot be deleted as those dim_dateid's will be already assigned to some facts and deleting old rows from dim_date_factory_calendar will need historic refresh to fix such rows */

DROP TABLE IF EXISTS tmp_dim_date_noplant;
CREATE TABLE tmp_dim_date_noplant
AS
SELECT * FROM dim_date_factory_calendar d
WHERE plantcode_factory = 'Not Set'
AND EXISTS ( SELECT 1 FROM tmp_newplants_dim_date t where t.companycode = d.companycode );

INSERT INTO tmp_dim_date_factory
(
dim_dateid,
DIM_DATE_FACTORY_CALENDARID,
datevalue,
juliandate,
calendaryear,
financialyear,
calendarquarter,
calendarquarterid,
calendarquartername,
financialquarter,
financialquarterid,
financialquartername,
season,
calendarmonthid,
calendarmonthnumber,
monthname,
monthabbreviation,
financialmonthid,
financialmonthnumber,
calendarweekid,
calendarweek,
financialweekid,
financialweek,
dayofcalendaryear,
dayoffinancialyear,
dayofmonth,
weekdaynumber,
weekdayname,
weekdayabbreviation,
isaweekendday,
isapublicholiday,
isaleapyear,
isaspecialday,
daysincalendaryear,
daysinfinancialyear,
weekdaysincalendaryear,
weekdaysinfinancialyear,
workdaysincalendaryear,
workdaysinfinancialyear,
daysincalendaryearsofar,
daysinfinancialyearsofar,
weekdaysincalendaryearsofar,
weekdaysinfinancialyearsofar,
workdaysincalendaryearsofar,
workdaysinfinancialyearsofar,
daysinmonth,
weekdaysinmonth,
workdaysinmonth,
daysinmonthsofar,
weekdaysinmonthsofar,
workdaysinmonthsofar,
companycode,
plantcode_factory,
datename,
monthyear,
weekstartdate,
weekenddate,
monthstartdate,
monthenddate,
financialmonthstartdate,
financialmonthenddate,
calendarweekyr,
financialmonthyear,
businessdaysseqno,
financialyearstartdate,
financialyearenddate,
weekenddatethursday,
financialhalf,
financialhalfid,
financialhalfname,
financialhalfstartdate,
financialhalfenddate
)
SELECT
( SELECT MAX(dim_dateid) from dim_date_factory_calendar) + 
row_number() over(ORDER BY d.companycode,pl.plantcode,d.datevalue) dim_dateid,
-99999,
datevalue,
juliandate,
calendaryear,
financialyear,
calendarquarter,
calendarquarterid,
calendarquartername,
financialquarter,
financialquarterid,
financialquartername,
season,
calendarmonthid,
calendarmonthnumber,
monthname,
monthabbreviation,
financialmonthid,
financialmonthnumber,
calendarweekid,
calendarweek,
financialweekid,
financialweek,
dayofcalendaryear,
dayoffinancialyear,
dayofmonth,
weekdaynumber,
weekdayname,
weekdayabbreviation,
isaweekendday,
isapublicholiday,
isaleapyear,
isaspecialday,
daysincalendaryear,
daysinfinancialyear,
weekdaysincalendaryear,
weekdaysinfinancialyear,
workdaysincalendaryear,
workdaysinfinancialyear,
daysincalendaryearsofar,
daysinfinancialyearsofar,
weekdaysincalendaryearsofar,
weekdaysinfinancialyearsofar,
workdaysincalendaryearsofar,
workdaysinfinancialyearsofar,
daysinmonth,
weekdaysinmonth,
workdaysinmonth,
daysinmonthsofar,
weekdaysinmonthsofar,
workdaysinmonthsofar,
d.companycode,
ifnull(pl.plantcode,'Not Set'),
datename,
monthyear,
weekstartdate,
weekenddate,
monthstartdate,
monthenddate,
financialmonthstartdate,
financialmonthenddate,
calendarweekyr,
financialmonthyear,
businessdaysseqno,
financialyearstartdate,
financialyearenddate,
weekenddatethursday,
financialhalf,
financialhalfid,
financialhalfname,
financialhalfstartdate,
financialhalfenddate
FROM tmp_dim_date_noplant d INNER JOIN tmp_newplants_dim_date pl
ON d.companycode = pl.companycode;

/* For these new plants, update the factory specific columns e.g publicholiday and businessdaysseqno */
UPDATE tmp_dim_date_factory a
SET a.IsaPublicHoliday = CASE SUBSTRING(case a.CalendarMonthNumber 

                                              when 1 then t.TFACS_MON01
                                              when 2 then t.TFACS_MON02
                                              when 3 then t.TFACS_MON03
                                              when 4 then t.TFACS_MON04
                                              when 5 then t.TFACS_MON05
                                              when 6 then t.TFACS_MON06
                                              when 7 then t.TFACS_MON07
                                              when 8 then t.TFACS_MON08
                                              when 9 then t.TFACS_MON09
                                              when 10 then t.TFACS_MON10
                                              when 11 then t.TFACS_MON11
                                              when 12 then t.TFACS_MON12
                                          end, a.DayofMonth, 1) WHEN '0' then 1 else 0 END
From TFACS t ,dim_plant pl,tmp_dim_date_factory a
Where pl.FactoryCalendarKey = t.TFACS_IDENT AND a.CompanyCode = pl.CompanyCode and t.TFACS_JAHR = a.CalendarYear
AND a.plantcode_factory = pl.plantcode;


/* business days sequence calculation */

drop table if exists dimdateseqtemp;
create table dimdateseqtemp (
companycode varchar(10) null,
plantcode varchar(20) null,
dim_dateid integer null,
datevalue timestamp null,
nextbusdate timestamp null,
dateseqno integer null);

Insert into dimdateseqtemp(companycode,plantcode,dim_dateid,datevalue,dateseqno)
select companycode,plantcode_factory,dim_dateid, datevalue, row_number() over(partition by companycode,plantcode_factory order by DateValue)  DtSeqNo
from tmp_dim_date_factory dt
where dt.IsaPublicHoliday = 0 and dt.IsaWeekendday = 0 ;

UPDATE dimdateseqtemp c
SET c.nextbusdate = nxt.datevalue
FROM dimdateseqtemp nxt,dimdateseqtemp c
WHERE c.companycode = nxt.companycode AND c.plantcode = nxt.plantcode
AND nxt.dateseqno = c.dateseqno + 1;


/* Update BusinessDaysSeqNo for business days */
update tmp_dim_date_factory dt
  set dt.BusinessDaysSeqNo = s.dateseqno
  from dimdateseqtemp s,tmp_dim_date_factory dt
where  dt.dim_dateid = s.dim_dateid
and dt.companycode = s.companycode  AND dt.plantcode_factory = s.plantcode
and dt.BusinessDaysSeqNo <> s.dateseqno;

DROP TABLE IF EXISTS tmp_tmp_dim_date_factory;
CREATE TABLE tmp_tmp_dim_date_factory
AS
SELECT * from tmp_dim_date_factory dt
WHERE (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1);

DROP TABLE IF EXISTS tmp_dimdateseqtemp_1;
CREATE TABLE tmp_dimdateseqtemp_1
AS
SELECT * FROM dimdateseqtemp s WHERE s.nextbusdate > add_days(s.datevalue,0);


DROP TABLE IF EXISTS tmp_dimdateseqtemp;
CREATE TABLE tmp_dimdateseqtemp
AS
SELECT dt.companycode,dt.plantcode_factory plantcode,dt.DateValue,s.dateseqno max_dateseqno
FROM tmp_tmp_dim_date_factory dt, tmp_dimdateseqtemp_1 s
WHERE s.companycode=dt.companycode AND s.plantcode = dt.plantcode_factory
AND dt.DateValue > s.datevalue AND dt.DateValue < s.nextbusdate;

UPDATE 	tmp_dim_date_factory dt
SET dt.BusinessDaysSeqNo = s.max_dateseqno
FROM tmp_dimdateseqtemp s,tmp_dim_date_factory dt
WHERE s.DateValue = dt.datevalue AND s.companycode=dt.companycode AND s.plantcode = dt.plantcode_factory
AND (dt.IsaPublicHoliday = 1 or dt.IsaWeekendday = 1)
AND ifnull(dt.BusinessDaysSeqNo,-1) <> ifnull(s.max_dateseqno,-2);


/* Handle the case where the first date or first few days were non-business days */
UPDATE tmp_dim_date_factory
SET BusinessDaysSeqNo = 0
WHERE BusinessDaysSeqNo IS NULL;

/* Populate default date for all plantcodes */
/*** This is not required. Wherever default date is required, just use 1 in the fact population scripts ***/

DROP TABLE IF EXISTS tmp_tmp_dim_date_factory;
DROP TABLE IF EXISTS tmp_dimdateseqtemp;
DROP TABLE IF EXISTS tmp_dimdateseqtemp_1;
drop table if exists dimdateseqtemp;

/* Insert all rows for new plants that appeared in dim_plant(and have a valid companycode in dim_company */ 

update tmp_dim_date_factory set nextworkingdate = '0001-01-01' where nextworkingdate is null;
INSERT INTO dim_date_factory_calendar
SELECT * FROM tmp_dim_date_factory where plantcode_factory <>'1779ZZ';


DROP TABLE IF EXISTS tmp_swap_default_date_dimdate;
CREATE TABLE tmp_swap_default_date_dimdate
AS
SELECT * FROM dim_date_factory_calendar
WHERE dim_dateid = 1;

UPDATE tmp_swap_default_date_dimdate t
SET t.dim_dateid = d.dim_dateid
FROM dim_date_factory_calendar d,tmp_swap_default_date_dimdate t
WHERE d.datevalue = '0001-01-01'
AND d.companycode = 'Not Set'
AND d.plantcode_factory = 'Not Set';

DELETE FROM dim_date_factory_calendar
WHERE dim_dateid = 1;

UPDATE dim_date_factory_calendar
SET dim_dateid = 1
WHERE datevalue = '0001-01-01'
AND companycode = 'Not Set'
AND plantcode_factory = 'Not Set';

INSERT INTO dim_date_factory_calendar
SELECT * FROM tmp_swap_default_date_dimdate;


drop table if exists tmp_nextworkingday;
create table tmp_nextworkingday
as
select 
	DIM_DATEID
	,datevalue
	,ISAPUBLICHOLIDAY
	,ISAWEEKENDDAY
	,companycode
	,PLANTCODE_FACTORY
	,min(case when ISAPUBLICHOLIDAY = 1 OR ISAWEEKENDDAY = 1 then '9999-12-31' else datevalue end) over (partition by companycode,PLANTCODE_FACTORY order by datevalue rows between current row and unbounded following) next_working_day
from dim_date_factory_calendar
where year(datevalue) between 2009 and 2026;

merge into dim_Date_factory_calendar d using
(
select 
	a.DIM_DATEID
	,a.datevalue
	,a.ISAPUBLICHOLIDAY
	,a.ISAWEEKENDDAY
	,a.companycode
	,a.PLANTCODE_FACTORY
	,b.next_working_day
from tmp_nextworkingday a
	inner join tmp_nextworkingday b on a.companycode = b.companycode and a.PLANTCODE_FACTORY = b.PLANTCODE_FACTORY and a.datevalue = b.datevalue - interval '1' day

) t on d.dim_dateid = t.dim_dateid
when matched then update set d.nextworkingdate = t.next_working_day;

drop table if exists tmp_lastworkingday;
create table tmp_lastworkingday
as
select
	DIM_DATEID
	,datevalue
	,ISAPUBLICHOLIDAY
	,ISAWEEKENDDAY
	,companycode
	,PLANTCODE_FACTORY
	,max(case when ISAPUBLICHOLIDAY = 1 OR ISAWEEKENDDAY = 1 then '0001-01-01' else datevalue end)
		over (partition by companycode,PLANTCODE_FACTORY order by datevalue rows between unbounded preceding and current row) last_working_day
from dim_date_factory_calendar
where year(datevalue) between 2009 and 2026;

merge into dim_Date_factory_calendar d using
(
select
	a.DIM_DATEID
	,a.datevalue
	,a.ISAPUBLICHOLIDAY
	,a.ISAWEEKENDDAY
	,a.companycode
	,a.PLANTCODE_FACTORY
	,b.last_working_day
from tmp_lastworkingday a
	inner join tmp_lastworkingday b on a.companycode = b.companycode and a.PLANTCODE_FACTORY = b.PLANTCODE_FACTORY and a.datevalue = b.datevalue + interval '1' day
) t on d.dim_dateid = t.dim_dateid
when matched then update set d.last_working_day = t.last_working_day;

update dim_date_factory_calendar
set DIM_DATE_FACTORY_CALENDARID = dim_Dateid 
where DIM_DATE_FACTORY_CALENDARID <> dim_dateid;

/* Expression test  */

update dim_date_factory_calendar
set CalendarWeekYr2 = replace(CalendarWeekYr,'-','')
where CalendarWeekYr2 <> replace(CalendarWeekYr,'-','');

update dim_date_factory_calendar
set IsaWeekendday2 = case when IsaWeekendday= 1 then 'Yes' else 'No' end
where IsaWeekendday2 <> case when IsaWeekendday= 1 then 'Yes' else 'No' end;

update dim_date_factory_calendar
set IsaPublicHoliday2 = case when IsaPublicHoliday= 1 then 'Yes' else 'No' end
where IsaPublicHoliday2 <> case when IsaPublicHoliday= 1 then 'Yes' else 'No' end;

update dim_date_factory_calendar
set IsaLeapYear2 = case when IsaLeapYear= 1 then 'Yes' else 'No' end
where IsaLeapYear2 <> case when IsaLeapYear= 1 then 'Yes' else 'No' end;

update dim_date_factory_calendar
set YearMonth = concat (left(calendarmonthid,4),'-',right(calendarmonthid,2))
where YearMonth <> concat (left(calendarmonthid,4),'-',right(calendarmonthid,2));

UPDATE dim_date_factory_calendar a
SET a.IsaPublicHoliday = CASE SUBSTRING(case a.CalendarMonthNumber

                                              when 1 then t.TFACS_MON01
                                              when 2 then t.TFACS_MON02
                                              when 3 then t.TFACS_MON03
                                              when 4 then t.TFACS_MON04
                                              when 5 then t.TFACS_MON05
                                              when 6 then t.TFACS_MON06
                                              when 7 then t.TFACS_MON07
                                              when 8 then t.TFACS_MON08
                                              when 9 then t.TFACS_MON09
                                              when 10 then t.TFACS_MON10
                                              when 11 then t.TFACS_MON11
                                              when 12 then t.TFACS_MON12
                                          end, a.DayofMonth, 1) WHEN '0' then 1 else 0 END
From TFACS t ,dim_plant pl,dim_date_factory_calendar a
Where pl.FactoryCalendarKey = t.TFACS_IDENT AND a.CompanyCode = pl.CompanyCode and t.TFACS_JAHR = a.CalendarYear
AND a.plantcode_factory = pl.plantcode;

drop table if exists tmp_nextworkingday;
create table tmp_nextworkingday
as
select
	DIM_DATEID
	,datevalue
	,ISAPUBLICHOLIDAY
	,ISAWEEKENDDAY
	,companycode
	,PLANTCODE_FACTORY
	,min(case when ISAPUBLICHOLIDAY = 1 OR ISAWEEKENDDAY = 1 then '9999-12-31' else datevalue end) over (partition by companycode,PLANTCODE_FACTORY order by datevalue rows between current row and unbounded following) next_working_day
from dim_date_factory_calendar
where year(datevalue) between 2009 and 2026;

merge into dim_Date_factory_calendar d using
(
select
	a.DIM_DATEID
	,a.datevalue
	,a.ISAPUBLICHOLIDAY
	,a.ISAWEEKENDDAY
	,a.companycode
	,a.PLANTCODE_FACTORY
	,b.next_working_day
from tmp_nextworkingday a
	inner join tmp_nextworkingday b on a.companycode = b.companycode and a.PLANTCODE_FACTORY = b.PLANTCODE_FACTORY and a.datevalue = b.datevalue - interval '1' day

) t on d.dim_dateid = t.dim_dateid
when matched then update set d.nextworkingdate = t.next_working_day;



DROP TABLE IF EXISTS tmp_tmp_dim_date_factory;
DROP TABLE IF EXISTS tmp_dimdateseqtemp_1;
DROP TABLE IF EXISTS tmp_dimdateseqtemp;
DROP TABLE IF EXISTS tmp_swap_default_date_dimdate;
DROP TABLE IF EXISTS tmp_tmp_dim_date_factory;
DROP TABLE IF EXISTS tmp_dimdateseqtemp;
DROP TABLE IF EXISTS tmp_dimdateseqtemp_1;
drop table if exists dimdateseqtemp;
drop table if exists tmp_distinctcompanycode;
DROP TABLE IF EXISTS tmp_existingplants_dim_date;
DROP TABLE IF EXISTS tmp_newplants_dim_date;
DROP TABLE IF EXISTS tmp_nextworkingday;
