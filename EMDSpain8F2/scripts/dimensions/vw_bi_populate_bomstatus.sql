INSERT INTO dim_bomstatus(dim_bomstatusid, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_bomstatus
               WHERE dim_bomstatusid = 1);

UPDATE  dim_bomstatus bs
SET 	bs.Description = ifnull(T415T_STTXT, 'Not Set'),
		bs.dw_update_date = current_timestamp
FROM	T415T t, dim_bomstatus bs
WHERE 	bs.RowIsCurrent = 1
		AND bs.bomstatuscode = T415T_STLST;

delete from number_fountain m where m.table_name = 'dim_bomstatus';

insert into number_fountain
select 	'dim_bomstatus',
	ifnull(max(d.dim_bomstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_bomstatus d
where d.dim_bomstatusid <> 1;

INSERT INTO dim_bomstatus(Dim_bomstatusid,
                                Description,
                                bomstatuscode,
                                RowStartDate,
                                RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_bomstatus')
          +  row_number() over(order by ''),
                        ifnull(T415T_STTXT, 'Not Set'),
            T415T_STLST,
            current_timestamp,
            1
       FROM T415T
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_bomstatus
                    WHERE bomstatuscode = T415T_STLST)
   ORDER BY 2 ;

delete from number_fountain m where m.table_name = 'dim_bomstatus';
