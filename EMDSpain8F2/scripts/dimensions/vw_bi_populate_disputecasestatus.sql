update 
dim_DisputeCaseStatus d
set d.Description = ifnull(SCMGSTATPROFST_STAT_ORDNO_DESCR,''),
	d.dw_update_date = current_timestamp
from 
dim_DisputeCaseStatus d,
SCMGSTATPROFST s
where d.RowIsCurrent = 1
and d.DisputeCaseStatus = ifnull(s.SCMGSTATPROFST_STAT_ORDERNO, '1') 
and d.DisputeCaseProfileId = ifnull(s.SCMGSTATPROFST_PROFILE_ID,'Not Set')
and d.Description <> ifnull(SCMGSTATPROFST_STAT_ORDNO_DESCR,'');

INSERT INTO dim_DisputeCaseStatus(
dim_DisputeCaseStatusid, DisputeCaseProfileId, DisputeCaseStatus, Description, 
RowStartDate,RowIsCurrent)
SELECT 1, 1, 1, 'Not Set', current_timestamp, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_DisputeCaseStatus
               WHERE dim_DisputeCaseStatusid = 1);

delete from number_fountain m where m.table_name = 'dim_DisputeCaseStatus';
   
insert into number_fountain
select 	'dim_DisputeCaseStatus',
	ifnull(max(d.dim_DisputeCaseStatusId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_DisputeCaseStatus d
where d.dim_DisputeCaseStatusId <> 1; 

INSERT INTO dim_DisputeCaseStatus (dim_DisputeCaseStatusId,
DisputeCaseProfileId,
DisputeCaseStatus,
Description,
RowStartDate,
RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_DisputeCaseStatus')
		+ row_number() over(order by '') ,
	ifnull(SCMGSTATPROFST_PROFILE_ID,'Not Set'),
	ifnull(SCMGSTATPROFST_STAT_ORDERNO, '1'),
	ifnull(SCMGSTATPROFST_STAT_ORDNO_DESCR,''),
	current_timestamp,
	1
FROM SCMGSTATPROFST s
WHERE NOT EXISTS 
(SELECT 1 
FROM dim_DisputeCaseStatus x 
WHERE x.DisputeCaseStatus = ifnull(s.SCMGSTATPROFST_STAT_ORDERNO, '1') 
and x.DisputeCaseProfileId = ifnull(s.SCMGSTATPROFST_PROFILE_ID,'Not Set')
);

delete from number_fountain m where m.table_name = 'dim_DisputeCaseStatus';