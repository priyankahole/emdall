/* 	Server: QA
	Process Name: WM Picking Area Transfer
	Interface No: 2
*/
INSERT INTO dim_wmpickingarea
(dim_wmpickingareaid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmpickingarea where dim_wmpickingareaid = 1);

UPDATE    
dim_wmpickingarea wmpa
SET  wmpa.PickingAreaName = t.T30AT_KBERT,
	 wmpa.dw_update_date = current_timestamp
FROM
dim_wmpickingarea wmpa,
T30AT t
WHERE wmpa.RowIsCurrent = 1
AND   wmpa.StorageType = t.T30AT_LGTYP
AND   wmpa.PickingArea = t.T30AT_KOBER
AND   wmpa.WarehouseNumber = t.T30AT_LGNUM;
 
delete from number_fountain m where m.table_name = 'dim_wmpickingarea';

insert into number_fountain				   
select 	'dim_wmpickingarea',
	ifnull(max(d.dim_wmpickingareaid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmpickingarea d
where d.dim_wmpickingareaid <> 1;

INSERT INTO dim_wmpickingarea(dim_wmpickingareaid,
							WarehouseNumber,
							StorageType,
							PickingArea,
                            PickingAreaName,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmpickingarea')   + row_number() over(order by ''),
		  t.T30AT_LGNUM,
          t.T30AT_KOBER,		  
          t.T30AT_LGTYP,
		  t.T30AT_KBERT,
          current_timestamp,
          1
     FROM T30AT t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_wmpickingarea s
                  WHERE    s.StorageType = t.T30AT_LGTYP
					    AND s.WarehouseNumber = t.T30AT_LGNUM
						AND s.PickingArea = t.T30AT_KOBER
                        AND s.RowIsCurrent = 1) ;
