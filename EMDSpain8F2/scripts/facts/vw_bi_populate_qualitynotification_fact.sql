/**************************************************************************************************************/
/*   Script         : vw_bi_populate_quality_notification_fact.sql                                                */
/*   Author         : Marius Closcaru                                                                                */
/*   Created On     : 17 Nov 2015                                                                            */ 
/*   Description    : Migratre script from App Manager to EXASol */
/**************************************************************************************************************/
/*  Date             By         Version	    Desc                                                               */
/**************************************************************************************************************/
/*17 Nov 2015        Marius     1.0         Consolidated App Manager scripts and moved to EXASol*/


/* Delete Object Status */

DELETE FROM jest
      WHERE NOT EXISTS (SELECT 1
                          FROM qmel
                         WHERE qmel_objnr = JEST_OBJNR)
            AND NOT EXISTS (SELECT 1
                              FROM fact_qualitynotification qn
                             WHERE qn.dd_ObjectNumber = JEST_OBJNR);
							 
/* Create Temp Table for QN Update */

DELETE FROM number_fountain m 
WHERE m.table_name = 'fact_qualitynotification';

INSERT INTO number_fountain
SELECT 'fact_qualitynotification',
       ifnull(MAX(fq.fact_qualitynotificationid), ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_qualitynotification fq;

DROP TABLE IF EXISTS tmp_fact_qualitynotification;
CREATE TABLE tmp_fact_qualitynotification
AS
SELECT
	qn.fact_qualitynotificationid fact_qualitynotificationid   
	,IFNULL(QMEL_RKMNG, 0) ct_complaintQty
	,IFNULL(QMEL_MGFRD, 0) ct_ExternalDefectiveQty
	,IFNULL(QMEL_MGEIG, 0) ct_InternalDefectiveQty
	,IFNULL(QMEL_RGMNG, 0) ct_returndeliveryqty
	,IFNULL(QMEL_MGFRD, 0) ct_QtyRejected
    ,CAST(1 AS INT) ct_LotRejected
	,IFNULL(QMEL_BUNAME, 'Not Set') dd_author
	,IFNULL(QMEL_CHARG, 'Not Set') dd_batchno
	,IFNULL(QMEL_AENAM, 'Not Set') dd_changedby
	,IFNULL(QMEL_ERNAM, 'Not Set') dd_createdby
	,IFNULL(QMEL_EBELP, 0) dd_documentitemno
	,IFNULL(QMEL_EBELN, 'Not Set') dd_documentno
	,IFNULL(QMEL_PRUEFLOS, 'Not Set') dd_InspectionLotNo
	,IFNULL(QMEL_MBLPO, 0) dd_MaterialDocItemNo
	,IFNULL(QMEL_MBLNR, 'Not Set') dd_MaterialDocNo
	,IFNULL(QMEL_MJAHR, 0) dd_MaterialDocYear
	,IFNULL(QMEL_QMNUM, 'Not Set') dd_notificationo
	,IFNULL(QMEL_AUFNR, 'Not Set') dd_orderno
	,IFNULL(QMEL_FERTAUFNR, 'Not Set') dd_productionorderno
	,IFNULL(QMEL_FERTAUFPL, 0) dd_productionorderplanno
	,IFNULL(QMEL_COAUFNR, 'Not Set') dd_qmorderno
	,IFNULL(QMEL_AEDAT, '0001-01-01') dd_RecordChanged
	,IFNULL(QMEL_ERDAT, '0001-01-01') dd_RecordCreated
	,IFNULL(QMEL_QWRNUM, 'Not Set') dd_referencenotificationo
	,IFNULL(QMEL_QMNAM, 'Not Set') dd_reportedby
	,IFNULL(QMEL_LS_POSNR, 0) dd_SalesOrderDeliveryItemNo
	,IFNULL(QMEL_LS_VBELN, 'Not Set') dd_salesorderdeliveryno
	,IFNULL(QMEL_LS_KDPOS, 0) dd_SalesOrderItemNo
	,IFNULL(QMEL_LS_KDAUF, 'Not Set') dd_SalesOrderNo
	,IFNULL(QMEL_VBELN, 'Not Set') dd_SalesOrderNo2
	,IFNULL(QMEL_SERIALNR, 'Not Set') dd_serialno
	,IFNULL(QMEL_OBJNR, 'Not Set') dd_objectnumber
	,CAST(1 AS BIGINT) dim_batchstoragelocationid
	,CAST(1 AS BIGINT) dim_catalogprofileid
	,CAST(1 AS BIGINT) dim_currencyid
	,CAST(1 AS BIGINT) dim_customerid
	,CAST(1 AS BIGINT) dim_dateidnotificationcompletion
	,CAST(1 AS BIGINT) dim_dateidnotificationdate
	,CAST(1 AS BIGINT) dim_dateidreturndate
	,CAST(1 AS BIGINT) Dim_dateidrecordcreated
	,CAST(1 AS BIGINT) dim_defectreporttypeid
	,CAST(1 AS BIGINT) dim_distributionchannelid
	,CAST(1 AS BIGINT) dim_inspectioncatalogtypeid
	,CAST(1 AS BIGINT) dim_inspectionlotstoragelocationid
	,CAST(1 AS BIGINT) dim_MaterialGroupid
    ,CAST(1 AS BIGINT) dim_notificationoriginid
    ,CAST(1 AS BIGINT) dim_notificationphaseid
    ,CAST(1 AS BIGINT) dim_notificationpriorityid
	,CAST(1 AS BIGINT) dim_notificationtypeid
    ,CAST(1 AS BIGINT) dim_partid
	,CAST(1 AS BIGINT) dim_plantid
    ,CAST(1 AS BIGINT) dim_producthierarchyid
    ,CAST(1 AS BIGINT) dim_purchasegroupid
    ,CAST(1 AS BIGINT) dim_purchaseorgid
    ,CAST(1 AS BIGINT) Dim_SalesDivisionid
    ,CAST(1 AS BIGINT) dim_SalesGroupid
    ,CAST(1 AS BIGINT) dim_SalesOfficeid
    ,CAST(1 AS BIGINT) dim_SalesOrgid
    ,CAST(1 AS BIGINT) Dim_UnitOfMeasureid
    ,CAST(1 AS BIGINT) dim_vendorid
    ,CAST(1 AS BIGINT) dim_workplantid
    ,CAST(1 AS BIGINT) dim_qualitynotificationmiscid
	,IFNULL(MNCOD, 'Not Set') dd_taskcode
	,IFNULL(PARNRKU, 'Not Set') dd_Coordinator
	,IFNULL(PARNRZV, 'Not Set') dd_Dispositioner
	,CAST(1 AS BIGINT) dim_dateidtaskcreated
    ,CAST(1 AS BIGINT) dim_dateiddispositionerassigned
	,CAST(1 AS BIGINT) dim_dateidtaskcompleted
	,IFNULL(ERNAM, 'Not Set') dd_taskcreatedby
	,IFNULL(ERLNAM, 'Not Set') dd_taskcompletedby
	,(CASE WHEN QMEL_QMDAB IS NULL THEN 2 ELSE 3 END) Dim_ActionStateid
	,'Not Set' dd_ZmrbFlag
	,IFNULL(QMEL_LTRMN, '0001-01-01') dd_tobecompletedon
	,CAST(1 AS BIGINT) dim_dateidrequestedstart
	,ps.DIM_PROJECTSOURCEID AS DIM_PROJECTSOURCEID
	,CAST(1 AS BIGINT) dim_mdg_partid
	,CAST(1 AS BIGINT) dim_bwproducthierarchyid
	,QMEL_OBJNR --ct_LotRejected
	,QMEL_LGORTCHARG --dim_batchstoragelocationid
	,QMEL_MAWERK --dim_batchstoragelocationid dim_inspectionlotstoragelocationid dim_partid dim_plantid
	,QMEL_RBNR --dim_catalogprofileid
	,QMEL_WAERS --dim_currencyid
	,QMEL_KUNUM --dim_customerid
	,QMEL_QMDAB --dim_dateidnotificationcompletion
	,QMEL_QMDAT --dim_dateidnotificationdate
	,QMEL_RKDAT --dim_dateidreturndate
	,QMEL_ERDAT --Dim_dateidrecordcreated
	,QMEL_FEART --dim_defectreporttypeid
	,QMEL_VTWEG --dim_distributionchannelid
	,QMEL_QMKAT --dim_inspectioncatalogtypeid
	,QMEL_LGORTVORG --dim_inspectionlotstoragelocationid
	,QMEL_MATKL --dim_MaterialGroupid
	,QMEL_HERKZ --dim_notificationoriginid
	,QMEL_PHASE --dim_notificationphaseid
	,QMEL_PRIOK --dim_notificationpriorityid
	,QMEL_ARTPR --dim_notificationpriorityid
	,QMEL_QMART --dim_notificationtypeid
	,QMEL_MATNR --dim_partid
	,QMEL_PRDHA --dim_producthierarchyid
	,QMEL_BKGRP --dim_purchasegroupid
	,QMEL_EKORG --dim_purchaseorgid
	,QMEL_SPART --Dim_SalesDivisionid
	,QMEL_VKGRP --dim_SalesGroupid
	,QMEL_VKBUR --dim_SalesOfficeid
	,QMEL_VKORG --dim_SalesOrgid
	,QMEL_MGEIN --Dim_UnitOfMeasureid
	,QMEL_LIFNUM --dim_vendorid
	,QMEL_ARBPLWERK --dim_workplantid
	,QMEL_MAKNZ --dim_qualitynotificationmiscid
	,QMEL_KZLOESCH --dim_qualitynotificationmiscid
	,QMEL_KZKRI --dim_qualitynotificationmiscid
	,QMEL_KZDKZ --dim_qualitynotificationmiscid
	,QMEL_FEKNZ --dim_qualitynotificationmiscid
	,ERDAT --dim_dateidtaskcreated
	,ERDATZV --dim_dateiddispositionerassigned
	,ERLDAT --dim_dateidtaskcompleted
	,QMEL_STRMN --dim_dateidrequestedstart
FROM QMEL q1
	,dim_projectsource ps, fact_qualitynotification qn where qn.dd_notificationo = q1.QMEL_QMNUM
UNION
SELECT
	ifnull(nf.max_id, 1) + row_number() over (order by '') fact_qualitynotificationid   
	,IFNULL(QMEL_RKMNG, 0) ct_complaintQty
	,IFNULL(QMEL_MGFRD, 0) ct_ExternalDefectiveQty
	,IFNULL(QMEL_MGEIG, 0) ct_InternalDefectiveQty
	,IFNULL(QMEL_RGMNG, 0) ct_returndeliveryqty
	,IFNULL(QMEL_MGFRD, 0) ct_QtyRejected
    ,CAST(1 AS INT) ct_LotRejected
	,IFNULL(QMEL_BUNAME, 'Not Set') dd_author
	,IFNULL(QMEL_CHARG, 'Not Set') dd_batchno
	,IFNULL(QMEL_AENAM, 'Not Set') dd_changedby
	,IFNULL(QMEL_ERNAM, 'Not Set') dd_createdby
	,IFNULL(QMEL_EBELP, 0) dd_documentitemno
	,IFNULL(QMEL_EBELN, 'Not Set') dd_documentno
	,IFNULL(QMEL_PRUEFLOS, 'Not Set') dd_InspectionLotNo
	,IFNULL(QMEL_MBLPO, 0) dd_MaterialDocItemNo
	,IFNULL(QMEL_MBLNR, 'Not Set') dd_MaterialDocNo
	,IFNULL(QMEL_MJAHR, 0) dd_MaterialDocYear
	,IFNULL(QMEL_QMNUM, 'Not Set') dd_notificationo
	,IFNULL(QMEL_AUFNR, 'Not Set') dd_orderno
	,IFNULL(QMEL_FERTAUFNR, 'Not Set') dd_productionorderno
	,IFNULL(QMEL_FERTAUFPL, 0) dd_productionorderplanno
	,IFNULL(QMEL_COAUFNR, 'Not Set') dd_qmorderno
	,IFNULL(QMEL_AEDAT, '0001-01-01') dd_RecordChanged
	,IFNULL(QMEL_ERDAT, '0001-01-01') dd_RecordCreated
	,IFNULL(QMEL_QWRNUM, 'Not Set') dd_referencenotificationo
	,IFNULL(QMEL_QMNAM, 'Not Set') dd_reportedby
	,IFNULL(QMEL_LS_POSNR, 0) dd_SalesOrderDeliveryItemNo
	,IFNULL(QMEL_LS_VBELN, 'Not Set') dd_salesorderdeliveryno
	,IFNULL(QMEL_LS_KDPOS, 0) dd_SalesOrderItemNo
	,IFNULL(QMEL_LS_KDAUF, 'Not Set') dd_SalesOrderNo
	,IFNULL(QMEL_VBELN, 'Not Set') dd_SalesOrderNo2
	,IFNULL(QMEL_SERIALNR, 'Not Set') dd_serialno
	,IFNULL(QMEL_OBJNR, 'Not Set') dd_objectnumber
	,CAST(1 AS BIGINT) dim_batchstoragelocationid
	,CAST(1 AS BIGINT) dim_catalogprofileid
	,CAST(1 AS BIGINT) dim_currencyid
	,CAST(1 AS BIGINT) dim_customerid
	,CAST(1 AS BIGINT) dim_dateidnotificationcompletion
	,CAST(1 AS BIGINT) dim_dateidnotificationdate
	,CAST(1 AS BIGINT) dim_dateidreturndate
	,CAST(1 AS BIGINT) Dim_dateidrecordcreated
	,CAST(1 AS BIGINT) dim_defectreporttypeid
	,CAST(1 AS BIGINT) dim_distributionchannelid
	,CAST(1 AS BIGINT) dim_inspectioncatalogtypeid
	,CAST(1 AS BIGINT) dim_inspectionlotstoragelocationid
	,CAST(1 AS BIGINT) dim_MaterialGroupid
    ,CAST(1 AS BIGINT) dim_notificationoriginid
    ,CAST(1 AS BIGINT) dim_notificationphaseid
    ,CAST(1 AS BIGINT) dim_notificationpriorityid
	,CAST(1 AS BIGINT) dim_notificationtypeid
    ,CAST(1 AS BIGINT) dim_partid
	,CAST(1 AS BIGINT) dim_plantid
    ,CAST(1 AS BIGINT) dim_producthierarchyid
    ,CAST(1 AS BIGINT) dim_purchasegroupid
    ,CAST(1 AS BIGINT) dim_purchaseorgid
    ,CAST(1 AS BIGINT) Dim_SalesDivisionid
    ,CAST(1 AS BIGINT) dim_SalesGroupid
    ,CAST(1 AS BIGINT) dim_SalesOfficeid
    ,CAST(1 AS BIGINT) dim_SalesOrgid
    ,CAST(1 AS BIGINT) Dim_UnitOfMeasureid
    ,CAST(1 AS BIGINT) dim_vendorid
    ,CAST(1 AS BIGINT) dim_workplantid
    ,CAST(1 AS BIGINT) dim_qualitynotificationmiscid
	,IFNULL(MNCOD, 'Not Set') dd_taskcode
	,IFNULL(PARNRKU, 'Not Set') dd_Coordinator
	,IFNULL(PARNRZV, 'Not Set') dd_Dispositioner
	,CAST(1 AS BIGINT) dim_dateidtaskcreated
    ,CAST(1 AS BIGINT) dim_dateiddispositionerassigned
	,CAST(1 AS BIGINT) dim_dateidtaskcompleted
	,IFNULL(ERNAM, 'Not Set') dd_taskcreatedby
	,IFNULL(ERLNAM, 'Not Set') dd_taskcompletedby
	,(CASE WHEN QMEL_QMDAB IS NULL THEN 2 ELSE 3 END) Dim_ActionStateid
	,'Not Set' dd_ZmrbFlag
	,IFNULL(QMEL_LTRMN, '0001-01-01') dd_tobecompletedon
	,CAST(1 AS BIGINT) dim_dateidrequestedstart
	,ps.DIM_PROJECTSOURCEID AS DIM_PROJECTSOURCEID
	,CAST(1 AS BIGINT) dim_mdg_partid
	,CAST(1 AS BIGINT) dim_bwproducthierarchyid
	,QMEL_OBJNR --ct_LotRejected
	,QMEL_LGORTCHARG --dim_batchstoragelocationid
	,QMEL_MAWERK --dim_batchstoragelocationid dim_inspectionlotstoragelocationid dim_partid dim_plantid
	,QMEL_RBNR --dim_catalogprofileid
	,QMEL_WAERS --dim_currencyid
	,QMEL_KUNUM --dim_customerid
	,QMEL_QMDAB --dim_dateidnotificationcompletion
	,QMEL_QMDAT --dim_dateidnotificationdate
	,QMEL_RKDAT --dim_dateidreturndate
	,QMEL_ERDAT --Dim_dateidrecordcreated
	,QMEL_FEART --dim_defectreporttypeid
	,QMEL_VTWEG --dim_distributionchannelid
	,QMEL_QMKAT --dim_inspectioncatalogtypeid
	,QMEL_LGORTVORG --dim_inspectionlotstoragelocationid
	,QMEL_MATKL --dim_MaterialGroupid
	,QMEL_HERKZ --dim_notificationoriginid
	,QMEL_PHASE --dim_notificationphaseid
	,QMEL_PRIOK --dim_notificationpriorityid
	,QMEL_ARTPR --dim_notificationpriorityid
	,QMEL_QMART --dim_notificationtypeid
	,QMEL_MATNR --dim_partid
	,QMEL_PRDHA --dim_producthierarchyid
	,QMEL_BKGRP --dim_purchasegroupid
	,QMEL_EKORG --dim_purchaseorgid
	,QMEL_SPART --Dim_SalesDivisionid
	,QMEL_VKGRP --dim_SalesGroupid
	,QMEL_VKBUR --dim_SalesOfficeid
	,QMEL_VKORG --dim_SalesOrgid
	,QMEL_MGEIN --Dim_UnitOfMeasureid
	,QMEL_LIFNUM --dim_vendorid
	,QMEL_ARBPLWERK --dim_workplantid
	,QMEL_MAKNZ --dim_qualitynotificationmiscid
	,QMEL_KZLOESCH --dim_qualitynotificationmiscid
	,QMEL_KZKRI --dim_qualitynotificationmiscid
	,QMEL_KZDKZ --dim_qualitynotificationmiscid
	,QMEL_FEKNZ --dim_qualitynotificationmiscid
	,ERDAT --dim_dateidtaskcreated
	,ERDATZV --dim_dateiddispositionerassigned
	,ERLDAT --dim_dateidtaskcompleted
	,QMEL_STRMN --dim_dateidrequestedstart
FROM QMEL q1
	,dim_projectsource ps
	INNER JOIN number_fountain nf ON nf.table_name = 'fact_qualitynotification'
WHERE q1.QMEL_QMNUM NOT IN (SELECT DISTINCT dd_notificationo FROM fact_qualitynotification);

UPDATE tmp_fact_qualitynotification f
SET f.dim_plantid = pl.dim_plantid
FROM tmp_fact_qualitynotification f, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK 
	AND pl.RowIsCurrent = 1
	AND f.dim_plantid <> pl.dim_plantid;
	
UPDATE tmp_fact_qualitynotification f
SET f.ct_LotRejected = 0
WHERE f.QMEL_OBJNR IN (SELECT DISTINCT j.JEST_OBJNR FROM JEST j WHERE j.JEST_STAT IN ('I0076', 'I0072') AND j.JEST_INACT IS NULL)
	AND QMEL_OBJNR IS NOT NULL
	AND f.ct_LotRejected <> 0;

UPDATE tmp_fact_qualitynotification f
SET f.dim_batchstoragelocationid = sl1.Dim_StorageLocationid
FROM tmp_fact_qualitynotification f, dim_storagelocation sl1
WHERE sl1.LocationCode = f.QMEL_LGORTCHARG
	AND sl1.RowIsCurrent = 1
	AND sl1.Plant = IFNULL(f.QMEL_MAWERK, 'Not Set')
	AND f.QMEL_LGORTCHARG IS NOT NULL
	AND f.dim_batchstoragelocationid <> sl1.Dim_StorageLocationid;
	
UPDATE tmp_fact_qualitynotification f
SET f.Dim_CatalogProfileid = cp.Dim_CatalogProfileid
FROM tmp_fact_qualitynotification f, dim_catalogprofile cp
WHERE cp.CatalogProfileCode = IFNULL(f.QMEL_RBNR, 'Not Set')
	AND cp.RowIsCurrent = 1
	AND f.Dim_CatalogProfileid <> cp.Dim_CatalogProfileid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_currencyid = dc.Dim_Currencyid
FROM tmp_fact_qualitynotification f, dim_currency dc
WHERE dc.CurrencyCode = IFNULL(f.QMEL_WAERS, 'Not Set')
	AND f.dim_currencyid <> dc.Dim_Currencyid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_customerid = c.dim_customerid
FROM tmp_fact_qualitynotification f, dim_customer c
WHERE c.CustomerNumber = IFNULL(f.QMEL_KUNUM, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_customerid <> c.dim_customerid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_dateidnotificationcompletion = dnc.dim_dateid
FROM tmp_fact_qualitynotification f, dim_date dnc, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND dnc.DateValue = f.QMEL_QMDAB
	AND dnc.CompanyCode = pl.CompanyCode
	AND f.QMEL_QMDAB IS NOT NULL
	AND f.dim_dateidnotificationcompletion <> dnc.dim_dateid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_dateidnotificationdate = dnc.dim_dateid
FROM tmp_fact_qualitynotification f, dim_date dnc, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND dnc.DateValue = f.QMEL_QMDAT
	AND dnc.CompanyCode = pl.CompanyCode
	AND f.QMEL_QMDAT IS NOT NULL
	AND f.dim_dateidnotificationdate <> dnc.dim_dateid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_dateidreturndate = dnc.dim_dateid
FROM tmp_fact_qualitynotification f, dim_date dnc, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND dnc.DateValue = f.QMEL_RKDAT
	AND dnc.CompanyCode = pl.CompanyCode
	AND f.QMEL_RKDAT IS NOT NULL
	AND f.dim_dateidreturndate <> dnc.dim_dateid;
	
UPDATE tmp_fact_qualitynotification f
SET f.Dim_dateidrecordcreated = dnc.dim_dateid
FROM tmp_fact_qualitynotification f, dim_date dnc, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND dnc.DateValue = f.QMEL_ERDAT
	AND dnc.CompanyCode = pl.CompanyCode
	AND f.QMEL_ERDAT IS NOT NULL
	AND f.Dim_dateidrecordcreated <> dnc.dim_dateid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_defectreporttypeid = c.dim_defectreporttypeid
FROM tmp_fact_qualitynotification f, dim_defectreporttype c
WHERE c.DefectReportTypeCode = IFNULL(f.QMEL_FEART, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_defectreporttypeid <> c.dim_defectreporttypeid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_distributionchannelid = c.dim_distributionchannelid
FROM tmp_fact_qualitynotification f, dim_distributionchannel c
WHERE c.DistributionChannelCode = IFNULL(f.QMEL_VTWEG, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_distributionchannelid <> c.dim_distributionchannelid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_inspectioncatalogtypeid = c.dim_inspectioncatalogtypeid
FROM tmp_fact_qualitynotification f, dim_inspectioncatalogtype c
WHERE c.InspectionCatalogTypeCode = IFNULL(f.QMEL_QMKAT, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_inspectioncatalogtypeid <> c.dim_inspectioncatalogtypeid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_inspectionlotstoragelocationid = c.dim_storagelocationid
FROM tmp_fact_qualitynotification f, dim_storagelocation c
WHERE c.LocationCode = f.QMEL_LGORTVORG
	AND c.Plant = IFNULL(f.QMEL_MAWERK, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.QMEL_LGORTVORG IS NOT NULL
	AND f.dim_inspectionlotstoragelocationid <> c.dim_storagelocationid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_MaterialGroupid = c.dim_MaterialGroupid
FROM tmp_fact_qualitynotification f, dim_MaterialGroup c
WHERE c.MaterialGroupCode = IFNULL(f.QMEL_MATKL, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_MaterialGroupid <> c.dim_MaterialGroupid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_notificationoriginid = c.dim_notificationoriginid
FROM tmp_fact_qualitynotification f, dim_notificationorigin c
WHERE c.NotificationOriginCode = IFNULL(f.QMEL_HERKZ, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_notificationoriginid <> c.dim_notificationoriginid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_notificationphaseid = c.dim_notificationphaseid
FROM tmp_fact_qualitynotification f, dim_notificationphase c
WHERE c.NotificationPhaseCode = IFNULL(f.QMEL_PHASE, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_notificationphaseid <> c.dim_notificationphaseid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_notificationpriorityid = c.dim_notificationpriorityid
FROM tmp_fact_qualitynotification f, dim_notificationpriority c
WHERE c.NotificationPriorityCode = IFNULL(f.QMEL_PRIOK, 'Not Set')
	AND c.NotificationPriorityType = IFNULL(f.QMEL_ARTPR, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_notificationpriorityid <> c.dim_notificationpriorityid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_notificationtypeid = c.dim_notificationtypeid
FROM tmp_fact_qualitynotification f, dim_notificationtype c
WHERE c.NotificationTypeCode = IFNULL(f.QMEL_QMART, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_notificationtypeid <> c.dim_notificationtypeid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_partid = dnc.dim_partid
FROM tmp_fact_qualitynotification f, dim_part dnc
WHERE dnc.PartNumber = IFNULL(f.QMEL_MATNR, 'Not Set')
	AND dnc.Plant = IFNULL(f.QMEL_MAWERK, 'Not Set')
	AND f.dim_partid <> dnc.dim_partid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_producthierarchyid = c.dim_producthierarchyid
FROM tmp_fact_qualitynotification f, dim_producthierarchy c
WHERE c.ProductHierarchy = IFNULL(f.QMEL_PRDHA, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_producthierarchyid <> c.dim_producthierarchyid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_purchasegroupid = c.dim_purchasegroupid
FROM tmp_fact_qualitynotification f, dim_purchasegroup c
WHERE c.PurchaseGroup = IFNULL(f.QMEL_BKGRP, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_purchasegroupid <> c.dim_purchasegroupid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_purchaseorgid = c.dim_purchaseorgid
FROM tmp_fact_qualitynotification f, dim_purchaseorg c
WHERE c.PurchaseOrgCode = IFNULL(f.QMEL_EKORG, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_purchaseorgid <> c.dim_purchaseorgid;
	
UPDATE tmp_fact_qualitynotification f
SET f.Dim_SalesDivisionid = c.Dim_SalesDivisionid
FROM tmp_fact_qualitynotification f, Dim_SalesDivision c
WHERE c.DivisionCode = IFNULL(f.QMEL_SPART, 'Not Set')
	AND f.Dim_SalesDivisionid <> c.Dim_SalesDivisionid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_SalesGroupid = c.dim_SalesGroupid
FROM tmp_fact_qualitynotification f, dim_SalesGroup c
WHERE c.SalesGroupCode = IFNULL(f.QMEL_VKGRP, 'Not Set')
	AND f.dim_SalesGroupid <> c.dim_SalesGroupid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_SalesOfficeid = c.dim_SalesOfficeid
FROM tmp_fact_qualitynotification f, dim_SalesOffice c
WHERE c.SalesOfficeCode = IFNULL(f.QMEL_VKBUR, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_SalesOfficeid <> c.dim_SalesOfficeid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_SalesOrgid = c.dim_SalesOrgid
FROM tmp_fact_qualitynotification f, dim_SalesOrg c
WHERE c.SalesOrgCode = IFNULL(f.QMEL_VKORG, 'Not Set')
	AND f.dim_SalesOrgid <> c.dim_SalesOrgid;
	
UPDATE tmp_fact_qualitynotification f
SET f.Dim_UnitOfMeasureid = c.Dim_UnitOfMeasureid
FROM tmp_fact_qualitynotification f, Dim_UnitOfMeasure c
WHERE c.UOM = IFNULL(f.QMEL_MGEIN, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.Dim_UnitOfMeasureid <> c.Dim_UnitOfMeasureid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_vendorid = c.dim_vendorid
FROM tmp_fact_qualitynotification f, dim_vendor c
WHERE c.VendorNumber = IFNULL(f.QMEL_LIFNUM, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_vendorid <> c.dim_vendorid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_workplantid = c.dim_plantid
FROM tmp_fact_qualitynotification f, dim_plant c
WHERE c.PlantCode = IFNULL(f.QMEL_ARBPLWERK, 'Not Set')
	AND c.RowIsCurrent = 1
	AND f.dim_workplantid <> c.dim_plantid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_qualitynotificationmiscid = c.dim_qualitynotificationmiscid
FROM tmp_fact_qualitynotification f, dim_qualitynotificationmisc c
WHERE c.TaskRecordsExist = ifnull(f.QMEL_MAKNZ, 'Not Set')
	AND c.DeleteDataRecord = ifnull(f.QMEL_KZLOESCH, 'Not Set')
	AND c.CriticalPart = ifnull(f.QMEL_KZKRI, 'Not Set')
	AND c.DocumentationRequired = ifnull(f.QMEL_KZDKZ, 'Not Set')
	AND c.ErrorRecordsExist = ifnull(f.QMEL_FEKNZ, 'Not Set')
	AND f.dim_qualitynotificationmiscid <> c.dim_qualitynotificationmiscid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_dateidtaskcreated = dnc.dim_dateid
FROM tmp_fact_qualitynotification f, dim_date dnc, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND dnc.DateValue = f.ERDAT
	AND dnc.CompanyCode = pl.CompanyCode
	AND f.ERDAT IS NOT NULL
	AND f.dim_dateidtaskcreated <> dnc.dim_dateid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_dateiddispositionerassigned = dnc.dim_dateid
FROM tmp_fact_qualitynotification f, dim_date dnc, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND dnc.DateValue = f.ERDATZV
	AND dnc.CompanyCode = pl.CompanyCode
	AND f.ERDATZV IS NOT NULL
	AND f.dim_dateiddispositionerassigned <> dnc.dim_dateid;
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_dateidtaskcompleted = dnc.dim_dateid
FROM tmp_fact_qualitynotification f, dim_date dnc, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND dnc.DateValue = f.ERLDAT
	AND dnc.CompanyCode = pl.CompanyCode
	AND f.ERLDAT IS NOT NULL
	AND f.dim_dateidtaskcompleted <> dnc.dim_dateid;
	
UPDATE tmp_fact_qualitynotification f
SET dd_ZmrbFlag = 'X'
FROM tmp_fact_qualitynotification f, QMSM q
WHERE q.QMSM_MNCOD = 'ZMRB' AND q.QMSM_QMNUM = f.QMEL_OBJNR
	AND q.QMSM_KZLOESCH IS NULL
	AND dd_ZmrbFlag <> 'X';
	
UPDATE tmp_fact_qualitynotification f
SET f.dim_dateidrequestedstart = dnc.dim_dateid
FROM tmp_fact_qualitynotification f, dim_date dnc, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND dnc.DateValue = f.QMEL_STRMN
	AND dnc.CompanyCode = pl.CompanyCode
	AND f.QMEL_STRMN IS NOT NULL
	AND f.dim_dateidrequestedstart <> dnc.dim_dateid;

	
MERGE INTO fact_qualitynotification f
USING tmp_fact_qualitynotification t ON f.fact_qualitynotificationid = t.fact_qualitynotificationid
WHEN MATCHED THEN UPDATE SET 
	f.ct_complaintQty = t.ct_complaintQty
	,f.ct_ExternalDefectiveQty = t.ct_ExternalDefectiveQty
	,f.ct_InternalDefectiveQty = t.ct_InternalDefectiveQty
	,f.ct_returndeliveryqty = t.ct_returndeliveryqty
	,f.ct_QtyRejected = t.ct_QtyRejected
	,f.ct_LotRejected = t.ct_LotRejected
	,f.dd_author = t.dd_author
	,f.dd_batchno = t.dd_batchno
	,f.dd_changedby = t.dd_changedby
	,f.dd_createdby = t.dd_createdby
	,f.dd_documentitemno = t.dd_documentitemno
	,f.dd_documentno = t.dd_documentno
	,f.dd_InspectionLotNo = t.dd_InspectionLotNo
	,f.dd_MaterialDocItemNo = t.dd_MaterialDocItemNo
	,f.dd_MaterialDocNo = t.dd_MaterialDocNo
	,f.dd_MaterialDocYear = t.dd_MaterialDocYear
	,f.dd_notificationo = t.dd_notificationo
	,f.dd_orderno = t.dd_orderno
	,f.dd_productionorderno = t.dd_productionorderno
	,f.dd_productionorderplanno = t.dd_productionorderplanno
	,f.dd_qmorderno = t.dd_qmorderno
	,f.dd_RecordChanged = t.dd_RecordChanged
	,f.dd_RecordCreated = t.dd_RecordCreated
	,f.dd_referencenotificationo = t.dd_referencenotificationo
	,f.dd_reportedby = t.dd_reportedby
	,f.dd_SalesOrderDeliveryItemNo = t.dd_SalesOrderDeliveryItemNo
	,f.dd_salesorderdeliveryno = t.dd_salesorderdeliveryno
	,f.dd_SalesOrderItemNo = t.dd_SalesOrderItemNo
	,f.dd_SalesOrderNo = t.dd_SalesOrderNo
	,f.dd_SalesOrderNo2 = t.dd_SalesOrderNo2
	,f.dd_serialno = t.dd_serialno
	,f.dd_objectnumber = t.dd_objectnumber
	,f.dim_batchstoragelocationid = t.dim_batchstoragelocationid
	,f.dim_catalogprofileid = t.dim_catalogprofileid
	,f.dim_currencyid = t.dim_currencyid
	,f.dim_customerid = t.dim_customerid
	,f.dim_dateidnotificationcompletion = t.dim_dateidnotificationcompletion
	,f.dim_dateidnotificationdate = t.dim_dateidnotificationdate
	,f.dim_dateidreturndate = t.dim_dateidreturndate
	,f.Dim_dateidrecordcreated = t.Dim_dateidrecordcreated
	,f.dim_defectreporttypeid = t.dim_defectreporttypeid
	,f.dim_distributionchannelid = t.dim_distributionchannelid
	,f.dim_inspectioncatalogtypeid = t.dim_inspectioncatalogtypeid
	,f.dim_inspectionlotstoragelocationid = t.dim_inspectionlotstoragelocationid
	,f.dim_MaterialGroupid = t.dim_MaterialGroupid
	,f.dim_notificationoriginid = t.dim_notificationoriginid
	,f.dim_notificationphaseid = t.dim_notificationphaseid
	,f.dim_notificationpriorityid = t.dim_notificationpriorityid
	,f.dim_notificationtypeid = t.dim_notificationtypeid
	,f.dim_partid = t.dim_partid
	,f.dim_plantid = t.dim_plantid
	,f.dim_producthierarchyid = t.dim_producthierarchyid
	,f.dim_purchasegroupid = t.dim_purchasegroupid
	,f.dim_purchaseorgid = t.dim_purchaseorgid
	,f.Dim_SalesDivisionid = t.Dim_SalesDivisionid
	,f.dim_SalesGroupid = t.dim_SalesGroupid
	,f.dim_SalesOfficeid = t.dim_SalesOfficeid
	,f.dim_SalesOrgid = t.dim_SalesOrgid
	,f.Dim_UnitOfMeasureid = t.Dim_UnitOfMeasureid
	,f.dim_vendorid = t.dim_vendorid
	,f.dim_workplantid = t.dim_workplantid
	,f.dim_qualitynotificationmiscid = t.dim_qualitynotificationmiscid
	,f.dd_taskcode = t.dd_taskcode
	,f.dd_Coordinator = t.dd_Coordinator
	,f.dd_Dispositioner = t.dd_Dispositioner
	,f.dim_dateidtaskcreated = t.dim_dateidtaskcreated
	,f.dim_dateiddispositionerassigned = t.dim_dateiddispositionerassigned
	,f.dim_dateidtaskcompleted = t.dim_dateidtaskcompleted
	,f.dd_taskcreatedby = t.dd_taskcreatedby
	,f.dd_taskcompletedby = t.dd_taskcompletedby
	,f.Dim_ActionStateid = t.Dim_ActionStateid
	,f.dd_ZmrbFlag = t.dd_ZmrbFlag
	,f.dd_tobecompletedon = t.dd_tobecompletedon
	,f.dim_dateidrequestedstart = t.dim_dateidrequestedstart
	,f.DIM_PROJECTSOURCEID = t.DIM_PROJECTSOURCEID
	,f.dim_mdg_partid = t.dim_mdg_partid
	,f.dim_bwproducthierarchyid = t.dim_bwproducthierarchyid;

	
INSERT INTO fact_qualitynotification(
	fact_qualitynotificationid   
	,ct_complaintQty
	,ct_ExternalDefectiveQty
	,ct_InternalDefectiveQty
	,ct_returndeliveryqty
	,ct_QtyRejected
    ,ct_LotRejected
	,dd_author
	,dd_batchno
	,dd_changedby
	,dd_createdby
	,dd_documentitemno
	,dd_documentno
	,dd_InspectionLotNo
	,dd_MaterialDocItemNo
	,dd_MaterialDocNo
	,dd_MaterialDocYear
	,dd_notificationo
	,dd_orderno
	,dd_productionorderno
	,dd_productionorderplanno
	,dd_qmorderno
	,dd_RecordChanged
	,dd_RecordCreated
	,dd_referencenotificationo
	,dd_reportedby
	,dd_SalesOrderDeliveryItemNo
	,dd_salesorderdeliveryno
	,dd_SalesOrderItemNo
	,dd_SalesOrderNo
	,dd_SalesOrderNo2
	,dd_serialno
	,dd_objectnumber
	,dim_batchstoragelocationid
	,dim_catalogprofileid
	,dim_currencyid
	,dim_customerid
	,dim_dateidnotificationcompletion
	,dim_dateidnotificationdate
	,dim_dateidreturndate
	,Dim_dateidrecordcreated
	,dim_defectreporttypeid
	,dim_distributionchannelid
	,dim_inspectioncatalogtypeid
	,dim_inspectionlotstoragelocationid
	,dim_MaterialGroupid
    ,dim_notificationoriginid
    ,dim_notificationphaseid
    ,dim_notificationpriorityid
	,dim_notificationtypeid
    ,dim_partid
	,dim_plantid
    ,dim_producthierarchyid
    ,dim_purchasegroupid
    ,dim_purchaseorgid
    ,Dim_SalesDivisionid
    ,dim_SalesGroupid
    ,dim_SalesOfficeid
    ,dim_SalesOrgid
    ,Dim_UnitOfMeasureid
    ,dim_vendorid
    ,dim_workplantid
    ,dim_qualitynotificationmiscid
	,dd_taskcode
	,dd_Coordinator
	,dd_Dispositioner
	,dim_dateidtaskcreated
    ,dim_dateiddispositionerassigned
	,dim_dateidtaskcompleted
	,dd_taskcreatedby
	,dd_taskcompletedby
	,Dim_ActionStateid
	,dd_ZmrbFlag
	,dd_tobecompletedon
	,dim_dateidrequestedstart
	,DIM_PROJECTSOURCEID
	,dim_mdg_partid
	,dim_bwproducthierarchyid)
SELECT
	fact_qualitynotificationid   
	,ct_complaintQty
	,ct_ExternalDefectiveQty
	,ct_InternalDefectiveQty
	,ct_returndeliveryqty
	,ct_QtyRejected
    ,ct_LotRejected
	,dd_author
	,dd_batchno
	,dd_changedby
	,dd_createdby
	,dd_documentitemno
	,dd_documentno
	,dd_InspectionLotNo
	,dd_MaterialDocItemNo
	,dd_MaterialDocNo
	,dd_MaterialDocYear
	,dd_notificationo
	,dd_orderno
	,dd_productionorderno
	,dd_productionorderplanno
	,dd_qmorderno
	,dd_RecordChanged
	,dd_RecordCreated
	,dd_referencenotificationo
	,dd_reportedby
	,dd_SalesOrderDeliveryItemNo
	,dd_salesorderdeliveryno
	,dd_SalesOrderItemNo
	,dd_SalesOrderNo
	,dd_SalesOrderNo2
	,dd_serialno
	,dd_objectnumber
	,dim_batchstoragelocationid
	,dim_catalogprofileid
	,dim_currencyid
	,dim_customerid
	,dim_dateidnotificationcompletion
	,dim_dateidnotificationdate
	,dim_dateidreturndate
	,Dim_dateidrecordcreated
	,dim_defectreporttypeid
	,dim_distributionchannelid
	,dim_inspectioncatalogtypeid
	,dim_inspectionlotstoragelocationid
	,dim_MaterialGroupid
    ,dim_notificationoriginid
    ,dim_notificationphaseid
    ,dim_notificationpriorityid
	,dim_notificationtypeid
    ,dim_partid
	,dim_plantid
    ,dim_producthierarchyid
    ,dim_purchasegroupid
    ,dim_purchaseorgid
    ,Dim_SalesDivisionid
    ,dim_SalesGroupid
    ,dim_SalesOfficeid
    ,dim_SalesOrgid
    ,Dim_UnitOfMeasureid
    ,dim_vendorid
    ,dim_workplantid
    ,dim_qualitynotificationmiscid
	,dd_taskcode
	,dd_Coordinator
	,dd_Dispositioner
	,dim_dateidtaskcreated
    ,dim_dateiddispositionerassigned
	,dim_dateidtaskcompleted
	,dd_taskcreatedby
	,dd_taskcompletedby
	,Dim_ActionStateid
	,dd_ZmrbFlag
	,dd_tobecompletedon
	,dim_dateidrequestedstart
	,DIM_PROJECTSOURCEID
	,dim_mdg_partid
	,dim_bwproducthierarchyid
FROM tmp_fact_qualitynotification t
WHERE NOT EXISTS
             (SELECT 1
                FROM fact_qualitynotification qn
               WHERE qn.dd_notificationo = t.dd_notificationo);

DROP TABLE IF EXISTS tmp_dim_qualnotificationstatus;
CREATE TABLE  tmp_dim_qualnotificationstatus AS 
SELECT distinct j1.JEST_OBJNR AS dd_ObjectNumber,
	'Not Set' AS Outstanding,
	'Not Set' AS Postponed,
	'Not Set' AS InProcess,
	'Not Set' AS Completed,
	'Not Set' AS DeletionFlag 
FROM JEST j1;

UPDATE tmp_dim_qualnotificationstatus t
SET Outstanding = 'X'
FROM tmp_dim_qualnotificationstatus t, JEST j
WHERE j.JEST_OBJNR = t.dd_ObjectNumber
	AND j.JEST_STAT = 'I0068' 
	AND j.JEST_INACT IS NULL; 

UPDATE tmp_dim_qualnotificationstatus
SET Postponed = 'X'
FROM tmp_dim_qualnotificationstatus t, JEST j
WHERE j.JEST_OBJNR = t.dd_ObjectNumber
	AND j.JEST_STAT = 'I0069' 
	AND j.JEST_INACT IS NULL;

UPDATE tmp_dim_qualnotificationstatus
SET InProcess = 'X'
FROM tmp_dim_qualnotificationstatus t, JEST j
WHERE j.JEST_OBJNR = t.dd_ObjectNumber
	AND j.JEST_STAT = 'I0070' 
	AND j.JEST_INACT IS NULL;

UPDATE tmp_dim_qualnotificationstatus
SET Completed = 'X'
FROM tmp_dim_qualnotificationstatus t, JEST j
WHERE j.JEST_OBJNR = t.dd_ObjectNumber
	AND j.JEST_STAT = 'I0072' 
	AND j.JEST_INACT IS NULL;

UPDATE tmp_dim_qualnotificationstatus
SET DeletionFlag = 'X'
FROM tmp_dim_qualnotificationstatus t, JEST j
WHERE j.JEST_OBJNR = t.dd_ObjectNumber
	AND j.JEST_STAT = 'I0076' 
	AND j.JEST_INACT IS NULL;

UPDATE fact_qualitynotification qn
SET qn.dim_notificationstatusid = ns.dim_notificationstatusid
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
FROM fact_qualitynotification qn,
	dim_notificationstatus ns,
	QMEL q1,
	tmp_dim_qualnotificationstatus t1
WHERE qn.dd_notificationo = q1.QMEL_QMNUM
	AND ns.Outstanding = t1.Outstanding
	AND ns.Postponed = t1.Postponed 
	AND ns.InProcess = t1.InProcess 
	AND ns.Completed = t1.Completed
	AND ns.DeletionFlag = t1.DeletionFlag
	AND qn.dd_ObjectNumber = t1.dd_ObjectNumber
	AND qn.dim_notificationstatusid <> ns.dim_notificationstatusid;

DROP TABLE IF EXISTS tmp_dim_qualnotificationstatus;

DROP TABLE IF EXISTS tmp_updt_QtyRejectedExternalAmt;
CREATE TABLE tmp_updt_QtyRejectedExternalAmt
AS
SELECT 
	ct_QtyRejected
	,ct_InternalDefectiveQty
	,dim_notificationtypeid
	,dim_notificationstatusid
	,dd_MaterialDocItemNo
	,dd_MaterialDocYear
	,dd_MaterialDocNo
	,row_number() over(partition by dd_MaterialDocItemNo,dd_MaterialDocYear,dd_MaterialDocNo order by dd_MaterialDocYear) rownumber
FROM fact_qualitynotification
WHERE dim_dateidnotificationcompletion <> 1;

DELETE FROM tmp_updt_QtyRejectedExternalAmt WHERE rownumber <> 1;

UPDATE fact_materialmovement f_mm  --PRODUS cartezian
SET f_mm.QtyRejectedExternalAmt =
          IFNULL(
             (CASE
                 WHEN nt.QualityNotification = 1 THEN qn.ct_QtyRejected
                 ELSE 0
              END),
             0),
       f_mm.QtyRejectedInternalAmt =
          IFNULL(
             (CASE
                 WHEN (nt.NotificationTypeCode = 'H1'
                       AND ns.DeletionFlag <> 'X')
                 THEN
                    qn.ct_InternalDefectiveQty
                 ELSE
                    0
              END),
             0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_materialmovement f_mm,
	dim_notificationtype nt,
	dim_notificationstatus ns,
	tmp_updt_QtyRejectedExternalAmt qn
WHERE qn.dim_notificationtypeid = nt.Dim_NotificationTypeid
	AND qn.dim_notificationstatusid = ns.dim_notificationstatusid
	AND qn.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo
	AND qn.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
	AND qn.dd_MaterialDocNo = f_mm.dd_MaterialDocNo;
	
DROP TABLE IF EXISTS tmp_updt_QtyRejectedExternalAmt;

UPDATE facT_qualitynotification
SET dim_notificationoriginid = 1
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dim_notificationoriginid IS NULL;

UPDATE facT_qualitynotification
SET dim_vendorid = 1
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dim_vendorid IS NULL;

UPDATE facT_qualitynotification
SET dim_materialgroupid = 1
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dim_materialgroupid IS NULL;

UPDATE facT_qualitynotification
SET dim_unitofmeasureid = 1
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dim_unitofmeasureid IS NULL;

UPDATE facT_qualitynotification
SET dim_notificationtypeid = 1
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dim_notificationtypeid IS NULL;

UPDATE facT_qualitynotification
SET dim_inspectioncatalogtypeid = 1
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dim_inspectioncatalogtypeid IS NULL;

UPDATE facT_qualitynotification
SET dim_catalogprofileid = 1
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dim_catalogprofileid IS NULL;

DROP TABLE IF EXISTS tmp_updt_dd_VendorMaterialNo;
CREATE TABLE tmp_updt_dd_VendorMaterialNo
AS
SELECT dd_DocumentNo, dd_documentitemno, dd_VendorMaterialNo, ROW_NUMBER() OVER(PARTITION BY dd_DocumentNo, dd_documentitemno ORDER BY '') rownumber
FROM fact_purchase;

DELETE FROM tmp_updt_dd_VendorMaterialNo WHERE rownumber <> 1;

UPDATE fact_qualitynotification fq  --produs cartezian
SET fq.dd_VendorMaterialNo = IFNULL(fp.dd_VendorMaterialNo,'Not Set')
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
FROM fact_qualitynotification fq, tmp_updt_dd_VendorMaterialNo fp
WHERE fp.dd_DocumentNo = fq.dd_documentno
  AND fp.dd_documentitemno = fq.dd_documentitemno
  AND fq.dd_VendorMaterialNo <> IFNULL(fp.dd_VendorMaterialNo,'Not Set');
  
DROP TABLE IF EXISTS tmp_updt_dd_VendorMaterialNo;

UPDATE fact_qualitynotification 
SET dd_VendorMaterialNo = 'Not Set' 
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dd_VendorMaterialNo IS NULL;
/*
UPDATE fact_qualitynotification fq
SET fq.Dim_codetextCausesid = IFNULL(fi.Dim_codetextCausesid,1)
	,dw_update_date = CURRENT_TIMESTAMP 
FROM fact_qualitynotification fq, fact_Qualitynotificationitem fi
WHERE fq.dd_notificationo = fi.dd_NotificationNo
	AND fq.Dim_codetextCausesid <> IFNULL(fi.Dim_codetextCausesid,1)
*/

merge into fact_qualitynotification fact
  using (
		select f.fact_qualitynotificationid, max(fi.Dim_codetextCausesid) Dim_codetextCausesid
		FROM fact_Qualitynotificationitem fi, fact_qualitynotification f
		WHERE f.dd_Notificationo = fi.dd_NotificationNo
		group by f.fact_qualitynotificationid
  ) t on fact.fact_qualitynotificationid = t.fact_qualitynotificationid
  when matched then update set fact.Dim_codetextCausesid = t.Dim_codetextCausesid
	,fact.dw_update_date = current_timestamp 
where fact.Dim_codetextCausesid <> t.Dim_codetextCausesid;

UPDATE fact_qualitynotification 
SET Dim_codetextCausesid = 1 
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE Dim_codetextCausesid IS NULL;
/*
UPDATE fact_qualitynotification fq
SET fq.dd_NumberOfDefects = IFNULL(fi.dd_NumberOfDefects,0)
	,dw_update_date = CURRENT_TIMESTAMP 
FROM fact_qualitynotification fq, fact_Qualitynotificationitem fi
WHERE fq.dd_notificationo = fi.dd_NotificationNo
	AND fq.dd_NumberOfDefects <> IFNULL(fi.dd_NumberOfDefects,0)
	*/
	

merge into fact_qualitynotification fact
  using (
		select f.fact_qualitynotificationid, max(fi.dd_NumberOfDefects) dd_NumberOfDefects
		FROM fact_Qualitynotificationitem fi, fact_qualitynotification f
		WHERE f.dd_Notificationo = fi.dd_NotificationNo
		group by f.fact_qualitynotificationid
  ) t on fact.fact_qualitynotificationid = t.fact_qualitynotificationid
  when matched then update set fact.dd_NumberOfDefects = t.dd_NumberOfDefects
	,fact.dw_update_date = current_timestamp 
where fact.dd_NumberOfDefects <> t.dd_NumberOfDefects;

UPDATE fact_qualitynotification 
SET dd_NumberOfDefects = 0 
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dd_NumberOfDefects IS NULL;
/*
UPDATE fact_qualitynotification fq
SET fq.dd_defectnumber = IFNULL(fi.dd_defectnumber,0)
	,dw_update_date = CURRENT_TIMESTAMP 
FROM fact_qualitynotification fq, fact_Qualitynotificationitem fi
WHERE fq.dd_notificationo = fi.dd_NotificationNo
	AND fq.dd_defectnumber <> IFNULL(fi.dd_defectnumber,0)
*/

merge into fact_qualitynotification fact
  using (
		select f.fact_qualitynotificationid, max(fi.dd_defectnumber) dd_defectnumber
		FROM fact_Qualitynotificationitem fi, fact_qualitynotification f
		WHERE f.dd_Notificationo = fi.dd_NotificationNo
		group by f.fact_qualitynotificationid
  ) t on fact.fact_qualitynotificationid = t.fact_qualitynotificationid
  when matched then update set fact.dd_defectnumber = t.dd_defectnumber
	,fact.dw_update_date = current_timestamp 
where fact.dd_defectnumber <> t.dd_defectnumber;

UPDATE fact_qualitynotification 
SET dd_defectnumber = 0 
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
WHERE dd_defectnumber IS NULL;

	
/* MDG Part */

DROP TABLE IF EXISTS tmp_dim_mdg_partid;
CREATE TABLE tmp_dim_mdg_partid as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;


UPDATE fact_qualitynotification f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1)
FROM fact_qualitynotification f, tmp_dim_mdg_partid tmp
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

drop table if exists tmp_dim_mdg_partid;

/* Update BW Hierarchy */

update fact_qualitynotification f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_qualitynotification f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;