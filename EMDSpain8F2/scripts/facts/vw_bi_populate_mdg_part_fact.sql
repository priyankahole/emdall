drop table if exists tmp_mgd_part_ins;
create table tmp_mgd_part_ins as
select dim_mdg_partid
	,ifnull(dim_partid,1) as dim_partid
 from dim_mdg_part md
    left outer join dim_part pr on right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18);

delete from fact_mdg_part where fact_mdg_partid <>1 ;
insert into  fact_mdg_part (fact_mdg_partid, dim_mdg_partid, dim_partid, dim_bwproducthierarchyid, dim_projectsourceid, dw_insert_date, dw_update_date)
	select 1,1,1,1,1,current_timestamp, current_timestamp
	from (select 1) a
	where not exists (select 'x' from fact_mdg_part where fact_mdg_partid = 1);

insert into fact_mdg_part (fact_mdg_partid, dim_mdg_partid, dim_partid, dim_bwproducthierarchyid,dim_projectsourceid)
	select (select s.dim_projectsourceid * s.multiplier from dim_projectsource s) + row_number() over(order by '') as fact_mdg_partid
		,t.dim_mdg_partid
		,ifnull(t.dim_partid,1) as dim_partid
		,1 as dim_bwproducthierarchyid
		,1 as dim_projectsourceid
	from tmp_mgd_part_ins t;

drop table if exists tmp_bwproducthier;
create table tmp_bwproducthier as
select dim_partid , max(dim_bwproducthierarchyid) dim_bwproducthierarchyid
	from dim_part dp, dim_bwproducthierarchy bw
where dp.producthierarchy = bw.lowerhierarchycode
	and dp.productgroupsbu = bw.upperhierarchycode
	and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
group by dim_partid;

insert into tmp_bwproducthier (dim_partid, dim_bwproducthierarchyid)
select dp.dim_partid , max(bw.dim_bwproducthierarchyid) dim_bwproducthierachy
from dim_part dp , dim_bwproducthierarchy bw
where dp.producthierarchy = bw.lowerhierarchycode
	and bw.upperhierarchycode = 'Not Set'
	and dp.dim_partid not in (select dim_partid from tmp_bwproducthier)
group by dp.dim_partid;

update fact_mdg_part f
	set f.dim_bwproducthierarchyid = t.dim_bwproducthierarchyid,
		f.dw_update_date = current_timestamp
from fact_mdg_part f,tmp_bwproducthier t
where f.dim_partid = t.dim_partid
and f.dim_bwproducthierarchyid <> t.dim_bwproducthierarchyid;
