
Drop table if exists fact_shipmentindelivery_temp;

create table fact_shipmentindelivery_temp
AS
Select * from fact_shipmentindelivery where 1 = 2;



delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery_temp',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery;


DROP TABLE IF EXISTS tmp_populate_shipment_t2;
CREATE TABLE tmp_populate_shipment_t2
AS
SELECT row_number() over (order by '') rid,
                  v1.VTTK_TKNUM dd_ShipmentNo, /* dd_ShipmentNo */
                  v1.VTTP_TPNUM dd_ShipmentItemNo, /* dd_ShipmentItemNo */
                  ifnull(v1.VTTP_VBELN, 'Not Set') dd_InboundDeliveryNo, /* dd_InboundDeliveryNo */
                  EKES_EBELN dd_DocumentNo, /* dd_DocumentNo */
                  EKES_EBELP dd_DocumentItemNo, /* dd_DocumentItemNo */
                  EKES_J_3AETENR dd_ScheduleNo, /* dd_ScheduleNo */
		          CONVERT(BIGINT, 1) Dim_DateIdShipmentCreated,      /*  Dim_DateIdShipmentCreated  */            
                  ifnull(VTTK_TPBEZ, 'Not Set') dd_Vessel, /* dd_Vessel */
                  ifnull(VTTK_TEXT4, 'Not Set') dd_MovementType, /* dd_MovementType */
                  ifnull(VTTK_TEXT3, 'Not Set') dd_CarrierActual,  /* dd_CarrierActual */
                  CONVERT(BIGINT, 1) Dim_CarrierBillingId,  /* Dim_CarrierBillingId */
                  ifnull(VTTK_SIGNI, 'Not Set') dd_DomesticContainerID,  /* dd_DomesticContainerID */
                  CONVERT(BIGINT, 1) Dim_ShipmentTypeId,    /* Dim_ShipmentTypeId */
                  CONVERT(BIGINT, 1) Dim_RouteId,   /* Dim_RouteId */
                  ifnull(VTTK_EXTI2, 'Not Set') dd_VoyageNo,  /* dD_VoyageNo */
	        CONVERT(BIGINT, 1) Dim_DateIdShipmentEnd,  
	        CONVERT(BIGINT, 1) Dim_DateIdDepartureFromOrigin,
	        ifnull(VTTK_ADD01, 'Not Set') dd_ContainerSize,   /* dd_ContainerSize  */
	        'Not Set' dd_3PLFlag,  /* dd_3PLFlag */
	        ifnull(LIKP_VOLUM, 0.0000) ct_InboundDlvryCMtrVolume,
		CONVERT(BIGINT, 1) Dim_DateidDlvrDocCreated,
		p.dim_plantidordering Dim_PlantId,  /* Dim_PlantId */
	        ifnull((CASE WHEN VTTK_EXTI1 IS NULL THEN LIKP_BOLNR ELSE VTTK_EXTI1 END),'Not Set') dd_billofladingNo, /* dd_billofladingNo */
		p.dim_DateIdDelivery Dim_DateIdPODelivery,  /* Dim_DateIdPODelivery */
		ifnull(EKES_DABMG, 0.0000) ct_DelivReceivedQty,  /* ct_DelivReceivedQty */
		ifnull(EKES_MENGE, 0.0000) ct_ReceivedQty,
		CONVERT(BIGINT, 1) dim_inbrouteid,
		p.ct_ItemQty,
		p.Dim_AFSTranspConditionid Dim_AFSTranspConditionid, /* Dim_AFSTranspConditionid */
		p.Dim_DateidOrder dim_dateidactualpo,  /*  dim_dateidactualpo */
		p.dim_dateidAFSExFactEst dim_dateidAFSExFactEst,   /* dim_dateidAFSExFactEst */
		p.dim_dateidMatAvailability dim_dateidMatAvailability,  /* dim_dateidMatAvailability */
	        p.dd_DeliveryNumber dd_DeliveryNumber,   /* dd_DeliveryNumber */
                CONVERT(BIGINT, 1) dim_dateidTransfer,
		ifnull(LIKP_BTGEW,0.0000) ct_DeliveryGrossWght, 
		ifnull(LIKP_NTGEW,0.0000) ct_deliveryNetWght,
		CONVERT(BIGINT, 1)  Dim_DeliveryRouteId,
		CONVERT(BIGINT, 1) dim_shippingconditionid,
		CONVERT(BIGINT, 1) dim_tranportbyshippingtypeid,
		p.dd_ShortText,
 		ifnull(VTTK_TEXT1,'Not Set') as dd_sealnumber,
                p.dd_noofforeigntrade,
		ifnull(LIKP_ANZPK, 0) dd_PackageCount,
		CONVERT(BIGINT, 1) dim_transportconditionid,
		v1.VTTK_ERDAT,  /*   Dim_DateIdShipmentCreated */
	        v1.VTTK_TDLNR,  /*   Dim_CarrierBillingId */
	        v1.VTTK_SHTYP,  /*   Dim_ShipmentTypeId */
	        v1.VTTK_ROUTE,  /*   Dim_RouteId */
	        v1.VTTK_DPTEN,  /*   Dim_DateIdShipmentEnd */
	        v1.VTTK_DATBG,  /*   Dim_DateIdDepartureFromOrigin */
	        v.LIKP_ERDAT,  /*   Dim_DateidDlvrDocCreated */
		v.LIKP_ROUTE,  /*   dim_inbrouteid , Dim_DeliveryRouteId */
		v.LIKP_BLDAT,  /*   dim_dateidTransfer */
		v.LIKP_VSBED,  /*   dim_shippingconditionid */
                v.LIKP_VSART,  /*   dim_tranportbyshippingtypeid */
                v1.VTTK_VSART   /*  dim_transportconditionid */
		
 FROM VTTK_VTTP v1
                  INNER JOIN EKES_LIKP_LIPS v ON v1.VTTP_VBELN = v.LIKP_VBELN 
				  /* AND v.LIKP_WADAT_IST IS NOT NULL */
                  INNER JOIN facT_purchase p
                     ON     p.dd_DocumentNo = EKES_EBELN
                        AND p.dd_DocumentItemNo = EKES_EBELP
                        AND p.dd_ScheduleNo = EKES_J_3AETENR
            WHERE NOT EXISTS
                         (SELECT 1
                            FROM fact_shipmentindelivery_temp f1
                           WHERE     f1.dd_ShipmentNo = VTTK_TKNUM
                                 AND f1.dd_ShipmentItemNo = VTTP_TPNUM
                                 AND f1.dd_InboundDeliveryNo = LIKP_VBELN
                                 AND f1.dd_DocumentNo = EKES_EBELN
                                 AND f1.dd_DocumentItemNo = EKES_EBELP
                                 AND f1.dd_ScheduleNo = EKES_J_3AETENR)	;
								 
UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DateIdShipmentCreated = ifnull (d.Dim_Dateid, 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.VTTK_ERDAT
                                 AND d.CompanyCode = 'Not Set';
								 
UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_CarrierBillingId = ifnull (v.Dim_VendorId , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Vendor v ON v.VendorNumber = t.VTTK_TDLNR
								   AND RowIsCurrent = 1;
								 
UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_ShipmentTypeId = ifnull (st.dim_Shipmenttypeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_ShipmentType st ON st.ShipmentType = t.VTTK_SHTYP
                                          AND st.RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_RouteId = ifnull (r.dim_Routeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.VTTK_ROUTE 
								  AND r.RowIsCurrent = 1;
								  
UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DateIdShipmentEnd = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.VTTK_DPTEN
                                 AND d.CompanyCode = 'Not Set';								  
									 								   
UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DateIdDepartureFromOrigin = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.VTTK_DATBG
                                 AND d.CompanyCode = 'Not Set';		

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DateidDlvrDocCreated = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.LIKP_ERDAT
                                 AND d.CompanyCode = 'Not Set';		

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_inbrouteid = ifnull (r.dim_Routeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.LIKP_ROUTE 
								  AND r.RowIsCurrent = 1;		

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_dateidTransfer = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.LIKP_BLDAT
                                 AND d.CompanyCode = 'Not Set';		

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DeliveryRouteId = ifnull (r.dim_Routeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.LIKP_ROUTE 
								  AND r.RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_shippingconditionid = ifnull (s.dim_shippingconditionid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_shippingcondition s ON s.shippingconditioncode = t.LIKP_VSBED 
								              AND s.rowiscurrent = 1;								  

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_tranportbyshippingtypeid = ifnull (tbst.dim_tranportbyshippingtypeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_tranportbyshippingtype tbst ON tbst.ShippingType = t.LIKP_VSART;
			
UPDATE tmp_populate_shipment_t2 t
SET  t.dim_transportconditionid = ifnull (tbst.dim_tranportbyshippingtypeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_tranportbyshippingtype tbst ON tbst.ShippingType = t.VTTK_VSART;
			
INSERT INTO fact_shipmentindelivery_temp
	(fact_shipmentindeliveryid,
					 dd_ShipmentNo,
					 dd_ShipmentItemNo,
					 dd_InboundDeliveryNo,
					 dd_DocumentNo,
					 dd_DocumentItemNo,
					 dd_ScheduleNo,
					 Dim_DateIdShipmentCreated,
					 dd_Vessel,
					 dd_MovementType,
					 dd_CarrierActual,
					 Dim_CarrierBillingId,
					 dd_DomesticContainerID,
					 Dim_ShipmentTypeId,
					 Dim_RouteId,
					 dD_VoyageNo,
					 Dim_DateIdShipmentEnd,
					 Dim_DateIdDepartureFromOrigin,
					 dd_ContainerSize,
					 dd_3PLFlag,
					 ct_InboundDlvryCMtrVolume,
					 Dim_DateidDlvrDocCreated,
					 Dim_PlantId,
					 dd_billofladingNo,
					 Dim_DateIdPODelivery,
					 ct_DelivReceivedQty,
					 ct_ReceivedQty,
					 dim_inbrouteid,
					 ct_ItemQty,
					 Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
					 dim_dateidactualpo, /* Roxana 02 dec 2014 */
					 dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
					 dim_dateidMatAvailability,  /* Marius 15 Dec 2014 */
					 dd_DeliveryNumber,          /* Marius 17 Dec 2014 */
					 dim_dateidTransfer, /* Marius 17 Dec 2014 */
					 ct_DeliveryGrossWght, /* Marius 17 Dec 2014 */
					 ct_deliveryNetWght, /* Marius 17 Dec 2014 */
					 Dim_DeliveryRouteId, /* Marius 17 Dec 2014 */
					 dim_shippingconditionid, /* Marius 17 Dec 2014 */
					 dim_tranportbyshippingtypeid, /* Marius 6 Jan 2014 */
					 dd_ShortText,
					 dd_sealnumber,   /* Marius 5 feb 2015 */
					 dd_noofforeigntrade , /* Marius 15 feb 2015 */
					 dd_PackageCount, /* Marius 4 mar 2015 */ 
					 dim_transportconditionid ) /*Roxana 08 may 2015*/										
SELECT ((SELECT ifnull(max_id, 0)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_shipmentindelivery_temp') +
        			 row_number() over (order by '')) fact_shipmentindeliveryid,
          a.*
	FROM (SELECT DISTINCT
                  dd_ShipmentNo,
                  dd_ShipmentItemNo,
                  dd_InboundDeliveryNo,
                  dd_DocumentNo,
                  dd_DocumentItemNo,
                  dd_ScheduleNo,
		  Dim_DateIdShipmentCreated,
		  dd_Vessel,
		  dd_MovementType,
		  dd_CarrierActual,
		  Dim_CarrierBillingId,
		  dd_DomesticContainerID,
		  Dim_ShipmentTypeId,
		  Dim_RouteId,
		  dD_VoyageNo,
		  Dim_DateIdShipmentEnd,
		  Dim_DateIdDepartureFromOrigin,
		  dd_ContainerSize,
		  dd_3PLFlag,
		  ct_InboundDlvryCMtrVolume,
		  Dim_DateidDlvrDocCreated,
		  Dim_PlantId,
		  dd_billofladingNo,
		  Dim_DateIdPODelivery,
		  ct_DelivReceivedQty,
		  ct_ReceivedQty,
		  dim_inbrouteid,
		  ct_ItemQty,
		  Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
		  dim_dateidactualpo, /* Roxana 02 dec 2014 */
		  dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
		  dim_dateidMatAvailability,  /* Marius 15 Dec 2014 */
		  dd_DeliveryNumber,          /* Marius 17 Dec 2014 */
		  dim_dateidTransfer, /* Marius 17 Dec 2014 */
		  ct_DeliveryGrossWght, /* Marius 17 Dec 2014 */
		  ct_deliveryNetWght, /* Marius 17 Dec 2014 */
		  Dim_DeliveryRouteId, /* Marius 17 Dec 2014 */
		  dim_shippingconditionid, /* Marius 17 Dec 2014 */
		  dim_tranportbyshippingtypeid, /* Marius 6 Jan 2014 */
		  dd_ShortText,
		  dd_sealnumber,   /* Marius 5 feb 2015 */
		  dd_noofforeigntrade , /* Marius 15 feb 2015 */
		  dd_PackageCount, /* Marius 4 mar 2015 */ 
		  dim_transportconditionid 
		FROM tmp_populate_shipment_t2 ) a;
				  

	 
	 
delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery_temp',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery_temp;

DROP TABLE IF EXISTS tmp_populate_shipment_t3;
CREATE TABLE tmp_populate_shipment_t3
AS
SELECT row_number() over (order by '') rid,
                  p.dd_DocumentNo,
                  p.dd_DocumentItemNo,
                  p.dd_ScheduleNo,
                  'Not Set' dd_ShipmentNo,
                  0 dd_ShipmentItemNo,
                  LIKP_VBELN dd_InboundDeliveryNo,
                  1 Dim_DateIdShipmentCreated,
                  'Not Set' dd_Vessel,
                  'Not Set' dd_MovementType,
                  'Not Set' dd_CarrierActual,
                  1 Dim_CarrierBillingId,
                  'Not Set' dd_DomesticContainerID,
                  1 Dim_ShipmentTypeId,
                  1 Dim_RouteId,
                  'Not Set' dD_VoyageNo,
                  1 Dim_DateIdShipmentEnd,
                  1 Dim_DateIdDepartureFromOrigin,
                  'Not Set' dd_ContainerSize,
                  'Not Set' dd_3PLFlag ,
                  p.Dim_DateIdSchedOrder Dim_DateIdPOBuy,
                  p.Dim_DocumentTypeid Dim_PODocumentTypeId,
                  p.Dim_DateidAFSDelivery Dim_DateIdPOETA,
                  p.dim_purchasegroupid,
                  p.Dim_partid,
                  p.Dim_Vendorid,
                  p.Dim_DateIdLastGR Dim_DateIdGR,
                  p.dim_AFSRouteid Dim_PORouteId,
                  p.Dim_CustomPartnerFunctionId4 Dim_CustomVendorPartnerFunctionId,
                  p.Dim_CustomPartnerFunctionId3 Dim_CustomVendorPartnerFunctionId1,
                  p.Dim_AfsSeasonId Dim_POAFSSeasonId,
                  p.ct_DeliveryQty ct_POScheduleQty,
                  p.ct_ReceivedQty ct_POItemReceivedQty,
		          p.ct_QtyReduced ct_POInboundDeliveryQty,
                  'Not Set' dd_CustomerPOStatus,
                  ifnull(LIKP_ANZPK, 0) dd_PackageCount,
                  ifnull(LIKP_BOLNR, 'Not Set') dd_BillofLadingNo,
                  p.Dim_CompanyId Dim_CompanyId,
                  p.Dim_plantidordering Dim_PlantId,
                  ifnull(LIKP_TRAID, 'Not Set') dd_OriginContainerId,
                  ifnull(LIKP_VOLUM, 0.0000) ct_InboundDlvryCMtrVolume,
		  1 Dim_DateidDlvrDocCreated,
		  p.Dim_DateIdDelivery Dim_DateIdPODelivery,
		  ifnull(EKES_DABMG, 0.0000) ct_DelivReceivedQty,
		  ifnull(EKES_MENGE, 0.0000) ct_ReceivedQty,
		  'Not Set' dd_ShipmentStatus,
		  1 dim_inbrouteid,
		  p.ct_ItemQty,
		  p.Dim_AFSTranspConditionid Dim_AFSTranspConditionid, /* MArius 28 nov 2014 */
		  p.Dim_DateidOrder dim_dateidactualpo,
		  p.dim_dateidAFSExFactEst dim_dateidAFSExFactEst,   /* MArius 03 Dec 2014 */
		  p.dim_dateidMatAvailability dim_dateidMatAvailability,
		  p.dd_DeliveryNumber,
		  1 dim_dateidTransfer,
		  ifnull(LIKP_BTGEW,0.0000) ct_DeliveryGrossWght,
	   	  ifnull(LIKP_NTGEW,0.0000) ct_deliveryNetWght,
		  1 Dim_DeliveryRouteId,
		  1 dim_shippingconditionid,
		  1 dim_tranportbyshippingtypeid,
		  p.dd_ShortText dd_ShortText,
		  p.dd_noofforeigntrade dd_noofforeigntrade,
		  v.LIKP_ERDAT, 
		  v.LIKP_ROUTE,
		  v.LIKP_BLDAT,
		  v.LIKP_VSBED,
		  v.LIKP_VSART
             FROM facT_purchase p
                  INNER JOIN EKES_LIKP_LIPS v
                     ON     dd_DocumentNo = EKES_EBELN
                        AND dd_DocumentItemNo = EKES_EBELP
                        AND dd_ScheduleNo = EKES_J_3AETENR
            WHERE NOT EXISTS
                         (SELECT 1
                            FROM fact_shipmentindelivery_temp f1
                           WHERE     f1.dd_InboundDeliveryNo = LIKP_VBELN
                                 AND f1.dd_DocumentNo = EKES_EBELN
                                 AND f1.dd_DocumentItemNo = EKES_EBELP
                                 AND f1.dd_ScheduleNo = EKES_J_3AETENR);

UPDATE tmp_populate_shipment_t3 t
SET t.Dim_DateidDlvrDocCreated	= ifnull (d.Dim_Dateid , 1)						 
FROM tmp_populate_shipment_t3 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.LIKP_ERDAT
                                 AND d.CompanyCode = 'Not Set';	
UPDATE tmp_populate_shipment_t3 t
SET t.dim_inbrouteid = ifnull (r.dim_Routeid , 1)						 
FROM tmp_populate_shipment_t3 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.LIKP_ROUTE
                                 AND r.RowIsCurrent = 1;	

UPDATE tmp_populate_shipment_t3 t
SET t.dim_dateidTransfer = ifnull (d.Dim_Dateid , 1)						 
FROM tmp_populate_shipment_t3 t
            LEFT JOIN Dim_Date d ON  d.datevalue = t.LIKP_BLDAT
                                 AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t3 t
SET t.Dim_DeliveryRouteId = ifnull (r.dim_Routeid , 1)						 
FROM tmp_populate_shipment_t3 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.LIKP_ROUTE
                                 AND r.RowIsCurrent = 1;	
								
UPDATE tmp_populate_shipment_t3 t
SET t.dim_shippingconditionid = ifnull (s.dim_shippingconditionid , 1)						 
FROM tmp_populate_shipment_t3 t
            LEFT JOIN dim_shippingcondition s ON s.shippingconditioncode = t.LIKP_VSBED 
 			                                  AND s.rowiscurrent = 1;

UPDATE tmp_populate_shipment_t3 t
SET t.dim_tranportbyshippingtypeid = ifnull (tbst.dim_tranportbyshippingtypeid , 1)						 
FROM tmp_populate_shipment_t3 t
            LEFT JOIN dim_tranportbyshippingtype tbst ON tbst.ShippingType = t.LIKP_VSART;
			
INSERT INTO fact_shipmentindelivery_temp(fact_shipmentindeliveryid,
                                         dd_DocumentNo,
                                         dd_DocumentItemNo,
                                         dd_ScheduleNo,
                                         dd_ShipmentNo,
                                         dd_ShipmentItemNo,
                                         dd_InboundDeliveryNo,
                                         Dim_DateIdShipmentCreated,
                                         dd_Vessel,
                                         dd_MovementType,
                                         dd_CarrierActual,
                                         Dim_CarrierBillingId,
                                         dd_DomesticContainerID,
                                         Dim_ShipmentTypeId,
                                         Dim_RouteId,
                                         dD_VoyageNo,
                                         Dim_DateIdShipmentEnd,
                                         Dim_DateIdDepartureFromOrigin,
                                         dd_ContainerSize,
                                         dd_3PLFlag,
                                         Dim_DateIdPOBuy,
                                         Dim_PODocumentTypeId,
                                         Dim_DateIdPOETA,
                                         dim_purchasegroupid,
                                         Dim_partid,
                                         Dim_Vendorid,
                                         Dim_DateIdGR,
                                         Dim_PORouteId,
                                         Dim_CustomVendorPartnerFunctionId,
                                         Dim_CustomVendorPartnerFunctionId1,
                                         Dim_POAFSSeasonId,
                                         ct_POScheduleQty,
                                         ct_POItemReceivedQty,
										 ct_POInboundDeliveryQty,
                                         dd_CustomerPOStatus,
                                         dd_PackageCount,
                                         dd_BillofLadingNo,
                                         Dim_CompanyId,
                                         Dim_PlantId,
                                         dd_OriginContainerId,
                                         ct_InboundDlvryCMtrVolume,
										 Dim_DateidDlvrDocCreated,
										 Dim_DateIdPODelivery,
										 ct_DelivReceivedQty,
										 ct_ReceivedQty,
										 dd_ShipmentStatus,
										 dim_inbrouteid,
										 ct_ItemQty,
										 Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
										 dim_dateidactualpo,  /* Roxana 02 dec 2014 */
										 dim_dateidAFSExFactEst,  /* Marius 03 Dec 2014 */
										 dim_dateidMatAvailability,
										 dd_DeliveryNumber,
										 dim_dateidTransfer,
										 ct_DeliveryGrossWght, 
										 ct_deliveryNetWght, 
										 Dim_DeliveryRouteId,
										 dim_shippingconditionid,
										 dim_tranportbyshippingtypeid,
										 dd_ShortText,
										 dd_noofforeigntrade)
   SELECT ((SELECT ifnull(max_id, 1)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_shipmentindelivery_temp') + row_number() over (order by '')) fact_shipmentindeliveryid,
          a.*
     FROM (SELECT DISTINCT
                  dd_DocumentNo,
                                         dd_DocumentItemNo,
                                         dd_ScheduleNo,
                                         dd_ShipmentNo,
                                         dd_ShipmentItemNo,
                                         dd_InboundDeliveryNo,
                                         Dim_DateIdShipmentCreated,
                                         dd_Vessel,
                                         dd_MovementType,
                                         dd_CarrierActual,
                                         Dim_CarrierBillingId,
                                         dd_DomesticContainerID,
                                         Dim_ShipmentTypeId,
                                         Dim_RouteId,
                                         dD_VoyageNo,
                                         Dim_DateIdShipmentEnd,
                                         Dim_DateIdDepartureFromOrigin,
                                         dd_ContainerSize,
                                         dd_3PLFlag,
                                         Dim_DateIdPOBuy,
                                         Dim_PODocumentTypeId,
                                         Dim_DateIdPOETA,
                                         dim_purchasegroupid,
                                         Dim_partid,
                                         Dim_Vendorid,
                                         Dim_DateIdGR,
                                         Dim_PORouteId,
                                         Dim_CustomVendorPartnerFunctionId,
                                         Dim_CustomVendorPartnerFunctionId1,
                                         Dim_POAFSSeasonId,
                                         ct_POScheduleQty,
                                         ct_POItemReceivedQty,
										 ct_POInboundDeliveryQty,
                                         dd_CustomerPOStatus,
                                         dd_PackageCount,
                                         dd_BillofLadingNo,
                                         Dim_CompanyId,
                                         Dim_PlantId,
                                         dd_OriginContainerId,
                                         ct_InboundDlvryCMtrVolume,
										 Dim_DateidDlvrDocCreated,
										 Dim_DateIdPODelivery,
										 ct_DelivReceivedQty,
										 ct_ReceivedQty,
										 dd_ShipmentStatus,
										 dim_inbrouteid,
										 ct_ItemQty,
										 Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
										 dim_dateidactualpo,  /* Roxana 02 dec 2014 */
										 dim_dateidAFSExFactEst,  /* Marius 03 Dec 2014 */
										 dim_dateidMatAvailability,
										 dd_DeliveryNumber,
										 dim_dateidTransfer,
										 ct_DeliveryGrossWght, 
										 ct_deliveryNetWght, 
										 Dim_DeliveryRouteId,
										 dim_shippingconditionid,
										 dim_tranportbyshippingtypeid,
										 dd_ShortText,
										 dd_noofforeigntrade
		
             FROM tmp_populate_shipment_t3 ) a;

DROP TABLE IF EXISTS tmp_populate_shipment_t3;


delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery_temp',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery_temp;

				 
INSERT INTO fact_shipmentindelivery_temp(fact_shipmentindeliveryid,
                                         dd_DocumentNo,
                                         dd_DocumentItemNo,
                                         dd_ScheduleNo,
                                         dd_ShipmentNo,
                                         dd_ShipmentItemNo,
                                         dd_InboundDeliveryNo,
                                         Dim_DateIdShipmentCreated,
                                         dd_Vessel,
                                         dd_MovementType,
                                         dd_CarrierActual,
                                         Dim_CarrierBillingId,
                                         dd_DomesticContainerID,
                                         Dim_ShipmentTypeId,
                                         Dim_RouteId,
                                         dD_VoyageNo,
                                         Dim_DateIdShipmentEnd,
                                         Dim_DateIdDepartureFromOrigin,
                                         dd_ContainerSize,
                                         dd_3PLFlag,
                                         Dim_DateIdPOBuy,
                                         Dim_PODocumentTypeId,
                                         Dim_DateIdPOETA,
                                         dim_purchasegroupid,
                                         Dim_partid,
                                         Dim_Vendorid,
                                         Dim_DateIdGR,
                                         Dim_PORouteId,
                                         Dim_CustomVendorPartnerFunctionId,
                                         Dim_CustomVendorPartnerFunctionId1,
                                         Dim_POAFSSeasonId,
                                         ct_POScheduleQty,
                                         ct_POItemReceivedQty,
					 ct_POInboundDeliveryQty,
                                         dd_CustomerPOStatus,
                                         dd_PackageCount,
                                         dd_BillofLadingNo,
                                         Dim_CompanyId,
                                         Dim_PlantId,
                                         dd_OriginContainerId,
                                         ct_InboundDlvryCMtrVolume,
                                         ct_InboundDeliveryQty,
										 Dim_DateidDlvrDocCreated,
										 Dim_DateIdPODelivery,
										 ct_DelivReceivedQty,
										 ct_ReceivedQty,
										 dd_ShipmentStatus,
										 ct_ItemQty,
										 Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
										 dim_dateidactualpo,
										 dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
										 dim_dateidMatAvailability,
										 dd_DeliveryNumber,
										 dim_dateidTransfer,
										 ct_DeliveryGrossWght, 
										 ct_deliveryNetWght, 
										 Dim_DeliveryRouteId, 
										 dim_shippingconditionid,
										 dim_tranportbyshippingtypeid,
										 dd_ShortText,
										 dd_noofforeigntrade,
										 dim_transportconditionid
										 )
   SELECT IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN 
           WHERE TABLE_NAME = 'fact_shipmentindelivery_temp' ), 1) + ROW_NUMBER() OVER(order by '')  fact_shipmentindeliveryid,
                  p.dd_DocumentNo,
                  p.dd_DocumentItemNo,
                  p.dd_ScheduleNo,
                  'Not Set',
                  0,
                  'Not Set',
                  1,
                  'Not Set',
                  'Not Set',
                  'Not Set',
                  1,
                  'Not Set',
                  1 Dim_ShipmentTypeId,
                  1 Dim_RouteId,
                  'Not Set',
                  1 Dim_DateIdShipmentEnd,
                  1 Dim_DateIdDepartureFromOrigin,
                  'Not Set',
                  'Not Set',
                  p.Dim_DateIdSchedOrder,
                  p.Dim_DocumentTypeid,
                  p.Dim_DateidAFSDelivery,
                  dim_purchasegroupid,
                  Dim_partid,
                  Dim_Vendorid,
                  Dim_DateIdLastGR,
                  p.dim_AFSRouteid,
                  p.Dim_CustomPartnerFunctionId4,
                  p.Dim_CustomPartnerFunctionId3,
                  p.Dim_AfsSeasonId,
                  ct_DeliveryQty,
                  p.ct_ReceivedQty ct_POItemReceivedQty,
		  ct_QtyReduced,
                  'Not Set',
                  0,
                  'Not Set' dd_BillofLadingNo,
                  p.Dim_CompanyId,
                  Dim_plantidordering,
                  'Not Set',
                  0.0000,
                  0,
				  1 Dim_DateidDlvrDocCreated,
				  p.Dim_DateIdDelivery,
				  0.0000,
				  0.0000 ct_ReceivedQty,
				  'Not Set',
				  p.ct_ItemQty,
				  p.Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
				  p.Dim_DateidOrder,
				  p.dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
				  p.dim_dateidMatAvailability,
				  p.dd_DeliveryNumber,
				  1 dim_dateidTransfer,
				  0.0000 ct_DeliveryGrossWght,
				  0.0000 ct_deliveryNetWght,
				  1 Dim_DeliveryRouteId,
				  1 dim_shippingconditionid,
				  1 dim_tranportbyshippingtypeid,
				  'Not Set',
				  p.dd_noofforeigntrade,
				  1 dim_transportconditionid
				  
             FROM facT_purchase p
            WHERE NOT EXISTS
                         (SELECT 1
                            FROM fact_shipmentindelivery_temp f1
                           WHERE    f1.dd_DocumentNo = p.dd_DocumentNo
                                 AND f1.dd_DocumentItemNo = p.dd_DocumentItemNo
                                 AND f1.dd_ScheduleNo = p.dd_ScheduleNo);



DROP TABLE IF EXISTS tmp_EKES_LIKP_LIPS;

CREATE TABLE tmp_EKES_LIKP_LIPS AS 
SELECT EKES_EBELN,EKES_EBELP,EKES_J_3AETENR,LIKP_ANZPK,LIKP_BOLNR,LIKP_LFDAT,LIKP_TRAID,LIKP_TRATY,LIKP_VBELN,LIKP_VOLUM,LIKP_WADAT_IST,LIKP_ERDAT,sum(LIPS_LFIMG) AS LFIMG
FROM EKES_LIKP_LIPS
WHERE ekes_j_3aetenr <> 0 
group by EKES_EBELN,EKES_EBELP,EKES_J_3AETENR,LIKP_ANZPK,LIKP_BOLNR,LIKP_LFDAT,LIKP_TRAID,LIKP_TRATY,LIKP_VBELN,LIKP_VOLUM,LIKP_WADAT_IST,LIKP_ERDAT;

UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateIdETAFinalDest = dt.Dim_DateId
FROM     tmp_EKES_LIKP_LIPS elp, 
	 Dim_Date dt,
         Dim_Company c,
	 fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_LFDAT
  AND dt.CompanyCode = c.CompanyCode;

  
UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateIdInboundDeliveryDate = dt.Dim_DateId
FROM tmp_EKES_LIKP_LIPS elp, 
		Dim_Date dt,
		Dim_Company c,
		fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_LFDAT
  AND dt.CompanyCode = c.CompanyCode;


UPDATE fact_shipmentindelivery_temp ft
SET Dim_MeansofTransTypeId = pmt.Dim_PackagingMaterialTypeId
FROM tmp_EKES_LIKP_LIPS elp, 
     Dim_PackagingMaterialType pmt,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND pmt.PackagingMaterialType = LIKP_TRATY
  AND pmt.RowIsCurrent = 1;

UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateidActualGI = dt.Dim_DateId
FROM tmp_EKES_LIKP_LIPS elp, 
      Dim_Date dt,
      Dim_Company c,
      fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_WADAT_IST
  AND dt.CompanyCode = c.companycode;


  
UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateidDlvrDocCreated = dt.Dim_DateId
FROM tmp_EKES_LIKP_LIPS elp,  
     Dim_Date dt,
     Dim_Company c,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_ERDAT
  AND dt.CompanyCode = c.companycode;  
  
  
  
UPDATE fact_shipmentindelivery_temp ft
SET ft.dd_OriginContainerId = ifnull(LIKP_TRAID,'Not Set')
FROM tmp_EKES_LIKP_LIPS elp,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR;


UPDATE fact_shipmentindelivery_temp ft
SET ft.ct_InboundDeliveryQty = ifnull(LFIMG,0)
FROM tmp_EKES_LIKP_LIPS elp,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR;

DROP TABLE IF EXISTS tmp_EKES_LIKP_LIPS;

UPDATE fact_shipmentindelivery_temp ft
SET dd_3PLFlag = ifnull(EIKP_LADEL, 'Not Set')
FROM EKES_LIKP_LIPS elp, EIKP e,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
AND ft.dd_DocumentNo = elp.EKES_EBELN
AND ft.dd_DocumentItemNo = elp.EKES_EBELP
AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
AND elp.LIKP_EXNUM = e.EIKP_EXNUM
AND (dd_3PLFlag IS NULL OR dd_3PLFlag = 'Not Set');


UPDATE facT_shipmentindelivery_temp ft
SET ft.Dim_DateidASNOutput =  dt.dim_dateid
FROM NAST n, Dim_Date dt, Dim_company c,
     facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND ft.dim_companyid = c.dim_companyid
AND n.NAST_DATVR = dt.DateValue
AND dt.CompanyCode = c.CompanyCode;



UPDATE facT_shipmentindelivery_temp ft
SET ft.Dim_ProcessingStatusId =  s.Dim_ProcessingMessageStatusId
FROM NAST n, Dim_ProcessingMessageStatus s,
     facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_VSTAT = s.ProcessingMessage
AND n.NAST_KSCHL = 'ZINB'
AND s.RowIsCurrent = 1;


UPDATE facT_shipmentindelivery_temp ft
SET ft.dd_processingtime =  n.NAST_UHRVR
FROM NAST n, facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_UHRVR IS NOT NULL;


UPDATE facT_shipmentindelivery_temp ft
SET ft.dd_processingtime =  '000000'
FROM NAST n, facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_UHRVR IS NULL;

UPDATE facT_shipmentindelivery_temp ft
SET ft.dd_MessageType =  n.NAST_KSCHL
FROM NAST n, facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY;

DROP TABLE IF EXISTS tmp_vttk_vttp_maxcreateddatetime; 
CREATE table tmp_vttk_vttp_maxcreateddatetime as
SELECT vttp_vbeln, vttk_tknum,vttk_erdat,vttk_erzet,RANK() OVER (PARTITION BY vttp_vbeln ORDER BY vttk_erdat DESC,vttk_erzet DESC) as Rank from vttk_vttp; 
 
update fact_shipmentindelivery_temp  t1
set dd_CurrentShipmentFlag = 'X' 
from tmp_vttk_vttp_maxcreateddatetime t,fact_shipmentindelivery_temp  t1
WHERE t.vttp_vbeln = t1.dd_InbounddeliveryNo
AND t.vttk_tknum = t1.dd_ShipmentNo
AND t.Rank = 1;


update fact_shipmentindelivery_temp  t1
set dd_CurrentShipmentFlag = 'Not Set' 
from tmp_vttk_vttp_maxcreateddatetime t, fact_shipmentindelivery_temp  t1
WHERE t.vttp_vbeln = t1.dd_InbounddeliveryNo
AND t.vttk_tknum = t1.dd_ShipmentNo
AND t.Rank <> 1;
	
DROP TABLE IF EXISTS tmp_vttk_vttp_maxcreateddatetime; 

UPDATE fact_shipmentindelivery_temp t1
SET t1.dd_CartonLevelDtlFlag = 'X'
WHERE t1.dd_InboundDeliveryNo <> 'Not Set'
AND EXISTS ( SELECT 1 FROM vekp v
               WHERE v.VEKP_VPOBJKEY = t1.dd_InboundDeliveryNo);

UPDATE fact_shipmentindelivery_temp t1
SET t1.dd_CartonLevelDtlFlag = 'Not Set'
WHERE t1.dd_InboundDeliveryNo <> 'Not Set'
AND NOT EXISTS ( SELECT 1 FROM vekp v
                WHERE v.VEKP_VPOBJKEY = t1.dd_InboundDeliveryNo);

UPDATE	facT_shipmentindelivery_temp  st
SET st.Dim_DateIdPOBuy = p.Dim_DateIdSchedOrder,
    st.Dim_PODocumentTypeId = p.Dim_DocumentTypeid,
    st.Dim_DateIdPOETA = p.Dim_DateidAFSDelivery
FROM fact_purchase p, facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

/* Update Material Description = dd_ShortText*/ 

UPDATE	facT_shipmentindelivery_temp  st
SET st.dd_ShortText = p.dd_ShortText  
FROM fact_purchase p, facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

/* Commented Marius

update facT_shipmentindelivery_temp st
FROM VBFA_MKPF vm, dim_date dt
SET st.Dim_DateIdGR = dt.dim_dateid
Where vm.MKPF_BUDAT = dt.DateValue
AND st.dd_InboundDeliveryNo=vm.VBFA_VBELV
AND st.Dim_DateIdGR <> dt.dim_dateid */


UPDATE	facT_shipmentindelivery_temp  st
SET st.dim_purchasegroupid = p.Dim_PurchaseGroupId,
    st.Dim_PartId = p.Dim_Partid,
        st.Dim_VendorId = p.Dim_vendorid
FROM fact_purchase p, facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

UPDATE fact_shipmentindelivery_temp t
SET t.Dim_ProductHierarchyId = ph.Dim_ProductHierarchyId
FROM dim_part pt, dim_producthierarchy ph, fact_shipmentindelivery_temp t
WHERE t.dim_partid = pt.dim_partid
  AND pt.ProductHierarchy = ph.ProductHierarchy
  AND ph.RowIsCurrent = 1;

UPDATE fact_shipmentindelivery_temp st
SET st.ct_POInboundDeliveryQty = p.ct_QtyReduced
FROM fact_purchase p,
     fact_shipmentindelivery_temp st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo
AND st.dd_InboundDeliveryNo <> 'Not Set'
AND st.ct_POInboundDeliveryQty = 0;
  
UPDATE facT_shipmentindelivery_temp  st
SET st.Dim_PORouteId = p.dim_AFSRouteid,
    st.Dim_CustomVendorPartnerFunctionId = p.Dim_CustomPartnerFunctionId4,
    st.Dim_CustomVendorPartnerFunctionId1 = p.Dim_CustomPartnerFunctionId3,
    st.Dim_POAFSSeasonId = p.Dim_AfsSeasonId
FROM fact_purchase p, facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_ScheduleQty_POItem;
CREATE TABLE tmp_ScheduleQty_POItem AS
SELECT dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo, max(ct_DeliveryQty) as ScheduleQty,max(ct_ReceivedQty) as ReceivedQty from fact_purchase
group by dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo;

UPDATE facT_shipmentindelivery_temp  st
SET st.ct_POScheduleQty = p.ScheduleQty,
    st.ct_POItemReceivedQty = p.ReceivedQty
FROM tmp_ScheduleQty_POItem p,
     facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;


DROP TABLE IF EXISTS tmp_ScheduleQty_POItem;

UPDATE facT_shipmentindelivery_temp st
SET st.dd_SalesDocNo = ifnull(a.dd_salesdocno,'Not Set'),
      st.dd_SalesItemNo = ifnull(a.dd_salesitemno,0),
      st.dd_SalesScheduleNo = ifnull(a.dd_Salesscheduleno,0)
FROM fact_purchase a,  facT_shipmentindelivery_temp st
WHERE st.dd_DocumentNo = a.dd_DocumentNo
AND st.dd_DocumentItemNo = a.dd_DocumentItemNo
AND st.dd_ScheduleNo = a.dd_ScheduleNo;


drop table if exists tmp_uniquejoincond;
create table tmp_uniquejoincond
as
select distinct so.Dim_DateIdAfsCancelDate, so.Dim_SalesDocumentTypeid, so.Dim_DateIdAfsReqDelivery, so.Dim_CustomPartnerFunctionId1,
                so.Dim_DocumentCategoryid,
                so.dd_SalesDocNo,  so.dd_SalesItemNo, so.dd_ScheduleNo
from fact_SalesOrder so;



UPDATE facT_shipmentindelivery_temp st
SET st.Dim_DateIdSOCancelDate = so.Dim_DateIdAfsCancelDate,
    st.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid,
    st.Dim_DateIdReqDeliveryDate = so.Dim_DateIdAfsReqDelivery,
    st.Dim_CustomPartnerFunctionId1 = so.Dim_CustomPartnerFunctionId1
FROM tmp_uniquejoincond so,
	 facT_shipmentindelivery_temp st	  
WHERE st.dd_SalesDocNo = so.dd_SalesDocNo
  AND st.dd_SalesItemNo = so.dd_SalesItemNo
  AND st.dd_SalesScheduleNo = so.dd_ScheduleNo;


/*Added on Dec 10th 2014, by Roxana*/

UPDATE facT_shipmentindelivery_temp st
SET st.Dim_DocumentCategoryid=so.Dim_DocumentCategoryid
FROM tmp_uniquejoincond so, facT_shipmentindelivery_temp st
WHERE st.dd_SalesDocNo = so.dd_SalesDocNo
  AND st.dd_SalesItemNo = so.dd_SalesItemNo
  AND st.dd_SalesScheduleNo = so.dd_ScheduleNo
  AND st.Dim_DocumentCategoryid<>so.Dim_DocumentCategoryid;
  
drop table if exists tmp_uniquejoincond; 
  
DROP TABLE IF EXISTS tmp_AirSeaEstDateCalculation;

CREATE TABLE  tmp_AirSeaEstDateCalculation as 
select distinct vttk_shtyp ShipType, vttk_Dpten shipenddate, 
                vttk_route Route, substr(vttk_route,1,5) altrouteprefix ,
				'Not Set' altroute, 
                CONVERT( decimal (11,0), 0) totalduration 
from vttk_vttp
where vttk_Dpten IS NOT NULL and vttk_Route is not null;


UPDATE tmp_AirSeaEstDateCalculation t2
SET t2.altroute = t1.tvrab_route,
    t2.totalduration = t1.tvrab_gesztd
FROM tvrab t1, tmp_AirSeaEstDateCalculation t2
where t1.tvrab_route <> t2.route
AND substr(t1.tvrab_route,1,5) = t2.altrouteprefix
AND t2.altroute = 'Not Set';


UPDATE tmp_AirSeaEstDateCalculation t2
SET totalduration = round(totalduration/(3600*24),0)
where totalduration <> 0;

UPDATE facT_shipmentindelivery_temp t
SET t.Dim_DateIdAirorSeaPlanningEstETA =  t.Dim_DateIdShipmentEnd
FROM tmp_AirSeaEstDateCalculation t1,
     dim_Route r1, 
     facT_shipmentindelivery_temp t
where r1.dim_routeid = t.dim_routeid
AND r1.routecode = t1.route
AND t1.totalduration = 0;

DROP TABLE IF EXISTS tmp_DateAirSeaCalculation;
CREATE TABLE tmp_DateAirSeaCalculation AS
SELECT  distinct r1.dim_routeid, t.Dim_DateIdShipmentEnd, dt2.dim_dateid  FROM facT_shipmentindelivery_temp t,
       dim_company c1, dim_shipmenttype st, dim_Route r1,
       dim_date dt,
	   tmp_AirSeaEstDateCalculation t1,
       dim_Date dt2
where t.dim_Companyid = c1.dim_companyid
AND  t.dim_shipmenttypeid = st.dim_shipmenttypeid 
AND t.Dim_DateIdShipmentEnd = dt.dim_Dateid
AND r1.dim_routeid = t.dim_routeid
AND dt.datevalue = t1.shipenddate
AND r1.routecode = t1.route
and st.shipmenttype = t1.ShipType
AND t1.totalduration > 0
AND dt2.datevalue = (dt.datevalue + (interval '1' day)*t1.totalduration )
AND dt2.companycode = c1.companycode;

UPDATE facT_shipmentindelivery_temp t
SET t.Dim_DateIdAirorSeaPlanningEstETA =  t1.dim_dateid
   FROM tmp_DateAirSeaCalculation t1, facT_shipmentindelivery_temp t
where t.dim_routeid = t1.dim_routeid
AND t.Dim_DateIdShipmentEnd = t1.Dim_DateIdShipmentEnd
AND (t.Dim_DateIdAirorSeaPlanningEstETA IS NULL OR t.Dim_DateIdAirorSeaPlanningEstETA = 1);

DROP TABLE IF EXISTS tmp_DateAirSeaCalculation;
DROP TABLE IF EXISTS tmp_AirSeaEstDateCalculation;   

/* Andra : 19th of May 2014 : Add Dim_PurchaseMiscid, dd_DeletionIndicator*/ 

UPDATE fact_shipmentindelivery_temp  st
SET st.Dim_PurchaseMiscid = p.Dim_PurchaseMiscid
FROM fact_purchase p, fact_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;


UPDATE fact_shipmentindelivery_temp  st
SET st.dd_DeletionIndicator = p.dd_DeletionIndicator
FROM fact_purchase p, fact_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

UPDATE fact_shipmentindelivery_temp  st
SET st.ct_scheduleLineQty = ifnull(p.ct_scheduleLineQty,0)
FROM fact_purchase p, fact_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp1_facT_shipmentindelivery_temp;
CREATE TABLE tmp1_facT_shipmentindelivery_temp 
AS 
sELECT distinct dd_DocumentNo,dd_DocumentItemNo,
sum(ct_POInboundDeliveryQty) InboundQty,
sum(ct_POItemReceivedQty) Receivedqty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,dd_DocumentItemNo;
drop table if exists tmp_facT_shipmentindelivery_temp_unstable;
create table tmp_facT_shipmentindelivery_temp_unstable
as
select distinct facT_shipmentindeliveryid
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp1_facT_shipmentindelivery_temp t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.InboundQty = 0;
  
UPDATE facT_shipmentindelivery_temp t
SET t.dd_CustomerPOStatus = 'Open'
FROM  
   tmp_facT_shipmentindelivery_temp_unstable tmp, facT_shipmentindelivery_temp t
WHERE tmp.facT_shipmentindeliveryid = t.facT_shipmentindeliveryid;
drop table if exists tmp_facT_shipmentindelivery_temp_unstable;

UPDATE facT_shipmentindelivery_temp t
SET dd_CustomerPOStatus = 'In-Transit'
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp1_facT_shipmentindelivery_temp t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.ReceivedQty = 0
  AND t1.InboundQty > 0;


drop table if exists tmp_facT_shipmentindelivery_temp_unstable;
create table tmp_facT_shipmentindelivery_temp_unstable
as
select distinct facT_shipmentindeliveryid
from     fact_purchase p,
         dim_purchasemisc pm,
         tmp1_facT_shipmentindelivery_temp t1, 
         facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.ReceivedQty > 0;

UPDATE facT_shipmentindelivery_temp t
SET dd_CustomerPOStatus = 'Received'
FROM  tmp_facT_shipmentindelivery_temp_unstable tmp, facT_shipmentindelivery_temp t
WHERE tmp.facT_shipmentindeliveryid = t.facT_shipmentindeliveryid;

drop table if exists tmp_facT_shipmentindelivery_temp_unstable;
create table tmp_facT_shipmentindelivery_temp_unstable
as
select distinct facT_shipmentindeliveryid
from    fact_purchase p,
        dim_purchasemisc pm, facT_shipmentindelivery_temp t
WHERE  p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete = 'X'
  AND pm.ItemGRIndicator = 'X'
  AND t.ct_POItemReceivedQty > 0;

UPDATE facT_shipmentindelivery_temp t
SET dd_CustomerPOStatus = 'Closed'
FROM  tmp_facT_shipmentindelivery_temp_unstable tmp, facT_shipmentindelivery_temp t
WHERE tmp.facT_shipmentindeliveryid = t.facT_shipmentindeliveryid;


DROP TABLE IF EXISTS tmp1_facT_shipmentindelivery_temp;  

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POScheduleQty
WHERE   dd_CustomerPOStatus IN ('Open');

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POItemReceivedQty
WHERE   dd_CustomerPOStatus IN ('Closed','Received');

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POInboundDeliveryQty
WHERE   dd_CustomerPOStatus IN ('In-Transit');


/* Andra 16 May: Update of the dd_ShipmentStatus*/
DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_shipmentstatus;
CREATE TABLE tmp_facT_shipmentindelivery_shipmentstatus AS 
SELECT dd_DocumentNo,
       dd_DocumentItemNo, 
	   dd_ScheduleNo,
	   dd_ShipmentNo,
       dd_InboundDeliveryNo
	   ,sum(ct_POInboundDeliveryQty) as ct_POInboundDeliveryQty, sum(ct_DelivReceivedQty) AS ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,
       dd_DocumentItemNo, 
	   dd_ScheduleNo,
	   dd_ShipmentNo,
       dd_InboundDeliveryNo;

UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'In Transit'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo 
  AND pm.afsdeliverycomplete <> 'X'
  AND p.dd_DeletionIndicator = 'Not Set'
  AND t1.ct_DelivReceivedQty = 0;


UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'Received'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo 
  AND pm.afsdeliverycomplete <> 'X'
  AND p.dd_DeletionIndicator = 'Not Set'
  AND t1.ct_DelivReceivedQty > 0;

UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'Closed'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo 
  AND pm.afsdeliverycomplete = 'X'
  AND t1.ct_POInboundDeliveryQty > 0;
  
DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_shipmentstatus;

DROP TABLE IF EXISTS tmp_facT_sid_shipmentstatus_scheduleLevel;
CREATE TABLE tmp_facT_sid_shipmentstatus_scheduleLevel AS 
SELECT dd_DocumentNo,
       dd_DocumentItemNo, 
	   dd_ScheduleNo
	   ,sum(ct_POInboundDeliveryQty) as ct_POInboundDeliveryQty, sum(ct_DelivReceivedQty) AS ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,
       dd_DocumentItemNo, 
	   dd_ScheduleNo;
  
UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'Cancelled'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_sid_shipmentstatus_scheduleLevel t1,
	facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND pm.afsdeliverycomplete = 'X'
  AND t1.ct_POInboundDeliveryQty = 0;

  
UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'Deleted'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_sid_shipmentstatus_scheduleLevel t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND p.dd_DeletionIndicator = 'L';


DROP TABLE IF EXISTS tmp_facT_sid_shipmentstatus_scheduleLevel; 

/* Andra 16 May: Update of the dd_ShipmentStatus*/


/* Andra 16 May: Update of the Qtys for DirectShip*/

UPDATE facT_shipmentindelivery_temp t
SET t.ct_InTransitQty = (t.ct_ReceivedQty - t.ct_DelivReceivedQty); 
  
UPDATE facT_shipmentindelivery_temp t
SET t.ct_ClosedQty = t.ct_DelivReceivedQty
FROM fact_purchase p,
    dim_purchasemisc pm,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND pm.afsdeliverycomplete = 'X'; 
  
UPDATE facT_shipmentindelivery_temp t
SET t.ct_CancelledQty = p.ct_scheduleLineQty
FROM fact_purchase p,
    dim_purchasemisc pm,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND p.ct_QtyReduced = 0
  AND p.ct_ReceivedQty = 0
  AND pm.afsdeliverycomplete = 'X'; 
  
DROP TABLE IF EXISTS tmp_fact_purchase_OpenQty_Item; 
CREATE TABLE tmp_fact_purchase_OpenQty_Item AS 
SELECT dd_DocumentNo,dd_DocumentItemNo,sum(ct_scheduleLineQty) as ct_scheduleLineQty
from fact_purchase
group by dd_DocumentNo,dd_DocumentItemNo;

DROP TABLE IF EXISTS tmp_fact_sid_OpenQty_Item; 
CREATE TABLE tmp_fact_sid_OpenQty_Item AS 
SELECT dd_DocumentNo,dd_DocumentItemNo,sum(ct_InTransitQty) as ct_InTransitQty,sum(ct_DelivReceivedQty) as ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,dd_DocumentItemNo;

UPDATE facT_shipmentindelivery_temp t
SET t.ct_OpenQty = ifnull((tp.ct_scheduleLineQty - (tsid.ct_InTransitQty - tsid.ct_DelivReceivedQty)),0)
FROM tmp_fact_purchase_OpenQty_Item tp,
     tmp_fact_sid_OpenQty_Item tsid,
     facT_shipmentindelivery_temp t 
WHERE t.dd_DocumentNo = tp.dd_DocumentNo
  AND t.dd_documentItemNo = tp.dd_DocumentItemNo
  AND t.dd_DocumentNo = tsid.dd_DocumentNo
  AND t.dd_documentItemNo = tsid.dd_DocumentItemNo; 

  
DROP TABLE IF EXISTS tmp_fact_purchase_OpenQty_Item;
DROP TABLE IF EXISTS tmp_fact_sid_OpenQty_Item;
  
DROP TABLE IF EXISTS tmp_tsegeUpdateEventDates;

CREATE TABLE tmp_tsegeUpdateEventDates AS 
SELECT DISTINCT TSEGE_HEAD_HDL, TSEGE_EVEN, TSEGE_EVEN_VERTY, 
(case when TSEGE_EVEN_TSTTO = 0 THEN '19000101' ELSE substr(tsege_even_tstto,1,8) END) AS TSEGE_EVEN_TSTTO
FROM TSEGE;

 UPDATE fact_shipmentindelivery_temp st
 SET st.Dim_DateId3plYard = dt.dim_Dateid
  FROM vttk_vttp v, 
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c,
       fact_shipmentindelivery_temp st
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'Z3PL'
   AND to_date(t.tsege_even_tstto, 'MM-DD-YYYY') = to_date(dt.datevalue, 'MM-DD-YYYY')
   AND t.TSEGE_EVEN_VERTY = 1
   AND dt.companycode = c.companycode;

   
/* Andra 18 Jun - add Max functionon 3PL In Yard Date for each inbound delivery */

DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_3pl;
CREATE TABLE tmp_facT_shipmentindelivery_3pl AS 
SELECT dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo,dd_InboundDeliveryNo, max(Dim_DateId3plYard) Dim_DateId3plYard
FROM fact_shipmentindelivery_temp
GROUP BY dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo,dd_InboundDeliveryNo;

UPDATE fact_shipmentindelivery_temp sd
SET sd.Dim_DateId3plYard = t.Dim_DateId3plYard
FROM tmp_facT_shipmentindelivery_3pl t, fact_shipmentindelivery_temp sd
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.Dim_DateId3plYard <> t.Dim_DateId3plYard;


/* End of changes Andra 18 Jun - add Max functionon 3PL In Yard Date for each inbound delivery */

UPDATE fact_shipmentindelivery_temp st
 SET st.Dim_DateIdContainerAtDCDoor = dt.dim_Dateid
  FROM vttk_vttp v, 
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c, 
        fact_shipmentindelivery_temp st
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'ZDOOR'
   AND t.TSEGE_EVEN_VERTY = 1
   AND to_date(t.tsege_even_tstto, 'MM-DD-YYYY') = to_date(dt.datevalue, 'MM-DD-YYYY')
   AND dt.companycode = c.companycode;


 UPDATE fact_shipmentindelivery_temp st
 SET st.Dim_DateIdContainerInYard = dt.dim_Dateid
  FROM vttk_vttp v, 
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c,
       fact_shipmentindelivery_temp st
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'ZYARD'
   AND t.TSEGE_EVEN_VERTY = 1
   AND to_date(t.tsege_even_tstto, 'MM-DD-YYYY') = to_date(dt.datevalue, 'MM-DD-YYYY')
   AND dt.companycode = c.companycode;



DROP TABLE IF EXISTS tmp_SplitSalesNetValue;
CREATE TABLE tmp_SplitSalesNetValue 
AS
select dd_Documentno,dd_documentitemno,sd.dd_Inbounddeliveryno,sd.dd_Shipmentno,sd.dd_ShipmentItemNo,
sd.dd_salesdocno,sd.dd_salesitemno,dd_Salesscheduleno,
sum(sd.ct_InboundDeliveryQty ) totalschedqty, 
SUM(so.amt_DicountAccrualNetPrice*so.ct_ConfirmedQty) avgNetValue,
SUM(so.amt_DicountAccrualNetPrice*so.ct_ConfirmedQty)/(case when sum(sd.ct_InboundDeliveryQty) = 0 then null else sum(sd.ct_InboundDeliveryQty) end) avgConfInb

from fact_shipmentindelivery_temp sd,
fact_Salesorder so
where sd.dd_Salesdocno = so.dd_Salesdocno
and sd.dd_salesitemno = so.dd_salesitemno
and sd.dd_Salesscheduleno = so.dd_scheduleno
and sd.dd_Salesdocno <> 'Not Set'
group by (dd_Documentno,dd_documentitemno,sd.dd_Inbounddeliveryno,sd.dd_Shipmentno,sd.dd_ShipmentItemNo,sd.dd_salesdocno,sd.dd_salesitemno,dd_Salesscheduleno);

DROP TABLE IF EXISTS tmp_ResultFromSplitSalesandShipmentInbound;
CREATE TABLE tmp_ResultFromSplitSalesandShipmentInbound AS
SELECT t.*,sd.dd_scheduleno,sd.ct_InboundDeliveryQty  *  avgNetValue / (CASE WHEN totalschedqty <> 0  THEN totalschedqty ELSE 1 END) AS avg_Value  
FROM fact_shipmentindelivery_temp sd
,tmp_SplitSalesNetValue t
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
AND sd.dd_Shipmentno = t.dd_Shipmentno
AND sd.dd_ShipmentItemNo = t.dd_ShipmentItemNo
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.dd_salesdocno = t.dd_salesdocno
AND sd.dd_salesitemno = t.dd_salesitemno
AND sd.dd_Salesscheduleno = t.dd_Salesscheduleno
AND  sd.dd_salesdocno <> 'NotSet'; 


UPDATE fact_shipmentindelivery_temp sd
SET sd.amt_so_afsnetvalue = t.avg_Value
from  tmp_ResultFromSplitSalesandShipmentInbound t,
      fact_shipmentindelivery_temp sd
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
and sd.dd_scheduleno = t.dd_Scheduleno
AND sd.dd_Shipmentno = t.dd_Shipmentno
AND sd.dd_ShipmentItemNo = t.dd_ShipmentItemNo
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.dd_salesdocno = t.dd_salesdocno
AND sd.dd_salesitemno = t.dd_salesitemno
AND sd.dd_Salesscheduleno = t.dd_Salesscheduleno
AND  sd.dd_salesdocno <> 'Not Set';

DROP TABLE IF EXISTS tmp_ResultFromSplitSalesandShipmentInbound;
DROP TABLE IF EXISTS tmp_SplitSalesNetValue;


   
/* Setting the nulls to default values */		

UPDATE fact_shipmentindelivery_temp SET Dim_BillingBlockid = 1 WHERE Dim_BillingBlockid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Companyid = 1 WHERE Dim_Companyid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ControllingAreaid = 1 WHERE Dim_ControllingAreaid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CostCenterid = 1 WHERE Dim_CostCenterid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerGroup1id = 1 WHERE Dim_CustomerGroup1id IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerGroup2id = 1 WHERE Dim_CustomerGroup2id IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerID = 1 WHERE Dim_CustomerID IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomeridShipTo = 1 WHERE Dim_CustomeridShipTo IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId = 1 WHERE Dim_CustomPartnerFunctionId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId1 = 1 WHERE Dim_CustomPartnerFunctionId1 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId2 = 1 WHERE Dim_CustomPartnerFunctionId2 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomVendorPartnerFunctionId = 1 WHERE Dim_CustomVendorPartnerFunctionId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomVendorPartnerFunctionId1 = 1 WHERE Dim_CustomVendorPartnerFunctionId1 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateId3plYard = 1 WHERE Dim_DateId3plYard IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidActualGI = 1 WHERE Dim_DateidActualGI IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidDlvrDocCreated = 1 WHERE Dim_DateidDlvrDocCreated IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdAirorSeaPlanningEstETA = 1 WHERE Dim_DateIdAirorSeaPlanningEstETA IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidASNExpectedOutput = 1 WHERE Dim_DateidASNExpectedOutput IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidASNOutput = 1 WHERE Dim_DateidASNOutput IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdContainerAtDCDoor = 1 WHERE Dim_DateIdContainerAtDCDoor IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdContainerInYard = 1 WHERE Dim_DateIdContainerInYard IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdDepartureFromOrigin = 1 WHERE Dim_DateIdDepartureFromOrigin IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdETAFinalDest = 1 WHERE Dim_DateIdETAFinalDest IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidFirstDate = 1 WHERE Dim_DateidFirstDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdGR = 1 WHERE Dim_DateIdGR IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdPOBuy = 1 WHERE Dim_DateIdPOBuy IS NULL;

UPDATE fact_shipmentindelivery_temp SET Dim_DateIdPOETA = 1 WHERE Dim_DateIdPOETA IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdReqDeliveryDate = 1 WHERE Dim_DateIdReqDeliveryDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidShipDlvrFill = 1 WHERE Dim_DateidShipDlvrFill IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdShipmentCreated = 1 WHERE Dim_DateIdShipmentCreated IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidShipmentDelivery = 1 WHERE Dim_DateidShipmentDelivery IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdShipmentEnd = 1 WHERE Dim_DateIdShipmentEnd IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdSOCancelDate = 1 WHERE Dim_DateIdSOCancelDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidValidFrom = 1 WHERE Dim_DateidValidFrom IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidValidTo = 1 WHERE Dim_DateidValidTo IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DocumentCategoryid = 1 WHERE Dim_DocumentCategoryid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_MeansofTransTypeId = 1 WHERE Dim_MeansofTransTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Partid = 1 WHERE Dim_Partid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Plantid = 1 WHERE Dim_Plantid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_POAFSSeasonId = 1 WHERE Dim_POAFSSeasonId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PODocumentTypeId = 1 WHERE Dim_PODocumentTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PORouteId = 1 WHERE Dim_PORouteId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ProcessingStatusId = 1 WHERE Dim_ProcessingStatusId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ProfitCenterId = 1 WHERE Dim_ProfitCenterId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PurchasegroupId = 1 WHERE Dim_PurchasegroupId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_routeId = 1 WHERE Dim_routeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesDivisionid = 1 WHERE Dim_SalesDivisionid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesDocumentTypeid = 1 WHERE Dim_SalesDocumentTypeid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesGroupid = 1 WHERE Dim_SalesGroupid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesMiscId = 1 WHERE Dim_SalesMiscId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderHeaderStatusid = 1 WHERE Dim_SalesOrderHeaderStatusid IS NULL;
UPDATE fact_shipmentindelivery_temp SET dim_salesorderitemcategoryid = 1 WHERE dim_salesorderitemcategoryid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderItemStatusid = 1 WHERE Dim_SalesOrderItemStatusid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderRejectReasonid = 1 WHERE Dim_SalesOrderRejectReasonid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrgid = 1 WHERE Dim_SalesOrgid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ScheduleLineCategoryId = 1 WHERE Dim_ScheduleLineCategoryId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ShipmentTypeId = 1 WHERE Dim_ShipmentTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_TransactionGroupid = 1 WHERE Dim_TransactionGroupid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_TranspPlanningPointId = 1 WHERE Dim_TranspPlanningPointId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_VendorId = 1 WHERE Dim_VendorId IS NULL;
update fact_shipmentindelivery_temp set Dim_AFSTranspConditionid = 1 where Dim_AFSTranspConditionid is null; /* Marius 28 nov 2014 */
update fact_shipmentindelivery_temp SET dim_dateidactualpo = 1 where dim_dateidactualpo = 0;
update fact_shipmentindelivery_temp set dim_dateidAFSExFactEst = 1 where dim_dateidAFSExFactEst is null;  /* Marius 03 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_dateidMatAvailability  = 1 WHERE dim_dateidMatAvailability IS NULL; /*Marius 15 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_dateidTransfer  = 1 WHERE dim_dateidTransfer IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET Dim_DeliveryRouteId  = 1 WHERE Dim_DeliveryRouteId IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_shippingconditionid  = 1 WHERE dim_shippingconditionid IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_tranportbyshippingtypeid = 1 WHERE dim_tranportbyshippingtypeid IS NULL;

UPDATE fact_shipmentindelivery_temp SET dd_3PLFlag = 'Not Set' WHERE dd_3PLFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_BillofladingNo = 'Not Set' WHERE dd_BillofladingNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_BusinessCustomerPONo = 'Not Set' WHERE dd_BusinessCustomerPONo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CarrierActual  = 'Not Set' WHERE dd_CarrierActual IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CarrierBillingId  = 1 WHERE Dim_CarrierBillingId IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CartonLevelDtlFlag = 'Not Set' WHERE dd_CartonLevelDtlFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ContainerSize = 'Not Set' WHERE dd_ContainerSize IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CurrentShipmentFlag = 'Not Set' WHERE dd_CurrentShipmentFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CustomerPONo = 'Not Set' WHERE dd_CustomerPONo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CustomerPOStatus = 'Not Set' WHERE dd_CustomerPOStatus IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DocumentItemNo = 0 WHERE dd_DocumentItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DocumentNo  = 'Not Set' WHERE dd_DocumentNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DomesticContainerID  = 'Not Set' WHERE dd_DomesticContainerID IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_InboundDeliveryItemNo  = 0 WHERE dd_InboundDeliveryItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_InboundDeliveryNo  = 'Not Set' WHERE dd_InboundDeliveryNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ItemRelForDelv = 'Not Set' WHERE dd_ItemRelForDelv IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_MessageType = 'Not Set' WHERE dd_MessageType IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_MovementType = 'Not Set' WHERE dd_MovementType IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_OriginContainerId = 'Not Set' WHERE dd_OriginContainerId IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_PackageCount = 0 WHERE dd_PackageCount IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ProcessingTime = '000000' WHERE dd_ProcessingTime IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesDocNo = 'Not Set' WHERE dd_SalesDocNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesItemNo = 0 WHERE dd_SalesItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesScheduleNo = 0 WHERE dd_SalesScheduleNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ScheduleNo = 0 WHERE dd_ScheduleNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentItemNo = 0 WHERE dd_ShipmentItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentNo = 'Not Set' WHERE dd_ShipmentNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_Vessel = 'Not Set' WHERE dd_Vessel IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_VoyageNo = 'Not Set' WHERE dd_VoyageNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentStatus = 'Not Set' WHERE dd_ShipmentStatus IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DeliveryNumber = 'Not Set' WHERE dd_DeliveryNumber IS NULL;   /* Marius 17 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_transportconditionid = 1 WHERE dim_transportconditionid IS NULL;

UPDATE fact_shipmentindelivery_temp SET ct_ConfirmedQty = 0 WHERE ct_ConfirmedQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_DelayDays = 0 WHERE ct_DelayDays IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_InboundDeliveryQty = 0 WHERE ct_InboundDeliveryQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_InboundDlvryCMtrVolume = 0 WHERE ct_InboundDlvryCMtrVolume IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_POScheduleQty = 0 WHERE ct_POScheduleQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_PriceUnit = 0 WHERE ct_PriceUnit IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_POItemReceivedQty = 0 WHERE ct_POItemReceivedQty IS NULL; 
UPDATE fact_shipmentindelivery_temp SET ct_ReceivedQty = 0 WHERE ct_ReceivedQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_ShipmentQty = 0 WHERE ct_ShipmentQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_DeliveryGrossWght = 0 WHERE ct_DeliveryGrossWght IS NULL;  /* Marius 17 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET ct_deliveryNetWght = 0 WHERE ct_deliveryNetWght IS NULL;  /* Marius 17 Dec 2014 */

DROP TABLE IF EXISTS tmp_tsegeUpdateEventDates;

delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery;

truncate table fact_shipmentindelivery ;


insert into fact_shipmentindelivery i 
(FACT_SHIPMENTINDELIVERYID	,
DD_SHIPMENTNO	,
DD_SHIPMENTITEMNO	,
DD_SALESDOCNO	,
DD_DOCUMENTNO	,
DD_DOCUMENTITEMNO	,
CT_INBOUNDDELIVERYQTY	,
CT_CONFIRMEDQTY	,
CT_RECEIVEDQTY	,
CT_PRICEUNIT	,
DIM_DATEID3PLYARD	,
DIM_DATEIDASNOUTPUT	,
DIM_DATEIDASNEXPECTEDOUTPUT	,
DIM_PURCHASEGROUPID	,
DIM_DATEIDSHIPMENTEND	,
DIM_PLANTID	,
DIM_COMPANYID	,
DIM_SALESDIVISIONID	,
DIM_DOCUMENTCATEGORYID	,
DIM_SALESDOCUMENTTYPEID	,
DIM_SALESORGID	,
DIM_CUSTOMERID	,
DIM_DATEIDVALIDFROM	,
DIM_DATEIDVALIDTO	,
DIM_SALESGROUPID	,
DIM_COSTCENTERID	,
DIM_CONTROLLINGAREAID	,
DIM_BILLINGBLOCKID	,
DIM_TRANSACTIONGROUPID	,
DIM_SALESORDERREJECTREASONID	,
DIM_PARTID	,
DIM_SALESORDERHEADERSTATUSID	,
DIM_SALESORDERITEMSTATUSID	,
DIM_CUSTOMERGROUP1ID	,
DIM_CUSTOMERGROUP2ID	,
DIM_SALESORDERITEMCATEGORYID	,
DIM_DATEIDFIRSTDATE	,
DIM_SCHEDULELINECATEGORYID	,
DIM_SALESMISCID	,
DD_ITEMRELFORDELV	,
DIM_DATEIDSHIPMENTDELIVERY	,
DIM_DATEIDACTUALGI	,
DIM_DATEIDSHIPDLVRFILL	,
DIM_PROFITCENTERID	,
DIM_CUSTOMERIDSHIPTO	,
DIM_CUSTOMPARTNERFUNCTIONID	,
DIM_CUSTOMPARTNERFUNCTIONID1	,
DIM_CUSTOMPARTNERFUNCTIONID2	,
DD_CUSTOMERPONO	,
DIM_DATEIDSOCANCELDATE	,
DD_BUSINESSCUSTOMERPONO	,
DIM_ROUTEID	,
DD_CARRIERACTUAL	,
DD_PACKAGECOUNT	,
DD_CARTONLEVELDTLFLAG	,
DIM_DATEIDCONTAINERATDCDOOR	,
DIM_DATEIDCONTAINERINYARD	,
DD_CONTAINERSIZE	,
DD_CURRENTSHIPMENTFLAG	,
DIM_DATEIDDEPARTUREFROMORIGIN	,
DIM_DATEIDETAFINALDEST	,
DIM_DATEIDGR	,
DD_ORIGINCONTAINERID	,
CT_INBOUNDDLVRYCMTRVOLUME	,
DD_BILLOFLADINGNO	,
DIM_DATEIDAIRORSEAPLANNINGESTETA	,
DD_INBOUNDDELIVERYNO	,
DD_INBOUNDDELIVERYITEMNO	,
DIM_VENDORID	,
DIM_MEANSOFTRANSTYPEID	,
DD_MESSAGETYPE	,
DD_MOVEMENTTYPE	,
DIM_DATEIDPOBUY	,
DIM_DATEIDPOETA	,
DIM_PODOCUMENTTYPEID	,
DIM_POROUTEID	,
CT_POSCHEDULEQTY	,
DD_SCHEDULENO	,
DIM_POAFSSEASONID	,
DIM_SHIPMENTTYPEID	,
CT_SHIPMENTQTY	,
DD_VESSEL	,
DD_VOYAGENO	,
DIM_DATEIDSHIPMENTCREATED	,
DD_DOMESTICCONTAINERID	,
DD_3PLFLAG	,
DIM_DATEIDREQDELIVERYDATE	,
DD_CUSTOMERPOSTATUS	,
DIM_CUSTOMVENDORPARTNERFUNCTIONID	,
DIM_CUSTOMVENDORPARTNERFUNCTIONID1	,
DD_SALESITEMNO	,
DD_SALESSCHEDULENO	,
DIM_PROCESSINGSTATUSID	,
DIM_TRANSPPLANNINGPOINTID	,
CT_DELAYDAYS	,
DD_PROCESSINGTIME	,
DIM_PRODUCTHIERARCHYID	,
DIM_CARRIERBILLINGID	,
AMT_SO_AFSNETVALUE	,
DIM_DATEIDDLVRDOCCREATED	,
CT_POINBOUNDDELIVERYQTY	,
DIM_DATEIDPODELIVERY	,
CT_DELIVRECEIVEDQTY	,
DD_SHIPMENTSTATUS	,
CT_POITEMRECEIVEDQTY	,
CT_OPENQTY	,
CT_PRESHIPQTY	,
CT_INTRANSITQTY	,
CT_CLOSEDQTY	,
CT_CANCELLEDQTY	,
DIM_PURCHASEMISCID	,
DD_DELETIONINDICATOR	,
CT_SCHEDULELINEQTY	,
DIM_INBROUTEID	,
DW_INSERT_DATE	,
DW_UPDATE_DATE	,
DIM_PROJECTSOURCEID	,
CT_ITEMQTY	,
DIM_AFSTRANSPCONDITIONID	,
DIM_DATEIDACTUALPO	,
DIM_DATEIDAFSEXFACTEST	,
DIM_DATEIDMATAVAILABILITY	,
DD_DELIVERYNUMBER	,
DIM_DATEIDTRANSFER	,
CT_DELIVERYGROSSWGHT	,
CT_DELIVERYNETWGHT	,
DIM_DELIVERYROUTEID	,
DIM_SHIPPINGCONDITIONID	,
DIM_TRANPORTBYSHIPPINGTYPEID	,
DD_SHORTTEXT	,
DD_SEALNUMBER	,
DIM_TRANSPORTCONDITIONID	,
DIM_DATEIDINBOUNDDELIVERYDATE	,
DD_NOOFFOREIGNTRADE	)


Select

IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN 
           WHERE TABLE_NAME = 'fact_shipmentindelivery' ), 1) + ROW_NUMBER() OVER(order by '')  fact_shipmentindeliveryid,
DD_SHIPMENTNO	,
DD_SHIPMENTITEMNO	,
ifnull(DD_SALESDOCNO, 'Not Set'),
IFNULL(DD_DOCUMENTNO, 'Not Set'),
IFNULL(DD_DOCUMENTITEMNO,0),
IFNULL(CT_INBOUNDDELIVERYQTY,0),
IFNULL(CT_CONFIRMEDQTY,0)	,
IFNULL(CT_RECEIVEDQTY,0),
IFNULL(CT_PRICEUNIT,0)	,
IFNULL(DIM_DATEID3PLYARD, 1)	,
ifnull(DIM_DATEIDASNOUTPUT,1)	,
ifnull(DIM_DATEIDASNEXPECTEDOUTPUT,1)	,
ifnull(DIM_PURCHASEGROUPID, 1),
ifnull(DIM_DATEIDSHIPMENTEND,1)	,
ifnull(DIM_PLANTID,1)	,
ifnull(DIM_COMPANYID,1)	,
ifnull(DIM_SALESDIVISIONID,1)	,
ifnull(DIM_DOCUMENTCATEGORYID,1)	,
ifnull(DIM_SALESDOCUMENTTYPEID,1)	,
ifnull(DIM_SALESORGID, 1)	,
ifnull(DIM_CUSTOMERID, 1)	,
ifnull(DIM_DATEIDVALIDFROM,1)	,
ifnull(DIM_DATEIDVALIDTO, 1),
ifnull(DIM_SALESGROUPID, 1)	,
ifnull(DIM_COSTCENTERID,1)	,
ifnull(DIM_CONTROLLINGAREAID,1)	,
ifnull(DIM_BILLINGBLOCKID,1)	,
ifnull(DIM_TRANSACTIONGROUPID,1)	,
ifnull(DIM_SALESORDERREJECTREASONID,1)	,
ifnull(DIM_PARTID, 1) ,
ifnull(DIM_SALESORDERHEADERSTATUSID, 1)	,
ifnull(DIM_SALESORDERITEMSTATUSID,1)	,
ifnull(DIM_CUSTOMERGROUP1ID,1)	,
ifnull(DIM_CUSTOMERGROUP2ID, 1)	,
ifnull(DIM_SALESORDERITEMCATEGORYID,1)	,
ifnull(DIM_DATEIDFIRSTDATE,1)	,
ifnull(DIM_SCHEDULELINECATEGORYID, 1)	,
ifnull(DIM_SALESMISCID,1)	,
ifnull(DD_ITEMRELFORDELV, 'Not Set'),
ifnull(DIM_DATEIDSHIPMENTDELIVERY,1)	,
ifnull(DIM_DATEIDACTUALGI, 1)	,
ifnull(DIM_DATEIDSHIPDLVRFILL,1)	,
ifnull(DIM_PROFITCENTERID,1)	,
ifnull(DIM_CUSTOMERIDSHIPTO, 1)	,
ifnull(DIM_CUSTOMPARTNERFUNCTIONID,1)	,
ifnull(DIM_CUSTOMPARTNERFUNCTIONID1	, 1),
ifnull(DIM_CUSTOMPARTNERFUNCTIONID2, 1),
ifnull(DD_CUSTOMERPONO,'Not Set') ,
ifnull(DIM_DATEIDSOCANCELDATE,1)	,
ifnull(DD_BUSINESSCUSTOMERPONO, 'Not Set'),
ifnull(DIM_ROUTEID,1)	,
ifnull(DD_CARRIERACTUAL, 'Not Set')	,
ifnull(DD_PACKAGECOUNT,0)	,
ifnull(DD_CARTONLEVELDTLFLAG, 'Not Set'),
ifnull (DIM_DATEIDCONTAINERATDCDOOR,1)	,
ifnull (DIM_DATEIDCONTAINERINYARD,1)	,
ifnull(DD_CONTAINERSIZE, 0)	,
ifnull(DD_CURRENTSHIPMENTFLAG, 'Not Set')	,
ifnull(DIM_DATEIDDEPARTUREFROMORIGIN,1)	,
ifnull(DIM_DATEIDETAFINALDEST,1)	,
ifnull(DIM_DATEIDGR,1)	,
ifnull(DD_ORIGINCONTAINERID, 'Not Set'),
ifnull(CT_INBOUNDDLVRYCMTRVOLUME, 0),
ifnull(DD_BILLOFLADINGNO, 'Not Set'),
ifnull(DIM_DATEIDAIRORSEAPLANNINGESTETA,1)	,
ifnull(DD_INBOUNDDELIVERYNO,'Not Set')	,
ifnull(DD_INBOUNDDELIVERYITEMNO,0)	,
ifnull(DIM_VENDORID,1)	,
ifnull(DIM_MEANSOFTRANSTYPEID,1)	,
ifnull(DD_MESSAGETYPE,'Not Set'	),
ifnull(DD_MOVEMENTTYPE, 'Not Set'	),
ifnull(DIM_DATEIDPOBUY,1)	,
ifnull(DIM_DATEIDPOETA,1)	,
ifnull(DIM_PODOCUMENTTYPEID,1)	,
ifnull(DIM_POROUTEID,1)	,
ifnull(CT_POSCHEDULEQTY,0)	,
ifnull(DD_SCHEDULENO,0)	,
ifnull(DIM_POAFSSEASONID,1)	,
ifnull(DIM_SHIPMENTTYPEID,1)	,
ifnull(	CT_SHIPMENTQTY	,	0)	,
ifnull(	DD_VESSEL	,	'Not Set')	,
ifnull(	DD_VOYAGENO	,	'Not Set')	,
ifnull(	DIM_DATEIDSHIPMENTCREATED	,	1)	,
ifnull(	DD_DOMESTICCONTAINERID	,	0)	,
ifnull(	DD_3PLFLAG	,	'Not Set')	,
ifnull(	DIM_DATEIDREQDELIVERYDATE	,	1)	,
ifnull(	DD_CUSTOMERPOSTATUS	,	'Not Set')	,
ifnull(	DIM_CUSTOMVENDORPARTNERFUNCTIONID	,	1)	,
ifnull(	DIM_CUSTOMVENDORPARTNERFUNCTIONID1	,	1)	,
ifnull(	DD_SALESITEMNO	,	0)	,
ifnull(	DD_SALESSCHEDULENO	,	0)	,
ifnull(	DIM_PROCESSINGSTATUSID	,	1)	,
ifnull(	DIM_TRANSPPLANNINGPOINTID	,	1)	,
ifnull(	CT_DELAYDAYS	,	0)	,
ifnull(	DD_PROCESSINGTIME	,	'Not Set')	,
ifnull(	DIM_PRODUCTHIERARCHYID	,	1)	,
ifnull(	DIM_CARRIERBILLINGID	,	1)	,
ifnull(	AMT_SO_AFSNETVALUE	,	0)	,
ifnull(	DIM_DATEIDDLVRDOCCREATED	,	1)	,
ifnull(	CT_POINBOUNDDELIVERYQTY	,	0)	,
ifnull(	DIM_DATEIDPODELIVERY	,	1)	,
ifnull(	CT_DELIVRECEIVEDQTY	,	0)	,
ifnull(	DD_SHIPMENTSTATUS	,	'Not Set')	,
ifnull(	CT_POITEMRECEIVEDQTY	,	0)	,
ifnull(	CT_OPENQTY	,	0)	,
ifnull(	CT_PRESHIPQTY	,	0)	,
ifnull(	CT_INTRANSITQTY	,	0)	,
ifnull(	CT_CLOSEDQTY	,	0)	,
ifnull(	CT_CANCELLEDQTY	,	0)	,
ifnull(	DIM_PURCHASEMISCID	,	1)	,
ifnull(	DD_DELETIONINDICATOR	,	'Not Set')	,
ifnull(	CT_SCHEDULELINEQTY	,	0)	,
ifnull(	DIM_INBROUTEID	,	1)	,
ifnull(	DW_INSERT_DATE	,	CURRENT_DATE	),
ifnull(	DW_UPDATE_DATE	,	CURRENT_DATE	),
ifnull(	DIM_PROJECTSOURCEID	,	1)	,
ifnull(	CT_ITEMQTY	,	0)	,
ifnull(	DIM_AFSTRANSPCONDITIONID	,	1)	,
ifnull(	DIM_DATEIDACTUALPO	,	1)	,
ifnull(	DIM_DATEIDAFSEXFACTEST	,	1)	,
ifnull(	DIM_DATEIDMATAVAILABILITY	,	1)	,
ifnull(	DD_DELIVERYNUMBER	,	'Not Set')	,
ifnull(	DIM_DATEIDTRANSFER	,	1)	,
ifnull(	CT_DELIVERYGROSSWGHT	,	0)	,
ifnull(	CT_DELIVERYNETWGHT	,	0)	,
ifnull(	DIM_DELIVERYROUTEID	,	1)	,
ifnull(	DIM_SHIPPINGCONDITIONID	,	1)	,
ifnull(	DIM_TRANPORTBYSHIPPINGTYPEID	,	1)	,
ifnull(	DD_SHORTTEXT	,	'Not Set')	,
ifnull(	DD_SEALNUMBER	,	'Not Set')	,
ifnull(	DIM_TRANSPORTCONDITIONID	,	1)	,
ifnull(	DIM_DATEIDINBOUNDDELIVERYDATE	,	1)	,
ifnull(	DD_NOOFFOREIGNTRADE	,	'Not Set')	



from  fact_shipmentindelivery_temp d;


/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate;
create table tmp_updatedate as
select max(dw_update_date) dd_updatedate from  fact_shipmentindelivery;


update fact_shipmentindelivery f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_shipmentindelivery f, tmp_updatedate t
where f.dd_updatedate <> t.dd_updatedate;
