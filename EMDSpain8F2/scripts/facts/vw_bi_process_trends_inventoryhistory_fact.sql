/**********************************************************************************/
/*  28 Aug 2013   1.56	      Shanthi    New Field WIP Qty			  */
/*  15 Oct 2013   1.81        Issam 	 Added material movement measures		  */
/*  21  Oct 2013  1.82        Shanthi New Fields for prod orders  */
/*  20 Dec 2013   1.82        Issam 	 Update optimization					  */
/*  9 Nov 2016                Cornelia   Add dd_batch_status_qkz                  */
/*  13 Mar 2017   1.83        Cristi B   Add Weekly, Monthly and Yearly Trend     */           
/**********************************************************************************/


drop table if exists fact_inventoryhistory_delete;
create table fact_inventoryhistory_delete as
select fact_inventoryhistoryid,SnapshotDate from fact_inventoryhistory
where SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end;

MERGE INTO fact_inventoryhistory t
USING 
( select fact_inventoryhistoryid
  from fact_inventoryhistory_delete ) del
ON (t.fact_inventoryhistoryid = del.fact_inventoryhistoryid)
WHEN MATCHED THEN DELETE;		

/* Moved to the end in order to use the snpashot date  -  Roxana D 2017-11-06
drop table if exists fact_inventoryhistory_delete
*/


DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_inventoryhistory';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_inventoryhistory', ifnull(max(fact_inventoryhistoryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM fact_inventoryhistory;

INSERT INTO fact_inventoryhistory
(ct_POOpenQty, /* Issam change 24th Jun */
ct_SOOpenQty, /* Issam change 24th Jun */
amt_BlockedStockAmt,
amt_BlockedStockAmt_GBL,
amt_CommericalPrice1,
amt_CurPlannedPrice,
amt_MovingAvgPrice,
amt_MtlDlvrCost,
amt_OtherCost,
amt_OverheadCost,
amt_PlannedPrice1,
amt_PreviousPrice,
amt_PrevPlannedPrice,
amt_StdUnitPrice,
amt_StdUnitPrice_GBL,
amt_StockInQInspAmt,
amt_StockInQInspAmt_GBL,
amt_StockInTransferAmt,
amt_StockInTransferAmt_GBL,
amt_StockInTransitAmt,
amt_StockInTransitAmt_GBL,
amt_StockValueAmt,
amt_StockValueAmt_GBL,
amt_UnrestrictedConsgnStockAmt,
amt_UnrestrictedConsgnStockAmt_GBL,
amt_WIPBalance,
ct_WIPQty,
ct_BlockedConsgnStock,
ct_BlockedStock,
ct_BlockedStockReturns,
ct_ConsgnStockInQInsp,
ct_LastReceivedQty,
ct_RestrictedConsgnStock,
ct_StockInQInsp,
ct_StockInTransfer,
ct_StockInTransit,
ct_StockQty,
ct_TotalRestrictedStock,
ct_UnrestrictedConsgnStock,
ct_WIPAging,
dd_BatchNo,
dd_DocumentItemNo,
dd_DocumentNo,
dd_MovementType,
dd_ValuationType,
dim_Companyid,
Dim_ConsumptionTypeid,
dim_costcenterid,
dim_Currencyid,
Dim_DateIdLastChangedPrice,
Dim_DateIdPlannedPrice2,
Dim_DocumentStatusid,
Dim_DocumentTypeid,
Dim_IncoTermid,
Dim_ItemCategoryid,
Dim_ItemStatusid,
dim_LastReceivedDateid,
Dim_MovementIndicatorid,
dim_Partid,
dim_Plantid,
Dim_ProfitCenterId,
dim_producthierarchyid,
Dim_PurchaseGroupid,
Dim_PurchaseMiscid,
Dim_PurchaseOrgid,
dim_specialstockid,
dim_stockcategoryid,
dim_StockTypeid,
dim_StorageLocationid,
dim_StorageLocEntryDateid,
Dim_SupplyingPlantId,
Dim_Termid,
Dim_UnitOfMeasureid,
dim_Vendorid,
SnapshotDate,
Dim_DateidSnapshot,
fact_inventoryhistoryid,
amt_ExchangeRate_GBL,
amt_ExchangeRate,
dim_Currencyid_TRA,
dim_Currencyid_GBL,
/* Begin 15 Oct 2013 changes */	
ct_GRQty_Late30,
ct_GRQty_31_60,
ct_GRQty_61_90,
ct_GRQty_91_180,
ct_GIQty_Late30,
ct_GIQty_31_60,
ct_GIQty_61_90,
ct_GIQty_91_180,
/* End 15 Oct 2013 changes */	
dd_prodordernumber,
dd_prodorderitemno,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_partsalesid,
dim_bwproducthierarchyid,
dim_mdg_partid,
dd_invcovflag_emd,
ct_avgfcst_emd,
amt_cogsactualrate_emd,
amt_cogsfixedrate_emd,
amt_cogsfixedplanrate_emd,
amt_cogsplanrate_emd,
amt_cogsprevyearfixedrate_emd,
amt_cogsprevyearrate_emd,
amt_cogsprevyearto1_emd,
amt_cogsprevyearto2_emd,
amt_cogsprevyearto3_emd,
amt_cogsturnoverrate1_emd,
amt_cogsturnoverrate2_emd,
amt_cogsturnoverrate3_emd,
dim_bwhierarchycountryid,
dim_countryhierpsid,
dim_countryhierarid,
dim_clusterid,
ct_countmaterialsafetystock,
dd_batch_status_qkz,
ct_baseuomratioKG,
ct_intransitkg,
ct_dependentdemandkg,
amt_OnHand,
dd_batchstatus,
								  dim_dateidexpirydate,
								  dim_batchstatusid,
								  ct_openOrderQtyUOM,
								  amt_openOrder,
								  ct_Numberofpallets,
                  ct_StockInTransitbaseuom,
                  amt_StockInTransitAmtbaseuom,
                  amt_OnHandbaseuom,
                  amt_restricted
				  )
   SELECT ifnull(ct_POOpenQty, 0),/* Issam change 24th Jun */
          ifnull(ct_SOOpenQty, 0), /* Issam change 24th Jun */
          ifnull(amt_BlockedStockAmt,0) amt_BlockedStockAmt,
          ifnull(amt_BlockedStockAmt_GBL,0) amt_BlockedStockAmt_GBL,
          ifnull(amt_CommericalPrice1,0) amt_CommericalPrice1,
          ifnull(amt_CurPlannedPrice,0) amt_CurPlannedPrice,
          ifnull(amt_MovingAvgPrice,0) amt_MovingAvgPrice,
          ifnull(amt_MtlDlvrCost,0) amt_MtlDlvrCost,
          ifnull(amt_OtherCost,0) amt_OtherCost,
          ifnull(amt_OverheadCost,0) amt_OverheadCost,
          ifnull(amt_PlannedPrice1,0) amt_PlannedPrice1,
          ifnull(amt_PreviousPrice,0) amt_PreviousPrice,
          ifnull(amt_PrevPlannedPrice,0) amt_PrevPlannedPrice,
          ifnull(amt_StdUnitPrice,0) amt_StdUnitPrice,
          ifnull(amt_StdUnitPrice_GBL,0) amt_StdUnitPrice_GBL,
          ifnull(amt_StockInQInspAmt,0) amt_StockInQInspAmt,
          ifnull(amt_StockInQInspAmt_GBL,0) amt_StockInQInspAmt_GBL,
          ifnull(amt_StockInTransferAmt,0) amt_StockInTransferAmt,
          ifnull(amt_StockInTransferAmt_GBL,0) amt_StockInTransferAmt_GBL,
          ifnull(amt_StockInTransitAmt,0) amt_StockInTransitAmt,
          ifnull(amt_StockInTransitAmt_GBL,0) amt_StockInTransitAmt_GBL,
          ifnull(amt_StockValueAmt,0) amt_StockValueAmt,
          ifnull(amt_StockValueAmt_GBL,0) amt_StockValueAmt_GBL,
          ifnull(amt_UnrestrictedConsgnStockAmt,0) amt_UnrestrictedConsgnStockAmt,
          ifnull(amt_UnrestrictedConsgnStockAmt_GBL,0) amt_UnrestrictedConsgnStockAmt_GBL,
          ifnull(amt_WIPBalance,0) amt_WIPBalance,
	      ifnull(ct_WIPQty,0) ct_WIPQty,
          ifnull(ct_BlockedConsgnStock,0) ct_BlockedConsgnStock,
          ifnull(ct_BlockedStock,0) ct_BlockedStock,
          ifnull(ct_BlockedStockReturns,0) ct_BlockedStockReturns,
          ifnull(ct_ConsgnStockInQInsp,0) ct_ConsgnStockInQInsp,
          ifnull(ct_LastReceivedQty,0) ct_LastReceivedQty,
          ifnull(ct_RestrictedConsgnStock,0) ct_RestrictedConsgnStock,
          ifnull(ct_StockInQInsp,0) ct_StockInQInsp,
          ifnull(ct_StockInTransfer,0) ct_StockInTransfer,
          ifnull(ct_StockInTransit,0) ct_StockInTransit,
          ifnull(ct_StockQty,0) ct_StockQty,
          ifnull(ct_TotalRestrictedStock,0) ct_TotalRestrictedStock,
          ifnull(ct_UnrestrictedConsgnStock,0) ct_UnrestrictedConsgnStock,
          ifnull(ct_WIPAging,0) ct_WIPAging,
          ifnull(dd_BatchNo,'Not Set') dd_BatchNo,
          ifnull(dd_DocumentItemNo,0) dd_DocumentItemNo,
          ifnull(dd_DocumentNo,'Not Set') dd_DocumentNo,
          ifnull(dd_MovementType,'Not Set') dd_MovementType,
          ifnull(dd_ValuationType,'Not Set') dd_ValuationType,
          ifnull(iag.dim_Companyid,convert(bigint,1)) dim_Companyid,
          ifnull(Dim_ConsumptionTypeid,convert(bigint,1)) Dim_ConsumptionTypeid,
          ifnull(dim_costcenterid,convert(bigint,1)) dim_costcenterid,
          ifnull(dim_Currencyid,convert(bigint,1)) dim_Currencyid,
          ifnull(Dim_DateIdLastChangedPrice,convert(bigint,1)) Dim_DateIdLastChangedPrice,
          ifnull(Dim_DateIdPlannedPrice2,convert(bigint,1)) Dim_DateIdPlannedPrice2,
          ifnull(Dim_DocumentStatusid,convert(bigint,1)) Dim_DocumentStatusid,
          ifnull(Dim_DocumentTypeid,convert(bigint,1)) Dim_DocumentTypeid,
          ifnull(Dim_IncoTermid,convert(bigint,1)) Dim_IncoTermid,
          ifnull(Dim_ItemCategoryid,convert(bigint,1)) Dim_ItemCategoryid,
          ifnull(Dim_ItemStatusid,convert(bigint,1)) Dim_ItemStatusid,
          ifnull(dim_LastReceivedDateid,convert(bigint,1)) dim_LastReceivedDateid,
          ifnull(Dim_MovementIndicatorid,convert(bigint,1)) Dim_MovementIndicatorid,
          ifnull(dim_Partid,convert(bigint,1)) dim_Partid,
          ifnull(dim_Plantid,convert(bigint,1)) dim_Plantid,
	      ifnull(Dim_ProfitCenterId,convert(bigint,1)) Dim_ProfitCenterId,
          ifnull(dim_producthierarchyid,convert(bigint,1)) dim_producthierarchyid,
          ifnull(Dim_PurchaseGroupid,convert(bigint,1)) Dim_PurchaseGroupid,
          ifnull(Dim_PurchaseMiscid,convert(bigint,1)) Dim_PurchaseMiscid,
          ifnull(Dim_PurchaseOrgid,convert(bigint,1)) Dim_PurchaseOrgid,
          ifnull(dim_specialstockid,convert(bigint,1)) dim_specialstockid,
          ifnull(dim_stockcategoryid,convert(bigint,1)) dim_stockcategoryid,
          ifnull(dim_StockTypeid,convert(bigint,1)) dim_StockTypeid,
          ifnull(dim_StorageLocationid,convert(bigint,1)) dim_StorageLocationid,
          ifnull(dim_StorageLocEntryDateid,convert(bigint,1)) dim_StorageLocEntryDateid,
          ifnull(Dim_SupplyingPlantId,convert(bigint,1)) Dim_SupplyingPlantId,
          ifnull(Dim_Termid,convert(bigint,1)) Dim_Termid,
          ifnull(Dim_UnitOfMeasureid,convert(bigint,1)) Dim_UnitOfMeasureid,
          ifnull(dim_Vendorid,convert(bigint,1)) dim_Vendorid,
          case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end SnapshotDate,
          dt.dim_dateid Dim_DateidSnapshot,
          (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryhistory') + row_number() over (order by ''),
	      amt_ExchangeRate_GBL,
	      amt_ExchangeRate,
	      ifnull(dim_Currencyid_TRA,convert(bigint,1)) dim_Currencyid_TRA,
	      ifnull(dim_Currencyid_GBL,convert(bigint,1)) dim_Currencyid_GBL,
/* Begin 15 Oct 2013 changes */				 
	 ct_GRQty_Late30,
	 ct_GRQty_31_60,
	 ct_GRQty_61_90,
	 ct_GRQty_91_180,
	 ct_GIQty_Late30,
	 ct_GIQty_31_60,
	 ct_GIQty_61_90,
	 ct_GIQty_91_180,
/* End 15 Oct 2013 changes */	  
	  ifnull(dd_prodordernumber,'Not Set'),
	  ifnull(dd_prodorderitemno,0),
	  ifnull(dim_productionorderstatusid,1),
	  ifnull(dim_productionordertypeid,1),
	  ifnull(dim_partsalesid,convert(bigint,1)) dim_partsalesid,
	  ifnull(dim_bwproducthierarchyid,convert(bigint,1)) dim_bwproducthierarchyid,
	  ifnull(dim_mdg_partid,convert(bigint,1)) dim_mdg_partid,
	  ifnull(dd_invcovflag_emd,'Not Set') dd_invcovflag_emd,
	  ifnull(ct_avgfcst_emd,0) ct_avgfcst_emd,
	  ifnull(amt_cogsactualrate_emd,0) amt_cogsactualrate_emd,
	  ifnull(amt_cogsfixedrate_emd,0) amt_cogsfixedrate_emd,
	  ifnull(amt_cogsfixedplanrate_emd,0) amt_cogsfixedplanrate_emd,
	  ifnull(amt_cogsplanrate_emd,0) amt_cogsplanrate_emd,
	  ifnull(amt_cogsprevyearfixedrate_emd,0) amt_cogsprevyearfixedrate_emd,
	  ifnull(amt_cogsprevyearrate_emd,0) amt_cogsprevyearrate_emd,
	  ifnull(amt_cogsprevyearto1_emd,0) amt_cogsprevyearto1_emd,
	  ifnull(amt_cogsprevyearto2_emd,0) amt_cogsprevyearto2_emd,
	  ifnull(amt_cogsprevyearto3_emd,0) amt_cogsprevyearto3_emd,
	  ifnull(amt_cogsturnoverrate1_emd,0) amt_cogsturnoverrate1_emd,
	  ifnull(amt_cogsturnoverrate2_emd,0) amt_cogsturnoverrate2_emd,
	  ifnull(amt_cogsturnoverrate3_emd,0) amt_cogsturnoverrate3_emd,
	  ifnull(dim_bwhierarchycountryid,1) dim_bwhierarchycountryid,
	  ifnull(dim_countryhierpsid,1) dim_countryhierpsid,
	  ifnull(dim_countryhierarid,1) dim_countryhierarid,
	  ifnull(dim_clusterid,1) dim_clusterid,
	  ifnull(ct_countmaterialsafetystock,0) ct_countmaterialsafetystock,
	  ifnull(dd_batch_status_qkz,'Not Set') dd_batch_status_qkz,
	  ifnull(ct_baseuomratioKG,0) ct_baseuomratioKG,
	  ifnull(ct_intransitkg,0) ct_intransitkg,
	  ifnull(ct_dependentdemandkg,0) ct_dependentdemandkg,
	  ifnull(amt_OnHand,0) amt_OnHand,
	  ifnull(dd_batchstatus,'Not Set') dd_batchstatus,
	  dim_dateidexpirydate,
	  ifnull(dim_batchstatusid,convert(bigint,1)) dim_batchstatusid
	  ,ifnull(ct_openOrderQtyUOM,0) ct_openOrderQtyUOM
      ,ifnull(amt_openOrder,0) amt_openOrder
	  ,ifnull(ct_Numberofpallets,0) ct_Numberofpallets
    ,ifnull(ct_StockInTransitbaseuom,0) ct_StockInTransitbaseuom
    ,ifnull(amt_StockInTransitAmtbaseuom,0) amt_StockInTransitAmtbaseuom
    ,ifnull(amt_OnHandbaseuom,0) amt_OnHandbaseuom
    ,ifnull(amt_restricted,0) amt_restricted
     FROM    facT_inventoryaging iag
          INNER JOIN
             dim_Company dc
          ON iag.dim_companyid = dc.dim_Companyid
		  INNER JOIN dim_date dt ON dt.companycode = dc.companycode and dt.DateValue = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end;
/*
UPDATE fact_inventoryhistory t0
SET t0.Dim_DateidSnapshot = dt.dim_dateid
FROM 
fact_inventoryhistory t0,
dim_Company dc,
dim_date dt
WHERE       t0.dim_companyid = dc.dim_Companyid
	AND dt.DateValue = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
	AND dt.companycode = dc.companycode
	AND t0.Dim_DateidSnapshot = 1*/

UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_inventoryhistoryid), 0) FROM fact_inventoryhistory)
 WHERE table_name = 'fact_inventoryhistory';

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'processinglog', ifnull(max(processinglogid),  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) FROM processinglog;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          current_date,
          'Start of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

delete from fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_StorageLocationid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_StorageLocationid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid IN (2, 3)
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end,
                  (case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - 1),
                  (case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - 7),
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_StorageLocationid,
            dim_stockcategoryid,
            SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
and ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
and ih1.SnapshotDate - (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND ih1.SnapshotDate - ( INTERVAL '7' DAY) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,	   
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,	   
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,	   
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,	   
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM 
fact_inventoryhistory ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid in (2,3)
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end;

delete from fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_Vendorid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_Vendorid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 4
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end,
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '1' DAY),
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '7' DAY),
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_Vendorid,
            dim_stockcategoryid,
            SnapshotDate;

UPDATE    
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1DayChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE    
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
        ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;


truncate table fact_inventoryhistory_tmp;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,          
fact_inventoryhistory_tmp ih2 
WHERE  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
where  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
and ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,	   
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,	   
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,	   
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,	   
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM
fact_inventoryhistory ih1,          
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid = 4
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end;

truncate table fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)		  
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 5
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end,
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '1' DAY),
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '7' DAY),
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_stockcategoryid,
            SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1DayChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
        ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;


 UPDATE    
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,		  
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM 
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,	   
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,	   
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,	   
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,	   
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM 
fact_inventoryhistory ih1,
fact_inventoryhistory_tmp ih2 
WHERE ih1.dim_stockcategoryid = 5
AND ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          current_date,
          'End of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

truncate table fact_inventoryhistory_tmp;

/* Begin 15 Oct 2013 changes */
/* Begin 20 Dec 2013 changes */		
UPDATE fact_inventoryhistory		 
SET ct_GRQty_Late30 = 0
WHERE ct_GRQty_Late30 IS NULL;

UPDATE fact_inventoryhistory		 
SET ct_GRQty_31_60 = 0
WHERE ct_GRQty_31_60 IS NULL;

UPDATE fact_inventoryhistory		 
SET ct_GRQty_61_90 = 0
WHERE ct_GRQty_61_90 IS NULL;

UPDATE fact_inventoryhistory		 
SET ct_GRQty_91_180 = 0
WHERE ct_GRQty_91_180 IS NULL;

UPDATE fact_inventoryhistory		 
SET ct_GIQty_Late30 = 0
WHERE ct_GIQty_Late30 IS NULL;

UPDATE fact_inventoryhistory		 
SET ct_GIQty_31_60 = 0
WHERE ct_GIQty_31_60 IS NULL;

UPDATE fact_inventoryhistory		 
SET ct_GIQty_61_90 = 0
WHERE ct_GIQty_61_90 IS NULL;

UPDATE fact_inventoryhistory		 
SET ct_GIQty_91_180 = 0
WHERE ct_GIQty_91_180 IS NULL;

UPDATE fact_inventoryhistory f
SET f.dim_dateidlastprocessed = (select dim_dateid from dim_date d where d.datevalue = current_date and d.companycode = 'Not Set');

/* Marius 1 sep 2016 add Daily variance */

DROP TABLE IF EXISTS TMP_LATEST_DATE;
CREATE TABLE TMP_LATEST_DATE
AS
SELECT MAX(snp.datevalue) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT MAX(snp.datevalue)-1 max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT MAX(snp.datevalue)-7 max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-1) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_1DAY_CHANGE;
CREATE TABLE TMP_1DAY_CHANGE
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(CAST(CASE WHEN (amt_cogsfixedplanrate_emd/case when amt_exchangerate_gbl = 0 then 1 else amt_exchangerate_gbl end) = 0 THEN ((f_ih.amt_StockValueAmt + f_ih.amt_StockInQInspAmt + f_ih.amt_BlockedStockAmt 
    + f_ih.amt_StockInTransitAmt + f_ih.amt_StockInTransferAmt + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice))) ELSE ((f_ih.ct_StockQty 
    + f_ih.ct_StockInQInsp + f_ih.ct_BlockedStock + f_ih.ct_StockInTransit + f_ih.ct_StockInTransfer + f_ih.ct_TotalRestrictedStock)) 
    * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END AS DECIMAL (18,4))) COGS_ON_HAND
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max_dt FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;

DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT Dim_DateidSnapshot,dim_plantid,dim_partid,max(fact_inventoryhistoryid) ID 
FROM fact_inventoryhistory f_ih
	INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY Dim_DateidSnapshot,dim_plantid,dim_partid;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = snp.datevalue -1 AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1daychange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1daychange = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = snp.datevalue - 7 AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1weekchange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1weekchange = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-1) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1monthchange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x 
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1monthchange = x.v_cogs;
/* Begin 13 March Cristi B Add weekly, monthly and yearly trend */

/* weekly */

DROP TABLE IF EXISTS TMP_CHANGE_WEEKLY;
CREATE TABLE TMP_CHANGE_WEEKLY
AS
SELECT
  snp.CALENDARWEEKYR,
  dim_plantid,
  dim_partid,
  SUM(f_ih.amt_StockValueAmt
     + f_ih.amt_StockInQInspAmt
     + f_ih.amt_BlockedStockAmt
     + f_ih.amt_StockInTransitAmt
     + f_ih.amt_StockInTransferAmt
     + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice)
     ) ON_HAND_AMT
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.WEEKDAYABBREVIATION = 'Sat'
AND
(
snp.CALENDARWEEKYR
= ( CASE
        WHEN WEEK(CURRENT_DATE) = 0
        THEN CONCAT(EXTRACT(YEAR FROM ADD_YEARS(CURRENT_DATE,-1)),53)
        ELSE TO_CHAR(ADD_WEEKS(CURRENT_DATE,-1),'YYYYuW')
     END )
 OR
 snp.CALENDARWEEKYR = TO_CHAR(CURRENT_DATE,'YYYYuW')
 )
GROUP BY snp.CALENDARWEEKYR,dim_plantid,dim_partid
ORDER BY TO_NUMBER(snp.CALENDARWEEKYR) DESC;

DROP TABLE IF EXISTS TMP_CHANGE_WEEKLY_R;
CREATE TABLE TMP_CHANGE_WEEKLY_R AS
SELECT
      DENSE_RANK() OVER( ORDER BY TO_NUMBER(CALENDARWEEKYR)) rn
     ,t.*
  FROM TMP_CHANGE_WEEKLY t;


UPDATE fact_inventoryhistory f
SET f.amt_onhand_weekly_diff = t2.on_hand_amt - t1.on_hand_amt, f.dw_update_date = current_date
    FROM TMP_CHANGE_WEEKLY_R t1
        ,TMP_CHANGE_WEEKLY_R t2
        ,fact_inventoryhistory f
        ,dim_date snpd
   WHERE t1.rn = t2.rn - 1
     AND t1.dim_partid = t2.dim_partid
     AND t1.dim_plantid = t2.dim_plantid
     AND t2.dim_partid = f.dim_partid
     AND t2.dim_plantid = f.dim_plantid
     AND f.dim_dateidsnapshot = snpd.dim_dateid
     AND t2.CALENDARWEEKYR = snpd.CALENDARWEEKYR
	 AND f.amt_onhand_weekly_diff <> t2.on_hand_amt - t1.on_hand_amt;

	 
	 
/* monthly */ 

DROP TABLE IF EXISTS TMP_CHANGE_MONTHLY;
CREATE TABLE TMP_CHANGE_MONTHLY
AS
SELECT
  snp.CALENDARMONTHID,
  dim_plantid,
  dim_partid,
  SUM(f_ih.amt_StockValueAmt
     + f_ih.amt_StockInQInspAmt
     + f_ih.amt_BlockedStockAmt
     + f_ih.amt_StockInTransitAmt
     + f_ih.amt_StockInTransferAmt
     + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice)
     ) ON_HAND_AMT
    ,snp.datevalue
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue = snp.MONTHENDDATE AND CALENDARMONTHID IN (TO_CHAR(CURRENT_DATE,'YYYYMM'), TO_CHAR(ADD_MONTHS(CURRENT_DATE,-1),'YYYYMM'))
GROUP BY snp.CALENDARMONTHID,dim_plantid,dim_partid,snp.datevalue
ORDER BY TO_NUMBER(snp.CALENDARMONTHID);

DROP TABLE IF EXISTS TMP_CHANGE_MONTHLY_R;
CREATE TABLE TMP_CHANGE_MONTHLY_R AS
SELECT
      DENSE_RANK() OVER( ORDER BY TO_NUMBER(CALENDARMONTHID)) rn
     ,t.*
  FROM TMP_CHANGE_MONTHLY t;


UPDATE fact_inventoryhistory f
SET f.amt_onhand_monthly_diff = t2.on_hand_amt - t1.on_hand_amt, f.dw_update_date = current_date
    FROM TMP_CHANGE_MONTHLY_R t1
        ,TMP_CHANGE_MONTHLY_R t2
        ,fact_inventoryhistory f
        ,dim_date snpd
   WHERE t1.rn = t2.rn - 1
     AND t1.dim_partid = t2.dim_partid
     AND t1.dim_plantid = t2.dim_plantid
     AND t2.dim_partid = f.dim_partid
     AND t2.dim_plantid = f.dim_plantid
     AND f.dim_dateidsnapshot = snpd.dim_dateid
     AND t2.CALENDARMONTHID = snpd.CALENDARMONTHID
	 AND f.amt_onhand_monthly_diff <> t2.on_hand_amt - t1.on_hand_amt;


/* yearly */


DROP TABLE IF EXISTS TMP_CHANGE_YEARLY;
CREATE TABLE TMP_CHANGE_YEARLY
AS
SELECT
  snp.CALENDARYEAR,
  dim_plantid,
  dim_partid,
  SUM(f_ih.amt_StockValueAmt
     + f_ih.amt_StockInQInspAmt
     + f_ih.amt_BlockedStockAmt
     + f_ih.amt_StockInTransitAmt
     + f_ih.amt_StockInTransferAmt
     + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice)
     ) ON_HAND_AMT
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE TO_CHAR(snp.datevalue,'MMDD')  = '1231' AND TO_CHAR(snp.datevalue,'YYYY') IN ( TO_CHAR(ADD_YEARS(CURRENT_DATE,-1),'YYYY'),TO_CHAR(CURRENT_DATE-1,'YYYY'))
GROUP BY snp.CALENDARYEAR,dim_plantid,dim_partid
ORDER BY TO_NUMBER(snp.CALENDARYEAR);

DROP TABLE IF EXISTS TMP_CHANGE_YEARLY_R;
CREATE TABLE TMP_CHANGE_YEARLY_R AS
SELECT
      DENSE_RANK() OVER( ORDER BY TO_NUMBER(CALENDARYEAR)) rn
     ,t.*
  FROM TMP_CHANGE_YEARLY t;


UPDATE fact_inventoryhistory f
SET f.amt_onhand_yearly_diff = t2.on_hand_amt - t1.on_hand_amt, f.dw_update_date = current_date
    FROM TMP_CHANGE_YEARLY_R t1
        ,TMP_CHANGE_YEARLY_R t2
        ,fact_inventoryhistory f
        ,dim_date snpd
   WHERE t1.rn = t2.rn - 1
     AND t1.dim_partid = t2.dim_partid
     AND t1.dim_plantid = t2.dim_plantid
     AND t2.dim_partid = f.dim_partid
     AND t2.dim_plantid = f.dim_plantid
     AND f.dim_dateidsnapshot = snpd.dim_dateid
     AND t2.CALENDARYEAR = snpd.CALENDARYEAR
	 AND f.amt_onhand_yearly_diff <> t2.on_hand_amt - t1.on_hand_amt;

/* End 13 March Cristi B Add weekly, monthly and yearly trend */
/* APP-6460 - Oana 24 July 2017  */
DROP TABLE IF EXISTS tmp_lastdayofmth;
CREATE TABLE tmp_lastdayofmth AS
SELECT
  snp.datevalue snapshotdate,
  f_ih.dim_plantid,
  f_ih.dim_partid,
	f_ih.dim_bwproducthierarchyid,
  SUM(CAST ((CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0 THEN (amt_OnHand) ELSE ((f_ih.ct_StockQty + f_ih.ct_StockInQInsp
    + f_ih.ct_BlockedStock + f_ih.ct_StockInTransfer + f_ih.ct_StockInTransit+ f_ih.ct_TotalRestrictedStock))
    * f_ih.ct_baseuomratioPCcustom * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END) * (CASE WHEN mdg.PPU = 0 THEN 1 ELSE mdg.PPU END)
    * (CASE WHEN mdg.ACT_CONV = 0 THEN 1 ELSE mdg.ACT_CONV END) * amt_exchangerate_gbl  AS DECIMAL (18,4))) COGS_ON_HAND,
  SUM (CAST ((f_ih.amt_StockValueAmt + f_ih.amt_StockInQInspAmt + f_ih.amt_BlockedStockAmt + f_ih.amt_StockInTransitAmt + f_ih.amt_StockInTransferAmt + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice))* amt_exchangerate_gbl AS DECIMAL (18,4))) ON_HAND_AMT,
  SUM(CAST((f_ih.ct_StockQty + f_ih.ct_StockInQInsp + f_ih.ct_BlockedStock + f_ih.ct_StockInTransit +f_ih.ct_StockInTransfer + f_ih.ct_TotalRestrictedStock) as decimal (18,4)) ) ON_HAND_QTY
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
	inner join dim_bwproducthierarchy bw on f_ih.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
	inner join dim_mdg_part mdg on f_ih.dim_mdg_partid = mdg.dim_mdg_partid
WHERE  snp.datevalue = snp.monthenddate
  and businesssector = 'BS-01'
GROUP BY snp.datevalue, f_ih.dim_plantid, f_ih.dim_partid, f_ih.dim_bwproducthierarchyid;

drop table if exists tmp_dummydate;
create table tmp_dummydate as
select dt.dim_dateid, dt.datevalue
from dim_date dt
where dt.companycode = 'Not Set'
	and dt.datevalue = dt.monthenddate
	and  dt.datevalue >= '2016-03-31'
	and datevalue < current_date;

drop table if exists tmp_dummyrows;
create table tmp_dummyrows as
select distinct
cast(0 as DECIMAL(18,5)) as CT_STOCKQTY,
cast(0 as DECIMAL(18,5)) as AMT_STOCKVALUEAMT,
cast(0 as DECIMAL(18,5)) as CT_LASTRECEIVEDQTY,
'Not Set' as DD_BATCHNO,
'Not Set' as DD_VALUATIONTYPE,
'Not Set' as DD_DOCUMENTNO,
cast(0 as DECIMAL(18,0)) as DD_DOCUMENTITEMNO,
'Not Set' as DD_MOVEMENTTYPE,
1 as DIM_STORAGELOCENTRYDATEID,
1 as DIM_LASTRECEIVEDDATEID,
DIM_PARTID,
DIM_PLANTID,
1 as DIM_STORAGELOCATIONID,
1 as DIM_COMPANYID,
1 as DIM_VENDORID,
1 as DIM_CURRENCYID,
1 as DIM_STOCKTYPEID,
1 as DIM_SPECIALSTOCKID,
1 as DIM_PURCHASEORGID,
1 as DIM_PURCHASEGROUPID,
1 as DIM_PRODUCTHIERARCHYID,
1 as DIM_UNITOFMEASUREID,
1 as DIM_MOVEMENTINDICATORID,
1 as DIM_CONSUMPTIONTYPEID,
1 as DIM_COSTCENTERID,
1 as DIM_DOCUMENTSTATUSID,
1 as DIM_DOCUMENTTYPEID,
1 as DIM_INCOTERMID,
1 as DIM_ITEMCATEGORYID,
1 as DIM_ITEMSTATUSID,
1 as DIM_TERMID,
1 as DIM_PURCHASEMISCID,
cast(0 as DECIMAL(18,5)) as AMT_STOCKVALUEAMT_GBL,
cast(0 as DECIMAL(18,5)) as CT_TOTALRESTRICTEDSTOCK,
cast(0 as DECIMAL(18,5)) as CT_STOCKINQINSP,
cast(0 as DECIMAL(18,5)) as CT_BLOCKEDSTOCK,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSFER,
cast(0 as DECIMAL(18,5)) as CT_UNRESTRICTEDCONSGNSTOCK,
cast(0 as DECIMAL(18,5)) as CT_RESTRICTEDCONSGNSTOCK,
cast(0 as DECIMAL(18,5)) as CT_BLOCKEDCONSGNSTOCK,
cast(0 as DECIMAL(18,5)) as CT_CONSGNSTOCKINQINSP,
cast(0 as DECIMAL(18,5)) as CT_BLOCKEDSTOCKRETURNS,
cast(0 as DECIMAL(18,5)) as AMT_BLOCKEDSTOCKAMT,
cast(0 as DECIMAL(18,5)) as AMT_BLOCKEDSTOCKAMT_GBL,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINQINSPAMT,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINQINSPAMT_GBL,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINTRANSFERAMT,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINTRANSFERAMT_GBL,
cast(0 as DECIMAL(18,5)) as AMT_UNRESTRICTEDCONSGNSTOCKAMT,
cast(0 as DECIMAL(18,5)) as AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,
cast(0 as DECIMAL(18,5)) as AMT_STDUNITPRICE,
cast(0 as DECIMAL(18,5)) as AMT_STDUNITPRICE_GBL,
1 as DIM_STOCKCATEGORYID,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSIT,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINTRANSITAMT,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINTRANSITAMT_GBL,
1 as DIM_SUPPLYINGPLANTID,
cast(0 as DECIMAL(18,5)) as AMT_MTLDLVRCOST,
cast(0 as DECIMAL(18,5)) as AMT_OVERHEADCOST,
cast(0 as DECIMAL(18,5)) as AMT_OTHERCOST,
cast(0 as DECIMAL(18,5)) as AMT_WIPBALANCE,
cast(0 as DECIMAL(18,0)) as CT_WIPAGING,
cast(0 as DECIMAL(18,4)) as AMT_MOVINGAVGPRICE,
cast(0 as DECIMAL(18,4)) as AMT_PREVIOUSPRICE,
cast(0 as DECIMAL(18,4)) as AMT_COMMERICALPRICE1,
cast(0 as DECIMAL(18,4)) as AMT_PLANNEDPRICE1,
cast(0 as DECIMAL(18,4)) as AMT_PREVPLANNEDPRICE,
cast(0 as DECIMAL(18,4)) as AMT_CURPLANNEDPRICE,
1 as DIM_DATEIDPLANNEDPRICE2,
1 as DIM_DATEIDLASTCHANGEDPRICE,
dim_dateid as DIM_DATEIDSNAPSHOT,
datevalue as SNAPSHOTDATE,
cast(0 as DECIMAL(18,5)) as CT_STOCKQTY_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKQTY_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKQTY_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKQTY_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSIT_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSIT_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSIT_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSIT_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSFER_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSFER_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSFER_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINTRANSFER_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINQINSP_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINQINSP_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINQINSP_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as CT_STOCKINQINSP_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as CT_TOTALRESTRICTEDSTOCK_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as CT_TOTALRESTRICTEDSTOCK_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as CT_TOTALRESTRICTEDSTOCK_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as CT_TOTALRESTRICTEDSTOCK_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKVALUEAMT_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKVALUEAMT_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKVALUEAMT_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKVALUEAMT_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINTRANSFERAMT_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINTRANSFERAMT_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINTRANSFERAMT_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINTRANSFERAMT_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINQINSPAMT_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINQINSPAMT_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINQINSPAMT_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_STOCKINQINSPAMT_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_BLOCKEDSTOCKAMT_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_BLOCKEDSTOCKAMT_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_BLOCKEDSTOCKAMT_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as AMT_BLOCKEDSTOCKAMT_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as CT_BLOCKEDSTOCK_1DAYCHANGE,
cast(0 as DECIMAL(18,5)) as CT_BLOCKEDSTOCK_1WEEKCHANGE,
cast(0 as DECIMAL(18,5)) as CT_BLOCKEDSTOCK_1QUARTERCHANGE,
cast(0 as DECIMAL(18,5)) as CT_BLOCKEDSTOCK_1MONTHCHANGE,
cast(0 as DECIMAL(18,5)) as CT_POOPENQTY,
cast(0 as DECIMAL(18,5)) as CT_SOOPENQTY,
cast(1 as DECIMAL(18,6)) as AMT_EXCHANGERATE_GBL,
cast(1 as DECIMAL(18,6)) as AMT_EXCHANGERATE,
1 as DIM_PROFITCENTERID,
cast(0 as DECIMAL(18,4)) as CT_WIPQTY,
1 as DIM_CURRENCYID_TRA,
1 as DIM_CURRENCYID_GBL,
cast(0 as DECIMAL(18,4)) as CT_GRQTY_LATE30,
cast(0 as DECIMAL(18,4)) as CT_GRQTY_31_60,
cast(0 as DECIMAL(18,4)) as CT_GRQTY_61_90,
cast(0 as DECIMAL(18,4)) as CT_GRQTY_91_180,
cast(0 as DECIMAL(18,4)) as CT_GIQTY_LATE30,
cast(0 as DECIMAL(18,4)) as CT_GIQTY_31_60,
cast(0 as DECIMAL(18,4)) as CT_GIQTY_61_90,
cast(0 as DECIMAL(18,4)) as CT_GIQTY_91_180,
'Not Set' as DD_PRODORDERNUMBER,
0 as DD_PRODORDERITEMNO,
1 as DIM_PRODUCTIONORDERSTATUSID,
1 as DIM_PRODUCTIONORDERTYPEID,
1 as DIM_PARTSALESID,
cast(0 as DECIMAL(18,4)) as AMT_STOCKINTRANSITAMT_1WEEKCHANGE,
cast(0 as DECIMAL(18,4)) as AMT_STOCKINTRANSITAMT_1MONTHCHANGE,
cast(0 as DECIMAL(18,4)) as AMT_STOCKINTRANSITAMT_1QUARTERCHANGE,
cast(0 as DECIMAL(18,4)) as AMT_STOCKINTRANSITAMT_1DAYCHANGE,
current_timestamp as DW_INSERT_DATE,
current_timestamp as DW_UPDATE_DATE,
1 as DIM_PROJECTSOURCEID,
ifnull(t.DIM_BWPRODUCTHIERARCHYID,1) as DIM_BWPRODUCTHIERARCHYID,
1 as DIM_MDG_PARTID,
'Not Set' as DD_INVCOVFLAG_EMD,
cast(0 as DECIMAL(18,4)) as CT_AVGFCST_EMD,
1 as DIM_BWPARTID,
cast(0 as DECIMAL(18,5)) as AMT_COGSACTUALRATE_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSFIXEDRATE_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSFIXEDPLANRATE_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSPLANRATE_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSPREVYEARFIXEDRATE_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSPREVYEARRATE_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSPREVYEARTO1_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSPREVYEARTO2_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSPREVYEARTO3_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSTURNOVERRATE1_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSTURNOVERRATE2_EMD,
cast(0 as DECIMAL(18,5)) as AMT_COGSTURNOVERRATE3_EMD,
1 as DIM_DATEIDLASTPROCESSED,
cast(0 as DECIMAL(18,2)) as AMT_ONHAND_STOCK_HIST,
cast(0 as DECIMAL(18,3)) as CT_ONHAND_STOCK_HIST,
1 as DIM_BWHIERARCHYCOUNTRYID,
1 as DIM_CLUSTERID,
1 as DIM_COMMERCIALVIEWID,
1 as DIM_COUNTRYHIERARID,
1 as DIM_COUNTRYHIERPSID,
cast(0 as DECIMAL(18,0)) as CT_COUNTMATERIALSAFETYSTOCK,
cast(0 as DECIMAL(18,4)) as AMT_COGS_1DAYCHANGE,
cast(0 as DECIMAL(18,4)) as AMT_COGS_1WEEKCHANGE,
cast(0 as DECIMAL(18,4)) as AMT_COGS_1MONTHCHANGE,
cast(0 as DECIMAL(18,0)) as DD_ISCONSIGNED,
1 as DIM_GSAMAPIMPORTID,
1 as DIM_CUSTOMERIDSHIPTO,
1 as DIM_CUSTOMERID,
cast(1 as DECIMAL(18,4)) as CT_BASEUOMRATIOPCCUSTOM,
'Not Set' as DD_BATCH_STATUS_QKZ,
1 as DIM_PRODUCTIONSCHEDULERID,
cast(0 as DECIMAL(36,0)) as CT_COGSONHAND,
cast(0 as DECIMAL(36,0)) as CT_COGSONHAND_1M,
cast(0 as DECIMAL(36,0)) as CT_COGSONHAND_3M,
cast(0 as DECIMAL(36,0)) as CT_COGSONHAND_12M,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_1MDELTA,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_3MDELTA,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_12MDELTA,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_1M,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_3M,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_12M,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_1MCEF,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_3MCEF,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_12MCEF,
cast(0 as DECIMAL(18,0)) as CT_GRQTY,
cast(0 as DECIMAL(18,0)) as CT_ORDERITEMQTY,
cast(1 as DECIMAL(18,4)) as CT_BASEUOMRATIOKG,
cast(0 as DECIMAL(18,4)) as CT_INTRANSITKG,
cast(0 as DECIMAL(18,4)) as CT_DEPENDENTDEMANDKG,
cast(0 as DECIMAL(18,4)) as AMT_ONHAND,
cast(0 as DECIMAL(18,4)) as AMT_ONHAND_WEEKLY_DIFF,
cast(0 as DECIMAL(18,4)) as AMT_ONHAND_MONTHLY_DIFF,
cast(0 as DECIMAL(18,4)) as AMT_ONHAND_YEARLY_DIFF,
'Not Set' as DD_BATCHSTATUS,
1 as DIM_DATEIDEXPIRYDATE,
1 as DIM_BATCHSTATUSID,
cast(0 as DECIMAL(18,5)) as AMT_COGSFIXEDRATE_EMD_NEW,
cast(0 as DECIMAL(18,5)) as AMT_COGSPLANRATE_EMD_NEW,
cast(0 as DECIMAL(18,5)) as AMT_COGSFIXEDPLANRATE_EMD_NEW,
cast(1 as DECIMAL(18,6)) as AMT_EXCHANGERATE_GBL_NEW,
cast(0 as DECIMAL(18,4)) as AMT_ONHAND_NEW,
cast(0 as DECIMAL(18,4)) as AMT_STDUNITPRICE_NEW,
cast(0 as DECIMAL(18,4)) as AMT_STDUNITPRICE_VALIDATING,
cast(0 as DECIMAL(18,4)) as CT_OPENORDERQTYUOM,
cast(0 as DECIMAL(18,4)) as AMT_OPENORDER,
cast(0 as DECIMAL(18,0)) as CT_NUMBEROFPALLETS,
cast(0 as DECIMAL(18,5)) as AMT_COGSFIXEDPLANRATE_EMD_BIS,
cast(0 as DECIMAL(18,5)) as AMT_STDUNITPRICE_BIS,
cast(1 as DECIMAL(18,6)) as AMT_EXCHANGERATE_GBL_BIS,
cast(0 as DECIMAL(36,6)) as AMT_COGSONHAND_1MTHDELTA,
1 as DD_DUMMYROW
from tmp_lastdayofmth t
	full outer join  tmp_dummydate td on year(t.snapshotdate) = year(td.datevalue);

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_inventoryhistory';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_inventoryhistory', ifnull(max(fact_inventoryhistoryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM fact_inventoryhistory;

drop table if exists tmp_insertdummyrows;
create table tmp_insertdummyrows as
select
(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryhistory') + row_number() over (order by '') as fact_inventoryhistoryid,
 t.*
from fact_inventoryhistory f_ih
	right join tmp_dummyrows t
			on f_ih.snapshotdate = t.snapshotdate
  				and f_ih.dim_plantid = t.dim_plantid
  				and f_ih.dim_partid = t.dim_partid
				and f_ih.dim_bwproducthierarchyid = t.dim_bwproducthierarchyid
where f_ih.snapshotdate  is null
	and f_ih.dim_plantid is null
	and f_ih.dim_partid is null
	and f_ih.dim_bwproducthierarchyid is null;

insert into fact_inventoryhistory (FACT_INVENTORYHISTORYID,
	CT_STOCKQTY,
	AMT_STOCKVALUEAMT,
	CT_LASTRECEIVEDQTY,
	DD_BATCHNO,
	DD_VALUATIONTYPE,
	DD_DOCUMENTNO,
	DD_DOCUMENTITEMNO,
	DD_MOVEMENTTYPE,
	DIM_STORAGELOCENTRYDATEID,
	DIM_LASTRECEIVEDDATEID,
	DIM_PARTID,
	DIM_PLANTID,
	DIM_STORAGELOCATIONID,
	DIM_COMPANYID,
	DIM_VENDORID,
	DIM_CURRENCYID,
	DIM_STOCKTYPEID,
	DIM_SPECIALSTOCKID,
	DIM_PURCHASEORGID,
	DIM_PURCHASEGROUPID,
	DIM_PRODUCTHIERARCHYID,
	DIM_UNITOFMEASUREID,
	DIM_MOVEMENTINDICATORID,
	DIM_CONSUMPTIONTYPEID,
	DIM_COSTCENTERID,
	DIM_DOCUMENTSTATUSID,
	DIM_DOCUMENTTYPEID,
	DIM_INCOTERMID,
	DIM_ITEMCATEGORYID,
	DIM_ITEMSTATUSID,
	DIM_TERMID,
	DIM_PURCHASEMISCID,
	AMT_STOCKVALUEAMT_GBL,
	CT_TOTALRESTRICTEDSTOCK,
	CT_STOCKINQINSP,
	CT_BLOCKEDSTOCK,
	CT_STOCKINTRANSFER,
	CT_UNRESTRICTEDCONSGNSTOCK,
	CT_RESTRICTEDCONSGNSTOCK,
	CT_BLOCKEDCONSGNSTOCK,
	CT_CONSGNSTOCKINQINSP,
	CT_BLOCKEDSTOCKRETURNS,
	AMT_BLOCKEDSTOCKAMT,
	AMT_BLOCKEDSTOCKAMT_GBL,
	AMT_STOCKINQINSPAMT,
	AMT_STOCKINQINSPAMT_GBL,
	AMT_STOCKINTRANSFERAMT,
	AMT_STOCKINTRANSFERAMT_GBL,
	AMT_UNRESTRICTEDCONSGNSTOCKAMT,
	AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,
	AMT_STDUNITPRICE,
	AMT_STDUNITPRICE_GBL,
	DIM_STOCKCATEGORYID,
	CT_STOCKINTRANSIT,
	AMT_STOCKINTRANSITAMT,
	AMT_STOCKINTRANSITAMT_GBL,
	DIM_SUPPLYINGPLANTID,
	AMT_MTLDLVRCOST,
	AMT_OVERHEADCOST,
	AMT_OTHERCOST,
	AMT_WIPBALANCE,
	CT_WIPAGING,
	AMT_MOVINGAVGPRICE,
	AMT_PREVIOUSPRICE,
	AMT_COMMERICALPRICE1,
	AMT_PLANNEDPRICE1,
	AMT_PREVPLANNEDPRICE,
	AMT_CURPLANNEDPRICE,
	DIM_DATEIDPLANNEDPRICE2,
	DIM_DATEIDLASTCHANGEDPRICE,
	DIM_DATEIDSNAPSHOT,
	SNAPSHOTDATE,
	CT_STOCKQTY_1DAYCHANGE,
	CT_STOCKQTY_1WEEKCHANGE,
	CT_STOCKQTY_1MONTHCHANGE,
	CT_STOCKQTY_1QUARTERCHANGE,
	CT_STOCKINTRANSIT_1DAYCHANGE,
	CT_STOCKINTRANSIT_1WEEKCHANGE,
	CT_STOCKINTRANSIT_1MONTHCHANGE,
	CT_STOCKINTRANSIT_1QUARTERCHANGE,
	CT_STOCKINTRANSFER_1DAYCHANGE,
	CT_STOCKINTRANSFER_1WEEKCHANGE,
	CT_STOCKINTRANSFER_1MONTHCHANGE,
	CT_STOCKINTRANSFER_1QUARTERCHANGE,
	CT_STOCKINQINSP_1DAYCHANGE,
	CT_STOCKINQINSP_1WEEKCHANGE,
	CT_STOCKINQINSP_1MONTHCHANGE,
	CT_STOCKINQINSP_1QUARTERCHANGE,
	CT_TOTALRESTRICTEDSTOCK_1DAYCHANGE,
	CT_TOTALRESTRICTEDSTOCK_1WEEKCHANGE,
	CT_TOTALRESTRICTEDSTOCK_1MONTHCHANGE,
	CT_TOTALRESTRICTEDSTOCK_1QUARTERCHANGE,
	AMT_STOCKVALUEAMT_1DAYCHANGE,
	AMT_STOCKVALUEAMT_1WEEKCHANGE,
	AMT_STOCKVALUEAMT_1MONTHCHANGE,
	AMT_STOCKVALUEAMT_1QUARTERCHANGE,
	AMT_STOCKINTRANSFERAMT_1DAYCHANGE,
	AMT_STOCKINTRANSFERAMT_1WEEKCHANGE,
	AMT_STOCKINTRANSFERAMT_1MONTHCHANGE,
	AMT_STOCKINTRANSFERAMT_1QUARTERCHANGE,
	AMT_STOCKINQINSPAMT_1DAYCHANGE,
	AMT_STOCKINQINSPAMT_1WEEKCHANGE,
	AMT_STOCKINQINSPAMT_1QUARTERCHANGE,
	AMT_STOCKINQINSPAMT_1MONTHCHANGE,
	AMT_BLOCKEDSTOCKAMT_1DAYCHANGE,
	AMT_BLOCKEDSTOCKAMT_1WEEKCHANGE,
	AMT_BLOCKEDSTOCKAMT_1QUARTERCHANGE,
	AMT_BLOCKEDSTOCKAMT_1MONTHCHANGE,
	CT_BLOCKEDSTOCK_1DAYCHANGE,
	CT_BLOCKEDSTOCK_1WEEKCHANGE,
	CT_BLOCKEDSTOCK_1QUARTERCHANGE,
	CT_BLOCKEDSTOCK_1MONTHCHANGE,
	CT_POOPENQTY,
	CT_SOOPENQTY,
	AMT_EXCHANGERATE_GBL,
	AMT_EXCHANGERATE,
	DIM_PROFITCENTERID,
	CT_WIPQTY,
	DIM_CURRENCYID_TRA,
	DIM_CURRENCYID_GBL,
	CT_GRQTY_LATE30,
	CT_GRQTY_31_60,
	CT_GRQTY_61_90,
	CT_GRQTY_91_180,
	CT_GIQTY_LATE30,
	CT_GIQTY_31_60,
	CT_GIQTY_61_90,
	CT_GIQTY_91_180,
	DD_PRODORDERNUMBER,
	DD_PRODORDERITEMNO,
	DIM_PRODUCTIONORDERSTATUSID,
	DIM_PRODUCTIONORDERTYPEID,
	DIM_PARTSALESID,
	AMT_STOCKINTRANSITAMT_1WEEKCHANGE,
	AMT_STOCKINTRANSITAMT_1MONTHCHANGE,
	AMT_STOCKINTRANSITAMT_1QUARTERCHANGE,
	AMT_STOCKINTRANSITAMT_1DAYCHANGE,
	DW_INSERT_DATE,
	DW_UPDATE_DATE,
	DIM_PROJECTSOURCEID,
	DIM_BWPRODUCTHIERARCHYID,
	DIM_MDG_PARTID,
	DD_INVCOVFLAG_EMD,
	CT_AVGFCST_EMD,
	DIM_BWPARTID,
	AMT_COGSACTUALRATE_EMD,
	AMT_COGSFIXEDRATE_EMD,
	AMT_COGSFIXEDPLANRATE_EMD,
	AMT_COGSPLANRATE_EMD,
	AMT_COGSPREVYEARFIXEDRATE_EMD,
	AMT_COGSPREVYEARRATE_EMD,
	AMT_COGSPREVYEARTO1_EMD,
	AMT_COGSPREVYEARTO2_EMD,
	AMT_COGSPREVYEARTO3_EMD,
	AMT_COGSTURNOVERRATE1_EMD,
	AMT_COGSTURNOVERRATE2_EMD,
	AMT_COGSTURNOVERRATE3_EMD,
	DIM_DATEIDLASTPROCESSED,
	AMT_ONHAND_STOCK_HIST,
	CT_ONHAND_STOCK_HIST,
	DIM_BWHIERARCHYCOUNTRYID,
	DIM_CLUSTERID,
	DIM_COMMERCIALVIEWID,
	DIM_COUNTRYHIERARID,
	DIM_COUNTRYHIERPSID,
	CT_COUNTMATERIALSAFETYSTOCK,
	AMT_COGS_1DAYCHANGE,
	AMT_COGS_1WEEKCHANGE,
	AMT_COGS_1MONTHCHANGE,
	DD_ISCONSIGNED,
	DIM_GSAMAPIMPORTID,
	DIM_CUSTOMERIDSHIPTO,
	DIM_CUSTOMERID,
	CT_BASEUOMRATIOPCCUSTOM,
	DD_BATCH_STATUS_QKZ,
	DIM_PRODUCTIONSCHEDULERID,
	CT_COGSONHAND,
	CT_COGSONHAND_1M,
	CT_COGSONHAND_3M,
	CT_COGSONHAND_12M,
	AMT_COGSONHAND_1MDELTA,
	AMT_COGSONHAND_3MDELTA,
	AMT_COGSONHAND_12MDELTA,
	AMT_COGSONHAND,
	AMT_COGSONHAND_1M,
	AMT_COGSONHAND_3M,
	AMT_COGSONHAND_12M,
	AMT_COGSONHAND_1MCEF,
	AMT_COGSONHAND_3MCEF,
	AMT_COGSONHAND_12MCEF,
	CT_GRQTY,
	CT_ORDERITEMQTY,
	CT_BASEUOMRATIOKG,
	CT_INTRANSITKG,
	CT_DEPENDENTDEMANDKG,
	AMT_ONHAND,
	AMT_ONHAND_WEEKLY_DIFF,
	AMT_ONHAND_MONTHLY_DIFF,
	AMT_ONHAND_YEARLY_DIFF,
	DD_BATCHSTATUS,
	DIM_DATEIDEXPIRYDATE,
	DIM_BATCHSTATUSID,
	AMT_COGSFIXEDRATE_EMD_NEW,
	AMT_COGSPLANRATE_EMD_NEW,
	AMT_COGSFIXEDPLANRATE_EMD_NEW,
	AMT_EXCHANGERATE_GBL_NEW,
	AMT_ONHAND_NEW,
	AMT_STDUNITPRICE_NEW,
	AMT_STDUNITPRICE_VALIDATING,
	CT_OPENORDERQTYUOM,
	AMT_OPENORDER,
	CT_NUMBEROFPALLETS,
	AMT_COGSFIXEDPLANRATE_EMD_BIS,
	AMT_STDUNITPRICE_BIS,
	AMT_EXCHANGERATE_GBL_BIS,
	AMT_COGSONHAND_1MTHDELTA,
	DD_DUMMYROW)
select t.FACT_INVENTORYHISTORYID,
	t.CT_STOCKQTY,
	t.AMT_STOCKVALUEAMT,
	t.CT_LASTRECEIVEDQTY,
	t.DD_BATCHNO,
	t.DD_VALUATIONTYPE,
	t.DD_DOCUMENTNO,
	t.DD_DOCUMENTITEMNO,
	t.DD_MOVEMENTTYPE,
	t.DIM_STORAGELOCENTRYDATEID,
	t.DIM_LASTRECEIVEDDATEID,
	t.DIM_PARTID,
	t.DIM_PLANTID,
	t.DIM_STORAGELOCATIONID,
	t.DIM_COMPANYID,
	t.DIM_VENDORID,
	t.DIM_CURRENCYID,
	t.DIM_STOCKTYPEID,
	t.DIM_SPECIALSTOCKID,
	t.DIM_PURCHASEORGID,
	t.DIM_PURCHASEGROUPID,
	t.DIM_PRODUCTHIERARCHYID,
	t.DIM_UNITOFMEASUREID,
	t.DIM_MOVEMENTINDICATORID,
	t.DIM_CONSUMPTIONTYPEID,
	t.DIM_COSTCENTERID,
	t.DIM_DOCUMENTSTATUSID,
	t.DIM_DOCUMENTTYPEID,
	t.DIM_INCOTERMID,
	t.DIM_ITEMCATEGORYID,
	t.DIM_ITEMSTATUSID,
	t.DIM_TERMID,
	t.DIM_PURCHASEMISCID,
	t.AMT_STOCKVALUEAMT_GBL,
	t.CT_TOTALRESTRICTEDSTOCK,
	t.CT_STOCKINQINSP,
	t.CT_BLOCKEDSTOCK,
	t.CT_STOCKINTRANSFER,
	t.CT_UNRESTRICTEDCONSGNSTOCK,
	t.CT_RESTRICTEDCONSGNSTOCK,
	t.CT_BLOCKEDCONSGNSTOCK,
	t.CT_CONSGNSTOCKINQINSP,
	t.CT_BLOCKEDSTOCKRETURNS,
	t.AMT_BLOCKEDSTOCKAMT,
	t.AMT_BLOCKEDSTOCKAMT_GBL,
	t.AMT_STOCKINQINSPAMT,
	t.AMT_STOCKINQINSPAMT_GBL,
	t.AMT_STOCKINTRANSFERAMT,
	t.AMT_STOCKINTRANSFERAMT_GBL,
	t.AMT_UNRESTRICTEDCONSGNSTOCKAMT,
	t.AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,
	t.AMT_STDUNITPRICE,
	t.AMT_STDUNITPRICE_GBL,
	t.DIM_STOCKCATEGORYID,
	t.CT_STOCKINTRANSIT,
	t.AMT_STOCKINTRANSITAMT,
	t.AMT_STOCKINTRANSITAMT_GBL,
	t.DIM_SUPPLYINGPLANTID,
	t.AMT_MTLDLVRCOST,
	t.AMT_OVERHEADCOST,
	t.AMT_OTHERCOST,
	t.AMT_WIPBALANCE,
	t.CT_WIPAGING,
	t.AMT_MOVINGAVGPRICE,
	t.AMT_PREVIOUSPRICE,
	t.AMT_COMMERICALPRICE1,
	t.AMT_PLANNEDPRICE1,
	t.AMT_PREVPLANNEDPRICE,
	t.AMT_CURPLANNEDPRICE,
	t.DIM_DATEIDPLANNEDPRICE2,
	t.DIM_DATEIDLASTCHANGEDPRICE,
	t.DIM_DATEIDSNAPSHOT,
	t.SNAPSHOTDATE,
	t.CT_STOCKQTY_1DAYCHANGE,
	t.CT_STOCKQTY_1WEEKCHANGE,
	t.CT_STOCKQTY_1MONTHCHANGE,
	t.CT_STOCKQTY_1QUARTERCHANGE,
	t.CT_STOCKINTRANSIT_1DAYCHANGE,
	t.CT_STOCKINTRANSIT_1WEEKCHANGE,
	t.CT_STOCKINTRANSIT_1MONTHCHANGE,
	t.CT_STOCKINTRANSIT_1QUARTERCHANGE,
	t.CT_STOCKINTRANSFER_1DAYCHANGE,
	t.CT_STOCKINTRANSFER_1WEEKCHANGE,
	t.CT_STOCKINTRANSFER_1MONTHCHANGE,
	t.CT_STOCKINTRANSFER_1QUARTERCHANGE,
	t.CT_STOCKINQINSP_1DAYCHANGE,
	t.CT_STOCKINQINSP_1WEEKCHANGE,
	t.CT_STOCKINQINSP_1MONTHCHANGE,
	t.CT_STOCKINQINSP_1QUARTERCHANGE,
	t.CT_TOTALRESTRICTEDSTOCK_1DAYCHANGE,
	t.CT_TOTALRESTRICTEDSTOCK_1WEEKCHANGE,
	t.CT_TOTALRESTRICTEDSTOCK_1MONTHCHANGE,
	t.CT_TOTALRESTRICTEDSTOCK_1QUARTERCHANGE,
	t.AMT_STOCKVALUEAMT_1DAYCHANGE,
	t.AMT_STOCKVALUEAMT_1WEEKCHANGE,
	t.AMT_STOCKVALUEAMT_1MONTHCHANGE,
	t.AMT_STOCKVALUEAMT_1QUARTERCHANGE,
	t.AMT_STOCKINTRANSFERAMT_1DAYCHANGE,
	t.AMT_STOCKINTRANSFERAMT_1WEEKCHANGE,
	t.AMT_STOCKINTRANSFERAMT_1MONTHCHANGE,
	t.AMT_STOCKINTRANSFERAMT_1QUARTERCHANGE,
	t.AMT_STOCKINQINSPAMT_1DAYCHANGE,
	t.AMT_STOCKINQINSPAMT_1WEEKCHANGE,
	t.AMT_STOCKINQINSPAMT_1QUARTERCHANGE,
	t.AMT_STOCKINQINSPAMT_1MONTHCHANGE,
	t.AMT_BLOCKEDSTOCKAMT_1DAYCHANGE,
	t.AMT_BLOCKEDSTOCKAMT_1WEEKCHANGE,
	t.AMT_BLOCKEDSTOCKAMT_1QUARTERCHANGE,
	t.AMT_BLOCKEDSTOCKAMT_1MONTHCHANGE,
	t.CT_BLOCKEDSTOCK_1DAYCHANGE,
	t.CT_BLOCKEDSTOCK_1WEEKCHANGE,
	t.CT_BLOCKEDSTOCK_1QUARTERCHANGE,
	t.CT_BLOCKEDSTOCK_1MONTHCHANGE,
	t.CT_POOPENQTY,
	t.CT_SOOPENQTY,
	t.AMT_EXCHANGERATE_GBL,
	t.AMT_EXCHANGERATE,
	t.DIM_PROFITCENTERID,
	t.CT_WIPQTY,
	t.DIM_CURRENCYID_TRA,
	t.DIM_CURRENCYID_GBL,
	t.CT_GRQTY_LATE30,
	t.CT_GRQTY_31_60,
	t.CT_GRQTY_61_90,
	t.CT_GRQTY_91_180,
	t.CT_GIQTY_LATE30,
	t.CT_GIQTY_31_60,
	t.CT_GIQTY_61_90,
	t.CT_GIQTY_91_180,
	t.DD_PRODORDERNUMBER,
	t.DD_PRODORDERITEMNO,
	t.DIM_PRODUCTIONORDERSTATUSID,
	t.DIM_PRODUCTIONORDERTYPEID,
	t.DIM_PARTSALESID,
	t.AMT_STOCKINTRANSITAMT_1WEEKCHANGE,
	t.AMT_STOCKINTRANSITAMT_1MONTHCHANGE,
	t.AMT_STOCKINTRANSITAMT_1QUARTERCHANGE,
	t.AMT_STOCKINTRANSITAMT_1DAYCHANGE,
	t.DW_INSERT_DATE,
	t.DW_UPDATE_DATE,
	t.DIM_PROJECTSOURCEID,
	t.DIM_BWPRODUCTHIERARCHYID,
	t.DIM_MDG_PARTID,
	t.DD_INVCOVFLAG_EMD,
	t.CT_AVGFCST_EMD,
	t.DIM_BWPARTID,
	t.AMT_COGSACTUALRATE_EMD,
	t.AMT_COGSFIXEDRATE_EMD,
	t.AMT_COGSFIXEDPLANRATE_EMD,
	t.AMT_COGSPLANRATE_EMD,
	t.AMT_COGSPREVYEARFIXEDRATE_EMD,
	t.AMT_COGSPREVYEARRATE_EMD,
	t.AMT_COGSPREVYEARTO1_EMD,
	t.AMT_COGSPREVYEARTO2_EMD,
	t.AMT_COGSPREVYEARTO3_EMD,
	t.AMT_COGSTURNOVERRATE1_EMD,
	t.AMT_COGSTURNOVERRATE2_EMD,
	t.AMT_COGSTURNOVERRATE3_EMD,
	t.DIM_DATEIDLASTPROCESSED,
	t.AMT_ONHAND_STOCK_HIST,
	t.CT_ONHAND_STOCK_HIST,
	t.DIM_BWHIERARCHYCOUNTRYID,
	t.DIM_CLUSTERID,
	t.DIM_COMMERCIALVIEWID,
	t.DIM_COUNTRYHIERARID,
	t.DIM_COUNTRYHIERPSID,
	t.CT_COUNTMATERIALSAFETYSTOCK,
	t.AMT_COGS_1DAYCHANGE,
	t.AMT_COGS_1WEEKCHANGE,
	t.AMT_COGS_1MONTHCHANGE,
	t.DD_ISCONSIGNED,
	t.DIM_GSAMAPIMPORTID,
	t.DIM_CUSTOMERIDSHIPTO,
	t.DIM_CUSTOMERID,
	t.CT_BASEUOMRATIOPCCUSTOM,
	t.DD_BATCH_STATUS_QKZ,
	t.DIM_PRODUCTIONSCHEDULERID,
	t.CT_COGSONHAND,
	t.CT_COGSONHAND_1M,
	t.CT_COGSONHAND_3M,
	t.CT_COGSONHAND_12M,
	t.AMT_COGSONHAND_1MDELTA,
	t.AMT_COGSONHAND_3MDELTA,
	t.AMT_COGSONHAND_12MDELTA,
	t.AMT_COGSONHAND,
	t.AMT_COGSONHAND_1M,
	t.AMT_COGSONHAND_3M,
	t.AMT_COGSONHAND_12M,
	t.AMT_COGSONHAND_1MCEF,
	t.AMT_COGSONHAND_3MCEF,
	t.AMT_COGSONHAND_12MCEF,
	t.CT_GRQTY,
	t.CT_ORDERITEMQTY,
	t.CT_BASEUOMRATIOKG,
	t.CT_INTRANSITKG,
	t.CT_DEPENDENTDEMANDKG,
	t.AMT_ONHAND,
	t.AMT_ONHAND_WEEKLY_DIFF,
	t.AMT_ONHAND_MONTHLY_DIFF,
	t.AMT_ONHAND_YEARLY_DIFF,
	t.DD_BATCHSTATUS,
	t.DIM_DATEIDEXPIRYDATE,
	t.DIM_BATCHSTATUSID,
	t.AMT_COGSFIXEDRATE_EMD_NEW,
	t.AMT_COGSPLANRATE_EMD_NEW,
	t.AMT_COGSFIXEDPLANRATE_EMD_NEW,
	t.AMT_EXCHANGERATE_GBL_NEW,
	t.AMT_ONHAND_NEW,
	t.AMT_STDUNITPRICE_NEW,
	t.AMT_STDUNITPRICE_VALIDATING,
	t.CT_OPENORDERQTYUOM,
	t.AMT_OPENORDER,
	t.CT_NUMBEROFPALLETS,
	t.AMT_COGSFIXEDPLANRATE_EMD_BIS,
	t.AMT_STDUNITPRICE_BIS,
	t.AMT_EXCHANGERATE_GBL_BIS,
	t.AMT_COGSONHAND_1MTHDELTA,
	t.DD_DUMMYROW
from tmp_insertdummyrows t;

DROP TABLE IF EXISTS TMP_MAXIMUM_ID;
CREATE TABLE TMP_MAXIMUM_ID
AS
SELECT snapshotdate,dim_plantid,dim_partid,f_ih.DIM_BWPRODUCTHIERARCHYID,max(fact_inventoryhistoryid) ID
FROM fact_inventoryhistory f_ih
	INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
	inner join dim_bwproducthierarchy bw on f_ih.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
WHERE snp.datevalue = snp.monthenddate
and businesssector = 'BS-01'
GROUP BY snapshotdate,dim_plantid,dim_partid,f_ih.DIM_BWPRODUCTHIERARCHYID;

merge into fact_inventoryhistory f
using(
select f.fact_inventoryhistoryid
  ,(ifnull(t1.cogs_on_hand,0)- ifnull(t2.cogs_on_hand,0)) as v_cogs
  ,(ifnull(t1.on_hand_amt,0)- ifnull(t2.on_hand_amt,0)) as v_onhand
  ,(ifnull(t1.on_hand_qty,0)- ifnull(t2.on_hand_qty,0)) as q_onhand
  from fact_inventoryhistory f
    inner join TMP_MAXIMUM_ID tmp on f.fact_inventoryhistoryid = tmp.id
    inner join dim_date dt on f.Dim_DateidSnapshot = dt.dim_dateid
    left join tmp_lastdayofmth t1 on dt.datevalue = t1.snapshotdate and f.dim_plantid = t1.dim_plantid and f.dim_partid = t1.dim_partid and f.DIM_BWPRODUCTHIERARCHYID = t1.DIM_BWPRODUCTHIERARCHYID
      left join tmp_lastdayofmth t2 on dt.datevalue = add_months(t2.snapshotdate,1) and f.dim_plantid = t2.dim_plantid and f.dim_partid = t2.dim_partid and f.DIM_BWPRODUCTHIERARCHYID = t2.DIM_BWPRODUCTHIERARCHYID) x
  on f.fact_inventoryhistoryid = x.fact_inventoryhistoryid
when matched then update set f.AMT_COGSONHAND_1MTHDELTA = x.v_cogs,f.AMT_ONHAND_MTH_END_DELTA = x.v_onhand,f.CT_ONHAND_MTH_END_DELTA = x.q_onhand;

merge into fact_inventoryhistory f
using(
  select f.fact_inventoryhistoryid
    ,(ifnull(t1.cogs_on_hand,0)- ifnull(t2.cogs_on_hand,0)) as v_cogs
	   ,(ifnull(t1.on_hand_amt,0)- ifnull(t2.on_hand_amt,0)) as v_onhand
     ,(ifnull(t1.on_hand_qty,0)- ifnull(t2.on_hand_qty,0)) as q_onhand
  from fact_inventoryhistory f
    inner join TMP_MAXIMUM_ID tmp on f.fact_inventoryhistoryid = tmp.id
    inner join dim_date dt on f.Dim_DateidSnapshot = dt.dim_dateid
    left join tmp_lastdayofmth t1 on dt.datevalue = t1.snapshotdate and f.dim_plantid = t1.dim_plantid and f.dim_partid = t1.dim_partid and f.DIM_BWPRODUCTHIERARCHYID = t1.DIM_BWPRODUCTHIERARCHYID
    left join tmp_lastdayofmth t2 on dt.datevalue = add_months(t2.snapshotdate,3) and f.dim_plantid = t2.dim_plantid and f.dim_partid = t2.dim_partid and f.DIM_BWPRODUCTHIERARCHYID = t2.DIM_BWPRODUCTHIERARCHYID
  where month(dt.datevalue) in (3,6,9,12)) x
  on f.fact_inventoryhistoryid = x.fact_inventoryhistoryid
when matched then update set f.AMT_COGSONHAND_Q_END_DELTA = x.v_cogs, AMT_ONHAND_Q_END_DELTA = x.v_onhand, CT_ONHAND_Q_END_DELTA = x.q_onhand;

/* END - APP-6460 - Oana 24 July 2017  */


/* 27 Oct 2017 - Roxana H - add the filter from the report as a flag so that it improves the performance */

update fact_inventoryhistory f_ih
set f_ih.dd_companyemdpm = case when dc.Name2 not in ('001670 - Merck Adv. Tech., Korea','1042 - Merck Sdn Bhd Malaysia','1044 - Merck Ltd. Thailand',
                              '1055 - Merck (Pty) Ltd. South Africa','1779 - Litec-LLL GmbH, Germ','1798 - Merck Vietnam Ltd. Vietnam',
                              '1923 - Merck Pty. Ltd. Australia','Not Set') 
                                then 'Yes' 
                                  else 'No' 
                            end
from fact_inventoryhistory f_ih, dim_company dc
where f_ih.dim_companyid = dc.dim_companyid;


/*Add dd_primaryproductionlocation Roxana D 2017-11-01*/

update FACT_INVENTORYHISTORY f
set f.DD_PRIMARYPRODUCTIONLOCATION = mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
FROM  FACT_INVENTORYHISTORY f
inner join  dim_cluster dc on dc.dim_clusterid = f.dim_clusterid
inner join dim_bwproducthierarchy ph on f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
   and businesssector = 'BS-02'
inner join (select distinct primary_production_location,primary_production_location_name  
from dim_mdg_part ) mdg on mdg.primary_production_location = dc.primary_manufacturing_site
where f.DD_PRIMARYPRODUCTIONLOCATION <> mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
and f.snapshotdate in (select distinct snapshotdate from fact_inventoryhistory_delete);

drop table if exists fact_inventoryhistory_delete;


/*End*/


