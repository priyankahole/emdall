/* Marius 10 Jun 2016 New SA for Merck HC - Merck Healthcare IBP */


delete from fact_inventorycoverage;

insert into fact_inventorycoverage(AMT_BLOCKEDSTOCKAMT,
AMT_BLOCKEDSTOCKAMT_GBL,
AMT_COGSACTUALRATE_EMD,
AMT_COGSFIXEDPLANRATE_EMD,
AMT_COGSFIXEDRATE_EMD,
AMT_COGSPLANRATE_EMD,
AMT_COGSPREVYEARFIXEDRATE_EMD,
AMT_COGSPREVYEARRATE_EMD,
AMT_COGSPREVYEARTO1_EMD,
AMT_COGSPREVYEARTO2_EMD,
AMT_COGSPREVYEARTO3_EMD,
AMT_COGSTURNOVERRATE1_EMD,
AMT_COGSTURNOVERRATE2_EMD,
AMT_COGSTURNOVERRATE3_EMD,
AMT_COMMERICALPRICE1,
AMT_CURPLANNEDPRICE,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
AMT_GBLSTDPRICE_MERCK,
AMT_MOVINGAVGPRICE,
AMT_MTLDLVRCOST,
AMT_ONHAND,
AMT_ONHAND_GBL,
AMT_OTHERCOST,
AMT_OVERHEADCOST,
AMT_PLANNEDPRICE1,
AMT_PREVIOUSPRICE,
AMT_PREVPLANNEDPRICE,
AMT_PRICEUNIT,
AMT_STDPRICEPMRA_MERCK,
AMT_STDUNITPRICE,
AMT_STDUNITPRICE_GBL,
AMT_STOCKINQINSPAMT,
AMT_STOCKINQINSPAMT_GBL,
AMT_STOCKINTRANSFERAMT,
AMT_STOCKINTRANSFERAMT_GBL,
AMT_STOCKINTRANSITAMT,
AMT_STOCKINTRANSITAMT_GBL,
AMT_STOCKVALUEAMT,
AMT_STOCKVALUEAMT_GBL,
AMT_UNRESTRICTEDCONSGNSTOCKAMT,
AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,
AMT_WIPBALANCE,
CT_AVGFCST_EMD,
CT_BLOCKEDCONSGNSTOCK,
CT_BLOCKEDSTOCK,
CT_BLOCKEDSTOCKRETURNS,
CT_CONSGNSTOCKINQINSP,
CT_GIQTY_31_60,
CT_GIQTY_61_90,
CT_GIQTY_91_180,
CT_GIQTY_LATE30,
CT_GLOBALEXTTOTALCOST_MERCK,
CT_GLOBALONHANDAMT_MERCK,
CT_GRQTY_31_60,
CT_GRQTY_61_90,
CT_GRQTY_91_180,
CT_GRQTY_LATE30,
CT_LASTRECEIVEDQTY,
CT_LOCALEXTTOTALCOST_MERCK,
CT_LOCALONHANDAMT_MERCK,
CT_POOPENQTY,
CT_RESTRICTEDCONSGNSTOCK,
CT_SOOPENQTY,
CT_STOCKINQINSP,
CT_STOCKINTRANSFER,
CT_STOCKINTRANSIT,
CT_STOCKQTY,
CT_TOTALRESTRICTEDSTOCK,
CT_TOTVALSTKQTY,
CT_UNRESTRICTEDCONSGNSTOCK,
CT_WIPAGING,
CT_WIPQTY,
DD_BATCHNO,
DD_DOCUMENTITEMNO,
DD_DOCUMENTNO,
DD_INVCOVFLAG_EMD,
DD_MOVEMENTTYPE,
DD_PLANNEDORDERNO,
DD_PRODORDERITEMNO,
DD_PRODORDERNUMBER,
DD_VALUATIONTYPE,
DIM_BWHIERARCHYCOUNTRYID,
DIM_BWPARTID,
DIM_BWPRODUCTHIERARCHYID,
DIM_COMPANYID,
DIM_CONSUMPTIONTYPEID,
DIM_COSTCENTERID,
DIM_CURRENCYID,
DIM_CURRENCYID_GBL,
DIM_CURRENCYID_TRA,
DIM_CUSTOMERID,
DIM_DATEIDACTUALRELEASE,
DIM_DATEIDACTUALSTART,
DIM_DATEIDEXPIRYDATE,
DIM_DATEIDLASTCHANGEDPRICE,
DIM_DATEIDLASTPROCESSED,
DIM_DATEIDPLANNEDPRICE2,
DIM_DATEIDTRANSACTION,
DIM_DOCUMENTSTATUSID,
DIM_DOCUMENTTYPEID,
DIM_INCOTERMID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_LASTRECEIVEDDATEID,
DIM_MDG_PARTID,
DIM_MOVEMENTINDICATORID,
DIM_PARTID,
DIM_PARTSALESID,
DIM_PLANTID,
DIM_PRODUCTHIERARCHYID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PROFITCENTERID,
DIM_PROJECTSOURCEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEMISCID,
DIM_PURCHASEORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKCATEGORYID,
DIM_STOCKTYPEID,
DIM_STORAGELOCATIONID,
DIM_STORAGELOCENTRYDATEID,
DIM_SUPPLYINGPLANTID,
DIM_TERMID,
DIM_UNITOFMEASUREID,
DIM_VENDORID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
FACT_INVENTORYCOVERAGEID,
DIM_CLUSTERID,
AMT_EXCHANGERATE_CUSTOM,
DIM_DATEOFMANUFACTURE,
DIM_DATEOFLASTGOODSRECEIPT,
CT_BASEUOMRATIOKG,
DD_JDACONTAINER)
select
AMT_BLOCKEDSTOCKAMT,
AMT_BLOCKEDSTOCKAMT_GBL,
AMT_COGSACTUALRATE_EMD,
AMT_COGSFIXEDPLANRATE_EMD,
AMT_COGSFIXEDRATE_EMD,
AMT_COGSPLANRATE_EMD,
AMT_COGSPREVYEARFIXEDRATE_EMD,
AMT_COGSPREVYEARRATE_EMD,
AMT_COGSPREVYEARTO1_EMD,
AMT_COGSPREVYEARTO2_EMD,
AMT_COGSPREVYEARTO3_EMD,
AMT_COGSTURNOVERRATE1_EMD,
AMT_COGSTURNOVERRATE2_EMD,
AMT_COGSTURNOVERRATE3_EMD,
AMT_COMMERICALPRICE1,
AMT_CURPLANNEDPRICE,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
AMT_GBLSTDPRICE_MERCK,
AMT_MOVINGAVGPRICE,
AMT_MTLDLVRCOST,
AMT_ONHAND,
AMT_ONHAND_GBL,
AMT_OTHERCOST,
AMT_OVERHEADCOST,
AMT_PLANNEDPRICE1,
AMT_PREVIOUSPRICE,
AMT_PREVPLANNEDPRICE,
AMT_PRICEUNIT,
AMT_STDPRICEPMRA_MERCK,
AMT_STDUNITPRICE,
AMT_STDUNITPRICE_GBL,
AMT_STOCKINQINSPAMT,
AMT_STOCKINQINSPAMT_GBL,
AMT_STOCKINTRANSFERAMT,
AMT_STOCKINTRANSFERAMT_GBL,
AMT_STOCKINTRANSITAMT,
AMT_STOCKINTRANSITAMT_GBL,
AMT_STOCKVALUEAMT,
AMT_STOCKVALUEAMT_GBL,
AMT_UNRESTRICTEDCONSGNSTOCKAMT,
AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,
AMT_WIPBALANCE,
CT_AVGFCST_EMD,
CT_BLOCKEDCONSGNSTOCK,
CT_BLOCKEDSTOCK,
CT_BLOCKEDSTOCKRETURNS,
CT_CONSGNSTOCKINQINSP,
CT_GIQTY_31_60,
CT_GIQTY_61_90,
CT_GIQTY_91_180,
CT_GIQTY_LATE30,
CT_GLOBALEXTTOTALCOST_MERCK,
CT_GLOBALONHANDAMT_MERCK,
CT_GRQTY_31_60,
CT_GRQTY_61_90,
CT_GRQTY_91_180,
CT_GRQTY_LATE30,
CT_LASTRECEIVEDQTY,
CT_LOCALEXTTOTALCOST_MERCK,
CT_LOCALONHANDAMT_MERCK,
CT_POOPENQTY,
CT_RESTRICTEDCONSGNSTOCK,
CT_SOOPENQTY,
CT_STOCKINQINSP,
CT_STOCKINTRANSFER,
CT_STOCKINTRANSIT,
CT_STOCKQTY,
CT_TOTALRESTRICTEDSTOCK,
CT_TOTVALSTKQTY,
CT_UNRESTRICTEDCONSGNSTOCK,
CT_WIPAGING,
CT_WIPQTY,
DD_BATCHNO,
DD_DOCUMENTITEMNO,
DD_DOCUMENTNO,
DD_INVCOVFLAG_EMD,
DD_MOVEMENTTYPE,
DD_PLANNEDORDERNO,
DD_PRODORDERITEMNO,
DD_PRODORDERNUMBER,
DD_VALUATIONTYPE,
DIM_BWHIERARCHYCOUNTRYID,
DIM_BWPARTID,
DIM_BWPRODUCTHIERARCHYID,
DIM_COMPANYID,
DIM_CONSUMPTIONTYPEID,
DIM_COSTCENTERID,
DIM_CURRENCYID,
DIM_CURRENCYID_GBL,
DIM_CURRENCYID_TRA,
DIM_CUSTOMERID,
DIM_DATEIDACTUALRELEASE,
DIM_DATEIDACTUALSTART,
DIM_DATEIDEXPIRYDATE,
DIM_DATEIDLASTCHANGEDPRICE,
DIM_DATEIDLASTPROCESSED,
DIM_DATEIDPLANNEDPRICE2,
DIM_DATEIDTRANSACTION,
DIM_DOCUMENTSTATUSID,
DIM_DOCUMENTTYPEID,
DIM_INCOTERMID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_LASTRECEIVEDDATEID,
DIM_MDG_PARTID,
DIM_MOVEMENTINDICATORID,
DIM_PARTID,
DIM_PARTSALESID,
DIM_PLANTID,
DIM_PRODUCTHIERARCHYID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PROFITCENTERID,
DIM_PROJECTSOURCEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEMISCID,
DIM_PURCHASEORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKCATEGORYID,
DIM_STOCKTYPEID,
DIM_STORAGELOCATIONID,
DIM_STORAGELOCENTRYDATEID,
DIM_SUPPLYINGPLANTID,
DIM_TERMID,
DIM_UNITOFMEASUREID,
DIM_VENDORID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
fact_inventoryagingid,
DIM_CLUSTERID,
AMT_EXCHANGERATE_CUSTOM,
DIM_DATEOFMANUFACTURE,
DIM_DATEOFLASTGOODSRECEIPT,
CT_BASEUOMRATIOKG,
DD_JDACONTAINER
from fact_inventoryaging;

/* 10 Oct 2016 CristianT Start: Removed droping tables with source from EMDTEMPOCC4 schema. Added truncate and insert logic */
TRUNCATE TABLE tmp_packsbyproduct;

INSERT INTO tmp_packsbyproduct(
dim_jda_productid,
product_name,
datevalue,
packs_adjhist,
packs_mktfcst,
packs_op
)
select
	max(f.DIM_JDA_PRODUCTID) DIM_JDA_PRODUCTID
	,p.PRODUCT_NAME
	,date_trunc('month',strt.datevalue) datevalue
	,sum(case when month(strt.datevalue) < month(current_date) and f.dd_type = 'ADJHIST' then f.ct_packs else 0 end) packs_ADJHIST
	,sum(case when month(strt.datevalue) >= month(current_date) and f.dd_type in ('TENDER','MKTFCST') then f.ct_packs else 0 end) packs_MKTFCST
	,sum(case when f.dd_type in ('OPERATING PLAN') then f.ct_packs else 0 end) packs_OP
from EMDTempoCC4.fact_jda_demandforecast f
	inner join EMDTempoCC4.DIM_JDA_PRODUCT p on f.DIM_JDA_PRODUCTID = p.DIM_JDA_PRODUCTID
	inner join EMDTempoCC4.DIM_JDA_LOCATION lc on f.DIM_JDA_LOCATIONID = lc.DIM_JDA_LOCATIONID
	inner join EMDTempoCC4.dim_date strt on f.DIM_DATEIDSTARTDATE = strt.dim_dateid
where f.dd_type in ('ADJHIST','TENDER','MKTFCST','OPERATING PLAN')
	and f.DD_FCSTLEVEL = '111' 
	and lc.LOCATION_NAME not like '%DISTR'
	and year(strt.datevalue) = year(current_date)
group by p.PRODUCT_NAME,date_trunc('month',strt.datevalue)
union
select
	max(f.DIM_JDA_PRODUCTID) DIM_JDA_PRODUCTID
	,f.dd_itemlocalcode PRODUCT_NAME
	,date_trunc('month',strt.datevalue) datevalue
	,sum(case when month(strt.datevalue) < month(current_date) and f.dd_type = 'ADJHIST' then f.ct_packs else 0 end) packs_ADJHIST
	,sum(case when month(strt.datevalue) >= month(current_date) and f.dd_type in ('TENDER','MKTFCST') then f.ct_packs else 0 end) packs_MKTFCST
	,sum(case when f.dd_type in ('OPERATING PLAN') then f.ct_packs else 0 end) packs_OP
from EMDTempoCC4.fact_jda_demandforecast f
	inner join EMDTempoCC4.DIM_JDA_LOCATION lc on f.DIM_JDA_LOCATIONID = lc.DIM_JDA_LOCATIONID
	inner join EMDTempoCC4.dim_date strt on f.DIM_DATEIDSTARTDATE = strt.dim_dateid
where f.dd_type in ('ADJHIST','TENDER','MKTFCST','OPERATING PLAN')
	and f.DD_FCSTLEVEL = '111' 
	and lc.LOCATION_NAME not like '%DISTR'
	and year(strt.datevalue) = year(current_date)
	and f.dd_itemlocalcode not in (select distinct x.PRODUCT_NAME from EMDTempoCC4.DIM_JDA_PRODUCT x)
group by f.dd_itemlocalcode,date_trunc('month',strt.datevalue);

/* 10 Oct 2016 CristianT End */

/*
DROP TABLE IF EXISTS STSC_DFUVIEW
CREATE TABLE STSC_DFUVIEW
AS
SELECT * FROM EMDTEmpoCC4.STSC_DFUVIEW

drop table if exists tmp_sameproduct
create table tmp_sameproduct
as
select distinct stsc_dfuview_dmdunit,stsc_dfuview_udc_prodlocalid from STSC_DFUVIEW where stsc_dfuview_udc_fcstlevel = 111 and stsc_dfuview_dmdunit <> stsc_dfuview_udc_prodlocalid

drop table if exists tmp_packsbyproduct_original
create table tmp_packsbyproduct_original
as
select a.PRODUCT_NAME
	,a.datevalue
	,sum(c.packs_ADJHIST) packs_ADJHIST
	,sum(c.packs_MKTFCST) packs_MKTFCST
	,sum(c.packs_OP) packs_OP
from tmp_packsbyproduct a
	inner join tmp_sameproduct b on a.PRODUCT_NAME = b.stsc_dfuview_dmdunit
	inner join tmp_packsbyproduct c on b.stsc_dfuview_udc_prodlocalid = c.PRODUCT_NAME and a.datevalue = c.datevalue
group by a.PRODUCT_NAME, a.datevalue

update tmp_packsbyproduct a
set a.packs_ADJHIST = a.packs_ADJHIST + c.packs_ADJHIST
	,a.packs_MKTFCST = a.packs_MKTFCST + c.packs_MKTFCST
	,a.packs_OP = a.packs_OP + c.packs_OP
from tmp_packsbyproduct a
	inner join tmp_packsbyproduct_original c on a.PRODUCT_NAME = c.PRODUCT_NAME and a.datevalue = c.datevalue
	
drop table if exists tmp_packsbyproduct_original
create table tmp_packsbyproduct_original
as
select a.PRODUCT_NAME
	,a.datevalue
	,sum(c.packs_ADJHIST) packs_ADJHIST
	,sum(c.packs_MKTFCST) packs_MKTFCST
	,sum(c.packs_OP) packs_OP
from tmp_packsbyproduct a
	inner join tmp_sameproduct b on a.PRODUCT_NAME = b.stsc_dfuview_udc_prodlocalid
	inner join tmp_packsbyproduct c on b.stsc_dfuview_dmdunit = c.PRODUCT_NAME and a.datevalue = c.datevalue
group by a.PRODUCT_NAME, a.datevalue
	
update tmp_packsbyproduct a
set a.packs_ADJHIST = a.packs_ADJHIST + c.packs_ADJHIST
	,a.packs_MKTFCST = a.packs_MKTFCST + c.packs_MKTFCST
	,a.packs_OP = a.packs_OP + c.packs_OP
from tmp_packsbyproduct a
	inner join tmp_packsbyproduct_original c on a.PRODUCT_NAME = c.PRODUCT_NAME and a.datevalue = c.datevalue
*/
	
DROP TABLE IF EXISTS tmp_fcst_updt;
CREATE TABLE tmp_fcst_updt
AS
SELECT dc.company
	,dp.partnumber
	,MAX(f.fact_inventorycoverageid) fact_inventorycoverageid
FROM fact_inventorycoverage f
	INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	INNER JOIN dim_part dp ON f.dim_partid = dp.dim_partid
WHERE f.dd_invcovflag_emd = 'X'
GROUP BY dc.company, dp.partnumber;

update fact_inventorycoverage f
set f.ct_jda_actual_jan = p.packs_ADJHIST
	,f.ct_jda_fcst_jan = p.packs_MKTFCST
	,f.ct_jda_op_jan = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 1;
	
update fact_inventorycoverage f
set f.ct_jda_actual_feb = p.packs_ADJHIST
	,f.ct_jda_fcst_feb = p.packs_MKTFCST
	,f.ct_jda_op_feb = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 2;
	
update fact_inventorycoverage f
set f.ct_jda_actual_mar = p.packs_ADJHIST
	,f.ct_jda_fcst_mar = p.packs_MKTFCST
	,f.ct_jda_op_mar = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 3;
	
update fact_inventorycoverage f
set f.ct_jda_actual_apr = p.packs_ADJHIST
	,f.ct_jda_fcst_apr = p.packs_MKTFCST
	,f.ct_jda_op_apr = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 4;
	
update fact_inventorycoverage f
set f.ct_jda_actual_may = p.packs_ADJHIST
	,f.ct_jda_fcst_may = p.packs_MKTFCST
	,f.ct_jda_op_may = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 5;
	
update fact_inventorycoverage f
set f.ct_jda_actual_jun = p.packs_ADJHIST
	,f.ct_jda_fcst_jun = p.packs_MKTFCST
	,f.ct_jda_op_jun = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 6;
	
update fact_inventorycoverage f
set f.ct_jda_actual_jul = p.packs_ADJHIST
	,f.ct_jda_fcst_jul = p.packs_MKTFCST
	,f.ct_jda_op_jul = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 7;
	
update fact_inventorycoverage f
set f.ct_jda_actual_aug = p.packs_ADJHIST
	,f.ct_jda_fcst_aug = p.packs_MKTFCST
	,f.ct_jda_op_aug = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 8;
	
update fact_inventorycoverage f
set f.ct_jda_actual_sep = p.packs_ADJHIST
	,f.ct_jda_fcst_sep = p.packs_MKTFCST
	,f.ct_jda_op_sep = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 9;
	
update fact_inventorycoverage f
set f.ct_jda_actual_oct = p.packs_ADJHIST
	,f.ct_jda_fcst_oct = p.packs_MKTFCST
	,f.ct_jda_op_oct = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 10;
	
update fact_inventorycoverage f
set f.ct_jda_actual_nov = p.packs_ADJHIST
	,f.ct_jda_fcst_nov = p.packs_MKTFCST
	,f.ct_jda_op_nov = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 11;
	
update fact_inventorycoverage f
set f.ct_jda_actual_dec = p.packs_ADJHIST
	,f.ct_jda_fcst_dec = p.packs_MKTFCST
	,f.ct_jda_op_dec = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 12;
	
drop table if exists tmp_updt_jda_prod;
create table tmp_updt_jda_prod
as
select PRODUCT_NAME,max(DIM_JDA_PRODUCTID) DIM_JDA_PRODUCTID from tmp_packsbyproduct group by PRODUCT_NAME;
	
update fact_inventorycoverage f
set f.DIM_JDA_PRODUCTID = p.DIM_JDA_PRODUCTID
from fact_inventorycoverage f
	INNER JOIN dim_part dp ON f.dim_partid = dp.dim_partid
	inner join tmp_updt_jda_prod p on dp.partnumber = p.PRODUCT_NAME;