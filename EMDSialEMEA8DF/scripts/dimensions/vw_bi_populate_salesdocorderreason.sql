INSERT INTO dim_salesdocorderreason(dim_salesdocorderreasonid,
                             ReasonCode,
                             Description,
                             RowStartDate,
                             RowEndDate,
                             RowIsCurrent,
                             RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          current_timestamp,
          NULL,
          1,
          NULL		  
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesdocorderreason
               WHERE dim_salesdocorderreasonid = 1);

UPDATE dim_salesdocorderreason sdor
   SET sdor.Description = ifnull(TVAUT_BEZEI, 'Not Set'),
			sdor.dw_update_date = current_timestamp
    FROM dim_salesdocorderreason sdor, TVAUT t
 WHERE sdor.ReasonCode = t.TVAUT_AUGRU 
 AND sdor.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_salesdocorderreason';

insert into number_fountain
select 	'dim_salesdocorderreason',
	ifnull(max(d.dim_salesdocorderreasonid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesdocorderreason d
where d.dim_salesdocorderreasonid <> 1; 
 
 INSERT INTO dim_salesdocorderreason(dim_salesdocorderreasonid,
                                    ReasonCode,
                                    Description,
                                    RowStartDate,
                                    RowisCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesdocorderreason') 
          + row_number() over(order by ''),
			  TVAUT_AUGRU,
          ifnull(TVAUT_BEZEI, 'Not Set'),
          current_timestamp,
          1
     FROM TVAUT t
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM dim_salesdocorderreason sdor
                   WHERE sdor.ReasonCode = t.TVAUT_AUGRU);