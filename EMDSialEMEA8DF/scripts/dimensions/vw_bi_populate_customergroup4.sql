INSERT INTO dim_customergroup4(dim_customergroup4id, RowIsCurrent, RowStartDate, Description)
SELECT 1, 1, current_timestamp, 'Not Set'
FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS (SELECT 1 FROM dim_customergroup4 WHERE dim_customergroup4id = 1);

UPDATE
dim_customergroup4 cg4
SET cg4.Description = ifnull(TVV4T_BEZEI, 'Not Set'),
	cg4.dw_update_date = current_timestamp
FROM
dim_customergroup4 cg4,
TVV4T t
WHERE t.TVV4T_KVGR4 = cg4.CustomerGroup
AND TVV4T_BEZEI <> 'Dummy - do not use';

delete from number_fountain m where m.table_name = 'dim_customergroup4';

insert into number_fountain
select 	'dim_customergroup4',
	ifnull(max(d.dim_customergroup4id),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customergroup4 d
where d.dim_customergroup4id <> 1;

INSERT INTO dim_customergroup4(dim_customergroup4id,
                          LanguageKey,
                          CustomerGroup,
                          Description)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customergroup4')
          + row_number() over(order by ''),
                        TVV4T_SPRAS,
        TVV4T_KVGR4,
        ifnull(TVV4T_BEZEI,'Not Set')
FROM TVV4T
WHERE not exists (select 1 from dim_customergroup4 a where a.CustomerGroup = TVV4T_KVGR4)
AND TVV4T_BEZEI <> 'Dummy - do not use';

delete from number_fountain m where m.table_name = 'dim_customergroup4';
