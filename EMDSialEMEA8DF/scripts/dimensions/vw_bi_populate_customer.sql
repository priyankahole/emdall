/* ##################################################################################################################

   Script         : bi_vw_populate_customer
   Author         : AUngureanu
   Created On     : 22 09 2014


   Description    : Combine 3 script from Application Manager for populating DIM_CUSTOMER

   Change History
   Date            By        	Version           Desc
   22 09 2014     AUngureanu    1.0               Combine 3 script in one file
   Dec 2014	  SVinayan	1.1		  Added additional fields as a part of Columbia Report 856

#################################################################################################################### */

insert into dim_customer
(Dim_CustomerID,RowChangeReason,RowEndDate,RowIsCurrent,RowStartDate,Industry,CustomerNumber,Country,Name,
City,PostalCode,Region,Plant,OneTime,IndustryCode,Classification,ClassificationDescription,CreationDate,CreditRep,CreditLimit,
RiskClass,CustAccountGroup,RegionDescription,CustAccountGroupDescription,Name2,HighCustomerNumber,HigherCustomerNumber,
TopLevelCustomerNumber,HierarchyType,HighCustomerDesc,HigherCustomerDesc,TopLevelCustomerDesc,CountryName,Channel,SubChannel,
ChannelDescription,SubChannelDescription,TradingPartner,Attribute3,Attribute4,Attribute5,Attribute6,Attribute7,Attribute8,ConditionGroup1,
ConditionGroup2,IndustryCode1,ConditionGroupDescription2,IndustryCode1Description
,custBillingBlock,dataLine,deliveryBlock,deletionFlag,faxNumber,languageKey  /* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
,orderBlock,searchTerm,telephone1,transportationZone,brandDescription,transportationZoneDesc,addressnumber /* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
)
SELECT
1,null,null,1,current_timestamp,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set',null,'Not Set',0.00,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set'
,'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', /* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
'Not Set', 'Not Set', 'Not Set', 'Not Set','Not Set','Not Set','Not Set' /* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
FROM (SELECT 1) a
where not exists(select 1 from dim_customer where dim_customerid = 1);

/*  UPDATE PART  */
/* Start Of Update Customer Transfer Interface*/

UPDATE dim_customer dc
   SET 	dc.IndustryCode = ifnull(KNA1_BRSCH, 'Not Set'),
		dc.Country = ifnull(KNA1_LAND1, 'Not Set'),
		dc.Name = ifnull(KNA1_NAME1, 'Not Set'),
		dc.Name2 = ifnull(KNA1_NAME2, 'Not Set'),
		dc.City = ifnull(KNA1_ORT01, 'Not Set'),
		dc.PostalCode = ifnull(KNA1_PSTLZ, 'Not Set'),
		dc.Region = ifnull(KNA1_REGIO, 'Not Set'),
		dc.OneTime =(CASE WHEN ifnull(KNA1_XCPDK, '') = 'X' THEN 'Yes' ELSE 'No' END),
		dc.Classification = ifnull(KNA1_KUKLA,'Not Set'),
		dc.CreationDate = KNA1_ERDAT,
		dc.CustAccountGroup = ifnull(KNA1_KTOKD,'Not Set'),
		dc.CountryName = ifnull(t005t_landx, 'Not Set'),
		dc.Channel = ifnull(KNA1_KATR1,'Not Set'),
		dc.SubChannel = ifnull(KNA1_KATR2,'Not Set'),
		dc.TradingPartner = ifnull(KNA1_VBUND,'Not Set'),
		dc.Attribute3 = ifnull(KNA1_KATR3,'Not Set'),
		dc.Attribute4 = ifnull(KNA1_KATR4,'Not Set'),
		dc.Attribute5 = ifnull(KNA1_KATR5,'Not Set'),
		dc.Attribute6 = ifnull(KNA1_KATR6,'Not Set'),
		dc.Attribute7 = ifnull(KNA1_KATR7,'Not Set'),
		dc.Attribute8 = ifnull(KNA1_KATR8,'Not Set'),
		dc.ConditionGroup1 = ifnull(KNA1_KDKG1,'Not Set'),
		dc.ConditionGroup2 = ifnull(KNA1_KDKG2,'Not Set'),
		dc.IndustryCode1 = ifnull(KNA1_BRAN1,'Not Set'),
		dc.Address = ifnull(KNA1_STRAS,'Not Set')
		,dc.custBillingBlock = ifnull(KNA1_FAKSD, 'Not Set') 	/* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
		,dc.dataLine = ifnull(KNA1_DATLT, 'Not Set')
		,dc.deliveryBlock = ifnull(KNA1_LIFSD, 'Not Set')
		,dc.deletionFlag = ifnull(KNA1_LOEVM, 'Not Set')
		,dc.faxNumber = ifnull(KNA1_TELFX, 'Not Set')
		,dc.languageKey = ifnull(KNA1_SPRAS, 'Not Set')
		,dc.orderBlock = ifnull(KNA1_AUFSD, 'Not Set')
		,dc.searchTerm = ifnull(KNA1_SORTL, 'Not Set')
		,dc.telephone1 = ifnull(KNA1_TELF1, 'Not Set')
		,dc.transportationZone = ifnull(KNA1_LZONE, 'Not Set')
		,dc.addressnumber = ifnull(KNA1_ADRNR,'Not Set') 	/* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
		,dc.dw_update_date = current_timestamp
		,dc.NielsenID = ifnull(KNA1_NIELS,'Not Set')
FROM dim_customer dc, kna1 kn, t005t
	WHERE    dc.CustomerNumber = kn.kna1_kunnr
	     AND dc.RowIsCurrent = 1
	     AND KNA1_LAND1 = T005T_LAND1;

update dim_customer dc
set dc.Industry = ifnull(T5UNT_NTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join T5UNT t1 on t1.T5UNT_NAICS = kn.KNA1_BRSCH
where dc.RowIsCurrent = 1 and
	  dc.Industry <> ifnull(T5UNT_NTEXT, 'Not Set');

update dim_customer dc
set dc.ClassificationDescription = ifnull(TKUKT_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TKUKT t1 on t1.TKUKT_KUKLA = KNA1_KUKLA
where dc.RowIsCurrent = 1 and
	  dc.ClassificationDescription <> ifnull(TKUKT_VTEXT, 'Not Set');

update dim_customer dc
set dc.CustAccountGroupDescription = ifnull(T077X_TXT30, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join T077X t1 on t1.T077X_KTOKD = KNA1_KTOKD
where dc.RowIsCurrent = 1 and
	  dc.CustAccountGroupDescription <> ifnull(T077X_TXT30, 'Not Set');

update dim_customer dc
set dc.RegionDescription = ifnull(T005U_BEZEI, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join T005U t1 on t1.T005U_LAND1 = KNA1_LAND1 AND T005U_BLAND = KNA1_REGIO
where dc.RowIsCurrent = 1 and
	  dc.RegionDescription <> ifnull(T005U_BEZEI, 'Not Set');

update dim_customer dc
set dc.RegionDescription = ifnull(T005U_BEZEI, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join T005U t1 on t1.T005U_LAND1 = KNA1_LAND1 AND T005U_BLAND = KNA1_REGIO
where dc.RowIsCurrent = 1 and
	  dc.RegionDescription <> ifnull(T005U_BEZEI, 'Not Set');

update dim_customer dc
set dc.ChannelDescription = ifnull(TVK1T_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TVK1T t1 on t1.TVK1T_KATR1 = KNA1_KATR1
where dc.RowIsCurrent = 1 and
	  dc.ChannelDescription <> ifnull(TVK1T_VTEXT, 'Not Set');

update dim_customer dc
set dc.SubChannelDescription = ifnull(TVK2T_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TVK2T t1 on t1.TVK2T_KATR2 = KNA1_KATR2
where dc.RowIsCurrent = 1 and
	  dc.SubChannelDescription <> ifnull(TVK2T_VTEXT, 'Not Set');

update dim_customer dc
set dc.ConditionGroupDescription2 = ifnull(TVKGGT_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TVKGGT t1 on t1.TVKGGT_KDKGR = kn.KNA1_KDKG2
where dc.RowIsCurrent = 1 and
	  dc.ConditionGroupDescription2 <> ifnull(TVKGGT_VTEXT, 'Not Set');

update dim_customer dc
set dc.IndustryCode1Description = ifnull(TBRCT_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TBRCT t1 on t1.TBRCT_BRACO = kn.KNA1_BRAN1
where dc.RowIsCurrent = 1 and
	  dc.IndustryCode1Description <> ifnull(TBRCT_VTEXT, 'Not Set');

merge into dim_customer dim
using(select dc.dim_customerid, ifnull(max(KNB1_ZWELS), 'Not Set') paymethod
	  FROM dim_customer dc
				inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
				inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
				left join kna1_knb1 t1 on t1.KNA1_KUNNR = kn.kna1_kunnr
	  where dc.RowIsCurrent = 1
	  group by dc.dim_customerid) src on dim.dim_customerid = src.dim_customerid
when matched then update set dim.paymethod = src.paymethod
where dim.paymethod <> src.paymethod;

merge into dim_customer dim
using(select dc.dim_customerid, ifnull(t1.T042Z_TEXT1, 'Not Set') paymethoddescription
	  FROM dim_customer dc
				inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
				inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
				left join (select x.KNA1_KUNNR, t2.T042Z_TEXT1
						   from kna1_knb1 x
									inner join T042Z t2 on x.KNB1_ZWELS = t2.T042Z_ZLSCH) t1 on t1.KNA1_KUNNR = kn.kna1_kunnr
	  where dc.RowIsCurrent = 1) src on dim.dim_customerid = src.dim_customerid
when matched then update set dim.paymethoddescription = src.paymethoddescription
where dim.paymethoddescription <> src.paymethoddescription;

update dim_customer dc
set dc.brandDescription = ifnull(TVK6T_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TVK6T t1 on t1.TVK6T_KATR6 = KNA1_KATR6
where dc.RowIsCurrent = 1 and
	  dc.brandDescription <> ifnull(TVK6T_VTEXT, 'Not Set');

update dim_customer dc
set dc.transportationZoneDesc = ifnull(TZONT_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TZONE_TZONT t1 on t1.TZONE_ZONE1 = KNA1_LZONE
where dc.RowIsCurrent = 1 and
	  dc.transportationZoneDesc <> ifnull(TZONT_VTEXT, 'Not Set');

/* Begin of Customer Transfer Interface - Insert*/
drop table if exists tmp_cust_ins_t1;
create table tmp_cust_ins_t1 as
select  KNA1_KUNNR CustomerNumber,
		ifnull(KNA1_BRSCH, 'Not Set') IndustryCode,
		convert(VARCHAR(200), 'Not Set') as Industry, 						-- ifnull((SELECT T5UNT_NTEXT FROM T5UNT WHERE T5UNT_NAICS = KNA1_BRSCH ),'Not Set') Industry,
		ifnull(KNA1_LAND1, 'Not Set') Country,
		ifnull(KNA1_NAME1, 'Not Set') Name,
		ifnull(KNA1_NAME2, 'Not Set') Name2,
		ifnull(KNA1_ORT01, 'Not Set') City,
		ifnull(KNA1_PSTLZ, 'Not Set') PostalCode,
		ifnull(KNA1_REGIO, 'Not Set') Region,
		CASE WHEN ifnull(KNA1_XCPDK, '') = 'X' THEN 'Yes' ELSE 'No' END OneTime,
		ifnull(KNA1_KUKLA,'Not Set') Classification,
		convert(VARCHAR(200), 'Not Set') as ClassificationDescription, 		-- ifnull((SELECT t.TKUKT_VTEXT FROM TKUKT t WHERE t.TKUKT_KUKLA = KNA1_KUKLA), 'Not Set') ClassificationDescription,
		1 RowIsCurrent,
		current_timestamp RowStartDate,
		KNA1_ERDAT CreationDate,
		ifnull(KNA1_KTOKD,'Not Set') CustAccountGroup,
		convert(VARCHAR(200), 'Not Set') as CustAccountGroupDescription,	-- ifnull((SELECT t.T077X_TXT30 FROM T077X t WHERE t.T077X_KTOKD = KNA1_KTOKD AND T.T077X_SPRAS = 'E'), 'Not Set') CustAccountGroupDescription,
		convert(VARCHAR(200), 'Not Set') as RegionDescription, 				--	ifnull((SELECT T005U_BEZEI FROM T005U WHERE T005U_LAND1 = KNA1_LAND1 AND T005U_BLAND = KNA1_REGIO),'Not Set') RegionDescription,
		ifnull(t005t_landx, 'Not Set') CountryName,
		ifnull(KNA1_KATR1, 'Not Set') Channel,
		ifnull(KNA1_KATR2, 'Not Set') SubChannel,
		convert(VARCHAR(200), 'Not Set') as ChannelDescription, 			-- ifnull((SELECT t.TVK1T_VTEXT FROM TVK1T t WHERE t.TVK1T_KATR1 = KNA1_KATR1),'Not Set') ChannelDescription,
		convert(VARCHAR(200), 'Not Set') as SubChannelDescription, 			-- ifnull((SELECT t.TVK2T_VTEXT FROM TVK2T t WHERE t.TVK2T_KATR2 = KNA1_KATR2),'Not Set') SubChannelDescription,
		ifnull(KNA1_VBUND,'Not Set') TradingPartner,
		ifnull(KNA1_KATR3,'Not Set') Attribute3,
		ifnull(KNA1_KATR4,'Not Set') Attribute4,
		ifnull(KNA1_KATR5,'Not Set') Attribute5,
		ifnull(KNA1_KATR6,'Not Set') Attribute6,
		ifnull(KNA1_KATR7,'Not Set') Attribute7,
		ifnull(KNA1_KATR8,'Not Set') Attribute8,
		ifnull(KNA1_KDKG1,'Not Set') ConditionGroup1,
		ifnull(KNA1_KDKG2,'Not Set') ConditionGroup2,
		ifnull(KNA1_BRAN1,'Not Set') IndustryCode1,
		convert(VARCHAR(200), 'Not Set') as ConditionGroupDescription2, 	-- ifnull((SELECT TVKGGT_VTEXT FROM TVKGGT where TVKGGT_KDKGR = KNA1_KDKG2),'Not Set') ConditionGroupDescription2,
		convert(VARCHAR(200), 'Not Set') as IndustryCode1Description, 		-- ifnull((SELECT TBRCT_VTEXT FROM TBRCT where TBRCT_BRACO = KNA1_BRAN1),'Not Set') IndustryCode1Description,
		ifnull(KNA1_STRAS,'Not Set') Address,
		convert(VARCHAR(200), 'Not Set') as PayMethod, 						-- ifnull((select KNB1_ZWELS from kna1_knb1 t1 where t1.KNA1_KUNNR = kna1_kunnr) ,'Not Set') as PayMethod,
		convert(VARCHAR(200), 'Not Set') as PayMethodDescription 			-- ifnull((select T042Z_TEXT1 from kna1_knb1 t1, T042Z t2 where t1.KNB1_ZWELS = t2.T042Z_ZLSCH and t1.KNA1_KUNNR = kna1_kunnr),'Not Set') as PayMethodDescription
		,ifnull(KNA1_FAKSD, 'Not Set') custBillingBlock /* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
		,ifnull(KNA1_DATLT, 'Not Set') dataLine
		,ifnull(KNA1_LIFSD, 'Not Set') deliveryBlock
		,ifnull(KNA1_LOEVM, 'Not Set') deletionFlag
		,ifnull(KNA1_TELFX, 'Not Set') faxNumber
		,ifnull(KNA1_SPRAS, 'Not Set') languageKey
		,ifnull(KNA1_AUFSD, 'Not Set') orderBlock
		,ifnull(KNA1_SORTL, 'Not Set') searchTerm
		,ifnull(KNA1_TELF1, 'Not Set') telephone1
		,ifnull(KNA1_LZONE, 'Not Set') transportationZone
		,convert(VARCHAR(200), 'Not Set') as brandDescription 				-- ifnull((SELECT t.TVK6T_VTEXT FROM TVK6T t	WHERE KNA1_KATR6 = t.TVK6T_KATR6 AND t.TVK6T_SPRAS='E'), 'Not Set') brandDescription
		,convert(VARCHAR(200), 'Not Set') as transportationZoneDesc			-- ifnull((SELECT TZONT_VTEXT FROM TZONE_TZONT t WHERE KNA1_LZONE = t.TZONE_ZONE1 AND t.TZONT_SPRAS='E'),'Not Set') transportationZoneDesc
		,ifnull(KNA1_ADRNR,'Not Set') addressnumber /* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
	FROM kna1 LEFT JOIN t005t on KNA1_LAND1 = T005T_LAND1
	WHERE NOT EXISTS (SELECT 1
	                FROM dim_customer dc
	               WHERE dc.CustomerNumber = kna1_kunnr);

update tmp_cust_ins_t1 kn
set kn.Industry = ifnull(T5UNT_NTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join T5UNT t1 on t1.T5UNT_NAICS = kn.IndustryCode
where kn.Industry <> ifnull(T5UNT_NTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.ClassificationDescription = ifnull(TKUKT_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TKUKT t1 on t1.TKUKT_KUKLA = kn.Classification
where kn.ClassificationDescription <> ifnull(TKUKT_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.CustAccountGroupDescription = ifnull(T077X_TXT30, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join T077X t1 on t1.T077X_KTOKD = kn.CustAccountGroup
where kn.CustAccountGroupDescription <> ifnull(T077X_TXT30, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.RegionDescription = ifnull(T005U_BEZEI, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join T005U t1 on t1.T005U_LAND1 = kn.Country AND T1.T005U_BLAND = Region
where kn.RegionDescription <> ifnull(T005U_BEZEI, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.ChannelDescription = ifnull(TVK1T_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TVK1T t1 on t1.TVK1T_KATR1 = kn.Channel
where kn.ChannelDescription <> ifnull(TVK1T_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.SubChannelDescription = ifnull(TVK2T_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TVK2T t1 on t1.TVK2T_KATR2 = kn.SubChannel
where kn.SubChannelDescription <> ifnull(TVK2T_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.ConditionGroupDescription2 = ifnull(TVKGGT_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TVKGGT t1 on t1.TVKGGT_KDKGR = kn.ConditionGroup2
where kn.ConditionGroupDescription2 <> ifnull(TVKGGT_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.IndustryCode1Description = ifnull(TBRCT_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TBRCT t1 on t1.TBRCT_BRACO = kn.IndustryCode1
where kn.IndustryCode1Description <> ifnull(TBRCT_VTEXT, 'Not Set');

merge into tmp_cust_ins_t1 dim
using(select kn.rowid rid, ifnull(max(KNB1_ZWELS), 'Not Set') paymethod
	  FROM tmp_cust_ins_t1 kn
				left join kna1_knb1 t1 on t1.KNA1_KUNNR = kn.CustomerNumber
	  group by kn.rowid) src on dim.rowid = src.rid
when matched then update set dim.paymethod = src.paymethod
where dim.paymethod <> src.paymethod;

merge into tmp_cust_ins_t1 dim
using(select kn.rowid rid, ifnull(t1.T042Z_TEXT1, 'Not Set') paymethoddescription
	  FROM tmp_cust_ins_t1 kn
				left join (select x.KNA1_KUNNR, t2.T042Z_TEXT1
						   from kna1_knb1 x
									inner join T042Z t2 on x.KNB1_ZWELS = t2.T042Z_ZLSCH) t1 on t1.KNA1_KUNNR = kn.CustomerNumber
	  ) src on dim.rowid = src.rid
when matched then update set dim.paymethoddescription = src.paymethoddescription
where dim.paymethoddescription <> src.paymethoddescription;

update tmp_cust_ins_t1 kn
set kn.brandDescription = ifnull(TVK6T_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TVK6T t1 on t1.TVK6T_KATR6 = kn.Attribute6
where kn.brandDescription <> ifnull(TVK6T_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.transportationZoneDesc = ifnull(TZONT_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TZONE_TZONT t1 on t1.TZONE_ZONE1 = kn.transportationZone
where kn.transportationZoneDesc <> ifnull(TZONT_VTEXT, 'Not Set');


delete from number_fountain m where m.table_name = 'dim_customer';

insert into number_fountain
select 	'dim_customer',
	ifnull(max(d.Dim_CustomerID),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customer d
where d.Dim_CustomerID <> 1;

INSERT INTO dim_customer(Dim_CustomerID,
                         CustomerNumber,
                         IndustryCode,
                         Industry,
                         Country,
                         Name,
						Name2,
                         City,
                         PostalCode,
                         Region,
                         OneTime,
						Classification,
						ClassificationDescription,
                         RowIsCurrent,
                         RowStartDate,
						CreationDate,
						CustAccountGroup,
						CustAccountGroupDescription,
                        RegionDescription,
						CountryName,
						Channel,
						SubChannel,
						ChannelDescription,
						SubChannelDescription,
						TradingPartner,
						Attribute3,
						Attribute4,
						Attribute5,
						Attribute6,
						Attribute7,
						Attribute8,
						ConditionGroup1,
						ConditionGroup2,
						IndustryCode1,
						ConditionGroupDescription2,
						IndustryCode1Description,
						Address,
						PayMethod,
						PayMethodDescription
						,custBillingBlock /* Start New Fields added as a part of Columbia 856 Report, Dec 2014*/
						,dataLine
						,deliveryBlock
						,deletionFlag
						,faxNumber
						,languageKey
						,orderBlock
						,searchTerm
						,telephone1
						,transportationZone
						,brandDescription
						,transportationZoneDesc
						,addressnumber /* End New Fields added as a part of Columbia 856 Report, Dec 2014*/
		)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customer') + row_number() over(order by '') Dim_CustomerID,
	   a.*
from (
		SELECT distinct CustomerNumber,
					IndustryCode,
					Industry,
					Country,
					Name,
					Name2,
					City,
					PostalCode,
					Region,
					OneTime,
					Classification,
					ClassificationDescription,
					RowIsCurrent,
					RowStartDate,
					CreationDate,
					CustAccountGroup,
					CustAccountGroupDescription,
					RegionDescription,
					CountryName,
					Channel,
					SubChannel,
					ChannelDescription,
					SubChannelDescription,
					TradingPartner,
					Attribute3,
					Attribute4,
					Attribute5,
					Attribute6,
					Attribute7,
					Attribute8,
					ConditionGroup1,
					ConditionGroup2,
					IndustryCode1,
					ConditionGroupDescription2,
					IndustryCode1Description,
					Address,
					PayMethod,
					PayMethodDescription
					,custBillingBlock
					,dataLine
					,deliveryBlock
					,deletionFlag
					,faxNumber
					,languageKey
					,orderBlock
					,searchTerm
					,telephone1
					,transportationZone
					,brandDescription
					,transportationZoneDesc
					,addressnumber
		from tmp_cust_ins_t1) a
	WHERE NOT EXISTS (SELECT 1
	                FROM dim_customer dc
	               WHERE dc.CustomerNumber = a.CustomerNumber);

delete from number_fountain m where m.table_name = 'dim_customer';


/* Begin of Populate Customer Hierarchy Interface*/

/* ##################################################################################################################
   Script         : bi_populate_customer_hierarchy
#################################################################################################################### */

Drop table if exists pCustHierType_701;

create table pCustHierType_701 As
Select convert(varchar(5),
			   ifnull((SELECT property_value
                    FROM systemproperty
                    WHERE property = 'customer.hierarchy.type'),
                  'A')) pCustomerHierarchyType;

Drop table if exists custhierarchy_tmp;

Create table custhierarchy_tmp
as Select HierarchyType as dd_HierarchyType,
		  CustomerNumber,
		  HighCustomerNumber,
		  HigherCustomerNumber,
		  TopLevelCustomerNumber,
		  HighCustomerDesc,
		  HigherCustomerDesc,
		  TopLevelCustomerDesc
from dim_customer where 1=2;

INSERT INTO custhierarchy_tmp(dd_HierarchyType,
                              CustomerNumber,
                              HighCustomerNumber,
                              HigherCustomerNumber,
                              TopLevelCustomerNumber,
                              HighCustomerDesc,
                              HigherCustomerDesc,
                              TopLevelCustomerDesc)
			      	/* EndDate, StartDate) Not in USE */
   SELECT ifnull(KNVH_HITYP,'Not Set'),
          ifnull(KNVH_KUNNR,'Not Set'),
          max(ifnull(KNVH_HKUNNR,'Not Set')),
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set'
           	/* KNVH_DATBI, KNVH_DATAB Not in USE */
     FROM KNVH
 WHERE  current_date BETWEEN KNVH_DATAB AND KNVH_DATBI
 group by KNVH_HITYP,KNVH_KUNNR;


DROP TABLE IF EXISTS TMP_CUSTHIERARCHY_UPDATE;
CREATE TABLE TMP_CUSTHIERARCHY_UPDATE
AS SELECT MAX(HighCustomerNumber) HighCustomerNumber,
          dd_HierarchyType,
          CustomerNumber
FROM custhierarchy_tmp
GROUP BY
          dd_HierarchyType,
          CustomerNumber;

UPDATE    custhierarchy_tmp ct
 SET ct.HigherCustomerNumber = ctPar.HighCustomerNumber
 FROM      custhierarchy_tmp ct,
           TMP_CUSTHIERARCHY_UPDATE ctPar
 WHERE     ctPar.CustomerNumber = ct.HighCustomerNumber
          AND ctPar.dd_HierarchyType = ct.dd_HierarchyType
          AND  ct.HigherCustomerNumber = 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ctPar.HighCustomerNumber
FROM custhierarchy_tmp ct,
           TMP_CUSTHIERARCHY_UPDATE ctPar
 WHERE ctPar.CustomerNumber = ct.HigherCustomerNumber
          AND ctPar.dd_HierarchyType = ct.dd_HierarchyType
          AND ct.TopLevelCustomerNumber = 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.HigherCustomerNumber,
       ct.HigherCustomerNumber = ct.HighCustomerNumber,
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber <> 'Not Set'
    and ct.HighCustomerNumber <> 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.HighCustomerNumber,
       ct.HigherCustomerNumber = 'Not Set',
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber = 'Not Set'
    and ct.HighCustomerNumber <> 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.CustomerNumber,
       ct.HigherCustomerNumber = 'Not Set',
       ct.HighCustomerNumber = 'Not Set'
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber = 'Not Set'
    and ct.HighCustomerNumber = 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerDesc = c.Name
FROM custhierarchy_tmp ct,
          dim_customer c
 WHERE ct.TopLevelCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

UPDATE    custhierarchy_tmp ct
   SET ct.HigherCustomerDesc = c.Name
FROM custhierarchy_tmp ct,
          dim_customer c
 WHERE ct.HigherCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

 UPDATE    custhierarchy_tmp ct
   SET ct.HighCustomerDesc = c.Name
 FROM custhierarchy_tmp ct,
          dim_customer c
 WHERE ct.HighCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerDesc = 'Not Set'
 WHERE ct.TopLevelCustomerDesc IS NULL;

UPDATE    custhierarchy_tmp ct
   SET ct.HigherCustomerDesc = 'Not Set'
 WHERE ct.HigherCustomerDesc IS NULL;

 UPDATE    custhierarchy_tmp ct
   SET ct.HighCustomerDesc = 'Not Set'
 WHERE ct.HighCustomerDesc IS NULL;

 UPDATE dim_customer c
  SET c.HighCustomerNumber = ct.HighCustomerNumber,
      c.HierarchyType = ct.dd_HierarchyType,
      c.HigherCustomerNumber = ct.HigherCustomerNumber,
      c.TopLevelCustomerNumber = ct.TopLevelCustomerNumber,
      c.HighCustomerDesc = ct.HighCustomerDesc,
      c.HigherCustomerDesc = ct.HigherCustomerDesc,
      c.TopLevelCustomerDesc = ct.TopLevelCustomerDesc,
			c.dw_update_date = current_timestamp
 FROM dim_customer c,
         custhierarchy_tmp ct, pCustHierType_701 p
 WHERE ct.CustomerNumber = c.CustomerNumber
       AND ct.dd_HierarchyType = p.pCustomerHierarchyType;

/* BI-3692 KNKA add credit limit - Oana 12Sept2016*/
UPDATE dim_customer c
		SET creditlimitcurr = ifnull(KNKA_WAERS,'Not Set')
FROM dim_customer c, KNKA
WHERE c.CUSTOMERNUMBER = KNKA_KUNNR;

UPDATE dim_customer c
	SET creditlimittotal = KNKA_KLIMG
FROM dim_customer c, KNKA
WHERE c.CUSTOMERNUMBER = KNKA_KUNNR;

UPDATE dim_customer c
	SET creditlimittotal_eur = ifnull(CASE WHEN KNKA_WAERS = 'EUR' THEN KNKA_KLIMG ELSE (KNKA_KLIMG * csvo.exrate) END,0)
FROM dim_customer c
	inner join KNKA on c.CUSTOMERNUMBER = KNKA_KUNNR
	left join csv_oprate csvo on KNKA_WAERS = csvo.FROMC;

UPDATE dim_customer c
	SET creditlimitindividual = KNKA_KLIME
FROM dim_customer c, KNKA
WHERE c.CUSTOMERNUMBER = KNKA_KUNNR;

UPDATE dim_customer c
	SET creditlimitindividual_eur = ifnull(CASE WHEN KNKA_WAERS = 'EUR' THEN KNKA_KLIME ELSE (KNKA_KLIME * csvo.exrate) END,0)
FROM dim_customer c
	inner join KNKA on c.CUSTOMERNUMBER = KNKA_KUNNR
	left join csv_oprate csvo on csvo.FROMC = KNKA_WAERS;
/* END BI-3692 KNKA add credit limit - Oana 12Sept2016*/

truncate table custhierarchy_tmp;
DROP TABLE IF EXISTS TMP_CUSTHIERARCHY_UPDATE;
	/* VW Original: call vectorwise ( combine 'custhierarchy_tmp-custhierarchy_tmp') */

/* Import from csv */
update dim_customer cm
SET CUSTOMERGROUP = 'Not Set';

drop table if exists tmp_csv_customergroup;
CREATE TABLE  tmp_csv_customergroup
AS SELECT  CUSTOMERNR , max(CUSTOMERGROUP) CUSTOMERGROUP FROM EMD586.csv_customergroup
GROUP by CUSTOMERNR;


UPDATE dim_customer cm
SET cm.CUSTOMERGROUP = csvc.CUSTOMERGROUP
FROM tmp_csv_customergroup csvc, dim_customer cm
WHERE trim(leading '0' from cm.CustomerNumber) = trim(leading '0' from csvc.CUSTOMERNR);

UPDATE dim_customer
set city=upper(city);

/* Expression test  */

update dim_Customer
set CustomerNumber2 = TRIM(LEADING '0' FROM CustomerNumber)
where CustomerNumber2 <> TRIM(LEADING '0' FROM CustomerNumber);

update dim_Customer
set ADDRESS2 = TRIM(LEADING '0' FROM CustomerNumber)||' - '||Name
where ADDRESS2 <> TRIM(LEADING '0' FROM CustomerNumber)||' - '||Name;

update dim_Customer
set CREDITLIMITINDIVIDUAL2 = to_char(CREDITLIMITINDIVIDUAL,'9,999,999,999,990.00')
where CREDITLIMITINDIVIDUAL2 <> to_char(CREDITLIMITINDIVIDUAL,'9,999,999,999,990.00');

update dim_Customer
set HighCustomerNumber2 = TRIM(LEADING '0' FROM HighCustomerNumber)
where HighCustomerNumber2 <> TRIM(LEADING '0' FROM HighCustomerNumber);

update dim_Customer
set CREDITLIMITTOTAL2 = to_char(CREDITLIMITTOTAL,'9,999,999,999,990.00')
where CREDITLIMITTOTAL2 <> to_char(CREDITLIMITTOTAL,'9,999,999,999,990.00');

update dim_Customer
set HigherCustomerNumber2 = TRIM(LEADING '0' FROM HigherCustomerNumber)
where HigherCustomerNumber2 <> TRIM(LEADING '0' FROM HigherCustomerNumber);

update dim_Customer
set TopLevelCustomerNumber2 = TRIM(LEADING '0' FROM TopLevelCustomerNumber)
where TopLevelCustomerNumber2 <> TRIM(LEADING '0' FROM TopLevelCustomerNumber);

update dim_Customer
set CREDITLIMITTOTAL_EUR2 = to_char(CREDITLIMITTOTAL_EUR,'9,999,999,999,990.00')
where CREDITLIMITTOTAL_EUR2 <> to_char(CREDITLIMITTOTAL_EUR,'9,999,999,999,990.00');

/*
update dim_customer
set gsa=sold_to_gsa
from dim_customer,EMDTempoCC4.customer_gsa_mapping
where customernumber2=sold_to_party
and platform='SIAL'
and gsa<>sold_to_gsa*/


MERGE INTO dim_customer D
USING
(
SELECT customernumber2,MAX(sold_to_gsa) AS GSA
FROM dim_customer,EMDTempoCC4.customer_gsa_mapping
where customernumber2=sold_to_party
and platform='SIAL'
and gsa<>sold_to_gsa
GROUP BY customernumber2
) X
ON
(X.customernumber2  = D.customernumber2)
WHEN MATCHED THEN
UPDATE SET D.GSA = X.GSA
WHERE D.GSA <> X.GSA;

update dim_customer c
set c.gsa_all_products=cc.gsa_all_products
from dim_customer c,EMDTempoCC4.GSA_MAPPING_NEW_FILED cc
where c.customernumber2=cc.customer_number
and c.gsa_all_products<>cc.gsa_all_products;


update dim_customer c
set c.country_region=cc.region
from dim_customer c,EMDTempoCC4.countr_mapping_20171212 cc
where c.country=cc.country
and c.country_region<>cc.region;
