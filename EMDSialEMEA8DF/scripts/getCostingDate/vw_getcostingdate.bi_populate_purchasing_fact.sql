/* START OF CODE BLOCKS - tmp_getcostingdate would have the final the data    */

DELETE FROM tmp_getcostingdate
WHERE fact_script_name = 'bi_populate_purchasing_fact';

drop table if exists ekko_ekpo_eket_jj890;

create table ekko_ekpo_eket_jj890 as 
select min(x.EKET_BEDAT) EKET_BEDAT, x.EKPO_EBELN, x.EKPO_EBELP
from ekko_ekpo_eket x
group by x.EKPO_EBELN, x.EKPO_EBELP;

INSERT INTO tmp_getcostingdate
(pCompanyCode,
pPlant,             
pMaterialNo,            
pFiYear,     
pPeriod, 
fact_script_name
)       
SELECT DISTINCT EKPO_BUKRS pCompanyCode, EKPO_WERKS pPlant, EKPO_MATNR pMaterialNo, dt.FinancialYear pFiYear, dt.FinancialMonthNumber pPeriod, 
	 'bi_populate_purchasing_fact' fact_script_name
FROM fact_purchase fp
		inner join (select t2.EKET_BEDAT min_EKET_BEDAT, t1.*
					from ekko_ekpo_eket t1
							inner join ekko_ekpo_eket_jj890 t2 on    t1.EKPO_EBELN = t2.EKPO_EBELN
															     and t1.EKPO_EBELP = t2.EKPO_EBELP
					) ds on    fp.dd_DocumentNo = ds.EKPO_EBELN
						   AND fp.dd_DocumentItemNo = ds.EKPO_EBELP
						   AND fp.dd_ScheduleNo = ds.EKET_ETENR
		inner join dim_date dt on    dt.DateValue   = ds.min_EKET_BEDAT
								 and dt.CompanyCode = ds.EKPO_BUKRS
WHERE fp.amt_StdUnitPrice = 0;

drop table if exists ekko_ekpo_eket_jj890;

