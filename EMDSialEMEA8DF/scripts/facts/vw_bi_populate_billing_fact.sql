/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 20 May 2013 */
/*   Description    : Stored Proc bi_populate_billing_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc     
/*   13 Aug 2015     Liviu Turcu 2.5             Populate Dim_BillingMiscId ( BI-1083 ) */
/*	18 May 2015       Liviu I     1.8                              Add Billing No */
/*	 25 Feb 2015      Octavian  1.7  			  Adding Material Price Group 2 */
/*	 17 Feb 2015	  Octavian  1.6				  Adding Sales Document Category from VBAK_VBTYP     */
/*   22 Dec 2014      Octavian  1.54              Added dim_CustomerConditionGroups4id,dim_CustomerConditionGroups5id */
/*   29 Apr 2014	Lokesh	1.6     Optimized queries */
/*   27 Mar 2014      Cornelia          Added: dd_conditionno */
/*   24 Feb 2014      Cornelia          Added: Dim_partsalesid */
/*   14 Feb 2014      George    1.5     Added: Dim_CustomerGroup4id */ 
/*   03 Feb 2014      George    1.4     Added: dim_materialpricegroup4id, dim_materialpricegroup5id,dim_CustomerConditionGroups1id,dim_CustomerConditionGroups2id,dim_CustomerConditionGroups3id */
/*   21 Nov 2013      Cornelia  1.4               Enable Material Pricing Group ->Dim_MaterialPricingGroupId				  */
/*   09 Sep 2013      Lokesh	1.3		  Currency and Exchange rate changes. Store tran amts and store Tran->loc and tran->gbl rates */
/*   31 May 2013      Lokesh    1.2       Converted multi-column updates to single-column 						  */
/*   23 May 2013      Lokesh	1.1		  Merged Shanthi's changes from vw_bi_populate_billing_sales_shipment_attributes.sql  */
/*   20 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/



/* ALTER TABLE fact_billing DISABLE KEYS */
/* SET FOREIGN_KEY_CHECKS = 0 */
DROP TABLE IF EXISTS tmp_pGlobalCurrency_fact_billing;
CREATE TABLE tmp_pGlobalCurrency_fact_billing ( pGlobalCurrency CHAR(3) NULL);

INSERT INTO tmp_pGlobalCurrency_fact_billing VALUES ( 'USD' );

UPDATE tmp_pGlobalCurrency_fact_billing tfb 
   SET tfb.pGlobalCurrency = ifnull(sys.property_value, 'USD')
  FROM systemproperty sys, tmp_pGlobalCurrency_fact_billing tfb
 WHERE sys.property = 'customer.global.currency';

/* Insert 1 */

DROP TABLE IF EXISTS tmp_db_fb1;
CREATE TABLE tmp_db_fb1
AS
SELECT pc.ProfitCenterCode,pc.ControllingArea,VBRK_FKDAT,min(pc.ValidTo) as min_ValidTo
  FROM dim_profitcenter pc,VBRK_VBRP v
 WHERE pc.ProfitCenterCode = VBRP_PRCTR
   AND pc.ControllingArea = VBRP_KOKRS
   AND pc.ValidTo >= VBRK_FKDAT
   AND pc.RowIsCurrent = 1
 GROUP BY pc.ProfitCenterCode,pc.ControllingArea,VBRK_FKDAT;

DROP TABLE IF EXISTS dim_profitcenter_fact_billing;
CREATE TABLE dim_profitcenter_fact_billing
AS
SELECT * 
  FROM dim_profitcenter 
 ORDER BY ValidTo ASC;

DROP TABLE IF EXISTS tmp_db_fb2;
CREATE TABLE tmp_db_fb2
AS
SELECT pc.*,t.VBRK_FKDAT,t.min_ValidTo
  FROM dim_profitcenter_fact_billing pc,tmp_db_fb1 t
 WHERE pc.ProfitCenterCode = t.ProfitCenterCode
   AND pc.ControllingArea = t.ControllingArea
   AND pc.ValidTo = t.min_ValidTo;

DELETE FROM NUMBER_FOUNTAIN 
 WHERE table_name = 'fact_billing';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_billing',
       ifnull(max(fact_billingid),
       ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
  FROM fact_billing;

DROP TABLE IF EXISTS tmp_ins_fact_billing;
CREATE TABLE tmp_ins_fact_billing
AS
SELECT DISTINCT vbk.VBRK_VBELN as dd_billing_no,vbk.VBRP_POSNR as dd_billing_item_no,
       cast(1 as bigint) dim_companyid,cast(1 as bigint) dim_plantid , 'Not Set' currency
  FROM VBRK_VBRP vbk;

DROP TABLE IF EXISTS tmp_del_fact_billing;
CREATE TABLE tmp_del_fact_billing
AS
SELECT * FROM tmp_ins_fact_billing where 1 = 2;

INSERT INTO tmp_del_fact_billing
SELECT DISTINCT b.dd_billing_no, b.dd_billing_item_no,1,1,'Not Set'
  FROM fact_billing b;

MERGE INTO tmp_ins_fact_billing i 
USING tmp_del_fact_billing d
   ON i.dd_billing_no = d.dd_billing_no
  AND i.dd_billing_item_no = d.dd_billing_item_no
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_del_fact_billing;

UPDATE fact_billing fb
   SET fb.Dim_Companyid = IFNULL(dc.Dim_Companyid, 1)
  FROM dim_company dc,VBRK_VBRP vkp, fact_billing fb
 WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
   AND fb.dd_billing_no = vkp.VBRK_VBELN 
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND ifnull(fb.Dim_Companyid,-1) = IFNULL(dc.Dim_Companyid, 1);  

UPDATE tmp_ins_fact_billing b
   SET b.currency = IFNULL(c.currency, 'Not Set')
  FROM dim_company c,VBRK_VBRP,tmp_ins_fact_billing b
 WHERE b.dd_billing_no = VBRK_VBELN 
   AND b.dd_billing_item_no = VBRP_POSNR
   AND c.companycode = VBRK_BUKRS AND c.RowIsCurrent = 1
   AND ifnull(b.Dim_Companyid,-1) <> ifnull(c.dim_companyid,-2);

UPDATE tmp_ins_fact_billing b
   SET b.dim_plantid = IFNULL(dp.dim_plantid, 1)
  FROM dim_plant dp, VBRK_VBRP,tmp_ins_fact_billing b
 WHERE b.dd_billing_no = VBRK_VBELN 
   AND b.dd_billing_item_no = VBRP_POSNR
   AND dp.PlantCode = VBRP_WERKS AND dp.RowIsCurrent = 1;

INSERT INTO fact_billing(ct_BillingQtySalesUOM,
                         amt_ExchangeRate,
                         amt_ExchangeRate_GBL,
                         amt_CashDiscount,
                         ct_BillingQtyStockUOM,
                         dd_billing_no,
                         dd_billing_item_no,
                         dd_CancelledDocumentNo,
                         dd_SalesDlvrItemNo,
                         dd_SalesDlvrDocNo,
                         dd_fiscalyear,
                         dd_postingperiod,
                         Dim_AccountingTransferStatusid,
                         Dim_Companyid,
                         Dim_ConditionProcedureid,
                         Dim_ControllingAreaid,
                         Dim_CreditControlAreaid,
                         Dim_Currencyid,
                         Dim_CustomerGroup1id,
                         Dim_CustomerID,
                         Dim_CustomerPaymentTermsid,
                         Dim_DateidBilling,
                         Dim_DateidCreated,
                         Dim_DistributionChannelId,
                         Dim_DocumentCategoryid,
                         Dim_DocumentTypeid,
                         Dim_IncoTermid,
                         Dim_MaterialGroupid,
                         Dim_Partid,
                         Dim_Plantid,
                         Dim_ProductHierarchyid,
                         Dim_ProfitCenterid,
                         Dim_SalesDivisionid,
                         Dim_SalesGroupid,
                         Dim_SalesOfficeid,
                         Dim_SalesOrgid,
                         Dim_ShipReceivePointid,
                         Dim_StorageLocationid,
                         Dim_UnitOfMeasureId,
                         Dim_SalesOrderItemCategoryid,
                         dd_salesdocno,
                         dd_salesitemno,
                         Dim_BillingCategoryId,
                         Dim_RevenueRecognitionCategoryId,
                         amt_cost_InDocCurrency,
                         amt_NetValueHeader_InDocCurrency,
                         amt_NetValueItem_InDocCurrency,
                         amt_CustomerConfigSubtotal1_InDocCurrency,
                         amt_CustomerConfigSubtotal2_InDocCurrency,
                         amt_CustomerConfigSubtotal3_InDocCurrency,
                         amt_CustomerConfigSubtotal4_InDocCurrency,
                         amt_CustomerConfigSubtotal5_InDocCurrency,
                         amt_CustomerConfigSubtotal6_InDocCurrency,
                         dd_CustomerPONumber,
                         dd_CreatedBy,
						 fact_billingid,
                         Dim_Currencyid_TRA,
                         Dim_Currencyid_GBL,
                         Dim_MaterialPricingGroupId,
                         dim_partsalesid)
SELECT   ifnull((vkp.VBRP_FKLMG * vkp.VBRP_UMVKN / vkp.VBRP_UMVKZ), 0) AS ct_BillingQtySalesUOM,
          1 AS amt_ExchangeRate,
          1 AS amt_ExchangeRate_GBL ,
          ifnull(vkp.VBRP_SKFBP, 0) AS amt_CashDiscount,
          ifnull(vkp.VBRP_FKLMG, 0) AS ct_BillingQtyStockUOM,
          vkp.VBRK_VBELN AS dd_billing_no,
          vkp.VBRP_POSNR AS dd_billing_item_no,
          ifnull(vkp.VBRK_SFAKN,'Not Set') AS dd_CancelledDocumentNo,
          ifnull(vkp.VBRP_VGPOS, 0) AS dd_SalesDlvrItemNo,
          ifnull(vkp.VBRP_VGBEL, 'Not Set') AS dd_SalesDlvrDocNo,
          vkp.VBRK_GJAHR AS dd_fiscalyear,
          vkp.VBRK_POPER AS dd_postingperiod,
          1 AS Dim_AccountingTransferStatusid,
          1 AS Dim_Companyid,
          1 AS Dim_ConditionProcedureid,
          1 AS Dim_ControllingAreaid,
          1 AS Dim_CreditControlAreaid,
          1 AS Dim_Currencyid,
          1 AS Dim_CustomerGroup1id,
          1 AS Dim_CustomerID,
          1 AS Dim_CustomerPaymentTermsid,
          1 AS Dim_DateidBilling,
          1 AS Dim_DateIdCreated,
          1 AS Dim_DistributionChannelId,
          1 AS Dim_DocumentCategoryid,
          1 AS Dim_DocumentTypeid,
          1 AS Dim_IncoTermid,
          1 AS Dim_MaterialGroupid,
          1 AS Dim_Partid,
          ifnull(dp.dim_Plantid,1) dim_Plantid,
          1 AS dim_producthierarchyid,
          1 AS Dim_ProfitCenterid,
          1 AS Dim_SalesDivisionid,
          1 AS Dim_SalesGroupid,
          1 AS Dim_SalesOfficeid,
          1 AS Dim_SalesOrgid,
          1 AS Dim_ShipReceivePointid,
          1 AS Dim_StorageLocationid,
          1 AS Dim_UnitOfMeasureId,
          1 AS Dim_SalesOrderItemCategoryid,
          ifnull(vkp.VBRP_AUBEL, 'Not Set') AS dd_salesdocno,
          ifnull(vkp.VBRP_AUPOS, 0) AS dd_salesitemno,
          1 AS Dim_BillingCategoryId ,
          1 AS Dim_RevenueRecognitionCategoryId,
          ifnull(vkp.VBRP_WAVWR, 0) AS amt_cost_InDocCurrency,
          ifnull(vkp.VBRK_NETWR, 0) AS amt_NetValueHeader_InDocCurrency,
          (CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_NETWR AS amt_NetValueItem_InDocCurrency,
          (CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1 AS amt_CustomerConfigSubtotal1_InDocCurrency,
          (CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2 AS amt_CustomerConfigSubtotal2_InDocCurrency,
          (CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3 AS amt_CustomerConfigSubtotal3_InDocCurrency,
          (CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4 AS amt_CustomerConfigSubtotal4_InDocCurrency,
          (CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5 AS amt_CustomerConfigSubtotal5_InDocCurrency,
          (CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6 AS amt_CustomerConfigSubtotal6_InDocCurrency,
          ifnull(vkp.VBRK_BSTNK_VF,'Not Set') AS dd_CustomerPONumber,
          ifnull(vkp.VBRK_ERNAM,'Not Set') AS dd_CreatedBy,
          (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_billing') + row_number() over (ORDER BY ''),
          1 AS dim_currencyid_TRA,
          1 AS dim_currencyid_GBL,
          1 AS Dim_MaterialPricingGroupId,
          1 AS Dim_PartSalesId
 FROM VBRK_VBRP vkp
INNER JOIN dim_plant dp ON dp.PlantCode = VBRP_WERKS AND dp.RowIsCurrent = 1
INNER JOIN dim_company dc ON dc.CompanyCode = VBRK_BUKRS,
         tmp_pGlobalCurrency_fact_billing,tmp_ins_fact_billing b
WHERE b.dd_billing_no = VBRK_VBELN
  AND b.dd_billing_item_no = VBRP_POSNR;

UPDATE NUMBER_FOUNTAIN 
   SET max_id = ( select ifnull(max(fact_billingid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) from fact_billing)
 WHERE table_name = 'fact_billing';

/* Create a combined lookup table so that same joins are not required to be repeated in similar queries */
DROP TABLE IF EXISTS TMP_BILLING_COMBINED_UPD;

CREATE TABLE TMP_BILLING_COMBINED_UPD
AS
SELECT * FROM VBRK_VBRP vkp
 WHERE EXISTS ( select 1 from fact_billing fb where fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR );


UPDATE fact_billing fb
   SET fb.ct_BillingQtySalesUOM = ifnull(vkp.VBRP_FKLMG * (vkp.VBRP_UMVKN / vkp.VBRP_UMVKZ),0),
       fb.ct_BillingQtyStockUOM = ifnull(vkp.VBRP_FKLMG,0)
  FROM VBRK_VBRP vkp,fact_billing fb
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN 
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND ifnull(ct_BillingQtySalesUOM,-1) <> ifnull(VBRP_FKLMG * (VBRP_UMVKN / VBRP_UMVKZ),0);

/* Update currency IDs */
UPDATE fact_billing fb
   SET fb.dim_CurrencyId = ifnull(cur.dim_CurrencyID,1)
  FROM dim_company dc,dim_currency cur,VBRK_VBRP vkp,fact_billing fb			  
 WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
   AND cur.CurrencyCode = dc.currency
   AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND ifnull(fb.dim_CurrencyId,-1) <> ifnull(cur.dim_CurrencyID,-2);

UPDATE fact_billing fb
   SET fb.dim_CurrencyId_TRA = ifnull(tra.dim_currencyid, 1)
  FROM VBRK_VBRP vkp,dim_currency tra,fact_billing fb
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND tra.currencycode = VBRK_WAERK 
   AND ifnull(fb.dim_CurrencyId_TRA,-1) <> ifnull(tra.dim_currencyid,-2);

 
UPDATE fact_billing fb
   SET fb.Dim_Currencyid_GBL =  ifnull(cur.Dim_Currencyid, 1)
  FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,
       dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	   tmp_pGlobalCurrency_fact_billing, fact_billing fb
 WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
   AND cur.CurrencyCode = pGlobalCurrency
   AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
   AND so.SalesOrgCode = vkp.VBRK_VKORG
   AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
   AND ca.ControllingAreaCode = vkp.VBRP_KOKRS
   AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND dc.CompanyCode = dp.CompanyCode
   AND ifnull(fb.Dim_Currencyid_GBL,-1) <> ifnull(cur.Dim_Currencyid, -2);
   

MERGE INTO fact_billing fact
USING ( SELECT t1.fact_billingid, ifnull(z.exchangeRate,1) AS exchangeRate,
               ifnull(z2.exchangeRate,1) AS amt_ExchangeRate_GBL
          FROM (SELECT vkp.VBRK_WAERK, dc.currency, vkp.VBRK_KURRF,vkp.VBRK_FKDAT, fb.fact_billingid
                  FROM fact_billing fb
					 INNER JOIN VBRK_VBRP vkp
					   ON fb.dd_billing_no = vkp.VBRK_VBELN 
					  AND fb.dd_billing_item_no = vkp.VBRP_POSNR	
					INNER JOIN dim_company dc
					   ON fb.dim_companyid = dc.dim_companyid 
		    	) t1 
					LEFT JOIN (SELECT * from tmp_getExchangeRate1 x
					            WHERE x.fact_script_name = 'bi_populate_billing_fact') z
					   ON z.pFromCurrency  = t1.VBRK_WAERK
					  AND z.pToCurrency = t1.currency 
					  AND z.pFromExchangeRate = t1.VBRK_KURRF 
					  AND z.pDate = t1.VBRK_FKDAT
					LEFT JOIN (SELECT * from tmp_getExchangeRate1 y, tmp_pGlobalCurrency_fact_billing t_exch
					            WHERE y.fact_script_name = 'bi_populate_billing_fact'
								  AND y.pToCurrency = t_exch.pGlobalCurrency) z2
					   ON z2.pFromCurrency  = t1.VBRK_WAERK
					  AND z2.pFromExchangeRate = 0
					  AND z2.pDate = t1.VBRK_FKDAT
		  
	  ) src
   ON src.fact_billingid = fact.fact_billingid
 WHEN MATCHED THEN UPDATE 
          SET fact.amt_ExchangeRate = ifnull(src.exchangeRate, 1),
		      fact.amt_ExchangeRate_GBL = ifnull(src.amt_ExchangeRate_GBL, 1);

UPDATE fact_billing fb
  SET fb.amt_CashDiscount = ifnull(vkp.VBRP_SKFBP, 0),
      fb.dd_CancelledDocumentNo = ifnull(vkp.VBRK_SFAKN,'Not Set'),
      fb.dd_SalesDlvrItemNo = ifnull(vkp.VBRP_VGPOS, 0),
      fb.dd_SalesDlvrDocNo = ifnull(vkp.VBRP_VGBEL, 'Not Set')
 FROM VBRK_VBRP vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN 
  AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(dt.dim_dateid,1) AS Dim_DateidCreated,
	                ifnull(dt2.dim_dateid,1) AS Dim_DateidBilling
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_date dt
                ON dt.DateValue = vkp.VBRK_ERDAT 
               AND dt.CompanyCode = vkp.VBRK_BUKRS
			  LEFT JOIN dim_date dt2
                ON dt2.DateValue = vkp.VBRK_FKDAT 
               AND dt2.CompanyCode = vkp.VBRK_BUKRS) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_DateidCreated = ifnull(src.Dim_DateidCreated, 1),
      fact.Dim_DateidBilling = ifnull(src.Dim_DateidBilling, 1);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(dc.Dim_DistributionChannelId,1) AS Dim_DistributionChannelId
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_distributionchannel dc
                ON dc.DistributionChannelCode = vkp.VBRK_VTWEG 
			 WHERE dc.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_DistributionChannelId = ifnull(src.Dim_DistributionChannelId, 1)
WHERE ifnull(fact.Dim_DistributionChannelId,-1) <> ifnull(src.Dim_DistributionChannelId,-2);

DROP TABLE IF EXISTS dim_profitcenter_fact_billing;
CREATE TABLE dim_profitcenter_fact_billing
AS
SELECT * 
  FROM dim_profitcenter 
 ORDER BY ValidTo ASC;

 
MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(dc1.Dim_documentcategoryid,1) AS Dim_documentcategoryid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_documentcategory dc1
                ON trim(dc1.DocumentCategory) = trim(vkp.VBRK_VBTYP)
			 WHERE dc1.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_documentcategoryid = ifnull(src.Dim_documentcategoryid, 1)
WHERE ifnull(fact.Dim_documentcategoryid,-1) <> ifnull(src.Dim_documentcategoryid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(bdt.dim_billingdocumenttypeid,1) AS Dim_DocumentTypeid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_billingdocumenttype bdt
                ON bdt.Type = vkp.VBRK_FKART
			 WHERE bdt.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_DocumentTypeid = ifnull(src.Dim_DocumentTypeid, 1)
WHERE ifnull(fact.Dim_DocumentTypeid,-1) <> ifnull(src.Dim_DocumentTypeid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(it.Dim_IncoTermid,1) AS Dim_IncoTermid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_incoterm it
                ON it.IncoTermCode = vkp.VBRK_INCO1
			 WHERE it.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_IncoTermid = ifnull(src.Dim_IncoTermid, 1)
WHERE ifnull(fact.Dim_IncoTermid,-1) <> ifnull(src.Dim_IncoTermid,-2);

UPDATE fact_billing fb
   SET fb.Dim_Plantid = ifnull(dp.dim_plantid, 1)
  FROM Dim_Plant dp, VBRK_VBRP vkp, fact_billing fb
 WHERE dp.PlantCode = vkp.VBRP_WERKS 
   AND dp.RowIsCurrent = 1
   AND fb.dd_billing_no = vkp.VBRK_VBELN 
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND ifnull(fb.Dim_Plantid,-1) <> ifnull(dp.dim_plantid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(mg.Dim_MaterialGroupid,1) AS Dim_MaterialGroupid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_materialgroup mg
                ON mg.MaterialGroupCode = vkp.VBRP_MATKL
			 WHERE mg.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_MaterialGroupid = ifnull(src.Dim_MaterialGroupid, 1)
WHERE ifnull(fact.Dim_MaterialGroupid,-1) <> ifnull(src.Dim_MaterialGroupid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(p.dim_partid,1) AS dim_partid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_part p
                ON p.PartNumber = vkp.VBRP_MATNR
               AND p.Plant = vkp.VBRP_WERKS
			 WHERE p.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.dim_partid = ifnull(src.dim_partid, 1)
WHERE ifnull(fact.dim_partid,-1) <> ifnull(src.dim_partid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(ph.dim_producthierarchyid,1) AS dim_producthierarchyid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_producthierarchy ph
                ON ph.ProductHierarchy = vkp.VBRP_PRODH
			 WHERE ph.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.dim_producthierarchyid = ifnull(src.dim_producthierarchyid, 1)
WHERE ifnull(fact.dim_producthierarchyid,-1) <> ifnull(src.dim_producthierarchyid,-2);
	
MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(pc.Dim_ProfitCenterid,1) AS Dim_ProfitCenterid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_profitcenter_fact_billing pc
                ON pc.ProfitCenterCode = vkp.VBRP_PRCTR
               AND pc.ControllingArea = vkp.VBRP_KOKRS
			 WHERE pc.ValidTo >= vkp.VBRK_FKDAT
               AND pc.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_ProfitCenterid = ifnull(src.Dim_ProfitCenterid, 1)
WHERE ifnull(fact.Dim_ProfitCenterid,-1) <> ifnull(src.Dim_ProfitCenterid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(sd.Dim_SalesDivisionid,1) AS Dim_SalesDivisionid,
	               ifnull(sg.Dim_SalesGroupid,1) AS Dim_SalesGroupid, ifnull(so.Dim_SalesOfficeid,1) AS Dim_SalesOfficeid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_salesdivision sd
                ON sd.DivisionCode = vkp.VBRK_SPART
			  LEFT JOIN dim_salesgroup sg
                ON sg.SalesGroupCode = vkp.VBRP_VKGRP
			  LEFT JOIN dim_salesoffice so
                ON so.SalesOfficeCode = vkp.VBRP_VKBUR
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_SalesDivisionid = ifnull(src.Dim_SalesDivisionid, 1),
      fact.Dim_SalesGroupid = ifnull(src.Dim_SalesGroupid, 1),
	  fact.Dim_SalesOfficeid = ifnull(src.Dim_SalesOfficeid, 1);

UPDATE fact_billing fb
   SET fb.Dim_SalesOrgid = ifnull(so.dim_salesorgid,1)
  FROM dim_salesorg so, fact_billing fb, VBRK_VBRP vkp
 WHERE so.SalesOrgCode = vkp.VBRK_VKORG
   AND fb.dd_billing_no = vkp.VBRK_VBELN 
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND ifnull(fb.Dim_SalesOrgid,-1) <> ifnull(so.dim_salesorgid,-2);
  
UPDATE fact_billing fb
   SET fb.Dim_ControllingAreaid = ifnull(ca.dim_controllingareaid, 1)
  FROM dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
 WHERE ca.ControllingAreaCode = vkp.VBRP_KOKRS
   AND fb.dd_billing_no = vkp.VBRK_VBELN 
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND ifnull(fb.Dim_ControllingAreaid,-1) <> ifnull(ca.dim_controllingareaid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(sl.Dim_StorageLocationid,1) AS Dim_StorageLocationid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_storagelocation sl
                ON sl.LocationCode = vkp.VBRP_LGORT
               AND sl.plant = vkp.VBRP_WERKS
			 WHERE sl.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_StorageLocationid = ifnull(src.Dim_StorageLocationid, 1)
WHERE ifnull(fact.Dim_StorageLocationid,-1) <> ifnull(src.Dim_StorageLocationid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(ats.Dim_AccountingTransferStatusid,1) AS Dim_AccountingTransferStatusid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_accountingtransferstatus ats
                ON ats.AccountingTransferStatusCode = vkp.VBRK_RFBSK
			 WHERE ats.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_AccountingTransferStatusid = ifnull(src.Dim_AccountingTransferStatusid, 1)
WHERE ifnull(fact.Dim_AccountingTransferStatusid,-1) <> ifnull(src.Dim_AccountingTransferStatusid,-2);

/*
MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(dc1.Dim_ConditionProcedureid,1) AS Dim_ConditionProcedureid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_conditionprocedure dc1
                ON dc1.ProcedureCode = vkp.VBRK_KALSM
			 WHERE dc1.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_ConditionProcedureid = src.Dim_ConditionProcedureid
WHERE ifnull(fact.Dim_ConditionProcedureid,-1) <> ifnull(src.Dim_ConditionProcedureid,-2) */

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(cca.Dim_CreditControlAreaid,1) AS Dim_CreditControlAreaid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_creditcontrolarea cca
                ON cca.CreditControlAreaName = vkp.VBRP_KOKRS
			 WHERE cca.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_CreditControlAreaid = ifnull(src.Dim_CreditControlAreaid, 1)
WHERE ifnull(fact.Dim_CreditControlAreaid,-1) <> ifnull(src.Dim_CreditControlAreaid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(c.Dim_CustomerID,1) AS Dim_CustomerID
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_customer c
                ON c.CustomerNumber = vkp.VBRK_KUNAG
			 WHERE c.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_CustomerID = ifnull(src.Dim_CustomerID, 1)
WHERE ifnull(fact.Dim_CustomerID,-1) <> ifnull(src.Dim_CustomerID,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(cg1.Dim_CustomerGroup1id,1) AS Dim_CustomerGroup1id
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_customergroup1 cg1
                ON cg1.CustomerGroup = vkp.VBRK_KDGRP
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_CustomerGroup1id = ifnull(src.Dim_CustomerGroup1id, 1)
WHERE ifnull(fact.Dim_CustomerGroup1id,-1) <> ifnull(src.Dim_CustomerGroup1id,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(cpt.Dim_CustomerPaymentTermsid,1) AS Dim_CustomerPaymentTermsid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_customerpaymentterms cpt
                ON cpt.PaymentTermCode = vkp.VBRK_ZTERM
			 WHERE cpt.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_CustomerPaymentTermsid = ifnull(src.Dim_CustomerPaymentTermsid, 1)
WHERE ifnull(fact.Dim_CustomerPaymentTermsid,-1) <> ifnull(src.Dim_CustomerPaymentTermsid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(srp.Dim_ShipReceivePointid,1) AS Dim_ShipReceivePointid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_shipreceivepoint srp
                ON srp.ShipReceivePointCode = vkp.VBRP_VSTEL
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_ShipReceivePointid = ifnull(src.Dim_ShipReceivePointid, 1)
WHERE ifnull(fact.Dim_ShipReceivePointid,-1) <> ifnull(src.Dim_ShipReceivePointid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(um.Dim_UnitOfMeasureId,1) AS Dim_UnitOfMeasureId
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_unitofmeasure um
                ON um.UOM = vkp.VBRP_MEINS
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_UnitOfMeasureId = ifnull(src.Dim_UnitOfMeasureId, 1)
WHERE ifnull(fact.Dim_UnitOfMeasureId,-1) <> ifnull(src.Dim_UnitOfMeasureId,-2);

UPDATE fact_billing fb
   SET fb.Dim_Companyid = ifnull(dc.Dim_Companyid, 1),
       fb.dd_fiscalyear = vkp.VBRK_GJAHR,
       fb.dd_postingperiod = vkp.VBRK_POPER,
	   fb.dd_salesdocno = ifnull(vkp.VBRP_AUBEL, 'Not Set'),
       fb.dd_salesitemno = ifnull(vkp.VBRP_AUPOS, 0),
	   fb.amt_cost_InDocCurrency = ifnull(vkp.VBRP_WAVWR, 0),
       fb.amt_NetValueHeader_InDocCurrency = ifnull(vkp.VBRK_NETWR, 0)
  FROM dim_company dc,VBRK_VBRP vkp, fact_billing fb
 WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
   AND fb.dd_billing_no = vkp.VBRK_VBELN 
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(soic.Dim_SalesOrderItemCategoryid,1) AS Dim_SalesOrderItemCategoryid
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN Dim_SalesOrderItemCategory soic
                ON soic.SalesOrderItemCategory = vkp.VBRP_PSTYV
			 WHERE soic.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_SalesOrderItemCategoryid = ifnull(src.Dim_SalesOrderItemCategoryid, 1)
WHERE ifnull(fact.Dim_SalesOrderItemCategoryid,-1) <> ifnull(src.Dim_SalesOrderItemCategoryid,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(bc.Dim_BillingCategoryId,1) AS Dim_BillingCategoryId
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN Dim_BillingCategory bc
                ON bc.Category = vkp.VBRK_FKTYP
			 WHERE bc.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_BillingCategoryId = ifnull(src.Dim_BillingCategoryId, 1)
WHERE ifnull(fact.Dim_BillingCategoryId,-1) <> ifnull(src.Dim_BillingCategoryId,-2);

MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(rrc.Dim_RevenueRecognitionCategoryId,1) AS Dim_RevenueRecognitionCategoryId
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_RevenueRecognitioncategory rrc
                ON rrc.Category = vkp.VBRP_RRREL
			 WHERE rrc.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_RevenueRecognitionCategoryId = ifnull(src.Dim_RevenueRecognitionCategoryId, 1)
WHERE ifnull(fact.Dim_RevenueRecognitionCategoryId,-1) <> ifnull(src.Dim_RevenueRecognitionCategoryId,-2);

UPDATE fact_billing fb
   SET fb.amt_NetValueItem_InDocCurrency = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * vkp.VBRP_NETWR, 0),
       fb.amt_CustomerConfigSubtotal1_InDocCurrency = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI1, 0),
       fb.amt_CustomerConfigSubtotal2_InDocCurrency = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI2, 0),
       fb.amt_CustomerConfigSubtotal3_InDocCurrency = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI3, 0),
       fb.amt_CustomerConfigSubtotal4_InDocCurrency = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI4, 0),
       fb.amt_CustomerConfigSubtotal5_InDocCurrency = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI5, 0),
       fb.amt_CustomerConfigSubtotal6_InDocCurrency = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI6, 0),
       fb.dd_CustomerPONumber = ifnull(vkp.VBRK_BSTNK_VF,'Not Set'),
       fb.dd_CreatedBy = ifnull(vkp.VBRK_ERNAM,'Not Set')
  FROM VBRK_VBRP vkp, fact_billing fb
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN 
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
   SET fb.amt_cost = ifnull(vkp.VBRP_WAVWR, 0), /** amt_ExchangeRate*/
       fb.amt_NetValueHeader = ifnull(vkp.VBRK_NETWR, 0), /** amt_ExchangeRate,*/
       fb.amt_NetValueItem = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * vkp.VBRP_NETWR, 0), /** amt_ExchangeRate,*/
       fb.amt_CustomerConfigSubtotal1 = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI1, 0), /** amt_ExchangeRate,*/
       fb.amt_CustomerConfigSubtotal2 = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI2, 0), /** amt_ExchangeRate,*/
       fb.amt_CustomerConfigSubtotal3 = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI3, 0), /** amt_ExchangeRate,*/
       fb.amt_CustomerConfigSubtotal4 = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI4, 0), /** amt_ExchangeRate,*/
       fb.amt_CustomerConfigSubtotal5 = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI5, 0), /** amt_ExchangeRate,*/
       fb.amt_CustomerConfigSubtotal6 = ifnull((CASE WHEN vkp.VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *vkp.VBRP_KZWI6, 0) /** amt_ExchangeRate,*/
  FROM vbrk_vbrp vkp, fact_billing fb  
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
   
MERGE INTO fact_billing fact
	USING (  SELECT fb.fact_billingid, ifnull(mpg.Dim_MaterialPricingGroupId,1) AS Dim_MaterialPricingGroupId
			  FROM fact_billing fb
			 INNER JOIN VBRK_VBRP vkp
				ON fb.dd_billing_no = vkp.VBRK_VBELN 
			   AND fb.dd_billing_item_no = vkp.VBRP_POSNR    		  
			  LEFT JOIN dim_MaterialPricingGroup mpg
                ON mpg.MaterialPricingGroupCode = vkp.VBRP_KONDM
			 WHERE mpg.RowIsCurrent = 1
            ) src
   ON fact.fact_billingid = src.fact_billingid
 WHEN MATCHED THEN UPDATE
  SET fact.Dim_MaterialPricingGroupId = ifnull(src.Dim_MaterialPricingGroupId, 1)
WHERE ifnull(fact.Dim_MaterialPricingGroupId,-1) <> ifnull(src.Dim_MaterialPricingGroupId,-2);

/*UPDATE fact_Billing fb
  FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm
   SET fb.dim_Customermastersalesid = cms.dim_Customermastersalesid
 WHERE     fb.dim_salesorgid = sorg.dim_salesorgid
       AND fb.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = fb.dim_salesdivisionid
       AND cm.dim_customerid = fb.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber*/

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid;
CREATE TABLE tmp_perf_fb_dim_Customermastersalesid
AS
SELECT DISTINCT fb.dim_salesorgid,fb.dim_distributionchannelid,
       fb.dim_salesdivisionid,fb.dim_customerid, CONVERT(integer,1) dim_Customermastersalesid
  FROM fact_billing fb;

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid_2;
CREATE TABLE tmp_perf_fb_dim_Customermastersalesid_2
AS	   
SELECT DISTINCT fb.dim_salesorgid,fb.dim_distributionchannelid,fb.dim_salesdivisionid,
       fb.dim_customerid, max(cms.dim_Customermastersalesid) AS dim_Customermastersalesid
  FROM dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm,
	   tmp_perf_fb_dim_Customermastersalesid fb
 WHERE fb.dim_salesorgid = sorg.dim_salesorgid
   AND fb.dim_distributionchannelid = dc.dim_distributionchannelid
   AND sd.dim_salesdivisionid = fb.dim_salesdivisionid
   AND cm.dim_customerid = fb.dim_customerid
   AND sorg.SalesOrgCode = cms.SalesOrg
   AND dc.distributionchannelcode = cms.distributionchannel
   AND sd.DivisionCode = cms.Divisioncode
   AND cm.customernumber = cms.CustomerNumber
   GROUP BY fb.dim_salesorgid,fb.dim_distributionchannelid,fb.dim_salesdivisionid,fb.dim_customerid;	   
	   
UPDATE fact_Billing fb
   SET fb.dim_Customermastersalesid = ifnull(upd.dim_Customermastersalesid, 1)
  FROM tmp_perf_fb_dim_Customermastersalesid_2 upd,fact_Billing fb
 WHERE fb.dim_salesorgid =  upd.dim_salesorgid
   AND fb.dim_distributionchannelid = upd.dim_distributionchannelid
   AND fb.dim_salesdivisionid = upd.dim_salesdivisionid
   AND fb.dim_customerid = upd.dim_customerid
   AND fb.dim_Customermastersalesid <> ifnull(upd.dim_Customermastersalesid, 1);
   
DROP TABLE IF EXISTS TMP_UPD_BILLING_SO;
CREATE TABLE TMP_UPD_BILLING_SO
AS
SELECT fso.dd_SalesDocNo, fso.dd_SalesItemNo, max(fso.Dim_CostCenterid) Dim_CostCenterid, max(fso.Dim_SalesDocumentTypeid) Dim_SalesDocumentTypeid, max(fso.Dim_CustomPartnerFunctionId) Dim_CustomPartnerFunctionId
	,max(fso.Dim_PayerPartnerFunctionId) Dim_PayerPartnerFunctionId, max(fso.Dim_BillToPartyPartnerFunctionId) Dim_BillToPartyPartnerFunctionId, max(fso.Dim_CustomerGroupId) Dim_CustomerGroupId
	,max(fso.Dim_CustomPartnerFunctionId1) Dim_CustomPartnerFunctionId1, max(fso.Dim_CustomPartnerFunctionId2) Dim_CustomPartnerFunctionId2
FROM fact_salesorder fso
GROUP BY fso.dd_SalesDocNo, fso.dd_SalesItemNo;
   
UPDATE fact_billing fb 
   SET fb.Dim_so_CostCenterid = ifnull(fso.Dim_CostCenterid, 1),
	   fb.Dim_so_SalesDocumentTypeid = ifnull(fso.Dim_SalesDocumentTypeid, 1),
	   fb.Dim_CustomPartnerFunctionId = ifnull(fso.Dim_CustomPartnerFunctionId, 1),
	   fb.Dim_PayerPartnerFunctionId = ifnull(fso.Dim_PayerPartnerFunctionId, 1),
	   fb.Dim_BillToPartyPartnerFunctionId = ifnull(fso.Dim_BillToPartyPartnerFunctionId, 1),
	   fb.Dim_CustomerGroupId = ifnull(fso.Dim_CustomerGroupId, 1),
	   fb.Dim_CustomPartnerFunctionId1 = ifnull(fso.Dim_CustomPartnerFunctionId1, 1),
	   fb.Dim_CustomPartnerFunctionId2 = ifnull(fso.Dim_CustomPartnerFunctionId2, 1)
  FROM TMP_UPD_BILLING_SO fso, vbrk_vbrp vkp, fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
   
DROP TABLE IF EXISTS TMP_UPD_BILLING_SO;

DROP TABLE IF EXISTS tmp1_fact_billing;	   
CREATE TABLE tmp1_fact_billing
AS
SELECT DISTINCT fso.dim_salesorderitemstatusid,fso.dd_SalesDocNo,fso.dd_SalesItemNo
  FROM fact_billing fb,fact_salesorder fso,vbrk_vbrp vkp
 WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND fb.Dim_SalesDocumentItemStatusId <> fso.dim_salesorderitemstatusid;

UPDATE fact_billing fb 
   SET fb.Dim_SalesDocumentItemStatusId = ifnull(fso.dim_salesorderitemstatusid,1)
  FROM tmp1_fact_billing fso,vbrk_vbrp vkp,fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
DROP TABLE IF EXISTS tmp1_fact_billing;	

DROP TABLE IF EXISTS tmp2_fact_billing;	   
CREATE TABLE tmp2_fact_billing
AS
SELECT max(fso.Dim_DateidSchedDelivery) AS Dim_DateidSchedDelivery,fso.dd_SalesDocNo,fso.dd_SalesItemNo
  FROM fact_billing fb,fact_salesorder fso,vbrk_vbrp vkp
 WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND fb.Dim_so_DateidSchedDelivery <> fso.Dim_DateidSchedDelivery
 GROUP BY fso.dd_SalesDocNo,fso.dd_SalesItemNo;

UPDATE fact_billing fb 
   SET fb.Dim_so_DateidSchedDelivery = ifnull(fso.Dim_DateidSchedDelivery, 1)
  FROM tmp2_fact_billing fso,vbrk_vbrp vkp,fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
DROP TABLE IF EXISTS tmp2_fact_billing;	

DROP TABLE IF EXISTS tmp3_fact_billing;	   
CREATE TABLE tmp3_fact_billing
AS
SELECT DISTINCT fso.Dim_DateidSalesOrderCreated AS Dim_DateidSalesOrderCreated,fso.dd_SalesDocNo,fso.dd_SalesItemNo,
row_number() over (partition by fso.dd_SalesDocNo,fso.dd_SalesItemNo order by '')rowno
  FROM fact_billing fb,fact_salesorder fso,vbrk_vbrp vkp
 WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND fb.Dim_so_SalesOrderCreatedId <> fso.Dim_DateidSalesOrderCreated;

UPDATE fact_billing fb 
   SET fb.Dim_so_SalesOrderCreatedId = ifnull(fso.Dim_DateidSalesOrderCreated, 1)
  FROM tmp3_fact_billing fso,vbrk_vbrp vkp,fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND fso.rowno = 1;
DROP TABLE IF EXISTS tmp3_fact_billing;	

DROP TABLE IF EXISTS tmp4_fact_billing;	   
CREATE TABLE tmp4_fact_billing
AS
SELECT max(fso.Dim_CustomeridShipTo) AS Dim_CustomeridShipTo,fso.dd_SalesDocNo,fso.dd_SalesItemNo
  FROM fact_billing fb,fact_salesorder fso,vbrk_vbrp vkp
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND fb.Dim_CustomeridShipTo <> fso.Dim_CustomeridShipTo
 GROUP BY fso.dd_SalesDocNo,fso.dd_SalesItemNo;

UPDATE fact_billing fb 
   SET fb.Dim_CustomeridShipTo = ifnull(fso.Dim_CustomeridShipTo, 1)
  FROM tmp4_fact_billing fso,vbrk_vbrp vkp,fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
DROP TABLE IF EXISTS tmp4_fact_billing;


DROP TABLE IF EXISTS tmp_fb_fact_salesorder;
CREATE TABLE tmp_fb_fact_salesorder
AS
SELECT f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,SUM(f_so.ct_ConfirmedQty) sum_ct_ConfirmedQty,
       SUM(f_so.ct_DeliveredQty) sum_ct_DeliveredQty,
	   SUM(f_so.ct_ScheduleQtySalesUnit) - SUM(f_so.ct_DeliveredQty ) as diff_sched_dlvrqty,
       SUM( f_so.ct_ConfirmedQty * (f_so.amt_UnitPrice / (CASE WHEN f_so.ct_PriceUnit = 0 THEN NULL ELSE f_so.ct_PriceUnit END))) upd_amt_so_confirmed
  FROM fact_salesorder f_so
GROUP BY f_so.dd_SalesDocNo,f_so.dd_SalesItemNo;

/* tmp_fb_fact_salesorder_a stores aggregate value diff_sched_dlvrqty grouped by doc,item. But it also has an additinoal column Dim_DocumentCategoryid
If there are no doc,item combinations in fso having different documentcategoryid, then rowcount in tmp_fb_fact_salesorder_a will be same as tmp_fb_fact_salesorder */
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_a;
CREATE TABLE  tmp_fb_fact_salesorder_a
AS
SELECT DISTINCT fso.dd_SalesDocNo,fso.dd_SalesItemNo,fso.Dim_DocumentCategoryid,
       f_so.diff_sched_dlvrqty,f_so.upd_amt_so_confirmed
  FROM tmp_fb_fact_salesorder f_so, fact_salesorder fso
 WHERE fso.dd_SalesDocNo = f_so.dd_SalesDocNo
   AND fso.dd_SalesItemNo = f_so.dd_SalesItemNo;

DROP TABLE IF EXISTS tmp_fb_amt_updates;
CREATE TABLE tmp_fb_amt_updates
AS
SELECT fb.*
  FROM fact_billing fb,dim_billingmisc bm,dim_billingdocumenttype bdt,vbrk_vbrp vkp
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND fb.dim_billingmiscid = bm.dim_billingmiscid
   AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

/* This is required for using combine*/
ALTER TABLE tmp_fb_amt_updates
ADD PRIMARY KEY (fact_billingid);

UPDATE tmp_fb_amt_updates fb
   SET fb.ct_so_ConfirmedQty = 0,
       fb.amt_so_UnitPrice = 0,
	   fb.ct_so_DeliveredQty = 0,
	   fb.amt_so_Returned = 0,
	   fb.ct_so_OpenOrderQty = 0,
	   fb.ct_so_ReturnedQty = 0,
	   fb.amt_so_confirmed = 0,
	   fb.amt_sd_Cost = 0;

UPDATE tmp_fb_amt_updates fb
   SET fb.ct_so_ConfirmedQty = ifnull(fso.sum_ct_ConfirmedQty,0)  
  FROM tmp_fb_fact_salesorder fso,tmp_fb_amt_updates fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.ct_so_ConfirmedQty <> ifnull(fso.sum_ct_ConfirmedQty,-5);

DROP TABLE IF EXISTS TMP_UPDT_SLS;
CREATE TABLE TMP_UPDT_SLS
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,max(amt_UnitPrice) amt_UnitPrice FROM fact_salesorder GROUP BY dd_SalesDocNo,dd_SalesItemNo;

UPDATE tmp_fb_amt_updates fb
   SET fb.amt_so_UnitPrice = ifnull(fso.amt_UnitPrice,0) 
  FROM TMP_UPDT_SLS fso,tmp_fb_amt_updates fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.amt_so_UnitPrice <> ifnull(fso.amt_UnitPrice,-5);
   
DROP TABLE IF EXISTS TMP_UPDT_SLS;
   
UPDATE tmp_fb_amt_updates fb
   SET fb.ct_so_DeliveredQty = ifnull(fso.sum_ct_DeliveredQty,0)  
  FROM tmp_fb_fact_salesorder fso,tmp_fb_amt_updates fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.ct_so_DeliveredQty <> ifnull(fso.sum_ct_DeliveredQty,-5);


DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2;
CREATE TABLE tmp_fb_fact_salesorder_2
AS
/* select f_so.dd_SalesDocNo,f_so.dd_SalesItemNo, SUM(f_so.amt_ScheduleTotal) sum_amt_ScheduleTotal, SUM(f_so.ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit, SUM(f_so.ct_ScheduleQtySalesUnit) - SUM(f_so.ct_DeliveredQty ) as diff_sched_dlvrqt */
SELECT f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,SUM(f_so.ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit, 
       SUM(f_so.amt_ScheduleTotal) sum_amt_ScheduleTotal
  FROM fact_salesorder f_so, Dim_documentcategory dc 
 WHERE dc.Dim_DocumentCategoryid = f_so.Dim_DocumentCategoryid
   AND dc.RowIsCurrent = 1
   AND dc.DocumentCategory IN ('H', 'K')
   AND f_so.Dim_SalesOrderRejectReasonid <> 1
 GROUP BY f_so.dd_SalesDocNo,f_so.dd_SalesItemNo;

DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2a;
CREATE TABLE tmp_fb_fact_salesorder_2a
AS
SELECT DISTINCT fso.dd_SalesDocNo,fso.dd_SalesItemNo,fso.Dim_DocumentCategoryid,f_so.sum_ct_ScheduleQtySalesUnit
  FROM tmp_fb_fact_salesorder_2 f_so, fact_salesorder fso
 WHERE fso.dd_SalesDocNo = f_so.dd_SalesDocNo
   AND fso.dd_SalesItemNo = f_so.dd_SalesItemNo;

UPDATE tmp_fb_amt_updates fb 
   SET fb.amt_so_Returned = ifnull(fso.sum_amt_ScheduleTotal, 0)
  FROM tmp_fb_fact_salesorder_2 fso,tmp_fb_amt_updates fb
 WHERE fso.dd_SalesDocNo = fb.dd_salesdocno
   AND fso.dd_SalesItemNo = fb.dd_salesitemno
   AND fb.amt_so_Returned <> ifnull(fso.sum_amt_ScheduleTotal, 0);     


   
UPDATE tmp_fb_amt_updates fb 
   SET fb.ct_so_OpenOrderQty = ifnull(fso.diff_sched_dlvrqty, 0)
  FROM tmp_fb_fact_salesorder_a fso,Dim_documentcategory dc,tmp_fb_amt_updates fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
   AND dc.RowIsCurrent = 1
   AND fb.ct_so_OpenOrderQty <> ifnull(fso.diff_sched_dlvrqty, 0);


UPDATE tmp_fb_amt_updates fb 
   SET fb.ct_so_ReturnedQty = ifnull(fso.sum_ct_ScheduleQtySalesUnit, 0)
  FROM tmp_fb_fact_salesorder_2 fso,tmp_fb_amt_updates fb 
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND fb.ct_so_ReturnedQty <> ifnull(fso.sum_ct_ScheduleQtySalesUnit, 0);

DROP TABLE IF EXISTS TMP_UPDT_SLS;
CREATE TABLE TMP_UPDT_SLS
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,MAX(Dim_DocumentCategoryid) Dim_DocumentCategoryid FROM fact_salesorder GROUP BY dd_SalesDocNo,dd_SalesItemNo;

UPDATE tmp_fb_amt_updates fb
   SET fb.amt_revenue =
          ifnull(CASE dc.DocumentCategory
           WHEN 'U' THEN 0
           WHEN '5' THEN 0
           WHEN '6' THEN 0
           WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
           WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
           ELSE fb.amt_NetValueItem
           END, 0) 
  FROM TMP_UPDT_SLS fso,Dim_documentcategory dc,tmp_fb_amt_updates fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
   AND dc.RowIsCurrent = 1;

UPDATE tmp_fb_amt_updates fb
   SET fb.amt_Revenue_InDocCurrency = 
          ifnull(CASE dc.DocumentCategory
           WHEN 'U' THEN 0
           WHEN '5' THEN 0
           WHEN '6' THEN 0
           WHEN 'N' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
           WHEN 'O' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
           ELSE fb.amt_NetValueItem_InDocCurrency
           END, 0)
  FROM TMP_UPDT_SLS fso,Dim_documentcategory dc,tmp_fb_amt_updates fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
   AND dc.RowIsCurrent = 1;
   
DROP TABLE IF EXISTS TMP_UPDT_SLS;

UPDATE tmp_fb_amt_updates fb 
   SET fb.amt_so_confirmed = ifnull(fso.upd_amt_so_confirmed, 0)
  FROM tmp_fb_fact_salesorder_a fso,Dim_documentcategory dc,tmp_fb_amt_updates fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
   AND dc.RowIsCurrent = 1
   AND fb.amt_so_confirmed <> ifnull(fso.upd_amt_so_confirmed, 0);


DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;
CREATE TABLE tmp_openorder_from_Sales_to_billing AS
SELECT f_so.dd_SalesDocNo, f_so.dd_salesitemno,SUM(f_so.ct_ScheduleQtySalesUnit) as orderqty,
       SUM(f_so.ct_DeliveredQty) as DlvrQty, AVG(f_so.amt_UnitPrice) as TotalUnitPrice, 
	   ifnull(avg(f_so.ct_PriceUnit),0) as PriceUnit 
  FROM fact_salesorder f_so
 GROUP BY f_so.dd_SalesDocNo,f_so.dd_salesitemno;

UPDATE tmp_fb_amt_updates fb
   SET fb.amt_so_openorder =
       ifnull((t.orderqty - t.DlvrQty)* t.TotalUnitPrice /(CASE WHEN t.PriceUnit = 0 THEN 1 ELSE t.PriceUnit END), 0)
  FROM tmp_openorder_from_Sales_to_billing t,tmp_fb_amt_updates fb
 WHERE t.dd_salesDocNo = fb.dd_Salesdocno
   AND t.dd_SalesItemNo = fb.dd_SalesItemNo;
DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;

DROP TABLE IF EXISTS tmp_fb_fact_salesorderdelivery;
CREATE TABLE tmp_fb_fact_salesorderdelivery
AS
SELECT f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo,sum(f_sod.amt_Cost) as sum_amt_Cost
  FROM fact_salesorderdelivery f_sod
 GROUP BY f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo;

UPDATE tmp_fb_amt_updates fb
   SET fb.amt_sd_Cost = ifnull(f_sod.sum_amt_Cost, 0)
  FROM tmp_fb_fact_salesorderdelivery f_sod,tmp_fb_amt_updates fb
 WHERE f_sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
   AND f_sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo
   AND fb.amt_sd_Cost <> ifnull(f_sod.sum_amt_Cost, 0);

DROP TABLE IF EXISTS TMP_UPDT_BILLING_SOD;
CREATE TABLE TMP_UPDT_BILLING_SOD
AS
SELECT sod.dd_SalesDlvrDocNo,sod.dd_SalesDlvrItemNo,max(sod.Dim_DateidActualGoodsIssue) Dim_DateidActualGoodsIssue FROM fact_salesorderdelivery sod GROUP BY sod.dd_SalesDlvrDocNo,sod.dd_SalesDlvrItemNo;
   
UPDATE tmp_fb_amt_updates fb
   SET fb.Dim_sd_DateidActualGoodsIssue = ifnull(sod.Dim_DateidActualGoodsIssue, 1)
  FROM TMP_UPDT_BILLING_SOD sod,tmp_fb_amt_updates fb
 WHERE sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
   AND sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo
   AND ifnull(fb.Dim_sd_DateidActualGoodsIssue,-1) <> ifnull(sod.Dim_DateidActualGoodsIssue, 1);
   
DROP TABLE IF EXISTS TMP_UPDT_BILLING_SOD;

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;
CREATE TABLE tmp_openorder_from_Shipment_to_billing 
AS
SELECT f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo,SUM(f_sod.ct_QtyDelivered) as dlvrqty,
       AVG(f_sod.amt_UnitPrice) as AmtUnitPrice,ifnull(avg(f_sod.ct_PriceUnit),0) as PriceUnit 
  FROM fact_salesorderdelivery f_sod
 GROUP BY f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo;

UPDATE tmp_fb_amt_updates fb
   SET fb.amt_sd_Shipped 
       = ifnull((t.dlvrqty * t.AmtUnitPrice/(case when t.PriceUnit = 0 then null else t.PriceUnit end )),0)
  FROM tmp_openorder_from_Shipment_to_billing t,tmp_fb_amt_updates fb
 WHERE fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
   AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo;
   
UPDATE tmp_fb_amt_updates fb
   SET fb.ct_sd_QtyDelivered = ifnull(t.dlvrqty, 0)
  FROM tmp_openorder_from_Shipment_to_billing t,tmp_fb_amt_updates fb
 WHERE fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
   AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo;
   
DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;
DROP TABLE IF EXISTS tmp_SalesOrderAmtForBilling;


MERGE INTO fact_billing i 
USING tmp_fb_amt_updates d
   ON i.dd_billing_no = d.dd_billing_no
  AND i.dd_billing_item_no = d.dd_billing_item_no
WHEN MATCHED THEN DELETE;

INSERT INTO fact_billing i
SELECT d.* 
  FROM tmp_fb_amt_updates d;


CREATE TABLE tmp_SalesOrderAmtForBilling
AS
SELECT fb.dd_billing_no,fb.dd_billing_item_no,dt.datevalue as billingdate,
       so.dd_SalesDocNo,so.dd_salesItemno,AVG(fb.ct_sd_QtyDelivered) as sd_QtyDelivered,
	   AVG(fb.amt_sd_Shipped) as sd_shipped,sum(so.ct_ScheduleQtySalesUnit) as totalorderqty,
	   sum(so.amt_ScheduleTotal) as totalorderamt,convert(decimal (18,4),0) as acttotalqty, convert(decimal (18,4),0) as acttotalamt
  FROM fact_billing fb, fact_salesorder so,dim_date dt, 
       dim_billingdocumenttype bdt,dim_billingmisc bm
 WHERE fb.dd_SalesDocNo = so.dd_salesdocno
   AND fb.dd_salesitemno = so.dd_SalesItemNo
   AND fb.Dim_DateidBilling = dt.dim_dateid
   AND fb.dd_Salesdocno <> 'Not Set'
   AND fb.dim_billingmiscid = bm.dim_billingmiscid
   AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C')
 GROUP BY fb.dd_billing_no,fb.dd_billing_item_no,dt.datevalue,so.dd_SalesDocNo,so.dd_salesItemno;

UPDATE tmp_SalesOrderAmtForBilling
   SET acttotalamt =  sd_Shipped,
       acttotalqty =  sd_QtyDelivered
 WHERE totalorderamt > sd_Shipped;

DROP TABLE IF EXISTS tmp_SalesOrderAmtForBilling_100;
CREATE TABLE tmp_SalesOrderAmtForBilling_100
AS
SELECT dd_salesDocNo,dd_salesitemno, max(billingdate) as billdate,
       sum(acttotalamt) as ActualAmt,sum(acttotalqty) as ActualQty 
  FROM tmp_SalesOrderAmtForBilling 
 GROUP BY dd_salesDocNo,dd_salesitemno;

UPDATE tmp_SalesOrderAmtForBilling t1
   SET t1.acttotalamt = t1.acttotalamt + (t1.TotalOrderamt - t2.ActualAmt)
  FROM tmp_SalesOrderAmtForBilling_100 t2,tmp_SalesOrderAmtForBilling t1 
 WHERE t1.dd_Salesdocno = t2.dd_salesdocno
   AND t1.dd_salesitemno = t2.dd_Salesitemno
   AND t1.billingdate = t2.billdate
   AND t1.totalorderamt > t2.ActualAmt
   AND ifnull(t1.acttotalamt,-1) <> t1.acttotalamt + (t1.TotalOrderamt - t2.ActualAmt);

UPDATE tmp_SalesOrderAmtForBilling t1
   SET t1.acttotalqty = t1.acttotalqty + (t1.Totalorderqty - t2.actualqty)
  FROM tmp_SalesOrderAmtForBilling_100 t2,tmp_SalesOrderAmtForBilling t1 
 WHERE t1.dd_Salesdocno = t2.dd_salesdocno
   AND t1.dd_salesitemno = t2.dd_Salesitemno
   AND t1.billingdate = t2.billdate
   AND t1.totalorderamt > t2.ActualAmt
   AND ifnull(t1.acttotalqty,-1) <>  t1.acttotalqty + (t1.Totalorderqty - t2.actualqty);

DROP TABLE IF EXISTS tmp_SalesOrderAmtForBilling_100;

UPDATE fact_billing fb
   SET fb.amt_so_ScheduleTotal = ifnull(tmp.acttotalamt, 0)
  FROM tmp_SalesOrderAmtForBilling tmp,fact_billing fb
 WHERE fb.dd_billing_no = tmp.dd_billing_no
   AND fb.dd_billing_item_no = tmp.dd_billing_item_no
   AND fb.amt_so_ScheduleTotal <> tmp.acttotalamt
   AND IFNULL(fb.amt_so_ScheduleTotal,-1) = IFNULL(tmp.acttotalamt,-2);
  
UPDATE fact_billing fb
   SET fb.ct_so_ScheduleQtySalesUnit = ifnull(tmp.acttotalqty, 0)
  FROM tmp_SalesOrderAmtForBilling tmp,fact_billing fb      
 WHERE fb.dd_billing_no = tmp.dd_billing_no
   AND fb.dd_billing_item_no = tmp.dd_billing_item_no
   AND IFNULL(fb.ct_so_ScheduleQtySalesUnit,-1) <> IFNULL(tmp.acttotalqty,-2);  

DROP TABLE IF EXISTS tmp_SalesOrderAmtForBilling;
  
UPDATE fact_billing fb
   SET fb.Dim_BillingMiscId = ifnull(bmisc.Dim_BillingMiscId, 1)
	   ,fb.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM Dim_BillingMisc bmisc,VBRK_VBRP vrp,fact_billing fb
 WHERE fb.dd_Billing_no = vrp.VBRK_VBELN
   AND fb.dd_billing_item_no = vrp.VBRP_POSNR
   AND bmisc.CancelFlag = ifnull(vrp.VBRK_FKSTO,'Not Set')
   AND bmisc.CashDiscountIndicator = ifnull( VBRP_SKTOF,'Not Set')
   AND ifnull(fb.Dim_BillingMiscId,-1) <> ifnull(bmisc.Dim_BillingMiscId,-2);  

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid;
DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid_2;
DROP TABLE IF EXISTS tmp_pGlobalCurrency_fact_billing;
drop table if exists dim_profitcenter_fact_billing;
DROP TABLE IF EXISTS tmp_db_fb1 ;
DROP TABLE IF EXISTS tmp_db_fb2;
DROP TABLE IF EXISTS tmp_ins_fact_billing;
DROP TABLE IF EXISTS tmp_del_fact_billing;
DROP TABLE IF EXISTS tmp_fact_billing_VBRK_VBRP;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_a;
DROP TABLE IF EXISTS tmp_fb_amt_updates;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2a;
DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;
DROP TABLE IF EXISTS tmp_fb_fact_salesorderdelivery;
DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;
DROP table if exists tmp_SalesOrderAmtForBilling;
DROP table if exists tmp_SalesOrderAmtForBilling_100;



DROP TABLE IF EXISTS TMP_UPDT_BILLING_SO;
CREATE TABLE TMP_UPDT_BILLING_SO
AS
SELECT fso.dd_SalesDocNo, fso.dd_SalesItemNo, max(fso.dim_materialpricegroup4id) dim_materialpricegroup4id, max(fso.dim_materialpricegroup5id) dim_materialpricegroup5id, max(fso.dim_CustomerConditionGroups1id) dim_CustomerConditionGroups1id
	,max(fso.dim_CustomerConditionGroups2id) dim_CustomerConditionGroups2id, max(fso.dim_CustomerConditionGroups3id) dim_CustomerConditionGroups3id, max(fso.dim_CustomerConditionGroups4id) dim_CustomerConditionGroups4id
	,max(fso.dim_CustomerConditionGroups5id) dim_CustomerConditionGroups5id, max(fso.Dim_CustomerGroup4id) Dim_CustomerGroup4id, max(fso.dim_documentcategoryid) dim_documentcategoryid
FROM fact_salesorder fso
GROUP BY fso.dd_SalesDocNo, fso.dd_SalesItemNo;


UPDATE fact_billing fb 
   SET fb.dim_materialpricegroup4id= ifnull(fso.dim_materialpricegroup4id,1),
       fb.dim_materialpricegroup5id= ifnull(fso.dim_materialpricegroup5id,1),
       fb.dim_CustomerConditionGroups1id = ifnull(fso.dim_CustomerConditionGroups1id,1),
       fb.dim_CustomerConditionGroups2id= ifnull(fso.dim_CustomerConditionGroups2id,1),
       fb.dim_CustomerConditionGroups3id= ifnull(fso.dim_CustomerConditionGroups3id,1),
       fb.dim_CustomerConditionGroups4id = ifnull(fso.dim_CustomerConditionGroups4id,1),
       fb.dim_CustomerConditionGroups5id = ifnull(fso.dim_CustomerConditionGroups5id,1),
       fb.Dim_CustomerGroup4id= ifnull(fso.Dim_CustomerGroup4id,1),
	   fb.dim_salesdocumentcategoryid = ifnull(fso.dim_documentcategoryid,1)
  FROM TMP_UPDT_BILLING_SO fso,fact_billing fb 
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo;
   
DROP TABLE IF EXISTS TMP_UPDT_BILLING_SO;

UPDATE fact_billing fb
   SET fb.dim_partsalesid = 1
 WHERE IFNULL(fb.dim_partsalesid,-1) <> 1;

DROP TABLE IF EXISTS TMP_UPDT_PS;
CREATE TABLE TMP_UPDT_PS
AS
SELECT fb.fact_billingid,dp.partnumber,so.salesorgcode,dc.distributionchannelcode
FROM fact_billing fb
	INNER JOIN dim_part dp ON fb.dim_partid = dp.dim_partid
	INNER JOIN dim_salesorg so ON fb.dim_salesorgid = so.dim_salesorgid
	INNER JOIN dim_distributionchannel dc ON fb.dim_distributionchannelid = dc.dim_distributionchannelid;
	
UPDATE fact_billing fb
SET fb.dim_partsalesid = ifnull(ps.dim_partsalesid, 1)
FROM fact_billing fb
	INNER JOIN TMP_UPDT_PS t on fb.fact_billingid = t.fact_billingid
	INNER JOIN dim_partsales ps ON t.partnumber = ps.partnumber AND t.salesorgcode = ps.salesorgcode AND t.distributionchannelcode = ps.distributionchannelcode AND ps.RowIsCurrent = 1
WHERE IFNULL(fb.dim_partsalesid,-1) <> IFNULL(ps.dim_partsalesid,-2);

DROP TABLE IF EXISTS TMP_UPDT_PS;

UPDATE fact_billing fb
   SET fb.dd_ConditionNo=ifnull(vkp.VBRK_KNUMV,'Not Set')
  FROM VBRK_VBRP vkp,fact_billing fb
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND fb.dd_ConditionNo<>ifnull(vkp.VBRK_KNUMV,'Not Set');

UPDATE fact_billing fb
   SET fb.Dim_MaterialPriceGroup2Id= ifnull(mpg.Dim_MaterialPriceGroup2Id, 1)
  FROM VBRK_VBRP vkp,Dim_MaterialPriceGroup2 mpg,fact_billing fb
 WHERE mpg.MaterialPriceGroup2 = ifnull(vkp.VBRP_MVGR2,'Not Set')
   AND fb.dd_billing_no = vkp.VBRK_VBELN
   AND fb.dd_billing_item_no = vkp.VBRP_POSNR
   AND ifnull(fb.Dim_MaterialPriceGroup2Id,-1) <> ifnull(mpg.Dim_MaterialPriceGroup2Id,-2);

/* MDG Part */
DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_BILL;
CREATE TABLE TMP_DIM_MDG_PARTID_BILL as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
    dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;

UPDATE fact_billing f
   SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
       dw_update_date = current_timestamp
  FROM TMP_DIM_MDG_PARTID_BILL tmp,fact_billing f
 WHERE f.dim_partid = tmp.dim_partid
   AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);
DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_BILL;

/* Updates into fact_salesorder from fact_billing*/
DROP TABLE IF EXISTS TMP_UPDT_SALES_BIL;
CREATE TABLE TMP_UPDT_SALES_BIL
AS
SELECT fb.dd_salesdocno,fb.dd_salesitemno,max(fb.dd_billing_no) dd_billing_no FROM fact_billing fb GROUP BY fb.dd_salesdocno,fb.dd_salesitemno;

UPDATE fact_salesorder fso
   SET fso.dd_billing_no = fb.dd_billing_no
  FROM TMP_UPDT_SALES_BIL fb, fact_salesorder fso
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND ifnull(fso.dd_billing_no,-1) <> ifnull(fb.dd_billing_no,-2);

DROP TABLE IF EXISTS TMP_UPDT_SALES_BIL;

/* Updates into fact_salesorderdelivery from fact_billing*/
UPDATE fact_salesorderdelivery sod
   SET sod.dd_billing_no = fb.dd_billing_no,
       sod.dd_billingitem_no = fb.dd_billing_item_no
  FROM fact_billing fb,dim_documentcategory dc,
       dim_billingmisc bm,fact_salesorderdelivery sod
 WHERE fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
   AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
   AND dc.dim_documentcategoryid = fb.dim_documentcategoryid
   AND dc.DOCUMENTCATEGORY IN ('F1','F2','ZF2','ZF2C')
   AND bm.dim_billingmiscid = fb.dim_billingmiscid
   AND bm.CancelFlag = 'Not Set';

/* Inv Price/Cogs */

drop table if exists tmp_inv_cogs;
create table tmp_inv_cogs
as
select
	dim_partid
	,MAX(amt_cogsfixedplanrate_emd) COGS
	,max(cast(amt_StdUnitPrice * amt_exchangerate_gbl as decimal (18,4))) stdPrice
	,SUM(cast(amt_OnHand * amt_exchangerate_gbl as decimal (18,4))) OHValue
	,SUM(ct_StockQty + ct_StockInQInsp + ct_BlockedStock + ct_StockInTransit + ct_StockInTransfer + ct_TotalRestrictedStock) Qty
	,MIN((CASE WHEN mdg_part.PPU = 0 THEN 1 ELSE mdg_part.PPU END) * (CASE WHEN mdg_part.ACT_CONV = 0 THEN 1 ELSE mdg_part.ACT_CONV END)) PPU
from fact_inventoryaging f
	inner join dim_mdg_part mdg_part on f.dim_mdg_partid = mdg_part.dim_mdg_partid
group by dim_partid;

insert into tmp_inv_cogs (dim_partid,COGS,stdPrice,OHValue,Qty,PPU)
select f.dim_partid,MAX(ifnull(c.Z_GCPLFF,0)),0,0,0,MIN((CASE WHEN mdg_part.PPU = 0 THEN 1 ELSE mdg_part.PPU END) * (CASE WHEN mdg_part.ACT_CONV = 0 THEN 1 ELSE mdg_part.ACT_CONV END))
from fact_billing f
	inner join dim_mdg_part mdg_part on f.dim_mdg_partid = mdg_part.dim_mdg_partid
	inner join dim_part dp on f.dim_partid = dp.dim_partid
	inner join dim_company dc on f.dim_companyid = dc.dim_companyid
	left join csv_cogs c on lpad(c.PRODUCT,18,'0') = lpad(dp.partnumber,18,'0') and right('000000' || c.Z_REPUNIT, 6) = dc.company
where f.dim_partid not in (select x.dim_partid from tmp_inv_cogs x)
group by f.dim_partid;

MERGE INTO fact_billing f
	USING tmp_inv_cogs t ON f.dim_partid = t.dim_partid
WHEN MATCHED THEN UPDATE
	SET f.amt_invcogsprice = CASE WHEN t.COGS =0 AND t.stdPrice = 0 AND t.Qty <> 0 THEN cast(t.OHValue / t.Qty as decimal (18,4)) WHEN t.COGS =0 AND t.stdPrice <> 0 THEN t.stdPrice ELSE t.COGS END;



/*add DIM_dateidactualgimerckls - from sales Roxana D 2017-11-13  */

drop table if exists tmp_actualdategi_merckls;
create table tmp_actualdategi_merckls
as select distinct f.dd_salesdocno,
f.DD_SALESITEMNO,
first_value(f.DIM_DATEIDACTUALGI) over (partition by f.dd_salesdocno, f.DD_SALESITEMNO order by dd.datevalue desc) as DD_DATEIDACTUALGI
from fact_salesorder f
inner join dim_date dd on f.DIM_DATEIDACTUALGI = dd.dim_dateid;

update fact_billing f
set f.dim_dateidactualgimerckls = ifnull(t.DD_DATEIDACTUALGI, 1)
from fact_billing f, tmp_actualdategi_merckls t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_dateidactualgimerckls <> ifnull(t.DD_DATEIDACTUALGI, 1);

/*End DIM_dateidactualgimerckls */
/*Add dim_bwproducthierarchyid, dim_commercialviewid - from sales  Roxana D 2017-11-13  */


update fact_billing f
set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_billing f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);

update fact_billing f
set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_billing f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);


update fact_billing f
	set f.dim_commercialviewid = ifnull(ds.dim_commercialviewid, 1)
	from dim_commercialview ds, dim_customer a, dim_bwproducthierarchy b, fact_billing f
	where ds.SOLDTOCOUNTRY=a.COUNTRY
	and ds.UPPERPRODHIER=b.BUSINESS
	and f.DIM_BWPRODUCTHIERARCHYID=b.DIM_BWPRODUCTHIERARCHYID
	and f.dim_customerid=a.dim_customerid
	and f.dim_commercialviewid <> ifnull(ds.dim_commercialviewid, 1);

/*End dim_bwproducthierarchyid, dim_commercialviewid */


/*add dim_accordinggidatemto_emd, dim_dateidexpectedship_emd, dim_mercklsconforreqdateid - from sales Roxana D 2017-11-14  */

drop table if exists tmp_accordinggimto_merckls;
create table tmp_accordinggimto_merckls
as select distinct f.dd_salesdocno,
f.DD_SALESITEMNO,
first_value(f.dim_accordinggidatemto_emd) over (partition by f.dd_salesdocno, f.DD_SALESITEMNO order by dmto.datevalue desc) as dim_accordinggidatemto_emd,
first_value(f.dim_dateidexpectedship_emd) over (partition by f.dd_salesdocno, f.DD_SALESITEMNO order by dmts.datevalue desc) as dim_dateidexpectedship_emd,
first_value(f.dim_mercklsconforreqdateid) over (partition by f.dd_salesdocno, f.DD_SALESITEMNO order by dreq.datevalue desc) as dim_mercklsconforreqdateid,
first_value(f.dim_clusterid) over (partition by f.dd_salesdocno, f.DD_SALESITEMNO order by dc.clust desc) as dim_clusterid
from fact_salesorder f
left join dim_date_factory_calendar dmto on f.dim_accordinggidatemto_emd = dmto.dim_dateid
left join dim_date_factory_calendar dmts on f.dim_dateidexpectedship_emd = dmts.dim_dateid
left join dim_date_factory_calendar dreq on f.dim_mercklsconforreqdateid = dreq.dim_dateid
left join dim_cluster dc on f.dim_clusterid = dc.dim_clusterid;

update fact_billing f
set f.dim_accordinggidatemto_emd = ifnull(t.dim_accordinggidatemto_emd, 1)
from fact_billing f, tmp_accordinggimto_merckls t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_accordinggidatemto_emd <> ifnull(t.dim_accordinggidatemto_emd, 1);

update fact_billing f
set f.dim_dateidexpectedship_emd = ifnull(t.dim_dateidexpectedship_emd, 1)
from fact_billing f, tmp_accordinggimto_merckls t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_dateidexpectedship_emd <> ifnull(t.dim_dateidexpectedship_emd, 1);

update fact_billing f
set f.dim_mercklsconforreqdateid = ifnull(t.dim_mercklsconforreqdateid, 1)
from fact_billing f, tmp_accordinggimto_merckls t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_mercklsconforreqdateid <> ifnull(t.dim_mercklsconforreqdateid, 1);

update fact_billing f
set f.dim_clusterid = ifnull(t.dim_clusterid, 1)
from fact_billing f, tmp_accordinggimto_merckls t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_clusterid <> ifnull(t.dim_clusterid, 1);

drop table if exists tmp_actualdategi_merckls;
drop table if exists tmp_accordinggimto_merckls;

/*End dim_accordinggidatemto_emd, dim_dateidexpectedship_emd, dim_mercklsconforreqdateid */

/*Add dd_ace_openorders from fact_salesorder Roxana D 2017-11-23 */


update fact_billing f
set f.dd_ace_openorders = ifnull(t.dd_ace_openorders,'Not Set')
from fact_billing f, (SELECT dd_salesdocno,DD_SALESITEMNO, MAX(dd_ace_openorders) AS dd_ace_openorders FROM fact_salesorder GROUP BY dd_salesdocno,DD_SALESITEMNO) t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dd_ace_openorders <> ifnull(t.dd_ace_openorders,'Not Set');


/*End dd_ace_openorders*/



/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate_billing;
create table tmp_updatedate_billing as
select max(dw_update_date) dd_updatedate from  fact_billing;


update fact_billing f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_billing f, tmp_updatedate_billing t
where f.dd_updatedate <> t.dd_updatedate;
