UPDATE fact_mrp m
SET m.Dim_ItemCategoryid = p.Dim_ItemCategoryid
From fact_purchase p,
	tmpvariable_00e,
        fact_mrp m	
WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    AND m.Dim_ItemCategoryid <> p.Dim_ItemCategoryid;


UPDATE fact_mrp m
SET  m.Dim_Vendorid = p.Dim_Vendorid
From fact_purchase p,
     tmpvariable_00e,
     fact_mrp m
WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    AND m.Dim_Vendorid <> p.Dim_Vendorid;



UPDATE fact_mrp m
SET m.Dim_DocumentTypeid = p.Dim_DocumentTypeid
From fact_purchase p,
	 tmpvariable_00e,
     fact_mrp m
WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_DocumentTypeid <> p.Dim_DocumentTypeid;


DROP TABLE IF EXISTS tmp_upd_leadtimevar_mrp;
CREATE TABLE tmp_upd_leadtimevar_mrp
AS
SELECT DISTINCT m.dd_DocumentNo,m.dd_PlannScenarioLTP,m.Dim_Partid,MDTB_UMDAT,MDTB_DAT01,MDTB_DAT02,p.LeadTime, k.MDKP_DTNUM, t.MDTB_DELKZ, t.mdtb_dtpos, k.mdkp_matnr, 
k.mdkp_plwrk, t.MDTB_DELNR, t.MDTB_DELPS,t.MDTB_DELET, mdtb_dat00
FROM fact_mrp m,mdkp k,mdtb t,dim_part p
WHERE m.Dim_ActionStateid = 2
AND m.dd_DocumentNo = t.MDTB_DELNR
AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
AND k.MDKP_DTNUM = t.MDTB_DTNUM
AND m.Dim_Partid = p.Dim_Partid
AND p.partnumber = k.mdkp_matnr
AND p.plant = k.mdkp_plwrk;

DELETE FROM tmp_upd_leadtimevar_mrp a
WHERE EXISTS ( SELECT 1 FROM tmp_upd_leadtimevar_mrp b
        WHERE a.dd_DocumentNo = b.dd_DocumentNo AND a.dd_PlannScenarioLTP = b.dd_PlannScenarioLTP AND a.MDKP_DTNUM = b.MDKP_DTNUM AND a.MDTB_DELKZ = b.MDTB_DELKZ
        AND a.Dim_Partid = b.Dim_Partid AND a.MDTB_DELET = b.MDTB_DELET
        AND b.mdtb_dtpos > a.mdtb_dtpos);

DROP TABLE IF EXISTS tmp_mdkp_mdtb_distinct;
CREATE TABLE tmp_mdkp_mdtb_distinct
AS
SELECT t.*,k.*,pl.dim_plantid, dp.dim_partid
FROM mdtb t, mdkp k, dim_plant pl, dim_part dp
WHERE t.MDTB_DTNUM = k.mdkp_dtnum AND k.mdkp_matnr = dp.partnumber AND dp.plant = k.mdkp_plwrk AND k.mdkp_plwrk = pl.plantcode;

DELETE FROM tmp_mdkp_mdtb_distinct a
WHERE EXISTS ( SELECT 1 FROM tmp_mdkp_mdtb_distinct b WHERE a.mdtb_delnr = b.mdtb_delnr AND a.mdtb_delps = b.mdtb_delps AND a.mdtb_delet = b.mdtb_delet
		AND a.mdtb_dtnum = b.mdtb_dtnum AND a.dim_plantid = b.dim_plantid AND a.dim_partid = b.dim_partid
		AND a.MDTB_DELKZ = b.MDTB_DELKZ AND a.MDKP_PLSCN = b.MDKP_PLSCN AND a.MDTB_MNG01 = b.MDTB_MNG01
		AND a.mdtb_dtpos < b.mdtb_dtpos );


MERGE INTO fact_mrp fact
USING (SELECT t1.fact_mrpid, 
			ifnull((t1.DateValue - ifnull(t1.MDTB_UMDAT, t1.MDTB_DAT00)), 0) 
			- ifnull(t2.v_leadTime, 0) ct_leadTimeVariance
		FROM (SELECT m.fact_mrpid, dt.DateValue, t.MDTB_UMDAT, t.MDTB_DAT00, 
                     t.MDKP_MATNR , t.MDKP_PLWRK, v.vendorNumber, t.MDTB_DELNR, t.MDTB_DELPS
				From fact_purchase p, 
					 tmp_mdkp_mdtb_distinct t, 
				     dim_date dt,
					 tmpvariable_00e,
					 dim_mrpelement me,
					 dim_vendor v, 		
                     fact_mrp m
				WHERE       tmpTotalCount > 0 and m.Dim_ActionStateid = 2
				        AND dt.Dim_Dateid <> 1 
						AND m.dd_DocumentNo = p.dd_DocumentNo 
						AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
				        AND t.MDTB_DELNR = m.dd_DocumentNo 
						AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
				        AND t.dim_plantid = m.dim_plantid 
						AND t.dim_partid = m.dim_partid 
						AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
				        AND me.Dim_MRPElementID = m.Dim_MRPElementid
				        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
				        AND p.Dim_DateidDelivery = dt.Dim_Dateid
				        AND t.MDTB_MNG01 = m.ct_QtyMRP
				        AND v.Dim_Vendorid = p.Dim_Vendorid) t1 
	        LEFT JOIN (SELECT v_leadTime, glt.pPart, glt.pPlant, glt.pVendor,
							  glt.DocumentNumber, glt.DocumentLineNumber
						 FROM tmp_getLeadTime glt 
						 WHERE    glt.fact_script_name = 'bi_populate_mrp_fact'
                              AND glt.DocumentType = 'PO') t2
				ON 	  t2.pPart = t1.MDKP_MATNR 
				  AND t2.pPlant = t1.MDKP_PLWRK
				  AND t2.pVendor = t1.vendorNumber
				  AND t2.DocumentNumber = t1.MDTB_DELNR
				  AND t2.DocumentLineNumber = t1.MDTB_DELPS ) src
ON fact.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN UPDATE 
		SET fact.ct_leadTimeVariance = src.ct_leadTimeVariance;


UPDATE fact_mrp m
SET m.amt_ExtendedPrice = ifnull((MDTB_MNG01 * p.amt_UnitPrice), 0)  /* amt_UnitPrice in fact_purchase is in tran curr. So, no need to use exchg rate here */
From fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
    AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
    AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
	AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
	AND me.Dim_MRPElementID = m.Dim_MRPElementid
	AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
	AND p.Dim_DateidDelivery = dt.Dim_Dateid
	AND t.MDTB_MNG01 = m.ct_QtyMRP
    AND m.amt_ExtendedPrice <> ifnull((MDTB_MNG01 * p.amt_UnitPrice), 0);




UPDATE fact_mrp m
SET m.amt_ExtendedPrice_GBL = ifnull((MDTB_MNG01 * p.amt_UnitPrice), 0) * p.amt_ExchangeRate_GBL
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
 WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND t.MDTB_MNG01 = m.ct_QtyMRP
	    AND m.amt_ExtendedPrice_GBL <>
            ifnull((MDTB_MNG01 * p.amt_UnitPrice), 0) * p.amt_ExchangeRate_GBL;


UPDATE fact_mrp m
SET m.dim_consumptiontypeid = p.Dim_ConsumptionTypeid
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND m.dim_consumptiontypeid <> p.Dim_ConsumptionTypeid;



UPDATE fact_mrp m
SET m.amt_UnitStdPrice = p.amt_StdUnitPrice 			/* * p.amt_ExchangeRate */ 
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
    
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND m.amt_UnitStdPrice <> p.amt_StdUnitPrice;

UPDATE fact_mrp m
SET m.Dim_PurchaseGroupid = p.Dim_PurchaseGroupid
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND m.Dim_PurchaseGroupid <> p.Dim_PurchaseGroupid;


UPDATE fact_mrp m
SET Dim_DateidReschedule = 1
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.Dim_DateidReschedule <> 1;

/*  Se fac 2 update uri cu Dim_DateidReschedule = 1 */
UPDATE fact_mrp m
SET Dim_DateidReschedule = 1
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, dim_date od, dim_plant pl, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND pl.dim_plantid = m.dim_plantid AND od.DateValue = t.MDTB_UMDAT AND od.CompanyCode = pl.CompanyCode
AND m.Dim_DateidReschedule <> od.dim_dateid;



UPDATE fact_mrp m
SET m.Dim_DateidPOCreated = p.Dim_DateidCreate
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
	AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND m.Dim_DateidPOCreated <> p.Dim_DateidCreate;


/* Update currency and exchange rates from fact_purchase for rows updated above */

UPDATE fact_mrp m
SET m.dim_currencyid = p.dim_currencyid
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
	AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND m.dim_currencyid <> p.dim_currencyid;



UPDATE fact_mrp m
SET m.dim_currencyid_tra = p.dim_currencyid_tra
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me,  fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
	AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND m.dim_currencyid_tra <> p.dim_currencyid_tra;

UPDATE fact_mrp m
SET m.amt_ExchangeRate = p.amt_ExchangeRate
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me , fact_mrp m
WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
	AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND m.amt_ExchangeRate <> p.amt_ExchangeRate;

UPDATE fact_mrp m
SET m.amt_ExchangeRate_GBL = p.amt_ExchangeRate_GBL
FROM fact_purchase p, tmp_mdkp_mdtb_distinct t, dim_date dt, tmpvariable_00e, dim_mrpelement me, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_DocumentItemNo = p.dd_DocumentItemNo AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
	AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND m.amt_ExchangeRate_GBL <> p.amt_ExchangeRate_GBL;


	
/* End of updates from 	fact_purchase */

UPDATE fact_mrp m
SET amt_ExtendedPrice = ifnull((m.ct_QtyMRP * (p.amt_estimatedTotalCost / (case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end))), 0)
From  fact_productionorder p,tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me, fact_mrp m
WHERE      tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = p.dd_ordernumber AND m.dd_DocumentItemNo = p.dd_orderitemno
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
	AND t.MDTB_MNG01 = m.ct_QtyMRP
	AND amt_ExtendedPrice <> ifnull((m.ct_QtyMRP * (p.amt_estimatedTotalCost / (case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end))), 0);
 


UPDATE fact_mrp m
SET amt_ExtendedPrice_GBL = ifnull((m.ct_QtyMRP * (p.amt_estimatedTotalCost / (case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end))),0) * p.amt_ExchangeRate_GBL
From  fact_productionorder p,tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me, fact_mrp m
WHERE       tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = p.dd_ordernumber AND m.dd_DocumentItemNo = p.dd_orderitemno
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
	    AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND  amt_ExtendedPrice_GBL 
	              <> ifnull((m.ct_QtyMRP * (p.amt_estimatedTotalCost / (case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end))),0) * p.amt_ExchangeRate_GBL;
 


UPDATE fact_mrp m
SET Dim_DateidReschedule = 1
From  fact_productionorder p,tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me, fact_mrp m
WHERE        tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
		 AND m.dd_DocumentNo = p.dd_ordernumber AND m.dd_DocumentItemNo = p.dd_orderitemno
		 AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
		 AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
		 AND me.Dim_MRPElementID = m.Dim_MRPElementid
		 AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
		 AND t.MDTB_MNG01 = m.ct_QtyMRP
		AND m.Dim_DateidReschedule <> 1;


UPDATE fact_mrp m
SET Dim_DateidReschedule = od.dim_dateid
FROM  fact_productionorder p,tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me ,dim_date od, dim_company dc, fact_mrp m
  WHERE      tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
 AND m.dd_DocumentNo = p.dd_ordernumber AND m.dd_DocumentItemNo = p.dd_orderitemno
 AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
 AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
 AND me.Dim_MRPElementID = m.Dim_MRPElementid
 AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
 AND t.MDTB_MNG01 = m.ct_QtyMRP
AND dc.dim_companyid = p.dim_companyid AND od.DateValue = t.MDTB_UMDAT AND od.CompanyCode = dc.CompanyCode
AND m.Dim_DateidReschedule <> od.dim_dateid;


/* Update currency and exchange rates from fact_productionorder */
UPDATE fact_mrp m
SET m.dim_currencyid = p.dim_currencyid
FROM fact_productionorder p, tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me, fact_mrp m
WHERE      tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
		AND m.dd_DocumentNo = p.dd_ordernumber AND m.dd_DocumentItemNo = p.dd_orderitemno
		AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
		AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
		AND me.Dim_MRPElementID = m.Dim_MRPElementid
		AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
		AND t.MDTB_MNG01 = m.ct_QtyMRP
		AND m.dim_currencyid <> p.dim_currencyid;


UPDATE fact_mrp m
SET m.dim_currencyid_tra = p.dim_currencyid_tra
FROM fact_productionorder p, tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me, fact_mrp m
WHERE      tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
		AND m.dd_DocumentNo = p.dd_ordernumber AND m.dd_DocumentItemNo = p.dd_orderitemno
		AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
		AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
		AND me.Dim_MRPElementID = m.Dim_MRPElementid
		AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
		AND t.MDTB_MNG01 = m.ct_QtyMRP
		AND m.dim_currencyid_tra <> p.dim_currencyid_tra;

UPDATE fact_mrp m
SET m.amt_ExchangeRate = p.amt_ExchangeRate
FROM fact_productionorder p, tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me, fact_mrp m
WHERE      tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
		AND m.dd_DocumentNo = p.dd_ordernumber AND m.dd_DocumentItemNo = p.dd_orderitemno
		AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
		AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
		AND me.Dim_MRPElementID = m.Dim_MRPElementid
		AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
		AND t.MDTB_MNG01 = m.ct_QtyMRP
		AND m.amt_ExchangeRate <> p.amt_ExchangeRate;


UPDATE fact_mrp m
SET m.amt_ExchangeRate_GBL = p.amt_ExchangeRate_GBL
FROM fact_productionorder p, tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me, fact_mrp m
WHERE      tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
		AND m.dd_DocumentNo = p.dd_ordernumber AND m.dd_DocumentItemNo = p.dd_orderitemno
		AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
		AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
		AND me.Dim_MRPElementID = m.Dim_MRPElementid
		AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
		AND t.MDTB_MNG01 = m.ct_QtyMRP
		AND m.amt_ExchangeRate_GBL <> p.amt_ExchangeRate_GBL;


		
