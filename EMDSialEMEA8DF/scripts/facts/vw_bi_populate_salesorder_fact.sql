/* ################################################################################################################## */
/* */
/*   Script         : bi_populate_salesorder_fact */
/*   Author         : Ashu */
/*   Created On     : 17 Jan 2013 */
/*  */
/*  */
/*   Description    : Stored Proc bi_populate_salesorder_fact from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By         Version           Desc */
/*   14 Oct 2016     CristianT  1.82              Bring new measures from Stock Analytics to Sales. Requested on BI-4389 */
/*   16-Jun-2016     CatalinM	1.81		  Update dim_AccordingGIDateMTO_emd and dim_dateidexpectedship_emd with nextworkingday from dim_date_factory_calendar */
/*   24-Mar-2015 	 Octavian   1.8 		      Reject Reason updated at the level of Item level instead of Schedule */
/*   25 Feb 2015     Octavian   1.7				  Adding Material Price Group 2 */
/*	 16 Feb 2015     Octavian   1.6				  Adding from PRD script missing dim_MaterialPricingGroupId */
/*   12 Dec 2014     Alex M.    1.53              Added dd_purchaseorderitem */
/*   19 Nov 2014     Victor     1.53              Added dim_CustomerConditionGroups4id,dim_CustomerConditionGroups5id */
/*   09 Sep 2014     Victor     1.52              Added dd_deliveryindicator */
/*   23 Apr 2014     Cornelia		              Added dim_agreementsid population */
/*   27 Mar 2014     Radu		                  Added dd_conditionno population */
/*   78 Mar 2014     Lokesh	    1.49  	          Added ct_CumOrderQty population */
/*   25 Feb 2014     Lokesh	    1.48       		  Use CDPOS for hard-deletes to resolve missing item issue ( identified in textron sales ). */
/*   14 Feb 2014     George     1.47              Added Dim_CustomerGroup4id */
/*   12 Feb 2014     George     1.46              Added Dim_ScheduleDeliveryBlockid */
/*   03 Feb 2014     George     1.45		      Added: dim_CustomerConditionGroups1id, dim_CustomerConditionGroups2id, dim_CustomerConditionGroups3id  */
/*   14 Jan 2014     Cornelia   1.44              Added new fields dd_ProdOrderNo and dd_ProdOrderItemNo*/
/*   29 Dec 2013     Issam	    1.42	          Added fields amt_SubTotal1, amt_SubTotal2, amt_SubTotal5, amt_SubTotal6 */
/*   20 Dec 2013     George     1.41              Added dd_HighLevelItem field										  */
/*   30 Nov 2013     Lokesh	    1.40              Merged changes for  amt_ScheduleTotal and  vbak_vbap_vbep/ vbak_vbap updates */
/*   12 Nov 2013     Lokesh     1.34	          Use vbak_audat instead of vbap_erdat as date for retrieving local/stat exchange rate from tmp_getexchangerate1*/
/*	 26 Sep 2013     Issam      1.33              Added fields dd_SOCreateTime dd_ReqDeliveryTime, dd_SOLineCreateTime,
												  dd_DeliveryTime, dd_PlannedGITime  								  */
/*   07 Sep 2013     Lokesh     1.25              Exchange Rate and Currency changes. Merged prev versions                               */
/*   29 Aug 2013     Lokesh     1.22              Changes done for amt_Subtotal3inCustConfig_Billing and amt_Subtotal3_OrderQty.  */
/*   29 Aug 2013     Shanthi    1.21		 	  Made fixes related to the Textron    */
/*   20 Aug 2013     Shanthi    1.20              added new fields */
/*   11 Mar 2013     Lokesh     1.3               Sync with Shanthi's latest changes in mysql   */
/*   25 Mar 2013     Lokesh     1.2				  Changes to fix Dim_BillToPartyPartnerFunctionId discrepancies - add order by cpf.PartnerCounter desc limit 1 */
/*   20 Mar 2013     Lokesh     1.1               Minor changes, sync with current prod/qa version. */
/*   17 Jan 2013     Ashu       1.0               Existing code migrated to Vectorwise > */
/* #################################################################################################################### */

/* Refresh all tables in VW from MySQL */
/* cd /home/fusionops/ispring/db/schema_migration/bin	*/
/* */

DROP TABLE IF EXISTS variable_holder_701;
CREATE TABLE variable_holder_701
AS
SELECT CONVERT(VARCHAR(3),ifnull((SELECT property_value
                                  FROM systemproperty
                                  WHERE property = 'customer.global.currency'), 'USD')) pGlobalCurrency,
       CONVERT(VARCHAR(10), ifnull((SELECT property_value
                                    FROM systemproperty
                                    WHERE property = 'custom.partnerfunction.key'), 'Not Set')) pCustomPartnerFunctionKey,
       CONVERT(VARCHAR(10), ifnull((SELECT property_value
                                    FROM systemproperty
                                    WHERE property = 'custom.partnerfunction.key1'), 'Not Set')) pCustomPartnerFunctionKey1,
       CONVERT(VARCHAR(10), ifnull((SELECT property_value
                                    FROM systemproperty
                                    WHERE property = 'custom.partnerfunction.key2'), 'Not Set')) pCustomPartnerFunctionKey2,
       CONVERT(VARCHAR(5),'RE') pBillToPartyPartnerFunction,
       CONVERT(VARCHAR(5),'RG') pPayerPartnerFunction;

/* update from item data if it exists (posnr <> 0), otherwise update from header data (posnr  =  0) */
UPDATE vbak_vbap_vbep p
SET p.VBAP_STCUR = k.VBKD_KURSK
FROM vbak_vbap_vbkd k,
     vbak_vbap_vbep p
WHERE p.vbak_vbeln = k.VBKD_VBELN
      AND k.VBKD_POSNR = 0
      AND IFNULL(p.VBAP_STCUR,-1) <> IFNULL(k.VBKD_KURSK,-1);

UPDATE vbak_vbap_vbep p
SET p.PRSDT = k.VBKD_PRSDT
FROM vbak_vbap_vbkd k,
     vbak_vbap_vbep p
WHERE p.vbak_vbeln = k.VBKD_VBELN
      AND k.VBKD_POSNR = 0
      AND IFNULL(p.PRSDT,to_date('01011900','DDMMYYYY')) <> IFNULL(k.VBKD_PRSDT,to_date('01011900','DDMMYYYY'));

UPDATE vbak_vbap p
SET p.VBAP_STCUR = k.VBKD_KURSK
FROM vbak_vbap_vbkd k,
     vbak_vbap p
WHERE p.vbap_vbeln = k.VBKD_VBELN
      AND VBKD_POSNR = 0
      AND IFNULL(p.VBAP_STCUR,-1) <> IFNULL(k.VBKD_KURSK,-1);

UPDATE vbak_vbap p
SET p.PRSDT = k.VBKD_PRSDT
FROM vbak_vbap_vbkd k,
     vbak_vbap p
WHERE p.vbap_vbeln = k.VBKD_VBELN
      AND k.VBKD_POSNR = 0
      AND IFNULL(p.PRSDT,to_date('01011900','DDMMYYYY')) <> IFNULL(k.VBKD_PRSDT,to_date('01011900','DDMMYYYY'));


/* Update item data if it exists. So this will overwrite updates from header, where a match is found */

UPDATE vbak_vbap_vbep p
SET p.VBAP_STCUR = k.VBKD_KURSK
FROM vbak_vbap_vbkd k,
     vbak_vbap_vbep p
WHERE p.vbak_vbeln = k.VBKD_VBELN
      AND p.vbap_posnr = k.VBKD_POSNR
      AND IFNULL(p.VBAP_STCUR,-1) <> IFNULL(k.VBKD_KURSK,-1);

UPDATE vbak_vbap_vbep p
SET p.PRSDT = k.VBKD_PRSDT
FROM vbak_vbap_vbkd k,
     vbak_vbap_vbep p
WHERE p.vbak_vbeln = k.VBKD_VBELN
      AND p.vbap_posnr = k.VBKD_POSNR
      AND IFNULL(p.PRSDT,to_date('01011900','DDMMYYYY')) <> IFNULL(k.VBKD_PRSDT,to_date('01011900','DDMMYYYY'));


UPDATE vbak_vbap p
SET p.VBAP_STCUR = k.VBKD_KURSK
FROM vbak_vbap_vbkd k,
     vbak_vbap p
WHERE p.vbap_vbeln = k.VBKD_VBELN
      AND p.vbap_posnr = k.VBKD_POSNR
      AND IFNULL(p.VBAP_STCUR,-1) <> IFNULL(k.VBKD_KURSK,-1);

UPDATE vbak_vbap p
SET p.PRSDT = k.VBKD_PRSDT
FROM vbak_vbap_vbkd k,
     vbak_vbap p
WHERE p.vbap_vbeln = k.VBKD_VBELN
      AND p.vbap_posnr = k.VBKD_POSNR
      AND IFNULL(p.PRSDT,to_date('01011900', 'ddmmyyyy')) <> IFNULL(k.VBKD_PRSDT,to_date('01011900', 'ddmmyyyy'));


/* Backup rows for tracking before they are DELETEd */
INSERT INTO tmp_fact_salesorder_deleted
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       current_timestamp,
       fact_salesorderid,
       'D'
FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE a.CDPOS_OBJECTID = dd_SalesDocNo
                    AND a.CDPOS_TABNAME = 'VBAK'
                    AND a.CDPOS_CHNGIND  =  'D');

INSERT INTO tmp_fact_salesorder_deleted
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       current_timestamp,
       fact_salesorderid,
       'I'
FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE SUBSTRING(a.CDPOS_TABKEY,4,10)  =  dd_SalesDocNo
                    AND SUBSTRING(a.CDPOS_TABKEY,14,6)  =  dd_SalesItemNo
                    AND a.CDPOS_TABNAME  =  'VBAP'
                    AND a.CDPOS_CHNGIND  =  'D' )
      AND NOT EXISTS (SELECT 1
                      FROM VBAK_VBAP_VBEP
                      WHERE VBAK_VBELN = dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo);

INSERT INTO tmp_fact_salesorder_deleted
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       current_timestamp,
       fact_salesorderid,
       'S'
FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_SalesDocNo
                    AND SUBSTRING(a.CDPOS_TABKEY,14,6)  =  dd_SalesItemNo
                    AND SUBSTRING(a.CDPOS_TABKEY,20,4)  =  dd_scheduleno
                    AND a.CDPOS_TABNAME  =  'VBEP'
                    AND a.CDPOS_CHNGIND  =  'D' )
      AND NOT EXISTS (SELECT 1
                      FROM VBAK_VBAP_VBEP
                      WHERE VBAK_VBELN  =  dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo
                            AND VBEP_ETENR  =  dd_ScheduleNo);

/* DELETE based on docno */
DELETE FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
	          WHERE a.CDPOS_OBJECTID  =  dd_SalesDocNo
                    AND a.CDPOS_TABNAME  =  'VBAK'
                    AND a.CDPOS_CHNGIND  =  'D');

/* DELETE based on itemno */
DELETE FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE SUBSTRING(a.CDPOS_TABKEY,4,10)  =  dd_SalesDocNo
                    AND SUBSTRING(a.CDPOS_TABKEY,14,6)  =  dd_SalesItemNo
                    AND a.CDPOS_TABNAME  =  'VBAP'
                    AND  a.CDPOS_CHNGIND  =  'D' )
      AND NOT EXISTS (SELECT 1
                      FROM VBAK_VBAP_VBEP
                      WHERE VBAK_VBELN  =  dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo);

/* DELETE based on schedule */
DELETE FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE SUBSTRING(a.CDPOS_TABKEY,4,10)  =  dd_SalesDocNo
                    AND SUBSTRING(a.CDPOS_TABKEY,14,6)  =  dd_SalesItemNo
                    AND SUBSTRING(a.CDPOS_TABKEY,20,4)  =  dd_scheduleno
                    AND a.CDPOS_TABNAME  =  'VBEP'
                    AND  a.CDPOS_CHNGIND  =  'D' )
      AND NOT EXISTS (SELECT 1
                      FROM VBAK_VBAP_VBEP
                      WHERE VBAK_VBELN  =  dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo
                            AND VBEP_ETENR  =  dd_ScheduleNo);

/* ################################### 12 Apr changes - sync with mysql changes	by Shanthi part 2 starts ###################################	*/
/* Create temporary tables to avoide multiple joins for updating dd_CreditLimit, Dim_CustomerRiskCategoryId and dd_CreditRep */

/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/

DROP TABLE IF EXISTS TMP_UPD_dd_CreditRep1;
CREATE TABLE TMP_UPD_dd_CreditRep1
AS
SELECT CASE
         WHEN b.NAME_FIRST IS NULL THEN b.NAME_LAST
		 ELSE b.NAME_FIRST || ' ' || IFNULL(b.NAME_LAST,'')
	   END upd_dd_CreditRep1,
       d.BUT050_PARTNER2
FROM cvi_cust_link c
     INNER JOIN but000 b1 ON c.PARTNER_GUID = b1.PARTNER_GUID
     INNER JOIN BUT050 d ON b1.PARTNER = d.BUT050_PARTNER2
     INNER JOIN but000 b ON b.PARTNER = d.BUT050_PARTNER1
WHERE current_date BETWEEN d.BUT050_DATE_FROM AND d.BUT050_DATE_TO
      AND b.NAME_FIRST IS NOT NULL
	  OR b.NAME_LAST IS NOT NULL
	  AND b.BU_GROUP = 'CRED';


/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/

/* Begin 11 Dec 2013 */
DROP TABLE IF EXISTS TMP_UPD_dd_CreditMgr1;
CREATE TABLE TMP_UPD_dd_CreditMgr1
AS
SELECT (CASE
          WHEN b.MC_NAME1 IS NULL THEN b.MC_NAME2
		  ELSE b.MC_NAME1 ||' '|| IFNULL(b.MC_NAME2,'')
		END) upd_dd_CreditMgr1,
        d.BUT050_PARTNER2
FROM cvi_cust_link c
     INNER JOIN but000 b1 ON c.PARTNER_GUID  =  b1.PARTNER_GUID
	 INNER JOIN BUT050 d ON b1.PARTNER  =  d.BUT050_PARTNER2
	 INNER JOIN BUT050 d1 ON d1.BUT050_PARTNER2  =  d.BUT050_PARTNER1
	 INNER JOIN but000 b ON b.PARTNER  =  d1.BUT050_PARTNER1
WHERE current_date BETWEEN TO_DATE(d.BUT050_DATE_FROM) AND TO_DATE(d.BUT050_DATE_TO)
      AND (b.MC_NAME1 IS NOT NULL OR b.MC_NAME2 IS NOT NULL);

/* End 11 Dec 2013 */

DROP TABLE IF EXISTS TMP_UPD_dd_CreditLimit;
CREATE TABLE TMP_UPD_dd_CreditLimit
AS
SELECT c.CUSTOMER,
       a.CREDIT_LIMIT
FROM UKMBP_CMS_SGM a
     INNER JOIN BUT000 b ON a.PARTNER = b.PARTNER AND current_date <=  a.LIMIT_VALID_DATE
     INNER JOIN cvi_cust_link c ON c.PARTNER_GUID  =  b.PARTNER_GUID;

/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/
DROP TABLE IF EXISTS TMP_UPD_Dim_CustomerRiskCategoryId;
CREATE TABLE TMP_UPD_Dim_CustomerRiskCategoryId
AS
SELECT c.CUSTOMER,
       src.CreditControlArea,
       src.dim_salesriskcategoryid
FROM ukmbp_cms a
     INNER JOIN BUT000 b ON a.PARTNER = b.PARTNER
     INNER JOIN cvi_cust_link c ON c.PARTNER_GUID = b.PARTNER_GUID
     INNER JOIN dim_salesriskcategory src ON src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent  =  1;

/*###################################End of 12 Apr tmp table creation ###################################*/

DROP TABLE IF EXISTS STAGING_UPDATE_701;
DROP TABLE IF EXISTS VBAK_VBAP_VBEP_701;

CREATE TABLE VBAK_VBAP_VBEP_701
AS
SELECT MIN(x.VBEP_ETENR) v_SalesSchedNo,
       x.VBAK_VBELN v_SaleseDocNo,
	   x.VBAP_POSNR v_SalesItemNo
FROM VBAK_VBAP_VBEP x
GROUP BY x.VBAK_VBELN,
         x.VBAP_POSNR;

CREATE TABLE staging_update_701
AS
SELECT vbep_wmeng as ct_ScheduleQtySalesUnit,
	   vbep_bmeng as ct_ConfirmedQty,
       vbep_cmeng as ct_CorrectedQty,
	   ifnull(convert(numeric(18,4), vbap_netpr), 0) as amt_UnitPrice,	--LK: Removed multiplication with stat exchg rate
       vbap_kpein as ct_PriceUnit,
	   convert(numeric(18,4),CASE
	             WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			       THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )),2), 0)
			     WHEN vbep_wmeng  =  VBAP_KWMENG
				   THEN VBAP_NETWR
			     ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end )), 0)
		      END) as amt_ScheduleTotal,
       convert(numeric(18,4),CASE
	              WHEN VBEP_ETENR  =  y.v_SalesSchedNo THEN vbap_wavwr
			      ELSE 0
		        END) as amt_StdCost,
	   convert(numeric(18,4),CASE
	              WHEN VBEP_ETENR  =  y.v_SalesSchedNo THEN vbap_zwert
		          ELSE 0
		        END) as amt_TargetValue,
       ifnull(convert(numeric(18,4),((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0)  =  0 THEN 1
						          WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						            THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))  =  0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )) END)),4),1)
								           WHEN 0 THEN 1
								           ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))  =  0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )) END)),4),1)
							             END
						          ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end))  =  0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)) END)),1)
					        END)* vbep_bmeng)), 0) amt_Tax,
       CASE
          WHEN VBEP_ETENR  =  y.v_SalesSchedNo THEN vbap_zmeng
	      ELSE 0
	   END  ct_TargetQty,
       convert(numeric(18,4), 1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =   co.Currency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate,
       convert(numeric(18,4), 1) as amt_ExchangeRate_GBL, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate_GBL,
       vbap_uebto ct_OverDlvrTolerance,
       vbap_untto ct_UnderDlvrTolerance,
       convert(bigint, 1) as Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint, 1) as Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
	   dd_SalesDocNo,
	   dd_SalesItemNo,
	   dd_ScheduleNo,
	   convert(bigint, 1) as Dim_UnitOfMeasureId, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_kmein AND uom.RowIsCurrent  =  1), 1) Dim_UnitOfMeasureId,
	   convert(bigint, 1) as Dim_BaseUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_meins AND uom.RowIsCurrent  =  1), 1) Dim_BaseUoMid,
	   convert(bigint, 1) as Dim_SalesUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_vrkme AND uom.RowIsCurrent  =  1), 1) Dim_SalesUoMid
	   convert(numeric(18,4),CASE
	            WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			      THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			    ELSE ifnull(vbap_netpr, 0)
		       END) amt_UnitPriceUoM,	/*LK: 8 Sep 2013: Removed multiplication by stat exch rate*/
	/* LK: 8 Sep 2013: Added 4 new columns */
       convert(bigint, 1) as Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  VBAP_WAERK),1) Dim_Currencyid_TRA,
       convert(bigint, 1) as Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  co.Currency),1) Dim_Currencyid,
       convert(bigint, 1) as dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pGlobalCurrency),1) dim_Currencyid_GBL,
       convert(bigint, 1) as dim_currencyid_STAT, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  vbak_stwae),1) dim_currencyid_STAT,
       convert(numeric(18,4), 1) as amt_exchangerate_STAT, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate  =  0 AND z.pToCurrency  =  vbak_stwae),1) amt_exchangerate_STAT,
       ifnull(VBAP_KWMENG,0) ct_CumOrderQty,
	   VBAP_WAERK,
       PRSDT,
       vbak_audat,
       vbap_erdat,
       VBAP_STADAT,
	   pl.CompanyCode as CompanyCode,
       vbap_kmein,
       vbap_meins,
       vbap_vrkme,
       co.Currency as Currency,
       pGlobalCurrency,
       vbak_stwae
from fact_salesorder so,
	 VBAK_VBAP_VBEP,
	 Dim_Plant pl,
	 Dim_Company co,
	 Dim_SalesOrderItemCategory soic,
	 VBAK_VBAP_VBEP_701 y,
	 variable_holder_701
WHERE pl.PlantCode  =  VBAP_WERKS
      AND co.CompanyCode  =  pl.CompanyCode
	  and VBAK_VBELN  =  dd_SalesDocNo
	  and VBAP_POSNR  =  dd_SalesItemNo
	  and VBEP_ETENR  =  dd_ScheduleNo
	  and VBAK_VBELN  =  y.v_SaleseDocNo
	  and VBAP_POSNR  =  y.v_SalesItemNo
	  AND VBEP_ETENR  =  y.v_SalesSchedNo
	  AND soic.SalesOrderItemCategory  =  VBAP_PSTYV
	  AND soic.RowIsCurrent  =  1;

INSERT INTO staging_update_701(
ct_ScheduleQtySalesUnit,
ct_ConfirmedQty,
ct_CorrectedQty,
amt_UnitPrice,
ct_PriceUnit,
amt_ScheduleTotal,
amt_StdCost,
amt_TargetValue,
amt_Tax,
ct_TargetQty,
amt_ExchangeRate,
amt_ExchangeRate_GBL,
ct_OverDlvrTolerance,
ct_UnderDlvrTolerance,
Dim_DateidSalesOrderCreated,
Dim_DateidFirstDate,
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo,
Dim_UnitOfMeasureId,
Dim_BaseUoMid,
Dim_SalesUoMid,
amt_UnitPriceUoM,
dim_Currencyid_TRA,
dim_Currencyid,
dim_Currencyid_GBL,
dim_currencyid_STAT,
amt_exchangerate_STAT,
ct_CumOrderQty,
VBAP_WAERK,
PRSDT,
vbak_audat,
vbap_erdat,
VBAP_STADAT,
CompanyCode,
vbap_kmein,
vbap_meins,
vbap_vrkme,
Currency,
pGlobalCurrency,
vbak_stwae
)
SELECT vbep_wmeng as ct_ScheduleQtySalesUnit,
	   vbep_bmeng as ct_ConfirmedQty,
       vbep_cmeng as ct_CorrectedQty,
	   ifnull(convert(numeric(18,4),vbap_netpr), 0) as amt_UnitPrice,
       vbap_kpein ct_PriceUnit,
       convert(numeric(18,4), CASE
                                WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			                     THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )),2), 0)
			                    WHEN vbep_wmeng  =  VBAP_KWMENG THEN VBAP_NETWR
			                    ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end )), 0)
		                      END) as amt_ScheduleTotal,
       convert(numeric(18,4), 0) as amt_StdCost,
       convert(numeric(18,4), 0) as amt_TargetValue,
	   ifnull(convert(numeric(18,4),((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0)  =  0 THEN 1
						WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						THEN ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))  =  0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )) END)),4),1)
						ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end))  =  0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)) END)),1)
					 END)
			* (vbep_bmeng))), 0) amt_Tax,
       0 ct_TargetQty,
       convert(numeric(18,4),1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =   co.Currency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate,
       convert(numeric(18,4),1) as amt_ExchangeRate_GBL, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate_GBL,
       vbap_uebto as ct_OverDlvrTolerance,
       vbap_untto as ct_UnderDlvrTolerance,
       convert(bigint,1) as Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint,1) as Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
	   dd_SalesDocNo,
	   dd_SalesItemNo,
	   dd_ScheduleNo,
	   convert(bigint,1) as Dim_UnitOfMeasureId, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_kmein AND uom.RowIsCurrent  =  1), 1) Dim_UnitOfMeasureId,
	   convert(bigint,1) as Dim_BaseUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_meins AND uom.RowIsCurrent  =  1), 1) Dim_BaseUoMid,
	   convert(bigint,1) as Dim_SalesUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_vrkme AND uom.RowIsCurrent  =  1), 1) Dim_SalesUoMid,
	   convert(numeric(18,4),CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END) amt_UnitPriceUoM,
       convert(bigint,1) as Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  VBAP_WAERK),1) Dim_Currencyid_TRA,
       convert(bigint,1) as Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  co.currency ),1) Dim_Currencyid,
	   convert(bigint,1) as dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pGlobalCurrency),1) dim_Currencyid_GBL,
       convert(bigint,1) as dim_currencyid_STAT, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  vbak_stwae),1) dim_currencyid_STAT,
       convert(numeric(18,4),1) as amt_exchangerate_STAT, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate  =  0 AND z.pToCurrency  =  vbak_stwae),1) amt_exchangerate_STAT,
       ifnull(VBAP_KWMENG,0) ct_CumOrderQty,
	   VBAP_WAERK,
       PRSDT,
       vbak_audat,
       vbap_erdat,
       VBAP_STADAT,
	   pl.CompanyCode as CompanyCode,
       vbap_kmein,
       vbap_meins,
       vbap_vrkme,
       co.Currency as Currency,
       pGlobalCurrency,
       vbak_stwae
FROM fact_salesorder so,
	 VBAK_VBAP_VBEP,
	 Dim_Plant pl,
	 Dim_Company co,
	 Dim_SalesOrderItemCategory soic,
	 variable_holder_701
WHERE pl.PlantCode  =  VBAP_WERKS
      AND co.CompanyCode  =  pl.CompanyCode
      AND VBAK_VBELN  =  dd_SalesDocNo
	  AND VBAP_POSNR  =  dd_SalesItemNo
	  AND VBEP_ETENR  =  dd_ScheduleNo
      AND soic.SalesOrderItemCategory  =  VBAP_PSTYV
	  AND soic.RowIsCurrent  =  1
      AND NOT EXISTS (SELECT 1
	                  FROM VBAK_VBAP_VBEP_701 y
			          WHERE VBAK_VBELN = y.v_SaleseDocNo
					        and VBAP_POSNR = y.v_SalesItemNo
							AND VBEP_ETENR = y.v_SalesSchedNo);

/* 22 Dec 2015 CristianT Start: Replaced subqueries from the above 2 inserts into updates */

UPDATE staging_UPDATE_701 sut
SET sut.amt_ExchangeRate = ifnull(z.exchangeRate, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' and pFromExchangeRate = 0) z
	     ON z.pFromCurrency = sut.VBAP_WAERK AND z.pDate = ifnull(sut.PRSDT,sut.vbak_audat) AND z.pToCurrency = sut.Currency
WHERE sut.amt_ExchangeRate <> ifnull(z.exchangeRate, 1);

UPDATE staging_UPDATE_701 sut
SET sut.amt_ExchangeRate_GBL = 1
WHERE sut.amt_ExchangeRate_GBL <> 1;

UPDATE staging_UPDATE_701 sut
SET sut.Dim_DateidSalesOrderCreated = ifnull(dd.dim_dateid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_date dd ON dd.datevalue = sut.vbap_erdat AND dd.companycode = sut.companycode
WHERE sut.Dim_DateidSalesOrderCreated <> ifnull(dd.dim_dateid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_DateidFirstDate = ifnull(dd.dim_dateid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_date dd ON dd.datevalue = sut.VBAP_STADAT AND dd.companycode = sut.companycode
WHERE sut.Dim_DateidFirstDate <> ifnull(dd.dim_dateid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_UnitOfMeasureId = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom
	     ON uom.UOM = sut.vbap_kmein
WHERE sut.Dim_UnitOfMeasureId <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_BaseUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom
	     ON uom.UOM = sut.vbap_meins
WHERE sut.Dim_BaseUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_SalesUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom
	     ON uom.UOM = sut.vbap_vrkme
WHERE sut.Dim_SalesUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_Currencyid_TRA = ifnull(cur.dim_currencyid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_currency cur ON cur.CurrencyCode = sut.VBAP_WAERK
WHERE sut.Dim_Currencyid_TRA <> ifnull(cur.dim_currencyid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_Currencyid = ifnull(cur.dim_currencyid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_currency cur ON cur.CurrencyCode = sut.currency
WHERE sut.Dim_Currencyid <> ifnull(cur.dim_currencyid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.dim_Currencyid_GBL = ifnull(cur.dim_currencyid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_currency cur ON cur.CurrencyCode = sut.pGlobalCurrency
WHERE sut.dim_Currencyid_GBL <> ifnull(cur.dim_currencyid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.dim_currencyid_STAT = ifnull(cur.dim_currencyid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_currency cur ON cur.CurrencyCode = sut.vbak_stwae
WHERE sut.dim_currencyid_STAT <> ifnull(cur.dim_currencyid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.amt_exchangerate_STAT = ifnull(z.exchangeRate, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name  =  'bi_populate_salesorder_fact' AND pFromExchangeRate = 0) z
	     ON z.pFromCurrency = sut.VBAP_WAERK AND z.pDate = ifnull(sut.PRSDT,sut.vbak_audat) AND z.pToCurrency = sut.vbak_stwae
WHERE sut.amt_exchangerate_STAT <> ifnull(z.exchangeRate, 1);

/* 22 Dec 2015 CristianT End: Replaced subqueries from the above 2 inserts into updates */

DROP TABLE IF EXISTS VBAK_VBAP_VBEP_701;

MERGE INTO fact_salesorder so
USING staging_UPDATE_701 sut ON so.dd_SalesDocNo = sut.dd_SalesDocNo AND so.dd_SalesItemNo = sut.dd_SalesItemNo AND so.dd_ScheduleNo = sut.dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET
	so.ct_ScheduleQtySalesUnit = IFNULL(sut.ct_ScheduleQtySalesUnit, 0)
	,so.ct_ConfirmedQty = IFNULL(sut.ct_ConfirmedQty, 0)
	,so.ct_CorrectedQty = IFNULL(sut.ct_CorrectedQty, 0)
	,so.amt_UnitPrice = IFNULL(sut.amt_UnitPrice, 0)
	,so.amt_UnitPriceUoM = IFNULL(sut.amt_UnitPriceUoM, 0)
	,so.ct_PriceUnit = IFNULL(sut.ct_PriceUnit, 0)
	,so.amt_ScheduleTotal = IFNULL(sut.amt_ScheduleTotal, 0)
	,so.amt_StdCost = IFNULL(sut.amt_StdCost, 0)
	,so.amt_TargetValue  =  IFNULL(sut.amt_TargetValue, 0)
	,so.amt_Tax  =  IFNULL(sut.amt_Tax, 0)
	,so.ct_TargetQty  =  IFNULL(sut.ct_TargetQty, 0)
	,so.amt_ExchangeRate  =  IFNULL(sut.amt_ExchangeRate, 1)
	,so.amt_ExchangeRate_GBL = IFNULL(sut.amt_ExchangeRate_GBL, 1)
	,so.ct_OverDlvrTolerance = IFNULL(sut.ct_OverDlvrTolerance, 0)
	,so.ct_UnderDlvrTolerance = IFNULL(sut.ct_UnderDlvrTolerance, 0)
	,so.Dim_DateidSalesOrderCreated = IFNULL(sut.Dim_DateidSalesOrderCreated, 1)
	,so.Dim_DateidFirstDate = IFNULL(sut.Dim_DateidFirstDate, 1)
	,so.Dim_UnitOfMeasureId  =  IFNULL(sut.Dim_UnitOfMeasureId, 1)
	,so.Dim_BaseUoMid  =  IFNULL(sut.Dim_BaseUoMid, 1)
	,so.Dim_SalesUoMid  =  IFNULL(sut.Dim_SalesUoMid, 1)
	,so.dim_Currencyid_TRA  =  IFNULL(sut.dim_Currencyid_TRA, 1)
	,so.dim_Currencyid_GBL  =  IFNULL(sut.dim_Currencyid_GBL, 1)
	,so.dim_currencyid_STAT  =  IFNULL(sut.dim_currencyid_STAT, 1)
	,so.amt_exchangerate_STAT  =  IFNULL(sut.amt_exchangerate_STAT, 1)
	,so.dim_Currencyid  =  IFNULL(sut.dim_Currencyid, 1)
	,so.ct_CumOrderQty  =  IFNULL(sut.ct_CumOrderQty, 0);

DROP TABLE IF EXISTS staging_UPDATE_701;

/* ashu temporary code split to handle stringparse issue - Actian is working on to fix it-- */
/*create table staging_update_701 as
Select  ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue  =  vbep_edatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDelivery,
         ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue  =  vbep_wadat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGoodsIssue,
         ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue  =  vbep_mbdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidMtrlAvail,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue  =  vbep_lddat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidLoading,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGuaranteedate,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue  =  vbep_tddat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidTransport,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode  =  vbak_stwae),1) Dim_Currencyid,
           ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy  =  vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid Dim_Plantid,
           co.Dim_Companyid Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode  =  vbap_lgort and sl.plant  =  vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode  =  vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode  =  vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory  =  vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType  =  vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode  =  vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber  =  vbak_kunnr),1) Dim_CustomerID,
          ifnull((SELECT Dim_ScheduleLineCategoryId
                  FROM Dim_ScheduleLineCategory slc
                  WHERE slc.ScheduleLineCategory  =  VBEP_ETTYP),1) Dim_ScheduleLineCategoryId,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode  =  vbak_vkgrp),1) Dim_SalesGroupid,
          1 Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode  =  vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode  =  vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup  =  vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode  =  vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS),1) Dim_Partid,
          ifnull((select Dim_SalesOrderHeaderStatusid
                    from Dim_SalesOrderHeaderStatus sohs
                    where sohs.SalesDocumentNumber  =  VBAK_VBELN),1) Dim_SalesOrderHeaderStatusid,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo
from fact_salesorder so,
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	(select min(x.VBEP_ETENR) _SalesSchedNo, x.VBAK_VBELN _SaleseDocNo, x.VBAP_POSNR _SalesItemNo
	 from VBAK_VBAP_VBEP x group by x.VBAK_VBELN, x.VBAP_POSNR) y
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  dd_SalesDocNo and VBAP_POSNR  =  dd_SalesItemNo and VBEP_ETENR  =  dd_ScheduleNo
      and VBAK_VBELN  =  y._SaleseDocNo and VBAP_POSNR  =  y._SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
*/
/* 22 Dec 2015 CristianT Start: Commented this statement and recreated as SELECT statement
CREATE TABLE staging_update_701(
        dim_dateidscheddeliveryreq integer default 1 not null,
        dim_dateidscheddelivery integer default 1 not null,
        dim_dateidgoodsissue integer default 1 not null,
        dim_dateidmtrlavail integer default 1 not null,
        dim_dateidloading integer default 1 not null,
        dim_dateidguaranteedate integer default 1 not null,
        dim_dateidtransport integer default 1 not null,
        dim_currencyid smallint default 1 not null,
		dim_currencyid_TRA smallint default 1 not null,
		dim_currencyid_GBL smallint default 1 not null,
		dim_currencyid_STAT smallint default 1 not null,
        dim_producthierarchyid integer default 1 not null,
        dim_plantid smallint default 1 not null,
        dim_companyid smallint default 1 not null,
        dim_storagelocationid smallint default 1 not null,
        dim_salesdivisionid integer default 1 not null,
        dim_shipreceivepointid integer default 1 not null,
        dim_documentcategoryid integer default 1 not null,
        dim_salesdocumenttypeid integer default 1 not null,
        dim_salesorgid integer default 1 not null,
        dim_customerid integer default 1 not null,
        dim_schedulelinecategoryid smallint default 1 not null,
        dim_dateidvalidfrom integer default 1 not null,
        dim_dateidvalidto integer default 1 not null,
        dim_salesgroupid integer default 1 not null,
        dim_costcenterid smallint default 1 not null,
        dim_controllingareaid integer default 1 not null,
        dim_billingblockid integer default 1 not null,
        dim_transactiongroupid integer default 1 not null,
        dim_salesorderrejectreasonid integer default 1 not null,
        dim_partid integer default 1 not null,
        dim_salesorderheaderstatusid integer default 1 not null,
        dd_salesdocno varchar(10) default 'Not Set' null,
        dd_salesitemno integer default 0 null,
        dd_scheduleno integer default 0 null
) */

/* Not needed
DROP TABLE IF EXISTS VBAK_VBAP_VBEP_701
CREATE TABLE VBAK_VBAP_VBEP_701
AS
SELECT MIN(x.VBEP_ETENR) v_SalesSchedNo,
       x.VBAK_VBELN v_SaleseDocNo,
	   x.VBAP_POSNR v_SalesItemNo
FROM VBAK_VBAP_VBEP x
GROUP BY x.VBAK_VBELN, x.VBAP_POSNR */


/* 22 Dec 2015 CristianT Start: Commented this insert and recreated as SELECT statement
Insert into staging_update_701 (
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo
)
select
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo
from fact_salesorder so,
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_701 y
Where pl.PlantCode  =  VBAP_WERKS
      and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  dd_SalesDocNo
	  and VBAP_POSNR  =  dd_SalesItemNo
	  and VBEP_ETENR  =  dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo
	  and VBAP_POSNR  =  y.v_SalesItemNo
      AND soic.SalesOrderItemCategory  =  VBAP_PSTYV
	  AND soic.RowIsCurrent  =  1 */

DROP TABLE IF EXISTS staging_update_701;
CREATE TABLE staging_update_701
AS
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       convert(bigint, 1) as dim_dateidscheddeliveryreq,
       convert(bigint, 1) as dim_dateidscheddelivery,
       convert(bigint, 1) as dim_dateidgoodsissue,
       convert(bigint, 1) as dim_dateidmtrlavail,
       convert(bigint, 1) as dim_dateidloading,
       convert(bigint, 1) as dim_dateidguaranteedate,
       convert(bigint, 1) as dim_dateidtransport,
       convert(bigint, 1) as dim_currencyid,
	   convert(bigint, 1) as dim_currencyid_TRA,
	   convert(bigint, 1) as dim_currencyid_GBL,
	   convert(bigint, 1) as dim_currencyid_STAT,
	   convert(bigint, 1) as dim_producthierarchyid,
	   convert(bigint, 1) as dim_plantid,
	   convert(bigint, 1) as dim_companyid,
	   convert(bigint, 1) as dim_storagelocationid,
	   convert(bigint, 1) as dim_salesdivisionid,
	   convert(bigint, 1) as dim_shipreceivepointid,
	   convert(bigint, 1) as dim_documentcategoryid,
	   convert(bigint, 1) as dim_salesdocumenttypeid,
	   convert(bigint, 1) as dim_salesorgid,
	   convert(bigint, 1) as dim_customerid,
	   convert(bigint, 1) as dim_schedulelinecategoryid,
	   convert(bigint, 1) as dim_dateidvalidfrom,
	   convert(bigint, 1) as dim_dateidvalidto,
	   convert(bigint, 1) as dim_salesgroupid,
	   convert(bigint, 1) as dim_costcenterid,
	   convert(bigint, 1) as dim_controllingareaid,
	   convert(bigint, 1) as dim_billingblockid,
	   convert(bigint, 1) as dim_transactiongroupid,
	   convert(bigint, 1) as dim_salesorderrejectreasonid,
	   convert(bigint, 1) as dim_partid,
	   convert(bigint, 1) as dim_salesorderheaderstatusid,
	   pl.PlantCode as plantcode,
	   co.CompanyCode as companycode,
	   VBAP_PSTYV,
	   vbak_vdatu,
       vbep_edatu,
       vbep_wadat,
       vbep_mbdat,
       vbep_lddat,
       vbak_gwldt,
       vbep_tddat,
       VBAP_WAERK,
       vbap_prodh,
       vbak_stwae,
       co.currency as currency,
       vbap_lgort,
       vbap_spart,
       vbap_vstel,
       vbak_vbtyp,
       vbak_auart,
       vbak_vkorg,
       vbak_kunnr,
       VBEP_ETTYP,
       vbak_guebg,
       vbak_gueen,
       vbak_vkgrp,
       vbak_kokrs,
       vbap_faksp,
       vbak_trvog,
       VBAP_MATNR,
       vbak_kostl,
       pGlobalCurrency
FROM fact_salesorder so,
	 VBAK_VBAP_VBEP,
	 Dim_Plant pl,
	 Dim_Company co,
	 Dim_SalesOrderItemCategory soic,
	 variable_holder_701
WHERE pl.PlantCode  =  VBAP_WERKS
      AND co.CompanyCode  =  pl.CompanyCode
      AND VBAK_VBELN  =  dd_SalesDocNo
	  AND VBAP_POSNR  =  dd_SalesItemNo
	  AND VBEP_ETENR  =  dd_ScheduleNo
      AND soic.SalesOrderItemCategory  =  VBAP_PSTYV
	  AND soic.RowIsCurrent  =  1;

UPDATE staging_update_701 sg
SET Dim_DateidSchedDeliveryReq = IFNULL(dd.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_Date dd on dd.DateValue  =  sg.vbak_vdatu AND dd.CompanyCode  =  sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidSchedDeliveryReq,-1) <> IFNULL(dd.Dim_Dateid, -2);

UPDATE staging_update_701 sg
SET sg.Dim_DateidSchedDelivery = IFNULL(dd.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_Date dd on dd.DateValue  =  sg.vbep_edatu AND dd.CompanyCode  =  sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidSchedDelivery,-1) <> IFNULL(dd.Dim_Dateid, -2);

UPDATE staging_update_701 sg
SET sg.Dim_DateidGoodsIssue = IFNULL(dd.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_Date dd on dd.DateValue  =  sg.vbep_wadat AND dd.CompanyCode  =  sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidGoodsIssue,-1) <> IFNULL(dd.Dim_Dateid, -2);

UPDATE staging_update_701 sg
SET sg.Dim_DateidMtrlAvail = IFNULL(dd.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_Date dd on dd.DateValue  =  sg.vbep_mbdat AND dd.CompanyCode  =  sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidMtrlAvail,-1) <> IFNULL(dd.Dim_Dateid, -2);

UPDATE staging_update_701 sg
SET sg.Dim_DateidLoading = IFNULL(dd.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_Date dd on dd.DateValue  =  sg.vbep_lddat AND dd.CompanyCode  =  sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidLoading,-1) <> IFNULL(dd.Dim_Dateid, -2);

UPDATE staging_update_701 sg
SET sg.Dim_DateidGuaranteedate = IFNULL(dd.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_Date dd on dd.DateValue  =  sg.vbak_gwldt AND dd.CompanyCode  =  sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidGuaranteedate,-1) <> IFNULL(dd.Dim_Dateid, -2);

UPDATE staging_update_701 sg
SET sg.Dim_DateidTransport = IFNULL(dd.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_Date dd on dd.DateValue  =  sg.vbep_tddat AND dd.CompanyCode  =  sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidTransport,-1) <> IFNULL(dd.Dim_Dateid, -2);

UPDATE staging_update_701 sg
SET Dim_Currencyid_TRA = IFNULL(cur.Dim_Currencyid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_Currency cur on cur.CurrencyCode  =  sg.VBAP_WAERK
WHERE IFNULL(sg.Dim_Currencyid_TRA,-1) <> IFNULL(cur.Dim_Currencyid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_ProductHierarchyid = IFNULL(ph.Dim_ProductHierarchyid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_ProductHierarchy ph on ph.ProductHierarchy  =  sg.vbap_prodh
WHERE IFNULL(sg.Dim_ProductHierarchyid,-1) <> IFNULL(ph.Dim_ProductHierarchyid,-2);

UPDATE staging_update_701 sg
SET Dim_Currencyid_STAT = IFNULL(cur.Dim_Currencyid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_Currency cur on cur.CurrencyCode  =  sg.vbak_stwae
WHERE IFNULL(sg.Dim_Currencyid_STAT,-1) <>  IFNULL(cur.Dim_Currencyid,-2);

UPDATE staging_update_701 sg
SET Dim_Currencyid = IFNULL(cur.Dim_Currencyid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_Currency cur on cur.CurrencyCode  =  sg.currency
where  IFNULL(sg.Dim_Currencyid,-1) <> IFNULL(cur.Dim_Currencyid,-2);

UPDATE staging_update_701 sg
SET Dim_Currencyid_GBL = IFNULL(cur.Dim_Currencyid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_Currency cur on cur.CurrencyCode  =  sg.pGlobalCurrency
WHERE IFNULL(sg.Dim_Currencyid_GBL,-1) <> IFNULL(cur.Dim_Currencyid,-2);

UPDATE staging_update_701 sg
SET Dim_Plantid  =  IFNULL(pl.Dim_Plantid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_Plant pl on pl.PlantCode  =  sg.plantcode
AND IFNULL(sg.Dim_Plantid,-1) <> IFNULL(pl.Dim_Plantid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_Companyid  =  IFNULL(co.Dim_Companyid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_company co on co.companycode  =  sg.companycode
WHERE IFNULL(sg.Dim_Companyid,-1) <> IFNULL(co.Dim_Companyid,-2);

UPDATE staging_update_701 sg
SET Dim_StorageLocationid = IFNULL(sl.Dim_StorageLocationid, 1)
FROM staging_update_701 sg
     LEFT JOIN  (select locationcode, plant, max(dim_storagelocationid) dim_storagelocationid
from dim_storagelocation group by locationcode, plant) sl
 on sl.LocationCode  =  sg.vbap_lgort and sl.plant  =  sg.plantcode
WHERE IFNULL(sg.Dim_StorageLocationid,-1) <> IFNULL(sl.Dim_StorageLocationid,-2);

UPDATE staging_update_701 sg
SET Dim_SalesDivisionid = IFNULL(sd.Dim_SalesDivisionid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_SalesDivision sd on sd.DivisionCode  =  sg.vbap_spart
AND IFNULL(sg.Dim_SalesDivisionid,-1) <> IFNULL(sd.Dim_SalesDivisionid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_ShipReceivePointid = IFNULL(srp.Dim_ShipReceivePointid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_ShipReceivePoint srp on srp.ShipReceivePointCode  =  sg.vbap_vstel
WHERE IFNULL(sg.Dim_ShipReceivePointid,-1) <>  IFNULL(srp.Dim_ShipReceivePointid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_DocumentCategoryid = IFNULL(dc.Dim_DocumentCategoryid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_DocumentCategory dc on dc.DocumentCategory  = sg.vbak_vbtyp
WHERE IFNULL(sg.Dim_DocumentCategoryid,-1) <> IFNULL(dc.Dim_DocumentCategoryid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_SalesDocumentTypeid = IFNULL(sdt.Dim_SalesDocumentTypeid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_SalesDocumentType sdt on sdt.DocumentType  =  sg.vbak_auart
WHERE IFNULL(sg.Dim_SalesDocumentTypeid,-1) <> IFNULL(sdt.Dim_SalesDocumentTypeid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_SalesOrgid = IFNULL(sog.Dim_SalesOrgid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_SalesOrg sog on sog.SalesOrgCode  = sg.vbak_vkorg
WHERE IFNULL(sg.Dim_SalesOrgid,-1) <> IFNULL(sog.Dim_SalesOrgid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_CustomerID = IFNULL(dim.Dim_CustomerID, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_Customer dim on dim.CustomerNumber  =  sg.vbak_kunnr
WHERE IFNULL(sg.Dim_CustomerID,-1) <> IFNULL(dim.Dim_CustomerID,-2);

UPDATE staging_update_701 sg
SET sg.Dim_ScheduleLineCategoryId = IFNULL(dim.Dim_ScheduleLineCategoryId, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_ScheduleLineCategory dim on dim.ScheduleLineCategory = sg.VBEP_ETTYP
WHERE IFNULL(sg.Dim_ScheduleLineCategoryId,-1) <> IFNULL(dim.Dim_ScheduleLineCategoryId,-2);

UPDATE staging_update_701 sg
SET sg.Dim_DateidValidFrom = IFNULL(vf.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_Date vf on vf.DateValue = sg.vbak_guebg AND vf.CompanyCode = sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidValidFrom,-1) <> IFNULL(vf.Dim_Dateid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_DateidValidTo = IFNULL(vt.Dim_Dateid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_Date vt on vt.DateValue = sg.vbak_gueen AND vt.CompanyCode = sg.CompanyCode
WHERE IFNULL(sg.Dim_DateidValidTo,-1) <> IFNULL(vt.Dim_Dateid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_SalesGroupid  = IFNULL(ssg.Dim_SalesGroupid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_SalesGroup ssg on ssg.SalesGroupCode = sg.vbak_vkgrp
WHERE IFNULL(sg.Dim_SalesGroupid,-1) <> IFNULL(ssg.Dim_SalesGroupid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_BillingBlockid  = IFNULL(bb.Dim_BillingBlockid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_BillingBlock bb on bb.BillingBlockCode = sg.vbap_faksp
WHERE IFNULL(sg.Dim_BillingBlockid,-1)  <> IFNULL(bb.Dim_BillingBlockid ,-2) ;

UPDATE staging_update_701 sg
SET sg.Dim_TransactionGroupid = IFNULL(tg.Dim_TransactionGroupid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_TransactionGroup tg on tg.TransactionGroup = sg.vbak_trvog
WHERE IFNULL(sg.Dim_TransactionGroupid,-1)  <> IFNULL(tg.Dim_TransactionGroupid,-2);


/* Updated below at the Item level instead of Schedule
update staging_update_701 sg
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_SalesOrderRejectReason sorr
Set       sg.Dim_SalesOrderRejectReasonid  = sorr.Dim_SalesOrderRejectReasonid
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y._SaleseDocNo and VBAP_POSNR  =  y._SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND sorr.RejectReasonCode = ifnull(vbap_abgru,'Not Set')
AND ifnull(sg.Dim_SalesOrderRejectReasonid,-1) <> ifnull(sorr.Dim_SalesOrderRejectReasonid,-2)
*/

UPDATE staging_update_701 sg
SET sg.Dim_Partid = IFNULL(dp.dim_partid, 1)
FROM staging_update_701 sg
     LEFT JOIN dim_part dp on dp.PartNumber = VBAP_MATNR AND dp.Plant = sg.plantcode
WHERE IFNULL(sg.Dim_Partid,-1) <> IFNULL(dp.dim_partid,-2);

UPDATE staging_update_701 sg
SET sg.Dim_SalesOrderHeaderStatusid = IFNULL(sohs.Dim_SalesOrderHeaderStatusid, 1)
FROM staging_update_701 sg
     LEFT JOIN Dim_SalesOrderHeaderStatus sohs on sohs.SalesDocumentNumber = sg.dd_SalesDocNo
WHERE IFNULL(sg.Dim_SalesOrderHeaderStatusid,-1)  <> IFNULL(sohs.Dim_SalesOrderHeaderStatusid,-2);

/*End of temporary code split */

UPDATE staging_update_701 sg
SET sg.Dim_CostCenterid = IFNULL(cc.Dim_CostCenterid, 1)
FROM staging_update_701 sg
     LEFT JOIN (SELECT max(Dim_CostCenterid) Dim_CostCenterid,Code,ControllingArea    FROM Dim_CostCenter WHERE RowIsCurrent  =  1 group by Code,ControllingArea) cc on cc.Code = sg.vbak_kostl and cc.ControllingArea = sg.vbak_kokrs
WHERE IFNULL(sg.Dim_CostCenterid,-1) <> IFNULL(cc.Dim_CostCenterid,-2);



/* Now update fact table from the stg table */

MERGE INTO fact_salesorder so
USING staging_update_701 sut ON so.dd_SalesDocNo = sut.dd_SalesDocNo AND so.dd_SalesItemNo = sut.dd_SalesItemNo AND so.dd_ScheduleNo = sut.dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET
	so.Dim_DateidSchedDeliveryReq = IFNULL(sut.Dim_DateidSchedDeliveryReq, 1)
	,so.Dim_DateidSchedDelivery = IFNULL(sut.Dim_DateidSchedDelivery, 1)
	,so.Dim_DateidGoodsIssue = IFNULL(sut.Dim_DateidGoodsIssue, 1)
	,so.Dim_DateidMtrlAvail = IFNULL(sut.Dim_DateidMtrlAvail, 1)
	,so.Dim_DateidLoading = IFNULL(sut.Dim_DateidLoading, 1)
	,so.Dim_DateidGuaranteedate = IFNULL(sut.Dim_DateidGuaranteedate, 1)
	,so.Dim_DateidTransport = IFNULL(sut.Dim_DateidTransport, 1)
	,so.Dim_Currencyid = IFNULL(sut.Dim_Currencyid, 1)
	,so.Dim_ProductHierarchyid = IFNULL(sut.Dim_ProductHierarchyid, 1)
	,so.Dim_Plantid = IFNULL(sut.Dim_Plantid, 1)
	,so.Dim_Companyid = IFNULL(sut.Dim_Companyid, 1)
	,so.Dim_StorageLocationid = IFNULL(sut.Dim_StorageLocationid, 1)
	,so.Dim_SalesDivisionid = IFNULL(sut.Dim_SalesDivisionid, 1)
	,so.Dim_ShipReceivePointid = IFNULL(sut.Dim_ShipReceivePointid, 1)
	,so.Dim_DocumentCategoryid = IFNULL(sut.Dim_DocumentCategoryid, 1)
	,so.Dim_SalesDocumentTypeid = IFNULL(sut.Dim_SalesDocumentTypeid, 1)
	,so.Dim_SalesOrgid = IFNULL(sut.Dim_SalesOrgid, 1)
	,so.Dim_CustomerID = IFNULL(sut.Dim_CustomerID, 1)
	,so.Dim_ScheduleLineCategoryId = IFNULL(sut.Dim_ScheduleLineCategoryId, 1)
	,so.Dim_DateidValidFrom = IFNULL(sut.Dim_DateidValidFrom, 1)
	,so.Dim_DateidValidTo = IFNULL(sut.Dim_DateidValidTo, 1)
	,so.Dim_SalesGroupid = IFNULL(sut.Dim_SalesGroupid, 1)
	,so.Dim_CostCenterid = IFNULL(sut.Dim_CostCenterid, 1)
	,so.Dim_ControllingAreaid = IFNULL(sut.Dim_ControllingAreaid, 1)
	,so.Dim_BillingBlockid = IFNULL(sut.Dim_BillingBlockid, 1)
	,so.Dim_TransactionGroupid = IFNULL(sut.Dim_TransactionGroupid, 1)
	,so.Dim_Partid = IFNULL(sut.Dim_Partid, 1)
	,so.Dim_SalesOrderHeaderStatusid = IFNULL(sut.Dim_SalesOrderHeaderStatusid, 1);

/* Review done until here */
DROP TABLE IF EXISTS staging_update_701;

CREATE TABLE staging_update_701
AS
SELECT convert(bigint, 1) as Dim_SalesOrderItemStatusid, --ifnull((select sois.Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber  =  VBAK_VBELN and sois.SalesItemNumber  =  VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
       convert(bigint, 1) as Dim_CustomerGroup1id, --ifnull((select cg1.Dim_CustomerGroup1id from Dim_CustomerGroup1 cg1 where cg1.CustomerGroup  =  VBAK_KVGR1),1) Dim_CustomerGroup1id,
       convert(bigint, 1) as Dim_CustomerGroup2id, --ifnull((select cg2.Dim_CustomerGroup2id from Dim_CustomerGroup2 cg2 where cg2.CustomerGroup  =  VBAK_KVGR2),1) Dim_CustomerGroup2id,
	   soic.Dim_SalesOrderItemCategoryid Dim_salesorderitemcategoryid,
       ifnull(vbep_lfrel, 'Not Set') dd_ItemRelForDelv,
	   convert(bigint, 1) as Dim_ProfitCenterId,
       convert(bigint, 1) as Dim_DistributionChannelId, --ifnull((select dc.Dim_DistributionChannelid from dim_DistributionChannel dc where   dc.DistributionChannelCode  =  VBAK_VTWEG AND dc.RowIsCurrent  =  1), 1) Dim_DistributionChannelId,
	   convert(bigint, 1) as Dim_UnitOfMeasureId, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_kmein AND uom.RowIsCurrent  =  1), 1) Dim_UnitOfMeasureId,
	   convert(bigint, 1) as Dim_BaseUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_meins AND uom.RowIsCurrent  =  1), 1) Dim_BaseUoMid,
	   convert(bigint, 1) as Dim_SalesUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_vrkme AND uom.RowIsCurrent  =  1), 1) Dim_SalesUoMid,
       ifnull(VBAP_CHARG, 'Not Set') dd_BatchNo,
       ifnull(VBAK_ERNAM, 'Not Set') dd_CreatedBy,
       convert(bigint, 1) as Dim_DateidNextDate, --ifnull((SELECT nd.Dim_Dateid FROM Dim_Date nd WHERE nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidNextDate,
	   convert(bigint, 1) as Dim_RouteId, --ifnull((SELECT r.dim_routeid from dim_route r where r.RouteCode  =  VBAP_ROUTE and r.RowIsCurrent  =  1),1) Dim_RouteId,
	   convert(bigint, 1) as Dim_SalesRiskCategoryId,
	   convert(decimal (18,4), 0) as dd_CreditLimit,
	   convert(bigint, 1) as Dim_CustomerRiskCategoryId,
	   convert(varchar(100), 'Not Set') as dd_CreditRep,
	   convert(varchar(100), 'Not Set') as dd_CreditMgr,
	   dd_SalesDocNo,
	   dd_SalesItemNo,
	   dd_ScheduleNo,
	   VBAK_KVGR1,
	   VBAK_KVGR2,
	   VBAK_VTWEG,
	   vbap_kmein,
	   vbap_meins,
	   vbap_vrkme,
	   VBAK_CMNGV,
	   pl.CompanyCode as companycode,
	   VBAP_ROUTE,
	   pl.PlantCode as plantcode,
	   VBAP_PRCTR,
	   VBAK_KOKRS,
	   VBAK_ERDAT,
	   VBAK_CTLPC,
	   VBAK_KKBER,
	   VBAK_KUNNR
FROM fact_salesorder so,
     VBAK_VBAP_VBEP,
     Dim_Plant pl,
     Dim_Company co,
	 Dim_SalesOrderItemCategory soic
WHERE pl.PlantCode  = VBAP_WERKS
      AND co.CompanyCode  =  pl.CompanyCode
      AND VBAK_VBELN  =  dd_SalesDocNo
	  AND VBAP_POSNR  =  dd_SalesItemNo
	  AND VBEP_ETENR  =  dd_ScheduleNo
	  AND soic.SalesOrderItemCategory  =  VBAP_PSTYV
	  AND soic.RowIsCurrent = 1;

UPDATE staging_update_701 sut
SET sut.Dim_SalesOrderItemStatusid = IFNULL(sois.Dim_SalesOrderItemStatusid, 1)
FROM staging_update_701 sut
     LEFT JOIN Dim_SalesOrderItemStatus sois on sois.SalesDocumentNumber = dd_SalesDocNo and sois.SalesItemNumber  =  dd_SalesItemNo
WHERE sut.Dim_SalesOrderItemStatusid <> IFNULL(sois.Dim_SalesOrderItemStatusid, 1);

UPDATE staging_update_701 sut
SET sut.Dim_CustomerGroup1id = IFNULL(cg1.Dim_CustomerGroup1id, 1)
FROM staging_update_701 sut
     LEFT JOIN Dim_CustomerGroup1 cg1 on cg1.CustomerGroup  =  VBAK_KVGR1
WHERE sut.Dim_CustomerGroup1id <> IFNULL(cg1.Dim_CustomerGroup1id, 1);

UPDATE staging_update_701 sut
SET sut.Dim_CustomerGroup2id = IFNULL(cg2.Dim_CustomerGroup2id, 1)
FROM staging_update_701 sut
     LEFT JOIN Dim_CustomerGroup2 cg2 on cg2.CustomerGroup  =  VBAK_KVGR2
WHERE sut.Dim_CustomerGroup2id <> IFNULL(cg2.Dim_CustomerGroup2id, 1);

UPDATE staging_update_701 sut
SET sut.Dim_DistributionChannelId = IFNULL(dc.Dim_DistributionChannelid, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_DistributionChannel WHERE RowIsCurrent  =  1) dc on dc.DistributionChannelCode  =  VBAK_VTWEG
WHERE sut.Dim_DistributionChannelId <> IFNULL(dc.Dim_DistributionChannelid, 1);

UPDATE staging_update_701 sut
SET sut.Dim_UnitOfMeasureId = IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE RowIsCurrent  =  1) uom on uom.UOM  =  vbap_kmein
WHERE sut.Dim_UnitOfMeasureId <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_update_701 sut
SET sut.Dim_BaseUoMid = IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE RowIsCurrent  =  1) uom on uom.UOM  =  vbap_meins
WHERE sut.Dim_BaseUoMid <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_update_701 sut
SET sut.Dim_SalesUoMid = IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE RowIsCurrent  =  1) uom on uom.UOM  =  vbap_vrkme
WHERE sut.Dim_SalesUoMid <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_update_701 sut
SET sut.Dim_DateidNextDate = IFNULL(nd.Dim_Dateid, 1)
FROM staging_update_701 sut
     LEFT JOIN Dim_Date nd on nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode = sut.CompanyCode
WHERE sut.Dim_DateidNextDate <> IFNULL(nd.Dim_Dateid, 1);

UPDATE staging_update_701 sut
SET sut.Dim_RouteId = IFNULL(r.dim_routeid, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_route where rowiscurrent = 1) r on r.RouteCode  =  VBAP_ROUTE
WHERE sut.Dim_RouteId <> IFNULL(r.dim_routeid, 1);

UPDATE staging_update_701 sut
SET Dim_ProfitCenterId = IFNULL(pc.dim_profitcenterid, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_profitcenter WHERE RowIsCurrent  =  1) pc on pc.ProfitCenterCode  =  VBAP_PRCTR AND pc.ControllingArea  =  VBAK_KOKRS AND pc.ValidTo >=  VBAK_ERDAT
WHERE sut.Dim_ProfitCenterId <> IFNULL(pc.dim_profitcenterid, 1);

UPDATE staging_update_701 sut
SET Dim_SalesRiskCategoryId = ifnull(src.Dim_SalesRiskCategoryId, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM Dim_SalesRiskCategory WHERE rowiscurrent = 1) src
	     ON src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER
WHERE sut.Dim_SalesRiskCategoryId <> ifnull(src.Dim_SalesRiskCategoryId, 1);

UPDATE staging_update_701 sut
SET sut.dd_CreditLimit = IFNULL(c.CREDIT_LIMIT,0)   /* IFNULL ??*/
FROM staging_update_701 sut
	 LEFT JOIN TMP_UPD_dd_CreditLimit c ON c.CUSTOMER  =  VBAK_KUNNR
WHERE sut.dd_CreditLimit <> c.CREDIT_LIMIT;

UPDATE staging_update_701 sut
SET sut.Dim_CustomerRiskCategoryId  = ifnull(src.dim_salesriskcategoryid, 1)
FROM staging_update_701 sut
	 LEFT JOIN TMP_UPD_Dim_CustomerRiskCategoryId src ON src.CUSTOMER  =  sut.VBAK_KUNNR and src.CreditControlArea  =  sut.VBAK_KKBER
WHERE sut.Dim_CustomerRiskCategoryId <> ifnull(src.dim_salesriskcategoryid, 1);

UPDATE staging_update_701 sut
SET sut.dd_CreditRep = ifnull(b.upd_dd_CreditRep1, 'Not Set')
FROM staging_update_701 sut
	 LEFT JOIN TMP_UPD_dd_CreditRep1 b ON b.BUT050_PARTNER2  =  sut.VBAK_KUNNR
WHERE sut.dd_CreditRep <> ifnull(b.upd_dd_CreditRep1, 'Not Set');

UPDATE staging_update_701 sut
SET sut.dd_CreditMgr  = ifnull(bm.upd_dd_CreditMgr1, 'Not Set')
FROM staging_update_701 sut
	 LEFT JOIN TMP_UPD_dd_CreditMgr1 bm ON bm.BUT050_PARTNER2 = sut.VBAK_KUNNR
WHERE sut.dd_CreditMgr <> ifnull(bm.upd_dd_CreditMgr1, 'Not Set');

/* Now update fact table from the stg table */

MERGE INTO fact_salesorder so
USING staging_update_701 sut ON so.dd_SalesDocNo = sut.dd_SalesDocNo AND so.dd_SalesItemNo = sut.dd_SalesItemNo AND so.dd_ScheduleNo = sut.dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET
	so.Dim_SalesOrderItemStatusid = ifnull(sut.Dim_SalesOrderItemStatusid, 1)
	,so.Dim_CustomerGroup1id = ifnull(sut.Dim_CustomerGroup1id, 1)
	,so.Dim_CustomerGroup2id = ifnull(sut.Dim_CustomerGroup2id, 1)
	,so.Dim_salesorderitemcategoryid = ifnull(sut.Dim_salesorderitemcategoryid, 1)
	,so.dd_ItemRelForDelv = ifnull(sut.dd_ItemRelForDelv, 1)
	,so.Dim_ProfitCenterId = ifnull(sut.Dim_ProfitCenterId, 1)
	,so.Dim_DistributionChannelId = ifnull(sut.Dim_DistributionChannelId, 1)
	,so.Dim_UnitOfMeasureId = ifnull(sut.Dim_UnitOfMeasureId, 1)
	,so.Dim_BaseUoMid  =  ifnull(sut.Dim_BaseUoMid, 1)
	,so.Dim_SalesUoMid  =  ifnull(sut.Dim_SalesUoMid, 1)
	,so.dd_BatchNo = ifnull(sut.dd_BatchNo, 'Not Set')
	,so.dd_CreatedBy = ifnull(sut.dd_CreatedBy, 'Not Set')
	,so.Dim_DateidNextDate = ifnull(sut.Dim_DateidNextDate, 1)
	,so.Dim_RouteId = ifnull(sut.Dim_RouteId, 1)
	,so.Dim_SalesRiskCategoryId = ifnull(sut.Dim_SalesRiskCategoryId, 1)
	,so.dd_CreditLimit = IFNULL(sut.dd_CreditLimit, 0)
	,so.Dim_CustomerRiskCategoryId = IFNULL(sut.Dim_CustomerRiskCategoryId, 1)
	,so.dd_CreditRep = IFNULL(sut.dd_CreditRep, 'Not Set')
	,so.dd_CreditMgr = IFNULL(sut.dd_CreditMgr, 'Not Set');

/* 12 Apr changes - sync with mysql changes	by Shanthi part 2 ends	*/

DROP TABLE IF EXISTS staging_update_701;

DROP TABLE IF EXISTS Dim_CostCenter_first1_701;
CREATE TABLE Dim_CostCenter_first1_701
AS
SELECT code,
       ControllingArea,
	   RowIsCurrent,
	   MAX(Validto) validto,
	   CONVERT(BIGINT, NULL) dim_costcenterid
FROM Dim_CostCenter
GROUP BY code,
         ControllingArea,
		 RowIsCurrent;

UPDATE Dim_CostCenter_first1_701 b
SET b.dim_costcenterid = a.dim_costcenterid
FROM Dim_CostCenter_first1_701 b
     INNER JOIN Dim_CostCenter a ON b.code = a.code and b.ControllingArea = a.ControllingArea and b.RowIsCurrent = a.RowIsCurrent and b.validto = a.validto;


DROP TABLE IF EXISTS max_holder_701;
CREATE TABLE max_holder_701
AS
SELECT ifnull(max(fact_salesorderid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) maxid
FROM fact_salesorder;

DROP TABLE IF EXISTS fact_salesorder_tmptbl;
CREATE TABLE fact_salesorder_tmptbl
AS
SELECT *
FROM fact_salesorder
WHERE 1 = 2;

DROP TABLE IF EXISTS fact_salesorder_useinsub;
CREATE TABLE fact_salesorder_useinsub
AS
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
	   dd_ScheduleNo
FROM fact_salesorder;

/* Create temporary table tmp_pop_fact_salesorder_tmptbl to simplify the next insert query */

DROP TABLE IF EXISTS TMP_INS_VBAK_VBAP_VBEP;
CREATE TABLE TMP_INS_VBAK_VBAP_VBEP
AS
SELECT VBAK_VBELN,
       VBAP_POSNR,
       VBEP_ETENR
FROM VBAK_VBAP_VBEP;

DROP TABLE IF EXISTS TMP_DEL_VBAK_VBAP_VBEP;
CREATE TABLE TMP_DEL_VBAK_VBAP_VBEP
AS
SELECT *
FROM TMP_INS_VBAK_VBAP_VBEP
WHERE 1=2;

INSERT INTO TMP_DEL_VBAK_VBAP_VBEP(VBAK_VBELN,VBAP_POSNR,VBEP_ETENR)
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo
FROM fact_salesorder;

/* CALL VECTORWISE(COMBINE 'TMP_INS_VBAK_VBAP_VBEP-TMP_DEL_VBAK_VBAP_VBEP') */

MERGE INTO TMP_INS_VBAK_VBAP_VBEP i
USING TMP_DEL_VBAK_VBAP_VBEP d ON i.VBAK_VBELN = d.VBAK_VBELN
	AND i.VBAP_POSNR = d.VBAP_POSNR AND i.VBEP_ETENR = d.VBEP_ETENR
when matched then DELETE;

DROP TABLE IF EXISTS TMP_NEWROWS_VBAK_VBAP_VBEP;
CREATE TABLE TMP_NEWROWS_VBAK_VBAP_VBEP
AS
SELECT DISTINCT v.*
FROM VBAK_VBAP_VBEP v,
     TMP_INS_VBAK_VBAP_VBEP i,
     dim_date mdt
WHERE v.VBAK_VBELN = i.VBAK_VBELN
      AND v.VBAK_VBELN = i.VBAK_VBELN
      AND v.VBAP_POSNR = i.VBAP_POSNR
      AND mdt.DateValue = v.vbap_erdat
      AND mdt.Dim_Dateid > 1
      AND mdt.companycode = 'Not Set';

DROP TABLE IF EXISTS VBAK_VBAP_VBEP_701;
CREATE TABLE VBAK_VBAP_VBEP_701
AS
SELECT MIN(x.VBEP_ETENR) v_SalesSchedNo,
       x.VBAK_VBELN v_SaleseDocNo,
	   x.VBAP_POSNR v_SalesItemNo
FROM VBAK_VBAP_VBEP x
GROUP BY x.VBAK_VBELN, x.VBAP_POSNR;

DROP TABLE IF EXISTS tmp_pop_fact_salesorder_tmptbl;
CREATE TABLE tmp_pop_fact_salesorder_tmptbl
AS
SELECT v.*,
       pl.PlantCode,
       pl.CompanyCode,
       pl.dim_plantid,
       soic.Dim_SalesOrderItemCategoryid,
       co.Dim_Companyid,
       y.v_SalesSchedNo,
       co.Currency
FROM TMP_NEWROWS_VBAK_VBAP_VBEP v
      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
      INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
      INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
      INNER JOIN VBAK_VBAP_VBEP_701 y ON VBAK_VBELN = y.v_SaleseDocNo AND VBAP_POSNR = y.v_SalesItemNo;

INSERT INTO fact_salesorder_tmptbl(
	        fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
	        Dim_RouteId,
	        Dim_SalesRiskCategoryId,
            dd_CreditRep,
	        dd_CreditMgr,
            dd_CreditLimit,
            Dim_CustomerRiskCategoryId,
	        amt_UnitPriceUoM,
	        dim_Currencyid_TRA,
	        dim_Currencyid_GBL,
	        dim_currencyid_STAT,
	        amt_exchangerate_STAT)
SELECT max_holder_701.maxid + row_number() over(order by ''),
       vbak_vbeln dd_SalesDocNo,
       vbap_posnr dd_SalesItemNo,
       vbep_etenr dd_ScheduleNo,
       vbep_wmeng ct_ScheduleQtySalesUnit,
       vbep_bmeng ct_ConfirmedQty,
       vbep_cmeng ct_CorrectedQty,
       ifnull(convert(numeric(18,4), vbap_netpr),0) amt_UnitPrice,
       vbap_kpein ct_PriceUnit,
       convert(numeric(18,4), CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )),2), 0)
			WHEN vbep_wmeng  =  VBAP_KWMENG THEN VBAP_NETWR
			ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end )), 0)
		   END) amt_ScheduleTotal,
       convert(numeric(18,4), 0) as amt_StdCost,
       convert(numeric(18,4), 0) as amt_TargetValue,
	   ifnull(convert(numeric(18,4), CASE ifnull(vbap_mwsbp,0) WHEN 0 THEN 0
				ELSE
				(vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0)  =  0 THEN 1
						WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))  =  0 THEN 1.000000 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )) END)),2),1)
							WHEN 0 THEN 1
							ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))  =  0 THEN 1.000000 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )) END)),2),1)
							END
						ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end))  =  0 THEN 1.000000 ELSE (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)) END)),1)
					 END)
		END * (vbep_bmeng)), 0) amt_Tax,
       convert(numeric(18,4), 0) as ct_TargetQty,
       convert(bigint, 1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  pl.Currency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate,
	   convert(bigint, 1) as amt_ExchangeRate_GBL, --ifnull( ( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate_GBL ,
       vbap_uebto ct_OverDlvrTolerance,
       vbap_untto ct_UnderDlvrTolerance,
       convert(bigint, 1) as Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint, 1) as Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
       convert(bigint, 1) as Dim_DateidSchedDeliveryReq, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
       convert(bigint, 1) as Dim_DateidSchedDlvrReqPrev, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
       convert(bigint, 1) as Dim_DateidSchedDelivery, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_edatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDelivery,
       convert(bigint, 1) as Dim_DateidGoodsIssue, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_wadat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGoodsIssue,
       convert(bigint, 1) as Dim_DateidMtrlAvail, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_mbdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidMtrlAvail,
       convert(bigint, 1) as Dim_DateidLoading, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_lddat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidLoading,
       convert(bigint, 1) as Dim_DateidTransport, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_tddat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidTransport,
       convert(bigint, 1) as Dim_DateidGuaranteedate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGuaranteedate,
       convert(bigint, 1) as Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pl.currency ),1) Dim_Currencyid,
       convert(bigint, 1) as Dim_ProductHierarchyid, --ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy  =  vbap_prodh),1) Dim_ProductHierarchyid,
       pl.Dim_Plantid,
       pl.Dim_Companyid,
       convert(bigint, 1) as Dim_StorageLocationid, --ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode  =  vbap_lgort and sl.plant  =  vbap_werks),1) Dim_StorageLocationid,
       convert(bigint, 1) as Dim_SalesDivisionid, --ifnull((SELECT Dim_SalesDivisionid FROM Dim_SalesDivision sd WHERE sd.DivisionCode  =  vbap_spart),1) Dim_SalesDivisionid,
       convert(bigint, 1) as Dim_ShipReceivePointid, --ifnull((SELECT Dim_ShipReceivePointid FROM Dim_ShipReceivePoint srp WHERE srp.ShipReceivePointCode  =  vbap_vstel),1) Dim_ShipReceivePointid,
       convert(bigint, 1) as Dim_DocumentCategoryid, --ifnull((SELECT Dim_DocumentCategoryid FROM Dim_DocumentCategory dc WHERE dc.DocumentCategory  =  vbak_vbtyp),1) Dim_DocumentCategoryid,
       convert(bigint, 1) as Dim_SalesDocumentTypeid, --ifnull((SELECT Dim_SalesDocumentTypeid FROM Dim_SalesDocumentType sdt WHERE sdt.DocumentType  =  vbak_auart),1) Dim_SalesDocumentTypeid,
       convert(bigint, 1) as Dim_SalesOrgid, --ifnull((SELECT Dim_SalesOrgid FROM Dim_SalesOrg so WHERE so.SalesOrgCode  =  vbak_vkorg),1) Dim_SalesOrgid,
       convert(bigint, 1) as Dim_CustomerID, --ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber  =  vbak_kunnr),1) Dim_CustomerID,
       convert(bigint, 1) as Dim_DateidValidFrom, --ifnull((SELECT Dim_Dateid FROM Dim_Date vf WHERE vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidFrom,
       convert(bigint, 1) as Dim_DateidValidTo, --ifnull((SELECT Dim_Dateid FROM Dim_Date vt WHERE vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidTo,
       convert(bigint, 1) as Dim_SalesGroupid, --ifnull((SELECT Dim_SalesGroupid FROM Dim_SalesGroup sg WHERE sg.SalesGroupCode  =  vbak_vkgrp),1) Dim_SalesGroupid,
       convert(bigint, 1) as Dim_CostCenterid, --ifnull((SELECT Dim_CostCenterid FROM Dim_CostCenter_first1_701 cc WHERE cc.Code  =  vbak_kostl and cc.ControllingArea  =  vbak_kokrs and cc.RowIsCurrent  =  1),1) Dim_CostCenterid,
       convert(bigint, 1) as Dim_ControllingAreaid, --ifnull((SELECT Dim_ControllingAreaid FROM Dim_ControllingArea ca WHERE ca.ControllingAreaCode  =  vbak_kokrs),1) Dim_ControllingAreaid,
       convert(bigint, 1) as Dim_BillingBlockid, --ifnull((SELECT Dim_BillingBlockid FROM Dim_BillingBlock bb WHERE bb.BillingBlockCode  =  vbap_faksp),1) Dim_BillingBlockid,
       convert(bigint, 1) as Dim_TransactionGroupid, --ifnull((SELECT Dim_TransactionGroupid FROM Dim_TransactionGroup tg WHERE tg.TransactionGroup  =  vbak_trvog),1) Dim_TransactionGroupid,
       convert(bigint, 1) as Dim_SalesOrderRejectReasonid, --ifnull((SELECT Dim_SalesOrderRejectReasonid FROM Dim_SalesOrderRejectReason sorr WHERE sorr.RejectReasonCode  =  vbap_abgru),1) Dim_SalesOrderRejectReasonid,
       convert(bigint, 1) as Dim_Partid, --ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS),1) Dim_Partid,
       convert(bigint, 1) as Dim_SalesOrderHeaderStatusid, --ifnull((select Dim_SalesOrderHeaderStatusid from Dim_SalesOrderHeaderStatus sohs where sohs.SalesDocumentNumber  =  VBAK_VBELN),1) Dim_SalesOrderHeaderStatusid,
       convert(bigint, 1) as Dim_SalesOrderItemStatusid, --ifnull((select Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber  =  VBAK_VBELN and sois.SalesItemNumber  =  VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
       convert(bigint, 1) as Dim_CustomerGroup1id, --ifnull((select Dim_CustomerGroup1id from Dim_CustomerGroup1 cg1 where cg1.CustomerGroup  =  VBAK_KVGR1),1) Dim_CustomerGroup1id,
       convert(bigint, 1) as Dim_CustomerGroup2id, --ifnull((select Dim_CustomerGroup2id from Dim_CustomerGroup2 cg2 where cg2.CustomerGroup  =  VBAK_KVGR2),1) Dim_CustomerGroup2id,
	   ifnull((pl.Dim_SalesOrderItemCategoryid),1)  Dim_SalesOrderItemCategoryid,
       convert(bigint, 1) as Dim_ScheduleLineCategoryId, --ifnull((SELECT slc.Dim_ScheduleLineCategoryId FROM Dim_ScheduleLineCategory slc WHERE slc.ScheduleLineCategory  =  VBEP_ETTYP),1) Dim_ScheduleLineCategoryId,
       ifnull(vbep_lfrel,'Not Set') dd_ItemRelForDelv,
	   convert(bigint, 1) as Dim_ProfitCenterId,
       convert(bigint, 1) as Dim_DistributionChannelId, --ifnull((select dc.Dim_DistributionChannelid from dim_DistributionChannel dc where   dc.DistributionChannelCode  =  VBAK_VTWEG AND dc.RowIsCurrent  =  1), 1) Dim_DistributionChannelId,
       ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
       ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
       convert(bigint, 1) as Dim_DateidNextDate, --ifnull((SELECT nd.Dim_Dateid FROM Dim_Date nd WHERE nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidNextDate,
	   convert(bigint, 1) as Dim_RouteId, --ifnull((SELECT r.dim_routeid from dim_route r where r.RouteCode  =  VBAP_ROUTE and r.RowIsCurrent  =  1),1),
       convert(bigint, 1) as Dim_SalesRiskCategoryId, --ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER and src.RowIsCurrent  =  1),1),
       'Not Set' as dd_CreditRep, --ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditRep1,
       'Not Set' as dd_CreditMgr, --ifnull((select upd_dd_CreditMgr1 from TMP_UPD_dd_CreditMgr1 bm where bm.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditMgr1,
       convert(numeric(18,4), 0) as dd_CreditLimit, --ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER  =  VBAK_KUNNR ),0) dd_CreditLimit,
       convert(bigint, 1) Dim_CustomerRiskCategoryId,
       convert(numeric(18,4), CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END) amt_UnitPriceUoM,
       convert(bigint, 1) Dim_Currencyid_TRA,
	   convert(bigint, 1) dim_Currencyid_GBL,
	   convert(bigint, 1) dim_currencyid_STAT,
	   convert(numeric(18,4), 1) amt_exchangerate_STAT
FROM max_holder_701,
     tmp_pop_fact_salesorder_tmptbl pl,
	 variable_holder_701;


UPDATE fact_salesorder_tmptbl tmp
SET tmp.amt_ExchangeRate = IFNULL(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' and pFromExchangeRate  =  0) z
         ON z.pFromCurrency  = VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  pl.Currency
WHERE tmp.amt_ExchangeRate <> IFNULL(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.amt_ExchangeRate_GBL = 1
WHERE tmp.amt_ExchangeRate_GBL <> 1;

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidSalesOrderCreated = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidSalesOrderCreated <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidFirstDate = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidFirstDate <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidSchedDeliveryReq = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidSchedDeliveryReq <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidSchedDlvrReqPrev = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidSchedDlvrReqPrev <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidSchedDelivery = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_edatu AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidSchedDelivery <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidGoodsIssue = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_wadat AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidGoodsIssue <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidMtrlAvail = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_mbdat AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidMtrlAvail <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidLoading = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_lddat AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidLoading <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidTransport = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_tddat AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidTransport <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidGuaranteedate = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidGuaranteedate <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_Currencyid = IFNULL(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  pl.currency
WHERE tmp.Dim_Currencyid <> IFNULL(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ProductHierarchyid = IFNULL(ph.Dim_ProductHierarchyid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_ProductHierarchy ph ON ph.ProductHierarchy  =  vbap_prodh
WHERE tmp.Dim_ProductHierarchyid <> IFNULL(ph.Dim_ProductHierarchyid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_StorageLocationid = IFNULL(sl.Dim_StorageLocationid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_StorageLocation sl ON sl.LocationCode  =  vbap_lgort and sl.plant  =  vbap_werks
WHERE tmp.Dim_StorageLocationid <> IFNULL(sl.Dim_StorageLocationid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesDivisionid = IFNULL(sd.Dim_SalesDivisionid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesDivision sd ON sd.DivisionCode  =  vbap_spart
WHERE tmp.Dim_SalesDivisionid <> IFNULL(sd.Dim_SalesDivisionid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ShipReceivePointid = IFNULL(srp.Dim_ShipReceivePointid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_ShipReceivePoint srp ON srp.ShipReceivePointCode  =  vbap_vstel
WHERE tmp.Dim_ShipReceivePointid <> IFNULL(srp.Dim_ShipReceivePointid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DocumentCategoryid = IFNULL(dc.Dim_DocumentCategoryid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_DocumentCategory dc ON dc.DocumentCategory  =  vbak_vbtyp
WHERE tmp.Dim_DocumentCategoryid <> IFNULL(dc.Dim_DocumentCategoryid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesDocumentTypeid = IFNULL(sdt.Dim_SalesDocumentTypeid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesDocumentType sdt ON sdt.DocumentType  =  vbak_auart
WHERE tmp.Dim_SalesDocumentTypeid <> IFNULL(sdt.Dim_SalesDocumentTypeid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesOrgid = IFNULL(so.Dim_SalesOrgid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesOrg so ON so.SalesOrgCode  =  vbak_vkorg
WHERE tmp.Dim_SalesOrgid <> IFNULL(so.Dim_SalesOrgid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_CustomerID = IFNULL(cust.Dim_CustomerID, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Customer cust ON cust.CustomerNumber  =  vbak_kunnr
WHERE tmp.Dim_CustomerID <> IFNULL(cust.Dim_CustomerID, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidValidFrom = IFNULL(vf.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Date vf ON vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidValidFrom <> IFNULL(vf.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidValidTo = IFNULL(vt.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Date vt ON vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidValidTo <> IFNULL(vt.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesGroupid = IFNULL(sg.Dim_SalesGroupid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesGroup sg ON sg.SalesGroupCode  =  vbak_vkgrp
WHERE tmp.Dim_SalesGroupid <> IFNULL(sg.Dim_SalesGroupid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_CostCenterid = IFNULL(cc.Dim_CostCenterid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT * FROM Dim_CostCenter_first1_701 WHERE  RowIsCurrent = 1) cc ON cc.Code  =  vbak_kostl and cc.ControllingArea  =  vbak_kokrs
WHERE tmp.Dim_CostCenterid <> IFNULL(cc.Dim_CostCenterid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ControllingAreaid = IFNULL(ca.Dim_ControllingAreaid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_ControllingArea ca ON ca.ControllingAreaCode  =  vbak_kokrs
WHERE tmp.Dim_ControllingAreaid <> IFNULL(ca.Dim_ControllingAreaid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_BillingBlockid = IFNULL(bb.Dim_BillingBlockid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_BillingBlock bb ON bb.BillingBlockCode  =  vbap_faksp
WHERE tmp.Dim_BillingBlockid <> IFNULL(bb.Dim_BillingBlockid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_TransactionGroupid = IFNULL(tg.Dim_TransactionGroupid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_TransactionGroup tg ON tg.TransactionGroup  =  vbak_trvog
WHERE tmp.Dim_TransactionGroupid <> IFNULL(tg.Dim_TransactionGroupid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesOrderRejectReasonid = IFNULL(sorr.Dim_SalesOrderRejectReasonid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesOrderRejectReason sorr ON sorr.RejectReasonCode  =  vbap_abgru
WHERE tmp.Dim_SalesOrderRejectReasonid <> IFNULL(sorr.Dim_SalesOrderRejectReasonid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_Partid = IFNULL(dp.dim_partid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_part dp ON dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS
WHERE tmp.Dim_Partid <> IFNULL(dp.dim_partid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesOrderHeaderStatusid = IFNULL(sohs.Dim_SalesOrderHeaderStatusid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesOrderHeaderStatus sohs ON sohs.SalesDocumentNumber  =  VBAK_VBELN
WHERE tmp.Dim_SalesOrderHeaderStatusid <> IFNULL(sohs.Dim_SalesOrderHeaderStatusid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesOrderItemStatusid = IFNULL(sois.Dim_SalesOrderItemStatusid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesOrderItemStatus sois ON sois.SalesDocumentNumber  =  VBAK_VBELN and sois.SalesItemNumber  =  VBAP_POSNR
WHERE tmp.Dim_SalesOrderItemStatusid <> IFNULL(sois.Dim_SalesOrderItemStatusid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_CustomerGroup1id = IFNULL(cg1.Dim_CustomerGroup1id, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_CustomerGroup1 cg1 ON cg1.CustomerGroup  =  VBAK_KVGR1
WHERE tmp.Dim_CustomerGroup1id <> IFNULL(cg1.Dim_CustomerGroup1id, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_CustomerGroup2id = IFNULL(cg2.Dim_CustomerGroup2id, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_CustomerGroup2 cg2 ON cg2.CustomerGroup  =  VBAK_KVGR2
WHERE tmp.Dim_CustomerGroup2id <> IFNULL(cg2.Dim_CustomerGroup2id, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ScheduleLineCategoryId = IFNULL(slc.Dim_ScheduleLineCategoryId, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_ScheduleLineCategory slc ON slc.ScheduleLineCategory  =  VBEP_ETTYP
WHERE tmp.Dim_ScheduleLineCategoryId <> IFNULL(slc.Dim_ScheduleLineCategoryId, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DistributionChannelId = IFNULL(dc.Dim_DistributionChannelid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT * FROM dim_DistributionChannel WHERE rowiscurrent = 1) dc ON dc.DistributionChannelCode  =  VBAK_VTWEG
WHERE tmp.Dim_DistributionChannelId <> IFNULL(dc.Dim_DistributionChannelid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidNextDate = IFNULL(nd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Date nd ON nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode
WHERE tmp.Dim_DateidNextDate <> IFNULL(nd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_RouteId = IFNULL(r.Dim_RouteId, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT * FROM dim_route WHERE rowiscurrent = 1) r ON r.RouteCode  =  VBAP_ROUTE
WHERE tmp.Dim_RouteId <> IFNULL(r.Dim_RouteId, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesRiskCategoryId = IFNULL(src.Dim_SalesRiskCategoryId, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT * FROM Dim_SalesRiskCategory WHERE rowiscurrent = 1) src ON src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER
WHERE tmp.Dim_SalesRiskCategoryId <> IFNULL(src.Dim_SalesRiskCategoryId, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.dd_CreditRep = IFNULL(b.upd_dd_CreditRep1, 'Not Set')
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN TMP_UPD_dd_CreditRep1 b ON b.BUT050_PARTNER2  =  VBAK_KUNNR
WHERE tmp.dd_CreditRep <> IFNULL(b.upd_dd_CreditRep1, 'Not Set');

UPDATE fact_salesorder_tmptbl tmp
SET tmp.dd_CreditMgr = IFNULL(bm.upd_dd_CreditMgr1, 'Not Set')
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN TMP_UPD_dd_CreditMgr1 bm ON bm.BUT050_PARTNER2  =  VBAK_KUNNR
WHERE tmp.dd_CreditMgr <> IFNULL(bm.upd_dd_CreditMgr1, 'Not Set');

UPDATE fact_salesorder_tmptbl tmp
SET tmp.dd_CreditLimit = IFNULL(c.CREDIT_LIMIT, 0)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN TMP_UPD_dd_CreditLimit c ON c.CUSTOMER  =  VBAK_KUNNR
WHERE tmp.dd_CreditLimit <> IFNULL(c.CREDIT_LIMIT, 0);

UPDATE fact_salesorder_tmptbl f
SET Dim_Currencyid_TRA = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON f.dd_salesdocno = pl.vbak_vbeln AND f.dd_salesitemno = pl.vbap_posnr AND f.dd_scheduleno= pl.vbep_etenr
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  VBAP_WAERK
WHERE f.Dim_Currencyid_TRA <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl f
SET dim_Currencyid_GBL = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN (SELECT x.*,pGlobalCurrency FROM tmp_pop_fact_salesorder_tmptbl x, variable_holder_701) pl ON f.dd_salesdocno = pl.vbak_vbeln AND f.dd_salesitemno = pl.vbap_posnr AND f.dd_scheduleno= pl.vbep_etenr
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  pGlobalCurrency
WHERE f.dim_Currencyid_GBL <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl f
SET dim_currencyid_STAT = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON f.dd_salesdocno = pl.vbak_vbeln AND f.dd_salesitemno = pl.vbap_posnr AND f.dd_scheduleno= pl.vbep_etenr
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  vbak_stwae
WHERE f.dim_currencyid_STAT <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl f
SET amt_exchangerate_STAT = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON f.dd_salesdocno = pl.vbak_vbeln AND f.dd_salesitemno = pl.vbap_posnr AND f.dd_scheduleno= pl.vbep_etenr
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' AND pFromExchangeRate = 0) z ON z.pFromCurrency = VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  vbak_stwae
WHERE amt_exchangerate_STAT <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl f
SET amt_StdCost  =  ifnull(convert(numeric(18,4), vbap_wavwr),0)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl vbk ON vbk.VBAK_VBELN  =  f.dd_SalesDocNo AND vbk.VBAP_POSNR  =  f.dd_SalesItemNo AND vbk.VBEP_ETENR  =  f.dd_ScheduleNo AND f.dd_ScheduleNo  =  vbk.v_SalesSchedNo
WHERE amt_StdCost  <>  ifnull(convert(numeric(18,4), vbap_wavwr),0);

UPDATE fact_salesorder_tmptbl f
SET amt_TargetValue  =  ifnull(convert(numeric(18,4), vbap_zwert),0)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl vbk ON vbk.VBAK_VBELN  =  f.dd_SalesDocNo AND vbk.VBAP_POSNR  =  f.dd_SalesItemNo AND vbk.VBEP_ETENR  =  f.dd_ScheduleNo AND f.dd_ScheduleNo  =  vbk.v_SalesSchedNo
WHERE amt_TargetValue  <>  ifnull(convert(numeric(18,4), vbap_zwert),0);

UPDATE fact_salesorder_tmptbl f
SET ct_TargetQty  =  ifnull(vbap_zmeng,0)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl vbk ON vbk.VBAK_VBELN  =  f.dd_SalesDocNo AND vbk.VBAP_POSNR  =  f.dd_SalesItemNo AND vbk.VBEP_ETENR  =  f.dd_ScheduleNo AND f.dd_ScheduleNo  =  vbk.v_SalesSchedNo
WHERE ct_TargetQty  <>  ifnull(vbap_zmeng,0);

UPDATE fact_salesorder_tmptbl f
SET f.ct_CumOrderQty  =  ifnull(vbk.VBAP_KWMENG, 0)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl vbk ON vbk.VBAK_VBELN  =  f.dd_SalesDocNo AND vbk.VBAP_POSNR  =  f.dd_SalesItemNo AND vbk.VBEP_ETENR  =  f.dd_ScheduleNo
WHERE ifnull(f.ct_CumOrderQty,-1) <> ifnull(vbk.VBAP_KWMENG, 0);


/* Update this column separately to avoid the previous insert from failing */

UPDATE fact_salesorder_tmptbl f
SET Dim_CustomerRiskCategoryId  = ifnull(c.dim_salesriskcategoryid , 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl ft ON ft.VBEP_ETENR  =  f.dd_ScheduleNo AND ft.VBAP_POSNR  =  f.dd_SalesItemNo AND ft.VBAK_VBELN  =  f.dd_SalesDocNo
	 LEFT JOIN TMP_UPD_Dim_CustomerRiskCategoryId c ON c.CUSTOMER  =  VBAK_KUNNR and c.CreditControlArea  =  VBAK_KKBER
WHERE f.Dim_CustomerRiskCategoryId <> ifnull(c.dim_salesriskcategoryid , 1);

UPDATE fact_salesorder_tmptbl fo
SET Dim_UnitOfMeasureId  =  IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP_VBEP WHERE VBAP_KMEIN IS NOT NULL) ON fo.dd_SalesDocNo = VBAK_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = VBEP_ETENR
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom ON uom.UOM  =  vbap_kmein
WHERE ifnull(fo.Dim_UnitOfMeasureId,-1) <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET Dim_UnitOfMeasureId  =  1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP_VBEP WHERE VBAP_KMEIN IS NULL) ON fo.dd_SalesDocNo = VBAK_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = VBEP_ETENR
WHERE ifnull(fo.Dim_UnitOfMeasureId,-1) <> 1;

UPDATE fact_salesorder_tmptbl fo
SET Dim_BaseUoMid  =   IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP_VBEP WHERE vbap_meins IS NOT NULL) ON fo.dd_SalesDocNo = VBAK_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = VBEP_ETENR
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom ON uom.UOM  =  vbap_meins
WHERE ifnull(fo.Dim_BaseUoMid,-1) <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET Dim_BaseUoMid  =   1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP_VBEP WHERE vbap_meins IS NULL) ON fo.dd_SalesDocNo = VBAK_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = VBEP_ETENR
WHERE ifnull(fo.Dim_BaseUoMid,-1) <> 1;

UPDATE fact_salesorder_tmptbl fo
SET Dim_SalesUoMid  =  IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP_VBEP WHERE vbap_vrkme IS NOT NULL) ON fo.dd_SalesDocNo = VBAK_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = VBEP_ETENR
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom ON uom.UOM  =  vbap_vrkme
WHERE ifnull(fo.Dim_SalesUoMid,-1) <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET Dim_SalesUoMid  =  1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP_VBEP WHERE vbap_vrkme IS NULL) ON fo.dd_SalesDocNo = VBAK_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = VBEP_ETENR
WHERE ifnull(fo.Dim_SalesUoMid,-1) <> 1;


DROP TABLE IF EXISTS dim_profitcenter_upd_701;

CREATE TABLE dim_profitcenter_upd_701
AS
SELECT max_holder_701.maxid+row_number() over(order by '') iid,
       VBAP_PRCTR,
	   VBAK_KOKRS,
	   VBAK_ERDAT,
	   convert(bigint, 1) dim_profitcenterid
FROM max_holder_701,
     VBAK_VBAP_VBEP
     INNER JOIN Dim_Plant pl ON pl.PlantCode  =  VBAP_WERKS
     INNER JOIN Dim_Company co ON co.CompanyCode  =  pl.CompanyCode
	 INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
WHERE  NOT EXISTS (SELECT 1
                   FROM fact_salesorder_useinsub f
                   WHERE f.dd_SalesDocNo  =  VBAK_VBELN
				         AND f.dd_SalesItemNo  =  VBAP_POSNR
						 AND f.dd_ScheduleNo  =  VBEP_ETENR)
       AND EXISTS (SELECT 1
	               FROM dim_date mdt
				   WHERE mdt.DateValue  =  vbap_erdat
				         AND mdt.Dim_Dateid > 1);

DROP TABLE IF EXISTS fact_salesorder_useinsub;


DROP TABLE IF EXISTS TMP_minvalidto_sopc;
CREATE TABLE TMP_minvalidto_sopc
AS
SELECT pc.ProfitCenterCode,
       pc.ControllingArea,
	   VBAK_ERDAT,
	   MIN(pc.ValidTo) ValidTo
FROM dim_profitcenter_upd_701 u,
     dim_profitcenter pc
WHERE pc.ProfitCenterCode  =  VBAP_PRCTR
      AND pc.ControllingArea  =  VBAK_KOKRS
      AND pc.ValidTo >=  VBAK_ERDAT
      AND pc.RowIsCurrent  =  1
GROUP BY pc.ProfitCenterCode,
         pc.ControllingArea,
		 VBAK_ERDAT;

DROP TABLE IF EXISTS TMP_minvalidto_sopc2;
CREATE TABLE TMP_minvalidto_sopc2
AS
SELECT a.*,pc.dim_profitcenterid
FROM TMP_minvalidto_sopc a,
     dim_profitcenter pc
WHERE a.ProfitCenterCode = pc.ProfitCenterCode
      AND a.ControllingArea = pc.ControllingArea
      AND a.ValidTo = pc.ValidTo;


UPDATE dim_profitcenter_upd_701 u
SET dim_profitcenterid  = IFNULL(pc.dim_profitcenterid, 1)
FROM dim_profitcenter_upd_701 u
     INNER JOIN TMP_minvalidto_sopc2 pc ON pc.ProfitCenterCode  =  VBAP_PRCTR AND pc.ControllingArea = VBAK_KOKRS AND pc.VBAK_ERDAT = u.VBAK_ERDAT
WHERE ifnull(u.dim_profitcenterid,-1) <> IFNULL(pc.dim_profitcenterid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ProfitCenterId  = IFNULL(ud.Dim_ProfitCenterId, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN dim_profitcenter_upd_701 ud ON tmp.fact_salesorderid  =  ud.iid
WHERE ifnull(tmp.Dim_ProfitCenterId,-1) <> IFNULL(ud.Dim_ProfitCenterId, 1);

DROP TABLE IF EXISTS dim_profitcenter_upd_701;

/* 22 Dec 2015 CristianT Start: Replaced this combine with an INSERT stmt
call vectorwise (combine 'fact_salesorder+fact_salesorder_tmptbl')
*/

alter table fact_salesorder_tmptbl add constraint primary key (dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo);

INSERT INTO fact_salesorder
(
       fact_salesorderid,
       dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       ct_ScheduleQtySalesUnit,
       ct_ConfirmedQty,
       ct_CorrectedQty,
       amt_UnitPrice,
       ct_PriceUnit,
       amt_ScheduleTotal,
       amt_StdCost,
       amt_TargetValue,
       amt_Tax,
       ct_TargetQty,
       amt_ExchangeRate,
       amt_ExchangeRate_GBL,
       ct_OverDlvrTolerance,
       ct_UnderDlvrTolerance,
       Dim_DateidSalesOrderCreated,
       Dim_DateidFirstDate,
       Dim_DateidSchedDeliveryReq,
       Dim_DateidSchedDlvrReqPrev,
       Dim_DateidSchedDelivery,
       Dim_DateidGoodsIssue,
       Dim_DateidMtrlAvail,
       Dim_DateidLoading,
       Dim_DateidTransport,
       Dim_DateidGuaranteedate,
       Dim_Currencyid,
       Dim_ProductHierarchyid,
       Dim_Plantid,
       Dim_Companyid,
       Dim_StorageLocationid,
       Dim_SalesDivisionid,
       Dim_ShipReceivePointid,
       Dim_DocumentCategoryid,
       dim_SalesDocumentTypeid,
       Dim_SalesOrgid,
       Dim_CustomerID,
       Dim_DateidValidFrom,
       Dim_DateidValidTo,
       Dim_SalesGroupid,
       Dim_CostCenterid,
       Dim_ControllingAreaid,
       Dim_BillingBlockid,
       Dim_TransactionGroupid,
       Dim_SalesOrderRejectReasonid,
       Dim_Partid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       Dim_CustomerGroup1id,
       Dim_CustomerGroup2id,
       Dim_SalesOrderItemCategoryid,
       Dim_ScheduleLineCategoryId,
       dd_ItemRelForDelv,
       Dim_ProfitCenterId,
       Dim_DistributionChannelId,
       dd_BatchNo,
       dd_CreatedBy,
       Dim_DateidNextDate,
	   Dim_RouteId,
	   Dim_SalesRiskCategoryId,
       dd_CreditRep,
	   dd_CreditMgr,
       dd_CreditLimit,
       Dim_CustomerRiskCategoryId,
	   amt_UnitPriceUoM,
	   dim_Currencyid_TRA,
	   dim_Currencyid_GBL,
	   dim_currencyid_STAT,
	   amt_exchangerate_STAT,
     Dim_SalesUoMid,
     Dim_BaseUoMid,
     Dim_UnitOfMeasureId,
     ct_CumOrderQty
)
SELECT fact_salesorderid,
       dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       ct_ScheduleQtySalesUnit,
       ct_ConfirmedQty,
       ct_CorrectedQty,
       amt_UnitPrice,
       ct_PriceUnit,
       amt_ScheduleTotal,
       amt_StdCost,
       amt_TargetValue,
       amt_Tax,
       ct_TargetQty,
       amt_ExchangeRate,
       amt_ExchangeRate_GBL,
       ct_OverDlvrTolerance,
       ct_UnderDlvrTolerance,
       Dim_DateidSalesOrderCreated,
       Dim_DateidFirstDate,
       Dim_DateidSchedDeliveryReq,
       Dim_DateidSchedDlvrReqPrev,
       Dim_DateidSchedDelivery,
       Dim_DateidGoodsIssue,
       Dim_DateidMtrlAvail,
       Dim_DateidLoading,
       Dim_DateidTransport,
       Dim_DateidGuaranteedate,
       Dim_Currencyid,
       Dim_ProductHierarchyid,
       Dim_Plantid,
       Dim_Companyid,
       Dim_StorageLocationid,
       Dim_SalesDivisionid,
       Dim_ShipReceivePointid,
       Dim_DocumentCategoryid,
       dim_SalesDocumentTypeid,
       Dim_SalesOrgid,
       Dim_CustomerID,
       Dim_DateidValidFrom,
       Dim_DateidValidTo,
       Dim_SalesGroupid,
       Dim_CostCenterid,
       Dim_ControllingAreaid,
       Dim_BillingBlockid,
       Dim_TransactionGroupid,
       Dim_SalesOrderRejectReasonid,
       Dim_Partid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       Dim_CustomerGroup1id,
       Dim_CustomerGroup2id,
       Dim_SalesOrderItemCategoryid,
       Dim_ScheduleLineCategoryId,
       dd_ItemRelForDelv,
       Dim_ProfitCenterId,
       Dim_DistributionChannelId,
       dd_BatchNo,
       dd_CreatedBy,
       Dim_DateidNextDate,
	   Dim_RouteId,
	   Dim_SalesRiskCategoryId,
       dd_CreditRep,
	   dd_CreditMgr,
       dd_CreditLimit,
       Dim_CustomerRiskCategoryId,
	   amt_UnitPriceUoM,
	   dim_Currencyid_TRA,
	   dim_Currencyid_GBL,
	   dim_currencyid_STAT,
	   amt_exchangerate_STAT,
     Dim_SalesUoMid,
     Dim_BaseUoMid,
     Dim_UnitOfMeasureId,
     ct_CumOrderQty
FROM fact_salesorder_tmptbl t
WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder f WHERE f.dd_SalesDocNo = t.dd_SalesDocNo AND f.dd_SalesItemNo = t.dd_SalesItemNo AND f.dd_ScheduleNo = t.dd_ScheduleNo);

/* 22 Dec 2015 CristianT End: Replaced this combine with an INSERT stmt */

DROP TABLE IF EXISTS Dim_CostCenterid_2nd_701;
DROP TABLE IF EXISTS fact_salesorder_tmptbl;

CREATE TABLE Dim_CostCenterid_2nd_701
AS
SELECT Dim_CostCenterid,
       cc.Code,
	   cc.ControllingArea,
	   cc.RowIsCurrent,
	   row_number() over (partition by cc.Code,cc.ControllingArea,cc.RowIsCurrent order by cc.ValidTo DESC) tt
FROM Dim_CostCenter cc  ;

CREATE TABLE fact_salesorder_tmptbl
AS
SELECT vbap_kwmeng ct_ScheduleQtySalesUnit,
	   ifnull(VBAP_KWMENG,0) ct_CumOrderQty,
       vbap_kbmeng ct_ConfirmedQty,
       convert(numeric(18,4), 0) as ct_CorrectedQty,
       ifnull(convert(numeric(18,4), vbap_netpr), 0) amt_UnitPrice,
       vbap_kpein ct_PriceUnit,
       convert(numeric(18,4), vbap_netwr) amt_ScheduleTotal,
       ifnull(convert(numeric(18,4), vbap_wavwr),0) amt_StdCost,
       ifnull(convert(numeric(18,4), vbap_zwert),0) amt_TargetValue,
       ifnull(vbap_mwsbp, 0) amt_Tax,	/* Shanthi changes 12 Apr */
       vbap_zmeng ct_TargetQty,
       convert(numeric(18,4), 1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  co.Currency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate,
       convert(numeric(18,4), 1) as amt_ExchangeRate_GBL, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate_GBL,
       vbap_uebto ct_OverDlvrTolerance,
       vbap_untto ct_UnderDlvrTolerance,
       convert(bigint, 1) as Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint, 1) as Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
       convert(bigint, 1) as Dim_DateidSchedDeliveryReq, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
       convert(bigint, 1) as Dim_DateidSchedDelivery,
       convert(bigint, 1) as Dim_DateidGoodsIssue,
       convert(bigint, 1) as Dim_DateidMtrlAvail,
       convert(bigint, 1) as Dim_DateidLoading,
       convert(bigint, 1) as Dim_DateidGuaranteedate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGuaranteedate,
       convert(bigint, 1) as Dim_DateidTransport,
       convert(bigint, 1) as Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  co.currency ),1) Dim_Currencyid,
       convert(bigint, 1) as Dim_ProductHierarchyid, --ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy  =  vbap_prodh),1) Dim_ProductHierarchyid,
       pl.Dim_Plantid Dim_Plantid,
       co.Dim_Companyid  Dim_Companyid,
       convert(bigint, 1) as Dim_StorageLocationid, --ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode  =  vbap_lgort AND sl.plant  =  vbap_werks),1) Dim_StorageLocationid,
       convert(bigint, 1) as Dim_SalesDivisionid, --ifnull((SELECT Dim_SalesDivisionid FROM Dim_SalesDivision sd WHERE sd.DivisionCode  =  vbap_spart),1) Dim_SalesDivisionid,
       convert(bigint, 1) as Dim_ShipReceivePointid, --ifnull((SELECT Dim_ShipReceivePointid FROM Dim_ShipReceivePoint srp WHERE srp.ShipReceivePointCode  =  vbap_vstel),1) Dim_ShipReceivePointid,
       convert(bigint, 1) as Dim_DocumentCategoryid, --ifnull((SELECT Dim_DocumentCategoryid FROM Dim_DocumentCategory dc WHERE dc.DocumentCategory  =  vbak_vbtyp),1) Dim_DocumentCategoryid,
       convert(bigint, 1) as Dim_SalesDocumentTypeid, --ifnull((SELECT Dim_SalesDocumentTypeid FROM Dim_SalesDocumentType sdt WHERE sdt.DocumentType  =  vbak_auart),1) Dim_SalesDocumentTypeid,
       convert(bigint, 1) as Dim_SalesOrgid, --ifnull((SELECT Dim_SalesOrgid FROM Dim_SalesOrg so WHERE so.SalesOrgCode  =  vbak_vkorg),1) Dim_SalesOrgid,
       convert(bigint, 1) as Dim_CustomerID, --ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber  =  vbak_kunnr),1) Dim_CustomerID,
       convert(bigint, 1) as Dim_ScheduleLineCategoryId,
       convert(bigint, 1) as Dim_DateidValidFrom, --ifnull((SELECT Dim_Dateid FROM Dim_Date vf WHERE vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidFrom,
       convert(bigint, 1) as Dim_DateidValidTo, --ifnull((SELECT Dim_Dateid FROM Dim_Date vt WHERE vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidTo,
       convert(bigint, 1) as Dim_SalesGroupid, --ifnull((SELECT Dim_SalesGroupid FROM Dim_SalesGroup sg WHERE sg.SalesGroupCode  =  vbak_vkgrp),1) Dim_SalesGroupid ,
       convert(bigint, 1) as Dim_CostCenterid, --ifnull((SELECT Dim_CostCenterid FROM Dim_CostCenterid_2nd_701 cc WHERE cc.Code  =  vbak_kostl AND cc.ControllingArea  =  vbak_kokrs AND cc.RowIsCurrent  =  1 and tt = 1),1) Dim_CostCenterid,
       convert(bigint, 1) as Dim_ControllingAreaid, --ifnull((SELECT Dim_ControllingAreaid FROM Dim_ControllingArea ca WHERE ca.ControllingAreaCode  =  vbak_kokrs),1) Dim_ControllingAreaid,
       convert(bigint, 1) as Dim_BillingBlockid, --ifnull((SELECT Dim_BillingBlockid FROM Dim_BillingBlock bb WHERE bb.BillingBlockCode  =  vbap_faksp),1) Dim_BillingBlockid,
       convert(bigint, 1) as Dim_TransactionGroupid, --ifnull((SELECT Dim_TransactionGroupid FROM Dim_TransactionGroup tg WHERE tg.TransactionGroup  =  vbak_trvog),1) Dim_TransactionGroupid,
       convert(bigint, 1) as Dim_SalesOrderRejectReasonid, --ifnull((SELECT Dim_SalesOrderRejectReasonid FROM Dim_SalesOrderRejectReason sorr WHERE sorr.RejectReasonCode  =  vbap_abgru),1) Dim_SalesOrderRejectReasonid,
       convert(bigint, 1) as Dim_Partid, --ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS),1) Dim_Partid,
       convert(bigint, 1) as Dim_SalesOrderHeaderStatusid, --ifnull((SELECT Dim_SalesOrderHeaderStatusid FROM Dim_SalesOrderHeaderStatus sohs WHERE sohs.SalesDocumentNumber  =  VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
       convert(bigint, 1) as Dim_SalesOrderItemStatusid, --ifnull((SELECT sois.Dim_SalesOrderItemStatusid FROM Dim_SalesOrderItemStatus sois WHERE sois.SalesDocumentNumber  =  VBAP_VBELN AND sois.SalesItemNumber  =  VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
       convert(bigint, 1) as Dim_CustomerGroup1id, --ifnull((SELECT cg1.Dim_CustomerGroup1id FROM Dim_CustomerGroup1 cg1 WHERE cg1.CustomerGroup  =  VBAK_KVGR1),1) Dim_CustomerGroup1id,
       convert(bigint, 1) as Dim_CustomerGroup2id, --ifnull((SELECT cg2.Dim_CustomerGroup2id FROM Dim_CustomerGroup2 cg2 WHERE cg2.CustomerGroup  =  VBAK_KVGR2),1) Dim_CustomerGroup2id,
       ifnull((soic.Dim_SalesOrderItemCategoryid),1) Dim_salesorderitemcategoryid,
       'Not Set' dd_ItemRelForDelv,
       convert(bigint, 1) as Dim_ProfitCenterId,
       convert(bigint, 1) as Dim_DistributionChannelId, --ifnull((SELECT dc.Dim_DistributionChannelid FROM dim_DistributionChannel dc WHERE   dc.DistributionChannelCode  =  VBAK_VTWEG AND dc.RowIsCurrent  =  1), 1) Dim_DistributionChannelId,
	   convert(bigint, 1) as Dim_UnitOfMeasureId, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_kmein AND uom.RowIsCurrent  =  1), 1) Dim_UnitOfMeasureId,
	   convert(bigint, 1) as Dim_BaseUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_meins AND uom.RowIsCurrent  =  1), 1) Dim_BaseUoMid,
	   convert(bigint, 1) as Dim_SalesUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_vrkme AND uom.RowIsCurrent  =  1), 1) Dim_SalesUoMid,
       ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
       ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
       convert(bigint, 1) as Dim_DateidNextDate, --ifnull((SELECT nd.Dim_Dateid FROM Dim_Date nd WHERE nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidNextDate,
       convert(bigint, 1) as Dim_RouteId, --ifnull((SELECT r.dim_routeid from dim_route r where r.RouteCode  =  VBAP_ROUTE and r.RowIsCurrent  =  1),1) Dim_RouteId,
	   dd_SalesDocNo,
	   dd_SalesItemNo,
	   dd_ScheduleNo,
	   convert(bigint, 1) as Dim_SalesRiskCategoryId, --ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER and src.RowIsCurrent  =  1),1) Dim_SalesRiskCategoryId,
	   'Not Set' as dd_CreditRep, --ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditRep,
	   'Not Set' as dd_CreditMgr, --ifnull((select upd_dd_CreditMgr1 FROM TMP_UPD_dd_CreditMgr1 bm WHERE bm.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditMgr,
	   convert(numeric(18,4), 0) as dd_CreditLimit, --ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER  =  VBAK_KUNNR ),0) dd_CreditLimit,
       convert(bigint, 1) as Dim_CustomerRiskCategoryId, --ifnull((select c.dim_salesriskcategoryid from TMP_UPD_Dim_CustomerRiskCategoryId c where c.CUSTOMER  =  VBAK_KUNNR and c.CreditControlArea  =  VBAK_KKBER),1) Dim_CustomerRiskCategoryId,
	   convert(numeric(18,4), CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END) amt_UnitPriceUoM,
       convert(bigint, 1) as Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid WHERE cur.CurrencyCode  =  VBAP_WAERK),1) Dim_Currencyid_TRA,
       convert(bigint, 1) as dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pGlobalCurrency),1) dim_Currencyid_GBL,
       convert(bigint, 1) as dim_currencyid_STAT, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  vbak_stwae),1) dim_currencyid_STAT,
       convert(numeric(18,4), 1) as amt_exchangerate_STAT, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate  =  0 AND z.pToCurrency  =  vbak_stwae),1) amt_exchangerate_STAT
	   VBAP_WAERK,
	   PRSDT,
	   vbak_audat,
	   pGlobalCurrency,
	   co.Currency as currency,
	   vbap_erdat,
	   pl.CompanyCode as companycode,
	   VBAP_STADAT,
	   vbak_vdatu,
	   vbak_gwldt,
	   vbap_prodh,
	   vbap_lgort,
	   vbap_werks,
	   vbap_spart,
	   vbap_vstel,
	   vbak_vbtyp,
	   vbak_auart,
	   vbak_vkorg,
	   vbak_kunnr,
	   vbak_guebg,
	   vbak_gueen,
	   vbak_vkgrp,
	   vbak_kostl,
	   vbak_kokrs,
	   vbap_faksp,
	   vbak_trvog,
	   vbap_abgru,
	   VBAP_MATNR,
	   VBAK_KVGR1,
	   VBAK_KVGR2,
	   VBAK_VTWEG,
	   vbap_kmein,
	   vbap_meins,
	   vbap_vrkme,
	   VBAK_CMNGV,
	   VBAP_ROUTE,
	   VBAK_CTLPC,
	   VBAK_KKBER,
	   vbak_stwae,
	   VBAK_ERDAT,
	   VBAP_PRCTR
FROM fact_salesorder so,
     VBAK_VBAP,
     dim_SalesOrderItemCategory soic,
     Dim_Plant pl,
     Dim_Company co,
	 variable_holder_701
WHERE pl.PlantCode = VBAP_WERKS
      AND co.CompanyCode = pl.CompanyCode
      AND VBAP_VBELN = dd_SalesDocNo
	  AND VBAP_POSNR = dd_SalesItemNo
	  AND soic.SalesOrderItemCategory = VBAP_PSTYV
	  AND soic.RowIsCurrent = 1
	  AND dd_ScheduleNo = 0;	/* Shanthi changes - sync on 12 Apr */

UPDATE fact_salesorder_tmptbl so
SET so.Dim_ProfitCenterId = ifnull(pc.dim_profitcenterid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_profitcenter WHERE RowIsCurrent = 1) pc
         ON pc.ProfitCenterCode  =  VBAP_PRCTR AND pc.ControllingArea  =  VBAK_KOKRS AND pc.ValidTo >=  VBAK_ERDAT
WHERE so.Dim_ProfitCenterId <> ifnull(pc.dim_profitcenterid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.amt_ExchangeRate = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' AND pFromExchangeRate = 0) z
	     ON z.pFromCurrency  = so.VBAP_WAERK AND z.pDate = ifnull(so.PRSDT,so.vbak_audat) AND z.pToCurrency = so.Currency
WHERE so.amt_ExchangeRate <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl so
SET so.amt_ExchangeRate_GBL = 1
WHERE so.amt_ExchangeRate_GBL <> 1;

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DateidSalesOrderCreated = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN dim_date dd ON dd.DateValue = so.vbap_erdat AND dd.CompanyCode = so.CompanyCode
WHERE so.Dim_DateidSalesOrderCreated <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DateidFirstDate = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN dim_date dd ON dd.DateValue = so.VBAP_STADAT AND dd.CompanyCode = so.CompanyCode
WHERE so.Dim_DateidFirstDate <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DateidSchedDeliveryReq = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN dim_date dd ON dd.DateValue = so.vbak_vdatu AND dd.CompanyCode = so.CompanyCode
WHERE so.Dim_DateidSchedDeliveryReq <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DateidGuaranteedate = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN dim_date dd ON dd.DateValue = so.vbak_gwldt AND dd.CompanyCode = so.CompanyCode
WHERE so.Dim_DateidGuaranteedate <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_Currencyid = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  so.currency
WHERE so.Dim_Currencyid <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_ProductHierarchy ph ON ph.ProductHierarchy  =  vbap_prodh
WHERE so.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_StorageLocation sl ON sl.LocationCode  =  vbap_lgort AND sl.plant  =  vbap_werks
WHERE so.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesDivisionid = ifnull(sd.Dim_SalesDivisionid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesDivision sd ON sd.DivisionCode  =  vbap_spart
WHERE so.Dim_SalesDivisionid <> ifnull(sd.Dim_SalesDivisionid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_ShipReceivePointid = ifnull(srp.Dim_ShipReceivePointid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_ShipReceivePoint srp ON srp.ShipReceivePointCode  =  vbap_vstel
WHERE so.Dim_ShipReceivePointid <> ifnull(srp.Dim_ShipReceivePointid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DocumentCategoryid = ifnull(dc.Dim_DocumentCategoryid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_DocumentCategory dc ON dc.DocumentCategory  =  vbak_vbtyp
WHERE so.Dim_DocumentCategoryid <> ifnull(dc.Dim_DocumentCategoryid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesDocumentTypeid = ifnull(sdt.Dim_SalesDocumentTypeid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesDocumentType sdt ON sdt.DocumentType  =  vbak_auart
WHERE so.Dim_SalesDocumentTypeid <> ifnull(sdt.Dim_SalesDocumentTypeid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesOrgid = ifnull(sorg.Dim_SalesOrgid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesOrg sorg ON sorg.SalesOrgCode  =  vbak_vkorg
WHERE so.Dim_SalesOrgid <> ifnull(sorg.Dim_SalesOrgid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CustomerID = ifnull(cust.Dim_CustomerID, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Customer cust ON cust.CustomerNumber  =  vbak_kunnr
WHERE so.Dim_CustomerID <> ifnull(cust.Dim_CustomerID, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DateidValidFrom = ifnull(vf.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Date vf ON vf.DateValue  =  vbak_guebg AND vf.CompanyCode  = so.CompanyCode
WHERE so.Dim_DateidValidFrom <> ifnull(vf.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DateidValidTo = ifnull(vt.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Date vt ON vt.DateValue  =  vbak_gueen AND vt.CompanyCode  = so.CompanyCode
WHERE so.Dim_DateidValidTo <> ifnull(vt.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesGroupid = ifnull(sg.Dim_SalesGroupid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesGroup sg ON sg.SalesGroupCode  =  vbak_vkgrp
WHERE so.Dim_SalesGroupid <> ifnull(sg.Dim_SalesGroupid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CostCenterid = ifnull(cc.Dim_CostCenterid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM Dim_CostCenterid_2nd_701 WHERE RowIsCurrent = 1 and tt = 1)cc
	     ON cc.Code  =  vbak_kostl AND cc.ControllingArea  =  vbak_kokrs
WHERE so.Dim_CostCenterid <> ifnull(cc.Dim_CostCenterid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_ControllingAreaid = ifnull(ca.Dim_ControllingAreaid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_ControllingArea ca ON ca.ControllingAreaCode  =  vbak_kokrs
WHERE so.Dim_ControllingAreaid <> ifnull(ca.Dim_ControllingAreaid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_BillingBlockid = ifnull(bb.Dim_BillingBlockid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_BillingBlock bb ON bb.BillingBlockCode  =  vbap_faksp
WHERE so.Dim_BillingBlockid <> ifnull(bb.Dim_BillingBlockid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_TransactionGroupid = ifnull(tg.Dim_TransactionGroupid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_TransactionGroup tg ON tg.TransactionGroup  =  vbak_trvog
WHERE so.Dim_TransactionGroupid <> ifnull(tg.Dim_TransactionGroupid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesOrderRejectReasonid = ifnull(sorr.Dim_SalesOrderRejectReasonid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesOrderRejectReason sorr ON sorr.RejectReasonCode  =  vbap_abgru
WHERE so.Dim_SalesOrderRejectReasonid <> ifnull(sorr.Dim_SalesOrderRejectReasonid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_Partid = ifnull(dp.dim_partid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN dim_part dp ON dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS
WHERE so.Dim_Partid <> ifnull(dp.dim_partid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesOrderHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesOrderHeaderStatus sohs ON sohs.SalesDocumentNumber  = dd_salesdocno
WHERE so.Dim_SalesOrderHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesOrderItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesOrderItemStatus sois ON sois.SalesDocumentNumber  =  dd_salesdocno AND sois.SalesItemNumber  =  dd_salesitemno
WHERE so.Dim_SalesOrderItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CustomerGroup1id = ifnull(cg1.Dim_CustomerGroup1id, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_CustomerGroup1 cg1 ON cg1.CustomerGroup  =  VBAK_KVGR1
WHERE so.Dim_CustomerGroup1id <> ifnull(cg1.Dim_CustomerGroup1id, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CustomerGroup2id = ifnull(cg2.Dim_CustomerGroup2id, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_CustomerGroup2 cg2 ON cg2.CustomerGroup  =  VBAK_KVGR2
WHERE so.Dim_CustomerGroup2id <> ifnull(cg2.Dim_CustomerGroup2id, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_DistributionChannel WHERE RowIsCurrent  =  1) dc
	     ON dc.DistributionChannelCode  =  VBAK_VTWEG
WHERE so.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_UnitOfMeasureId = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE rowiscurrent = 1) uom
	     ON uom.UOM  =  vbap_kmein
WHERE so.Dim_UnitOfMeasureId <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_BaseUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE rowiscurrent = 1) uom
	     ON uom.UOM  =  vbap_meins
WHERE so.Dim_BaseUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE rowiscurrent = 1) uom
	     ON uom.UOM  =  vbap_vrkme
WHERE so.Dim_SalesUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DateidNextDate = ifnull(nd.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Date nd ON nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = so.CompanyCode
WHERE so.Dim_DateidNextDate <> ifnull(nd.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_RouteId = ifnull(r.dim_routeid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_route WHERE rowiscurrent = 1) r
	     ON r.RouteCode  =  VBAP_ROUTE
WHERE so.Dim_RouteId <> ifnull(r.dim_routeid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesRiskCategoryId = ifnull(src.Dim_SalesRiskCategoryId, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM Dim_SalesRiskCategory WHERE rowiscurrent = 1) src
	     ON src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER
WHERE so.Dim_SalesRiskCategoryId <> ifnull(src.Dim_SalesRiskCategoryId, 1);

UPDATE fact_salesorder_tmptbl so
SET so.dd_CreditRep = ifnull(b.upd_dd_CreditRep1, 'Not Set')
FROM fact_salesorder_tmptbl so
     LEFT JOIN TMP_UPD_dd_CreditRep1 b ON b.BUT050_PARTNER2 = VBAK_KUNNR
WHERE so.dd_CreditRep <> ifnull(b.upd_dd_CreditRep1, 'Not Set');

UPDATE fact_salesorder_tmptbl so
SET so.dd_CreditMgr = ifnull(bm.upd_dd_CreditMgr1, 'Not Set')
FROM fact_salesorder_tmptbl so
     LEFT JOIN TMP_UPD_dd_CreditMgr1 bm ON bm.BUT050_PARTNER2 = VBAK_KUNNR
WHERE so.dd_CreditMgr <> ifnull(bm.upd_dd_CreditMgr1, 'Not Set');

UPDATE fact_salesorder_tmptbl so
SET so.dd_CreditLimit = ifnull(c.CREDIT_LIMIT, 0)
FROM fact_salesorder_tmptbl so
     LEFT JOIN TMP_UPD_dd_CreditLimit c ON c.CUSTOMER = VBAK_KUNNR
WHERE so.dd_CreditLimit <> ifnull(c.CREDIT_LIMIT, 0);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CustomerRiskCategoryId = ifnull(c.dim_salesriskcategoryid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN TMP_UPD_Dim_CustomerRiskCategoryId c ON c.CUSTOMER = VBAK_KUNNR and c.CreditControlArea = VBAK_KKBER
WHERE so.Dim_CustomerRiskCategoryId <> ifnull(c.dim_salesriskcategoryid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_Currencyid_TRA = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  VBAP_WAERK
WHERE so.Dim_Currencyid_TRA <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.dim_Currencyid_GBL = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  pGlobalCurrency
WHERE so.dim_Currencyid_GBL <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.dim_currencyid_STAT = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  vbak_stwae
WHERE so.dim_currencyid_STAT <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.amt_exchangerate_STAT = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE pFromExchangeRate  =  0 AND fact_script_name  =  'bi_populate_salesorder_fact') z
	     ON z.pFromCurrency   =  VBAP_WAERK AND z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  vbak_stwae
WHERE so.amt_exchangerate_STAT <> ifnull(z.exchangeRate, 1);


/* Now update fact table from the STG table */

MERGE INTO fact_salesorder so
USING fact_salesorder_tmptbl sot ON so.dd_salesdocno = sot.dd_salesdocno AND so.dd_salesitemno = sot.dd_salesitemno AND so.dd_scheduleno = sot.dd_scheduleno
WHEN MATCHED THEN UPDATE
   SET so.ct_ScheduleQtySalesUnit = IFNULL(sot.ct_ScheduleQtySalesUnit, 0),
       so.ct_ConfirmedQty = IFNULL(sot.ct_ConfirmedQty, 0),
       so.ct_CorrectedQty = IFNULL(sot.ct_CorrectedQty, 0),
       so.amt_UnitPrice = IFNULL(sot.amt_UnitPrice, 0),
       so.amt_UnitPriceUoM = IFNULL(sot.amt_UnitPriceUoM, 0),
       so.ct_PriceUnit = IFNULL(sot.ct_PriceUnit, 0),
       so.amt_ScheduleTotal = IFNULL(sot.amt_ScheduleTotal, 0),
       so.amt_StdCost = IFNULL(sot.amt_StdCost, 0),
       so.amt_TargetValue = IFNULL(sot.amt_TargetValue, 0),
       so.amt_Tax = IFNULL(sot.amt_Tax, 0),
       so.ct_TargetQty = IFNULL(sot.ct_TargetQty, 0),
       so.amt_ExchangeRate = IFNULL(sot.amt_ExchangeRate, 1),
       so.amt_ExchangeRate_GBL = IFNULL(sot.amt_ExchangeRate_GBL, 1),
       so.ct_OverDlvrTolerance = IFNULL(sot.ct_OverDlvrTolerance, 0),
       so.ct_UnderDlvrTolerance = IFNULL(sot.ct_UnderDlvrTolerance, 0),
       so.Dim_DateidSalesOrderCreated = IFNULL(sot.Dim_DateidSalesOrderCreated, 1),
       so.Dim_DateidFirstDate = IFNULL(sot.Dim_DateidFirstDate, 1),
       so.Dim_DateidSchedDeliveryReq = IFNULL(sot.Dim_DateidSchedDeliveryReq, 1),
       so.Dim_DateidSchedDelivery = IFNULL(sot.Dim_DateidSchedDelivery, 1),
       so.Dim_DateidGoodsIssue = IFNULL(sot.Dim_DateidGoodsIssue, 1),
       so.Dim_DateidMtrlAvail = IFNULL(sot.Dim_DateidMtrlAvail, 1),
       so.Dim_DateidLoading = IFNULL(sot.Dim_DateidLoading, 1),
       so.Dim_DateidGuaranteedate = IFNULL(sot.Dim_DateidGuaranteedate, 1),
       so.Dim_DateidTransport = IFNULL(sot.Dim_DateidTransport, 1),
       so.Dim_Currencyid = IFNULL(sot.Dim_Currencyid, 1),
       so.Dim_ProductHierarchyid = IFNULL(sot.Dim_ProductHierarchyid, 1),
       so.Dim_Plantid = IFNULL(sot.Dim_Plantid, 1),
       so.Dim_Companyid = IFNULL(sot.Dim_Companyid, 1),
       so.Dim_StorageLocationid = IFNULL(sot.Dim_StorageLocationid, 1),
       so.Dim_SalesDivisionid = IFNULL(sot.Dim_SalesDivisionid, 1),
       so.Dim_ShipReceivePointid = IFNULL(sot.Dim_ShipReceivePointid, 1),
       so.Dim_DocumentCategoryid = IFNULL(sot.Dim_DocumentCategoryid, 1),
       so.Dim_SalesDocumentTypeid = IFNULL(sot.Dim_SalesDocumentTypeid, 1),
       so.Dim_SalesOrgid = IFNULL(sot.Dim_SalesOrgid, 1),
       so.Dim_CustomerID = IFNULL(sot.Dim_CustomerID, 1),
       so.Dim_ScheduleLineCategoryId = IFNULL(sot.Dim_ScheduleLineCategoryId, 1),
       so.Dim_DateidValidFrom = IFNULL(sot.Dim_DateidValidFrom, 1),
       so.Dim_DateidValidTo = IFNULL(sot.Dim_DateidValidTo, 1),
       so.Dim_SalesGroupid = IFNULL(sot.Dim_SalesGroupid, 1),
       so.Dim_CostCenterid = IFNULL(sot.Dim_CostCenterid, 1),
       so.Dim_ControllingAreaid = IFNULL(sot.Dim_ControllingAreaid, 1),
       so.Dim_BillingBlockid = IFNULL(sot.Dim_BillingBlockid, 1),
       so.Dim_TransactionGroupid = IFNULL(sot.Dim_TransactionGroupid, 1),
       so.Dim_Partid = IFNULL(sot.Dim_Partid, 1),
       so.Dim_SalesOrderHeaderStatusid = IFNULL(sot.Dim_SalesOrderHeaderStatusid, 1),
       so.Dim_SalesOrderItemStatusid = IFNULL(sot.Dim_SalesOrderItemStatusid, 1),
       so.Dim_CustomerGroup1id = IFNULL(sot.Dim_CustomerGroup1id, 1),
       so.Dim_CustomerGroup2id = IFNULL(sot.Dim_CustomerGroup2id, 1),
       so.Dim_salesorderitemcategoryid = IFNULL(sot.Dim_salesorderitemcategoryid, 1),
       so.dd_ItemRelForDelv = IFNULL(sot.dd_ItemRelForDelv, 'Not Set'),
       so.Dim_ProfitCenterId = IFNULL(sot.Dim_ProfitCenterId, 1),
       so.Dim_DistributionChannelId = IFNULL(sot.Dim_DistributionChannelId, 1),
       so.Dim_UnitOfMeasureId = IFNULL(sot.Dim_UnitOfMeasureId, 1),
       so.Dim_BaseUoMid = IFNULL(sot.Dim_BaseUoMid, 1),
       so.Dim_SalesUoMid = IFNULL(sot.Dim_SalesUoMid, 1),
       so.dd_BatchNo = IFNULL(sot.dd_BatchNo, 'Not Set'),
       so.dd_CreatedBy = IFNULL(sot.dd_CreatedBy, 'Not Set'),
       so.Dim_DateidNextDate = IFNULL(sot.Dim_DateidNextDate, 1),
       so.Dim_RouteId = IFNULL(sot.Dim_RouteId, 1),
       so.Dim_SalesRiskCategoryId = IFNULL(sot.Dim_SalesRiskCategoryId, 1),
       so.dd_CreditRep = IFNULL(sot.dd_CreditRep, 'Not Set'),
       so.dd_CreditMgr = IFNULL(sot.dd_CreditMgr, 'Not Set'),
       so.dd_CreditLimit = IFNULL(sot.dd_CreditLimit, 0),
       so.Dim_CustomerRiskCategoryId = IFNULL(sot.Dim_CustomerRiskCategoryId, 1),
       so.ct_CumOrderQty = IFNULL(sot.ct_CumOrderQty, 0),
       so.dim_Currencyid_TRA = IFNULL(sot.dim_Currencyid_TRA, 1),
       so.dim_Currencyid_GBL = IFNULL(sot.dim_Currencyid_GBL, 1),
       so.dim_currencyid_STAT = IFNULL(sot.dim_currencyid_STAT, 1),
       so.amt_exchangerate_STAT = IFNULL(sot.amt_exchangerate_STAT, 1);


DROP TABLE IF EXISTS max_holder_701;
CREATE TABLE max_holder_701
AS
SELECT IFNULL(max(fact_salesorderid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) AS maxid
FROM fact_salesorder;

DROP TABLE IF EXISTS fact_salesorder_tmptbl;
CREATE TABLE fact_salesorder_tmptbl
AS
SELECT *
FROM fact_salesorder
WHERE 1 = 2;

DROP TABLE IF EXISTS fact_salesorder_useinsub;
CREATE TABLE fact_salesorder_useinsub
AS
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
	   dd_ScheduleNo
FROM fact_salesorder;


INSERT INTO fact_salesorder_tmptbl
(
            fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
            Dim_routeid,
	        Dim_SalesRiskCategoryId,
            dd_CreditRep,
	        dd_CreditMgr,
            dd_CreditLimit,
            Dim_CustomerRiskCategoryId,
	        amt_UnitPriceUoM,
		    dim_Currencyid_TRA,
		    dim_Currencyid_GBL,
		    dim_currencyid_STAT,
		    amt_exchangerate_STAT,
		    ct_CumOrderQty
)
SELECT max_holder_701.maxid + row_number() over(order by ''),
       vbap_vbeln AS dd_SalesDocNo,
       vbap_posnr AS dd_SalesItemNo,
       0 AS dd_ScheduleNo,
       vbap_kwmeng AS ct_ScheduleQtySalesUnit,
       vbap_kbmeng AS ct_ConfirmedQty,
       convert(numeric(18,4), 0) AS ct_CorrectedQty,
       ifnull(convert(numeric(18,4), vbap_netpr),0) AS amt_UnitPrice,
       vbap_kpein AS ct_PriceUnit,
       convert(numeric(18,4), vbap_netwr) AS amt_ScheduleTotal,
       ifnull(convert(numeric(18,4), vbap_wavwr),0) AS amt_StdCost,
       ifnull(convert(numeric(18,4), vbap_zwert),0) AS amt_TargetValue,
       ifnull(vbap_mwsbp ,0) AS amt_Tax,
       vbap_zmeng AS ct_TargetQty,
       convert(numeric(18,4), 1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  co.Currency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate,
	   convert(numeric(18,4), 1) as amt_ExchangeRate_GBL, --ifnull( ( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate_GBL ,
       vbap_uebto AS ct_OverDlvrTolerance,
       vbap_untto AS ct_UnderDlvrTolerance,
       convert(bigint, 1) AS Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint, 1) AS Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
       convert(bigint, 1) AS Dim_DateidSchedDeliveryReq, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
       convert(bigint, 1) AS Dim_DateidSchedDlvrReqPrev, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
       convert(bigint, 1) AS Dim_DateidSchedDelivery,
       convert(bigint, 1) AS Dim_DateidGoodsIssue,
       convert(bigint, 1) AS Dim_DateidMtrlAvail,
       convert(bigint, 1) AS Dim_DateidLoading,
       convert(bigint, 1) AS Dim_DateidTransport,
       convert(bigint, 1) AS Dim_DateidGuaranteedate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGuaranteedate,
       convert(bigint, 1) AS Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  co.currency ),1) Dim_Currencyid,
       convert(bigint, 1) AS Dim_ProductHierarchyid, --ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy  =  vbap_prodh),1) Dim_ProductHierarchyid,
       pl.Dim_Plantid,
       co.Dim_Companyid,
       convert(bigint, 1) AS Dim_StorageLocationid, --ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode  =  vbap_lgort AND sl.plant  =  vbap_werks),1) Dim_StorageLocationid,
       convert(bigint, 1) AS Dim_SalesDivisionid, --ifnull((SELECT Dim_SalesDivisionid FROM Dim_SalesDivision sd WHERE sd.DivisionCode  =  vbap_spart),1) Dim_SalesDivisionid,
       convert(bigint, 1) AS Dim_ShipReceivePointid, --ifnull((SELECT Dim_ShipReceivePointid FROM Dim_ShipReceivePoint srp WHERE srp.ShipReceivePointCode  =  vbap_vstel),1) Dim_ShipReceivePointid,
       convert(bigint, 1) AS Dim_DocumentCategoryid, --ifnull((SELECT Dim_DocumentCategoryid FROM Dim_DocumentCategory dc WHERE dc.DocumentCategory  =  vbak_vbtyp),1) Dim_DocumentCategoryid,
       convert(bigint, 1) AS Dim_SalesDocumentTypeid, --ifnull((SELECT Dim_SalesDocumentTypeid FROM Dim_SalesDocumentType sdt WHERE sdt.DocumentType  =  vbak_auart),1) Dim_SalesDocumentTypeid,
       convert(bigint, 1) AS Dim_SalesOrgid, --ifnull((SELECT Dim_SalesOrgid FROM Dim_SalesOrg so WHERE so.SalesOrgCode  =  vbak_vkorg),1) Dim_SalesOrgid,
       convert(bigint, 1) AS Dim_CustomerID, --ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber  =  vbak_kunnr),1) Dim_CustomerID,
       convert(bigint, 1) AS Dim_DateidValidFrom, --ifnull((SELECT Dim_Dateid FROM Dim_Date vf WHERE vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidFrom,
       convert(bigint, 1) AS Dim_DateidValidTo, --ifnull((SELECT Dim_Dateid FROM Dim_Date vt WHERE vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidTo,
       convert(bigint, 1) AS Dim_SalesGroupid, --ifnull((SELECT Dim_SalesGroupid FROM Dim_SalesGroup sg WHERE sg.SalesGroupCode  =  vbak_vkgrp),1) Dim_SalesGroupid,
	   convert(bigint, 1) AS Dim_CostCenterid,
       convert(bigint, 1) AS Dim_ControllingAreaid, --ifnull((SELECT Dim_ControllingAreaid FROM Dim_ControllingArea ca WHERE ca.ControllingAreaCode  =  vbak_kokrs),1) Dim_ControllingAreaid,
       convert(bigint, 1) AS Dim_BillingBlockid, --ifnull((SELECT Dim_BillingBlockid FROM Dim_BillingBlock bb WHERE bb.BillingBlockCode  =  vbap_faksp),1) Dim_BillingBlockid,
       convert(bigint, 1) AS Dim_TransactionGroupid, --ifnull((SELECT Dim_TransactionGroupid FROM Dim_TransactionGroup tg WHERE tg.TransactionGroup  =  vbak_trvog),1) Dim_TransactionGroupid,
       convert(bigint, 1) AS Dim_SalesOrderRejectReasonid, --ifnull((SELECT Dim_SalesOrderRejectReasonid FROM Dim_SalesOrderRejectReason sorr WHERE sorr.RejectReasonCode  =  vbap_abgru),1) Dim_SalesOrderRejectReasonid,
       convert(bigint, 1) AS Dim_Partid, --ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS),1) Dim_Partid,
       convert(bigint, 1) AS Dim_SalesOrderHeaderStatusid, --ifnull((SELECT Dim_SalesOrderHeaderStatusid FROM Dim_SalesOrderHeaderStatus sohs WHERE sohs.SalesDocumentNumber  =  VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
       convert(bigint, 1) AS Dim_SalesOrderItemStatusid, --ifnull((SELECT Dim_SalesOrderItemStatusid FROM Dim_SalesOrderItemStatus sois WHERE sois.SalesDocumentNumber  =  VBAP_VBELN AND sois.SalesItemNumber  =  VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
       convert(bigint, 1) AS Dim_CustomerGroup1id, --ifnull((SELECT Dim_CustomerGroup1id FROM Dim_CustomerGroup1 cg1 WHERE cg1.CustomerGroup  =  VBAK_KVGR1),1) Dim_CustomerGroup1id,
       convert(bigint, 1) AS Dim_CustomerGroup2id, --ifnull((SELECT Dim_CustomerGroup2id FROM Dim_CustomerGroup2 cg2 WHERE cg2.CustomerGroup  =  VBAK_KVGR2),1) Dim_CustomerGroup2id,
       ifnull((soic.Dim_SalesOrderItemCategoryid),1) AS Dim_SalesOrderItemCategoryid,
       convert(bigint, 1) AS Dim_ScheduleLineCategoryId,
       'Not Set' AS dd_ItemRelForDelv,
	   convert(bigint, 1) AS Dim_ProfitCenterId,
       convert(bigint, 1) AS Dim_DistributionChannelId, --ifnull((SELECT dc.Dim_DistributionChannelid FROM dim_DistributionChannel dc WHERE   dc.DistributionChannelCode  =  VBAK_VTWEG AND dc.RowIsCurrent  =  1), 1) Dim_DistributionChannelId,
       ifnull(VBAP_CHARG,'Not Set') AS dd_BatchNo,
       ifnull(VBAK_ERNAM,'Not Set') AS dd_CreatedBy,
       convert(bigint, 1) AS Dim_DateidNextDate, --ifnull((SELECT nd.Dim_Dateid FROM Dim_Date nd WHERE nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidNextDate,
       convert(bigint, 1) AS Dim_routeid, --ifnull((SELECT r.dim_routeid from dim_route r where r.RouteCode  =  VBAP_ROUTE and r.RowIsCurrent  =  1),1),
	   convert(bigint, 1) AS Dim_SalesRiskCategoryId, --ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER and src.RowIsCurrent  =  1),1),
       'Not Set' AS dd_CreditRep, --ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditRep,
       'Not Set' AS dd_CreditMgr, --ifnull((select upd_dd_CreditMgr1 FROM TMP_UPD_dd_CreditMgr1 bm WHERE bm.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditMgr,
       convert(numeric(18,4), 0) AS dd_CreditLimit, --ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER  =  VBAK_KUNNR ),0) dd_CreditLimit,
       convert(bigint, 1) AS Dim_CustomerRiskCategoryId, --ifnull((select c.dim_salesriskcategoryid from TMP_UPD_Dim_CustomerRiskCategoryId c where c.CUSTOMER  =  VBAK_KUNNR and c.CreditControlArea  =  VBAK_KKBER),1) Dim_CustomerRiskCategoryId,
	   convert(numeric(18,4), CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END) amt_UnitPriceUoM,
       convert(bigint, 1) AS Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  VBAP_WAERK),1) Dim_Currencyid_TRA,
       convert(bigint, 1) AS dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pGlobalCurrency),1) dim_Currencyid_GBL,
       convert(bigint, 1) AS dim_currencyid_STAT, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  vbak_stwae),1) dim_currencyid_STAT,
       convert(numeric(18,4), 1) as amt_exchangerate_STAT, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate  =  0 AND z.pToCurrency  =  vbak_stwae),1) amt_exchangerate_STAT,
	   ifnull(VBAP_KWMENG, 0) ct_CumOrderQty
FROM VBAK_VBAP
     INNER JOIN Dim_Plant pl ON pl.PlantCode  =  VBAP_WERKS
     INNER JOIN Dim_Company co ON co.CompanyCode  =  pl.CompanyCode
     INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
	 CROSS JOIN variable_holder_701
	 CROSS JOIN max_holder_701
WHERE NOT EXISTS (SELECT 1
                  FROM fact_salesorder_useinsub f
                  WHERE f.dd_SalesDocNo  =  VBAP_VBELN
				        AND f.dd_SalesItemNo  =  VBAP_POSNR
						AND f.dd_ScheduleNo  =  0)
      AND EXISTS (SELECT 1
	              FROM dim_date mdt
				  WHERE mdt.DateValue = vbap_erdat
				        AND mdt.Dim_Dateid > 1)
	  AND NOT EXISTS (SELECT 1
	                  FROM vbak_vbap_vbep v
					  WHERE vbak_vbap.VBAP_VBELN = v.VBAK_VBELN
                            AND vbak_vbap.VBAP_POSNR  =  v.VBAP_POSNR);

UPDATE fact_salesorder_tmptbl fo
SET fo.amt_ExchangeRate = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        co.Currency as currency
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
                      INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE pFromExchangeRate  =  0 AND fact_script_name  =  'bi_populate_salesorder_fact') z
	     ON z.pFromCurrency  = VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency  = v.Currency
WHERE fo.amt_ExchangeRate <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.amt_ExchangeRate_GBL = 1
WHERE fo.amt_ExchangeRate_GBL <> 1;

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidSalesOrderCreated = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = vbap_erdat AND dd.CompanyCode = v.CompanyCode
WHERE fo.Dim_DateidSalesOrderCreated <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidFirstDate = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = VBAP_STADAT AND dd.CompanyCode = v.CompanyCode
WHERE fo.Dim_DateidFirstDate <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidSchedDeliveryReq = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = vbak_vdatu AND dd.CompanyCode = v.CompanyCode
WHERE fo.Dim_DateidSchedDeliveryReq <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidSchedDlvrReqPrev = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = vbak_vdatu AND dd.CompanyCode = v.CompanyCode
WHERE fo.Dim_DateidSchedDlvrReqPrev <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidGuaranteedate = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = vbak_gwldt AND dd.CompanyCode = v.CompanyCode
WHERE fo.Dim_DateidGuaranteedate <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_Currencyid = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        co.Currency as currency
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
                      INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  v.currency
WHERE fo.Dim_Currencyid <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_ProductHierarchy ph ON ph.ProductHierarchy  =  vbap_prodh
WHERE fo.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_StorageLocation sl ON sl.LocationCode  =  vbap_lgort AND sl.plant  =  vbap_werks
WHERE fo.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesDivisionid = ifnull(sd.Dim_SalesDivisionid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesDivision sd ON sd.DivisionCode  =  vbap_spart
WHERE fo.Dim_SalesDivisionid <> ifnull(sd.Dim_SalesDivisionid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_ShipReceivePointid = ifnull(srp.Dim_ShipReceivePointid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_ShipReceivePoint srp ON srp.ShipReceivePointCode  =  vbap_vstel
WHERE fo.Dim_ShipReceivePointid <> ifnull(srp.Dim_ShipReceivePointid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DocumentCategoryid = ifnull(dc.Dim_DocumentCategoryid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_DocumentCategory dc ON dc.DocumentCategory  =  vbak_vbtyp
WHERE fo.Dim_DocumentCategoryid <> ifnull(dc.Dim_DocumentCategoryid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesDocumentTypeid = ifnull(sdt.Dim_SalesDocumentTypeid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesDocumentType sdt ON sdt.DocumentType  =  vbak_auart
WHERE fo.Dim_SalesDocumentTypeid <> ifnull(sdt.Dim_SalesDocumentTypeid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesOrgid = ifnull(so.Dim_SalesOrgid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesOrg so ON so.SalesOrgCode  =  vbak_vkorg
WHERE fo.Dim_SalesOrgid <> ifnull(so.Dim_SalesOrgid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CustomerID = ifnull(cust.Dim_CustomerID, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Customer cust ON cust.CustomerNumber  =  vbak_kunnr
WHERE fo.Dim_CustomerID <> ifnull(cust.Dim_CustomerID, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidValidFrom = ifnull(vf.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date vf ON vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  v.CompanyCode
WHERE fo.Dim_DateidValidFrom <> ifnull(vf.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidValidTo = ifnull(vt.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date vt ON vt.DateValue  =  vbak_gueen AND vt.CompanyCode  = v.CompanyCode
WHERE fo.Dim_DateidValidTo <> ifnull(vt.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesGroupid = ifnull(sg.Dim_SalesGroupid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesGroup sg ON sg.SalesGroupCode  =  vbak_vkgrp
WHERE fo.Dim_SalesGroupid <> ifnull(sg.Dim_SalesGroupid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_ControllingAreaid = ifnull(ca.Dim_ControllingAreaid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_ControllingArea ca ON ca.ControllingAreaCode  =  vbak_kokrs
WHERE fo.Dim_ControllingAreaid <> ifnull(ca.Dim_ControllingAreaid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_BillingBlockid = ifnull(bb.Dim_BillingBlockid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_BillingBlock bb ON bb.BillingBlockCode  =  vbap_faksp
WHERE fo.Dim_BillingBlockid <> ifnull(bb.Dim_BillingBlockid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_TransactionGroupid = ifnull(tg.Dim_TransactionGroupid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_TransactionGroup tg ON tg.TransactionGroup  =  vbak_trvog
WHERE fo.Dim_TransactionGroupid <> ifnull(tg.Dim_TransactionGroupid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesOrderRejectReasonid = ifnull(sorr.Dim_SalesOrderRejectReasonid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesOrderRejectReason sorr ON sorr.RejectReasonCode  =  vbap_abgru
WHERE fo.Dim_SalesOrderRejectReasonid <> ifnull(sorr.Dim_SalesOrderRejectReasonid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_Partid = ifnull(dp.dim_partid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN dim_part dp ON dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS
WHERE fo.Dim_Partid <> ifnull(dp.dim_partid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesOrderHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesOrderHeaderStatus sohs ON sohs.SalesDocumentNumber  =  VBAP_VBELN
WHERE fo.Dim_SalesOrderHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesOrderItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesOrderItemStatus sois ON sois.SalesDocumentNumber  =  VBAP_VBELN AND sois.SalesItemNumber  =  VBAP_POSNR
WHERE fo.Dim_SalesOrderItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CustomerGroup1id = ifnull(cg1.Dim_CustomerGroup1id, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_CustomerGroup1 cg1 ON cg1.CustomerGroup  =  VBAK_KVGR1
WHERE fo.Dim_CustomerGroup1id <> ifnull(cg1.Dim_CustomerGroup1id, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CustomerGroup2id = ifnull(cg2.Dim_CustomerGroup2id, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_CustomerGroup2 cg2 ON cg2.CustomerGroup  =  VBAK_KVGR2
WHERE fo.Dim_CustomerGroup2id <> ifnull(cg2.Dim_CustomerGroup2id, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM dim_DistributionChannel WHERE RowIsCurrent  =  1) dc
	     ON dc.DistributionChannelCode  =  VBAK_VTWEG
WHERE fo.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidNextDate = ifnull(nd.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date nd ON nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  v.CompanyCode
WHERE fo.Dim_DateidNextDate <> ifnull(nd.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_routeid = ifnull(r.dim_routeid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM dim_route WHERE rowiscurrent = 1) r
	     ON r.RouteCode  =  VBAP_ROUTE
WHERE fo.Dim_routeid <> ifnull(r.dim_routeid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesRiskCategoryId = ifnull(src.Dim_SalesRiskCategoryId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_SalesRiskCategory WHERE rowiscurrent = 1) src
	     ON src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER
WHERE fo.Dim_SalesRiskCategoryId <> ifnull(src.Dim_SalesRiskCategoryId, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.dd_CreditRep = ifnull(b.upd_dd_CreditRep1, 'Not Set')
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN TMP_UPD_dd_CreditRep1 b ON b.BUT050_PARTNER2  =  VBAK_KUNNR
WHERE fo.dd_CreditRep <> ifnull(b.upd_dd_CreditRep1, 'Not Set');

UPDATE fact_salesorder_tmptbl fo
SET fo.dd_CreditMgr = ifnull(bm.upd_dd_CreditMgr1, 'Not Set')
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN TMP_UPD_dd_CreditMgr1 bm ON bm.BUT050_PARTNER2  =  VBAK_KUNNR
WHERE fo.dd_CreditMgr <> ifnull(bm.upd_dd_CreditMgr1, 'Not Set');

UPDATE fact_salesorder_tmptbl fo
SET fo.dd_CreditLimit = ifnull(c.CREDIT_LIMIT, 0)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN TMP_UPD_dd_CreditLimit c ON c.CUSTOMER  =  VBAK_KUNNR
WHERE fo.dd_CreditLimit <> ifnull(c.CREDIT_LIMIT, 0);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CustomerRiskCategoryId = ifnull(c.dim_salesriskcategoryid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN TMP_UPD_Dim_CustomerRiskCategoryId c ON c.CUSTOMER  =  VBAK_KUNNR and c.CreditControlArea  =  VBAK_KKBER
WHERE fo.Dim_CustomerRiskCategoryId <> ifnull(c.dim_salesriskcategoryid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_Currencyid_TRA = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  VBAP_WAERK
WHERE fo.Dim_Currencyid_TRA <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.dim_Currencyid_GBL = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,pGlobalCurrency FROM VBAK_VBAP x, variable_holder_701) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  pGlobalCurrency
WHERE fo.dim_Currencyid_GBL <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.dim_currencyid_STAT = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  vbak_stwae
WHERE fo.dim_currencyid_STAT <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.amt_exchangerate_STAT = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE pFromExchangeRate  =  0 AND fact_script_name  =  'bi_populate_salesorder_fact') z
	     ON z.pFromCurrency = VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  vbak_stwae
WHERE fo.amt_exchangerate_STAT <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CostCenterid = ifnull(cc.Dim_CostCenterid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_CostCenter WHERE RowIsCurrent = 1 and current_date <= VALIDTO) cc
	     ON cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs
WHERE fo.Dim_CostCenterid <> ifnull(cc.Dim_CostCenterid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_ProfitCenterId  =  ifnull(pc.dim_profitcenterid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM dim_profitcenter WHERE RowIsCurrent  =  1) pc
         ON pc.ProfitCenterCode  =  VBAP_PRCTR AND pc.ControllingArea  =  VBAK_KOKRS AND pc.ValidTo >=  VBAK_ERDAT
WHERE fo.Dim_ProfitCenterId <> ifnull(pc.dim_profitcenterid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_UnitOfMeasureId  =  ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE VBAP_KMEIN IS NOT NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent  =  1) uom
	     ON uom.UOM  =  vbap_kmein
WHERE ifnull(fo.Dim_UnitOfMeasureId,-1) <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_UnitOfMeasureId  =  1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE VBAP_KMEIN IS NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0;

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_BaseUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_meins IS NOT NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent  =  1) uom
	     ON uom.UOM  =  vbap_meins
WHERE ifnull(fo.Dim_BaseUoMid,-1) <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_BaseUoMid  =   1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_meins IS NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0;

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesUoMid  =  ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_vrkme IS NOT NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent  =  1) uom
	     ON uom.UOM  =  vbap_vrkme
WHERE ifnull(fo.Dim_SalesUoMid,-1) <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesUoMid  =  1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_vrkme IS NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0;

/* 22 Dec 2015 CristianT Start: Replaced the combine with INSERT statement
call vectorwise (combine 'fact_salesorder+fact_salesorder_tmptbl')
*/

alter table fact_salesorder_tmptbl add constraint primary key (dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo);

INSERT INTO fact_salesorder
(
            fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
            Dim_routeid,
	        Dim_SalesRiskCategoryId,
            dd_CreditRep,
	        dd_CreditMgr,
            dd_CreditLimit,
            Dim_CustomerRiskCategoryId,
	        amt_UnitPriceUoM,
		    dim_Currencyid_TRA,
		    dim_Currencyid_GBL,
		    dim_currencyid_STAT,
		    amt_exchangerate_STAT,
        Dim_SalesUoMid,
        Dim_BaseUoMid,
        Dim_UnitOfMeasureId,
        ct_CumOrderQty
)
SELECT fact_salesorderid,
       dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       ct_ScheduleQtySalesUnit,
       ct_ConfirmedQty,
       ct_CorrectedQty,
       amt_UnitPrice,
       ct_PriceUnit,
       amt_ScheduleTotal,
       amt_StdCost,
       amt_TargetValue,
       amt_Tax,
       ct_TargetQty,
       amt_ExchangeRate,
       amt_ExchangeRate_GBL,
       ct_OverDlvrTolerance,
       ct_UnderDlvrTolerance,
       Dim_DateidSalesOrderCreated,
       Dim_DateidFirstDate,
       Dim_DateidSchedDeliveryReq,
       Dim_DateidSchedDlvrReqPrev,
       dim_DateidSchedDelivery,
       Dim_DateidGoodsIssue,
       Dim_DateidMtrlAvail,
       Dim_DateidLoading,
       Dim_DateidTransport,
       Dim_DateidGuaranteedate,
       Dim_Currencyid,
       Dim_ProductHierarchyid,
       Dim_Plantid,
       Dim_Companyid,
       Dim_StorageLocationid,
       Dim_SalesDivisionid,
       Dim_ShipReceivePointid,
       Dim_DocumentCategoryid,
       Dim_SalesDocumentTypeid,
       Dim_SalesOrgid,
       Dim_CustomerID,
       Dim_DateidValidFrom,
       Dim_DateidValidTo,
       Dim_SalesGroupid,
       Dim_CostCenterid,
       Dim_ControllingAreaid,
       Dim_BillingBlockid,
       Dim_TransactionGroupid,
       Dim_SalesOrderRejectReasonid,
       Dim_Partid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       Dim_CustomerGroup1id,
       Dim_CustomerGroup2id,
       Dim_SalesOrderItemCategoryid,
       Dim_ScheduleLineCategoryId,
       dd_ItemRelForDelv,
       Dim_ProfitCenterId,
       Dim_DistributionChannelId,
       dd_BatchNo,
       dd_CreatedBy,
       Dim_DateidNextDate,
       Dim_routeid,
	   Dim_SalesRiskCategoryId,
       dd_CreditRep,
	   dd_CreditMgr,
       dd_CreditLimit,
       Dim_CustomerRiskCategoryId,
	   amt_UnitPriceUoM,
       dim_Currencyid_TRA,
	   dim_Currencyid_GBL,
	   dim_currencyid_STAT,
	   amt_exchangerate_STAT,
     Dim_SalesUoMid,
     Dim_BaseUoMid,
     Dim_UnitOfMeasureId,
     ct_CumOrderQty
FROM fact_salesorder_tmptbl t
WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder f WHERE f.dd_SalesDocNo = t.dd_SalesDocNo AND f.dd_SalesItemNo = t.dd_SalesItemNo AND f.dd_ScheduleNo = t.dd_ScheduleNo);
/* 22 Dec 2015 CristianT End: Replaced the combine with INSERT statement */

DROP TABLE IF EXISTS fact_salesorder_useinsub;

UPDATE fact_salesorder fso
SET fso.Dim_SalesMiscId = IFNULL(smisc.Dim_SalesMiscId, 1)
FROM fact_salesorder fso
	INNER JOIN VBAK_VBAP vkp ON fso.dd_SalesDocNo = vkp.VBAP_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = 0
	LEFT JOIN Dim_SalesMisc smisc
		ON smisc.DeliveryDateQuantityFixed  =  ifnull(VBAP_FIXMG,'Not Set')
			AND smisc.FixedQuantity  =  ifnull(VBAP_FMENG, 'Not Set')
			AND smisc.OverDeliveryAllowed  =  ifnull(VBAP_UEBTK, 'Not Set')
			AND smisc.CashDiscountIndicator  =  ifnull(VBAP_SKTOF, 'Not Set')
			AND smisc.ReturnsItem  =  ifnull(VBAP_SHKZG, 'Not Set')
			AND smisc.PricingOk  =  ifnull(VBAP_PRSOK, 'Not Set')
			AND smisc.CustomerNotPostedGoodsReceipt  =  ifnull(VBAP_NACHL, 'Not Set')
			AND smisc.ItemRelevantForDelivery  =  ifnull(VBAP_LFREL, 'Not Set')
			AND smisc.ScheduleConfirmStatus  =  'Not Set'
			AND smisc.InvoiceReceiptIndicator  =  'Not Set'
WHERE fso.Dim_SalesMiscId <> smisc.Dim_SalesMiscId;

UPDATE fact_salesorder fso
SET fso.Dim_SalesMiscId = ifnull(smisc.Dim_SalesMiscId, 1)
FROM fact_salesorder fso
     INNER JOIN VBAK_VBAP_VBEP vkp ON fso.dd_SalesDocNo = vkp.VBAK_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN Dim_SalesMisc smisc
	     ON smisc.DeliveryDateQuantityFixed = ifnull(VBAP_FIXMG,'Not Set')
            AND smisc.FixedQuantity = ifnull(VBAP_FMENG, 'Not Set')
            AND smisc.OverDeliveryAllowed = ifnull(VBAP_UEBTK, 'Not Set')
            AND smisc.CashDiscountIndicator = ifnull(VBAP_SKTOF, 'Not Set')
            AND smisc.ReturnsItem = ifnull(VBAP_SHKZG, 'Not Set')
            AND smisc.PricingOk = ifnull(VBAP_PRSOK, 'Not Set')
            AND smisc.CustomerNotPostedGoodsReceipt = ifnull(VBAP_NACHL, 'Not Set')
            AND smisc.ItemRelevantForDelivery = ifnull(VBAP_LFREL, 'Not Set')
            AND smisc.ScheduleConfirmStatus = ifnull(VBEP_WEPOS, 'Not Set')
            AND smisc.InvoiceReceiptIndicator = ifnull(VBEP_REPOS, 'Not Set')
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_SalesMiscId <> ifnull(smisc.Dim_SalesMiscId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = ifnull(so.Dim_SalesOfficeId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT * FROM VBAK_VBAP_VBEP WHERE vbak_vkbur IS NOT NULL) vkp
	      ON fso.dd_SalesDocNo = vkp.VBAK_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN (SELECT * FROM dim_SalesOffice WHERE RowIsCurrent = 1) so
	     ON so.SalesOfficeCode = vbak_vkbur
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_SalesOfficeId <> ifnull(so.Dim_SalesOfficeId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = ifnull(so.Dim_SalesOfficeId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT * FROM vbak_vbap WHERE vbak_vkbur IS NOT NULL) vkp
	      ON fso.dd_SalesDocNo = vkp.VBAP_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
	 LEFT JOIN (SELECT * FROM dim_SalesOffice WHERE RowIsCurrent = 1) so
	     ON so.SalesOfficeCode = vbak_vkbur
WHERE fso.dd_ScheduleNo = 0
      AND fso.Dim_SalesOfficeId <> ifnull(so.Dim_SalesOfficeId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_CustomerGroupId = ifnull(cg.Dim_CustomerGroupId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_kdgrp
	             FROM vbak_vbap_vbep x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAK_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN (SELECT * FROM dim_CustomerGroup WHERE RowIsCurrent = 1) cg
	     ON cg.CustomerGroup = vkp.knvv_kdgrp
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_CustomerGroupId <> ifnull(cg.Dim_CustomerGroupId, 1);


/* Roxana D - 2017-10-23 - keep the update for Sales Office Code with VBAK_VKBUR only
UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = ifnull(so.Dim_SalesOfficeId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_vkbur
	             FROM vbak_vbap_vbep x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAK_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN (SELECT * FROM dim_SalesOffice WHERE RowIsCurrent = 1) so
         ON so.SalesOfficeCode = vkp.knvv_vkbur
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_SalesOfficeId <> ifnull(so.Dim_SalesOfficeId, 1)
*/

UPDATE fact_salesorder fso
SET fso.Dim_CustomerPaymentTermsId = ifnull(cpt.dim_Customerpaymenttermsid, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_zterm
	             FROM vbak_vbap_vbep x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAK_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN (SELECT * FROM dim_customerpaymentterms WHERE rowiscurrent = 1) cpt
	     ON cpt.PaymentTermCode = vkp.knvv_zterm
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_CustomerPaymentTermsId <> ifnull(cpt.dim_Customerpaymenttermsid, 1);

UPDATE fact_salesorder fso
SET fso.Dim_CustomerGroupId = ifnull(cg.Dim_CustomerGroupId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_kdgrp
	             FROM vbak_vbap x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM dim_CustomerGroup where rowiscurrent = 1) cg
	     ON cg.CustomerGroup = vkp.knvv_kdgrp
WHERE fso.Dim_CustomerGroupId <> ifnull(cg.Dim_CustomerGroupId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = ifnull(so.Dim_SalesOfficeId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_vkbur
	             FROM vbak_vbap x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM dim_SalesOffice where rowiscurrent = 1) so
	     ON so.SalesOfficeCode  =  vkp.knvv_vkbur
WHERE fso.Dim_SalesOfficeId <> ifnull(so.Dim_SalesOfficeId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_CustomerPaymentTermsId = ifnull(cpt.dim_Customerpaymenttermsid, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_zterm
	             FROM vbak_vbap x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM dim_customerpaymentterms where rowiscurrent = 1) cpt
         ON cpt.PaymentTermCode = vkp.knvv_zterm
WHERE fso.Dim_CustomerPaymentTermsId <> ifnull(cpt.dim_Customerpaymenttermsid, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = 1
WHERE fso.Dim_SalesOfficeId IS NULL;

/*
UPDATE       fact_salesorder so
FROM     vbuk v, dim_overallstatusforcreditcheck oscc
SET so.Dim_OverallStatusCreditCheckId  =  oscc.dim_overallstatusforcreditcheckID
WHERE so.dd_SalesDocNo  =  v.VBUK_VBELN
AND oscc.overallstatusforcreditcheck  =  ifnull(v.VBUK_CMGST, 'Not Set')
AND oscc.RowIsCurrent  =  1
AND so.Dim_OverallStatusCreditCheckId <> oscc.dim_overallstatusforcreditcheckID
*/
-- CristiT: Update asta are duplicate, trebuie vazut de ce  ESTE O PB CU dim_overallstatusforcreditcheck SUNT PREA MULTE NOT SET
UPDATE fact_salesorder so
SET so.Dim_OverallStatusCreditCheckId = ifnull(oscc.dim_overallstatusforcreditcheckID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT DISTINCT VBUK_VBELN,VBUK_CMGST FROM vbuk) v
          ON so.dd_SalesDocNo = v.VBUK_VBELN
	 LEFT JOIN (SELECT overallstatusforcreditcheck, MIN(dim_overallstatusforcreditcheckid) dim_overallstatusforcreditcheckid FROM dim_overallstatusforcreditcheck WHERE RowIsCurrent = 1 GROUP BY overallstatusforcreditcheck) oscc
	     ON oscc.overallstatusforcreditcheck  =  ifnull(v.VBUK_CMGST, 'Not Set')
WHERE so.Dim_OverallStatusCreditCheckId <> ifnull(oscc.dim_overallstatusforcreditcheckID, 1);

/*tmp table to handle Dim_BillToPartyPartnerFunctionId updates */

DROP TABLE IF EXISTS tmp1_sof_Dim_CustomerPartnerFunctions;
CREATE TABLE tmp1_sof_Dim_CustomerPartnerFunctions
AS
SELECT cpf.CustomerNumber1,
       cpf.SalesOrgCode,
       cpf.DivisionCode,
       cpf.DistributionChannelCode,
       cpf.PartnerFunction,
       cpf.RowIsCurrent,
       max(PartnerCounter) max_PartnerCounter
FROM Dim_CustomerPartnerFunctions cpf
GROUP BY cpf.CustomerNumber1,
         cpf.SalesOrgCode,
         cpf.DivisionCode,
         cpf.DistributionChannelCode,
         cpf.PartnerFunction,
         cpf.RowIsCurrent;

DROP TABLE IF EXISTS tmp_sof_Dim_CustomerPartnerFunctions;
CREATE TABLE tmp_sof_Dim_CustomerPartnerFunctions
AS
SELECT cpf.CustomerNumber1,
       cpf.SalesOrgCode,
       cpf.DivisionCode,
       cpf.DistributionChannelCode,
       cpf.PartnerFunction,
       cpf.RowIsCurrent,
       Dim_CustomerPartnerFunctionsID
FROM Dim_CustomerPartnerFunctions cpf,
     tmp1_sof_Dim_CustomerPartnerFunctions t
WHERE cpf.CustomerNumber1   =  t.CustomerNumber1
      AND cpf.SalesOrgCode  =  t.SalesOrgCode
      AND cpf.DivisionCode  =  t.DivisionCode
      AND cpf.DistributionChannelCode  =  t.DistributionChannelCode
      AND cpf.PartnerFunction  =  t.PartnerFunction
      AND cpf.RowIsCurrent  =  t.RowIsCurrent
      AND cpf.PartnerCounter  =  t.max_PartnerCounter;

DROP TABLE IF EXISTS tmp1_sof_Dim_CustomerPartnerFunctions;

/*tmp table to handle Dim_BillToPartyPartnerFunctionId updates */

UPDATE fact_salesorder so
SET so.Dim_BillToPartyPartnerFunctionId = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (
SELECT distinct x.vbak_vbeln, x.vbap_posnr, x.vbep_etenr,x.vbak_kunnr,x.vbak_vkorg,x.vbap_spart,x.VBAK_VTWEG
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pBillToPartyPartnerFunction) shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM tmp_sof_Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pBillToPartyPartnerFunction
				      AND x.RowIsCurrent = 1) cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_BillToPartyPartnerFunctionId <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.Dim_BillToPartyPartnerFunctionId = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pBillToPartyPartnerFunction) shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM tmp_sof_Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pBillToPartyPartnerFunction
				      AND x.RowIsCurrent = 1) cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_BillToPartyPartnerFunctionId <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.Dim_PayerPartnerFunctionId = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT  distinct x.vbak_vbeln, x.vbap_posnr, x.vbep_etenr,x.vbak_kunnr, x.vbak_vkorg, x.vbap_spart,x.VBAK_VTWEG
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pPayerPartnerFunction) shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT distinct x.CustomerNumber1, x.SalesOrgCode, x.DivisionCode, x.DistributionChannelCode, x.Dim_CustomerPartnerFunctionsID
                ,row_number()over(partition by  x.CustomerNumber1, x.SalesOrgCode, x.DivisionCode, x.DistributionChannelCode order by '') rowno
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pPayerPartnerFunction
				      AND x.RowIsCurrent = 1) cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
            AND cpf.rowno = 1
WHERE so.dd_scheduleno <> 0
      AND so.Dim_PayerPartnerFunctionId <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.Dim_PayerPartnerFunctionId = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pPayerPartnerFunction) shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pPayerPartnerFunction
				      AND x.RowIsCurrent = 1) cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_PayerPartnerFunctionId <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey
					   AND vb.pCustomPartnerFunctionKey <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);


UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey
					   AND vb.pCustomPartnerFunctionKey <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId1 = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey1
					   AND vb.pCustomPartnerFunctionKey1 <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey1
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey1 <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId1 <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId1 = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey1
					   AND vb.pCustomPartnerFunctionKey1 <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey1
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey1 <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId1 <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId2 = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey2
					   AND vb.pCustomPartnerFunctionKey2 <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey2
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey2 <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId2 <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId2 = IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey2
					   AND vb.pCustomPartnerFunctionKey2 <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey2
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey2 <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId2 <> IFNULL(cpf.Dim_CustomerPartnerFunctionsID, 1);

UPDATE fact_salesorder so
SET so.dd_CustomerPONo  =  ifnull(vbk.VBAK_BSTNK,'Not Set')
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
WHERE so.dd_scheduleno <> 0
      AND so.dd_CustomerPONo <> ifnull(vbk.VBAK_BSTNK,'Not Set');

UPDATE fact_salesorder so
SET so.Dim_CreditRepresentativeId  = ifnull(Dim_CreditRepresentativegroupId, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM dim_creditrepresentativegroup WHERE RowIsCurrent  =  1) cg
	     ON cg.CreditRepresentativeGroup  =  vbk.VBAK_SBGRP
            AND cg.CreditControlArea  =  vbk.VBAK_KKBER
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CreditRepresentativeId <> ifnull(Dim_CreditRepresentativegroupId, 1);

UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup1Id  = ifnull(mpg.Dim_MaterialPriceGroup1Id, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM Dim_MaterialPriceGroup1 WHERE rowiscurrent = 1) mpg
         ON mpg.MaterialPriceGroup1  =  ifnull(vbk.VBAP_MVGR1,'Not Set')
WHERE so.dd_ScheduleNo <> 0
      AND so.Dim_MaterialPriceGroup1Id <> ifnull(mpg.Dim_MaterialPriceGroup1Id, 1);

UPDATE fact_salesorder so
SET so.Dim_DeliveryBlockId  = ifnull(db.Dim_DeliveryBlockId, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM Dim_DeliveryBlock WHERE RowIsCurrent  =  1 ) db
         ON db.DeliveryBlock  =  vbk.VBAK_LIFSK
WHERE so.dd_ScheduleNo <> 0
      AND so.Dim_DeliveryBlockId <> ifnull(db.Dim_DeliveryBlockId, 1);

UPDATE fact_salesorder so
SET so.Dim_SalesDocOrderReasonId  = ifnull(sor.dim_salesdocorderreasonid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM dim_salesdocorderreason WHERE rowiscurrent = 1) sor
         ON sor.ReasonCode  =  vbk.VBAK_AUGRU
WHERE so.dd_ScheduleNo <> 0
      AND so.Dim_SalesDocOrderReasonId <> ifnull(sor.dim_salesdocorderreasonid, 1);

UPDATE fact_salesorder so
SET so.Dim_MaterialGroupId  = ifnull(mg.dim_materialgroupid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep  vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM dim_materialgroup where rowiscurrent = 1) mg
         ON mg.MaterialGroupCode  =  vbk.VBAP_MATKL
WHERE so.dd_ScheduleNo <> 0
      AND so.Dim_MaterialGroupId <> ifnull(mg.dim_materialgroupid, 1);

UPDATE fact_salesorder so
SET so.amt_SubTotal3  =  ifnull((vbk.VBAP_KZWI3 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)),1)
													   END)
								* vbep_bmeng , 0),
    so.amt_SubTotal4  =  ifnull((vbk.VBAP_KZWI4 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)),1)
													   END)
										* vbep_bmeng , 0)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x
				 WHERE x.vbap_netpr <> 0
                       AND x.vbap_netwr <> 0) vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
WHERE so.dd_ScheduleNo <> 0;

/* LK : 29 Aug changes - amt_Subtotal3_OrderQty */
UPDATE fact_salesorder so
SET so.amt_Subtotal3_OrderQty  =  ifnull((vbk.VBAP_KZWI3 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)),1)
													   END)
								* vbep_wmeng , 0)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x
				 WHERE x.vbap_netpr <> 0
                       AND x.vbap_netwr <> 0) vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
WHERE so.dd_ScheduleNo <> 0;

/* LK : 29 Aug changes - End of amt_Subtotal3_OrderQty changes */

/* Andra : 10 Jan changes - Adding to fact_salesorder columns VBAP_ARKTX  =  dd_RO_MaterialDesc, dd_RQ_ShiptoName, amt_RO_baseprice */
UPDATE fact_salesorder so
SET so.dd_RO_MaterialDesc = ifnull(VBAP_ARKTX,'Not Set')
FROM fact_salesorder so
     INNER JOIN VBAK_VBAP_VBEP ON VBAK_VBELN = so.dd_SalesDocNo AND VBAP_POSNR = so.dd_SalesItemNo AND VBEP_ETENR = so.dd_ScheduleNo
WHERE so.dd_RO_MaterialDesc <> ifnull(VBAP_ARKTX,'Not Set');
/* Andra : 10 Jan changes - End */

/* OanaV : 08 Jun 2016 - same code also in Line 4287
UPDATE fact_salesorder so
SET so.Dim_PurchaseOrderTypeId = ifnull(cpt.dim_customerpurchaseordertypeid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN (SELECT * FROM dim_customerpurchaseordertype WHERE rowiscurrent = 1) cpt
         ON cpt.CustomerPOType  =  vbk.VBAK_BSARK
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_PurchaseOrderTypeId <> ifnull(cpt.dim_customerpurchaseordertypeid, 1)
*/

UPDATE fact_salesorder so
SET so.Dim_DateIdPurchaseOrder = ifnull(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*, pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_BSTDK AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_DateIdPurchaseOrder <> ifnull(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdQuotationValidFrom = ifnull(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*, pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_ANGDT AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_DateIdQuotationValidFrom <> ifnull(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdQuotationValidTo = ifnull(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*, pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_BNDDT AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_DateIdQuotationValidTo <> ifnull(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdSOCreated = ifnull(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*, pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_ERDAT AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  <>  0
      AND so.Dim_DateIdSOCreated <> ifnull(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdSODocument = ifnull(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*, pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_AUDAT AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_DateIdSODocument <> ifnull(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.dd_ReferenceDocumentNo = ifnull(vbk.VBAP_VGBEL,'Not Set')
FROM fact_salesorder so
     INNER JOIN (SELECT x.*, pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.dd_ScheduleNo  =  0
      AND so.dd_ReferenceDocumentNo <> ifnull(vbk.VBAP_VGBEL,'Not Set');

UPDATE fact_salesorder so
SET so.Dim_CreditRepresentativeId = ifnull(cg.Dim_CreditRepresentativegroupId, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN (SELECT * FROM dim_creditrepresentativegroup WHERE RowIsCurrent  =  1) cg
         ON cg.CreditRepresentativeGroup  =  vbk.VBAK_SBGRP
            AND cg.CreditControlArea  =  vbk.VBAK_KKBER
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_CreditRepresentativeId <> ifnull(cg.Dim_CreditRepresentativegroupId, 1);

UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup1Id = ifnull(mpg.Dim_MaterialPriceGroup1Id, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN (SELECT * FROM Dim_MaterialPriceGroup1 WHERE rowiscurrent = 1) mpg
         ON mpg.MaterialPriceGroup1  =  ifnull(vbk.VBAP_MVGR1,'Not Set')
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_MaterialPriceGroup1Id <> ifnull(mpg.Dim_MaterialPriceGroup1Id, 1);

UPDATE fact_salesorder so
SET so.Dim_DeliveryBlockId = ifnull(db.Dim_DeliveryBlockId, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap  vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN (SELECT * FROM Dim_DeliveryBlock WHERE rowiscurrent = 1) db
         ON db.DeliveryBlock  =  vbk.VBAK_LIFSK
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_DeliveryBlockId <> ifnull(db.Dim_DeliveryBlockId, 1);

UPDATE fact_salesorder so
SET so.Dim_SalesDocOrderReasonId = IFNULL(sor.dim_salesdocorderreasonid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN (SELECT * FROM dim_salesdocorderreason WHERE rowiscurrent = 1) sor
         ON sor.ReasonCode  =  vbk.VBAK_AUGRU
WHERE so.dd_ScheduleNo  =  0
      AND so.Dim_SalesDocOrderReasonId <> IFNULL(sor.dim_salesdocorderreasonid, 1);

UPDATE fact_salesorder so
SET so.Dim_MaterialGroupId = IFNULL(mg.dim_materialgroupid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN (SELECT * FROM dim_materialgroup WHERE rowiscurrent = 1) mg
         ON mg.MaterialGroupCode  =  vbk.VBAP_MATKL
WHERE so.dd_ScheduleNo  = 0
      AND so.Dim_MaterialGroupId <> IFNULL(mg.dim_materialgroupid, 1);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id  =  IFNULL(mg4.dim_materialpricegroup4id, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep v ON so.dd_SalesDocNo  =  VBAK_VBELN AND so.dd_SalesItemNo  =  VBAP_POSNR AND so.dd_ScheduleNo  =  VBEP_ETENR
	 LEFT JOIN (SELECT * FROM dim_materialpricegroup4 WHERE rowiscurrent = 1) mg4
         ON ifnull(VBAP_MVGR4,'Not Set')  =  mg4.MaterialPriceGroup4
WHERE so.dim_materialpricegroup4id <> IFNULL(mg4.dim_materialpricegroup4id, 1);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id  =  IFNULL(mg4.dim_materialpricegroup4id, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap v ON v.VBAP_VBELN  =  so.dd_SalesDocNo AND so.dd_SalesItemNo  =  v.VBAP_POSNR
	 LEFT JOIN (SELECT * FROM dim_materialpricegroup4 WHERE rowiscurrent = 1) mg4
         ON ifnull(VBAP_MVGR4,'Not Set')  =  mg4.MaterialPriceGroup4
WHERE so.dd_ScheduleNo  =  0
      AND so.dim_materialpricegroup4id <> IFNULL(mg4.dim_materialpricegroup4id, 1);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id  =  1
WHERE so.dim_materialpricegroup4id IS NULL;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id  =  IFNULL(mg5.dim_materialpricegroup5id, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep v ON so.dd_SalesDocNo  =  VBAK_VBELN AND so.dd_SalesItemNo  =  VBAP_POSNR AND so.dd_ScheduleNo  =  VBEP_ETENR
	 LEFT JOIN (SELECT * FROM dim_materialpricegroup5 WHERE rowiscurrent = 1) mg5
         ON ifnull(VBAP_MVGR5,'Not Set')  =  mg5.MaterialPriceGroup5
WHERE so.dim_materialpricegroup5id <> IFNULL(mg5.dim_materialpricegroup5id, 1);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id  =  IFNULL(mg5.dim_materialpricegroup5id, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap v ON v.VBAP_VBELN  =  so.dd_SalesDocNo AND so.dd_SalesItemNo  =  v.VBAP_POSNR
	 LEFT JOIN (SELECT * FROM dim_materialpricegroup5 WHERE rowiscurrent = 1) mg5
         ON ifnull(VBAP_MVGR5,'Not Set')  =  mg5.MaterialPriceGroup5
WHERE so.dd_ScheduleNo  =  0
      AND so.dim_materialpricegroup5id <> IFNULL(mg5.dim_materialpricegroup5id, 1);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id  =  1
WHERE so.dim_materialpricegroup5id IS NULL;

UPDATE fact_salesorder so
SET so.amt_SubTotal3  =  ifnull(vbk.VBAP_KZWI3, 0)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.dd_ScheduleNo  = 0
      AND so.amt_SubTotal3 <> ifnull(vbk.VBAP_KZWI3, 0);

UPDATE fact_salesorder so
SET so.amt_SubTotal4 = ifnull(vbk.VBAP_KZWI4, 0)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.dd_ScheduleNo  = 0
      AND so.amt_SubTotal4 <> ifnull(vbk.VBAP_KZWI4, 0);


/* Begin 29 Dec 2013 */

UPDATE fact_salesorder so
SET so.amt_SubTotal1 = ifnull(vbk.VBAP_KZWI1, 0)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk ON vbk.VBAP_VBELN = so.dd_SalesDocNo AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.amt_SubTotal1 <> ifnull(vbk.VBAP_KZWI1, 0);

UPDATE fact_salesorder so
SET so.amt_SubTotal2 = ifnull(vbk.VBAP_KZWI2, 0)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk ON vbk.VBAP_VBELN = so.dd_SalesDocNo AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.amt_SubTotal2 <> ifnull(vbk.VBAP_KZWI2, 0);

UPDATE fact_salesorder so
SET so.amt_SubTotal5 = ifnull(vbk.VBAP_KZWI5, 0)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk ON vbk.VBAP_VBELN = so.dd_SalesDocNo AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.amt_SubTotal5 <> ifnull(vbk.VBAP_KZWI5, 0);

UPDATE fact_salesorder so
SET so.amt_SubTotal6 = ifnull(vbk.VBAP_KZWI6, 0)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk ON vbk.VBAP_VBELN = so.dd_SalesDocNo AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.amt_SubTotal6 <> ifnull(vbk.VBAP_KZWI6, 0);

UPDATE fact_salesorder so
SET so.dd_DocumentConditionNo = ifnull(vbk.VBAK_KNUMV, 'Not Set')
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk ON vbk.VBAP_VBELN = so.dd_SalesDocNo AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.dd_DocumentConditionNo <> ifnull(vbk.VBAK_KNUMV, 'Not Set');

UPDATE fact_salesorder so
SET so.amt_SubTotal1 = 0
WHERE so.amt_SubTotal1 is null;

UPDATE fact_salesorder so
SET so.amt_SubTotal2 = 0
WHERE so.amt_SubTotal2 is null;

UPDATE fact_salesorder so
SET so.amt_SubTotal5 = 0
WHERE so.amt_SubTotal5 is null;

UPDATE fact_salesorder so
SET so.amt_SubTotal6 = 0
WHERE so.amt_SubTotal6 is null;

UPDATE fact_salesorder so
SET so.dd_DocumentConditionNo = 'Not Set'
WHERE so.dd_DocumentConditionNo is null;

/* End 29 Dec 2013 */

/* LK : 29 Aug change - Update new column amt_Subtotal3_OrderQty */

/* No need to split when there are no schedules */

UPDATE fact_salesorder so
SET so.amt_Subtotal3_OrderQty  =  IFNULL(amt_SubTotal3, 0)
WHERE so.dd_ScheduleNo  =  0;

/* End of 29 Aug change - Update new column amt_Subtotal3_OrderQty */

UPDATE fact_salesorder so
SET so.Dim_PurchaseOrderTypeId = IFNULL(cpt.dim_customerpurchaseordertypeid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN (SELECT * FROM dim_customerpurchaseordertype WHERE rowiscurrent = 1 ) cpt
         ON cpt.CustomerPOType  =  vbk.VBAK_BSARK
/* WHERE so.dd_ScheduleNo  = 0  OanaV: ticket BI-3142 missing Customer PO Type for Sales Documents with no ScheduleNo=0*/
WHERE so.Dim_PurchaseOrderTypeId <> IFNULL(cpt.dim_customerpurchaseordertypeid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdPurchaseOrder = IFNULL(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*,pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_BSTDK AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  = 0
      AND so.Dim_DateIdPurchaseOrder <> IFNULL(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdQuotationValidFrom = IFNULL(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*,pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_ANGDT AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  = 0
      AND so.Dim_DateIdQuotationValidFrom <> IFNULL(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdQuotationValidTo = IFNULL(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*,pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_BNDDT AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  = 0
      AND so.Dim_DateIdQuotationValidTo <> IFNULL(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdSOCreated = IFNULL(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*,pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_ERDAT AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  = 0
      AND so.Dim_DateIdSOCreated <> IFNULL(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdSODocument = IFNULL(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*,pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_AUDAT AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_ScheduleNo  = 0
      AND so.Dim_DateIdSODocument <> IFNULL(dt.Dim_Dateid, 1);

UPDATE fact_salesorder so
SET so.dd_ReferenceDocumentNo  =  ifnull(vbk.VBAP_VGBEL,'Not Set')
FROM fact_salesorder so
     INNER JOIN vbak_vbap  vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHERE so.dd_ScheduleNo  = 0
      AND so.dd_ReferenceDocumentNo <> ifnull(vbk.VBAP_VGBEL,'Not Set');

-- CristiT: Aici am ramas

UPDATE    fact_salesorder so
SET so.Dim_SalesDistrictId  =  IFNULL(sd.Dim_SalesDistrictid,1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap_vbkd vkd
		ON so.dd_SalesDocNo  =  vkd.VBKD_VBELN
			AND vkd.VBKD_VBELN = 0
	LEFT JOIN dim_salesdistrict sd
		ON sd.SalesDistrict  =  vkd.VBKD_BZIRK
			AND sd.RowIsCurrent  =  1
WHERE so.Dim_SalesDistrictId <> IFNULL(sd.Dim_SalesDistrictid,1);

UPDATE    fact_salesorder so
SET so.Dim_SalesDistrictId  =  IFNULL(sd.Dim_SalesDistrictid,1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap_vbkd vkd
		ON so.dd_SalesDocNo  =  vkd.VBKD_VBELN
			AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
	LEFT JOIN dim_salesdistrict sd
		ON sd.SalesDistrict  =  vkd.VBKD_BZIRK
			AND sd.RowIsCurrent  =  1
WHERE so.Dim_SalesDistrictId <> IFNULL(sd.Dim_SalesDistrictid,1);

UPDATE    fact_salesorder so
SET so.Dim_SalesDistrictId  =  1
WHERE so.Dim_SalesDistrictId  IS NULL;

UPDATE    fact_salesorder so
SET so.Dim_AccountAssignmentGroupId  =  IFNULL(aag.Dim_AccountAssignmentGroupId,1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap_vbkd vkd
		ON so.dd_SalesDocNo  =  vkd.VBKD_VBELN
			AND vkd.VBKD_POSNR = 0
	LEFT JOIN dim_accountassignmentgroup aag
		ON aag.AccountAssignmentGroup  =  vkd.VBKD_KTGRD
			AND aag.RowIsCurrent  =  1
WHERE so.Dim_AccountAssignmentGroupId <> IFNULL(aag.Dim_AccountAssignmentGroupId,1);

UPDATE    fact_salesorder so
SET so.Dim_AccountAssignmentGroupId  =  IFNULL(aag.Dim_AccountAssignmentGroupId,1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap_vbkd vkd
		ON so.dd_SalesDocNo  =  vkd.VBKD_VBELN
			AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
	LEFT JOIN dim_accountassignmentgroup aag
		ON aag.AccountAssignmentGroup  =  vkd.VBKD_KTGRD
			AND aag.RowIsCurrent  =  1
WHERE so.Dim_AccountAssignmentGroupId <> IFNULL(aag.Dim_AccountAssignmentGroupId,1);

UPDATE    fact_salesorder so
SET so.Dim_AccountAssignmentGroupId  =  1
WHERE so.Dim_AccountAssignmentGroupId  IS NULL;

/* Alex D. Cover all Dim_AccountAssignmentGroupId which are left with 1 using the same dd_SalesDocNo  */

drop table if exists tmp_DimAccountAssignmentGroup;
create table tmp_DimAccountAssignmentGroup as
select distinct f.dd_SalesDocNo,f.Dim_AccountAssignmentGroupid
from fact_salesorder f where f.Dim_AccountAssignmentGroupid<>1;

update fact_salesorder f
set  f.Dim_AccountAssignmentGroupid = ifnull(tmp.Dim_AccountAssignmentGroupid, 1)
from fact_salesorder f ,tmp_DimAccountAssignmentGroup tmp
where f.dd_SalesDocNo = tmp.dd_SalesDocNo
and f.Dim_AccountAssignmentGroupid = 1;


UPDATE    fact_salesorder so
SET so.dd_BusinessCustomerPONo  =  ifnull(vkd.VBKD_BSTKD,'Not Set')
FROM fact_salesorder so, vbak_vbap_vbkd vkd
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN AND vkd.VBKD_POSNR = 0
	AND so.dd_BusinessCustomerPONo <> ifnull(vkd.VBKD_BSTKD,'Not Set');

UPDATE    fact_salesorder so
SET so.dd_BusinessCustomerPONo  =  ifnull(vkd.VBKD_BSTKD,'Not Set')
FROM fact_salesorder so, vbak_vbap_vbkd vkd
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
	AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
	AND so.dd_BusinessCustomerPONo <> ifnull(vkd.VBKD_BSTKD,'Not Set');

UPDATE    fact_salesorder so
SET so.Dim_BillingDateId  =  IFNULL(dt.dim_dateid, 1)
FROM fact_salesorder so, vbak_vbap_vbkd vkd, dim_date dt, dim_company dc
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
	AND vkd.VBKD_POSNR = 0
	AND so.dim_companyid  =  dc.dim_companyid
	AND dt.datevalue  =  VBKD_FKDAT
	AND dt.companycode  =  dc.CompanyCode
	AND VBKD_FKDAT IS NOT NULL
	AND so.Dim_BillingDateId <> IFNULL(dt.dim_dateid, 1);

UPDATE    fact_salesorder so
SET so.Dim_BillingDateId  =  IFNULL(dt.dim_dateid, 1)
FROM fact_salesorder so, vbak_vbap_vbkd vkd, dim_date dt, dim_company dc
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
	AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
	AND so.dim_companyid  =  dc.dim_companyid
	AND dt.datevalue  =  VBKD_FKDAT
	AND dt.companycode  =  dc.CompanyCode
	AND VBKD_FKDAT IS NOT NULL
	AND so.Dim_BillingDateId <> IFNULL(dt.dim_dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_BillingDateId  =  1
WHERE so.Dim_BillingDateId IS NULL;

UPDATE fact_salesorder so
SET so.Dim_DateIdSOItemChangedOn  = IFNULL(dt.Dim_Dateid, 1)
FROM fact_salesorder so
	INNER JOIN (SELECT x.*,pl.CompanyCode FROM vbak_vbap_vbep x,dim_plant pl WHERE pl.plantcode  =  x.VBAP_WERKS AND pl.rowiscurrent  =  1 AND x.VBAP_AEDAT IS NOT NULL) vbk
		ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
			AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
			AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
	LEFT JOIN dim_Date dt
		ON dt.DateValue  =  vbk.VBAP_AEDAT
		AND dt.CompanyCode  =  vbk.CompanyCode
WHERE so.dd_scheduleno <> 0
	AND so.Dim_DateIdSOItemChangedOn <> IFNULL(dt.dim_dateid, 1);

UPDATE fact_salesorder so
SET so.Dim_DateIdSOItemChangedOn  =  1
WHERE   so.Dim_DateIdSOItemChangedOn IS NULL;

DROP TABLE IF EXISTS tmp_vbfa_vbak_vbap;
create table tmp_vbfa_vbak_vbap
as select VBFA_VBELV,VBFA_POSNV,VBFA_VBELN,VBFA_POSNN,VBFA_VBTYP_N,VBFA_VBTYP_P,VBFA_RFMNG,VBFA_MEINS,VBFA_RFWRT,VBFA_WAERS,VBFA_PLMIN,VBFA_TAQUI,VBFA_ERDAT,VBFA_ERZET,VBFA_MATNR,VBFA_BWART,VBFA_WBSTA,VBAP_WERKS,VBFA_J_3AETENR,VBFA_AEDAT,
row_number()over(partition by  VBFA_POSNV,VBFA_VBELV order by VBFA_POSNN desc) rowno
from vbfa_vbak_vbap;

UPDATE    fact_salesorder so
SET so.ct_AfsTotalDrawn  =  ifnull(f.VBFA_RFMNG,0)
FROM fact_salesorder so, tmp_vbfa_vbak_vbap f
WHERE f.VBFA_VBTYP_N  =  'J'
	AND  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	AND f.rowno = 1
	AND so.ct_AfsTotalDrawn <> ifnull(f.VBFA_RFMNG,0);


UPDATE    fact_salesorder so
SET so.dd_SubsequentDocNo  =  ifnull(f.VBFA_VBELN, 'Not Set')
FROM fact_salesorder so, tmp_vbfa_vbak_vbap f
WHERE f.VBFA_VBTYP_N  =  'J'
	AND  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	AND f.rowno = 1
	AND so.dd_SubsequentDocNo <> ifnull(f.VBFA_VBELN, 'Not Set');


UPDATE    fact_salesorder so
SET so.dd_SubsDocItemNo  =  ifnull(f.VBFA_POSNN,0)
FROM fact_salesorder so, tmp_vbfa_vbak_vbap f
WHERE f.VBFA_VBTYP_N  =  'J'
	AND  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	AND f.rowno = 1
	AND so.dd_SubsDocItemNo <> ifnull(f.VBFA_POSNN,0);


UPDATE    fact_salesorder so
SET so.Dim_SubsDocCategoryId  =  IFNULL(dc.Dim_DocumentCategoryid, 1)
FROM fact_salesorder so, tmp_vbfa_vbak_vbap f,Dim_DocumentCategory dc
WHERE f.VBFA_VBTYP_N  =  'J'
	AND  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	AND dc.DocumentCategory  =  f.VBFA_VBTYP_N
	AND f.rowno = 1
	AND so.Dim_SubsDocCategoryId <> IFNULL(dc.Dim_DocumentCategoryid, 1);


update fact_salesorder
set Dim_CustomeridShipTo  =  Dim_Customerid
where Dim_CustomeridShipTo  =  1
	AND Dim_CustomeridShipTo <> Dim_Customerid;


/* LK : 29 Aug changes - amt_Subtotal3inCustConfig_Billing */

/* First sum amt_customerconfigsubtotal3 over docno+itemno */
DROP TABLE IF EXISTS tmp_sof_fb_amt_CustomerConfigSubtotal3;
CREATE TABLE tmp_sof_fb_amt_CustomerConfigSubtotal3
AS
SELECT dd_salesdlvrdocno, dd_salesdlvritemno, sum(amt_customerconfigsubtotal3) sum_amt_customerconfigsubtotal3
FROM fact_billing
GROUP BY dd_salesdlvrdocno, dd_salesdlvritemno;

/* Update amt_Subtotal3inCustConfig_Billing in sof for dd_salesdlvrdocno, dd_salesdlvritemno with minimum schedule no */
DROP TABLE IF EXISTS tmp_sof_sof_minsche;
CREATE TABLE tmp_sof_sof_minsche
AS
SELECT dd_salesdocno,dd_salesitemno,min(dd_scheduleno) min_dd_scheduleno
FROM fact_salesorder
GROUP BY dd_salesdocno,dd_salesitemno;

DROP TABLE IF EXISTS tmp_sof_amt_CustomerConfigSubtotal3_upd;
CREATE TABLE tmp_sof_amt_CustomerConfigSubtotal3_upd
AS
SELECT so.dd_salesdocno,so.dd_salesitemno,so.min_dd_scheduleno,fb.sum_amt_customerconfigsubtotal3
FROM tmp_sof_fb_amt_CustomerConfigSubtotal3 fb,tmp_sof_sof_minsche so
WHERE so.dd_salesdocno  =  fb.dd_salesdlvrdocno
AND so.dd_salesitemno  =  fb.dd_salesdlvritemno;

UPDATE fact_salesorder so
SET amt_Subtotal3inCustConfig_Billing  =  0
WHERE amt_Subtotal3inCustConfig_Billing <> 0;

UPDATE fact_salesorder so
SET so.amt_Subtotal3inCustConfig_Billing  =  ifnull(t.sum_amt_customerconfigsubtotal3, 0)
FROM fact_salesorder so, tmp_sof_amt_CustomerConfigSubtotal3_upd t
WHERE so.dd_salesdocno  =  t.dd_salesdocno
AND so.dd_salesitemno  =  t.dd_salesitemno
AND so.dd_scheduleno  =  t.min_dd_scheduleno
AND so.amt_Subtotal3inCustConfig_Billing <> ifnull(t.sum_amt_customerconfigsubtotal3, 0);

UPDATE fact_salesorder so
SET so.dd_SalesOrderBlocked  =  'Not Set';

UPDATE fact_salesorder so
SET so.dd_SalesOrderBlocked  =  IFNULL('X', 'Not Set')
FROM fact_salesorder so, dim_overallstatusforcreditcheck os
WHERE so.Dim_OverallStatusCreditCheckId  =  os.dim_overallstatusforcreditcheckid
AND ( so.Dim_DeliveryBlockId <> 1 OR so.Dim_BillingBlockid <> 1
OR os.overallstatusforcreditcheck  =  'B')
AND so.dd_SalesOrderBlocked <> IFNULL('X', 'Not Set');


/* End of 29 Aug changes - amt_Subtotal3inCustConfig_Billing */

/* Delete 1 */
/* Delete 2 */

/* Delete 3 */

  DELETE FROM fact_salesorder
  WHERE dd_ScheduleNo  =  0
        AND EXISTS
                (SELECT 1
                  FROM VBAK_VBAP
                  WHERE VBAP_VBELN  =  dd_SalesDocNo)
        AND NOT EXISTS
                    (SELECT 1
                      FROM VBAK_VBAP
                      WHERE VBAP_VBELN  =  dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo);


/* Delete 4	*/

DROP TABLE IF EXISTS tmp_MinSalesSchedules_0;
CREATE TABLE tmp_MinSalesSchedules_0
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,MIN(dd_ScheduleNo) AS MinScheduleNo
FROM facT_salesorder
WHERE dd_ScheduleNo <> 0
GROUP BY dd_SalesDocNo,dd_SalesItemNo;

DROP TABLE IF EXISTS deletepart_702;
CREATE TABLE deletepart_702
AS
SELECT *  FROM fact_salesorder f
  WHERE dd_ScheduleNo  =  0
        AND EXISTS
                (SELECT 1 FROM tmp_MinSalesSchedules_0 t WHERE t.dd_SalesDocNo  =  f.dd_SalesDocNo
                  AND t.dd_SalesItemNo  =  f.dd_SalesItemNo);

/*call vectorwise ( combine 'fact_salesorder-deletepart_702')*/

MERGE INTO fact_salesorder i
USING deletepart_702 d ON i.fact_salesorderid = d.fact_salesorderid
WHEN MATCHED THEN DELETE;

/* UPDATE fact_salesorder so
   SET so.dim_Customermastersalesid  =  cms.dim_Customermastersalesid
  FROM fact_salesorder so,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm,
       dim_Customermastersales cms
 WHERE     so.dim_salesorgid  =  sorg.dim_salesorgid
       AND so.dim_distributionchannelid  =  dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid  =  so.dim_salesdivisionid
       AND cm.dim_customerid  =  so.dim_customerid
       AND sorg.SalesOrgCode  =  cms.SalesOrg
       AND dc.distributionchannelcode  =  cms.distributionchannel
       AND sd.DivisionCode  =  cms.Divisioncode
       AND cm.customernumber  =  cms.CustomerNumber
AND so.dim_Customermastersalesid <> cms.dim_Customermastersalesid */

/* BB20170213 unstable set of rows - used max, should be checked. */
MERGE INTO fact_salesorder so
USING (
select max(so.fact_salesorderid) fact_salesorderid, so.dim_Customermastersalesid  ,  max(cms.dim_Customermastersalesid) as cmsdim_Customermastersalesid
FROM fact_salesorder so,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm,
       dim_Customermastersales cms
 WHERE     so.dim_salesorgid  =  sorg.dim_salesorgid
       AND so.dim_distributionchannelid  =  dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid  =  so.dim_salesdivisionid
       AND cm.dim_customerid  =  so.dim_customerid
       AND sorg.SalesOrgCode  =  cms.SalesOrg
       AND dc.distributionchannelcode  =  cms.distributionchannel
       AND sd.DivisionCode  =  cms.Divisioncode
       AND cm.customernumber  =  cms.CustomerNumber
AND so.dim_Customermastersalesid <> cms.dim_Customermastersalesid
group by so.dim_Customermastersalesid) src ON so.fact_salesorderid = src.fact_salesorderid
WHEN MATCHED THEN UPDATE
SET so.dim_Customermastersalesid  =  ifnull(src.cmsdim_Customermastersalesid, 1)
where so.dim_Customermastersalesid <> ifnull(src.cmsdim_Customermastersalesid, 1)
;

UPDATE fact_salesorder so
SET so.dim_Customermastersalesid  =  1
WHERE so.dim_Customermastersalesid IS NULL;

UPDATE fact_salesorder so
SET so.dd_CustomerMaterialNo  =  IFNULL(vbk.VBAP_KDMAT, 'Not Set')
FROM fact_salesorder so, vbak_vbap_vbep vbk
Where vbk.VBAK_VBELN  =  so.dd_SalesDocNo
	AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
	AND  so.dd_scheduleno <> 0
	AND  vbk.VBAP_KDMAT IS NOT NULL
	AND so.dd_CustomerMaterialNo <> IFNULL(vbk.VBAP_KDMAT, 'Not Set');

UPDATE fact_salesorder so
SET so.dd_CustomerMaterialNo  =  IFNULL(vbk.VBAP_KDMAT, 'Not Set')
FROM fact_salesorder so, vbak_vbap vbk
Where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
	AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	AND  so.dd_scheduleno  =  0
	AND  vbk.VBAP_KDMAT IS NOT NULL
	AND so.dd_CustomerMaterialNo <> IFNULL(vbk.VBAP_KDMAT, 'Not Set');

UPDATE          fact_salesorder so
SET so.dd_CustomerMaterialNo  =  'Not Set'
Where so.dd_CustomerMaterialNo IS NULL;

/* changes 26 Sep 2013 */

MERGE INTO fact_salesorder so
USING vbak_vbap_vbep vbk ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET
	/*dd_SOCreateTime  =  ifnull(VBAK_ERZET, '000000') Changed - it is at Order and item level only*/
	dd_ReqDeliveryTime  =  ifnull(VBAK_VZEIT, '000000')
	,dd_SOLineCreateTime  =  ifnull(VBAP_ERZET, '000000')
	,dd_DeliveryTime  =  ifnull(VBEP_EZEIT, '000000')
	,dd_PlannedGITime  =  ifnull(VBEP_WAUHR, '000000')
	,dd_HighLevelItem  =  ifnull(VBAP_UEPOS, 0);

/* Roxana D - 2017-10-19 removed the schedule no from join*/

MERGE INTO fact_salesorder so
USING (Select distinct VBAK_VBELN,VBAP_POSNR,VBAK_ERZET from vbak_vbap_vbep) vbk
ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
WHEN MATCHED THEN UPDATE SET
	dd_SOCreateTime  =  ifnull(VBAK_ERZET, '000000');


UPDATE fact_salesorder so
SET dd_SOCreateTime  =  '000000'
WHERE dd_SOCreateTime IS NULL;

UPDATE fact_salesorder so
SET dd_ReqDeliveryTime  =  '000000'
WHERE dd_ReqDeliveryTime IS NULL;

UPDATE fact_salesorder so
SET dd_SOLineCreateTime  =  '000000'
WHERE dd_SOLineCreateTime IS NULL;

UPDATE fact_salesorder so
SET dd_DeliveryTime  =  '000000'
WHERE dd_DeliveryTime IS NULL;

UPDATE fact_salesorder so
SET dd_PlannedGITime  =  '000000'
WHERE dd_PlannedGITime IS NULL;

update fact_salesorder set dd_HighLevelItem  =  0 where dd_HighLevelItem is NULL;

/* END changes 20 Dec 2013 */

/*Cornelia update new fields dd_ProdOrderNo and dd_ProdOrderItemNo*/

DROP TABLE IF EXISTS tmp_fact_productionorder;
CREATE TABLE tmp_fact_productionorder AS
select  PO.dd_OrderNumber,po.dd_OrderItemNo,PO.dd_SalesOrderNo,PO.dd_SalesOrderItemNo,PO.dd_SalesOrderDeliveryScheduleNo,
row_number () over (partition by PO.dd_SalesOrderNo,PO.dd_SalesOrderItemNo,PO.dd_SalesOrderDeliveryScheduleNo order by '') rowno
from fact_productionorder po;

UPDATE fact_salesorder so
SET so.dd_ProdOrderNo  =  ifnull(po.dd_OrderNumber,'Not Set')
FROM  fact_salesorder so, tmp_fact_productionorder po
WHERE  SO.dd_SalesDocNo  =  PO.dd_SalesOrderNo AND
	SO.dd_SalesItemNo  =  PO.dd_SalesOrderItemNo AND
	SO.dd_ScheduleNo  =  PO.dd_SalesOrderDeliveryScheduleNo AND
    PO.rowno = 1
AND so.dd_ProdOrderNo <> ifnull(po.dd_OrderNumber,'Not Set');


UPDATE fact_salesorder so
SET so.dd_ProdOrderItemNo  =  ifnull(po.dd_OrderItemNo,0)
FROM  fact_salesorder so, tmp_fact_productionorder po
WHERE  SO.dd_SalesDocNo  =  PO.dd_SalesOrderNo AND
        SO.dd_SalesItemNo  =  PO.dd_SalesOrderItemNo AND
        SO.dd_ScheduleNo  =  PO.dd_SalesOrderDeliveryScheduleNo AND
        PO.rowno = 1
AND so.dd_ProdOrderItemNo <> ifnull(po.dd_OrderItemNo,0);


/*End Cornelia update */
/* Andra 21 Jan : update of Cleared/Blocked Status*/

update fact_salesorder set dd_clearedblockedsts  =  'Cleared';

UPDATE fact_salesorder so
SET so.dd_clearedblockedsts  =  'Blocked'
FROM fact_salesorder so, dim_salesorderheaderstatus sohs,dim_salesorderitemstatus sois
WHERE
	(sohs.GeneralIncompleteStatusItem in ('Not yet processed','Partially processed')
		OR sohs.OverallBlkdStatus in ('Partially processed','Completely processed')
		OR sois.GeneralIncompletionStatus in ('Not yet processed','Partially processed'))
	AND so.dim_salesorderheaderstatusid  =  sohs.dim_salesorderheaderstatusid
	AND so.dim_salesorderitemstatusid  =  sois.dim_salesorderitemstatusid
	AND so.dd_clearedblockedsts <> 'Blocked';

update fact_salesorder set dd_clearedblockedsts  =  'Cleared' where dd_clearedblockedsts is null;

/*End Andra update of Cleared/Blocked Status*/

/* Start Changes 03 Feb 2014 */

UPDATE fact_salesorder fso
SET dim_CustomerConditionGroups1id  =  ifnull(cg.dim_CustomerConditionGroupsid,1)
FROM fact_salesorder fso
	INNER JOIN VBAK_VBAP_VBKD vp
		ON fso.dd_SalesDocNo  =  vp.VBKD_VBELN
			AND fso.dd_SalesItemNo  =  vp.VBKD_POSNR
    LEFT JOIN dim_CustomerConditionGroups cg
		ON cg.CustomerCondGrp  =  VBKD_KDKG1
where ifnull(fso.dim_CustomerConditionGroups1id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);

update fact_salesorder set dim_CustomerConditionGroups1id  =  1 where dim_CustomerConditionGroups1id is NULL;

UPDATE fact_salesorder fso
SET dim_CustomerConditionGroups2id  =  ifnull(cg.dim_CustomerConditionGroupsid,1)
FROM fact_salesorder fso
	INNER JOIN VBAK_VBAP_VBKD vp
		ON fso.dd_SalesDocNo  =  vp.VBKD_VBELN
			AND fso.dd_SalesItemNo  =  vp.VBKD_POSNR
    LEFT JOIN dim_CustomerConditionGroups cg
		ON cg.CustomerCondGrp  =  VBKD_KDKG2
where ifnull(fso.dim_CustomerConditionGroups2id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);

update fact_salesorder set dim_CustomerConditionGroups2id  =  1 where dim_CustomerConditionGroups2id is NULL;

UPDATE fact_salesorder fso
SET dim_CustomerConditionGroups3id  =  ifnull(cg.dim_CustomerConditionGroupsid,1)
FROM fact_salesorder fso
	INNER JOIN VBAK_VBAP_VBKD vp
		ON fso.dd_SalesDocNo  =  vp.VBKD_VBELN
			AND fso.dd_SalesItemNo  =  vp.VBKD_POSNR
    LEFT JOIN dim_CustomerConditionGroups cg
		ON cg.CustomerCondGrp  =  VBKD_KDKG3
where IFNULL(fso.dim_CustomerConditionGroups3id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);

update fact_salesorder set dim_CustomerConditionGroups3id  =  1 where dim_CustomerConditionGroups3id is NULL;

/* End Changes 03 Feb 2014 */

/* Start Changes 12 Feb 2014 */

UPDATE fact_salesorder so
SET so.dim_scheduledeliveryblockid  = IFNULL(db.Dim_DeliveryBlockId, 1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap_vbep vbk
		ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
			AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
			AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
	LEFT JOIN Dim_DeliveryBlock db
		ON db.DeliveryBlock  =  vbk.VBEP_LIFSP
			AND db.RowIsCurrent  =  1
Where vbk.VBEP_LIFSP IS NOT NULL
	and so.dim_scheduledeliveryblockid <> IFNULL(db.Dim_DeliveryBlockId, 1);

UPDATE fact_salesorder so SET so.dim_scheduledeliveryblockid  =  1 WHERE so.dim_scheduledeliveryblockid is NULL;

/* End Changes 12 Feb 2014 */

/* Start Changes 14 Feb 2014 */

UPDATE fact_salesorder so
set so.Dim_CustomerGroup4id  =  ifnull(cg4.Dim_CustomerGroup4id,1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap_vbep vbk
		ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
			AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
			AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
	LEFT JOIN Dim_CustomerGroup4 cg4
		ON cg4.CustomerGroup  =  VBAK_KVGR4
Where ifnull(so.Dim_CustomerGroup4id,-1) <> ifnull(cg4.Dim_CustomerGroup4id, 1);

update fact_salesorder set Dim_CustomerGroup4id  =  1 where Dim_CustomerGroup4id is NULL;

/* End Changes 14 Feb 2014 */


update fact_salesorder so
set so.dd_ConditionNo = ifnull(vbk.VBAK_KNUMV,'Not Set')
from fact_salesorder so, vbak_vbap_vbep vbk
where
vbk.VBAK_VBELN  =  so.dd_SalesDocNo
and vbk.VBAP_POSNR  =  so.dd_SalesItemNo
and vbk.VBEP_ETENR  =  so.dd_ScheduleNo
and so.dd_ConditionNo <> ifnull(vbk.VBAK_KNUMV,'Not Set');


/*Agreements KNUMA_AG*/

update fact_salesorder so
set so.dim_agreementsid = IFNULL(ag.dim_agreementsid, 1)
from fact_salesorder so
	INNER JOIN vbak_vbap_vbep vbk
		ON vbk.VBAK_VBELN = so.dd_SalesDocNo
			and vbk.VBAP_POSNR = so.dd_SalesItemNo
			and vbk.VBEP_ETENR = so.dd_ScheduleNo
	LEFT JOIN dim_agreements ag
		ON IFNULL(vbk.VBAP_KNUMA_AG,'Not Set') = IFNULL(ag.agreement,'Not Set')
where IFNULL(so.dim_agreementsid,-1) <> IFNULL(ag.dim_agreementsid, 1);

/*charge back contract header KNUMA_AG*/
update fact_salesorder so
set so.dim_chargebackcontractid = IFNULL(ag.dim_chargebackcontractid,1)
from fact_salesorder so
	INNER JOIN vbak_vbap_vbep vbk
		ON vbk.VBAK_VBELN = so.dd_SalesDocNo
			and vbk.VBAP_POSNR = so.dd_SalesItemNo
			and vbk.VBEP_ETENR = so.dd_ScheduleNo
	LEFT JOIN dim_chargebackcontract ag
		ON IFNULL(vbk.VBAP_KNUMA_AG,'Not Set') = IFNULL(ag.agreement,'Not Set')
where IFNULL(so.dim_chargebackcontractid,-1) <> IFNULL(ag.dim_chargebackcontractid,1);

update fact_salesorder so
set so.dim_customeridcbowner = IFNULL(c.dim_customerid, 1)
from fact_salesorder so, dim_chargebackcontract ag, dim_customer c
where so.dim_chargebackcontractid = ag.dim_chargebackcontractid
	and ag.owner = c.customernumber
	and IFNULL(so.dim_customeridcbowner,1) <> IFNULL(c.dim_customerid, 1);


/*added a DD_DeliveryIndicator BY VICTORC  9/9/2014*/
UPDATE fact_salesorder t1
SET t1.dd_deliveryindicator = ifnull(t2.vbap_kztlf,'Not Set')
FROM fact_salesorder t1, vbak_vbap_vbep t2
WHERE t1.dd_salesdocno = t2.vbak_vbeln
   AND t1.dd_salesitemno = t2.vbap_posnr
   AND t1.dd_scheduleno =  t2.vbep_etenr
   AND t1.dd_deliveryindicator <> ifnull(t2.vbap_kztlf,'Not Set');

UPDATE fact_salesorder t1
SET t1.dd_deliveryindicator = ifnull(t2.vbap_kztlf,'Not Set')
FROM fact_salesorder t1, vbak_vbap t2
WHERE t1.dd_salesdocno = t2.vbap_vbeln
   AND t1.dd_salesitemno = t2.vbap_posnr
   AND t1.dd_deliveryindicator = 'Not Set'
   AND t1.dd_deliveryindicator <> ifnull(t2.vbap_kztlf,'Not Set');

/*added a dim_CustomerConditionGroups4id,dim_CustomerConditionGroups5id BY VICTOR  11/19/2014*/
UPDATE fact_salesorder fso
SET dim_CustomerConditionGroups4id = ifnull(cg.dim_CustomerConditionGroupsid,1)
FROM fact_salesorder fso
	INNER JOIN VBAK_VBAP_VBKD vp
		ON fso.dd_SalesDocNo = vp.VBKD_VBELN
			AND fso.dd_SalesItemNo = vp.VBKD_POSNR
			AND VBKD_KDKG4 IS NOT NULL
    LEFT JOIN dim_CustomerConditionGroups cg
		ON cg.CustomerCondGrp = VBKD_KDKG4
where ifnull(fso.dim_CustomerConditionGroups4id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);

UPDATE fact_salesorder fso
SET dim_CustomerConditionGroups5id = ifnull(cg.dim_CustomerConditionGroupsid,1)
FROM fact_salesorder fso
	INNER JOIN VBAK_VBAP_VBKD vp
		ON fso.dd_SalesDocNo = vp.VBKD_VBELN
			AND fso.dd_SalesItemNo = vp.VBKD_POSNR
			AND VBKD_KDKG5 IS NOT NULL
	LEFT JOIN dim_CustomerConditionGroups cg
		ON cg.CustomerCondGrp = VBKD_KDKG5
where ifnull(fso.dim_CustomerConditionGroups5id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);

/* END CHANGES ON 11/19/2014*/

/* added a dd_Purchaseorderitem BY Alexandru Manolache   12 December 2014 */
UPDATE fact_salesorder t1
SET t1.dd_Purchaseorderitem = ifnull(t2.vbap_posex,'Not Set')
FROM fact_salesorder t1, vbak_vbap_vbep t2
WHERE     t1.dd_salesdocno  = t2.vbak_vbeln
      AND t1.dd_salesitemno = t2.vbap_posnr
      AND t1.dd_scheduleno  = t2.vbep_etenr
      AND t1.dd_Purchaseorderitem <> ifnull(t2.vbap_posex,'Not Set');

/* 16 FEB 2015 Octavian : Adding from PRD script missing dim_MaterialPricingGroupId */

UPDATE fact_salesorder so
SET so.dim_MaterialPricingGroupId  = ifnull(mpg.dim_MaterialPricingGroupId,1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap_vbep vbk
		ON vbk.VBAK_VBELN = so.dd_SalesDocNo
			AND vbk.VBAP_POSNR = so.dd_SalesItemNo
			AND vbk.VBEP_ETENR = so.dd_ScheduleNo
	LEFT JOIN dim_MaterialPricingGroup mpg
		ON mpg.MaterialPricingGroupCode = vbk.VBAP_KONDM
			AND mpg.RowIsCurrent  =  1
where so.dd_ScheduleNo <> 0
	AND ifnull(so.dim_MaterialPricingGroupId,-1) <> ifnull(mpg.dim_MaterialPricingGroupId,1);

UPDATE fact_salesorder so
SET so.dim_MaterialPricingGroupId = ifnull(mpg.dim_MaterialPricingGroupId,1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap vbk
		ON vbk.VBAP_VBELN = so.dd_SalesDocNo
			AND vbk.VBAP_POSNR = so.dd_SalesItemNo
			AND so.dd_ScheduleNo = 0
	LEFT JOIN dim_MaterialPricingGroup mpg
		ON mpg.MaterialPricingGroupCode = vbk.VBAP_KONDM
			AND mpg.RowIsCurrent  =  1
where ifnull(so.dim_MaterialPricingGroupId,-1) <> ifnull(mpg.dim_MaterialPricingGroupId,1);

UPDATE fact_salesorder so set so.dim_MaterialPricingGroupId = 1 where dim_MaterialPricingGroupId is NULL;

/* 16 FEB 2015 Octavian: End of changes */

/* 25-Feb-2015 Octavian: Adding Material Price Group 2 */
UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup2Id = ifnull( mpg.Dim_MaterialPriceGroup2Id,1)
FROM fact_salesorder so
	INNER JOIN vbak_vbap_vbep vbk
		ON vbk.VBAK_VBELN = so.dd_SalesDocNo
			AND vbk.VBAP_POSNR = so.dd_SalesItemNo
			AND vbk.VBEP_ETENR = so.dd_ScheduleNo
	LEFT JOIN Dim_MaterialPriceGroup2 mpg
		ON mpg.MaterialPriceGroup2 = ifnull(vbk.VBAP_MVGR2,'Not Set')
			AND mpg.RowIsCurrent = 1
WHERE so.dd_ScheduleNo <> 0
	AND so.Dim_MaterialPriceGroup2Id <> ifnull( mpg.Dim_MaterialPriceGroup2Id,1);
/* 25-Feb-2015 Octavian: END OF CHANGES */

/* 24-Mar-2015 Octavian: Reject Reason updated at the level of Item level instead of Schedule */

update fact_salesorder so
SET so.Dim_SalesOrderRejectReasonid = ifnull(sorr.Dim_SalesOrderRejectReasonid,1)
from fact_salesorder so
	INNER JOIN VBAK_VBAP
		ON so.dd_SalesDocNo = VBAP_VBELN
			and so.dd_SalesItemNo = VBAP_POSNR
	LEFT JOIN Dim_SalesOrderRejectReason sorr
		ON sorr.RejectReasonCode = ifnull(vbap_abgru,'Not Set')
where ifnull(so.Dim_SalesOrderRejectReasonid,-1) <> ifnull(sorr.Dim_SalesOrderRejectReasonid,1);
/* 24-Mar-2015 Octavian: END OF CHANGES */

/* MDG Part */

/*UPDATE fact_salesorder f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM fact_salesorder f, dim_mdg_part tmp, dim_part p
WHERE f.dim_partid = p.dim_partid
	AND p.marazzmdg = tmp.partnumber
	AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1)*/

UPDATE fact_salesorder f
SET f.dim_mdg_partid = ifnull(mdg.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM fact_salesorder f, dim_part dp, dim_mdg_part mdg
WHERE dp.PartNumber  =  mdg.PartNumber
  AND dp.dim_partid  =  f.dim_partid
  AND mdg.milliporeflag = 'Not Set'
  AND f.dim_mdg_partid <> ifnull(mdg.dim_mdg_partid, 1)	;

/* Update just millipore materilas with values from other instances but Sia */
drop table if exists tmp_mdg;
create table tmp_mdg as
select distinct  dp.partnumber, max(mdg.partnumber) as mdgpartnumber
from EMD586.dim_part dp, EMD586.fact_salesorder f , EMD586.dim_mdg_part mdg
where f.dim_partid = dp.dim_partid
and   f.dim_mdg_partid = mdg.dim_mdg_partid
and dp.projectsourceid in (1,2,3,5,6,7)
and mdg.partnumber <> 'Not Set'
group by dp.partnumber;

update fact_salesorder f
set f.dim_mdg_partid = ifnull(mdg.dim_mdg_partid, 1)
from fact_salesorder f,dim_materialgroup dm,  tmp_mdg tmp, dim_mdg_part mdg, dim_part dp
where dp.dim_partid = f.dim_partid
and dm.dim_materialgroupid =  f.dim_materialgroupid
and dm.MATERIALGROUPNAME = 'Millipore'
and mdg.milliporeflag = 'Y'
and mdg.partnumber = tmp.mdgpartnumber
and f.dim_partid = dp.dim_partid
and dp.partnumber = tmp.partnumber
and f.dim_mdg_partid <> ifnull(mdg.dim_mdg_partid, 1);

drop table if exists tmp_mdg;
create table tmp_mdg as
select distinct  replace(dp.partnumber,'.','') partnumber, max(replace(mdg.partnumber,'.','')) as mdgpartnumber
from EMD586.dim_part dp, EMD586.fact_salesorder f , EMD586.dim_mdg_part mdg
where f.dim_partid = dp.dim_partid
and   f.dim_mdg_partid = mdg.dim_mdg_partid
and dp.projectsourceid in (1,2,3,5,6,7)
and mdg.partnumber <> 'Not Set'
group by dp.partnumber;


drop table if exists update_mdg;
create table update_mdg
as select distinct dim_mdg_partid,partnumber
from dim_mdg_part mdg
where mdg.milliporeflag = 'Y';

drop table if exists last_update_mdg;
create table last_update_mdg
as select distinct mdg.dim_mdg_partid,replace(tmp.mdgpartnumber,'.','') mdgpartnumber
from update_mdg mdg,tmp_mdg tmp
where mdg.partnumber=tmp.mdgpartnumber;

update fact_salesorder f
set f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1)
from fact_salesorder f,dim_materialgroup dm,  last_update_mdg tmp, dim_mdg_part mdg
where mdg.dim_mdg_partid = f.dim_mdg_partid
and dm.dim_materialgroupid =  f.dim_materialgroupid
and dm.MATERIALGROUPNAME = 'Millipore'
and trim(leading '0' from replace(mdg.partnumber,'.','')) = tmp.mdgpartnumber
and f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);



/* Update BW Hierarchy */

/*update fact_salesorder f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_salesorder f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid

update fact_salesorder f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_salesorder f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid*/


/* Alex D - Bw product hierarchy for LS */
drop table if exists tmp_maxversion;
create table tmp_maxversion as
select max(right (versiondesc,4))versiondesc , productgroup  from dim_bwproducthierarchy  where right (versiondesc,4) ='2017'
group by productgroup;



drop table if exists tmp_dim_bwproducthierarchy;
create table tmp_dim_bwproducthierarchy as
select max(dbw.dim_bwproducthierarchyid)dim_bwproducthierarchyid, dbw.productgroup
from dim_bwproducthierarchy dbw ,tmp_maxversion tmp
where concat(right (dbw.versiondesc,4),dbw.productgroup) = concat(tmp.versiondesc,tmp.productgroup)
group by dbw.productgroup;

update  fact_salesorder f
set f.dim_bwproducthierarchyid =  ifnull(dbw.dim_bwproducthierarchyid, 1)
from dim_part dp, tmp_dim_bwproducthierarchy dbw, fact_salesorder f
where f.dim_partid = dp.dim_partid
  and dp.productgroupsbu = REPLACE(dbw.productgroup,'SBU-','');


/* add custom partener function EN - End User for F.Trade */

update fact_salesorder f
set f.dim_customerenduserforftrade = ifnull(dc.dim_customerid,1)
from fact_salesorder f, vbpa v, dim_customer dc
where f.dd_salesdocno = v.vbpa_vbeln
	and v.vbpa_parvw = 'EN'
	and v.vbpa_kunnr = dc.customernumber
	and ifnull(f.dim_customerenduserforftrade,-1) <> ifnull(dc.dim_customerid,1);

/* Alex D add custom partener function WE - Ship To*/
/*update fact_salesorder f
set f.dim_customer_shipto = ifnull(dc.dim_customerid,1)
from fact_salesorder f, vbpa v, dim_customer dc
where f.dd_salesdocno = v.vbpa_vbeln
	and v.vbpa_parvw = 'WE'
	and v.vbpa_kunnr = dc.customernumber
    and v.VBPA_POSNR = 0
	and ifnull(f.dim_customer_shipto,-1) <> ifnull(dc.dim_customerid,1)*/

MERGE INTO fact_salesorder f
USING ( SELECT f.dd_salesdocno, MAX(ifnull(dc.dim_customerid,1)) as dim_customer_shipto
      FROM fact_salesorder f, vbpa v, dim_customer dc
	  where f.dd_salesdocno = v.vbpa_vbeln
	  and v.vbpa_parvw = 'WE'
	  and v.vbpa_kunnr = dc.customernumber
      and v.VBPA_POSNR = 0
	  GROUP BY f.dd_salesdocno) x
ON (f.dd_salesdocno = x.dd_salesdocno)
WHEN MATCHED THEN
UPDATE SET f.dim_customer_shipto = ifnull(x.dim_customer_shipto, 1)
where ifnull(f.dim_customer_shipto,-1) <> ifnull(x.dim_customer_shipto,1);

/* EMD Marius Add OP Rate */

UPDATE fact_salesorder f
SET f.amt_ExchangeRate_GBL = ifnull(r.exrate,1)
FROM fact_salesorder f, dim_currency dc, csv_oprate_sigma r
WHERE f.Dim_Currencyid_TRA = dc.dim_currencyid
	AND dc.CurrencyCode = r.fromc
	AND r.toc = (SELECT ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD'))
	AND ifnull(f.amt_ExchangeRate_GBL,-1) <> ifnull(r.exrate,1);

/* Emd OP Rate */

UPDATE fact_salesorder f
SET ct_baseuomratio = IFNULL(MARM_UMREZ/MARM_UMREN, 0)
FROM fact_salesorder f,MARM uc, dim_part dp, dim_unitofmeasure uom
WHERE f.dim_partid = dp.dim_partid
and f.dim_unitofmeasureid = uom.dim_unitofmeasureid
and uc.marm_matnr = dp.partnumber
and uc.marm_meinh = uom.UOM	;

UPDATE fact_salesorder f
SET ct_baseuomratioKG = IFNULL(MARM_UMREN/MARM_UMREZ, 0)
FROM fact_salesorder f,MARM uc, dim_part dp
WHERE f.dim_partid = dp.dim_partid
and uc.marm_matnr = dp.partnumber
and uc.marm_meinh = 'KG';

UPDATE fact_salesorder f
SET ct_baseuomratioKG = IFNULL(1/1000, 0)
FROM fact_salesorder f,MARM uc, dim_part dp
WHERE f.dim_partid = dp.dim_partid
and uc.marm_matnr = dp.partnumber
and uc.marm_meinh = 'G';

/* Update 3 June 2014 */
UPDATE fact_salesorder so
SET so.dim_deliverypriorityId = IFNULL(d.dim_deliverypriorityId, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder so, vbak_vbap_vbep vbk,dim_deliverypriority d
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
  AND vbk.VBAP_POSNR = so.dd_SalesItemNo
  AND vbk.VBEP_ETENR = so.dd_ScheduleNo
  AND vbk.VBAP_LPRIO IS NOT NULL
  AND d.deliverypriority = vbk.VBAP_LPRIO
  AND so.dim_deliverypriorityId <> IFNULL(d.dim_deliverypriorityId, 1)
  AND d.RowIsCurrent = 1;

UPDATE fact_salesorder so
SET so.dim_deliverypriorityId = IFNULL(d.dim_deliverypriorityId, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder so, vbak_vbap vbk,dim_deliverypriority d
WHERE  vbk.VBAP_VBELN = so.dd_SalesDocNo
   AND vbk.VBAP_POSNR = so.dd_SalesItemNo
   AND  so.dd_ScheduleNo = 0
AND d.deliverypriority = IFNULL(vbk.VBAP_LPRIO,0)
AND so.dim_deliverypriorityId <> IFNULL(d.dim_deliverypriorityId, 1)
AND d.rowIsCurrent = 1;

UPDATE fact_salesorder so
SET so.dim_deliverypriorityId = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE  dim_deliverypriorityId IS NULL;
/* End of Update 3 June 2014 */

/*  CALL bi_populate_customer_hierarchy() */

/*UPDATE fact_salesorder f
SET AMT_EXCHANGERATE_CUSTOM = 100
FROM fact_salesorder f, dim_currency dc
where
f.dim_currencyid_tra = dc.dim_currencyid
and dc.CURRENCYCODE in ('CLP','COP','HUF','IDR','JPY','KRW','TWD','VND')


UPDATE fact_salesorder
SET AMT_EXCHANGERATE_CUSTOM = 0.1
FROM fact_salesorder f, dim_currency dc
where f.dim_currencyid_tra = dc.dim_currencyid
and dc.CURRENCYCODE in ('TND')*/


/* Commercial View*/
update fact_salesorder f
	set f.dim_commercialviewid= ifnull(ds.dim_commercialviewid, 1)
	from dim_commercialview ds, dim_customer a, dim_bwproducthierarchy b, fact_salesorder f
	where ds.SOLDTOCOUNTRY=a.COUNTRY
	and ds.UPPERPRODHIER=b.BUSINESS
	and f.DIM_BWPRODUCTHIERARCHYID=b.DIM_BWPRODUCTHIERARCHYID
	and f.dim_customerid=a.dim_customerid
	and f.dim_commercialviewid <> ifnull(ds.dim_commercialviewid, 1);

UPDATE fact_salesorder so
SET ct_scheduleqtybaseuom  = ifnull(VBEP_LMENG, 0 )
FROM fact_salesorder so , VBAK_VBAP_VBEP vbk
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
  AND vbk.VBAP_POSNR = so.dd_SalesItemNo
  AND vbk.VBEP_ETENR = so.dd_ScheduleNo;


UPDATE fact_salesorder so
SET dd_movementtype  = ifnull(VBEP_BWART, 'Not Set' )
FROM fact_salesorder so , VBAK_VBAP_VBEP vbk
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
  AND vbk.VBAP_POSNR = so.dd_SalesItemNo
  AND vbk.VBEP_ETENR = so.dd_ScheduleNo;

/* Roxana - 04 Nov 2016 - Request from PM to get the date only from VBAP, else the field remains null */

/*update fact_salesorder so
set so.dim_dateidrequested_emd = ifnull(dd.dim_dateid, 1)
from fact_salesorder so, VBAK_VBAP_VBEP s, dim_plant dp, dim_date dd
where s.VBAK_VBELN = so.dd_SalesDocNo and s.VBAP_POSNR = so.dd_SalesItemNo and s.VBEP_ETENR = 1
	and dp.PlantCode = s.VBAP_WERKS
	and dd.DateValue = VBEP_EDATU AND dd.CompanyCode = dp.CompanyCode
	and ifnull(so.dim_dateidrequested_emd,-1) <> ifnull(dd.dim_dateid, 1)*/



UPDATE fact_salesorder so
SET ct_GrossWeightItem  = ifnull(VBAP_BRGEW, 0 )
FROM fact_salesorder so , VBAK_VBAP_VBEP vbk
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
  AND vbk.VBAP_POSNR = so.dd_SalesItemNo
  AND vbk.VBEP_ETENR = so.dd_ScheduleNo;

UPDATE fact_salesorder so
SET ct_NetWeightItem  = ifnull(case when VBAP_GEWEI = 'G' THEN  ifnull(VBAP_NTGEW, 0 )/1000
                                 else ifnull(VBAP_NTGEW, 0 )
                               end, 0)
FROM fact_salesorder so , VBAK_VBAP_VBEP vbk
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
  AND vbk.VBAP_POSNR = so.dd_SalesItemNo
  AND vbk.VBEP_ETENR = so.dd_ScheduleNo;


UPDATE fact_salesorder so
SET dd_WeightUnit  = ifnull(VBAP_GEWEI, 'Not Set' )
FROM fact_salesorder so , VBAK_VBAP_VBEP vbk
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
  AND vbk.VBAP_POSNR = so.dd_SalesItemNo
  AND vbk.VBEP_ETENR = so.dd_ScheduleNo;





/*
drop table if exists deletepart_701
drop table if exists Dim_CostCenter_701
drop table if exists Dim_CostCenter_first1_701
drop table if exists dim_profitcenter_701
drop table if exists dim_profitcenteri_701
drop table if exists dim_profitcenter_upd_701
drop table if exists fact_salesorder_tmptbl
drop table if exists staging_update_701
drop table if exists max_holder_701
drop table if exists variable_holder_701
drop table if exists VBAK_VBAP_VBEP_701
drop table if exists tmp_sof_Dim_CustomerPartnerFunctions
select 'Process Complete'
select 'sales order count ',count(*) from fact_salesorder
DROP TABLE IF EXISTS TMP_UPD_dd_CreditRep1
*/

UPDATE fact_salesorder f
SET dd_PMDeliveryServiceflag = ifnull(case when ct_ConfirmedQty = 0 and OverallProcessingStatus in ('Completely processed','Tratado completamente') then 'X'
                                      else 'Not Set' end, 'Not Set')
FROM fact_salesorder f,dim_salesorderitemstatus d
WHERE  f.dim_salesorderitemstatusid = d.dim_salesorderitemstatusid;

/* Marius 30 AUG 2016 add SO at risk */

drop table if exists tmp_qty_onhand_with_wip;
create table tmp_qty_onhand_with_wip
  as
  select f_invagng.dim_partid
    ,sum(f_invagng.ct_WIPQty + f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock
      + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) qty_on_hnd_wip
  from fact_inventoryaging f_invagng
  group by f_invagng.dim_partid;

drop table if exists tmp_order_qty;
create table tmp_order_qty
  as
  select dd_salesdocno,dd_salesitemno,dim_partid,sum(ct_ScheduleQtySalesUnit) order_qty
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
  where pd.calendaryear = year(add_months(current_date,1)) and pd.calendarmonthnumber = month(add_months(current_date,1))
  group by dd_salesdocno,dd_salesitemno,dim_partid;

drop table if exists tmp_order_qty_cum;
create table tmp_order_qty_cum
  as
  select dd_salesdocno,dd_salesitemno,dim_partid,order_qty,sum(order_qty) over (partition by dim_partid order by order_qty asc) order_qty_cum
  from tmp_order_qty;

update fact_salesorder f
  set f.dd_mrkhc_soatrisk = 'Not Set'
  where f.dd_mrkhc_soatrisk <> 'Not Set';

update fact_salesorder f
  set f.dd_mrkhc_soatrisk = ifnull('X', 'Not Set')
  from fact_salesorder f
    inner join tmp_qty_onhand_with_wip ohqty on f.dim_partid = ohqty.dim_partid
    inner join tmp_order_qty_cum ocqty on f.dd_salesdocno = ocqty.dd_salesdocno and f.dd_salesitemno = ocqty.dd_salesitemno
  where ohqty.qty_on_hnd_wip < ocqty.order_qty_cum;

 drop table if exists tmp_qty_onhand_with_wip;
 drop table if exists tmp_order_qty;
 drop table if exists tmp_order_qty_cum;

/* Marius 1 sep 2016 add On Hand Qty */

DROP TABLE IF EXISTS TMP_UPDATE_ONHANDQTY;
CREATE TABLE TMP_UPDATE_ONHANDQTY
AS
SELECT prt.partnumber, pl.plantcode, sum(f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) OnHandQty
FROM fact_inventoryaging f_invagng
  INNER JOIN dim_part prt ON f_invagng.dim_partid = prt.dim_partid
  INNER JOIN dim_plant pl ON f_invagng.dim_plantid = pl.dim_plantid
GROUP BY prt.partnumber, pl.plantcode;

DROP TABLE IF EXISTS TMP_SALES_ID;
CREATE TABLE TMP_SALES_ID
AS
SELECT dd_salesdocno,dd_salesitemno,min(dd_ScheduleNo) sched
FROM fact_salesorder
GROUP BY dd_salesdocno,dd_salesitemno;

UPDATE fact_salesorder SET ct_onhandqtyinv = 0 WHERE ct_onhandqtyinv <> 0;

UPDATE fact_salesorder f
SET f.ct_onhandqtyinv = ifnull(t.OnHandQty, 0)
FROM fact_salesorder f
  INNER JOIN TMP_SALES_ID x ON f.dd_salesdocno = x.dd_salesdocno AND f.dd_salesitemno = x.dd_salesitemno AND f.dd_ScheduleNo = x.sched
  INNER JOIN dim_part prt ON f.dim_partid = prt.dim_partid
  INNER JOIN dim_plant pl ON f.dim_plantid = pl.dim_plantid
  INNER JOIN TMP_UPDATE_ONHANDQTY t ON prt.partnumber = t.partnumber AND pl.plantcode = t.plantcode
WHERE f.ct_onhandqtyinv <> ifnull(t.OnHandQty, 0);

DROP TABLE IF EXISTS TMP_UPDATE_ONHANDQTY;
DROP TABLE IF EXISTS TMP_SALES_ID;


/*update fact_salesorder f
set f.dim_dateidrequested_emd  = f.dim_dateidscheddelivery
where f.dim_dateidrequested_emd = 1*/

DROP TABLE IF EXISTS tmp_sof_fb_amt_CustomerConfigSubtotal3;
DROP TABLE IF EXISTS TMP_NEWROWS_VBAK_VBAP_VBEP;
DROP TABLE IF EXISTS tmp_sof_sof_minsche;
DROP TABLE IF EXISTS tmp_sof_amt_CustomerConfigSubtotal3_upd;
DROP TABLE IF EXISTS TMP_INS_VBAK_VBAP_VBEP;
DROP TABLE IF EXISTS TMP_DEL_VBAK_VBAP_VBEP;
DROP TABLE IF EXISTS TMP_minvalidto_sopc;

/* 14 Oct 2016 CristianT Start: Bring new measures from Stock Analytics to Sales. Requested on BI-4389 */
DROP TABLE IF EXISTS tmp_soforupdate;
CREATE TABLE tmp_soforupdate
AS
SELECT dd_salesdocno,
       dd_salesitemno,
       dim_partid,
       dim_plantid,
       dim_projectsourceid,
       min(dd_scheduleno) as min_dd_scheduleno
FROM fact_salesorder
GROUP BY dd_salesdocno,
         dd_salesitemno,
         dim_partid,
         dim_plantid,
         dim_projectsourceid;

DROP TABLE IF EXISTS tmp_inventorymeasures;
CREATE TABLE tmp_inventorymeasures
AS
SELECT f_invagng.dim_partid as dim_partid,
       f_invagng.dim_plantid as dim_plantid,
	   f_invagng.dim_projectsourceid as dim_projectsourceid,
       sum(f_invagng.ct_TotalRestrictedStock) as ct_totalrestrictedstock,
       sum(f_invagng.ct_StockQty) as ct_stockqty,
       sum(f_invagng.ct_StockInTransfer) as ct_stockintransfer,
       sum(f_invagng.CT_WIPQTY + f_invagng.CT_STOCKQTY + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) as ct_onhandwipqty,
       sum(f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) as ct_onhand,
       sum(f_invagng.ct_StockInTransit) as ct_stockintransit,
       sum(f_invagng.ct_orderitemqty - f_invagng.ct_grqty) as ct_wipqty
FROM fact_inventoryaging f_invagng
GROUP BY f_invagng.dim_partid,
         f_invagng.dim_plantid,
		 f_invagng.dim_projectsourceid;

UPDATE fact_salesorder SET ct_totalrestrictedstock = 0 WHERE ct_totalrestrictedstock <> 0;
UPDATE fact_salesorder SET ct_stockqty = 0 WHERE ct_stockqty <> 0;
UPDATE fact_salesorder SET ct_stockintransfer = 0 WHERE ct_stockintransfer <> 0;
UPDATE fact_salesorder SET ct_onhandwipqty = 0 WHERE ct_onhandwipqty <> 0;
UPDATE fact_salesorder SET ct_onhand = 0 WHERE ct_onhand <> 0;
UPDATE fact_salesorder SET ct_stockintransit = 0 WHERE ct_stockintransit <> 0;
UPDATE fact_salesorder SET ct_wipqty = 0 WHERE ct_wipqty <> 0;

UPDATE fact_salesorder so
SET so.ct_totalrestrictedstock = ifnull(tmpinv.ct_totalrestrictedstock, 0),
    so.ct_stockqty = ifnull(tmpinv.ct_stockqty, 0),
    so.ct_stockintransfer = ifnull(tmpinv.ct_stockintransfer, 0),
    so.ct_onhandwipqty = ifnull(tmpinv.ct_onhandwipqty, 0),
    so.ct_onhand = ifnull(tmpinv.ct_onhand, 0),
    so.ct_stockintransit = ifnull(tmpinv.ct_stockintransit, 0),
    so.ct_wipqty = ifnull(tmpinv.ct_wipqty, 0)
FROM fact_salesorder so,
     tmp_soforupdate tmpso,
     tmp_inventorymeasures tmpinv
WHERE so.dd_salesdocno = tmpso.dd_salesdocno
      AND so.dd_salesitemno = tmpso.dd_salesitemno
      AND so.dim_partid = tmpso.dim_partid
      AND so.dim_plantid = tmpso.dim_plantid
      AND so.dim_projectsourceid = tmpso.dim_projectsourceid
      AND tmpso.dim_projectsourceid = tmpinv.dim_projectsourceid
      AND so.dd_scheduleno = tmpso.min_dd_scheduleno
      AND so.dim_partid = tmpinv.dim_partid
      AND so.dim_plantid = tmpinv.dim_plantid;

DROP TABLE IF EXISTS tmp_soforupdate;
DROP TABLE IF EXISTS tmp_inventorymeasures;
/* 14 Oct 2016 CristianT End */

/* Merck LS Intercompany */

update fact_salesorder f
set f.DD_INTERCOMPFLAG = ifnull('3rd Party', 'Not Set')
from fact_salesorder f,Dim_AccountAssignmentGroup d
where f.Dim_AccountAssignmentGroupid = d.Dim_AccountAssignmentGroupid
	and d.accountassignmentgroup in ('01','02');

update fact_salesorder f
set f.DD_INTERCOMPFLAG = ifnull('Intercompany', 'Not Set')
from fact_salesorder f,Dim_AccountAssignmentGroup d
where f.Dim_AccountAssignmentGroupid = d.Dim_AccountAssignmentGroupid
	and d.accountassignmentgroup in ('03','04','08');

update fact_salesorder
set DD_INTERCOMPFLAG = 'Intercompany'
where DD_INTERCOMPFLAG = 'Not Set';

/* Marius Sigma MTO MTS */

update fact_salesorder f
set f.dd_mtomts = ifnull('Make to Order', 'Not Set')
from fact_salesorder f, dim_part dp, dim_unitofmeasure u
where f.dim_partid = dp.dim_partid and f.Dim_SalesUoMid = u.dim_unitofmeasureid
	and dp.PartType in ('ZFRT','KMAT') and u.uom <> 'EA' and left(dp.partnumber,3) <> 'GPS' and f.dd_mtomts = 'Not Set' and f.dd_mtomts <> 'Make to Order';

update fact_salesorder f
set f.dd_mtomts = ifnull('Make to Order', 'Not Set')
from fact_salesorder f, dim_part dp
where f.dim_partid = dp.dim_partid
	and dp.PartType = 'ZCSM' and left(dp.partnumber,3) <> 'GPS' and f.dd_mtomts = 'Not Set' and f.dd_mtomts <> 'Make to Order';

update fact_salesorder f
set f.dd_mtomts = ifnull('Make to Stock', 'Not Set')
from fact_salesorder f, dim_part dp, dim_unitofmeasure u, dim_MaterialPricingGroup mpg
where f.dim_partid = dp.dim_partid and f.Dim_SalesUoMid = u.dim_unitofmeasureid
	and f.dim_MaterialPricingGroupId = mpg.dim_MaterialPricingGroupId
	and dp.PartType = 'FERT' and u.uom = 'EA' and left(dp.partnumber,3) <> 'GPS'
	and mpg.MaterialPricingGroupCode not in ('20','21') and f.dd_mtomts = 'Not Set' and f.dd_mtomts <> 'Make to Stock';

update fact_salesorder f
set f.dd_mtomts = ifnull('Make to Order', 'Not Set')
from fact_salesorder f, dim_part dp, dim_unitofmeasure u, dim_MaterialPricingGroup mpg
where f.dim_partid = dp.dim_partid and f.Dim_SalesUoMid = u.dim_unitofmeasureid
	and f.dim_MaterialPricingGroupId = mpg.dim_MaterialPricingGroupId
	and dp.PartType = 'FERT' and u.uom = 'EA' and left(dp.partnumber,3) <> 'GPS'
	and mpg.MaterialPricingGroupCode in ('20','21') and f.dd_mtomts = 'Not Set' and f.dd_mtomts <> 'Make to Order';

update fact_salesorder f
set f.dd_mtomts = ifnull('Make to Order', 'Not Set')
from fact_salesorder f, dim_part dp
where f.dim_partid = dp.dim_partid and left(dp.partnumber,3) = 'GPS' and f.dd_mtomts = 'Not Set' and f.dd_mtomts <> 'Make to Order';

/* Merck LS get line item level gi date */

drop table if exists tmp_update_max_gidate;
create table tmp_update_max_gidate
  as
  select dd_salesdocno,dd_salesitemno,max(datevalue) max_gidate,min(datevalue) min_gidate
  from fact_salesorder inner join dim_date on dim_dateidgoodsissue = dim_dateid where dim_dateidgoodsissue <> 1 group by dd_salesdocno,dd_salesitemno;

update fact_salesorder f
  set f.dim_dateidgoodsissue_max = ifnull(d.dim_dateid, 1)
  from fact_salesorder f
    inner join tmp_update_max_gidate t on f.dd_salesdocno = t.dd_salesdocno and f.dd_salesitemno = t.dd_salesitemno
    inner join dim_date sdd on f.dim_dateidgoodsissue = sdd.dim_dateid
    inner join dim_date d on t.max_gidate = d.datevalue and d.companycode = sdd.companycode
  where f.dim_dateidgoodsissue_max <> ifnull(d.dim_dateid, 1);

update fact_salesorder f
  set f.dim_dateidgoodsissue_min = ifnull(dd.dim_dateid, 1)
  from fact_salesorder f
    inner join tmp_update_max_gidate t on f.dd_salesdocno = t.dd_salesdocno and f.dd_salesitemno = t.dd_salesitemno
    inner join dim_date sdd on f.dim_dateidgoodsissue = sdd.dim_dateid
    inner join dim_date dd on t.min_gidate = dd.datevalue and dd.companycode = sdd.companycode
  where f.dim_dateidgoodsissue_min <> ifnull(dd.dim_dateid, 1);

drop table if exists tmp_update_max_gidate;

update fact_salesorder f
set f.Dim_Addressid = ifnull(da.Dim_Addressid,1)
from fact_salesorder f, vbpa v, dim_Address da
where f.dd_salesdocno = v.vbpa_vbeln
and v.vbpa_parvw = 'WE'
   and v.VBPA_POSNR = 0
   and v.VBPA_ADRNR = da.AddressNumber
   and f.Dim_Addressid <> ifnull(da.Dim_Addressid,1);

   update fact_salesorder f
   set dd_customdatingflag = ifnull(CASE WHEN fc2.last_working_day > cd.datevalue  THEN '>3 days Past Due' ELSE 'Within 3 days' END, 'Not Set')
   from fact_salesorder f
     inner join dim_salesorderitemcategory ic on f.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
     inner join dim_date cd on f.dim_dateidsalesordercreated = cd.dim_dateid
     inner join dim_plant pl on f.dim_plantid = pl.dim_plantid
     inner join dim_Date_factory_calendar fc on cd.companycode = fc.companycode and pl.plantcode = fc.plantcode_factory and fc.DateValue = current_date - 1
     inner join dim_Date_factory_calendar fc1 on cd.companycode = fc1.companycode and pl.plantcode = fc1.plantcode_factory and fc1.datevalue = fc.last_working_day
     inner join dim_Date_factory_calendar fc2 on cd.companycode = fc2.companycode and pl.plantcode = fc2.plantcode_factory and fc2.datevalue = fc1.last_working_day
   where salesorderitemcategory IN ( 'ZTAB','YTAB','TAC','ZTAC','YTN2','ZGDM','ZGPE','ZGDP','ZGSP')
   and dd_customdatingflag <> ifnull(CASE WHEN fc2.last_working_day > cd.datevalue  THEN '>3 days Past Due' ELSE 'Within 3 days' END, 'Not Set');
/* Marius Merck lS Sigma Cluster */

update fact_salesorder f
set f.dim_clusterid = ifnull(c.dim_clusterid, 1)
from fact_salesorder f
	inner join dim_part p on f.dim_partid = p.dim_partid
	inner join dim_cluster c on p.laboratory = c.labor
where f.dim_clusterid <> ifnull(c.dim_clusterid, 1);


/*Add additional logic for dim_cluster =1 but with a primary production location Roxana D 2017-11-03*/
update fact_salesorder f
set f.dim_clusterid = ifnull(dc.dim_clusterid, 1)
from fact_salesorder f,
(select distinct primary_production_location_name,dim_mdg_partid from dim_mdg_part) mdg,
dim_bwproducthierarchy ph,
(SELECT primary_manufacturing_site, MAX(dim_clusterid) AS dim_clusterid FROM dim_cluster GROUP BY primary_manufacturing_site) dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location_name = dc.primary_manufacturing_site
	and f.dim_clusterid <> ifnull(dc.dim_clusterid, 1)
	and f.dim_clusterid = 1;
/*End 2017-11-03*/




merge into fact_salesorder so
using ( select fact_salesorderid, sprt.Dim_PartSalesId Dim_PartSalesId
		FROM VBAK_VBAP_VBEP vp, Dim_PartSales sprt, fact_salesorder fso
		where   fso.dd_SalesDocNo = vp.VBAK_VBELN
			AND fso.dd_SalesItemNo = vp.VBAP_POSNR
			AND fso.dd_ScheduleNo = vp.VBEP_ETENR
			AND sprt.SalesOrgCode = VBAK_VKORG
			AND sprt.PartNumber = VBAP_MATNR
            AND sprt.DISTRIBUTIONCHANNELCODE = VBAK_VTWEG
			AND sprt.RowIsCurrent = 1
			AND ifnull(fso.Dim_PartSalesId,-1) <> sprt.Dim_PartSalesId
group by fact_salesorderid,sprt.Dim_PartSalesId) sut on sut.fact_salesorderid = so.fact_salesorderid
when matched then update
SET Dim_PartSalesId = ifnull(sut.Dim_PartSalesId,1);

/* Update SIAl BI Filters*/

update fact_salesorder f
set dd_BiFilters_PRE_PRD = ifnull('X', 'Not Set')
from fact_salesorder f
inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
where UPPER(uph.businessdesc) not in ('APPLIED SOLUTIONS','RESEARCH SOLUTIONS')
and (dd_mercklsbackorderfilter='X' or dd_ace_openorders='X');


update fact_salesorder f
set dd_BiFilters_PRE_PRD = ifnull(case when
                 (case when f.ct_ScheduleQtySalesUnit - f.ct_ConfirmedQty < 0 then 0 else f.ct_ScheduleQtySalesUnit - f.ct_ConfirmedQty end)>0
                  and dp.PlantCode in ('AL01','AR01','AU01','BE01','BR01','CH02','CN01','CN02','DE04','DE06','FR02','GB01','IL01','IN01',
                    'IN02','KR01','MW01','MXTO','ONTO','SE02','SG01','SL01','TK02','CL01')
                  and sc.ScheduleLineCategory in ('CP','ZA','CV','ZF','CB')
                  and db.DeliveryBlock='Not Set'
				  and dbl.DeliveryBlock='Not Set'
                  and prt.PartType in ('FERT','NLAG')
                  and ph.Level2Code not in ('U399','C400')
                  and sorg.SalesOrgCode<>'1820'
                  and mpg4.MaterialPriceGroup4 in ('STK','NST')
                  and cat.salesorderitemcategory in ('KBN','LPN','TAB','TAC','TAN','TANN','YTAB','ZBSP','ZFD','ZFDC','ZOC','ZGDP','ZGOP','ZGPE','ZPOD','ZRBR','ZRCO','ZRFD','ZROR','ZTAB','ZTAN','ZTBN','ZTSD','ZZFC','ZZKB','ZZTN','ZZTQ')
				  and ph.Level1Code not in ('GN','FG')
                  and mpg4.MaterialPriceGroup4 <> 'POD'
                  then 'X' else 'Not Set' end, 'Not Set')

from fact_salesorder f
inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
inner join dim_plant dp on f.dim_plantid=dp.dim_plantid
inner join dim_schedulelinecategory sc on f.dim_schedulelinecategoryid=sc.dim_schedulelinecategoryid
inner join dim_deliveryblock db on f.dim_scheduledeliveryblockid=IFNULL(db.Dim_DeliveryBlockId,1)
inner join dim_deliveryblock dbl on f.dim_deliveryblockid = IFNULL(dbl.Dim_DeliveryBlockId,1)
inner join dim_part prt on f.dim_partid = prt.dim_partid
inner join dim_salesorg sorg on f.Dim_SalesOrgid=sorg.dim_salesorgid
inner join dim_materialpricegroup4 mpg4 on f.dim_materialpricegroup4id = mpg4.dim_materialpricegroup4id
inner join dim_producthierarchy ph on f.dim_producthierarchyid = ph.dim_producthierarchyid
inner join dim_salesorderitemcategory cat on f.dim_salesorderitemcategoryid = cat.dim_salesorderitemcategoryid
where UPPER(uph.businessdesc) in ('APPLIED SOLUTIONS','RESEARCH SOLUTIONS')
and (dd_mercklsbackorderfilter='X' or dd_ace_openorders='X');

update fact_salesorder f
set DD_BIFILTERS_PRE_PRD_new = ifnull('X', 'Not Set')
from fact_salesorder f
inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
where UPPER(uph.businessdesc) not in ('APPLIED SOLUTIONS','RESEARCH SOLUTIONS') and dd_ace_openorders='X';

update fact_salesorder f
set DD_BIFILTERS_PRE_PRD_new = ifnull(case when
                  dp.PlantCode in ('AL01','AR01','AU01','BE01','BR01','CH02','CN01','CN02','DE04','DE06','FR02','GB01','IL01','IN01',
                    'IN02','KR01','MW01','MXTO','ONTO','SE02','SG01','SL01','TK02','CL01')
                  and sc.ScheduleLineCategory in ('CP','ZA','CV','ZF','CB')
                  and db.DeliveryBlock='Not Set'
				  and dbl.DeliveryBlock='Not Set'
                  and prt.PartType in ('FERT','NLAG')
                  and ph.Level2Code not in ('U399','C400')
                  and sorg.SalesOrgCode<>'1820'
                  and mpg4.MaterialPriceGroup4 in ('STK','NST')
                  and cat.salesorderitemcategory in ('KBN','LPN','TAB','TAC','TAN','TANN','YTAB','ZBSP','ZFD','ZFDC','ZOC','ZGDP','ZGOP','ZGPE','ZPOD','ZRBR','ZRCO','ZRFD','ZROR','ZTAB','ZTAN','ZTBN','ZTSD','ZZFC','ZZKB','ZZTN','ZZTQ')
				  and ph.Level1Code not in ('GN','FG')
                  and mpg4.MaterialPriceGroup4 <> 'POD'
                  then 'X' else 'Not Set' end, 'Not Set')

from fact_salesorder f
inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
inner join dim_plant dp on f.dim_plantid=dp.dim_plantid
inner join dim_schedulelinecategory sc on f.dim_schedulelinecategoryid=sc.dim_schedulelinecategoryid
inner join dim_deliveryblock db on f.dim_scheduledeliveryblockid=IFNULL(db.Dim_DeliveryBlockId,1)
inner join dim_deliveryblock dbl on f.dim_deliveryblockid = IFNULL(dbl.Dim_DeliveryBlockId,1)
inner join dim_part prt on f.dim_partid = prt.dim_partid
inner join dim_salesorg sorg on f.Dim_SalesOrgid=sorg.dim_salesorgid
inner join dim_materialpricegroup4 mpg4 on f.dim_materialpricegroup4id = mpg4.dim_materialpricegroup4id
inner join dim_producthierarchy ph on f.dim_producthierarchyid = ph.dim_producthierarchyid
inner join dim_salesorderitemcategory cat on f.dim_salesorderitemcategoryid = cat.dim_salesorderitemcategoryid
where UPPER(uph.businessdesc) in ('APPLIED SOLUTIONS','RESEARCH SOLUTIONS') and dd_ace_openorders='X';

update fact_salesorder so
set dd_itemcategory = ifnull('Exclusion', 'Not Set')
from fact_salesorder so, dim_salesorderitemcategory ic
where so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
and ic.Description NOT IN ('Third Party Item', '3rd Party Item w/ GR', 'ThirdParty Serv WIP', '3rdParty2Consignment', 'Third Party Item-MES',
				'3rd FOC W/O Cost', '3rd FOC with Cost', '3rd Party Item 71', '3rd Party Item w/GR', '3rd Party w/ GR FOC', 'FOC- Third party-MHK',
				'Third Party Item FOC', 'Third Party Item-MHK' , 'Third Party Item_MDA');

/* Update According GI Date MTO and According GI Date MTS */
UPDATE fact_salesorder f
SET f.dim_accordinggidatemto_emd = ifnull(f.Dim_DateidGoodsIssue, 1),
  dim_dateidexpectedship_emd = ifnull(f.Dim_DateidGoodsIssue, 1);

UPDATE  fact_salesorder f
SET f.dim_dateidactualgimerckls = ifnull(f.dim_dateidactualgi, 1);

/*Update flag for fillrate snapshot in Sales analysis*/

drop table if exists fillratesnapshot_flag_update;
create table fillratesnapshot_flag_update
as select distinct dd_salesdocno,dd_salesitemno
from fact_fillrate_snapshot;

update fact_salesorder f
set dd_fillratesnapshot_flag = ifnull('Y', 'Not Set')
from fact_salesorder f,fillratesnapshot_flag_update ff
where f.dd_salesdocno=ff.dd_salesdocno and f.dd_salesitemno=ff.dd_salesitemno
and ifnull(f.dd_fillratesnapshot_flag,'Not Set') = 'Not Set';




drop table if exists min_scheduleno;
create table min_scheduleno
as select min(dd_scheduleno) dd_min_scheduleno,dd_salesdocno,dd_salesitemno
from fact_salesorder
group by dd_salesdocno,dd_salesitemno;

update fact_salesorder f
set f.dd_min_scheduleno = ifnull(m.dd_min_scheduleno, 0)
from fact_salesorder f,min_scheduleno m
where f.dd_salesdocno=m.dd_salesdocno
and f.dd_salesitemno=m.dd_salesitemno;

/* Update MTO - MTS */

update fact_salesorder
set dd_mtomts='MTS'
from fact_salesorder
where dd_mtomts='Make to Stock';


update fact_salesorder
set dd_mtomts='MTO'
from fact_salesorder
where dd_mtomts='Make to Order';

update fact_salesorder
set dd_mtomts= mtomts
from fact_salesorder f,dim_part d
where f.dim_partid=d.dim_partid
and dd_mtomts='Not Set';

/* GSA - Update - Alina 07.04.2017 */

update fact_salesorder f_so set f_so.dim_gsamapimportid = ifnull(gsa.dim_gsamapimportid,1)
   FROM fact_salesorder f_so
        ,dim_customer dc
        ,dim_bwproducthierarchy dph
        ,dim_gsamapimport gsa
 WHERE f_so.dim_customerid = dc.dim_customerid
   AND f_so.dim_bwproducthierarchyid = dph.dim_bwproducthierarchyid
   AND TRIM(LEADING '0' FROM dc.customernumber) = CAST(gsa.customer_number AS VARCHAR(10))
   AND TRIM(UPPER(dph.businessdesc)) LIKE '%'||TRIM(UPPER(gsa.business_name))||'%'
   AND gsa.platform = 'SIAL'
   AND gsa.customer_number_ship='Not Set'
      and f_so.dim_gsamapimportid <> ifnull(gsa.dim_gsamapimportid,1);


/* Moved the update for GSA Code based on dim_customer_shipTo in Sales delivery
after the update of the dim_customeridShipto  Roxana D 2017-11-27*/

/* 10 april 2017 Cornelia - add CompleteInSingleDelivery Flag */
UPDATE fact_salesorder fso
SET fso.dd_CompleteInSingleDelivery = ifnull(vkp.VBAK_AUTLF , 'Not Set')
FROM fact_salesorder fso
INNER JOIN VBAK_VBAP_VBEP vkp ON fso.dd_SalesDocNo = vkp.VBAK_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
WHERE fso.dd_CompleteInSingleDelivery <> ifnull(vkp.VBAK_AUTLF , 'Not Set');

/* 12 april 2017 Alina - Delivery Group and Batch Split */

UPDATE fact_salesorder fso
SET fso.dd_batch_split = ifnull(vkp.VBAP_CHSPL,'Not Set')
FROM fact_salesorder fso
INNER JOIN VBAK_VBAP_VBEP vkp ON fso.dd_SalesDocNo = vkp.VBAK_VBELN
AND fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
WHERE fso.dd_batch_split <> ifnull(vkp.VBAP_CHSPL , 'Not Set');


/* UPDATE fact_salesorder fso
SET fso.dd_delivery_group = vkp.VBAP_GRKOR
FROM fact_salesorder fso
INNER JOIN VBAK_VBAP_VBEP vkp ON fso.dd_SalesDocNo = vkp.VBAK_VBELN AND
fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
WHERE fso.dd_delivery_group <> vkp.VBAP_GRKOR */

UPDATE fact_salesorder fso
SET fso.dd_delivery_group = ifnull(CAST(VBAP_GRKOR AS VARCHAR(40)), 'Not Set')
FROM fact_salesorder fso
INNER JOIN VBAK_VBAP_VBEP vkp ON fso.dd_SalesDocNo = vkp.VBAK_VBELN AND
fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
WHERE fso.dd_delivery_group <> ifnull(CAST(VBAP_GRKOR AS VARCHAR(40)), 'Not Set');

/* Requested Shipment date ( Controlling ) */
update fact_salesorder
set  dim_ReqShipmentControlling  = ifnull(dim_dateidscheddelivery, 1)
where dim_ReqShipmentControlling <> ifnull(dim_dateidscheddelivery, 1);


/* Roxana - 7 jul 2017 - populate Merck Ls Confirmed or Requested Date */


update fact_salesorder f_so
set dim_mercklsconforreqdateid = ifnull(CASE WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN f_so.dim_dateidexpectedship_emd ELSE
  CASE WHEN trim(prt.mtomts) = 'MTO' THEN f_so.dim_accordinggidatemto_emd
      ELSE f_so.dim_dateidexpectedship_emd END END, 1)
from fact_salesorder f_so, dim_part prt, dim_bwproducthierarchy bw,dim_date_factory_calendar esdemd,dim_date_factory_calendar  acgidmto
where f_so.Dim_Partid = prt.Dim_Partid
and f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
and f_so.dim_dateidexpectedship_emd = esdemd.dim_dateid
and f_so.dim_accordinggidatemto_emd = acgidmto.dim_dateid;


/* Add Batch Expiration date - Roxana D */


DROP TABLE IF EXISTS tmp_for_DateidExpiryDate_SO;
CREATE TABLE tmp_for_DateidExpiryDate_SO as
SELECT DISTINCT dt1.dim_dateid,i.fact_salesorderid, row_number() over (partition by fact_salesorderid order by fact_salesorderid) as row_num
FROM
fact_salesorder i
INNER JOIN mch1 m ON i.dd_BatchNo = m.mch1_charg
INNER JOIN dim_company dc ON i.dim_companyid = dc.dim_companyid
INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid AND dp1.partnumber = m.mch1_matnr
INNER JOIN dim_date dt1 ON dt1.CompanyCode = dc.CompanyCode AND dt1.datevalue = m.mch1_vfdat
WHERE 1=1;


UPDATE fact_salesorder i
SET dim_DateidExpiryDate = ifnull(dt1.dim_dateid, 1)
FROM fact_salesorder i,tmp_for_DateidExpiryDate_SO dt1
WHERE i.fact_salesorderid=dt1.fact_salesorderid
AND row_num=1
AND  dim_DateidExpiryDate <> ifnull(dt1.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_for_DateidExpiryDate_SO;

/* End Batch Expiration Date*/


/*21 Dec 2017 - Add CT_SalesOrderStock from MSKA on min(ScheduleNo) - Roxana Dragnea*/

UPDATE fact_salesorder  fso
SET fso.CT_SalesOrderStock = m.MSKA_KALABTOTAL
from
(SELECT MSKA_VBELN, MSKA_POSNR, SUM(MSKA_KALAB) MSKA_KALABTOTAL FROM MSKA m WHERE MSKA_LFGJA*100+MSKA_LFMON <= YEAR(CURRENT_DATE)*100 + MONTH(CURRENT_DATE) AND MSKA_SOBKZ = 'E' GROUP BY MSKA_VBELN, MSKA_POSNR) m
inner join (SELECT dd_salesdocNo, dd_salesitemno, min(dd_scheduleno) min_dd_schedulen0 from fact_salesorder group by dd_salesdocNo, dd_salesitemno ) f
on m.MSKA_VBELN = f.dd_salesdocno and IFNULL(m.MSKA_POSNR,'Not Set') = f.dd_salesitemno
inner join fact_salesorder fso on fso.dd_salesdocno = f.dd_salesdocno and fso.dd_salesitemno = f.dd_salesitemno AND fso.dd_scheduleno = f.min_dd_schedulen0
WHERE fso.CT_SalesOrderStock <> m.MSKA_KALABTOTAL;



drop table if exists Update_accDates;
create table Update_accDates as
select distinct dd_salesdocno,dd_salesitemno,d.datevalue
from fact_salesorder f,dim_date d
where f.Dim_DateidGoodsIssue=d.dim_dateid
and dd_scheduleno=2
and f.Dim_DateidGoodsIssue<>1;

drop table if exists Update_accDates_2;
create table Update_accDates_2 as
select distinct
dd_salesdocno,dd_salesitemno,min(datevalue) minDatevalue
from fact_salesorder f,dim_date d
where f.Dim_DateidGoodsIssue=d.dim_dateid
group by dd_salesdocno,dd_salesitemno;

update fact_salesorder f
set f.dim_original_accordinggidatemto_emd = ifnull(d.dim_dateid, 1)
from fact_salesorder f,Update_accDates up,dim_date d,dim_company c
where f.dd_salesdocno=up.dd_salesdocno and f.dd_salesitemno=up.dd_salesitemno
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and up.Datevalue=d.datevalue
and f.dim_original_accordinggidatemto_emd=1
and f.dim_original_accordinggidatemto_emd <> ifnull(d.dim_dateid, 1);

update fact_salesorder f
set f.dim_original_accordinggidatemto_emd = ifnull(d.dim_dateid, 1)
from fact_salesorder f,Update_accDates_2 ups,dim_date d,dim_company c
where f.dd_salesdocno=ups.dd_salesdocno and f.dd_salesitemno=ups.dd_salesitemno
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and ups.minDatevalue=d.datevalue
and f.dim_original_accordinggidatemto_emd=1
and f.dim_original_accordinggidatemto_emd<> ifnull(d.dim_dateid, 1);


update fact_salesorder f
set f.dim_original_accordinggidatemto_emd = dim_original_dateidexpectedship_emd;
