/* Start of custom exchange rate proc for bi_inventoryturn_processing. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_inventoryturn_processing';

Drop table if exists tmp_globalcur;

Create table tmp_globalcur
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD') as char(3)) pGlobalCurrency
;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT c.Currency pFromCurrency, t.pGlobalCurrency pToCurrency,null pFromExchangeRate,CURRENT_DATE pDate,'bi_inventoryturn_processing' fact_script_name
FROM fact_inventory inv,dim_company c, tmp_globalcur t
WHERE inv.Dim_Companyid = c.Dim_Companyid;

/* tmptable for exchrate is populated here, rather than creating another separate custom script */

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT HWAER pFromCurrency,t.pGlobalCurrency pToCurrency,null pFromExchangeRate,CURRENT_DATE pDate,'bi_inventoryturn_processing' fact_script_name
FROM S031 a, tmp_globalcur t;

UPDATE tmp_getExchangeRate1 t0
SET 
t0.pToCurrency = ifnull(t1.property_value,'USD')
FROM 
tmp_getExchangeRate1 t0
CROSS JOIN ( SELECT property_value  
	  FROM systemproperty 
	  WHERE property = 'customer.global.currency') T1
WHERE t0.fact_script_name = 'bi_inventoryturn_processing';

UPDATE tmp_getExchangeRate1
set exchangeRate = NULL
WHERE fact_script_name = 'bi_inventoryturn_processing';

drop table if exists tmp_getExchangeRate1_nodups_sof;
create table tmp_getExchangeRate1_nodups_sof
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_inventoryturn_processing';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_inventoryturn_processing';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_sof;

drop table tmp_getExchangeRate1_nodups_sof;