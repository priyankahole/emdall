DROP TABLE IF EXISTS tmp_del1_inslot;
CREATE TABLE tmp_del1_inslot
AS
SELECT DISTINCT il.dd_inspectionlotno from fact_inspectionlot il;


DROP TABLE IF EXISTS tmp_ins1_inslot;
CREATE TABLE tmp_ins1_inslot
AS
SELECT il.dd_inspectionlotno from fact_inspectionlot il where 1=2;

insert into tmp_ins1_inslot
select distinct q.QALS_PRUEFLOS from qals q;

/*call vectorwise(combine 'tmp_ins1_inslot-tmp_del1_inslot')*/
delete from tmp_ins1_inslot
WHERE dd_inspectionlotno in  (SELECT dd_inspectionlotno FROM tmp_del1_inslot);



DROP TABLE IF EXISTS tmp_ins2_inslot;
CREATE TABLE tmp_ins2_inslot
AS
SELECT DISTINCT q.*
FROM qals q,tmp_ins1_inslot i
WHERE q.QALS_PRUEFLOS = i.dd_inspectionlotno;



DROP TABLE IF EXISTS tmp_dim_profitcenter;
CREATE TABLE tmp_dim_profitcenter
AS
SELECT  ControllingArea, ProfitCenterCode,QALS_ERSTELDAT, MIN(ValidTo) as min_validto
FROM dim_profitcenter, qals
WHERE  ControllingArea = QALS_KOKRS
AND ProfitCenterCode = QALS_PRCTR
AND ValidTo >= QALS_ERSTELDAT
AND RowIsCurrent = 1
GROUP BY  ControllingArea, ProfitCenterCode,QALS_ERSTELDAT ;

DROP TABLE IF EXISTS tmp_dim_profitcenter2;
CREATE TABLE tmp_dim_profitcenter2
AS
SELECT DISTINCT p1.*,p2.dim_profitcenterid
FROM dim_profitcenter p2,tmp_dim_profitcenter p1
WHERE p1.ControllingArea = p2.ControllingArea
AND p1.ProfitCenterCode = p2.ProfitCenterCode
AND p1.min_validto = p2.validto
AND p2.RowIsCurrent = 1;


/*INSERT INTO fact_inspectionlot(fact_inspectionlotid,
                               ct_actualinspectedqty,
                               ct_actuallotqty,
                               ct_blockedqty,
                               ct_inspectionlotqty,
                               ct_postedqty,
                               ct_qtydefective,
                               ct_qtyreturned,
                               ct_reserveqty,
                               ct_sampleqty,
                               ct_samplesizeqty,
                               ct_scrapqty,
                               ct_unrestrictedqty,
                               dd_ObjectNumber,
                               dd_batchno,
                               dd_changedby,
                               dd_createdby,
                               dd_documentitemno,
                               dd_documentno,
                               dd_inspectionlotno,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocNo,
                               dd_MaterialDocYear,
                               dd_orderno,
                               dd_ScheduleNo,
                               Dim_AccountCategoryid,
                               dim_costcenterid,
                               dim_dateidinspectionend,
                               dim_dateidinspectionstart,
                               dim_dateidkeydate,
                               dim_dateidlotcreated,
                               dim_dateidposting,
                               dim_documenttypetextid,
                               dim_inspectionlotmiscid,
                               dim_inspectionlotoriginid,
                               dim_inspectionlotstoragelocationid,
                               dim_inspectionsamplestatusid,
                               dim_inspectionseverityid,
                               dim_inspectionstageid,
                               dim_inspectiontypeid,
                               dim_itemcategoryid,
                               dim_lotunitofmeasureid,
                               dim_manufacturerid,
                               dim_ObjectCategoryid,
                               dim_partid,
                               dim_plantid,
                               dim_purchaseorgid,
                               dim_routeid,
                               dim_sampleunitofmeasureid,
                               dim_specialstockid,
                               dim_statusprofileid,
                               dim_storagelocationid,
                               dim_tasklisttypeid,
                               dim_tasklistusageid,
                               dim_vendorid,
                               dim_warehousenumberid,
                               dim_Controllingareaid,
                               dim_profitcenterid)
        SELECT (SELECT ifnull(max(fil.fact_inspectionlotid), 1) id
             FROM fact_inspectionlot fil)
          + row_number() over(),
                  QALS_LMENGEPR ct_actualinspectedqty,
          QALS_LMENGEIST ct_actuallotqty,
          QALS_LMENGE04 ct_blockedqty,
          QALS_LOSMENGE ct_inspectionlotqty,
          QALS_LMENGEZUB ct_postedqty,
          QALS_LMENGESCH ct_qtydefective,
          QALS_LMENGE07 ct_qtyreturned,
          QALS_LMENGE05 ct_reserveqty,
          QALS_LMENGE03 ct_sampleqty,
          QALS_GESSTICHPR ct_samplesizeqty,
          QALS_LMENGE02 ct_scrapqty,
          QALS_LMENGE01 ct_unrestrictedqty,
          ifnull(QALS_OBJNR,'Not Set') dd_ObjectNumber,
          ifnull(QALS_CHARG, 'Not Set') dd_batchno,
          ifnull(QALS_AENDERER, 'Not Set') dd_changedby,
          QALS_ERSTELLER dd_createdby,
          QALS_EBELP dd_documentitemno,
          ifnull(QALS_EBELN, 'Not Set') dd_documentno,
          QALS_PRUEFLOS dd_inspectionlotno,
          QALS_ZEILE dd_MaterialDocItemNo,
          ifnull(QALS_MBLNR, 'Not Set') dd_MaterialDocNo,
          QALS_MJAHR dd_MaterialDocYear,
          ifnull(QALS_AUFNR, 'Not Set') dd_orderno,
          QALS_ETENR dd_ScheduleNo,
          (SELECT dim_accountcategoryid
             FROM dim_accountcategory ac
            WHERE ac.Category = ifnull(QALS_KNTTP, 'Not Set')
                  AND ac.RowIsCurrent = 1)
             Dim_AccountCategoryid,
          ifnull((SELECT dim_costcenterid
             FROM dim_costcenter cc
            WHERE     cc.Code = ifnull(QALS_KOSTL, 'Not Set')
                  AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set')
                  AND cc.RowIsCurrent = 1),1)
             dim_costcenterid,
          ifnull((SELECT dim_dateid
                   FROM dim_date ie
                  WHERE ie.DateValue = QALS_PAENDTERM
                        AND ie.CompanyCode = dp.CompanyCode
                                                AND QALS_PAENDTERM IS NOT NULL), 1)
             dim_dateidinspectionend,
          ifnull((SELECT dim_dateid
                   FROM dim_date dis
                  WHERE dis.DateValue = QALS_PASTRTERM
                        AND dis.CompanyCode = dp.CompanyCode
                                                AND QALS_PASTRTERM IS NOT NULL), 1)
             dim_dateidinspectionstart,
          ifnull((SELECT dim_dateid
                   FROM dim_date kd
                  WHERE kd.DateValue = QALS_GUELTIGAB
                        AND kd.CompanyCode = dp.CompanyCode
                                                AND QALS_GUELTIGAB IS NOT NULL), 1)
             dim_dateidkeydate,
          ifnull((SELECT dim_dateid
                      FROM dim_date lcd
                     WHERE lcd.DateValue = QALS_ENSTEHDAT
                           AND lcd.CompanyCode = dp.CompanyCode
                                                   AND QALS_ENSTEHDAT IS NOT NULL), 1)
             dim_dateidlotcreated,
          ifnull((SELECT dim_dateid
                   FROM dim_date pd
                  WHERE pd.DateValue = QALS_BUDAT
                        AND pd.companycode = dp.CompanyCode
                                                AND QALS_BUDAT IS NOT NULL), 1)
             dim_dateidposting,
          (SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.type = ifnull(QALS_BLART, 'Not Set')
                  AND dtt.RowIsCurrent = 1)
             dim_documenttypetextid,
          (SELECT dim_inspectionlotmiscid
             FROM dim_inspectionlotmisc
            WHERE     AutomaticInspectionLot = ifnull(QALS_STAT01, 'Not Set')
                  AND BatchManagementRequired = ifnull(QALS_XCHPF, 'Not Set')
                  AND CompleteInspection = ifnull(QALS_HPZ, 'Not Set')
                  AND DocumentationRequired = ifnull(QALS_STAT19, 'Not Set')
                  AND InspectionPlanRequired = ifnull(QALS_STAT20, 'Not Set')
                  AND InspectionStockQuantity = ifnull(QALS_INSMK, 'Not Set')
                  AND ShotTermInspectionComplete =
                         ifnull(QALS_STAT14, 'Not Set')
                  AND StockPostingsComplete = ifnull(QALS_STAT34, 'Not Set')
                  AND UsageDecisionMade = ifnull(QALS_STAT35, 'Not Set')
                  AND LotSkipped = ifnull(QALS_KZSKIPLOT, 'Not Set'))
             dim_inspectionlotmiscid,
          (SELECT dim_inspectionlotoriginid
             FROM dim_inspectionlotorigin ilo
            WHERE ilo.InspectionLotOriginCode =
                     ifnull(QALS_HERKUNFT, 'Not Set')
                  AND ilo.RowIsCurrent = 1)
             dim_inspectionlotoriginid,
          ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation isl
                  WHERE     isl.LocationCode = QALS_LAGORTVORG
                        AND isl.RowIsCurrent = 1
                        AND isl.Plant = dp.PlantCode
                                                AND QALS_LAGORTVORG IS NOT NULL), 1)
          dim_inspectionlotstoragelocationid,
          (SELECT dim_inspectionsamplestatusid
             FROM dim_inspectionsamplestatus iss
            WHERE iss.statuscode = ifnull(QALS_LVS_STIKZ, 'Not Set')
                  AND iss.RowIsCurrent = 1)
             dim_inspectionsamplestatusid,
          ifnull((SELECT dim_inspectionseverityid
             FROM dim_inspectionseverity isvr
            WHERE isvr.InspectionSeverityCode = QALS_PRSCHAERFE
                  AND isvr.RowIsCurrent = 1),1)
             dim_inspectionseverityid,
          ifnull((SELECT dim_inspectionstageid
             FROM dim_inspectionstage istg
            WHERE istg.InspectionStageCode = QALS_PRSTUFE
                  AND istg.InspectionStageRuleCode =
                         ifnull(QALS_DYNREGEL, 'Not Set')
                  AND istg.RowIsCurrent = 1),1)
             dim_inspectionstageid,
          (SELECT dim_inspectiontypeid
             FROM dim_inspectiontype ityp
            WHERE ityp.InspectionTypeCode = ifnull(QALS_ART, 'Not Set')
                  AND ityp.RowIsCurrent = 1)
             dim_inspectiontypeid,
          (SELECT dim_itemcategoryid
             FROM dim_itemcategory icc
            WHERE icc.CategoryCode = ifnull(QALS_PSTYP, 'Not Set')
                  AND icc.RowIsCurrent = 1)
             dim_itemcategoryid,
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure luom
            WHERE luom.UOM = ifnull(QALS_MENGENEINH, 'Not Set')
                  AND luom.RowIsCurrent = 1)
             dim_lotunitofmeasureid,
          ifnull((SELECT dim_vendorid
             FROM dim_vendor dv
            WHERE dv.VendorNumber = ifnull(QALS_HERSTELLER, 'Not Set')
                  AND dv.RowIsCurrent = 1),1)
             dim_manufacturerid,
          (SELECT dim_ObjectCategoryid
             FROM dim_ObjectCategory oc
            WHERE oc.ObjectCategoryCode = ifnull(QALS_OBTYP, 'Not Set')
                  AND oc.RowIsCurrent = 1)
             dim_ObjectCategoryid,
          dim_partid,
          dim_plantid,
          (SELECT dim_purchaseorgid
             FROM dim_purchaseorg po
            WHERE po.PurchaseOrgCode = ifnull(QALS_EKORG, 'Not Set')
                  AND po.RowIsCurrent = 1)
             dim_purchaseorgid,
          (SELECT dim_routeid
             FROM dim_route r
            WHERE r.RouteCode = ifnull(QALS_LS_ROUTE, 'Not Set')
                  AND r.RowIsCurrent = 1)
             dim_routeid,
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure uom
            WHERE uom.UOM = ifnull(QALS_EINHPROBE, 'Not Set')
                  AND uom.RowIsCurrent = 1)
             dim_sampleunitofmeasureid,
          (SELECT dim_specialstockid
             FROM dim_specialstock ss
            WHERE ss.specialstockindicator = ifnull(QALS_SOBKZ, 'Not Set')
                  AND ss.RowIsCurrent = 1)
             dim_specialstockid,
          (SELECT dim_statusprofileid
             FROM dim_statusprofile sp
            WHERE sp.StatusProfileCode = ifnull(QALS_STSMA, 'Not Set')
                  AND sp.RowIsCurrent = 1)
             dim_statusprofileid,
          ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation sl
                  WHERE sl.LocationCode = ifnull(QALS_LAGORTCHRG, 'Not Set')
                        AND sl.Plant = dp.PlantCode
                        AND sl.RowIsCurrent = 1
                                                AND QALS_LAGORTCHRG IS NOT NULL), 1)
          dim_storagelocationid,
          (SELECT dim_tasklisttypeid
             FROM dim_tasklisttype tlt
            WHERE tlt.TaskListTypeCode = ifnull(QALS_PLNTY, 'Not Set')
                  AND tlt.RowIsCurrent = 1)
             dim_tasklisttypeid,
          (SELECT dim_tasklistusageid
             FROM dim_tasklistusage tlu
            WHERE tlu.UsageCode = ifnull(QALS_PPLVERW, 'Not Set')
                  AND tlu.RowIsCurrent = 1)
             dim_tasklistusageid,
          ifnull((SELECT dim_vendorid
             FROM dim_vendor dv1
            WHERE dv1.VendorNumber = ifnull(QALS_LIFNR, 'Not Set')
                  AND dv1.RowIsCurrent = 1),1)
             dim_vendorid,
          ifnull((SELECT dim_warehouseid
             FROM dim_warehousenumber wn
            WHERE wn.WarehouseCode = ifnull(QALS_LGNUM, 'Not Set')
                  AND wn.RowIsCurrent = 1),1)
             dim_warehousenumberid,
          ifnull((SELECT dim_controllingareaid
             FROM dim_controllingarea ca
            WHERE ca.ControllingAreaCode = QALS_KOKRS),1) dim_controllingareaid,
ifnull((SELECT  pc.dim_profitcenterid FROM tmp_dim_profitcenter2 pc WHERE  pc.ControllingArea = QALS_KOKRS AND pc.ProfitCenterCode = QALS_PRCTR 
AND pc.QALS_ERSTELDAT = QALS_ERSTELDAT ),1) dim_profitcenterid
      FROM tmp_ins2_inslot q
          INNER JOIN dim_plant dp
             ON dp.PlantCode = q.QALS_WERK AND dp.RowIsCurrent = 1
          INNER JOIN dim_part dpa
             ON     dpa.PartNumber = QALS_MATNR
                AND dpa.Plant = dp.PlantCode
                AND dpa.RowIsCurrent = 1*/
			


/* INSERT INTO tmp_pre_fact_inspectionlot(fact_inspectionlotid,
                               ct_actualinspectedqty,
                               ct_actuallotqty,
                               ct_blockedqty,
                               ct_inspectionlotqty,
                               ct_postedqty,
                               ct_qtydefective,
                               ct_qtyreturned,
                               ct_reserveqty,
                               ct_sampleqty,
                               ct_samplesizeqty,
                               ct_scrapqty,
                               ct_unrestrictedqty,
                               dd_ObjectNumber,
                               dd_batchno,
                               dd_changedby,
                               dd_createdby,
                               dd_documentitemno,
                               dd_documentno,
                               dd_inspectionlotno,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocNo,
                               dd_MaterialDocYear,
                               dd_orderno,
                               dd_ScheduleNo,
                               Dim_AccountCategoryid,
                               dim_costcenterid,
                               dim_dateidinspectionend,
                               dim_dateidinspectionstart,
                               dim_dateidkeydate,
                               dim_dateidlotcreated,
                               dim_dateidposting,
                               dim_documenttypetextid,
                               dim_inspectionlotmiscid,
                               dim_inspectionlotoriginid,
                               dim_inspectionlotstoragelocationid,
                               dim_inspectionsamplestatusid,
                               dim_inspectionseverityid,
                               dim_inspectionstageid,
                               dim_inspectiontypeid,
                               dim_itemcategoryid,
                               dim_lotunitofmeasureid,
                               dim_manufacturerid,
                               dim_ObjectCategoryid,
                               dim_partid,
                               dim_plantid,
                               dim_purchaseorgid,
                               dim_routeid,
                               dim_sampleunitofmeasureid,
                               dim_specialstockid,
                               dim_statusprofileid,
                               dim_storagelocationid,
                               dim_tasklisttypeid,
                               dim_tasklistusageid,
                               dim_vendorid,
                               dim_warehousenumberid,
                               dim_Controllingareaid,
                               dim_profitcenterid)*/
							   
DROP TABLE IF EXISTS tmp_pre_fact_inspectionlot;	
CREATE TABLE tmp_pre_fact_inspectionlot AS
SELECT (SELECT ifnull(max(fil.fact_inspectionlotid), 1) id FROM fact_inspectionlot fil) + row_number() over(order by '')+ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0) as fact_inspectionlotid,
        QALS_LMENGEPR ct_actualinspectedqty,
        QALS_LMENGEIST ct_actuallotqty,
        QALS_LMENGE04 ct_blockedqty,
        QALS_LOSMENGE ct_inspectionlotqty,
        QALS_LMENGEZUB ct_postedqty,
        QALS_LMENGESCH ct_qtydefective,
        QALS_LMENGE07 ct_qtyreturned,
        QALS_LMENGE05 ct_reserveqty,
        QALS_LMENGE03 ct_sampleqty,
        QALS_GESSTICHPR ct_samplesizeqty,
        QALS_LMENGE02 ct_scrapqty,
        QALS_LMENGE01 ct_unrestrictedqty,
        ifnull(QALS_OBJNR,'Not Set') dd_ObjectNumber,
        ifnull(QALS_CHARG, 'Not Set') dd_batchno,
        ifnull(QALS_AENDERER, 'Not Set') dd_changedby,
        QALS_ERSTELLER dd_createdby,
        QALS_EBELP dd_documentitemno,
        ifnull(QALS_EBELN, 'Not Set') dd_documentno,
        QALS_PRUEFLOS dd_inspectionlotno,
        QALS_ZEILE dd_MaterialDocItemNo,
        ifnull(QALS_MBLNR, 'Not Set') dd_MaterialDocNo,
        QALS_MJAHR dd_MaterialDocYear,
        ifnull(QALS_AUFNR, 'Not Set') dd_orderno,
        QALS_ETENR dd_ScheduleNo,
       convert(BIGINT, 1) as Dim_AccountCategoryid,
       convert(BIGINT, 1) as dim_costcenterid,
       convert(BIGINT, 1) as dim_dateidinspectionend,
       convert(BIGINT, 1) as dim_dateidinspectionstart,
       convert(BIGINT, 1) as dim_dateidkeydate,
       convert(BIGINT, 1) as dim_dateidlotcreated,
       convert(BIGINT, 1) as dim_dateidposting,
       convert(BIGINT, 1) as dim_documenttypetextid,
       convert(BIGINT, 1) as dim_inspectionlotmiscid,
       convert(BIGINT, 1) as dim_inspectionlotoriginid,
       convert(BIGINT, 1) as dim_inspectionlotstoragelocationid,
       convert(BIGINT, 1) as dim_inspectionsamplestatusid,
       convert(BIGINT, 1) as dim_inspectionseverityid,
       convert(BIGINT, 1) as dim_inspectionstageid,
       convert(BIGINT, 1) as dim_inspectiontypeid,
       convert(BIGINT, 1) as dim_itemcategoryid,
       convert(BIGINT, 1) as dim_lotunitofmeasureid,
       convert(BIGINT, 1) as dim_manufacturerid,
       convert(BIGINT, 1) as dim_ObjectCategoryid,
       dim_partid as dim_partid,
       dim_plantid as dim_plantid,
       convert(BIGINT, 1) as dim_purchaseorgid,
       convert(BIGINT, 1) as dim_routeid,
       convert(BIGINT, 1) as dim_sampleunitofmeasureid,
       convert(BIGINT, 1) as dim_specialstockid,
       convert(BIGINT, 1) as dim_statusprofileid,
       convert(BIGINT, 1) as dim_storagelocationid,
       convert(BIGINT, 1) as dim_tasklisttypeid,
       convert(BIGINT, 1) as dim_tasklistusageid,
       convert(BIGINT, 1) as dim_vendorid,
       convert(BIGINT, 1) as dim_warehousenumberid,
       convert(BIGINT, 1) as dim_Controllingareaid,
       convert(BIGINT, 1) as dim_profitcenterid,
	   QALS_KNTTP, 				  -- used in dim_accountcategoryid
	   QALS_KOSTL, QALS_KOKRS,	  -- used in dim_costcenterid
	   QALS_PAENDTERM, QALS_WERK, -- used in dim_dateidinspectionend 
	   QALS_PASTRTERM,            -- used in dim_dateidinspectionstart
	   QALS_GUELTIGAB,            -- dim_dateidkeydate
	   QALS_ENSTEHDAT,            -- dim_dateidlotcreated
	   QALS_BUDAT,                -- dim_dateidposting
	   QALS_BLART,        -- dim_documenttypetextid
	   QALS_STAT01,QALS_XCHPF,QALS_HPZ,QALS_STAT19,QALS_STAT20,QALS_INSMK,QALS_STAT14,QALS_STAT34,QALS_STAT35,QALS_KZSKIPLOT,  -- dim_inspectionlotmiscid
	   QALS_HERKUNFT,     -- dim_inspectionlotoriginid
	   QALS_LAGORTVORG,   -- dim_inspectionlotstoragelocationid 
	   QALS_LVS_STIKZ,    -- dim_inspectionsamplestatusid
	   QALS_PRSCHAERFE,   --  dim_inspectionseverityid
	   QALS_PRSTUFE, QALS_DYNREGEL, -- dim_inspectionstageid
	   QALS_ART,         -- dim_inspectiontypeid
	   QALS_PSTYP,	     -- dim_itemcategoryid 
	   QALS_MENGENEINH,  -- dim_lotunitofmeasureid
	   QALS_HERSTELLER,  -- dim_manufacturerid
	   QALS_OBTYP,       -- dim_ObjectCategoryid
	   QALS_EKORG,       -- dim_purchaseorgid
	   QALS_LS_ROUTE,    -- dim_routeid
	   QALS_EINHPROBE,   -- dim_sampleunitofmeasureid
	   QALS_SOBKZ,       --dim_specialstockid
	   QALS_STSMA,       --dim_statusprofileid
	   QALS_LAGORTCHRG,  --dim_storagelocationid
	   QALS_PLNTY,       --dim_tasklisttypeid
	   QALS_PPLVERW,     --dim_tasklistusageid
	   QALS_LIFNR,       --dim_vendorid
	   QALS_LGNUM, --dim_warehousenumberid
	   QALS_PRCTR,QALS_ERSTELDAT  --dim_profitcenterid 

	   
	   
FROM tmp_ins2_inslot q
INNER JOIN dim_plant dp
           ON dp.PlantCode = q.QALS_WERK AND dp.RowIsCurrent = 1
INNER JOIN dim_part dpa
           ON     dpa.PartNumber = q.QALS_MATNR
           AND dpa.Plant = dp.PlantCode
           AND dpa.RowIsCurrent = 1;	  
		  
UPDATE 	tmp_pre_fact_inspectionlot f
SET f.Dim_AccountCategoryid = ac.dim_accountcategoryid 
FROM tmp_pre_fact_inspectionlot f, 
     dim_accountcategory ac
WHERE     ac.Category = ifnull(f.QALS_KNTTP, 'Not Set')
      AND ac.RowIsCurrent = 1
	  AND f.Dim_AccountCategoryid <> ac.dim_accountcategoryid;
	 
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_costcenterid = cc.dim_costcenterid
FROM dim_costcenter cc, tmp_pre_fact_inspectionlot f
WHERE     cc.Code = ifnull(f.QALS_KOSTL, 'Not Set')
      AND cc.ControllingArea = ifnull(f.QALS_KOKRS, 'Not Set')
      AND cc.RowIsCurrent = 1
      AND f.dim_costcenterid <> cc.dim_costcenterid;

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_dateidinspectionend = ie.dim_dateid
FROM tmp_pre_fact_inspectionlot f, dim_date ie, dim_plant dp
WHERE    ie.DateValue = f.QALS_PAENDTERM
     AND ie.CompanyCode = dp.CompanyCode
     AND f.QALS_PAENDTERM IS NOT NULL
     AND dp.PlantCode = ifnull(f.QALS_WERK,'Not Set')
     AND f.dim_dateidinspectionend <> ie.dim_dateid;
	 
	 
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_dateidinspectionstart = dis.dim_dateid
FROM tmp_pre_fact_inspectionlot f, dim_date dis, dim_plant dp
WHERE     dis.DateValue = f.QALS_PASTRTERM
      AND dis.CompanyCode = dp.CompanyCode
      AND f.QALS_PASTRTERM IS NOT NULL
      AND dp.PlantCode = f.QALS_WERK
      AND f.dim_dateidinspectionstart <> dis.dim_dateid;
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_dateidkeydate = kd.dim_dateid
FROM tmp_pre_fact_inspectionlot f, dim_date kd, dim_plant dp
WHERE     kd.DateValue = f.QALS_GUELTIGAB
      AND kd.CompanyCode = dp.CompanyCode
      AND f.QALS_GUELTIGAB IS NOT NULL
      AND dp.PlantCode = f.QALS_WERK
      AND f.dim_dateidkeydate <> kd.dim_dateid;
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_dateidlotcreated = lcd.dim_dateid
FROM  tmp_pre_fact_inspectionlot f, dim_date lcd,dim_plant dp
WHERE lcd.DateValue = f.QALS_ENSTEHDAT
      AND lcd.CompanyCode = dp.CompanyCode
      AND f.QALS_ENSTEHDAT IS NOT NULL
      AND dp.PlantCode = f.QALS_WERK
      AND f.dim_dateidlotcreated <> lcd.dim_dateid;
	  
 UPDATE tmp_pre_fact_inspectionlot f
 SET f.dim_dateidposting = pd.dim_dateid
 FROM tmp_pre_fact_inspectionlot f, dim_date pd,dim_plant dp
 WHERE     pd.DateValue = f.QALS_BUDAT
       AND pd.companycode = dp.CompanyCode
       AND f.QALS_BUDAT IS NOT NULL
       AND dp.PlantCode = f.QALS_WERK
       AND f.dim_dateidposting <> pd.dim_dateid;
	   
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_documenttypetextid = dtt.dim_documenttypetextid
FROM tmp_pre_fact_inspectionlot f,dim_documenttypetext dtt
WHERE     dtt.type = ifnull(f.QALS_BLART, 'Not Set')
      AND dtt.RowIsCurrent = 1 
      AND f.dim_documenttypetextid <> dtt.dim_documenttypetextid;
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionlotmiscid = dins.dim_inspectionlotmiscid
FROM tmp_pre_fact_inspectionlot f, dim_inspectionlotmisc dins
WHERE     dins.AutomaticInspectionLot = ifnull(f.QALS_STAT01, 'Not Set')
      AND dins.BatchManagementRequired = ifnull(f.QALS_XCHPF, 'Not Set')
      AND dins.CompleteInspection = ifnull(f.QALS_HPZ, 'Not Set')
      AND dins.DocumentationRequired = ifnull(f.QALS_STAT19, 'Not Set')
      AND dins.InspectionPlanRequired = ifnull(f.QALS_STAT20, 'Not Set')
      AND dins.InspectionStockQuantity = ifnull(f.QALS_INSMK, 'Not Set')
      AND dins.ShotTermInspectionComplete = ifnull(f.QALS_STAT14, 'Not Set')
      AND dins.StockPostingsComplete = ifnull(f.QALS_STAT34, 'Not Set')
      AND dins.UsageDecisionMade = ifnull(f.QALS_STAT35, 'Not Set')
      AND dins.LotSkipped = ifnull(f.QALS_KZSKIPLOT, 'Not Set')
	  AND f.dim_inspectionlotmiscid <> dins.dim_inspectionlotmiscid;  
	  
	  

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionlotoriginid = ilo.dim_inspectionlotoriginid
FROM dim_inspectionlotorigin ilo, tmp_pre_fact_inspectionlot f
WHERE     ilo.InspectionLotOriginCode = ifnull(f.QALS_HERKUNFT, 'Not Set')
      AND ilo.RowIsCurrent = 1
	  AND f.dim_inspectionlotoriginid <> ilo.dim_inspectionlotoriginid; 
	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionlotstoragelocationid = isl.dim_storagelocationid
FROM tmp_pre_fact_inspectionlot f, dim_storagelocation isl,dim_plant dp
WHERE     isl.LocationCode = f.QALS_LAGORTVORG
      AND isl.RowIsCurrent = 1
      AND isl.Plant = dp.PlantCode
      AND f.QALS_LAGORTVORG IS NOT NULL
      AND dp.PlantCode = f.QALS_WERK
      AND f.dim_inspectionlotstoragelocationid <> isl.dim_storagelocationid;	  
	  
	
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionsamplestatusid = iss.dim_inspectionsamplestatusid
FROM dim_inspectionsamplestatus iss,tmp_pre_fact_inspectionlot f
WHERE     iss.statuscode = ifnull(f.QALS_LVS_STIKZ, 'Not Set')
      AND iss.RowIsCurrent = 1
      AND f.dim_inspectionsamplestatusid <> iss.dim_inspectionsamplestatusid;
     
	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionseverityid = isvr.dim_inspectionseverityid
FROM dim_inspectionseverity isvr, tmp_pre_fact_inspectionlot f
WHERE     isvr.InspectionSeverityCode = f.QALS_PRSCHAERFE
      AND isvr.RowIsCurrent = 1
      AND f.dim_inspectionseverityid <> isvr.dim_inspectionseverityid;
       

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionstageid = istg.dim_inspectionstageid
FROM dim_inspectionstage istg, tmp_pre_fact_inspectionlot f
 WHERE     istg.InspectionStageCode = f.QALS_PRSTUFE
       AND istg.InspectionStageRuleCode = ifnull(f.QALS_DYNREGEL, 'Not Set')
       AND istg.RowIsCurrent = 1
       AND f.dim_inspectionstageid <> istg.dim_inspectionstageid;	  
	   

	   
	   
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectiontypeid = ityp.dim_inspectiontypeid
FROM dim_inspectiontype ityp, tmp_pre_fact_inspectionlot f
WHERE     ityp.InspectionTypeCode = ifnull(f.QALS_ART, 'Not Set')
      AND ityp.RowIsCurrent = 1
      AND f.dim_inspectiontypeid <> ityp.dim_inspectiontypeid;
	  
	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_itemcategoryid = icc.dim_itemcategoryid
FROM dim_itemcategory icc,tmp_pre_fact_inspectionlot f
WHERE     icc.CategoryCode = ifnull(f.QALS_PSTYP, 'Not Set')
      AND icc.RowIsCurrent = 1
      AND f.dim_itemcategoryid <> icc.dim_itemcategoryid;

	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_lotunitofmeasureid = luom.dim_unitofmeasureid
FROM dim_unitofmeasure luom, tmp_pre_fact_inspectionlot f
WHERE     luom.UOM = ifnull(f.QALS_MENGENEINH, 'Not Set')
      AND luom.RowIsCurrent = 1
      AND f.dim_lotunitofmeasureid <> luom.dim_unitofmeasureid;
	
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_manufacturerid = dv.dim_vendorid
FROM dim_vendor dv, tmp_pre_fact_inspectionlot f
WHERE     dv.VendorNumber = ifnull(f.QALS_HERSTELLER, 'Not Set')
      AND dv.RowIsCurrent = 1  
      AND  f.dim_manufacturerid <> dv.dim_vendorid;
	  
	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_ObjectCategoryid = oc.dim_ObjectCategoryid
FROM dim_ObjectCategory oc, tmp_pre_fact_inspectionlot f
WHERE     oc.ObjectCategoryCode = ifnull(f.QALS_OBTYP, 'Not Set')
      AND oc.RowIsCurrent = 1
      AND f.dim_ObjectCategoryid <> oc.dim_ObjectCategoryid;
	    
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_purchaseorgid = po.dim_purchaseorgid
FROM dim_purchaseorg po, tmp_pre_fact_inspectionlot f 
WHERE po.PurchaseOrgCode = ifnull(f.QALS_EKORG, 'Not Set')
      AND po.RowIsCurrent = 1
      AND f.dim_purchaseorgid <> po.dim_purchaseorgid;
	  
	  

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_routeid = r.dim_routeid
FROM tmp_pre_fact_inspectionlot f, dim_route r
WHERE     r.RouteCode = ifnull(f.QALS_LS_ROUTE, 'Not Set')
      AND r.RowIsCurrent = 1
      AND f.dim_routeid <> r.dim_routeid;
	  
	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_sampleunitofmeasureid = uom.dim_unitofmeasureid
FROM dim_unitofmeasure uom, tmp_pre_fact_inspectionlot f 
WHERE      uom.UOM = ifnull(f.QALS_EINHPROBE, 'Not Set')
      AND uom.RowIsCurrent = 1
      AND f.dim_sampleunitofmeasureid <> uom.dim_unitofmeasureid;
	 

	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_specialstockid = ss.dim_specialstockid
FROM  dim_specialstock ss, tmp_pre_fact_inspectionlot f
WHERE     ss.specialstockindicator = ifnull(f.QALS_SOBKZ, 'Not Set')
      AND ss.RowIsCurrent = 1
      AND f.dim_specialstockid <> ss.dim_specialstockid; 
	
	 
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_statusprofileid = sp.dim_statusprofileid
FROM dim_statusprofile sp , tmp_pre_fact_inspectionlot f
 WHERE    sp.StatusProfileCode = ifnull(f.QALS_STSMA, 'Not Set')
      AND sp.RowIsCurrent = 1
      AND f.dim_statusprofileid <> sp.dim_statusprofileid;
	    
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_storagelocationid = sl.dim_storagelocationid
FROM dim_storagelocation sl, tmp_pre_fact_inspectionlot f, dim_plant dp
WHERE    sl.LocationCode = ifnull(f.QALS_LAGORTCHRG, 'Not Set')
     AND sl.Plant = dp.PlantCode
     AND sl.RowIsCurrent = 1
     AND f.QALS_LAGORTCHRG IS NOT NULL
     AND dp.PlantCode = f.QALS_WERK
     AND f.dim_storagelocationid <> sl.dim_storagelocationid;
	 
	 
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_tasklisttypeid = tlt.dim_tasklisttypeid
FROM dim_tasklisttype tlt,tmp_pre_fact_inspectionlot f
WHERE     tlt.TaskListTypeCode = ifnull(f.QALS_PLNTY, 'Not Set')
      AND tlt.RowIsCurrent = 1
      AND f.dim_tasklisttypeid <> tlt.dim_tasklisttypeid;
	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_tasklistusageid = tlu.dim_tasklistusageid
FROM dim_tasklistusage tlu,tmp_pre_fact_inspectionlot f
WHERE     tlu.UsageCode = ifnull(f.QALS_PPLVERW, 'Not Set')
      AND tlu.RowIsCurrent = 1
      AND f.dim_tasklistusageid <> tlu.dim_tasklistusageid;
	  
	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_vendorid  = dv1.dim_vendorid
FROM dim_vendor dv1, tmp_pre_fact_inspectionlot f
WHERE    dv1.VendorNumber = ifnull(f.QALS_LIFNR, 'Not Set')
     AND dv1.RowIsCurrent = 1
     AND f.dim_vendorid  <> dv1.dim_vendorid;
	
	 
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_warehousenumberid = wn.dim_warehouseid
FROM dim_warehousenumber wn, tmp_pre_fact_inspectionlot f
WHERE     wn.WarehouseCode = ifnull(f.QALS_LGNUM, 'Not Set')
      AND wn.RowIsCurrent = 1
      AND f.dim_warehousenumberid <> wn.dim_warehouseid;
	  
	  
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_controllingareaid = ca.dim_controllingareaid
FROM dim_controllingarea ca,tmp_pre_fact_inspectionlot f
WHERE    ca.ControllingAreaCode = f.QALS_KOKRS
     AND f.dim_controllingareaid <> ca.dim_controllingareaid;
	 
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_profitcenterid = pc.dim_profitcenterid 
FROM tmp_pre_fact_inspectionlot f, tmp_dim_profitcenter2 pc 
WHERE      pc.ControllingArea = f.QALS_KOKRS 
	   AND pc.ProfitCenterCode = f.QALS_PRCTR 
       AND pc.QALS_ERSTELDAT = f.QALS_ERSTELDAT
       AND f.dim_profitcenterid <> pc.dim_profitcenterid ;

	  

	
DROP TABLE IF EXISTS tmp_del1_inslot;
DROP TABLE IF EXISTS tmp_ins1_inslot;
DROP TABLE IF EXISTS tmp_ins2_inslot;
DROP TABLE IF EXISTS tmp_dim_profitcenter2;
DROP TABLE IF EXISTS tmp_dim_profitcenter;

	



INSERT INTO fact_inspectionlot(fact_inspectionlotid,
                               ct_actualinspectedqty,
                               ct_actuallotqty,
                               ct_blockedqty,
                               ct_inspectionlotqty,
                               ct_postedqty,
                               ct_qtydefective,
                               ct_qtyreturned,
                               ct_reserveqty,
                               ct_sampleqty,
                               ct_samplesizeqty,
                               ct_scrapqty,
                               ct_unrestrictedqty,
                               dd_ObjectNumber,
                               dd_batchno,
                               dd_changedby,
                               dd_createdby,
                               dd_documentitemno,
                               dd_documentno,
                               dd_inspectionlotno,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocNo,
                               dd_MaterialDocYear,
                               dd_orderno,
                               dd_ScheduleNo,
                               Dim_AccountCategoryid,
                               dim_costcenterid,
                               dim_dateidinspectionend,
                               dim_dateidinspectionstart,
                               dim_dateidkeydate,
                               dim_dateidlotcreated,
                               dim_dateidposting,
                               dim_documenttypetextid,
                               dim_inspectionlotmiscid,
                               dim_inspectionlotoriginid,
                               dim_inspectionlotstoragelocationid,
                               dim_inspectionsamplestatusid,
                               dim_inspectionseverityid,
                               dim_inspectionstageid,
                               dim_inspectiontypeid,
                               dim_itemcategoryid,
                               dim_lotunitofmeasureid,
                               dim_manufacturerid,
                               dim_ObjectCategoryid,
                               dim_partid,
                               dim_plantid,
                               dim_purchaseorgid,
                               dim_routeid,
                               dim_sampleunitofmeasureid,
                               dim_specialstockid,
                               dim_statusprofileid,
                               dim_storagelocationid,
                               dim_tasklisttypeid,
                               dim_tasklistusageid,
                               dim_vendorid,
                               dim_warehousenumberid,
                               dim_Controllingareaid,
                               dim_profitcenterid)
							   
SELECT  fact_inspectionlotid,
        ifnull(ct_actualinspectedqty,0),
        ifnull(ct_actuallotqty,0),
        ifnull(ct_blockedqty,0),
        ifnull(ct_inspectionlotqty,0),
        ifnull(ct_postedqty,0),
        ifnull(ct_qtydefective,0),
        ifnull(ct_qtyreturned,0),
        ifnull(ct_reserveqty,0),
        ifnull(ct_sampleqty,0),
        ifnull(ct_samplesizeqty,0),
        ifnull(ct_scrapqty,0),
        ifnull(ct_unrestrictedqty,0),
        ifnull(dd_ObjectNumber, 'Not Set'),
        ifnull(dd_batchno, 'Not Set'),
        ifnull(dd_changedby, 'Not Set'),
        ifnull(dd_createdby, 'Not Set'),
        ifnull(dd_documentitemno, 0),
        ifnull(dd_documentno, 'Not Set'),
        ifnull(dd_inspectionlotno, '0'),
        ifnull(dd_MaterialDocItemNo, 0),
        ifnull(dd_MaterialDocNo, 'Not Set'),
        ifnull(dd_MaterialDocYear, 0),
        ifnull(dd_orderno, 'Not Set'),
        ifnull(dd_ScheduleNo, 0),
        ifnull(Dim_AccountCategoryid,1),
        ifnull(dim_costcenterid,1),
        ifnull(dim_dateidinspectionend,1),
        ifnull(dim_dateidinspectionstart,1),
        ifnull(dim_dateidkeydate,1),
        ifnull(dim_dateidlotcreated,1),
        ifnull(dim_dateidposting,1),
        ifnull(dim_documenttypetextid,1),
        ifnull(dim_inspectionlotmiscid,1),
        ifnull(dim_inspectionlotoriginid,1),
        ifnull(dim_inspectionlotstoragelocationid,1),
        ifnull(dim_inspectionsamplestatusid,1),
        ifnull(dim_inspectionseverityid,1),
        ifnull(dim_inspectionstageid,1),
        ifnull(dim_inspectiontypeid,1),
        ifnull(dim_itemcategoryid,1),
        ifnull(dim_lotunitofmeasureid,1),
        ifnull(dim_manufacturerid,1),
        ifnull(dim_ObjectCategoryid,1),
        ifnull(dim_partid,1),
        ifnull(dim_plantid,1),
        ifnull(dim_purchaseorgid,1),
        ifnull(dim_routeid,1),
        ifnull(dim_sampleunitofmeasureid,1),
        ifnull(dim_specialstockid,1),
        ifnull(dim_statusprofileid,1),
        ifnull(dim_storagelocationid,1),
        ifnull(dim_tasklisttypeid,1),
        ifnull(dim_tasklistusageid,1),
        ifnull(dim_vendorid,1),
        ifnull(dim_warehousenumberid,1),
        ifnull(dim_Controllingareaid,1),
        ifnull(dim_profitcenterid,1)
FROM tmp_pre_fact_inspectionlot;

DROP TABLE IF EXISTS tmp_pre_fact_inspectionlot;


/* Begin - QMAT Extraction */

update fact_inspectionlot f_il
set f_il.ct_AverageInspectionDuration = ifnull(qm.QMAT_MPDAU,0)
    ,dw_update_date = current_timestamp 
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and ifnull(f_il.ct_AverageInspectionDuration,-1) <> ifnull(qm.QMAT_MPDAU,0);

update fact_inspectionlot f_il
set f_il.dd_posttoinspstock = ifnull(qm.QMAT_INSMK,'Not Set')
    ,dw_update_date = current_timestamp 
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_posttoinspstock <> ifnull(qm.QMAT_INSMK,'Not Set');

update fact_inspectionlot f_il
set f_il.dd_skipsallowed = ifnull(qm.QMAT_DYN,'Not Set')
    ,dw_update_date = current_timestamp 
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_skipsallowed <> ifnull(qm.QMAT_DYN,'Not Set');

update fact_inspectionlot f_il
set f_il.dd_insptypematerialisactive = ifnull(qm.QMAT_AKTIV,'Not Set')
    ,dw_update_date = current_timestamp 
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_insptypematerialisactive <> ifnull(qm.QMAT_AKTIV,'Not Set');

update fact_inspectionlot f_il
set f_il.dd_controlofinsplotcreation = ifnull(qm.QMAT_CHG,'Not Set')
    ,dw_update_date = current_timestamp 
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_controlofinsplotcreation <> ifnull(qm.QMAT_CHG,'Not Set');

update fact_inspectionlot f_il
set f_il.dd_prefferedinsptype = ifnull(qm.QMAT_APA,'Not Set')
    ,dw_update_date = current_timestamp 
from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_prefferedinsptype <> ifnull(qm.QMAT_APA,'Not Set');

/* End - QMAT Extraction  */


/* Start QAMR Extraction */

-- QAMR - CharacteristicResultsDuringInspectionProcessing
-- QAMR_PRUEFDATUV - Inspection Start Date
-- QAMR_PRUEFDATUB - End Date of the Inspection


drop table if exists tmp_qamr_inspectionlot_data;
create table tmp_qamr_inspectionlot_data as 
select qamr_prueflos as dd_inspectionlotno, min(qamr_pruefdatuv) dd_charresultinsprocstartdate, max(qamr_pruefdatub) dd_charresultinsprocpenddate 
from qamr 
group by qamr_prueflos;

update fact_inspectionlot f_il
set f_il.dd_charresultinsprocstartdate = ifnull(q.dd_charresultinsprocstartdate,'0001-01-01'), dw_update_date = current_timestamp 
from fact_inspectionlot f_il, tmp_qamr_inspectionlot_data q
where f_il.dd_inspectionlotno = q.dd_inspectionlotno
and f_il.dd_charresultinsprocstartdate <> ifnull(q.dd_charresultinsprocstartdate,'0001-01-01');

update fact_inspectionlot f_il
set f_il.dd_charresultinsprocpenddate = ifnull(q.dd_charresultinsprocpenddate,'0001-01-01'), dw_update_date = current_timestamp 
from fact_inspectionlot f_il, tmp_qamr_inspectionlot_data q
where f_il.dd_inspectionlotno = q.dd_inspectionlotno
and f_il.dd_charresultinsprocpenddate <> ifnull(q.dd_charresultinsprocpenddate,'0001-01-01');

update fact_inspectionlot f
set f.dim_dateidcrdipstart = ifnull(pd.dim_dateid, 1)
from fact_inspectionlot f, dim_plant dp, dim_date pd
where f.dim_plantid = dp.dim_plantid
and pd.datevalue = f.dd_charresultinsprocstartdate
and pd.companycode = dp.companycode
and f.dim_dateidcrdipstart <> ifnull(pd.dim_dateid, 1);

update fact_inspectionlot f
set f.dim_dateidcrdipend = ifnull(pd.dim_dateid, 1)
from fact_inspectionlot f, dim_plant dp, dim_date pd
where f.dim_plantid = dp.dim_plantid
and pd.datevalue = f.dd_charresultinsprocpenddate
and pd.companycode = dp.companycode
and f.dim_dateidcrdipend <> ifnull(pd.dim_dateid, 1);

drop table if exists tmp_qamr_inspectionlot_data;

/* End QAMR Extraction */



/* Start QPRN Extraction */

drop table if exists tmp_qprn_inspectionlot_data;
create table tmp_qprn_inspectionlot_data as 
select qprn_plos as dd_inspectionlotno, qprn_matnr as partnumber, qprn_werks as plantcode, qprn_charg as dd_batchno,
	   min(qprn_anldt) dd_sampdrawphyscreatedate, min(qprn_frgdt) dd_sampdrawphysreleasedate 
from qprn 
group by qprn_plos, qprn_matnr, qprn_werks, qprn_charg;

update fact_inspectionlot f_il
set f_il.dd_sampdrawphyscreatedate = ifnull(q.dd_sampdrawphyscreatedate,'0001-01-01'), dw_update_date = current_timestamp 
from fact_inspectionlot f_il, dim_part p, tmp_qprn_inspectionlot_data q
where f_il.dim_partid = p.dim_partid
and f_il.dd_inspectionlotno = q.dd_inspectionlotno
and p.partnumber = q.partnumber and p.plant = q.plantcode and f_il.dd_batchno = q.dd_batchno
and f_il.dd_sampdrawphyscreatedate <> ifnull(q.dd_sampdrawphyscreatedate,'0001-01-01');

update fact_inspectionlot f_il
set f_il.dd_sampdrawphysreleasedate = ifnull(q.dd_sampdrawphysreleasedate,'0001-01-01'), dw_update_date = current_timestamp 
from fact_inspectionlot f_il, dim_part p, tmp_qprn_inspectionlot_data q
where f_il.dim_partid = p.dim_partid
and f_il.dd_inspectionlotno = q.dd_inspectionlotno
and p.partnumber = q.partnumber and p.plant = q.plantcode and f_il.dd_batchno = q.dd_batchno
and f_il.dd_sampdrawphysreleasedate <> ifnull(q.dd_sampdrawphysreleasedate,'0001-01-01');

update fact_inspectionlot f
set f.dim_dateidsdopscreate = ifnull(pd.dim_dateid, 1)
from fact_inspectionlot f, dim_plant dp, dim_date pd
where f.dim_plantid = dp.dim_plantid
and pd.datevalue = f.dd_sampdrawphyscreatedate
and pd.companycode = dp.companycode
and f.dim_dateidsdopscreate <> ifnull(pd.dim_dateid, 1);

update fact_inspectionlot f
set f.dim_dateidsdopsrelease = ifnull(pd.dim_dateid, 1)
from fact_inspectionlot f, dim_plant dp, dim_date pd
where f.dim_plantid = dp.dim_plantid
and pd.datevalue = f.dd_sampdrawphysreleasedate
and pd.companycode = dp.companycode
and f.dim_dateidsdopsrelease <> ifnull(pd.dim_dateid, 1);

drop table if exists tmp_qprn_inspectionlot_data;

/* End QPRN Extraction */

/* APP-6336 - Business Line  - Oana 24 May 2017 */
update fact_inspectionlot f
	set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_inspectionlot f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
	and dp.producthierarchy = bw.lowerhierarchycode
	and dp.productgroupsbu = bw.upperhierarchycode
	and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
	and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);

update fact_inspectionlot f
	set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_inspectionlot f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);
/* END APP-6336 - Business Line  - Oana 24 May 2017 */

/* Alex D dim_batch */
update fact_inspectionlot ia
set ia.dim_batchid = ifnull(b.dim_batchid, 1)
from fact_inspectionlot ia,dim_batch b, dim_part dp
 where  ia.dim_partid = dp.dim_partid
	and ia.dd_batchno = b.batchnumber 
    and dp.partnumber = b.partnumber 
    and b.plantcode = dp.plant;

/* Alex D InitialInspectionEndDate - APP-6507 */
drop table if exists tmp_dim_date_buss_day;
create table tmp_dim_date_buss_day
as
select companycode,BUSINESSDAYSSEQNO,max(datevalue) datevalue,cast(1 as bigint) as dim_dateid
from dim_date
group by companycode,BUSINESSDAYSSEQNO;

update tmp_dim_date_buss_day t
set t.dim_dateid = ifnull(dd.dim_dateid, 1)
from tmp_dim_date_buss_day t
inner join dim_date dd on t.companycode = dd.companycode and t.datevalue = dd.datevalue;

update fact_inspectionlot f
set dim_InitialInspectionEndDate = ifnull(dd2.dim_dateid,1)
from fact_inspectionlot f
    inner join dim_date  dd on f.dim_dateidinspectionstart= dd.dim_dateid
    inner join dim_part dim on f.dim_partid = dim.dim_partid
    inner join tmp_dim_date_buss_day dd2 on dd2.companycode = dd.companycode and dd.BUSINESSDAYSSEQNO + GRProcessingTime = dd2.BUSINESSDAYSSEQNO;

update fact_inspectionlot f
	set f.dim_qadecisiondateid = ifnull(dd2.dim_dateid,1)
from fact_inspectionlot f
    inner join dim_date  dd on f.dim_dateidposting= dd.dim_dateid
    inner join dim_part dim on f.dim_partid = dim.dim_partid
    inner join tmp_dim_date_buss_day dd2 on dd2.companycode = dd.companycode and dd.BUSINESSDAYSSEQNO + GRProcessingTime = dd2.BUSINESSDAYSSEQNO;

/* MDG Part */
DROP TABLE IF EXISTS tmp_inspl_mdg;
CREATE TABLE tmp_inspl_mdg as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE md.partnumber = pr.mdm_code_emd
GROUP BY dim_partid;

UPDATE fact_inspectionlot f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM tmp_inspl_mdg tmp, fact_inspectionlot f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

DROP TABLE IF EXISTS tmp_inspl_mdg;

/* BW Country Hierarchy - APP-7497 Oana 21Sept2017 */
update fact_inspectionlot f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_inspectionlot f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-32')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY04' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_inspectionlot f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_inspectionlot f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-31','DIV-35','DIV-34')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY05' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_inspectionlot f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_inspectionlot f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY07' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_inspectionlot f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_inspectionlot f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-87')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY08' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

/* APP-7613 Create Number of Pallets measure - Oana 24Oct2017 */
drop table if exists tmp_insplot_Numberofpallets;
create table tmp_insplot_Numberofpallets as
select LQUA_MATNR partnumber ,LQUA_WERKS plant,count(LQUA_LGPLA) Numberofpallets
from LQUA
group by  LQUA_MATNR,LQUA_WERKS;

update fact_inspectionlot f
set f.ct_Numberofpallets = ifnull(tmp.Numberofpallets, 0)
from fact_inspectionlot f, tmp_insplot_Numberofpallets tmp, dim_part dp, dim_plant dpl
where f.dim_partid = dp.dim_partid
  and f.dim_plantid = dpl.dim_plantid
  and trim(leading '0' from dp.partnumber) = trim(leading'0' from tmp.partnumber)
  and dpl.plantcode = tmp.plant;
/* END APP-7613 Create Number of Pallets measure - Oana 24Oct2017 */
