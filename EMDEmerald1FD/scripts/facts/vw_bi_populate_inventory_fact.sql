/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Lokesh */
/*   Created On     : 19 Aug 2013 */
/*   Description    : Stored Proc bi_populate_inventory_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   19 Aug 2013      Lokesh    1.0               Existing code migrated to Vectorwise.                                       */
/*                                                Run after 1.vw_funct_fiscal_year.inventory_fact_and_process_S031.sql */
/*                                                                                                2.vw_funct_fiscal_year.standard.sql and 3.vw_process_S031.sql    */
/******************************************************************************************************************/

/* Run after process_S031          */

DROP TABLE IF EXISTS tmp_inv_fact_cursor;
CREATE TABLE    tmp_inv_fact_cursor
(
done INT ,
pFiYear INT,
pCompanyCode VARCHAR(4),
pPlantCode VARCHAR(4),
pPeriod INT,
pDates VARCHAR(45),
pDates1 VARCHAR(45),
pFromDate date,
pToDate date,
pToYear INT,
pToMonth INT
);


INSERT INTO tmp_inv_fact_cursor
(pCompanyCode,pPlantCode,pFiYear,pPeriod)
SELECT DISTINCT BUKRS, BWKEY, LFGJA, LFMON
FROM (SELECT b.BUKRS, a.BWKEY, a.LFGJA, a.LFMON FROM MBEW a INNER JOIN T001K b ON a.BWKEY = b.BWKEY
                UNION
          SELECT b.BUKRS, a.BWKEY, a.LFGJA, a.LFMON FROM MBEWH a INNER JOIN T001K b ON a.BWKEY = b.BWKEY) x;


UPDATE tmp_inv_fact_cursor t
SET pDates = z.pReturn,pDates1 = z.pReturn
FROM tmp_inv_fact_cursor t, tmp_funct_fiscal_year z
WHERE z.pCompanyCode = t.pCompanyCode
and z.FiscalYear = t.pFiYear
and z.Period = t.pPeriod
and z.fact_script_name = 'bi_populate_inventory_fact';


UPDATE tmp_inv_fact_cursor t
SET pDates = '01-01-0001|01-01-0001'
where pDates = '|';

  UPDATE tmp_inv_fact_cursor t
  SET pFromDate = to_date(substr(pDates, 1, 10),'YYYY-MM-DD');

  UPDATE tmp_inv_fact_cursor t
  SET pToDate = to_date(substr(pDates, 22, 32),'YYYY-MM-DD');

  UPDATE tmp_inv_fact_cursor t
  set pToYear = Year(pToDate);

  UPDATE tmp_inv_fact_cursor t
  set pToMonth = Month(pToDate);


DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_inventory';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_inventory', ifnull(max(fact_inventoryid),  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) FROM fact_inventory;


  INSERT INTO fact_inventory(Dim_Plantid,
                            Dim_Partid,
                            Dim_producthierarchyid,
                            Dim_Companyid,
                            Dim_DateidRecorded,
                            Dim_DateidRecordedEnd,
							fact_inventoryid)
   SELECT DISTINCT
           Dim_Plantid,
           Dim_Partid,
           convert(bigint,1) dim_producthierarchyid,
           cc.Dim_Companyid,
           ddr.Dim_Dateid,
           ddre.Dim_Dateid,
           (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventory') + row_number() over (order by '')
   FROM 
           tmp_inv_fact_cursor t
	       inner join S031 a on a.SPMON_YEAR = t.pToYear AND a.SPMON_MONTH = t.pToMonth
		   INNER JOIN T001K b ON a.WERKS = b.BWKEY AND b.BUKRS = t.pCompanyCode AND b.BWKEY = t.pPlantCode
           inner join dim_date ddr on ddr.DateValue = t.pFromDate AND b.BUKRS = ddr.companycode              
		   inner join dim_plant pl on b.BWKEY = pl.PlantCode
		   inner join dim_date ddre on ddre.DateValue = t.pToDate AND b.BUKRS = ddre.companycode
           INNER JOIN dim_part p ON    ( a.MATNR = p.PartNumber AND a.WERKS = p.Plant AND a.MATNR IS NOT NULL )
           INNER JOIN dim_company cc ON b.BUKRS = cc.CompanyCode
    WHERE    
           not exists
                 (SELECT 1
                    FROM fact_inventory fi
                   WHERE     fi.Dim_Companyid = cc.Dim_Companyid
                         AND fi.Dim_DateidRecorded = ddr.Dim_Dateid
                         AND ddr.CompanyCode = b.BUKRS
                         AND fi.Dim_DateidRecordedEnd = ddre.Dim_Dateid
                         AND ddre.CompanyCode = b.BUKRS
                         AND fi.Dim_Partid = p.Dim_Partid
                         AND p.Plant = b.BWKEY
                         AND fi.Dim_Plantid = pl.Dim_Plantid);


UPDATE fact_inventory fi
set fi.dim_producthierarchyid = ifnull(dph.dim_producthierarchyid,1)
FROM
fact_inventory fi
INNER JOIN dim_part p ON    ( p.dim_partid = fi.Dim_Partid )
LEFT  JOIN dim_producthierarchy dph ON (dph.ProductHierarchy = p.ProductHierarchy)
WHERE dph.RowIsCurrent = 1
AND fi.dim_producthierarchyid <> ifnull(dph.dim_producthierarchyid,1);


DROP TABLE IF EXISTS tmp_inv_fact_cursor;

/* MDG Part */

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_INV;
CREATE TABLE TMP_DIM_MDG_PARTID_INV as
SELECT DISTINCT pr.dim_partid, max(md.dim_mdg_partid) dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
group by pr.dim_partid;

UPDATE fact_inventory f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM TMP_DIM_MDG_PARTID_INV tmp, fact_inventory f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_INV;
