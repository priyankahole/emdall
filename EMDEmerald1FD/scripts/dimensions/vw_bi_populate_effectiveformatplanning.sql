UPDATE    dim_effectiveformatplanning emp
   SET emp.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			emp.dw_update_date = current_timestamp
       FROM
          dim_effectiveformatplanning emp, DD07T t
 WHERE emp.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'NO_DISP_PLUS'
          AND emp.EffectiveForMatPlanning = ifnull(t.DD07T_DOMVALUE, 'Not Set');
		  
INSERT INTO dim_effectiveformatplanning(dim_effectiveformatplanningid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_effectiveformatplanning
               WHERE dim_effectiveformatplanningid = 1);

delete from number_fountain m where m.table_name = 'dim_effectiveformatplanning';
   
insert into number_fountain
select 	'dim_effectiveformatplanning',
	ifnull(max(d.dim_EffectiveForMatPlanningid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_effectiveformatplanning d
where d.dim_EffectiveForMatPlanningid <> 1; 

INSERT INTO dim_effectiveformatplanning(dim_EffectiveForMatPlanningid,
                          Description,
                          EffectiveForMatPlanning,
                          RowStartDate,
                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_effectiveformatplanning')
          + row_number() over(order by '') ,
                         ifnull(DD07T_DDTEXT, 'Not Set'),
            ifnull(DD07T_DOMVALUE,'Not Set'),
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'NO_DISP_PLUS'
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_effectiveformatplanning
                    WHERE EffectiveForMatPlanning = ifnull(DD07T_DOMVALUE, 'Not Set')
                          AND DD07T_DOMNAME = 'NO_DISP_PLUS')
   ORDER BY 2;
   
/* Expression test  */

update dim_effectiveformatplanning
set EffectiveForMatPlanning2 = case when EffectiveForMatPlanning = '1' then '1' when EffectiveForMatPlanning = 'X' then '2' else '3' end
where EffectiveForMatPlanning2 <> case when EffectiveForMatPlanning = '1' then '1' when EffectiveForMatPlanning = 'X' then '2' else '3' end;  

