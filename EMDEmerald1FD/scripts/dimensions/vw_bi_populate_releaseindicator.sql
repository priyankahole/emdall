UPDATE    
dim_ReleaseIndicator ri
SET ri.ReleaseIndicator = ifnull(t.T16FE_FRGET, 'Not Set'),
	ri.dw_update_date = current_timestamp
FROM
dim_ReleaseIndicator ri,
T16FE t
WHERE ri.RowIsCurrent = 1
AND t.T16FE_FRGKE = ri.ReleaseIndicatorCode;

INSERT INTO dim_ReleaseIndicator(dim_ReleaseIndicatorId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_ReleaseIndicator
               WHERE dim_ReleaseIndicatorId = 1);

delete from number_fountain m where m.table_name = 'dim_ReleaseIndicator';

insert into number_fountain
select 	'dim_ReleaseIndicator',
	ifnull(max(d.dim_ReleaseIndicatorid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ReleaseIndicator d
where d.dim_ReleaseIndicatorid <> 1; 

INSERT INTO dim_ReleaseIndicator(dim_ReleaseIndicatorid,
								ReleaseIndicator,
								ReleaseIndicatorCode,
								RowStartDate,
								RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_ReleaseIndicator') 
          + row_number() over(order by ''),
		 ifnull(t.T16FE_FRGET, 'Not Set'),
		 ifnull(t.T16FE_FRGKE, 'Not Set'),
        current_timestamp,
         1
     FROM T16FE t
    WHERE t.T16FE_FRGET IS NOT NULL
          AND NOT EXISTS
                 (SELECT 1
                    FROM dim_ReleaseIndicator ri
                   WHERE ri.ReleaseIndicatorCode = t.T16FE_FRGKE);

