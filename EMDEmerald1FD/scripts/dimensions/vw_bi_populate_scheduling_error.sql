INSERT INTO dim_schedulingerror(dim_schedulingerrorid,
                                SchedulingErrorCode,
                                Description,
                                RowStartDate,
                                RowEndDate,
                                RowIsCurrent,
                                RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          current_timestamp,
          NULL,
          1,
          NULL
     FROM (select 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_schedulingerror
               WHERE dim_schedulingerrorid = 1);

UPDATE    dim_schedulingerror se
   SET se.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			se.dw_update_date = current_timestamp
       FROM dim_schedulingerror se,
          DD07T t
 WHERE se.RowIsCurrent = 1
       AND t.DD07T_DOMNAME = 'TRMER'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND se.SchedulingErrorCode = t.DD07T_DOMVALUE;

delete from number_fountain m where m.table_name = 'dim_schedulingerror';

insert into number_fountain
select 	'dim_schedulingerror',
	ifnull(max(d.dim_schedulingerrorid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_schedulingerror d
where d.dim_schedulingerrorid <> 1; 		  

INSERT INTO dim_schedulingerror(dim_schedulingerrorid,
                           Description,
                           SchedulingErrorCode,
                           RowStartDate,
                           RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_schedulingerror') 
          + row_number() over(order by ''),
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'TRMER' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_schedulingerror
                    WHERE SchedulingErrorCode = DD07T_DOMVALUE AND DD07T_DOMNAME = 'TRMER')
   ORDER BY 2 ;