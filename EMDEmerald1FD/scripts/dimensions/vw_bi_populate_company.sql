DELETE FROM adrc where 1=1;
insert into adrc
(
ADRC_NAME1,ADRC_NAME2,ADRC_STREET,ADRC_HOUSE_NUM1,ADRC_CITY1,ADRC_REGION,ADRC_POST_CODE1,ADRC_COUNTRY,ADRC_TEL_NUMBER,ADRC_ADDRNUMBER,ADRC_FAX_NUMBER,ADRC_PO_BOX,ADRC_CITY2,ADRC_POST_CODE2,ADRC_LANGU
)
select 
ADRC_NAME1,ADRC_NAME2,ADRC_STREET,ADRC_HOUSE_NUM1,ADRC_CITY1,ADRC_REGION,ADRC_POST_CODE1,ADRC_COUNTRY,ADRC_TEL_NUMBER,ADRC_ADDRNUMBER,ADRC_FAX_NUMBER,ADRC_PO_BOX,ADRC_CITY2,ADRC_POST_CODE2,ADRC_LANGU
FROM ADRC_LFA1_LFM1;

insert into adrc
(
ADRC_NAME1,ADRC_NAME2,ADRC_STREET,ADRC_HOUSE_NUM1,ADRC_CITY1,ADRC_REGION,ADRC_POST_CODE1,ADRC_COUNTRY,ADRC_TEL_NUMBER,ADRC_ADDRNUMBER,ADRC_FAX_NUMBER,ADRC_PO_BOX,ADRC_CITY2,ADRC_POST_CODE2,ADRC_LANGU
)
select 
ADRC_NAME1,ADRC_NAME2,ADRC_STREET,ADRC_HOUSE_NUM1,ADRC_CITY1,ADRC_REGION,ADRC_POST_CODE1,ADRC_COUNTRY,ADRC_TEL_NUMBER,ADRC_ADDRNUMBER,ADRC_FAX_NUMBER,ADRC_PO_BOX,ADRC_CITY2,ADRC_POST_CODE2,ADRC_LANGU
FROM ADRC_KNA1_KNB1;


insert into dim_company(Dim_Companyid,CompanyCode,CompanyName,PostalCode,City,"state",Country,Currency,Language,FiscalYearKey,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason,ChartOfAccounts,Region,CountryName) 
SELECT 1,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',current_timestamp,null,1,null,'Not Set','Not Set','Not Set'
FROM (SELECT 1) a
where not exists(select 1 from dim_company where dim_companyid = 1);

/* Commenting mysql code for harmonization
-- Interface Populate Company Code MySQL 

UPDATE dim_company dc
  INNER JOIN t001 t ON t.bukrs = dc.CompanyCode and dc.RowIsCurrent = 1
  LEFT JOIN adrc ON t.t001_adrnr = adrc_addrnumber
  INNER JOIN t005t on t.land1 = t005t_land1
SET dc.CompanyName = butxt,
    dc.PostalCode =  ifnull(adrc_post_code1, 'Not Set'),
    dc.City = ifnull(adrc_city1, 'Not Set'),
    dc.State = ifnull(adrc_region, 'Not Set'),
    dc.Country = ifnull(land1, 'Not Set'),
    dc.CountryName = ifnull(t005t_landx, 'Not Set'),
    dc.Currency = waers,
    dc.ChartOfAccounts =  ktopl,
    dc.Language = spras,
    dc.FiscalYearKey = ifnull(periv, 'Not Set'),
	dc.dw_update_date = current_timestamp
	
INSERT INTO dim_company(CompanyCode,
                        CompanyName,
                        PostalCode,
                        City,
                        State,
                        Country,
                        Currency,
			ChartOfAccounts,
                        Language,
                        FiscalYearKey,
                        RowStartDate,
                        RowIsCurrent,
			CountryName)
   SELECT distinct bukrs,
          butxt,
          ifnull(adrc_post_code1, 'Not Set'),
          ifnull(adrc_city1, 'Not Set'),
          ifnull(adrc_region, 'Not Set'),
          ifnull(land1, 'Not Set'),
          waers,
          ktopl,
          spras,
          ifnull(periv, 'Not Set'),
          current_timestamp,
          1,
	  ifnull(t005t_landx, 'Not Set')
     FROM t001 t LEFT JOIN adrc
             ON t.t001_adrnr = adrc_addrnumber and t.SPRAS = ADRC_LANGU
	  INNER JOIN t005t on t.land1 = t005t_land1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_company
                       WHERE CompanyCode = t.bukrs)

UPDATE dim_company d, country_region_mapping s
   SET d.region = s.region,
		d.dw_update_date = current_timestamp
 WHERE s.countrycode = d.country
       AND ifnull(s.region, 'Not Set') <> ifnull(d.region, 'Not Set')
	   
UPDATE dim_company t
   SET t.region = 'Not Set',
		t.dw_update_date = current_timestamp
 WHERE t.region IS NULL
 
*/

 
/* Interface Populate Company Code VW */
 
UPDATE dim_company dc
SET dc.CompanyName = ifnull(butxt,'Not Set'),
    dc.PostalCode =  ifnull(adrc_post_code1, 'Not Set'),
    dc.City = ifnull(adrc_city1, 'Not Set'),
    dc."state" = ifnull(adrc_region, 'Not Set'),
    dc.Country = ifnull(land1, 'Not Set'),
    dc.CountryName = ifnull(t005t_landx, 'Not Set'),
    dc.Currency = waers,
    dc.ChartOfAccounts =  ifnull(ktopl, 'Not Set'),
    dc.Language = spras,
    dc.FiscalYearKey = ifnull(periv, 'Not Set'),
	dc.dw_update_date = current_timestamp
FROM dim_company dc  
	inner join t001 t on t.bukrs = dc.CompanyCode
           LEFT JOIN adrc x ON t.t001_adrnr = x.adrc_addrnumber
  	   INNER JOIN t005t y on t.land1 = y.t005t_land1
	WHERE   dc.RowIsCurrent = 1;
	
delete from number_fountain m where m.table_name = 'dim_company';

insert into number_fountain
select 	'dim_company',
	ifnull(max(d.Dim_Companyid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_company d
where d.Dim_Companyid <> 1;

	
INSERT INTO dim_company(Dim_Companyid,
			CompanyCode,
                        CompanyName,
                        PostalCode,
                        City,
                        "state",
                        Country,
                        Currency,
			            ChartOfAccounts,
                        Language,
                        FiscalYearKey,
                        RowStartDate,
                        RowIsCurrent,
			CountryName)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_company') 
          + row_number() over(order by '') ,
			bukrs,
          butxt,
          ifnull(adrc_post_code1, 'Not Set'),
          ifnull(adrc_city1, 'Not Set'),
          ifnull(adrc_region, 'Not Set'),
          ifnull(land1, 'Not Set'),
          waers,
          IFNULL(ktopl, 'Not Set'),
          spras,
          ifnull(periv, 'Not Set'),
          current_timestamp,
          1,
	  ifnull(t005t_landx, 'Not Set')
     FROM t001 t LEFT JOIN adrc
             ON t.t001_adrnr = adrc_addrnumber and t.SPRAS = ADRC_LANGU
	  INNER JOIN t005t on t.land1 = t005t_land1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_company
                       WHERE CompanyCode = t.bukrs);

delete from number_fountain m where m.table_name = 'dim_company';
					   
UPDATE dim_company d
SET d.region = s.region,
		d.dw_update_date = current_timestamp
from country_region_mapping s,dim_company d
WHERE 
s.countrycode = d.country
       AND 
ifnull(s.region, 'Not Set') <> ifnull(d.region, 'Not Set');

UPDATE dim_company t
   SET t.region = 'Not Set',
		t.dw_update_date = current_timestamp
 WHERE t.region IS NULL;
 
UPDATE dim_company c
SET c.company = ifnull(t.RCOMP, 'Not Set')
FROM t001 t, dim_company c
WHERE t.bukrs = c.CompanyCode
      AND c.RowIsCurrent = 1
	  AND c.company <> ifnull(t.RCOMP, 'Not Set');
	  
	  
update dim_company 
set pm_company = ifnull(company,'Not Set')
where pm_company <> ifnull(company,'Not Set');

update dim_company 
set pm_company = ifnull( new_value,'Not Set')
from  EMDTEMPOCC4.company_mapping mp, dim_company  dc
where trim(leading '0' from dc.company) = trim (leading '0' from mp.old_value)
   and pm_company <> ifnull( new_value,'Not Set');

/* Expression test  */

update Dim_Company
set Name2 = case when company != 'Not Set' then company || ' - ' || companyname else companyname end
where Name2 <> case when company != 'Not Set' then company || ' - ' || companyname else companyname end; 