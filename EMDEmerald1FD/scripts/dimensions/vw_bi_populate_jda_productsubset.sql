delete from DMDUNIT;
insert into DMDUNIT(DMDUNIT_DMDUNIT,
DMDUNIT_DESCR,
DMDUNIT_UDC_NEW,
DMDUNIT_UDC_TA,
DMDUNIT_UDC_STRENGTH,
DMDUNIT_UDC_STRENGTHDESCR,
DMDUNIT_UDC_DOSETOMCG,
DMDUNIT_UDC_DOSETOIU,
DMDUNIT_UDC_DMDUNITLEVEL,
DMDUNIT_UDC_BRAND,
DMDUNIT_UDC_BOXTODOSES,
DMDUNIT_UDC_PACKSIZEDESCR,
DMDUNIT_UDC_FAMILYGRPDESCR,
DMDUNIT_UDC_BRANDDESCR,
DMDUNIT_UDC_TADESCR,
DMDUNIT_UDC_LOCALPRODUCTSW,
DMDUNIT_UDC_SIZETOSTRENGTH,
DMDUNIT_UDC_SIZETOBRAND,
DMDUNIT_UDC_LRPDUMMY,
DMDUNIT_UDC_LRPPRODLEVEL,
DMDUNIT_UDC_LRPSCOPE,
DMDUNIT_UDC_PHARMAFORM,
DMDUNIT_UDC_CONTAINER,
DMDUNIT_UDC_GPH,
DMDUNIT_UDC_GPHDESCR,
DMDUNIT_UDC_PACKAGINGSTYLE,
DMDUNIT_UDC_ACTIVEINGR,
DMDUNIT_UDC_SUPPLIERNAME,
DMDUNIT_UDC_SOPSW,
DMDUNIT_UDC_PRODCONFIG,
DMDUNIT_UDC_PRODMACROCONFIG,
DMDUNIT_UDC_MACROCONFIGDESCR,
DMDUNIT_UDC_CORPDESCR,
DMDUNIT_UDC_CREATIONDATE,
DMDUNIT_UDC_UPDATEDATE,
DMDUNIT_UDC_DMDUNITEXCEPTION,
DMDUNIT_UDC_PROJECTREF,
DMDUNIT_UDC_GROUPING,
DMDUNIT_UDC_SECPACKPERBOX,
DMDUNIT_UDC_PACKERMFG,
DMDUNIT_UDC_GAUSSGLOBALCODE,
DMDUNIT_PRICELINK,
DMDUNIT_ONORDERQTY,
DMDUNIT_COLLECTION,
DMDUNIT_UDC_BFDP,
DMDUNIT_UDC_GPS,
DMDUNIT_UDC_SUPMFG,
DMDUNIT_UDC_PACKMFG,
DMDUNIT_UDC_ACTIVE,
DMDUNIT_BRAND,
DMDUNIT_HIERARCHYLEVEL,
DMDUNIT_IGNOREPRICINGLVLSW,
DMDUNIT_PACKSIZE,
DMDUNIT_UNITSIZE,
DMDUNIT_UOM,
DMDUNIT_WDDCATEGORY,
DMDUNIT_UDC_GRPDVLP)
select DMDUNIT_DMDUNIT,
	DMDUNIT_DESCR,
	DMDUNIT_UDC_NEW,
	DMDUNIT_UDC_TA,
	DMDUNIT_UDC_STRENGTH,
	DMDUNIT_UDC_STRENGTHDESCR,
	DMDUNIT_UDC_DOSETOMCG,
	DMDUNIT_UDC_DOSETOIU,
	DMDUNIT_UDC_DMDUNITLEVEL,
	DMDUNIT_UDC_BRAND,
	DMDUNIT_UDC_BOXTODOSES,
	DMDUNIT_UDC_PACKSIZEDESCR,
	DMDUNIT_UDC_FAMILYGRPDESCR,
	DMDUNIT_UDC_BRANDDESCR,
	DMDUNIT_UDC_TADESCR,
	DMDUNIT_UDC_LOCALPRODUCTSW,
	DMDUNIT_UDC_SIZETOSTRENGTH,
	DMDUNIT_UDC_SIZETOBRAND,
	DMDUNIT_UDC_LRPDUMMY,
	DMDUNIT_UDC_LRPPRODLEVEL,
	DMDUNIT_UDC_LRPSCOPE,
	DMDUNIT_UDC_PHARMAFORM,
	DMDUNIT_UDC_CONTAINER,
	DMDUNIT_UDC_GPH,
	DMDUNIT_UDC_GPHDESCR,
	DMDUNIT_UDC_PACKAGINGSTYLE,
	DMDUNIT_UDC_ACTIVEINGR,
	DMDUNIT_UDC_SUPPLIERNAME,
	DMDUNIT_UDC_SOPSW,
	DMDUNIT_UDC_PRODCONFIG,
	DMDUNIT_UDC_PRODMACROCONFIG,
	DMDUNIT_UDC_MACROCONFIGDESCR,
	DMDUNIT_UDC_CORPDESCR,
	DMDUNIT_UDC_CREATIONDATE,
	DMDUNIT_UDC_UPDATEDATE,
	DMDUNIT_UDC_DMDUNITEXCEPTION,
	DMDUNIT_UDC_PROJECTREF,
	DMDUNIT_UDC_GROUPING,
	DMDUNIT_UDC_SECPACKPERBOX,
	DMDUNIT_UDC_PACKERMFG,
	DMDUNIT_UDC_GAUSSGLOBALCODE,
	DMDUNIT_PRICELINK,
	DMDUNIT_ONORDERQTY,
	DMDUNIT_COLLECTION,
	DMDUNIT_UDC_BFDP,
	DMDUNIT_UDC_GPS,
	DMDUNIT_UDC_SUPMFG,
	DMDUNIT_UDC_PACKMFG,
	DMDUNIT_UDC_ACTIVE,
	DMDUNIT_BRAND,
	DMDUNIT_HIERARCHYLEVEL,
	DMDUNIT_IGNOREPRICINGLVLSW,
	DMDUNIT_PACKSIZE,
	DMDUNIT_UNITSIZE,
	DMDUNIT_UOM,
	DMDUNIT_WDDCATEGORY,
	DMDUNIT_UDC_GRPDVLP
from EMDOTHERDATASOURCESBE3.DMDUNIT;

drop table if exists tmp_dmdunit;
create table tmp_dmdunit as
	select du.dmdunit_dmdunit as itemglobalcode
	,du.dmdunit_udc_container	as container
	,du.dmdunit_udc_strength		as gbu
	,du.DMDUNIT_UDC_GPH				as gph
	,du.DMDUNIT_UDC_GPHDESCR		as gphdescr
	,du.dmdunit_udc_suppliername	as ManufSite
	,du.dmdunit_udc_packermfg		as PackSite
	,du.dmdunit_udc_boxtodoses		as Pack_Size
	,du.dmdunit_udc_pharmaform		as pharmaform
from dmdunit du
where du.dmdunit_dmdunit is not null;

delete from number_fountain m where m.table_name = 'dim_jda_productsubset';
insert into number_fountain
	select 'dim_jda_productsubset', ifnull(max(d.dim_jda_productsubsetid ),
									 ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from dim_jda_productsubset d;

insert into dim_jda_productsubset (DIM_JDA_PRODUCTSUBSETID,ITEMGLOBALCODE,CONTAINER,GBU,GPH,GPHDESCR,MANUFSITE,PACKSITE,PACK_SIZE,PHARMAFORM)
	select (select max_id from number_fountain where table_name = 'dim_jda_productsubset') + row_number() over (order by '') as dim_jda_productsubsetid,
		ifnull(ITEMGLOBALCODE,'Not Set') as itemglobalcode,
		ifnull(CONTAINER,'Not Set') as container,
		ifnull(GBU,'Not Set') as gbu,
		ifnull(GPH,'Not Set') as gph,
		ifnull(GPHDESCR,'Not Set') as gphdescr,
		ifnull(MANUFSITE,'Not Set') as ManufSite,
		ifnull(PACKSITE,'Not Set') as PackSite,
		ifnull(PACK_SIZE,0) as Pack_Size,
		ifnull(PHARMAFORM,'Not Set') as Pharmaform
from tmp_dmdunit t
where not exists (select 'X' from dim_jda_productsubset d where t.itemglobalcode = d.itemglobalcode);

update dim_jda_productsubset d
	set d.container = ifnull(t.container, 'Not Set')
from dim_jda_productsubset d, tmp_dmdunit t
where d.itemglobalcode = t.itemglobalcode
	and d.container <> ifnull(t.container, 'Not Set');

update dim_jda_productsubset d
	set d.gbu = ifnull(t.gbu, 'Not Set')
from dim_jda_productsubset d, tmp_dmdunit t
where d.itemglobalcode = t.itemglobalcode
	and d.gbu <> ifnull(t.gbu, 'Not Set');

update dim_jda_productsubset d
	set d.gph = ifnull(t.gph, 'Not Set')
from dim_jda_productsubset d, tmp_dmdunit t
where d.itemglobalcode = t.itemglobalcode
	and d.gph <> ifnull(t.gph, 'Not Set');

update dim_jda_productsubset d
	set d.gphdescr = ifnull(t.gphdescr, 'Not Set')
from dim_jda_productsubset d, tmp_dmdunit t
where d.itemglobalcode = t.itemglobalcode
	and d.gphdescr <> ifnull(t.gphdescr, 'Not Set');

update dim_jda_productsubset d
	set d.ManufSite = ifnull(t.ManufSite, 'Not Set')
from dim_jda_productsubset d, tmp_dmdunit t
where d.itemglobalcode = t.itemglobalcode
	and d.ManufSite <> ifnull(t.ManufSite, 'Not Set');

update dim_jda_productsubset d
	set d.PackSite = ifnull(t.PackSite, 'Not Set')
from dim_jda_productsubset d, tmp_dmdunit t
where d.itemglobalcode = t.itemglobalcode
	and d.PackSite <> ifnull(t.PackSite, 'Not Set');

update dim_jda_productsubset d
	set d.Pack_Size = ifnull(t.Pack_Size, 0)
from dim_jda_productsubset d, tmp_dmdunit t
where d.itemglobalcode = t.itemglobalcode
	and d.Pack_Size <> ifnull(t.Pack_Size, 0);

update dim_jda_productsubset d
	set d.pharmaform = ifnull(t.pharmaform, 'Not Set')
from dim_jda_productsubset d, tmp_dmdunit t
where d.itemglobalcode = t.itemglobalcode
	and d.pharmaform <> ifnull(t.pharmaform, 'Not Set');
