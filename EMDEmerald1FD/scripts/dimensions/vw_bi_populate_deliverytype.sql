INSERT INTO dim_deliverytype(dim_deliverytypeid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_deliverytype
               WHERE dim_deliverytypeid = 1);
			   
UPDATE 
dim_deliverytype a 
SET a.Description = ifnull(TVLKT_VTEXT, 'Not Set'),
	a.dw_update_date = current_timestamp
FROM 
dim_deliverytype a,
TVLKT
WHERE a.deliverytype = TVLKT_LFART AND RowIsCurrent = 1;


delete from number_fountain m where m.table_name = 'dim_deliverytype';

insert into number_fountain
select 	'dim_deliverytype',
	ifnull(max(d.dim_deliverytypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_deliverytype d
where d.dim_deliverytypeid <> 1;

INSERT INTO dim_deliverytype(dim_deliverytypeId,
                              deliverytype,
                              Description,
                              RowStartDate,
                              RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_deliverytype')
          + row_number() over(order by '') ,
                        t.TVLKT_LFART,
          ifnull(TVLKT_VTEXT, 'Not Set'),
          current_timestamp,
          1
     FROM TVLKT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_deliverytype a
               WHERE a.deliverytype = TVLKT_LFART);

delete from number_fountain m where m.table_name = 'dim_deliverytype';
