insert into Dim_BillingCategory(Dim_BillingCategoryId,Description,Category,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason)
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from Dim_BillingCategory where Dim_BillingCategoryid = 1);
               
UPDATE    dim_billingcategory bc
	SET bc.Description = ifnull(dt.DD07T_DDTEXT, 'Not Set'),
		bc.dw_update_date = current_timestamp
       FROM
          dim_billingcategory bc, DD07T dt		
 WHERE bc.Category = dt.DD07T_DOMVALUE 
 AND bc.RowIsCurrent = 1
 AND dt.DD07T_DOMNAME = 'FKTYP' 
 AND dt.DD07T_DOMNAME IS NOT NULL;
 
delete from number_fountain m where m.table_name = 'Dim_BillingCategory';

insert into number_fountain
select 	'Dim_BillingCategory',
	ifnull(max(d.Dim_BillingCategoryId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_BillingCategory d
where d.Dim_BillingCategoryId <> 1;
               
INSERT INTO dim_billingcategory(Dim_BillingCategoryId,
								Description,
                               Category,
                               RowStartDate,
                               RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_BillingCategory') 
          + row_number() over(order by '') ,
			ifnull(DD07T_DDTEXT, 'Not Set'),
            ifnull(DD07T_DOMVALUE,'Not Set'),
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'FKTYP' 
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_billingcategory
                    WHERE Category = DD07T_DOMVALUE AND DD07T_DOMNAME = 'FKTYP')
   ORDER BY 2;
   
delete from number_fountain m where m.table_name = 'Dim_BillingCategory';
