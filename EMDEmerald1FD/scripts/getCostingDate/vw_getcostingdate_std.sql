/* #########################################################################################################################################	*/
/* */
/*   Script         : vw_getcostingdate_std.sql */
/*   Author         : Lokesh */
/*   Created On     : 22 Jul 2013 */
/*  */
/*  */
/*   Description    : getcostingdate function migrated from MySQL  */
/* */
/*   Change History */
/*   Date            By        Version            Desc 																											*/
/*   22-Jul-2013     Lokesh      1.0              Standard function for getcostingdate in VW ( Created from Ashu's migrated script vw_getLeadTime) */
/*                                                Run this after running the fact-specific script that populates tmp_getcostingdate	*/
/* ##########################################################################################################################################					*/

UPDATE tmp_getcostingdate
set processed_flag = 'Y',upd_flag = 'X'
WHERE processed_flag IS NULL;       /* Only pick up the rows which are not processed already */ 

 
update tmp_getcostingdate
SET pprevFiYear = pFiYear - 1,pprevPeriod = 12
WHERE pPeriod = 1
AND processed_flag = 'Y';

update tmp_getcostingdate
SET pprevFiYear = pFiYear,pprevPeriod = pPeriod - 1
WHERE ifnull(pPeriod,-1) <> 1
AND processed_flag = 'Y';

DROP TABLE IF EXISTS tmp_costingdate_mbew_no_bwtar;
CREATE TABLE tmp_costingdate_mbew_no_bwtar
AS 
SELECT a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,max(MBEW_ZPLD1) max_MBEW_ZPLD1
FROM MBEW_NO_BWTAR a
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

/* original script was splited in 2 parts */
update tmp_getcostingdate
SET upd_flag = 'A'
WHERE processed_flag = 'Y' AND upd_flag = 'X'
	 and upd_flag <> 'A';

merge into tmp_getcostingdate dest
using (select t0.rowid src_rid, ifnull(ds2.Dim_DateId, 1) costingDate_Id
	   from tmp_getcostingdate t0
				inner join (select ds1.rid, dt.Dim_DateId
						   from (select t1.rowid rid,
										a.max_MBEW_ZPLD1, t1.pCompanyCode
								 from tmp_getcostingdate t1
										inner join tmp_costingdate_mbew_no_bwtar a
												on    a.MATNR = t1.pMaterialNo
												  AND a.LFGJA = t1.pFiYear
												  AND a.LFMON = t1.pPeriod
												  AND a.BWKEY = t1.pPlant) ds1
								inner join dim_date dt on    dt.DateValue = ds1.max_MBEW_ZPLD1
														 AND dt.CompanyCode = ds1.pCompanyCode ) ds2
						on t0.rowid = ds2.rid
		where    t0.processed_flag = 'Y' 
			 AND t0.upd_flag = 'X'
	  ) src
on dest.rowid = src.src_rid
when matched then update set dest.costingDate_Id = src.costingDate_Id
where dest.costingDate_Id <> src.costingDate_Id;

			/* ORIGINAL SCRIPT 
			update tmp_getcostingdate
				  SET upd_flag = 'A',costingDate_Id =
								(SELECT dt.Dim_DateId
								  FROM tmp_costingdate_mbew_no_bwtar a , Dim_Date dt
								  WHERE     a.MATNR = pMaterialNo
										AND a.LFGJA = pFiYear
										AND a.LFMON = pPeriod
										AND a.BWKEY = pPlant
										AND a.max_MBEW_ZPLD1 = dt.DateValue
										AND dt.CompanyCode = pCompanyCode )
			WHERE processed_flag = 'Y' AND upd_flag = 'X' */


DROP TABLE IF EXISTS tmp_costingdate_mbew;
CREATE TABLE tmp_costingdate_mbew
AS 
SELECT a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,max(MBEW_ZPLD1) max_MBEW_ZPLD1
FROM MBEW a
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

/* original script was splited in 2 parts */
update tmp_getcostingdate
SET upd_flag = 'B'
WHERE processed_flag = 'Y' AND upd_flag = 'X'
	 and upd_flag <> 'B';

merge into tmp_getcostingdate dest
using (select t0.rowid src_rid, ifnull(ds2.Dim_DateId, 1) costingDate_Id
	   from tmp_getcostingdate t0
				inner join (select ds1.rid, dt.Dim_DateId
						   from (select t1.rowid rid,
										a.max_MBEW_ZPLD1, t1.pCompanyCode
								 from tmp_getcostingdate t1
										inner join tmp_costingdate_mbew a
												on    a.MATNR = t1.pMaterialNo
												  AND a.LFGJA = t1.pFiYear
												  AND a.LFMON = t1.pPeriod
												  AND a.BWKEY = t1.pPlant) ds1
								inner join dim_date dt on    dt.DateValue = ds1.max_MBEW_ZPLD1
														 AND dt.CompanyCode = ds1.pCompanyCode ) ds2
						on t0.rowid = ds2.rid
		where    t0.processed_flag = 'Y' 
			 AND t0.upd_flag = 'X'
	  ) src
on dest.rowid = src.src_rid
when matched then update set dest.costingDate_Id = src.costingDate_Id
where dest.costingDate_Id <> src.costingDate_Id;

			/*  ORIGINAL SCRIPT 
			update tmp_getcostingdate
				  SET upd_flag = 'B',costingDate_Id =
								(SELECT dt.Dim_DateId
								  FROM tmp_costingdate_mbew a , Dim_Date dt
								  WHERE     a.MATNR = pMaterialNo
										AND a.LFGJA = pFiYear
										AND a.LFMON = pPeriod
										AND a.BWKEY = pPlant
										AND a.max_MBEW_ZPLD1 = dt.DateValue
										AND dt.CompanyCode = pCompanyCode )
			WHERE processed_flag = 'Y' AND upd_flag = 'X' */


/* original script was splited in 2 parts */
update tmp_getcostingdate
SET upd_flag = 'C'
WHERE processed_flag = 'Y' AND upd_flag = 'X'
	 and upd_flag <> 'C';

merge into tmp_getcostingdate dest
using (select t0.rowid src_rid, ifnull(ds2.Dim_DateId, 1) costingDate_Id
	   from tmp_getcostingdate t0
				inner join (select ds1.rid, dt.Dim_DateId
						   from (select t1.rowid rid,
										a.max_MBEW_ZPLD1, t1.pCompanyCode
								 from tmp_getcostingdate t1
										inner join tmp_costingdate_mbew_no_bwtar a
												on    a.MATNR = t1.pMaterialNo
												  AND a.LFGJA = t1.pprevFiYear
												  AND a.LFMON = t1.pprevPeriod
												  AND a.BWKEY = t1.pPlant) ds1
								inner join dim_date dt on    dt.DateValue = ds1.max_MBEW_ZPLD1
														 AND dt.CompanyCode = ds1.pCompanyCode ) ds2
						on t0.rowid = ds2.rid
		where    t0.processed_flag = 'Y' 
			 AND t0.upd_flag = 'X'
	  ) src
on dest.rowid = src.src_rid
when matched then update set dest.costingDate_Id = src.costingDate_Id
where dest.costingDate_Id <> src.costingDate_Id;

			/*  ORIGINAL SCRIPT
			update tmp_getcostingdate
				  SET upd_flag = 'C',costingDate_Id =
								(SELECT dt.Dim_DateId
								  FROM tmp_costingdate_mbew_no_bwtar a , Dim_Date dt
								  WHERE     a.MATNR = pMaterialNo
										AND a.LFGJA = pprevFiYear
										AND a.LFMON = pprevPeriod
										AND a.BWKEY = pPlant
										AND a.max_MBEW_ZPLD1 = dt.DateValue
										AND dt.CompanyCode = pCompanyCode )
			WHERE processed_flag = 'Y' AND upd_flag = 'X' */

/* original script was splited in 2 parts */
update tmp_getcostingdate
SET upd_flag = 'D'
WHERE processed_flag = 'Y' AND upd_flag = 'X'
	 and upd_flag <> 'D';

merge into tmp_getcostingdate dest
using (select t0.rowid src_rid, ifnull(ds2.Dim_DateId, 1) costingDate_Id
	   from tmp_getcostingdate t0
				inner join (select ds1.rid, dt.Dim_DateId
						   from (select t1.rowid rid,
										a.max_MBEW_ZPLD1, t1.pCompanyCode
								 from tmp_getcostingdate t1
										inner join tmp_costingdate_mbew a
												on    a.MATNR = t1.pMaterialNo
												  AND a.LFGJA = t1.pprevFiYear
												  AND a.LFMON = t1.pprevPeriod
												  AND a.BWKEY = t1.pPlant) ds1
								inner join dim_date dt on    dt.DateValue = ds1.max_MBEW_ZPLD1
														 AND dt.CompanyCode = ds1.pCompanyCode ) ds2
						on t0.rowid = ds2.rid
		where    t0.processed_flag = 'Y' 
			 AND t0.upd_flag = 'X'
	  ) src
on dest.rowid = src.src_rid
when matched then update set dest.costingDate_Id = src.costingDate_Id
where dest.costingDate_Id <> src.costingDate_Id;

			/*  ORIGINAL SCRIPT
			update tmp_getcostingdate
				  SET upd_flag = 'D',costingDate_Id =
								(SELECT dt.Dim_DateId
								  FROM tmp_costingdate_mbew a , Dim_Date dt
								  WHERE     a.MATNR = pMaterialNo
										AND a.LFGJA = pprevFiYear
										AND a.LFMON = pprevPeriod
										AND a.BWKEY = pPlant
										AND a.max_MBEW_ZPLD1 = dt.DateValue
										AND dt.CompanyCode = pCompanyCode )
			WHERE processed_flag = 'Y' AND upd_flag = 'X' */

/* original script was splited in 2 parts */
update tmp_getcostingdate
SET upd_flag = 'E'
WHERE processed_flag = 'Y' AND upd_flag = 'X'
	 and upd_flag <> 'E';

merge into tmp_getcostingdate dest
using (select t0.rowid src_rid, ifnull(ds2.Dim_DateId, 1) costingDate_Id
	   from tmp_getcostingdate t0
				inner join (select ds1.rid, dt.Dim_DateId
						   from (select t1.rowid rid,
										a.max_MBEW_ZPLD1, t1.pCompanyCode
								 from tmp_getcostingdate t1
										inner join tmp_costingdate_mbew_no_bwtar a
												on    a.MATNR = t1.pMaterialNo
												  AND a.BWKEY = t1.pPlant) ds1
								inner join dim_date dt on    dt.DateValue = ds1.max_MBEW_ZPLD1
														 AND dt.CompanyCode = ds1.pCompanyCode ) ds2
						on t0.rowid = ds2.rid
		where    t0.processed_flag = 'Y' 
			 AND t0.upd_flag = 'X'
	  ) src
on dest.rowid = src.src_rid
when matched then update set dest.costingDate_Id = src.costingDate_Id
where dest.costingDate_Id <> src.costingDate_Id;

			/*  ORIGINAL SCRIPT
			update tmp_getcostingdate
				  SET upd_flag = 'E',costingDate_Id =
								(SELECT dt.Dim_DateId
								  FROM tmp_costingdate_mbew_no_bwtar a , Dim_Date dt
								  WHERE     a.MATNR = pMaterialNo
										AND a.BWKEY = pPlant
										AND a.max_MBEW_ZPLD1 = dt.DateValue
										AND dt.CompanyCode = pCompanyCode )
			WHERE processed_flag = 'Y' AND upd_flag = 'X' */

/* original script was splited in 2 parts */
update tmp_getcostingdate
SET upd_flag = 'F'
WHERE processed_flag = 'Y' AND upd_flag = 'X'
	 and upd_flag <> 'F';

merge into tmp_getcostingdate dest
using (select t0.rowid src_rid, ifnull(ds2.Dim_DateId, 1) costingDate_Id
	   from tmp_getcostingdate t0
				inner join (select ds1.rid, dt.Dim_DateId
						   from (select t1.rowid rid,
										a.max_MBEW_ZPLD1, t1.pCompanyCode
								 from tmp_getcostingdate t1
										inner join tmp_costingdate_mbew a
												on    a.MATNR = t1.pMaterialNo
												  AND a.BWKEY = t1.pPlant) ds1
								inner join dim_date dt on    dt.DateValue = ds1.max_MBEW_ZPLD1
														 AND dt.CompanyCode = ds1.pCompanyCode ) ds2
						on t0.rowid = ds2.rid
		where    t0.processed_flag = 'Y' 
			 AND t0.upd_flag = 'X'
	  ) src
on dest.rowid = src.src_rid
when matched then update set dest.costingDate_Id = src.costingDate_Id
where dest.costingDate_Id <> src.costingDate_Id;

			/*  ORIGINAL SCRIPT
			update tmp_getcostingdate
				  SET upd_flag = 'F',costingDate_Id =
								(SELECT dt.Dim_DateId
								  FROM tmp_costingdate_mbew a , Dim_Date dt
								  WHERE     a.MATNR = pMaterialNo
										AND a.BWKEY = pPlant
										AND a.max_MBEW_ZPLD1 = dt.DateValue
										AND dt.CompanyCode = pCompanyCode )
			WHERE processed_flag = 'Y' AND upd_flag = 'X' */

update tmp_getcostingdate
SET costingDate_Id = 1
WHERE processed_flag = 'Y' AND upd_flag = 'X';


DROP TABLE IF EXISTS tmp_costingdate_mbew_no_bwtar;
DROP TABLE IF EXISTS tmp_costingdate_mbew;

