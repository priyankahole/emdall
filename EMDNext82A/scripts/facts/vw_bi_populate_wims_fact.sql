/******************************************************************************************************************/
/*   Script         : bi_populate_wims_fact                                                                      */
/*   Author         : Cornelia                                                                                    */
/*   Created On     : 23 August 2017                                                                                 */
/*   Description    : Populating script of fact_wims                                                            */
/* ********************************************Change History****************************************************** */
/*   Date                By             Version      Desc                                                         */
/*   23 August 2017     Cornelia       1.0          Creating the script.                                         */
/*   30 August 2017     Cornelia       1.0          add Additional Fields                                         */
/*   10 October 2017    Roxana D	   1.0          Add new columns and change the logic for Additional Fields
/*  07 June 2018         Liviu I         1.1             Use extarctions as source (extractions can be found by searching "for wimps" on Next Platform  */
/* **************************************************************************************************************** */

/* use tables from the new extractions
 insert data in the same table that was used before */
delete from WIMS_EventHandlerDetail;
insert into WIMS_EventHandlerDetail
(ZSO_NO,
ZSO_ITM_NO,
Z_DLV_DATE,
Z_MAT_AVLB_DATE,
Z_MAT_NO,
Z_CALC_PROMISED_DATE,
Z_ORG_MAT_AVL_DT,
Z_ITEM_QTY,
Z_ITEM_UOM,
Z_LAST_MAT_CHG_DATE,
Z_PLANT,
Z_NEW_RE_PD_NT_YT_COM,
Z_RDD_DATE,
Z_LAST_COM_PD,
Z_MANUAL_OVERRIDE_DATE,
Z_COMMUNICATION_FLAG,
Z_FIRST_PROMISE_DATE,
Z_CUSTOMER_REQUESTED_DATE,
EVENT_CODE,
SOURCE_SYSTEM,
Z_ORDER_LINE_CONFIRM,
EH_GUID,
BLANC,
BLANC1,
TURCU,
TURCU1,
TIPID,
Z_LAST_COM_COUNTER,
Z_CALC_PD_COUNTER,
Z_REPROMISED_DT_NT_YT_COM_COUN,
Z_MANUAL_OVERRIDE_COUNTER,
ZSO_NO1,
Z_ORIG_PROMISED_DATE,
EVENT_DATE_UTC
)
 select 
f.ZSO_NO ZSO_NO_Header,
lpad(e.ZSO_ITM_NO,'6','0'),
ifnull(e.Z_DLV_DATE,'00000000'),
ifnull(e.Z_MAT_AVLB_DATE,'00000000'),
e.Z_MAT_NO,
ifnull(e.Z_CALC_PROMISED_DATE,'00000000'),
ifnull(e.Z_ORIG_MAT_AVL_DT,'00000000'),
null Z_ITEM_QTY,
null Z_ITEM_UOM,
ifnull(e.Z_LAST_MAT_CHG_DAT,'00000000'),
null Z_PLANT,
ifnull(e.Z_NEW_RE_PD_NT_YT_COM,'00000000'),
ifnull(e.Z_RDD_DATE,'00000000'),
ifnull(e.Z_LAST_COM_PD,'00000000'),
ifnull(e.Z_MANUAL_OVERRIDE_DATE,'00000000'),
null Z_COMMUNICATION_FLAG,
ifnull(e.Z_FIRST_PROMISE_DATE,'00000000'),
ifnull(e.Z_CUSTOMER_REQUESTED_DATE,'00000000'),
a.EVENT_CODE,
null SOURCE_SYSTEM,
e.Z_ORDER_LINE_CONFIRM,
c.EH_GUID,
null BLANC,
null BLANC1,
null TURCU,
null TURCU1,
b.TIPID,
e.Z_LAST_COM_COUNTER,
e.Z_CALC_PD_COUNTER,
e.Z_REPROMISED_DT_NT_YT_COM_COUN,
h.Z_MANUAL_OVERRIDE_COUNTER,
e.ZSO_NO ZSO_NO_Item,
ifnull(e.Z_ORIG_PROMISED_DATE,  '00000000'), 
lpad(a.EVENT_DATE_UTC,15,'0')
from SAPTRX_EH_EVMSG a 
	inner join SAPTRX_EVM_HDR b on b.EVT_GUID = a.MSG_GUID
	inner join SAPTRX_EH_CNTRL c on c.EH_GUID = a.EH_GUID and c.PARAM_NAME = 'SO_NO'
	inner join ZOTC_SO_ITEM e on e.ZSO_NO = c.PARAM_VALUE
	inner join ZOTC_SO_HEAD f on f.ZSO_NO = c.PARAM_VALUE
	inner join WIMS_MANUAL_OVERRIDE h on h.ZSO_NO = e.ZSO_NO and h.EH_GUID = e.EH_GUID and h.ZSO_ITM_NO = e.ZSO_ITM_NO
	inner join SAPTRX_EH_CNTRL d on e.ZSO_NO = e.ZSO_NO and e.ZSO_ITM_NO = trim(leading '0' from d.PARAM_VALUE) and d.EH_GUID = a.EH_GUID and d.PARAM_NAME = 'SO_ITEM'
where a.EVENT_CODE = 'GI'
		and e.EH_GUID in (select g.EH_GUID from SAPTRX_EH_CNTRL_CP g where g.PARAM_NAME = 'Z_SO_DELV_ITM_STA' and g.PARAM_VALUE = 'C - Completely processed');

TRUNCATE TABLE FACT_WIMS;

DROP TABLE IF EXISTS tmp_fact_wims;
CREATE TABLE tmp_fact_wims 
AS 
SELECT *
FROM FACT_WIMS;
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_wims';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_wims',IFNULL(MAX(fact_wimsID),0)
FROM tmp_fact_wims;

INSERT INTO tmp_fact_wims( 
    FACT_WIMSID,
    dd_event_code ,
    dd_source ,
    dd_guid_code ,
    dd_so_no ,
    dd_so_item_no ,
    dd_materialno ,
	dim_dateiddlv ,
    dim_dateidmatavlb ,
    dim_dateidcalcpromised,
    dim_dateidorigmatavl ,
    dim_dateidorigpromised ,
    dim_dateidrdd ,
    dim_dateidlastcompd,
    dim_dateidfirstpromise ,
    dim_dateidcustomerrequested,
    DW_INSERT_DATE ,
    DW_UPDATE_DATE ,
    DIM_PROJECTSOURCEID,
	dd_z_dlv_date,
	dd_Z_MAT_AVLB_DATE,
	dd_Z_CALC_PROMISED_DATE,
	dd_Z_ORIG_MAT_AVL_DT,
	dd_Z_RDD_DATE,
	dd_LAST_COM_PD,
	dd_Z_FIRST_PROMISE_DATE,
	dd_Z_CUSTOMER_REQUESTED_DATE,
	dd_salesorgdesc,
	dd_salesofficedesc,
	dd_salesgroupdesc,
	dd_soldtocust,
	dd_shiptocust,
	dd_deliveryblockcode,
	dd_deliveryblockdesc,
	dd_uom,
	dd_GlbProductGroup,
	dd_partdesc,
	dd_destcountry,
	dd_OverallDeliveryStatus,
	dd_destcountrydesc,
	dim_dateidsocreated,
	dim_plantid,
	ct_itemqty,
	dd_z_calc_pd_counter,
	ct_z_calc_pd_counter,
    DD_Z_NEW_RE_PD_NT_YT_COM,
    dd_z_manual_override_counter,
    dd_z_REPROMISED_DT_NT_YT_COM_COUN,
    ct_z_manual_override_counter,
    ct_z_REPROMISED_DT_NT_YT_COM_COUN,
    dim_dateidagi,
    ct_Z_LAST_COM_COUNTER,
    dd_Z_LAST_COM_COUNTER,
    dd_Z_ORIG_PROMISED_DATE,
	dd_SourceId,
	dd_Actualgoodsissuedate,
	dim_dateidwimsactualgoodissue,
	DD_Z_ORDER_LINE_CONFIRM
)
SELECT 
(select ifnull(max_id,1) from number_fountain where table_name = 'tmp_fact_wims') + row_number() over(order by '') AS fact_wimsID,
ifnull(st.event_code,'Not Set') as dd_event_code,
ifnull(st.source_system,'Not Set') as dd_source,
ifnull(st.eh_guid,'Not Set') as dd_guid_code,
ifnull(st.zso_no,'Not Set') as dd_so_no,
ifnull(st.zso_itm_no ,0) as dd_so_item_no,
ifnull(st.z_mat_no,'Not Set') as dd_materialno,
1 as dim_dateiddlv ,
1 as dim_dateidmatavlb ,
1 as dim_dateidcalcpromised,
1 as dim_dateidorigmatavl ,
1 as dim_dateidorigpromised ,
1 as dim_dateidrdd ,
1 as dim_dateidlastcompd,
1 as dim_dateidfirstpromise ,
1 as dim_dateidcustomerrequested,
current_timestamp as DW_INSERT_DATE, 
current_timestamp as DW_UPDATE_DATE, 
1 as dim_projectsourceid,
ifnull(st.z_dlv_date,'0001-01-01') dd_z_dlv_date,
ifnull(st.Z_MAT_AVLB_DATE,'0001-01-01') dd_Z_MAT_AVLB_DATE,
ifnull(st.Z_CALC_PROMISED_DATE,'0001-01-01')  dd_Z_CALC_PROMISED_DATE,
ifnull(st.Z_ORG_MAT_AVL_DT,'0001-01-01')  dd_Z_ORGI_MAT_AVL_DT,
ifnull(st.Z_RDD_DATE,'0001-01-01') dd_Z_RDD_DATE,
ifnull(st.Z_LAST_COM_PD,'0001-01-01')  dd_LAST_COM_PD,
ifnull(st.Z_FIRST_PROMISE_DATE,'0001-01-01')  dd_Z_FIRST_PROMISE_DATE,
ifnull(st.Z_CUSTOMER_REQUESTED_DATE,'0001-01-01')  dd_Z_CUSTOMER_REQUESTED_DATE,
'Not Set' as dd_salesorgdesc,
'Not Set' as dd_salesofficedesc,
'Not Set' as dd_salesgroupdesc,
'Not Set' as dd_soldtocust,
'Not Set' as dd_shiptocust,
'Not Set' as dd_deliveryblockcode,
'Not Set' as dd_deliveryblockdesc,
'Not Set' as dd_uom,
'Not Set' as dd_GlbProductGroup,
'Not Set' as dd_partdesc,
'Not Set' as dd_destcountry,
'Not Set' as dd_OverallDeliveryStatus,
'Not Set' as dd_destcountrydesc,
1 dim_dateidsocreated,
1 dim_plantid,
0 ct_itemqty,
ifnull(st.z_calc_pd_counter,'Not Set') as dd_z_calc_pd_counter,
ifnull(st.z_calc_pd_counter,0) as ct_z_calc_pd_counter,
ifnull(st.Z_NEW_RE_PD_NT_YT_COM,'Not Set') as DD_Z_NEW_RE_PD_NT_YT_COM,
ifnull(st.z_manual_override_counter,'Not Set') as dd_z_manual_override_counter,
ifnull(st.z_REPROMISED_DT_NT_YT_COM_COUN, 'Not Set') as dd_z_REPROMISED_DT_NT_YT_COM_COUN,
ifnull(to_number(st.z_manual_override_counter),0) as  ct_z_manual_override_counter,
ifnull(to_number(st.z_REPROMISED_DT_NT_YT_COM_COUN), 0) as  ct_z_REPROMISED_DT_NT_YT_COM_COUN,
1 as dim_dateidagi,
ifnull(to_number(st.Z_LAST_COM_COUNTER),0) as ct_Z_LAST_COM_COUNTER,
ifnull(st.Z_LAST_COM_COUNTER,'Not Set') as dd_Z_LAST_COM_COUNTER,
ifnull(st.Z_ORIG_PROMISED_DATE,'0001-01-01') as dd_Z_ORIG_PROMISED_DATE,
ifnull(TIPID,'Not Set') AS dd_SourceID,
ifnull(EVENT_DATE_UTC,'Not Set') as dd_Actualgoodsissuedate,
1 AS dim_dateidwimsactualgoodissue,
IFNULL(Z_ORDER_LINE_CONFIRM,'Not Set')  AS DD_Z_ORDER_LINE_CONFIRM
FROM  WIMS_EventHandlerDetail st
where Z_ORG_MAT_AVL_DT <> 'EVM_HDR_'
--AND EVENT_DATE_UTC >= '020170901000000'
;


UPDATE tmp_fact_wims F
SET F.dim_dateidorigpromised  = D.DIM_DATEID
FROM  tmp_fact_wims F, DIM_DATE D 
WHERE left(dd_Z_ORIG_PROMISED_DATE,4)||'-'||substr(dd_Z_ORIG_PROMISED_DATE,5,2)||'-'||substr(dd_Z_ORIG_PROMISED_DATE,7,2) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidorigpromised   <> D.DIM_DATEID;

UPDATE tmp_fact_wims F
SET F.dim_dateiddlv = D.DIM_DATEID
FROM  tmp_fact_wims F, DIM_DATE D 
WHERE  left(f.dd_z_dlv_date,4)||'-'||substr(f.dd_z_dlv_date,5,2)||'-'||substr(f.dd_z_dlv_date,7,2)  = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateiddlv  <> D.DIM_DATEID;

UPDATE tmp_fact_wims F
SET F.dim_dateidmatavlb = D.DIM_DATEID
FROM  tmp_fact_wims F,DIM_DATE D 
WHERE   left(f.dd_Z_MAT_AVLB_DATE,4)||'-'||substr(f.dd_Z_MAT_AVLB_DATE,5,2)||'-'||substr(f.dd_Z_MAT_AVLB_DATE,7,2) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidmatavlb  <> D.DIM_DATEID;

UPDATE tmp_fact_wims F
SET F.dim_dateidcalcpromised = D.DIM_DATEID
FROM  tmp_fact_wims F, DIM_DATE D 
WHERE   left(f.dd_Z_CALC_PROMISED_DATE,4)||'-'||substr(f.dd_Z_CALC_PROMISED_DATE,5,2)||'-'||substr(f.dd_Z_CALC_PROMISED_DATE,7,2) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidcalcpromised  <> D.DIM_DATEID;

UPDATE tmp_fact_wims F
SET F.dim_dateidorigmatavl = D.DIM_DATEID
FROM  tmp_fact_wims F, DIM_DATE D 
WHERE  left(f.dd_Z_ORIG_MAT_AVL_DT ,4)||'-'||substr(f.dd_Z_ORIG_MAT_AVL_DT ,5,2)||'-'||substr(f.dd_Z_ORIG_MAT_AVL_DT ,7,2) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidorigmatavl  <> D.DIM_DATEID;


UPDATE tmp_fact_wims F
SET F.dim_dateidrdd  = D.DIM_DATEID
FROM  tmp_fact_wims F,DIM_DATE D 
WHERE  left(f.dd_Z_RDD_DATE,4)||'-'||substr(f.dd_Z_RDD_DATE ,5,2)||'-'||substr(f.dd_Z_RDD_DATE ,7,2)= D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidrdd   <> D.DIM_DATEID;

UPDATE tmp_fact_wims F
SET F.dim_dateidlastcompd  = D.DIM_DATEID
FROM  tmp_fact_wims F,DIM_DATE D 
WHERE   left(f.dd_LAST_COM_PD,4)||'-'||substr(f.dd_LAST_COM_PD ,5,2)||'-'||substr(f.dd_LAST_COM_PD ,7,2) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidlastcompd   <> D.DIM_DATEID;

UPDATE tmp_fact_wims F
SET F.dim_dateidfirstpromise  = D.DIM_DATEID
FROM  tmp_fact_wims F, DIM_DATE D 
WHERE  left(f.dd_Z_FIRST_PROMISE_DATE,4)||'-'||substr(f.dd_Z_FIRST_PROMISE_DATE ,5,2)||'-'||substr(f.dd_Z_FIRST_PROMISE_DATE ,7,2) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidfirstpromise   <> D.DIM_DATEID;

UPDATE tmp_fact_wims F
SET F.dim_dateidcustomerrequested  = D.DIM_DATEID
FROM  tmp_fact_wims F, DIM_DATE D 
WHERE left(f.dd_Z_CUSTOMER_REQUESTED_DATE,4)||'-'||substr(f.dd_Z_CUSTOMER_REQUESTED_DATE ,5,2)||'-'||substr(f.dd_Z_CUSTOMER_REQUESTED_DATE ,7,2) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidcustomerrequested   <> D.DIM_DATEID;

UPDATE tmp_fact_wims F
SET F.dim_dateidwimsactualgoodissue  = D.DIM_DATEID
FROM  tmp_fact_wims F, DIM_DATE D 
WHERE SUBSTR(f.dd_Actualgoodsissuedate,2,4)||'-'||substr(f.dd_Actualgoodsissuedate ,6,2)||'-'||substr(f.dd_Actualgoodsissuedate ,8,2) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_dateidwimsactualgoodissue  <> D.DIM_DATEID;


/* Fix multiple GUID values for one (dd_so_no,dd_so_item_no) combination  - not needed - keep the last actual GI or delivery date

drop table if exists tmp_fix_guid_code;
create table tmp_fix_guid_code as 
select dd_so_no,dd_so_item_no, max(dd_guid_code) as max_guid
from tmp_fact_wims 
group by dd_so_no,dd_so_item_no;

update tmp_fact_wims f
set f.dd_guid_code = t.max_guid 
from tmp_fact_wims f, tmp_fix_guid_code t
where f.dd_so_no = t.dd_so_no 
and f.dd_so_item_no = t.dd_so_item_no */


 
/* Add Actual Good Issue date or Delivery Date */

update tmp_fact_wims f
set  f.dim_dateidAGI_or_Delivery = ifnull((case when dd_SourceID in ('PRE','PRD','ORACLE') then dim_dateidwimsactualgoodissue 
												when dd_sourceID in ('PRP','TEMPO','Emerald','Phoenix') then dim_dateiddlv
												else 1 end),1);


delete from tmp_fact_wims f
where dim_dateidAGI_or_Delivery in (select dim_dateid from dim_date  d where  d.CompanyCode  = 'Not Set' and d.datevalue <'2017-11-01');



ALTER TABLE TMP_FACT_WIMS ADD COLUMN SORT_DATE_NO INT;

UPDATE TMP_FACT_WIMS F
SET F.SORT_DATE_NO = SF.SORTNO
FROM TMP_FACT_WIMS F, 
	(SELECT ROW_NUMBER() OVER (PARTITION BY DD_SO_NO,DD_SO_ITEM_NO ORDER BY DT.DATEVALUE DESC) AS SORTNO, F.FACT_WIMSID
	FROM TMP_FACT_WIMS F, DIM_DATE DT WHERE F.dim_dateidAGI_or_Delivery = DT.DIM_DATEID) SF
WHERE F.FACT_WIMSID = SF.FACT_WIMSID;


INSERT INTO fact_wims(
 FACT_WIMSID,
    dd_event_code ,
    dd_source ,
    dd_guid_code ,
    dd_so_no ,
    dd_so_item_no ,
    dd_materialno ,
	dim_dateiddlv ,
    dim_dateidmatavlb ,
    dim_dateidcalcpromised,
    dim_dateidorigmatavl ,
    dim_dateidorigpromised ,
    dim_dateidrdd ,
    dim_dateidlastcompd,
    dim_dateidfirstpromise ,
    dim_dateidcustomerrequested,
    DW_INSERT_DATE ,
    DW_UPDATE_DATE ,
    DIM_PROJECTSOURCEID,
	dd_z_dlv_date,
	dd_Z_MAT_AVLB_DATE,
	dd_Z_CALC_PROMISED_DATE,
	dd_Z_ORIG_MAT_AVL_DT,
	dd_Z_RDD_DATE,
	dd_LAST_COM_PD,
	dd_Z_FIRST_PROMISE_DATE,
	dd_Z_CUSTOMER_REQUESTED_DATE,
	dd_salesorgdesc,
	dd_salesofficedesc,
	dd_salesgroupdesc,
	dd_soldtocust,
	dd_shiptocust,
	dd_deliveryblockcode,
	dd_deliveryblockdesc,
	dd_uom,
	dd_GlbProductGroup,
	dd_partdesc,
	dd_destcountry,
	dd_OverallDeliveryStatus,
	dd_destcountrydesc,
	dim_dateidsocreated,
	dim_plantid,
	ct_itemqty,
	dd_z_calc_pd_counter,
	ct_Z_CALC_PD_COUNTER,
    DD_Z_NEW_RE_PD_NT_YT_COM,
    dd_z_manual_override_counter,
    dd_z_REPROMISED_DT_NT_YT_COM_COUN,
    ct_z_manual_override_counter,
    ct_z_REPROMISED_DT_NT_YT_COM_COUN,
    ct_Z_LAST_COM_COUNTER,
    dd_Z_LAST_COM_COUNTER,
    dd_Z_ORIG_PROMISED_DATE,
	dd_SourceID,
	dd_Actualgoodsissuedate,
	dim_dateidwimsactualgoodissue,
	dim_dateidAGI_or_Delivery,
	DD_Z_ORDER_LINE_CONFIRM)
SELECT  
	fact_wimsID,
    dd_event_code ,
    dd_source ,
    dd_guid_code ,
    dd_so_no ,
    dd_so_item_no ,
    dd_materialno ,
	dim_dateiddlv ,
    dim_dateidmatavlb ,
    dim_dateidcalcpromised,
    dim_dateidorigmatavl ,
    dim_dateidorigpromised ,
    dim_dateidrdd ,
    dim_dateidlastcompd,
    dim_dateidfirstpromise ,
    dim_dateidcustomerrequested,
    DW_INSERT_DATE ,
    DW_UPDATE_DATE ,
    DIM_PROJECTSOURCEID,
	dd_z_dlv_date,
	dd_Z_MAT_AVLB_DATE,
	dd_Z_CALC_PROMISED_DATE,
	dd_Z_ORIG_MAT_AVL_DT,
	dd_Z_RDD_DATE,
	dd_LAST_COM_PD,
	dd_Z_FIRST_PROMISE_DATE,
	dd_Z_CUSTOMER_REQUESTED_DATE,
	dd_salesorgdesc,
	dd_salesofficedesc,
	dd_salesgroupdesc,
	dd_soldtocust,
	dd_shiptocust,
	dd_deliveryblockcode,
	dd_deliveryblockdesc,
	dd_uom,
	dd_GlbProductGroup,
	dd_partdesc,
	dd_destcountry,
	dd_OverallDeliveryStatus,
	dd_destcountrydesc,
	dim_dateidsocreated,
	dim_plantid,
	ct_itemqty,
	dd_z_calc_pd_counter,
	ct_Z_CALC_PD_COUNTER,
    DD_Z_NEW_RE_PD_NT_YT_COM,
    dd_z_manual_override_counter,
    dd_z_REPROMISED_DT_NT_YT_COM_COUN,
    ct_z_manual_override_counter,
    ct_z_REPROMISED_DT_NT_YT_COM_COUN,
    ct_Z_LAST_COM_COUNTER,
    dd_Z_LAST_COM_COUNTER,
    dd_Z_ORIG_PROMISED_DATE,
	dd_SourceID,
	dd_Actualgoodsissuedate,
	dim_dateidwimsactualgoodissue,
	dim_dateidAGI_or_Delivery,
	DD_Z_ORDER_LINE_CONFIRM
FROM  tmp_fact_wims
where SORT_DATE_NO = 1;


DROP TABLE IF EXISTS tmp_fact_wims;


/* Updates for Additional Fields */
--
drop table if exists tmp_selectedschedule;
create table tmp_selectedschedule as 
select  fso.dd_salesdocno, fso.dd_salesitemno,  max(projectcode) as ProjectCode, min(dd_ScheduleNo) as min_schedule
from EMD586.fact_salesorder fso 
  inner join EMD586.dim_projectsource dp on fso.dim_projectsourceid = dp.dim_projectsourceid
  inner join fact_wims f on  fso.dd_salesdocno = f.dd_so_no and  fso.dd_salesitemno=  trim(leading '0' from f.dd_so_item_no)
group by fso.dd_salesdocno, fso.dd_salesitemno
; 

drop table if exists tmp_additionalfields;
create table tmp_additionalfields as
select 
	fso.dd_salesdocno, 
	fso.dd_salesitemno,
	max(fso.dim_SalesOrgID) dim_SalesOrgID,
	max(sorg.salesorgcode) salesorgcode,
	max(sorg.salesorgname) salesorgname,
	max(fso.dim_SalesOfficeID) dim_SalesOfficeID,
	max(soff.salesofficecode) salesofficecode,
    max(soff.salesofficename) salesofficename,
	max(fso.dim_salesgroupid) dim_salesgroupid,
    max(sgr.salesgroupcode) salesgroupcode,
    max(sgr.salesgroupname) salesgroupname,
	max(fso.dim_customerid) dim_customerid,
  	max(scu.country) SoldTo_CustCountry, 
	max(scu.countryname) SoldTo_CustCountryName, 
	max(scu.name) SoldTo_CUSTName, 
	max(scu.customernumber) SoldTo_CustNo,
	max(fso.dim_customer_shipto) dim_customer_shipto,
	max(scsh.name) ShipTo_CustName, 
	max(scu.customernumber) ShipTo_CustNo,
	max(fso.dim_deliveryblockid) dim_deliveryblockid,
	max(sdlv.Description) DeliveryBlockDesc,
	max(sdlv.Deliveryblock) Deliveryblock,
	max(fso.dim_dateidsocreated) dim_dateidsocreated,
	max(fso.dim_unitofmeasureid) dim_unitofmeasureid,
	max(um.uom) uom,
	max(fso.dim_partid) dim_partid,
	max(dpart.PartDescription) PartDescription,
	max(fso.dim_plantid) dim_plantid,
	max(fso.dim_mdg_partid)dim_mdg_partid,
	MAX(mdg.GlbProductGroup) GlbProductGroup,
	MAX(fso.dim_salesorderitemstatusid)dim_salesorderitemstatusid,
	MAX(sts.OverallDeliveryStatus) OverallDeliveryStatus,
	MAX(fso.dim_dateidactualgi) dim_dateidactualgi,
	MAX(fso.DIM_COMMERCIALVIEWID) DIM_COMMERCIALVIEWID,
	MAX(fso.dd_BiFilters_PRE_PRD) dd_BiFilters_PRE_PRD,
	dtagi.companycode   as CompanyCodeAGI,
	dt.CompanyCode as CompanyCodeSOCD,
	max(fso.Dim_AccountAssignmentGroupid) Dim_AccountAssignmentGroupid,
	max(Dim_ProductHierarchyid) Dim_ProductHierarchyid,
	max(fso.dd_intercompflag) dd_intercompflag

from EMD586.fact_salesorder fso 
  inner join tmp_selectedschedule selsch on fso.dd_salesdocno = selsch.dd_salesdocno and  fso.dd_salesitemno = selsch.dd_salesitemno and fso.dd_ScheduleNo = selsch.min_schedule
  inner join EMD586.dim_projectsource dp on fso.dim_projectsourceid = dp.dim_projectsourceid
  inner join EMD586.dim_SalesOrg sorg on fso.dim_SalesOrgID = sorg.dim_SalesOrgID
  inner join EMD586.dim_SalesOffice soff on fso.dim_SalesOfficeID = soff.dim_SalesOfficeID
  inner join EMD586.dim_salesgroup sgr on fso.dim_salesgroupid = sgr.dim_salesgroupid
  inner join EMD586.dim_customer scu on fso.dim_customerid = scu.dim_customerid
  inner join EMD586.dim_customer scsh on fso.dim_customer_shipto = scsh.dim_customerid
  inner join EMD586.dim_deliveryblock  sdlv on fso.dim_deliveryblockid = sdlv.dim_deliveryblockid
  inner join EMD586.dim_date dt on fso.dim_dateidsocreated = dt.dim_dateid 
  inner join EMD586.dim_unitofmeasure um on fso.dim_unitofmeasureid = um.dim_unitofmeasureid
  inner join EMD586.dim_part dpart on fso.dim_partid = dpart.dim_partid
  inner join EMD586.dim_plant plant on fso.dim_plantid = plant.dim_plantid
  inner join EMD586.dim_mdg_part mdg on fso.dim_mdg_partid = mdg.dim_mdg_partid
  inner join EMD586.dim_salesorderitemstatus sts on fso.dim_salesorderitemstatusid = sts.dim_salesorderitemstatusid
  inner join EMD586.dim_date dtagi on fso.dim_dateidactualgi = dtagi.dim_dateid
  inner join EMD586.dim_commercialview commv on fso.DIM_COMMERCIALVIEWID = commv.DIM_COMMERCIALVIEWID 
GROUP BY fso.dd_salesdocno, 
	fso.dd_salesitemno,dtagi.companycode ,
	dt.CompanyCode 
;


update fact_wims f
set f.dd_salesorgdesc = t.salesorgname
from  fact_wims f, (SELECT MAX(salesorgname) AS salesorgname, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_salesorgdesc <> t.salesorgname;

update fact_wims f
set f.dd_salesorgkey = t.SalesOrgCode
from  fact_wims f, (SELECT MAX(SalesOrgCode) AS SalesOrgCode, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_salesorgkey <> t.SalesOrgCode;

update fact_wims f
set f.dd_salesofficekey = t.SalesOfficeCode
from  fact_wims f, (SELECT MAX(SalesOfficeCode) AS SalesOfficeCode, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_salesofficekey <> t.SalesOfficeCode;

update fact_wims f
set f.dd_salesofficedesc = t.SalesOfficeName
from  fact_wims f, (SELECT MAX(SalesOfficeName) AS SalesOfficeName, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_salesofficedesc <> t.SalesOfficeName;

update fact_wims f
set f.dd_salesgroupkey = t.SalesGroupCode
from  fact_wims f, (SELECT MAX(SalesGroupCode) AS SalesGroupCode, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_salesgroupkey <> t.SalesGroupCode;

update fact_wims f
set f.dd_salesgroupdesc = t.SalesGroupName
from  fact_wims f, (SELECT MAX(SalesGroupName) AS SalesGroupName, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_salesgroupdesc <> t.SalesGroupName;

update fact_wims f
set f.dd_soldtocustkey = t.SoldTo_CustNo
from  fact_wims f, (SELECT MAX(SoldTo_CustNo) AS SoldTo_CustNo, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_soldtocustkey <> t.SoldTo_CustNo;

update fact_wims f
set f.dd_soldtocust = t.SoldTo_CustName
from  fact_wims f, (SELECT MAX(SoldTo_CustName) AS SoldTo_CustName, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_soldtocust <> t.SoldTo_CustName;

update fact_wims f
set f.dd_destcountry = t.SoldTo_CustCountry
from  fact_wims f, (SELECT MAX(SoldTo_CustCountry) AS SoldTo_CustCountry, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_destcountry <> t.SoldTo_CustCountry;

update fact_wims f
set f.dd_destcountrydesc = t.SoldTo_CustCountryName
from  fact_wims f, (SELECT MAX(SoldTo_CustCountryName) AS SoldTo_CustCountryName, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_destcountrydesc <> t.SoldTo_CustCountryName;

update fact_wims f
set f.dd_shiptocust = t.ShipTo_CustName
from  fact_wims f, (SELECT MAX(ShipTo_CustName) AS ShipTo_CustName, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_shiptocust <> t.ShipTo_CustName;

update fact_wims f
set f.dd_shiptocustkey = t.ShipTo_custNo
from  fact_wims f, (SELECT MAX(ShipTo_custNo) AS ShipTo_custNo, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_shiptocustkey <> t.ShipTo_custNo;

update fact_wims f
set f.dd_deliveryblockcode = t.DeliveryBlock
from  fact_wims f, (SELECT MAX(DeliveryBlock) AS DeliveryBlock, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_deliveryblockcode <> t.DeliveryBlock;

update fact_wims f
set f.dd_deliveryblockdesc = t.DeliveryBlockDesc
from  fact_wims f, (SELECT MAX(DeliveryBlockDesc) AS DeliveryBlockDesc, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_deliveryblockdesc <> t.deliveryblockdesc;

---IDs not matching 
/*
update fact_wims f
set f.dim_dateidsocreated = t.dim_dateidsocreated
from  fact_wims f, tmp_additionalfields t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dim_dateidsocreated <> t.dim_dateidsocreated */

update fact_wims f
set f.dim_dateidsocreated = ifnull(dt.dim_dateid,1)
from fact_wims f, (select dd_salesitemno,dd_salesdocno,companycodesocd, max(dim_dateidsocreated) dim_dateidsocreated
				 from tmp_additionalfields group by dd_salesitemno,dd_salesdocno,companycodesocd) t, 
				 (select max(dim_dateid) dim_dateid,companycode,datevalue from emd586.dim_date group by companycode,datevalue) pjdt, 
				 (select max(dim_dateid) dim_dateid,datevalue,companycode from dim_date group by datevalue,companycode) dt
where 
f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and t.dim_dateidsocreated = pjdt.dim_dateid
and t.companycodesocd = pjdt.companycode
and dt.datevalue = pjdt.datevalue 
and dt.companycode = pjdt.companycode
and f.dim_dateidsocreated <> ifnull(dt.dim_dateid,1);


update fact_wims f
set f.dd_uom = t.uom
from  fact_wims f, (SELECT MAX(uom) AS uom, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_uom <> t.uom;

update fact_wims f
set f.dim_plantid = t.dim_plantid
from  fact_wims f, (SELECT MAX(dim_plantid) AS dim_plantid, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dim_plantid <> t.dim_plantid;


update fact_wims f
set f.dd_GlbProductGroup = t.GlbProductGroup
from  fact_wims f, (SELECT MAX(GlbProductGroup) AS GlbProductGroup, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_GlbProductGroup <> t.GlbProductGroup;

update fact_wims f
set f.dd_partdesc = t.PartDescription
from  fact_wims f, (SELECT MAX(PartDescription) AS PartDescription, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_partdesc <> t.PartDescription;


update fact_wims f
set f.dd_OverallDeliveryStatus = t.OverallDeliveryStatus
from  fact_wims f, (SELECT MAX(OverallDeliveryStatus) AS OverallDeliveryStatus, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_OverallDeliveryStatus <> t.OverallDeliveryStatus;

/*
update fact_wims f
set f.dim_dateidagi = t.dim_dateidactualgi
from  fact_wims f, tmp_additionalfields t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and ifnull(f.dim_dateidagi,1) <> ifnull(t.dim_dateidactualgi,2)*/

update fact_wims f
set f.dim_dateidagi = ifnull(dt.dim_dateid,1)
from fact_wims f, tmp_additionalfields t, emd586.dim_date pjdt, dim_date dt
where 
f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and t.dim_dateidactualgi = pjdt.dim_dateid and t.CompanyCodeAGI = pjdt.companycode
and dt.datevalue = pjdt.datevalue 
and dt.companycode = pjdt.companycode
and f.dim_dateidagi <> ifnull(dt.dim_dateid,1);

update fact_wims f
set f.dim_partid = t.dim_partid
from  fact_wims f, (SELECT MAX(dim_partid) AS dim_partid, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dim_partid <> t.dim_partid;

update fact_wims f
set f.DIM_COMMERCIALVIEWID = t.DIM_COMMERCIALVIEWID
from  fact_wims f, (SELECT MAX(DIM_COMMERCIALVIEWID) AS DIM_COMMERCIALVIEWID, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.DIM_COMMERCIALVIEWID <> t.DIM_COMMERCIALVIEWID;

update fact_wims f
set f.dd_BiFilters_PRE_PRD = t.dd_BiFilters_PRE_PRD
from  fact_wims f, (SELECT MAX(dd_BiFilters_PRE_PRD) AS dd_BiFilters_PRE_PRD, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_BiFilters_PRE_PRD <> t.dd_BiFilters_PRE_PRD;


/*Merck LS Intercompany Flag and Merck LS Drop Shipment Flag*/
/* update fact_wims f
set f.dd_intercompflag = ifnull(case when d.accountassignmentgroup in ('01','02') then ifnull('3rd Party', 'Not Set')
							when d.accountassignmentgroup in ('03','04','08') then ifnull('Intercompany', 'Not Set')
							else 'Intercompany' end,'Intercompany')
from  fact_wims f
	inner join (SELECT MAX(Dim_AccountAssignmentGroupid) AS Dim_AccountAssignmentGroupid, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno) t
			on f.dd_so_no = t.dd_salesdocno
				and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
	inner join EMD586.Dim_AccountAssignmentGroup d on t.Dim_AccountAssignmentGroupid = d.Dim_AccountAssignmentGroupid
where f.dd_intercompflag <> ifnull(case when d.accountassignmentgroup in ('01','02') then ifnull('3rd Party', 'Not Set')
							when d.accountassignmentgroup in ('03','04','08') then ifnull('Intercompany', 'Not Set')
							else 'Intercompany' end,'Intercompany') */

update fact_wims f
set f.dd_intercompflag = t.dd_intercompflag
from  fact_wims f, (SELECT MAX(dd_intercompflag) AS dd_intercompflag, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.dd_intercompflag <> t.dd_intercompflag;



update fact_wims
set dd_intercompflag = 'Intercompany'
where dd_intercompflag = 'Not Set';

update fact_wims f
set f.Dim_ProductHierarchyid = t.Dim_ProductHierarchyid
from  fact_wims f, (SELECT MAX(Dim_ProductHierarchyid) AS Dim_ProductHierarchyid, dd_salesdocno,dd_salesitemno FROM tmp_additionalfields GROUP BY dd_salesdocno,dd_salesitemno)t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.Dim_ProductHierarchyid <> t.Dim_ProductHierarchyid;

drop table if exists tmp_itemqty;
create table tmp_itemqty as 
select fso.dd_salesdocno, fso.dd_salesitemno, CASE WHEN SUM(fso.ct_ScheduleQtySalesUnit) <> 0 THEN SUM(fso.ct_ScheduleQtySalesUnit) ELSE 1.0000 END as ct_itemqty
from fact_salesorder fso
group by fso.dd_salesdocno, fso.dd_salesitemno;

update fact_wims f
set f.ct_itemqty = t.ct_itemqty
from  fact_wims f, tmp_itemqty t
where f.dd_so_no = t.dd_salesdocno
and trim(leading '0' from f.dd_so_item_no) = t.dd_salesitemno
and f.ct_itemqty <> t.ct_itemqty;

drop table if exists tmp_itemqty;


/* END Updates fro Additional Fields */

/* Internal Nervousness */
drop table if exists tmp_total_count_per_item;
create table tmp_total_count_per_item 
as select  dd_so_no, dd_so_item_no,sum(case when dd_Z_REPROMISED_DT_NT_YT_COM_COUN = 'Not Set' then 0 else to_number(dd_Z_REPROMISED_DT_NT_YT_COM_COUN) end) as total_count_per_item
from fact_wims
group by dd_so_no, dd_so_item_no;


update fact_wims f 
set f.ct_total_count_nyc_per_item = t.total_count_per_item
from  fact_wims f, tmp_total_count_per_item t
where f.dd_so_item_no = t.dd_so_item_no
and f.dd_so_no = t.dd_so_no
and ifnull(f.ct_total_count_nyc_per_item,0) <> t.total_count_per_item;

/* External Nervousness */



drop table if exists tmp_total_count_comm_date;
create table tmp_total_count_comm_date
as select  dd_so_no, dd_so_item_no,sum(case when dd_Z_LAST_COM_COUNTER = 'Not Set' then 0 else to_number(dd_Z_LAST_COM_COUNTER) end) as total_count_comm_date
from fact_wims
group by dd_so_no, dd_so_item_no;

update fact_wims f 
set f.ct_total_count_comm_date = t.total_count_comm_date
from  fact_wims f, tmp_total_count_comm_date t
where f.dd_so_item_no = t.dd_so_item_no
and f.dd_so_no = t.dd_so_no
and ifnull(f.ct_total_count_comm_date,0) <> t.total_count_comm_date;

/* Manual override frequency */


drop table if exists tmp_count_manual_override;
create table tmp_count_manual_override
as select  dd_so_no, dd_so_item_no,sum(case when dd_Z_MANUAL_OVERRIDE_COUNTER='Not Set' then 0 else dd_Z_MANUAL_OVERRIDE_COUNTER end) as count_manual_override
from fact_wims
group by dd_so_no, dd_so_item_no;

update fact_wims f 
set f.ct_count_manual_override = t.count_manual_override
from  fact_wims f, tmp_count_manual_override t
where f.dd_so_item_no = t.dd_so_item_no
and f.dd_so_no = t.dd_so_no
and ifnull(f.ct_count_manual_override,0) <> t.count_manual_override;

/* 1st Promised Date Accuracy */

drop table if exists tmp_date_diff;
create table tmp_date_diff
as select  f.dd_so_no, f.dd_so_item_no,
sum(DAYS_BETWEEN(agi.datevalue, opd.datevalue)) as date_diff
from fact_wims f, dim_date agi, dim_date opd
where f.dim_dateidagi = agi.dim_dateid
and f.dim_dateidorigpromised = opd.dim_dateid
group by f.dd_so_no, f.dd_so_item_no;

update fact_wims f 
set f.ct_date_diff_accuracy = t.date_diff
from  fact_wims f, tmp_date_diff t
where f.dd_so_item_no = t.dd_so_item_no
and f.dd_so_no = t.dd_so_no
and ifnull(f.ct_date_diff_accuracy,0) <> t.date_diff;

/* Last Promised Date Accuracy */

drop table if exists tmp_date_diff_pd;
create table tmp_date_diff_pd
as select  f.dd_so_no, f.dd_so_item_no,
sum(DAYS_BETWEEN(agi.datevalue, pd.datevalue)) as date_diff_pd
from fact_wims f, dim_date agi, dim_date pd
where f.dim_dateidagi = agi.dim_dateid
and f.dim_dateidlastcompd = pd.dim_dateid
group by f.dd_so_no, f.dd_so_item_no;


update fact_wims f 
set f.ct_date_diff_pd = t.date_diff_pd
from  fact_wims f, tmp_date_diff_pd t
where f.dd_so_item_no = t.dd_so_item_no
and f.dd_so_no = t.dd_so_no
and ifnull(f.ct_date_diff_pd,0) <> t.date_diff_pd;

/* Last VS Original Promised Date  - nu e folosit */


drop table if exists tmp_date_diff_last_vs_orig;
create table tmp_date_diff_last_vs_orig
as select  f.dd_so_no, f.dd_so_item_no,
sum(DAYS_BETWEEN(l.datevalue, o.datevalue)) as date_diff_last_vs_orig
from fact_wims f, dim_date l, dim_date o
where f.dim_dateidlastcompd = l.dim_dateid
and f.dim_dateidorigpromised = o.dim_dateid
group by f.dd_so_no, f.dd_so_item_no;


update fact_wims f 
set f.ct_date_diff_last_vs_orig = t.date_diff_last_vs_orig
from  fact_wims f, tmp_date_diff_last_vs_orig t
where f.dd_so_item_no = t.dd_so_item_no
and f.dd_so_no = t.dd_so_no
and ifnull(f.ct_date_diff_last_vs_orig,0) <> t.date_diff_last_vs_orig;


update fact_wims f
set f.ct_Z_CALC_PD_COUNTER = case when f.dd_Z_CALC_PD_COUNTER = 'Not Set' then 0 else cast(dd_Z_CALC_PD_COUNTER as INT) END;


-- Bucket internal 

update fact_wims f 
set f.dd_bucket_internal_nerv = IFNULL(ct_Z_CALC_PD_COUNTER ,0);


-- Bucket External

update fact_wims f 
set f.dd_bucket_external_nerv = IFNULL(ct_Z_LAST_COM_COUNTER,0);
-- Bucket Manual Override Freq.


update fact_wims f 
set f.dd_bucket_manual_override = IFNULL(ct_Z_MANUAL_OVERRIDE_COUNTER,0);

update fact_wims f_wims 
set f_wims.ct_OriginalPromisedDateAccuracy = src.ct_OriginalPromisedDateAccuracy
FROM (SELECT f_wims.fact_wimsid,
sum(case when f_wims.dim_dateidfirstpromise = 1 THEN
                                               case when f_wims.dim_dateidagi_or_delivery = 1 or f_wims.dim_dateidorigpromised = 1 then 0.0000
                                                    else agiordd.DateValue - opd.DateValue end 
                                              else case when dim_dateidagi_or_delivery = 1 then 0.0000
                                                        else agiordd.DateValue - fpd.DateValue end END)/count(distinct concat(f_wims.dd_so_no,f_wims.dd_so_item_no)) AS ct_OriginalPromisedDateAccuracy 
from  fact_wims f_wims
INNER JOIN dim_date agiordd ON agiordd.dim_dateid = f_wims.dim_dateidagi_or_delivery
INNER JOIN dim_date opd     ON opd.dim_dateid     = f_wims.dim_dateidorigpromised
INNER JOIN dim_date fpd     ON fpd.dim_dateid     = f_wims.dim_dateidfirstpromise
GROUP BY f_wims.fact_wimsid) src
INNER JOIN fact_wims f_wims ON src.fact_wimsid = f_wims.fact_wimsid
where f_wims.ct_OriginalPromisedDateAccuracy <> src.ct_OriginalPromisedDateAccuracy;
;

update fact_wims f_wims 
set f_wims.ct_LastPromisedDateAccuracy = src.ct_LastPromisedDateAccuracy
FROM (SELECT f_wims.fact_wimsid,
sum(case when (f_wims.dim_dateidagi_or_delivery = 1 or f_wims.dim_dateidlastcompd = 1) then 0.0000 
         else agiordd.DateValue - lcpd.DateValue end)/count(distinct concat(f_wims.dd_so_no,f_wims.dd_so_item_no)) AS ct_LastPromisedDateAccuracy 
from  fact_wims f_wims
INNER JOIN dim_date agiordd ON agiordd.dim_dateid = f_wims.dim_dateidagi_or_delivery
INNER JOIN dim_date lcpd    ON lcpd.dim_dateid     = f_wims.dim_dateidlastcompd
GROUP BY f_wims.fact_wimsid) src
INNER JOIN fact_wims f_wims ON src.fact_wimsid = f_wims.fact_wimsid
where f_wims.ct_LastPromisedDateAccuracy <> src.ct_LastPromisedDateAccuracy;
;

update fact_wims f_wims 
set f_wims.ct_LastVSOriginalPromisedDateAccuracy = src.ct_LastVSOriginalPromisedDateAccuracy
FROM (SELECT f_wims.fact_wimsid,
sum(case when f_wims.dim_dateidfirstpromise =1 then 
          case when f_wims.dim_dateidlastcompd =1 or f_wims.dim_dateidorigpromised =1 then 0.0000
                else lcpd.DateValue - opd.DateValue end 
          else case when f_wims.dim_dateidlastcompd =1 then 0.0000 
                     else lcpd.DateValue - fpd.DateValue end end)/count(distinct concat(f_wims.dd_so_no,f_wims.dd_so_item_no)) AS ct_LastVSOriginalPromisedDateAccuracy 
from  fact_wims f_wims
INNER JOIN dim_date lcpd    ON lcpd.dim_dateid     = f_wims.dim_dateidlastcompd
INNER JOIN dim_date opd     ON opd.dim_dateid     = f_wims.dim_dateidorigpromised
INNER JOIN dim_date fpd     ON fpd.dim_dateid     = f_wims.dim_dateidfirstpromise
GROUP BY f_wims.fact_wimsid) src
INNER JOIN fact_wims f_wims ON src.fact_wimsid = f_wims.fact_wimsid
where f_wims.ct_LastVSOriginalPromisedDateAccuracy <> src.ct_LastVSOriginalPromisedDateAccuracy;
;

/* Copy to emd586 */
TRUNCATE TABLE emd586.FACT_WIMS;
INSERT INTO emd586.FACT_WIMS (
	FACT_WIMSID, 
	DD_EVENT_CODE, 
	DD_SOURCE, 
	DD_GUID_CODE, 
	DD_SO_NO, 
	DD_SO_ITEM_NO, 
	DD_MATERIALNO, 
	DIM_DATEIDDLV, 
	DIM_DATEIDMATAVLB, 
	DIM_DATEIDCALCPROMISED, 
	DIM_DATEIDORIGMATAVL, 
	DIM_DATEIDORIGPROMISED, 
	DIM_DATEIDRDD, 
	DIM_DATEIDLASTCOMPD, 
	DIM_DATEIDFIRSTPROMISE, 
	DIM_DATEIDCUSTOMERREQUESTED, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	DIM_PROJECTSOURCEID, 
	DD_Z_DLV_DATE, 
	DD_Z_MAT_AVLB_DATE, 
	DD_Z_CALC_PROMISED_DATE, 
	DD_Z_ORIG_MAT_AVL_DT, 
	DD_Z_RDD_DATE, 
	DD_LAST_COM_PD, 
	DD_Z_FIRST_PROMISE_DATE, 
	DD_Z_CUSTOMER_REQUESTED_DATE, 
	DD_DELIVERYBLOCKCODE, 
	DD_DELIVERYBLOCKDESC, 
	DIM_DATEIDSOCREATED, 
	CT_ITEMQTY, 
	DD_UOM, 
	DIM_PLANTID, 
	DD_GLBPRODUCTGROUP, 
	DD_PARTDESC, 
	DD_DESTCOUNTRY, 
	DD_OVERALLDELIVERYSTATUS, 
	DD_DESTCOUNTRYDESC, 
	DD_SALESORGKEY, 
	DD_SALESORGDESC, 
	DD_SALESOFFICEDESC, 
	DD_SALESOFFICEKEY, 
	DD_SALESGROUPDESC, 
	DD_SALESGROUPKEY, 
	DD_SOLDTOCUSTKEY, 
	DD_SOLDTOCUST, 
	DD_SHIPTOCUSTKEY, 
	DD_SHIPTOCUST, 
	DD_Z_LAST_MAT_CHG_DATE, 
	DD_Z_NEW_RE_PD_NT_YT_COM, 
	DD_Z_MANUAL_OVERRIDE_DATE, 
	DIM_DATEIDLASTMATERIALAVLBCH, 
	DIM_DATEIDREPROMISEDATE, 
	DIM_DATEIDMANUALOVERRIDE, 
	DIM_DATEIDLASTCOMMUNICATEDCOUNTER, 
	DIM_DATEIDORDERLINECONF, 
	DD_Z_LAST_COM_COUNTER, 
	DD_Z_ORDER_LINE_CONFIRM, 
	DD_Z_MANUAL_OVERRIDE_COUNTER, 
	DD_Z_CALC_PD_COUNTER, 
	DIM_DATEIDAGI, 
	DD_Z_REPROMISED_DT_NT_YT_COM_COUN, 
	CT_TOTAL_COUNT_NYC_PER_ITEM, 
	CT_TOTAL_COUNT_COMM_DATE, 
	CT_COUNT_MANUAL_OVERRIDE, 
	CT_DATE_DIFF_ACCURACY, 
	CT_DATE_DIFF_PD, 
	CT_DATE_DIFF_LAST_VS_ORIG, 
	DD_BUCKET_INTERNAL_NERV, 
	DD_BUCKET_EXTERNAL_NERV, 
	DD_BUCKET_MANUAL_OVERRIDE, 
	CT_Z_MANUAL_OVERRIDE_COUNTER, 
	CT_Z_REPROMISED_DT_NT_YT_COM_COUN, 
	CT_Z_LAST_COM_COUNTER, 
	DD_Z_ORIG_PROMISED_DATE, 
	DD_SOURCEID, 
	DD_ACTUALGOODSISSUEDATE, 
	CT_Z_CALC_PD_COUNTER, 
	DIM_DATEIDWIMSACTUALGOODISSUE, 
	DIM_DATEIDAGI_OR_DELIVERY, 
	DIM_PARTID,
	ct_OriginalPromisedDateAccuracy,
	ct_LastPromisedDateAccuracy,
	ct_LastVSOriginalPromisedDateAccuracy,
	DIM_COMMERCIALVIEWID,
	dd_BiFilters_PRE_PRD,
	dd_intercompflag,
	Dim_ProductHierarchyid)
SELECT
	FACT_WIMSID, 
	DD_EVENT_CODE, 
	DD_SOURCE, 
	DD_GUID_CODE, 
	DD_SO_NO, 
	DD_SO_ITEM_NO, 
	DD_MATERIALNO, 
	DIM_DATEIDDLV, 
	DIM_DATEIDMATAVLB, 
	DIM_DATEIDCALCPROMISED, 
	DIM_DATEIDORIGMATAVL, 
	DIM_DATEIDORIGPROMISED, 
	DIM_DATEIDRDD, 
	DIM_DATEIDLASTCOMPD, 
	DIM_DATEIDFIRSTPROMISE, 
	DIM_DATEIDCUSTOMERREQUESTED, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	DIM_PROJECTSOURCEID, 
	DD_Z_DLV_DATE, 
	DD_Z_MAT_AVLB_DATE, 
	DD_Z_CALC_PROMISED_DATE, 
	DD_Z_ORIG_MAT_AVL_DT, 
	DD_Z_RDD_DATE, 
	DD_LAST_COM_PD, 
	DD_Z_FIRST_PROMISE_DATE, 
	DD_Z_CUSTOMER_REQUESTED_DATE, 
	DD_DELIVERYBLOCKCODE, 
	DD_DELIVERYBLOCKDESC, 
	DIM_DATEIDSOCREATED, 
	CT_ITEMQTY, 
	DD_UOM, 
	DIM_PLANTID, 
	DD_GLBPRODUCTGROUP, 
	DD_PARTDESC, 
	DD_DESTCOUNTRY, 
	DD_OVERALLDELIVERYSTATUS, 
	DD_DESTCOUNTRYDESC, 
	DD_SALESORGKEY, 
	DD_SALESORGDESC, 
	DD_SALESOFFICEDESC, 
	DD_SALESOFFICEKEY, 
	DD_SALESGROUPDESC, 
	DD_SALESGROUPKEY, 
	DD_SOLDTOCUSTKEY, 
	DD_SOLDTOCUST, 
	DD_SHIPTOCUSTKEY, 
	DD_SHIPTOCUST, 
	DD_Z_LAST_MAT_CHG_DATE, 
	DD_Z_NEW_RE_PD_NT_YT_COM, 
	DD_Z_MANUAL_OVERRIDE_DATE, 
	DIM_DATEIDLASTMATERIALAVLBCH, 
	DIM_DATEIDREPROMISEDATE, 
	DIM_DATEIDMANUALOVERRIDE, 
	DIM_DATEIDLASTCOMMUNICATEDCOUNTER, 
	DIM_DATEIDORDERLINECONF, 
	DD_Z_LAST_COM_COUNTER, 
	DD_Z_ORDER_LINE_CONFIRM, 
	DD_Z_MANUAL_OVERRIDE_COUNTER, 
	DD_Z_CALC_PD_COUNTER, 
	DIM_DATEIDAGI, 
	DD_Z_REPROMISED_DT_NT_YT_COM_COUN, 
	CT_TOTAL_COUNT_NYC_PER_ITEM, 
	CT_TOTAL_COUNT_COMM_DATE, 
	CT_COUNT_MANUAL_OVERRIDE, 
	CT_DATE_DIFF_ACCURACY, 
	CT_DATE_DIFF_PD, 
	CT_DATE_DIFF_LAST_VS_ORIG, 
	DD_BUCKET_INTERNAL_NERV, 
	DD_BUCKET_EXTERNAL_NERV, 
	DD_BUCKET_MANUAL_OVERRIDE, 
	CT_Z_MANUAL_OVERRIDE_COUNTER, 
	CT_Z_REPROMISED_DT_NT_YT_COM_COUN, 
	CT_Z_LAST_COM_COUNTER, 
	DD_Z_ORIG_PROMISED_DATE, 
	DD_SOURCEID, 
	DD_ACTUALGOODSISSUEDATE, 
	CT_Z_CALC_PD_COUNTER, 
	DIM_DATEIDWIMSACTUALGOODISSUE, 
	DIM_DATEIDAGI_OR_DELIVERY, 
	DIM_PARTID,
	ct_OriginalPromisedDateAccuracy,
	ct_LastPromisedDateAccuracy,
	ct_LastVSOriginalPromisedDateAccuracy,
	DIM_COMMERCIALVIEWID,
	dd_BiFilters_PRE_PRD,
	dd_intercompflag,
	Dim_ProductHierarchyid
FROM FACT_WIMS;



