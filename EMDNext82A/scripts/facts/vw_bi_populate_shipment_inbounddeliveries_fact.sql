
DROP TABLE IF EXISTS fact_shipmentindelivery_temp;

CREATE TABLE fact_shipmentindelivery_temp LIKE fact_shipmentindelivery INCLUDING DEFAULTS INCLUDING IDENTITY;

delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';

  INSERT INTO NUMBER_FOUNTAIN
  select 'fact_shipmentindelivery_temp',ifnull((select DIM_PROJECTSOURCEID*MULTIPLIER from dim_projectsource),1);


DROP TABLE IF EXISTS tmp_populate_shipment_t2;
CREATE TABLE tmp_populate_shipment_t2
AS
SELECT row_number() over (order by '') rid,
  v1.VTTK_TKNUM dd_ShipmentNo, /* dd_ShipmentNo */
  v1.VTTP_TPNUM dd_ShipmentItemNo, /* dd_ShipmentItemNo */
  ifnull(v1.VTTP_VBELN, 'Not Set') dd_InboundDeliveryNo, /* dd_InboundDeliveryNo */
  ifnull(v.LIPS_POSNR,0) dd_InboundDeliveryItemNo,
  ifnull(v.EKES_ETENS,0) dd_seqnoofvendorconf,
  EKES_EBELN dd_DocumentNo, /* dd_DocumentNo */
  EKES_EBELP dd_DocumentItemNo, /* dd_DocumentItemNo */
  p.dd_ScheduleNo dd_ScheduleNo, /* dd_ScheduleNo */
  CONVERT(BIGINT, 1) Dim_DateIdShipmentCreated,      /*  Dim_DateIdShipmentCreated  */
  ifnull(VTTK_TPBEZ, 'Not Set') dd_Vessel, /* dd_Vessel */
  ifnull(VTTK_TEXT4, 'Not Set') dd_MovementType, /* dd_MovementType */
  ifnull(VTTK_TEXT3, 'Not Set') dd_CarrierActual,  /* dd_CarrierActual */
  CONVERT(BIGINT, 1) Dim_CarrierBillingId,  /* Dim_CarrierBillingId */
  ifnull(VTTK_SIGNI, 'Not Set') dd_DomesticContainerID,  /* dd_DomesticContainerID */
  CONVERT(BIGINT, 1) Dim_ShipmentTypeId,    /* Dim_ShipmentTypeId */
  CONVERT(BIGINT, 1) Dim_RouteId,   /* Dim_RouteId */
  ifnull(VTTK_EXTI2, 'Not Set') dd_VoyageNo,  /* dD_VoyageNo */
  CONVERT(BIGINT, 1) Dim_DateIdShipmentEnd,
  CONVERT(BIGINT, 1) Dim_DateIdDepartureFromOrigin,
  ifnull(VTTK_ADD01, 'Not Set') dd_ContainerSize,   /* dd_ContainerSize  */
  'Not Set' dd_3PLFlag,  /* dd_3PLFlag */
  ifnull(LIKP_VOLUM, 0.0000) ct_InboundDlvryCMtrVolume,
  CONVERT(BIGINT, 1) Dim_DateidDlvrDocCreated,
  p.dim_plantidordering Dim_PlantId,  /* Dim_PlantId */
  ifnull((CASE WHEN VTTK_EXTI1 IS NULL THEN LIKP_BOLNR ELSE VTTK_EXTI1 END),'Not Set') dd_billofladingNo, /* dd_billofladingNo */
  p.dim_DateIdDelivery Dim_DateIdPODelivery,  /* Dim_DateIdPODelivery */
  ifnull(EKES_DABMG, 0.0000) ct_DelivReceivedQty,  /* ct_DelivReceivedQty */
  ifnull(EKES_MENGE, 0.0000) ct_ReceivedQty,
  CONVERT(BIGINT, 1) dim_inbrouteid,
  p.ct_ItemQty,
  p.Dim_AFSTranspConditionid Dim_AFSTranspConditionid, /* Dim_AFSTranspConditionid */
  p.Dim_DateidOrder dim_dateidactualpo,  /*  dim_dateidactualpo */
  p.dim_dateidAFSExFactEst dim_dateidAFSExFactEst,   /* dim_dateidAFSExFactEst */
  p.dim_dateidMatAvailability dim_dateidMatAvailability,  /* dim_dateidMatAvailability */
  p.dd_DeliveryNumber dd_DeliveryNumber,   /* dd_DeliveryNumber */
  CONVERT(BIGINT, 1) dim_dateidTransfer,
  ifnull(LIKP_BTGEW,0.0000) ct_DeliveryGrossWght,
  ifnull(LIKP_NTGEW,0.0000) ct_deliveryNetWght,
  CONVERT(BIGINT, 1)  Dim_DeliveryRouteId,
  CONVERT(BIGINT, 1) dim_shippingconditionid,
  CONVERT(BIGINT, 1) dim_tranportbyshippingtypeid,
  p.dd_ShortText,
  ifnull(VTTK_TEXT1,'Not Set') as dd_sealnumber,
  p.dd_noofforeigntrade,
  ifnull(LIKP_ANZPK, 0) dd_PackageCount,
  CONVERT(BIGINT, 1) dim_transportconditionid,
  p.dim_shippinginstructionid,
  ifnull(v.EKES_EBTYP,'Not Set') dd_confirmationcateg,
  ifnull(v.EKES_ESTKZ,'Not Set') dd_creationindvendorconf,
  CONVERT(BIGINT, 1) dim_deliverydateofvendorconf,
  p.dim_mdg_partid,
  v1.VTTK_ERDAT,  /*   Dim_DateIdShipmentCreated */
  v1.VTTK_TDLNR,  /*   Dim_CarrierBillingId */
  v1.VTTK_SHTYP,  /*   Dim_ShipmentTypeId */
  v1.VTTK_ROUTE,  /*   Dim_RouteId */
  v1.VTTK_DPTEN,  /*   Dim_DateIdShipmentEnd */
  v1.VTTK_DATBG,  /*   Dim_DateIdDepartureFromOrigin */
  v.LIKP_ERDAT,  /*   Dim_DateidDlvrDocCreated */
  v.LIKP_ROUTE,  /*   dim_inbrouteid , Dim_DeliveryRouteId */
  v.LIKP_BLDAT,  /*   dim_dateidTransfer */
  v.LIKP_VSBED,  /*   dim_shippingconditionid */
  v.LIKP_VSART,  /*   dim_tranportbyshippingtypeid */
  v1.VTTK_VSART,   /*  dim_transportconditionid */
  v.EKES_EINDT /* dim_deliverydateofvendorconf */
FROM VTTK_VTTP v1
  INNER JOIN EKES_LIKP_LIPS v ON v1.VTTP_VBELN = v.LIKP_VBELN
				  /* AND v.LIKP_WADAT_IST IS NOT NULL */
  INNER JOIN facT_purchase p ON
    p.dd_DocumentNo = EKES_EBELN
    AND p.dd_DocumentItemNo = EKES_EBELP
    /*AND p.dd_ScheduleNo = EKES_J_3AETENR*/
/*WHERE NOT EXISTS
   (SELECT 1
    FROM fact_shipmentindelivery_temp f1
    WHERE      f1.dd_ShipmentNo = VTTK_TKNUM
           AND f1.dd_ShipmentItemNo = VTTP_TPNUM
           AND f1.dd_InboundDeliveryNo = LIKP_VBELN
           AND f1.dd_DocumentNo = EKES_EBELN
           AND f1.dd_DocumentItemNo = EKES_EBELP
           AND f1.dd_ScheduleNo = EKES_J_3AETENR)	*/;

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DateIdShipmentCreated = ifnull (d.Dim_Dateid, 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.VTTK_ERDAT
                                 AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_CarrierBillingId = ifnull (v.Dim_VendorId , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Vendor v ON v.VendorNumber = t.VTTK_TDLNR
								   AND RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_ShipmentTypeId = ifnull (st.dim_Shipmenttypeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_ShipmentType st ON st.ShipmentType = t.VTTK_SHTYP
                                          AND st.RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_RouteId = ifnull (r.dim_Routeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.VTTK_ROUTE
								  AND r.RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DateIdShipmentEnd = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.VTTK_DPTEN
                                 AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DateIdDepartureFromOrigin = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.VTTK_DATBG
                                 AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DateidDlvrDocCreated = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.LIKP_ERDAT
                                 AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_inbrouteid = ifnull (r.dim_Routeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.LIKP_ROUTE
								  AND r.RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_dateidTransfer = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.LIKP_BLDAT
                                 AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_deliverydateofvendorconf = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t2 t
           LEFT JOIN Dim_Date d ON d.datevalue = t.EKES_EINDT
                                AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t2 t
SET  t.Dim_DeliveryRouteId = ifnull (r.dim_Routeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.LIKP_ROUTE
								  AND r.RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_shippingconditionid = ifnull (s.dim_shippingconditionid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_shippingcondition s ON s.shippingconditioncode = t.LIKP_VSBED
								              AND s.rowiscurrent = 1;

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_tranportbyshippingtypeid = ifnull (tbst.dim_tranportbyshippingtypeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_tranportbyshippingtype tbst ON tbst.ShippingType = t.LIKP_VSART;

UPDATE tmp_populate_shipment_t2 t
SET  t.dim_transportconditionid = ifnull (tbst.dim_tranportbyshippingtypeid , 1)
FROM tmp_populate_shipment_t2 t
            LEFT JOIN dim_tranportbyshippingtype tbst ON tbst.ShippingType = t.VTTK_VSART;

INSERT INTO fact_shipmentindelivery_temp
	(fact_shipmentindeliveryid,
					 dd_ShipmentNo,
					 dd_ShipmentItemNo,
					 dd_InboundDeliveryNo,
					 dd_DocumentNo,
					 dd_DocumentItemNo,
					 dd_ScheduleNo,
					 Dim_DateIdShipmentCreated,
					 dd_Vessel,
					 dd_MovementType,
					 dd_CarrierActual,
					 Dim_CarrierBillingId,
					 dd_DomesticContainerID,
					 Dim_ShipmentTypeId,
					 Dim_RouteId,
					 dD_VoyageNo,
					 Dim_DateIdShipmentEnd,
					 Dim_DateIdDepartureFromOrigin,
					 dd_ContainerSize,
					 dd_3PLFlag,
					 ct_InboundDlvryCMtrVolume,
					 Dim_DateidDlvrDocCreated,
					 Dim_PlantId,
					 dd_billofladingNo,
					 Dim_DateIdPODelivery,
					 ct_DelivReceivedQty,
					 ct_ReceivedQty,
					 dim_inbrouteid,
					 ct_ItemQty,
					 Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
					 dim_dateidactualpo, /* Roxana 02 dec 2014 */
					 dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
					 dim_dateidMatAvailability,  /* Marius 15 Dec 2014 */
					 dd_DeliveryNumber,          /* Marius 17 Dec 2014 */
					 dim_dateidTransfer, /* Marius 17 Dec 2014 */
					 ct_DeliveryGrossWght, /* Marius 17 Dec 2014 */
					 ct_deliveryNetWght, /* Marius 17 Dec 2014 */
					 Dim_DeliveryRouteId, /* Marius 17 Dec 2014 */
					 dim_shippingconditionid, /* Marius 17 Dec 2014 */
					 dim_tranportbyshippingtypeid, /* Marius 6 Jan 2014 */
					 dd_ShortText,
					 dd_sealnumber,   /* Marius 5 feb 2015 */
					 dd_noofforeigntrade , /* Marius 15 feb 2015 */
					 dd_PackageCount, /* Marius 4 mar 2015 */
					 dim_transportconditionid, /*Roxana 08 may 2015*/
           dd_seqnoofvendorconf,
           dd_InboundDeliveryItemNo,
           dim_shippinginstructionid,
           dd_confirmationcateg,
           dd_creationindvendorconf,
           dim_deliverydateofvendorconf,
           dim_mdg_partid)
SELECT ((SELECT ifnull(max_id, 0)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_shipmentindelivery_temp') +
        			 row_number() over (order by '')) fact_shipmentindeliveryid,
          a.*
	FROM
    (SELECT DISTINCT
      dd_ShipmentNo,
      dd_ShipmentItemNo,
      dd_InboundDeliveryNo,
      dd_DocumentNo,
      dd_DocumentItemNo,
      dd_ScheduleNo,
      Dim_DateIdShipmentCreated,
      dd_Vessel,
      dd_MovementType,
      dd_CarrierActual,
      Dim_CarrierBillingId,
      dd_DomesticContainerID,
      Dim_ShipmentTypeId,
      Dim_RouteId,
      dD_VoyageNo,
      Dim_DateIdShipmentEnd,
      Dim_DateIdDepartureFromOrigin,
      dd_ContainerSize,
      dd_3PLFlag,
      ct_InboundDlvryCMtrVolume,
      Dim_DateidDlvrDocCreated,
      Dim_PlantId,
      dd_billofladingNo,
      Dim_DateIdPODelivery,
      ct_DelivReceivedQty,
      ct_ReceivedQty,
      dim_inbrouteid,
      ct_ItemQty,
      Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
      dim_dateidactualpo, /* Roxana 02 dec 2014 */
      dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
      dim_dateidMatAvailability,  /* Marius 15 Dec 2014 */
      dd_DeliveryNumber,          /* Marius 17 Dec 2014 */
      dim_dateidTransfer, /* Marius 17 Dec 2014 */
      ct_DeliveryGrossWght, /* Marius 17 Dec 2014 */
      ct_deliveryNetWght, /* Marius 17 Dec 2014 */
      Dim_DeliveryRouteId, /* Marius 17 Dec 2014 */
      dim_shippingconditionid, /* Marius 17 Dec 2014 */
      dim_tranportbyshippingtypeid, /* Marius 6 Jan 2014 */
      dd_ShortText,
      dd_sealnumber,   /* Marius 5 feb 2015 */
      dd_noofforeigntrade , /* Marius 15 feb 2015 */
      dd_PackageCount, /* Marius 4 mar 2015 */
      dim_transportconditionid,
      dd_seqnoofvendorconf,
      dd_InboundDeliveryItemNo,
      dim_shippinginstructionid,
      dd_confirmationcateg,
      dd_creationindvendorconf,
      dim_deliverydateofvendorconf,
      dim_mdg_partid
		FROM tmp_populate_shipment_t2 ) a;




delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery_temp',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery_temp;

DROP TABLE IF EXISTS tmp_populate_shipment_t3;
CREATE TABLE tmp_populate_shipment_t3
AS
SELECT row_number() over (order by '') rid, a.* from (
SELECT
  p.dd_DocumentNo,
  p.dd_DocumentItemNo,
  p.dd_ScheduleNo,
  'Not Set' dd_ShipmentNo,
  0 dd_ShipmentItemNo,
  LIKP_VBELN dd_InboundDeliveryNo,
  ifnull(v.LIPS_POSNR,0) dd_InboundDeliveryItemNo,
  ifnull(v.EKES_ETENS,0) dd_seqnoofvendorconf,
  CONVERT(BIGINT, 1)  Dim_DateIdShipmentCreated,
  'Not Set' dd_Vessel,
  'Not Set' dd_MovementType,
  'Not Set' dd_CarrierActual,
  CONVERT(BIGINT, 1) Dim_CarrierBillingId,
  'Not Set' dd_DomesticContainerID,
  CONVERT(BIGINT, 1) Dim_ShipmentTypeId,
  CONVERT(BIGINT, 1) Dim_RouteId,
  'Not Set' dD_VoyageNo,
  CONVERT(BIGINT, 1) Dim_DateIdShipmentEnd,
  CONVERT(BIGINT, 1) Dim_DateIdDepartureFromOrigin,
  'Not Set' dd_ContainerSize,
  'Not Set' dd_3PLFlag ,
  p.Dim_DateIdSchedOrder Dim_DateIdPOBuy,
  p.Dim_DocumentTypeid Dim_PODocumentTypeId,
  p.Dim_DateidAFSDelivery Dim_DateIdPOETA,
  p.dim_purchasegroupid,
  p.Dim_partid,
  p.Dim_Vendorid,
  p.Dim_DateIdLastGR Dim_DateIdGR,
  p.dim_AFSRouteid Dim_PORouteId,
  p.Dim_CustomPartnerFunctionId4 Dim_CustomVendorPartnerFunctionId,
  p.Dim_CustomPartnerFunctionId3 Dim_CustomVendorPartnerFunctionId1,
  p.Dim_AfsSeasonId Dim_POAFSSeasonId,
  p.ct_DeliveryQty ct_POScheduleQty,
  p.ct_ReceivedQty ct_POItemReceivedQty,
  p.ct_QtyReduced ct_POInboundDeliveryQty,
  'Not Set' dd_CustomerPOStatus,
  ifnull(LIKP_ANZPK, 0) dd_PackageCount,
  ifnull(LIKP_BOLNR, 'Not Set') dd_BillofLadingNo,
  p.Dim_CompanyId Dim_CompanyId,
  p.Dim_plantidordering Dim_PlantId,
  ifnull(LIKP_TRAID, 'Not Set') dd_OriginContainerId,
  ifnull(LIKP_VOLUM, 0.0000) ct_InboundDlvryCMtrVolume,
  CONVERT(BIGINT, 1) Dim_DateidDlvrDocCreated,
  p.Dim_DateIdDelivery Dim_DateIdPODelivery,
  ifnull(EKES_DABMG, 0.0000) ct_DelivReceivedQty,
  ifnull(EKES_MENGE, 0.0000) ct_ReceivedQty,
  'Not Set' dd_ShipmentStatus,
  CONVERT(BIGINT, 1) dim_inbrouteid,
  p.ct_ItemQty,
  p.Dim_AFSTranspConditionid Dim_AFSTranspConditionid, /* MArius 28 nov 2014 */
  p.Dim_DateidOrder dim_dateidactualpo,
  p.dim_dateidAFSExFactEst dim_dateidAFSExFactEst,   /* MArius 03 Dec 2014 */
  p.dim_dateidMatAvailability dim_dateidMatAvailability,
  p.dd_DeliveryNumber,
  CONVERT(BIGINT, 1) dim_dateidTransfer,
  ifnull(LIKP_BTGEW,0.0000) ct_DeliveryGrossWght,
  ifnull(LIKP_NTGEW,0.0000) ct_deliveryNetWght,
  CONVERT(BIGINT, 1) Dim_DeliveryRouteId,
  CONVERT(BIGINT, 1) dim_shippingconditionid,
  CONVERT(BIGINT, 1) dim_tranportbyshippingtypeid,
  p.dd_ShortText dd_ShortText,
  p.dd_noofforeigntrade dd_noofforeigntrade,
  p.dim_shippinginstructionid,
  ifnull(v.EKES_EBTYP,'Not Set') dd_confirmationcateg,
  ifnull(v.EKES_ESTKZ,'Not Set') dd_creationindvendorconf,
  CONVERT(BIGINT, 1) dim_deliverydateofvendorconf,
  p.dim_mdg_partid,
  v.LIKP_ERDAT,
  v.LIKP_ROUTE,
  v.LIKP_BLDAT,
  v.LIKP_VSBED,
  v.LIKP_VSART,
  v.EKES_EINDT
FROM facT_purchase p
      INNER JOIN EKES_LIKP_LIPS v
         ON     dd_DocumentNo = EKES_EBELN
            AND dd_DocumentItemNo = EKES_EBELP
          /*  AND dd_ScheduleNo = EKES_J_3AETENR*/
WHERE NOT EXISTS
             (SELECT 1
                FROM fact_shipmentindelivery_temp f1
               WHERE     f1.dd_InboundDeliveryNo = LIKP_VBELN
                     AND f1.dd_InboundDeliveryItemNo = LIPS_POSNR
                     AND f1.dd_seqnoofvendorconf = EKES_ETENS
                     AND f1.dd_DocumentNo = EKES_EBELN
                     AND f1.dd_DocumentItemNo = EKES_EBELP
                     AND f1.dd_ScheduleNo = p.dd_ScheduleNo)
UNION
SELECT
  p.dd_DocumentNo,
  p.dd_DocumentItemNo,
  p.dd_ScheduleNo,
  'Not Set' dd_ShipmentNo,
  0 dd_ShipmentItemNo,
  ifnull(LIKP_VBELN,'Not Set') dd_InboundDeliveryNo,
  ifnull(v.LIPS_POSNR,0) dd_InboundDeliveryItemNo,
  ifnull(v.EKES_ETENS,0) dd_seqnoofvendorconf,
  CONVERT(BIGINT, 1)  Dim_DateIdShipmentCreated,
  'Not Set' dd_Vessel,
  'Not Set' dd_MovementType,
  'Not Set' dd_CarrierActual,
  CONVERT(BIGINT, 1) Dim_CarrierBillingId,
  'Not Set' dd_DomesticContainerID,
  CONVERT(BIGINT, 1) Dim_ShipmentTypeId,
  CONVERT(BIGINT, 1) Dim_RouteId,
  'Not Set' dD_VoyageNo,
  CONVERT(BIGINT, 1) Dim_DateIdShipmentEnd,
  CONVERT(BIGINT, 1) Dim_DateIdDepartureFromOrigin,
  'Not Set' dd_ContainerSize,
  'Not Set' dd_3PLFlag ,
  p.Dim_DateIdSchedOrder Dim_DateIdPOBuy,
  p.Dim_DocumentTypeid Dim_PODocumentTypeId,
  p.Dim_DateidAFSDelivery Dim_DateIdPOETA,
  p.dim_purchasegroupid,
  p.Dim_partid,
  p.Dim_Vendorid,
  p.Dim_DateIdLastGR Dim_DateIdGR,
  p.dim_AFSRouteid Dim_PORouteId,
  p.Dim_CustomPartnerFunctionId4 Dim_CustomVendorPartnerFunctionId,
  p.Dim_CustomPartnerFunctionId3 Dim_CustomVendorPartnerFunctionId1,
  p.Dim_AfsSeasonId Dim_POAFSSeasonId,
  p.ct_DeliveryQty ct_POScheduleQty,
  p.ct_ReceivedQty ct_POItemReceivedQty,
  p.ct_QtyReduced ct_POInboundDeliveryQty,
  'Not Set' dd_CustomerPOStatus,
  ifnull(LIKP_ANZPK, 0) dd_PackageCount,
  ifnull(LIKP_BOLNR, 'Not Set') dd_BillofLadingNo,
  p.Dim_CompanyId Dim_CompanyId,
  p.Dim_plantidordering Dim_PlantId,
  ifnull(LIKP_TRAID, 'Not Set') dd_OriginContainerId,
  ifnull(LIKP_VOLUM, 0.0000) ct_InboundDlvryCMtrVolume,
  CONVERT(BIGINT, 1) Dim_DateidDlvrDocCreated,
  p.Dim_DateIdDelivery Dim_DateIdPODelivery,
  ifnull(EKES_DABMG, 0.0000) ct_DelivReceivedQty,
  ifnull(EKES_MENGE, 0.0000) ct_ReceivedQty,
  'Not Set' dd_ShipmentStatus,
  CONVERT(BIGINT, 1) dim_inbrouteid,
  p.ct_ItemQty,
  p.Dim_AFSTranspConditionid Dim_AFSTranspConditionid, /* MArius 28 nov 2014 */
  p.Dim_DateidOrder dim_dateidactualpo,
  p.dim_dateidAFSExFactEst dim_dateidAFSExFactEst,   /* MArius 03 Dec 2014 */
  p.dim_dateidMatAvailability dim_dateidMatAvailability,
  p.dd_DeliveryNumber,
  CONVERT(BIGINT, 1) dim_dateidTransfer,
  ifnull(LIKP_BTGEW,0.0000) ct_DeliveryGrossWght,
  ifnull(LIKP_NTGEW,0.0000) ct_deliveryNetWght,
  CONVERT(BIGINT, 1) Dim_DeliveryRouteId,
  CONVERT(BIGINT, 1) dim_shippingconditionid,
  CONVERT(BIGINT, 1) dim_tranportbyshippingtypeid,
  p.dd_ShortText dd_ShortText,
  p.dd_noofforeigntrade dd_noofforeigntrade,
  p.dim_shippinginstructionid,
  ifnull(v.EKES_EBTYP,'Not Set') dd_confirmationcateg,
  ifnull(v.EKES_ESTKZ,'Not Set') dd_creationindvendorconf,
  CONVERT(BIGINT, 1) dim_deliverydateofvendorconf,
  p.dim_mdg_partid,
  v.LIKP_ERDAT,
  v.LIKP_ROUTE,
  v.LIKP_BLDAT,
  v.LIKP_VSBED,
  v.LIKP_VSART,
  v.EKES_EINDT
FROM facT_purchase p
      INNER JOIN EKES v
         ON     dd_DocumentNo = EKPO_EBELN
            AND dd_DocumentItemNo = EKPO_EBELP
          /*  AND dd_ScheduleNo = EKES_J_3AETENR*/
WHERE NOT EXISTS
             (SELECT 1
                FROM fact_shipmentindelivery_temp f1
               WHERE     f1.dd_InboundDeliveryNo = LIKP_VBELN
                     AND f1.dd_InboundDeliveryItemNo = LIPS_POSNR
                     AND f1.dd_seqnoofvendorconf = EKES_ETENS
                     AND f1.dd_DocumentNo = EKES_EBELN
                     AND f1.dd_DocumentItemNo = EKES_EBELP
                     AND f1.dd_ScheduleNo = p.dd_ScheduleNo)
) a;

UPDATE tmp_populate_shipment_t3 t
SET t.Dim_DateidDlvrDocCreated	= ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t3 t
            LEFT JOIN Dim_Date d ON d.datevalue = t.LIKP_ERDAT
                                 AND d.CompanyCode = 'Not Set';
UPDATE tmp_populate_shipment_t3 t
SET t.dim_inbrouteid = ifnull (r.dim_Routeid , 1)
FROM tmp_populate_shipment_t3 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.LIKP_ROUTE
                                 AND r.RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t3 t
SET t.dim_dateidTransfer = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t3 t
            LEFT JOIN Dim_Date d ON  d.datevalue = t.LIKP_BLDAT
                                 AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t3 t
SET  t.dim_deliverydateofvendorconf = ifnull (d.Dim_Dateid , 1)
FROM tmp_populate_shipment_t3 t
          LEFT JOIN Dim_Date d ON d.datevalue = t.EKES_EINDT
                               AND d.CompanyCode = 'Not Set';

UPDATE tmp_populate_shipment_t3 t
SET t.Dim_DeliveryRouteId = ifnull (r.dim_Routeid , 1)
FROM tmp_populate_shipment_t3 t
            LEFT JOIN dim_Route r ON r.RouteCode = t.LIKP_ROUTE
                                 AND r.RowIsCurrent = 1;

UPDATE tmp_populate_shipment_t3 t
SET t.dim_shippingconditionid = ifnull (s.dim_shippingconditionid , 1)
FROM tmp_populate_shipment_t3 t
            LEFT JOIN dim_shippingcondition s ON s.shippingconditioncode = t.LIKP_VSBED
 			                                  AND s.rowiscurrent = 1;

UPDATE tmp_populate_shipment_t3 t
SET t.dim_tranportbyshippingtypeid = ifnull (tbst.dim_tranportbyshippingtypeid , 1)
FROM tmp_populate_shipment_t3 t
            LEFT JOIN dim_tranportbyshippingtype tbst ON tbst.ShippingType = t.LIKP_VSART;

INSERT INTO fact_shipmentindelivery_temp(fact_shipmentindeliveryid,
  dd_DocumentNo,
  dd_DocumentItemNo,
  dd_ScheduleNo,
  dd_ShipmentNo,
  dd_ShipmentItemNo,
  dd_InboundDeliveryNo,
  Dim_DateIdShipmentCreated,
  dd_Vessel,
  dd_MovementType,
  dd_CarrierActual,
  Dim_CarrierBillingId,
  dd_DomesticContainerID,
  Dim_ShipmentTypeId,
  Dim_RouteId,
  dD_VoyageNo,
  Dim_DateIdShipmentEnd,
  Dim_DateIdDepartureFromOrigin,
  dd_ContainerSize,
  dd_3PLFlag,
  Dim_DateIdPOBuy,
  Dim_PODocumentTypeId,
  Dim_DateIdPOETA,
  dim_purchasegroupid,
  Dim_partid,
  Dim_Vendorid,
  Dim_DateIdGR,
  Dim_PORouteId,
  Dim_CustomVendorPartnerFunctionId,
  Dim_CustomVendorPartnerFunctionId1,
  Dim_POAFSSeasonId,
  ct_POScheduleQty,
  ct_POItemReceivedQty,
  ct_POInboundDeliveryQty,
  dd_CustomerPOStatus,
  dd_PackageCount,
  dd_BillofLadingNo,
  Dim_CompanyId,
  Dim_PlantId,
  dd_OriginContainerId,
  ct_InboundDlvryCMtrVolume,
  Dim_DateidDlvrDocCreated,
  Dim_DateIdPODelivery,
  ct_DelivReceivedQty,
  ct_ReceivedQty,
  dd_ShipmentStatus,
  dim_inbrouteid,
  ct_ItemQty,
  Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
  dim_dateidactualpo,  /* Roxana 02 dec 2014 */
  dim_dateidAFSExFactEst,  /* Marius 03 Dec 2014 */
  dim_dateidMatAvailability,
  dd_DeliveryNumber,
  dim_dateidTransfer,
  ct_DeliveryGrossWght,
  ct_deliveryNetWght,
  Dim_DeliveryRouteId,
  dim_shippingconditionid,
  dim_tranportbyshippingtypeid,
  dd_ShortText,
  dd_noofforeigntrade,
  dd_seqnoofvendorconf,
  dd_InboundDeliveryItemNo,
  dim_shippinginstructionid,
  dd_confirmationcateg,
  dd_creationindvendorconf,
dim_deliverydateofvendorconf,
dim_mdg_partid )
SELECT ((SELECT ifnull(max_id, 1)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_shipmentindelivery_temp') + row_number() over (order by '')) fact_shipmentindeliveryid,
          a.*
FROM
  (SELECT DISTINCT
    dd_DocumentNo,
    dd_DocumentItemNo,
    dd_ScheduleNo,
    dd_ShipmentNo,
    dd_ShipmentItemNo,
    dd_InboundDeliveryNo,
    Dim_DateIdShipmentCreated,
    dd_Vessel,
    dd_MovementType,
    dd_CarrierActual,
    Dim_CarrierBillingId,
    dd_DomesticContainerID,
    Dim_ShipmentTypeId,
    Dim_RouteId,
    dD_VoyageNo,
    Dim_DateIdShipmentEnd,
    Dim_DateIdDepartureFromOrigin,
    dd_ContainerSize,
    dd_3PLFlag,
    Dim_DateIdPOBuy,
    Dim_PODocumentTypeId,
    Dim_DateIdPOETA,
    dim_purchasegroupid,
    Dim_partid,
    Dim_Vendorid,
    Dim_DateIdGR,
    Dim_PORouteId,
    Dim_CustomVendorPartnerFunctionId,
    Dim_CustomVendorPartnerFunctionId1,
    Dim_POAFSSeasonId,
    ct_POScheduleQty,
    ct_POItemReceivedQty,
    ct_POInboundDeliveryQty,
    dd_CustomerPOStatus,
    dd_PackageCount,
    dd_BillofLadingNo,
    Dim_CompanyId,
    Dim_PlantId,
    dd_OriginContainerId,
    ct_InboundDlvryCMtrVolume,
    Dim_DateidDlvrDocCreated,
    Dim_DateIdPODelivery,
    ct_DelivReceivedQty,
    ct_ReceivedQty,
    dd_ShipmentStatus,
    dim_inbrouteid,
    ct_ItemQty,
    Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
    dim_dateidactualpo,  /* Roxana 02 dec 2014 */
    dim_dateidAFSExFactEst,  /* Marius 03 Dec 2014 */
    dim_dateidMatAvailability,
    dd_DeliveryNumber,
    dim_dateidTransfer,
    ct_DeliveryGrossWght,
    ct_deliveryNetWght,
    Dim_DeliveryRouteId,
    dim_shippingconditionid,
    dim_tranportbyshippingtypeid,
    dd_ShortText,
    dd_noofforeigntrade,
    dd_seqnoofvendorconf,
    dd_InboundDeliveryItemNo,
    dim_shippinginstructionid,
    dd_confirmationcateg,
    dd_creationindvendorconf,
    dim_deliverydateofvendorconf,
    dim_mdg_partid
  FROM tmp_populate_shipment_t3 ) a;

DROP TABLE IF EXISTS tmp_populate_shipment_t3;


delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery_temp',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery_temp;


INSERT INTO fact_shipmentindelivery_temp(fact_shipmentindeliveryid,
  dd_DocumentNo,
  dd_DocumentItemNo,
  dd_ScheduleNo,
  dd_ShipmentNo,
  dd_ShipmentItemNo,
  dd_InboundDeliveryNo,
  Dim_DateIdShipmentCreated,
  dd_Vessel,
  dd_MovementType,
  dd_CarrierActual,
  Dim_CarrierBillingId,
  dd_DomesticContainerID,
  Dim_ShipmentTypeId,
  Dim_RouteId,
  dD_VoyageNo,
  Dim_DateIdShipmentEnd,
  Dim_DateIdDepartureFromOrigin,
  dd_ContainerSize,
  dd_3PLFlag,
  Dim_DateIdPOBuy,
  Dim_PODocumentTypeId,
  Dim_DateIdPOETA,
  dim_purchasegroupid,
  Dim_partid,
  Dim_Vendorid,
  Dim_DateIdGR,
  Dim_PORouteId,
  Dim_CustomVendorPartnerFunctionId,
  Dim_CustomVendorPartnerFunctionId1,
  Dim_POAFSSeasonId,
  ct_POScheduleQty,
  ct_POItemReceivedQty,
  ct_POInboundDeliveryQty,
  dd_CustomerPOStatus,
  dd_PackageCount,
  dd_BillofLadingNo,
  Dim_CompanyId,
  Dim_PlantId,
  dd_OriginContainerId,
  ct_InboundDlvryCMtrVolume,
  ct_InboundDeliveryQty,
  Dim_DateidDlvrDocCreated,
  Dim_DateIdPODelivery,
  ct_DelivReceivedQty,
  ct_ReceivedQty,
  dd_ShipmentStatus,
  ct_ItemQty,
  Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
  dim_dateidactualpo,
  dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
  dim_dateidMatAvailability,
  dd_DeliveryNumber,
  dim_dateidTransfer,
  ct_DeliveryGrossWght,
  ct_deliveryNetWght,
  Dim_DeliveryRouteId,
  dim_shippingconditionid,
  dim_tranportbyshippingtypeid,
  dd_ShortText,
  dd_noofforeigntrade,
  dim_transportconditionid,
  dim_shippinginstructionid,
dim_mdg_partid)
SELECT IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN
           WHERE TABLE_NAME = 'fact_shipmentindelivery_temp' ), 1) + ROW_NUMBER() OVER(order by '')  fact_shipmentindeliveryid,
  p.dd_DocumentNo,
  p.dd_DocumentItemNo,
  p.dd_ScheduleNo,
  'Not Set',
  0,
  'Not Set',
  CONVERT(BIGINT, 1),
  'Not Set',
  'Not Set',
  'Not Set',
  CONVERT(BIGINT, 1),
  'Not Set',
  CONVERT(BIGINT, 1) Dim_ShipmentTypeId,
  CONVERT(BIGINT, 1) Dim_RouteId,
  'Not Set',
  CONVERT(BIGINT, 1) Dim_DateIdShipmentEnd,
  CONVERT(BIGINT, 1) Dim_DateIdDepartureFromOrigin,
  'Not Set',
  'Not Set',
  p.Dim_DateIdSchedOrder,
  p.Dim_DocumentTypeid,
  p.Dim_DateidAFSDelivery,
  dim_purchasegroupid,
  Dim_partid,
  Dim_Vendorid,
  Dim_DateIdLastGR,
  p.dim_AFSRouteid,
  p.Dim_CustomPartnerFunctionId4,
  p.Dim_CustomPartnerFunctionId3,
  p.Dim_AfsSeasonId,
  ct_DeliveryQty,
  p.ct_ReceivedQty ct_POItemReceivedQty,
  ct_QtyReduced,
  'Not Set',
  0,
  'Not Set' dd_BillofLadingNo,
  p.Dim_CompanyId,
  Dim_plantidordering,
  'Not Set',
  0.0000,
  0,
  CONVERT(BIGINT, 1) Dim_DateidDlvrDocCreated,
  p.Dim_DateIdDelivery,
  0.0000,
  0.0000 ct_ReceivedQty,
  'Not Set',
  p.ct_ItemQty,
  p.Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
  p.Dim_DateidOrder,
  p.dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
  p.dim_dateidMatAvailability,
  p.dd_DeliveryNumber,
  CONVERT(BIGINT, 1) dim_dateidTransfer,
  0.0000 ct_DeliveryGrossWght,
  0.0000 ct_deliveryNetWght,
  CONVERT(BIGINT, 1) Dim_DeliveryRouteId,
  CONVERT(BIGINT, 1) dim_shippingconditionid,
  CONVERT(BIGINT, 1) dim_tranportbyshippingtypeid,
  'Not Set',
  p.dd_noofforeigntrade,
  CONVERT(BIGINT, 1) dim_transportconditionid,
  dim_shippinginstructionid,
  p.dim_mdg_partid
FROM facT_purchase p
            WHERE NOT EXISTS
                         (SELECT 1
                            FROM fact_shipmentindelivery_temp f1
                           WHERE    f1.dd_DocumentNo = p.dd_DocumentNo
                                 AND f1.dd_DocumentItemNo = p.dd_DocumentItemNo
                                 AND f1.dd_ScheduleNo = p.dd_ScheduleNo);


/*
DROP TABLE IF EXISTS tmp_EKES_LIKP_LIPS

CREATE TABLE tmp_EKES_LIKP_LIPS AS
SELECT EKES_EBELN,EKES_EBELP,EKES_J_3AETENR,LIKP_ANZPK,LIKP_BOLNR,LIKP_LFDAT,LIKP_TRAID,LIKP_TRATY,LIKP_VBELN,LIKP_VOLUM,LIKP_WADAT_IST,LIKP_ERDAT,sum(LIPS_LFIMG) AS LFIMG
FROM EKES_LIKP_LIPS
WHERE ekes_j_3aetenr <> 0
group by EKES_EBELN,EKES_EBELP,EKES_J_3AETENR,LIKP_ANZPK,LIKP_BOLNR,LIKP_LFDAT,LIKP_TRAID,LIKP_TRATY,LIKP_VBELN,LIKP_VOLUM,LIKP_WADAT_IST,LIKP_ERDAT

UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateIdETAFinalDest = dt.Dim_DateId
FROM     tmp_EKES_LIKP_LIPS elp,
	 Dim_Date dt,
         Dim_Company c,
	 fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_LFDAT
  AND dt.CompanyCode = c.CompanyCode


UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateIdInboundDeliveryDate = dt.Dim_DateId
FROM tmp_EKES_LIKP_LIPS elp,
		Dim_Date dt,
		Dim_Company c,
		fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_LFDAT
  AND dt.CompanyCode = c.CompanyCode


UPDATE fact_shipmentindelivery_temp ft
SET Dim_MeansofTransTypeId = pmt.Dim_PackagingMaterialTypeId
FROM tmp_EKES_LIKP_LIPS elp,
     Dim_PackagingMaterialType pmt,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND pmt.PackagingMaterialType = LIKP_TRATY
  AND pmt.RowIsCurrent = 1

UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateidActualGI = dt.Dim_DateId
FROM tmp_EKES_LIKP_LIPS elp,
      Dim_Date dt,
      Dim_Company c,
      fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_WADAT_IST
  AND dt.CompanyCode = c.companycode



UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateidDlvrDocCreated = dt.Dim_DateId
FROM tmp_EKES_LIKP_LIPS elp,
     Dim_Date dt,
     Dim_Company c,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_ERDAT
  AND dt.CompanyCode = c.companycode



UPDATE fact_shipmentindelivery_temp ft
SET ft.dd_OriginContainerId = ifnull(LIKP_TRAID,'Not Set')
FROM tmp_EKES_LIKP_LIPS elp,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR


UPDATE fact_shipmentindelivery_temp ft
SET ft.ct_InboundDeliveryQty = ifnull(LFIMG,0)
FROM tmp_EKES_LIKP_LIPS elp,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR

DROP TABLE IF EXISTS tmp_EKES_LIKP_LIPS*/

/* Marius 4 Oct 2016 add attributes */

UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateIdETAFinalDest = dt.Dim_DateId
FROM     EKES_LIKP_LIPS elp,
	 Dim_Date dt,
         Dim_Company c,
	 fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
 AND ft.dd_InboundDeliveryItemNo = elp.LIPS_POSNR
 AND ft.dd_seqnoofvendorconf = elp.EKES_ETENS
 AND ft.dd_DocumentNo = elp.EKES_EBELN
 AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_LFDAT
  AND dt.CompanyCode = c.CompanyCode;


UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateIdInboundDeliveryDate = dt.Dim_DateId
FROM EKES_LIKP_LIPS elp,
		Dim_Date dt,
		Dim_Company c,
		fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_InboundDeliveryItemNo = elp.LIPS_POSNR
  AND ft.dd_seqnoofvendorconf = elp.EKES_ETENS
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_LFDAT
  AND dt.CompanyCode = c.CompanyCode;


UPDATE fact_shipmentindelivery_temp ft
SET Dim_MeansofTransTypeId = pmt.Dim_PackagingMaterialTypeId
FROM EKES_LIKP_LIPS elp,
     Dim_PackagingMaterialType pmt,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_InboundDeliveryItemNo = elp.LIPS_POSNR
  AND ft.dd_seqnoofvendorconf = elp.EKES_ETENS
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND pmt.PackagingMaterialType = LIKP_TRATY
  AND pmt.RowIsCurrent = 1;

UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateidActualGI = dt.Dim_DateId
FROM EKES_LIKP_LIPS elp,
      Dim_Date dt,
      Dim_Company c,
      fact_shipmentindelivery_temp ft
      WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
        AND ft.dd_InboundDeliveryItemNo = elp.LIPS_POSNR
        AND ft.dd_seqnoofvendorconf = elp.EKES_ETENS
        AND ft.dd_DocumentNo = elp.EKES_EBELN
        AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_WADAT_IST
  AND dt.CompanyCode = c.companycode;



UPDATE fact_shipmentindelivery_temp ft
SET ft.Dim_DateidDlvrDocCreated = dt.Dim_DateId
FROM EKES_LIKP_LIPS elp,
     Dim_Date dt,
     Dim_Company c,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
 AND ft.dd_InboundDeliveryItemNo = elp.LIPS_POSNR
 AND ft.dd_seqnoofvendorconf = elp.EKES_ETENS
 AND ft.dd_DocumentNo = elp.EKES_EBELN
 AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_ERDAT
  AND dt.CompanyCode = c.companycode;



UPDATE fact_shipmentindelivery_temp ft
SET ft.dd_OriginContainerId = ifnull(LIKP_TRAID,'Not Set')
FROM EKES_LIKP_LIPS elp,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
 AND ft.dd_InboundDeliveryItemNo = elp.LIPS_POSNR
 AND ft.dd_seqnoofvendorconf = elp.EKES_ETENS
 AND ft.dd_DocumentNo = elp.EKES_EBELN
 AND ft.dd_DocumentItemNo = elp.EKES_EBELP;


UPDATE fact_shipmentindelivery_temp ft
SET ft.ct_InboundDeliveryQty = ifnull(LIPS_LFIMG,0)
FROM EKES_LIKP_LIPS elp,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_InboundDeliveryItemNo = elp.LIPS_POSNR
  AND ft.dd_seqnoofvendorconf = elp.EKES_ETENS
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP;

/* end Marius fix attributes */

UPDATE fact_shipmentindelivery_temp ft
SET dd_3PLFlag = ifnull(EIKP_LADEL, 'Not Set')
FROM EKES_LIKP_LIPS elp, EIKP e,
     fact_shipmentindelivery_temp ft
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
AND ft.dd_DocumentNo = elp.EKES_EBELN
AND ft.dd_DocumentItemNo = elp.EKES_EBELP
AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
AND elp.LIKP_EXNUM = e.EIKP_EXNUM
AND (dd_3PLFlag IS NULL OR dd_3PLFlag = 'Not Set');


UPDATE facT_shipmentindelivery_temp ft
SET ft.Dim_DateidASNOutput =  dt.dim_dateid
FROM NAST n, Dim_Date dt, Dim_company c,
     facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND ft.dim_companyid = c.dim_companyid
AND n.NAST_DATVR = dt.DateValue
AND dt.CompanyCode = c.CompanyCode;



UPDATE facT_shipmentindelivery_temp ft
SET ft.Dim_ProcessingStatusId =  s.Dim_ProcessingMessageStatusId
FROM NAST n, Dim_ProcessingMessageStatus s,
     facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_VSTAT = s.ProcessingMessage
AND n.NAST_KSCHL = 'ZINB'
AND s.RowIsCurrent = 1;


UPDATE facT_shipmentindelivery_temp ft
SET ft.dd_processingtime =  n.NAST_UHRVR
FROM NAST n, facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_UHRVR IS NOT NULL;


UPDATE facT_shipmentindelivery_temp ft
SET ft.dd_processingtime =  '000000'
FROM NAST n, facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_UHRVR IS NULL;

UPDATE facT_shipmentindelivery_temp ft
SET ft.dd_MessageType =  n.NAST_KSCHL
FROM NAST n, facT_shipmentindelivery_temp ft
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY;

DROP TABLE IF EXISTS tmp_vttk_vttp_maxcreateddatetime;
CREATE table tmp_vttk_vttp_maxcreateddatetime as
SELECT vttp_vbeln, vttk_tknum,vttk_erdat,vttk_erzet,RANK() OVER (PARTITION BY vttp_vbeln ORDER BY vttk_erdat DESC,vttk_erzet DESC) as Rank from vttk_vttp;

update fact_shipmentindelivery_temp  t1
set dd_CurrentShipmentFlag = 'X'
from tmp_vttk_vttp_maxcreateddatetime t,fact_shipmentindelivery_temp  t1
WHERE t.vttp_vbeln = t1.dd_InbounddeliveryNo
AND t.vttk_tknum = t1.dd_ShipmentNo
AND t.Rank = 1;


update fact_shipmentindelivery_temp  t1
set dd_CurrentShipmentFlag = 'Not Set'
from tmp_vttk_vttp_maxcreateddatetime t, fact_shipmentindelivery_temp  t1
WHERE t.vttp_vbeln = t1.dd_InbounddeliveryNo
AND t.vttk_tknum = t1.dd_ShipmentNo
AND t.Rank <> 1;

DROP TABLE IF EXISTS tmp_vttk_vttp_maxcreateddatetime;

/*update removed because attribute Carton Level Dtl not used in reports - BogdanS - 20180227 - Start 
UPDATE fact_shipmentindelivery_temp t1
SET t1.dd_CartonLevelDtlFlag = 'X'
WHERE t1.dd_InboundDeliveryNo <> 'Not Set'
AND EXISTS ( SELECT 1 FROM vekp v
               WHERE v.VEKP_VPOBJKEY = t1.dd_InboundDeliveryNo);

UPDATE fact_shipmentindelivery_temp t1
SET t1.dd_CartonLevelDtlFlag = 'Not Set'
WHERE t1.dd_InboundDeliveryNo <> 'Not Set'
AND NOT EXISTS ( SELECT 1 FROM vekp v
                WHERE v.VEKP_VPOBJKEY = t1.dd_InboundDeliveryNo);
/*update removed because attribute Carton Level Dtl not used in reports - BogdanS - 20180227 - End */

UPDATE	facT_shipmentindelivery_temp  st
SET st.Dim_DateIdPOBuy = p.Dim_DateIdSchedOrder,
    st.Dim_PODocumentTypeId = p.Dim_DocumentTypeid,
    st.Dim_DateIdPOETA = p.Dim_DateidAFSDelivery
FROM fact_purchase p, facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

/* Update Material Description = dd_ShortText*/

UPDATE	facT_shipmentindelivery_temp  st
SET st.dd_ShortText = p.dd_ShortText
FROM fact_purchase p, facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

/* Commented Marius

update facT_shipmentindelivery_temp st
FROM VBFA_MKPF vm, dim_date dt
SET st.Dim_DateIdGR = dt.dim_dateid
Where vm.MKPF_BUDAT = dt.DateValue
AND st.dd_InboundDeliveryNo=vm.VBFA_VBELV
AND st.Dim_DateIdGR <> dt.dim_dateid */


UPDATE	facT_shipmentindelivery_temp  st
SET st.dim_purchasegroupid = p.Dim_PurchaseGroupId,
    st.Dim_PartId = p.Dim_Partid,
        st.Dim_VendorId = p.Dim_vendorid
FROM fact_purchase p, facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

UPDATE fact_shipmentindelivery_temp t
SET t.Dim_ProductHierarchyId = ph.Dim_ProductHierarchyId
FROM dim_part pt, dim_producthierarchy ph, fact_shipmentindelivery_temp t
WHERE t.dim_partid = pt.dim_partid
  AND pt.ProductHierarchy = ph.ProductHierarchy
  AND ph.RowIsCurrent = 1;

UPDATE fact_shipmentindelivery_temp st
SET st.ct_POInboundDeliveryQty = p.ct_QtyReduced
FROM fact_purchase p,
     fact_shipmentindelivery_temp st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo
AND st.dd_InboundDeliveryNo <> 'Not Set'
AND st.ct_POInboundDeliveryQty = 0;

UPDATE facT_shipmentindelivery_temp  st
SET st.Dim_PORouteId = p.dim_AFSRouteid,
    st.Dim_CustomVendorPartnerFunctionId = p.Dim_CustomPartnerFunctionId4,
    st.Dim_CustomVendorPartnerFunctionId1 = p.Dim_CustomPartnerFunctionId3,
    st.Dim_POAFSSeasonId = p.Dim_AfsSeasonId
FROM fact_purchase p, facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_ScheduleQty_POItem;
CREATE TABLE tmp_ScheduleQty_POItem AS
SELECT dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo, max(ct_DeliveryQty) as ScheduleQty,max(ct_ReceivedQty) as ReceivedQty from fact_purchase
group by dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo;

UPDATE facT_shipmentindelivery_temp  st
SET st.ct_POScheduleQty = p.ScheduleQty,
    st.ct_POItemReceivedQty = p.ReceivedQty
FROM tmp_ScheduleQty_POItem p,
     facT_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;


DROP TABLE IF EXISTS tmp_ScheduleQty_POItem;

UPDATE facT_shipmentindelivery_temp st
SET st.dd_SalesDocNo = ifnull(a.dd_salesdocno,'Not Set'),
      st.dd_SalesItemNo = ifnull(a.dd_salesitemno,0),
      st.dd_SalesScheduleNo = ifnull(a.dd_Salesscheduleno,0)
FROM fact_purchase a,  facT_shipmentindelivery_temp st
WHERE st.dd_DocumentNo = a.dd_DocumentNo
AND st.dd_DocumentItemNo = a.dd_DocumentItemNo
AND st.dd_ScheduleNo = a.dd_ScheduleNo;


drop table if exists ekko_ekpo_ekkn_tmp ;
create table ekko_ekpo_ekkn_tmp
as select distinct ekkn_vbeln, ekkn_vbelp,ekkn_ebeln,ekkn_ebelp
from EKKO_EKPO_EKKN_Next
where ekkn_vbeln is not null;

update facT_shipmentindelivery_temp f
set dd_SalesDocNo=ifnull(ek.EKKN_VBELN, 'Not Set'),
			 dd_SalesItemNo = ifnull(ek.EKKN_VBELP,0)
from facT_shipmentindelivery_temp f,ekko_ekpo_ekkn_tmp ek
where f.dd_DocumentNo = ek.EKKN_EBELN
			 AND f.dd_DocumentItemNo = ek.EKKN_EBELP
and dd_SalesDocNo<>ifnull(ek.EKKN_VBELN, 'Not Set');


drop table if exists tmp_uniquejoincond;
create table tmp_uniquejoincond
as
select distinct so.Dim_DateIdAfsCancelDate, so.Dim_SalesDocumentTypeid, so.Dim_DateIdAfsReqDelivery, so.Dim_CustomPartnerFunctionId1,
                so.Dim_DocumentCategoryid,
                so.dd_SalesDocNo,  so.dd_SalesItemNo, so.dd_ScheduleNo
from fact_SalesOrder so;



UPDATE facT_shipmentindelivery_temp st
SET st.Dim_DateIdSOCancelDate = so.Dim_DateIdAfsCancelDate,
    st.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid,
    st.Dim_DateIdReqDeliveryDate = so.Dim_DateIdAfsReqDelivery,
    st.Dim_CustomPartnerFunctionId1 = so.Dim_CustomPartnerFunctionId1
FROM tmp_uniquejoincond so,
	 facT_shipmentindelivery_temp st
WHERE st.dd_SalesDocNo = so.dd_SalesDocNo
  AND st.dd_SalesItemNo = so.dd_SalesItemNo
  AND st.dd_SalesScheduleNo = so.dd_ScheduleNo;


/*Added on Dec 10th 2014, by Roxana*/

UPDATE facT_shipmentindelivery_temp st
SET st.Dim_DocumentCategoryid=so.Dim_DocumentCategoryid
FROM tmp_uniquejoincond so, facT_shipmentindelivery_temp st
WHERE st.dd_SalesDocNo = so.dd_SalesDocNo
  AND st.dd_SalesItemNo = so.dd_SalesItemNo
  AND st.dd_SalesScheduleNo = so.dd_ScheduleNo
  AND st.Dim_DocumentCategoryid<>so.Dim_DocumentCategoryid;

drop table if exists tmp_uniquejoincond;

DROP TABLE IF EXISTS tmp_AirSeaEstDateCalculation;

CREATE TABLE  tmp_AirSeaEstDateCalculation as
select distinct vttk_shtyp ShipType, vttk_Dpten shipenddate,
                vttk_route Route, substr(vttk_route,1,5) altrouteprefix ,
				'Not Set' altroute,
                CONVERT( decimal (11,0), 0) totalduration
from vttk_vttp
where vttk_Dpten IS NOT NULL and vttk_Route is not null;


UPDATE tmp_AirSeaEstDateCalculation t2
SET t2.altroute = t1.tvrab_route,
    t2.totalduration = t1.tvrab_gesztd
FROM tvrab t1, tmp_AirSeaEstDateCalculation t2
where t1.tvrab_route <> t2.route
AND substr(t1.tvrab_route,1,5) = t2.altrouteprefix
AND t2.altroute = 'Not Set';


UPDATE tmp_AirSeaEstDateCalculation t2
SET totalduration = round(totalduration/(3600*24),0)
where totalduration <> 0;

UPDATE facT_shipmentindelivery_temp t
SET t.Dim_DateIdAirorSeaPlanningEstETA =  t.Dim_DateIdShipmentEnd
FROM tmp_AirSeaEstDateCalculation t1,
     dim_Route r1,
     facT_shipmentindelivery_temp t
where r1.dim_routeid = t.dim_routeid
AND r1.routecode = t1.route
AND t1.totalduration = 0;

DROP TABLE IF EXISTS tmp_DateAirSeaCalculation;
CREATE TABLE tmp_DateAirSeaCalculation AS
SELECT  distinct r1.dim_routeid, t.Dim_DateIdShipmentEnd, dt2.dim_dateid  FROM facT_shipmentindelivery_temp t,
       dim_company c1, dim_shipmenttype st, dim_Route r1,
       dim_date dt,
	   tmp_AirSeaEstDateCalculation t1,
       dim_Date dt2
where t.dim_Companyid = c1.dim_companyid
AND  t.dim_shipmenttypeid = st.dim_shipmenttypeid
AND t.Dim_DateIdShipmentEnd = dt.dim_Dateid
AND r1.dim_routeid = t.dim_routeid
AND dt.datevalue = t1.shipenddate
AND r1.routecode = t1.route
and st.shipmenttype = t1.ShipType
AND t1.totalduration > 0
AND dt2.datevalue = (dt.datevalue + (interval '1' day)*t1.totalduration )
AND dt2.companycode = c1.companycode;

UPDATE facT_shipmentindelivery_temp t
SET t.Dim_DateIdAirorSeaPlanningEstETA =  t1.dim_dateid
   FROM tmp_DateAirSeaCalculation t1, facT_shipmentindelivery_temp t
where t.dim_routeid = t1.dim_routeid
AND t.Dim_DateIdShipmentEnd = t1.Dim_DateIdShipmentEnd
AND (t.Dim_DateIdAirorSeaPlanningEstETA IS NULL OR t.Dim_DateIdAirorSeaPlanningEstETA = 1);

DROP TABLE IF EXISTS tmp_DateAirSeaCalculation;
DROP TABLE IF EXISTS tmp_AirSeaEstDateCalculation;

/* Andra : 19th of May 2014 : Add Dim_PurchaseMiscid, dd_DeletionIndicator*/

UPDATE fact_shipmentindelivery_temp  st
SET st.Dim_PurchaseMiscid = p.Dim_PurchaseMiscid
FROM fact_purchase p, fact_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;


UPDATE fact_shipmentindelivery_temp  st
SET st.dd_DeletionIndicator = p.dd_DeletionIndicator
FROM fact_purchase p, fact_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

UPDATE fact_shipmentindelivery_temp  st
SET st.ct_scheduleLineQty = ifnull(p.ct_scheduleLineQty,0)
FROM fact_purchase p, fact_shipmentindelivery_temp  st
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp1_facT_shipmentindelivery_temp;
CREATE TABLE tmp1_facT_shipmentindelivery_temp
AS
sELECT distinct dd_DocumentNo,dd_DocumentItemNo,
sum(ct_POInboundDeliveryQty) InboundQty,
sum(ct_POItemReceivedQty) Receivedqty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,dd_DocumentItemNo;
drop table if exists tmp_facT_shipmentindelivery_temp_unstable;
create table tmp_facT_shipmentindelivery_temp_unstable
as
select distinct facT_shipmentindeliveryid
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp1_facT_shipmentindelivery_temp t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.InboundQty = 0;

UPDATE facT_shipmentindelivery_temp t
SET t.dd_CustomerPOStatus = 'Open'
FROM
   tmp_facT_shipmentindelivery_temp_unstable tmp, facT_shipmentindelivery_temp t
WHERE tmp.facT_shipmentindeliveryid = t.facT_shipmentindeliveryid;
drop table if exists tmp_facT_shipmentindelivery_temp_unstable;

UPDATE facT_shipmentindelivery_temp t
SET dd_CustomerPOStatus = 'In-Transit'
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp1_facT_shipmentindelivery_temp t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.ReceivedQty = 0
  AND t1.InboundQty > 0;


drop table if exists tmp_facT_shipmentindelivery_temp_unstable;
create table tmp_facT_shipmentindelivery_temp_unstable
as
select distinct facT_shipmentindeliveryid
from     fact_purchase p,
         dim_purchasemisc pm,
         tmp1_facT_shipmentindelivery_temp t1,
         facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.ReceivedQty > 0;

UPDATE facT_shipmentindelivery_temp t
SET dd_CustomerPOStatus = 'Received'
FROM  tmp_facT_shipmentindelivery_temp_unstable tmp, facT_shipmentindelivery_temp t
WHERE tmp.facT_shipmentindeliveryid = t.facT_shipmentindeliveryid;

drop table if exists tmp_facT_shipmentindelivery_temp_unstable;
create table tmp_facT_shipmentindelivery_temp_unstable
as
select distinct facT_shipmentindeliveryid
from    fact_purchase p,
        dim_purchasemisc pm, facT_shipmentindelivery_temp t
WHERE  p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete = 'X'
  AND pm.ItemGRIndicator = 'X'
  AND t.ct_POItemReceivedQty > 0;

UPDATE facT_shipmentindelivery_temp t
SET dd_CustomerPOStatus = 'Closed'
FROM  tmp_facT_shipmentindelivery_temp_unstable tmp, facT_shipmentindelivery_temp t
WHERE tmp.facT_shipmentindeliveryid = t.facT_shipmentindeliveryid;


DROP TABLE IF EXISTS tmp1_facT_shipmentindelivery_temp;

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POScheduleQty
WHERE   dd_CustomerPOStatus IN ('Open');

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POItemReceivedQty
WHERE   dd_CustomerPOStatus IN ('Closed','Received');

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POInboundDeliveryQty
WHERE   dd_CustomerPOStatus IN ('In-Transit');


/* Andra 16 May: Update of the dd_ShipmentStatus*/
DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_shipmentstatus;
CREATE TABLE tmp_facT_shipmentindelivery_shipmentstatus AS
SELECT dd_DocumentNo,
       dd_DocumentItemNo,
	   dd_ScheduleNo,
	   dd_ShipmentNo,
       dd_InboundDeliveryNo
	   ,sum(ct_POInboundDeliveryQty) as ct_POInboundDeliveryQty, sum(ct_DelivReceivedQty) AS ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,
       dd_DocumentItemNo,
	   dd_ScheduleNo,
	   dd_ShipmentNo,
       dd_InboundDeliveryNo;

UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'In Transit'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo
  AND pm.afsdeliverycomplete <> 'X'
  AND p.dd_DeletionIndicator = 'Not Set'
  AND t1.ct_DelivReceivedQty = 0;


UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'Received'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo
  AND pm.afsdeliverycomplete <> 'X'
  AND p.dd_DeletionIndicator = 'Not Set'
  AND t1.ct_DelivReceivedQty > 0;

UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'Closed'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo
  AND pm.afsdeliverycomplete = 'X'
  AND t1.ct_POInboundDeliveryQty > 0;

DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_shipmentstatus;

DROP TABLE IF EXISTS tmp_facT_sid_shipmentstatus_scheduleLevel;
CREATE TABLE tmp_facT_sid_shipmentstatus_scheduleLevel AS
SELECT dd_DocumentNo,
       dd_DocumentItemNo,
	   dd_ScheduleNo
	   ,sum(ct_POInboundDeliveryQty) as ct_POInboundDeliveryQty, sum(ct_DelivReceivedQty) AS ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,
       dd_DocumentItemNo,
	   dd_ScheduleNo;

UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'Cancelled'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_sid_shipmentstatus_scheduleLevel t1,
	facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND pm.afsdeliverycomplete = 'X'
  AND t1.ct_POInboundDeliveryQty = 0;


UPDATE facT_shipmentindelivery_temp t
SET dd_ShipmentStatus = 'Deleted'
FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_sid_shipmentstatus_scheduleLevel t1,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND p.dd_DeletionIndicator = 'L';


DROP TABLE IF EXISTS tmp_facT_sid_shipmentstatus_scheduleLevel;

/* Andra 16 May: Update of the dd_ShipmentStatus*/


/* Andra 16 May: Update of the Qtys for DirectShip*/

UPDATE facT_shipmentindelivery_temp t
SET t.ct_InTransitQty = (t.ct_ReceivedQty - t.ct_DelivReceivedQty);

UPDATE facT_shipmentindelivery_temp t
SET t.ct_ClosedQty = t.ct_DelivReceivedQty
FROM fact_purchase p,
    dim_purchasemisc pm,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND pm.afsdeliverycomplete = 'X';

UPDATE facT_shipmentindelivery_temp t
SET t.ct_CancelledQty = p.ct_scheduleLineQty
FROM fact_purchase p,
    dim_purchasemisc pm,
    facT_shipmentindelivery_temp t
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND p.ct_QtyReduced = 0
  AND p.ct_ReceivedQty = 0
  AND pm.afsdeliverycomplete = 'X';

DROP TABLE IF EXISTS tmp_fact_purchase_OpenQty_Item;
CREATE TABLE tmp_fact_purchase_OpenQty_Item AS
SELECT dd_DocumentNo,dd_DocumentItemNo,sum(ct_scheduleLineQty) as ct_scheduleLineQty
from fact_purchase
group by dd_DocumentNo,dd_DocumentItemNo;

DROP TABLE IF EXISTS tmp_fact_sid_OpenQty_Item;
CREATE TABLE tmp_fact_sid_OpenQty_Item AS
SELECT dd_DocumentNo,dd_DocumentItemNo,sum(ct_InTransitQty) as ct_InTransitQty,sum(ct_DelivReceivedQty) as ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,dd_DocumentItemNo;

UPDATE facT_shipmentindelivery_temp t
SET t.ct_OpenQty = ifnull((tp.ct_scheduleLineQty - (tsid.ct_InTransitQty - tsid.ct_DelivReceivedQty)),0)
FROM tmp_fact_purchase_OpenQty_Item tp,
     tmp_fact_sid_OpenQty_Item tsid,
     facT_shipmentindelivery_temp t
WHERE t.dd_DocumentNo = tp.dd_DocumentNo
  AND t.dd_documentItemNo = tp.dd_DocumentItemNo
  AND t.dd_DocumentNo = tsid.dd_DocumentNo
  AND t.dd_documentItemNo = tsid.dd_DocumentItemNo;


DROP TABLE IF EXISTS tmp_fact_purchase_OpenQty_Item;
DROP TABLE IF EXISTS tmp_fact_sid_OpenQty_Item;

DROP TABLE IF EXISTS tmp_tsegeUpdateEventDates;

CREATE TABLE tmp_tsegeUpdateEventDates AS
SELECT DISTINCT TSEGE_HEAD_HDL, TSEGE_EVEN, TSEGE_EVEN_VERTY,
(case when TSEGE_EVEN_TSTTO = 0 THEN '19000101' ELSE substr(tsege_even_tstto,1,8) END) AS TSEGE_EVEN_TSTTO
FROM TSEGE;

 UPDATE fact_shipmentindelivery_temp st
 SET st.Dim_DateId3plYard = dt.dim_Dateid
  FROM vttk_vttp v,
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c,
       fact_shipmentindelivery_temp st
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'Z3PL'
   AND to_date(t.tsege_even_tstto, 'MM-DD-YYYY') = to_date(dt.datevalue, 'MM-DD-YYYY')
   AND t.TSEGE_EVEN_VERTY = 1
   AND dt.companycode = c.companycode;


/* Andra 18 Jun - add Max functionon 3PL In Yard Date for each inbound delivery */

DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_3pl;
CREATE TABLE tmp_facT_shipmentindelivery_3pl AS
SELECT dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo,dd_InboundDeliveryNo, max(Dim_DateId3plYard) Dim_DateId3plYard
FROM fact_shipmentindelivery_temp
GROUP BY dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo,dd_InboundDeliveryNo;

UPDATE fact_shipmentindelivery_temp sd
SET sd.Dim_DateId3plYard = t.Dim_DateId3plYard
FROM tmp_facT_shipmentindelivery_3pl t, fact_shipmentindelivery_temp sd
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.Dim_DateId3plYard <> t.Dim_DateId3plYard;


/* End of changes Andra 18 Jun - add Max functionon 3PL In Yard Date for each inbound delivery */

UPDATE fact_shipmentindelivery_temp st
 SET st.Dim_DateIdContainerAtDCDoor = dt.dim_Dateid
  FROM vttk_vttp v,
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c,
        fact_shipmentindelivery_temp st
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'ZDOOR'
   AND t.TSEGE_EVEN_VERTY = 1
   AND to_date(t.tsege_even_tstto, 'MM-DD-YYYY') = to_date(dt.datevalue, 'MM-DD-YYYY')
   AND dt.companycode = c.companycode;


 UPDATE fact_shipmentindelivery_temp st
 SET st.Dim_DateIdContainerInYard = dt.dim_Dateid
  FROM vttk_vttp v,
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c,
       fact_shipmentindelivery_temp st
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'ZYARD'
   AND t.TSEGE_EVEN_VERTY = 1
   AND to_date(t.tsege_even_tstto, 'MM-DD-YYYY') = to_date(dt.datevalue, 'MM-DD-YYYY')
   AND dt.companycode = c.companycode;



DROP TABLE IF EXISTS tmp_SplitSalesNetValue;
CREATE TABLE tmp_SplitSalesNetValue
AS
select dd_Documentno,dd_documentitemno,sd.dd_Inbounddeliveryno,sd.dd_Shipmentno,sd.dd_ShipmentItemNo,
sd.dd_salesdocno,sd.dd_salesitemno,dd_Salesscheduleno,
sum(sd.ct_InboundDeliveryQty ) totalschedqty,
SUM(so.amt_DicountAccrualNetPrice*so.ct_ConfirmedQty) avgNetValue,
SUM(so.amt_DicountAccrualNetPrice*so.ct_ConfirmedQty)/(case when sum(sd.ct_InboundDeliveryQty) = 0 then null else sum(sd.ct_InboundDeliveryQty) end) avgConfInb

from fact_shipmentindelivery_temp sd,
fact_Salesorder so
where sd.dd_Salesdocno = so.dd_Salesdocno
and sd.dd_salesitemno = so.dd_salesitemno
and sd.dd_Salesscheduleno = so.dd_scheduleno
and sd.dd_Salesdocno <> 'Not Set'
group by (dd_Documentno,dd_documentitemno,sd.dd_Inbounddeliveryno,sd.dd_Shipmentno,sd.dd_ShipmentItemNo,sd.dd_salesdocno,sd.dd_salesitemno,dd_Salesscheduleno);

DROP TABLE IF EXISTS tmp_ResultFromSplitSalesandShipmentInbound;
CREATE TABLE tmp_ResultFromSplitSalesandShipmentInbound AS
SELECT t.*,sd.dd_scheduleno,sd.ct_InboundDeliveryQty  *  avgNetValue / (CASE WHEN totalschedqty <> 0  THEN totalschedqty ELSE 1 END) AS avg_Value
FROM fact_shipmentindelivery_temp sd
,tmp_SplitSalesNetValue t
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
AND sd.dd_Shipmentno = t.dd_Shipmentno
AND sd.dd_ShipmentItemNo = t.dd_ShipmentItemNo
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.dd_salesdocno = t.dd_salesdocno
AND sd.dd_salesitemno = t.dd_salesitemno
AND sd.dd_Salesscheduleno = t.dd_Salesscheduleno
AND  sd.dd_salesdocno <> 'NotSet';

/*
UPDATE fact_shipmentindelivery_temp sd
SET sd.amt_so_afsnetvalue = t.avg_Value
from  tmp_ResultFromSplitSalesandShipmentInbound t,
      fact_shipmentindelivery_temp sd
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
and sd.dd_scheduleno = t.dd_Scheduleno
AND sd.dd_Shipmentno = t.dd_Shipmentno
AND sd.dd_ShipmentItemNo = t.dd_ShipmentItemNo
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.dd_salesdocno = t.dd_salesdocno
AND sd.dd_salesitemno = t.dd_salesitemno
AND sd.dd_Salesscheduleno = t.dd_Salesscheduleno
AND  sd.dd_salesdocno <> 'Not Set'*/

DROP TABLE IF EXISTS tmp_ResultFromSplitSalesandShipmentInbound;
DROP TABLE IF EXISTS tmp_SplitSalesNetValue;

/* Update BW Hierarchy */

update fact_shipmentindelivery_temp f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_shipmentindelivery_temp f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2018-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

update fact_shipmentindelivery_temp f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_shipmentindelivery_temp f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

/*drop table if exists partnumber_businessSectorNotSet_shipment
create table partnumber_businessSectorNotSet_shipment
as
select distinct fact_shipmentindeliveryid
from fact_shipmentindelivery_temp f,dim_part p,dim_bwproducthierarchy bw
where f.dim_partid=p.dim_partid
and f.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='Not Set'

update fact_shipmentindelivery_temp f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_shipmentindelivery_temp f, dim_part dp,dim_mdg_part mdg, dim_bwproducthierarchy bw,partnumber_businessSectorNotSet_shipment pn
where f.dim_partid = dp.dim_partid
and f.dim_mdg_partid=mdg.dim_mdg_partid
and f.fact_shipmentindeliveryid = pn.fact_shipmentindeliveryid
and dp.producthierarchy = bw.lowerhierarchycode
and mdg.GlbProductGroup = bw.upperhierarchycode
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid
and to_date('2018-12-28') between bw.upperhierstartdate and bw.upperhierenddate*/


/* Setting the nulls to default values */

UPDATE fact_shipmentindelivery_temp SET Dim_BillingBlockid = 1 WHERE Dim_BillingBlockid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Companyid = 1 WHERE Dim_Companyid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ControllingAreaid = 1 WHERE Dim_ControllingAreaid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CostCenterid = 1 WHERE Dim_CostCenterid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerGroup1id = 1 WHERE Dim_CustomerGroup1id IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerGroup2id = 1 WHERE Dim_CustomerGroup2id IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerID = 1 WHERE Dim_CustomerID IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomeridShipTo = 1 WHERE Dim_CustomeridShipTo IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId = 1 WHERE Dim_CustomPartnerFunctionId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId1 = 1 WHERE Dim_CustomPartnerFunctionId1 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId2 = 1 WHERE Dim_CustomPartnerFunctionId2 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomVendorPartnerFunctionId = 1 WHERE Dim_CustomVendorPartnerFunctionId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomVendorPartnerFunctionId1 = 1 WHERE Dim_CustomVendorPartnerFunctionId1 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateId3plYard = 1 WHERE Dim_DateId3plYard IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidActualGI = 1 WHERE Dim_DateidActualGI IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidDlvrDocCreated = 1 WHERE Dim_DateidDlvrDocCreated IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdAirorSeaPlanningEstETA = 1 WHERE Dim_DateIdAirorSeaPlanningEstETA IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidASNExpectedOutput = 1 WHERE Dim_DateidASNExpectedOutput IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidASNOutput = 1 WHERE Dim_DateidASNOutput IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdContainerAtDCDoor = 1 WHERE Dim_DateIdContainerAtDCDoor IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdContainerInYard = 1 WHERE Dim_DateIdContainerInYard IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdDepartureFromOrigin = 1 WHERE Dim_DateIdDepartureFromOrigin IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdETAFinalDest = 1 WHERE Dim_DateIdETAFinalDest IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidFirstDate = 1 WHERE Dim_DateidFirstDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdGR = 1 WHERE Dim_DateIdGR IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdPOBuy = 1 WHERE Dim_DateIdPOBuy IS NULL;

UPDATE fact_shipmentindelivery_temp SET Dim_DateIdPOETA = 1 WHERE Dim_DateIdPOETA IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdReqDeliveryDate = 1 WHERE Dim_DateIdReqDeliveryDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidShipDlvrFill = 1 WHERE Dim_DateidShipDlvrFill IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdShipmentCreated = 1 WHERE Dim_DateIdShipmentCreated IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidShipmentDelivery = 1 WHERE Dim_DateidShipmentDelivery IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdShipmentEnd = 1 WHERE Dim_DateIdShipmentEnd IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdSOCancelDate = 1 WHERE Dim_DateIdSOCancelDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidValidFrom = 1 WHERE Dim_DateidValidFrom IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidValidTo = 1 WHERE Dim_DateidValidTo IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DocumentCategoryid = 1 WHERE Dim_DocumentCategoryid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_MeansofTransTypeId = 1 WHERE Dim_MeansofTransTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Partid = 1 WHERE Dim_Partid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Plantid = 1 WHERE Dim_Plantid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_POAFSSeasonId = 1 WHERE Dim_POAFSSeasonId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PODocumentTypeId = 1 WHERE Dim_PODocumentTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PORouteId = 1 WHERE Dim_PORouteId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ProcessingStatusId = 1 WHERE Dim_ProcessingStatusId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ProfitCenterId = 1 WHERE Dim_ProfitCenterId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PurchasegroupId = 1 WHERE Dim_PurchasegroupId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_routeId = 1 WHERE Dim_routeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesDivisionid = 1 WHERE Dim_SalesDivisionid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesDocumentTypeid = 1 WHERE Dim_SalesDocumentTypeid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesGroupid = 1 WHERE Dim_SalesGroupid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesMiscId = 1 WHERE Dim_SalesMiscId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderHeaderStatusid = 1 WHERE Dim_SalesOrderHeaderStatusid IS NULL;
UPDATE fact_shipmentindelivery_temp SET dim_salesorderitemcategoryid = 1 WHERE dim_salesorderitemcategoryid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderItemStatusid = 1 WHERE Dim_SalesOrderItemStatusid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderRejectReasonid = 1 WHERE Dim_SalesOrderRejectReasonid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrgid = 1 WHERE Dim_SalesOrgid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ScheduleLineCategoryId = 1 WHERE Dim_ScheduleLineCategoryId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ShipmentTypeId = 1 WHERE Dim_ShipmentTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_TransactionGroupid = 1 WHERE Dim_TransactionGroupid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_TranspPlanningPointId = 1 WHERE Dim_TranspPlanningPointId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_VendorId = 1 WHERE Dim_VendorId IS NULL;
update fact_shipmentindelivery_temp set Dim_AFSTranspConditionid = 1 where Dim_AFSTranspConditionid is null; /* Marius 28 nov 2014 */
update fact_shipmentindelivery_temp SET dim_dateidactualpo = 1 where dim_dateidactualpo = 0;
update fact_shipmentindelivery_temp set dim_dateidAFSExFactEst = 1 where dim_dateidAFSExFactEst is null;  /* Marius 03 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_dateidMatAvailability  = 1 WHERE dim_dateidMatAvailability IS NULL; /*Marius 15 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_dateidTransfer  = 1 WHERE dim_dateidTransfer IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET Dim_DeliveryRouteId  = 1 WHERE Dim_DeliveryRouteId IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_shippingconditionid  = 1 WHERE dim_shippingconditionid IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_tranportbyshippingtypeid = 1 WHERE dim_tranportbyshippingtypeid IS NULL;

UPDATE fact_shipmentindelivery_temp SET dd_3PLFlag = 'Not Set' WHERE dd_3PLFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_BillofladingNo = 'Not Set' WHERE dd_BillofladingNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_BusinessCustomerPONo = 'Not Set' WHERE dd_BusinessCustomerPONo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CarrierActual  = 'Not Set' WHERE dd_CarrierActual IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CarrierBillingId  = 1 WHERE Dim_CarrierBillingId IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CartonLevelDtlFlag = 'Not Set' WHERE dd_CartonLevelDtlFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ContainerSize = 'Not Set' WHERE dd_ContainerSize IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CurrentShipmentFlag = 'Not Set' WHERE dd_CurrentShipmentFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CustomerPONo = 'Not Set' WHERE dd_CustomerPONo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CustomerPOStatus = 'Not Set' WHERE dd_CustomerPOStatus IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DocumentItemNo = 0 WHERE dd_DocumentItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DocumentNo  = 'Not Set' WHERE dd_DocumentNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DomesticContainerID  = 'Not Set' WHERE dd_DomesticContainerID IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_InboundDeliveryItemNo  = 0 WHERE dd_InboundDeliveryItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_InboundDeliveryNo  = 'Not Set' WHERE dd_InboundDeliveryNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ItemRelForDelv = 'Not Set' WHERE dd_ItemRelForDelv IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_MessageType = 'Not Set' WHERE dd_MessageType IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_MovementType = 'Not Set' WHERE dd_MovementType IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_OriginContainerId = 'Not Set' WHERE dd_OriginContainerId IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_PackageCount = 0 WHERE dd_PackageCount IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ProcessingTime = '000000' WHERE dd_ProcessingTime IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesDocNo = 'Not Set' WHERE dd_SalesDocNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesItemNo = 0 WHERE dd_SalesItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesScheduleNo = 0 WHERE dd_SalesScheduleNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ScheduleNo = 0 WHERE dd_ScheduleNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentItemNo = 0 WHERE dd_ShipmentItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentNo = 'Not Set' WHERE dd_ShipmentNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_Vessel = 'Not Set' WHERE dd_Vessel IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_VoyageNo = 'Not Set' WHERE dd_VoyageNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentStatus = 'Not Set' WHERE dd_ShipmentStatus IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DeliveryNumber = 'Not Set' WHERE dd_DeliveryNumber IS NULL;   /* Marius 17 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_transportconditionid = 1 WHERE dim_transportconditionid IS NULL;

UPDATE fact_shipmentindelivery_temp SET ct_ConfirmedQty = 0 WHERE ct_ConfirmedQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_DelayDays = 0 WHERE ct_DelayDays IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_InboundDeliveryQty = 0 WHERE ct_InboundDeliveryQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_InboundDlvryCMtrVolume = 0 WHERE ct_InboundDlvryCMtrVolume IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_POScheduleQty = 0 WHERE ct_POScheduleQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_PriceUnit = 0 WHERE ct_PriceUnit IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_POItemReceivedQty = 0 WHERE ct_POItemReceivedQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_ReceivedQty = 0 WHERE ct_ReceivedQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_ShipmentQty = 0 WHERE ct_ShipmentQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_DeliveryGrossWght = 0 WHERE ct_DeliveryGrossWght IS NULL;  /* Marius 17 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET ct_deliveryNetWght = 0 WHERE ct_deliveryNetWght IS NULL;  /* Marius 17 Dec 2014 */

DROP TABLE IF EXISTS tmp_tsegeUpdateEventDates;

delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery;

truncate table fact_shipmentindelivery ;


insert into fact_shipmentindelivery i
(FACT_SHIPMENTINDELIVERYID	,
DD_SHIPMENTNO	,
DD_SHIPMENTITEMNO	,
DD_SALESDOCNO	,
DD_DOCUMENTNO	,
DD_DOCUMENTITEMNO	,
CT_INBOUNDDELIVERYQTY	,
CT_CONFIRMEDQTY	,
CT_RECEIVEDQTY	,
CT_PRICEUNIT	,
DIM_DATEID3PLYARD	,
DIM_DATEIDASNOUTPUT	,
DIM_DATEIDASNEXPECTEDOUTPUT	,
DIM_PURCHASEGROUPID	,
DIM_DATEIDSHIPMENTEND	,
DIM_PLANTID	,
DIM_COMPANYID	,
DIM_SALESDIVISIONID	,
DIM_DOCUMENTCATEGORYID	,
DIM_SALESDOCUMENTTYPEID	,
DIM_SALESORGID	,
DIM_CUSTOMERID	,
DIM_DATEIDVALIDFROM	,
DIM_DATEIDVALIDTO	,
DIM_SALESGROUPID	,
DIM_COSTCENTERID	,
DIM_CONTROLLINGAREAID	,
DIM_BILLINGBLOCKID	,
DIM_TRANSACTIONGROUPID	,
DIM_SALESORDERREJECTREASONID	,
DIM_PARTID	,
DIM_SALESORDERHEADERSTATUSID	,
DIM_SALESORDERITEMSTATUSID	,
DIM_CUSTOMERGROUP1ID	,
DIM_CUSTOMERGROUP2ID	,
DIM_SALESORDERITEMCATEGORYID	,
DIM_DATEIDFIRSTDATE	,
DIM_SCHEDULELINECATEGORYID	,
DIM_SALESMISCID	,
DD_ITEMRELFORDELV	,
DIM_DATEIDSHIPMENTDELIVERY	,
DIM_DATEIDACTUALGI	,
DIM_DATEIDSHIPDLVRFILL	,
DIM_PROFITCENTERID	,
DIM_CUSTOMERIDSHIPTO	,
DIM_CUSTOMPARTNERFUNCTIONID	,
DIM_CUSTOMPARTNERFUNCTIONID1	,
DIM_CUSTOMPARTNERFUNCTIONID2	,
DD_CUSTOMERPONO	,
DIM_DATEIDSOCANCELDATE	,
DD_BUSINESSCUSTOMERPONO	,
DIM_ROUTEID	,
DD_CARRIERACTUAL	,
DD_PACKAGECOUNT	,
DD_CARTONLEVELDTLFLAG	,
DIM_DATEIDCONTAINERATDCDOOR	,
DIM_DATEIDCONTAINERINYARD	,
DD_CONTAINERSIZE	,
DD_CURRENTSHIPMENTFLAG	,
DIM_DATEIDDEPARTUREFROMORIGIN	,
DIM_DATEIDETAFINALDEST	,
DIM_DATEIDGR	,
DD_ORIGINCONTAINERID	,
CT_INBOUNDDLVRYCMTRVOLUME	,
DD_BILLOFLADINGNO	,
DIM_DATEIDAIRORSEAPLANNINGESTETA	,
DD_INBOUNDDELIVERYNO	,
DD_INBOUNDDELIVERYITEMNO	,
DIM_VENDORID	,
DIM_MEANSOFTRANSTYPEID	,
DD_MESSAGETYPE	,
DD_MOVEMENTTYPE	,
DIM_DATEIDPOBUY	,
DIM_DATEIDPOETA	,
DIM_PODOCUMENTTYPEID	,
DIM_POROUTEID	,
CT_POSCHEDULEQTY	,
DD_SCHEDULENO	,
DIM_POAFSSEASONID	,
DIM_SHIPMENTTYPEID	,
CT_SHIPMENTQTY	,
DD_VESSEL	,
DD_VOYAGENO	,
DIM_DATEIDSHIPMENTCREATED	,
DD_DOMESTICCONTAINERID	,
DD_3PLFLAG	,
DIM_DATEIDREQDELIVERYDATE	,
DD_CUSTOMERPOSTATUS	,
DIM_CUSTOMVENDORPARTNERFUNCTIONID	,
DIM_CUSTOMVENDORPARTNERFUNCTIONID1	,
DD_SALESITEMNO	,
DD_SALESSCHEDULENO	,
DIM_PROCESSINGSTATUSID	,
DIM_TRANSPPLANNINGPOINTID	,
CT_DELAYDAYS	,
DD_PROCESSINGTIME	,
DIM_PRODUCTHIERARCHYID	,
DIM_CARRIERBILLINGID	,
AMT_SO_AFSNETVALUE	,
DIM_DATEIDDLVRDOCCREATED	,
CT_POINBOUNDDELIVERYQTY	,
DIM_DATEIDPODELIVERY	,
CT_DELIVRECEIVEDQTY	,
DD_SHIPMENTSTATUS	,
CT_POITEMRECEIVEDQTY	,
CT_OPENQTY	,
CT_PRESHIPQTY	,
CT_INTRANSITQTY	,
CT_CLOSEDQTY	,
CT_CANCELLEDQTY	,
DIM_PURCHASEMISCID	,
DD_DELETIONINDICATOR	,
CT_SCHEDULELINEQTY	,
DIM_INBROUTEID	,
DW_INSERT_DATE	,
DW_UPDATE_DATE	,
DIM_PROJECTSOURCEID	,
CT_ITEMQTY	,
DIM_AFSTRANSPCONDITIONID	,
DIM_DATEIDACTUALPO	,
DIM_DATEIDAFSEXFACTEST	,
DIM_DATEIDMATAVAILABILITY	,
DD_DELIVERYNUMBER	,
DIM_DATEIDTRANSFER	,
CT_DELIVERYGROSSWGHT	,
CT_DELIVERYNETWGHT	,
DIM_DELIVERYROUTEID	,
DIM_SHIPPINGCONDITIONID	,
DIM_TRANPORTBYSHIPPINGTYPEID	,
DD_SHORTTEXT	,
DD_SEALNUMBER	,
DIM_TRANSPORTCONDITIONID	,
DIM_DATEIDINBOUNDDELIVERYDATE	,
DD_NOOFFOREIGNTRADE,
dd_seqnoofvendorconf,
dim_shippinginstructionid,
dd_confirmationcateg,
dd_creationindvendorconf,
dim_deliverydateofvendorconf,
dim_bwproducthierarchyid,
dim_mdg_partid	)


Select

IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN
           WHERE TABLE_NAME = 'fact_shipmentindelivery' ), 1) + ROW_NUMBER() OVER(order by '')  fact_shipmentindeliveryid,
DD_SHIPMENTNO	,
DD_SHIPMENTITEMNO	,
ifnull(DD_SALESDOCNO, 'Not Set'),
IFNULL(DD_DOCUMENTNO, 'Not Set'),
IFNULL(DD_DOCUMENTITEMNO,0),
IFNULL(CT_INBOUNDDELIVERYQTY,0),
IFNULL(CT_CONFIRMEDQTY,0)	,
IFNULL(CT_RECEIVEDQTY,0),
IFNULL(CT_PRICEUNIT,0)	,
IFNULL(DIM_DATEID3PLYARD, 1)	,
ifnull(DIM_DATEIDASNOUTPUT,1)	,
ifnull(DIM_DATEIDASNEXPECTEDOUTPUT,1)	,
ifnull(DIM_PURCHASEGROUPID, 1),
ifnull(DIM_DATEIDSHIPMENTEND,1)	,
ifnull(DIM_PLANTID,1)	,
ifnull(DIM_COMPANYID,1)	,
ifnull(DIM_SALESDIVISIONID,1)	,
ifnull(DIM_DOCUMENTCATEGORYID,1)	,
ifnull(DIM_SALESDOCUMENTTYPEID,1)	,
ifnull(DIM_SALESORGID, 1)	,
ifnull(DIM_CUSTOMERID, 1)	,
ifnull(DIM_DATEIDVALIDFROM,1)	,
ifnull(DIM_DATEIDVALIDTO, 1),
ifnull(DIM_SALESGROUPID, 1)	,
ifnull(DIM_COSTCENTERID,1)	,
ifnull(DIM_CONTROLLINGAREAID,1)	,
ifnull(DIM_BILLINGBLOCKID,1)	,
ifnull(DIM_TRANSACTIONGROUPID,1)	,
ifnull(DIM_SALESORDERREJECTREASONID,1)	,
ifnull(DIM_PARTID, 1) ,
ifnull(DIM_SALESORDERHEADERSTATUSID, 1)	,
ifnull(DIM_SALESORDERITEMSTATUSID,1)	,
ifnull(DIM_CUSTOMERGROUP1ID,1)	,
ifnull(DIM_CUSTOMERGROUP2ID, 1)	,
ifnull(DIM_SALESORDERITEMCATEGORYID,1)	,
ifnull(DIM_DATEIDFIRSTDATE,1)	,
ifnull(DIM_SCHEDULELINECATEGORYID, 1)	,
ifnull(DIM_SALESMISCID,1)	,
ifnull(DD_ITEMRELFORDELV, 'Not Set'),
ifnull(DIM_DATEIDSHIPMENTDELIVERY,1)	,
ifnull(DIM_DATEIDACTUALGI, 1)	,
ifnull(DIM_DATEIDSHIPDLVRFILL,1)	,
ifnull(DIM_PROFITCENTERID,1)	,
ifnull(DIM_CUSTOMERIDSHIPTO, 1)	,
ifnull(DIM_CUSTOMPARTNERFUNCTIONID,1)	,
ifnull(DIM_CUSTOMPARTNERFUNCTIONID1	, 1),
ifnull(DIM_CUSTOMPARTNERFUNCTIONID2, 1),
ifnull(DD_CUSTOMERPONO,'Not Set') ,
ifnull(DIM_DATEIDSOCANCELDATE,1)	,
ifnull(DD_BUSINESSCUSTOMERPONO, 'Not Set'),
ifnull(DIM_ROUTEID,1)	,
ifnull(DD_CARRIERACTUAL, 'Not Set')	,
ifnull(DD_PACKAGECOUNT,0)	,
ifnull(DD_CARTONLEVELDTLFLAG, 'Not Set'),
ifnull (DIM_DATEIDCONTAINERATDCDOOR,1)	,
ifnull (DIM_DATEIDCONTAINERINYARD,1)	,
ifnull(DD_CONTAINERSIZE, 0)	,
ifnull(DD_CURRENTSHIPMENTFLAG, 'Not Set')	,
ifnull(DIM_DATEIDDEPARTUREFROMORIGIN,1)	,
ifnull(DIM_DATEIDETAFINALDEST,1)	,
ifnull(DIM_DATEIDGR,1)	,
ifnull(DD_ORIGINCONTAINERID, 'Not Set'),
ifnull(CT_INBOUNDDLVRYCMTRVOLUME, 0),
ifnull(DD_BILLOFLADINGNO, 'Not Set'),
ifnull(DIM_DATEIDAIRORSEAPLANNINGESTETA,1)	,
ifnull(DD_INBOUNDDELIVERYNO,'Not Set')	,
ifnull(DD_INBOUNDDELIVERYITEMNO,0)	,
ifnull(DIM_VENDORID,1)	,
ifnull(DIM_MEANSOFTRANSTYPEID,1)	,
ifnull(DD_MESSAGETYPE,'Not Set'	),
ifnull(DD_MOVEMENTTYPE, 'Not Set'	),
ifnull(DIM_DATEIDPOBUY,1)	,
ifnull(DIM_DATEIDPOETA,1)	,
ifnull(DIM_PODOCUMENTTYPEID,1)	,
ifnull(DIM_POROUTEID,1)	,
ifnull(CT_POSCHEDULEQTY,0)	,
ifnull(DD_SCHEDULENO,0)	,
ifnull(DIM_POAFSSEASONID,1)	,
ifnull(DIM_SHIPMENTTYPEID,1)	,
ifnull(	CT_SHIPMENTQTY	,	0)	,
ifnull(	DD_VESSEL	,	'Not Set')	,
ifnull(	DD_VOYAGENO	,	'Not Set')	,
ifnull(	DIM_DATEIDSHIPMENTCREATED	,	1)	,
ifnull(	DD_DOMESTICCONTAINERID	,	0)	,
ifnull(	DD_3PLFLAG	,	'Not Set')	,
ifnull(	DIM_DATEIDREQDELIVERYDATE	,	1)	,
ifnull(	DD_CUSTOMERPOSTATUS	,	'Not Set')	,
ifnull(	DIM_CUSTOMVENDORPARTNERFUNCTIONID	,	1)	,
ifnull(	DIM_CUSTOMVENDORPARTNERFUNCTIONID1	,	1)	,
ifnull(	DD_SALESITEMNO	,	0)	,
ifnull(	DD_SALESSCHEDULENO	,	0)	,
ifnull(	DIM_PROCESSINGSTATUSID	,	1)	,
ifnull(	DIM_TRANSPPLANNINGPOINTID	,	1)	,
ifnull(	CT_DELAYDAYS	,	0)	,
ifnull(	DD_PROCESSINGTIME	,	'Not Set')	,
ifnull(	DIM_PRODUCTHIERARCHYID	,	1)	,
ifnull(	DIM_CARRIERBILLINGID	,	1)	,
ifnull(	AMT_SO_AFSNETVALUE	,	0)	,
ifnull(	DIM_DATEIDDLVRDOCCREATED	,	1)	,
ifnull(	CT_POINBOUNDDELIVERYQTY	,	0)	,
ifnull(	DIM_DATEIDPODELIVERY	,	1)	,
ifnull(	CT_DELIVRECEIVEDQTY	,	0)	,
ifnull(	DD_SHIPMENTSTATUS	,	'Not Set')	,
ifnull(	CT_POITEMRECEIVEDQTY	,	0)	,
ifnull(	CT_OPENQTY	,	0)	,
ifnull(	CT_PRESHIPQTY	,	0)	,
ifnull(	CT_INTRANSITQTY	,	0)	,
ifnull(	CT_CLOSEDQTY	,	0)	,
ifnull(	CT_CANCELLEDQTY	,	0)	,
ifnull(	DIM_PURCHASEMISCID	,	1)	,
ifnull(	DD_DELETIONINDICATOR	,	'Not Set')	,
ifnull(	CT_SCHEDULELINEQTY	,	0)	,
ifnull(	DIM_INBROUTEID	,	1)	,
ifnull(	DW_INSERT_DATE	,	CURRENT_DATE	),
ifnull(	DW_UPDATE_DATE	,	CURRENT_DATE	),
ifnull(	DIM_PROJECTSOURCEID	,	1)	,
ifnull(	CT_ITEMQTY	,	0)	,
ifnull(	DIM_AFSTRANSPCONDITIONID	,	1)	,
ifnull(	DIM_DATEIDACTUALPO	,	1)	,
ifnull(	DIM_DATEIDAFSEXFACTEST	,	1)	,
ifnull(	DIM_DATEIDMATAVAILABILITY	,	1)	,
ifnull(	DD_DELIVERYNUMBER	,	'Not Set')	,
ifnull(	DIM_DATEIDTRANSFER	,	1)	,
ifnull(	CT_DELIVERYGROSSWGHT	,	0)	,
ifnull(	CT_DELIVERYNETWGHT	,	0)	,
ifnull(	DIM_DELIVERYROUTEID	,	1)	,
ifnull(	DIM_SHIPPINGCONDITIONID	,	1)	,
ifnull(	DIM_TRANPORTBYSHIPPINGTYPEID	,	1)	,
ifnull(	DD_SHORTTEXT	,	'Not Set')	,
ifnull(	DD_SEALNUMBER	,	'Not Set')	,
ifnull(	DIM_TRANSPORTCONDITIONID	,	1)	,
ifnull(	DIM_DATEIDINBOUNDDELIVERYDATE	,	1)	,
ifnull(	DD_NOOFFOREIGNTRADE	,	'Not Set'),
ifnull(dd_seqnoofvendorconf,0),
ifnull(dim_shippinginstructionid,1),
ifnull(dd_confirmationcateg,'Not Set'),
ifnull(dd_creationindvendorconf,'Not Set'),
ifnull(dim_deliverydateofvendorconf,1),
ifnull(dim_bwproducthierarchyid,1),
ifnull(dim_mdg_partid,1)
from  fact_shipmentindelivery_temp d;

UPDATE fact_shipmentindelivery f
SET f.dim_mdg_partid = ifnull(mdg.dim_mdg_partid, 1)
FROM fact_shipmentindelivery f, dim_part dp, dim_mdg_part mdg
WHERE dp.PartNumber=  mdg.PartNumber
  AND dp.dim_partid  =  f.dim_partid
  AND mdg.milliporeflag = 'Not Set'
  AND f.dim_mdg_partid <> ifnull(mdg.dim_mdg_partid, 1);

/* Goods Issue Date for S-Curve - Oana 10 Oct 2016 */
drop table if exists tmp_ekbe_sid;
create table tmp_ekbe_sid as
	select EKBE_EBELN, EKBE_EBELP, min(EKBE_BUDAT) as EKBE_BUDAT
	from ekbe_sid
	where ekbe_vgabe='1'
	group by EKBE_EBELN, EKBE_EBELP;

update fact_shipmentindelivery f
	set f.dim_dateidlastpostingdate = ifnull(dim_dateid, 1)
from fact_shipmentindelivery f, tmp_ekbe_sid t, dim_Date dt
where f.DD_DOCUMENTNO = t.EKBE_EBELN
	and  f.DD_DOCUMENTITEMNO = t.EKBE_EBELP
	and t.EKBE_BUDAT = dt.datevalue
	and companycode = 'Not Set'
	and f.dim_dateidlastpostingdate <> ifnull(dim_dateid, 1);
/* END Goods Issue Date for S-Curve - Oana 10 Oct 2016 */


  drop table if exists tmp_EKES_LIKP_LIPS;
create table tmp_EKES_LIKP_LIPS as
	select EKES_EBELN,EKES_EBELP, min(LIKP_ERDAT) as LIKP_ERDAT
	from EKES_LIKP_LIPS
	group by EKES_EBELN,EKES_EBELP;

update fact_shipmentindelivery f
	set f.Dim_DateidDlvrDocCreated = ifnull(dim_dateid, 1)
from fact_shipmentindelivery f, tmp_EKES_LIKP_LIPS t, dim_Date dt
where f.DD_DOCUMENTNO = t.EKES_EBELN
	and  f.DD_DOCUMENTITEMNO = t.EKES_EBELP
	and t.LIKP_ERDAT = dt.datevalue
	and companycode = 'Not Set'
	and f.Dim_DateidDlvrDocCreated <> ifnull(dim_dateid, 1);


  update fact_shipmentindelivery f
set f.Dim_PODateidCreate = ifnull(ff.Dim_DateidCreate, 1)
from fact_shipmentindelivery f,fact_purchase ff
where f.dd_DocumentNo=ff.dd_DocumentNo
and f.dd_DocumentItemNo=ff.dd_DocumentItemNo
and f.dd_ScheduleNo=ff.dd_ScheduleNo
and f.Dim_PODateidCreate <> ifnull(ff.Dim_DateidCreate, 1);

drop table if exists fact_shipmentindelivery_temp;
drop table if exists tmp_ekbe_sid;



drop table if exists tmp_salesinfo;
create table tmp_salesinfo
as select distinct so.dd_SalesDocNo,  so.dd_SalesItemNo, so.dd_ScheduleNo
,so.dim_dateidsalesordercreated
from fact_SalesOrder so;

update facT_shipmentindelivery f
set f.dim_dateidsalesordercreated = ifnull(ff.dim_dateidsalesordercreated, 1)
from facT_shipmentindelivery f,fact_SalesOrder ff
where f.dd_SalesDocNo=ff.dd_SalesDocNo and f.dd_SalesItemNo =ff.dd_SalesItemNo and f.dd_SalesScheduleNo =ff.dd_ScheduleNo
and f.dim_dateidsalesordercreated <> ifnull(ff.dim_dateidsalesordercreated, 1);

drop table if exists tmp_actualdategi_merckls_ship;
create table tmp_actualdategi_merckls_ship
as select distinct f.DD_DOCUMENTNO,
f.dd_DocumentItemNo,
first_value(f.DIM_DATEIDACTUALGI) over
(partition by f.DD_DOCUMENTNO, f.dd_DocumentItemNo order by dd.datevalue desc) as DD_DATEIDACTUALGI
from  fact_shipmentindelivery f
	inner join dim_date dd on f.DIM_DATEIDACTUALGI = dd.dim_dateid;

update fact_shipmentindelivery f
set f.dim_dateidactualgimerckls = ifnull(t.DD_DATEIDACTUALGI, 1)
from fact_shipmentindelivery f, tmp_actualdategi_merckls_ship t
where f.DD_DOCUMENTNO = t.DD_DOCUMENTNO
and f.dd_DocumentItemNo = t.dd_DocumentItemNo
and f.dim_dateidactualgimerckls <> ifnull(t.DD_DATEIDACTUALGI, 1);


drop table if exists tmp_salesinfo;
create table tmp_salesinfo
as select distinct so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero
,min(d.datevalue)datevalue
from fact_SalesOrder so,dim_mdg_part mdg,dim_date d,dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidsalesordercreated=d.dim_dateid
-- and dd_scheduleno=dd_min_scheduleno
and so.dim_dateidsalesordercreated<>1
group by so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero;

update facT_shipmentindelivery f
set f.dim_dateidsalesordercreated_interco = ifnull(d.dim_dateid, 1)
from facT_shipmentindelivery f,tmp_salesinfo ff,dim_mdg_part mdg,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateidsalesordercreated_interco <> ifnull(d.dim_dateid, 1);

drop table if exists tmp_salesinfo_magidate;
create table tmp_salesinfo_magidate
as select distinct so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero
,min(d.datevalue)datevalue
from fact_SalesOrder so,dim_mdg_part mdg,dim_date d,dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidactualgimerckls=d.dim_dateid
-- and dd_scheduleno=dd_min_scheduleno
and so.dim_dateidactualgimerckls<>1
group by so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero;

update facT_shipmentindelivery f
set f.dim_dateidactualgimerckls_interco =  ifnull(d.dim_dateid, 1)
from facT_shipmentindelivery f,tmp_salesinfo_magidate ff,dim_mdg_part mdg,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateidactualgimerckls_interco <> ifnull(d.dim_dateid, 1);


drop table if exists tmp_salesinfo;
create table tmp_salesinfo
as select distinct so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero
,min(d.datevalue)datevalue
from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidsalesordercreated=d.dim_dateid
-- and dd_scheduleno=dd_min_scheduleno
and so.dim_dateidsalesordercreated<>1
group by so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero;

update facT_shipmentindelivery f
set f.dim_dateidsalesordercreated_interco = ifnull(d.dim_dateid, 1)
from facT_shipmentindelivery f,tmp_salesinfo ff,dim_mdg_part mdg,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.partnumber=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateidsalesordercreated_interco <> ifnull(d.dim_dateid, 1)
and f.dim_dateidsalesordercreated_interco=1;

/*
drop table if exists tmp_salesinfo
create table tmp_salesinfo
as select distinct substring(so.dd_BusinessCustomerPONo,1,10) dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero
,min(d.datevalue)datevalue
from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidsalesordercreated=d.dim_dateid
and so.dim_dateidsalesordercreated<>1
group by substring(so.dd_BusinessCustomerPONo,1,10),mdg.mdgpartnumber_noleadzero 

drop table if exists tmp_salesinfo_2
create table tmp_salesinfo_2
as select distinct substring(so.dd_BusinessCustomerPONo,1,10) dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero,
dd_salesdocno,dd_salesitemno
,row_number() over (partition by substring(so.dd_BusinessCustomerPONo,1,10),mdg.mdgpartnumber_noleadzero
order by ''
) rn

from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
,tmp_salesinfo t
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidsalesordercreated=d.dim_dateid
and so.dim_dateidsalesordercreated<>1
and mdg.partnumber=t.mdgpartnumber_noleadzero
and substring(so.dd_BusinessCustomerPONo,1,10)=t.dd_BusinessCustomerPONo
and d.datevalue=t.datevalue */
drop table if exists tmp_salesinfo_2;
create table tmp_salesinfo_2
as select distinct substring(so.dd_BusinessCustomerPONo,1,10) dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero,
dd_salesdocno,dd_salesitemno
,row_number() over (partition by substring(so.dd_BusinessCustomerPONo,1,10),mdg.mdgpartnumber_noleadzero
order by ''
) rn

from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02';

update facT_shipmentindelivery f
set dd_SalesDocNo_Interco = ifnull(ff.dd_salesdocno, 'Not Set')
from facT_shipmentindelivery f,tmp_salesinfo_2 ff,dim_mdg_part mdg
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and rn=1
and dd_SalesDocNo_Interco <> ifnull(ff.dd_salesdocno, 'Not Set');



update facT_shipmentindelivery f
set dd_SalesDocNo_Interco = ifnull(ff.dd_salesdocno, 'Not Set')
from facT_shipmentindelivery f,tmp_salesinfo_2 ff,dim_mdg_part mdg,dim_part dp--,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and f.dim_partid=dp.dim_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and rn=1
and dd_SalesDocNo_Interco <> ifnull(ff.dd_salesdocno, 'Not Set')
and dd_SalesDocNo_Interco='Not Set';

update facT_shipmentindelivery f
set dd_SalesItemNo_Interco=to_char(ff.dd_salesitemno)
from facT_shipmentindelivery f,tmp_salesinfo_2 ff,dim_mdg_part mdg--,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and to_char(dd_SalesDocNo_Interco)=to_char(ff.dd_salesdocno)
and rn=1
and dd_SalesItemNo_Interco<>ff.dd_salesitemno;

update facT_shipmentindelivery f
set dd_SalesItemNo_Interco=to_char(ff.dd_salesitemno)
from facT_shipmentindelivery f,tmp_salesinfo_2 ff,dim_mdg_part mdg,dim_part dp--,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and f.dim_partid=dp.dim_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and to_char(dd_SalesDocNo_Interco)=to_char(ff.dd_salesdocno)
and rn=1
and dd_SalesItemNo_Interco<>ff.dd_salesitemno
and dd_SalesItemNo_Interco='Not Set';


update facT_shipmentindelivery f
set f.dim_dateidsalesordercreated_interco = ifnull(d.dim_dateid, 1)
from facT_shipmentindelivery f,tmp_salesinfo ff,dim_mdg_part mdg,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateidsalesordercreated_interco <> ifnull(d.dim_dateid, 1)
and f.dim_dateidsalesordercreated_interco=1;

update facT_shipmentindelivery f
set f.dim_dateidsalesordercreated_interco = ifnull(d.dim_dateid, 1)
from facT_shipmentindelivery f,tmp_salesinfo ff,dim_mdg_part mdg,dim_date d,dim_company c,dim_part dp
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and f.dim_partid=dp.dim_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateidsalesordercreated_interco <> ifnull(d.dim_dateid, 1)
and f.dim_dateidsalesordercreated_interco=1;




drop table if exists tmp_salesinfo_magidate;
create table tmp_salesinfo_magidate
as select distinct so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero
,min(d.datevalue)datevalue
from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidactualgimerckls=d.dim_dateid
-- and dd_scheduleno=dd_min_scheduleno
and so.dim_dateidactualgimerckls<>1
group by so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero;

update facT_shipmentindelivery f
set f.dim_dateidactualgimerckls_interco = ifnull(d.dim_dateid, 1)
from facT_shipmentindelivery f,tmp_salesinfo_magidate ff,dim_mdg_part mdg,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateidactualgimerckls_interco=1
and f.dim_dateidactualgimerckls_interco <> ifnull(d.dim_dateid, 1);


update facT_shipmentindelivery f
set f.dim_dateidactualgimerckls_interco = ifnull(d.dim_dateid, 1)
from facT_shipmentindelivery f,tmp_salesinfo_magidate ff,dim_mdg_part mdg,dim_date d,dim_company c,dim_part dp
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and f.dim_partid=dp.dim_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateidactualgimerckls_interco=1
and f.dim_dateidactualgimerckls_interco <> ifnull(d.dim_dateid, 1);

drop table if exists min_budat;
create table min_budat
as select distinct  EKBE_EBELN,EKBE_EBELP,min(ekbe_budat) ekbe_budat
from ekbe_sid_2
where ekbe_bwart is not null
group by EKBE_EBELN,EKBE_EBELP;

update ekbe_sid_2 e
set min_budat=m.ekbe_budat
from ekbe_sid_2 e,min_budat m
where e.EKBE_EBELN=m.EKBE_EBELN and
e.EKBE_EBELP=m.EKBE_EBELP;


drop table if exists tmp_ekbe_sid_test;
create table tmp_ekbe_sid_test
as select distinct EKBE_EBELN,EKBE_EBELP,min(ekbe_bwart) ekbe_bwart
from ekbe_sid_2
WHERE
ekbe_BEWTP='E'
and to_char(ekbe_vgabe)='1'
and min_budat= ekbe_budat
group by EKBE_EBELN,EKBE_EBELP;


update fact_shipmentindelivery f
set dd_MovementType = ifnull(t.ekbe_bwart, 'Not Set')
from fact_shipmentindelivery f,tmp_ekbe_sid_test t
where trim(f.DD_DOCUMENTNO) = trim(t.EKBE_EBELN)
	and  f.DD_DOCUMENTITEMNO = t.EKBE_EBELP;




delete
from facT_shipmentindelivery f
where f.facT_shipmentindeliveryid in
(select distinct facT_shipmentindeliveryid from facT_shipmentindelivery f,dim_date d
where f.dim_podateidcreate=d.dim_dateid
and datevalue<'2014-01-01'
and f.dim_podateidcreate<>1);



/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate;
create table tmp_updatedate as
select max(dw_update_date) dd_updatedate from  fact_shipmentindelivery;


update fact_shipmentindelivery f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_shipmentindelivery f, tmp_updatedate t
where f.dd_updatedate <> t.dd_updatedate;


  drop table if exists tmp_EKES;
create table tmp_EKES as
	select EKPO_EBELN,EKPO_EBELP, min(EKES_ERDAT) as EKES_ERDAT
	from EKES
where ekes_ebtyp='LA'
	group by EKPO_EBELN,EKPO_EBELP;

update fact_shipmentindelivery f
	set f.Dim_DateidDlvrDocCreated = ifnull(dim_dateid, 1)
from fact_shipmentindelivery f, tmp_EKES t, dim_Date dt
where f.DD_DOCUMENTNO = t.EKPO_EBELN
	and  f.DD_DOCUMENTITEMNO = t.EKPO_EBELP
	and t.EKES_ERDAT = dt.datevalue
	and companycode = 'Not Set'
	and f.Dim_DateidDlvrDocCreated <> ifnull(dim_dateid, 1)
and f.Dim_DateidDlvrDocCreated =1;


drop table if exists tmp_EKES;
create table tmp_EKES as
	select EKPO_EBELN,EKPO_EBELP, min(EKES_ERDAT) as EKES_ERDAT
	from EKES
where ekes_ebtyp='LA'
and ekes_DABMG<>0
	group by EKPO_EBELN,EKPO_EBELP;

update fact_shipmentindelivery f
	set f.Dim_DateidDlvrDocCreated = ifnull(dim_dateid, 1)
from fact_shipmentindelivery f, tmp_EKES t, dim_Date dt
where f.DD_DOCUMENTNO = t.EKPO_EBELN
	and  f.DD_DOCUMENTITEMNO = t.EKPO_EBELP
	and t.EKES_ERDAT = dt.datevalue
	and companycode = 'Not Set'
	and f.Dim_DateidDlvrDocCreated <> ifnull(dim_dateid, 1);


update facT_shipmentindelivery f
set f.Dim_AccountCategoryid=ff.Dim_AccountCategoryid
from facT_shipmentindelivery f,fact_purchase ff
where f.dd_DocumentNo = ff.dd_DocumentNo
  AND f.dd_documentItemNo = ff.dd_DocumentItemNo
  AND f.dd_ScheduleNo = ff.dd_ScheduleNo
and  f.Dim_AccountCategoryid<>ff.Dim_AccountCategoryid;

update fact_shipmentindelivery f
	set f.Dim_PODateidCreate = ifnull(dim_dateid, 1)
from fact_shipmentindelivery f,  EKKO_new ds, dim_Date dt
where trim(f.DD_DOCUMENTNO) = trim(ds.ekko_ebeln)
and  ds.EkkO_AEDAT = dt.datevalue
	and companycode = 'Not Set'
	and f.Dim_PODateidCreate <> ifnull(dim_dateid, 1)
and f.Dim_PODateidCreate=1;


 drop table if exists tmp_EKES_1;
create table tmp_EKES_1 as
	select distinct EKPO_EBELN,EKPO_EBELP,ekes_ebtyp
	from
ekes
where ekes_ebtyp='LA';

update fact_shipmentindelivery f
	set dd_confirm_categ_flag = 'X'
from fact_shipmentindelivery f,  tmp_EKES_1 t
where f.DD_DOCUMENTNO = t.EKPO_EBELN
	and  f.DD_DOCUMENTITEMNO = t.EKPO_EBELP
and f.dd_confirmationcateg=ekes_ebtyp;

  drop table if exists tmp_EKES_2;
create table tmp_EKES_2 as
	select distinct EKPO_EBELN,EKPO_EBELP,ekes_ebtyp
 from
ekes
where ekes_ebtyp<>'LA';

 drop table if exists tmp_EKES_3;
create table tmp_EKES_3 as
 select distinct count(*) count,EKPO_EBELN,EKPO_EBELP
 from
ekes
group by EKPO_EBELN,EKPO_EBELP
having count(*)<=1;

update fact_shipmentindelivery f
 set dd_confirm_categ_flag = case when ekes_ebtyp='AB' then 'X'
else 'Not Set' end
from fact_shipmentindelivery f, tmp_EKES_2 t,tmp_EKES_3 tt
where f.DD_DOCUMENTNO = t.EKPO_EBELN
 and  f.DD_DOCUMENTITEMNO = t.EKPO_EBELP
and  f.dd_confirmationcateg=t.ekes_ebtyp
and f.DD_DOCUMENTNO = tt.EKPO_EBELN
 and  f.DD_DOCUMENTITEMNO = tt.EKPO_EBELP
and dim_dateidlastpostingdate<>1
and dd_confirm_categ_flag <> case when ekes_ebtyp='AB' then 'X'
else 'Not Set' end;


drop table if exists tmp_EKES;
create table tmp_EKES as
	select EKPO_EBELN,EKPO_EBELP, min(EKES_ERDAT) as EKES_ERDAT
	from EKES
where ekes_ebtyp='AB'
and ekes_DABMG<>0
	group by EKPO_EBELN,EKPO_EBELP;

update fact_shipmentindelivery f
	set f.Dim_DateidDlvrDocCreated = ifnull(dim_dateid, 1)
from fact_shipmentindelivery f, tmp_EKES t, dim_Date dt
where f.DD_DOCUMENTNO = t.EKPO_EBELN
	and  f.DD_DOCUMENTITEMNO = t.EKPO_EBELP
	and t.EKES_ERDAT = dt.datevalue
	and companycode = 'Not Set'
	and f.Dim_DateidDlvrDocCreated <> ifnull(dim_dateid, 1)
    and f.Dim_DateidDlvrDocCreated=1
    and f.dd_confirm_categ_flag = 'X' and dd_confirmationcateg='AB';

    /* update fact_shipmentindelivery f
set f.DD_INTERCOMPANYFLAG='Internal Vendor'
from dim_vendor v,fact_shipmentindelivery f
where  v.dim_vendorid=f.dim_vendorid
and v.CompanyCode<>'Not Set'
and  f.DD_INTERCOMPANYFLAG<>'Internal Vendor'

update fact_shipmentindelivery f
set f.DD_INTERCOMPANYFLAG='External Vendor'
from dim_vendor v,fact_shipmentindelivery f
where  v.dim_vendorid=f.dim_vendorid
and f.DD_INTERCOMPANYFLAG='Not Set' */

update fact_shipmentindelivery f
set f.DD_INTERCOMPANYFLAG=case when vendornumber2 in ('VCNM1','VKRM1') then 'Internal Vendor - Dropship'
                          when p.Type='Z3' then 'Internal Vendor - Dropship'
                          when p.Type='UB' then 'Internal Vendor - Stock Transport'
                        when v.VendorGroup in ('ZIFV','ZPLT','ZITC','ZIPV','0007','ZFOR') then 'Internal Vendor'
                           else 'External Vendor' end
from dim_vendor v,fact_shipmentindelivery f,dim_documenttype p
where v.dim_vendorid=f.dim_vendorid
and f.dim_podocumenttypeid=p.Dim_DocumentTypeid
and  f.dd_intercompanyflag<>case when vendornumber2 in ('VCNM1','VKRM1') then 'Internal Vendor - Dropship'
                          when p.Type='Z3' then 'Internal Vendor - Dropship'
                          when p.Type='UB' then 'Internal Vendor - Stock Transport'
                        when v.VendorGroup in ('ZIFV','ZPLT','ZITC','ZIPV','0007','ZFOR') then 'Internal Vendor'
                           else 'External Vendor' end;

			   

update facT_shipmentindelivery f
set f.dim_dateiddlvrdoccreated=case when f.dim_dateiddlvrdoccreated=1 then f.dim_dateidactualgimerckls_interco
   else f.dim_dateiddlvrdoccreated end
from facT_shipmentindelivery f
where f.dim_dateiddlvrdoccreated<>case when f.dim_dateiddlvrdoccreated=1 then f.dim_dateidactualgimerckls_interco
   else f.dim_dateiddlvrdoccreated end;		   
			   

update fact_shipmentindelivery f
set  f.dd_shipmentstatus_flag= case when f.dim_dateidlastpostingdate=1 and f.Dim_DateidDlvrDocCreated=1 then 'Not yet delivered'
when f.dim_dateidlastpostingdate=1 and f.Dim_DateidDlvrDocCreated<>1 then 'In transit'
else 'Received' end
from fact_shipmentindelivery f
where dd_confirm_categ_flag='X'
and  f.dd_shipmentstatus_flag<>case when f.dim_dateidlastpostingdate=1 and f.Dim_DateidDlvrDocCreated=1 then 'Not yet delivered'
when f.dim_dateidlastpostingdate=1 and f.Dim_DateidDlvrDocCreated<>1 then 'In transit'
else 'Received' end ;


drop table if exists tmp_salesinfo_plant;
create table tmp_salesinfo_plant
as select distinct so.dd_salesdocno,so.dd_salesitemno
,min(d.datevalue)datevalue
from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidsalesordercreated=d.dim_dateid
and so.dim_dateidsalesordercreated<>1
group by so.dd_salesdocno,so.dd_salesitemno;


drop table if exists tmp_salesinfo_2;
create table tmp_salesinfo_2
as select distinct 
so.dd_salesdocno,so.dd_salesitemno,dp.plant_emd
from emd586.fact_SalesOrder so
,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
,tmp_salesinfo_plant t,emd586.dim_plant dp
where 
so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_plantid=dp.dim_plantid
and so.dim_dateidsalesordercreated=d.dim_dateid
and so.dim_dateidsalesordercreated<>1
and d.datevalue=t.datevalue
and so.dd_salesdocno=t.dd_salesdocno
and so.dd_salesitemno=t.dd_salesitemno;


update facT_shipmentindelivery f
set dd_plantemd = ifnull(plant_emd, 'Not Set')
from facT_shipmentindelivery f,tmp_salesinfo_2 ff
where 
f.dd_SalesDocNo_Interco =ff.dd_salesdocno
and f.dd_SalesItemNo_Interco=ff.dd_salesitemno
and  dd_plantemd <> ifnull(plant_emd, 'Not Set');



drop table if exists tmp_salesinfo_2;
create table tmp_salesinfo_2
as select distinct 
so.dd_salesdocno,so.dd_salesitemno,routecode,routename
from emd586.fact_SalesOrder so
,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
,tmp_salesinfo_plant t,emd586.dim_route rt
where 
so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidsalesordercreated=d.dim_dateid
and so.dim_routeid=rt.dim_routeid
and so.dim_dateidsalesordercreated<>1
and d.datevalue=t.datevalue
and so.dd_salesdocno=t.dd_salesdocno
and so.dd_salesitemno=t.dd_salesitemno
and so.dd_scheduleno=so.dd_min_scheduleno;


update facT_shipmentindelivery f
set dd_routecode = ifnull(routecode, 'Not Set')
from facT_shipmentindelivery f,tmp_salesinfo_2 ff
where 
f.dd_SalesDocNo_Interco =ff.dd_salesdocno
and f.dd_SalesItemNo_Interco=ff.dd_salesitemno
and dd_routecode <> ifnull(routecode, 'Not Set');

update facT_shipmentindelivery f
set dd_routename = ifnull(routename, 'Not Set')
from facT_shipmentindelivery f,tmp_salesinfo_2 ff
where 
f.dd_SalesDocNo_Interco =ff.dd_salesdocno
and f.dd_SalesItemNo_Interco=ff.dd_salesitemno
and dd_routename <>ifnull(routename, 'Not Set');


/* Alina - App-9979 - 20180710 - Update Delivery Doc Created for Internal Vendor - Dropship */

drop table if exists update_delivdoc_date;
create table update_delivdoc_date
as select distinct fact_shipmentindeliveryid,Dim_DateidDlvrDocCreated,f.dim_dateidactualgimerckls_interco
from fact_shipmentindelivery f
where  DD_INTERCOMPANYFLAG ='Internal Vendor - Dropship';

update fact_shipmentindelivery f
	set f.Dim_DateidDlvrDocCreated =1
from fact_shipmentindelivery f
where DD_INTERCOMPANYFLAG ='Internal Vendor - Dropship';

update fact_shipmentindelivery f
	set f.Dim_DateidDlvrDocCreated =case when ff.dim_dateidactualgimerckls_interco=1
                            then ff.Dim_DateidDlvrDocCreated else ff.dim_dateidactualgimerckls_interco end
from fact_shipmentindelivery f,update_delivdoc_date ff
where f.DD_INTERCOMPANYFLAG ='Internal Vendor - Dropship'
and f.fact_shipmentindeliveryid=ff.fact_shipmentindeliveryid;

/* End - Alina - App-9979 - 20180710 - Update Delivery Doc Created for Internal Vendor - Dropship */



/* SO Created Date */


drop table if exists tmp_salesinfo_socreate;
create table tmp_salesinfo_socreate
as select distinct so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero
,min(d.datevalue)datevalue
from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateidsocreated=d.dim_dateid
and so.dim_dateidsocreated<>1
group by so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero;


update facT_shipmentindelivery f
set f.dim_dateidsocreated_interco=d.dim_dateid
from facT_shipmentindelivery f,tmp_salesinfo_socreate ff,dim_mdg_part mdg,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateidsocreated_interco<>d.dim_dateid;


/* Shipment Create Date */

drop table if exists tmp_salesinfo_socreate;
create table tmp_salesinfo_socreate
as select distinct so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero
,min(d.datevalue)datevalue
from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_dateiddlvrdoccreated=d.dim_dateid
and so.dim_dateiddlvrdoccreated<>1
group by so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero;


update facT_shipmentindelivery f
set f.dim_dateiddlvrdoccreated_interco=d.dim_dateid
from facT_shipmentindelivery f,tmp_salesinfo_socreate ff,dim_mdg_part mdg,dim_date d,dim_company c
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_companyid=c.dim_companyid
and c.companycode=d.companycode
and d.datevalue=ff.datevalue
and f.dim_dateiddlvrdoccreated_interco<>d.dim_dateid;

/* Merck LS Confirmed or Requested Date */


drop table if exists tmp_salesinfo_socreate;
create table tmp_salesinfo_socreate
as select distinct so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero
,min(d.datevalue)datevalue
from emd586.fact_SalesOrder so,emd586.dim_mdg_part mdg,emd586.dim_date d,emd586.dim_bwproducthierarchy bw
where so.dim_mdg_partid=mdg.dim_mdg_partid
and so.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and so.dim_mercklsconforreqdateid=d.dim_dateid
and so.dim_mercklsconforreqdateid<>1
group by so.dd_BusinessCustomerPONo,mdg.mdgpartnumber_noleadzero;


update facT_shipmentindelivery f
set f.dim_mercklsconforreqdateid_interco=d.dim_dateid
from facT_shipmentindelivery f,tmp_salesinfo_socreate ff,dim_mdg_part mdg,dim_date_factory_calendar d,
dim_plant dp
where f.dd_DocumentNo=ff.dd_BusinessCustomerPONo
and f.dim_mdg_partid=mdg.dim_mdg_partid
and mdg.mdgpartnumber_noleadzero=ff.mdgpartnumber_noleadzero
and f.dim_plantid=dp.dim_plantid
and dp.plantcode=d.plantcode_factory
and d.datevalue=ff.datevalue
and f.dim_mercklsconforreqdateid_interco<>d.dim_dateid;




delete 
from fact_s_curve f
where f.fact_shipmentindeliveryid in 
(select distinct fact_shipmentindeliveryid
from fact_s_curve f,EMDSialEMEA8DF.main_delete_mapping_scurve d
where f.dd_documentno=d.po_number
and f.dd_DocumentItemNo =d.po_item
and f.dim_projectsourceid=15
and upper(platform) like '%NEXT%');


delete from fact_s_curve;

insert into fact_s_curve f

(

    FACT_SHIPMENTINDELIVERYID,
	DD_SHIPMENTNO,
	DD_SHIPMENTITEMNO,
	DD_SALESDOCNO,
	DD_DOCUMENTNO,
	DD_DOCUMENTITEMNO,
	CT_INBOUNDDELIVERYQTY,
	CT_CONFIRMEDQTY,
	CT_RECEIVEDQTY,
	CT_PRICEUNIT,
	DIM_DATEID3PLYARD,
	DIM_DATEIDASNOUTPUT,
	DIM_DATEIDASNEXPECTEDOUTPUT,
	DIM_PURCHASEGROUPID,
	DIM_DATEIDSHIPMENTEND,
	DIM_PLANTID,
	DIM_COMPANYID,
	DIM_SALESDIVISIONID,
	DIM_DOCUMENTCATEGORYID,
	DIM_SALESDOCUMENTTYPEID,
	DIM_SALESORGID,
	DIM_CUSTOMERID,
	DIM_DATEIDVALIDFROM,
	DIM_DATEIDVALIDTO,
	DIM_SALESGROUPID,
	DIM_COSTCENTERID,
	DIM_CONTROLLINGAREAID,
	DIM_BILLINGBLOCKID,
	DIM_TRANSACTIONGROUPID,
	DIM_SALESORDERREJECTREASONID,
	DIM_PARTID,
	DIM_SALESORDERHEADERSTATUSID,
	DIM_SALESORDERITEMSTATUSID,
	DIM_CUSTOMERGROUP1ID,
	DIM_CUSTOMERGROUP2ID,
	DIM_SALESORDERITEMCATEGORYID,
	DIM_DATEIDFIRSTDATE,
	DIM_SCHEDULELINECATEGORYID,
	DIM_SALESMISCID,
	DD_ITEMRELFORDELV,
	DIM_DATEIDSHIPMENTDELIVERY,
	DIM_DATEIDACTUALGI,
	DIM_DATEIDSHIPDLVRFILL,
	DIM_PROFITCENTERID,
	DIM_CUSTOMERIDSHIPTO,
	DIM_CUSTOMPARTNERFUNCTIONID,
	DIM_CUSTOMPARTNERFUNCTIONID1,
	DIM_CUSTOMPARTNERFUNCTIONID2,
	DD_CUSTOMERPONO,
	DIM_DATEIDSOCANCELDATE,
	DD_BUSINESSCUSTOMERPONO,
	DIM_ROUTEID,
	DD_CARRIERACTUAL,
	DD_PACKAGECOUNT,
	DD_CARTONLEVELDTLFLAG,
	DIM_DATEIDCONTAINERATDCDOOR,
	DIM_DATEIDCONTAINERINYARD,
	DD_CONTAINERSIZE,
	DD_CURRENTSHIPMENTFLAG,
	DIM_DATEIDDEPARTUREFROMORIGIN,
	DIM_DATEIDETAFINALDEST,
	DIM_DATEIDGR,
	DD_ORIGINCONTAINERID,
	CT_INBOUNDDLVRYCMTRVOLUME,
	DD_BILLOFLADINGNO,
	DIM_DATEIDAIRORSEAPLANNINGESTETA,
	DD_INBOUNDDELIVERYNO,
	DD_INBOUNDDELIVERYITEMNO,
	DIM_VENDORID,
	DIM_MEANSOFTRANSTYPEID,
	DD_MESSAGETYPE,
	DD_MOVEMENTTYPE,
	DIM_DATEIDPOBUY,
	DIM_DATEIDPOETA,
	DIM_PODOCUMENTTYPEID,
	DIM_POROUTEID,
	CT_POSCHEDULEQTY,
	DD_SCHEDULENO,
	DIM_POAFSSEASONID,
	DIM_SHIPMENTTYPEID,
	CT_SHIPMENTQTY,
	DD_VESSEL,
	DD_VOYAGENO,
	DIM_DATEIDSHIPMENTCREATED,
	DD_DOMESTICCONTAINERID,
	DD_3PLFLAG,
	DIM_DATEIDREQDELIVERYDATE,
	DD_CUSTOMERPOSTATUS,
	DIM_CUSTOMVENDORPARTNERFUNCTIONID,
	DIM_CUSTOMVENDORPARTNERFUNCTIONID1,
	DD_SALESITEMNO,
	DD_SALESSCHEDULENO,
	DIM_PROCESSINGSTATUSID,
	DIM_TRANSPPLANNINGPOINTID,
	CT_DELAYDAYS,
	DD_PROCESSINGTIME,
	DIM_PRODUCTHIERARCHYID,
	DIM_CARRIERBILLINGID,
	AMT_SO_AFSNETVALUE,
	DIM_DATEIDDLVRDOCCREATED,
	CT_POINBOUNDDELIVERYQTY,
	DIM_DATEIDPODELIVERY,
	CT_DELIVRECEIVEDQTY,
	DD_SHIPMENTSTATUS,
	CT_POITEMRECEIVEDQTY,
	CT_OPENQTY,
	CT_PRESHIPQTY,
	CT_INTRANSITQTY,
	CT_CLOSEDQTY,
	CT_CANCELLEDQTY,
	DIM_PURCHASEMISCID,
	DD_DELETIONINDICATOR,
	CT_SCHEDULELINEQTY,
	DIM_INBROUTEID,
	f.DW_INSERT_DATE,
	f.DW_UPDATE_DATE,
	DIM_PROJECTSOURCEID,
	CT_ITEMQTY,
	DIM_AFSTRANSPCONDITIONID,
	DIM_DATEIDACTUALPO,
	DIM_DATEIDAFSEXFACTEST,
	DIM_DATEIDMATAVAILABILITY,
	DD_DELIVERYNUMBER,
	DIM_DATEIDTRANSFER,
	CT_DELIVERYGROSSWGHT,
	CT_DELIVERYNETWGHT,
	DIM_DELIVERYROUTEID,
	DIM_SHIPPINGCONDITIONID,
	DIM_TRANPORTBYSHIPPINGTYPEID,
	DD_SHORTTEXT,
	DD_SEALNUMBER,
	DIM_TRANSPORTCONDITIONID,
	DIM_DATEIDINBOUNDDELIVERYDATE,
	DD_NOOFFOREIGNTRADE,
	DIM_MDG_PARTID,
	DD_SEQNOOFVENDORCONF,
	DIM_SHIPPINGINSTRUCTIONID,
	DD_CREATIONINDVENDORCONF,
	DD_CONFIRMATIONCATEG,
	DIM_DELIVERYDATEOFVENDORCONF,
	DIM_BWPRODUCTHIERARCHYID,
	DIM_DATEIDLASTPOSTINGDATE,
	DD_SHIP_TO_ORG_CODE,
	DIM_PODATEIDCREATE,
	DIM_DATEIDSALESORDERCREATED,
	DIM_DATEIDACTUALGIMERCKLS,
	DIM_DATEIDSALESORDERCREATED_INTERCO,
	DIM_DATEIDACTUALGIMERCKLS_INTERCO,
	DD_SALESDOCNO_INTERCO,
	DD_SALESITEMNO_INTERCO,
	DD_UPDATEDATE,
	DD_INTERCOMPANYFLAG,
	DIM_ACCOUNTCATEGORYID,
	DD_CONFIRM_CATEG_FLAG,
	DD_SHIPMENTSTATUS_FLAG,
	dd_plantemd,
	dd_routecode,
        dd_routename,
	dim_dateidsocreated_interco,
	dim_dateiddlvrdoccreated_interco,
	dim_mercklsconforreqdateid_interco
)



SELECT
	FACT_SHIPMENTINDELIVERYID,
	DD_SHIPMENTNO,
	DD_SHIPMENTITEMNO,
	DD_SALESDOCNO,
	DD_DOCUMENTNO,
	DD_DOCUMENTITEMNO,
	CT_INBOUNDDELIVERYQTY,
	CT_CONFIRMEDQTY,
	CT_RECEIVEDQTY,
	CT_PRICEUNIT,
	DIM_DATEID3PLYARD,
	DIM_DATEIDASNOUTPUT,
	DIM_DATEIDASNEXPECTEDOUTPUT,
	DIM_PURCHASEGROUPID,
	DIM_DATEIDSHIPMENTEND,
	DIM_PLANTID,
	DIM_COMPANYID,
	DIM_SALESDIVISIONID,
	DIM_DOCUMENTCATEGORYID,
	DIM_SALESDOCUMENTTYPEID,
	DIM_SALESORGID,
	DIM_CUSTOMERID,
	DIM_DATEIDVALIDFROM,
	DIM_DATEIDVALIDTO,
	DIM_SALESGROUPID,
	DIM_COSTCENTERID,
	DIM_CONTROLLINGAREAID,
	DIM_BILLINGBLOCKID,
	DIM_TRANSACTIONGROUPID,
	DIM_SALESORDERREJECTREASONID,
	DIM_PARTID,
	DIM_SALESORDERHEADERSTATUSID,
	DIM_SALESORDERITEMSTATUSID,
	DIM_CUSTOMERGROUP1ID,
	DIM_CUSTOMERGROUP2ID,
	DIM_SALESORDERITEMCATEGORYID,
	DIM_DATEIDFIRSTDATE,
	DIM_SCHEDULELINECATEGORYID,
	DIM_SALESMISCID,
	DD_ITEMRELFORDELV,
	DIM_DATEIDSHIPMENTDELIVERY,
	DIM_DATEIDACTUALGI,
	DIM_DATEIDSHIPDLVRFILL,
	DIM_PROFITCENTERID,
	DIM_CUSTOMERIDSHIPTO,
	DIM_CUSTOMPARTNERFUNCTIONID,
	DIM_CUSTOMPARTNERFUNCTIONID1,
	DIM_CUSTOMPARTNERFUNCTIONID2,
	DD_CUSTOMERPONO,
	DIM_DATEIDSOCANCELDATE,
	DD_BUSINESSCUSTOMERPONO,
	DIM_ROUTEID,
	DD_CARRIERACTUAL,
	DD_PACKAGECOUNT,
	DD_CARTONLEVELDTLFLAG,
	DIM_DATEIDCONTAINERATDCDOOR,
	DIM_DATEIDCONTAINERINYARD,
	DD_CONTAINERSIZE,
	DD_CURRENTSHIPMENTFLAG,
	DIM_DATEIDDEPARTUREFROMORIGIN,
	DIM_DATEIDETAFINALDEST,
	DIM_DATEIDGR,
	DD_ORIGINCONTAINERID,
	CT_INBOUNDDLVRYCMTRVOLUME,
	DD_BILLOFLADINGNO,
	DIM_DATEIDAIRORSEAPLANNINGESTETA,
	DD_INBOUNDDELIVERYNO,
	DD_INBOUNDDELIVERYITEMNO,
	DIM_VENDORID,
	DIM_MEANSOFTRANSTYPEID,
	DD_MESSAGETYPE,
	DD_MOVEMENTTYPE,
	DIM_DATEIDPOBUY,
	DIM_DATEIDPOETA,
	DIM_PODOCUMENTTYPEID,
	DIM_POROUTEID,
	CT_POSCHEDULEQTY,
	DD_SCHEDULENO,
	DIM_POAFSSEASONID,
	DIM_SHIPMENTTYPEID,
	CT_SHIPMENTQTY,
	DD_VESSEL,
	DD_VOYAGENO,
	DIM_DATEIDSHIPMENTCREATED,
	DD_DOMESTICCONTAINERID,
	DD_3PLFLAG,
	DIM_DATEIDREQDELIVERYDATE,
	DD_CUSTOMERPOSTATUS,
	DIM_CUSTOMVENDORPARTNERFUNCTIONID,
	DIM_CUSTOMVENDORPARTNERFUNCTIONID1,
	DD_SALESITEMNO,
	DD_SALESSCHEDULENO,
	DIM_PROCESSINGSTATUSID,
	DIM_TRANSPPLANNINGPOINTID,
	CT_DELAYDAYS,
	DD_PROCESSINGTIME,
	DIM_PRODUCTHIERARCHYID,
	DIM_CARRIERBILLINGID,
	AMT_SO_AFSNETVALUE,
	DIM_DATEIDDLVRDOCCREATED,
	CT_POINBOUNDDELIVERYQTY,
	DIM_DATEIDPODELIVERY,
	CT_DELIVRECEIVEDQTY,
	DD_SHIPMENTSTATUS,
	CT_POITEMRECEIVEDQTY,
	CT_OPENQTY,
	CT_PRESHIPQTY,
	CT_INTRANSITQTY,
	CT_CLOSEDQTY,
	CT_CANCELLEDQTY,
	DIM_PURCHASEMISCID,
	DD_DELETIONINDICATOR,
	CT_SCHEDULELINEQTY,
	DIM_INBROUTEID,
	f.DW_INSERT_DATE,
	f.DW_UPDATE_DATE,
	DIM_PROJECTSOURCEID,
	CT_ITEMQTY,
	DIM_AFSTRANSPCONDITIONID,
	DIM_DATEIDACTUALPO,
	DIM_DATEIDAFSEXFACTEST,
	DIM_DATEIDMATAVAILABILITY,
	DD_DELIVERYNUMBER,
	DIM_DATEIDTRANSFER,
	CT_DELIVERYGROSSWGHT,
	CT_DELIVERYNETWGHT,
	DIM_DELIVERYROUTEID,
	DIM_SHIPPINGCONDITIONID,
	DIM_TRANPORTBYSHIPPINGTYPEID,
	DD_SHORTTEXT,
	DD_SEALNUMBER,
	DIM_TRANSPORTCONDITIONID,
	DIM_DATEIDINBOUNDDELIVERYDATE,
	DD_NOOFFOREIGNTRADE,
	DIM_MDG_PARTID,
	DD_SEQNOOFVENDORCONF,
	DIM_SHIPPINGINSTRUCTIONID,
	DD_CREATIONINDVENDORCONF,
	DD_CONFIRMATIONCATEG,
	DIM_DELIVERYDATEOFVENDORCONF,
	f.DIM_BWPRODUCTHIERARCHYID,
	DIM_DATEIDLASTPOSTINGDATE,
	DD_SHIP_TO_ORG_CODE,
	DIM_PODATEIDCREATE,
	DIM_DATEIDSALESORDERCREATED,
	DIM_DATEIDACTUALGIMERCKLS,
	DIM_DATEIDSALESORDERCREATED_INTERCO,
	DIM_DATEIDACTUALGIMERCKLS_INTERCO,
	DD_SALESDOCNO_INTERCO,
	DD_SALESITEMNO_INTERCO,
	DD_UPDATEDATE,
	DD_INTERCOMPANYFLAG,
	DIM_ACCOUNTCATEGORYID,
	DD_CONFIRM_CATEG_FLAG,
	DD_SHIPMENTSTATUS_FLAG,
	ifnull(dd_plantemd,'Not Set'),
	ifnull(dd_routecode,'Not Set'),
        ifnull(dd_routename,'Not Set'),
	ifnull(dim_dateidsocreated_interco,1),
	ifnull(dim_dateiddlvrdoccreated_interco,1),
	ifnull(dim_mercklsconforreqdateid_interco,1)
FROM
	 fact_shipmentindelivery f,dim_bwproducthierarchy bw
where f.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02';

update fact_s_curve f
set f.dim_projectsourceid=15
from fact_s_curve f
where f.dim_projectsourceid<>15;




/*05072018 - Alina - SCurve New Requirements */

drop table if exists update_s_curve;
create table update_s_curve
as
select distinct salesorderitemCategory as ItemCategoryCode,dc.description ItemCategory
,dd_BusinessCustomerPONo,CustomerPOType,dcp.Description CustomerPoTypeDesc 
,sorr.Description as RejectReason,RejectReasonCode,dd_salesdocno,dd_salesitemno
,db.DeliveryBlock
,db.Description DelBlockDescription
,sois.deliverystatus DeliveryStatus
,dd_billing_no BillingNo
,DD_SALESDLVRDOCNO DeliveryDocNo
,dp.MTOMTS
,sum(ct_ScheduleQtySalesUnit) OrderQty
,sum(ct_ConfirmedQty) ConfirmedQty
,min(dd_SOLineCreateTime) dd_SOLineCreateTime

from emd586.fact_salesorder f,emd586.dim_salesorderitemcategory dc,emd586.dim_customerpurchaseordertype dcp
,emd586.dim_salesorderrejectreason sorr,emd586.dim_bwproducthierarchy bw
,emd586.dim_deliveryblock db
,emd586.dim_salesorderitemstatus sois
,emd586.dim_part dp
where f.dim_salesorderitemcategoryid=dc.dim_salesorderitemcategoryid
and f.dim_purchaseordertypeid=dcp.dim_customerpurchaseordertypeid
and f.dim_salesorderrejectreasonid=sorr.dim_salesorderrejectreasonid
and f.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and f.dim_deliveryblockid=db.dim_deliveryblockid
and f.dim_salesorderitemstatusid=sois.dim_salesorderitemstatusid
and f.dim_partid=dp.dim_partid
and businesssector='BS-02'
group by salesorderitemCategory,dc.description
,dd_BusinessCustomerPONo,CustomerPOType,dcp.Description
,sorr.Description,RejectReasonCode,dd_salesdocno,dd_salesitemno
,db.DeliveryBlock
,db.Description 
,sois.deliverystatus
,dd_billing_no 
,DD_SALESDLVRDOCNO
,dp.MTOMTS;


update fact_s_curve f
set f.dd_MTOMTS_interco=ff.MTOMTS
from fact_s_curve f,(select distinct min(MTOMTS) MTOMTS,dd_salesdocno,dd_salesitemno
from 
update_s_curve
where MTOMTS<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno;

update fact_s_curve f
set f.dd_SOLineCreateTime_interco=concat(substring(ff.dd_SOLineCreateTime,1,2),':',
substring(ff.dd_SOLineCreateTime,3,2),':',substring(ff.dd_SOLineCreateTime,5,2))
from fact_s_curve f,(select distinct min(dd_SOLineCreateTime) dd_SOLineCreateTime,dd_salesdocno,dd_salesitemno
from 
update_s_curve
where dd_SOLineCreateTime<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno;


update fact_s_curve f
set f.dd_ItemCategoryCode=ff.ItemCategoryCode
from fact_s_curve f,(select distinct min(ItemCategoryCode) ItemCategoryCode,dd_salesdocno,dd_salesitemno
from
update_s_curve
where ItemCategoryCode<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_ItemCategoryCode<>ff.ItemCategoryCode;



update fact_s_curve f
set f.dd_ItemCategory=ff.ItemCategory
from fact_s_curve f,
( select distinct min(ItemCategory) ItemCategory, dd_salesdocno,dd_salesitemno
from
update_s_curve
where ItemCategory<>'Not Set'
group by dd_salesdocno,dd_salesitemno ) ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_ItemCategory<>ff.ItemCategory;


update fact_s_curve f
set f.dd_CustomerPoTypeDesc=ff.CustomerPoTypeDesc
from fact_s_curve f,(
select min(CustomerPoTypeDesc)CustomerPoTypeDesc,dd_salesdocno,dd_salesitemno
from
update_s_curve
where CustomerPoTypeDesc<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_CustomerPoTypeDesc<>ff.CustomerPoTypeDesc;


update fact_s_curve f
set f.dd_RejectReason=ff.RejectReason
from fact_s_curve f,
(select min(RejectReason)RejectReason,dd_salesdocno,dd_salesitemno
from
update_s_curve
where RejectReason<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_RejectReason<>ff.RejectReason;

update fact_s_curve f
set f.dd_RejectReasonCode=ff.RejectReasonCode
from fact_s_curve f,
(select min(RejectReasonCode)RejectReasonCode,dd_salesdocno,dd_salesitemno
from
update_s_curve
where RejectReasonCode<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_RejectReasonCode<>ff.RejectReasonCode;


update fact_s_curve f
set f.dd_CustomerPOType=ff.CustomerPOType
from fact_s_curve f,
(select min(CustomerPOType)CustomerPOType,dd_salesdocno,dd_salesitemno
from
update_s_curve
where CustomerPOType<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_CustomerPOType<>ff.CustomerPOType;


update fact_s_curve f
set f.dd_BusinessCustomerPONo_interco=ff.dd_BusinessCustomerPONo
from fact_s_curve f,(select min(dd_BusinessCustomerPONo) dd_BusinessCustomerPONo,dd_salesdocno,dd_salesitemno
from update_s_curve
where dd_BusinessCustomerPONo<>'Not Set'
group by dd_salesdocno,dd_salesitemno) ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_BusinessCustomerPONo_interco<>ff.dd_BusinessCustomerPONo;

update fact_s_curve f
set f.ct_OrderQty=OrderQty
from fact_s_curve f,(select sum(OrderQty) OrderQty,dd_salesdocno,dd_salesitemno
from update_s_curve
group by dd_salesdocno,dd_salesitemno )ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.ct_OrderQty<>OrderQty;


update fact_s_curve f
set f.ct_ConfirmedQty_interco=ConfirmedQty
from fact_s_curve f,(select sum(ConfirmedQty) ConfirmedQty,dd_salesdocno,dd_salesitemno
from update_s_curve
group by dd_salesdocno,dd_salesitemno )ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.ct_ConfirmedQty_interco<>ConfirmedQty;



update fact_s_curve f
set f.dd_DeliveryBlock_interco=ff.DeliveryBlock
from fact_s_curve f,(select min(DeliveryBlock) DeliveryBlock,dd_salesdocno,dd_salesitemno
from update_s_curve
where DeliveryBlock<>'Not Set'
group by dd_salesdocno,dd_salesitemno) ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_DeliveryBlock_interco<>ff.DeliveryBlock;

update fact_s_curve f
set f.dd_DelBlockDescription_interco=ff.DelBlockDescription
from fact_s_curve f,(select min(DelBlockDescription) DelBlockDescription,dd_salesdocno,dd_salesitemno
from update_s_curve
where DelBlockDescription<>'Not Set'
group by dd_salesdocno,dd_salesitemno) ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_DelBlockDescription_interco<>ff.DelBlockDescription;

update fact_s_curve f
set f.dd_DeliveryStatus_interco=ff.deliverystatus
from fact_s_curve f,(select min(deliverystatus) deliverystatus,dd_salesdocno,dd_salesitemno
from update_s_curve
where deliverystatus<>'Not Set'
group by dd_salesdocno,dd_salesitemno) ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_DeliveryStatus_interco<>ff.deliverystatus;

update fact_s_curve f
set f.dd_BillingNo_interco =ff.BillingNo
from fact_s_curve f,(select min(BillingNo) BillingNo,dd_salesdocno,dd_salesitemno
from update_s_curve
where BillingNo<>'Not Set'
group by dd_salesdocno,dd_salesitemno) ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.dd_BillingNo_interco <>ff.BillingNo;


update fact_s_curve f
set f.DD_SALESDLVRDOCNO_interco =ff.DeliveryDocNo
from fact_s_curve f,(select min(DeliveryDocNo) DeliveryDocNo,dd_salesdocno,dd_salesitemno
from update_s_curve
where DeliveryDocNo<>'Not Set'
group by dd_salesdocno,dd_salesitemno) ff
where f.dd_salesdocno_interco=ff.dd_salesdocno
and f.dd_salesitemno_interco=ff.dd_salesitemno
and f.DD_SALESDLVRDOCNO_interco <>ff.DeliveryDocNo;



drop table if exists update_s_curve;
create table update_s_curve
as
select distinct dd_salesdocno,dd_salesitemno
,ocdt.datevalue SOCreateDate
,pgidsa.datevalue PlannedGoodsIssueDate
,bda.datevalue BillingActualDate
,minsdd.datevalue FirstDateDelivery
from emd586.fact_salesorder f,emd586.dim_date ocdt,emd586.dim_date pgidsa
,emd586.dim_date bda
,emd586.dim_date minsdd
,emd586.dim_bwproducthierarchy bw
where f.dim_dateidsocreated=ocdt.dim_dateid
and f.dim_dateidplannedgoodsissue=pgidsa.dim_dateid
and f.dim_dateidbillingactual=bda.dim_dateid
and f.dim_dateidscheddeliveryminsch=minsdd.dim_dateid
and f.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02';


update fact_s_curve f
set f.dim_SOCreateDateid=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(SOCreateDate) SOCreateDate,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve u
where SOCreateDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno_interco=m.dd_salesdocno
and f.dd_salesitemno_interco=m.dd_salesitemno
and d.datevalue=m.SOCreateDate
and f.dim_SOCreateDateid<>ifnull(d.dim_dateid, 1);

update fact_s_curve f
set f.dim_PlannedGoodsIssueDateid_interco=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(PlannedGoodsIssueDate) PlannedGoodsIssueDate,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve u
where PlannedGoodsIssueDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno_interco=m.dd_salesdocno
and f.dd_salesitemno_interco=m.dd_salesitemno
and d.datevalue=m.PlannedGoodsIssueDate
and f.dim_projectsourceid=d.projectsourceid
and f.dim_PlannedGoodsIssueDateid_interco<>ifnull(d.dim_dateid, 1);

update fact_s_curve f
set f.dim_BillingActualDateid_interco=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(BillingActualDate) BillingActualDate,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve u
where BillingActualDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno_interco=m.dd_salesdocno
and f.dd_salesitemno_interco=m.dd_salesitemno
and d.datevalue=m.BillingActualDate
and f.dim_projectsourceid=d.projectsourceid
and f.dim_BillingActualDateid_interco<>ifnull(d.dim_dateid, 1);


update fact_s_curve f
set f.dim_FirstDateDeliveryMinid_interco=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(FirstDateDelivery) FirstDateDelivery,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve u
where FirstDateDelivery<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno_interco=m.dd_salesdocno
and f.dd_salesitemno_interco=m.dd_salesitemno
and d.datevalue=m.FirstDateDelivery
and f.dim_projectsourceid=d.projectsourceid
and f.dim_FirstDateDeliveryMinid_interco<>ifnull(d.dim_dateid, 1);



update fact_s_curve f
set f.dim_FirstDateDeliveryMaxid_interco=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select max(FirstDateDelivery) FirstDateDelivery,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve u
where FirstDateDelivery<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno_interco=m.dd_salesdocno
and f.dd_salesitemno_interco=m.dd_salesitemno
and d.datevalue=m.FirstDateDelivery
and f.dim_projectsourceid=d.projectsourceid
and f.dim_FirstDateDeliveryMaxid_interco<>ifnull(d.dim_dateid, 1);


/* Alina - App-9979 - Jul 20180710 */

drop table if exists update_s_curve_ss;
create table update_s_curve_ss
as
select distinct dd_salesdocno,dd_salesitemno
,dd_BusinessCustomerPONo 
,CustomerPOType
,dcp.Description CustomerPoTypeDesc 
,sorr.Description as RejectReason
,RejectReasonCode
,dd_BatchNo
,sum(ct_ScheduleQtySalesUnit) OrderQty
,sum(ct_ConfirmedQty) ConfirmedQty
,bb.Description BillingBlock
,bb.BillingBlockCode
,sohs.overallcreditstatus
,sohs.overallcreditstatuscode
,db.DeliveryBlock
,db.Description DelBlockDescription
,dd_billing_no BillingDoc
,min(dd_SOLineCreateTime) dd_SOLineCreateTime

from emd586.fact_salesorder f
,emd586.dim_salesorderitemcategory dc
,emd586.dim_customerpurchaseordertype dcp
,emd586.dim_salesorderrejectreason sorr
,emd586.dim_bwproducthierarchy bw
,emd586.dim_billingblock bb
,emd586.dim_salesorderheaderstatus sohs
,emd586.dim_deliveryblock db
where f.dim_salesorderitemcategoryid=dc.dim_salesorderitemcategoryid
and f.dim_purchaseordertypeid=dcp.dim_customerpurchaseordertypeid
and f.dim_salesorderrejectreasonid=sorr.dim_salesorderrejectreasonid
and f.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='BS-02'
and f.dim_billingblockid=bb.dim_billingblockid
and f.dim_salesorderheaderstatusid=sohs.dim_salesorderheaderstatusid
and f.dim_deliveryblockid=db.dim_deliveryblockid
group by
dd_salesdocno
,dd_salesitemno
,dd_BusinessCustomerPONo 
,CustomerPOType
,dcp.Description 
,sorr.Description 
,RejectReasonCode
,dd_BatchNo
,bb.Description 
,bb.BillingBlockCode
,sohs.overallcreditstatus
,sohs.overallcreditstatuscode
,db.DeliveryBlock
,db.Description 
,dd_billing_no
;


update fact_s_curve f
set f.dd_SOLineCreateTime_ss=concat(substring(ff.dd_SOLineCreateTime,1,2),':',
substring(ff.dd_SOLineCreateTime,3,2),':',substring(ff.dd_SOLineCreateTime,5,2))
from fact_s_curve f,
(select min(dd_SOLineCreateTime)dd_SOLineCreateTime ,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where dd_SOLineCreateTime<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno);


update fact_s_curve f
set f.dd_BusinessCustomerPONo_SS=ff.dd_BusinessCustomerPONo 
from fact_s_curve f,
(select min(dd_BusinessCustomerPONo )dd_BusinessCustomerPONo ,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where dd_BusinessCustomerPONo <>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_BusinessCustomerPONo_SS<>ff.dd_BusinessCustomerPONo;



update fact_s_curve f
set f.dd_CustomerPOType_SS=ff.CustomerPOType
from fact_s_curve f,
(select min(CustomerPOType) CustomerPOType ,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where CustomerPOType<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_CustomerPOType_SS<>ff.CustomerPOType;

update fact_s_curve f
set f.dd_CustomerPoTypeDesc_SS=ff.CustomerPoTypeDesc
from fact_s_curve f,
(select min(CustomerPoTypeDesc) CustomerPoTypeDesc,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where CustomerPoTypeDesc <>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_CustomerPoTypeDesc_SS<>ff.CustomerPoTypeDesc;



update fact_s_curve f
set f.dd_RejectReason_SS=ff.RejectReason
from fact_s_curve f,
(select min(RejectReason) RejectReason,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where RejectReason <>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_RejectReason_SS<>ff.RejectReason;


update fact_s_curve f
set f.dd_RejectReasonCode_SS=ff.RejectReasonCode
from fact_s_curve f,
(select min(RejectReasonCode)RejectReasonCode,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where RejectReasonCode <>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_RejectReasonCode_SS<>ff.RejectReasonCode;


update fact_s_curve f
set f.dd_BatchNo_SS=ff.dd_BatchNo
from fact_s_curve f,
(select min(dd_BatchNo) dd_BatchNo,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where dd_BatchNo<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_BatchNo_SS<>ff.dd_BatchNo;



update fact_s_curve f
set f.dd_BillingBlock_SS=ff.BillingBlock
from fact_s_curve f,
(select min(BillingBlock) BillingBlock,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where BillingBlock<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_BillingBlock_SS<>ff.BillingBlock;



update fact_s_curve f
set f.dd_BillingBlockCode_SS=ff.BillingBlockCode
from fact_s_curve f,
(select min(BillingBlockCode) BillingBlockCode,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where BillingBlockCode<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_BillingBlockCode_SS<>ff.BillingBlockCode;


update fact_s_curve f
set f.dd_overallcreditstatus_SS=ff.overallcreditstatus
from fact_s_curve f,
(select min(overallcreditstatus) overallcreditstatus,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where overallcreditstatus<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_overallcreditstatus_SS<>ff.overallcreditstatus;


update fact_s_curve f
set f.dd_overallcreditstatuscode_SS=ff.overallcreditstatuscode
from fact_s_curve f,
(select min(overallcreditstatuscode) overallcreditstatuscode,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where overallcreditstatuscode<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_overallcreditstatuscode_SS<>ff.overallcreditstatuscode;


update fact_s_curve f
set f.dd_DeliveryBlock_SS=ff.DeliveryBlock
from fact_s_curve f,
(select min(DeliveryBlock) DeliveryBlock,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where DeliveryBlock<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_DeliveryBlock_SS<>ff.DeliveryBlock;

update fact_s_curve f
set f.dd_DelBlockDescription_SS=ff.DelBlockDescription
from fact_s_curve f,
(select min(DelBlockDescription)DelBlockDescription,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where DelBlockDescription<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_DelBlockDescription_SS<>ff.DelBlockDescription;


update fact_s_curve f
set f.dd_BillingDoc_SS=ff.BillingDoc
from fact_s_curve f,
(select min(BillingDoc)BillingDoc,dd_salesdocno,dd_salesitemno
from
update_s_curve_ss
where BillingDoc<>'Not Set'
group by dd_salesdocno,dd_salesitemno)ff
where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.dd_BillingDoc_SS<>ff.BillingDoc;


update fact_s_curve f
set f.ct_OrderQty_ss=OrderQty
from fact_s_curve f,(select sum(OrderQty) OrderQty,dd_salesdocno,dd_salesitemno
from update_s_curve_ss
group by dd_salesdocno,dd_salesitemno )ff
 where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.ct_OrderQty_ss<>OrderQty;


update fact_s_curve f
set f.ct_ConfirmedQty_ss=ConfirmedQty
from fact_s_curve f,(select sum(ConfirmedQty) ConfirmedQty,dd_salesdocno,dd_salesitemno
from  update_s_curve_ss
group by dd_salesdocno,dd_salesitemno )ff
 where f.DD_SALESDOCNO=ff.dd_salesdocno
and to_char(f.DD_SALESITEMNO)=to_char(ff.dd_salesitemno)
and f.ct_ConfirmedQty_ss<>ConfirmedQty;

drop table if exists update_s_curve_dates_ss;
create table update_s_curve_dates_ss
as
select distinct dd_salesdocno,dd_salesitemno
,ocdt.datevalue SOCreateDate
,soc.datevalue SOLineCreationDate
,bda.datevalue BillingActualDate
,sdrd.datevalue CustomerRequestedDate
,minsdd.datevalue FirstDateDelivery
,dfc.datevalue ConfirmedRequestedDate
from emd586.fact_salesorder f
,emd586.dim_date ocdt
,emd586.dim_date soc
,emd586.dim_date bda
,emd586.dim_date sdrd
,emd586.dim_date minsdd
,emd586.dim_bwproducthierarchy bw
,emd586.dim_date_factory_calendar dfc
where f.dim_dateidsocreated=ocdt.dim_dateid
and f.dim_dateidsalesordercreated=soc.dim_dateid
and f.dim_dateidbillingactual=bda.dim_dateid
and f.dim_dateidscheddeliveryreq=sdrd.dim_dateid
and f.dim_dateidscheddeliveryminsch=minsdd.dim_dateid
and f.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and f.dim_mercklsconforreqdateid=dfc.dim_dateid
and businesssector='BS-02';

update fact_s_curve f
set f.dim_confirmed_requesteddateid_ss=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(ConfirmedRequestedDate)ConfirmedRequestedDate,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve_dates_ss u
where ConfirmedRequestedDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno=m.dd_salesdocno
and to_char(f.dd_salesitemno)=to_char(m.dd_salesitemno)
and d.datevalue=m.ConfirmedRequestedDate
and f.dim_projectsourceid=d.projectsourceid
and f.dim_confirmed_requesteddateid_ss<>ifnull(d.dim_dateid, 1);

update fact_s_curve f
set f.dim_SOCreateDateid_SS=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(SOCreateDate)SOCreateDate,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve_dates_ss u
where SOCreateDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno=m.dd_salesdocno
and to_char(f.dd_salesitemno)=to_char(m.dd_salesitemno)
and d.datevalue=m.SOCreateDate
and f.dim_projectsourceid=d.projectsourceid
and f.dim_SOCreateDateid_SS<>ifnull(d.dim_dateid, 1);


update fact_s_curve f
set f.dim_SOLineCreationDateid_SS=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(SOLineCreationDate) SOLineCreationDate,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve_dates_ss u
where SOLineCreationDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno=m.dd_salesdocno
and to_char(f.dd_salesitemno)=to_char(m.dd_salesitemno)
and d.datevalue=m.SOLineCreationDate
and f.dim_projectsourceid=d.projectsourceid
and f.dim_SOLineCreationDateid_SS<>ifnull(d.dim_dateid, 1);



update fact_s_curve f
set f.dim_BillingActualDateid_SS =ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(BillingActualDate) BillingActualDate,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve_dates_ss u
where BillingActualDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno=m.dd_salesdocno
and to_char(f.dd_salesitemno)=to_char(m.dd_salesitemno)
and d.datevalue=m.BillingActualDate
and f.dim_projectsourceid=d.projectsourceid
and f.dim_BillingActualDateid_SS <>ifnull(d.dim_dateid, 1);



update fact_s_curve f
set f.dim_CustomerRequestedDate_SS=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(CustomerRequestedDate) CustomerRequestedDate,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve_dates_ss u
where CustomerRequestedDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno=m.dd_salesdocno
and to_char(f.dd_salesitemno)=to_char(m.dd_salesitemno)
and d.datevalue=m.CustomerRequestedDate
and f.dim_projectsourceid=d.projectsourceid
and f.dim_CustomerRequestedDate_SS<>ifnull(d.dim_dateid, 1);


update fact_s_curve f
set f.dim_FirstDateDeliveryid_min_SS=ifnull(d.dim_dateid,1)
from fact_s_curve f,dim_date d,
(select min(FirstDateDelivery) FirstDateDelivery,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve_dates_ss u
where CustomerRequestedDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno=m.dd_salesdocno
and to_char(f.dd_salesitemno)=to_char(m.dd_salesitemno)
and d.datevalue=m.FirstDateDelivery
and f.dim_projectsourceid=d.projectsourceid
and f.dim_FirstDateDeliveryid_min_SS<>ifnull(d.dim_dateid, 1);


update fact_s_curve f
set f.dim_FirstDateDeliveryid_max_SS=ifnull(d.dim_dateid,1)
from fact_s_curve f,emd586.dim_date d,
(select max(FirstDateDelivery) FirstDateDelivery,u.dd_salesdocno,u.dd_salesitemno
from update_s_curve_dates_ss u
where CustomerRequestedDate<>'0001-01-01'
group by u.dd_salesdocno,u.dd_salesitemno
) as m 
where 
 d.companycode='Not Set'
and f.dd_salesdocno=m.dd_salesdocno
and to_char(f.dd_salesitemno)=to_char(m.dd_salesitemno)
and d.datevalue=m.FirstDateDelivery
and f.dim_projectsourceid=d.projectsourceid
and f.dim_FirstDateDeliveryid_max_SS<>ifnull(d.dim_dateid, 1);



/* End - Alina - App-9979 - Jul 20180710 */

delete from emd586.fact_s_curve where dim_projectsourceid=15;

insert into emd586.fact_s_curve f
(

	FACT_SHIPMENTINDELIVERYID,
	DD_SHIPMENTNO,
	DD_SHIPMENTITEMNO,
	DD_SALESDOCNO,
	DD_DOCUMENTNO,
	DD_DOCUMENTITEMNO,
	CT_INBOUNDDELIVERYQTY,
	CT_CONFIRMEDQTY,
	CT_RECEIVEDQTY,
	CT_PRICEUNIT,
	DIM_DATEID3PLYARD,
	DIM_DATEIDASNOUTPUT,
	DIM_DATEIDASNEXPECTEDOUTPUT,
	DIM_PURCHASEGROUPID,
	DIM_DATEIDSHIPMENTEND,
	DIM_PLANTID,
	DIM_COMPANYID,
	DIM_SALESDIVISIONID,
	DIM_DOCUMENTCATEGORYID,
	DIM_SALESDOCUMENTTYPEID,
	DIM_SALESORGID,
	DIM_CUSTOMERID,
	DIM_DATEIDVALIDFROM,
	DIM_DATEIDVALIDTO,
	DIM_SALESGROUPID,
	DIM_COSTCENTERID,
	DIM_CONTROLLINGAREAID,
	DIM_BILLINGBLOCKID,
	DIM_TRANSACTIONGROUPID,
	DIM_SALESORDERREJECTREASONID,
	DIM_PARTID,
	DIM_SALESORDERHEADERSTATUSID,
	DIM_SALESORDERITEMSTATUSID,
	DIM_CUSTOMERGROUP1ID,
	DIM_CUSTOMERGROUP2ID,
	DIM_SALESORDERITEMCATEGORYID,
	DIM_DATEIDFIRSTDATE,
	DIM_SCHEDULELINECATEGORYID,
	DIM_SALESMISCID,
	DD_ITEMRELFORDELV,
	DIM_DATEIDSHIPMENTDELIVERY,
	DIM_DATEIDACTUALGI,
	DIM_DATEIDSHIPDLVRFILL,
	DIM_PROFITCENTERID,
	DIM_CUSTOMERIDSHIPTO,
	DIM_CUSTOMPARTNERFUNCTIONID,
	DIM_CUSTOMPARTNERFUNCTIONID1,
	DIM_CUSTOMPARTNERFUNCTIONID2,
	DD_CUSTOMERPONO,
	DIM_DATEIDSOCANCELDATE,
	DD_BUSINESSCUSTOMERPONO,
	DIM_ROUTEID,
	DD_CARRIERACTUAL,
	DD_PACKAGECOUNT,
	DD_CARTONLEVELDTLFLAG,
	DIM_DATEIDCONTAINERATDCDOOR,
	DIM_DATEIDCONTAINERINYARD,
	DD_CONTAINERSIZE,
	DD_CURRENTSHIPMENTFLAG,
	DIM_DATEIDDEPARTUREFROMORIGIN,
	DIM_DATEIDETAFINALDEST,
	DIM_DATEIDGR,
	DD_ORIGINCONTAINERID,
	CT_INBOUNDDLVRYCMTRVOLUME,
	DD_BILLOFLADINGNO,
	DIM_DATEIDAIRORSEAPLANNINGESTETA,
	DD_INBOUNDDELIVERYNO,
	DD_INBOUNDDELIVERYITEMNO,
	DIM_VENDORID,
	DIM_MEANSOFTRANSTYPEID,
	DD_MESSAGETYPE,
	DD_MOVEMENTTYPE,
	DIM_DATEIDPOBUY,
	DIM_DATEIDPOETA,
	DIM_PODOCUMENTTYPEID,
	DIM_POROUTEID,
	CT_POSCHEDULEQTY,
	DD_SCHEDULENO,
	DIM_POAFSSEASONID,
	DIM_SHIPMENTTYPEID,
	CT_SHIPMENTQTY,
	DD_VESSEL,
	DD_VOYAGENO,
	DIM_DATEIDSHIPMENTCREATED,
	DD_DOMESTICCONTAINERID,
	DD_3PLFLAG,
	DIM_DATEIDREQDELIVERYDATE,
	DD_CUSTOMERPOSTATUS,
	DIM_CUSTOMVENDORPARTNERFUNCTIONID,
	DIM_CUSTOMVENDORPARTNERFUNCTIONID1,
	DD_SALESITEMNO,
	DD_SALESSCHEDULENO,
	DIM_PROCESSINGSTATUSID,
	DIM_TRANSPPLANNINGPOINTID,
	CT_DELAYDAYS,
	DD_PROCESSINGTIME,
	DIM_PRODUCTHIERARCHYID,
	DIM_CARRIERBILLINGID,
	AMT_SO_AFSNETVALUE,
	DIM_DATEIDDLVRDOCCREATED,
	CT_POINBOUNDDELIVERYQTY,
	DIM_DATEIDPODELIVERY,
	CT_DELIVRECEIVEDQTY,
	DD_SHIPMENTSTATUS,
	CT_POITEMRECEIVEDQTY,
	CT_OPENQTY,
	CT_PRESHIPQTY,
	CT_INTRANSITQTY,
	CT_CLOSEDQTY,
	CT_CANCELLEDQTY,
	DIM_PURCHASEMISCID,
	DD_DELETIONINDICATOR,
	CT_SCHEDULELINEQTY,
	DIM_INBROUTEID,
	DW_INSERT_DATE,
	DW_UPDATE_DATE,
	DIM_PROJECTSOURCEID,
	CT_ITEMQTY,
	DIM_AFSTRANSPCONDITIONID,
	DIM_DATEIDACTUALPO,
	DIM_DATEIDAFSEXFACTEST,
	DIM_DATEIDMATAVAILABILITY,
	DD_DELIVERYNUMBER,
	DIM_DATEIDTRANSFER,
	CT_DELIVERYGROSSWGHT,
	CT_DELIVERYNETWGHT,
	DIM_DELIVERYROUTEID,
	DIM_SHIPPINGCONDITIONID,
	DIM_TRANPORTBYSHIPPINGTYPEID,
	DD_SHORTTEXT,
	DD_SEALNUMBER,
	DIM_TRANSPORTCONDITIONID,
	DIM_DATEIDINBOUNDDELIVERYDATE,
	DD_NOOFFOREIGNTRADE,
	DIM_MDG_PARTID,
	DD_SEQNOOFVENDORCONF,
	DIM_SHIPPINGINSTRUCTIONID,
	DD_CREATIONINDVENDORCONF,
	DD_CONFIRMATIONCATEG,
	DIM_DELIVERYDATEOFVENDORCONF,
	DIM_BWPRODUCTHIERARCHYID,
	DIM_DATEIDLASTPOSTINGDATE,
	DD_SHIP_TO_ORG_CODE,
	DIM_PODATEIDCREATE,
	DIM_DATEIDSALESORDERCREATED,
	DIM_DATEIDACTUALGIMERCKLS,
	DIM_DATEIDSALESORDERCREATED_INTERCO,
	DIM_DATEIDACTUALGIMERCKLS_INTERCO,
	DD_SALESDOCNO_INTERCO,
	DD_SALESITEMNO_INTERCO,
	DD_UPDATEDATE,
	DD_INTERCOMPANYFLAG,
	DIM_ACCOUNTCATEGORYID,
	DD_CONFIRM_CATEG_FLAG,
	DD_SHIPMENTSTATUS_FLAG,
	dd_plantemd,
	dd_routecode,
        dd_routename,
	dim_dateidsocreated_interco,
	dim_dateiddlvrdoccreated_interco,
	dim_mercklsconforreqdateid_interco,
	dim_confirmed_requesteddateid_ss,
	dim_FirstDateDeliveryid_max_SS,
	dim_FirstDateDeliveryid_min_SS,
	dim_CustomerRequestedDate_SS,
	dim_BillingActualDateid_SS,
	dim_SOLineCreationDateid_SS,
	dim_SOCreateDateid_SS,
	ct_ConfirmedQty_ss,
	ct_OrderQty_ss,
	dd_SOLineCreateTime_ss,
        dd_BusinessCustomerPONo_SS,
        dd_CustomerPOType_SS,
        dd_CustomerPoTypeDesc_SS,
        dd_RejectReason_SS,
        dd_RejectReasonCode_SS,
        dd_BatchNo_SS,
        dd_BillingBlock_SS,
        dd_BillingBlockCode_SS,
        dd_overallcreditstatus_SS,
        dd_overallcreditstatuscode_SS,
        dd_DeliveryBlock_SS,
        dd_DelBlockDescription_SS,
        dd_BillingDoc_SS,
        dd_SOLineCreateTime_interco,
        dd_ItemCategoryCode,
        dd_ItemCategory,
        dd_CustomerPoTypeDesc,
        dd_RejectReason,
        dd_RejectReasonCode,
        dd_CustomerPOType,
        dd_BusinessCustomerPONo_interco,
        ct_OrderQty,
        ct_ConfirmedQty_interco,
        dd_DeliveryBlock_interco,
        dd_DelBlockDescription_interco,
        dd_DeliveryStatus_interco,
       dd_BillingNo_interco,
       DD_SALESDLVRDOCNO_interco,
       dim_SOCreateDateid,
       dim_PlannedGoodsIssueDateid_interco,
      dim_BillingActualDateid_interco,
      dim_FirstDateDeliveryMinid_interco,
       dim_FirstDateDeliveryMaxid_interco,
       dd_MTOMTS_interco
	)

select
	FACT_SHIPMENTINDELIVERYID,
	DD_SHIPMENTNO,
	DD_SHIPMENTITEMNO,
	DD_SALESDOCNO,
	DD_DOCUMENTNO,
	DD_DOCUMENTITEMNO,
	CT_INBOUNDDELIVERYQTY,
	CT_CONFIRMEDQTY,
	CT_RECEIVEDQTY,
	CT_PRICEUNIT,
	DIM_DATEID3PLYARD,
	DIM_DATEIDASNOUTPUT,
	DIM_DATEIDASNEXPECTEDOUTPUT,
	DIM_PURCHASEGROUPID,
	DIM_DATEIDSHIPMENTEND,
	DIM_PLANTID,
	DIM_COMPANYID,
	DIM_SALESDIVISIONID,
	DIM_DOCUMENTCATEGORYID,
	DIM_SALESDOCUMENTTYPEID,
	DIM_SALESORGID,
	DIM_CUSTOMERID,
	DIM_DATEIDVALIDFROM,
	DIM_DATEIDVALIDTO,
	DIM_SALESGROUPID,
	DIM_COSTCENTERID,
	DIM_CONTROLLINGAREAID,
	DIM_BILLINGBLOCKID,
	DIM_TRANSACTIONGROUPID,
	DIM_SALESORDERREJECTREASONID,
	DIM_PARTID,
	DIM_SALESORDERHEADERSTATUSID,
	DIM_SALESORDERITEMSTATUSID,
	DIM_CUSTOMERGROUP1ID,
	DIM_CUSTOMERGROUP2ID,
	DIM_SALESORDERITEMCATEGORYID,
	DIM_DATEIDFIRSTDATE,
	DIM_SCHEDULELINECATEGORYID,
	DIM_SALESMISCID,
	DD_ITEMRELFORDELV,
	DIM_DATEIDSHIPMENTDELIVERY,
	DIM_DATEIDACTUALGI,
	DIM_DATEIDSHIPDLVRFILL,
	DIM_PROFITCENTERID,
	DIM_CUSTOMERIDSHIPTO,
	DIM_CUSTOMPARTNERFUNCTIONID,
	DIM_CUSTOMPARTNERFUNCTIONID1,
	DIM_CUSTOMPARTNERFUNCTIONID2,
	DD_CUSTOMERPONO,
	DIM_DATEIDSOCANCELDATE,
	DD_BUSINESSCUSTOMERPONO,
	DIM_ROUTEID,
	DD_CARRIERACTUAL,
	DD_PACKAGECOUNT,
	DD_CARTONLEVELDTLFLAG,
	DIM_DATEIDCONTAINERATDCDOOR,
	DIM_DATEIDCONTAINERINYARD,
	DD_CONTAINERSIZE,
	DD_CURRENTSHIPMENTFLAG,
	DIM_DATEIDDEPARTUREFROMORIGIN,
	DIM_DATEIDETAFINALDEST,
	DIM_DATEIDGR,
	DD_ORIGINCONTAINERID,
	CT_INBOUNDDLVRYCMTRVOLUME,
	DD_BILLOFLADINGNO,
	DIM_DATEIDAIRORSEAPLANNINGESTETA,
	DD_INBOUNDDELIVERYNO,
	DD_INBOUNDDELIVERYITEMNO,
	DIM_VENDORID,
	DIM_MEANSOFTRANSTYPEID,
	DD_MESSAGETYPE,
	DD_MOVEMENTTYPE,
	DIM_DATEIDPOBUY,
	DIM_DATEIDPOETA,
	DIM_PODOCUMENTTYPEID,
	DIM_POROUTEID,
	CT_POSCHEDULEQTY,
	DD_SCHEDULENO,
	DIM_POAFSSEASONID,
	DIM_SHIPMENTTYPEID,
	CT_SHIPMENTQTY,
	DD_VESSEL,
	DD_VOYAGENO,
	DIM_DATEIDSHIPMENTCREATED,
	DD_DOMESTICCONTAINERID,
	DD_3PLFLAG,
	DIM_DATEIDREQDELIVERYDATE,
	DD_CUSTOMERPOSTATUS,
	DIM_CUSTOMVENDORPARTNERFUNCTIONID,
	DIM_CUSTOMVENDORPARTNERFUNCTIONID1,
	DD_SALESITEMNO,
	DD_SALESSCHEDULENO,
	DIM_PROCESSINGSTATUSID,
	DIM_TRANSPPLANNINGPOINTID,
	CT_DELAYDAYS,
	DD_PROCESSINGTIME,
	DIM_PRODUCTHIERARCHYID,
	DIM_CARRIERBILLINGID,
	AMT_SO_AFSNETVALUE,
	DIM_DATEIDDLVRDOCCREATED,
	CT_POINBOUNDDELIVERYQTY,
	DIM_DATEIDPODELIVERY,
	CT_DELIVRECEIVEDQTY,
	DD_SHIPMENTSTATUS,
	CT_POITEMRECEIVEDQTY,
	CT_OPENQTY,
	CT_PRESHIPQTY,
	CT_INTRANSITQTY,
	CT_CLOSEDQTY,
	CT_CANCELLEDQTY,
	DIM_PURCHASEMISCID,
	DD_DELETIONINDICATOR,
	CT_SCHEDULELINEQTY,
	DIM_INBROUTEID,
	DW_INSERT_DATE,
	DW_UPDATE_DATE,
	DIM_PROJECTSOURCEID,
	CT_ITEMQTY,
	DIM_AFSTRANSPCONDITIONID,
	DIM_DATEIDACTUALPO,
	DIM_DATEIDAFSEXFACTEST,
	DIM_DATEIDMATAVAILABILITY,
	DD_DELIVERYNUMBER,
	DIM_DATEIDTRANSFER,
	CT_DELIVERYGROSSWGHT,
	CT_DELIVERYNETWGHT,
	DIM_DELIVERYROUTEID,
	DIM_SHIPPINGCONDITIONID,
	DIM_TRANPORTBYSHIPPINGTYPEID,
	DD_SHORTTEXT,
	DD_SEALNUMBER,
	DIM_TRANSPORTCONDITIONID,
	DIM_DATEIDINBOUNDDELIVERYDATE,
	DD_NOOFFOREIGNTRADE,
	DIM_MDG_PARTID,
	DD_SEQNOOFVENDORCONF,
	DIM_SHIPPINGINSTRUCTIONID,
	DD_CREATIONINDVENDORCONF,
	DD_CONFIRMATIONCATEG,
	DIM_DELIVERYDATEOFVENDORCONF,
	DIM_BWPRODUCTHIERARCHYID,
	DIM_DATEIDLASTPOSTINGDATE,
	DD_SHIP_TO_ORG_CODE,
	DIM_PODATEIDCREATE,
	DIM_DATEIDSALESORDERCREATED,
	DIM_DATEIDACTUALGIMERCKLS,
	DIM_DATEIDSALESORDERCREATED_INTERCO,
	DIM_DATEIDACTUALGIMERCKLS_INTERCO,
	DD_SALESDOCNO_INTERCO,
	DD_SALESITEMNO_INTERCO,
	DD_UPDATEDATE,
	DD_INTERCOMPANYFLAG,
	DIM_ACCOUNTCATEGORYID,
	DD_CONFIRM_CATEG_FLAG,
	DD_SHIPMENTSTATUS_FLAG,
	ifnull(dd_plantemd,'Not Set'),
	dd_routecode,
        dd_routename,
	dim_dateidsocreated_interco,
	dim_dateiddlvrdoccreated_interco,
	dim_mercklsconforreqdateid_interco,
	dim_confirmed_requesteddateid_ss,
	dim_FirstDateDeliveryid_max_SS,
	dim_FirstDateDeliveryid_min_SS,
	dim_CustomerRequestedDate_SS,
	dim_BillingActualDateid_SS,
	dim_SOLineCreationDateid_SS,
	dim_SOCreateDateid_SS,
	ct_ConfirmedQty_ss,
	ct_OrderQty_ss,
	dd_SOLineCreateTime_ss,
        dd_BusinessCustomerPONo_SS,
        dd_CustomerPOType_SS,
        dd_CustomerPoTypeDesc_SS,
        dd_RejectReason_SS,
        dd_RejectReasonCode_SS,
        dd_BatchNo_SS,
        dd_BillingBlock_SS,
        dd_BillingBlockCode_SS,
        dd_overallcreditstatus_SS,
        dd_overallcreditstatuscode_SS,
        dd_DeliveryBlock_SS,
        dd_DelBlockDescription_SS,
        dd_BillingDoc_SS,
        dd_SOLineCreateTime_interco,
        dd_ItemCategoryCode,
        dd_ItemCategory,
        dd_CustomerPoTypeDesc,
        dd_RejectReason,
        dd_RejectReasonCode,
        dd_CustomerPOType,
        dd_BusinessCustomerPONo_interco,
        ct_OrderQty,
        ct_ConfirmedQty_interco,
        dd_DeliveryBlock_interco,
        dd_DelBlockDescription_interco,
        dd_DeliveryStatus_interco,
       dd_BillingNo_interco,
       DD_SALESDLVRDOCNO_interco,
       dim_SOCreateDateid,
       dim_PlannedGoodsIssueDateid_interco,
      dim_BillingActualDateid_interco,
      dim_FirstDateDeliveryMinid_interco,
       dim_FirstDateDeliveryMaxid_interco,
       dd_MTOMTS_interco
FROM
	FACT_S_CURVE f where f.FACT_SHIPMENTINDELIVERYID not in
(select distinct FACT_SHIPMENTINDELIVERYID from emd586.FACT_S_CURVE) ;

