    /* Part 3 */

    delete from number_fountain where table_name = 'check_processing_duration';
    insert into number_fountain(table_name,max_id)
    select 'check_processing_duration',ifnull((select max(id)+1 from check_processing_duration),1);

    insert into check_processing_duration(id,fact,scrpt,startproc,endproc)
    select (select max_id from number_fountain where table_name = 'check_processing_duration'), 'fact_inventory', 'vw_bi_inventoryturn_processing.part3.sql', current_timestamp, null;

    DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold_GBL;
        CREATE TABLE tmp_ivturn_CostOfGoodsSold_GBL
        AS
        select a.MATNR ,a.WERKS, a.SPMON_YEAR,a.SPMON_MONTH,
        a.WGVBR WGVBR_gbl,
		a.rowid rid,
		a.S031_SPTAG, t.pCurrency, t.pGlobalCurrency
        FROM S031 a
		INNER JOIN tmp_inv_t_cursor t 
		ON (a.MATNR = t.pMaterialNo 
			and a.WERKS = t.pPlant 
			and  a.SPMON_YEAR = t.pFromYear 
			and a.SPMON_MONTH = t.pFromMonth );

	UPDATE tmp_ivturn_CostOfGoodsSold_GBL t0
	SET t0.WGVBR_gbl = t0.WGVBR_gbl * ifnull(z.exchangeRate,1)
	FROM tmp_ivturn_CostOfGoodsSold_GBL t0
			LEFT JOIN tmp_getExchangeRate1 z 
			ON (z.pFromCurrency =  t0.pCurrency 
			AND z.pToCurrency = t0.pGlobalCurrency
			AND z.pDate = t0.S031_SPTAG 
			AND z.fact_script_name = 'bi_inventoryturn_processing');

        DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold_GBL_2;
        CREATE TABLE tmp_ivturn_CostOfGoodsSold_GBL_2
        AS
        SELECT a.MATNR ,WERKS, SPMON_YEAR,SPMON_MONTH,sum(WGVBR_gbl) sum_WGVBR
        from tmp_ivturn_CostOfGoodsSold_GBL a
        group by a.MATNR ,WERKS, SPMON_YEAR,SPMON_MONTH;

        UPDATE tmp_inv_t_cursor
        set CostOfGoodsSold_GBL = 0;

        UPDATE tmp_inv_t_cursor
		SET CostOfGoodsSold_GBL = ifnull(sum_WGVBR,0)
        FROM tmp_inv_t_cursor,tmp_ivturn_CostOfGoodsSold_GBL_2 a     
        WHERE a.MATNR = pMaterialNo and a.WERKS = pPlant
        and A.SPMON_YEAR = pFromYear and a.SPMON_MONTH = pFromMonth;


        Update fact_inventory
        set amt_OpenStockValue = ifnull(OpeningStock,0)
        FROM fact_inventory,tmp_inv_t_cursor      
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_OpenStockValue <> OpeningStock ;

        Update fact_inventory
	set amt_CloseStockValue = ifnull(ClosingStock,0)
	FROM fact_inventory,tmp_inv_t_cursor  
	where Fact_Inventoryid = pFact_Inventoryid
	AND amt_CloseStockValue <> ClosingStock ;

        Update fact_inventory
	set amt_AvgInventoryValue = ifnull(AverageInv,0)
        FROM fact_inventory,tmp_inv_t_cursor
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_AvgInventoryValue <> AverageInv;

        Update fact_inventory
	set amt_COGS = ifnull(CostOfGoodsSold,0)
        FROM fact_inventory,tmp_inv_t_cursor
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_COGS <> CostOfGoodsSold;

        Update fact_inventory
        set amt_COGS_GBL = ifnull(CostOfGoodsSold_GBL,0)
        FROM fact_inventory,tmp_inv_t_cursor
        where Fact_Inventoryid = pFact_Inventoryid
        AND amt_COGS_GBL <> CostOfGoodsSold_GBL;

        Update fact_inventory
 	set ct_InventoryTurn = case ifnull(CostOfGoodsSold,0) when 0 then 0 else case ifnull(AverageInv,0) when 0 then 0 
				else (CostOfGoodsSold/AverageInv) end end
        FROM fact_inventory,tmp_inv_t_cursor
        where Fact_Inventoryid = pFact_Inventoryid;

  DELETE FROM fact_inventory_tmp;

  /* Drop all temporary tables created */

  DROP TABLE IF EXISTS tmp_ivturn_globalvars;
  DROP TABLE IF EXISTS tmp_inv_t_cursor;
        DROP TABLE IF EXISTS tmp_ivt_opcount0;
        DROP TABLE IF EXISTS tmp_ivt_opcount1;
        DROP TABLE IF EXISTS tmp_inventoryturn_mbewh_t1;
        DROP TABLE IF EXISTS tmp_inventoryturn_mbewh_t2;
        DROP TABLE IF EXISTS tmp_inventoryturn_mbew_t1;
        DROP TABLE IF EXISTS tmp_inventoryturn_mbew_t2;
        DROP TABLE IF EXISTS tmp_inv_t_cursor_innerwhile_OpCount_zero;
        DROP TABLE IF EXISTS min_pPer4_tmp;
 DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold;
    DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold_GBL;
        DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold_GBL_2;

        update check_processing_duration
        set endproc = current_timestamp
        where id = (select max_id from number_fountain where table_name = 'check_processing_duration');
