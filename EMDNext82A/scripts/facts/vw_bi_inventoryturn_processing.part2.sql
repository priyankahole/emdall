/* Start of part 2. This should run after the fiscal year scripts */
delete from number_fountain where table_name = 'check_processing_duration';
insert into number_fountain(table_name,max_id)
select 'check_processing_duration',ifnull((select max(id)+1 from check_processing_duration),1);

insert into check_processing_duration(id,fact,scrpt,startproc,endproc)
select (select max_id from number_fountain where table_name = 'check_processing_duration'), 'fact_inventory', 'vw_bi_inventoryturn_processing.part2.sql', current_timestamp, null;

UPDATE tmp_inv_t_cursor t
SET pPeriodDates = z.pReturn
FROM tmp_inv_t_cursor t,tmp_Funct_Fiscal_Year z
WHERE z.pCompanyCode = t.pCompCode
and z.FiscalYear = t.pFiYear
and z.Period = t.pPeriodFrom
and z.fact_script_name = 'bi_inventoryturn_processing';


UPDATE tmp_inv_t_cursor
SET pPeriodFromDate = to_date(substr(pPeriodDates, 1, 10),'YYYY-MM-DD')
where trim(pPeriodDates) <> '|';

UPDATE tmp_inv_t_cursor
SET pPeriodToDate = to_date(substr(pPeriodDates, 22, 32),'YYYY-MM-DD')
where trim(pPeriodDates) <> '|';

UPDATE tmp_inv_t_cursor
SET pPeriodMidDate = pPeriodToDate - (floor(((pPeriodToDate - pPeriodFromDate))/2));


  UPDATE tmp_inv_t_cursor
  set pFromYear = Year(pPeriodMidDate);

  UPDATE tmp_inv_t_cursor
  set pFromMonth = Month(pPeriodMidDate);

  UPDATE tmp_inv_t_cursor
  set pToYear = Year(pPeriodMidDate);

  UPDATE tmp_inv_t_cursor
  set pToMonth = Month(pPeriodMidDate);


  DROP TABLE IF EXISTS tmp_ivturn_CostOfGoodsSold;
  CREATE TABLE tmp_ivturn_CostOfGoodsSold
  AS
  SELECT MATNR,WERKS,SPMON_YEAR,SPMON_MONTH,sum(WGVBR) sum_WGVBR
  FROM S031
  GROUP BY MATNR,WERKS,SPMON_YEAR,SPMON_MONTH;

  UPDATE tmp_inv_t_cursor
  SET CostOfGoodsSold = 0;

    UPDATE tmp_inv_t_cursor
        set CostOfGoodsSold = sum_WGVBR
        FROM tmp_inv_t_cursor,tmp_ivturn_CostOfGoodsSold a   
        where a.MATNR = pMaterialNo and a.WERKS = pPlant
        and      a.SPMON_YEAR = pFromYear and a.SPMON_MONTH = pFromMonth;

/* Call exchange rate std function after the following combine */

update check_processing_duration
set endproc = current_timestamp
where id = (select max_id from number_fountain where table_name = 'check_processing_duration');
