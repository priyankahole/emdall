﻿/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_prodconfirmation_fact.sql */
/*   Author         : Lokesh */
/*   Created On     : 111 May 2015 */
/* */
/* */
/*   Description    : Script for Production Operation  Confirmation */
/* */
/*   Change History */
/*   Author         : BanciuB */
/*   Modified On     : 29 Dec 2015 */
/*   Modifications   : #1 - script edit for Exasol */
/* ####################################################################################################################   */

Drop table if exists pGlobalCurrency_po_77;

Create table pGlobalCurrency_po_77(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_77(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_77
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

Drop table if exists fact_prodop_confirmation_tmp;
Drop table if exists max_holder_prop;

Create table fact_prodop_confirmation_tmp as
SELECT dd_ordernumber,
dd_operationnumber, dd_RoutingOperationNo,dd_GeneralOrderCounter, 
dd_confirmation, dd_confirmationcounter
FROM fact_prodop_confirmation ;
-- select limit_0 from fact_prodop_confirmation 
Create table max_holder_prop
as
Select ifnull(max(fact_prodop_confirmationid),0) maxid
from fact_prodop_confirmation;

/* Temporarily do this as we have full data. Once measures etc are tested, remove this and add not exist in insert */
DELETE FROM fact_prodop_confirmation;


/* PHFLG should not be 'X'. If it is 'X', then sumnr should be blank */
DROP TABLE IF EXISTS tmp_afvc_afvv;
CREATE TABLE tmp_afvc_afvv
AS
SELECT *
FROM AFVC_AFVV
WHERE (ifnull(afvc_phflg,'yy')) <> 'X';

INSERT INTO tmp_afvc_afvv
SELECT *
FROM afvc_afvv
WHERE ifnull(AFVC_PHFLG,'yy') = 'X' AND (rtrim(afvc_sumnr) = '' or afvc_sumnr is null);

DROP TABLE IF EXISTS tmp_afru;
CREATE TABLE tmp_afru
AS
SELECT *
FROM AFRU
WHERE AFRU_SATZA in ( 'B10','B30','B40');

--#1 begin edit

DROP TABLE IF EXISTS tmp_FPC_1001L;
CREATE TABLE tmp_FPC_1001L AS
	SELECT --DISTINCT
	ifnull(AA.AFVC_MAT_PRKST, 0) amt_Meteriel_PrimaryCost,
	ifnull(AA.AFVC_PRKST, 0) amt_ActivityCostTtl,
	ifnull(AA.AFVC_PREIS, 0) amt_Price,
	ifnull(AA.AFVC_PEINH, 0) amt_PriceUnit,
	ifnull(AA.AFVV_DAUNO, 0) ct_ActivityDuration,
	ifnull(AA.AFVV_ISMNW, 0) ct_ActualWork,
	ifnull(AA.AFVV_ARBEI, 0) ct_ActivityWorkInvolved,
	CONVERT(BIGINT, 1)	dim_businessareaid,
	CONVERT(BIGINT, 1)	dim_companyid,
	CONVERT(BIGINT, 1)	dim_controllingareaid,	
	CONVERT(BIGINT, 1)	dim_materialgroupid,
	CONVERT(BIGINT, 1)	dim_currencyid,	
	CONVERT(BIGINT, 1)	dim_plantid,	
	CONVERT(BIGINT, 1)	dim_profitcenterid,	
	CONVERT(BIGINT, 1)	dim_purchasegroupid,	
	CONVERT(BIGINT, 1)	dim_purchaseorgid,	
	CONVERT(BIGINT, 1)	dim_tasklisttypeid,	
	CONVERT(BIGINT, 1)	dim_vendorid,	
	CONVERT(BIGINT, 1)	dim_costcenterid,	
	ifnull(AA.AFVC_EBELN, 'Not Set') dd_PODocumentNo,
	CONVERT(BIGINT, 1)	dim_functionalareaid,	
	CONVERT(BIGINT, 1)	dim_objecttypeid,	
	CONVERT(BIGINT, 1)	dim_unitofmeasureid,	
	ifnull(AA.AFVC_AUFPL, 0) dd_RoutingOperationNo,
	ifnull(AA.AFVC_APLZL, 0) dd_GeneralOrderCounter,
	ifnull(AA.AFVC_EBELP, 0) dd_POItemNo,
	CONVERT(BIGINT, 1)	Dim_DateIdActualStartExec,	
	CONVERT(BIGINT, 1)	Dim_DateIdActualFinishExec,	
	ifnull(AA.AFVV_ISDZ, 0) dd_hourActualStartExec,
	ifnull(AA.AFVV_IEDZ, 0) dd_hourActualFinishExe,
	CONVERT(BIGINT, 1)	Dim_DateIdSchedStartExec,	
	CONVERT(BIGINT, 1)	Dim_DateIdSchedFinishExec,	
	ifnull(AA.AFVV_FSAVZ, 0) dd_hourSchedStartExec,
	ifnull(AA.AFVV_FSEDZ, 0) dd_hourSchedFinishExec,
	ifnull(AA.AFVC_VPLFL, 'Not Set') dd_RoutingRefSequence,
	AA.AFVC_VORNR dd_OperationNumber,
	'Not Set'	dd_WorkCenter,
	'Not Set'	dd_bomexplosionno,
	CONVERT(BIGINT, 1)	dim_productionorderstatusid,
	CONVERT (bigint, 1) AS Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode = AFVC_WAERS),1) Dim_Currencyid_TRA,
	CONVERT (bigint, 1) AS dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode = 'USD'),1) dim_Currencyid_GBL,
	ifnull(AFVC_PHFLG, 'Not Set') dd_phaseindicator,
	ifnull(AFVC_SUMNR,0) dd_supopnode,
	ifnull(AFRU_RUECK,0) dd_confirmation,
	ifnull(AFRU_RMZHL,0) dd_confirmationcounter,
	ifnull(AFVV_MGVRG,0) ct_operationquantity,
	ifnull(AFVV_BMSCH,0) ct_basequantity,
	ifnull(AFVV_BEARZ,0) ct_processingtime,
	CONVERT(BIGINT, 1) dim_uomprocessingtime,
	0 ct_fixedruntime_theoretical,
	CONVERT(BIGINT, 1) dim_uomfixedruntime,
	0 ct_variableruntime_theoretical,
	CONVERT(BIGINT, 1) dim_uomvariableruntime_theoretical,
	ifnull(AFRU_SATZA,'Not Set') dd_confirmation_recordtype,
	ifnull(AFRU_ERNAM,'Not Set') dd_confirmationenteredby,
	ifnull(AFRU_AENAM,'Not Set') dd_lastchangeenteredby,
	ifnull(AFRU_ARBID,0) dd_confobjectid,
	CONVERT(BIGINT, 1) dim_plantidconfirmation,
	ifnull(AFRU_LTXA1,'Not Set') dd_confirmationtext,
	IFNULL(AFRU_TXTSP,'Not Set') dd_conf_languagekey,
	IFNULL(AFRU_ISERH,0) ct_confirmedbreaktime,
	IFNULL(AFRU_ISM01,0) dd_activitytobeconfirmed,
	IFNULL(AFRU_STOKZ,'Not Set') dd_reversed,
	IFNULL(AFRU_STZHL,0) dd_cancelconfirmation,
	IFNULL(AFRU_SMENG,0) ct_confoperationquantity,
	ifnull(AFRU_AUFNR,'Not Set') AFRU_AUFNR /*production order*/,
	ifnull(AFRU_AUFPL,0) AFRU_AUFPL /*operationtasklistno*/,
	ifnull(AFRU_APLZL,0) AFRU_APLZL /*counter*/,
	ifnull(AFVC_WAERS,0) AFVC_WAERS
FROM  pGlobalCurrency_po_77, AFVC_AFVV AA 
left outer join tmp_AFRU on AA.AFVC_RUECK = AFRU_RUECK ;

UPDATE tmp_FPC_1001L f
SET f.Dim_Currencyid_TRA = IFNULL(cur.Dim_Currencyid, 1)
FROM tmp_FPC_1001L f
LEFT JOIN Dim_Currency cur ON cur.CurrencyCode = f.AFVC_WAERS
AND f.Dim_Currencyid <> IFNULL(cur.Dim_Currencyid, 1);

UPDATE tmp_FPC_1001L f
SET f.dim_Currencyid_GBL = IFNULL(cur.Dim_Currencyid, 1)
FROM tmp_FPC_1001L f
CROSS JOIN (SELECT * FROM Dim_Currency WHERE CurrencyCode = 'USD') cur 
	AND f.dim_Currencyid_GBL <> IFNULL(cur.Dim_Currencyid, 1);
 
INSERT INTO fact_prodop_confirmation(
fact_prodop_confirmationid,fact_productionoperationid,
amt_Meteriel_PrimaryCost,
amt_ActivityCostTtl,
amt_Price,
amt_PriceUnit,
ct_ActivityDuration,
ct_ActualWork,
ct_ActivityWorkInvolved,
dim_businessareaid,
dim_companyid,
dim_controllingareaid,
dim_materialgroupid,
dim_currencyid,
dim_plantid,
dim_profitcenterid,
dim_purchasegroupid,
dim_purchaseorgid,
dim_tasklisttypeid,
dim_vendorid,
dim_costcenterid,
dd_PODocumentNo,
dim_functionalareaid,
dim_objecttypeid,
dim_unitofmeasureid,
dd_RoutingOperationNo,
dd_GeneralOrderCounter,
dd_POItemNo,
Dim_DateIdActualStartExec,
Dim_DateIdActualFinishExec,
dd_hourActualStartExec,
dd_hourActualFinishExe,
Dim_DateIdSchedStartExec,
Dim_DateIdSchedFinishExec,
dd_hourSchedStartExec,
dd_hourSchedFinishExec,
dd_RoutingRefSequence,
dd_OperationNumber,
dd_WorkCenter,
dd_bomexplosionno,
dim_productionorderstatusid,
dim_currencyid_TRA,
dim_currencyid_GBL,
dd_phaseindicator,
dd_supopnode,
dd_confirmation,
dd_confirmationcounter,
ct_operationquantity,
ct_basequantity,
ct_processingtime,
dim_uomprocessingtime,
ct_fixedruntime_theoretical,
dim_uomfixedruntime,
ct_variableruntime_theoretical,
dim_uomvariableruntime_theoretical,
dd_confirmation_recordtype,
dd_confirmationenteredby,
dd_lastchangeenteredby,
dd_confobjectid,
dim_plantidconfirmation,
dd_confirmationtext,
dd_conf_languagekey,
ct_confirmedbreaktime,
dd_activitytobeconfirmed,
dd_reversed,
dd_cancelconfirmation,
ct_confoperationquantity,
AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL) 
/*,
dd_confirmedstart_datetime,
dd_confirmedend_datetime,
dim_dateidconfirmationentry,
dim_dateidlastchange,
dim_dateidposting_confirmation,
dim_uombreaktime,
dd_activitytobeconfirmed,
dim_uom_activitytobeconfirmed,
dd_reversed,
dd_cancelconfirmation,
ct_confoperationquantity)*/
/*SELECT 1,1,*/

SELECT  m.maxid + row_number() over(order by''),1,
tp.*
From (	SELECT DISTINCT
amt_Meteriel_PrimaryCost,
amt_ActivityCostTtl,
amt_Price,
amt_PriceUnit,
ct_ActivityDuration,
ct_ActualWork,
ct_ActivityWorkInvolved,
dim_businessareaid,
dim_companyid,
dim_controllingareaid,
dim_materialgroupid,
dim_currencyid,
dim_plantid,
dim_profitcenterid,
dim_purchasegroupid,
dim_purchaseorgid,
dim_tasklisttypeid,
dim_vendorid,
dim_costcenterid,
dd_PODocumentNo,
dim_functionalareaid,
dim_objecttypeid,
dim_unitofmeasureid,
dd_RoutingOperationNo,
dd_GeneralOrderCounter,
dd_POItemNo,
Dim_DateIdActualStartExec,
Dim_DateIdActualFinishExec,
dd_hourActualStartExec,
dd_hourActualFinishExe,
Dim_DateIdSchedStartExec,
Dim_DateIdSchedFinishExec,
dd_hourSchedStartExec,
dd_hourSchedFinishExec,
dd_RoutingRefSequence,
dd_OperationNumber,
dd_WorkCenter,
dd_bomexplosionno,
dim_productionorderstatusid,
dim_currencyid_TRA,
dim_currencyid_GBL,
dd_phaseindicator,
dd_supopnode,
dd_confirmation,
dd_confirmationcounter,
ct_operationquantity,
ct_basequantity,
ct_processingtime,
dim_uomprocessingtime,
ct_fixedruntime_theoretical,
dim_uomfixedruntime,
ct_variableruntime_theoretical,
dim_uomvariableruntime_theoretical,
dd_confirmation_recordtype,
dd_confirmationenteredby,
dd_lastchangeenteredby,
dd_confobjectid,
dim_plantidconfirmation,
dd_confirmationtext,
dd_conf_languagekey,
ct_confirmedbreaktime,
dd_activitytobeconfirmed,
dd_reversed,
dd_cancelconfirmation,
ct_confoperationquantity,
AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL
FROM tmp_FPC_1001L
) tp, max_holder_prop m;

--#1 end modif 

UPDATE fact_prodop_confirmation f
SET f.dim_plantidconfirmation = pl.dim_plantid
FROM tmp_AFRU t, dim_plant pl, fact_prodop_confirmation f
WHERE f.dd_confirmation = t.AFRU_RUECK
AND f.dd_confirmationcounter = t.AFRU_RMZHL
AND t.AFRU_WERKS = pl.plantcode;

DROP TABLE IF EXISTS tmp_afru_calculated_1;
CREATE TABLE tmp_afru_calculated_1
AS
SELECT DISTINCT
AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL,AFRU_SATZA,AFRU_ISBD,AFRU_ISBZ,AFRU_IEBD,AFRU_IEBZ,
AFRU_RUECK,AFRU_RMZHL,AFRU_ARBID,afru_ltxa1, row_number() over( PARTITION BY AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL,AFRU_RUECK order by cast(AFRU_RMZHL as int)) sr_no
FROM tmp_AFRU
WHERE AFRU_SATZA in ( 'B10','B30','B40');

UPDATE tmp_afru_calculated_1 a
SET a.AFRU_IEBD = b.AFRU_IEBD
FROM tmp_afru_calculated_1 b, tmp_afru_calculated_1 a
WHERE a.AFRU_AUFNR = b.AFRU_AUFNR
AND a.AFRU_AUFPL = b.AFRU_AUFPL
AND a.AFRU_APLZL = B.AFRU_APLZL
AND a.AFRU_RUECK = b.AFRU_RUECK
AND a.AFRU_SATZA = 'B10'
AND a.sr_no = b.sr_no - 1;

UPDATE tmp_afru_calculated_1 a
SET a.AFRU_IEBZ = b.AFRU_IEBZ
FROM tmp_afru_calculated_1 b, tmp_afru_calculated_1 a
WHERE a.AFRU_AUFNR = b.AFRU_AUFNR
AND a.AFRU_AUFPL = b.AFRU_AUFPL
AND a.AFRU_APLZL = b.AFRU_APLZL
AND a.AFRU_RUECK = b.AFRU_RUECK
AND a.AFRU_SATZA = 'B10'
AND a.sr_no = b.sr_no - 1;

DELETE FROM tmp_afru_calculated_1
WHERE AFRU_SATZA NOT IN ('B10') ;

DROP TABLE IF EXISTS tmp_afru_calculated;
CREATE TABLE tmp_afru_calculated
AS
SELECT AFRU_AUFNR,AFRU_AUFPL,AFRU_APLZL,AFRU_SATZA,AFRU_ISBD,AFRU_ISBZ,AFRU_IEBD,AFRU_IEBZ,
AFRU_RUECK,AFRU_RMZHL,AFRU_ARBID,afru_ltxa1, (convert(varchar (10), AFRU_ISBD) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2))) date_hhmmss_beg,
CONVERT(varchar (10), AFRU_IEBD) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)) date_hhmmss_end,
cast(0 as int) ts_diff_secs
FROM tmp_afru_calculated_1;

UPDATE tmp_afru_calculated
SET AFRU_ISBD = AFRU_ISBD + 1
WHERE AFRU_ISBZ = 240000;

UPDATE tmp_afru_calculated
SET AFRU_ISBZ = '000000'
WHERE AFRU_ISBZ = 240000;

UPDATE tmp_afru_calculated
SET afrU_IEBD = AFRU_IEBD + 1
where AFRU_IEBZ = 240000;

UPDATE tmp_afru_calculated
set AFRU_IEBZ = '000000'
WHERE AFRU_IEBZ = '240000';

UPDATE tmp_afru_calculated
SET date_hhmmss_beg = (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)))
where date_hhmmss_beg <> (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)));

UPDATE tmp_afru_calculated
SET date_hhmmss_end = cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2))
WHERE date_hhmmss_end <> cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2));

UPDATE tmp_afru_calculated
SET ts_diff_secs = TRUNC(SECONDS_BETWEEN (date_hhmmss_end, date_hhmmss_beg)) --SET ts_diff_secs = TIMESTAMPDIFF(SECOND,date_hhmmss_beg,date_hhmmss_end) --#1
WHERE date_hhmmss_beg is not null AND date_hhmmss_end is not null;

/*UPDATE fact_prodop_confirmation f
FROM tmp_afru_calculated t
SET f.ct_duration_seconds = t.ts_diff_secs
WHERE f.dd_confirmation = t.AFRU_RUECK
AND f.dd_confirmationcounter = t.AFRU_RMZHL*/

UPDATE fact_prodop_confirmation f
SET f.ct_duration_seconds = t.ts_diff_secs
FROM tmp_afru_calculated t, fact_prodop_confirmation f
WHERE f.AFRU_AUFNR = t.AFRU_AUFNR
AND f.AFRU_AUFPL = t.AFRU_AUFPL
AND f.AFRU_APLZL = t.AFRU_APLZL
AND f.dd_confirmation = t.AFRU_RUECK
AND f.dd_confirmationcounter = t.AFRU_RMZHL;

UPDATE fact_prodop_confirmation f
SET f.ct_duration_seconds = f2.ct_duration_seconds
FROM fact_prodop_confirmation f2, fact_prodop_confirmation f
WHERE f.dd_confirmation = f2.dd_confirmation
AND f.dd_confirmation_recordtype <> 'B10'
AND f2.dd_confirmation_recordtype = 'B10';
		
Drop table if exists fact_prodop_confirmation_tmp;
Drop table if exists max_holder_prop;

DELETE FROM fact_prodop_confirmation
WHERE EXISTS
          (SELECT 1
             FROM AFVC_AFVV
            WHERE     AFVC_AUFPL = dd_RoutingOperationNo
                  AND AFVC_APLZL = dd_GeneralOrderCounter
				  AND AUFK_LOEKZ = 'X');                
				
UPDATE fact_prodop_confirmation pop
SET amt_Meteriel_PrimaryCost = ifnull(AFVC_MAT_PRKST ,0)
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_Meteriel_PrimaryCost <> ifnull(AA.AFVC_MAT_PRKST ,0);
				
UPDATE fact_prodop_confirmation pop
SET amt_ActivityCostTtl = ifnull (AFVC_PRKST ,0)
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_ActivityCostTtl <> ifnull (AFVC_PRKST ,0);
				
UPDATE fact_prodop_confirmation pop
SET amt_Price = ifnull(AFVC_PREIS ,0)
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_Price <> ifnull(AA.AFVC_PREIS ,0);
			
UPDATE fact_prodop_confirmation pop
SET amt_PriceUnit = ifnull(AFVC_PEINH ,0)
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_PriceUnit <> ifnull(AA.AFVC_PEINH ,0);
				
UPDATE fact_prodop_confirmation pop
SET ct_ActivityDuration = ifnull(AFVV_DAUNO ,0)
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActivityDuration <> ifnull(AA.AFVV_DAUNO ,0);
				
UPDATE fact_prodop_confirmation pop
SET ct_ActualWork = ifnull(AFVV_ISMNW ,0)
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActualWork <> ifnull(AA.AFVV_ISMNW ,0);	
				
				
UPDATE fact_prodop_confirmation pop
SET ct_ActivityWorkInvolved = ifnull(AFVV_ARBEI ,0)
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActivityWorkInvolved <> ifnull(AA.AFVV_ARBEI ,0);
				
UPDATE fact_prodop_confirmation pop
SET dim_businessareaid = bsar.dim_businessareaid
from AFVC_AFVV AA,dim_businessarea bsar, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_GSBER = bsar.Businessarea
		AND pop.dim_businessareaid <> bsar.dim_businessareaid;
									
UPDATE fact_prodop_confirmation pop
SET dim_companyid = comp.dim_companyid
from AFVC_AFVV AA,dim_company comp, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_BUKRS = comp.companycode
		AND pop.dim_companyid <> comp.dim_companyid;					
												
UPDATE fact_prodop_confirmation pop
SET dim_controllingareaid = ctrar.dim_controllingareaid
from AFVC_AFVV AA,dim_controllingarea ctrar, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_ANFKOKRS = ctrar.controllingareacode
		AND pop.dim_controllingareaid <> ctrar.dim_controllingareaid;	
						
UPDATE fact_prodop_confirmation pop
SET dim_materialgroupid = matgp.dim_materialgroupid
from AFVC_AFVV AA,dim_materialgroup matgp, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_MATKL = matgp.materialgroupcode
		AND pop.dim_materialgroupid <> matgp.dim_materialgroupid;

UPDATE fact_prodop_confirmation pop
SET dim_currencyid = cur.dim_currencyid
from AFVC_AFVV AA,dim_currency cur, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_WAERS = cur.currencycode
		AND pop.dim_currencyid <> cur.dim_currencyid;

UPDATE fact_prodop_confirmation pop
SET dim_plantid = pla.dim_plantid
from AFVC_AFVV AA,dim_plant pla, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_WERKS = pla.plantcode
		AND pop.dim_plantid <> pla.dim_plantid;
						
UPDATE fact_prodop_confirmation pop
SET dim_profitcenterid = proctr.dim_profitcenterid
from AFVC_AFVV AA,dim_profitcenter proctr, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_PRCTR = proctr.profitcentercode
		AND pop.dim_profitcenterid <> proctr.dim_profitcenterid;
						
UPDATE fact_prodop_confirmation pop
SET dim_purchasegroupid = porgp.dim_purchasegroupid
from AFVC_AFVV AA,dim_purchasegroup porgp, fact_prodop_confirmation pop		
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_EKGRP = porgp.purchasegroup
		AND pop.dim_purchasegroupid <> porgp.dim_purchasegroupid;
						
UPDATE fact_prodop_confirmation pop
SET dim_purchaseorgid = pororg.dim_purchaseorgid
from AFVC_AFVV AA,dim_purchaseorg pororg, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_EKORG = pororg.purchaseorgcode
		AND pop.dim_purchaseorgid <> pororg.dim_purchaseorgid;
						
UPDATE fact_prodop_confirmation pop
SET dim_tasklisttypeid = tsklt.dim_tasklisttypeid
from AFVC_AFVV AA,dim_tasklisttype tsklt, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_PLNTY = tsklt.tasklisttypecode
		AND pop.dim_tasklisttypeid <> tsklt.dim_tasklisttypeid;

UPDATE fact_prodop_confirmation pop
SET dim_vendorid = ven.dim_vendorid
from AFVC_AFVV AA,dim_vendor ven, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_LIFNR = ven.vendornumber
		AND pop.dim_vendorid <> ven.dim_vendorid;						
						
UPDATE fact_prodop_confirmation pop
SET dim_costcenterid = coctr.dim_costcenterid
from AFVC_AFVV AA,dim_costcenter coctr, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_ANFKO = coctr.code
		AND pop.dim_costcenterid <> coctr.dim_costcenterid;	
				
UPDATE fact_prodop_confirmation pop
SET dd_PODocumentNo = ifnull(AFVC_EBELN, 'Not Set')
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_PODocumentNo <> ifnull(AA.AFVC_EBELN, 'Not Set');

UPDATE fact_prodop_confirmation pop
SET dim_functionalareaid = fctar.dim_functionalareaid
from AFVC_AFVV AA,dim_functionalarea fctar, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_FUNC_AREA = fctar.functionalarea
		AND pop.dim_functionalareaid <> fctar.dim_functionalareaid;

UPDATE fact_prodop_confirmation pop
SET dim_objecttypeid = objt.dim_objecttypeid
from AFVC_AFVV AA,dim_objecttype objt, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_OTYPE = objt.objecttype
		AND pop.dim_objecttypeid <> objt.dim_objecttypeid;

UPDATE fact_prodop_confirmation pop
SET dim_unitofmeasureid = uniom.dim_unitofmeasureid
from AFVC_AFVV AA,dim_unitofmeasure uniom, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_MEINH = uniom.uom
		AND pop.dim_unitofmeasureid <> uniom.dim_unitofmeasureid;

UPDATE fact_prodop_confirmation pop
SET dd_POItemNo = ifnull(AFVC_EBELP, 0)
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_POItemNo <> ifnull(AA.AFVC_EBELP, 0);
		
UPDATE fact_prodop_confirmation pop
SET Dim_DateIdActualStartExec = asedt.Dim_DateId
from AFVC_AFVV AA,dim_date asedt, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_ISDD = asedt.datevalue 
		AND AA.AFVC_BUKRS = asedt.companycode
		AND pop.Dim_DateIdActualStartExec <> asedt.Dim_DateId;
						
UPDATE fact_prodop_confirmation pop
SET Dim_DateIdActualFinishExec = afedt.Dim_DateId
from AFVC_AFVV AA,dim_date afedt, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_IEDD = afedt.datevalue 
		AND AA.AFVC_BUKRS = afedt.companycode
		AND pop.Dim_DateIdActualFinishExec <> afedt.Dim_DateId;						
				
UPDATE fact_prodop_confirmation pop
SET dd_hourActualStartExec = ifnull(AFVV_ISDZ, 'Not Set')
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourActualStartExec <> ifnull(AA.AFVV_ISDZ, 'Not Set');						
				
UPDATE fact_prodop_confirmation pop
SET dd_hourActualFinishExe = ifnull(AFVV_IEDZ, 'Not Set')
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourActualFinishExe <> ifnull(AA.AFVV_IEDZ, 'Not Set');				

UPDATE fact_prodop_confirmation pop
SET Dim_DateIdSchedStartExec = ssedt.Dim_DateId
from AFVC_AFVV AA,dim_date ssedt, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_FSAVD = ssedt.datevalue 
		AND AA.AFVC_BUKRS = ssedt.companycode
		AND pop.Dim_DateIdSchedStartExec <> ssedt.Dim_DateId;	

UPDATE fact_prodop_confirmation pop
SET Dim_DateIdSchedFinishExec = sfedt.Dim_DateId
from AFVC_AFVV AA,dim_date sfedt, fact_prodop_confirmation pop	
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_FSEDD = sfedt.datevalue 
		AND AA.AFVC_BUKRS = sfedt.companycode
		AND pop.Dim_DateIdSchedFinishExec <> sfedt.Dim_DateId;	
				
UPDATE fact_prodop_confirmation pop
SET dd_hourSchedStartExec = ifnull(AFVV_FSAVZ, 'Not Set')
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourSchedStartExec <> ifnull(AA.AFVV_FSAVZ, 'Not Set');					
				
UPDATE fact_prodop_confirmation pop
SET dd_hourSchedFinishExec = ifnull(AFVV_FSEDZ, 'Not Set')
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourSchedFinishExec <> ifnull(AA.AFVV_FSEDZ, 'Not Set');				
				
UPDATE fact_prodop_confirmation pop
SET dd_RoutingRefSequence = ifnull(AFVC_VPLFL, 'Not Set')
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_RoutingRefSequence <> ifnull(AA.AFVC_VPLFL, 'Not Set');

UPDATE fact_prodop_confirmation pop
SET dd_Operationshorttext = ifnull(AFVC_LTXA1, 'Not Set')
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_Operationshorttext <> ifnull(AA.AFVC_LTXA1, 'Not Set');
		
UPDATE fact_prodop_confirmation pop
SET dd_descriptionline2 = ifnull(AFVC_LTXA2, 'Not Set')
from AFVC_AFVV AA, fact_prodop_confirmation pop
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_descriptionline2 <> ifnull(AA.AFVC_LTXA2, 'Not Set');		

UPDATE fact_prodop_confirmation pop
SET pop.dd_OrderNumber = po.dd_OrderNumber 
from fact_productionorder po, fact_prodop_confirmation pop 
	WHERE	pop.dd_RoutingOperationNo = po.dd_RoutingOperationNo;	
			
/*UPDATE fact_prodop_confirmation pop
from fact_productionorder po 
SET dd_OperationNumber = po.dd_OperationNumber,
dd_WorkCenter = po.dd_WorkCenter,
dd_bomexplosionno = po.dd_bomexplosionno,
dim_productionorderstatusid = po.dim_productionorderstatusid
	WHERE	pop.dd_RoutingOperationNo = po.dd_ordernumber*/											

UPDATE fact_prodop_confirmation pop
SET dd_WorkCenter = 'Not Set' 
	WHERE dd_WorkCenter is NULL;

UPDATE fact_prodop_confirmation pop
SET dd_bomexplosionno = 'Not Set'
	WHERE dd_bomexplosionno is NULL;

UPDATE fact_prodop_confirmation pop
SET dim_productionorderstatusid = 1
	WHERE dim_productionorderstatusid is NULL;
	
UPDATE fact_prodop_confirmation pop
SET amt_ExchangeRate = 1
FROM AFVC_AFVV AA,
     dim_company dc, fact_prodop_confirmation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND ifnull(amt_ExchangeRate,-1) <> 1; 

UPDATE fact_prodop_confirmation pop
SET amt_ExchangeRate = ex.exchangeRate 
FROM AFVC_AFVV AA,
     dim_company dc,
	 tmp_getExchangeRate1 ex, fact_prodop_confirmation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND  pFromCurrency = AA.AFVC_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = CURRENT_DATE -- ANSIDATE(LOCAL_TIMESTAMP) --#1
and fact_script_name = 'bi_populate_productionorder_fact' 
AND amt_ExchangeRate <> ex.exchangeRate ;


UPDATE fact_prodop_confirmation pop
SET amt_ExchangeRate_GBL = 1
FROM AFVC_AFVV AA,
     dim_company dc, fact_prodop_confirmation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;  

UPDATE fact_prodop_confirmation pop
SET amt_ExchangeRate_GBL = ex.exchangeRate 
FROM AFVC_AFVV AA,
     dim_company dc,
	 tmp_getExchangeRate1 ex,
	 pGlobalCurrency_po_77, fact_prodop_confirmation pop  --pGlobalCurrency_po_77 used??--BB
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND  pFromCurrency = AA.AFVC_WAERS
and pToCurrency = pGlobalCurrency
and pFromExchangeRate = 0
and pDate =  CURRENT_DATE --ANSIDATE(LOCAL_TIMESTAMP)   /*current date to be used for global rate*/
and fact_script_name = 'bi_populate_productionorder_fact'
AND amt_ExchangeRate <> ex.exchangeRate ;	


UPDATE fact_prodop_confirmation f
SET dd_objectid = a.afvc_arbid
FROM afvc_afvv a, fact_prodop_confirmation f
WHERE f.dd_RoutingOperationNo = a.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = a.AFVC_APLZL;


UPDATE fact_prodop_confirmation f
SET f.dim_workcenterid = wc.dim_workcenterid
FROM dim_workcenter wc, fact_prodop_confirmation f
WHERE wc.objectid = f.dd_objectid;

UPDATE fact_prodop_confirmation f
SET f.dim_costcenterid = dc.dim_costcenterid
FROM dim_costcenter dc, CRCO c, fact_prodop_confirmation f
WHERE f.dd_objectid = c.crco_objid
AND c.crco_KOSTL = dc.code
AND f.dim_costcenterid <> dc.dim_costcenterid;

/* Update fields from AFVU */
UPDATE fact_prodop_confirmation f
SET f.ct_fixedruntime_theoretical = ifnull(AA.AFVU_USR04,0)
from AFVU AA, fact_prodop_confirmation f
WHERE   f.dd_RoutingOperationNo = AA.AFVU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVU_APLZL
AND f.ct_fixedruntime_theoretical <> ifnull(AA.AFVU_USR04,0);

UPDATE fact_prodop_confirmation f
SET f.ct_variableruntime_theoretical = ifnull(AA.AFVU_USR05,0)
from AFVU AA, fact_prodop_confirmation f
WHERE   f.dd_RoutingOperationNo = AA.AFVU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVU_APLZL
AND f.ct_variableruntime_theoretical <> ifnull(AA.AFVU_USR05,0);

/* Update dates other missing fields */
UPDATE fact_prodop_confirmation f
SET f.dim_dateidconfirmationentry = dd.dim_dateid
FROM AFRU , dim_date dd, dim_company dc, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_ERSDA = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidconfirmationentry <> dd.dim_dateid;

UPDATE fact_prodop_confirmation f
SET f.dim_dateidlastchange = dd.dim_dateid
FROM AFRU , dim_date dd, dim_company dc, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_LAEDA = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidlastchange <> dd.dim_dateid;

UPDATE fact_prodop_confirmation f
SET f.dim_dateidposting_confirmation = dd.dim_dateid
FROM AFRU , dim_date dd, dim_company dc, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_BUDAT = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidposting_confirmation <> dd.dim_dateid;


UPDATE fact_prodop_confirmation f
SET f.dim_dateidconfirmedstart = dd.dim_dateid
FROM AFRU , dim_date dd, dim_company dc, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_ISBD = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidconfirmedstart <> dd.dim_dateid;

UPDATE fact_prodop_confirmation f
SET f.dim_dateidconfirmedfinish = dd.dim_dateid
FROM AFRU , dim_date dd, dim_company dc, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND f.dim_companyid = dc.dim_companyid
AND AFRU_IEBD = dd.datevalue AND dc.companycode = dd.companycode
AND f.dim_dateidconfirmedfinish <> dd.dim_dateid;

UPDATE fact_prodop_confirmation f
SET f.dd_confirmedstart_datetime = (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)))
FROM AFRU, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND AFRU_ISBD IS NOT NULL AND AFRU_ISBZ <> 240000
AND f.dd_confirmedstart_datetime <> (cast(AFRU_ISBD as varchar(10)) || ' ' || (substring(AFRU_ISBZ,1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)));


UPDATE fact_prodop_confirmation f
SET f.dd_confirmedstart_datetime = (cast((AFRU_ISBD + INTERVAL '1' DAY) as varchar(10)) || ' ' || (substring('00',1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)))
FROM AFRU, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND AFRU_ISBD IS NOT NULL AND AFRU_ISBZ = 240000
AND f.dd_confirmedstart_datetime <> (cast((AFRU_ISBD + INTERVAL '1' DAY) as varchar(10)) || ' ' || (substring('00',1,2) || ':' || substring(AFRU_ISBZ,3,2) || ':' || substring(AFRU_ISBZ,5,2)));

UPDATE fact_prodop_confirmation f
SET f.dd_confirmedend_datetime = (cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)))
FROM AFRU, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND AFRU_IEBD IS NOT NULL AND AFRU_IEBZ <> 240000
AND f.dd_confirmedend_datetime <> (cast(AFRU_IEBD as varchar(10)) || ' ' || (substring(AFRU_IEBZ,1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)));

UPDATE fact_prodop_confirmation f
SET f.dd_confirmedend_datetime = (cast((AFRU_IEBD + INTERVAL '1' DAY) as varchar(10)) || ' ' || (substring('00',1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)))
FROM AFRU, fact_prodop_confirmation f
WHERE f.dd_confirmation = afru_rueck
AND f.dd_confirmationcounter = afru_rmzhl
AND AFRU_IEBD IS NOT NULL AND AFRU_IEBZ = 240000
AND f.dd_confirmedend_datetime <> (cast((AFRU_IEBD + INTERVAL '1' DAY) as varchar(10)) || ' ' || (substring('00',1,2) || ':' || substring(AFRU_IEBZ,3,2) || ':' || substring(AFRU_IEBZ,5,2)));

/* Update all UOMs */

UPDATE fact_prodop_confirmation f
SET f.dim_uomprocessingtime = uniom.dim_unitofmeasureid
from AFVC_AFVV AA,dim_unitofmeasure uniom, fact_prodop_confirmation f
WHERE   f.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND AA.AFVV_BEAZE = uniom.uom
AND f.dim_uomprocessingtime <> uniom.dim_unitofmeasureid;


UPDATE fact_prodop_confirmation f
SET f.dim_uomfixedruntime = uniom.dim_unitofmeasureid
from AFVU AA,dim_unitofmeasure uniom, fact_prodop_confirmation f
WHERE   f.dd_RoutingOperationNo = AA.AFVU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVU_APLZL
AND AA.AFVU_USE04 = uniom.uom
AND f.dim_uomfixedruntime <> uniom.dim_unitofmeasureid;

UPDATE fact_prodop_confirmation f
SET f.dim_uomvariableruntime_theoretical = uniom.dim_unitofmeasureid
from AFVU AA,dim_unitofmeasure uniom, fact_prodop_confirmation f
WHERE   f.dd_RoutingOperationNo = AA.AFVU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVU_APLZL
AND AA.AFVU_USE05 = uniom.uom
AND f.dim_uomvariableruntime_theoretical <> uniom.dim_unitofmeasureid;

UPDATE fact_prodop_confirmation f
SET f.dim_uombreaktime = uniom.dim_unitofmeasureid
from AFRU AA,dim_unitofmeasure uniom, fact_prodop_confirmation f
WHERE   f.dd_RoutingOperationNo = AA.AFRU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFRU_APLZL
AND f.dd_confirmation = AA.AFRU_RUECK
AND f.dd_confirmationcounter = AA.AFRU_RMZHL
AND AA.AFRU_ZEIER = uniom.uom
AND f.dim_uombreaktime <> uniom.dim_unitofmeasureid;

UPDATE fact_prodop_confirmation f
SET f.dim_uom_activitytobeconfirmed = uniom.dim_unitofmeasureid
from AFRU AA,dim_unitofmeasure uniom, fact_prodop_confirmation f
WHERE   f.dd_RoutingOperationNo = AA.AFRU_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFRU_APLZL
AND f.dd_confirmation = AA.AFRU_RUECK
AND f.dd_confirmationcounter = AA.AFRU_RMZHL
AND AA.AFRU_ILE01 = uniom.uom
AND f.dim_uom_activitytobeconfirmed <> uniom.dim_unitofmeasureid;


UPDATE fact_prodop_confirmation pop
SET dd_OperationNumber = po.dd_OperationNumber,
dd_WorkCenter = po.dd_WorkCenter,
dd_bomexplosionno = po.dd_bomexplosionno,
dim_productionorderstatusid = po.dim_productionorderstatusid
from fact_productionorder po, fact_prodop_confirmation pop
        WHERE   pop.dd_RoutingOperationNo = po.dd_RoutingOperationNo;


		
		/* Update Production Order Columns */
UPDATE fact_prodop_confirmation pop
SET dim_dateidactualitemfinish_prodorder = po.dim_dateidactualitemfinish
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidactualitemfinish_prodorder <> po.dim_dateidactualitemfinish;

UPDATE fact_prodop_confirmation pop
SET dim_dateidactualheaderfinishdate_merck_prodorder = po.dim_dateidactualheaderfinishdate_merck
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidactualheaderfinishdate_merck_prodorder <> po.dim_dateidactualheaderfinishdate_merck;

UPDATE fact_prodop_confirmation pop
SET dim_dateidactualrelease_prodorder = po.dim_dateidactualrelease
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidactualrelease_prodorder <> po.dim_dateidactualrelease;

UPDATE fact_prodop_confirmation pop
SET dim_dateidactualstart_prodorder = po.dim_dateidactualstart
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidactualstart_prodorder <> po.dim_dateidactualstart;

UPDATE fact_prodop_confirmation pop
SET dim_dateidbasicfinish_prodorder = po.dim_dateidbasicfinish
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidbasicfinish_prodorder <> po.dim_dateidbasicfinish;

UPDATE fact_prodop_confirmation pop
SET dim_dateidbasicstart_prodorder = po.dim_dateidbasicstart
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidbasicstart_prodorder <> po.dim_dateidbasicstart;

UPDATE fact_prodop_confirmation pop
SET dim_dateidconfirmedorderfinish_prodorder = po.dim_dateidconfirmedorderfinish
from fact_productionorder po, fact_prodop_confirmation pop
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidconfirmedorderfinish_prodorder <> po.dim_dateidconfirmedorderfinish;

UPDATE fact_prodop_confirmation pop
SET dim_dateidlastscheduling_prodorder = po.dim_dateidlastscheduling
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidlastscheduling_prodorder <> po.dim_dateidlastscheduling;

UPDATE fact_prodop_confirmation pop
SET dim_partidheader_prodorder = po.dim_partidheader
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_partidheader_prodorder <> po.dim_partidheader;

UPDATE fact_prodop_confirmation pop
SET dim_partiditem_prodorder = po.dim_partiditem
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_partiditem_prodorder <> po.dim_partiditem;

UPDATE fact_prodop_confirmation pop
SET dim_mrpcontrollerid_prodorder = po.dim_mrpcontrollerid
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_mrpcontrollerid_prodorder <> po.dim_mrpcontrollerid;

UPDATE fact_prodop_confirmation pop
SET dim_ordertypeid_prodorder = po.dim_ordertypeid
from fact_productionorder po, fact_prodop_confirmation pop
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_ordertypeid_prodorder <> po.dim_ordertypeid;

UPDATE fact_prodop_confirmation pop
SET dim_dateidplannedorderdelivery_prodorder = po.dim_dateidplannedorderdelivery
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidplannedorderdelivery_prodorder <> po.dim_dateidplannedorderdelivery;

UPDATE fact_prodop_confirmation pop
SET dim_productionschedulerid_prodorder = po.dim_productionschedulerid
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_productionschedulerid_prodorder <> po.dim_productionschedulerid;

UPDATE fact_prodop_confirmation pop
SET dim_dateidrelease_prodorder = po.dim_dateidrelease
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidrelease_prodorder <> po.dim_dateidrelease;

UPDATE fact_prodop_confirmation pop
SET dim_dateidroutingtransfer_prodorder = po.dim_dateidroutingtransfer
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidroutingtransfer_prodorder <> po.dim_dateidroutingtransfer;

UPDATE fact_prodop_confirmation pop
SET dim_dateidscheduledfinishheader_prodorder = po.dim_dateidscheduledfinishheader
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidscheduledfinishheader_prodorder <> po.dim_dateidscheduledfinishheader;

UPDATE fact_prodop_confirmation pop
SET dim_dateidscheduledfinish_prodorder = po.dim_dateidscheduledfinish
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidscheduledfinish_prodorder <> po.dim_dateidscheduledfinish;

UPDATE fact_prodop_confirmation pop
SET dim_dateidscheduledrelease_prodorder = po.dim_dateidscheduledrelease
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidscheduledrelease_prodorder <> po.dim_dateidscheduledrelease;

UPDATE fact_prodop_confirmation pop
SET dim_dateidscheduledstart_prodorder = po.dim_dateidscheduledstart
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidscheduledstart_prodorder <> po.dim_dateidscheduledstart;

UPDATE fact_prodop_confirmation pop
SET dim_dateidtechnicalcompletion_prodorder = po.dim_dateidtechnicalcompletion
from fact_productionorder po, fact_prodop_confirmation pop 
WHERE	pop.dd_ordernumber = po.dd_ordernumber
AND dim_dateidtechnicalcompletion_prodorder <> po.dim_dateidtechnicalcompletion;

/* For some reason operation number is not getting set, so we are running the update statement here */
update fact_prodop_confirmation f
set f.dd_OperationNumber = AA.AFVC_VORNR
FROM  AFVC_AFVV AA, fact_prodop_confirmation f
WHERE f.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND f.dd_OperationNumber <> AA.AFVC_VORNR;

DROP TABLE IF EXISTS tmp_FPC_1001L;

