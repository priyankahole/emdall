DROP TABLE IF EXISTS tmp_prev_po_price_0;
CREATE TABLE tmp_prev_po_price_0
AS
SELECT DISTINCT f.Dim_Partid,
			   dt.DateValue,
			   f.dd_DocumentNo,
			   f.dd_DocumentItemNo,
			   f.amt_UnitPrice
FROM fact_purchase f
	  INNER JOIN dim_date dt
		 ON dt.Dim_Dateid = f.Dim_DateidCreate
	  INNER JOIN Dim_PurchaseMisc pmisc
		 ON f.Dim_PurchaseMiscid = pmisc.Dim_PurchaseMiscid
WHERE     f.amt_UnitPrice > 0
	  AND pmisc.ItemReturn = 'Not Set'
	  AND f.dim_partid <> 1;

DROP TABLE IF EXISTS tmp_prev_po_price_1;
CREATE TABLE tmp_prev_po_price_1
AS
SELECT f.*, row_number() over (order by DateValue desc,  dd_DocumentNo desc, dd_DocumentItemNo desc  )  ident
FROM   tmp_prev_po_price_0 f;

/* update the ident value for the previous latest date,dd_DocumentNo,dd_DocumentItemNo */
DROP TABLE IF EXISTS tmp_prev_po_price_2;
CREATE TABLE tmp_prev_po_price_2
AS
SELECT t1.Dim_Partid,
	  t1.DateValue,
	  t1.dd_DocumentNo,
	  t1.dd_DocumentItemNo,
	  t1.amt_UnitPrice,
	  t1.ident,
	  min(t2.ident) ident_prev_max
FROM tmp_prev_po_price_1 t1,tmp_prev_po_price_1 t2
WHERE     t1.Dim_Partid = t2.Dim_Partid
	  AND t2.ident > t1.ident
      AND t2.DateValue < t1.DateValue
GROUP BY t1.Dim_Partid,t1.DateValue,t1.dd_DocumentNo,t1.dd_DocumentItemNo,t1.amt_UnitPrice,t1.ident;

/* Delete all rows where prev rows have exactly the same part,date,docno and docitemno
   Some rows may have the same date but different ident values. Delete such rows */
DROP TABLE IF EXISTS tmp_del_tmp_prev_po_price;
CREATE TABLE tmp_del_tmp_prev_po_price
AS
SELECT x.*
FROM tmp_prev_po_price_2 x, tmp_prev_po_price_1 y
WHERE     x.ident_prev_max = y.ident
	  AND x.dim_partid = y.dim_partid
	  AND x.DateValue = y.DateValue;

/* Now alter tmp_prev_po_price_1 to have v_LastUnitPrice and update it using the prev table. 
   Where row is not found in _2, update to 0 */

ALTER TABLE  tmp_prev_po_price_1 ADD COLUMN v_LastUnitPrice decimal(18,5);

UPDATE tmp_prev_po_price_1 x
SET x.v_LastUnitPrice = z.amt_UnitPrice
FROM  tmp_prev_po_price_2 y, tmp_prev_po_price_1 z, tmp_prev_po_price_1 x
WHERE     x.ident = y.ident
      AND y.ident_prev_max = z.ident;

UPDATE tmp_prev_po_price_1
SET v_LastUnitPrice = 0
WHERE v_LastUnitPrice IS NULL;

UPDATE fact_purchase fp
SET amt_LastPOUnitPrice = x.v_LastUnitPrice
FROM tmp_prev_po_price_1 x, fact_purchase fp
WHERE     fp.dd_DocumentNo = x.dd_DocumentNo 
	  AND fp.dd_DocumentItemNo = x.dd_DocumentItemNo
      AND fp.dim_partid = x.dim_partid;
