/* Marius 10 Jun 2016 New SA for Merck HC - Merck Healthcare IBP */


delete from fact_inventorycoverage;

insert into fact_inventorycoverage(AMT_BLOCKEDSTOCKAMT,
AMT_BLOCKEDSTOCKAMT_GBL,
AMT_COGSACTUALRATE_EMD,
AMT_COGSFIXEDPLANRATE_EMD,
AMT_COGSFIXEDRATE_EMD,
AMT_COGSPLANRATE_EMD,
AMT_COGSPREVYEARFIXEDRATE_EMD,
AMT_COGSPREVYEARRATE_EMD,
AMT_COGSPREVYEARTO1_EMD,
AMT_COGSPREVYEARTO2_EMD,
AMT_COGSPREVYEARTO3_EMD,
AMT_COGSTURNOVERRATE1_EMD,
AMT_COGSTURNOVERRATE2_EMD,
AMT_COGSTURNOVERRATE3_EMD,
AMT_COMMERICALPRICE1,
AMT_CURPLANNEDPRICE,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
AMT_GBLSTDPRICE_MERCK,
AMT_MOVINGAVGPRICE,
AMT_MTLDLVRCOST,
AMT_ONHAND,
AMT_ONHAND_GBL,
AMT_OTHERCOST,
AMT_OVERHEADCOST,
AMT_PLANNEDPRICE1,
AMT_PREVIOUSPRICE,
AMT_PREVPLANNEDPRICE,
AMT_PRICEUNIT,
AMT_STDPRICEPMRA_MERCK,
AMT_STDUNITPRICE,
AMT_STDUNITPRICE_GBL,
AMT_STOCKINQINSPAMT,
AMT_STOCKINQINSPAMT_GBL,
AMT_STOCKINTRANSFERAMT,
AMT_STOCKINTRANSFERAMT_GBL,
AMT_STOCKINTRANSITAMT,
AMT_STOCKINTRANSITAMT_GBL,
AMT_STOCKVALUEAMT,
AMT_STOCKVALUEAMT_GBL,
AMT_UNRESTRICTEDCONSGNSTOCKAMT,
AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,
AMT_WIPBALANCE,
CT_AVGFCST_EMD,
CT_BLOCKEDCONSGNSTOCK,
CT_BLOCKEDSTOCK,
CT_BLOCKEDSTOCKRETURNS,
CT_CONSGNSTOCKINQINSP,
CT_GIQTY_31_60,
CT_GIQTY_61_90,
CT_GIQTY_91_180,
CT_GIQTY_LATE30,
CT_GLOBALEXTTOTALCOST_MERCK,
CT_GLOBALONHANDAMT_MERCK,
CT_GRQTY_31_60,
CT_GRQTY_61_90,
CT_GRQTY_91_180,
CT_GRQTY_LATE30,
CT_LASTRECEIVEDQTY,
CT_LOCALEXTTOTALCOST_MERCK,
CT_LOCALONHANDAMT_MERCK,
CT_POOPENQTY,
CT_RESTRICTEDCONSGNSTOCK,
CT_SOOPENQTY,
CT_STOCKINQINSP,
CT_STOCKINTRANSFER,
CT_STOCKINTRANSIT,
CT_STOCKQTY,
CT_TOTALRESTRICTEDSTOCK,
CT_TOTVALSTKQTY,
CT_UNRESTRICTEDCONSGNSTOCK,
CT_WIPAGING,
CT_WIPQTY,
DD_BATCHNO,
DD_DOCUMENTITEMNO,
DD_DOCUMENTNO,
DD_INVCOVFLAG_EMD,
DD_MOVEMENTTYPE,
DD_PLANNEDORDERNO,
DD_PRODORDERITEMNO,
DD_PRODORDERNUMBER,
DD_VALUATIONTYPE,
DIM_BWHIERARCHYCOUNTRYID,
DIM_BWPARTID,
DIM_BWPRODUCTHIERARCHYID,
DIM_COMPANYID,
DIM_CONSUMPTIONTYPEID,
DIM_COSTCENTERID,
DIM_CURRENCYID,
DIM_CURRENCYID_GBL,
DIM_CURRENCYID_TRA,
DIM_CUSTOMERID,
DIM_DATEIDACTUALRELEASE,
DIM_DATEIDACTUALSTART,
DIM_DATEIDEXPIRYDATE,
DIM_DATEIDLASTCHANGEDPRICE,
DIM_DATEIDLASTPROCESSED,
DIM_DATEIDPLANNEDPRICE2,
DIM_DATEIDTRANSACTION,
DIM_DOCUMENTSTATUSID,
DIM_DOCUMENTTYPEID,
DIM_INCOTERMID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_LASTRECEIVEDDATEID,
DIM_MDG_PARTID,
DIM_MOVEMENTINDICATORID,
DIM_PARTID,
DIM_PARTSALESID,
DIM_PLANTID,
DIM_PRODUCTHIERARCHYID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PROFITCENTERID,
DIM_PROJECTSOURCEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEMISCID,
DIM_PURCHASEORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKCATEGORYID,
DIM_STOCKTYPEID,
DIM_STORAGELOCATIONID,
DIM_STORAGELOCENTRYDATEID,
DIM_SUPPLYINGPLANTID,
DIM_TERMID,
DIM_UNITOFMEASUREID,
DIM_VENDORID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
FACT_INVENTORYCOVERAGEID,
DIM_CLUSTERID,
AMT_EXCHANGERATE_CUSTOM,
DIM_DATEOFMANUFACTURE,
DIM_DATEOFLASTGOODSRECEIPT,
CT_BASEUOMRATIOKG,
DD_JDACONTAINER)
select
AMT_BLOCKEDSTOCKAMT,
AMT_BLOCKEDSTOCKAMT_GBL,
AMT_COGSACTUALRATE_EMD,
AMT_COGSFIXEDPLANRATE_EMD,
AMT_COGSFIXEDRATE_EMD,
AMT_COGSPLANRATE_EMD,
AMT_COGSPREVYEARFIXEDRATE_EMD,
AMT_COGSPREVYEARRATE_EMD,
AMT_COGSPREVYEARTO1_EMD,
AMT_COGSPREVYEARTO2_EMD,
AMT_COGSPREVYEARTO3_EMD,
AMT_COGSTURNOVERRATE1_EMD,
AMT_COGSTURNOVERRATE2_EMD,
AMT_COGSTURNOVERRATE3_EMD,
AMT_COMMERICALPRICE1,
AMT_CURPLANNEDPRICE,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
AMT_GBLSTDPRICE_MERCK,
AMT_MOVINGAVGPRICE,
AMT_MTLDLVRCOST,
AMT_ONHAND,
AMT_ONHAND_GBL,
AMT_OTHERCOST,
AMT_OVERHEADCOST,
AMT_PLANNEDPRICE1,
AMT_PREVIOUSPRICE,
AMT_PREVPLANNEDPRICE,
AMT_PRICEUNIT,
AMT_STDPRICEPMRA_MERCK,
AMT_STDUNITPRICE,
AMT_STDUNITPRICE_GBL,
AMT_STOCKINQINSPAMT,
AMT_STOCKINQINSPAMT_GBL,
AMT_STOCKINTRANSFERAMT,
AMT_STOCKINTRANSFERAMT_GBL,
AMT_STOCKINTRANSITAMT,
AMT_STOCKINTRANSITAMT_GBL,
AMT_STOCKVALUEAMT,
AMT_STOCKVALUEAMT_GBL,
AMT_UNRESTRICTEDCONSGNSTOCKAMT,
AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,
AMT_WIPBALANCE,
CT_AVGFCST_EMD,
CT_BLOCKEDCONSGNSTOCK,
CT_BLOCKEDSTOCK,
CT_BLOCKEDSTOCKRETURNS,
CT_CONSGNSTOCKINQINSP,
CT_GIQTY_31_60,
CT_GIQTY_61_90,
CT_GIQTY_91_180,
CT_GIQTY_LATE30,
CT_GLOBALEXTTOTALCOST_MERCK,
CT_GLOBALONHANDAMT_MERCK,
CT_GRQTY_31_60,
CT_GRQTY_61_90,
CT_GRQTY_91_180,
CT_GRQTY_LATE30,
CT_LASTRECEIVEDQTY,
CT_LOCALEXTTOTALCOST_MERCK,
CT_LOCALONHANDAMT_MERCK,
CT_POOPENQTY,
CT_RESTRICTEDCONSGNSTOCK,
CT_SOOPENQTY,
CT_STOCKINQINSP,
CT_STOCKINTRANSFER,
CT_STOCKINTRANSIT,
CT_STOCKQTY,
CT_TOTALRESTRICTEDSTOCK,
CT_TOTVALSTKQTY,
CT_UNRESTRICTEDCONSGNSTOCK,
CT_WIPAGING,
CT_WIPQTY,
DD_BATCHNO,
DD_DOCUMENTITEMNO,
DD_DOCUMENTNO,
DD_INVCOVFLAG_EMD,
DD_MOVEMENTTYPE,
DD_PLANNEDORDERNO,
DD_PRODORDERITEMNO,
DD_PRODORDERNUMBER,
DD_VALUATIONTYPE,
DIM_BWHIERARCHYCOUNTRYID,
DIM_BWPARTID,
DIM_BWPRODUCTHIERARCHYID,
DIM_COMPANYID,
DIM_CONSUMPTIONTYPEID,
DIM_COSTCENTERID,
DIM_CURRENCYID,
DIM_CURRENCYID_GBL,
DIM_CURRENCYID_TRA,
DIM_CUSTOMERID,
DIM_DATEIDACTUALRELEASE,
DIM_DATEIDACTUALSTART,
DIM_DATEIDEXPIRYDATE,
DIM_DATEIDLASTCHANGEDPRICE,
DIM_DATEIDLASTPROCESSED,
DIM_DATEIDPLANNEDPRICE2,
DIM_DATEIDTRANSACTION,
DIM_DOCUMENTSTATUSID,
DIM_DOCUMENTTYPEID,
DIM_INCOTERMID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_LASTRECEIVEDDATEID,
DIM_MDG_PARTID,
DIM_MOVEMENTINDICATORID,
DIM_PARTID,
DIM_PARTSALESID,
DIM_PLANTID,
DIM_PRODUCTHIERARCHYID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PROFITCENTERID,
DIM_PROJECTSOURCEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEMISCID,
DIM_PURCHASEORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKCATEGORYID,
DIM_STOCKTYPEID,
DIM_STORAGELOCATIONID,
DIM_STORAGELOCENTRYDATEID,
DIM_SUPPLYINGPLANTID,
DIM_TERMID,
DIM_UNITOFMEASUREID,
DIM_VENDORID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
fact_inventoryagingid,
DIM_CLUSTERID,
AMT_EXCHANGERATE_CUSTOM,
DIM_DATEOFMANUFACTURE,
DIM_DATEOFLASTGOODSRECEIPT,
CT_BASEUOMRATIOKG,
DD_JDACONTAINER
from fact_inventoryaging;

drop table if exists tmp_packsbyproduct;
create table tmp_packsbyproduct
as
select 
	max(f.DIM_JDA_PRODUCTID) DIM_JDA_PRODUCTID
	,p.PRODUCT_NAME
	,strt.datevalue
	,sum(case when month(strt.datevalue) < month(current_date) and f.dd_type = 'ADJHIST' then f.ct_packs else 0 end) packs_ADJHIST
	,sum(case when month(strt.datevalue) >= month(current_date) and f.dd_type in ('TENDER','MKTFCST') then f.ct_packs else 0 end) packs_MKTFCST
	,sum(case when f.dd_type in ('OPERATING PLAN') then f.ct_packs else 0 end) packs_OP
from fact_jda_demandforecast f
	inner join DIM_JDA_PRODUCT p on f.DIM_JDA_PRODUCTID = p.DIM_JDA_PRODUCTID
	inner join DIM_JDA_LOCATION lc on f.DIM_JDA_LOCATIONID = lc.DIM_JDA_LOCATIONID
	inner join dim_date strt on f.DIM_DATEIDSTARTDATE = strt.dim_dateid
where f.dd_type in ('ADJHIST','TENDER','MKTFCST','OPERATING PLAN')
	and f.DD_FCSTLEVEL = '111' 
	and lc.LOCATION_NAME not like '%DISTR'
	and year(strt.datevalue) = year(current_date)
group by p.PRODUCT_NAME,strt.datevalue
union
select 
	max(f.DIM_JDA_PRODUCTID) DIM_JDA_PRODUCTID
	,f.dd_itemlocalcode PRODUCT_NAME
	,strt.datevalue
	,sum(case when month(strt.datevalue) < month(current_date) and f.dd_type = 'ADJHIST' then f.ct_packs else 0 end) packs_ADJHIST
	,sum(case when month(strt.datevalue) >= month(current_date) and f.dd_type in ('TENDER','MKTFCST') then f.ct_packs else 0 end) packs_MKTFCST
	,sum(case when f.dd_type in ('OPERATING PLAN') then f.ct_packs else 0 end) packs_OP
from fact_jda_demandforecast f
	inner join DIM_JDA_LOCATION lc on f.DIM_JDA_LOCATIONID = lc.DIM_JDA_LOCATIONID
	inner join dim_date strt on f.DIM_DATEIDSTARTDATE = strt.dim_dateid
where f.dd_type in ('ADJHIST','TENDER','MKTFCST','OPERATING PLAN')
	and f.DD_FCSTLEVEL = '111' 
	and lc.LOCATION_NAME not like '%DISTR'
	and year(strt.datevalue) = year(current_date)
	and f.dd_itemlocalcode not in (select distinct PRODUCT_NAME from DIM_JDA_PRODUCT)
group by f.dd_itemlocalcode,strt.datevalue;

/*
drop table if exists tmp_sameproduct
create table tmp_sameproduct
as
select distinct stsc_dfuview_dmdunit,stsc_dfuview_udc_prodlocalid from STSC_DFUVIEW where stsc_dfuview_udc_fcstlevel = 111 and stsc_dfuview_dmdunit <> stsc_dfuview_udc_prodlocalid

drop table if exists tmp_packsbyproduct_original
create table tmp_packsbyproduct_original
as
select a.PRODUCT_NAME
	,a.datevalue
	,sum(c.packs_ADJHIST) packs_ADJHIST
	,sum(c.packs_MKTFCST) packs_MKTFCST
	,sum(c.packs_OP) packs_OP
from tmp_packsbyproduct a
	inner join tmp_sameproduct b on a.PRODUCT_NAME = b.stsc_dfuview_dmdunit
	inner join tmp_packsbyproduct c on b.stsc_dfuview_udc_prodlocalid = c.PRODUCT_NAME and a.datevalue = c.datevalue
group by a.PRODUCT_NAME, a.datevalue

update tmp_packsbyproduct a
set a.packs_ADJHIST = a.packs_ADJHIST + c.packs_ADJHIST
	,a.packs_MKTFCST = a.packs_MKTFCST + c.packs_MKTFCST
	,a.packs_OP = a.packs_OP + c.packs_OP
from tmp_packsbyproduct a
	inner join tmp_packsbyproduct_original c on a.PRODUCT_NAME = c.PRODUCT_NAME and a.datevalue = c.datevalue
	
drop table if exists tmp_packsbyproduct_original
create table tmp_packsbyproduct_original
as
select a.PRODUCT_NAME
	,a.datevalue
	,sum(c.packs_ADJHIST) packs_ADJHIST
	,sum(c.packs_MKTFCST) packs_MKTFCST
	,sum(c.packs_OP) packs_OP
from tmp_packsbyproduct a
	inner join tmp_sameproduct b on a.PRODUCT_NAME = b.stsc_dfuview_udc_prodlocalid
	inner join tmp_packsbyproduct c on b.stsc_dfuview_dmdunit = c.PRODUCT_NAME and a.datevalue = c.datevalue
group by a.PRODUCT_NAME, a.datevalue
	
update tmp_packsbyproduct a
set a.packs_ADJHIST = a.packs_ADJHIST + c.packs_ADJHIST
	,a.packs_MKTFCST = a.packs_MKTFCST + c.packs_MKTFCST
	,a.packs_OP = a.packs_OP + c.packs_OP
from tmp_packsbyproduct a
	inner join tmp_packsbyproduct_original c on a.PRODUCT_NAME = c.PRODUCT_NAME and a.datevalue = c.datevalue
*/
	
DROP TABLE IF EXISTS tmp_fcst_updt;
CREATE TABLE tmp_fcst_updt
AS
SELECT dc.company
	,dp.partnumber
	,MAX(f.fact_inventorycoverageid) fact_inventorycoverageid
FROM fact_inventorycoverage f
	INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	INNER JOIN dim_part dp ON f.dim_partid = dp.dim_partid
WHERE f.dd_invcovflag_emd = 'X'
GROUP BY dc.company, dp.partnumber;

update fact_inventorycoverage f
set f.ct_jda_actual_jan = p.packs_ADJHIST
	,f.ct_jda_fcst_jan = p.packs_MKTFCST
	,f.ct_jda_op_jan = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 1;
	
update fact_inventorycoverage f
set f.ct_jda_actual_feb = p.packs_ADJHIST
	,f.ct_jda_fcst_feb = p.packs_MKTFCST
	,f.ct_jda_op_feb = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 2;
	
update fact_inventorycoverage f
set f.ct_jda_actual_mar = p.packs_ADJHIST
	,f.ct_jda_fcst_mar = p.packs_MKTFCST
	,f.ct_jda_op_mar = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 3;
	
update fact_inventorycoverage f
set f.ct_jda_actual_apr = p.packs_ADJHIST
	,f.ct_jda_fcst_apr = p.packs_MKTFCST
	,f.ct_jda_op_apr = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 4;
	
update fact_inventorycoverage f
set f.ct_jda_actual_may = p.packs_ADJHIST
	,f.ct_jda_fcst_may = p.packs_MKTFCST
	,f.ct_jda_op_may = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 5;
	
update fact_inventorycoverage f
set f.ct_jda_actual_jun = p.packs_ADJHIST
	,f.ct_jda_fcst_jun = p.packs_MKTFCST
	,f.ct_jda_op_jun = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 6;
	
update fact_inventorycoverage f
set f.ct_jda_actual_jul = p.packs_ADJHIST
	,f.ct_jda_fcst_jul = p.packs_MKTFCST
	,f.ct_jda_op_jul = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 7;
	
update fact_inventorycoverage f
set f.ct_jda_actual_aug = p.packs_ADJHIST
	,f.ct_jda_fcst_aug = p.packs_MKTFCST
	,f.ct_jda_op_aug = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 8;
	
update fact_inventorycoverage f
set f.ct_jda_actual_sep = p.packs_ADJHIST
	,f.ct_jda_fcst_sep = p.packs_MKTFCST
	,f.ct_jda_op_sep = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 9;
	
update fact_inventorycoverage f
set f.ct_jda_actual_oct = p.packs_ADJHIST
	,f.ct_jda_fcst_oct = p.packs_MKTFCST
	,f.ct_jda_op_oct = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 10;
	
update fact_inventorycoverage f
set f.ct_jda_actual_nov = p.packs_ADJHIST
	,f.ct_jda_fcst_nov = p.packs_MKTFCST
	,f.ct_jda_op_nov = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 11;
	
update fact_inventorycoverage f
set f.ct_jda_actual_dec = p.packs_ADJHIST
	,f.ct_jda_fcst_dec = p.packs_MKTFCST
	,f.ct_jda_op_dec = p.packs_OP
from fact_inventorycoverage f
	inner join tmp_fcst_updt x on f.fact_inventorycoverageid = x.fact_inventorycoverageid
	inner join tmp_packsbyproduct p on x.partnumber = p.PRODUCT_NAME and month(p.datevalue) = 12;
	
drop table if exists tmp_updt_jda_prod;
create table tmp_updt_jda_prod
as
select PRODUCT_NAME,max(DIM_JDA_PRODUCTID) DIM_JDA_PRODUCTID from tmp_packsbyproduct group by PRODUCT_NAME;
	
update fact_inventorycoverage f
set f.DIM_JDA_PRODUCTID = p.DIM_JDA_PRODUCTID
from fact_inventorycoverage f
	INNER JOIN dim_part dp ON f.dim_partid = dp.dim_partid
	inner join tmp_updt_jda_prod p on dp.partnumber = p.PRODUCT_NAME;
	
/* Marius 14 jun add avg sales price - Tempo EU */
	
drop table if exists tmp_salesprice_ibp;
create table tmp_salesprice_ibp
as
select dp.partnumber,cast(avg(f.amt_ScheduleTotal / f.ct_ScheduleQtySalesUnit * f.amt_ExchangeRate_GBL) as decimal (18,4)) amt_UnitPrice 
from fact_salesorder f
	inner join dim_part dp on f.dim_partid = dp.dim_partid
	inner join Dim_DocumentCategory dc on f.Dim_DocumentCategoryid = dc.Dim_DocumentCategoryid
	inner join dim_salesorderitemstatus sois on f.Dim_SalesOrderItemStatusid = sois.Dim_SalesOrderItemStatusid
	inner join Dim_SalesOrderItemCategory soic on f.Dim_salesorderitemcategoryid = soic.Dim_SalesOrderItemCategoryid
	inner join Dim_SalesDocumentType sdt on f.Dim_SalesDocumentTypeid = sdt.Dim_SalesDocumentTypeid
where dc.DocumentCategory in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBAK-VBTYP' and PLATFORMS like '%P01%')
	and sois.ItemRejectionStatus in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBUP-ABSTA' and PLATFORMS like '%P01%')
	and soic.SalesOrderItemCategory not in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBAP-PSTYV' and PLATFORMS like '%P01%')
	and sdt.DocumentType in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBAK-AUART' and PLATFORMS like '%P01%')
	and f.dd_BusinessCustomerPONo not like 'Forecast%'
	and f.ct_ScheduleQtySalesUnit > 0
group by dp.partnumber;

update fact_inventorycoverage f
set f.amt_avgsalesprice = t.amt_UnitPrice
from fact_inventorycoverage f, dim_part dp, tmp_salesprice_ibp t
where f.dim_partid = dp.dim_partid
	and dp.partnumber = t.partnumber
	and f.amt_avgsalesprice <> t.amt_UnitPrice;
	
	
/* Test Update for dd_quadrant_db flag - Amar - 06152016*/

drop table if exists tmp_dd_quadrant_db;
create table tmp_dd_quadrant_db as 
select dim_partid, 
case 
	when sum( (CT_JDA_ACTUAL_JAN) + (CT_JDA_FCST_JAN) + (CT_JDA_ACTUAL_FEB) + (CT_JDA_FCST_FEB) + (CT_JDA_ACTUAL_MAR) + (CT_JDA_FCST_MAR) + (CT_JDA_ACTUAL_APR) + (CT_JDA_FCST_APR) + (CT_JDA_ACTUAL_MAY) + (CT_JDA_FCST_MAY) + (CT_JDA_ACTUAL_JUN) + (CT_JDA_FCST_JUN) + (CT_JDA_ACTUAL_JUL) + (CT_JDA_FCST_JUL) + (CT_JDA_ACTUAL_AUG) + (CT_JDA_FCST_AUG) + (CT_JDA_ACTUAL_SEP) + (CT_JDA_FCST_SEP) + (CT_JDA_ACTUAL_OCT) + (CT_JDA_FCST_OCT) + (CT_JDA_ACTUAL_NOV) + (CT_JDA_FCST_NOV) + (CT_JDA_ACTUAL_DEC) + (CT_JDA_FCST_DEC) ) - sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) = 0 
		and sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) = 0
	then 0
	else
		case 
			when (CASE WHEN SUM(ct_avgfcst_emd) <> 0 THEN SUM(CASE WHEN (f_invcvrg.dd_invcovflag_emd) = 'X' THEN (f_invcvrg.ct_StockQty) ELSE 0 END) / SUM(ct_avgfcst_emd) ELSE 9999.00 END) < 1 and (case when sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) <> 0 then (sum( (CT_JDA_ACTUAL_JAN) + (CT_JDA_FCST_JAN) + (CT_JDA_ACTUAL_FEB) + (CT_JDA_FCST_FEB) + (CT_JDA_ACTUAL_MAR) + (CT_JDA_FCST_MAR) + (CT_JDA_ACTUAL_APR) + (CT_JDA_FCST_APR) + (CT_JDA_ACTUAL_MAY) + (CT_JDA_FCST_MAY) + (CT_JDA_ACTUAL_JUN) + (CT_JDA_FCST_JUN) + (CT_JDA_ACTUAL_JUL) + (CT_JDA_FCST_JUL) + (CT_JDA_ACTUAL_AUG) + (CT_JDA_FCST_AUG) + (CT_JDA_ACTUAL_SEP) + (CT_JDA_FCST_SEP) + (CT_JDA_ACTUAL_OCT) + (CT_JDA_FCST_OCT) + (CT_JDA_ACTUAL_NOV) + (CT_JDA_FCST_NOV) + (CT_JDA_ACTUAL_DEC) + (CT_JDA_FCST_DEC) ) - sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) )) / sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) else 1 end) < -0.15 then 1
			when (CASE WHEN SUM(ct_avgfcst_emd) <> 0 THEN SUM(CASE WHEN (f_invcvrg.dd_invcovflag_emd) = 'X' THEN (f_invcvrg.ct_StockQty) ELSE 0 END) / SUM(ct_avgfcst_emd) ELSE 9999.00 END) < 1 and (case when sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) <> 0 then (sum( (CT_JDA_ACTUAL_JAN) + (CT_JDA_FCST_JAN) + (CT_JDA_ACTUAL_FEB) + (CT_JDA_FCST_FEB) + (CT_JDA_ACTUAL_MAR) + (CT_JDA_FCST_MAR) + (CT_JDA_ACTUAL_APR) + (CT_JDA_FCST_APR) + (CT_JDA_ACTUAL_MAY) + (CT_JDA_FCST_MAY) + (CT_JDA_ACTUAL_JUN) + (CT_JDA_FCST_JUN) + (CT_JDA_ACTUAL_JUL) + (CT_JDA_FCST_JUL) + (CT_JDA_ACTUAL_AUG) + (CT_JDA_FCST_AUG) + (CT_JDA_ACTUAL_SEP) + (CT_JDA_FCST_SEP) + (CT_JDA_ACTUAL_OCT) + (CT_JDA_FCST_OCT) + (CT_JDA_ACTUAL_NOV) + (CT_JDA_FCST_NOV) + (CT_JDA_ACTUAL_DEC) + (CT_JDA_FCST_DEC) ) - sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) )) / sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) else 1 end) > 0.15 then 2
			when (CASE WHEN SUM(ct_avgfcst_emd) <> 0 THEN SUM(CASE WHEN (f_invcvrg.dd_invcovflag_emd) = 'X' THEN (f_invcvrg.ct_StockQty) ELSE 0 END) / SUM(ct_avgfcst_emd) ELSE 9999.00 END) >= 3 and (case when sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) <> 0 then (sum( (CT_JDA_ACTUAL_JAN) + (CT_JDA_FCST_JAN) + (CT_JDA_ACTUAL_FEB) + (CT_JDA_FCST_FEB) + (CT_JDA_ACTUAL_MAR) + (CT_JDA_FCST_MAR) + (CT_JDA_ACTUAL_APR) + (CT_JDA_FCST_APR) + (CT_JDA_ACTUAL_MAY) + (CT_JDA_FCST_MAY) + (CT_JDA_ACTUAL_JUN) + (CT_JDA_FCST_JUN) + (CT_JDA_ACTUAL_JUL) + (CT_JDA_FCST_JUL) + (CT_JDA_ACTUAL_AUG) + (CT_JDA_FCST_AUG) + (CT_JDA_ACTUAL_SEP) + (CT_JDA_FCST_SEP) + (CT_JDA_ACTUAL_OCT) + (CT_JDA_FCST_OCT) + (CT_JDA_ACTUAL_NOV) + (CT_JDA_FCST_NOV) + (CT_JDA_ACTUAL_DEC) + (CT_JDA_FCST_DEC) ) - sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) )) / sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) else 1 end) < -0.15 then 4
			when (CASE WHEN SUM(ct_avgfcst_emd) <> 0 THEN SUM(CASE WHEN (f_invcvrg.dd_invcovflag_emd) = 'X' THEN (f_invcvrg.ct_StockQty) ELSE 0 END) / SUM(ct_avgfcst_emd) ELSE 9999.00 END) >= 3 and (case when sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) <> 0 then (sum( (CT_JDA_ACTUAL_JAN) + (CT_JDA_FCST_JAN) + (CT_JDA_ACTUAL_FEB) + (CT_JDA_FCST_FEB) + (CT_JDA_ACTUAL_MAR) + (CT_JDA_FCST_MAR) + (CT_JDA_ACTUAL_APR) + (CT_JDA_FCST_APR) + (CT_JDA_ACTUAL_MAY) + (CT_JDA_FCST_MAY) + (CT_JDA_ACTUAL_JUN) + (CT_JDA_FCST_JUN) + (CT_JDA_ACTUAL_JUL) + (CT_JDA_FCST_JUL) + (CT_JDA_ACTUAL_AUG) + (CT_JDA_FCST_AUG) + (CT_JDA_ACTUAL_SEP) + (CT_JDA_FCST_SEP) + (CT_JDA_ACTUAL_OCT) + (CT_JDA_FCST_OCT) + (CT_JDA_ACTUAL_NOV) + (CT_JDA_FCST_NOV) + (CT_JDA_ACTUAL_DEC) + (CT_JDA_FCST_DEC) ) - sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) )) / sum( (CT_JDA_OP_APR) + (CT_JDA_OP_AUG) + (CT_JDA_OP_DEC) + (CT_JDA_OP_FEB) + (CT_JDA_OP_JAN) + (CT_JDA_OP_JUL) + (CT_JDA_OP_JUN) + (CT_JDA_OP_MAR) + (CT_JDA_OP_MAY) + (CT_JDA_OP_NOV) + (CT_JDA_OP_OCT) + (CT_JDA_OP_SEP) ) else 1 end) > 0.15 then 3
			else 0
		end
end dd_quadrant_db
from fact_inventorycoverage f_invcvrg
group by dim_partid;

update fact_inventorycoverage f
set f.dd_quadrant_db = ifnull(d.dd_quadrant_db,10)
from fact_inventorycoverage f, tmp_dd_quadrant_db d
where f.dim_partid = d.dim_partid
and f.dd_quadrant_db <> ifnull(d.dd_quadrant_db,10);	

drop table if exists tmp_dd_quadrant_db;	
	
	