

/* Start of bi_process_bom_fact */
/* Before this, run vw_getstdprice_custom.fact_bom.sql, vw_getstdprice_std.part1.sql, vw_funct_fiscal_year.custom.getStdPrice.sql, vw_funct_fiscal_year.standard.sql,vw_getstdprice_std.part2.sql */

/* Q1 */
/*Georgiana Changes 07 Oct 2015 changing updates from Q1 and Q2 in order to resolve memory issues*/

/*UPDATE fact_bom bom
FROM dim_part p, dim_plant pl, dim_date dd, tmp_getStdPrice z
SET bom.amt_CompStdPrice = z.StandardPrice
 WHERE     bom.Dim_BOMComponentId = p.Dim_Partid
AND pl.PlantCode = p.Plant AND pl.RowIsCurrent = 1
AND bom.Dim_ValidFromDateid = dd.Dim_Dateid AND p.RowIsCurrent = 1
AND bom.Dim_BOMComponentId <> 1 AND pl.Dim_PlantId <> 1
AND z.pCompanyCode = pl.CompanyCode
AND z.pPlant = p.plant
AND z.pMaterialNo = p.PartNumber AND z.pFiYear = dd.FinancialYear AND z.pPeriod = dd.FinancialMonthNumber
AND z.fact_script_name = 'bi_process_bom_fact'
AND z.vUMREZ = 1 AND z.vUMREN = 1
AND z.PONumber IS NULL
AND z.pUnitPrice =  amt_PriceUnit
AND bom.amt_CompStdPrice <> z.StandardPrice*/

DROP TABLE IF EXISTS TMP_FOR_UPDT_A;
CREATE TABLE TMP_FOR_UPDT_A
AS
SELECT p.Dim_Partid,dd.Dim_Dateid,pl.CompanyCode,p.plant,p.PartNumber,dd.FinancialYear,dd.FinancialMonthNumber,bom.amt_PriceUnit,bom.amt_CompStdPrice
FROM fact_bom bom
	INNER JOIN dim_part p ON bom.Dim_BOMComponentId = p.Dim_Partid AND p.RowIsCurrent = 1 AND bom.Dim_BOMComponentId <> 1
	INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant AND pl.RowIsCurrent = 1 AND pl.Dim_PlantId <> 1
	INNER JOIN dim_date dd ON bom.Dim_ValidFromDateid = dd.Dim_Dateid;

DROP TABLE IF EXISTS tmp_for_upd;
CREATE TABLE tmp_for_upd AS
SELECT DISTINCT z.StandardPrice, tmp.Dim_Partid, tmp.Dim_Dateid 
FROM tmp_getStdPrice z
	INNER JOIN TMP_FOR_UPDT_A tmp ON z.pCompanyCode = tmp.CompanyCode
		AND z.pPlant = tmp.plant
		AND z.pMaterialNo = tmp.PartNumber AND z.pFiYear = tmp.FinancialYear AND z.pPeriod = tmp.FinancialMonthNumber
		AND z.fact_script_name = 'bi_process_bom_fact'
		AND z.vUMREZ = 1 AND z.vUMREN = 1
		AND z.PONumber IS NULL
		AND z.pUnitPrice =  tmp.amt_PriceUnit
		AND tmp.amt_CompStdPrice <> z.StandardPrice;
						
UPDATE fact_bom bom
SET bom.amt_CompStdPrice =z.StandardPrice 
,bom.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_for_upd z, fact_bom bom
WHERE bom.Dim_BOMComponentId = z.Dim_Partid
 	  AND bom.Dim_ValidFromDateid = z.Dim_Dateid
      AND bom.Dim_BOMComponentId <> 1
      AND bom.amt_CompStdPrice <>z.StandardPrice;
			
/* Q2 */
/*UPDATE fact_bom bom
FROM dim_part p, dim_plant pl, dim_date dd, tmp_getStdPrice z
SET bom.amt_PartStdPrice = z.StandardPrice
WHERE     bom.Dim_PartId = p.Dim_Partid
AND bom.Dim_MaterialPlantId = pl.Dim_PlantId
AND pl.PlantCode = p.Plant
AND pl.RowIsCurrent = 1
AND bom.Dim_ValidFromDateid = dd.Dim_Dateid
AND p.RowIsCurrent = 1
AND bom.Dim_PartId <> 1
AND bom.Dim_MaterialPlantId <> 1
AND z.pCompanyCode = pl.CompanyCode
AND z.pPlant = pl.PlantCode
AND z.pMaterialNo = p.PartNumber
AND z.pFiYear = dd.FinancialYear
AND z.pPeriod = dd.FinancialMonthNumber
AND z.fact_script_name = 'bi_process_bom_fact'
AND z.vUMREZ = 1
AND z.vUMREN = 1
AND z.PONumber IS NULL
AND z.pUnitPrice =  amt_PriceUnit
AND bom.amt_PartStdPrice <> z.StandardPrice*/

DROP TABLE IF EXISTS TMP_FOR_UPDT_A;
CREATE TABLE TMP_FOR_UPDT_A
AS
SELECT p.Dim_Partid,dd.Dim_Dateid,pl.Dim_PlantId,pl.CompanyCode,pl.PlantCode,p.PartNumber,dd.FinancialYear,dd.FinancialMonthNumber,bom.amt_PriceUnit,bom.amt_PartStdPrice
FROM fact_bom bom
	INNER JOIN dim_part p ON bom.Dim_PartId = p.Dim_Partid AND p.RowIsCurrent = 1 AND bom.Dim_PartId <> 1
	INNER JOIN dim_plant pl ON bom.Dim_MaterialPlantId = pl.Dim_PlantId AND pl.PlantCode = p.Plant AND pl.RowIsCurrent = 1 AND bom.Dim_MaterialPlantId <> 1
	INNER JOIN dim_date dd ON bom.Dim_ValidFromDateid = dd.Dim_Dateid;

DROP TABLE IF EXISTS tmp_for_upd;
CREATE TABLE tmp_for_upd AS
SELECT DISTINCT z.StandardPrice, tmp.Dim_Partid,tmp.Dim_Dateid,tmp.Dim_PlantId 
FROM tmp_getStdPrice z
	INNER JOIN TMP_FOR_UPDT_A tmp ON z.pCompanyCode = tmp.CompanyCode
		AND z.pPlant = tmp.PlantCode
		AND z.pMaterialNo = tmp.PartNumber
		AND z.pFiYear = tmp.FinancialYear
		AND z.pPeriod = tmp.FinancialMonthNumber
		AND z.fact_script_name = 'bi_process_bom_fact'
		AND z.vUMREZ = 1
		AND z.vUMREN = 1
		AND z.PONumber IS NULL
		AND z.pUnitPrice =  tmp.amt_PriceUnit
		AND tmp.amt_PartStdPrice <> z.StandardPrice;
						
UPDATE fact_bom bom
SET bom.amt_PartStdPrice = z.StandardPrice 
FROM tmp_for_upd z, fact_bom bom
WHERE bom.Dim_PartId = z.Dim_Partid
AND bom.Dim_MaterialPlantId = z.Dim_PlantId
AND bom.Dim_ValidFromDateid = z.Dim_Dateid
AND bom.Dim_PartId <> 1
AND bom.Dim_MaterialPlantId <> 1
AND bom.amt_PartStdPrice <> z.StandardPrice;

DROP TABLE IF EXISTS tmp_for_upd;
/*End changes 07 Oct 2015*/
/* Q3 */

DROP TABLE IF EXISTS tmp_distinct_doc_fact_purchase;
CREATE TABLE tmp_distinct_doc_fact_purchase 
AS
SELECT DISTINCT p.Dim_PartId,p.dd_DocumentNo
FROM fact_purchase p
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_PartId = p.Dim_Partid AND bom.Dim_PartId <> 1 );

DROP TABLE IF EXISTS tmp_distinct_doc_fact_purchase_2;
CREATE TABLE tmp_distinct_doc_fact_purchase_2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_distinct_doc_fact_purchase
GROUP BY Dim_PartId;


UPDATE fact_bom bom 
SET bom.ct_ParentPartTotalPO = 0
WHERE bom.Dim_PartId <> 1
AND ifnull(bom.ct_ParentPartTotalPO,-1) <> 0;

UPDATE fact_bom bom 
SET bom.ct_ParentPartTotalPO =
	p.count_dd_DocumentNo
FROM tmp_distinct_doc_fact_purchase_2 p, fact_bom bom
WHERE p.Dim_PartId = bom.Dim_Partid
AND bom.Dim_PartId <> 1
AND  IFNULL(bom.ct_ParentPartTotalPO,-1)  <>  p.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_purchase;
DROP TABLE IF EXISTS tmp_distinct_doc_fact_purchase_2;


/* Q4 */
/* Not splitting the queries from Q4 onwards. Do it only if its necessary during performance testing */

merge into fact_bom dst
using (select bom.fact_bomid, ds.ct_ParentPartOpenPO
	   from fact_bom bom
				left join (select p.Dim_PartId, count(DISTINCT p.dd_DocumentNo) ct_ParentPartOpenPO
						   FROM fact_purchase p
									inner join dim_purchasemisc pm on p.Dim_PurchaseMiscId = pm.Dim_PurchaseMiscid
						   where    pm.ItemDeliveryComplete = 'Not Set'
							    AND (p.ct_DeliveryQty - p.ct_ReceivedQty > 0)
						   group by p.Dim_PartId) ds on ds.Dim_PartId = bom.Dim_Partid
	   where bom.Dim_PartId <> 1
	  ) src
on dst.fact_bomid = src.fact_bomid
when matched then update set dst.ct_ParentPartOpenPO = src.ct_ParentPartOpenPO
where dst.ct_ParentPartOpenPO <> src.ct_ParentPartOpenPO;
		/* VW Original
			UPDATE fact_bom bom 
			   SET bom.ct_ParentPartOpenPO =
					  (SELECT count(DISTINCT p.dd_DocumentNo)
						 FROM fact_purchase p, dim_purchasemisc pm
						WHERE     p.Dim_PartId = bom.Dim_Partid
							  AND p.Dim_PurchaseMiscId = pm.Dim_PurchaseMiscid
							  AND pm.ItemDeliveryComplete = 'Not Set'
							  AND (p.ct_DeliveryQty - p.ct_ReceivedQty > 0))
			 WHERE bom.Dim_PartId <> 1 */
 

/* Q5 */
merge into fact_bom dst
using (select bom.fact_bomid, ds.ct_CompPartTotalPO
	   from fact_bom bom
				left join (select p.Dim_PartId, count(DISTINCT p.dd_DocumentNo) ct_CompPartTotalPO
						   FROM fact_purchase p
						   group by p.Dim_PartId) ds on ds.Dim_PartId = bom.Dim_BOMComponentId
	   where bom.Dim_BOMComponentId <> 1
	  ) src
on dst.fact_bomid = src.fact_bomid
when matched then update set dst.ct_CompPartTotalPO = src.ct_CompPartTotalPO
where dst.ct_CompPartTotalPO <> src.ct_CompPartTotalPO;
		/* VW Original
			UPDATE fact_bom bom  
			   SET bom.ct_CompPartTotalPO =
					  (SELECT count(DISTINCT p.dd_DocumentNo)
						 FROM fact_purchase p
						WHERE p.Dim_Partid = bom.Dim_BOMComponentId)
			 WHERE bom.Dim_BOMComponentId <> 1
			AND bom.ct_CompPartTotalPO <> 
					  (SELECT count(DISTINCT p.dd_DocumentNo)
						 FROM fact_purchase p
						WHERE p.Dim_Partid = bom.Dim_BOMComponentId) */

/* Q6 */
merge into fact_bom dst
using (select bom.fact_bomid, ds.ct_CompPartOpenPO
	   from fact_bom bom
				left join (select p.Dim_PartId, count(DISTINCT p.dd_DocumentNo) ct_CompPartOpenPO
						   FROM fact_purchase p
									inner join dim_purchasemisc pm on p.Dim_PurchaseMiscId = pm.Dim_PurchaseMiscid
						   where    pm.ItemDeliveryComplete = 'Not Set'
							    AND (p.ct_DeliveryQty - p.ct_ReceivedQty > 0)
						   group by p.Dim_PartId) ds on ds.Dim_PartId = bom.Dim_BOMComponentId
	   where bom.Dim_BOMComponentId <> 1
	  ) src
on dst.fact_bomid = src.fact_bomid
when matched then update set dst.ct_CompPartOpenPO = src.ct_CompPartOpenPO
where dst.ct_CompPartOpenPO <> src.ct_CompPartOpenPO;
		/* VW Original
			UPDATE fact_bom bom 
			   SET bom.ct_CompPartOpenPO =
					  (SELECT count(DISTINCT p.dd_DocumentNo)
						 FROM fact_purchase p, dim_purchasemisc pm
						WHERE     p.Dim_Partid = bom.Dim_BOMComponentId
							  AND p.Dim_PurchaseMiscId = pm.Dim_PurchaseMiscid
							  AND pm.ItemDeliveryComplete = 'Not Set'
							  AND (p.ct_DeliveryQty - p.ct_ReceivedQty > 0))
			 WHERE bom.Dim_BOMComponentId <> 1
			AND bom.ct_CompPartOpenPO <>          (SELECT count(DISTINCT p.dd_DocumentNo)
						 FROM fact_purchase p, dim_purchasemisc pm
						WHERE     p.Dim_Partid = bom.Dim_BOMComponentId
							  AND p.Dim_PurchaseMiscId = pm.Dim_PurchaseMiscid
							  AND pm.ItemDeliveryComplete = 'Not Set'
							  AND (p.ct_DeliveryQty - p.ct_ReceivedQty > 0)) */

/* Q7 */
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
CREATE TABLE tmp_distinct_doc_fact_mrp 
AS
SELECT DISTINCT mp.Dim_PartId,mp.dd_DocumentNo, mp.dd_DocumentItemNo, mp.dd_ScheduleNo
FROM fact_mrp mp
INNER JOIN dim_MRPElement me ON me.Dim_MRPElementID = mp.Dim_MRPElementid
INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mp.dim_mrpexceptionID1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_BOMComponentId = mp.Dim_Partid AND bom.Dim_BOMComponentId <> 1 )
AND mex.exceptionkey = 'U1'
AND me.MRPElement IN ('BE', 'LE')
AND mp.ct_Completed = 0;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;
CREATE TABLE tmp_distinct_doc_fact_mrp2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp
GROUP BY Dim_PartId;


UPDATE fact_bom bom 
SET bom.ct_CompPullIn = 0
WHERE bom.Dim_BOMComponentId <> 1
AND  ifnull(bom.ct_CompPullIn,-1) <> 0;

UPDATE fact_bom bom 
SET bom.ct_CompPullIn =
		mp.count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_BOMComponentId
 AND bom.Dim_BOMComponentId <> 1
 AND ifnull(bom.ct_CompPullIn,-1) <> mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;


/* Q8 */
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
CREATE TABLE tmp_distinct_doc_fact_mrp 
AS
SELECT DISTINCT mp.Dim_PartId,mp.dd_DocumentNo, mp.dd_DocumentItemNo, mp.dd_ScheduleNo
FROM fact_mrp mp
INNER JOIN dim_MRPElement me ON me.Dim_MRPElementID = mp.Dim_MRPElementid
INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mp.dim_mrpexceptionID1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_BOMComponentId = mp.Dim_Partid AND bom.Dim_BOMComponentId <> 1 )
AND mex.exceptionkey = 'U2'
AND me.MRPElement IN ('BE', 'LE')
AND mp.ct_Completed = 0;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;
CREATE TABLE tmp_distinct_doc_fact_mrp2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp
GROUP BY Dim_PartId;


UPDATE fact_bom bom 
SET bom.ct_CompPushOut = 0
WHERE bom.Dim_BOMComponentId <> 1
AND bom.ct_CompPushOut <> 0;

UPDATE fact_bom bom 
SET bom.ct_CompPushOut = mp.count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_BOMComponentId
 AND bom.Dim_BOMComponentId <> 1
 AND ifnull(bom.ct_CompPushOut,-1) <> mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;

 
/* Q9 */
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
CREATE TABLE tmp_distinct_doc_fact_mrp 
AS
SELECT DISTINCT mp.Dim_PartId,mp.dd_DocumentNo, mp.dd_DocumentItemNo, mp.dd_ScheduleNo
FROM fact_mrp mp
INNER JOIN dim_MRPElement me ON me.Dim_MRPElementID = mp.Dim_MRPElementid
INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mp.dim_mrpexceptionID1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_BOMComponentId = mp.Dim_Partid AND bom.Dim_BOMComponentId <> 1 )
AND mex.exceptionkey = 'U3'
AND me.MRPElement IN ('BE', 'LE')
AND mp.ct_Completed = 0;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;
CREATE TABLE tmp_distinct_doc_fact_mrp2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp
GROUP BY Dim_PartId;


UPDATE fact_bom bom 
SET bom.ct_CompCancel = 0
WHERE bom.Dim_BOMComponentId <> 1
AND ifnull(bom.ct_CompCancel,-1) <> 0;

UPDATE fact_bom bom 
SET bom.ct_CompCancel =
		mp.count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_BOMComponentId
 AND bom.Dim_BOMComponentId <> 1
 AND  ifnull(bom.ct_CompCancel,-1) <> mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;


/* Q10 ct_ParentPullIn */
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
CREATE TABLE tmp_distinct_doc_fact_mrp 
AS
SELECT DISTINCT mp.Dim_PartId,mp.dd_DocumentNo, mp.dd_DocumentItemNo, mp.dd_ScheduleNo
FROM fact_mrp mp
INNER JOIN dim_MRPElement me ON me.Dim_MRPElementID = mp.Dim_MRPElementid
INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mp.dim_mrpexceptionID1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_PartId = mp.Dim_Partid AND bom.Dim_PartId <> 1 )
AND mex.exceptionkey = 'U1'
AND me.MRPElement IN ('BE', 'LE')
AND mp.ct_Completed = 0;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;
CREATE TABLE tmp_distinct_doc_fact_mrp2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp
GROUP BY Dim_PartId;


UPDATE fact_bom bom 
SET bom.ct_ParentPullIn = 0
WHERE bom.Dim_PartId <> 1
AND ifnull(bom.ct_ParentPullIn,-1) <>  0;

UPDATE fact_bom bom 
SET bom.ct_ParentPullIn =
		mp.count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_PartId
 AND bom.Dim_PartId <> 1
 AND ifnull(bom.ct_ParentPullIn,-1) <> 	mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;


/* Q11 - ct_ParentPushOut */
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
CREATE TABLE tmp_distinct_doc_fact_mrp 
AS
SELECT DISTINCT mp.Dim_PartId,mp.dd_DocumentNo, mp.dd_DocumentItemNo, mp.dd_ScheduleNo
FROM fact_mrp mp
INNER JOIN dim_MRPElement me ON me.Dim_MRPElementID = mp.Dim_MRPElementid
INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mp.dim_mrpexceptionID1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_PartId = mp.Dim_Partid AND bom.Dim_PartId <> 1 )
AND mex.exceptionkey = 'U2'
AND me.MRPElement IN ('BE', 'LE')
AND mp.ct_Completed = 0;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;
CREATE TABLE tmp_distinct_doc_fact_mrp2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp
GROUP BY Dim_PartId;


UPDATE fact_bom bom 
SET bom.ct_ParentPushOut = 0
WHERE bom.Dim_PartId <> 1
AND ifnull(bom.ct_ParentPushOut,-1) <> 0;

UPDATE fact_bom bom 
SET bom.ct_ParentPushOut =
		mp.count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp2 mp, fact_bom bom 
 WHERE mp.Dim_Partid = bom.Dim_PartId
 AND bom.Dim_PartId <> 1
 AND ifnull(bom.ct_ParentPushOut,-1) <> mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;


/* Q12 - ct_ParentCancel */
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
CREATE TABLE tmp_distinct_doc_fact_mrp 
AS
SELECT DISTINCT mp.Dim_PartId,mp.dd_DocumentNo, mp.dd_DocumentItemNo, mp.dd_ScheduleNo
FROM fact_mrp mp
INNER JOIN dim_MRPElement me ON me.Dim_MRPElementID = mp.Dim_MRPElementid
INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mp.dim_mrpexceptionID1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_PartId = mp.Dim_Partid AND bom.Dim_PartId <> 1 )
AND mex.exceptionkey = 'U3'
AND me.MRPElement IN ('BE', 'LE')
AND mp.ct_Completed = 0;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;
CREATE TABLE tmp_distinct_doc_fact_mrp2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp
GROUP BY Dim_PartId;


UPDATE fact_bom bom 
SET bom.ct_ParentCancel = 0
WHERE bom.Dim_PartId <> 1
AND ifnull(bom.ct_ParentCancel,-1) <>  0;

UPDATE fact_bom bom 
SET bom.ct_ParentCancel =
		mp.count_dd_DocumentNo
FROM tmp_distinct_doc_fact_mrp2 mp, fact_bom bom 
 WHERE mp.Dim_Partid = bom.Dim_PartId
 AND bom.Dim_PartId <> 1
 AND ifnull(bom.ct_ParentCancel,-1) <> 	mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp;
DROP TABLE IF EXISTS tmp_distinct_doc_fact_mrp2;


/* Q13 - ct_ParentTotalNewBuy */

DROP TABLE IF EXISTS tmp_ct_ParentTotalNewBuy;
CREATE TABLE tmp_ct_ParentTotalNewBuy 
AS
SELECT DISTINCT p.Dim_PartId,e.EBAN_BANFN
FROM EBAN e
INNER JOIN dim_part p  ON p.PartNumber = e.EBAN_MATNR AND p.Plant = e.EBAN_WERKS
INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant AND pl.RowIsCurrent = 1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_PartId = p.Dim_Partid AND bom.Dim_PartId <> 1 )
AND e.EBAN_LOEKZ IS NULL;

DROP TABLE IF EXISTS tmp_ct_ParentTotalNewBuy2;
CREATE TABLE tmp_ct_ParentTotalNewBuy2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_ct_ParentTotalNewBuy
GROUP BY Dim_PartId;

UPDATE fact_bom bom 
SET bom.ct_ParentTotalNewBuy = 0
WHERE bom.Dim_PartId <> 1
AND ifnull(bom.ct_ParentTotalNewBuy,-1) <> 0;

UPDATE fact_bom bom 
SET bom.ct_ParentTotalNewBuy = 		mp.count_dd_DocumentNo
FROM tmp_ct_ParentTotalNewBuy2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_PartId
 AND bom.Dim_PartId <> 1
 AND ifnull(bom.ct_ParentTotalNewBuy,-1) <> mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_ct_ParentTotalNewBuy;
DROP TABLE IF EXISTS tmp_ct_ParentTotalNewBuy2;



/* Q14 - ct_ParentOpenNewBuy */

DROP TABLE IF EXISTS tmp_ct_ParentOpenNewBuy;
CREATE TABLE tmp_ct_ParentOpenNewBuy 
AS
SELECT DISTINCT p.Dim_PartId,e.EBAN_BANFN
FROM EBAN e
INNER JOIN dim_part p  ON p.PartNumber = e.EBAN_MATNR AND p.Plant = e.EBAN_WERKS
INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant AND pl.RowIsCurrent = 1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_PartId = p.Dim_Partid AND bom.Dim_PartId <> 1 )
AND e.EBAN_MENGE > 0
AND e.EBAN_EBAKZ IS NULL
AND e.EBAN_LOEKZ IS NULL;

DROP TABLE IF EXISTS tmp_ct_ParentOpenNewBuy2;
CREATE TABLE tmp_ct_ParentOpenNewBuy2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_ct_ParentOpenNewBuy
GROUP BY Dim_PartId;

UPDATE fact_bom bom 
SET bom.ct_ParentOpenNewBuy = 0
WHERE bom.Dim_PartId <> 1
AND ifnull(bom.ct_ParentOpenNewBuy,-1) <> 0;

UPDATE fact_bom bom 
SET bom.ct_ParentOpenNewBuy = mp.count_dd_DocumentNo
FROM tmp_ct_ParentOpenNewBuy2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_PartId
 AND bom.Dim_PartId <> 1
 AND ifnull(bom.ct_ParentOpenNewBuy,-1) <> mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_ct_ParentTotalNewBuy;
DROP TABLE IF EXISTS tmp_ct_ParentTotalNewBuy2;

/* Done till here */

/* Q15 - ct_CompTotalNewBuy */

DROP TABLE IF EXISTS tmp_ct_CompTotalNewBuy;
CREATE TABLE tmp_ct_CompTotalNewBuy 
AS
SELECT DISTINCT p.Dim_PartId,e.EBAN_BANFN
FROM EBAN e
INNER JOIN dim_part p  ON p.PartNumber = e.EBAN_MATNR AND p.Plant = e.EBAN_WERKS
INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant AND pl.RowIsCurrent = 1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_BOMComponentId = p.Dim_Partid AND bom.Dim_BOMComponentId <> 1 )
AND e.EBAN_LOEKZ IS NULL;

DROP TABLE IF EXISTS tmp_ct_CompTotalNewBuy2;
CREATE TABLE tmp_ct_CompTotalNewBuy2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_ct_CompTotalNewBuy
GROUP BY Dim_PartId;

UPDATE fact_bom bom 
SET bom.ct_CompTotalNewBuy = 0
WHERE bom.Dim_BOMComponentId <> 1
AND ifnull(bom.ct_CompTotalNewBuy,-1) <> 0;

UPDATE fact_bom bom 
SET bom.ct_CompTotalNewBuy =
		mp.count_dd_DocumentNo
FROM tmp_ct_CompTotalNewBuy2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_BOMComponentId
 AND bom.Dim_BOMComponentId <> 1
 AND ifnull(bom.ct_CompTotalNewBuy,-1) <> mp.count_dd_DocumentNo;

DROP TABLE IF EXISTS tmp_ct_CompTotalNewBuy;
DROP TABLE IF EXISTS tmp_ct_CompTotalNewBuy2;

/* Q16 - ct_CompOpenNewBuy */

DROP TABLE IF EXISTS tmp_ct_CompOpenNewBuy;
CREATE TABLE tmp_ct_CompOpenNewBuy 
AS
SELECT DISTINCT p.Dim_PartId,e.EBAN_BANFN
FROM EBAN e
INNER JOIN dim_part p  ON p.PartNumber = e.EBAN_MATNR AND p.Plant = e.EBAN_WERKS
INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant AND pl.RowIsCurrent = 1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_BOMComponentId = p.Dim_Partid AND bom.Dim_BOMComponentId <> 1 )
AND e.EBAN_MENGE > 0
AND e.EBAN_EBAKZ IS NULL
AND e.EBAN_LOEKZ IS NULL;

DROP TABLE IF EXISTS tmp_ct_CompOpenNewBuy2;
CREATE TABLE tmp_ct_CompOpenNewBuy2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_ct_CompOpenNewBuy
GROUP BY Dim_PartId;

UPDATE fact_bom bom 
SET bom.ct_CompOpenNewBuy = 0
WHERE bom.Dim_BOMComponentId <> 1
and IFNULL(ct_CompOpenNewBuy,-1) <> 0;

UPDATE fact_bom bom 
SET bom.ct_CompOpenNewBuy =
		mp.count_dd_DocumentNo
FROM tmp_ct_CompOpenNewBuy2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_BOMComponentId
 AND bom.Dim_BOMComponentId <> 1
 AND ifnull(bom.ct_CompOpenNewBuy,-1) <> mp.count_dd_DocumentNo; 

DROP TABLE IF EXISTS tmp_ct_ParentTotalNewBuy;
DROP TABLE IF EXISTS tmp_ct_ParentTotalNewBuy2;

/* Q17 - Dim_FixedVendorId */

UPDATE fact_bom b
SET Dim_FixedVendorId = 1;

MERGE INTO fact_bom f
USING (SELECT b.fact_bomid, max(v.dim_VendorId) Dim_FixedVendorId
	FROM eord e,dim_part dp, dim_vendor v, fact_bom b
	WHERE dp.PArtnumber = e.EORD_MATNR
		AND dp.Plant = e.EORD_WERKS
		AND dp.RowIsCurrent = 1
		AND v.VendorNumber = e.EORD_LIFNR
		AND e.EORD_FLIFN = 'X'
		AND v.RowIsCurrent = 1
		AND e.EORD_BDATU >  current_timestamp
		AND dp.Dim_PartId = b.Dim_PartId
		AND b.Dim_FixedVendorId <> v.dim_VendorId
	GROUP BY b.fact_bomid) src
ON f.fact_bomid = src.fact_bomid
WHEN MATCHED THEN UPDATE SET f.Dim_FixedVendorId = src.Dim_FixedVendorId;
				 
/* Q18 - Dim_FixedComponentVendorId */				 

UPDATE fact_bom b
SET Dim_FixedComponentVendorId = 1;

merge into fact_bom dst
using (select bom.fact_bomid, max(v.dim_VendorId) as Dim_FixedComponentVendorId
	   from fact_bom bom
				inner join dim_part dp on dp.Dim_PartId = bom.Dim_BomComponentId
				inner join eord e on    e.EORD_WERKS = dp.Plant
									and e.EORD_MATNR = dp.PArtnumber
				inner join dim_vendor v on v.VendorNumber = e.EORD_LIFNR
	   where    e.EORD_BDATU >  current_timestamp
		    and e.EORD_FLIFN = 'X'
		    and v.RowIsCurrent = 1 
			and dp.RowIsCurrent = 1
	   group by bom.fact_bomid
	  ) src
on dst.fact_bomid = src.fact_bomid
when matched then update set dst.Dim_FixedComponentVendorId = src.Dim_FixedComponentVendorId
where dst.Dim_FixedComponentVendorId <> src.Dim_FixedComponentVendorId;
		/* VW Original
			UPDATE fact_bom b
			SET b.Dim_FixedComponentVendorId = v.dim_VendorId
			FROM eord e,dim_part dp, dim_vendor v, fact_bom b
			WHERE dp.PArtnumber = e.EORD_MATNR
			AND dp.Plant = e.EORD_WERKS
			AND dp.RowIsCurrent = 1
			AND v.VendorNumber = e.EORD_LIFNR
			AND e.EORD_FLIFN = 'X'
			AND v.RowIsCurrent = 1
			AND e.EORD_BDATU >  current_timestamp
			AND dp.Dim_PartId = b.Dim_BomComponentId
			AND b.Dim_FixedComponentVendorId <> v.dim_VendorId */

/* Q19a - amt_componentppv */					 
				 
DROP TABLE IF EXISTS tmp_amt_componentppv;
CREATE TABLE tmp_amt_componentppv 
AS
SELECT p.Dim_PartId,avg(p.amt_DeliveryPPV) avg_amt_DeliveryPPV
FROM fact_purchase p
INNER JOIN dim_date od ON p.Dim_DateidOrder = od.Dim_Dateid
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_BOMComponentId = p.Dim_Partid AND bom.Dim_BOMComponentId <> 1 )
AND od.DateValue >= current_timestamp - interval '3' month
GROUP BY p.Dim_PartId;

UPDATE fact_bom bom 
SET bom.amt_componentppv = 0
WHERE bom.Dim_BOMComponentId <> 1
AND ifnull(bom.amt_componentppv,-1)  <> 0;

UPDATE fact_bom bom 
SET bom.amt_componentppv =
		mp.avg_amt_DeliveryPPV
FROM tmp_amt_componentppv mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_BOMComponentId
 AND bom.Dim_BOMComponentId <> 1
 AND ifnull(bom.amt_componentppv,-1) <> mp.avg_amt_DeliveryPPV;

DROP TABLE IF EXISTS tmp_amt_componentppv;

/* Q19b - amt_ParentPartPPV */ 
 
DROP TABLE IF EXISTS tmp_amt_ParentPartPPV;
CREATE TABLE tmp_amt_ParentPartPPV 
AS
SELECT p.Dim_PartId,avg(p.amt_DeliveryPPV) avg_amt_DeliveryPPV
FROM fact_purchase p
INNER JOIN dim_date od ON p.Dim_DateidOrder = od.Dim_Dateid
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_partId = p.Dim_Partid AND bom.Dim_partId <> 1 )
AND od.DateValue >= current_timestamp - interval '3' month
GROUP BY p.Dim_PartId;

UPDATE fact_bom bom 
SET bom.amt_ParentPartPPV = 0
WHERE bom.Dim_partId <> 1;

UPDATE fact_bom bom 
SET bom.amt_ParentPartPPV =
		mp.avg_amt_DeliveryPPV
FROM tmp_amt_ParentPartPPV mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_partId
 AND bom.Dim_partId <> 1;

DROP TABLE IF EXISTS tmp_amt_ParentPartPPV;


 /* Q20 - ct_validinforecvendor */ 
 
DROP TABLE IF EXISTS tmp_ct_validinforecvendor1;
CREATE TABLE tmp_ct_validinforecvendor1
AS
SELECT distinct i.MATNR,i.LIFNR, e.WERKS
FROM eina i
INNER JOIN eine e ON e.INFNR = i.INFNR AND e.EINE_PRDAT >= current_timestamp;

DROP TABLE IF EXISTS tmp_ct_validinforecvendor;
CREATE TABLE tmp_ct_validinforecvendor
AS SELECT DISTINCT p.Dim_PartId,i.LIFNR
FROM tmp_ct_validinforecvendor1 i
INNER JOIN dim_part p  ON p.PartNumber = i.MATNR AND i.WERKS = p.Plant AND p.RowIsCurrent = 1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_BOMComponentId = p.Dim_Partid AND bom.Dim_BOMComponentId <> 1 );


DROP TABLE IF EXISTS tmp_ct_validinforecvendor2;
CREATE TABLE tmp_ct_validinforecvendor2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_ct_validinforecvendor
GROUP BY Dim_PartId;

UPDATE fact_bom bom 
SET bom.ct_validinforecvendor = 0
WHERE bom.Dim_BOMComponentId <> 1;

UPDATE fact_bom bom 
SET bom.ct_validinforecvendor =
		mp.count_dd_DocumentNo
FROM tmp_ct_validinforecvendor2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_BOMComponentId
 AND bom.Dim_BOMComponentId <> 1;

DROP TABLE IF EXISTS tmp_ct_validinforecvendor;
DROP TABLE IF EXISTS tmp_ct_validinforecvendor2;

  /* Q21 - ct_totalinforecvendor */ 
 
 DROP TABLE IF EXISTS tmp_ct_totalinforecvendor1;
CREATE TABLE tmp_ct_totalinforecvendor1 AS
SELECT DISTINCT e.WERKS, i.MATNR,i.LIFNR
FROM eina i
INNER JOIN eine e ON e.INFNR = i.INFNR;

DROP TABLE IF EXISTS tmp_ct_totalinforecvendor;
CREATE TABLE tmp_ct_totalinforecvendor AS
SELECT DISTINCT p.Dim_PartId,i.LIFNR
FROM tmp_ct_totalinforecvendor1 i
INNER JOIN dim_part p  ON p.PartNumber = i.MATNR AND p.RowIsCurrent = 1
WHERE EXISTS ( SELECT 1 FROM fact_bom bom WHERE bom.Dim_BOMComponentId = p.Dim_Partid AND bom.Dim_BOMComponentId <> 1 );
 
 

DROP TABLE IF EXISTS tmp_ct_totalinforecvendor2;
CREATE TABLE tmp_ct_totalinforecvendor2
AS
SELECT Dim_PartId, count(*) as count_dd_DocumentNo
FROM tmp_ct_totalinforecvendor
GROUP BY Dim_PartId;

UPDATE fact_bom bom 
SET bom.ct_totalinforecvendor = 0
WHERE bom.Dim_BOMComponentId <> 1;

UPDATE fact_bom bom 
SET bom.ct_totalinforecvendor =
		mp.count_dd_DocumentNo
FROM tmp_ct_totalinforecvendor2 mp, fact_bom bom
 WHERE mp.Dim_Partid = bom.Dim_BOMComponentId
 AND bom.Dim_BOMComponentId <> 1;

DROP TABLE IF EXISTS tmp_ct_totalinforecvendor;
DROP TABLE IF EXISTS tmp_ct_totalinforecvendor2;

/* Done till here */

   /* Q22 - ct_ValidSLVendorComp */ 
   
UPDATE fact_bom b
SET  ct_ValidSLVendorComp = 0
WHERE ifnull(ct_ValidSLVendorComp,-1) <> 0;  

UPDATE fact_bom b
SET  ct_TotalSLVendorComp = 0
WHERE ifnull(ct_TotalSLVendorComp,-1) <> 0;  

DROP TABLE IF EXISTS tmp_ct_ValidSLVendorComp;
CREATE TABLE tmp_ct_ValidSLVendorComp
AS
SELECT dp.PartNumber,dp.Plant,count(*) count_part_plant
FROM dim_part dp, eord e
WHERE  e.EORD_MATNR = dp.PartNumber
AND e.eord_werks = dp.Plant
AND e.EORD_BDATU >= current_timestamp
GROUP BY dp.PartNumber,dp.Plant;

DROP TABLE IF EXISTS tmp_ct_TotalSLVendorComp;
CREATE TABLE tmp_ct_TotalSLVendorComp
AS
SELECT dp.PartNumber,dp.Plant,count(*) count_part_plant
FROM dim_part dp, eord e
WHERE  e.EORD_MATNR = dp.PartNumber
AND e.eord_werks = dp.Plant
GROUP By dp.PartNumber,dp.Plant;
   
 
UPDATE    fact_bom b
SET ct_ValidSLVendorComp = e.count_part_plant
FROM dim_part dp,tmp_ct_ValidSLVendorComp e, fact_bom b
WHERE b.Dim_BOMComponentId = dp.Dim_Partid
AND dp.Dim_Partid <> 1 AND dp.RowIsCurrent = 1
AND e.PartNumber = dp.PartNumber
AND e.Plant = dp.Plant
AND ifnull(ct_ValidSLVendorComp,-1) <> e.count_part_plant;

UPDATE    fact_bom b
SET ct_TotalSLVendorComp = e.count_part_plant
FROM dim_part dp,tmp_ct_TotalSLVendorComp e, fact_bom b
WHERE b.Dim_BOMComponentId = dp.Dim_Partid
AND dp.Dim_Partid <> 1 AND dp.RowIsCurrent = 1
AND e.PartNumber = dp.PartNumber
AND e.Plant = dp.Plant
AND ifnull(ct_TotalSLVendorComp,-1) <> e.count_part_plant;


   /* Q23 - ct_ValidSLVendorParent */  
 
UPDATE fact_bom b
SET  ct_ValidSLVendorParent = 0
WHERE ifnull(ct_ValidSLVendorParent,-1) <> 0;  

UPDATE fact_bom b
SET  ct_TotalSLVendorParent = 0
WHERE ifnull(ct_TotalSLVendorParent,-1) <> 0;  

DROP TABLE IF EXISTS tmp_ct_ValidSLVendorParent;
CREATE TABLE tmp_ct_ValidSLVendorParent
AS
SELECT dp.PartNumber,dp.Plant,count(*) count_part_plant
FROM dim_part dp, eord e
WHERE  e.EORD_MATNR = dp.PartNumber
AND e.eord_werks = dp.Plant
AND e.EORD_BDATU >= current_timestamp
GROUP BY dp.PartNumber,dp.Plant;

DROP TABLE IF EXISTS tmp_ct_TotalSLVendorParent;
CREATE TABLE tmp_ct_TotalSLVendorParent
AS
SELECT dp.PartNumber,dp.Plant,count(*) count_part_plant
FROM dim_part dp, eord e
WHERE  e.EORD_MATNR = dp.PartNumber
AND e.eord_werks = dp.Plant
GROUP By dp.PartNumber,dp.Plant;
   
 
UPDATE    fact_bom b
SET ct_ValidSLVendorParent = e.count_part_plant
FROM dim_part dp,tmp_ct_ValidSLVendorParent e, fact_bom b
WHERE b.Dim_partId = dp.Dim_Partid
AND dp.Dim_Partid <> 1 AND dp.RowIsCurrent = 1
AND e.PartNumber = dp.PartNumber
AND e.Plant = dp.Plant
AND ifnull(ct_ValidSLVendorParent,-1) <e.count_part_plant;

UPDATE    fact_bom b
SET ct_TotalSLVendorParent = e.count_part_plant
FROM dim_part dp,tmp_ct_TotalSLVendorParent e, fact_bom b
WHERE b.Dim_partId = dp.Dim_Partid
AND dp.Dim_Partid <> 1 AND dp.RowIsCurrent = 1
AND e.PartNumber = dp.PartNumber
AND e.Plant = dp.Plant
AND ifnull(ct_TotalSLVendorParent,-1) <> e.count_part_plant;

   /* Q24 - amt_avgPOPriceComp */   
   
UPDATE fact_bom b
SET  amt_avgPOPriceComp = 0
WHERE ifnull(amt_avgPOPriceComp,-1) <> 0
AND Dim_BOMComponentId <> 1;     
 
DROP TABLE IF EXISTS TMP_amt_avgPOPriceComp;
CREATE TABLE TMP_amt_avgPOPriceComp
AS
SELECT p.Dim_Partid,avg(p.amt_UnitPrice) as avg_amt_UnitPrice
FROM    fact_purchase p, dim_date od, fact_bom bom
WHERE p.Dim_DateidOrder = od.Dim_Dateid
AND   p.Dim_Partid <> 1 AND p.Dim_Partid = bom.Dim_BOMComponentId
GROUP BY p.Dim_Partid; 

UPDATE TMP_amt_avgPOPriceComp
SET avg_amt_UnitPrice = avg_amt_UnitPrice + 1 
WHERE avg_amt_UnitPrice - floor(avg_amt_UnitPrice) >= 0.5;

UPDATE fact_bom bom
SET bom.amt_avgPOPriceComp = avg_amt_UnitPrice
FROM TMP_amt_avgPOPriceComp p, fact_bom bom
WHERE bom.Dim_BOMComponentId <> 1
AND p.Dim_Partid = bom.Dim_BOMComponentId
AND IFNULL(bom.amt_avgPOPriceComp,-1) <> avg_amt_UnitPrice;

DROP TABLE IF EXISTS TMP_amt_avgPOPriceComp;
 
   /* Q25 : Last query - amt_avgPOPricePart */   
   
UPDATE fact_bom b
SET  amt_avgPOPricePart = 0
WHERE ifnull(amt_avgPOPricePart,-1) <> 0
AND Dim_partId <> 1;        

DROP TABLE IF EXISTS TMP_amt_avgPOPricePart;
CREATE TABLE TMP_amt_avgPOPricePart
AS
SELECT p.Dim_Partid,avg(p.amt_UnitPrice) as avg_amt_UnitPrice
FROM    fact_purchase p, dim_date od, fact_bom bom
WHERE p.Dim_DateidOrder = od.Dim_Dateid
AND   p.Dim_Partid <> 1 AND p.Dim_Partid = bom.Dim_partId
GROUP BY p.Dim_Partid; 

UPDATE TMP_amt_avgPOPricePart
SET avg_amt_UnitPrice = avg_amt_UnitPrice + 1 
WHERE avg_amt_UnitPrice - floor(avg_amt_UnitPrice) >= 0.5;

UPDATE fact_bom bom
SET bom.amt_avgPOPricePart = avg_amt_UnitPrice
FROM TMP_amt_avgPOPricePart p, fact_bom bom
WHERE bom.Dim_partId <> 1
AND p.Dim_Partid = bom.Dim_partId
AND IFNULL(bom.amt_avgPOPricePart,-1) <> avg_amt_UnitPrice;

DROP TABLE IF EXISTS TMP_amt_avgPOPricePart;
 
update fact_bom b 
       set b.amt_componentirprice = netpr
from fact_bom b,
       dim_part p,
       eord e,
       eina irh,
       eine iri
 WHERE     p.Dim_Partid = b.Dim_BOMComponentId
       AND e.eord_matnr = p.partnumber
       AND e.eord_werks = p.plant
       AND eord_flifn = 'X'
       AND irh.matnr = eord_matnr
       AND irh.lifnr = eord_lifnr
       AND irh.infnr = iri.infnr
       AND iri.werks = eord_werks
       AND eord_bdatu >= current_date
       AND eine_prdat >= current_date
       and b.amt_componentirprice <> netpr;

update fact_bom b 
       set b.amt_parentpartirprice = netpr
from fact_bom b,
       dim_part p,
       eord e,
       eina irh,
       eine iri
 WHERE     p.Dim_Partid = b.Dim_PartId
       AND e.eord_matnr = p.partnumber
       AND e.eord_werks = p.plant
       AND eord_flifn = 'X'
       AND irh.matnr = eord_matnr
       AND irh.lifnr = eord_lifnr
       AND irh.infnr = iri.infnr
       AND iri.werks = eord_werks
       AND eord_bdatu >= current_date
       AND eine_prdat >= current_date
       and b.amt_parentpartirprice <> netpr;
