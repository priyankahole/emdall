/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_PurchaseHistory_fact.sql */
/*   Author         : Mohamed */
/*   Created On     : 29 Oct 2013 */
/* */
/* */
/*   Description    : Script for Purchase History fact*/
/* */
/*   Change History */
/* ####################################################################################################################   */

Drop table if exists pGlobalCurrency_ph_43;

Create table pGlobalCurrency_ph_43(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_ph_43(pGlobalCurrency) values(null);

Update pGlobalCurrency_ph_43
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD')
from pGlobalCurrency_ph_43;

Drop table if exists fact_purchasehistory_tmp_43;


Create table fact_purchasehistory_tmp_43 as
SELECT dd_PurchasingDocumentNo,dd_ItemPurchasingDocumentNo,dd_MaterialDocumentYear,dd_SequentialAccountAssignmentNo,dim_POTransactionTypeid,dd_MaterialDocumentNo,dd_MaterialDocumentItem
FROM fact_purchasehistory ;
	
Drop table if exists max_holder_43;
Create table max_holder_43 as
Select ifnull(max(fact_purchasehistoryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_purchasehistory;

drop table if exists tmp_fph_t001t;
create table tmp_fph_t001t as
SELECT max_holder_43.maxid + row_number() over(order by '') fact_purchasehistoryid,
        ifnull(EKBE_BELNR, 'Not Set') dd_MaterialDocumentNo ,				   
        convert(bigint, 1) dim_DateidPosting,			
        ifnull(EKBE_BUZEI, 0) dd_MaterialDocumentItem ,
        ifnull(EKBE_DMBTR, 0.000) amt_AmountLocalCurrency ,
        ifnull(EKBE_MENGE, 0.000) ct_Quantity ,
        convert(bigint, 1) dim_currencyid,
        ifnull(EKBE_WRBTR, 0) amt_AmountDocumentCurrency ,		 
        ifnull(EKBE_XBLNR, 'Not Set') dd_ReferenceDocumentNo ,
        1.0000 amt_ExchangeRate,
        1.0000 amt_ExchangeRate_GBL,
        convert(bigint, 1) dim_Currencyid_TRA,
        convert(bigint, 1) dim_Currencyid_GBL,	
        ifnull(EKBE_EBELN, 'Not Set') dd_PurchasingDocumentNo,
        ifnull(EKBE_EBELP, 0) dd_ItemPurchasingDocumentNo,
        ifnull(EKBE_GJAHR, 0) dd_MaterialDocumentYear,
        convert(bigint, 1) dim_POTransactionTypeid,
        ifnull(EKBE_ZEKKN, 0) dd_SequentialAccountAssignmentNo,
        convert(bigint, 1) Dim_POHistoryCategoryId,
        ifnull(EKBE_J_3AEBSP, 0) dd_afsScheduleLine,
        EKBE_VGABE --dim_POTransactionTypeid
      FROM  max_holder_43,EKBE e,pGlobalCurrency_ph_43
    WHERE NOT EXISTS
                (SELECT 1
                    FROM 	fact_purchasehistory_tmp_43 ph,
							dim_POTransactionType pot
                   WHERE 	ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
							AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
							AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
							AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
							AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
							AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
							AND ph.dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
						    AND ph.dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0));

	/* dim_POTransactionTypeid */
	update tmp_fph_t001t f
	set f.dim_POTransactionTypeid = ifnull(pott.dim_POTransactionTypeid, 1)
	from tmp_fph_t001t f
			left join (select * from dim_POTransactionType where rowiscurrent = 1
) pott on pott.POTransactionTypeCode = f.EKBE_VGABE
	where f.dim_POTransactionTypeid <> ifnull(pott.dim_POTransactionTypeid, 1);

    INSERT INTO fact_purchasehistory(fact_purchasehistoryid,
										dd_MaterialDocumentNo,
										dim_DateidPosting,
										dd_MaterialDocumentItem,
										amt_AmountLocalCurrency,
										ct_Quantity,
										dim_currencyid,
										amt_AmountDocumentCurrency,
										dd_ReferenceDocumentNo,
										amt_ExchangeRate,
										amt_ExchangeRate_GBL,
										dim_Currencyid_TRA,
										dim_Currencyid_GBL,
										dd_PurchasingDocumentNo,
										dd_ItemPurchasingDocumentNo,
										dd_MaterialDocumentYear,
										dim_POTransactionTypeid,
										dd_SequentialAccountAssignmentNo,
										Dim_POHistoryCategoryId,
										dd_afsScheduleLine)
      select fact_purchasehistoryid,
										dd_MaterialDocumentNo,
										dim_DateidPosting,
										dd_MaterialDocumentItem,
										amt_AmountLocalCurrency,
										ct_Quantity,
										dim_currencyid,
										amt_AmountDocumentCurrency,
										dd_ReferenceDocumentNo,
										amt_ExchangeRate,
										amt_ExchangeRate_GBL,
										dim_Currencyid_TRA,
										dim_Currencyid_GBL,
										dd_PurchasingDocumentNo,
										dd_ItemPurchasingDocumentNo,
										dd_MaterialDocumentYear,
										dim_POTransactionTypeid,
										dd_SequentialAccountAssignmentNo,
										Dim_POHistoryCategoryId,
										dd_afsScheduleLine
                        from tmp_fph_t001t;

drop table if exists tmp_fph_t001t;
						 
						 

Drop table if exists fact_purchasehistory_tmp_43;
Drop table if exists max_holder_43;


UPDATE 	fact_purchasehistory ph
SET 	ph.Dim_POHistoryCategoryId = poh.dim_podochistorycategoryid
        FROM  	EKBE e,
		dim_podochistorycategory poh,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE   e.EKBE_BEWTP = poh.podochistorycategory
		AND ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND ph.Dim_POHistoryCategoryId <> poh.dim_podochistorycategoryid; 

UPDATE 	fact_purchasehistory ph
SET 	ph.dim_DateidPosting = dp.dim_dateid
From 	EKBE e,
		dim_date dp,
		dim_plant pl,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE 	ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND pl.plantcode = e.EKBE_WERKS
		AND dp.DateValue = EKBE_BUDAT AND pl.companycode = dp.CompanyCode
		AND ph.dim_DateidPosting <> dp.dim_dateid;

	

UPDATE 	fact_purchasehistory ph
SET 	amt_AmountLocalCurrency = EKBE_DMBTR
FROM  	EKBE e,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE   ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND amt_AmountLocalCurrency <> EKBE_DMBTR;
	
	
UPDATE 	fact_purchasehistory ph
SET	 	ct_Quantity = EKBE_MENGE
FROM  	EKBE e,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE   ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND ct_Quantity <> EKBE_MENGE;	
	
  
UPDATE 	fact_purchasehistory ph
SET 	ph.dim_currencyid = cur.Dim_Currencyid
From 	EKBE e,
		dim_POTransactionType pot,
		Dim_Currency cur,
        fact_purchasehistory ph
WHERE   ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)	
		AND cur.CurrencyCode = e.EKBE_WAERS
		AND ph.dim_currencyid <> cur.Dim_Currencyid;
  

UPDATE 	fact_purchasehistory ph
SET 	amt_AmountDocumentCurrency = EKBE_WRBTR
        FROM  	EKBE e,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE   ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND amt_AmountDocumentCurrency <> EKBE_WRBTR;
	

UPDATE 	fact_purchasehistory ph
SET 	dd_ReferenceDocumentNo = EKBE_XBLNR
FROM  	EKBE e,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE   ph.dd_PurchasingDocumentNo = e.EKBE_EBELN
		AND ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND dd_ReferenceDocumentNo <> EKBE_XBLNR;		 	
  
  
  
/* UPDATE COLONNES amt_ExchangeRate amt_ExchangeRate_GBL dim_Currencyid_TRA dim_Currencyid_GBL*/
  
  
UPDATE 	fact_purchasehistory ph
SET 	amt_ExchangeRate = 1
FROM 	EKBE e,
		dim_company dc,
		dim_plant pl,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE   ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND pl.plantcode = e.EKBE_WERKS
		AND pl.companycode = dc.CompanyCode
		AND ifnull(amt_ExchangeRate,-1) <> 1; 

		
UPDATE fact_purchasehistory ph
SET amt_ExchangeRate = ex.exchangeRate ,
dim_Currencyid_TRA = dcr.Dim_Currencyid
FROM dim_company dc,
	EKBE e,
	tmp_getExchangeRate1 ex,
	dim_plant pl,
	dim_POTransactionType pot,
	dim_currency dcr,
    fact_purchasehistory ph
WHERE ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')
AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
AND pl.plantcode = e.EKBE_WERKS
AND pl.companycode = dc.CompanyCode
AND pFromCurrency = EKBE_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = EKBE_BUDAT
and fact_script_name = 'bi_populate_prodorder_fact'
AND dcr.currencycode = dc.Currency
AND amt_ExchangeRate <> ex.exchangeRate;


UPDATE 	fact_purchasehistory ph
SET 	amt_ExchangeRate_GBL = 1
FROM 	EKBE e,
		dim_company dc,
		dim_plant pl,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE   ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND pl.plantcode = e.EKBE_WERKS
		AND pl.companycode = dc.CompanyCode
		AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;  


UPDATE 	fact_purchasehistory ph
SET 	amt_ExchangeRate_GBL = ex.exchangeRate ,
		dim_Currencyid_GBL = pGlobalCurrency
FROM 	dim_company dc,
		EKBE e,
		tmp_getExchangeRate1 ex,
		pGlobalCurrency_ph_43,
		dim_plant pl,
		dim_POTransactionType pot,
        fact_purchasehistory ph
WHERE 	ph.dd_PurchasingDocumentNo = ifnull(EKBE_EBELN, 'Not Set')
		AND ph.dd_ItemPurchasingDocumentNo = ifnull(EKBE_EBELP, 0)
		AND ph.dd_MaterialDocumentYear = ifnull(EKBE_GJAHR, 0)
		AND ph.dd_SequentialAccountAssignmentNo = ifnull(EKBE_ZEKKN, 0)
		AND ph.dim_POTransactionTypeid = pot.dim_POTransactionTypeid
		AND pot.POTransactionTypeCode = ifnull(e.EKBE_VGABE, 'Not Set')
		AND dd_MaterialDocumentNo = ifnull(EKBE_BELNR, 'Not Set')				   
		AND dd_MaterialDocumentItem = ifnull(EKBE_BUZEI, 0)
		AND pl.plantcode = e.EKBE_WERKS
		AND pl.companycode = dc.CompanyCode
		AND pFromCurrency = EKBE_WAERS
		and pToCurrency = pGlobalCurrency
		and pFromExchangeRate = 0
		and pDate =  current_date
		and fact_script_name = 'bi_populate_PurchaseHistory_fact'
		AND amt_ExchangeRate <> ex.exchangeRate ;
