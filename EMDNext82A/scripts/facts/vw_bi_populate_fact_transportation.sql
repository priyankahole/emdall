/******************************************************************************************************************/
/*   Script         : bi_populate_TransportationAnalysis_fact                                                                      */
/*   Author         : Roxana D.                                                                                    */
/*   Created On     : 02 October 2017                                                                                 */
/*   Description    : Populating script of Fact_TransportationAnalysis                                                            */
/* ********************************************Change History****************************************************** */
/*   Date                By             Version      Desc                                                         */
/*   02 October 2017     Roxana D.       1.0          Creating the script.                                         */
/*   																	                                       */
/* **************************************************************************************************************** */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_transportation';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_transportation', IFNULL(MAX(fact_TRANSPORTATIONID),0)
FROM fact_TRANSPORTATION;

TRUNCATE TABLE tmp_fact_transportation;
INSERT INTO tmp_fact_TRANSPORTATION( 
    FACT_TRANSPORTATIONID ,
	DD_ID ,
	DD_PackageName ,
	DD_TimeStamp ,
	DIM_DATEID_TimeStamp ,
	DD_File_ID ,
	CT_Rec_No ,
	DD_ForwarderName ,
	DD_ForwarderOffice ,
	DD_ShippersRef ,
	DD_ForwarderRef ,
	DD_DOT,
	DIM_DATEID_DOT ,
	DD_ETD ,
	DIM_DATEID_ETD ,
	DD_ATD ,
	DIM_DATEID_ATD ,
	DD_ETA ,
	DIM_DATEID_ETA,
	DD_ATA ,
	DIM_DATEID_ATA ,
	DD_DHO ,
	DIM_DATEID_DHO ,
	DD_DLV ,
	DIM_DATEID_DLV ,
	DD_OriginShipperName ,
	DD_OriginShipperCity ,
	DD_OriginShipperCountry ,
	DD_OriginLoadingPoint ,
	DD_OriginZipCodeLoadingPoint,
	DD_OriginPort ,
	DD_DestPort,
	DD_DestConsigneeName ,
	DD_DestConsigneeCity,
	DD_DestConsigneeCountry , 
 	DD_ServiceType ,
	DD_DestUnloadingPoint  ,
	DD_DestZipCodeUnloadingPoint ,
	DD_Incoterm1 ,
	DD_Incoterm2 ,
	DD_PaidBy ,
	DD_BusinessUnit ,
	DD_BusinessType,
	DD_ModeOfTransport ,
	DD_DangerousGoodsInShipment ,
	DD_DryIceInShipment,
	DD_Carrier,
	DD_Flight1_Vessel ,
	DD_Flight2_Voyage ,
	DD_HAWB_HBL_Number ,
	DD_MAWB_MBL_Number ,
	CT_NoFCL20 ,
	CT_NoFCL40 ,
	DD_ContainerType,
	DD_ContainerNumber ,
	CT_NoOfPackages ,
	CT_GrossWeight ,
	DD_GrossWeightUnit ,
	CT_ChargeableWeight ,
	DD_ChargeableWeightUnit ,
	CT_Volume ,
	DD_VolumeUnit ,
	CT_FreightCost,
	DD_CurrFreightCost , 
	CT_FuelBafSurcharge ,
	DD_CurrFuelBafSurcharge ,
	CT_SecuritySurcharge ,
	DD_CurrSecuritySurcharge ,
	DD_CalculationBase ,
	CT_IMODGRSurcharge ,
	DD_CurrIMODGRSurcharge,
	CT_CAF ,
	DD_CurrCaf ,
	CT_CustomsCharges ,
	DD_CurrCustomsCharges ,
	CT_PreOncarriageCost ,
	DD_CurrPreOncarriageCost ,
	CT_ReIcingCost ,
	DD_CurrReIcingCost ,
	CT_OtherCost ,
	DD_CurrOtherCost,
	CT_TotalFreightCost,
	DD_CurrTotalFreightCost,
	LANEID)
SELECT 
(select ifnull(max_id,1) from number_fountain where table_name = 'tmp_fact_transportation') + row_number() over(order by '') AS FACT_TRANSPORTATIONID ,
	IFNULL(ID,'Not Set') ,
	IFNULL(PackageName,'Not Set') ,
	IFNULL(TimeStamp1,'0001-01-01') ,
	1 AS DIM_DATEID_TimeStamp ,
	IFNULL(File_ID,'Not Set') ,
	IFNULL(Rec_No,0) ,
	IFNULL(ForwarderName,'Not Set') ,
	IFNULL(ForwarderOffice,'Not Set') ,
	IFNULL(ShippersRef,'Not Set') ,
	IFNULL(ForwarderRef,'Not Set') ,
	IFNULL(DOT,'0001-01-01'),
	1 AS DIM_DATEID_DOT ,
	IFNULL(ETD,'0001-01-01') ,
	1 AS DIM_DATEID_ETD ,
	IFNULL(ATD,'0001-01-01') ,
	1 AS DIM_DATEID_ATD ,
	IFNULL(ETA,'0001-01-01') ,
	1 AS DIM_DATEID_ETA,
	IFNULL(ATA,'0001-01-01') ,
	1 AS DIM_DATEID_ATA ,
	IFNULL(DHO,'0001-01-01') ,
	1 AS DIM_DATEID_DHO ,
	IFNULL(DLV,'0001-01-01') ,
	1 AS DIM_DATEID_DLV ,
	IFNULL(OriginShipperName,'Not Set') ,
	IFNULL(OriginShipperCity,'Not Set') ,
	IFNULL(OriginShipperCountry,'Not Set') ,
	IFNULL(OriginLoadingPoint,'Not Set') ,
	IFNULL(OriginZipCodeLoadingPoint,'Not Set'),
	IFNULL(OriginPort,'Not Set') ,
	IFNULL(DestPort,'Not Set'),
	IFNULL(DestConsigneeName,'Not Set') ,
	IFNULL(DestConsigneeCity,'Not Set'),
	IFNULL(DestConsigneeCountry ,'Not Set'),  
	IFNULL(ServiceType ,'Not Set'),
	IFNULL(DestUnloadingPoint,'Not Set')  ,
	IFNULL(DestZipCodeUnloadingPoint,'Not Set') ,
	IFNULL(Incoterm1,'Not Set') ,
	IFNULL(Incoterm2,'Not Set') ,
	IFNULL(PaidBy,'Not Set') ,
	IFNULL(BusinessUnit,'Not Set') ,
	IFNULL(BusinessType,'Not Set'),
	IFNULL(upper(ModeOfTransport),'Not Set') ,
	IFNULL(DangerousGoodsInShipment,'Not Set') ,
	IFNULL(DryIceInShipment,'Not Set'),
	IFNULL(Carrier,'Not Set'),
	IFNULL(Flight1_Vessel,'Not Set') ,
	IFNULL(Flight2_Voyage,'Not Set') ,
	IFNULL(HAWB_HBL_Number,'Not Set') ,
	IFNULL(MAWB_MBL_Number,'Not Set') ,
	IFNULL(NoFCL20,0) ,
	IFNULL(NoFCL40,0) ,
	IFNULL(ContainerType,'Not Set'),
	IFNULL(ContainerNumber ,'Not Set'),
	IFNULL(NoOfPackages,0) ,
	IFNULL(GrossWeight,0) ,
	IFNULL(GrossWeightUnit,'Not Set') ,
	IFNULL(ChargeableWeight,0) ,
	IFNULL(ChargeableWeightUnit ,'Not Set'),
	IFNULL(Volume ,0),
	IFNULL(VolumeUnit,0) ,
	IFNULL(FreightCost,0),
	IFNULL(CurrFreightCost,'Not Set') , 
	IFNULL(FuelBafSurcharge,0) ,
	IFNULL(CurrFuelBafSurcharge,'Not Set') ,
	IFNULL(SecuritySurcharge,0) ,
	IFNULL(CurrSecuritySurcharge,'Not Set') ,
	IFNULL(CalculationBase,'Not Set') ,
	IFNULL(IMODGRSurcharge,0) ,
	IFNULL(CurrIMODGRSurcharge,'Not Set'),
	IFNULL(CAF,0) ,
	IFNULL(CurrCaf ,'Not Set'),
	IFNULL(CustomsCharges,0) ,
	IFNULL(CurrCustomsCharges,'Not Set') ,
	IFNULL(PreOncarriageCost,0) ,
	IFNULL(CurrPreOncarriageCost,'Not Set') ,
	IFNULL(ReIcingCost,0) ,
	IFNULL(CurrReIcingCost,'Not Set') ,
	IFNULL(OtherCost,0) ,
	IFNULL(CurrOtherCost,'Not Set'),
	IFNULL(TotalFreightCost,0),
	IFNULL(CurrTotalFreightCost ,'Not Set'),
    IFNULL(LANEID ,'Not Set')	 
FROM EMDNEXT82A.SHIPMENTDATA_CSV;


UPDATE tmp_fact_transportation F
SET F.DIM_DATEID_TimeStamp  = D.DIM_DATEID
FROM  tmp_fact_transportation F, DIM_DATE D 
WHERE DD_TimeStamp = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
--AND F.DIM_DATEID_TimeStamp  <> D.DIM_DATEID
;


UPDATE tmp_fact_transportation F
SET F.DIM_DATEID_DOT  = D.DIM_DATEID
FROM  tmp_fact_transportation F, DIM_DATE D 
WHERE DD_DOT = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.DIM_DATEID_DOT  <> D.DIM_DATEID;

UPDATE tmp_fact_transportation F
SET F.DIM_DATEID_ETD  = D.DIM_DATEID
FROM  tmp_fact_transportation F, DIM_DATE D 
WHERE DD_ETD = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.DIM_DATEID_ETD  <> D.DIM_DATEID;


UPDATE tmp_fact_transportation F
SET F.DIM_DATEID_ATD  = D.DIM_DATEID
FROM  tmp_fact_transportation F, DIM_DATE D 
WHERE DD_ATD = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.DIM_DATEID_ATD  <> D.DIM_DATEID;


UPDATE tmp_fact_transportation F
SET F.DIM_DATEID_ETA  = D.DIM_DATEID
FROM  tmp_fact_transportation F, DIM_DATE D 
WHERE DD_ETA = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.DIM_DATEID_ETA  <> D.DIM_DATEID;


UPDATE tmp_fact_transportation F
SET F.DIM_DATEID_ATA  = D.DIM_DATEID
FROM  tmp_fact_transportation F, DIM_DATE D 
WHERE DD_ATA = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.DIM_DATEID_ATA  <> D.DIM_DATEID;


UPDATE tmp_fact_transportation F
SET F.DIM_DATEID_DHO  = D.DIM_DATEID
FROM  tmp_fact_transportation F, DIM_DATE D 
WHERE DD_DHO = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.DIM_DATEID_DHO  <> D.DIM_DATEID;

UPDATE tmp_fact_transportation F
SET F.DIM_DATEID_DLV  = D.DIM_DATEID
FROM  tmp_fact_transportation F, DIM_DATE D 
WHERE DD_DLV = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.DIM_DATEID_DLV  <> D.DIM_DATEID;


update tmp_fact_transportation F set 
f.DD_MONTHOFYEAR = concat(right(right(f.DD_FILE_ID,7),2),'-',left(right(f.DD_FILE_ID,7),4))
from tmp_fact_transportation f;


INSERT INTO fact_transportation(
 	FACT_TRANSPORTATIONID ,
	DD_ID ,
	DD_PackageName ,
	DIM_DATEID_TimeStamp ,
	DD_File_ID ,
	CT_Rec_No ,
	DD_ForwarderName ,
	DD_ForwarderOffice ,
	DD_ShippersRef ,
	DD_ForwarderRef ,
	DIM_DATEID_DOT ,
	DIM_DATEID_ETD ,
	DIM_DATEID_ATD ,
	DIM_DATEID_ETA,
	DIM_DATEID_ATA ,
	DIM_DATEID_DHO ,
	DIM_DATEID_DLV ,
	DD_OriginShipperName ,
	DD_OriginShipperCity ,
	DD_OriginShipperCountry ,
	DD_OriginLoadingPoint ,
	DD_OriginZipCodeLoadingPoint,
	DD_OriginPort ,
	DD_DestPort,
	DD_DestConsigneeName ,
	DD_DestConsigneeCity,
	DD_DestConsigneeCountry ,  
	DD_DestUnloadingPoint  ,
	DD_DestZipCodeUnloadingPoint ,
	DD_Incoterm1 ,
	DD_Incoterm2 ,
	DD_PaidBy ,
	DD_BusinessUnit ,
	DD_BusinessType,
	DD_ModeOfTransport ,
	DD_DangerousGoodsInShipment ,
	DD_DryIceInShipment,
	DD_Carrier,
	DD_Flight1_Vessel ,
	DD_Flight2_Voyage ,
	DD_ServiceType ,
	DD_HAWB_HBL_Number ,
	DD_MAWB_MBL_Number ,
	CT_NoFCL20 ,
	CT_NoFCL40 ,
	DD_ContainerType,
	DD_ContainerNumber ,
	CT_NoOfPackages ,
	CT_GrossWeight ,
	DD_GrossWeightUnit ,
	CT_ChargeableWeight ,
	DD_ChargeableWeightUnit ,
	CT_Volume ,
	DD_VolumeUnit ,
	CT_FreightCost,
	DD_CurrFreightCost , 
	CT_FuelBafSurcharge ,
	DD_CurrFuelBafSurcharge ,
	CT_SecuritySurcharge ,
	DD_CurrSecuritySurcharge ,
	DD_CalculationBase ,
	CT_IMODGRSurcharge ,
	DD_CurrIMODGRSurcharge,
	CT_CAF ,
	DD_CurrCaf ,
	CT_CustomsCharges ,
	DD_CurrCustomsCharges ,
	CT_PreOncarriageCost ,
	DD_CurrPreOncarriageCost ,
	CT_ReIcingCost ,
	DD_CurrReIcingCost ,
	CT_OtherCost ,
	DD_CurrOtherCost,
	CT_TotalFreightCost,
	DD_CurrTotalFreightCost,
	DD_MONTHOFYEAR,
	LANEID )
SELECT  FACT_TRANSPORTATIONID ,
	DD_ID ,
	DD_PackageName ,
	DIM_DATEID_TimeStamp ,
	DD_File_ID ,
	CT_Rec_No ,
	DD_ForwarderName ,
	DD_ForwarderOffice ,
	DD_ShippersRef ,
	DD_ForwarderRef ,
	DIM_DATEID_DOT ,
	DIM_DATEID_ETD ,
	DIM_DATEID_ATD ,
	DIM_DATEID_ETA,
	DIM_DATEID_ATA ,
	DIM_DATEID_DHO ,
	DIM_DATEID_DLV ,
	DD_OriginShipperName ,
	DD_OriginShipperCity ,
	DD_OriginShipperCountry ,
	DD_OriginLoadingPoint ,
	DD_OriginZipCodeLoadingPoint,
	DD_OriginPort ,
	DD_DestPort,
	DD_DestConsigneeName ,
	DD_DestConsigneeCity,
	DD_DestConsigneeCountry ,  
	DD_DestUnloadingPoint  ,
	DD_DestZipCodeUnloadingPoint ,
	DD_Incoterm1 ,
	DD_Incoterm2 ,
	DD_PaidBy ,
	DD_BusinessUnit ,
	DD_BusinessType,
	DD_ModeOfTransport ,
	DD_DangerousGoodsInShipment ,
	DD_DryIceInShipment,
	DD_Carrier,
	DD_Flight1_Vessel ,
	DD_Flight2_Voyage ,
	DD_ServiceType ,
	DD_HAWB_HBL_Number ,
	DD_MAWB_MBL_Number ,
	CT_NoFCL20 ,
	CT_NoFCL40 ,
	DD_ContainerType,
	DD_ContainerNumber ,
	CT_NoOfPackages ,
	CT_GrossWeight ,
	DD_GrossWeightUnit ,
	CT_ChargeableWeight ,
	DD_ChargeableWeightUnit ,
	CT_Volume ,
	DD_VolumeUnit ,
	CT_FreightCost,
	DD_CurrFreightCost , 
	CT_FuelBafSurcharge ,
	DD_CurrFuelBafSurcharge ,
	CT_SecuritySurcharge ,
	DD_CurrSecuritySurcharge ,
	DD_CalculationBase ,
	CT_IMODGRSurcharge ,
	DD_CurrIMODGRSurcharge,
	CT_CAF ,
	DD_CurrCaf ,
	CT_CustomsCharges ,
	DD_CurrCustomsCharges ,
	CT_PreOncarriageCost ,
	DD_CurrPreOncarriageCost ,
	CT_ReIcingCost ,
	DD_CurrReIcingCost ,
	CT_OtherCost ,
	DD_CurrOtherCost,
	CT_TotalFreightCost,
	DD_CurrTotalFreightCost,
	DD_MONTHOFYEAR,
	LANEID
FROM tmp_fact_transportation;



update fact_transportation t set t.DIM_DATEID_FIELID = dt.dim_dateid
from fact_transportation t 
inner join dim_date dt on dt.datevalue = to_date(concat('01-',t.DD_MONTHOFYEAR),'dd-MM-yyyy') and dt.CompanyCode  = 'Not Set'
where ifnull(t.DIM_DATEID_FIELID,1) <>  dt.dim_dateid;

