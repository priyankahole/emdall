/**********************************************************************************/
/*  28 Aug 2013   1.56	      Shanthi    New Field WIP Qty			  */
/*  15 Oct 2013   1.81        Issam 	 Added material movement measures		  */
/*  21  Oct 2013  1.82        Shanthi New Fields for prod orders  */
/*  20 Dec 2013   1.82        Issam 	 Update optimization					  */
/*  9 Nov 2016                Cornelia   Add dd_batch_status_qkz                  */
/**********************************************************************************/


drop table if exists fact_inventoryhistory_delete;
create table fact_inventoryhistory_delete as
select fact_inventoryhistoryid,SnapshotDate from fact_inventoryhistory
where SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end;

MERGE INTO fact_inventoryhistory t
USING
( select fact_inventoryhistoryid
  from fact_inventoryhistory_delete ) del
ON (t.fact_inventoryhistoryid = del.fact_inventoryhistoryid)
WHEN MATCHED THEN DELETE;

/* Moved to the end in order to use the snpashot date  -  Roxana D 2017-11-06
drop table if exists fact_inventoryhistory_delete
*/

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_inventoryhistory';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_inventoryhistory', ifnull(max(fact_inventoryhistoryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM fact_inventoryhistory;

INSERT INTO fact_inventoryhistory(ct_POOpenQty, /* Issam change 24th Jun */
                                  ct_SOOpenQty, /* Issam change 24th Jun */
                                  amt_BlockedStockAmt,
                                  amt_BlockedStockAmt_GBL,
                                  amt_CommericalPrice1,
                                  amt_CurPlannedPrice,
                                  amt_MovingAvgPrice,
                                  amt_MtlDlvrCost,
                                  amt_OtherCost,
                                  amt_OverheadCost,
                                  amt_PlannedPrice1,
                                  amt_PreviousPrice,
                                  amt_PrevPlannedPrice,
                                  amt_StdUnitPrice,
                                  amt_StdUnitPrice_GBL,
                                  amt_StockInQInspAmt,
                                  amt_StockInQInspAmt_GBL,
                                  amt_StockInTransferAmt,
                                  amt_StockInTransferAmt_GBL,
                                  amt_StockInTransitAmt,
                                  amt_StockInTransitAmt_GBL,
                                  amt_StockValueAmt,
                                  amt_StockValueAmt_GBL,
                                  amt_UnrestrictedConsgnStockAmt,
                                  amt_UnrestrictedConsgnStockAmt_GBL,
                                  amt_WIPBalance,
				  				  ct_WIPQty,
                                  ct_BlockedConsgnStock,
                                  ct_BlockedStock,
                                  ct_BlockedStockReturns,
                                  ct_ConsgnStockInQInsp,
                                  ct_LastReceivedQty,
                                  ct_RestrictedConsgnStock,
                                  ct_StockInQInsp,
                                  ct_StockInTransfer,
                                  ct_StockInTransit,
                                  ct_StockQty,
                                  ct_TotalRestrictedStock,
                                  ct_UnrestrictedConsgnStock,
                                  ct_WIPAging,
                                  dd_BatchNo,
                                  dd_DocumentItemNo,
                                  dd_DocumentNo,
                                  dd_MovementType,
                                  dd_ValuationType,
                                  dim_Companyid,
                                  Dim_ConsumptionTypeid,
                                  dim_costcenterid,
                                  dim_Currencyid,
                                  Dim_DateIdLastChangedPrice,
                                  Dim_DateIdPlannedPrice2,
                                  Dim_DocumentStatusid,
                                  Dim_DocumentTypeid,
                                  Dim_IncoTermid,
                                  Dim_ItemCategoryid,
                                  Dim_ItemStatusid,
                                  dim_LastReceivedDateid,
                                  Dim_MovementIndicatorid,
                                  dim_Partid,
                                  dim_Plantid,
				  				  Dim_ProfitCenterId,
                                  dim_producthierarchyid,
                                  Dim_PurchaseGroupid,
                                  Dim_PurchaseMiscid,
                                  Dim_PurchaseOrgid,
                                  dim_specialstockid,
                                  dim_stockcategoryid,
                                  dim_StockTypeid,
                                  dim_StorageLocationid,
                                  dim_StorageLocEntryDateid,
                                  Dim_SupplyingPlantId,
                                  Dim_Termid,
                                  Dim_UnitOfMeasureid,
                                  dim_Vendorid,
                                  SnapshotDate,
                                  Dim_DateidSnapshot,
                                  fact_inventoryhistoryid,
				  				  amt_ExchangeRate_GBL,
				  				  amt_ExchangeRate,
				 				  dim_Currencyid_TRA,
								  dim_Currencyid_GBL,
/* Begin 15 Oct 2013 changes */
				  				  ct_GRQty_Late30,
								  ct_GRQty_31_60,
								  ct_GRQty_61_90,
								  ct_GRQty_91_180,
								  ct_GIQty_Late30,
								  ct_GIQty_31_60,
								  ct_GIQty_61_90,
								  ct_GIQty_91_180,
/* End 15 Oct 2013 changes */
								  dd_prodordernumber,
								  dd_prodorderitemno,
								  dim_productionorderstatusid,
								  dim_productionordertypeid,
								  dim_partsalesid,
								  dim_bwproducthierarchyid,
								  dim_mdg_partid,
								  dd_invcovflag_emd,
								  ct_avgfcst_emd,
								  amt_cogsactualrate_emd,
								  amt_cogsfixedrate_emd,
								  amt_cogsfixedplanrate_emd,
								  amt_cogsplanrate_emd,
								  amt_cogsprevyearfixedrate_emd,
								  amt_cogsprevyearrate_emd,
								  amt_cogsprevyearto1_emd,
								  amt_cogsprevyearto2_emd,
								  amt_cogsprevyearto3_emd,
								  amt_cogsturnoverrate1_emd,
								  amt_cogsturnoverrate2_emd,
								  amt_cogsturnoverrate3_emd,
								  dim_bwhierarchycountryid,
								  dim_clusterid,
								  ct_countmaterialsafetystock,
								  dd_batch_status_qkz,
								  ct_baseuomratioKG,
								  ct_intransitkg,
								  ct_dependentdemandkg,
								  dim_countryhierpsid,
								  dim_countryhierarid,
								  amt_OnHand,
								  CT_ORDERITEMQTY,
								  CT_GRQTY,
								  dim_dateidexpirydate,
								  dim_batchstatusid,
                  amt_restricted,
	/* Liviu Ionescu - APP-9146 - Add in transit stock qty */
	ct_stockintransferplant,
	amt_stockintransferamtplant,
	amt_stockintransferamtplant_gbl,
	ct_StockInTransitERPs,
	amt_StockInTransitERPs_SPrice,
		dd_primary_production_location,
	dd_primary_production_location_name,
	dd_mtomts,
	amt_StdUnitPrice_fxd
				  )
   SELECT ifnull(ct_POOpenQty, 0),/* Issam change 24th Jun */
          ifnull(ct_SOOpenQty, 0), /* Issam change 24th Jun */
          ifnull(amt_BlockedStockAmt,0) amt_BlockedStockAmt,
          ifnull(amt_BlockedStockAmt_GBL,0) amt_BlockedStockAmt_GBL,
          ifnull(amt_CommericalPrice1,0) amt_CommericalPrice1,
          ifnull(amt_CurPlannedPrice,0) amt_CurPlannedPrice,
          ifnull(amt_MovingAvgPrice,0) amt_MovingAvgPrice,
          ifnull(amt_MtlDlvrCost,0) amt_MtlDlvrCost,
          ifnull(amt_OtherCost,0) amt_OtherCost,
          ifnull(amt_OverheadCost,0) amt_OverheadCost,
          ifnull(amt_PlannedPrice1,0) amt_PlannedPrice1,
          ifnull(amt_PreviousPrice,0) amt_PreviousPrice,
          ifnull(amt_PrevPlannedPrice,0) amt_PrevPlannedPrice,
          ifnull(amt_StdUnitPrice,0) amt_StdUnitPrice,
          ifnull(amt_StdUnitPrice_GBL,0) amt_StdUnitPrice_GBL,
          ifnull(amt_StockInQInspAmt,0) amt_StockInQInspAmt,
          ifnull(amt_StockInQInspAmt_GBL,0) amt_StockInQInspAmt_GBL,
          ifnull(amt_StockInTransferAmt,0) amt_StockInTransferAmt,
          ifnull(amt_StockInTransferAmt_GBL,0) amt_StockInTransferAmt_GBL,
          ifnull(amt_StockInTransitAmt,0) amt_StockInTransitAmt,
          ifnull(amt_StockInTransitAmt_GBL,0) amt_StockInTransitAmt_GBL,
          ifnull(amt_StockValueAmt,0) amt_StockValueAmt,
          ifnull(amt_StockValueAmt_GBL,0) amt_StockValueAmt_GBL,
          ifnull(amt_UnrestrictedConsgnStockAmt,0) amt_UnrestrictedConsgnStockAmt,
          ifnull(amt_UnrestrictedConsgnStockAmt_GBL,0) amt_UnrestrictedConsgnStockAmt_GBL,
          ifnull(amt_WIPBalance,0) amt_WIPBalance,
	      ifnull(ct_WIPQty,0) ct_WIPQty,
          ifnull(ct_BlockedConsgnStock,0) ct_BlockedConsgnStock,
          ifnull(ct_BlockedStock,0) ct_BlockedStock,
          ifnull(ct_BlockedStockReturns,0) ct_BlockedStockReturns,
          ifnull(ct_ConsgnStockInQInsp,0) ct_ConsgnStockInQInsp,
          ifnull(ct_LastReceivedQty,0) ct_LastReceivedQty,
          ifnull(ct_RestrictedConsgnStock,0) ct_RestrictedConsgnStock,
          ifnull(ct_StockInQInsp,0) ct_StockInQInsp,
          ifnull(ct_StockInTransfer,0) ct_StockInTransfer,
          ifnull(ct_StockInTransit,0) ct_StockInTransit,
          ifnull(ct_StockQty,0) ct_StockQty,
          ifnull(ct_TotalRestrictedStock,0) ct_TotalRestrictedStock,
          ifnull(ct_UnrestrictedConsgnStock,0) ct_UnrestrictedConsgnStock,
          ifnull(ct_WIPAging,0) ct_WIPAging,
          ifnull(dd_BatchNo,'Not Set') dd_BatchNo,
          ifnull(dd_DocumentItemNo,0) dd_DocumentItemNo,
          ifnull(dd_DocumentNo,'Not Set') dd_DocumentNo,
          ifnull(dd_MovementType,'Not Set') dd_MovementType,
          ifnull(dd_ValuationType,'Not Set') dd_ValuationType,
          ifnull(iag.dim_Companyid,convert(bigint,1)) dim_Companyid,
          ifnull(Dim_ConsumptionTypeid,convert(bigint,1)) Dim_ConsumptionTypeid,
          ifnull(dim_costcenterid,convert(bigint,1)) dim_costcenterid,
          ifnull(dim_Currencyid,convert(bigint,1)) dim_Currencyid,
          ifnull(Dim_DateIdLastChangedPrice,convert(bigint,1)) Dim_DateIdLastChangedPrice,
          ifnull(Dim_DateIdPlannedPrice2,convert(bigint,1)) Dim_DateIdPlannedPrice2,
          ifnull(Dim_DocumentStatusid,convert(bigint,1)) Dim_DocumentStatusid,
          ifnull(Dim_DocumentTypeid,convert(bigint,1)) Dim_DocumentTypeid,
          ifnull(Dim_IncoTermid,convert(bigint,1)) Dim_IncoTermid,
          ifnull(Dim_ItemCategoryid,convert(bigint,1)) Dim_ItemCategoryid,
          ifnull(Dim_ItemStatusid,convert(bigint,1)) Dim_ItemStatusid,
          ifnull(dim_LastReceivedDateid,convert(bigint,1)) dim_LastReceivedDateid,
          ifnull(Dim_MovementIndicatorid,convert(bigint,1)) Dim_MovementIndicatorid,
          ifnull(iag.dim_Partid,convert(bigint,1)) dim_Partid,
          ifnull(dim_Plantid,convert(bigint,1)) dim_Plantid,
	      ifnull(Dim_ProfitCenterId,convert(bigint,1)) Dim_ProfitCenterId,
          ifnull(dim_producthierarchyid,convert(bigint,1)) dim_producthierarchyid,
          ifnull(Dim_PurchaseGroupid,convert(bigint,1)) Dim_PurchaseGroupid,
          ifnull(Dim_PurchaseMiscid,convert(bigint,1)) Dim_PurchaseMiscid,
          ifnull(Dim_PurchaseOrgid,convert(bigint,1)) Dim_PurchaseOrgid,
          ifnull(dim_specialstockid,convert(bigint,1)) dim_specialstockid,
          ifnull(dim_stockcategoryid,convert(bigint,1)) dim_stockcategoryid,
          ifnull(dim_StockTypeid,convert(bigint,1)) dim_StockTypeid,
          ifnull(dim_StorageLocationid,convert(bigint,1)) dim_StorageLocationid,
          ifnull(dim_StorageLocEntryDateid,convert(bigint,1)) dim_StorageLocEntryDateid,
          ifnull(Dim_SupplyingPlantId,convert(bigint,1)) Dim_SupplyingPlantId,
          ifnull(Dim_Termid,convert(bigint,1)) Dim_Termid,
          ifnull(Dim_UnitOfMeasureid,convert(bigint,1)) Dim_UnitOfMeasureid,
          ifnull(dim_Vendorid,convert(bigint,1)) dim_Vendorid,
          case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end SnapshotDate,
          convert(bigint,1) Dim_DateidSnapshot,
          (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryhistory') + row_number() over (order by ''),
	      ifnull(amt_ExchangeRate_GBL,1),
	      ifnull(amt_ExchangeRate,1),
	      ifnull(dim_Currencyid_TRA,convert(bigint,1)) dim_Currencyid_TRA,
	      ifnull(dim_Currencyid_GBL,convert(bigint,1)) dim_Currencyid_GBL,
/* Begin 15 Oct 2013 changes */				 
	 ifnull(ct_GRQty_Late30,0),
	 ifnull(ct_GRQty_31_60,0),
	 ifnull(ct_GRQty_61_90,0),
	 ifnull(ct_GRQty_91_180,0),
	 ifnull(ct_GIQty_Late30,0),
	 ifnull(ct_GIQty_31_60,0),
	 ifnull(ct_GIQty_61_90,0),
	 ifnull(ct_GIQty_91_180,0),
/* End 15 Oct 2013 changes */	  
	  ifnull(dd_prodordernumber,'Not Set'),
	  ifnull(dd_prodorderitemno,0),
	  ifnull(dim_productionorderstatusid,1),
	  ifnull(dim_productionordertypeid,1),
	  ifnull(dim_partsalesid,convert(bigint,1)) dim_partsalesid,
	  ifnull(dim_bwproducthierarchyid,convert(bigint,1)) dim_bwproducthierarchyid,
	  ifnull(iag.dim_mdg_partid,convert(bigint,1)) dim_mdg_partid,
	  ifnull(dd_invcovflag_emd,'Not Set') dd_invcovflag_emd,
	  ifnull(ct_avgfcst_emd,0) ct_avgfcst_emd,
	  ifnull(amt_cogsactualrate_emd,0) amt_cogsactualrate_emd,
	  ifnull(amt_cogsfixedrate_emd,0) amt_cogsfixedrate_emd,
	  ifnull(amt_cogsfixedplanrate_emd,0) amt_cogsfixedplanrate_emd,
	  ifnull(amt_cogsplanrate_emd,0) amt_cogsplanrate_emd,
	  ifnull(amt_cogsprevyearfixedrate_emd,0) amt_cogsprevyearfixedrate_emd,
	  ifnull(amt_cogsprevyearrate_emd,0) amt_cogsprevyearrate_emd,
	  ifnull(amt_cogsprevyearto1_emd,0) amt_cogsprevyearto1_emd,
	  ifnull(amt_cogsprevyearto2_emd,0) amt_cogsprevyearto2_emd,
	  ifnull(amt_cogsprevyearto3_emd,0) amt_cogsprevyearto3_emd,
	  ifnull(amt_cogsturnoverrate1_emd,0) amt_cogsturnoverrate1_emd,
	  ifnull(amt_cogsturnoverrate2_emd,0) amt_cogsturnoverrate2_emd,
	  ifnull(amt_cogsturnoverrate3_emd,0) amt_cogsturnoverrate3_emd,
	  ifnull(dim_bwhierarchycountryid,1) dim_bwhierarchycountryid,
	  ifnull(dim_clusterid,1) dim_clusterid,
	  ifnull(ct_countmaterialsafetystock,0) ct_countmaterialsafetystock,
	  ifnull(dd_batch_status_qkz,'Not Set') dd_batch_status_qkz,
	  ifnull(ct_baseuomratioKG,0) ct_baseuomratioKG,
	  ifnull(ct_intransitkg,0) ct_intransitkg,
	  ifnull(ct_dependentdemandkg,0) ct_dependentdemandkg,
	  ifnull(dim_countryhierpsid,1) dim_countryhierpsid,
	  ifnull(dim_countryhierarid,1) dim_countryhierarid,
	  ifnull(amt_OnHand,0) amt_OnHand,
	  ifnull(CT_ORDERITEMQTY,0),
	  ifnull(CT_GRQTY,0),
	  ifnull(dim_dateidexpirydate,1),
	  ifnull(dim_batchstatusid,convert(bigint,1)) dim_batchstatusid,
    ifnull(amt_restricted,0) amt_restricted
		  /* Liviu Ionescu - APP-9146 - Add in transit stock qty */
		,ifnull(ct_stockintransferplant,0) ct_stockintransferplant
		,ifnull(amt_stockintransferamtplant,0) amt_stockintransferamtplant
		,ifnull(amt_stockintransferamtplant_gbl,0) amt_stockintransferamtplant_gbl
	,ifnull(ct_StockInTransitERPs,0)  ct_StockInTransitERPs
	,ifnull(amt_StockInTransitERPs_SPrice,0)  amt_StockInTransitERPs_SPrice
	,mdg.primary_production_location
	,mdg.primary_production_location_name
	,ifnull(prt.mtomts,'Not Set') as dd_mtomts
	,iag.amt_StdUnitPrice_fxd
	
FROM fact_inventoryaging iag
inner join dim_mdg_part mdg on iag.dim_mdg_partid=mdg.dim_mdg_partid
inner join dim_Company dc ON iag.dim_companyid = dc.dim_Companyid
inner join dim_part prt on iag.dim_partid = prt.dim_partid;

/*Disabled 2017-10-27 Roxana D - as requested 
update fact_inventoryhistory f
set f.dim_clusterid = c.dim_clusterid
from fact_inventoryhistory f
	inner join dim_part p on f.dim_partid = p.dim_partid
	inner join dim_cluster c on p.laboratory = c.labor
where f.dim_clusterid <> c.dim_clusterid

update emd586.fact_inventoryhistory f
set f.dim_clusterid = ff.dim_clusterid
from emd586.fact_inventoryhistory f,EMDNext82A.fact_inventoryhistory ff
where f.fact_inventoryhistoryid = ff.fact_inventoryhistoryid
	and f.dim_projectsourceid=15
	and f.dim_clusterid <> ff.dim_clusterid
	
	*/


/*Add dd_primaryproductionlocation Roxana D 2017-11-01*/
update FACT_INVENTORYHISTORY f
set f.DD_PRIMARYPRODUCTIONLOCATION = p.PRIMARY_MANUFACTURING_SITE || ' - ' || p.PRIMARY_MANUFACTURING_location
FROM  FACT_INVENTORYHISTORY f
inner join  dim_cluster dc on dc.dim_clusterid = f.dim_clusterid
inner join (select distinct LABORATORY_CODE, LABORATORY, CLUSTER, PRIMARY_MANUFACTURING_SITE, PRIMARY_MANUFACTURING_LOCATION from  EMDNEXT82A.NEW_CLUSTER_MAPPING) p 
on p.LABORATORY_CODE = dc.labor 
where f.DD_PRIMARYPRODUCTIONLOCATION <>  p.PRIMARY_MANUFACTURING_SITE || ' - ' || p.PRIMARY_MANUFACTURING_location
and f.snapshotdate in (select distinct snapshotdate from fact_inventoryhistory_delete);

update FACT_INVENTORYHISTORY f
set f.DD_PRIMARYPRODUCTIONLOCATION = case when mdg.primary_production_location  = 'Not Set' then 'Not Set' else 
		mdg.primary_production_location || ' - ' || mdg.primary_production_location_name end 
FROM  FACT_INVENTORYHISTORY f
inner join  dim_cluster dc on dc.dim_clusterid = f.dim_clusterid
inner join dim_bwproducthierarchy ph on f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
   and businesssector = 'BS-02'
inner join (select distinct primary_production_location,primary_production_location_name  
from dim_mdg_part ) mdg on mdg.primary_production_location_name = dc.primary_manufacturing_site
where f.DD_PRIMARYPRODUCTIONLOCATION <> case when mdg.primary_production_location  = 'Not Set' then 'Not Set' else 
		mdg.primary_production_location || ' - ' || mdg.primary_production_location_name end 
and f.DD_PRIMARYPRODUCTIONLOCATION = 'Not Set' and mdg.primary_production_location_name <>'Not Set'
and f.snapshotdate in (select distinct snapshotdate from fact_inventoryhistory_delete);

drop table if exists fact_inventoryhistory_delete;
/*End*/

/*End*/

UPDATE fact_inventoryhistory t0
SET t0.Dim_DateidSnapshot = dt.dim_dateid
FROM
fact_inventoryhistory t0,
dim_Company dc,
dim_date dt
WHERE       t0.dim_companyid = dc.dim_Companyid
	AND dt.DateValue = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
	AND dt.companycode = dc.companycode
	AND t0.Dim_DateidSnapshot = 1;

UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_inventoryhistoryid), 0) FROM fact_inventoryhistory)
 WHERE table_name = 'fact_inventoryhistory';

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'processinglog', ifnull(max(processinglogid),  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) FROM processinglog;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          current_date,
          'Start of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

delete from fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_StorageLocationid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_StorageLocationid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid IN (2, 3)
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end,
                  (case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - 1),
                  (case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - 7),
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_StorageLocationid,
            dim_stockcategoryid,
            SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
and ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
and ih1.SnapshotDate - (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND ih1.SnapshotDate - ( INTERVAL '7' DAY) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM
fact_inventoryhistory ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid in (2,3)
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end;

delete from fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_Vendorid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_Vendorid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 4
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end,
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '1' DAY),
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '7' DAY),
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_Vendorid,
            dim_stockcategoryid,
            SnapshotDate;

UPDATE
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1DayChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
        ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;


truncate table fact_inventoryhistory_tmp;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
where  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
and ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM
fact_inventoryhistory ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid = 4
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end;

truncate table fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 5
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end,
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '1' DAY),
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '7' DAY),
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_stockcategoryid,
            SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1DayChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
        ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;


 UPDATE
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM
fact_inventoryhistory ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.dim_stockcategoryid = 5
AND ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 17 then current_date - 1 else current_date end;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          current_date,
          'End of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

truncate table fact_inventoryhistory_tmp;

/* Begin 15 Oct 2013 changes */
/* Begin 20 Dec 2013 changes */
UPDATE fact_inventoryhistory
SET ct_GRQty_Late30 = 0
WHERE ct_GRQty_Late30 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_31_60 = 0
WHERE ct_GRQty_31_60 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_61_90 = 0
WHERE ct_GRQty_61_90 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_91_180 = 0
WHERE ct_GRQty_91_180 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_Late30 = 0
WHERE ct_GIQty_Late30 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_31_60 = 0
WHERE ct_GIQty_31_60 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_61_90 = 0
WHERE ct_GIQty_61_90 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_91_180 = 0
WHERE ct_GIQty_91_180 IS NULL;

UPDATE fact_inventoryhistory f
SET f.dim_dateidlastprocessed = (select dim_dateid from dim_date d where d.datevalue = current_date and d.companycode = 'Not Set');

/* Marius 1 sep 2016 add Daily variance */

DROP TABLE IF EXISTS TMP_LATEST_DATE;
CREATE TABLE TMP_LATEST_DATE
AS
SELECT MAX(snp.datevalue) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT MAX(snp.datevalue)-1 max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT MAX(snp.datevalue)-7 max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-1) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_1DAY_CHANGE;
CREATE TABLE TMP_1DAY_CHANGE
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(CAST(CASE WHEN (amt_cogsfixedplanrate_emd/case when amt_exchangerate_gbl = 0 then 1 else amt_exchangerate_gbl end) = 0 THEN ((f_ih.amt_StockValueAmt + f_ih.amt_StockInQInspAmt + f_ih.amt_BlockedStockAmt
    + f_ih.amt_StockInTransitAmt + f_ih.amt_StockInTransferAmt + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice))) ELSE ((f_ih.ct_StockQty
    + f_ih.ct_StockInQInsp + f_ih.ct_BlockedStock + f_ih.ct_StockInTransit + f_ih.ct_StockInTransfer + f_ih.ct_TotalRestrictedStock))
    * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END AS DECIMAL (18,4))) COGS_ON_HAND
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max_dt FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;

DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT Dim_DateidSnapshot,dim_plantid,dim_partid,max(fact_inventoryhistoryid) ID
FROM fact_inventoryhistory f_ih
	INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY Dim_DateidSnapshot,dim_plantid,dim_partid;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = snp.datevalue -1 AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1daychange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1daychange = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = snp.datevalue - 7 AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1weekchange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1weekchange = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-1) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1monthchange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1monthchange = x.v_cogs;



/*Update dimension dim_batchno on EMD586  Roxana D 2018-01-17*/


delete from number_fountain m where m.table_name = 'dim_batchno';

insert into number_fountain
select 	'dim_batchno',
	ifnull(max(d.dim_batchnoid),1)
from EMD586.dim_batchno d;

INSERT INTO EMD586.DIM_BATCHNO (DIM_BATCHNOID, BATCHNO, PROJECTSOURCEID, DW_INSERT_DATE, DW_UPDATE_DATE)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_batchno')
          +  row_number() over(order by ''),F.dd_batchNo, 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP
FROM 
(SELECT DISTINCT DD_BATCHNO FROM FACT_INVENTORYhistory ) F
WHERE NOT EXISTS (SELECT 1 FROM EMD586.DIM_BATCHNO  D WHERE D.BATCHNO = F.DD_BATCHNO);


UPDATE FACT_inventoryhistory F
SET F.DIM_BATCHNOID = IFNULL(D.DIM_BATCHNOID,1)
FROM FACT_inventoryhistory F, EMD586.DIM_BATCHNO D
WHERE F.DD_BATCHNo = D.BATCHNO
AND F.DIM_BATCHNOID <> IFNULL(D.DIM_BATCHNOID,1);


/* Liviu Ionescu - APP - APP-9188 */
update fact_inventoryhistory f
set f.dim_ClusterPlantid = 1;

update fact_inventoryhistory f
set f.dim_ClusterPlantid = ifnull(dcp.dim_ClusterPlantid,1) -- select count(1)
from fact_inventoryhistory f
	inner join dim_cluster dc on dc.dim_clusterid = f.dim_clusterid
	inner join (select max(DIM_CLUSTERID) DIM_CLUSTERID, CLUST from dim_cluster  group by CLUST) dcmax on dcmax.CLUST = dc.CLUST
	inner join dim_plant dp on dp.dim_plantid = f.dim_plantid
	inner join (select max(dim_ClusterPlantid) dim_ClusterPlantid, MerckLSCluster,
								ifnull(case when position(' ' in ltrim(rtrim(PlantEMD))) = 0 then ltrim(rtrim(PlantEMD)) else substring(ltrim(rtrim(PlantEMD)),1,position(' ' in ltrim(rtrim(PlantEMD))) -1) end,'Not Set') PlantEMD from dim_ClusterPlant 
			group by MerckLSCluster, ifnull(case when position(' ' in ltrim(rtrim(PlantEMD))) = 0 then ltrim(rtrim(PlantEMD)) else substring(ltrim(rtrim(PlantEMD)),1,position(' ' in ltrim(rtrim(PlantEMD))) -1) end,'Not Set')) dcp on lower(dp.PlantCode) = lower(dcp.PlantEMD)
								and lower(dcmax.Clust) = lower(dcp.MerckLSCluster)
								and f.dim_ClusterPlantid <> ifnull(dcp.dim_ClusterPlantid,1);
								
/* Alina APP-9883 26 June 2018 */
drop table  if exists tmp_date ;
create table tmp_date as
select current_date-1 datevalue;


drop table if exists tmp_ShippedAgnstOrder;
create table tmp_ShippedAgnstOrder as
select MDGPartNumber_NoLeadZero as partnumber,dpl.plantcode,dp.UnitOfMeasure,
sum(ct_QtyDelivered*case when ct_baseuomratio_shipped=0 then 1 else ct_baseuomratio_shipped end) as ct_ShippedAgnstOrderQty
from fact_salesorderdelivery f
inner join dim_part dp on f.dim_partid = dp.dim_partid
inner join dim_mdg_part mdg on mdg.dim_mdg_partid = f.dim_mdg_partid
inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
inner join dim_date dd on f.dim_dateidactualgoodsissue = dd.dim_dateid
inner join tmp_date tmp on dd.datevalue = dd.datevalue
where dd.datevalue between add_months((convert(date,tmp.datevalue))+1,-12) and (convert(date,tmp.datevalue))
group by MDGPartNumber_NoLeadZero, dpl.plantcode,dp.UnitOfMeasure;


drop table if exists tmp_invntdata;
create table tmp_invntdata as
select tmp.datevalue,MDGPartNumber_NoLeadZero as partnumber,dpl.plantcode,dp.UnitOfMeasure,
sum(f.ct_StockQty + f.ct_StockInQInsp + f.ct_BlockedStock + f.ct_StockInTransit +f.ct_StockInTransfer + f.ct_TotalRestrictedStock) ct_StockQty,
min(FACT_INVENTORYHISTORYid) FACT_INVENTORYHISTORYid,
cast (0 as decimal (18,4)) as ct_InvConsumption,
cast (0 as decimal (18,4)) as ct_ShippedAgnstOrderQty
from FACT_INVENTORYHISTORY f
inner join dim_part dp on f.dim_partid = dp.dim_partid
inner join dim_mdg_part mdg on mdg.dim_mdg_partid = f.dim_mdg_partid
inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
inner join dim_date dd on f.DIM_DATEIDSNAPSHOT = dd.dim_dateid
inner join tmp_date tmp on dd.datevalue = tmp.datevalue
group by tmp.datevalue,MDGPartNumber_NoLeadZero,dpl.plantcode,dp.UnitOfMeasure;

update tmp_invntdata inv
set ct_InvConsumption = inv.ct_StockQty / shp.ct_ShippedAgnstOrderQty * 360,
ct_ShippedAgnstOrderQty = shp.ct_ShippedAgnstOrderQty
from tmp_invntdata inv
left join tmp_ShippedAgnstOrder shp on inv.partnumber = shp.partnumber
                                     and inv.plantcode = shp.plantcode
                                     and inv.UnitOfMeasure = shp.UnitOfMeasure
where shp.ct_ShippedAgnstOrderQty<>0;

update FACT_INVENTORYHISTORY f
set f.ct_ShippedAgnstOrderQty = ifnull(inv.ct_ShippedAgnstOrderQty,0)
from FACT_INVENTORYHISTORY f
inner join tmp_invntdata inv on f.FACT_INVENTORYHISTORYid = inv.FACT_INVENTORYHISTORYid;

update FACT_INVENTORYHISTORY f
set f.ct_InvConsumption = ifnull(inv.ct_InvConsumption,0)
from FACT_INVENTORYHISTORY f
inner join dim_part dp on f.dim_partid = dp.dim_partid
inner join dim_mdg_part mdg on mdg.dim_mdg_partid = f.dim_mdg_partid
inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
inner join dim_date dd on f.DIM_DATEIDSNAPSHOT = dd.dim_dateid
inner join tmp_invntdata inv on inv.datevalue = dd.datevalue
                              and mdg.MDGPartNumber_NoLeadZero = inv.partnumber
                              and dpl.plantcode = inv.plantcode
                              and dp.UnitOfMeasure = inv.UnitOfMeasure
where f.ct_InvConsumption <> ifnull(inv.ct_InvConsumption,0);




                              update fact_inventoryhistory f set f.dim_consumptionBucketsid=upd.dim_consumptionBucketsid
                              from fact_inventoryhistory f
                              inner join dim_date dd on f.DIM_DATEIDSNAPSHOT = dd.dim_dateid
                              inner join tmp_date tmp on dd.datevalue = tmp.datevalue
                              inner join (select fact_inventoryhistoryid
                              		,case when CT_INVCONSUMPTION<360 and CT_INVCONSUMPTION<>0 then 2
                              		      when CT_INVCONSUMPTION>=360 AND CT_INVCONSUMPTION<720 then 3
                              		      when CT_INVCONSUMPTION>=720 AND CT_INVCONSUMPTION<1440 then 4
                              		      when CT_INVCONSUMPTION>=1440 then 5
                              		      when CT_INVCONSUMPTION = 0 then 6 else 1 end as dim_consumptionBucketsid
                              	    from fact_inventoryhistory f) upd on upd.fact_inventoryhistoryid = f.fact_inventoryhistoryid
                              where f.dim_consumptionBucketsid<>upd.dim_consumptionBucketsid;

                              update fact_inventoryhistory f set f.DIM_CONSUMPTIONBUCKETS_MONTHSID=upd.DIM_CONSUMPTIONBUCKETS_MONTHSID
                              from fact_inventoryhistory f
                              inner join dim_date dd on f.DIM_DATEIDSNAPSHOT = dd.dim_dateid
                              inner join tmp_date tmp on dd.datevalue = tmp.datevalue
                              inner join (select fact_inventoryhistoryid
                                 		  ,case when CT_INVCONSUMPTION<90 and CT_INVCONSUMPTION<>0 then 1
                              		  	when CT_INVCONSUMPTION>=90 AND CT_INVCONSUMPTION<180 then 2
                              		 	when CT_INVCONSUMPTION>=180 AND CT_INVCONSUMPTION<360 then 3
                              		 	when CT_INVCONSUMPTION>=360 AND CT_INVCONSUMPTION<540 then 4
                              		 	when CT_INVCONSUMPTION>=540 AND CT_INVCONSUMPTION<720 then 5
                              		 	when CT_INVCONSUMPTION>=720 then 6
                              		 	when CT_INVCONSUMPTION = 0 then 7 else 8 end as DIM_CONSUMPTIONBUCKETS_MONTHSID
                              	    from fact_inventoryhistory f) upd on upd.fact_inventoryhistoryid = f.fact_inventoryhistoryid
                              where f.DIM_CONSUMPTIONBUCKETS_MONTHSID<>upd.DIM_CONSUMPTIONBUCKETS_MONTHSID;


     update fact_inventoryhistory f set f.dim_consumptionbuckets_mixed_monthsid=upd.DIM_CONSUMPTIONBUCKETS_MONTHSID
                              from fact_inventoryhistory f
                              inner join dim_date dd on f.DIM_DATEIDSNAPSHOT = dd.dim_dateid
                              inner join tmp_date tmp on dd.datevalue = tmp.datevalue
                              inner join (select fact_inventoryhistoryid
                                 		  ,case when CT_INVCONSUMPTION<90 and CT_INVCONSUMPTION<>0 then 2
                              		  	when CT_INVCONSUMPTION>=90 AND CT_INVCONSUMPTION<180 then 3
                              		 	when CT_INVCONSUMPTION>=180 AND CT_INVCONSUMPTION<360 then 4
                              		 	when CT_INVCONSUMPTION>=360 AND CT_INVCONSUMPTION<540 then 5
                              		 	when CT_INVCONSUMPTION>=540 AND CT_INVCONSUMPTION<720 then 6
                              		 	when CT_INVCONSUMPTION>=720 then 7
                              		 	when CT_INVCONSUMPTION = 0 then 8 else 1 end as DIM_CONSUMPTIONBUCKETS_MONTHSID
                              	    from fact_inventoryhistory f) upd on upd.fact_inventoryhistoryid =
f.fact_inventoryhistoryid
                              where f.dim_consumptionbuckets_mixed_monthsid<>upd.DIM_CONSUMPTIONBUCKETS_MONTHSID;
			      
/* END - Alina APP-9883 26 June 2018 */

/* Liviu Ionescu 20180710 - basecamp.com/2105228/projects/10840778/todos/346079395 */
update fact_inventoryhistory f
	set f.DIM_PPLTierID = dpplmh.DIM_PPLTierID
from fact_inventoryhistory f
	 inner join dim_mdg_part mdg on f.dim_mdg_partid = mdg.dim_mdg_partid and f.dim_projectsourceid = mdg.projectsourceid
	 inner join dim_plant pl on f.dim_plantid = pl.dim_plantid and f.dim_projectsourceid = pl.projectsourceid
	 inner join dim_cluster dc on dc.dim_clusterid = f.dim_clusterid and f.dim_projectsourceid = dc.projectsourceid
	 inner join DIM_PPLTier dpplmh on mdg.primary_production_location_name || '-' || pl.PlantCode = dpplmh.PPL_MainHub
		and dpplmh.MerckLSCluster = dc.CLUST
		 and f.dim_projectsourceid = dpplmh.projectsourceid
where f.DIM_PPLTierID <> dpplmh.DIM_PPLTierID;
