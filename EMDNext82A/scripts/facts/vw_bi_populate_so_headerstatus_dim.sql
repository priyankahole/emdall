/* ##################################################################################################################
  
     Script         : bi_populate_so_headerstatus_dim 
     Author         : Ashu
     Created On     : 17 Jan 2013
  
  
     Description    : Stored Proc bi_populate_so_headerstatus_dim from MySQL to Vectorwise syntax
  
     Change History
     Date            By        Version           Desc
     17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
	 7 July 2014     George    1.1               Added GeneralIncompleteStatusItemCode,OverallCreditStatus,OverallCreditStatusCode,OverallBlkdStatusCode,
														OverallDlvrBlkStatusCode,TotalIncompleteStatusAllItemsCode,RejectionStatusCode
	 11 July 2013    George    1.2               Added TVBST source fields: GeneralIncompletionProcessingstatusOfHeader,OverallProcessingstatusofcreditchecks,
												 OverallBlockedProcessingStatus,DeliveryBlockProcessingStatus,TotalIncompletionProcessingStatusAllItems,OverallRejectionProcessingStatus
	  5 Feb 2015     Liviu Ionescu   1.3     Add combine for table dim_salesorderheaderstatus, VBUK
	 #################################################################################################################### */
insert into dim_salesorderheaderstatus
 (dim_salesorderheaderstatusid
           )
select 1
from (select 1) a
where not exists ( select 'x' from dim_salesorderheaderstatus where dim_salesorderheaderstatusid = 1);

DELETE FROM VBUK
  WHERE NOT EXISTS
            (SELECT 1
               FROM VBAK_VBAP_VBEP
              WHERE VBUK_VBELN = VBAK_VBELN) 
        AND NOT EXISTS
               (SELECT 1
                  FROM fact_salesorder
                 WHERE dd_SalesDocNo = VBUK_VBELN);

 
DELETE FROM VBUK
  WHERE NOT EXISTS
               (SELECT 1
                  FROM VBAK_VBAP
                 WHERE VBUK_VBELN = VBAP_VBELN)
        AND NOT EXISTS
               (SELECT 1
                  FROM fact_salesorder
                 WHERE dd_SalesDocNo = VBUK_VBELN);



UPDATE dim_salesorderheaderstatus sohs
SET sohs.RefStatusAllItems=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_RFGSK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.ConfirmationStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_BESTK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.DeliveryStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LFSTK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrStatusOfItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LFGSK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.BillingStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_FKSTK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.BillingStatusOfOrdRelBillDoc=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_FKSAK;


UPDATE dim_salesorderheaderstatus sohs
SET sohs.RejectionStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_ABSTK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallProcessStatusItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_GBSTK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.TotalIncompleteStatusAllItems=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALS;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.GeneralIncompleteStatusItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALL;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallPickingStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_KOSTK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.HeaderIncompletionStautusConcernDlvry=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVVLK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallBlkdStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_SPSTG;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrBlkStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LSSTK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.GeneralIncompleteStatusItemCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALL;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallCreditStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_CMGST;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallCreditStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_CMGST;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallBlkdStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_SPSTG;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrBlkStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LSSTK;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.TotalIncompleteStatusAllItemsCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALS;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.RejectionStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_ABSTK;
 
delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 
 
INSERT INTO dim_salesorderheaderstatus(
	    dim_salesorderheaderstatusid,
            SalesDocumentNumber,
            RefStatusAllItems,
            ConfirmationStatus,
            DeliveryStatus,
            OverallDlvrStatusOfItem,
            BillingStatus,
            BillingStatusOfOrdRelBillDoc,
            RejectionStatus,
            OverallProcessStatusItem,
            TotalIncompleteStatusAllItems,
            GeneralIncompleteStatusItem,
            OverallPickingStatus,
	    HeaderIncompletionStautusConcernDlvry,
	    OverallBlkdStatus,
	    OverallDlvrBlkStatus,
		GeneralIncompleteStatusItemCode,
		OverallCreditStatus,
		OverallCreditStatusCode,
		OverallBlkdStatusCode,
		OverallDlvrBlkStatusCode,
		TotalIncompleteStatusAllItemsCode,
		RejectionStatusCode		
		)
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        'Not Set' RefStatusAllItems,
        'Not Set' ConfirmationStatus,
        'Not Set' DeliveryStatus,
        'Not Set' OverallDlvrStatusOfItem,
        'Not Set' BillingStatus,
        'Not Set' BillingStatusOfOrdRelBillDoc,
        'Not Set' RejectionStatus,
        'Not Set' OverallProcessStatusItem,
        'Not Set' TotalIncompleteStatusAllItems,
        'Not Set' GeneralIncompleteStatusItem,
        'Not Set' OverallPickingStatus,
		'Not Set' HeaderIncompletionStautusConcernDlvry,
		'Not Set' OverallBlkdStatus,
		'Not Set' OverallDlvrBlkStatus,
		'Not Set' GeneralIncompleteStatusItemCode,
		'Not Set' OverallCreditStatus,
		'Not Set' OverallCreditStatusCode,
		'Not Set' OverallBlkdStatusCode,
		'Not Set' OverallDlvrBlkStatusCode,
		'Not Set' TotalIncompleteStatusAllItemsCode,
		'Not Set' RejectionStatusCode		
FROM VBUK inner join VBAK_VBAP_VBEP on VBAK_VBELN = VBUK_VBELN
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = VBUK_VBELN)) dtbl1;

DROP TABLE IF EXISTS TMP_VBAK_VBAP_VBEP_SOHS;
CREATE TABLE TMP_VBAK_VBAP_VBEP_SOHS
AS
SELECT DISTINCT VBAK_VBELN FROM VBAK_VBAP_VBEP;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.RefStatusAllItems=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_RFGSK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.ConfirmationStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_BESTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.DeliveryStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LFSTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrStatusOfItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LFGSK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.BillingStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_FKSTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.BillingStatusOfOrdRelBillDoc=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_FKSAK AND vp.VBAK_VBELN = v.VBUK_VBELN;


UPDATE dim_salesorderheaderstatus sohs
SET sohs.RejectionStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_ABSTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallProcessStatusItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_GBSTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.TotalIncompleteStatusAllItems=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALS AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.GeneralIncompleteStatusItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALL AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallPickingStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_KOSTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.HeaderIncompletionStautusConcernDlvry=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVVLK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallBlkdStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_SPSTG AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrBlkStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LSSTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.GeneralIncompleteStatusItemCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALL AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallCreditStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_CMGST AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallCreditStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_CMGST AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallBlkdStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_SPSTG AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrBlkStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LSSTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.TotalIncompleteStatusAllItemsCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALS AND vp.VBAK_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.RejectionStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_VBEP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_ABSTK AND vp.VBAK_VBELN = v.VBUK_VBELN;

DROP TABLE IF EXISTS TMP_VBAK_VBAP_VBEP_SOHS;
   
delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 

INSERT
INTO dim_salesorderheaderstatus(
	dim_salesorderheaderstatusid,
   SalesDocumentNumber,
   RefStatusAllItems,
   ConfirmationStatus,
   DeliveryStatus,
   OverallDlvrStatusOfItem,
   BillingStatus,
   BillingStatusOfOrdRelBillDoc,
   RejectionStatus,
   OverallProcessStatusItem,
   TotalIncompleteStatusAllItems,
   GeneralIncompleteStatusItem,
   OverallPickingStatus,
   HeaderIncompletionStautusConcernDlvry,
   OverallBlkdStatus,
   OverallDlvrBlkStatus,
   GeneralIncompleteStatusItemCode,
   OverallCreditStatus,
   OverallCreditStatusCode,
   OverallBlkdStatusCode,
   OverallDlvrBlkStatusCode,
   TotalIncompleteStatusAllItemsCode,
   RejectionStatusCode		
   )
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
          'Not Set' RefStatusAllItems,
        'Not Set' ConfirmationStatus,
        'Not Set' DeliveryStatus,
        'Not Set' OverallDlvrStatusOfItem,
        'Not Set' BillingStatus,
        'Not Set' BillingStatusOfOrdRelBillDoc,
        'Not Set' RejectionStatus,
        'Not Set' OverallProcessStatusItem,
        'Not Set' TotalIncompleteStatusAllItems,
        'Not Set' GeneralIncompleteStatusItem,
        'Not Set' OverallPickingStatus,
		'Not Set' HeaderIncompletionStautusConcernDlvry,
		'Not Set' OverallBlkdStatus,
		'Not Set' OverallDlvrBlkStatus,
		'Not Set' GeneralIncompleteStatusItemCode,
		'Not Set' OverallCreditStatus,
		'Not Set' OverallCreditStatusCode,
		'Not Set' OverallBlkdStatusCode,
		'Not Set' OverallDlvrBlkStatusCode,
		'Not Set' TotalIncompleteStatusAllItemsCode,
		'Not Set' RejectionStatusCode	
FROM VBUK inner join VBAK_VBAP on VBAP_VBELN = VBUK_VBELN
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = VBUK_VBELN)) dtbl1;


DROP TABLE IF EXISTS TMP_VBAK_VBAP_SOHS;
CREATE TABLE TMP_VBAK_VBAP_SOHS
AS
SELECT DISTINCT VBAP_VBELN FROM VBAK_VBAP;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.RefStatusAllItems=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_RFGSK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.ConfirmationStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_BESTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.DeliveryStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LFSTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrStatusOfItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LFGSK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.BillingStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_FKSTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.BillingStatusOfOrdRelBillDoc=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_FKSAK AND vp.VBAP_VBELN = v.VBUK_VBELN;


UPDATE dim_salesorderheaderstatus sohs
SET sohs.RejectionStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_ABSTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallProcessStatusItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_GBSTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.TotalIncompleteStatusAllItems=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALS AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.GeneralIncompleteStatusItem=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALL AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallPickingStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_KOSTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.HeaderIncompletionStautusConcernDlvry=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVVLK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallBlkdStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_SPSTG AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrBlkStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LSSTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.GeneralIncompleteStatusItemCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALL AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallCreditStatus=IFNULL(t.DD07T_DDTEXT,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_CMGST AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallCreditStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_CMGST AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallBlkdStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_SPSTG AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.OverallDlvrBlkStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_LSSTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.TotalIncompleteStatusAllItemsCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_UVALS AND vp.VBAP_VBELN = v.VBUK_VBELN;

UPDATE dim_salesorderheaderstatus sohs
SET sohs.RejectionStatusCode=IFNULL(t.DD07T_DOMVALUE,'Not Set') 
FROM dim_salesorderheaderstatus sohs,dd07t t ,VBUK v,TMP_VBAK_VBAP_SOHS vp
WHERE sohs.SalesDocumentNumber = v.VBUK_VBELN
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUK_ABSTK AND vp.VBAP_VBELN = v.VBUK_VBELN;

DROP TABLE IF EXISTS TMP_VBAK_VBAP_SOHS;