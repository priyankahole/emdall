/**********************************************************************************/
/* 19 Apr 2018   2.0      CristianT Added logic for Batch APP-8373                */
/* 6 March 2014    		  Cornelia 	 add ct_qtyuoe 	  */
/* 30 Dec 2013    		  Cornelia 	 update dim_customer 	  */
/* 24 Oct 2013    1.11		  Issam	 	 New fields AFKO.VORUE and AUFK.VAPLZ 	  */
/* 7 Sep 2013     1.5		  Lokesh	 Exchange rate, currency and amt changes */
/*  28 Aug 2013   1.4	      Shanthi    New Field WIP Qty from AUFM		  */
/**********************************************************************************/

delete from JEST_AUFK;
insert into JEST_AUFK(JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS)
SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0001
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0002
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0004
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0008
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0010
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0012
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0016
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0028
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0045
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0046
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0056
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0082
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0281
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0321
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0340
UNION SELECT JEST_OBJNR,JEST_STAT,JEST_INACT,AUFK_WERKS FROM JEST_AUFK_I0420;

Drop table if exists pGlobalCurrency_po_43;

Create table pGlobalCurrency_po_43(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_43(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_43
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

Drop table if exists fact_productionorder_tmp_43;
Drop table if exists max_holder_43;

Create table fact_productionorder_tmp_43 as
SELECT dd_ordernumber,dd_orderitemno
FROM fact_productionorder ;

Create table max_holder_43
as Select ifnull(max(fact_productionorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_productionorder;

DROP TABLE IF EXISTS TMP_001_productionorder;
CREATE TABLE TMP_001_productionorder
AS SELECT p.*,convert(varchar(5),'') AUFK_WAERS from fact_productionorder p where 1=2;


INSERT INTO TMP_001_productionorder(
fact_productionorderid,
Dim_DateidRelease,
Dim_DateidBOMExplosion,
Dim_DateidLastScheduling,
Dim_DateidTechnicalCompletion,
Dim_DateidBasicStart,
Dim_DateidScheduledRelease,
Dim_DateidScheduledFinish,
Dim_DateidScheduledStart,
Dim_DateidActualStart,
Dim_DateidConfirmedOrderFinish,
Dim_DateidActualHeaderFinish,
Dim_DateidActualRelease,
Dim_DateidActualItemFinish,
Dim_DateidPlannedOrderDelivery,
Dim_DateidRoutingTransfer,
Dim_DateidBasicFinish,
Dim_ControllingAreaid,
Dim_ProfitCenterId,
Dim_tasklisttypeid,
Dim_PartidHeader,
Dim_bomstatusid,
Dim_bomusageid,
dim_productionschedulerid,
Dim_ProcurementID,
Dim_SpecialProcurementID,
Dim_UnitOfMeasureid,
Dim_PartidItem,
Dim_AccountCategoryid,
Dim_StockTypeid,
Dim_StorageLocationid,
Dim_Plantid,
dim_specialstockid,
Dim_ConsumptionTypeid,
Dim_SalesOrgid,
Dim_PurchaseOrgid,
Dim_Currencyid,
Dim_Companyid,
Dim_ordertypeid,
dim_productionorderstatusid,
dim_productionordermiscid,
Dim_BuildTypeid,
dd_ordernumber,
dd_orderitemno,
dd_bomexplosionno,
dd_plannedorderno,
dd_SalesOrderNo,
dd_SalesOrderItemNo,
dd_SalesOrderDeliveryScheduleNo,
ct_ConfirmedReworkQty,
ct_ScrapQty,
ct_OrderItemQty,
ct_TotalOrderQty,
ct_GRQty,
ct_GRProcessingTime,
amt_estimatedTotalCost,
amt_ValueGR,
dd_BOMLevel,
dd_sequenceno,
dd_ObjectNumber,
ct_ConfirmedScrapQty,
dd_OrderDescription,
Dim_MrpControllerId,
dim_currencyid_TRA,
dim_currencyid_GBL,
dd_OperationNumber,
dd_WorkCenter,
dim_customerid,
AUFK_WAERS
)
SELECT max_holder_43.maxid + row_number() over(order by '') fact_productionorderid,
       1 as Dim_DateidRelease,
       1 as Dim_DateidBOMExplosion,
       1 as Dim_DateidLastScheduling,
       1 as Dim_DateidTechnicalCompletion,
       1 as Dim_DateidBasicStart,
       1 as Dim_DateidScheduledRelease,
       1 as Dim_DateidScheduledFinish,
       1 as Dim_DateidScheduledStart,
       1 as Dim_DateidActualStart,
       1 as Dim_DateidConfirmedOrderFinish,
       1 as Dim_DateidActualHeaderFinish,
       1 as Dim_DateidActualRelease,
       1 as Dim_DateidActualItemFinish,
       1 as Dim_DateidPlannedOrderDelivery,
       1 as Dim_DateidRoutingTransfer,
       1 as Dim_DateidBasicFinish,
       1 as Dim_ControllingAreaid,
       1 as Dim_ProfitCenterId,
       1 as Dim_tasklisttypeid,
       1 as Dim_PartidHeader,
       1 as Dim_bomstatusid,
       1 as Dim_bomusageid,
       1 as dim_productionschedulerid,
       1 as Dim_ProcurementID,
       1 as Dim_SpecialProcurementID,
       1 as Dim_UnitOfMeasureid,
       1 as Dim_PartidItem,
       1 as Dim_AccountCategoryid,
       1 as Dim_StockTypeid,
       1 as Dim_StorageLocationid,
       1 as Dim_Plantid,
       1 as dim_specialstockid,
       1 as Dim_ConsumptionTypeid,
       1 as Dim_SalesOrgid,
       1 as Dim_PurchaseOrgid,
       1 as Dim_Currencyid,
       1 as Dim_Companyid,
       1 as Dim_productionordertypeid,
       1 as dim_productionorderstatusid,
       1 as dim_productionordermiscid,
       1 as Dim_BuildTypeid,
       ifnull(AFKO_AUFNR,'Not Set') dd_ordernumber,
       AFPO_POSNR dd_orderitemno,
       ifnull(AFPO_SERNR, 'Not Set') dd_bomexplosionno,
       ifnull(AFPO_PLNUM,'Not Set') dd_plannedorderno,
       ifnull(AFPO_KDAUF,'Not Set') dd_SalesOrderNo,
       ifnull(AFPO_KDPOS,0) dd_SalesOrderItemNo,
       ifnull(AFPO_KDEIN,0) dd_SalesOrderDeliveryScheduleNo,
       AFKO_RMNGA ct_ConfirmedReworkQty,
       AFPO_PSAMG ct_ScrapQty,
       AFPO_PSMNG ct_OrderItemQty,
       AFKO_GAMNG ct_TotalOrderQty,
       AFPO_WEMNG ct_GRQty,
       AFPO_WEBAZ ct_GRProcessingTime,
       AUFK_USER4 amt_estimatedTotalCost,
       mhi.AFPO_WEWRT amt_ValueGR,
       AFKO_STUFE dd_BOMLevel,
       ifnull(AFKO_CY_SEQNR,'Not Set') dd_sequenceno,
       ifnull(AUFK_OBJNR,'Not Set') dd_ObjectNumber,
       ifnull(AFKO_IASMG,0.000) ct_ConfirmedScrapQty,
       ifnull(AUFK_KTEXT,'Not Set') dd_OrderDescription,
       1 Dim_MrpControllerId,
       1 Dim_Currencyid_TRA,
       1 dim_Currencyid_GBL,
       ifnull(AFKO_VORUE,'Not Set') dd_OperationNumber,
       ifnull(AUFK_VAPLZ,'Not Set') dd_WorkCenter,
       1 dim_customerid,
       mhi.AUFK_WAERS -- used in Dim_Currencyid_TRA
FROM max_holder_43,
     AFKO_AFPO_AUFK mhi,
     pGlobalCurrency_po_43
WHERE NOT EXISTS (SELECT 1
                  FROM fact_productionorder_tmp_43 po
                  WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
                        AND po.dd_orderitemno = mhi.AFPO_POSNR);

UPDATE TMP_001_productionorder t1
SET t1.Dim_Currencyid_TRA = IFNULL(cur.Dim_Currencyid,1)
FROM TMP_001_productionorder t1 
		LEFT JOIN Dim_Currency cur ON		
			cur.CurrencyCode = t1.AUFK_WAERS;

UPDATE TMP_001_productionorder t1
SET t1.Dim_Currencyid_GBL = IFNULL(t2.Dim_Currencyid,1)
FROM TMP_001_productionorder t1 
		CROSS JOIN (SELECT cr.Dim_Currencyid 
		            FROM Dim_Currency cr 
							INNER JOIN pGlobalCurrency_po_43 p
                  on cr.CurrencyCode = p.pGlobalCurrency) t2;

INSERT INTO fact_productionorder(
fact_productionorderid,
Dim_DateidRelease,
Dim_DateidBOMExplosion,
Dim_DateidLastScheduling,
Dim_DateidTechnicalCompletion,
Dim_DateidBasicStart,
Dim_DateidScheduledRelease,
Dim_DateidScheduledFinish,
Dim_DateidScheduledStart,
Dim_DateidActualStart,
Dim_DateidConfirmedOrderFinish,
Dim_DateidActualHeaderFinish,
Dim_DateidActualRelease,
Dim_DateidActualItemFinish,
Dim_DateidPlannedOrderDelivery,
Dim_DateidRoutingTransfer,
Dim_DateidBasicFinish,
Dim_ControllingAreaid,
Dim_ProfitCenterId,
Dim_tasklisttypeid,
Dim_PartidHeader,
Dim_bomstatusid,
Dim_bomusageid,
dim_productionschedulerid,
Dim_ProcurementID,
Dim_SpecialProcurementID,
Dim_UnitOfMeasureid,
Dim_PartidItem,
Dim_AccountCategoryid,
Dim_StockTypeid,
Dim_StorageLocationid,
Dim_Plantid,
dim_specialstockid,
Dim_ConsumptionTypeid,
Dim_SalesOrgid,
Dim_PurchaseOrgid,
Dim_Currencyid,
Dim_Companyid,
Dim_ordertypeid,
dim_productionorderstatusid,
dim_productionordermiscid,
Dim_BuildTypeid,
dd_ordernumber,
dd_orderitemno,
dd_bomexplosionno,
dd_plannedorderno,
dd_SalesOrderNo,
dd_SalesOrderItemNo,
dd_SalesOrderDeliveryScheduleNo,
ct_ConfirmedReworkQty,
ct_ScrapQty,
ct_OrderItemQty,
ct_TotalOrderQty,
ct_GRQty,
ct_GRProcessingTime,
amt_estimatedTotalCost,
amt_ValueGR,
dd_BOMLevel,
dd_sequenceno,
dd_ObjectNumber,
ct_ConfirmedScrapQty,
dd_OrderDescription,
Dim_MrpControllerId,
dim_currencyid_TRA,
dim_currencyid_GBL,
dd_OperationNumber,
dd_WorkCenter,
dim_customerid)
SELECT fact_productionorderid,
       Dim_DateidRelease,
       Dim_DateidBOMExplosion,
       Dim_DateidLastScheduling,
       Dim_DateidTechnicalCompletion,
       Dim_DateidBasicStart,
       Dim_DateidScheduledRelease,
       Dim_DateidScheduledFinish,
       Dim_DateidScheduledStart,
       Dim_DateidActualStart,
       Dim_DateidConfirmedOrderFinish,
       Dim_DateidActualHeaderFinish,
       Dim_DateidActualRelease,
       Dim_DateidActualItemFinish,
       Dim_DateidPlannedOrderDelivery,
       Dim_DateidRoutingTransfer,
       Dim_DateidBasicFinish,
       Dim_ControllingAreaid,
       Dim_ProfitCenterId,
       Dim_tasklisttypeid,
       Dim_PartidHeader,
       Dim_bomstatusid,
       Dim_bomusageid,
       dim_productionschedulerid,
       Dim_ProcurementID,
       Dim_SpecialProcurementID,
       Dim_UnitOfMeasureid,
       Dim_PartidItem,
       Dim_AccountCategoryid,
       Dim_StockTypeid,
       Dim_StorageLocationid,
       Dim_Plantid,
       dim_specialstockid,
       Dim_ConsumptionTypeid,
       Dim_SalesOrgid,
       Dim_PurchaseOrgid,
       Dim_Currencyid,
       Dim_Companyid,
       Dim_ordertypeid,
       dim_productionorderstatusid,
       dim_productionordermiscid,
       Dim_BuildTypeid,
       dd_ordernumber,
       dd_orderitemno,
       dd_bomexplosionno,
       dd_plannedorderno,
       dd_SalesOrderNo,
       dd_SalesOrderItemNo,
       dd_SalesOrderDeliveryScheduleNo,
       ct_ConfirmedReworkQty,
       ct_ScrapQty,
       ct_OrderItemQty,
       ct_TotalOrderQty,
       ct_GRQty,
       ct_GRProcessingTime,
       amt_estimatedTotalCost,
       amt_ValueGR,
       dd_BOMLevel,
       dd_sequenceno,
       dd_ObjectNumber,
       ct_ConfirmedScrapQty,
       dd_OrderDescription,
       Dim_MrpControllerId,
       dim_currencyid_TRA,
       dim_currencyid_GBL,
       dd_OperationNumber,
       dd_WorkCenter,
       dim_customerid
FROM TMP_001_productionorder;


Drop table if exists fact_productionorder_tmp_43;
Drop table if exists max_holder_43;

DELETE FROM fact_productionorder
WHERE EXISTS
          (SELECT 1
             FROM AFKO_AFPO_AUFK
            WHERE     AFKO_AUFNR = dd_ordernumber
                  AND AFPO_POSNR = dd_orderitemno
                  AND AFPO_XLOEK = 'X');
                  
DELETE FROM fact_productionorder
 WHERE EXISTS
          (SELECT 1
             FROM AFKO_AFPO_AUFK
            WHERE AFKO_AUFNR = dd_ordernumber AND AUFK_LOEKZ = 'X');


UPDATE fact_productionorder po
SET po.Dim_DateidRelease = ifnull(dr.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date dr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dr.DateValue = AUFK_IDAT1 AND mhi.AUFK_BUKRS = dr.CompanyCode
  AND po.Dim_DateidRelease <> ifnull(dr.dim_dateid, 1);

UPDATE fact_productionorder po
SET po.Dim_DateidBOMExplosion = ifnull(dr.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date dr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dr.DateValue = AFKO_AUFLD AND mhi.AUFK_BUKRS = dr.CompanyCode
  AND po.Dim_DateidBOMExplosion <> ifnull(dr.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidLastScheduling = ifnull(ls.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ls
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ls.DateValue = AFKO_TRMDT AND mhi.AUFK_BUKRS = ls.CompanyCode
  AND po.Dim_DateidLastScheduling <> ifnull(ls.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidTechnicalCompletion = ifnull(tc.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date tc
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND tc.DateValue = AUFK_IDAT2 AND mhi.AUFK_BUKRS = tc.CompanyCode
  AND po.Dim_DateidTechnicalCompletion <> ifnull(tc.dim_dateid, 1);

  UPDATE fact_productionorder po
   SET po.Dim_DateidBasicStart= ifnull(bst.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date bst
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bst.DateValue = AFKO_GSTRP AND mhi.AUFK_BUKRS = bst.CompanyCode
  AND po.Dim_DateidBasicStart <> ifnull(bst.dim_dateid, 1);

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledRelease = ifnull(sr.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date sr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sr.DateValue = AFKO_FTRMS AND mhi.AUFK_BUKRS = sr.CompanyCode
  AND po.Dim_DateidScheduledRelease <> ifnull(sr.dim_dateid, 1);

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledFinish = ifnull(sf.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date sf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sf.DateValue = AFPO_DGLTS AND mhi.AUFK_BUKRS = sf.CompanyCode
  AND po.Dim_DateidScheduledFinish <> ifnull(sf.dim_dateid, 1);

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledStart = ifnull(ss.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ss
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ss.DateValue = AFKO_GSTRS AND mhi.AUFK_BUKRS = ss.CompanyCode
  AND po.Dim_DateidScheduledStart <> ifnull(ss.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualStart = ifnull(ast.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ast
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ast.DateValue = AFKO_GSTRI AND mhi.AUFK_BUKRS = ast.CompanyCode
  AND po.Dim_DateidActualStart <> ifnull(ast.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidConfirmedOrderFinish = ifnull(cof.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date cof
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND cof.DateValue = AFKO_GETRI AND mhi.AUFK_BUKRS = cof.CompanyCode
  AND po.Dim_DateidConfirmedOrderFinish <> ifnull(cof.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualHeaderFinish = ifnull(ahf.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ahf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ahf.DateValue = AFKO_GLTRI AND mhi.AUFK_BUKRS = ahf.CompanyCode
  AND po.Dim_DateidActualHeaderFinish <> ifnull(ahf.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualRelease = ifnull(ar.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ar
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ar.DateValue = AFKO_FTRMI AND mhi.AUFK_BUKRS = ar.CompanyCode
  AND po.Dim_DateidActualRelease <> ifnull(ar.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualItemFinish = ifnull(aif.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date aif
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND aif.DateValue = AFPO_LTRMI AND mhi.AUFK_BUKRS = aif.CompanyCode
  AND po.Dim_DateidActualItemFinish <> ifnull(aif.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualItemFinish = ifnull(aif.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date aif
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND aif.DateValue = AFPO_LTRMI AND mhi.AUFK_BUKRS = aif.CompanyCode
  AND po.Dim_DateidActualItemFinish <> ifnull(aif.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidPlannedOrderDelivery = ifnull(pod.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date pod
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pod.DateValue = AFPO_LTRMP AND mhi.AUFK_BUKRS = pod.CompanyCode
  AND po.Dim_DateidPlannedOrderDelivery <> ifnull(pod.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidRoutingTransfer = ifnull(rt.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date rt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND rt.DateValue = AFKO_PLAUF AND mhi.AUFK_BUKRS = rt.CompanyCode
  AND po.Dim_DateidRoutingTransfer <> ifnull(rt.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidBasicFinish = ifnull(bf.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date bf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bf.DateValue = AFPO_DGLTP AND mhi.AUFK_BUKRS = bf.CompanyCode
  AND po.Dim_DateidBasicFinish <> ifnull(bf.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_ControllingAreaid = ifnull(ca.dim_controllingareaid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_ControllingArea ca
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ca.ControllingAreaCode = AUFK_KOKRS
  AND po.Dim_ControllingAreaid <> ifnull(ca.dim_controllingareaid, 1);

Drop table if exists dim_profitcenter_tmp_43;


Create table dim_profitcenter_tmp_43 as
Select  * from dim_profitcenter  ORDER BY ValidTo ASC;



UPDATE  fact_productionorder po
   SET po.Dim_ProfitCenterId = IFNULL(pc.Dim_ProfitCenterid,1)
FROM fact_productionorder po 
		INNER JOIN AFKO_AFPO_AUFK mhi ON po.dd_ordernumber = mhi.AFKO_AUFNR
									 AND po.dd_orderitemno = mhi.AFPO_POSNR
		LEFT JOIN dim_profitcenter_tmp_43 pc ON pc.ProfitCenterCode = AUFK_PRCTR
											AND pc.ControllingArea = AUFK_KOKRS
											AND pc.ValidTo >= AFKO_GSTRP;

drop table if exists dim_profitcenter_tmp_43;


UPDATE fact_productionorder po
   SET po.Dim_tasklisttypeid = ifnull(tlt.Dim_tasklisttypeid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_tasklisttype tlt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND tlt.TaskListTypeCode = AFKO_PLNTY 
  AND po.Dim_tasklisttypeid <> ifnull(tlt.Dim_tasklisttypeid, 1);

UPDATE fact_productionorder po
   SET po.Dim_PartidHeader = ifnull(phdr.Dim_Partid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part phdr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND phdr.PartNumber = AFKO_STLBEZ AND phdr.Plant = mhi.AUFK_WERKS
  AND po.Dim_PartidHeader <> ifnull(phdr.Dim_Partid, 1);

UPDATE fact_productionorder po
   SET po.Dim_bomstatusid = ifnull(bs.Dim_bomstatusid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_bomstatus bs
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bs.BOMStatusCode = AFKO_STLST 
  AND po.Dim_bomstatusid <> ifnull(bs.Dim_bomstatusid, 1);

UPDATE fact_productionorder po
   SET po.Dim_bomusageid = ifnull(bu.Dim_bomusageid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_bomusage bu
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bu.BOMUsageCode = AFKO_STLAN 
  aND po.Dim_bomusageid <> ifnull(bu.Dim_bomusageid, 1);

UPDATE fact_productionorder po
   SET po.dim_productionschedulerid = ifnull(ps.dim_productionschedulerid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_productionscheduler ps
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ps.ProductionScheduler = AFKO_FEVOR
  AND ps.Plant = mhi.AUFK_WERKS
  AND po.dim_productionschedulerid <> ifnull(ps.dim_productionschedulerid, 1);

UPDATE fact_productionorder po
   SET po.Dim_ProcurementID = ifnull(dpr.Dim_ProcurementID, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_procurement dpr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dpr.procurement = AFPO_BESKZ 
  AND po.Dim_ProcurementID <> ifnull(dpr.Dim_ProcurementID, 1);


UPDATE fact_productionorder po
   SET po.Dim_SpecialProcurementID = ifnull(spr.Dim_SpecialProcurementID, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_specialprocurement spr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND spr.specialprocurement = AFPO_PSOBS 
  AND po.Dim_SpecialProcurementID <> ifnull(spr.Dim_SpecialProcurementID,1);

UPDATE fact_productionorder po
   SET po.Dim_UnitOfMeasureid = ifnull(uom.Dim_UnitOfMeasureid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       Dim_UnitOfMeasure uom
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND uom.UOM = afpo_meins 
  AND po.Dim_UnitOfMeasureid <> ifnull(uom.Dim_UnitOfMeasureid,1);

UPDATE fact_productionorder po
   SET po.Dim_PartidItem = ifnull(pitem.Dim_Partid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part pitem
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pitem.PartNumber = mhi.AFPO_MATNR
       AND pitem.Plant = mhi.AFPO_DWERK 
       AND po.Dim_PartidItem <> ifnull(pitem.Dim_Partid,1);

UPDATE fact_productionorder po
   SET po.Dim_AccountCategoryid = ifnull(ac.Dim_AccountCategoryid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_accountcategory ac
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ac.Category = AFPO_KNTTP 
  AND po.Dim_AccountCategoryid <> ifnull(ac.Dim_AccountCategoryid,1);

UPDATE fact_productionorder po
   SET po.Dim_StockTypeid = ifnull(st.Dim_StockTypeid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_stocktype st
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND st.TypeCode = AFPO_INSMK 
  AND po.Dim_StockTypeid <> st.Dim_StockTypeid;

UPDATE fact_productionorder po
   SET po.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi, dim_storagelocation sl
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND sl.LocationCode = AFPO_LGORT
       AND sl.Plant = AFPO_DWERK
       AND po.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid,1);

UPDATE fact_productionorder po
   SET po.Dim_ConsumptionTypeid = ifnull(ct.Dim_ConsumptionTypeid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_consumptiontype ct
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ct.ConsumptionCode = AFPO_KZVBR 
  AND po.Dim_ConsumptionTypeid <> ifnull(ct.Dim_ConsumptionTypeid,1);

UPDATE fact_productionorder po
   SET po.dim_specialstockid = ifnull(spt.dim_specialstockid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_specialstock spt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND spt.specialstockindicator = AFPO_SOBKZ
  AND po.dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

UPDATE fact_productionorder po
   SET po.Dim_Plantid = ifnull(dp.Dim_Plantid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_plant dp
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dp.PlantCode = mhi.afpo_dwerk
  ANd  po.Dim_Plantid <> ifnull(dp.Dim_Plantid,1);

UPDATE fact_productionorder po
   SET po.dim_salesorgid = ifnull(so.dim_salesorgid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_salesorg so,
       dim_plant dp
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dp.PlantCode = mhi.afpo_dwerk
       AND so.SalesOrgCode = dp.SalesOrg
       AND  po.dim_salesorgid <> ifnull(so.dim_salesorgid,1);

UPDATE fact_productionorder po
   SET po.Dim_PurchaseOrgid = ifnull(porg.Dim_PurchaseOrgid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_purchaseorg porg,
       dim_plant dp
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND porg.PurchaseOrgCode = dp.PurchOrg
       AND dp.PlantCode = mhi.AUFK_WERKS
       AND po.Dim_PurchaseOrgid <> ifnull(porg.Dim_PurchaseOrgid, 1);

	   /*
UPDATE fact_productionorder po
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_Currency cur
   SET po.Dim_Currencyid = cur.Dim_Currencyid
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND cur.CurrencyCode = mhi.AUFK_WAERS */
       
UPDATE fact_productionorder po
   SET po.Dim_Companyid = ifnull(dc.Dim_Companyid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_Company dc
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dc.CompanyCode = mhi.AUFK_BUKRS
       AND po.Dim_Companyid <> ifnull(dc.Dim_Companyid, 1);
	   
/* Dim_Currencyid is now the local curr */	   
UPDATE fact_productionorder po
   SET po.Dim_Currencyid = ifnull(cur.Dim_Currencyid, 1)
FROM fact_productionorder po,dim_Company dc,
       dim_Currency cur
 WHERE     po.Dim_Companyid = dc.Dim_Companyid
 AND cur.CurrencyCode = dc.currency;	   


UPDATE fact_productionorder po
SET  ct_ConfirmedScrapQty = ifnull(AFKO_IASMG ,0.000)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND ct_ConfirmedScrapQty <> ifnull(AFKO_IASMG ,0.000);


UPDATE fact_productionorder po
   SET dd_OrderDescription = ifnull(AUFK_KTEXT,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dd_OrderDescription <>  ifnull(AUFK_KTEXT,'Not Set');
       
UPDATE fact_productionorder po
   SET po.dim_MrpControllerid = ifnull(mrp.dim_MrpControllerid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_MrpController mrp, dim_plant pl
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dim_plantid = pl.dim_plantid
       AND mhi.AFKO_DISPO IS  NOT NULL
       AND mrp.MRPController = mhi.AFKO_DISPO AND mrp.plant = pl.plantcode
       AND mrp.RowIsCurrent = 1
	AND po.dim_MrpControllerid <>  ifnull(mrp.dim_MrpControllerid, 1);
       
UPDATE fact_productionorder po
   SET po.dim_MrpControllerid = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND mhi.AFKO_DISPO IS  NULL
	AND  po.dim_MrpControllerid <> 1; 

UPDATE fact_productionorder po
   SET po.Dim_ordertypeid = ifnull(pot.Dim_productionordertypeid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       Dim_productionordertype pot
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND pot.typecode = AFPO_DAUAT AND pot.RowIsCurrent = 1
       AND po.Dim_ordertypeid <> ifnull(pot.Dim_productionordertypeid,1);

UPDATE fact_productionorder po
   SET po.dim_productionordermiscid  = ifnull(misc.dim_productionordermiscid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_productionordermisc misc
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND     CollectiveOrder = ifnull(AFKO_PRODNET, 'Not Set')
                  AND DeliveryComplete = ifnull(AFPO_ELIKZ, 'Not Set')
                  AND GRChangeIndicator = ifnull(AFPO_WEAED, 'Not Set')
                  AND GRIndicator = ifnull(AFPO_WEPOS, 'Not Set')
                  AND GRNonValue = ifnull(AFPO_WEUNB, 'Not Set')
                  AND NoAutoCost = ifnull(AFKO_NAUCOST, 'Not Set')
                  AND NoAutoSchedule = ifnull(AFKO_NAUTERM, 'Not Set')
                  AND NoCapacityRequirement = ifnull(AFKO_KBED, 'Not Set')
                  AND NonMRPMaterial = ifnull(AFPO_NDISR, 'Not Set')
                  AND NonMRPOrderItem = ifnull(AFPO_DNREL, 'Not Set')
                  AND NoPlannedCostCalculation = ifnull(AFKO_NOPCOST, 'Not Set')
                  AND OrderReleased = ifnull(AUFK_PHAS1, 'Not Set')
                  AND ReleasedToMRP = ifnull(AFPO_DFREI, 'Not Set')
                  AND SchedulingBreak = ifnull(AFKO_BREAKS, 'Not Set')
                  AND UnlimitedOverdelivery = ifnull(AFPO_UEBTK, 'Not Set')
                  AND ValueWorkRelevant = ifnull(AFKO_FLG_ARBEI, 'Not Set')
                  AND po.dim_productionordermiscid  <> ifnull(misc.dim_productionordermiscid,1);

UPDATE fact_productionorder po
SET dd_bomexplosionno = ifnull(AFPO_SERNR, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_bomexplosionno <>  ifnull(AFPO_SERNR, 'Not Set');

     


 UPDATE fact_productionorder po
   SET  dd_plannedorderno = ifnull(AFPO_PLNUM,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_plannedorderno <> ifnull(AFPO_PLNUM,'Not Set');



       UPDATE fact_productionorder po
   SET dd_SalesOrderNo = ifnull(AFPO_KDAUF,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_SalesOrderNo <> ifnull(AFPO_KDAUF,'Not Set');



       UPDATE fact_productionorder po
   SET dd_SalesOrderItemNo = ifnull(AFPO_KDPOS, 0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_SalesOrderItemNo <> ifnull(AFPO_KDPOS, 0);



       UPDATE fact_productionorder po
   SET dd_SalesOrderDeliveryScheduleNo = ifnull(AFPO_KDEIN, 0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_SalesOrderDeliveryScheduleNo <> ifnull(AFPO_KDEIN, 0);



       UPDATE fact_productionorder po
   SET ct_ConfirmedReworkQty = ifnull(AFKO_RMNGA,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_ConfirmedReworkQty <> ifnull(AFKO_RMNGA,0);



       UPDATE fact_productionorder po
   SET ct_ScrapQty = ifnull(AFPO_PSAMG,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_ScrapQty  <> ifnull(AFPO_PSAMG,0);



       UPDATE fact_productionorder po
   SET ct_OrderItemQty = ifnull(AFPO_PSMNG,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_OrderItemQty <> ifnull(AFPO_PSMNG,0);



       UPDATE fact_productionorder po
   SET ct_TotalOrderQty = ifnull(AFKO_GAMNG,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_TotalOrderQty  <> ifnull(AFKO_GAMNG,0);



       UPDATE fact_productionorder po
   SET ct_GRQty = ifnull(AFPO_WEMNG,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_GRQty  <> ifnull(AFPO_WEMNG,0);



       UPDATE fact_productionorder po
   SET ct_GRProcessingTime = ifnull(AFPO_WEBAZ,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_GRProcessingTime <> ifnull(AFPO_WEBAZ,0);



       UPDATE fact_productionorder po
   SET amt_estimatedTotalCost = ifnull(AUFK_USER4,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND amt_estimatedTotalCost <> ifnull(AUFK_USER4,0);


       UPDATE fact_productionorder po
   SET amt_ValueGR = ifnull(mhi.AFPO_WEWRT,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND  amt_ValueGR <> ifnull(mhi.AFPO_WEWRT,0);



       UPDATE fact_productionorder po
   SET Dim_BuildTypeid = (CASE WHEN AFPO_KDAUF IS NOT NULL THEN 1 ELSE 2 END)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND  Dim_BuildTypeid <> (CASE WHEN AFPO_KDAUF IS NOT NULL THEN 1 ELSE 2 END);


       UPDATE fact_productionorder po
   SET dd_BOMLevel = ifnull(AFKO_STUFE, 0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_BOMLevel <> ifnull(AFKO_STUFE, 0);


       UPDATE fact_productionorder po
   SET dd_sequenceno = ifnull(AFKO_CY_SEQNR,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_sequenceno <> ifnull(AFKO_CY_SEQNR,'Not Set');



UPDATE fact_productionorder po
   SET dd_ObjectNumber = ifnull(AUFK_OBJNR, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_ObjectNumber <> ifnull(AUFK_OBJNR, 'Not Set');


Drop table if exists subqry_po_001;

Create table subqry_po_001 as 
Select distinct dd_ObjectNumber,
CONVERT(varchar(20),'Not Set') Closed,
CONVERT(varchar(20),'Not Set') Delivered,
CONVERT(varchar(20),'Not Set') GoodsMovementPosted,
CONVERT(varchar(20),'Not Set') MaterialCommitted,
CONVERT(varchar(20),'Not Set') PreCosted,
CONVERT(varchar(20),'Not Set') SettlementRuleCreated, 
CONVERT(varchar(20),'Not Set') VariancesCalculated,
CONVERT(varchar(20),'Not Set') TechnicallyCompleted,
CONVERT(varchar(20),'Not Set') Created_col,
CONVERT(varchar(20),'Not Set') Released,
CONVERT(varchar(20),'Not Set') MaterialShortage,
CONVERT(varchar(20),'Not Set') PartPrinted,
CONVERT(varchar(20),'Not Set') PartiallyConfirmed,
CONVERT(varchar(20),'Not Set') InspectionLotAssigned,
CONVERT(varchar(20),'Not Set') MaterialAvailabilityNotChecked,
CONVERT(varchar(20),'Not Set') ResultsAnalysisCarriedOut
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
Where  po.dd_ordernumber = mhi.AFKO_AUFNR AND po.dd_orderitemno = mhi.AFPO_POSNR;


Update subqry_po_001 k
Set k.Closed = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0046' AND j.JEST_INACT is NULL;



Update subqry_po_001 k
Set k.Delivered = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0012' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.GoodsMovementPosted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0321' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialCommitted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0340' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PreCosted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0016' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.SettlementRuleCreated = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0028' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.VariancesCalculated = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0056' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.TechnicallyCompleted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0045' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.Created_col = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0001' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.Released = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0002' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialShortage = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0004' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PartPrinted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0008' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PartiallyConfirmed = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0010' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.InspectionLotAssigned = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0281' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialAvailabilityNotChecked = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0420' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.ResultsAnalysisCarriedOut = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0082' AND j.JEST_INACT is NULL;

UPDATE fact_productionorder po
SET po.dim_productionorderstatusid = ifnull(post.dim_productionorderstatusid, 1)
FROM fact_productionorder po,dim_productionorderstatus post,
     AFKO_AFPO_AUFK mhi,
     subqry_po_001 sq
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND sq.dd_ObjectNumber = po.dd_ObjectNumber
       AND post.Closed = sq.Closed
       AND post.Delivered = sq.Delivered
       AND post.GoodsMovementPosted = sq.GoodsMovementPosted
       AND post.MaterialCommitted =sq.MaterialCommitted
       AND post.PreCosted = sq.PreCosted
       AND post.SettlementRuleCreated =sq.SettlementRuleCreated 
       AND post.VariancesCalculated =sq.VariancesCalculated
       AND post.TechnicallyCompleted = sq.TechnicallyCompleted
       AND post."CREATED" = sq.Created_col
       AND post.Released = sq.Released
       AND post.MaterialShortage = sq.MaterialShortage
       AND post.PartPrinted = sq.PartPrinted
       AND post.PartiallyConfirmed = sq.PartiallyConfirmed
       AND post.InspectionLotAssigned = sq.InspectionLotAssigned
       AND post.MaterialAvailabilityNotChecked = sq.MaterialAvailabilityNotChecked
       AND post.ResultsAnalysisCarriedOut = sq.ResultsAnalysisCarriedOut;


UPDATE fact_productionorder po
SET amt_ExchangeRate = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
     dim_company dc
WHERE       po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS
AND ifnull(amt_ExchangeRate,-1) <> 1; 

UPDATE fact_productionorder po
SET amt_ExchangeRate = ifnull(ex.exchangeRate, 1)
FROM fact_productionorder po,dim_company dc,
     AFKO_AFPO_AUFK mhi,
	 tmp_getExchangeRate1 ex 
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS 
AND  pFromCurrency = AUFK_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = AUFK_AEDAT
and fact_script_name = 'bi_populate_prodorder_fact' 
AND amt_ExchangeRate <> ifnull(ex.exchangeRate, 1);


UPDATE fact_productionorder po
SET amt_ExchangeRate_GBL = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
     dim_company dc
WHERE       po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS
AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;  

UPDATE fact_productionorder po
SET amt_ExchangeRate_GBL = ifnull(ex.exchangeRate, 1)
FROM fact_productionorder po,dim_company dc,
     AFKO_AFPO_AUFK mhi,
	 tmp_getExchangeRate1 ex,
	 pGlobalCurrency_po_43
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS 
AND  pFromCurrency = AUFK_WAERS
and pToCurrency = pGlobalCurrency
and pFromExchangeRate = 0
and pDate =  current_date  /*AUFK_AEDAT - current date to be used for global rate*/
and fact_script_name = 'bi_populate_prodorder_fact'
AND amt_ExchangeRate <> ifnull(ex.exchangeRate, 1);

DROP TABLE IF EXISTS TMP_AUFM_UPDT;
CREATE TABLE TMP_AUFM_UPDT
AS
select AUFM_AUFNR, sum((case a.AUFM_SHKZG when 'S' then -1 else 1 end) * a.AUFM_DMBTR) sum_aufm
from AUFM a group by a.AUFM_AUFNR;

DROP TABLE IF EXISTS TMP_COSS_UPDT;
CREATE TABLE TMP_COSS_UPDT
AS
select COSS_OBJNR,sum(c.COSS_WOG001 + c.COSS_WOG002 + c.COSS_WOG003 + c.COSS_WOG004
                                    + c.COSS_WOG005 + c.COSS_WOG006 + c.COSS_WOG007 + c.COSS_WOG008
                                    + c.COSS_WOG009 + c.COSS_WOG010 + c.COSS_WOG011 + c.COSS_WOG012
                                    + c.COSS_WOG013 + c.COSS_WOG014 + c.COSS_WOG015 + c.COSS_WOG016) sum_cos
from coss c
where c.COSS_WRTTP = '04'
group by COSS_OBJNR;

UPDATE fact_productionorder po
SET po.amt_WIPBalance = ifnull(a.sum_aufm,0)
FROM fact_productionorder po
	LEFT JOIN TMP_AUFM_UPDT a ON po.dd_ordernumber = a.AUFM_AUFNR 
WHERE ifnull(amt_WIPBalance,-1) <> ifnull(a.sum_aufm,0);

UPDATE fact_productionorder po
SET po.amt_WIPBalance = ifnull(po.amt_WIPBalance,0) + ifnull(c.sum_cos,0)
FROM fact_productionorder po
	LEFT JOIN TMP_COSS_UPDT c ON 'OR' || po.dd_ordernumber = c.COSS_OBJNR
WHERE ifnull(amt_WIPBalance,-1) <> ifnull(po.amt_WIPBalance,0) + ifnull(c.sum_cos,0);

DROP TABLE IF EXISTS TMP_AUFM_UPDT;
DROP TABLE IF EXISTS TMP_COSS_UPDT;
						   
/* LK: 15 Sep 2013: aufm_dmbtr and coss amts are in local currency. So convert amts to transaction currency */
						   
UPDATE fact_productionorder po
SET amt_WIPBalance = ( amt_WIPBalance / amt_ExchangeRate )
WHERE amt_ExchangeRate <> 1;						   

DROP TABLE IF EXISTS FPO_TMP;
CREATE TABLE FPO_TMP
AS SELECT a.AUFM_AUFNR,sum(a.AUFM_MENGE) sm_aufmenge
from AUFM a
GROUP BY a.AUFM_AUFNR;

UPDATE fact_productionorder po
SET ct_WIPQty = ifnull(sm_aufmenge,0)
from fact_productionorder po, FPO_TMP a 
where a.AUFM_AUFNR = po.dd_ordernumber;
						 
UPDATE fact_productionorder po
SET dd_OperationNumber = ifnull(AFKO_VORUE, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_OperationNumber <>  ifnull(AFKO_VORUE, 'Not Set');

UPDATE fact_productionorder po
SET dd_WorkCenter = ifnull(AUFK_VAPLZ, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_WorkCenter <>  ifnull(AUFK_VAPLZ, 'Not Set');

DROP TABLE IF EXISTS tmp_salesorder;

CREATE TABLE tmp_salesorder AS
SELECT distinct dd_salesdocno,dd_Salesitemno,dim_customerid from fact_salesorder;

UPDATE fact_productionorder po
 SET po.Dim_customerid = ifnull(fso.Dim_customerid, 1)
FROM  fact_productionorder po,tmp_salesorder fso
WHERE     po.dd_salesorderno = fso.dd_salesdocno
     AND po.dd_salesorderitemno = fso.dd_salesitemno
     AND ifnull(po.Dim_customerid, -1) <> ifnull(fso.Dim_customerid, 1);
     
DROP TABLE IF EXISTS tmp_salesorder;
     
UPDATE fact_productionorder po
SET dd_OperationNumber = 'Not Set' WHERE dd_OperationNumber IS NULL;

UPDATE fact_productionorder po
SET dd_WorkCenter = 'Not Set' WHERE dd_WorkCenter IS NULL;

DROP TABLE IF EXISTS FPO_TMP2;
CREATE TABLE FPO_TMP2
AS SELECT r.RESB_AUFNR,sum(r.RESB_ERFMG) sm_resberfmg
from RESB r
GROUP BY r.RESB_AUFNR;

UPDATE fact_productionorder po
SET ct_qtyuoe = ifnull(sm_resberfmg,0)
FROM fact_productionorder po,FPO_TMP2 r
WHERE r.RESB_AUFNR = po.dd_ordernumber;

/* MDG Part */

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_PRDO;
CREATE TABLE TMP_DIM_MDG_PARTID_PRDO as
SELECT DISTINCT pr.dim_partid, md.dim_mdg_partid, md.partnumber
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18);

/*
UPDATE fact_productionorder f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM TMP_DIM_MDG_PARTID_PRDO tmp, fact_productionorder f
WHERE f.dim_partidheader = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1)
*/

/*update above replaced with merge due to 'unable to get stable set of rows error'
Alin GH - 18.02
*/

merge into fact_productionorder f
using(
select distinct f.fact_productionorderid, max(tmp.dim_mdg_partid) as dim_mdg_partid
FROM TMP_DIM_MDG_PARTID_PRDO tmp,fact_productionorder f
WHERE f.dim_partidheader = tmp.dim_partid
group by f.fact_productionorderid
) t
on t.fact_productionorderid = f.fact_productionorderid
when matched
then update set f.dim_mdg_partid = ifnull(t.dim_mdg_partid, 1)
where f.dim_mdg_partid <> ifnull(t.dim_mdg_partid, 1);

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_PRDO;


UPDATE fact_productionorder f
SET f.amt_ExchangeRate_GBL = 1
WHERE ifnull(f.amt_ExchangeRate_GBL,-1) <> 1;


UPDATE fact_productionorder f
SET f.amt_ExchangeRate_GBL = ifnull(r.exrate,1)
FROM fact_productionorder f, dim_currency dc, csv_oprate r
WHERE f.dim_currencyid = dc.dim_currencyid
	AND dc.CurrencyCode = r.fromc
	AND r.toc = (SELECT ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD'))
	AND ifnull(f.amt_ExchangeRate_GBL,-1) <> ifnull(r.exrate,1);
/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate;
create table tmp_updatedate as
select max(dw_update_date) dd_updatedate from  fact_productionorder;


update fact_productionorder f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_productionorder f, tmp_updatedate t
where f.dd_updatedate <> t.dd_updatedate;

/* 19 Apr 2018 CristianT Start: Added logic for Batch APP-8373 */
UPDATE fact_productionorder po
SET dd_batch = ifnull(a.AFPO_CHARG,'Not Set')
FROM fact_productionorder po,
     AFKO_AFPO_AUFK a
WHERE a.AFKO_AUFNR = po.dd_ordernumber
      AND a.AFPO_POSNR = po.dd_orderitemno
      AND ifnull(dd_batch, 'X') <> ifnull(a.AFPO_CHARG,'Not Set');

/* 19 Apr 2018 CristianT End */
