
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   18 May 2014      Hiten	1.17              Fix for duplicate records 	  */
/*   29 Sep 2013      Lokesh	1.1               Curr and Exchange rate changes	  */
/*   27 Sep 2013      Lokesh	1.0  		  Existing version taken from CVS */
/******************************************************************************************************************/

/* Marius Removes Script */
drop table if exists this_table_does_not_exist;
