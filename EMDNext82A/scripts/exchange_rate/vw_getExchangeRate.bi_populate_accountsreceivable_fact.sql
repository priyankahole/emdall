
/*   17 Sep 2013      Lokesh	1.1  		  Changed queries to use BSID and BSAD so that there is no need to split AR script */

/* IMPORTANT : THIS SHOULD BE RUN AFTER AR PART 1 AND BEFORE AR PART 2 */


/* Populate Tran->Global curr */
/* LK: As discussed with Hiten, from exchange rate should never be used for getting blobal rate */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_accountsreceivable_fact';

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSID_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSID_BUDAT pDate,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM   BSID arc;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAD_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSAD_BUDAT pDate,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM BSAD arc;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull( property_value,'USD')
from tmp_getExchangeRate1, systemproperty 
where property = 'customer.global.currency'
and fact_script_name = 'bi_populate_accountsreceivable_fact'
;

/* Insert Tran--> Local curr mappings - again with posting date */

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSID_WAERS pFromCurrency,dc.Currency pToCurrency,NULL pFromExchangeRate,BSID_BUDAT pDate,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM   BSID arc,dim_company dc
WHERE dc.CompanyCode = arc.BSID_BUKRS;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAD_WAERS pFromCurrency,dc.Currency pToCurrency,NULL pFromExchangeRate,BSAD_BUDAT pDate,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM BSAD arc,dim_company dc
WHERE dc.CompanyCode = arc.BSAD_BUKRS;

/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_fact_accountsreceivable_nodups;
create table tmp_getExchangeRate1_fact_accountsreceivable_nodups
as
select distinct * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_accountsreceivable_fact';

delete from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_accountsreceivable_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_fact_accountsreceivable_nodups;

drop table tmp_getExchangeRate1_fact_accountsreceivable_nodups;

