

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_materialmovement_fact';


/* Tran to Global */

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate, fact_script_name)
SELECT DISTINCT MSEG_WAERS as pFromCurrency,m.MKPF_BUDAT as pDate, 'USD' pToCurrency, NULL exchangeRate,'bi_populate_materialmovement_fact'
FROM MKPF_MSEG m,dim_plant dp,dim_company dc
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode;

INSERT  INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate, fact_script_name)
SELECT DISTINCT MSEG1_WAERS as pFromCurrency,m.MKPF_BUDAT as pDate, 'USD' pToCurrency, NULL  exchangeRate,'bi_populate_materialmovement_fact'
FROM MKPF_MSEG1 m,dim_plant dp,dim_company dc
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode;

/* Current date for global rate */
INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate, fact_script_name)
SELECT DISTINCT MSEG_WAERS as pFromCurrency,current_date as pDate, 'USD' pToCurrency, NULL exchangeRate,'bi_populate_materialmovement_fact'
FROM MKPF_MSEG m,dim_plant dp,dim_company dc
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode;

INSERT  INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate, fact_script_name)
SELECT DISTINCT MSEG1_WAERS as pFromCurrency,current_date as pDate, 'USD' pToCurrency, NULL  exchangeRate,'bi_populate_materialmovement_fact'
FROM MKPF_MSEG1 m,dim_plant dp,dim_company dc
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode;



UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_materialmovement_fact';

/* Tran to Local */

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name )
SELECT DISTINCT m.MSEG_WAERS as pFromCurrency, m.MKPF_BUDAT as pDate, dc.Currency pToCurrency, NULL  exchangeRate,'bi_populate_materialmovement_fact'
FROM MKPF_MSEG m,dim_plant dp,dim_company dc
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode;

INSERT  INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate, fact_script_name)
SELECT DISTINCT m.MSEG1_WAERS as pFromCurrency, m.MKPF_BUDAT as pDate, dc.Currency pToCurrency, NULL  exchangeRate,'bi_populate_materialmovement_fact'
FROM MKPF_MSEG1 m,dim_plant dp,dim_company dc
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode;


drop table if exists tmp_getExchangeRate1_nodups_mm;
create table tmp_getExchangeRate1_nodups_mm
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_materialmovement_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_materialmovement_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_mm;

drop table tmp_getExchangeRate1_nodups_mm;


