/* Standard proc funct_fiscal_year */
delete from number_fountain where table_name = 'check_processing_duration';
insert into number_fountain(table_name,max_id)
select 'check_processing_duration',ifnull((select max(id)+1 from check_processing_duration),1);

insert into check_processing_duration(id,fact,scrpt,startproc,endproc)
select (select max_id from number_fountain where table_name = 'check_processing_duration'), 'fact_inventory', 'vw_funct_fiscal_year.standard.sql', current_timestamp, null;

UPDATE tmp_funct_fiscal_year
SET processed_flag = 'Y'
WHERE processed_flag IS NULL;

UPDATE tmp_funct_fiscal_year
SET upd_flag = 'N'
WHERE processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year f
SET f.pPeriv = t.PERIV
FROM tmp_funct_fiscal_year f, T001 t
WHERE f.processed_flag = 'Y'
	AND t.BUKRS = f.pCompanyCode;

UPDATE tmp_funct_fiscal_year
SET pPrevPeriod = 12
	,pPrevFIYEAR = FiscalYear - 1
	,upd_flag = 'Q1A'
WHERE Period = 1
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pPrevPeriod = Period - 1
	,pPrevFIYEAR = FiscalYear
	,upd_flag = 'Q1B'
WHERE upd_flag = 'N'
AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year f
SET f.pVariant1 = t.XJABH
FROM tmp_funct_fiscal_year f, T009 t
WHERE f.processed_flag = 'Y'
	AND t.PERIV = f.pPeriv;

UPDATE tmp_funct_fiscal_year t
SET t.pVariant2 = s.XKALE
FROM tmp_funct_fiscal_year t, T009 s
WHERE t.processed_flag = 'Y'
	AND s.PERIV = t.pPeriv;


/**************************************************************************************MAIN IF SECTION ***************************/

DROP TABLE IF EXISTS TMP_FFY_T009B_1;
CREATE TABLE TMP_FFY_T009B_1
AS
SELECT 
	T.PERIV
	,T.POPER
	,T.BDATJ
	,T.RELJR
	,MIN(T.BUMON) min_BUMON
FROM T009B T, tmp_funct_fiscal_year R
WHERE T.PERIV = R.pPeriv AND T.POPER = R.Period
	AND ((T.BDATJ = R.FiscalYear AND T.RELJR = '0') OR (T.BDATJ = R.FiscalYear - 1 AND T.RELJR = '+1') OR (T.BDATJ = R.FiscalYear + 1 AND T.RELJR = '-1'))
	AND  R.processed_flag = 'Y'
GROUP BY T.PERIV,T.POPER,T.BDATJ,T.RELJR;

DROP TABLE IF EXISTS TMP_FFY_T009B_2;
CREATE TABLE TMP_FFY_T009B_2
AS
SELECT T.*
FROM T009B T,TMP_FFY_T009B_1 x
WHERE T.PERIV = x.PERIV
	AND T.POPER = x.POPER
	AND T.BDATJ = x.BDATJ
	AND T.RELJR = x.RELJR
	AND T.BUMON = x.min_BUMON;

UPDATE tmp_funct_fiscal_year f
SET f.pYearShift = t.RELJR
	,f.upd_flag = 'Q2A'
FROM tmp_funct_fiscal_year f, TMP_FFY_T009B_2 t
WHERE t.PERIV = f.pPeriv AND t.POPER = f.Period
	AND ((t.BDATJ = f.FiscalYear AND t.RELJR = '0') OR (t.BDATJ = f.FiscalYear - 1 AND t.RELJR = '+1') OR (t.BDATJ = f.FiscalYear + 1 AND t.RELJR = '-1'))
	AND f.pVariant1 = 'X'
	AND f.processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year f
SET f.pCalMth = t.min_BUMON
	,f.upd_flag = 'Q2B'
FROM tmp_funct_fiscal_year f, TMP_FFY_T009B_1 t
WHERE t.PERIV = f.pPeriv AND t.POPER = f.Period
	AND ((t.BDATJ = f.FiscalYear AND t.RELJR = '0') OR (t.BDATJ = f.FiscalYear - 1 AND t.RELJR = '+1') OR (t.BDATJ = f.FiscalYear + 1 AND t.RELJR = '-1'))
	AND f.pVariant1 = 'X'
	AND f.processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear - 1
	,upd_flag = 'Q2C'
WHERE pVariant1 = 'X'
	AND pYearShift = '+1'
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear + 1
	,upd_flag = 'Q2D'
WHERE pVariant1 = 'X'
	AND pYearShift = '-1'
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear
	,upd_flag = 'Q2E'
WHERE pVariant1 = 'X'
	AND upd_flag NOT IN ( 'Q2C','Q2D' )
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pcalyear_minus_1 = pCalYear - 1
WHERE processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pcalyear_plus_1 = pCalYear + 1
WHERE processed_flag = 'Y';

DROP TABLE IF EXISTS tmp_ffyr_minbutag;
CREATE TABLE tmp_ffyr_minbutag
AS
SELECT PERIV
	,POPER
	,BDATJ
	,BUMON
	,MIN(BUTAG) AS min_BUTAG
FROM T009B
GROUP BY PERIV,POPER,BDATJ,BUMON;

UPDATE tmp_funct_fiscal_year f
SET f.pCalMthDay = t.min_BUTAG
FROM tmp_funct_fiscal_year f, tmp_ffyr_minbutag t
WHERE t.PERIV = f.pPeriv AND t.POPER = f.Period AND t.BDATJ = f.pCalYear AND t.BUMON = f.pCalMth
	AND f.pVariant1 = 'X'
	AND f.processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
/* SET CalDate = pCalMthDay || '/' || pCalMth || '/' || pCalYear */
SET CalDate = TO_DATE(pCalMth || '-' || pCalMthDay || '-' || pCalYear, 'MM-DD-YYYY')
WHERE pVariant1 = 'X'
	AND processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_1;
CREATE TABLE TMP_T009B_1
as
/* SELECT PERIV,BDATJ,POPER,BUTAG||BUMON||BDATJ as tDT */
/* cast expects string in mm/dd/yyyy format */
SELECT t.PERIV
	,t.BDATJ
	,t.POPER
	,TO_DATE(t.BUMON || '-' || t.BUTAG || '-' || t.BDATJ,'MM-DD-YYYY') AS tDT
FROM T009B t, tmp_funct_fiscal_year f
WHERE t.PERIV = f.pPeriv 
	AND (t.BDATJ = f.pCalYear OR t.BDATJ = f.pCalYear - 1 OR t.BDATJ = f.pCalYear + 1)
	AND f.processed_flag = 'Y';


 /* if (pVariant1 = 'X') */

/*Update pFromDate */

/*UPDATE tmp_funct_fiscal_year f
SET pFromDate = ( SELECT MAX(TMP.tDT) + INTERVAL '1' DAY
                                        FROM TMP_T009B_1 TMP
                                        WHERE TMP.PERIV = f.pPeriv and (TMP.BDATJ = f.pCalYear or TMP.BDATJ = f.pCalYear - 1)
                        and TMP.POPER = 12
                        and TMP.tDT < f.CalDate )
WHERE f.pVariant1 = 'X'
AND f.Period = 1
AND f.processed_flag = 'Y'*/

DROP TABLE IF EXISTS tmp_update_pFromDate;
CREATE TABLE tmp_update_pFromDate
AS
SELECT f.pPeriv, f.pCalYear, f.CalDate, f.pVariant1, f.Period, f.processed_flag, MAX(t.tDT) + INTERVAL '1' DAY pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv 
		AND (t.BDATJ = f.pCalYear OR t.BDATJ = f.pCalYear - 1)
		AND t.POPER = 12
		AND t.tDT < f.CalDate
WHERE f.pVariant1 = 'X'
	AND f.Period = 1
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.pCalYear, f.CalDate, f.pVariant1, f.Period, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pFromDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pFromDate t
WHERE f.pPeriv = t.pPeriv
	AND f.pCalYear = t.pCalYear
	AND f.CalDate = t.CalDate
	AND f.pVariant1 = t.pVariant1
	AND f.Period = t.Period
	AND f.processed_flag = t.processed_flag;

/* UPDATE tmp_funct_fiscal_year
SET pFromDate = ( SELECT MAX(tDT) + INTERVAL '1' DAY
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv
                                            and (BDATJ = pCalYear or BDATJ = pCalYear - 1)
                        and POPER < Period
                        and tDT < CalDate )
WHERE pVariant1 = 'X'
AND Period > 1
AND  cast(processed_flag as varchar(1)) = 'Y' */	
	
DROP TABLE IF EXISTS tmp_update_pFromDate;
CREATE TABLE tmp_update_pFromDate
AS
SELECT f.pPeriv, f.pCalYear, f.Period, f.CalDate, f.pVariant1, f.processed_flag, MAX(t.tDT) + INTERVAL '1' DAY pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv
		and (t.BDATJ = f.pCalYear or t.BDATJ = f.pCalYear - 1)
		and t.POPER < f.Period
		and t.tDT < f.CalDate
WHERE f.pVariant1 = 'X'
	AND f.Period > 1
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.pCalYear, f.Period, f.CalDate, f.pVariant1, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pFromDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pFromDate t
WHERE f.pPeriv = t.pPeriv
	AND f.pCalYear = t.pCalYear
	AND f.Period = t.Period
	AND f.CalDate = t.CalDate
	AND f.pVariant1 = t.pVariant1
	AND f.processed_flag = t.processed_flag;

/*Update pToDate */

/*
UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MIN(tDT)
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear + 1)
                        and POPER = 1
                        and tDT > CalDate )
WHERE pVariant1 = 'X'
AND Period = 12
AND  cast(processed_flag as varchar(1)) = 'Y'
*/

DROP TABLE IF EXISTS tmp_update_pToDate;
CREATE TABLE tmp_update_pToDate
AS
SELECT f.pPeriv, f.pCalYear, f.CalDate, f.pVariant1, f.Period, f.processed_flag, MIN(t.tDT) pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv 
		AND (t.BDATJ = f.pCalYear OR t.BDATJ = f.pCalYear + 1)
		AND t.POPER = 1
		AND t.tDT > f.CalDate
WHERE f.pVariant1 = 'X'
	AND f.Period = 12
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.pCalYear, f.CalDate, f.pVariant1, f.Period, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pToDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pToDate t
WHERE f.pPeriv = t.pPeriv
	AND f.pCalYear = t.pCalYear
	AND f.CalDate = t.CalDate
	AND f.pVariant1 = t.pVariant1
	AND f.Period = t.Period
	AND f.processed_flag = t.processed_flag;

/* 
UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MIN(tDT)
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear + 1)
                        and POPER > Period
                        and tDT > CalDate )
WHERE pVariant1 = 'X'
AND Period < 12
AND  cast(processed_flag as varchar(1)) = 'Y'
*/

DROP TABLE IF EXISTS tmp_update_pToDate;
CREATE TABLE tmp_update_pToDate
AS
SELECT f.pPeriv, f.pCalYear, f.Period, f.CalDate, f.pVariant1, f.processed_flag, MIN(t.tDT) pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv
		and (t.BDATJ = f.pCalYear or t.BDATJ = f.pCalYear + 1)
		and t.POPER > f.Period
		and t.tDT > f.CalDate
WHERE f.pVariant1 = 'X'
	AND f.Period < 12
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.pCalYear, f.Period, f.CalDate, f.pVariant1, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pToDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pToDate t
WHERE f.pPeriv = t.pPeriv
	AND f.pCalYear = t.pCalYear
	AND f.Period = t.Period
	AND f.CalDate = t.CalDate
	AND f.pVariant1 = t.pVariant1
	AND f.processed_flag = t.processed_flag;
	
/*
UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MAX(tDT)
                                from TMP_T009B_1
                                WHERE PERIV = pPeriv and POPER = Period
                                and tDT < pToDate )

WHERE pVariant1 = 'X'
AND  cast(processed_flag as varchar(1)) = 'Y'
*/

DROP TABLE IF EXISTS tmp_update_pToDate;
CREATE TABLE tmp_update_pToDate
AS
SELECT f.pPeriv, f.Period, f.pToDate, f.pVariant1, f.processed_flag, MAX(t.tDT) pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv
		and t.POPER = f.Period
        and t.tDT < f.pToDate
WHERE f.pVariant1 = 'X'
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.Period, f.pToDate, f.pVariant1, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pToDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pToDate t
WHERE f.pPeriv = t.pPeriv
	AND f.Period = t.Period
	AND f.pToDate = t.pToDate
	AND f.pVariant1 = t.pVariant1
	AND f.processed_flag = t.processed_flag;

/*********************************************       ELSEIF SECTION      *********************************************************/
/********************************************(pVariant1 is null and pVariant2 is null)  ******************************************/

DROP TABLE IF EXISTS TMP_T009B_2a;
CREATE TABLE TMP_T009B_2a
AS
SELECT PERIV
	,POPER
	,BDATJ
	,MIN(BUMON) min_BUMON
FROM T009B
GROUP BY PERIV,POPER,BDATJ;

DROP TABLE IF EXISTS TMP_T009B_2;
CREATE TABLE TMP_T009B_2
AS
SELECT B.*
FROM T009B B,TMP_T009B_2a T
WHERE B.PERIV = T.PERIV
	AND B.POPER = T.POPER
	AND B.BDATJ = T.BDATJ
	AND B.BUMON = T.min_BUMON;

UPDATE tmp_funct_fiscal_year t0
SET t0.pYearShift = t1.RELJR
FROM tmp_funct_fiscal_year t0, TMP_T009B_2 t1
WHERE t0.pVariant1 IS NULL 
	AND t0.pVariant2 IS NULL
	AND t0.processed_flag = 'Y'
	AND t1.PERIV = t0.pPeriv 
	AND t1.POPER = t0.Period 
	AND t1.BDATJ = 0;

UPDATE tmp_funct_fiscal_year t0
SET t0.pCalMth = t1.min_BUMON
FROM tmp_funct_fiscal_year t0, TMP_T009B_2a t1
WHERE t0.pVariant1 IS NULL 
	AND t0.pVariant2 IS NULL
	AND t0.processed_flag = 'Y'
	AND t1.PERIV = t0.pPeriv 
	AND t1.POPER = t0.Period 
	AND t1.BDATJ = 0;

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear - 1
WHERE pYearShift = '+1'
	AND pVariant1 IS NULL 
	AND pVariant2 IS NULL
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear + 1
WHERE pYearShift = '-1'
	AND pVariant1 IS NULL 
	AND pVariant2 IS NULL
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear
WHERE pYearShift NOT IN ( '+1','-1')
	AND pVariant1 IS NULL 
	AND pVariant2 IS NULL
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pcalyear_minus_1 = pCalYear - 1
WHERE pVariant1 IS NULL 
	AND pVariant2 IS NULL
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pcalyear_plus_1 = pCalYear + 1
WHERE pVariant1 IS NULL 
	AND pVariant2 IS NULL
	AND processed_flag = 'Y';

DROP TABLE IF EXISTS tmp_ffyr_minbutag;
CREATE TABLE tmp_ffyr_minbutag
AS
SELECT PERIV
	,POPER
	,BDATJ
	,BUMON
	,min(BUTAG) as min_BUTAG
FROM T009B
GROUP BY PERIV,POPER,BDATJ,BUMON;

UPDATE tmp_funct_fiscal_year f
SET f.pCalMthDay = t.min_BUTAG
FROM tmp_funct_fiscal_year f, tmp_ffyr_minbutag t
WHERE t.PERIV = f.pPeriv 
	AND t.POPER = f.Period 
	AND t.BDATJ = 0
	AND t.BUMON = f.pCalMth
	AND f.pVariant1 IS NULL 
	AND f.pVariant2 IS NULL
	AND f.processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET CalDate = CASE WHEN MOD(pCalYear,4) <> 0 AND pCalMthDay || '-' || pCalMth = '29-2'
                   THEN '2-28-' || pCalYear
                /* else pCalMthDay || '/' || pCalMth || '/' || pCalYear */
                   ELSE pCalMth || '-' || pCalMthDay || '-' || pCalYear
              END
WHERE pVariant1 IS NULL 
	AND pVariant2 IS NULL
	AND processed_flag = 'Y'
	AND CASE WHEN MOD(pCalYear,4) <> 0 AND pCalMthDay || '-' || pCalMth = '29-2'
             THEN '2-28-' || pCalYear
          /* else pCalMthDay || '/' || pCalMth || '/' || pCalYear */
             ELSE pCalMth || '-' || pCalMthDay || '-' || pCalYear
        END <> '--';

DROP TABLE IF EXISTS TMP_T009B_3A;
CREATE TABLE TMP_T009B_3A
AS
SELECT 
	t.pPeriv
	,t.Period
	,t.CalDate
	,t.pCalYear
	,CASE WHEN MOD(t.pCalYear,4) <> 0 AND t0.BUTAG||'-'||t0.BUMON = '29-2'
          THEN TO_DATE(('2-28-' || t.pCalYear),'MM-DD-YYYY')  + INTERVAL '1' DAY
          ELSE TO_DATE((t0.BUMON||'-'||t0.BUTAG||'-'||t.pCalYear),'MM-DD-YYYY') + INTERVAL '1' DAY
     END tDT
FROM T009B t0, tmp_funct_fiscal_year t
WHERE t0.PERIV = t.pPeriv AND t0.BDATJ = 0
	AND ((t.Period > 1 AND t0.POPER < t.Period) OR (t.Period = 1 AND t0.POPER = 12))
	AND CalDate > CASE WHEN MOD(pCalYear,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
					   THEN TO_DATE(('2-28-' || pCalYear),'MM-DD-YYYY')  + INTERVAL '1' DAY
					   ELSE TO_DATE((BUMON||'-'||BUTAG||'-'||pCalYear),'MM-DD-YYYY') + INTERVAL '1' DAY
				  END
	AND t.pVariant1 IS NULL 
	AND t.pVariant2 IS NULL
	AND t.processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_3B;
CREATE TABLE TMP_T009B_3B
AS
SELECT t.pPeriv
	,t.Period
	,t.CalDate
	,t.pCalYear
	,MAX(tDT) max_tDT
FROM TMP_T009B_3A t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;

UPDATE  tmp_funct_fiscal_year x
SET x.pFromDate = y.max_tDT
	,upd_flag = upd_flag || 'E1'
FROM tmp_funct_fiscal_year x, TMP_T009B_3B y
WHERE x.pPeriv = y.pPeriv
	AND x.Period = y.Period
	AND x.CalDate = y.CalDate
	AND x.pCalYear = y.pCalYear
	AND x.pVariant1 IS NULL 
	AND x.pVariant2 IS NULL
	AND x.processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_4A;
CREATE TABLE TMP_T009B_4A
AS
SELECT t.pPeriv
	,t.Period
	,t.CalDate
	,t.pCalYear
	,TO_DATE(CASE WHEN MOD(t.pCalYear_minus_1,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                  THEN TO_DATE(('2-28-' || t.pCalYear_minus_1 ),'MM-DD-YYYY') + INTERVAL '1' DAY
                  ELSE TO_DATE((s.BUMON||'/'||s.BUTAG||'/'||t.pCalYear_minus_1),'MM-DD-YYYY') + INTERVAL '1' DAY
             END,'MM-DD-YYYY')  tDT
FROM T009B s, tmp_funct_fiscal_year t
WHERE s.PERIV = t.pPeriv 
	AND s.BDATJ = 0
	AND ((t.Period > 1 AND s.POPER < t.Period) OR (t.Period = 1 AND s.POPER = 12))
	AND t.CalDate >  CASE WHEN MOD(t.pCalYear_minus_1,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                              THEN TO_DATE(('2-28-' || t.pCalYear_minus_1 ),'MM-DD-YYYY') + INTERVAL '1' DAY
                              ELSE TO_DATE((s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear_minus_1),'MM-DD-YYYY') + INTERVAL '1' DAY
                         END
	AND t.pVariant1 IS NULL 
	AND t.pVariant2 IS NULL
	AND t.pFromDate IS NULL
	AND t.processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_4B;
CREATE TABLE TMP_T009B_4B
AS
SELECT t.pPeriv
	,t.Period
	,t.CalDate
	,t.pCalYear
	,MAX(tDT) max_tDT
FROM TMP_T009B_4A t
GROUP BY t.pPeriv,t.Period,t.CalDate,t.pCalYear;

UPDATE  tmp_funct_fiscal_year x
SET x.pFromDate = y.max_tDT
	,x.upd_flag = x.upd_flag || 'E2'
FROM tmp_funct_fiscal_year x, TMP_T009B_4B y
WHERE x.pPeriv = y.pPeriv
	AND x.Period = y.Period
	AND x.CalDate = y.CalDate
	AND x.pCalYear = y.pCalYear
	AND x.pVariant1 IS NULL 
	AND x.pVariant2 IS NULL
	AND x.pFromDate IS NULL
	AND x.processed_flag = 'Y';

/* To date queries now. */

/* To Date Q1 */

DROP TABLE IF EXISTS TMP_T009B_3A_TO;
CREATE TABLE TMP_T009B_3A_TO
AS
SELECT t.pPeriv
	,t.Period
	,t.CalDate
	,t.pCalYear
	,TO_DATE(CASE WHEN MOD(t.pCalYear,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2' 
                  THEN ('2-28-' || t.pCalYear) 
				  ELSE (s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear)
             END,'MM-DD-YYYY') tDT
FROM T009B s,tmp_funct_fiscal_year t
WHERE s.PERIV = t.pPeriv AND s.BDATJ = 0
	AND ((t.Period < 12 AND s.POPER > t.Period) OR (t.Period = 12 AND s.POPER = 1))
	AND t.CalDate < TO_DATE(CASE WHEN MOD(t.pCalYear,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                                 THEN ('2-28-' || t.pCalYear)
                                 ELSE (s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear)
                            END,'MM-DD-YYYY')
	AND t.pVariant1 IS NULL 
	AND t.pVariant2 IS NULL
	AND t.processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_3B_TO;
CREATE TABLE TMP_T009B_3B_TO
AS
SELECT t.pPeriv
	,t.Period
	,t.CalDate
	,t.pCalYear
	,MIN(tDT) min_tDT
FROM TMP_T009B_3A_TO t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;

UPDATE  tmp_funct_fiscal_year x
SET x.pToDate1 = y.min_tDT
	,x.upd_flag = x.upd_flag || 'F1'
FROM tmp_funct_fiscal_year x, TMP_T009B_3B_TO y
WHERE x.pPeriv = y.pPeriv
	AND x.Period = y.Period
	AND x.CalDate = y.CalDate
	AND x.pCalYear = y.pCalYear
	AND x.pVariant1 IS NULL 
	AND x.pVariant2 IS NULL
	AND x.processed_flag = 'Y';

/* Q2 */

DROP TABLE IF EXISTS TMP_T009B_4A_TO;
CREATE TABLE TMP_T009B_4A_TO
AS
SELECT t.pPeriv
	,t.Period
	,t.CalDate
	,t.pCalYear
	,TO_DATE(CASE WHEN MOD(t.pCalYear_plus_1,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                  THEN ('2-28-' || t.pCalYear_plus_1 )
                  ELSE (s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear_plus_1)
             END,'MM-DD-YYYY') tDT
FROM T009B s, tmp_funct_fiscal_year t
WHERE s.PERIV = t.pPeriv AND s.BDATJ = 0
	AND ((t.Period < 12 AND s.POPER > t.Period) OR (t.Period = 12 AND s.POPER = 1))
	AND t.CalDate <  TO_DATE(CASE WHEN MOD(t.pCalYear_plus_1,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                                THEN ('2-28-' || t.pCalYear_plus_1 )
                                ELSE (s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear_plus_1)
                           END,'MM-DD-YYYY') 
	AND t.pVariant1 is null 
	and t.pVariant2 is null
	AND t.pToDate1 is null
	AND t.processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_4B_TO;
CREATE TABLE TMP_T009B_4B_TO
AS
SELECT  t.pPeriv
	,t.Period
	,t.CalDate
	,t.pCalYear
	,MIN(tDT) min_tDT
FROM TMP_T009B_4A_TO t
GROUP BY t.pPeriv,t.Period,t.CalDate,t.pCalYear;

UPDATE  tmp_funct_fiscal_year x
SET x.pToDate1 = y.min_tDT
	,upd_flag = upd_flag || 'F2'
FROM tmp_funct_fiscal_year x, TMP_T009B_4B_TO y
WHERE x.pPeriv = y.pPeriv
	AND x.Period = y.Period
	AND x.CalDate = y.CalDate
	AND x.pCalYear = y.pCalYear
	AND x.pVariant1 IS NULL 
	AND x.pVariant2 IS NULL
	AND x.pToDate1 IS NULL
	AND x.processed_flag = 'Y';

/* To Date Q3 */

DROP TABLE IF EXISTS TMP_TO_3;
CREATE TABLE TMP_TO_3
AS
SELECT t.pPeriv
	,t.Period
	,t.pToDate1
	,t.pCalYear
	,to_date(case when MOD(t.pCalYear_plus_1,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                  THEN ('2-28-' || t.pCalYear_plus_1 )
                  ELSE (s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear_plus_1)
             END,'MM-DD-YYYY') tDT
FROM T009B s, tmp_funct_fiscal_year t
WHERE s.PERIV = t.pPeriv AND s.BDATJ = 0 AND s.POPER = t.Period
	AND ((t.Period < 12 and s.POPER > t.Period) OR (t.Period = 12 AND s.POPER = 1))
	AND t.pToDate1 >  TO_DATE(CASE WHEN MOD(t.pCalYear_plus_1,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                                 THEN ('2-28-' || t.pCalYear_plus_1 )
                                 ELSE (s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear_plus_1)
                              END,'MM-DD-YYYY') 
	AND t.pVariant1 IS NULL 
	AND t.pVariant2 IS NULL
	AND t.processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_TO_3B;
CREATE TABLE TMP_TO_3B
AS
SELECT  t.pPeriv
	,t.Period
	,t.pToDate1
	,t.pCalYear
	,MAX(tDT) max_tDT
FROM TMP_TO_3 t
GROUP BY t.pPeriv,t.Period,t.pToDate1,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
SET x.pToDate = y.max_tDT
	,upd_flag = upd_flag || 'G1'
FROM tmp_funct_fiscal_year x, TMP_TO_3B y
WHERE x.pPeriv = y.pPeriv
	AND x.Period = y.Period
	AND x.pToDate1 = y.pToDate1
	AND x.pCalYear = y.pCalYear
	AND x.pVariant1 IS NULL 
	AND x.pVariant2 IS NULL
	AND x.processed_flag = 'Y';


/* To Date Q4 */

DROP TABLE IF EXISTS TMP_TO_4;
CREATE TABLE TMP_TO_4
AS
SELECT t.pPeriv
	,t.Period
	,t.pToDate1
	,t.pCalYear
	,TO_DATE(CASE WHEN MOD(t.pCalYear,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                  THEN ('2-28-' || t.pCalYear )
                  ELSE (s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear)
             END,'MM-DD-YYYY') tDT
FROM T009B s, tmp_funct_fiscal_year t
WHERE s.PERIV = t.pPeriv 
	and s.BDATJ = 0 
	and s.POPER = t.Period
	AND t.pToDate1 >  TO_DATE(CASE WHEN MOD(t.pCalYear,4) <> 0 AND s.BUTAG||'-'||s.BUMON = '29-2'
                                 THEN ('2-28-' || t.pCalYear )
                                 ELSE (s.BUMON||'-'||s.BUTAG||'-'||t.pCalYear)
                            END,'MM-DD-YYYY')
	AND t.pVariant1 IS NULL 
	AND t.pVariant2 IS NULL
	AND t.processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_TO_4B;
CREATE TABLE TMP_TO_4B
AS
SELECT t.pPeriv
	,t.Period
	,t.pToDate1
	,t.pCalYear
	,MAX(tDT) max_tDT
FROM TMP_TO_4 t
GROUP BY t.pPeriv,t.Period,t.pToDate1,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
SET x.pToDate = y.max_tDT
	,x.upd_flag = x.upd_flag || 'G2'
FROM tmp_funct_fiscal_year x,TMP_TO_4B y
WHERE x.pPeriv = y.pPeriv
	AND x.Period = y.Period
	AND x.pToDate1 = y.pToDate1
	AND x.pCalYear = y.pCalYear
	AND x.pVariant1 IS NULL 
	AND x.pVariant2 IS NULL
	AND x.pToDate IS NULL
	AND x.processed_flag = 'Y';


/************ELSE SECTION ***************************/

UPDATE tmp_funct_fiscal_year
/*set pFromDate = '01/' || Period ||  '/' || FiscalYear*/
SET pFromDate = FiscalYear ||  '-' || Period || '-01'
WHERE IFNULL(pVariant1,'Y') <> 'X' 
	AND ( pVariant1 IS NOT NULL OR pVariant2 IS NOT NULL )
	AND Period >=1 AND Period <= 12
	AND FiscalYear >= 1900
	AND processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pToDate = TO_DATE(pFromDate,'MM-DD-YYYY') + INTERVAL '1' MONTH - INTERVAL '1' DAY
WHERE IFNULL(pVariant1,'Y') <> 'X' 
	AND ( pVariant1 IS NOT NULL or pVariant2 IS NOT NULL )
	AND processed_flag = 'Y';

/************End of if-elseif-else ***************************/

UPDATE tmp_funct_fiscal_year
SET pReturn = CAST(pFromDate AS CHAR(20)) || '|' || CAST(pToDate AS CHAR(20))
WHERE processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_FFY_T009B_1;
DROP TABLE IF EXISTS TMP_FFY_T009B_2;
DROP TABLE IF EXISTS TMP_T009B_1;
DROP TABLE IF EXISTS TMP_T009B_2a;
DROP TABLE IF EXISTS TMP_T009B_2;
DROP TABLE IF EXISTS TMP_T009B_3A;
DROP TABLE IF EXISTS TMP_T009B_3B;
DROP TABLE IF EXISTS TMP_T009B_4A;
DROP TABLE IF EXISTS TMP_T009B_4B;
DROP TABLE IF EXISTS TMP_T009B_3A_TO;
DROP TABLE IF EXISTS TMP_T009B_3B_TO;
DROP TABLE IF EXISTS TMP_T009B_4A_TO;
DROP TABLE IF EXISTS TMP_T009B_4B_TO;
DROP TABLE IF EXISTS TMP_TO_3;
DROP TABLE IF EXISTS TMP_TO_3B;
DROP TABLE IF EXISTS TMP_TO_4;
DROP TABLE IF EXISTS TMP_TO_4B;
DROP TABLE IF EXISTS tmp_ffyr_minbutag;

UPDATE tmp_funct_fiscal_year
SET processed_flag = 'D'
WHERE processed_flag = 'Y';

update check_processing_duration
set endproc = current_timestamp
where id = (select max_id from number_fountain where table_name = 'check_processing_duration');
