/*********************************************Change History*******************************************************/
/*Date             By        Version           Desc                                                            */
/*   5 Feb 2015      Liviu Ionescu              Add Combine for table LIKP_LIPS_VBUP, dim_salesorderitemstatus */
/******************************************************************************************************************/

drop table if exists tmp_del1_LIKP_LIPS_VBUK;
create table tmp_del1_LIKP_LIPS_VBUK
as
select distinct LIKP_VBELN,LIPS_POSNR from LIKP_LIPS
union
select distinct dd_SalesDlvrDocNo,dd_SalesDlvrItemNo from fact_salesorderdelivery;

  delete from LIKP_LIPS_VBUP
  where not exists (select 1 from tmp_del1_LIKP_LIPS_VBUK where VBUP_VBELN = LIKP_VBELN and VBUP_POSNR = LIPS_POSNR);

  UPDATE dim_salesorderitemstatus
   SET OverallReferenceStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
	FROM LIKP_LIPS_VBUP, dd07t t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA
  AND  OverallReferenceStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

  UPDATE dim_salesorderitemstatus
   SET ConfirmationStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, dd07t t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA
  AND ConfirmationStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

    UPDATE dim_salesorderitemstatus
   SET DeliveryStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, dd07t t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA
  AND  DeliveryStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

  UPDATE dim_salesorderitemstatus
   SET OverallDeliveryStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, dd07t t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA
  AND  OverallDeliveryStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

    UPDATE dim_salesorderitemstatus
   SET      BillingStatusDeliveryRelated = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM (SELECT DISTINCT VBUP_FKSTA, VBUP_POSNR, VBUP_VBELN  FROM LIKP_LIPS_VBUP), (SELECT MAX(DD07T_DDTEXT) DD07T_DDTEXT,DD07T_DOMNAME,DD07T_DOMVALUE FROM DD07t GROUP BY DD07T_DOMNAME,DD07T_DOMVALUE) t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA
  AND BillingStatusDeliveryRelated <> ifnull(t.DD07T_DDTEXT,'Not Set');

       UPDATE dim_salesorderitemstatus
   SET   BillingStatusOrderRelated = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA
  AND BillingStatusOrderRelated <> ifnull(t.DD07T_DDTEXT,'Not Set');

      UPDATE dim_salesorderitemstatus
   SET    ItemRejectionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA
  AND ItemRejectionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

      UPDATE dim_salesorderitemstatus
   SET    OverallProcessingStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM (SELECT DISTINCT VBUP_GBSTA, VBUP_POSNR, VBUP_VBELN  FROM LIKP_LIPS_VBUP), DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA
  AND OverallProcessingStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

      UPDATE dim_salesorderitemstatus
   SET    GeneralIncompletionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL
  AND GeneralIncompletionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

       UPDATE dim_salesorderitemstatus
   SET   DelayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA
  AND DelayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

       UPDATE dim_salesorderitemstatus
   SET   PODStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA
  AND PODStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

     /* UPDATE dim_salesorderitemstatus
   SET    GoodsMovementStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA
  AND GoodsMovementStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') */
  
  
  drop table if exists update_goods_movStatus;
create table update_goods_movStatus
as select distinct VBUP_VBELN,VBUP_POSNR,ifnull(t.DD07T_DDTEXT,'Not Set') as DD07T_DDTEXT
from  LIKP_LIPS_VBUP, DD07T t
where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA;

update dim_salesorderitemstatus ds
   SET  GoodsMovementStatus = ifnull(u.DD07T_DDTEXT,'Not Set')
from dim_salesorderitemstatus ds,update_goods_movStatus u
where ds.SalesDocumentNumber = VBUP_VBELN and ds.SalesItemNumber = VBUP_POSNR
and GoodsMovementStatus <> ifnull(u.DD07T_DDTEXT,'Not Set');
  
  

      UPDATE dim_salesorderitemstatus
   SET    PickingPutawayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA
  AND PickingPutawayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');
  
  UPDATE dim_salesorderitemstatus
   SET    ItemDataforDeliv = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK
  AND ItemDataforDeliv <> ifnull(t.DD07T_DDTEXT,'Not Set');

  
   UPDATE dim_salesorderitemstatus
   SET    Customerres1Itemstatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
        FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVP01
  AND Customerres1Itemstatus <> ifnull(t.DD07T_DDTEXT,'Not Set');
  
delete from number_fountain m where m.table_name = 'dim_salesorderitemstatus';

insert into number_fountain
select 	'dim_salesorderitemstatus',
	ifnull(max(d.dim_salesorderitemstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemstatus d
where d.dim_salesorderitemstatusid <> 1; 

  INSERT
  INTO dim_salesorderitemstatus(
	  dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
		  ItemDataforDeliv,
		  Customerres1Itemstatus
          )
   SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by dtbl1.VBUP_VBELN,dtbl1.VBUP_POSNR)),dtbl1.*
 FROM (SELECT  distinct
          VBUP_VBELN,
          VBUP_POSNR,
		  ifnull(t1.DD07T_DDTEXT, 'Not Set'), 
		  ifnull(t2.DD07T_DDTEXT, 'Not Set'), 		  
		  ifnull(t3.DD07T_DDTEXT, 'Not Set'), 		  
		  ifnull(t4.DD07T_DDTEXT, 'Not Set'), 		  
		  ifnull(t5.DD07T_DDTEXT, 'Not Set'), 		  
		  ifnull(t6.DD07T_DDTEXT, 'Not Set'), 		  
		  ifnull(t7.DD07T_DDTEXT, 'Not Set'), 		  
		  ifnull(t8.DD07T_DDTEXT, 'Not Set'), 		  
		  ifnull(t9.DD07T_DDTEXT, 'Not Set'),		  
		  ifnull(t10.DD07T_DDTEXT, 'Not Set'),		  
		  ifnull(t11.DD07T_DDTEXT, 'Not Set'),		  
		  ifnull(t12.DD07T_DDTEXT, 'Not Set'),		  
		  ifnull(t13.DD07T_DDTEXT, 'Not Set'),		  
		  ifnull(t14.DD07T_DDTEXT, 'Not Set'),		  
		  ifnull(t15.DD07T_DDTEXT, 'Not Set'),
		  ifnull(t16.DD07T_DDTEXT, 'Not Set')
  FROM LIKP_LIPS_VBUP ds
		inner join LIKP_LIPS t0 on t0.LIKP_VBELN = VBUP_VBELN and t0.LIPS_POSNR = VBUP_POSNR
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t1  on  t1.DD07T_DOMVALUE = ds.VBUP_RFGSA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t2  on  t2.DD07T_DOMVALUE = ds.VBUP_BESTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t3  on  t3.DD07T_DOMVALUE = ds.VBUP_LFSTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t4  on  t4.DD07T_DOMVALUE = ds.VBUP_LFGSA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t5  on  t5.DD07T_DOMVALUE = ds.VBUP_FKSTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t6  on  t6.DD07T_DOMVALUE = ds.VBUP_FKSAA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t7  on  t7.DD07T_DOMVALUE = ds.VBUP_ABSTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t8  on  t8.DD07T_DOMVALUE = ds.VBUP_GBSTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t9  on  t9.DD07T_DOMVALUE = ds.VBUP_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t10 on t10.DD07T_DOMVALUE = ds.VBUP_DCSTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t11 on t11.DD07T_DOMVALUE = ds.VBUP_PDSTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t12 on t12.DD07T_DOMVALUE = ds.VBUP_WBSTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t13 on t13.DD07T_DOMVALUE = ds.VBUP_KOSTA
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t14 on t14.DD07T_DOMVALUE = ds.VBUP_LSSTA		
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t15 on t15.DD07T_DOMVALUE = ds.VBUP_UVVLK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t16 on t16.DD07T_DOMVALUE = ds.VBUP_UVP01
  WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR)) dtbl1;
