
DELETE FROM emd586.dim_part WHERE projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );


/*insert all rows*/
INSERT INTO emd586.dim_part
(
	 dim_partid
	,plant
	,partdescription
	,partnumber
	,partnumber_noleadzero
	,leadtime
	,unitofmeasure
	,safetystock
	,mrpcontroller
	,mrpcontrollerdescription
	,parttype
	,productgroupsbu
	,producthierarchy
	,rowstartdate
	,rowenddate
	,rowiscurrent
	,rowchangereason
	,projectsourceid
	,mtomts
	,org_description	
	,abcindicator
	,abcgroup
	,abcsegments
    ,standardunitprice	
	,HAZARDOUSMATERIALNUMBER
	,HAZARDOUS_CLASS
	,HAZARDOUS_DESCRIPTION
	,laboratorydesc
	,abcdesc
	,gpsname
	,ownership
	,MDGM_MATERIAL_NO
	,SBU_PLANNING_VALUE
	,GPH_PLANNING_VALUE
	,MATERIAL_SOURCE_SUBSIDIARY
	,PRODUCT_MANAGER
	,GLOBAL_REVENUE_REC_CAT
	,LAUNCH_DATE
)
SELECT
		 dim_ora_invproductid + ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM emdoracle4ae.dim_projectsource s),1) dim_ora_invproductid 
		,segment16 plantcode--CONVERT(VARCHAR(50),organization_id ) organization_id
		,SUBSTR(description,1,50) description
		,segment1
		,segment1
		,full_lead_time
		,primary_uom_code
		,safety_stock_quantity
		,planner_code
		,SUBSTR(plannername,1,18) planner_description --,SUBSTR(planner_description,1,18) planner_description
		,item_type
		,productgroupsbu
		,producthierarchy
		,rowstartdate
		,rowenddate
		,rowiscurrent
		,rowchangereason
		,(SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s )
		,CASE WHEN item_status = 'STOCK' THEN 'MTS' ELSE 'MTO' END
	    ,org_description	
		,abc_indicator
		,abcgroup
		,abcsegments
		,1 standardunitprice
		,HAZARDOUS_UN_NUMBER
		,HAZARDOUS_CLASS
		,HAZARDOUS_Description
		,dd_laboratorydesc
		,abcdesc
		,'Not Set' gpsname
		,'Not Set' ownership
		,'Not Set' MDGM_MATERIAL_NO
		,'Not Set' SBU_PLANNING_VALUE
		,'Not Set' GPH_PLANNING_VALUE
		,'Not Set' MATERIAL_SOURCE_SUBSIDIARY
		,'Not Set' PRODUCT_MANAGER
		,'Not Set' GLOBAL_REVENUE_REC_CAT
		,to_date('0001-01-01','YYYY-MM-DD') LAUNCH_DATE
FROM emdoracle4ae.dim_ora_invproduct d;

update emd586.Dim_Part
set numberofperiods2 = cast(numberofperiods as decimal(18,0))
where numberofperiods2 <> cast(numberofperiods as decimal(18,0))
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.Dim_Part
set productgroupsbu2 = REPLACE(productgroupsbu,'SBU-','')
where productgroupsbu2 <> REPLACE(productgroupsbu,'SBU-','')
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.Dim_Part
set ProfitCenterCode2 = TRIM(LEADING '0' FROM ProfitCenterCode)
where ProfitCenterCode2 <> TRIM(LEADING '0' FROM ProfitCenterCode)
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.Dim_Part
set DaysofCoverage = case when lotsizeindicator ='W' THEN cast(numberofperiods * 7 as decimal(18,0)) when lotsizeindicator ='M' THEN cast(numberofperiods * 30 as decimal(18,0)) when lotsizeindicator ='T' THEN cast(numberofperiods as decimal(18,0)) ELSE 0 END
where DaysofCoverage <> case when lotsizeindicator ='W' THEN cast(numberofperiods * 7 as decimal(18,0)) when lotsizeindicator ='M' THEN cast(numberofperiods * 30 as decimal(18,0)) when lotsizeindicator ='T' THEN cast(numberofperiods as decimal(18,0)) ELSE 0 END
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );

update emd586.Dim_Part
set Coverageprofilecalendardays = case when RangeOfCoverage ='Not Set' THEN 0 when periodindicator ='M' AND typeperiodlength=2 THEN   cast(targetcoverage1 as decimal(18,0)) when periodindicator ='M' AND typeperiodlength=3 THEN cast(targetcoverage1 as decimal(18,0))  * 30  when periodindicator ='W' AND typeperiodlength=1 THEN cast(targetcoverage1 / 5 * 7 as decimal(18,0)) when periodindicator ='W' AND typeperiodlength=3 THEN cast(targetcoverage1 as decimal(18,0))  *30 END
where Coverageprofilecalendardays <> case when RangeOfCoverage ='Not Set' THEN 0 when periodindicator ='M' AND typeperiodlength=2 THEN   cast(targetcoverage1 as decimal(18,0)) when periodindicator ='M' AND typeperiodlength=3 THEN cast(targetcoverage1 as decimal(18,0))  * 30  when periodindicator ='W' AND typeperiodlength=1 THEN cast(targetcoverage1 / 5 * 7 as decimal(18,0)) when periodindicator ='W' AND typeperiodlength=3 THEN cast(targetcoverage1 as decimal(18,0))  *30 END
AND   projectsourceid = (SELECT s.dim_projectsourceid FROM emdoracle4ae.dim_projectsource s );
