INSERT INTO dim_customermastersales(dim_customermastersalesid,RowIsCurrent)
   SELECT 1,1
     FROM ( SELECT 1 )  AS t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customermastersales
               WHERE dim_customermastersalesid = 1);

/*Update Process */
UPDATE dim_customermastersales 
   SET ABCClassification = ifnull(KNVV_KLABC, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND ABCClassification <> ifnull(KNVV_KLABC, 'Not Set');

UPDATE dim_customermastersales 
   SET AccountAssignmentGroup = ifnull(KNVV_KTGRD, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND AccountAssignmentGroup <> ifnull(KNVV_KTGRD, 'Not Set');

UPDATE dim_customermastersales 
   SET AccountAssignmentGroupDesciption = ifnull(TVKTT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVKTT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND tvktt.TVKTT_KTGRD = KNVV_KTGRD
       AND AccountAssignmentGroupDesciption <> ifnull(TVKTT_VTEXT, 'Not Set');

UPDATE dim_customermastersales 
   SET SalesOffice = ifnull(knvv_VKBUR, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND SalesOffice <> ifnull(knvv_VKBUR, 'Not Set');

UPDATE dim_customermastersales 
   SET SalesOfficeDescription = ifnull(TVKBT_BEZEI, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVKBT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_VKBUR = TVKBT_VKBUR
       AND SalesOfficeDescription <> ifnull(TVKBT_BEZEI, 'Not Set');

UPDATE dim_customermastersales 
   SET SalesDistrict = ifnull(KNVV_BZIRK, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND SalesDistrict <> ifnull(KNVV_BZIRK, 'Not Set');

UPDATE dim_customermastersales 
   SET SalesDistrictDescription = ifnull(T171T_BZTXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, T171T, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_BZIRK = T171T_BZIRK
       AND SalesDistrictDescription <> ifnull(T171T_BZTXT, 'Not Set');

UPDATE dim_customermastersales 
   SET CustomerGroup = ifnull(KNVV_KDGRP, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND CustomerGroup <> ifnull(KNVV_KDGRP, 'Not Set');

UPDATE dim_customermastersales 
   SET CustomerGroupDescription = ifnull(T151T_KTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, T151T, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND knvv.knvv_KDGRP = T151T_KDGRP
       AND CustomerGroupDescription <> ifnull(T151T_KTEXT, 'Not Set');

UPDATE dim_customermastersales 
   SET PaymentTerms = ifnull(KNVV_ZTERM, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND PaymentTerms <> ifnull(KNVV_ZTERM, 'Not Set');

UPDATE dim_customermastersales 
   SET CustomerName = ifnull(k.KNA1_NAME1, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, KNA1 k, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNA1_KUNNR = knvv.knvv_KUNNR
       AND CustomerName <> ifnull(k.KNA1_NAME1, 'Not Set');

UPDATE dim_customermastersales 
   SET DistributionChannelDescription = ifnull(TVTWT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVTWT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVTWT_VTWEG = knvv.KNVV_VTWEG
       AND DistributionChannelDescription <> ifnull(TVTWT_VTEXT, 'Not Set');

UPDATE dim_customermastersales 
   SET Division = ifnull(TSPAT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TSPAT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TSPAT_SPART = knvv.KNVV_SPART
       AND Division <> ifnull(TSPAT_VTEXT, 'Not Set');

UPDATE dim_customermastersales 
   SET SalesOrgDescription = ifnull(TVKOT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVKOT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVKOT_VKORG = knvv.KNVV_VKORG
       AND SalesOrgDescription <> ifnull(TVKOT_VTEXT, 'Not Set');


UPDATE dim_customermastersales 
   SET Customergroup3 = ifnull(KNVV_KVGR3, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND ifnull(Customergroup3,'X') <> ifnull(KNVV_KVGR3, 'Not Set')
       AND Customergroup3 <> ifnull(KNVV_KVGR3, 'Not Set');
       
 UPDATE dim_customermastersales 
 SET deletionFlag = ifnull(KNVV_LOEVM, 'Not Set'),
 			dw_update_date = current_timestamp
 FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND deletionFlag <> ifnull(KNVV_LOEVM, 'Not Set');  
 
 
  /* Start New fields added as a part of Columbia Report 856 */ 
UPDATE dim_customermastersales 
   SET custpricingProcedure = ifnull(KNVV_KALKS, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND custpricingProcedure <> ifnull(KNVV_KALKS, 'Not Set'); 

UPDATE dim_customermastersales 
   SET pricingProcedure = ifnull(KNVV_KALKS, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND pricingProcedure <> ifnull(KNVV_KALKS, 'Not Set');        
	   
UPDATE dim_customermastersales 
   SET deliveryPriority = ifnull(KNVV_LPRIO,1.0000),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND deliveryPriority <> ifnull(KNVV_LPRIO,1.0000);
       
UPDATE dim_customermastersales 
   SET incoterm1 = ifnull(KNVV_INCO1, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND incoterm1 <> ifnull(KNVV_INCO1, 'Not Set');	   

UPDATE dim_customermastersales 
   SET incoTerm2 = ifnull(KNVV_INCO2, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND incoTerm2 <> ifnull(KNVV_INCO2, 'Not Set');   
	   
UPDATE dim_customermastersales 
   SET maxPartialDelivery = ifnull(KNVV_ANTLF,1.0000),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND maxPartialDelivery <> ifnull(KNVV_ANTLF,1.0000);
	   
UPDATE dim_customermastersales 
   SET storeLocator = ifnull(KNVV_AGREL, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND storeLocator <> ifnull(KNVV_AGREL, 'Not Set');


       	
UPDATE dim_customermastersales 
   SET customergroup5 = ifnull(KNVV_KVGR5, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup5 <> ifnull(KNVV_KVGR5, 'Not Set');	
	   
UPDATE dim_customermastersales 
   SET customergroup2 = ifnull(KNVV_KVGR2, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup2 <> ifnull(KNVV_KVGR2, 'Not Set');	
	   
UPDATE dim_customermastersales 
   SET customergroup1 = ifnull(KNVV_KVGR1, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup1 <> ifnull(KNVV_KVGR1, 'Not Set');	
	   

/* End New fields added as a part of Columbia Report 856 Nov 2014*/
			   
/* Insert Process */			  
/* Modifed on 12222014 to fix issue while processing */ 
truncate table tmp_dim_customermastersales_knvv_ins;
truncate table tmp_dim_customermastersales_knvv_del;

drop table if exists tmp_dim_customermastersales_knvv_ins;
create table tmp_dim_customermastersales_knvv_ins as
select knvv_KUNNR,KNVV_VTWEG,KNVV_VKORG,KNVV_SPART
FROM    KNVV;

drop table if exists tmp_dim_customermastersales_knvv_del;
create table tmp_dim_customermastersales_knvv_del as
select * from tmp_dim_customermastersales_knvv_ins where 1=2;
/* end of modification on 12222014 */ 

insert into tmp_dim_customermastersales_knvv_del
select CustomerNumber,DistributionChannel,SalesOrg,DivisionCode
from dim_customermastersales;

merge into tmp_dim_customermastersales_knvv_ins dst
using (select distinct t0.rowid rid
	   from tmp_dim_customermastersales_knvv_ins t0
				inner join tmp_dim_customermastersales_knvv_del t1 on ifnull(t0.knvv_KUNNR,'Not Set') = ifnull(t1.knvv_KUNNR,'Not Set') and 
																	  ifnull(t0.KNVV_VTWEG,'Not Set') = ifnull(t1.KNVV_VTWEG,'Not Set') and 
																	  ifnull(t0.KNVV_VKORG,'Not Set') = ifnull(t1.KNVV_VKORG,'Not Set') and
																	  ifnull(t0.KNVV_SPART,'Not Set') = ifnull(t1.KNVV_SPART,'Not Set') 
	   ) src on dst.rowid = src.rid
when matched then delete;


drop table if exists tmp_dcms_ins_t1;
create table tmp_dcms_ins_t1 as
select 	ifnull(KNVV_KLABC,'Not Set') ABCClassification,
		ifnull(KNVV_KTGRD,'Not Set') AccountAssignmentGroup,
        convert(varchar(200), 'Not Set') as AccountAssignmentGroupDesciption, 		-- ifnull((SELECT TVKTT_VTEXT FROM TVKTT WHERE tvktt.TVKTT_KTGRD = knvv.KNVV_KTGRD), 'Not Set') AccountAssignmentGroupDesciption,
		convert(varchar(200), 'Not Set') as CustomerName, 							-- ifnull((SELECT k1.KNA1_NAME1 FROM KNA1 k1 WHERE k1.KNA1_KUNNR = knvv.KNVV_KUNNR), 'Not Set') CustomerName,
		ifnull(knvv.knvv_KUNNR,'Not Set') CustomerNumber,
		ifnull(knvv.knvv_VTWEG,'Not Set') DistributionChannel,
		convert(varchar(200), 'Not Set') as DistributionChannelDescription, 		-- ifnull((SELECT t.TVTWT_VTEXT FROM TVTWT t WHERE t.TVTWT_VTWEG = knvv.KNVV_VTWEG), 'Not Set') DistributionChannelDescription,
		convert(varchar(200), 'Not Set') as Division, 								-- ifnull((SELECT t.TSPAT_VTEXT FROM TSPAT t WHERE t.TSPAT_SPART = knvv.KNVV_SPART), 'Not Set') Division,
		ifnull(knvv.knvv_SPART,'Not Set') DivisionCode,
		1 RowIsCurrent,
		current_Date RowStartDate,
		ifnull(knvv.knvv_VKORG,'Not Set') SalesOrg,
		convert(varchar(200), 'Not Set') as SalesOrgDescription, 					-- ifnull((SELECT t.TVKOT_VTEXT FROM TVKOT t WHERE t.TVKOT_VKORG = knvv.KNVV_VKORG), 'Not Set') SalesOrgDescription,
		ifnull(knvv_VKBUR,'Not Set') SalesOffice,
		convert(varchar(200), 'Not Set') as SalesOfficeDescription, 				-- ifnull((SELECT t.TVKBT_BEZEI FROM TVKBT t WHERE t.TVKBT_VKBUR = knvv.KNVV_VKBUR), 'Not Set') SalesOfficeDescription,
		ifnull(KNVV_BZIRK,'Not Set') SalesDistrict,
		convert(varchar(200), 'Not Set') as SalesDistrictDescription, 				-- ifnull((SELECT t.T171T_BZTXT FROM T171T t WHERE t.T171T_BZIRK = knvv.KNVV_BZIRK), 'Not Set') SalesDistrictDescription,
		ifnull(knvv_KDGRP,'Not Set') CustomerGroup,
		convert(varchar(200), 'Not Set') as CustomerGroupDescription, 				-- ifnull((SELECT t.T151T_KTEXT FROM T151T t WHERE t.T151T_KDGRP = knvv.KNVV_KDGRP), 'Not Set') CustomerGroupDescription,
		ifnull(knvv_zterm,'Not Set') PaymentTerms,
		'Not Set' PaymentTermsDescription,
		ifnull(KNVV_KVGR3,'Not Set') CustomerGroup3,
	    ifnull(KNVV_VKGRP,'Not Set') SalesGroup	 
		,ifnull(KNVV_KALKS,'Not Set') custpricingProcedure
		,ifnull(KNVV_LPRIO,0.0000) deliveryPriority
		,ifnull(KNVV_INCO1, 'Not Set') incoterm1
		,ifnull(KNVV_INCO2,'Not Set') incoTerm2
		,ifnull(KNVV_ANTLF,0.0000) maxPartialDelivery
		,ifnull(KNVV_AGREL,'Not Set') storeLocator
		,ifnull(KNVV_KVGR5,'Not Set') customergroup5
		,ifnull(KNVV_KVGR2,'Not Set') customergroup2
		,ifnull(KNVV_KVGR1,'Not Set') customergroup1
		,ifnull(KNVV_LOEVM,'Not Set') deletionFlag	  
FROM KNVV knvv, 
	 tmp_dim_customermastersales_knvv_ins c
WHERE    knvv.knvv_KUNNR = c.knvv_KUNNR
     AND knvv.KNVV_VTWEG = c.KNVV_VTWEG
     AND knvv.KNVV_VKORG = c.KNVV_VKORG
     AND knvv.KNVV_SPART = c.KNVV_SPART;

/* AccountAssignmentGroupDesciption */
update tmp_dcms_ins_t1 knvv
set knvv.AccountAssignmentGroupDesciption = ifnull(t.TVKTT_VTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TVKTT t on t.TVKTT_KTGRD = knvv.AccountAssignmentGroup
where knvv.AccountAssignmentGroupDesciption <> ifnull(t.TVKTT_VTEXT, 'Not Set');

/* CustomerName */
update tmp_dcms_ins_t1 knvv
set knvv.CustomerName = ifnull(t.KNA1_NAME1, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join KNA1 t on t.KNA1_KUNNR = knvv.CustomerNumber
where knvv.CustomerName <> ifnull(t.KNA1_NAME1, 'Not Set');

/* DistributionChannelDescription */
update tmp_dcms_ins_t1 knvv
set knvv.DistributionChannelDescription = ifnull(t.TVTWT_VTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TVTWT t on t.TVTWT_VTWEG = knvv.DistributionChannel
where knvv.DistributionChannelDescription <> ifnull(t.TVTWT_VTEXT, 'Not Set');

/* Division */
update tmp_dcms_ins_t1 knvv
set knvv.Division = ifnull(t.TSPAT_VTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TSPAT t on t.TSPAT_SPART = knvv.DivisionCode
where knvv.Division <> ifnull(t.TSPAT_VTEXT, 'Not Set');

/* SalesOrgDescription */
update tmp_dcms_ins_t1 knvv
set knvv.SalesOrgDescription = ifnull(t.TVKOT_VTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TVKOT t on t.TVKOT_VKORG = knvv.SalesOrg
where knvv.SalesOrgDescription <> ifnull(t.TVKOT_VTEXT, 'Not Set');

/* SalesOfficeDescription */
update tmp_dcms_ins_t1 knvv
set knvv.SalesOfficeDescription = ifnull(t.TVKBT_BEZEI, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TVKBT t on t.TVKBT_VKBUR = knvv.SalesOffice
where knvv.SalesOfficeDescription <> ifnull(t.TVKBT_BEZEI, 'Not Set');

/* SalesDistrictDescription */
update tmp_dcms_ins_t1 knvv
set knvv.SalesDistrictDescription = ifnull(t.T171T_BZTXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join T171T t on t.T171T_BZIRK = knvv.SalesDistrict
where knvv.SalesDistrictDescription <> ifnull(t.T171T_BZTXT, 'Not Set');

/* CustomerGroupDescription */
update tmp_dcms_ins_t1 knvv
set knvv.CustomerGroupDescription = ifnull(t.T151T_KTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join T151T t on t.T151T_KDGRP = knvv.CustomerGroup
where knvv.CustomerGroupDescription <> ifnull(t.T151T_KTEXT, 'Not Set');

delete from number_fountain m where m.table_name = 'dim_customermastersales';

insert into number_fountain
select 	'dim_customermastersales',
	ifnull(max(d.dim_customermastersalesid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customermastersales d
where d.dim_customermastersalesid <> 1;

INSERT INTO dim_customermastersales(
				Dim_CustomerMasterSalesId,
				ABCClassification,
				AccountAssignmentGroup,
				AccountAssignmentGroupDesciption,
				CustomerName,
				CustomerNumber,
				DistributionChannel,
				DistributionChannelDescription,
				Division,
				DivisionCode,
				RowIsCurrent,
				RowStartDate,
				SalesOrg,
				SalesOrgDescription,
				SalesOffice,
				SalesOfficeDescription,
				SalesDistrict,
				SalesDistrictDescription,
				CustomerGroup,
				CustomerGroupDescription,
				PaymentTerms,
				PaymentTermsDescription,
				CustomerGroup3,
				SalesGroup
				,custpricingProcedure
				,deliveryPriority
				,incoterm1
				,incoTerm2
				,maxPartialDelivery
				,storeLocator
				,customergroup5
				,customergroup2
				,customergroup1
				,deletionFlag )
 SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customermastersales') + row_number() over(order by '') Dim_CustomerMasterSalesId,
		a.* 
	FROM (SELECT distinct ABCClassification,
				AccountAssignmentGroup,
				AccountAssignmentGroupDesciption,
				CustomerName,
				CustomerNumber,
				DistributionChannel,
				DistributionChannelDescription,
				Division,
				DivisionCode,
				RowIsCurrent,
				RowStartDate,
				SalesOrg,
				SalesOrgDescription,
				SalesOffice,
				SalesOfficeDescription,
				SalesDistrict,
				SalesDistrictDescription,
				CustomerGroup,
				CustomerGroupDescription,
				PaymentTerms,
				PaymentTermsDescription,
				CustomerGroup3,
				SalesGroup
				,custpricingProcedure
				,deliveryPriority
				,incoterm1
				,incoTerm2
				,maxPartialDelivery
				,storeLocator
				,customergroup5
				,customergroup2
				,customergroup1
				,deletionFlag
		  from tmp_dcms_ins_t1 ) a;
		  
drop table if exists tmp_dcms_ins_t1;
		  
delete from number_fountain m where m.table_name = 'dim_customermastersales';	

truncate table tmp_dim_customermastersales_knvv_ins;
truncate table tmp_dim_customermastersales_knvv_del;


