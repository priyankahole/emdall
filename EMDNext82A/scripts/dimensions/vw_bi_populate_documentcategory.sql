UPDATE    dim_documentcategory dc
   SET dc.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			dc.dw_update_date = current_timestamp
       FROM dim_documentcategory dc,
          DD07T dt
 WHERE dc.RowIsCurrent = 1
 AND  dt.DD07T_DOMNAME = 'VBTYP'
          AND dt.DD07T_DOMVALUE IS NOT NULL
          AND dc.DocumentCategory = dt.DD07T_DOMVALUE;

INSERT INTO dim_documentcategory(dim_documentcategoryid, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_documentcategory
               WHERE dim_documentcategoryid = 1);

delete from number_fountain m where m.table_name = 'dim_documentcategory';
   
insert into number_fountain
select 	'dim_documentcategory',
	ifnull(max(d.Dim_DocumentCategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_documentcategory d
where d.Dim_DocumentCategoryid <> 1; 

INSERT INTO dim_documentcategory(Dim_DocumentCategoryid,
                                                                                                                DocumentCategory,
                                                          Description,
                                                          RowStartDate,
                                                          RowIsCurrent)
   SELECT distinct (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_documentcategory')
          + row_number() over(order by '') ,
                        DD07T_DOMVALUE,
        ifnull(DD07T_DDTEXT, 'Not Set'),
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'VBTYP' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM Dim_documentcategory
                    WHERE DocumentCategory = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'VBTYP');

