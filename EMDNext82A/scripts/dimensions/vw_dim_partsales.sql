
UPDATE 
dim_partsales p
SET PartNumber = mv.MARA_MATNR,
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E' 
	    AND p.PartNumber <> mv.MARA_MATNR ;


UPDATE 
dim_partsales p
SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.PartDescription <> ifnull(MAKT_MAKTX, 'Not Set') ;


UPDATE 
dim_partsales p
SET Revision = ifnull(MARA_KZREV, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.Revision <> ifnull(MARA_KZREV, 'Not Set');


UPDATE 
dim_partsales p
SET UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.UnitOfMeasure <> ifnull(MARA_MEINS, 'Not Set');


UPDATE 
dim_partsales p
SET RowStartDate = current_timestamp,
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E';


UPDATE 
dim_partsales p
SET RowIsCurrent =  1,
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.RowIsCurrent <> 1;


UPDATE 
dim_partsales p
SET MaterialGroup = ifnull(MARA_MATKL, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialGroup<> ifnull(MARA_MATKL, 'Not Set');


UPDATE 
dim_partsales p
SET TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.TransportationGroup <> ifnull(MARA_TRAGR, 'Not Set');


UPDATE 
dim_partsales p
SET Division = ifnull(MARA_SPART, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.Division <> ifnull(MARA_SPART, 'Not Set');


UPDATE 
dim_partsales p
SET GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
    	AND  p.GeneralItemCategory <> ifnull(MARA_MTPOS, 'Not Set');


UPDATE 
dim_partsales p
SET MaterialStatus = ifnull(MARA_MSTAE, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.MaterialStatus <>  ifnull(MARA_MSTAE, 'Not Set');


UPDATE 
dim_partsales p
SET ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.ProductHierarchy <> ifnull(MARA_PRDHA, 'Not Set');


UPDATE 
dim_partsales p
SET MPN = ifnull(MARA_MFRPN, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MPN <> ifnull(MARA_MFRPN, 'Not Set');


UPDATE 
dim_partsales p
SET p.ProductHierarchyDescription = ifnull(t.VTEXT, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
left JOIN t179t t ON t.PRODH = mv.MARA_PRDHA
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.ProductHierarchyDescription <>  ifnull(t.VTEXT, 'Not Set');


UPDATE 
dim_partsales p
SET PartType = ifnull(MARA_MTART, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.PartType <> ifnull(MARA_MTART, 'Not Set');


UPDATE 
dim_partsales p
SET PartTypeDescription = ifnull(pt.T134T_MTBEZ, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN T134T pt ON pt.T134T_MTART = mv.MARA_MTART
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.PartTypeDescription <> ifnull(pt.T134T_MTBEZ, 'Not Set');

UPDATE dim_partsales p
SET p.MaterialGroupDescription = ifnull(mg.T023T_WGBEZ, 'Not Set'),
    p.dw_update_date = current_timestamp
FROM dim_partsales p
		inner join MARA_MVKE_MAKT mv on     p.PartNumber = mv.MARA_MATNR
										AND p.SalesOrgCode = mv.MVKE_VKORG
										AND p.DistributionChannelCode = mv.MVKE_VTWEG
		INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
        INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
		left join T023T mg on mg.T023T_MATKL = mv.MARA_MATKL
WHERE mv.MAKT_SPRAS = 'E'
	  AND p.MaterialGroupDescription <> ifnull(mg.T023T_WGBEZ, 'Not Set');


UPDATE 
dim_partsales p
SET MaterialStatusDescription = ifnull(mst.t141t_MTSTB, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN t141t mst ON mst.T141T_MMSTA =  mv.MARA_MSTAE
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialStatusDescription <> ifnull(mst.t141t_MTSTB, 'Not Set');


UPDATE 
dim_partsales p
SET ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.ExternalMaterialGroupCode <> ifnull(MARA_EXTWG, 'Not Set');


UPDATE 
dim_partsales p
SET ExternalMaterialGroupDescription = ifnull(t.TWEWT_EWBEZ, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN twewt t ON t.TWEWT_EXTWG = mv.MARA_EXTWG
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.ExternalMaterialGroupDescription  <> ifnull(t.TWEWT_EWBEZ, 'Not Set');


UPDATE 
dim_partsales p
SET OldPartNumber = ifnull(mv.MARA_BISMT, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.OldPartNumber <> ifnull(mv.MARA_BISMT, 'Not Set');


UPDATE 
dim_partsales p
SET AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.AFSColor <> ifnull(MARA_J_3ACOL, 'Not Set');


UPDATE 
dim_partsales p
SET AFSColorDescription = ifnull(t.J_3ACOLRT_TEXT, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN J_3ACOLRT t ON t.J_3ACOLRT_J_3ACOL = mv.MARA_J_3ACOL
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.AFSColorDescription  <> ifnull(t.J_3ACOLRT_TEXT, 'Not Set');


UPDATE 
dim_partsales p
SET p.SDMaterialStatusDescription = ifnull(t.TVMST_VMSTB, 'Not Set'),
	p.dw_update_date = current_timestamp 
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN tvmst t ON t.TVMST_VMSTA = mv.MARA_MSTAV
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.SDMaterialStatusDescription <> ifnull(t.TVMST_VMSTB, 'Not Set');


UPDATE 
dim_partsales p
SET p.Volume = MARA_VOLUM,
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.Volume <>  MARA_VOLUM;


UPDATE 
dim_partsales p
SET p.AFSMasterGrid = ifnull(mv.MARA_J_3APGNR, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.AFSMasterGrid <> ifnull(MARA_J_3APGNR, 'Not Set');


UPDATE 
dim_partsales p
SET p.AFSPattern = ifnull(mv.MARA_AFS_SCHNITT, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.AFSPattern  <> ifnull(mv.MARA_AFS_SCHNITT, 'Not Set');


UPDATE 
dim_partsales p
SET p.SalesOrgCode = ifnull(mv.MVKE_VKORG, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.SalesOrgCode <> ifnull(mv.MVKE_VKORG, 'Not Set');


UPDATE 
dim_partsales p
SET p.SalesOrg = ifnull(t.TVKOT_VTEXT, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN TVKOT t ON t.TVKOT_VKORG = mv.MVKE_VKORG
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.SalesOrg  <>  ifnull(t.TVKOT_VTEXT, 'Not Set');


UPDATE 
dim_partsales p
SET p.DistributionChannelCode = ifnull(mv.MVKE_VTWEG, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.DistributionChannelCode <> ifnull(MVKE_VTWEG, 'Not Set');


UPDATE 
dim_partsales p
SET p.DistributionChannel = ifnull(t.TVTWT_VTEXT, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN TVTWT t ON t.TVTWT_VTWEG = mv.MVKE_VTWEG
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.DistributionChannel <>  ifnull(t.TVTWT_VTEXT, 'Not Set');


UPDATE 
dim_partsales p
SET p.CSDMaterialStatusCode = ifnull(mv.MVKE_VMSTA, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.CSDMaterialStatusCode <> ifnull(mv.MVKE_VMSTA, 'Not Set');


UPDATE 
dim_partsales p
SET p.CSDMaterialStatus = ifnull(t.TVMST_VMSTB, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN TVMST t ON t.TVMST_VMSTA = mv.MVKE_VMSTA
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.CSDMaterialStatus <> ifnull(t.TVMST_VMSTB, 'Not Set');


UPDATE dim_partsales p
SET p.SalesGridNumber = ifnull(j.J_3AGRHD_J_3APGNR,'Not Set'),
	p.dw_update_date = current_timestamp
FROM dim_partsales p
		inner join MARA_MVKE_MAKT mv on     p.PartNumber = mv.MARA_MATNR
										AND p.SalesOrgCode = mv.MVKE_VKORG
										AND p.DistributionChannelCode = mv.MVKE_VTWEG
		left join (select KOTJ010_VTWEG, KOTJ010_VKORG, KOTJ010_MATNR, J_3AGRHD_J_3APGNR
		           from KOTJ010 inner join J_3AGRHD on J_3AGRHD_KNUMH = KOTJ010_KNUMH) j ON    j.KOTJ010_VTWEG = mv.MVKE_VTWEG
																						   AND j.KOTJ010_VKORG = mv.MVKE_VKORG
																						   AND j.KOTJ010_MATNR = mv.MARA_MATNR
		INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
		INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE    mv.MAKT_SPRAS = 'E'
	 AND p.SalesGridNumber <> ifnull(j.J_3AGRHD_J_3APGNR,'Not Set');
	
UPDATE 
dim_partsales p
SET p.MaterialGroup1 = ifnull(mv.MVKE_MVGR1, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG

WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialGroup1  <> ifnull(mv.MVKE_MVGR1, 'Not Set');


UPDATE 
dim_partsales p
SET p.MaterialGroup2 = ifnull(mv.MVKE_MVGR2, 'Not Set'),
	p.dw_update_date = current_timestamp 
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.MaterialGroup2 <> ifnull(mv.MVKE_MVGR2, 'Not Set');


UPDATE 
dim_partsales p
SET p.MaterialGroup3 = ifnull(mv.MVKE_MVGR3, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND  p.MaterialGroup3 <> ifnull(mv.MVKE_MVGR3, 'Not Set');


UPDATE 
dim_partsales p
SET p.MaterialGroup4 = ifnull(mv.MVKE_MVGR4, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG      
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialGroup4 <> ifnull(mv.MVKE_MVGR4, 'Not Set');


UPDATE 
dim_partsales p
SET p.MaterialGroup5 = ifnull(mv.MVKE_MVGR5, 'Not Set'),
	p.dw_update_date = current_timestamp 
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialGroup5 <> ifnull(mv.MVKE_MVGR5, 'Not Set');


UPDATE 
dim_partsales p
SET p.DChainValidFromDt = mv.MVKE_VMSTD,
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.DChainValidFromDt <> mv.MVKE_VMSTD;


UPDATE 
dim_partsales p
SET p.DeliveryPlant = ifnull(mv.MVKE_DWERK, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.DeliveryPlant <> ifnull(mv.MVKE_DWERK, 'Not Set');


UPDATE 
dim_partsales p
SET p.MaterialPricingGroup = ifnull(mv.MVKE_KONDM, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialPricingGroup  <> ifnull(mv.MVKE_KONDM, 'Not Set');


UPDATE 
dim_partsales p
SET p.RoundingProfle = ifnull(mv.MVKE_RDPRF, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.RoundingProfle <> ifnull(mv.MVKE_RDPRF, 'Not Set');


UPDATE 
dim_partsales p
SET p.CommissionGroup = ifnull(mv.MVKE_PROVG, 'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.CommissionGroup <> ifnull(mv.MVKE_PROVG, 'Not Set');


UPDATE 
dim_partsales p
SET p.DeliveryUnits = ifnull(mv.MVKE_SCMNG,0),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
	AND p.SalesOrgCode = mv.MVKE_VKORG
	AND p.DistributionChannelCode = mv.MVKE_VTWEG
	AND mv.MAKT_SPRAS = 'E'
	AND p.DeliveryUnits <> ifnull(mv.MVKE_SCMNG,0);


UPDATE 
dim_partsales p
Set p.MaterialPriceGroup1Description = ifnull(t1.TVM1T_BEZEI,'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN TVM1T t1 ON t1.TVM1T_MVGR1 =  mv.MVKE_MVGR1
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialPriceGroup1Description <> ifnull(t1.TVM1T_BEZEI,'Not Set');


UPDATE 
dim_partsales p
Set p.MaterialPriceGroup2Description = ifnull(t2.TVM2T_BEZEI,'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN TVM2T t2 ON t2.TVM2T_MVGR2 =  mv.MVKE_MVGR2
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialPriceGroup2Description <> ifnull(t2.TVM2T_BEZEI,'Not Set');


UPDATE 
dim_partsales p
Set p.MaterialPriceGroup3Description = ifnull(t3.TVM3T_BEZEI,'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN TVM3T t3 ON t3.TVM3T_MVGR3 =  mv.MVKE_MVGR3
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialPriceGroup3Description <> ifnull(t3.TVM3T_BEZEI,'Not Set');


UPDATE 
dim_partsales p
Set p.MaterialPriceGroup4Description = ifnull(t4.TVM4T_BEZEI,'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN TVM4T t4 ON t4.TVM4T_MVGR4 =  mv.MVKE_MVGR4
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialPriceGroup4Description <> ifnull(t4.TVM4T_BEZEI,'Not Set');


UPDATE 
dim_partsales p
Set p.MaterialPriceGroup5Description = ifnull(t5.TVM5T_BEZEI,'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
LEFT JOIN TVM5T t5 ON t5.TVM5T_MVGR5 =  mv.MVKE_MVGR5
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialPriceGroup5Description  <>  ifnull(t5.TVM5T_BEZEI,'Not Set');


insert into dim_partsales (dim_partsalesid,rowstartdate,rowiscurrent,partnumber_noleadzero)
select 1,current_timestamp,1,'Not Set' from (select 1) as src
where
not exists (select 1 from dim_partsales where dim_partsalesid=1);		


drop table if exists subtbl_t001;
Create table subtbl_t001 as Select distinct PartNumber,SalesOrgCode,DistributionChannelCode from dim_partsales;

truncate table tmp_MARAMVKEMAKT_ins;
truncate table tmp_MARAMVKEMAKT_del;

insert into tmp_MARAMVKEMAKT_ins
select mv.MARA_MATNR, mv.MVKE_VKORG, mv.MVKE_VTWEG
FROM    MARA_MVKE_MAKT mv;


insert into tmp_MARAMVKEMAKT_del
select p.PartNumber, p.SalesOrgCode,  p.DistributionChannelCode
from dim_partsales p;


MERGE INTO tmp_MARAMVKEMAKT_ins dst
USING (select mv.rowid rid
	   from tmp_MARAMVKEMAKT_ins mv
				inner join tmp_MARAMVKEMAKT_del p ON ifnull(p.MARA_MATNR,'Not Set') = ifnull(mv.MARA_MATNR,'Not Set') AND
													 ifnull(p.MVKE_VKORG,'Not Set') = ifnull(mv.MVKE_VKORG,'Not Set') AND
													 ifnull(p.MVKE_VTWEG,'Not Set') = ifnull(mv.MVKE_VTWEG,'Not Set') 
	   ) src on dst.rowid = src.rid
WHEN MATCHED THEN DELETE;

drop table if exists tmp_dps_ins_t1;
create table tmp_dps_ins_t1 as 
select  mv.MARA_MATNR PartNumber,
		ifnull(MAKT_MAKTX, 'Not Set') PartDescription,
		ifnull(MARA_KZREV, 'Not Set') Revision,
		ifnull(MARA_MEINS, 'Not Set') UnitOfMeasure,
		current_timestamp RowStartDate,
		1 RowIsCurrent,
		ifnull(MARA_MATKL, 'Not Set') MaterialGroup,
		ifnull(MARA_TRAGR, 'Not Set') TransportationGroup,
		ifnull(MARA_SPART, 'Not Set') Division,
		ifnull(MARA_MTPOS, 'Not Set') GeneralItemCategory,
		ifnull(MARA_MSTAE, 'Not Set') MaterialStatus,
		ifnull(MARA_PRDHA, 'Not Set') ProductHierarchy,
		ifnull(MARA_MFRPN, 'Not Set') MPN,
		convert(varchar(200), 'Not Set') as ProductHierarchyDescription, 	-- ifnull((SELECT t.VTEXT FROM t179t t WHERE t.PRODH = mv.MARA_PRDHA), 'Not Set') ProductHierarchyDescription,
		ifnull(MARA_MTART, 'Not Set') PartType,
		convert(varchar(200), 'Not Set') as PartTypeDescription, 			-- ifnull((SELECT pt.T134T_MTBEZ FROM T134T pt WHERE pt.T134T_MTART = mv.MARA_MTART), 'Not Set') PartTypeDescription,
		convert(varchar(200), 'Not Set') as MaterialGroupDescription, 		-- ifnull((SELECT mg.T023T_WGBEZ FROM T023T mg WHERE mg.T023T_MATKL = mv.MARA_MATKL), 'Not Set') MaterialGroupDescription,
		convert(varchar(200), 'Not Set') as materialstatusdescription, 		-- ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA =  mv.MARA_MSTAE), 'Not Set') materialstatusdescription,
		ifnull(MARA_EXTWG, 'Not Set') ExternalMaterialGroupCode,  
		convert(varchar(200), 'Not Set') as ExternalMaterialGroupDescription, 	-- ifnull((SELECT t.TWEWT_EWBEZ FROM twewt t WHERE t.TWEWT_EXTWG = MARA_EXTWG), 'Not Set')ExternalMaterialGroupDescription,
		ifnull(mv.MARA_BISMT, 'Not Set') OldPartNumber,
		ifnull(MARA_J_3ACOL, 'Not Set') AFSColor,
		convert(varchar(200), 'Not Set') as AFSColorDescription, 			-- ifnull((SELECT J_3ACOLRT_TEXT FROM J_3ACOLRT WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL), 'Not Set') AFSColorDescription,
		convert(varchar(200), 'Not Set') as SDMaterialStatusDescription, 	-- ifnull((SELECT TVMST_VMSTB FROM tvmst WHERE TVMST_VMSTA = MARA_MSTAV), 'Not Set') SDMaterialStatusDescription,
		MARA_VOLUM Volume,
		ifnull(MARA_J_3APGNR, 'Not Set') AFSMasterGrid,
		ifnull(MARA_AFS_SCHNITT, 'Not Set') AFSPattern,
		ifnull(mv.MVKE_VKORG, 'Not Set') SalesOrgCode,
		convert(varchar(200), 'Not Set') as SalesOrg, 						-- ifnull((SELECT TVKOT_VTEXT FROM TVKOT WHERE TVKOT_VKORG = mv.MVKE_VKORG) , 'Not Set') SalesOrg,
		ifnull(mv.MVKE_VTWEG, 'Not Set') DistributionChannelCode,				 
		convert(varchar(200), 'Not Set') as DistributionChannel, 			-- ifnull((SELECT TVTWT_VTEXT FROM TVTWT WHERE TVTWT_VTWEG = mv.MVKE_VTWEG) , 'Not Set') DistributionChannel,
		ifnull(MVKE_VMSTA, 'Not Set') CSDMaterialStatusCode, 
		convert(varchar(200), 'Not Set') as CSDMaterialStatus, 				-- ifnull((SELECT TVMST_VMSTB FROM TVMST WHERE TVMST_VMSTA = mv.MVKE_VMSTA), 'Not Set') CSDMaterialStatus,
		convert(varchar(200), 'Not Set') as SalesGridNumber, 				-- ifnull((SELECT J_3AGRHD_J_3APGNR FROM KOTJ010, J_3AGRHD WHERE KOTJ010_VTWEG = mv.MVKE_VTWEG AND KOTJ010_VKORG = mv.MVKE_VKORG AND KOTJ010_MATNR = mv.MARA_MATNR AND J_3AGRHD_KNUMH = KOTJ010_KNUMH), 'Not Set') SalesGridNumber,
		ifnull(mv.MVKE_MVGR1, 'Not Set') MaterialGroup1,
		ifnull(mv.MVKE_MVGR2, 'Not Set') MaterialGroup2,
		ifnull(mv.MVKE_MVGR3, 'Not Set') MaterialGroup3,
		ifnull(mv.MVKE_MVGR4, 'Not Set') MaterialGroup4,
		ifnull(mv.MVKE_MVGR5, 'Not Set') MaterialGroup5,
		mv.MVKE_VMSTD DChainValidFromDt,
		ifnull(mv.MVKE_DWERK, 'Not Set') DeliveryPlant,  
		ifnull(mv.MVKE_KONDM, 'Not Set') MaterialPricingGroup,  
		ifnull(mv.MVKE_RDPRF, 'Not Set') RoundingProfle,
		ifnull(mv.MVKE_PROVG, 'Not Set') CommissionGroup,
		ifnull(mv.MVKE_SCMNG,0) DeliveryUnits,	
		convert(varchar(200), 'Not Set') as MaterialPriceGroup1Description, 	-- ifnull((SELECT TVM1T_BEZEI FROM TVM1T t1 WHERE t1.TVM1T_MVGR1 =  mv.MVKE_MVGR1),'Not Set') MaterialPriceGroup1Description,
		convert(varchar(200), 'Not Set') as MaterialPriceGroup2Description, 	-- ifnull((SELECT TVM2T_BEZEI FROM TVM2T t2 WHERE t2.TVM2T_MVGR2 =  mv.MVKE_MVGR2),'Not Set') MaterialPriceGroup2Description,
		convert(varchar(200), 'Not Set') as MaterialPriceGroup3Description, 	-- ifnull((SELECT TVM3T_BEZEI FROM TVM3T t3 WHERE t3.TVM3T_MVGR3 =  mv.MVKE_MVGR3),'Not Set') MaterialPriceGroup3Description,
		convert(varchar(200), 'Not Set') as MaterialPriceGroup4Description, 	-- ifnull((SELECT TVM4T_BEZEI FROM TVM4T t4 WHERE t4.TVM4T_MVGR4 =  mv.MVKE_MVGR4),'Not Set') MaterialPriceGroup4Description,
		convert(varchar(200), 'Not Set') as MaterialPriceGroup5Description	 	-- ifnull((SELECT TVM5T_BEZEI FROM TVM5T t5 WHERE t5.TVM5T_MVGR5 =  mv.MVKE_MVGR5),'Not Set') MaterialPriceGroup5Description
		,MARA_MSTAV				-- SDMaterialStatusDescription
FROM MARA_MVKE_MAKT mv 
		INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
		INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG 
		inner join tmp_MARAMVKEMAKT_ins p on    mv.MARA_MATNR = p.MARA_MATNR
										    AND mv.MVKE_VKORG = p.MVKE_VKORG
										    AND mv.MVKE_VTWEG = p.MVKE_VTWEG
WHERE mv.MAKT_SPRAS = 'E';

/* ProductHierarchyDescription */
update tmp_dps_ins_t1 ds
set ds.ProductHierarchyDescription = ifnull(t.VTEXT, 'Not Set')
from tmp_dps_ins_t1 ds
		left join t179t t on t.PRODH = ds.ProductHierarchy
where ds.ProductHierarchyDescription <> ifnull(t.VTEXT, 'Not Set');

/* PartTypeDescription */
update tmp_dps_ins_t1 ds
set ds.PartTypeDescription = ifnull(t.T134T_MTBEZ, 'Not Set')
from tmp_dps_ins_t1 ds
		left join T134T t on t.T134T_MTART = ds.PartType
where ds.PartTypeDescription <> ifnull(t.T134T_MTBEZ, 'Not Set');

/* MaterialGroupDescription */
update tmp_dps_ins_t1 ds
set ds.MaterialGroupDescription = ifnull(t.T023T_WGBEZ, 'Not Set')
from tmp_dps_ins_t1 ds
		left join T023T t on t.T023T_MATKL = ds.MaterialGroup
where ds.MaterialGroupDescription <> ifnull(t.T023T_WGBEZ, 'Not Set');

/* materialstatusdescription */
update tmp_dps_ins_t1 ds
set ds.materialstatusdescription = ifnull(t.t141t_MTSTB, 'Not Set')
from tmp_dps_ins_t1 ds
		left join t141t t on t.T141T_MMSTA = ds.MaterialStatus
where ds.materialstatusdescription <> ifnull(t.t141t_MTSTB, 'Not Set');

/* ExternalMaterialGroupDescription */
update tmp_dps_ins_t1 ds
set ds.ExternalMaterialGroupDescription = ifnull(t.TWEWT_EWBEZ, 'Not Set')
from tmp_dps_ins_t1 ds
		left join twewt t on t.TWEWT_EXTWG = ds.ExternalMaterialGroupCode
where ds.ExternalMaterialGroupDescription <> ifnull(t.TWEWT_EWBEZ, 'Not Set');

/* AFSColorDescription */
update tmp_dps_ins_t1 ds
set ds.AFSColorDescription = ifnull(t.J_3ACOLRT_TEXT, 'Not Set')
from tmp_dps_ins_t1 ds
		left join J_3ACOLRT t on t.J_3ACOLRT_J_3ACOL = ds.AFSColor
where ds.AFSColorDescription <> ifnull(t.J_3ACOLRT_TEXT, 'Not Set');

/* SDMaterialStatusDescription */
update tmp_dps_ins_t1 ds
set ds.SDMaterialStatusDescription = ifnull(t.TVMST_VMSTB, 'Not Set')
from tmp_dps_ins_t1 ds
		left join tvmst t on t.TVMST_VMSTA = ds.MARA_MSTAV
where ds.SDMaterialStatusDescription <> ifnull(t.TVMST_VMSTB, 'Not Set');

/* SalesOrg */
update tmp_dps_ins_t1 ds
set ds.SalesOrg = ifnull(t.TVKOT_VTEXT, 'Not Set')
from tmp_dps_ins_t1 ds
		left join TVKOT t on t.TVKOT_VKORG = ds.SalesOrgCode
where ds.SalesOrg <> ifnull(t.TVKOT_VTEXT, 'Not Set');

/* DistributionChannel */
update tmp_dps_ins_t1 ds
set ds.DistributionChannel = ifnull(t.TVTWT_VTEXT, 'Not Set')
from tmp_dps_ins_t1 ds
		left join TVTWT t on t.TVTWT_VTWEG = ds.DistributionChannelCode
where ds.DistributionChannel <> ifnull(t.TVTWT_VTEXT, 'Not Set');

/* CSDMaterialStatus */
update tmp_dps_ins_t1 ds
set ds.CSDMaterialStatus = ifnull(t.TVMST_VMSTB, 'Not Set')
from tmp_dps_ins_t1 ds
		left join TVMST t on t.TVMST_VMSTA = ds.CSDMaterialStatusCode
where ds.CSDMaterialStatus <> ifnull(t.TVMST_VMSTB, 'Not Set');

/* SalesGridNumber */
update tmp_dps_ins_t1 ds
set ds.SalesGridNumber = ifnull(t.J_3AGRHD_J_3APGNR, 'Not Set')
from tmp_dps_ins_t1 ds
		left join (select KOTJ010_VTWEG, KOTJ010_VKORG, KOTJ010_MATNR, J_3AGRHD_J_3APGNR
				   from KOTJ010 inner join J_3AGRHD on J_3AGRHD_KNUMH = KOTJ010_KNUMH) t on t.KOTJ010_VTWEG = ds.DistributionChannelCode AND 
																							t.KOTJ010_VKORG = ds.SalesOrgCode AND
																							t.KOTJ010_MATNR = ds.PartNumber
where ds.SalesGridNumber <> ifnull(t.J_3AGRHD_J_3APGNR, 'Not Set');

/* MaterialPriceGroup1Description */
update tmp_dps_ins_t1 ds
set ds.MaterialPriceGroup1Description = ifnull(t.TVM1T_BEZEI, 'Not Set')
from tmp_dps_ins_t1 ds
		left join TVM1T t on t.TVM1T_MVGR1 = ds.MaterialGroup1
where ds.MaterialPriceGroup1Description <> ifnull(t.TVM1T_BEZEI, 'Not Set');

/* MaterialPriceGroup2Description */
update tmp_dps_ins_t1 ds
set ds.MaterialPriceGroup2Description = ifnull(t.TVM2T_BEZEI, 'Not Set')
from tmp_dps_ins_t1 ds
		left join TVM2T t on t.TVM2T_MVGR2 = ds.MaterialGroup2
where ds.MaterialPriceGroup2Description <> ifnull(t.TVM2T_BEZEI, 'Not Set');

/* MaterialPriceGroup3Description */
update tmp_dps_ins_t1 ds
set ds.MaterialPriceGroup3Description = ifnull(t.TVM3T_BEZEI, 'Not Set')
from tmp_dps_ins_t1 ds
		left join TVM3T t on t.TVM3T_MVGR3 = ds.MaterialGroup3
where ds.MaterialPriceGroup3Description <> ifnull(t.TVM3T_BEZEI, 'Not Set');

/* MaterialPriceGroup4Description */
update tmp_dps_ins_t1 ds
set ds.MaterialPriceGroup4Description = ifnull(t.TVM4T_BEZEI, 'Not Set')
from tmp_dps_ins_t1 ds
		left join TVM4T t on t.TVM4T_MVGR4 = ds.MaterialGroup4
where ds.MaterialPriceGroup4Description <> ifnull(t.TVM4T_BEZEI, 'Not Set');

/* MaterialPriceGroup5Description */
update tmp_dps_ins_t1 ds
set ds.MaterialPriceGroup5Description = ifnull(t.TVM5T_BEZEI, 'Not Set')
from tmp_dps_ins_t1 ds
		left join TVM5T t on t.TVM5T_MVGR5 = ds.MaterialGroup5
where ds.MaterialPriceGroup5Description <> ifnull(t.TVM5T_BEZEI, 'Not Set');



delete from number_fountain m where m.table_name = 'dim_partsales';

insert into number_fountain				   
select 	'dim_partsales',
	ifnull(max(d.dim_partsalesid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_partsales d
where d.dim_partsalesid <> 1;

INSERT INTO dim_partsales
            (dim_partsalesid,
			PartNumber,
			PartDescription,
			Revision,
			UnitOfMeasure,
			RowStartDate,
			RowIsCurrent,
			MaterialGroup,
			TransportationGroup,
			Division,
			GeneralItemCategory,
			MaterialStatus,
			ProductHierarchy,
			MPN,
			ProductHierarchyDescription,
			PartType,
			PartTypeDescription,
			MaterialGroupDescription,
			MaterialStatusDescription,
			ExternalMaterialGroupCode,
			ExternalMaterialGroupDescription,
			OldPartNumber,
			AFSColor,
			AFSColorDescription,
			SDMaterialStatusDescription,
			Volume,
			AFSMasterGrid,
			AFSPattern,
			SalesOrgCode,
			SalesOrg,
			DistributionChannelCode,
			DistributionChannel,
			CSDMaterialStatusCode,
			CSDMaterialStatus,
			SalesGridNumber,
			MaterialGroup1,
			MaterialGroup2,
			MaterialGroup3,
			MaterialGroup4,
			MaterialGroup5,
			DChainValidFromDt,
			DeliveryPlant,  
			MaterialPricingGroup,  
			RoundingProfle,
			CommissionGroup,
			DeliveryUnits,
			 MaterialPriceGroup1Description,
			 MaterialPriceGroup2Description,
			 MaterialPriceGroup3Description,
			 MaterialPriceGroup4Description,
			 MaterialPriceGroup5Description)
Select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_partsales') + row_number() over(order by ''),
	   tp.*
From (SELECT DISTINCT PartNumber,
			PartDescription,
			Revision,
			UnitOfMeasure,
			RowStartDate,
			RowIsCurrent,
			MaterialGroup,
			TransportationGroup,
			Division,
			GeneralItemCategory,
			MaterialStatus,
			ProductHierarchy,
			MPN,
			ProductHierarchyDescription,
			PartType,
			PartTypeDescription,
			MaterialGroupDescription,
			MaterialStatusDescription,
			ExternalMaterialGroupCode,
			ExternalMaterialGroupDescription,
			OldPartNumber,
			AFSColor,
			AFSColorDescription,
			SDMaterialStatusDescription,
			Volume,
			AFSMasterGrid,
			AFSPattern,
			SalesOrgCode,
			SalesOrg,
			DistributionChannelCode,
			DistributionChannel,
			CSDMaterialStatusCode,
			CSDMaterialStatus,
			SalesGridNumber,
			MaterialGroup1,
			MaterialGroup2,
			MaterialGroup3,
			MaterialGroup4,
			MaterialGroup5,
			DChainValidFromDt,
			DeliveryPlant,  
			MaterialPricingGroup,  
			RoundingProfle,
			CommissionGroup,
			DeliveryUnits,
			MaterialPriceGroup1Description,
			MaterialPriceGroup2Description,
			MaterialPriceGroup3Description,
			MaterialPriceGroup4Description,
			MaterialPriceGroup5Description
	  from tmp_dps_ins_t1) tp;

drop table if exists tmp_dps_ins_t1;
	  
UPDATE dim_partsales p
	SET  
p.PartNumber_NoLeadZero= ifnull(case when length(p.partnumber)=18 and p.partnumber regexp_like '[0-9]*' then trim(leading '0' from p.partnumber) else p.partnumber end,'Not Set'),
p.dw_update_date = current_timestamp;	  

/* 29Jan2014 Added by Victor - MaterialPricingGroupDescription*/
UPDATE 
dim_partsales p
SET p.MaterialPricingGroupDescription = IFNULL(t1.T178T_VTEXT,'Not Set'),
	p.dw_update_date = current_timestamp
FROM 
dim_partsales p,
MARA_MVKE_MAKT mv 
left JOIN T178T t1 ON t1.T178T_KONDM =  mv.MVKE_KONDM
INNER JOIN dim_salesorg so ON so.SalesOrgCode = mv.MVKE_VKORG
INNER JOIN dim_distributionchannel dc ON dc.DistributionChannelCode = mv.MVKE_VTWEG
WHERE   p.PartNumber = mv.MARA_MATNR
        AND p.SalesOrgCode = mv.MVKE_VKORG
        AND p.DistributionChannelCode = mv.MVKE_VTWEG
        AND mv.MAKT_SPRAS = 'E'
	    AND p.MaterialPricingGroupDescription  <> IFNULL(t1.T178T_VTEXT,'Not Set');  
/* End Modification 29Jan2014 */

truncate table tmp_MARAMVKEMAKT_ins;
truncate table tmp_MARAMVKEMAKT_del;

drop table subtbl_t001;