INSERT INTO dim_scheduletype(dim_scheduletypeid,
                             ScheduleTypeCode,
                             Description,
                             RowStartDate,
                             RowEndDate,
                             RowIsCurrent,
                             RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          NULL,
          NULL,
          1,
          NULL
     FROM (select 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_scheduletype
               WHERE dim_scheduletypeid = 1);

UPDATE    dim_scheduletype st
      
   SET st.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			st.dw_update_date = current_timestamp
			 FROM  DD07T t, dim_scheduletype st
 WHERE st.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'SCHED_TYPE'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND st.scheduletypecode = DD07T_DOMVALUE;

delete from number_fountain m where m.table_name = 'dim_scheduletype';

insert into number_fountain
select 	'dim_scheduletype',
	ifnull(max(d.dim_scheduletypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_scheduletype d
where d.dim_scheduletypeid <> 1; 	
		  
INSERT INTO dim_scheduletype(dim_scheduletypeid,
                           Description,
                           Scheduletypecode,
                           RowStartDate,
                           RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_scheduletype') 
          + row_number() over(order by ''),
			  ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'SCHED_TYPE' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_scheduletype
                    WHERE scheduletypecode = DD07T_DOMVALUE AND DD07T_DOMNAME = 'SCHED_TYPE')
   ORDER BY 2 ;