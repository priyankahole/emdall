/* ##################################################################################################################

   Script         : bi_populate_so_itemstatus_dim
   Author         : Ashu
   Created On     : 17 Jan 2013


   Description    : Stored Proc bi_populate_so_itemstatus_dim from MySQL to Vectorwise syntax

   Change History
   Date            By        Version           Desc
   17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
   7 July 2014     George    1.1               Added GeneralIncompletionStatusCode
   5 Feb 2015      Liviu Ionescu  1.2            Add Combine for table VBUP , dim_salesorderitemstatus
#################################################################################################################### */

INSERT INTO dim_salesorderitemstatus(
          dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
		  ItemDeliveryBlockStatus
          )
		  
  SELECT 1,
          'Not Set',
          0,
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set'
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorderitemstatus
               WHERE dim_salesorderitemstatusid = 1);		  

 DELETE FROM VBUP
 WHERE NOT EXISTS
              (SELECT 1
                 FROM VBAK_VBAP
                WHERE VBUP_VBELN = VBAP_VBELN AND VBUP_POSNR = VBAP_POSNR)
       AND NOT EXISTS
                  (SELECT 1
                     FROM fact_salesorder
                    WHERE dd_SalesDocNo = VBUP_VBELN
                          AND dd_SalesItemNo = VBUP_POSNR);

    
UPDATE dim_salesorderitemstatus 
SET OverallReferenceStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus ,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA
AND OverallReferenceStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus 
SET ConfirmationStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA
AND  ConfirmationStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus 
SET DeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA
AND DeliveryStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus 
SET OverallDeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA
AND OverallDeliveryStatus <>  ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus 
SET BillingStatusDeliveryRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA
AND BillingStatusDeliveryRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus 
SET BillingStatusOrderRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA
AND  BillingStatusOrderRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus 
SET ItemRejectionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus ,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA
AND ItemRejectionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus 
SET OverallProcessingStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
from VBUP,dd07t t,dim_salesorderitemstatus
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA
AND OverallProcessingStatus <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus 
SET GeneralIncompletionStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
from VBUP,dd07t t,dim_salesorderitemstatus
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL
AND  GeneralIncompletionStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus 
SET DelayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA
AND DelayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus 
SET PODStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus ,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA
AND  PODStatus  <> ifnull( t.DD07T_DDTEXT ,'Not Set') ;

/*
UPDATE dim_salesorderitemstatus 
SET GoodsMovementStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA
AND  GoodsMovementStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set') */



merge into dim_salesorderitemstatus ds
using 
(select distinct dim_salesorderitemstatusid, ifnull(t.DD07T_DDTEXT,'Not Set') as DD07T_DDTEXT
FROM LIKP_LIPS_VBUP, DD07T t, dim_salesorderitemstatus
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA
) x
on ds.dim_salesorderitemstatusid = x.dim_salesorderitemstatusid
when matched then
update set ds.GoodsMovementStatus = x.DD07T_DDTEXT
where ds.GoodsMovementStatus <> ifnull(x.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus 
SET PickingPutawayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA
AND PickingPutawayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus 
Set ItemDeliveryBlockStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
from dim_salesorderitemstatus,VBUP,dd07t t
Where SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LSSTA
AND  ItemDeliveryBlockStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus 
   SET    ItemDataforDeliv = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
FROM dim_salesorderitemstatus,VBUP, DD07T t
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK
  AND ItemDataforDeliv <> ifnull(t.DD07T_DDTEXT,'Not Set');
  
UPDATE dim_salesorderitemstatus 
   SET    GeneralIncompletionStatusCode = ifnull(t.DD07T_DOMVALUE,'Not Set'),
			dw_update_date = current_timestamp
FROM dim_salesorderitemstatus,VBUP, DD07T t
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL
  AND  GeneralIncompletionStatusCode <> ifnull(t.DD07T_DOMVALUE ,'Not Set');
  
UPDATE dim_salesorderitemstatus 
   SET    GeneralIncompletionProcessingStatusOfItem = ifnull(t.TVBST_BEZEI,'Not Set'),
			dw_update_date = current_timestamp
FROM dim_salesorderitemstatus,VBUP, TVBST t
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND convert(varchar(1),VBUP_UVALL) = convert(varchar(1),t.TVBST_STATU) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'
  AND  GeneralIncompletionProcessingStatusOfItem <> ifnull(t.TVBST_BEZEI ,'Not Set');

  
delete from number_fountain m where m.table_name = 'dim_salesorderitemstatus';

insert into number_fountain
select 	'dim_salesorderitemstatus',
	ifnull(max(d.dim_salesorderitemstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemstatus d
where d.dim_salesorderitemstatusid <> 1; 
  
  
/*INSERT FROM VBAK_VBAP_VBEP */
  
/*INSERT INTO dim_salesorderitemstatus(
          dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
	  ItemDeliveryBlockStatus,
	  ItemDataforDeliv,
	  GeneralIncompletionStatusCode,
	  GeneralIncompletionProcessingStatusOfItem
          )
  SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by dtbl1.VBUP_VBELN,dtbl1.VBUP_POSNR)),dtbl1.*
 FROM (SELECT  distinct
          VBUP_VBELN,
          VBUP_POSNR,
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LSSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK),'Not Set'),
	  ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
	  ifnull((select t.TVBST_BEZEI from TVBST t where varchar(VBUP_UVALL,1) = varchar(t.TVBST_STATU,1) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'),'Not Set')
FROM VBUP inner join VBAK_VBAP_VBEP on VBAK_VBELN = VBUP_VBELN and VBAP_POSNR = VBUP_POSNR
WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR)) dtbl1*/

DROP TABLE IF EXISTS tmp_VBUP;
CREATE TABLE tmp_VBUP AS 
SELECT  distinct 
VBUP_VBELN as SalesDocumentNumber,
VBUP_POSNR as SalesItemNumber,
convert(varchar(60),'Not Set') as  OverallReferenceStatus,
convert(varchar(60),'Not Set') as ConfirmationStatus,
convert(varchar(60),'Not Set') as DeliveryStatus,
convert(varchar(60),'Not Set') as  OverallDeliveryStatus,
convert(varchar(60),'Not Set') as BillingStatusDeliveryRelated,
convert(varchar(60),'Not Set') as BillingStatusOrderRelated,
convert(varchar(60),'Not Set') as ItemRejectionStatus,
convert(varchar(60),'Not Set') as OverallProcessingStatus,
convert(varchar(60),'Not Set') as GeneralIncompletionStatus,
convert(varchar(60),'Not Set') as DelayStatus,
convert(varchar(60),'Not Set') as PODStatus,
convert(varchar(60),'Not Set') as GoodsMovementStatus,
convert(varchar(60),'Not Set') as PickingPutawayStatus,
convert(varchar(60),'Not Set') as ItemDeliveryBlockStatus,
convert(varchar(60),'Not Set') as  ItemDataforDeliv,
convert(varchar(60),'Not Set') as GeneralIncompletionStatusCode,
convert(varchar(60),'Not Set') as   GeneralIncompletionProcessingStatusOfItem,
          VBUP_RFGSA,
          VBUP_BESTA,
          VBUP_LFSTA,
          VBUP_LFGSA,
          VBUP_FKSTA,
          VBUP_FKSAA,
          VBUP_ABSTA,
          VBUP_GBSTA,
          VBUP_UVALL,
          VBUP_DCSTA,
          VBUP_PDSTA,
          VBUP_WBSTA,
          VBUP_KOSTA,
	      VBUP_LSSTA,
	      VBUP_UVVLK
FROM VBUP inner join VBAK_VBAP_VBEP on VBAK_VBELN = VBUP_VBELN and VBAP_POSNR = VBUP_POSNR
WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR);


UPDATE tmp_VBUP
SET OverallReferenceStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_RFGSA;
	 
	 
UPDATE tmp_VBUP
SET ConfirmationStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_BESTA;
	 
UPDATE tmp_VBUP
SET DeliveryStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_LFSTA;

UPDATE tmp_VBUP
SET OverallDeliveryStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_LFGSA;
	 
UPDATE tmp_VBUP
SET BillingStatusDeliveryRelated = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_FKSTA;
	 
	 
UPDATE tmp_VBUP
SET BillingStatusOrderRelated = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_FKSAA;
	 
UPDATE tmp_VBUP
SET ItemRejectionStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_ABSTA;
	 
	 
UPDATE tmp_VBUP
SET OverallProcessingStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_GBSTA;
	 
UPDATE tmp_VBUP
SET GeneralIncompletionStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_UVALL;

UPDATE tmp_VBUP
SET DelayStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_DCSTA;
	 
	 
UPDATE tmp_VBUP
SET PODStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_PDSTA;
	 
UPDATE tmp_VBUP
SET GoodsMovementStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_WBSTA;
	 
UPDATE tmp_VBUP
SET PickingPutawayStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_KOSTA;

UPDATE tmp_VBUP
SET ItemDeliveryBlockStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_LSSTA;
	 
UPDATE tmp_VBUP
SET ItemDataforDeliv = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_UVVLK;
	 
	 
UPDATE tmp_VBUP
SET GeneralIncompletionStatusCode = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_UVALL;

UPDATE tmp_VBUP
SET GeneralIncompletionProcessingStatusOfItem = IFNULL(t.TVBST_BEZEI, 'Not Set')
FROM tmp_VBUP, TVBST t
WHERE CONVERT(varchar(1),VBUP_UVALL) = CONVERT(varchar(1),t.TVBST_STATU) 
	  AND t.TVBST_TBNAM = 'VBUP' 
      AND t.TVBST_FDNAM = 'UVALL';

INSERT INTO dim_salesorderitemstatus
(
	      dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
	      ItemDataforDeliv,
	      GeneralIncompletionStatusCode,
	      GeneralIncompletionProcessingStatusOfItem
          )
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by tmp.SalesDocumentNumber,tmp.SalesItemNumber)),
 SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
	      ItemDataforDeliv,
	      GeneralIncompletionStatusCode,
	      GeneralIncompletionProcessingStatusOfItem
FROM tmp_VBUP tmp;













delete from number_fountain m where m.table_name = 'dim_salesorderitemstatus';

insert into number_fountain
select 	'dim_salesorderitemstatus',
	ifnull(max(d.dim_salesorderitemstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemstatus d
where d.dim_salesorderitemstatusid <> 1; 

 /*INSERT FROM VBAK_VBAP */     
  /*INSERT
  INTO dim_salesorderitemstatus(
	  dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
	  ItemDataforDeliv,
	  GeneralIncompletionStatusCode,
	  GeneralIncompletionProcessingStatusOfItem
          )
  SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by dtbl1.VBUP_VBELN,dtbl1.VBUP_POSNR)),dtbl1.*
 FROM (SELECT  distinct
          VBUP_VBELN,
          VBUP_POSNR,
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA),'Not Set'),
          ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LSSTA),'Not Set'),
	  ifnull((select t.DD07T_DDTEXT from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK),'Not Set'),
	  ifnull((select t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL),'Not Set'),
	  ifnull((select t.TVBST_BEZEI from TVBST t where varchar(VBUP_UVALL,1) = varchar(t.TVBST_STATU,1) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'),'Not Set')
  FROM VBUP inner join VBAK_VBAP on VBAP_VBELN = VBUP_VBELN and VBAP_POSNR = VBUP_POSNR
  WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR)) dtbl1*/

DROP TABLE IF EXISTS tmp_VBUP;
CREATE TABLE tmp_VBUP AS 
SELECT  distinct 
VBUP_VBELN as SalesDocumentNumber,
VBUP_POSNR as SalesItemNumber,
convert(varchar(60),'Not Set') as  OverallReferenceStatus,
convert(varchar(60),'Not Set') as ConfirmationStatus,
convert(varchar(60),'Not Set') as DeliveryStatus,
convert(varchar(60),'Not Set') as  OverallDeliveryStatus,
convert(varchar(60),'Not Set') as BillingStatusDeliveryRelated,
convert(varchar(60),'Not Set') as BillingStatusOrderRelated,
convert(varchar(60),'Not Set') as ItemRejectionStatus,
convert(varchar(60),'Not Set') as OverallProcessingStatus,
convert(varchar(60),'Not Set') as GeneralIncompletionStatus,
convert(varchar(60),'Not Set') as DelayStatus,
convert(varchar(60),'Not Set') as PODStatus,
convert(varchar(60),'Not Set') as GoodsMovementStatus,
convert(varchar(60),'Not Set') as PickingPutawayStatus,
convert(varchar(60),'Not Set') as ItemDeliveryBlockStatus,
convert(varchar(60),'Not Set') as  ItemDataforDeliv,
convert(varchar(60),'Not Set') as GeneralIncompletionStatusCode,
convert(varchar(60),'Not Set') as   GeneralIncompletionProcessingStatusOfItem,
          VBUP_RFGSA,
          VBUP_BESTA,
          VBUP_LFSTA,
          VBUP_LFGSA,
          VBUP_FKSTA,
          VBUP_FKSAA,
          VBUP_ABSTA,
          VBUP_GBSTA,
          VBUP_UVALL,
          VBUP_DCSTA,
          VBUP_PDSTA,
          VBUP_WBSTA,
          VBUP_KOSTA,
	      VBUP_LSSTA,
	      VBUP_UVVLK
FROM VBUP inner join VBAK_VBAP on VBAP_VBELN = VBUP_VBELN and VBAP_POSNR = VBUP_POSNR
WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR);


UPDATE tmp_VBUP
SET OverallReferenceStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_RFGSA;
	 
	 
UPDATE tmp_VBUP
SET ConfirmationStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_BESTA;
	 
UPDATE tmp_VBUP
SET DeliveryStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_LFSTA;

UPDATE tmp_VBUP
SET OverallDeliveryStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_LFGSA;
	 
UPDATE tmp_VBUP
SET BillingStatusDeliveryRelated = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_FKSTA;
	 
	 
UPDATE tmp_VBUP
SET BillingStatusOrderRelated = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_FKSAA;
	 
UPDATE tmp_VBUP
SET ItemRejectionStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_ABSTA;
	 
	 
UPDATE tmp_VBUP
SET OverallProcessingStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_GBSTA;
	 
UPDATE tmp_VBUP
SET GeneralIncompletionStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_UVALL;

UPDATE tmp_VBUP
SET DelayStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_DCSTA;
	 
	 
UPDATE tmp_VBUP
SET PODStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_PDSTA;
	 
UPDATE tmp_VBUP
SET GoodsMovementStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_WBSTA;
	 
UPDATE tmp_VBUP
SET PickingPutawayStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_KOSTA;

UPDATE tmp_VBUP
SET ItemDeliveryBlockStatus = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_LSSTA;
	 
UPDATE tmp_VBUP
SET ItemDataforDeliv = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_UVVLK;
	 
	 
UPDATE tmp_VBUP
SET GeneralIncompletionStatusCode = IFNULL(t.DD07T_DDTEXT, 'Not Set')
FROM tmp_VBUP, dd07t t
WHERE t.DD07T_DOMNAME = 'STATV' 
     AND t.DD07T_DOMVALUE = VBUP_UVALL;

UPDATE tmp_VBUP
SET GeneralIncompletionProcessingStatusOfItem = IFNULL(t.TVBST_BEZEI, 'Not Set')
FROM tmp_VBUP, TVBST t
WHERE CONVERT(varchar(1),VBUP_UVALL) = CONVERT(varchar(1),t.TVBST_STATU) 
	  AND t.TVBST_TBNAM = 'VBUP' 
      AND t.TVBST_FDNAM = 'UVALL';
  
  
  
  
INSERT INTO dim_salesorderitemstatus
(
	      dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
	      ItemDataforDeliv,
	      GeneralIncompletionStatusCode,
	      GeneralIncompletionProcessingStatusOfItem
          )
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by tmp.SalesDocumentNumber,tmp.SalesItemNumber)),
 SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
	      ItemDataforDeliv,
	      GeneralIncompletionStatusCode,
	      GeneralIncompletionProcessingStatusOfItem
FROM tmp_VBUP tmp;

DROP TABLE IF EXISTS tmp_VBUP;