INSERT INTO dim_salesorg(Dim_SalesOrgid,
                         SalesOrgCode,
                         SalesOrgName,
                         CompanyCode,
                         AddressKey,
                         CustomerNumber,
                         SalesOrgCalendar,
                         StatCurrencyKey)
   SELECT 1,
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set'		  
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorg
               WHERE Dim_SalesOrgid = 1);

UPDATE       dim_salesorg a
   SET a.SalesOrgName = ifnull(TVKOT_VTEXT, 'Not Set'),
       a.CompanyCode = ifnull(TVKO_BUKRS, 'Not Set'),
       a.AddressKey = ifnull(TVKO_ADRNR, 'Not Set'),
       a.CustomerNumber = ifnull(TVKO_KUNNR, 'Not Set'),
       a.SalesOrgCalendar = ifnull(TVKO_VKOKL, 'Not Set'),
       a.StatCurrencyKey = ifnull(TVKO_WAERS, 'Not Set'),
			a.dw_update_date = current_timestamp
          FROM  dim_salesorg a 
	  		inner join TVKO t ON t.TVKO_VKORG = a.SalesOrgCode
       			INNER JOIN TVKOT i ON t.TVKO_VKORG = i.TVKOT_VKORG;

delete from number_fountain m where m.table_name = 'dim_salesorg';

insert into number_fountain
select 	'dim_salesorg',
	ifnull(max(d.Dim_SalesOrgid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorg d
where d.Dim_SalesOrgid <> 1; 
	   
INSERT INTO dim_salesorg(Dim_SalesOrgid,
                         SalesOrgCode,
                         SalesOrgName,
                         CompanyCode,
                         AddressKey,
                         CustomerNumber,
                         SalesOrgCalendar,
                         StatCurrencyKey)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorg') 
          + row_number() over(order by ''),
			  TVKO_VKORG,
          ifnull(TVKOT_VTEXT, 'Not Set'),
          ifnull(TVKO_BUKRS, 'Not Set'),
          ifnull(TVKO_ADRNR, 'Not Set'),
          ifnull(TVKO_KUNNR, 'Not Set'),
          ifnull(TVKO_VKOKL, 'Not Set'),
          ifnull(TVKO_WAERS, 'Not Set')
     FROM    TVKO
          INNER JOIN
             TVKOT
          ON TVKO_VKORG = TVKOT_VKORG
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorg a
               WHERE a.SalesOrgCode = TVKO_VKORG);	   