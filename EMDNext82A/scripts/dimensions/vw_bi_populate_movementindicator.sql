UPDATE    dim_movementindicator mi
   SET mi.Description = ifnull(t.DD07T_DDTEXT, 'Not Set'),
			mi.dw_update_date = current_timestamp
			  FROM DD07T t, dim_movementindicator mi
 WHERE mi.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'KZBEW'
          AND mi.TypeCode = ifnull(t.DD07T_DOMVALUE, 'Not Set');

INSERT INTO dim_movementindicator(dim_movementindicatorId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_movementindicator
               WHERE dim_movementindicatorId = 1);

delete from number_fountain m where m.table_name = 'dim_movementindicator';
   
insert into number_fountain
select 	'dim_movementindicator',
	ifnull(max(d.Dim_MovementIndicatorid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_movementindicator d
where d.Dim_MovementIndicatorid <> 1; 

INSERT INTO dim_movementindicator(Dim_MovementIndicatorid,
                          Description,
                          TypeCode,
                          RowStartDate,
                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_movementindicator') 
          + row_number() over(order by '') ,
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'KZBEW'
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_movementindicator
                    WHERE TypeCode = ifnull(DD07T_DOMVALUE, 'Not Set')
                          AND DD07T_DOMNAME = 'KZBEW')
   ORDER BY 2 
;
