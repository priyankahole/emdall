UPDATE    dim_tasklistusage u
      SET u.UsageName = t.T411T_TXT,
			u.dw_update_date = current_timestamp
			FROM
          dim_tasklistusage u,T411T t
 WHERE u.RowIsCurrent = 1
 AND  u.UsageCode = t.T411T_VERWE
;

INSERT INTO dim_tasklistusage(dim_tasklistusageId, RowIsCurrent,usagecode,usagename,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_tasklistusage
               WHERE dim_tasklistusageId = 1);

delete from number_fountain m where m.table_name = 'dim_tasklistusage';

insert into number_fountain
select 	'dim_tasklistusage',
	ifnull(max(d.dim_tasklistUsageid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_tasklistusage d
where d.dim_tasklistUsageid <> 1;

INSERT INTO dim_tasklistusage(dim_tasklistUsageid,
                              UsageCode,
                              UsageName,
                              RowStartDate,
                              RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_tasklistusage') 
          + row_number() over(order by ''),
			  t.T411T_VERWE,
          t.T411T_TXT,
          current_timestamp,
          1
     FROM T411T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_tasklistusage tlu
               WHERE tlu.UsageCode = t.T411T_VERWE AND tlu.RowIsCurrent = 1)
;
