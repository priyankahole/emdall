
INSERT INTO dim_bomitemcategory(dim_bomitemcategoryid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_bomitemcategory
               WHERE dim_bomitemcategoryid = 1);

UPDATE	dim_bomitemcategory bic
SET 	bic.ItemCatText = ifnull(t.T418T_PTEXT, 'Not Set'),
		bic.dw_update_date = current_timestamp
		FROM	dim_bomitemcategory bic, T418T t
WHERE bic.ItemCategory = t.T418T_POSTP 
AND bic.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_bomitemcategory';

insert into number_fountain
select 	'dim_bomitemcategory',
	ifnull(max(d.dim_bomitemcategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_bomitemcategory d
where d.dim_bomitemcategoryid <> 1;
			   
INSERT INTO dim_bomitemcategory(Dim_bomitemcategoryid,
                                                                ItemCatText,
                                ItemCategory,
                                RowStartDate,
                                RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_bomitemcategory')
          + row_number() over(order by ''),
                        ifnull(T418T_PTEXT, 'Not Set'),
            T418T_POSTP,
            current_timestamp,
            1
       FROM T418T
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_bomitemcategory
                    WHERE ItemCategory = T418T_POSTP)
   ORDER BY 2;

delete from number_fountain m where m.table_name = 'dim_bomitemcategory';
   