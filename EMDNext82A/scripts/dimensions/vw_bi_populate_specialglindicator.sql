INSERT INTO dim_specialglindicator(Dim_SpecialGLIndicatorId,
                                   AccountType,
                                   SpecialGLIndicator,
                                   Name,
                                   Description,
                                   RowStartDate,
                                   RowEndDate,
                                   RowIsCurrent,
                                   RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          current_timestamp,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_specialglindicator
               WHERE dim_specialglindicatorid = 1);


UPDATE    dim_specialglindicator sg
   SET sg.Name = t.T074T_KTEXT, sg.Description = t.T074T_LTEXT,
			sg.dw_update_date = current_timestamp
	FROM
          dim_specialglindicator sg, T074T t
 WHERE sg.RowIsCurrent = 1
 AND sg.AccountType = t.T074T_KOART
 AND sg.SpecialGLIndicator = t.T074T_SHBKZ;

delete from number_fountain m where m.table_name = 'dim_specialglindicator';

insert into number_fountain
select 	'dim_specialglindicator',
	ifnull(max(d.Dim_SpecialGLIndicatorId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_specialglindicator d
where d.Dim_SpecialGLIndicatorId <> 1;
		  
INSERT INTO dim_specialglindicator(Dim_SpecialGLIndicatorId,
                                   AccountType,
                                   SpecialGLIndicator,
                                   Name,
                                   Description,
                                   RowStartDate,
                                   RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_specialglindicator') 
          + row_number() over(order by ''),
			 T074T_KOART,
          T074T_SHBKZ,
          T074T_KTEXT,
          T074T_LTEXT,
          current_timestamp,
          1
     FROM T074T t
    WHERE NOT EXISTS
                     (SELECT 1
                        FROM dim_specialglindicator
                       WHERE     AccountType = t.T074T_KOART
                             AND SpecialGLIndicator = t.T074T_SHBKZ
                             AND RowIsCurrent = 1);