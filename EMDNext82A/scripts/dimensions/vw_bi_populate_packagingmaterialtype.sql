UPDATE    
dim_packagingmaterialtype s
SET s.Description = t.TVTYT_BEZEI,
	s.dw_update_date = current_timestamp
FROM
dim_packagingmaterialtype s,
TVTYT t
WHERE s.RowIsCurrent = 1
AND s.packagingmaterialtype = t.TVTYT_TRATY
;

INSERT INTO dim_packagingMaterialType(dim_packagingMaterialTypeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_packagingMaterialType
               WHERE dim_packagingMaterialTypeId = 1);

delete from number_fountain m where m.table_name = 'dim_packagingMaterialType';
   
insert into number_fountain
select 	'dim_packagingMaterialType',
	ifnull(max(d.dim_packagingMaterialTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_packagingMaterialType d
where d.dim_packagingMaterialTypeid <> 1; 

INSERT INTO dim_packagingMaterialType(dim_packagingMaterialTypeid,
                                packagingMaterialType,
                                Description,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_packagingMaterialType') 
          + row_number() over(order by ''),
	  t.TVTYT_TRATY,
          t.TVTYT_BEZEI,
          current_date,
          1
     FROM TVTYT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_packagingMaterialType r
               WHERE r.packagingMaterialType = t.TVTYT_TRATY AND r.RowIsCurrent = 1)
;
