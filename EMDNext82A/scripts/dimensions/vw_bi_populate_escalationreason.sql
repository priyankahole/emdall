UPDATE    
dim_escalationreason ds
SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set'),
	ds.dw_update_date = current_timestamp
FROM
dim_escalationreason ds,
SCMGATTR_SESCALT dt
WHERE ds.RowIsCurrent = 1
  AND ds.EscalReason = dt.ESCAL_REASON
  AND dt.ESCAL_REASON IS NOT NULL;
		  
INSERT INTO dim_escalationreason(dim_escalationreasonid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_escalationreason
               WHERE dim_escalationreasonid = 1);

delete from number_fountain m where m.table_name = 'dim_escalationreason';
   
insert into number_fountain
select 	'dim_escalationreason',
	ifnull(max(d.dim_escalationreasonid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_escalationreason d
where d.dim_escalationreasonid <> 1; 

INSERT INTO dim_escalationreason(dim_escalationreasonid,
                               EscalReason,
                               Description,
                               RowStartDate,
                               RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_escalationreason')
          + row_number() over(order by '') ,
            ifnull(ESCAL_REASON, 'Not Set'),
            ifnull(DESCRIPTION,'Not Set'),
            current_timestamp,
            1
       FROM SCMGATTR_SESCALT
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_escalationreason
                    WHERE EscalReason = ESCAL_REASON)
   ORDER BY 2;