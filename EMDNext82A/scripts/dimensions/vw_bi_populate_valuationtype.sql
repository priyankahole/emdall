UPDATE    
dim_ValuationType vt
SET AccountCategory = ifnull(T149D_KKREF, 'Not Set'),
	vt.dw_update_date = current_timestamp
FROM
dim_ValuationType vt,
T149D t
WHERE vt.RowIsCurrent = 1
AND t.T149D_BWTAR = vt.ValuationType;
 
INSERT INTO dim_valuationtype(dim_valuationtypeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_valuationtype
               WHERE dim_valuationtypeId = 1);

delete from number_fountain m where m.table_name = 'dim_valuationtype';

insert into number_fountain
select 	'dim_valuationtype',
	ifnull(max(d.dim_valuationtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_valuationtype d
where d.dim_valuationtypeid <> 1;	

INSERT INTO dim_valuationtype(dim_valuationtypeid,
				valuationType,
				accountcategory,
                          RowStartDate,
                          RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_valuationtype') 
          + row_number() over(order by ''),
	  ifnull( t149d_bwtar, 'Not Set'),
	ifnull( t149d_KKREF, 'Not Set'),
            current_timestamp,
	    1
       FROM t149d
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_valuationtype
                    WHERE ValuationType = ifnull( t149d_bwtar, 'Not Set'));
