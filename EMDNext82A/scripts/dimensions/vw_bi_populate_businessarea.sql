
INSERT INTO dim_businessarea(dim_businessareaid, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_businessarea
               WHERE dim_businessareaid = 1);

UPDATE    dim_businessarea ba
       SET ba.Description = t.TGSBT_GTEXT,
			ba.dw_update_date = current_timestamp
		FROM
          dim_businessarea ba, TGSBT t
 WHERE ba.RowIsCurrent = 1
 AND ba.BusinessArea = t.TGSBT_GSBER;

delete from number_fountain m where m.table_name = 'dim_businessarea';

insert into number_fountain
select 	'dim_businessarea',
	ifnull(max(d.dim_businessareaid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_businessarea d
where d.dim_businessareaid <> 1;

INSERT INTO dim_businessarea(Dim_BusinessAreaId,
                                                        BusinessArea,
                             Description,
                             RowStartDate,
                             RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_businessarea')
          + row_number() over(order by '') ,
                        TGSBT_GSBER,
          TGSBT_GTEXT,
          current_timestamp,
          1
     FROM TGSBT t
    WHERE NOT EXISTS
                     (SELECT 1
                        FROM dim_businessarea ba
                       WHERE ba.BusinessArea = t.TGSBT_GSBER
                             AND ba.RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_businessarea';
							 