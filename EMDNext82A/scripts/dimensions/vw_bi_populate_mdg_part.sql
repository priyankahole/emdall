/**********************************************************************************/
/*  06 Nov 2015   1.00		  LiviuT     First version */
/*  10 Oct 2016   1.01        CristianT  Removed droping tables with source from EMDTEMPOCC4 schema. Added truncate and insert logic */
/*  29 Mar 2018   1.10        CristianT  Added new fields for APP-8732            */
/**********************************************************************************/

drop table if exists tmp_part;
create table tmp_part as
select distinct
partnumber,
PartDescription  as PARTDESCRIPTION,
PartType as materialtype,
PartTypeDescription as materialtypedescr,
SIGMA_PRIMARY_MANUF as primary_production_location,
SIGMA_PRIMARY_MANUF as primary_production_location_name,
Laboratory,
productgroupsbu,
row_number() over (partition by PartNumber  order by '' ) rno
from dim_part
where PartNumber <>  'Not Set';


UPDATE  dim_mdg_part m
SET m.mattypeforglbrep = t.materialtype,
m.mattypeforglbrepdescr = t.materialtypedescr,
m.glbproductgroup = t.productgroupsbu,
m.PARTDESCRIPTION = t.PARTDESCRIPTION,
m.materialtype  =   t.materialtype,
m.materialtypedescr = t.materialtypedescr
FROM dim_mdg_part m, tmp_part t
WHERE  m.partnumber = t.PartNumber
 and milliporeflag ='Not Set'
    and rno = 1;

DELETE FROM number_fountain m
WHERE m.table_name = 'dim_mdg_part';

INSERT INTO number_fountain
SELECT 'dim_mdg_part',
       ifnull(MAX(d.dim_mdg_partid ), ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_mdg_part d
WHERE d.dim_mdg_partid <> 1;


insert into dim_mdg_part
(
dim_mdg_partid,
partnumber,
PARTDESCRIPTION,
materialtype,
materialtypedescr,
primary_production_location,
primary_production_location_name,
mattypeforglbrep,
mattypeforglbrepdescr,
glbproductgroup
)
SELECT
 (SELECT ifnull(m.max_id, 1)
        FROM number_fountain m
        WHERE m.table_name = 'dim_mdg_part') + ROW_NUMBER() OVER(ORDER BY '') as dim_mdg_partid,
partnumber,
PARTDESCRIPTION,
materialtype,
materialtypedescr,
'Not Set' as primary_production_location,
'Not Set' as primary_production_location_name,
materialtype as mattypeforglbrep,
materialtypedescr as mattypeforglbrepdescr,
productgroupsbu as glbproductgroup
FROM tmp_part m
WHERE rno = 1 AND NOT EXISTS (SELECT 'x'
                  FROM dim_mdg_part dmp
                  WHERE dmp.partnumber = m.partnumber
                     AND milliporeflag ='Not Set');

/*drop table if exists update_mdgm
create table update_mdgm
as select distinct MDGM_MATERIAL_NO,partnumber
from dim_part
where MDGM_MATERIAL_NO<>'Not Set'*/

drop table if exists update_mdgm;
create table update_mdgm
as select distinct MAX(TRIM(LEADING '0' FROM MDGM_MATERIAL_NO)) MDGM_MATERIAL_NO,partnumber
from dim_part
where MDGM_MATERIAL_NO<>'Not Set'
group by partnumber;



/*Roxana D 2018-02-12*/
update dim_mdg_part mdg
set mdg.MDGPartNumber_NoLeadZero=  CASE WHEN MDGM_MATERIAL_NO<>'Not Set' or MDGM_MATERIAL_NO is not null
  then MDGM_MATERIAL_NO
		     ELSE
			ifnull(case when length(mdg.partnumber) = 18 and
mdg.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from mdg.partnumber) else mdg.partnumber end,'Not Set')
		     END
from dim_mdg_part mdg
left join update_mdgm u
on  mdg.partnumber=u.partnumber
where mdg.MDGPartNumber_NoLeadZero<>CASE WHEN MDGM_MATERIAL_NO<>'Not Set' or MDGM_MATERIAL_NO is not null
  then MDGM_MATERIAL_NO
		     ELSE
			ifnull(case when length(mdg.partnumber) = 18 and
mdg.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from mdg.partnumber) else mdg.partnumber end,'Not Set')
		     END;
		     
/**/

drop table if exists tmp_part;
create table tmp_part as
select distinct
 TRIM(LEADING '0' FROM MDGM_MATERIAL_NO) AS MDGM_MATERIAL_NO ,
 partnumber,
 PartDescription  as PARTDESCRIPTION,
 PartType as materialtype,
 PartTypeDescription as materialtypedescr,
/*SIGMA_PRIMARY_MANUF as primary_production_location,*/
/*SIGMA_PRIMARY_MANUF as primary_production_location_name,*/
 Laboratory,
 productgroupsbu as glbproductgroup,
 row_number() over (partition by  TRIM(LEADING '0' FROM MDGM_MATERIAL_NO)  order by '' ) rno
from dim_part;
/*
where  MDGM_MATERIAL_NO <>  'Not Set'*/


update dim_mdg_part mdg
set

mdg.PARTDESCRIPTION=m.PARTDESCRIPTION
,mdg.materialtype=m.materialtype
,mdg.materialtypedescr=m.materialtypedescr
/*,mdg.primary_production_location=m.primary_production_location*/
/*,mdg.primary_production_location_name=m.primary_production_location_name*/
,mdg.mattypeforglbrep=m.materialtype
,mdg.mattypeforglbrepdescr=m.materialtypedescr
,mdg.glbproductgroup=m.glbproductgroup

from dim_mdg_part mdg,tmp_part m
where mdg.MDGPartNumber_NoLeadZero = CASE WHEN MDGM_MATERIAL_NO<>'Not Set'  then MDGM_MATERIAL_NO
		     ELSE
			ifnull(case when length(m.partnumber) = 18 and m.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from m.partnumber) else m.partnumber end,'Not Set')
		     END
and m.rno = 1;


update dim_mdg_part
set IndicatorSalesCatalogue = ifnull(YYD_YKZLP,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(IndicatorSalesCatalogue,'a') <> ifnull(YYD_YKZLP,'Not Set');

update dim_mdg_part dmp
set dmp.IndicatorSalesCatalogueDesc = ifnull(cd.description,'Not Set')
from dim_mdg_part dmp,  catalogue_desc cd
where dmp.IndicatorSalesCatalogue = cd.code
	and ifnull(IndicatorSalesCatalogueDesc,'a') <> ifnull(cd.description,'Not Set');


/* 21 March 2017 Cornelia add PRM and PRP */
drop table if exists tmp_aditionam_mdm_atributes;
create table tmp_aditionam_mdm_atributes
as
select a.md_mmmaterial,a.usmdkmmmaterial,b.usmd_active,b.usmd_o_yorg,b.md_mmyyd_orga,b.md_mmyyd_yorga
from MDG_YYD_V_MAPPING a
	inner join MDG_YYD_V_YORG b on a.usmdkmmmaterial = b.usmdkmmmaterial and usmd_active = 1 and ifnull(usmd_o_yorg,'Y') <> 'X';

update dim_mdg_part d
set d.PRM_Product_manager_code = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PRM'
	and ifnull(d.PRM_Product_manager_code,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.PRM_Product_manager_desc = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.PRM_Product_manager_code = t.YYD_YORGA and d.PRM_Product_manager_desc <> ifnull(t.LTEXT,'Not Set');


update dim_mdg_part d
set d.PRP_Price_responsible_code = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PRP'
	and ifnull(d.PRP_Price_responsible_code,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.PRP_Price_responsible_desc = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.PRP_Price_responsible_code = t.YYD_YORGA and d.PRP_Price_responsible_desc <> ifnull(t.LTEXT,'Not Set');


drop table if exists update_mdg_new;
create table update_mdg_new
as select distinct
MATERIALTYPEDESCR
,GLOBALBRAND
,APICODE
,MATTYPEFORGLBREPDESCR
,ARTICLEGROUP
,ACT_CONV_NEW
,MAINGROUP
,INDIAMATERIALS
,ZBW_MAINGROUP
,ZBW_BUSINESSUNIT
,PRODUCTHIERARCHYDESCR
,BASEUNITOFMEASURE
,ZBW_HIER_DESCRIPTION_LVL4
,PRODUCT_HIERARCHY_DESCRIPTION2
,ZBW_HIER_DESCRIPTION_LVL2
,PRODUCTMANAGER
,ZBW_HIER_PARENT_LVL3
,ZBW_HIER_DESCRIPTION_LVL6
,GLOBAL_SUPPLY_PLANNER_DESC
,GENERALITEMCATGROUP
,MATERIALCATEGORY
,PRM_PRODUCT_MANAGER_CODE
,ZBW_LSKU
,PRODUCT_HIERARCHY_DESCRIPTION
,MINREMAININGSHELFLIFE
,ZBW_HIER_PARENT_LVL8
,GLOBAL_OPERATIONAL_HIERARCHY
,MATTYPEFORGLBREP
,ZBW_PRODUCTTYPE
,MAINGROUPDESC
,UNIT2
,ARTICLENUMBER
,ZBW_HIER_DESCRIPTION_LVL9
,OLDMATNUMBER
,PRICE_HIERARCHY
,COGSICMINDICATOR
,UPPERGROUPDESC
,GLBPRODUCTGROUP
,WEIGHTUNIT
,GLBABCINDICATOR
,IMCCODEDESCR
,DIM_MDG_PARTID
,PRIMARY_PRODUCTION_LOCATION_NAME
,ZBW_HIER_PARENT_LVL6
,PRIMARY_PRODUCTION_LOCATION
,COMPLETECONTENTOFTHEPACKAGE
,PPU_UNITS
,ZBW_UPPERGROUP
,PRODUCT_HIERARCHY
,GAUSSUOMDESC
,ZBW_INTERNTAIONALARTICLE
,MATERIALTYPE
,PROJECTSOURCEID
,TOTALSHELFLIFE
,PRIMARYPRODUCTIONLOCATION
,PPU
,ZBW_HIER_DESCRIPTION_LVL1
,UPPERGROUP
,IMCCODE_AND_DESCR
,PRICE_HIERARCHY_DESCRIPTION
,ZBW_BUSINESSLINE
,GAUSSUOM
,GLOBAL_OPERATIONAL_HIERARCHY_DESCRIPTION
,MDGPARTNUMBER_NOLEADZERO
,CROSSPLANTMATSTATUSDESCR
,PARTDESCRIPTION
,GLOBAL_SUPPLY_PLANNER
,ZBW_HIER_DESCRIPTION_LVL10
,ZBW_HIER_PARENT_LVL7
,PPU_NEW
,PRP_PRICE_RESPONSIBLE_CODE
,SUPPLYCHAINPLANINGPPI
,ZBW_HIER_DESCRIPTION_LVL3
,GENERALITEMCATGROUPDESCR
,PRP_PRICE_RESPONSIBLE_DESC
,ZBW_HIER_PARENT_LVL4
,ZBW_HIER_DESCRIPTION_LVL5
,INDICATORSALESCATALOGUEDESC
,BATCHMANREQIND
,INTERNATIONALARTICLE
,ACT_CONV
,INTERNATIONALARTICLEDESC
,ZBW_HIER_PARENT_LVL9
,IMCCODE
,APORELEVANCE
,ZBW_HIER_PARENT_LVL10
,PARTNUMBER
,APORELEVANCEDESC
,PRODUCT_HIERARCHY_PHOENIX_DESCRIPTION
,ZBW_PRODUCTGROUP
,PRM_PRODUCT_MANAGER_DESC
,PLANNED_PRODUCT_HIERARCHY_DESCRIPTION
,ZBW_HIER_PARENT_LVL1
,ZBW_HIER_PARENT_LVL5
,ZBW_ARTICLEGROUP
,ZBW_DIVISION
,ZBW_HIER_DESCRIPTION_LVL7
,ARTICLEGROUPDESC
,GLOBAL_DEMAND_PLANNER
,DW_UPDATE_DATE
,GLOBAL_DEMAND_PLANNER_DESC
,ZBW_HIER_DESCRIPTION_LVL8
,ZBW_DESCRIPTION
,ZBW_HIER_PARENT_LVL2
,INDICATORSALESCATALOGUE
,ZBW_BUSINESSFIELD
,APICODEDESCR
,ZBW_VALUATIONCLASS
,DOSAGE
,UNITOFDOSAGE
,GLOBALBRANDDESCR
,PLANNED_PRODUCT_HIERARCHY
,CROSSPLANTMATSTATUS
,PRODUCT_HIERARCHY_PHOENIX
,NETWEIGHT
,PRODUCTHIERARCHY
,UNITOFCOMPLETECONTENTOFTHEPACKAGE
from emd586.dim_mdg_part mdg
where projectsourceid in ('1');


UPDATE  dim_mdg_part mdg
SET
mdg.MATERIALTYPEDESCR=m.MATERIALTYPEDESCR
,mdg.GLOBALBRAND=m.GLOBALBRAND
,mdg.APICODE=m.APICODE
,mdg.MATTYPEFORGLBREPDESCR=m.MATTYPEFORGLBREPDESCR
,mdg.ARTICLEGROUP=m.ARTICLEGROUP
,mdg.ACT_CONV_NEW=m.ACT_CONV_NEW
,mdg.MAINGROUP=m.MAINGROUP
,mdg.INDIAMATERIALS=m.INDIAMATERIALS
,mdg.ZBW_MAINGROUP=m.ZBW_MAINGROUP
,mdg.ZBW_BUSINESSUNIT=m.ZBW_BUSINESSUNIT
,mdg.PRODUCTHIERARCHYDESCR=m.PRODUCTHIERARCHYDESCR
,mdg.BASEUNITOFMEASURE=m.BASEUNITOFMEASURE
,mdg.ZBW_HIER_DESCRIPTION_LVL4=m.ZBW_HIER_DESCRIPTION_LVL4
,mdg.PRODUCT_HIERARCHY_DESCRIPTION2=m.PRODUCT_HIERARCHY_DESCRIPTION2
,mdg.ZBW_HIER_DESCRIPTION_LVL2=m.ZBW_HIER_DESCRIPTION_LVL2
,mdg.PRODUCTMANAGER=m.PRODUCTMANAGER
,mdg.ZBW_HIER_PARENT_LVL3=m.ZBW_HIER_PARENT_LVL3
,mdg.ZBW_HIER_DESCRIPTION_LVL6=m.ZBW_HIER_DESCRIPTION_LVL6
,mdg.GLOBAL_SUPPLY_PLANNER_DESC=m.GLOBAL_SUPPLY_PLANNER_DESC
,mdg.GENERALITEMCATGROUP=m.GENERALITEMCATGROUP
,mdg.MATERIALCATEGORY=m.MATERIALCATEGORY
,mdg.PRM_PRODUCT_MANAGER_CODE=m.PRM_PRODUCT_MANAGER_CODE
,mdg.ZBW_LSKU=m.ZBW_LSKU
,mdg.PRODUCT_HIERARCHY_DESCRIPTION=m.PRODUCT_HIERARCHY_DESCRIPTION
,mdg.MINREMAININGSHELFLIFE=m.MINREMAININGSHELFLIFE
,mdg.ZBW_HIER_PARENT_LVL8=m.ZBW_HIER_PARENT_LVL8
,mdg.GLOBAL_OPERATIONAL_HIERARCHY=m.GLOBAL_OPERATIONAL_HIERARCHY
,mdg.MATTYPEFORGLBREP=m.MATTYPEFORGLBREP
,mdg.ZBW_PRODUCTTYPE=m.ZBW_PRODUCTTYPE
,mdg.MAINGROUPDESC=m.MAINGROUPDESC
,mdg.UNIT2=m.UNIT2
,mdg.ARTICLENUMBER=m.ARTICLENUMBER
,mdg.ZBW_HIER_DESCRIPTION_LVL9=m.ZBW_HIER_DESCRIPTION_LVL9
,mdg.OLDMATNUMBER=m.OLDMATNUMBER
,mdg.PRICE_HIERARCHY=m.PRICE_HIERARCHY
,mdg.COGSICMINDICATOR=m.COGSICMINDICATOR
,mdg.UPPERGROUPDESC=m.UPPERGROUPDESC
,mdg.GLBPRODUCTGROUP=m.GLBPRODUCTGROUP
,mdg.WEIGHTUNIT=m.WEIGHTUNIT
,mdg.GLBABCINDICATOR=m.GLBABCINDICATOR
,mdg.IMCCODEDESCR=m.IMCCODEDESCR
,mdg.PRIMARY_PRODUCTION_LOCATION_NAME=m.PRIMARY_PRODUCTION_LOCATION_NAME
,mdg.ZBW_HIER_PARENT_LVL6=m.ZBW_HIER_PARENT_LVL6
,mdg.PRIMARY_PRODUCTION_LOCATION=m.PRIMARY_PRODUCTION_LOCATION
,mdg.COMPLETECONTENTOFTHEPACKAGE=m.COMPLETECONTENTOFTHEPACKAGE
,mdg.PPU_UNITS=m.PPU_UNITS
,mdg.ZBW_UPPERGROUP=m.ZBW_UPPERGROUP
,mdg.PRODUCT_HIERARCHY=m.PRODUCT_HIERARCHY
,mdg.GAUSSUOMDESC=m.GAUSSUOMDESC
,mdg.ZBW_INTERNTAIONALARTICLE=m.ZBW_INTERNTAIONALARTICLE
,mdg.MATERIALTYPE=m.MATERIALTYPE
,mdg.TOTALSHELFLIFE=m.TOTALSHELFLIFE
,mdg.PRIMARYPRODUCTIONLOCATION=m.PRIMARYPRODUCTIONLOCATION
,mdg.PPU=m.PPU
,mdg.ZBW_HIER_DESCRIPTION_LVL1=m.ZBW_HIER_DESCRIPTION_LVL1
,mdg.UPPERGROUP=m.UPPERGROUP
,mdg.IMCCODE_AND_DESCR=m.IMCCODE_AND_DESCR
,mdg.PRICE_HIERARCHY_DESCRIPTION=m.PRICE_HIERARCHY_DESCRIPTION
,mdg.ZBW_BUSINESSLINE=m.ZBW_BUSINESSLINE
,mdg.GAUSSUOM=m.GAUSSUOM
,mdg.GLOBAL_OPERATIONAL_HIERARCHY_DESCRIPTION=m.GLOBAL_OPERATIONAL_HIERARCHY_DESCRIPTION
,mdg.CROSSPLANTMATSTATUSDESCR=m.CROSSPLANTMATSTATUSDESCR
,mdg.PARTDESCRIPTION=m.PARTDESCRIPTION
,mdg.GLOBAL_SUPPLY_PLANNER=m.GLOBAL_SUPPLY_PLANNER
,mdg.ZBW_HIER_DESCRIPTION_LVL10=m.ZBW_HIER_DESCRIPTION_LVL10
,mdg.ZBW_HIER_PARENT_LVL7=m.ZBW_HIER_PARENT_LVL7
,mdg.PPU_NEW=m.PPU_NEW
,mdg.PRP_PRICE_RESPONSIBLE_CODE=m.PRP_PRICE_RESPONSIBLE_CODE
,mdg.SUPPLYCHAINPLANINGPPI=m.SUPPLYCHAINPLANINGPPI
,mdg.ZBW_HIER_DESCRIPTION_LVL3=m.ZBW_HIER_DESCRIPTION_LVL3
,mdg.GENERALITEMCATGROUPDESCR=m.GENERALITEMCATGROUPDESCR
,mdg.PRP_PRICE_RESPONSIBLE_DESC=m.PRP_PRICE_RESPONSIBLE_DESC
,mdg.ZBW_HIER_PARENT_LVL4=m.ZBW_HIER_PARENT_LVL4
,mdg.ZBW_HIER_DESCRIPTION_LVL5=m.ZBW_HIER_DESCRIPTION_LVL5
,mdg.INDICATORSALESCATALOGUEDESC=m.INDICATORSALESCATALOGUEDESC
,mdg.BATCHMANREQIND=m.BATCHMANREQIND
,mdg.INTERNATIONALARTICLE=m.INTERNATIONALARTICLE
,mdg.ACT_CONV=m.ACT_CONV
,mdg.INTERNATIONALARTICLEDESC=m.INTERNATIONALARTICLEDESC
,mdg.ZBW_HIER_PARENT_LVL9=m.ZBW_HIER_PARENT_LVL9
,mdg.IMCCODE=m.IMCCODE
,mdg.APORELEVANCE=m.APORELEVANCE
,mdg.ZBW_HIER_PARENT_LVL10=m.ZBW_HIER_PARENT_LVL10
,mdg.APORELEVANCEDESC=m.APORELEVANCEDESC
,mdg.PRODUCT_HIERARCHY_PHOENIX_DESCRIPTION=m.PRODUCT_HIERARCHY_PHOENIX_DESCRIPTION
,mdg.ZBW_PRODUCTGROUP=m.ZBW_PRODUCTGROUP
,mdg.PRM_PRODUCT_MANAGER_DESC=m.PRM_PRODUCT_MANAGER_DESC
,mdg.PLANNED_PRODUCT_HIERARCHY_DESCRIPTION=m.PLANNED_PRODUCT_HIERARCHY_DESCRIPTION
,mdg.ZBW_HIER_PARENT_LVL1=m.ZBW_HIER_PARENT_LVL1
,mdg.ZBW_HIER_PARENT_LVL5=m.ZBW_HIER_PARENT_LVL5
,mdg.ZBW_ARTICLEGROUP=m.ZBW_ARTICLEGROUP
,mdg.ZBW_DIVISION=m.ZBW_DIVISION
,mdg.ZBW_HIER_DESCRIPTION_LVL7=m.ZBW_HIER_DESCRIPTION_LVL7
,mdg.ARTICLEGROUPDESC=m.ARTICLEGROUPDESC
,mdg.GLOBAL_DEMAND_PLANNER=m.GLOBAL_DEMAND_PLANNER
,mdg.GLOBAL_DEMAND_PLANNER_DESC=m.GLOBAL_DEMAND_PLANNER_DESC
,mdg.ZBW_HIER_DESCRIPTION_LVL8=m.ZBW_HIER_DESCRIPTION_LVL8
,mdg.ZBW_DESCRIPTION=m.ZBW_DESCRIPTION
,mdg.ZBW_HIER_PARENT_LVL2=m.ZBW_HIER_PARENT_LVL2
,mdg.INDICATORSALESCATALOGUE=m.INDICATORSALESCATALOGUE
,mdg.ZBW_BUSINESSFIELD=m.ZBW_BUSINESSFIELD
,mdg.APICODEDESCR=m.APICODEDESCR
,mdg.ZBW_VALUATIONCLASS=m.ZBW_VALUATIONCLASS
,mdg.DOSAGE=m.DOSAGE
,mdg.UNITOFDOSAGE=m.UNITOFDOSAGE
,mdg.GLOBALBRANDDESCR=m.GLOBALBRANDDESCR
,mdg.PLANNED_PRODUCT_HIERARCHY=m.PLANNED_PRODUCT_HIERARCHY
,mdg.CROSSPLANTMATSTATUS=m.CROSSPLANTMATSTATUS
,mdg.PRODUCT_HIERARCHY_PHOENIX=m.PRODUCT_HIERARCHY_PHOENIX
,mdg.NETWEIGHT=m.NETWEIGHT
,mdg.PRODUCTHIERARCHY=m.PRODUCTHIERARCHY
,mdg.UNITOFCOMPLETECONTENTOFTHEPACKAGE=m.UNITOFCOMPLETECONTENTOFTHEPACKAGE
 from dim_mdg_part mdg,update_mdg_new m
where mdg.MDGPartNumber_NoLeadZero =m.MDGPartNumber_NoLeadZero;


update dim_mdg_part mdg set
 mdg.PRIMARY_PRODUCTION_LOCATION_NAME=ifnull(m.PRIMARY_PRODUCTION_LOCATION_NAME,'Not Set')
,mdg.PRIMARY_PRODUCTION_LOCATION=ifnull(m.PRIMARY_PRODUCTION_LOCATION,'Not Set')
from dim_mdg_part mdg
left join update_mdg_new m on mdg.MDGPartNumber_NoLeadZero =m.MDGPartNumber_NoLeadZero
where mdg.PRIMARY_PRODUCTION_LOCATION<> ifnull(m.PRIMARY_PRODUCTION_LOCATION,'Not Set');


drop table if exists tmp_part;
create table tmp_part as
select distinct
partnumber,
Laboratory,
row_number() over (partition by PartNumber  order by '' ) rno
from dim_part
where partnumber <>  'Not Set' and Laboratory <>  'Not Set';

drop table if exists new_cluster_mapping_distinct;
create table new_cluster_mapping_distinct
as select distinct primary_manufacturing_site_code as primary_manufacturing_site,
primary_manufacturing_location_name as primary_manufacturing_location,laboratory_code
from EMDSialUSA490.cluster_mapping_20180208 cm;


update dim_mdg_part mdg
set mdg.primary_production_location=cm.primary_manufacturing_site
from dim_mdg_part mdg,new_cluster_mapping_distinct cm,tmp_part tmp
where mdg.partnumber=tmp.partnumber
and tmp.laboratory=cm.laboratory_code
and rno=1
and milliporeflag ='Not Set'
and  mdg.primary_production_location='Not Set'
and mdg.primary_production_location<>cm.primary_manufacturing_site;


update dim_mdg_part mdg
set mdg.primary_production_location_name=cm.primary_manufacturing_location
from dim_mdg_part mdg,new_cluster_mapping_distinct cm,tmp_part tmp
where mdg.partnumber=tmp.partnumber
and tmp.laboratory=cm.laboratory_code
and rno=1
and milliporeflag ='Not Set'
and  mdg.primary_production_location_name='Not Set'
and mdg.primary_production_location_name<>cm.primary_manufacturing_location;

/*
drop table if exists tmp_part
create table tmp_part as
select distinct
MDGM_MATERIAL_NO  as partnumber,
Laboratory,
row_number() over (partition by MDGM_MATERIAL_NO  order by '' ) rno
from dim_part
where MDGM_MATERIAL_NO <> 'Not Set' and Laboratory<> 'Not Set'
*/



/* Expression test  */

update dim_mdg_part
set PRODUCT_HIERARCHY_DESCRIPTION2 = decode(producthierarchy,'Not Set',producthierarchy,left(producthierarchy,3))
where PRODUCT_HIERARCHY_DESCRIPTION2 <> decode(producthierarchy,'Not Set',producthierarchy,left(producthierarchy,3));

update dim_mdg_part
set PrimaryProductionLocation = primary_production_location || ' - ' || primary_production_location_name
where PrimaryProductionLocation <> primary_production_location || ' - ' || primary_production_location_name;

/* 7 august 2017 - Cornelia add new attribute MaterialCategory */
update dim_mdg_part
set materialcategory ='Finished goods'
where mattypeforglbrep in (
'MER',
'FERT',
'FIN',
'ZCSM',
'ZFRT',
'ZVFT',
'ZVAR',
'KMAT',
'LEER',
'NLAG',
'UNBW',
'ZLIT',
'ZRCH',
'ZCTO',
'Not Set'
);


update dim_mdg_part
set materialcategory ='Raw Material'
where mattypeforglbrep in (
'DIEN',
'DUM',
'HIBE',
'RAW',
'ROH',
'VERP',
'ZUNB',
'ZVRP',
'PROC',
'ZHLB',
'ZIMU',
'ABF'
);

update dim_mdg_part
set materialcategory ='Unfinished Goods'
where mattypeforglbrep in ('HALB','UFG');

/* disabled since we are using for the MDGM _Material_no logic from above - Roxana D 2018-02-12 
UPDATE dim_mdg_part dp
SET  dp.MDGPartNumber_NoLeadZero = ifnull(case when length(dp.partnumber) = 18 and dp.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from dp.partnumber) else dp.partnumber end,'Not Set')
*/

/* 29 Mar 2018 CristianT Start: Added new fields for APP-8732 */
DROP TABLE IF EXISTS tmp_emd586_mdg_part_newfields;
CREATE TABLE tmp_emd586_mdg_part_newfields
AS
SELECT DISTINCT mdgpartnumber_noleadzero,
       division,
       familyandstrength,
       packagingstyle,
       source_subsidiary,
       businessunit_sdv,
       memphisbusinessline,
       gblrevenuerecognitionclass,
       calculationrulesforsled,
       contentprimpackmat,
       stackingpack,
       tacticalplanningrelevance,
       supplychainplanningcat,
       containertypeofprimarypack,
       purpose,
       rims_productdosageform,
       localcountrybrand
FROM emd586.dim_mdg_part
WHERE projectsourceid = '1';

UPDATE dim_mdg_part d
SET division = mdg.division,
    familyandstrength = mdg.familyandstrength,
    packagingstyle = mdg.packagingstyle,
    source_subsidiary = mdg.source_subsidiary,
    businessunit_sdv = mdg.businessunit_sdv,
    memphisbusinessline = mdg.memphisbusinessline,
    gblrevenuerecognitionclass = mdg.gblrevenuerecognitionclass,
    calculationrulesforsled = mdg.calculationrulesforsled,
    contentprimpackmat = mdg.contentprimpackmat,
    stackingpack = mdg.stackingpack,
    tacticalplanningrelevance = mdg.tacticalplanningrelevance,
    supplychainplanningcat = mdg.supplychainplanningcat,
    containertypeofprimarypack = mdg.containertypeofprimarypack,
    purpose = mdg.purpose,
    rims_productdosageform = mdg.rims_productdosageform,
    localcountrybrand = mdg.localcountrybrand
FROM dim_mdg_part d,
     tmp_emd586_mdg_part_newfields mdg
WHERE d.mdgpartnumber_noleadzero = mdg.mdgpartnumber_noleadzero;

DROP TABLE IF EXISTS tmp_emd586_mdg_part_newfields;

/* 29 Mar 2018 CristianT End */

/* Ana Rusu - APP-9511 - Temperature and storage conditions - Start*/

UPDATE dim_mdg_part dmp
   SET dmp.storageconditions = ifnull(mdgm.RAUBE,'Not Set'),
	   	dmp.dw_update_date = current_timestamp
 from mdg_mara mdgm, dim_mdg_part dmp
 WHERE     dmp.PartNumber = mdgm.MATNR
   AND dmp.storageconditions <> ifnull(mdgm.RAUBE,'Not Set');


UPDATE dim_mdg_part dmp
   SET dmp.temperatureconditions = ifnull(mdgm.TEMPB,'Not Set'),
	   	dmp.dw_update_date = current_timestamp
 from mdg_mara mdgm, dim_mdg_part dmp
 WHERE     dmp.PartNumber = mdgm.MATNR
   AND dmp.temperatureconditions <> ifnull(mdgm.TEMPB,'Not Set');

/* Ana Rusu - APP-9511 - Temperature and storage conditions - End*/

/* Alex - Update Falcon Materials */
update dim_mdg_part m
set m.GblMatlFalcon = 'X'
from  dim_mdg_part m, EMD586.falcon_parts f
where m.partnumber = f.global_material_no; 