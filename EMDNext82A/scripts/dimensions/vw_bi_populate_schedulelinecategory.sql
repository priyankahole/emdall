UPDATE    dim_schedulelinecategory slc
   SET slc.Description = TVEPT_VTEXT,
			slc.dw_update_date = current_timestamp
   FROM
          dim_schedulelinecategory slc, TVEPT t
 WHERE slc.RowIsCurrent = 1 AND t.TVEPT_ETTYP = slc.ScheduleLineCategory
;

INSERT INTO dim_schedulelinecategory(dim_schedulelinecategoryId, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_schedulelinecategory
               WHERE dim_schedulelinecategoryId = 1);

delete from number_fountain m where m.table_name = 'dim_schedulelinecategory';

insert into number_fountain
select 	'dim_schedulelinecategory',
	ifnull(max(d.Dim_ScheduleLineCategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_schedulelinecategory d
where d.Dim_ScheduleLineCategoryid <> 1; 	

INSERT INTO dim_schedulelinecategory(Dim_ScheduleLineCategoryid,
	ScheduleLineCategory,
	Description,
 	RowStartDate,
	RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_schedulelinecategory') 
          + row_number() over(order by ''),
			  ifnull(TVEPT_ETTYP, 'Not Set'),
            TVEPT_VTEXT,
            current_timestamp,
            1
       FROM TVEPT t
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_schedulelinecategory slc
                    WHERE TVEPT_ETTYP = slc.ScheduleLineCategory)
   ORDER BY 2;

