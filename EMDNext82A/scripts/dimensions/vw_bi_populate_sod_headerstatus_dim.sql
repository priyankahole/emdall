/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   5 Feb 2015      Liviu Ionescu              Add Combine for dim_salesorderheaderstatus */
/******************************************************************************************************************/

drop table if exists tmp_del_LIKP_LIPS_VBUK;
create table tmp_del_LIKP_LIPS_VBUK
as
select distinct LIKP_VBELN from LIKP_LIPS
union
select distinct dd_SalesDlvrDocNo from fact_salesorderdelivery;

delete from LIKP_LIPS_VBUK
  where not exists (select 1 from tmp_del_LIKP_LIPS_VBUK where VBUK_VBELN = LIKP_VBELN);

merge into dim_salesorderheaderstatus dest
using(select dim_salesorderheaderstatusid, 
			ifnull(t1.DD07T_DDTEXT, 'Not Set') RefStatusAllItems,
			ifnull(t2.DD07T_DDTEXT, 'Not Set') ConfirmationStatus,
			ifnull(t3.DD07T_DDTEXT, 'Not Set') DeliveryStatus,
			ifnull(t4.DD07T_DDTEXT, 'Not Set') OverallDlvrStatusOfItem,
			ifnull(t5.DD07T_DDTEXT, 'Not Set') BillingStatus,
			ifnull(t6.DD07T_DDTEXT, 'Not Set') BillingStatusOfOrdRelBillDoc,
			ifnull(t7.DD07T_DDTEXT, 'Not Set') RejectionStatus,
			ifnull(t8.DD07T_DDTEXT, 'Not Set') OverallProcessStatusItem,
			ifnull(t9.DD07T_DDTEXT, 'Not Set') TotalIncompleteStatusAllItems,
			ifnull(t10.DD07T_DDTEXT, 'Not Set') GeneralIncompleteStatusItem,
			ifnull(t11.DD07T_DDTEXT, 'Not Set') OverallPickingStatus,
			ifnull(t12.DD07T_DDTEXT, 'Not Set') HeaderIncompletionStautusConcernDlvry
	  from dim_salesorderheaderstatus dim
		inner join LIKP_LIPS_VBUK ds on dim.SalesDocumentNumber = ds.VBUK_VBELN
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t1  on  t1.DD07T_DOMVALUE = ds.VBUK_RFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t2  on  t2.DD07T_DOMVALUE = ds.VBUK_BESTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t3  on  t3.DD07T_DOMVALUE = ds.VBUK_LFSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t4  on  t4.DD07T_DOMVALUE = ds.VBUK_LFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t5  on  t5.DD07T_DOMVALUE = ds.VBUK_FKSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t6  on  t6.DD07T_DOMVALUE = ds.VBUK_FKSAK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t7  on  t7.DD07T_DOMVALUE = ds.VBUK_ABSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t8  on  t8.DD07T_DOMVALUE = ds.VBUK_GBSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t9  on  t9.DD07T_DOMVALUE = ds.VBUK_UVALS
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t10 on t10.DD07T_DOMVALUE = ds.VBUK_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t11 on t11.DD07T_DOMVALUE = ds.VBUK_KOSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t12 on t12.DD07T_DOMVALUE = ds.VBUK_UVVLK) src
on dest.dim_salesorderheaderstatusid = src.dim_salesorderheaderstatusid
when matched then update set dest.dw_update_date = current_timestamp,
							 dest.RefStatusAllItems = src.RefStatusAllItems,
							 dest.ConfirmationStatus = src.ConfirmationStatus,
							 dest.DeliveryStatus = src.DeliveryStatus,
							 dest.OverallDlvrStatusOfItem = src.OverallDlvrStatusOfItem,
							 dest.BillingStatus = src.BillingStatus,
							 dest.BillingStatusOfOrdRelBillDoc = src.BillingStatusOfOrdRelBillDoc,
							 dest.RejectionStatus = src.RejectionStatus,
							 dest.OverallProcessStatusItem = src.OverallProcessStatusItem,
							 dest.TotalIncompleteStatusAllItems = src.TotalIncompleteStatusAllItems,
							 dest.GeneralIncompleteStatusItem = src.GeneralIncompleteStatusItem,
							 dest.OverallPickingStatus = src.OverallPickingStatus,
							 dest.HeaderIncompletionStautusConcernDlvry = src.HeaderIncompletionStautusConcernDlvry;

delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 

  INSERT
    INTO dim_salesorderheaderstatus(
	  dim_salesorderheaderstatusid,
          SalesDocumentNumber,
          RefStatusAllItems,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDlvrStatusOfItem,
          BillingStatus,
          BillingStatusOfOrdRelBillDoc,
          RejectionStatus,
          OverallProcessStatusItem,
          TotalIncompleteStatusAllItems,
          GeneralIncompleteStatusItem,
          OverallPickingStatus,
	  HeaderIncompletionStautusConcernDlvry
          )
 SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by dtbl1.VBUK_VBELN)),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull(t1.DD07T_DDTEXT, 'Not Set') RefStatusAllItems,
		ifnull(t2.DD07T_DDTEXT, 'Not Set') ConfirmationStatus,
		ifnull(t3.DD07T_DDTEXT, 'Not Set') DeliveryStatus,
		ifnull(t4.DD07T_DDTEXT, 'Not Set') OverallDlvrStatusOfItem,
		ifnull(t5.DD07T_DDTEXT, 'Not Set') BillingStatus,
		ifnull(t6.DD07T_DDTEXT, 'Not Set') BillingStatusOfOrdRelBillDoc,
		ifnull(t7.DD07T_DDTEXT, 'Not Set') RejectionStatus,
		ifnull(t8.DD07T_DDTEXT, 'Not Set') OverallProcessStatusItem,
		ifnull(t9.DD07T_DDTEXT, 'Not Set') TotalIncompleteStatusAllItems,
		ifnull(t10.DD07T_DDTEXT, 'Not Set') GeneralIncompleteStatusItem,
		ifnull(t11.DD07T_DDTEXT, 'Not Set') OverallPickingStatus,
		ifnull(t12.DD07T_DDTEXT, 'Not Set') HeaderIncompletionStautusConcernDlvry
FROM LIKP_LIPS_VBUK ds
		inner join LIKP_LIPS t0 on t0.LIKP_VBELN = ds.VBUK_VBELN
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t1  on  t1.DD07T_DOMVALUE = ds.VBUK_RFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t2  on  t2.DD07T_DOMVALUE = ds.VBUK_BESTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t3  on  t3.DD07T_DOMVALUE = ds.VBUK_LFSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t4  on  t4.DD07T_DOMVALUE = ds.VBUK_LFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t5  on  t5.DD07T_DOMVALUE = ds.VBUK_FKSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t6  on  t6.DD07T_DOMVALUE = ds.VBUK_FKSAK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t7  on  t7.DD07T_DOMVALUE = ds.VBUK_ABSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t8  on  t8.DD07T_DOMVALUE = ds.VBUK_GBSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t9  on  t9.DD07T_DOMVALUE = ds.VBUK_UVALS
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t10 on t10.DD07T_DOMVALUE = ds.VBUK_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t11 on t11.DD07T_DOMVALUE = ds.VBUK_KOSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t12 on t12.DD07T_DOMVALUE = ds.VBUK_UVVLK
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = ds.VBUK_VBELN)) dtbl1;

drop table if exists tmp_del_LIKP_LIPS_VBUK;
