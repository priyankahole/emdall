INSERT INTO dim_salesdistrict(Dim_SalesDistrictid,
                             SalesDistrict,
                             Description,
                             RowStartDate,
                             RowEndDate,
                             RowIsCurrent,
                             RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          current_timestamp,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesdistrict
               WHERE Dim_SalesDistrictid = 1);

UPDATE dim_salesdistrict sd
   SET sd.Description = ifnull(t.T171T_BZTXT, 'Not Set'),
			sd.dw_update_date = current_timestamp
  FROM dim_salesdistrict sd, T171T t
 WHERE sd.SalesDistrict = t.T171T_BZIRK 
 AND sd.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_salesdistrict';

insert into number_fountain
select 	'dim_salesdistrict',
	ifnull(max(d.Dim_SalesDistrictid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesdistrict d
where d.Dim_SalesDistrictid <> 1;  

INSERT INTO dim_salesdistrict(Dim_SalesDistrictid,
                              SalesDistrict,
                              Description,
                              RowStartDate,
                              RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesdistrict') 
          + row_number() over(order by ''),
			  t.T171T_BZIRK,
          ifnull(t.T171T_BZTXT, 'Not Set'),
          current_timestamp,
          1
     FROM t171t t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesdistrict sd
               WHERE sd.SalesDistrict = t.T171T_BZIRK);