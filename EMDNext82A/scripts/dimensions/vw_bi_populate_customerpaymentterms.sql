INSERT INTO dim_customerpaymentterms
(Dim_CustomerPaymentTermsid, PaymentTermCode, PaymentTermName, RowStartDate, RowEndDate, RowIsCurrent, RowChangeReason)
SELECT 1, 'Not Set', 'Not Set', current_timestamp, NULL, 1, NULL
FROM (SELECT 1) D
WHERE NOT EXISTS

             (SELECT 1

              FROM dim_customerpaymentterms

              WHERE Dim_CustomerPaymentTermsid = 1);

	
UPDATE dim_customerpaymentterms cpt
SET cpt.PaymentTermName = ifnull(TVZBT_VTEXT, 'Not Set'),

			cpt.dw_update_date = current_timestamp

       FROM

          TVZBT t   ,    dim_customerpaymentterms cpt

	WHERE t.TVZBT_ZTERM = cpt.PaymentTermCode   ;


delete from number_fountain m where m.table_name = 'dim_customerpaymentterms';

insert into number_fountain

select 	'dim_customerpaymentterms',

	ifnull(max(d.Dim_CustomerPaymentTermsid), 

	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))

from dim_customerpaymentterms d

where d.Dim_CustomerPaymentTermsid <> 1;



INSERT INTO dim_customerpaymentterms

(Dim_CustomerPaymentTermsid, PaymentTermCode, PaymentTermName, RowStartDate, RowIsCurrent)

SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customerpaymentterms') 

          + row_number() over(order by '') ,

			TVZBT_ZTERM,

          ifnull(TVZBT_VTEXT, 'Not Set'),

          current_timestamp,

          1

FROM TVZBT t

WHERE NOT EXISTS

                 (SELECT 1

                  FROM dim_customerpaymentterms pt

                  WHERE pt.PaymentTermCode = TVZBT_ZTERM AND pt.PaymentTermName = ifnull(TVZBT_VTEXT, 'Not Set')) ;



delete from number_fountain m where m.table_name = 'dim_customerpaymentterms';

								