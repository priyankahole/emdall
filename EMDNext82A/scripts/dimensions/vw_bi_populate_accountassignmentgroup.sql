
INSERT INTO dim_accountassignmentgroup(dim_accountassignmentgroupid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_accountassignmentgroup
               WHERE dim_accountassignmentgroupid = 1);


UPDATE dim_accountassignmentgroup aag 
SET aag.Description = ifnull(t.TVKTT_VTEXT, 'Not Set'),
	aag.dw_update_date = current_timestamp
	from dim_accountassignmentgroup aag, TVKTT t
WHERE aag.AccountAssignmentGroup = t.TVKTT_KTGRD 
AND aag.RowIsCurrent = 1;
			   
delete from number_fountain m where m.table_name = 'dim_accountassignmentgroup';

insert into number_fountain
select 	'dim_accountassignmentgroup',
	ifnull(max(d.dim_accountassignmentgroupid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_accountassignmentgroup d
where d.dim_accountassignmentgroupid <> 1;
			   
INSERT INTO dim_accountassignmentgroup(Dim_AccountAssignmentGroupid,
                                       AccountAssignmentGroup,
                                       Description,
                                       RowStartDate,
                                       RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_accountassignmentgroup')
          + row_number() over(order by '') ,
          TVKTT_KTGRD,
          ifnull(TVKTT_VTEXT, 'Not Set'),
          CURRENT_TIMESTAMP,
          1
     FROM TVKTT t
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM dim_accountassignmentgroup aag
                   WHERE aag.AccountAssignmentGroup = t.TVKTT_KTGRD);

delete from number_fountain m where m.table_name = 'dim_accountassignmentgroup';