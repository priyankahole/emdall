
INSERT INTO dim_codetext(dim_codetextId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_codetext
               WHERE dim_codetextId = 1);

UPDATE 	dim_codeText d
SET 	d.Description = s.QPCT_KURZTEXT,
			d.dw_update_date = current_timestamp
	from dim_codeText d, QPCT s
WHERE 	d."catalog" = s.QPCT_KATALOGART
		AND d.CodeGroup = s.QPCT_CODEGRUPPE
		AND d.Code = s.QPCT_CODE
		AND 'Version' = s.QPCT_VERSION
		AND d.RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_codetext';

insert into number_fountain
select 	'dim_codetext',
	ifnull(max(d.dim_codetextId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_codetext d
where d.dim_codetextId <> 1;

INSERT INTO dim_codetext(Dim_codetextid,
				"catalog",
                                  CodeGroup,
                                  Code,
                                  Version,
                                  Description,
                                  RowStartDate,
                                  RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_codetext') 
          + row_number() over(order by ''),
		  QPCT_KATALOGART,
          QPCT_CODEGRUPPE,
          QPCT_CODE,
          QPCT_VERSION,
          QPCT_KURZTEXT,
          current_timestamp,
          1
     FROM QPCT
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM dim_codetext
                   WHERE     "catalog" = QPCT_KATALOGART
                         AND CodeGroup = QPCT_CODEGRUPPE
                         AND Code = QPCT_CODE
                         AND Version = QPCT_VERSION
			 AND RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_codetext';
			 