INSERT INTO dim_salesorderitemcategory(Dim_SalesOrderItemCategoryid,
							SalesOrderItemCategory,
							Description,
							ScheduleLinesAllowed,
                             RowStartDate,
                             RowEndDate,
                             RowIsCurrent,
                             RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          'Not Set',		  
          current_timestamp,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorderitemcategory
               WHERE dim_salesorderitemcategoryid = 1);

UPDATE    Dim_SalesOrderItemCategory ic
   SET ic.Description = TVAPT_VTEXT, 
       ic.ScheduleLinesAllowed = ifnull(TVAP_ETERL,'Not Set'),
	   ic.dw_update_date = current_timestamp
        FROM  
		  Dim_SalesOrderItemCategory ic, TVAPT t
	WHERE t.TVAPT_PSTYV = ic.SalesOrderItemCategory  ;

/* Liviu Ionescu 2018-07-10 - Basecamp 335232254 */
UPDATE    Dim_SalesOrderItemCategory ic
   SET ic.ReturnsItem = ifnull(TVAPT_FKREL, 'Not Set'), 
       ic.Carryoutpricing = ifnull(TVAPT_PRSFD, 'Not Set'),
	   ic.RelevantforBilling = ifnull(TVAPT_SHKZG, 'Not Set')
        FROM  
		  Dim_SalesOrderItemCategory ic, TVAPT t
	WHERE t.TVAPT_PSTYV = ic.SalesOrderItemCategory  ;
/* End Liviu Ionescu */

delete from number_fountain m where m.table_name = 'dim_salesorderitemcategory';

insert into number_fountain
select 	'dim_salesorderitemcategory',
	ifnull(max(d.Dim_SalesOrderItemCategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemcategory d
where d.Dim_SalesOrderItemCategoryid <> 1; 
	
INSERT INTO dim_salesorderitemcategory(
	Dim_SalesOrderItemCategoryid,
	SalesOrderItemCategory,
	Description,
        ScheduleLinesAllowed,
/* Liviu Ionescu 2018-07-10 - Basecamp 335232254 */
	ReturnsItem,
	Carryoutpricing,
	RelevantforBilling,
 	RowStartDate,
	RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemcategory') 
          + row_number() over(order by ''),
			  ifnull(TVAPT_PSTYV, 'Not Set'),
            TVAPT_VTEXT,
	    ifnull(TVAP_ETERL,'Not Set'),
/* Liviu Ionescu 2018-07-10 - Basecamp 335232254 */
	    ifnull(TVAPT_FKREL, 'Not Set') ReturnsItem,
	    ifnull(TVAPT_PRSFD, 'Not Set') Carryoutpricing,
	    ifnull(TVAPT_SHKZG, 'Not Set') RelevantforBilling,
            current_timestamp,
            1
       FROM TVAPT t
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_salesorderitemcategory ic
                    WHERE TVAPT_PSTYV = ic.SalesOrderItemCategory)
   ORDER BY 2;
