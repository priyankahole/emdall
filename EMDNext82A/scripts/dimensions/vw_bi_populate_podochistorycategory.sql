
INSERT INTO dim_podochistorycategory
(dim_podochistorycategoryid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_podochistorycategory where dim_podochistorycategoryid = 1);


UPDATE    
dim_podochistorycategory poh
SET poh.longtext = ifnull(t.T163C_BEWTL, 'Not Set'),
	poh.shortdescription = ifnull(t.T163C_BEWTK, 'Not Set'),
	poh.dw_update_date = current_timestamp
FROM
dim_podochistorycategory poh,
t163c t
 WHERE poh.RowIsCurrent = 1
 AND t.T163C_BEWTP = poh.podochistorycategory;
 
delete from number_fountain m where m.table_name = 'dim_podochistorycategory';

insert into number_fountain				   
select 	'dim_podochistorycategory',
	ifnull(max(d.dim_podochistorycategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_podochistorycategory d
where d.dim_podochistorycategoryid <> 1;

INSERT INTO dim_podochistorycategory(dim_podochistorycategoryid,
								longtext,
								shortdescription,
								podochistorycategory,
								RowStartDate,
								RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_podochistorycategory') 
          + row_number() over(order by ''),
		 ifnull(t.T163C_BEWTL, 'Not Set'),
		 ifnull(t.T163C_BEWTK, 'Not Set'),
		 ifnull(t.T163C_BEWTP, 'Not Set'),
        current_timestamp,
         1
     FROM t163c t
    WHERE t.T163C_BEWTP IS NOT NULL
		  AND NOT EXISTS
                 (SELECT 1
                    FROM dim_podochistorycategory pht
                   WHERE pht.podochistorycategory = T163C_BEWTP);
