
INSERT INTO dim_customergroup2(dim_customergroup2id, description)
SELECT 1, 'Not Set'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customergroup2
               WHERE dim_customergroup2id = 1);

UPDATE    dim_customergroup2 cg2
   SET cg2.Description = ifnull(TVV2T_BEZEI, 'Not Set'),
			cg2.dw_update_date = current_timestamp
       FROM dim_customergroup2 cg2,
          TVV2T t
   WHERE t.TVV2T_KVGR2 = cg2.CustomerGroup;

delete from number_fountain m where m.table_name = 'dim_customergroup2';

insert into number_fountain
select 	'dim_customergroup2',
	ifnull(max(d.dim_customergroup2id), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customergroup2 d
where d.dim_customergroup2id <> 1;

INSERT
  INTO dim_customergroup2(Dim_CustomerGroup2id,
                                                        LanguageKey,
                          CustomerGroup,
                          Description)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customergroup2')
          + row_number() over(order by ''),
                        TVV2T_SPRAS,
        TVV2T_KVGR2,
        ifnull(TVV2T_BEZEI,'Not Set')
FROM TVV2T
WHERE not exists (select 1 from dim_customergroup2 a where a.CustomerGroup = TVV2T_KVGR2);

delete from number_fountain m where m.table_name = 'dim_customergroup2';		