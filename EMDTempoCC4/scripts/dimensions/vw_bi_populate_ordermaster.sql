
insert into PM_AUFK
(
	AUFK_AUFNR,
	AUFK_AUART,
	AUFK_AUTYP,
	AUFK_REFNR,
	AUFK_ERNAM,
	AUFK_ERDAT,
	AUFK_AENAM,
	AUFK_AEDAT,
	AUFK_KTEXT,
	AUFK_LTEXT,
	AUFK_BUKRS,
	AUFK_WERKS,
	AUFK_GSBER,
	AUFK_KOKRS,
	AUFK_CCKEY,
	AUFK_KOSTV,
	AUFK_STORT,
	AUFK_SOWRK,
	AUFK_ASTKZ,
	AUFK_WAERS,
	AUFK_ASTNR,
	AUFK_STDAT,
	AUFK_ESTNR,
	AUFK_PHAS0,
	AUFK_PHAS1,
	AUFK_PHAS2,
	AUFK_PHAS3,
	AUFK_PDAT1,
	AUFK_PDAT2,
	AUFK_PDAT3,
	AUFK_IDAT1,
	AUFK_IDAT2,
	AUFK_IDAT3,
	AUFK_OBJID,
	AUFK_VOGRP,
	AUFK_OBJNR,
	AUFK_KOSTL,
	AUFK_KVEWE,
	AUFK_KAPPL,
	AUFK_KALSM,
	AUFK_SCOPE,
	AUFK_KDAUF,
	AUFK_KDPOS,
	AUFK_RSORD,
	AUFK_BEMOT,
	AUFK_VAPLZ,
	AUFK_PRCTR
)
select distinct
	AUFK_AUFNR,
	AUFK_AUART,
	AUFK_AUTYP,
	AUFK_REFNR,
	AUFK_ERNAM,
	AUFK_ERDAT,
	AUFK_AENAM,
	AUFK_AEDAT,
	AUFK_KTEXT,
	AUFK_LTEXT,
	AUFK_BUKRS,
	AUFK_WERKS,
	AUFK_GSBER,
	AUFK_KOKRS,
	AUFK_CCKEY,
	AUFK_KOSTV,
	AUFK_STORT,
	AUFK_SOWRK,
	AUFK_ASTKZ,
	AUFK_WAERS,
	AUFK_ASTNR,
	AUFK_STDAT,
	AUFK_ESTNR,
	AUFK_PHAS0,
	AUFK_PHAS1,
	AUFK_PHAS2,
	AUFK_PHAS3,
	AUFK_PDAT1,
	AUFK_PDAT2,
	AUFK_PDAT3,
	AUFK_IDAT1,
	AUFK_IDAT2,
	AUFK_IDAT3,
	AUFK_OBJID,
	AUFK_VOGRP,
	AUFK_OBJNR,
	AUFK_KOSTL,
	AUFK_KVEWE,
	AUFK_KAPPL,
	AUFK_KALSM,
	AUFK_SCOPE,
	AUFK_KDAUF,
	AUFK_KDPOS,
	AUFK_RSORD,
	AUFK_BEMOT,
	AUFK_VAPLZ,
	AUFK_PRCTR
from PM_AUFK2 a2
where not exists
	(select 1 from PM_AUFK a
		where a.AUFK_AUFNR = a2.AUFK_AUFNR);
/* END BI-4666 */

insert into dim_ordermaster(
		dim_ordermasterid,
		"order",
		Order_Type,
		Order_category,
		Reference_order,
		Entered_by,
		Created_on,
		Last_changed_by,
		change_date,
		Description,
		Long_text_exists,
		Company_Code,
		Plant,
		Business_Area,
		Controlling_Area,
		Cost_collector_key,
		Responsible_CCtr,
		Location_plant,
		Statistical,
		Currency,
		Order_status,
		Status_change,
		Reached_status,
		"created",
		Released,
		Completed,
		Closed,
		Planned_release,
		Planned_completion,
		Planned_closing_date,
		Release_date,
		Technical_completion,
		"close",
		"object_id",
		Disallowed_trans_grp,
		TimeCreated,
		ObjectNumber,
		CostCenter,
		ProfitCenter,
		RowStartDate,
		RowEndDate,
		RowIsCurrent,
		RowChangeReason) 
SELECT
		1,
		'Not Set',
		'Not Set',
		0,
		'Not Set',
		'Not Set',
		'1970-01-01',
		'Not Set',
		'1970-01-01',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		0,
		'1970-01-01',
		0,
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'1970-01-01',
		'1970-01-01',
		'1970-01-01',
		'1970-01-01',
		'1970-01-01',
		'1970-01-01',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		current_timestamp,
		'2099-01-01 00:00:00',
		1,
		'Not Set'
FROM (SELECT 1) as a
where not exists (select 1 from dim_ordermaster where dim_ordermasterid = 1);


delete from number_fountain m where m.table_name = 'dim_ordermaster';

insert into number_fountain
select 	'dim_ordermaster',
	ifnull(max(d.dim_ordermasterid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ordermaster d
where d.dim_ordermasterid <> 1; 
 
INSERT INTO dim_ordermaster(
	dim_ordermasterid,
	"order",
	Order_Type,
	Order_category,
	Reference_order,
	Entered_by,
	Created_on,
	Last_changed_by,
	change_date,
	Description,
	Long_text_exists,
	Company_Code,
	Plant,
	Business_Area,
	Controlling_Area,
	Cost_collector_key,
	Responsible_CCtr,
	Location_plant,
	Statistical,
	Currency,
	Order_status,
	Status_change,
	Reached_status,
	"created",
	Released,
	Completed,
	Closed,
	Planned_release,
	Planned_completion,
	Planned_closing_date,
	Release_date,
	Technical_completion,
	"close",
	"object_id",
	Disallowed_trans_grp,
	TimeCreated,
	ObjectNumber,
	CostCenter,
	ProfitCenter,
	RowStartDate,
	RowIsCurrent
)
     SELECT (
		select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_ordermaster') + row_number() over( order by '' ),
		IFNULL(AUFK_AUFNR,'Not Set'),
		IFNULL(AUFK_AUART,'Not Set'),
		IFNULL(AUFK_AUTYP,0),
		IFNULL(AUFK_REFNR,'Not Set'),
		IFNULL(AUFK_ERNAM,'Not Set'),
		IFNULL(AUFK_ERDAT,'1970-01-01'),
		IFNULL(AUFK_AENAM,'Not Set'),
		IFNULL(AUFK_AEDAT,'1970-01-01'),
		IFNULL(AUFK_KTEXT,'Not Set'),
		IFNULL(AUFK_LTEXT,'Not Set'),
		IFNULL(AUFK_BUKRS,'Not Set'),
		IFNULL(AUFK_WERKS,'Not Set'),
		IFNULL(AUFK_GSBER,'Not Set'),
		IFNULL(AUFK_KOKRS,'Not Set'),
		IFNULL(AUFK_CCKEY,'Not Set'),
		IFNULL(AUFK_KOSTV,'Not Set'),
		IFNULL(AUFK_SOWRK,'Not Set'),
		IFNULL(AUFK_ASTKZ,'Not Set'),
		IFNULL(AUFK_WAERS,'Not Set'),
		IFNULL(AUFK_ASTNR,0),
		IFNULL(AUFK_STDAT,'1970-01-01'),
		IFNULL(AUFK_ESTNR,0),
		IFNULL(AUFK_PHAS0,'Not Set'),
		IFNULL(AUFK_PHAS1,'Not Set'),
		IFNULL(AUFK_PHAS2,'Not Set'),
		IFNULL(AUFK_PHAS3,'Not Set'),
		IFNULL(AUFK_PDAT1,'1970-01-01'),
		IFNULL(AUFK_PDAT2,'1970-01-01'),
		IFNULL(AUFK_PDAT3,'1970-01-01'),
		IFNULL(AUFK_IDAT1,'1970-01-01'),
		IFNULL(AUFK_IDAT2,'1970-01-01'),
		IFNULL(AUFK_IDAT3,'1970-01-01'),
		IFNULL(AUFK_OBJID,'Not Set'),
		IFNULL(AUFK_VOGRP,'Not Set'),
		IFNULL(AUFK_ERFZEIT,'Not Set'),
		IFNULL(AUFK_OBJNR,'Not Set'),
		IFNULL(AUFK_KOSTL,'Not Set'),
		IFNULL(AUFK_PRCTR,'Not Set'),
		current_timestamp,
		1
		FROM PM_AUFK
		WHERE NOT EXISTS
		(SELECT 1
		FROM dim_ordermaster
WHERE "order" = AUFK_AUFNR);

/* AUFK-VAPLZ*/
Update dim_ordermaster d
set d.workcenter = ifnull(AUFK_VAPLZ,'Not Set'),dw_update_date = current_timestamp
from PM_AUFK a , dim_ordermaster d
where d."order" = AUFK_AUFNR
and d.workcenter <> ifnull(AUFK_VAPLZ,'Not Set');

/* 03May2016 - Planning Indicator */
update dim_ordermaster d
set planningindicator = ifnull(AFIH_PLKNZ,'Not Set') ,dw_update_date = current_timestamp
from PM_afih , dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and planningindicator <> ifnull(AFIH_PLKNZ,'Not Set');

/*04May2016 - System Condition */
update dim_ordermaster d
set systemcondition = ifnull(AFIH_ANLZU,'Not Set'),dw_update_date = current_timestamp
from PM_afih , dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and systemcondition <> ifnull(AFIH_ANLZU,'Not Set');

/*06May2016 - Work Center Desc. Work CEnter Dim key is different. */
update dim_ordermaster o
set o.workcenterdesc =  ifnull(w.ktext_description,'Not Set'), dw_update_date = current_timestamp
from dim_workcenter w, dim_ordermaster o
where o.workcenter = w.workcenter 
and w.plant = o.plant
and w.costcenteractivitytype <> 'Not Set'
and o.workcenterdesc <>  ifnull(w.ktext_description,'Not Set');

update dim_ordermaster d
set orderpriority = ifnull(AFIH_PRIOK,'Not Set') ,dw_update_date = current_timestamp
from PM_afih , dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and orderpriority <> ifnull(AFIH_PRIOK,'Not Set');

update dim_ordermaster d
set orderrevision = ifnull(AFIH_REVNR,'Not Set') ,dw_update_date = current_timestamp
from PM_afih , dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and orderrevision <> ifnull(AFIH_REVNR,'Not Set');

/* add ReferenceDate BI-3471 Mirela */

update dim_ordermaster d
set ReferenceDate = ifnull(AFIH_ADDAT,'1970-01-01') ,dw_update_date = current_timestamp
from PM_afih , dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and ReferenceDate <> ifnull(AFIH_ADDAT,'1970-01-01');

update dim_ordermaster d
set d.Technical_completion = IFNULL(AUFK_IDAT2,'1970-01-01'), dw_update_date = current_timestamp
from PM_aufk a, dim_ordermaster d
where d."order" = AUFK_AUFNR
and d.Technical_completion <> IFNULL(AUFK_IDAT2,'1970-01-01');

delete from number_fountain m where m.table_name = 'dim_ordermaster';