insert into dim_availabilityconfirmation(dim_availabilityconfirmationid,AvailabilityConfirmationCode,Description,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason)
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from dim_availabilityconfirmation where dim_availabilityconfirmationid = 1);
               
UPDATE    dim_availabilityconfirmation ac
       
   SET ac.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
		ac.dw_update_date = current_timestamp
		FROM
          DD07T dt, dim_availabilityconfirmation ac
 WHERE ac.AvailabilityConfirmationCode = dt.DD07T_DOMVALUE
 AND dt.DD07T_DOMNAME = 'MDPBV' AND dt.DD07T_DOMVALUE IS NOT NULL;

delete from number_fountain m where m.table_name = 'dim_availabilityconfirmation';

insert into number_fountain
select 	'dim_availabilityconfirmation',
	ifnull(max(d.dim_availabilityconfirmationid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_availabilityconfirmation d
where d.dim_availabilityconfirmationid <> 1; 
 
INSERT INTO dim_availabilityconfirmation(dim_availabilityconfirmationid,
							Description,
                           AvailabilityConfirmationCode,
                           RowStartDate,
                           RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_availabilityconfirmation') 
          + row_number() over(order by '') ,
			ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'MDPBV' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_availabilityconfirmation
                    WHERE AvailabilityConfirmationCode = DD07T_DOMVALUE AND DD07T_DOMNAME = 'MDPBV')
   ORDER BY 2;

delete from number_fountain m where m.table_name = 'dim_availabilityconfirmation';