delete from dim_legalentity;
delete from number_fountain m where m.table_name = 'dim_legalentity';
insert into number_fountain
select 'dim_legalentity',
            ifnull(max(d.dim_legalentityid ), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_legalentity d
where d.dim_legalentityid <> 1;

insert into dim_legalentity (DIM_LEGALENTITYID,LE_CODE,LE_DESCRIPTION,DATETO,DATEFROM)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_legalentity') + row_number() over(order by '')  as dim_legalentityid
	,BIC_Z_REPUNIT as LE_CODE
	,TXTLG as LE_DESCRIPTION
	,DATETO
	,DATEFROM
from BIC_TZ_REPUNIT where DATETO = '9999-12-31';

update dim_legalentity dl
	set dl.region = csvls.region
from dim_legalentity dl,csv_lc_le_region csvls
where trim(leading 0 from dl.LE_CODE) = csvls.LE_CODE;