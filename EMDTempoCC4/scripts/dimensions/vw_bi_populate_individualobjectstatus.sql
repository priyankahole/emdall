INSERT INTO dim_individualobjectstatus(
	dim_individualobjectstatusid,
	client,
	objectnumber,
	objectstatus,
	indicatorstatusisinactive,
	changenumber,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	1,
	'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    0,
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    ifnull((select s.dim_projectsourceid from dim_projectsource s),1)
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_individualobjectstatus WHERE dim_individualobjectstatusid = 1);

delete from number_fountain m where m.table_name = 'dim_individualobjectstatus';
   
insert into number_fountain
select 	
	'dim_individualobjectstatus',
	ifnull(max(d.dim_individualobjectstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_individualobjectstatus d
where d.dim_individualobjectstatusid <> 1;


INSERT INTO dim_individualobjectstatus(
	dim_individualobjectstatusid,
	client,
	objectnumber,
	objectstatus,
	indicatorstatusisinactive,
	changenumber,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_individualobjectstatus') 
		+ row_number() over(order by ''),
	ifnull(JEST_MANDT,'Not Set'),
    ifnull(JEST_OBJNR,'Not Set'),
    ifnull(JEST_STAT,'Not Set'),
    ifnull(JEST_INACT,'Not Set'),
    ifnull(JEST_CHGNR,0),
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    ifnull((select s.dim_projectsourceid from dim_projectsource s),1)
FROM pma_jest;