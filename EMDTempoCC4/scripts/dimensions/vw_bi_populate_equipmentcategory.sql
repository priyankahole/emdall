INSERT INTO dim_equipmentcategory(dim_equipmentcategoryId, RowIsCurrent,equipmentcategory,equipmentcategorydesc,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_equipmentcategory
               WHERE dim_equipmentcategoryid = 1);

delete from number_fountain m where m.table_name = 'dim_equipmentcategory';
   
insert into number_fountain
select 	
	'dim_equipmentcategory',
	ifnull(max(d.dim_equipmentcategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_equipmentcategory d
where d.dim_equipmentcategoryid <> 1;

INSERT INTO dim_equipmentcategory
		(dim_equipmentcategoryid,
        equipmentcategory,
		equipmentcategorydesc,
        RowStartDate,
        RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_equipmentcategory') 
		+ row_number() over(order by ''),
		T370U_EQTYP,
		T370U_TYPTX,
		current_timestamp,
		1
FROM PM_T370U t1
WHERE  NOT EXISTS
			(SELECT 1
			   FROM dim_equipmentcategory s
			  WHERE     s.equipmentcategory = t1.T370U_EQTYP
					AND s.RowIsCurrent = 1);


UPDATE  dim_equipmentcategory s 
SET s.equipmentcategorydesc = T370U_TYPTX,
	s.dw_update_date = current_timestamp
FROM PM_T370U t1, dim_equipmentcategory s	
WHERE s.RowIsCurrent = 1
	AND  s.equipmentcategory = t1.T370U_EQTYP
	AND s.equipmentcategorydesc <> ifnull(T370U_TYPTX,'Not Set');