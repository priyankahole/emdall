INSERT INTO dim_techobjecttype(dim_techobjecttypeid, RowIsCurrent,techobjecttype,techobjecttypedesc,
rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_techobjecttype
               WHERE dim_techobjecttypeid = 1);

delete from number_fountain m where m.table_name = 'dim_techobjecttype';
   
insert into number_fountain
select 	'dim_techobjecttype',
	ifnull(max(d.dim_techobjecttypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_techobjecttype d
where d.dim_techobjecttypeid <> 1; 

INSERT INTO dim_techobjecttype(dim_techobjecttypeid,
                                     techobjecttype,
									 techobjecttypedesc,
                                     RowStartDate,
                                     RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_techobjecttype') 
          + row_number() over(order by ''),
			 T370K_T_EQART,
          T370K_T_EARTX,
          current_timestamp,
          1
     FROM PM_T370K_T t1
    WHERE  NOT EXISTS
                (SELECT 1
                   FROM dim_techobjecttype s
                  WHERE     s.techobjecttype = t1.T370K_T_EQART
                        AND s.RowIsCurrent = 1)
;


UPDATE       dim_techobjecttype s
   SET s.techobjecttypedesc = T370K_T_EARTX,
			s.dw_update_date = current_timestamp
FROM dim_techobjecttype s, PM_T370K_T t1
 WHERE s.RowIsCurrent = 1
 AND  s.techobjecttype = t1.T370K_T_EQART
 AND s.techobjecttypedesc <> ifnull(T370K_T_EARTX,'Not Set');