

insert into dim_keycountry(dim_keycountryid,
                           KeyCountry)
select 1, 'Not Set' 
from (select 1) a
where not exists ( select 'x' from dim_keycountry where dim_keycountryid = 1);

insert into dim_keycountry
(
  dim_keycountryid,   
  KeyCountry,
  Shortdescription , 
  Mediumdescription,  
  ObjectVersion , 
  Changeflag , 
  OldCountryID , 
  PurchLowCostCountry  ,
  IsValid , 
  Country, 
  KeyRegion,
  PROJECTSOURCEID,
  Region_ShortDescription,
  Region_MediumDescription
)
SELECT 
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1) + ROW_NUMBER() OVER(ORDER BY  '') dim_keycountryid,   
  IFNULL(tz.BIC_Z_KEYCO2,'Not Set') as KeyCountry,
  IFNULL(tz.TXTSH,'Not Set') as Shortdescription, 
  IFNULL(tz.TXTMD,'Not Set') as Mediumdescription,
  IFNULL(pz.OBJVERS,'Not Set')  as ObjectVersion,
  IFNULL(pz.CHANGED,'Not Set') as Changeflag, 
  IFNULL(pz.BIC_Z_OLDCTRY,'Not Set') as OldCountryID,
  IFNULL(pz.BIC_Z_PU_LC,'Not Set') as PurchLowCostCountry,
  IFNULL(pz.BIC_Z_VALID,'Not Set') as IsValid,
  IFNULL(pz.COUNTRY,'Not Set') as Country,
  IFNULL(pz.BIC_Z_KEYRG,'Not Set') as KeyRegion,
  1 as PROJECTSOURCEID,
  IFNULL(tzreg.TXTSH, 'Not Set') as Region_ShortDescription,
  IFNULL(tzreg.TXTMD, 'Not Set') as Region_MediumDescription

FROM BIC_PZ_KEYCO2 pz, BIC_TZ_KEYCO2 tz, BIC_TZ_KEYRG tzreg
WHERE	    pz.BIC_Z_KEYCO2  = tz.BIC_Z_KEYCO2
 	AND pz.BIC_Z_KEYRG = tzreg.BIC_Z_KEYRG
	AND pz.BIC_Z_KEYCO2 NOT IN (SELECT KeyCountry FROM dim_keycountry);  