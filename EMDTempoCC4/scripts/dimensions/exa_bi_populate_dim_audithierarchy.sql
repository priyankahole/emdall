drop table if exists tmp_audit_hierarchy;
create table tmp_audit_hierarchy as
select
				a.hieid, a.objvers, a.nodeid, a.iobjnm, a.nodename, a.tlevel, a.link, a.parentid, a.childid, a.nextid
				,b.langu , b.dateto , b.datefrom, b.txtsh, b.txtmd, b.txtlg
from BIC_HZ_AUDITID a
inner join BIC_TZ_AUDITID b on a.nodename = b.bic_z_auditid
where 1=1
and dateto = '9999-12-31'
union all
select a.hieid, a.objvers, a.nodeid, a.iobjnm, a.nodename, a.tlevel, a.link, a.parentid, a.childid, a.nextid
,b.langu , null as dateto , null as datefrom
, b.txtsh, b.txtmd, b.txtlg
from BIC_HZ_AUDITID a
inner join RSTHIERNODE b on a.nodename = b.nodename and a.hieid = b.hieid;

drop table if exists tmp_audit_hierarchy_level;
create table tmp_audit_hierarchy_level as
select
	 h1.nodename as nodename_l1, h1.txtlg as txtlg_l1
	,h2.nodename as nodename_l2, h2.txtlg as txtlg_l2
	,h3.nodename as nodename_l3, h3.txtlg as txtlg_l3
	,h4.nodename as nodename_l4, h4.txtlg as txtlg_l4
	,h5.nodename as nodename_l5, h5.txtlg as txtlg_l5
	,h5.nodename as nodename_l6, h5.txtlg as txtlg_l6
	,h1.hieid as hieid
from tmp_audit_hierarchy h1
left join tmp_audit_hierarchy h2 on h1.parentid = h2.nodeid and h1.hieid = h2.hieid
left join tmp_audit_hierarchy h3 on h2.parentid = h3.nodeid and h2.hieid = h3.hieid
left join tmp_audit_hierarchy h4 on h3.parentid = h4.nodeid and h3.hieid = h4.hieid
left join tmp_audit_hierarchy h5 on h4.parentid = h5.nodeid and h4.hieid = h5.hieid
left join tmp_audit_hierarchy h6 on h5.parentid = h6.nodeid and h5.hieid = h6.hieid
order by h1.tlevel;

TRUNCATE TABLE dim_audithierarchy;

INSERT INTO dim_audithierarchy(
dim_audithierarchyid,
hieid,
nodename_l1,
txtlg_l1,
nodename_l2,
txtlg_l2,
nodename_l3,
txtlg_l3,
nodename_l4,
txtlg_l4,
nodename_l5,
txtlg_l5,
nodename_l6,
txtlg_l6
)
SELECT 1 as dim_audithierarchyid,
       'Not Set' as hieid,
       'Not Set' as nodename_l1,
       'Not Set' as txtlg_l1,
       'Not Set' as nodename_l2,
       'Not Set' as txtlg_l2,
       'Not Set' as nodename_l3,
       'Not Set' as txtlg_l3,
       'Not Set' as nodename_l4,
       'Not Set' as txtlg_l4,
       'Not Set' as nodename_l5,
       'Not Set' as txtlg_l5,
       'Not Set' as nodename_l6,
       'Not Set' as txtlg_l6
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_audithierarchy WHERE dim_audithierarchyid = 1);

delete from number_fountain m where m.table_name = 'dim_audithierarchy';

INSERT INTO number_fountain
SELECT 	'dim_audithierarchy',
	ifnull(max(d.dim_audithierarchyid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM dim_audithierarchy d
WHERE d.dim_audithierarchyid <> 1;

INSERT INTO dim_audithierarchy(
dim_audithierarchyid,
hieid,
nodename_l1,
txtlg_l1,
nodename_l2,
txtlg_l2,
nodename_l3,
txtlg_l3,
nodename_l4,
txtlg_l4,
nodename_l5,
txtlg_l5,
nodename_l6,
txtlg_l6
)
SELECT (select max_id from number_fountain where table_name = 'dim_audithierarchy') + row_number() over(order by '') AS dim_audithierarchyid,
       hieid as hieid,
       ifnull(nodename_l1, 'Not Set'),
       ifnull(txtlg_l1,'Not Set'),
       ifnull(nodename_l2, 'Not Set'),
       ifnull(txtlg_l2,'Not Set'),
       ifnull(nodename_l3, 'Not Set'),
       ifnull(txtlg_l3,'Not Set'),
       ifnull(nodename_l4, 'Not Set'),
       ifnull(txtlg_l4,'Not Set'),
       ifnull(nodename_l5, 'Not Set'),
       ifnull(txtlg_l5,'Not Set'),
       ifnull(nodename_l6, 'Not Set'),
       ifnull(txtlg_l6,'Not Set')
FROM tmp_audit_hierarchy_level;

DROP TABLE IF EXISTS tmp_audit_hierarchy_level;

UPDATE dim_audithierarchy dim
SET dim.type = 'Not Set';

UPDATE dim_audithierarchy dim
SET dim.TYPE = ifnull(csvv.audit_type, 'Not Set')
FROM csv_audithierarchy csvv,
     dim_audithierarchy dim
WHERE csvv.AUDITID = dim.nodename_l1
      AND csvv.HIERARCHY_ID = dim.HIEID;

DELETE FROM emd586.dim_audithierarchy;
INSERT INTO emd586.dim_audithierarchy SELECT * FROM dim_audithierarchy;
