DROP TABLE IF EXISTS tmp_repos;
CREATE TABLE tmp_repos
AS
SELECT a.HIEID,
       a.OBJVERS,
       a.NODEID,
       a.IOBJNM,
       a.NODENAME,
       a.TLEVEL,
       a.LINK,
       a.PARENTID,
       a.CHILDID,
       a.NEXTID,
       b.LANGU,
       b.DATETO,
       b.DATEFROM,
       b.TXTSH,
       b.TXTMD,
       b.TXTLG
FROM BIC_HZ_REPPOS a
     INNER JOIN BIC_TZ_REPPOS b on a.nodename = b.bic_z_reppos
WHERE 1 = 1
      AND hieid in ('5845BW8QVWD4NVGQU4MMHPC1O', '5CDYCYHG1AEV0KXG0K1KOPLQ2')
      AND dateto = '9999-12-31';

drop table if exists tmp_repos_level;
create table tmp_repos_level as
select h1.nodename as nodename_l1,
       h1.txtlg as txtlg_l1,
       h2.nodename as nodename_l2,
       h2.txtlg as txtlg_l2,
       h3.nodename as nodename_l3,
       h3.txtlg as txtlg_l3,
       h4.nodename as nodename_l4,
       h4.txtlg as txtlg_l4,
       h1.HIEID
FROM tmp_repos h1
     LEFT JOIN tmp_repos h2 on h1.parentid = h2.nodeid and h1.hieid = h2.hieid
     LEFT JOIN tmp_repos h3 on h2.parentid = h3.nodeid and h2.hieid = h3.hieid
     LEFT JOIN tmp_repos h4 on h3.parentid = h4.nodeid and h3.hieid = h4.hieid
ORDER BY h1.tlevel;

TRUNCATE TABLE dim_positionhierarchy;

INSERT INTO dim_positionhierarchy(
dim_positionhierarchyid,
nodename_l1,
txtlg_l1,
nodename_l2,
txtlg_l2,
nodename_l3,
txtlg_l3,
nodename_l4,
txtlg_l4,
hieid
)
SELECT 1 as dim_positionhierarchyid,
       'Not Set' as nodename_l1,
       'Not Set' as txtlg_l1,
       'Not Set' as nodename_l2,
       'Not Set' as txtlg_l2,
       'Not Set' as nodename_l3,
       'Not Set' as txtlg_l3,
       'Not Set' as nodename_l4,
       'Not Set' as txtlg_l4,
       'Not Set' as hieid
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_positionhierarchy WHERE dim_positionhierarchyid = 1);

DELETE FROM number_fountain m where m.table_name = 'dim_positionhierarchy';

INSERT INTO number_fountain
SELECT 	'dim_positionhierarchy',
	ifnull(max(d.dim_positionhierarchyid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM dim_positionhierarchy d
WHERE d.dim_positionhierarchyid <> 1;


INSERT INTO dim_positionhierarchy(
dim_positionhierarchyid,
nodename_l1,
txtlg_l1,
nodename_l2,
txtlg_l2,
nodename_l3,
txtlg_l3,
nodename_l4,
txtlg_l4,
hieid
)
SELECT (select max_id from number_fountain where table_name = 'dim_positionhierarchy') + row_number() over(order by '') AS dim_positionhierarchyid,
       ifnull(nodename_l1, 'Not Set'),
       ifnull(txtlg_l1,'Not Set'),
       ifnull(nodename_l2, 'Not Set'),
       ifnull(txtlg_l2,'Not Set'),
       ifnull(nodename_l3, 'Not Set'),
       ifnull(txtlg_l3,'Not Set'),
       ifnull(nodename_l4, 'Not Set'),
       ifnull(txtlg_l4,'Not Set'),
       ifnull(HIEID, 'Not Set') as hieid
FROM tmp_repos_level;

DROP TABLE IF EXISTS tmp_repos_level;

DELETE FROM emd586.dim_positionhierarchy;
INSERT INTO emd586.dim_positionhierarchy SELECT * FROM dim_positionhierarchy;