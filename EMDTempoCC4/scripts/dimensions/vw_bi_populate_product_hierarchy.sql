INSERT INTO dim_producthierarchy
(dim_producthierarchyid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_producthierarchy where dim_producthierarchyid = 1);


UPDATE    dim_producthierarchy ph
       SET ph.Description = ifnull(VTEXT, 'Not Set'),
	       ph.dw_update_date = current_timestamp
	       FROM 
          t179t t,dim_producthierarchy ph 
 WHERE ph.RowIsCurrent = 1
 AND t.PRODH = ph.ProductHierarchy;
 
drop table if exists tmpt179t_ins;
drop table if exists tmpt179t_del;

create table tmpt179t_ins as
select PRODH from T179T;

create table tmpt179t_del as
select * from tmpt179t_ins where 1=2;

insert into tmpt179t_del
select ProductHierarchy from dim_producthierarchy;

merge into tmpt179t_ins dest
using tmpt179t_del src on dest.PRODH = src.PRODH
when matched then delete;

delete from number_fountain m where m.table_name = 'dim_producthierarchy';
   
insert into number_fountain
select 	'dim_producthierarchy',
	ifnull(max(d.dim_producthierarchyid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_producthierarchy d
where d.dim_producthierarchyid <> 1; 

INSERT INTO dim_producthierarchy(dim_producthierarchyid,
                                          ProductHierarchy,
                                          Description,
                                          RowStartDate,
                                          RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_producthierarchy') + row_number() over(order by ''),
		t1.PRODH,
        ifnull(t1.VTEXT, 'Not Set'),
        current_timestamp,
        1
FROM T179T t1, tmpt179t_ins t2
WHERE t1.PRODH = t2.prodh; 
