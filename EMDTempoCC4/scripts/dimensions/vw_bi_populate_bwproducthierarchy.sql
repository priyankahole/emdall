/******************************************************************************************************************/
/*   Script         : bi_populate_bwproducthierarchy                                                              */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 25 Nov 2015                                                                                 */
/*   Description    : Populating script of dim_bwproducthierarchy.                                                */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   25 Nov 2015         Cristian T      1.0               Creating the script.                                   */
/*   16 Feb 2016         Cristian T      1.1               Add logic for MRKHC_BRAND and MRKHC_STRENGTH           */
/******************************************************************************************************************/

/* Part 0 - Inserting the default row if not exists */ 
INSERT INTO dim_bwproducthierarchy
(
  dim_bwproducthierarchyid
)
SELECT 1
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 'x' FROM dim_bwproducthierarchy x WHERE x.dim_bwproducthierarchyid = 1);

/* Create tmp Upper Hierarchy table */

drop table if exists tmp_bw_upperhierarchy1;
create table tmp_bw_upperhierarchy1
as 
select 
	h1.hz_uphier_nodename h1_hz_uphier_nodename
	,ifnull(t1.txtlg,'Not Set') t1_txtlg
	,h2.hz_uphier_nodename h2_hz_uphier_nodename
	,ifnull(t2.txtlg,'Not Set') t2_txtlg
	,h3.hz_uphier_nodename h3_hz_uphier_nodename
	,ifnull(t3.txtlg,'Not Set') t3_txtlg
	,h4.hz_uphier_nodename h4_hz_uphier_nodename
	,ifnull(t4.txtlg,'Not Set') t4_txtlg
	,h5.hz_uphier_nodename h5_hz_uphier_nodename
	,ifnull(t5.txtlg,'Not Set') t5_txtlg
	,h6.hz_uphier_nodename h6_hz_uphier_nodename
	,ifnull(t6.txtlg,'Not Set') t6_txtlg
	,h7.hz_uphier_nodename h7_hz_uphier_nodename
	,ifnull(t7.txtlg,'Upper Product Hierarchy ' || left(rshiedir_datefrom,4)) t7_txtlg
	,right(h1.hz_uphier_nodename,3) upperhiercode
	,to_date(r.rshiedir_datefrom,'YYYYMMDD') upperhierstartdate
	,to_date(r.rshiedir_dateto,'YYYYMMDD') upperhierenddate
	,r.rshiedir_hieid upperhierid
from RSHIEDIR r
	inner join HZ_UPHIER h1 on h1.hz_uphier_hieid = r.rshiedir_hieid and h1.hz_uphier_tlevel = 7
	inner join HZ_UPHIER h2 on h2.hz_uphier_nodeid = h1.hz_uphier_parentid and h2.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h3 on h3.hz_uphier_nodeid = h2.hz_uphier_parentid and h3.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h4 on h4.hz_uphier_nodeid = h3.hz_uphier_parentid and h4.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h5 on h5.hz_uphier_nodeid = h4.hz_uphier_parentid and h5.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h6 on h6.hz_uphier_nodeid = h5.hz_uphier_parentid and h6.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h7 on h7.hz_uphier_nodeid = h6.hz_uphier_parentid and h7.hz_uphier_hieid = r.rshiedir_hieid
	left join BIC_TZ_UPHIER t1 on h1.hz_uphier_nodename = t1.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t1.datefrom and t1.dateto
	left join BIC_TZ_UPHIER t2 on h2.hz_uphier_nodename = t2.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t2.datefrom and t2.dateto
	left join BIC_TZ_UPHIER t3 on h3.hz_uphier_nodename = t3.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t3.datefrom and t3.dateto
	left join BIC_TZ_UPHIER t4 on h4.hz_uphier_nodename = t4.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t4.datefrom and t4.dateto
	left join BIC_TZ_UPHIER t5 on h5.hz_uphier_nodename = t5.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t5.datefrom and t5.dateto
	left join BIC_TZ_UPHIER t6 on h6.hz_uphier_nodename = t6.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t6.datefrom and t6.dateto
	left join BIC_TZ_UPHIER t7 on h7.hz_uphier_nodename = t7.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t7.datefrom and t7.dateto
where r.rshiedir_iobjnm = 'Z_UPHIER' and r.rshiedir_hienm = 'ORGUPH01' and r.rshiedir_version = 'A';

/* Part 1 - Update Codes and descriptions */

update dim_bwproducthierarchy d
set version = IFNULL(b.h7_hz_uphier_nodename,'Not Set')
	,versiondesc = IFNULL(b.t7_txtlg,'Not Set')
	,businesssector = IFNULL(b.h6_hz_uphier_nodename,'Not Set')
	,businesssectordesc = IFNULL(b.t6_txtlg,'Not Set')
	,business = IFNULL(b.h5_hz_uphier_nodename,'Not Set')
	,businessdesc = IFNULL(b.t5_txtlg,'Not Set')
	,businessunit = IFNULL(b.h4_hz_uphier_nodename,'Not Set')
	,businessunitdesc = IFNULL(b.t4_txtlg,'Not Set')
	,businessfield = IFNULL(b.h3_hz_uphier_nodename,'Not Set')
	,businessfielddesc = IFNULL(b.t3_txtlg,'Not Set')
	,businessline = IFNULL(b.h2_hz_uphier_nodename,'Not Set')
	,businesslinedesc = IFNULL(b.t2_txtlg,'Not Set')
	,productgroup = IFNULL(b.h1_hz_uphier_nodename,'Not Set')
	,productgroupdesc = IFNULL(b.t1_txtlg,'Not Set')
	,dw_update_date = current_date
from dim_bwproducthierarchy d, tmp_bw_upperhierarchy1 b
where d.upperhierarchycode = b.upperhiercode and d.upperhierid = b.upperhierid;


update dim_bwproducthierarchy d
set maingroup = IFNULL(c.level1code,'Not Set')
	,maingroupdesc = IFNULL(c.level1desc,'Not Set')
	,uppergroup = IFNULL(c.level2code,'Not Set')
	,uppergroupdesc = IFNULL(c.level2desc,'Not Set')
	,articlegroup = IFNULL(c.level3code,'Not Set')
	,articlegroupdesc = IFNULL(c.level3desc,'Not Set')
	,internationalarticle = IFNULL(c.level4code,'Not Set')
	,internationalarticledesc = IFNULL(c.level4desc,'Not Set')
	,dw_update_date = current_date
from dim_producthierarchy c, dim_bwproducthierarchy d
where d.lowerhierarchycode = c.producthierarchy;

/* Part 2 - Inserting new rows */

DROP TABLE IF EXISTS tmp_part_hiercodes;
CREATE TABLE tmp_part_hiercodes
as
SELECT DISTINCT productgroupsbu,producthierarchy FROM dim_part where productgroupsbu <> 'Not Set' or producthierarchy <> 'Not Set';

DELETE FROM number_fountain m 
WHERE m.table_name = 'dim_bwproducthierarchy';

INSERT INTO number_fountain
SELECT 'dim_bwproducthierarchy',
       IFNULL(MAX(bw.dim_bwproducthierarchyid), IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_bwproducthierarchy bw
WHERE bw.dim_bwproducthierarchyid <> 1;

DROP TABLE IF EXISTS tmp_insert_dim;
CREATE TABLE tmp_insert_dim
AS
SELECT DISTINCT
	1 dim_bwproducthierarchyid
	,IFNULL(b.h7_hz_uphier_nodename,'Not Set') version
	,IFNULL(b.t7_txtlg,'Not Set') versiondesc
	,IFNULL(b.h6_hz_uphier_nodename,'Not Set')  businesssector
	,IFNULL(b.t6_txtlg,'Not Set') businesssectordesc
	,IFNULL(b.h5_hz_uphier_nodename,'Not Set') business
	,IFNULL(b.t5_txtlg,'Not Set') businessdesc
	,IFNULL(b.h4_hz_uphier_nodename,'Not Set') businessunit
	,IFNULL(b.t4_txtlg,'Not Set') businessunitdesc
	,IFNULL(b.h3_hz_uphier_nodename,'Not Set') businessfield
	,IFNULL(b.t3_txtlg,'Not Set') businessfielddesc
	,IFNULL(b.h2_hz_uphier_nodename,'Not Set') businessline
	,IFNULL(b.t2_txtlg,'Not Set') businesslinedesc
	,IFNULL(b.h1_hz_uphier_nodename,'Not Set') productgroup
	,IFNULL(b.t1_txtlg,'Not Set') productgroupdesc
	,IFNULL(c.level1code,'Not Set') maingroup
	,IFNULL(c.level1desc,'Not Set') maingroupdesc
	,IFNULL(c.level2code,'Not Set') uppergroup
	,IFNULL(c.level2desc,'Not Set') uppergroupdesc
	,IFNULL(c.level3code,'Not Set') articlegroup
	,IFNULL(c.level3desc,'Not Set') articlegroupdesc
	,IFNULL(c.level4code,'Not Set') internationalarticle
	,IFNULL(c.level4desc,'Not Set') internationalarticledesc
	,IFNULL(b.upperhiercode,'Not Set') upperhierarchycode
	,IFNULL(b.upperhierstartdate,'0001-01-01') upperhierstartdate
	,IFNULL(b.upperhierenddate,'9999-01-01') upperhierenddate
	,IFNULL(b.upperhierid,'Not Set') upperhierid
	,IFNULL(c.producthierarchy,'Not Set') lowerhierarchycode
FROM tmp_part_hiercodes a 
     LEFT JOIN tmp_bw_upperhierarchy1 b ON b.upperhiercode = a.productgroupsbu
     LEFT JOIN dim_producthierarchy c ON c.producthierarchy = a.producthierarchy;

INSERT INTO dim_bwproducthierarchy
(
	dim_bwproducthierarchyid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,productgroup
	,productgroupdesc
	,maingroup
	,maingroupdesc
	,uppergroup
	,uppergroupdesc
	,articlegroup
	,articlegroupdesc
	,internationalarticle
	,internationalarticledesc
	,upperhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
	,lowerhierarchycode
)
SELECT
	(SELECT IFNULL(m.max_id, 1) 
	FROM number_fountain m 
	WHERE m.table_name = 'dim_bwproducthierarchy') + ROW_NUMBER() OVER(ORDER BY '') AS dim_bwproducthierarchyid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,productgroup
	,productgroupdesc
	,maingroup
	,maingroupdesc
	,uppergroup
	,uppergroupdesc
	,articlegroup
	,articlegroupdesc
	,internationalarticle
	,internationalarticledesc
	,upperhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
	,lowerhierarchycode
FROM tmp_insert_dim t
WHERE NOT EXISTS (SELECT 1 
                   FROM dim_bwproducthierarchy bw
                   WHERE IFNULL(t.upperhierarchycode,'Not Set') = bw.upperhierarchycode AND IFNULL(t.upperhierid,'Not Set') = bw.upperhierid AND IFNULL(t.lowerhierarchycode,'Not Set') = bw.lowerhierarchycode);

DROP TABLE IF EXISTS tmp_bw_upperhierarchy1;
DROP TABLE IF EXISTS tmp_part_hiercodes;

DROP TABLE IF EXISTS tmp_part_hiercodes;
CREATE TABLE tmp_part_hiercodes
AS
SELECT DISTINCT producthierarchy FROM dim_part WHERE producthierarchy <> 'Not Set';

DELETE FROM number_fountain m 
WHERE m.table_name = 'dim_bwproducthierarchy';

INSERT INTO number_fountain
SELECT 'dim_bwproducthierarchy',
       IFNULL(MAX(bw.dim_bwproducthierarchyid), IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_bwproducthierarchy bw
WHERE bw.dim_bwproducthierarchyid <> 1;

INSERT INTO dim_bwproducthierarchy
(
	dim_bwproducthierarchyid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,productgroup
	,productgroupdesc
	,maingroup
	,maingroupdesc
	,uppergroup
	,uppergroupdesc
	,articlegroup
	,articlegroupdesc
	,internationalarticle
	,internationalarticledesc
	,upperhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
	,lowerhierarchycode
)
SELECT (SELECT IFNULL(m.max_id, 1) 
        FROM number_fountain m 
        WHERE m.table_name = 'dim_bwproducthierarchy') + ROW_NUMBER() OVER(ORDER BY '') AS dim_bwproducthierarchyid
        ,'Not Set' version
        ,'Not Set' versiondesc
        ,'Not Set' businesssector
        ,'Not Set' businesssectordesc
        ,'Not Set' business
        ,'Not Set' businessdesc
        ,'Not Set' businessunit
        ,'Not Set' businessunitdesc
        ,'Not Set' businessfield
        ,'Not Set' businessfielddesc
        ,'Not Set' businessline
        ,'Not Set' businesslinedesc
        ,'Not Set' productgroup
        ,'Not Set' productgroupdesc
        ,IFNULL(c.level1code,'Not Set') maingroup
        ,IFNULL(c.level1desc,'Not Set') maingroupdesc
        ,IFNULL(c.level2code,'Not Set') uppergroup
        ,IFNULL(c.level2desc,'Not Set') uppergroupdesc
        ,IFNULL(c.level3code,'Not Set') articlegroup
        ,IFNULL(c.level3desc,'Not Set') articlegroupdesc
        ,IFNULL(c.level4code,'Not Set') internationalarticle
        ,IFNULL(c.level4desc,'Not Set') internationalarticledesc
        ,'Not Set' upperhierarchycode
        ,'0001-01-01' upperhierstartdate
        ,'9999-01-01' upperhierenddate
        ,'Not Set' upperhierid
        ,IFNULL(c.producthierarchy,'Not Set') lowerhierarchycode
FROM tmp_part_hiercodes a 
     INNER JOIN dim_producthierarchy c ON c.producthierarchy = a.producthierarchy
WHERE NOT EXISTS (SELECT 1 
                   FROM dim_bwproducthierarchy bw
                   WHERE bw.upperhierarchycode = 'Not Set' AND bw.upperhierid ='Not Set' AND IFNULL(c.producthierarchy,'Not Set') = bw.lowerhierarchycode);

/* 16 Feb 2016 CristianT Start: Add logic for MRKHC_BRAND and MRKHC_STRENGTH */
UPDATE dim_bwproducthierarchy
SET MRKHC_BRAND = IFNULL(
					  CASE
						WHEN LOCATE( '_', internationalarticledesc) = 0 THEN internationalarticledesc
						ELSE LEFT(internationalarticledesc, LOCATE( '_', internationalarticledesc)-1)
					  END
				  ,'Not Set')
WHERE upper(businesssectordesc) in ('HEALTHCARE','PHARMACEUTICALS');

DROP TABLE IF EXISTS tmp_MRKHC_STRENGTH;
CREATE TABLE tmp_MRKHC_STRENGTH
AS
SELECT businesssectordesc,
       internationalarticledesc,
       MRKHC_BRAND,
       ifnull(CASE
                WHEN LOCATE('_', MRKHC_STRENGTH) = 0 THEN MRKHC_STRENGTH
                ELSE MRKHC_BRAND || RIGHT(MRKHC_STRENGTH, LENGTH (MRKHC_STRENGTH) - LOCATE('_', MRKHC_STRENGTH))
              END, 'Not Set') MRKHC_STRENGTH
FROM (
SELECT distinct businesssectordesc,
       internationalarticledesc,
       MRKHC_BRAND,
       CASE
          WHEN internationalarticledesc = 'Not Set' THEN 'Not Set'
          WHEN LOCATE('_', internationalarticledesc) > length(internationalarticledesc) THEN internationalarticledesc
          ELSE RIGHT(internationalarticledesc, (length(internationalarticledesc) - LOCATE('_', internationalarticledesc)))
       END MRKHC_STRENGTH
FROM dim_bwproducthierarchy
WHERE 1 = 1
      AND upper(businesssectordesc) in ('HEALTHCARE','PHARMACEUTICALS')
      AND internationalarticledesc <> 'Not Set'
) t;

UPDATE dim_bwproducthierarchy dim
SET dim.MRKHC_STRENGTH = IFNULL(tmp.MRKHC_STRENGTH,'Not Set')
FROM dim_bwproducthierarchy dim, tmp_MRKHC_STRENGTH tmp
WHERE dim.businesssectordesc = tmp.businesssectordesc
      AND dim.internationalarticledesc = tmp.internationalarticledesc
      AND ifnull(dim.MRKHC_STRENGTH, 'aaaaccc') <> IFNULL(tmp.MRKHC_STRENGTH,'Not Set');

DROP TABLE IF EXISTS tmp_MRKHC_STRENGTH;

update dim_bwproducthierarchy set bussinessnamesort = CASE WHEN businessdesc = 'Applied Solutions' THEN 1 
WHEN businessdesc = 'Research Solutions' THEN 2 
WHEN businessdesc = 'Process Solutions' THEN 3 
ELSE 0 END
where bussinessnamesort <> CASE WHEN businessdesc = 'Applied Solutions' THEN 1 
WHEN businessdesc = 'Research Solutions' THEN 2 
WHEN businessdesc = 'Process Solutions' THEN 3 
ELSE 0 END;

/*@Catalin add business fields*/
update dim_bwproducthierarchy
set business_ps = 'R'||'&'||'A'
where business in ('DIV-61','DIV-62')
and business_ps <>'R'||'&'||'A';

update dim_bwproducthierarchy
set business_ps = 'PS'
where business in ('DIV-63')
and business_ps <>'PS';

update dim_bwproducthierarchy
set businessname_ps = 'Research and Applied Solutions'
where business in ('DIV-61','DIV-62')
and businessname_ps <>'Research and Applied Solutions';

update dim_bwproducthierarchy
set businessname_ps = 'Process Solutions'
where business in ('DIV-63')
and businessname_ps <>'Process Solutions';

/* Expression test  */

update dim_bwproducthierarchy
set businesslinedescription = case when businessline != 'Not Set' then businessline ||  ' - ' || businesslinedesc else businesslinedesc end
where businesslinedescription <> case when businessline != 'Not Set' then businessline ||  ' - ' || businesslinedesc else businesslinedesc end;

update dim_bwproducthierarchy
set businessfielddescription = case when businessfield != 'Not Set' then businessfield ||  ' - ' || businessfielddesc else businessfielddesc end
where businessfielddescription <> case when businessfield != 'Not Set' then businessfield ||  ' - ' || businessfielddesc else businessfielddesc end;

update dim_bwproducthierarchy
set mrkhc_brand = upper(mrkhc_brand)
where mrkhc_brand <> upper(mrkhc_brand);

update dim_bwproducthierarchy
set mrkhc_strength = upper(mrkhc_strength)
where mrkhc_strength <> upper(mrkhc_strength);

update dim_bwproducthierarchy
set businessunitdescription = case when businessunit != 'Not Set' then businessunit ||  ' - ' || businessunitdesc else businessunitdesc end
where businessunitdescription <> case when businessunit != 'Not Set' then businessunit ||  ' - ' || businessunitdesc else businessunitdesc end;

update dim_bwproducthierarchy
set productgroupdescription = case when productgroup != 'Not Set' then productgroup || ' - ' ||productgroupdesc else productgroupdesc end
where productgroupdescription <> case when productgroup != 'Not Set' then productgroup|| ' - ' ||productgroupdesc else productgroupdesc end;
