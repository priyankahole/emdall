
DELETE FROM dim_maintenanceorderheader;

INSERT INTO dim_maintenanceorderheader(
	dim_maintenanceorderheaderid,
	ordernumber,
	prioritytype,
	equipmentnumber,
	maintenanceplanningplant,
	plannergroupforplantmaintenance,
	maintenanceactivitytype,
	notificationno,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	1,
	'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1)
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_maintenanceorderheader WHERE dim_maintenanceorderheaderid = 1);

DELETE FROM number_fountain m WHERE m.table_name = 'dim_maintenanceorderheader';
   
INSERT INTO number_fountain
SELECT 	
	'dim_maintenanceorderheader',
	IFNULL(MAX(d.dim_maintenanceorderheaderid), 
	IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_maintenanceorderheader d
WHERE d.dim_maintenanceorderheaderid <> 1;


INSERT INTO dim_maintenanceorderheader(
	dim_maintenanceorderheaderid,
	ordernumber,
	prioritytype,
	equipmentnumber,
	maintenanceplanningplant,
	plannergroupforplantmaintenance,
	maintenanceactivitytype,
	notificationno,
	rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	(SELECT IFNULL(m.max_id, 1) FROM number_fountain m WHERE m.table_name = 'dim_maintenanceorderheader') 
		+ ROW_NUMBER() OVER(ORDER BY ''),
	IFNULL(AFIH_AUFNR,'Not Set'),
    IFNULL(afih_artpr,'Not Set'),
    IFNULL(afih_equnr,'Not Set'),
    IFNULL(afih_iwerk,'Not Set'),
    IFNULL(afih_ingpr,'Not Set'),
    IFNULL(afih_ilart,'Not Set'),
    IFNULL(afih_qmnum,'Not Set'),
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1)
FROM pma_afih;