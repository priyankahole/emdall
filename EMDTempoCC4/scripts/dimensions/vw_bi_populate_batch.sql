/*  Populate dim_batch */
/* preprocess */ 
drop table if exists INOB_BB;
create table INOB_BB
as select b.*,RTRIM(SUBSTR(INOB_OBJEK,1,INSTR(INOB_OBJEK,'  '))) INOB_MATNR,
LTRIM(SUBSTR(INOB_OBJEK,INSTR(INOB_OBJEK,'   ',-1)+1,LENGTH(INOB_OBJEK)-INSTR(INOB_OBJEK,'   ',-1))) INOB_CHARG from INOB b;

drop table if exists tmp_distinct_INOB_BB;
create table tmp_distinct_INOB_BB as
select b.* from (
select a.*,row_Number() over (partition by INOB_MATNR,INOB_CHARG order by INOB_CUOBJ desc) as RN 
from INOB_BB a) b
where b.rn=1;

drop table if exists INOB_BB;
rename tmp_distinct_INOB_BB to INOB_BB;
alter table INOB_BB drop RN cascade;


drop table if exists tmp_distinct_mch1;
create table tmp_distinct_mch1 as
select b.* from (
select a.*,row_Number() over (partition by mch1_matnr,mch1_charg order by mch1_laeda,mch1_ersda desc) as RN 
from MCH1 a) b
where b.rn=1; 

drop table if exists tmp_distinct_mcha;
create table tmp_distinct_mcha as
select b.* from (
select a.*,row_Number() over (partition by mcha_matnr,mcha_werks,mcha_charg order by mcha_laeda,mcha_ersda desc) as RN 
from MCHA a) b
where b.rn=1;

drop table if exists MCH1;
drop table if exists MCHA;
rename tmp_distinct_mcha to MCHA;
rename tmp_distinct_mch1 to MCH1;
alter table MCHA drop RN cascade;
alter table MCH1 drop RN cascade;



drop table if exists tmp_dim_batch;
CREATE TABLE tmp_dim_batch LIKE dim_batch INCLUDING DEFAULTS INCLUDING IDENTITY;

insert into tmp_dim_batch
select * from dim_batch;

insert into tmp_dim_batch (dim_batchid,
                          PartNumber,
                          BatchNumber,
     InternalObjNo)
select 1, 'Not Set', 'Not Set', 0
from (select 1) a
where not exists ( select 'x' from tmp_dim_batch where dim_batchid = 1);
    
delete from number_fountain m where m.table_name = 'dim_batch';
insert into number_fountain
select 'dim_batch',
 ifnull(max(d.dim_batchid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_dim_batch d
where d.dim_batchid <> 1;

insert into tmp_dim_batch (dim_batchid,
                          PartNumber,
                          BatchNumber,
     InternalObjNo,
                          dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_batch') + row_number() over(order by '') AS dim_batchid,
        ifnull(ds.MCH1_MATNR, 'Not Set') AS PartNumber,
        ifnull(ds.MCH1_CHARG, 'Not Set') AS BatchNumber,
 ifnull(ds.MCH1_CUOBJ_BM, 0) AS InternalObjNo,
       current_timestamp
from MCH1 ds
 where not exists (select 'x' from dim_batch dc where dc.PartNumber = ifnull(ds.MCH1_MATNR, 'Not Set') AND dc.BatchNumber = ifnull(ds.MCH1_CHARG, 'Not Set'));

delete from tmp_dim_batch d
where not exists (select 1 from MCH1 ds where d.PartNumber = ifnull(ds.MCH1_MATNR, 'Not Set') AND d.BatchNumber = ifnull(ds.MCH1_CHARG, 'Not Set'))
and d.plantcode = 'Not Set' and d.dim_batchid <> 1;

 /*Georgiana EA Addons -adding MCHA fields 03 Feb 2016*/
 
delete from number_fountain m where m.table_name = 'dim_batch';
insert into number_fountain
select 'dim_batch',
 ifnull(max(d.dim_batchid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_dim_batch d
where d.dim_batchid <> 1;

insert into tmp_dim_batch (dim_batchid,
                          PartNumber,
                          BatchNumber,
                          PlantCode,
                          dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_batch') + row_number() over(order by '') AS dim_batchid,
        ifnull(ds.MCHA_MATNR, 'Not Set') AS PartNumber,
        ifnull(ds.MCHA_CHARG, 'Not Set') AS BatchNumber,
        ifnull( ds.MCHA_WERKS, 'Not Set') as PlantCode,
       current_timestamp
from MCHA ds
 where not exists (select 'x' from dim_batch dc where dc.PartNumber = ifnull(ds.MCHA_MATNR, 'Not Set') AND dc.BatchNumber = ifnull(ds.MCHA_CHARG, 'Not Set') and dc.PlantCode = ifnull( ds.MCHA_WERKS, 'Not Set'));
 
delete from tmp_dim_batch d
 where not exists (select 1 from MCHA ds where d.PartNumber = ifnull(ds.MCHA_MATNR, 'Not Set') AND d.BatchNumber = ifnull(ds.MCHA_CHARG, 'Not Set') and d.PlantCode = ifnull( ds.MCHA_WERKS, 'Not Set'))
and d.plantcode <> 'Not Set' and d.dim_batchid <> 1;

/*Georgiana End Of Changes*/ 

update tmp_dim_batch bc
set bc.InternalObjNo = ifnull(m.MCH1_CUOBJ_BM, 0),
    bc.dw_update_date = current_timestamp
from MCH1 m, tmp_dim_batch bc
where     bc.PartNumber = ifnull(m.MCH1_MATNR,'Not Set')
   and  bc.BatchNumber = ifnull(m.MCH1_CHARG,'Not Set')
      and InternalObjNo <> ifnull(m.MCH1_CUOBJ_BM, 0);

/* Roxana - Fix processig eror : different ATZHL column  */
    

update tmp_dim_batch bt
set bt.dateofmanufacture = ifnull(m.MCH1_HSDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where m.MCH1_MATNR = bt.partnumber and m.MCH1_CHARG = bt.batchnumber
and bt.dateofmanufacture <> ifnull(m.MCH1_HSDAT,'0001-01-01');


 update tmp_dim_batch bt
SET vendorbatchnumber = ifnull(MCH1_LICHA,'Not Set'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and vendorbatchnumber <> ifnull(MCH1_LICHA,'Not Set');

/* Octavian: Every Angle Transition Addons - PPE changes */
 update tmp_dim_batch bt
SET vendoracctnumber = ifnull(MCH1_LIFNR,'Not Set'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and vendoracctnumber <> ifnull(MCH1_LIFNR,'Not Set');

 update tmp_dim_batch bt
SET batch_restriction = ifnull(MCH1_ZUSTD,'Not Set'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and batch_restriction <> ifnull(MCH1_ZUSTD,'Not Set');

 update tmp_dim_batch bt
SET lastgoodreceipt = ifnull(MCH1_LWEDT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and lastgoodreceipt <> ifnull(MCH1_LWEDT,'0001-01-01');

 update tmp_dim_batch bt
SET bestbeforedate = ifnull(MCH1_VFDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and bestbeforedate <> ifnull(MCH1_VFDAT,'0001-01-01');
/* Octavian: Every Angle Transition Addons - PPE changes */

/* Octavian: Every Angle Transition Addons */

/* Ovidiu: Every Angle Transition Addons */



/* Ovidiu: Every Angle changes */

/* Octavian: Every Angle changes */
 update tmp_dim_batch bt
SET creator = ifnull(MCH1_ERNAM,'Not Set'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and creator <> ifnull(MCH1_ERNAM,'Not Set'); 

 update tmp_dim_batch bt
SET creationdate = ifnull(MCH1_ERSDA,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and creationdate <> ifnull(MCH1_ERSDA,'0001-01-01');

 update tmp_dim_batch bt
SET nextinspectiondate = ifnull(MCH1_QNDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and nextinspectiondate <> ifnull(MCH1_QNDAT,'0001-01-01');

 update tmp_dim_batch bt
SET laststatuschangedate = ifnull(MCH1_ZAEDT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and laststatuschangedate <> ifnull(MCH1_ZAEDT,'0001-01-01');
/* Octavian: Every Angle changes */

/*Georgiana EA Addons 03 Feb 2016*/

 update tmp_dim_batch bt
SET creationdate = ifnull(MCHA_ERSDA,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCHA,tmp_dim_batch bt
where partnumber = ifnull(MCHA_MATNR,'Not Set')
and batchnumber = ifnull(MCHA_CHARG,'Not Set')
and plantcode = ifnull(MCHA_WERKS, 'Not Set')
and creationdate <> ifnull(MCHA_ERSDA,'0001-01-01');

 update tmp_dim_batch bt
SET bestbeforedate_mcha = ifnull(MCHA_VFDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCHA,tmp_dim_batch bt
where partnumber = ifnull(MCHA_MATNR,'Not Set')
and batchnumber = ifnull(MCHA_CHARG,'Not Set')
and plantcode = ifnull(MCHA_WERKS, 'Not Set')
and bestbeforedate_mcha <> ifnull(MCHA_VFDAT,'0001-01-01');

 update tmp_dim_batch bt
SET dateoflastchange = ifnull(MCHA_LAEDA,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCHA,tmp_dim_batch bt

where partnumber = ifnull(MCHA_MATNR,'Not Set')
and batchnumber = ifnull(MCHA_CHARG,'Not Set')
and plantcode = ifnull(MCHA_WERKS, 'Not Set')
and dateoflastchange <> ifnull(MCHA_LAEDA,'0001-01-01');

drop table if exists dim_batch;
rename table tmp_dim_batch to dim_batch;