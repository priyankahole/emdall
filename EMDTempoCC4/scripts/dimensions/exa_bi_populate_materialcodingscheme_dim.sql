/******************************************************************************************************************/
/*   Script         : exa_bi_populate_materialcodingscheme_dim                                                    */
/*   Author         : Cristian Cleciu                                                                             */
/*   Created On     : 26 Oct 2017                                                                                 */
/*   Description    : Populating script of dim_materialcodingscheme                                               */
/*********************************************Change History*******************************************************/
/*   Date             By                 Version           Desc                                                   */
/*   26 Oct 2017      Cristian Cleciu    1.0               Creating the script.                                   */
/******************************************************************************************************************/

DROP TABLE IF EXISTS number_fountain_dim_materialcodingscheme ;
CREATE TABLE number_fountain_dim_materialcodingscheme  LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM dim_materialcodingscheme
WHERE dim_materialcodingschemeid <> 1;

INSERT INTO dim_materialcodingscheme(
dim_materialcodingschemeid
)
SELECT 1 as dim_materialcodingschemeid
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_materialcodingscheme WHERE dim_materialcodingschemeid = 1);

DELETE FROM number_fountain_dim_materialcodingscheme m WHERE m.table_name = 'dim_materialcodingscheme';

INSERT INTO number_fountain_dim_materialcodingscheme
SELECT 	'dim_materialcodingscheme',
	    IFNULL(MAX(d.dim_materialcodingschemeid),IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_materialcodingscheme d
WHERE d.dim_materialcodingschemeid <> 1;

INSERT INTO dim_materialcodingscheme(
dim_materialcodingschemeid,
pos_1,
category,
dw_insert_date,
dw_update_date,
projectsourceid,
rowiscurrent
)
SELECT (SELECT IFNULL(m.max_id, 1) FROM number_fountain_dim_materialcodingscheme m WHERE m.table_name = 'dim_materialcodingscheme') + ROW_NUMBER() over(order by '') AS dim_materialcodingschemeid,
       t.*
FROM (
       SELECT ifnull(mcs.pos_1, 'Not Set') as pos_1,
              ifnull(mcs.category, 0) as category,
              current_timestamp as dw_insert_date,
              current_timestamp as dw_update_date,
              1 as projectsourceid,
              1 as rowiscurrent
       FROM E2E_LT_Material_Coding_Scheme mcs
     ) t;

UPDATE dim_materialcodingscheme dim
SET dim.projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     dim_materialcodingscheme dim;

DELETE FROM emd586.dim_materialcodingscheme;
INSERT INTO emd586.dim_materialcodingscheme
SELECT *
FROM dim_materialcodingscheme;