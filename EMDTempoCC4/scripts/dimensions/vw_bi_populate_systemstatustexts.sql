INSERT INTO dim_systemstatustexts(
	dim_systemstatustextsid,
	systemstatus,
	languagekey,
	individualstatusobjectshortform,
	objectstatus,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	1,
	'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    ifnull((select s.dim_projectsourceid from dim_projectsource s),1)
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_systemstatustexts WHERE dim_systemstatustextsid = 1);

delete from number_fountain m where m.table_name = 'dim_systemstatustexts';
   
insert into number_fountain
select 	
	'dim_systemstatustexts',
	ifnull(max(d.dim_systemstatustextsid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_systemstatustexts d
where d.dim_systemstatustextsid <> 1;


INSERT INTO dim_systemstatustexts(
	dim_systemstatustextsid,
	systemstatus,
	languagekey,
	individualstatusobjectshortform,
	objectstatus,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_systemstatustexts') 
		+ row_number() over(order by ''),
	ifnull(TJ02T_ISTAT,'Not Set'),
    ifnull(TJ02T_SPRAS,'Not Set'),
    ifnull(TJ02T_TXT04,'Not Set'),
    ifnull(TJ02T_TXT30,'Not Set'),
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    ifnull((select s.dim_projectsourceid from dim_projectsource s),1)
FROM pma_TJ02T;