insert into dim_keycustomer(dim_keycustomerid,
                           KeyCustomer)
select 1, 'Not Set' 
from (select 1) a
where not exists ( select 'x' from dim_keycustomer where dim_keycustomerid = 1);



insert into dim_keycustomer
(
dim_keycustomerid,
KeyCustomer,
Mediumdescription,
Longdescription,
ObjectVersion,
Changeflag,
KeyAccountIDLC,
Customer_global,
Location,
PROJECTSOURCEID
)
select 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1) + row_number() over(order by '') dim_keycustomerid,
ifnull(tz.BIC_Z_KEYCU,'Not Set') as KeyCustomer,
ifnull(tz.TXTMD,'Not Set') as Mediumdescription,
ifnull(tz.TXTLG,'Not Set') as Longdescription,
ifnull(pz.OBJVERS,'Not Set') as ObjectVersion,
ifnull(pz.CHANGED,'Not Set') as Changeflag,
ifnull(pz.BIC_Z_LC_KID,'Not Set') as KeyAccountIDLC,
ifnull(pz.BPARTNER,'Not Set') as Customer_global,
ifnull(pz.BIC_Z_LOCATIO,'Not Set') as Location,
1 as PROJECTSOURCEID
from BIC_TZ_KEYCU tz inner join  BIC_PZ_KEYCU pz
on tz.BIC_Z_KEYCU = pz.BIC_Z_KEYCU ;