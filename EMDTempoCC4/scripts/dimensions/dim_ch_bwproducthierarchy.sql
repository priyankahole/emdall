/**************************************************************************************************************** */
/*   Script         : bi_populate_ch_bwproducthierarchy                                                           */
/*   Author         : Veronica P                                                                                  */
/*   Created On     : 28 Dec 2017                                                                                 */
/*   Description    : Populating script of dim_ch_bwproducthierarchy.                                             */
/**************************************************************************************************************** */


/* Create tmp Upper Hierarchy table */

drop table if exists tmp_ch_bw_upperhierarchy1;
create table tmp_ch_bw_upperhierarchy1
as
select
	 h1.hz_uphier_nodename h1_hz_uphier_nodename
	,ifnull(t1.txtlg,'Not Set') t1_txtlg
	,h2.hz_uphier_nodename h2_hz_uphier_nodename
	,ifnull(t2.txtlg,'Not Set') t2_txtlg
	,h3.hz_uphier_nodename h3_hz_uphier_nodename
	,ifnull(t3.txtlg,'Not Set') t3_txtlg
	,h4.hz_uphier_nodename h4_hz_uphier_nodename
	,ifnull(t4.txtlg,'Not Set') t4_txtlg
	,h5.hz_uphier_nodename h5_hz_uphier_nodename
	,ifnull(t5.txtlg,'Not Set') t5_txtlg
	,h6.hz_uphier_nodename h6_hz_uphier_nodename
	,ifnull(t6.txtlg,'Upper Product Hierarchy ' || left(rshiedir_datefrom,4)) t6_txtlg
	,right(h1.hz_uphier_nodename,3) upperhiercode
	,to_date(r.rshiedir_datefrom,'YYYYMMDD') upperhierstartdate
	,to_date(r.rshiedir_dateto,'YYYYMMDD') upperhierenddate
	,r.rshiedir_hieid upperhierid
from RSHIEDIR r
	inner join HZ_UPHIER h1 on h1.hz_uphier_hieid = r.rshiedir_hieid and h1.hz_uphier_tlevel = 6
	inner join HZ_UPHIER h2 on h2.hz_uphier_nodeid = h1.hz_uphier_parentid and h2.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h3 on h3.hz_uphier_nodeid = h2.hz_uphier_parentid and h3.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h4 on h4.hz_uphier_nodeid = h3.hz_uphier_parentid and h4.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h5 on h5.hz_uphier_nodeid = h4.hz_uphier_parentid and h5.hz_uphier_hieid = r.rshiedir_hieid
	inner join HZ_UPHIER h6 on h6.hz_uphier_nodeid = h5.hz_uphier_parentid and h6.hz_uphier_hieid = r.rshiedir_hieid
	left join BIC_TZ_UPHIER t1 on h1.hz_uphier_nodename = t1.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t1.datefrom and t1.dateto
	left join BIC_TZ_UPHIER t2 on h2.hz_uphier_nodename = t2.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t2.datefrom and t2.dateto
	left join BIC_TZ_UPHIER t3 on h3.hz_uphier_nodename = t3.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t3.datefrom and t3.dateto
	left join BIC_TZ_UPHIER t4 on h4.hz_uphier_nodename = t4.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t4.datefrom and t4.dateto
	left join BIC_TZ_UPHIER t5 on h5.hz_uphier_nodename = t5.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t5.datefrom and t5.dateto
	left join BIC_TZ_UPHIER t6 on h6.hz_uphier_nodename = t6.bic_z_uphier and to_date(r.rshiedir_datefrom,'YYYYMMDD') between t6.datefrom and t6.dateto
where r.rshiedir_iobjnm = 'Z_UPHIER' and r.rshiedir_hienm = 'ORGUPH01' and r.rshiedir_version = 'A';

/* Populate Product Hierarchy dimension */

DELETE FROM number_fountain m
WHERE m.table_name = 'dim_ch_bwproducthierarchy';

INSERT INTO number_fountain
SELECT 'dim_ch_bwproducthierarchy',
       IFNULL(MAX(bw.dim_ch_bwproducthierarchyid), IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_ch_bwproducthierarchy bw
WHERE bw.dim_ch_bwproducthierarchyid <> 1;

DROP TABLE IF EXISTS tmp_insert_dim_ch;
CREATE TABLE tmp_insert_dim_ch
AS
SELECT DISTINCT
	1 dim_ch_bwproducthierarchyid
	,IFNULL(b.h6_hz_uphier_nodename,'Not Set') version
	,IFNULL(b.t6_txtlg,'Not Set') versiondesc
	,IFNULL(b.h5_hz_uphier_nodename,'Not Set')  businesssector
	,IFNULL(b.t5_txtlg,'Not Set') businesssectordesc
	,IFNULL(b.h4_hz_uphier_nodename,'Not Set') business
	,IFNULL(b.t4_txtlg,'Not Set') businessdesc
	,IFNULL(b.h3_hz_uphier_nodename,'Not Set') businessunit
	,IFNULL(b.t3_txtlg,'Not Set') businessunitdesc
	,IFNULL(b.h2_hz_uphier_nodename,'Not Set') businessfield
	,IFNULL(b.t2_txtlg,'Not Set') businessfielddesc
	,IFNULL(b.h1_hz_uphier_nodename,'Not Set') businessline
	,IFNULL(b.t1_txtlg,'Not Set') businesslinedesc
	,IFNULL(b.upperhiercode,'Not Set') upperhierarchycode
	,IFNULL(b.upperhierstartdate,'0001-01-01') upperhierstartdate
	,IFNULL(b.upperhierenddate,'9999-01-01') upperhierenddate
	,IFNULL(b.upperhierid,'Not Set') upperhierid
FROM tmp_ch_bw_upperhierarchy1 b;

truncate table dim_ch_bwproducthierarchy;
INSERT INTO dim_ch_bwproducthierarchy
(
	dim_ch_bwproducthierarchyid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,upperhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
)
SELECT
	(SELECT IFNULL(m.max_id, 1)
	FROM number_fountain m
	WHERE m.table_name = 'dim_ch_bwproducthierarchy') + ROW_NUMBER() OVER(ORDER BY '') AS dim_ch_bwproducthierarchyid
	,version
	,versiondesc
	,businesssector
	,businesssectordesc
	,business
	,businessdesc
	,businessunit
	,businessunitdesc
	,businessfield
	,businessfielddesc
	,businessline
	,businesslinedesc
	,upperhierarchycode
	,upperhierstartdate
	,upperhierenddate
	,upperhierid
FROM tmp_insert_dim_ch t;
