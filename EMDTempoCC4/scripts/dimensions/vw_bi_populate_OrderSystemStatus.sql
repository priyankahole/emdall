

Drop table if exists subqry_po_002;

Create table subqry_po_002 as 
Select distinct j1.JEST_OBJNR AS dd_ObjectNumber,
	convert(varchar(20),'Not Set') Closed,
	convert(varchar(20),'Not Set') GoodsMovementPosted,
	convert(varchar(20),'Not Set') TechnicallyCompleted,
	convert(varchar(20),'Not Set') Created,
	convert(varchar(20),'Not Set') Released,
	convert(varchar(20),'Not Set') PartPrinted,
	convert(varchar(20),'Not Set') PartiallyConfirmed,
	convert(varchar(20),'Not Set') confirmed,
	convert(varchar(20),'Not Set') Printed
FROM PM_JEST_AUFK j1;

Update subqry_po_002 k
Set k.Closed = 'X'
From PM_JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0046' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.GoodsMovementPosted = 'X'
From PM_JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0321' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.TechnicallyCompleted = 'X'
From PM_JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0045' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.Created = 'X'
From PM_JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0001' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.Released = 'X'
From PM_JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0002' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.PartPrinted = 'X'
From PM_JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0008' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.PartiallyConfirmed = 'X'
From PM_JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0010' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.confirmed = 'X'
From PM_JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0009' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.Printed = 'X'
From PM_JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0007' AND j.JEST_INACT is NULL;

/*Add to dimension the missing combinations only, not all of the possible combinations of X and Not Set */
delete from number_fountain m where m.table_name = 'dim_OrderSystemStatus';

insert into number_fountain
select  'dim_OrderSystemStatus',
        ifnull(max(d.dim_OrderSystemStatusid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_OrderSystemStatus d
where d.dim_OrderSystemStatusid <> 1;

insert into dim_OrderSystemStatus
(
	dim_OrderSystemStatusid,
	Closed,
	GoodsMovementPosted,
	TechnicallyCompleted,
	Created,
	Released,
	PartPrinted,
	PartiallyConfirmed,
	confirmed,
	Printed
)
select
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_OrderSystemStatus') + row_number() over(order by '') as dim_OrderSystemStatusid, 
	t.* 
	from 
		( select distinct
					Closed,
					GoodsMovementPosted,
					TechnicallyCompleted,
					Created,
					Released,
					PartPrinted,
					PartiallyConfirmed,
					confirmed,
					Printed
			from subqry_po_002 sq
			where not exists	
				( select 1 from dim_OrderSystemStatus post
				  where  post.Closed = sq.Closed
					   AND post.GoodsMovementPosted = sq.GoodsMovementPosted
					   AND post.TechnicallyCompleted = sq.TechnicallyCompleted
					   AND post.Created = sq.Created
					   AND post.Released = sq.Released
					   AND post.PartPrinted = sq.PartPrinted
					   AND post.PartiallyConfirmed = sq.PartiallyConfirmed
					   AND post.confirmed = sq.confirmed
					   AND post.Printed = sq.Printed)
		) t;
