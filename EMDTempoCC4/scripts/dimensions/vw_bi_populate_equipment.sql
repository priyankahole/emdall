
INSERT INTO dim_equipment(
	dim_equipmentid,
	EquipmentNo,
	EquipmentValidtoDate,
	EquipUsagePeriods,
	TechObjectAuthGroup,
	EquipmentVendor,
	TechnicalObjType,
	EquipmentCategory,
	EquipmentManufacturer,
	EquipmentStartUpDate,
	ManufacturerSerialNo,
	CatalogProfile,
	TechIdentificationNo,
	EquipmentDesc,
	MaterialNo,
	RowStartDate,
	RowIsCurrent)
SELECT    
	1,
	'Not Set',
	'0001-01-01',
	0, 			 
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'0001-01-01',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	current_timestamp,
	1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_equipment WHERE dim_equipmentId = 1);  


/* 06Jun2016 Update Existnig Records - EQUZ fields */
update dim_equipment d
set CatalogProfile = ifnull(z.equz_rbnr,'Not Set') 
	,dw_update_date = current_timestamp
from dim_equipment d, pm_EQUZ z
where d.EquipmentNo = ifnull(z.equz_equnr ,'Not Set')
	and d.EquipmentValidtoDate = ifnull(z.equz_datbi,'0001-01-01')
	and  d.EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
	and CatalogProfile <> ifnull(z.equz_rbnr,'Not Set') ;

update dim_equipment d
set  TechIdentificationNo = ifnull(z.equz_tidnr,'Not Set') 
	,dw_update_date = current_timestamp
from dim_equipment d, pm_EQUZ z
where d.EquipmentNo = ifnull(z.equz_equnr ,'Not Set')
	and d.EquipmentValidtoDate = ifnull(z.equz_datbi,'0001-01-01')
	and  d.EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
	and TechIdentificationNo <> ifnull(z.equz_tidnr,'Not Set') ;


/* Update Existing Records - EQUI fields */
update dim_equipment d
set 
	TechObjectAuthGroup = ifnull(equi_begru,'Not Set') ,
	dw_update_date = current_timestamp
from dim_equipment d, pm_equi e
where d.EquipmentNo = ifnull(e.equi_equnr,'Not Set') ;

update dim_equipment d
set 
	EquipmentVendor = ifnull(equi_elief,'Not Set') ,
	dw_update_date = current_timestamp
from dim_equipment d, pm_equi e
where d.EquipmentNo = ifnull(e.equi_equnr,'Not Set') 
and EquipmentVendor <> ifnull(equi_elief,'Not Set') ;

update dim_equipment d
set 
	TechnicalObjType = ifnull(equi_eqart,'Not Set') ,
	dw_update_date = current_timestamp
from dim_equipment d, pm_equi e
where d.EquipmentNo = ifnull(e.equi_equnr,'Not Set') 
and TechnicalObjType <> ifnull(equi_eqart,'Not Set') ;

update dim_equipment d
set 
	EquipmentCategory = ifnull(equi_eqtyp,'Not Set') ,
	dw_update_date = current_timestamp
from dim_equipment d, pm_equi e
where d.EquipmentNo = ifnull(e.equi_equnr,'Not Set') 
and EquipmentCategory <> ifnull(equi_eqtyp,'Not Set') ;

update dim_equipment d
set 
	EquipmentManufacturer = ifnull(equi_herst,'Not Set') ,
	dw_update_date = current_timestamp
from dim_equipment d, pm_equi e
where d.EquipmentNo = ifnull(e.equi_equnr,'Not Set') 
and EquipmentManufacturer <> ifnull(equi_herst,'Not Set') ;

update dim_equipment d
set 
	EquipmentStartUpDate = ifnull(equi_inbdt,'0001-01-01') ,
	dw_update_date = current_timestamp
from dim_equipment d, pm_equi e
where d.EquipmentNo = ifnull(e.equi_equnr,'Not Set') 
and EquipmentStartUpDate <> ifnull(equi_inbdt,'0001-01-01');

update dim_equipment d
set 
	ManufacturerSerialNo = ifnull(equi_serge,'Not Set') ,
	dw_update_date = current_timestamp
from dim_equipment d, pm_equi e
where d.EquipmentNo = ifnull(e.equi_equnr,'Not Set') 
and ManufacturerSerialNo <> ifnull(equi_serge,'Not Set');
/* End of Changes 06Jun2016 */


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_equipment';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_equipment',
		ifnull(max(d.dim_equipmentId),
		ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_equipment d
where d.dim_equipmentId <> 1;


INSERT INTO dim_equipment(
		dim_equipmentid,
		EquipmentNo,
		EquipmentValidtoDate,
		EquipUsagePeriods,
		TechObjectAuthGroup,
		EquipmentVendor,
		TechnicalObjType,
		EquipmentCategory,
		EquipmentManufacturer,
		EquipmentStartUpDate,
		ManufacturerSerialNo,
		CatalogProfile,
		TechIdentificationNo,
		-- EquipmentDesc,
		MaterialNo,
		RowStartDate,
		RowIsCurrent )
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_equipment') + row_number() over(order by '')  as dim_equipmentid,
		ifnull(equi_equnr,'Not Set') as EquipmentNo,
		ifnull(equz_datbi,'0001-01-01') as EquipmentValidtoDate,
		ifnull(equz_eqlfn,0)		as EquipUsagePeriods,
		ifnull(equi_begru,'Not Set') as TechObjectAuthGroup,
		ifnull(equi_elief,'Not Set') as EquipmentVendor,
		ifnull(equi_eqart,'Not Set') as TechnicalObjType,
		ifnull(equi_eqtyp,'Not Set') as EquipmentCategory,
		ifnull(equi_herst,'Not Set') as EquipmentManufacturer,
		ifnull(equi_inbdt,'0001-01-01') as EquipmentStartUpDate,
		ifnull(equi_serge,'Not Set') as ManufacturerSerialNo,
		ifnull(equz_rbnr,'Not Set') as CatalogProfile,
		ifnull(equz_tidnr,'Not Set') as TechIdentificationNo,
		-- ifnull(EQKT_EQKTX,'Not Set') as EquipmentDesc,
		ifnull(EQUI_MATNR,'Not Set') as MaterialNo,
		current_timestamp,
		1
FROM pm_EQUI
LEFT JOIN pm_EQUZ ON equi_equnr = equz_equnr  /* if there are no changes on an equipment there won't be an entry in EQUZ */
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_equipment
        WHERE EquipmentNo = ifnull(equi_equnr ,'Not Set')
			AND EquipmentValidtoDate = ifnull(equz_datbi,'0001-01-01')
			AND EquipUsagePeriods = ifnull(equz_eqlfn,0));
		
update dim_equipment a
set a.equipmentvalidfromdate = ifnull(b.EQUZ_DATAB,'0001-01-01')
	,dw_update_date = current_timestamp
from dim_equipment a,pm_EQUZ b
where a.EquipmentNo = ifnull(b.equz_equnr ,'Not Set')
	and a.EquipmentValidtoDate = ifnull(b.equz_datbi,'0001-01-01')
	and  a.EquipUsagePeriods = ifnull(b.equz_eqlfn,0)
	and a.equipmentvalidfromdate <> ifnull(b.EQUZ_DATAB,'0001-01-01');		
	
update dim_equipment d
set  EquipmentDesc = ifnull(EQKT_EQKTX,'Not Set')
	,dw_update_date = current_timestamp
from dim_equipment d,pm_eqkt e
where  EquipmentNo = EQKT_EQUNR
	and EQKT_SPRAS = 'E'
	and EquipmentDesc <> ifnull(EQKT_EQKTX,'Not Set'); 	

/*06May2016 - conside N/NL language text as well */
update dim_equipment d
set  EquipmentDesc = ifnull(EQKT_EQKTX,'Not Set')
	,dw_update_date = current_timestamp
from pm_eqkt e , dim_equipment d
where  EquipmentNo = EQKT_EQUNR
	and EQKT_SPRAS <> 'E'
	and EquipmentDesc = 'Not Set' /* Could not find E text */
	and EquipmentDesc <> ifnull(EQKT_EQKTX,'Not Set');
/* End of Changes 06May2016 */

/* 
02May2016 - Equipment Model No 
Get CUOBJ from INOB for a given Equipment # OBJEK. 
Get AUSP_ATWRT using AUSP_OBJEK = INOB_CUOBJ AND KLART = '002' AND ATINN = '0000001391'
*/
update dim_equipment d
set d.modelno = ifnull(ausp_atwrt,'Not Set')
	,dw_update_date = current_timestamp
from pm_equi e ,
		( select ausp_objek ,max(ausp_atwrt) ausp_atwrt from  pm_AUSP_model group by ausp_objek), 
		pm_inob, dim_equipment d
where d.EquipmentNo = ifnull(equi_equnr,'Not Set') 
	and  equi_equnr = inob_objek
	and inob_cuobj = ausp_objek
	and inob_klart = '002'
	and d.EquipmentNo <> ifnull(ausp_atwrt,'Not Set');
/*End of Changes 02May2016 */

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_equipment';        
