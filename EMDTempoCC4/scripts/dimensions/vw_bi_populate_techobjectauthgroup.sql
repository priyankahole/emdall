INSERT INTO dim_techobjectauthgroup(dim_techobjectauthgroupid, RowIsCurrent,techobjectauthgroup,techobjectauthgroupdesc,
rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_techobjectauthgroup
               WHERE dim_techobjectauthgroupid = 1);

delete from number_fountain m where m.table_name = 'dim_techobjectauthgroup';
   
insert into number_fountain
select 	'dim_techobjectauthgroup',
	ifnull(max(d.dim_techobjectauthgroupid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_techobjectauthgroup d
where d.dim_techobjectauthgroupid <> 1; 

INSERT INTO dim_techobjectauthgroup(dim_techobjectauthgroupid,
                                     techobjectauthgroup,
									 techobjectauthgroupdesc,
                                     RowStartDate,
                                     RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_techobjectauthgroup') 
          + row_number() over(order by ''),
			 T370B_T_BEGRU,
          T370B_T_BEGTX,
          current_timestamp,
          1
     FROM PM_T370B_T t1
    WHERE  NOT EXISTS
                (SELECT 1
                   FROM dim_techobjectauthgroup s
                  WHERE     s.techobjectauthgroup = t1.T370B_T_BEGRU
                        AND s.RowIsCurrent = 1)
;


UPDATE       dim_techobjectauthgroup s
          
   SET s.techobjectauthgroupdesc = T370B_T_BEGTX,
			s.dw_update_date = current_timestamp
 FROM dim_techobjectauthgroup s, PM_T370B_T t1
 WHERE s.RowIsCurrent = 1
 AND  s.techobjectauthgroup = t1.T370B_T_BEGRU
 AND s.techobjectauthgroupdesc <> ifnull(T370B_T_BEGTX,'Not Set');