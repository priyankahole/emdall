
DELETE FROM dim_statusobjectinformation;

INSERT INTO dim_statusobjectinformation(
	dim_statusobjectinformationid,
	client,
	objectnumber,
	objectcategory,
	statusprofile,
	indicatorchangedocumentsactive,
	changenumber,	
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	1,
	'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    0,
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1)
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_statusobjectinformation WHERE dim_statusobjectinformationid = 1);

DELETE FROM number_fountain m WHERE m.table_name = 'dim_statusobjectinformation';
   
INSERT INTO number_fountain
SELECT 	
	'dim_statusobjectinformation',
	IFNULL(MAX(d.dim_statusobjectinformationid), 
	IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_statusobjectinformation d
WHERE d.dim_statusobjectinformationid <> 1;


INSERT INTO dim_statusobjectinformation(
	dim_statusobjectinformationid,
	client,
	objectnumber,
	objectcategory,
	statusprofile,
	indicatorchangedocumentsactive,
	changenumber,	
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	(SELECT ifnull(m.max_id, 1) FROM number_fountain m WHERE m.table_name = 'dim_statusobjectinformation') 
		+ ROW_NUMBER() OVER(ORDER BY ''),
	IFNULL(JSTO_MANDT,'Not Set'),
    IFNULL(JSTO_OBJNR,'Not Set'),
    IFNULL(JSTO_OBTYP,'Not Set'),
    IFNULL(JSTO_STSMA,'Not Set'),
    IFNULL(JSTO_CHGKZ,'Not Set'),
    IFNULL(JSTO_CHGNR,0),
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1)
FROM pma_jsto;