

INSERT INTO dim_plannergroup(
	dim_plannergroupid,
	MaintPlanningPlant,
	CustomerServiceGroup,
	MaintPlannerGroup,
	TelephoneNumber,
	OrderType,
	RowStartDate,
	RowIsCurrent
)
  SELECT    
  	1,
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
    	current_timestamp,
    	1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_plannergroup WHERE dim_plannergroupId = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_plannergroup';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_plannergroup',
                ifnull(max(d.dim_plannergroupId),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_plannergroup d
where d.dim_plannergroupId <> 1;


INSERT INTO dim_plannergroup(
	dim_plannergroupid,
	MaintPlanningPlant,
	CustomerServiceGroup,
	MaintPlannerGroup,
	TelephoneNumber,
	OrderType,
	RowStartDate,
	RowIsCurrent
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_plannergroup') + row_number() over(order by '')  as dim_plannergroupid,
	ifnull(T024I_IWERK,'Not Set') as MaintPlanningPlant ,
	ifnull(T024I_INGRP,'Not Set') as CustomerServiceGroup,
	ifnull(T024I_INNAM,'Not Set') as MaintPlannerGroup, 
	ifnull(T024I_INTEL,'Not Set') as TelephoneNumber,
	ifnull(T024I_AUART_WP,'Not Set') as OrderType,
    	current_timestamp,
    	1
FROM PM_T024I
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_plannergroup
        WHERE MaintPlanningPlant = ifnull(T024I_IWERK,'Not Set')
        AND CustomerServiceGroup = ifnull(T024I_INGRP,'Not Set')
     );
	
DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_plannergroup'; 

