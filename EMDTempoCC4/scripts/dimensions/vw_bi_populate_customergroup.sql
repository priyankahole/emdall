
INSERT INTO dim_customergroup(dim_customergroupid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customergroup
               WHERE dim_customergroupid = 1);

UPDATE    dim_customergroup cg
   SET cg.Description = ifnull(T151T_KTEXT, 'Not Set'),
			cg.dw_update_date = current_timestamp
	 FROM
          dim_customergroup cg, T151T t
 WHERE cg.RowIsCurrent = 1
      AND t.T151T_KDGRP = cg.CustomerGroup;

delete from number_fountain m where m.table_name = 'dim_customergroup';

insert into number_fountain
select 	'dim_customergroup',
	ifnull(max(d.dim_customergroupid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customergroup d
where d.dim_customergroupid <> 1;

INSERT INTO dim_customergroup(Dim_CustomerGroupid,
                              LanguageKey,
                              CustomerGroup,
                              Description,
                              RowIsCurrent,
                              RowStartDate)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customergroup')
          + row_number() over(order by ''),
                        T151T_SPRAS,
          T151T_KDGRP,
          ifnull(T151T_KTEXT, 'Not Set'),
          1,
          current_timestamp
     FROM T151T
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customergroup a
               WHERE a.CustomerGroup = T151T_KDGRP);

delete from number_fountain m where m.table_name = 'dim_customergroup';
		  