/******************************************************************************************************************/
/*   Script         : bi_populate_qualityusers_dim                                                                */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 22 Nov 2016                                                                                 */
/*   Description    : Populating script of dim_qualityusers                                                       */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   22 Nov 2016         Cristian T      1.0               Creating the script.                                   */
/******************************************************************************************************************/

INSERT INTO dim_qualityusers(
dim_qualityusersid
)
SELECT 1 as dim_qualityusersid
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_qualityusers WHERE dim_qualityusersid = 1);

DELETE FROM number_fountain m WHERE m.table_name = 'dim_qualityusers';

INSERT INTO number_fountain
SELECT 	'dim_qualityusers',
	    IFNULL(MAX(d.dim_qualityusersid),IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_qualityusers d
WHERE d.dim_qualityusersid <> 1;

INSERT INTO dim_qualityusers(
dim_qualityusersid,
merck_uid,
first_name,
last_name,
fn_ln,
ln_fn,
mail,
cmgnumber,
costcenter,
department_short_name,
department_long_name,
department,
temporary_worker_flag,
department_number,
employee_manager,
contact_person,
dw_insert_date,
dw_update_date,
projectsourceid,
dd_service,
dd_subservice,
dd_site,
dd_country,
dd_departmenttype,
rowiscurrent,
userid
)
SELECT (SELECT IFNULL(m.max_id, 1) FROM number_fountain m WHERE m.table_name = 'dim_qualityusers') + ROW_NUMBER() over(order by t.merck_uid) AS dim_qualityusersid,
       t.*
FROM (
SELECT DISTINCT ifnull(usr.merck_uid, 'Not Set') as merck_uid,
       ifnull(usr.first_name, 'Not Set') as first_name,
       ifnull(usr.last_name, 'Not Set') as last_name,
       ifnull(usr.first_name || ' ' || usr.last_name, 'Not Set') as fn_ln,
       ifnull(usr.last_name || ', ' || usr.first_name, 'Not Set') as ln_fn,
       ifnull(usr.mail, 'Not Set') as mail,
       ifnull(usr.cmgnumber, 'Not Set') as cmgnumber,
       ifnull(usr.costcenter, 'Not Set') as costcenter,
       ifnull(usr.department_short_name, 'Not Set') as department_short_name,
       ifnull(usr.department_long_name, 'Not Set') as department_long_name,
       ifnull(usr.department, 'Not Set') as department,
       ifnull(usr.temporary_worker_flag, 'Not Set') as temporary_worker_flag,
       ifnull(usr.department_number, 'Not Set') as department_number,
       ifnull(usr.employee_manager, 'Not Set') as employee_manager,
       ifnull(usr.contact_person, 'Not Set') as contact_person,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as projectsourceid,
       'Not Set' as dd_service,
       'Not Set' as dd_subservice,
       'Not Set' as dd_site,
       'Not Set' as dd_country,
       'Not Set' as dd_departmenttype,
       1 as rowiscurrent,
       ifnull(usr.userid, 'Not Set') as userid
FROM cdm_users usr
WHERE NOT EXISTS (SELECT 1
                  FROM dim_qualityusers dim
				  WHERE dim.merck_uid = usr.merck_uid)
     ) t;

UPDATE dim_qualityusers dim
SET dim.projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     dim_qualityusers dim;

UPDATE dim_qualityusers dim
SET dim.first_name = ifnull(usr.first_name, 'Not Set'),
    dim.last_name = ifnull(usr.last_name, 'Not Set'),
    dim.fn_ln = ifnull(usr.first_name || ' ' || usr.last_name, 'Not Set'),
    dim.ln_fn = ifnull(usr.last_name || ', ' || usr.first_name, 'Not Set'),
    dim.mail = ifnull(usr.mail, 'Not Set'),
    dim.cmgnumber = ifnull(usr.cmgnumber, 'Not Set'),
    dim.costcenter = ifnull(usr.costcenter, 'Not Set'),
    dim.department_short_name = ifnull(usr.department_short_name, 'Not Set'),
    dim.department_long_name = ifnull(usr.department_long_name, 'Not Set'),
    dim.department = ifnull(usr.department, 'Not Set'),
    dim.temporary_worker_flag = ifnull(usr.temporary_worker_flag, 'Not Set'),
    dim.department_number = ifnull(usr.department_number, 'Not Set'),
    dim.employee_manager = ifnull(usr.employee_manager, 'Not Set'),
    dim.contact_person = ifnull(usr.contact_person, 'Not Set'),
    dim.dw_update_date = current_timestamp,
    dim.userid = ifnull(usr.userid, 'Not Set')
FROM cdm_users usr,
     dim_qualityusers dim
WHERE dim.merck_uid = usr.merck_uid
and (usr.merck_uid<>'M108081' and usr.cmgnumber<>'1500');

/* Mask German Names */
UPDATE dim_qualityusers dim
SET dim.fn_ln = 'Anonymus'
WHERE dim.cmgnumber = '1500';

UPDATE dim_qualityusers dim
SET dim.ln_fn = 'Anonymus'
WHERE dim.cmgnumber = '1500';
/* Mask German Names */

UPDATE dim_qualityusers dim
SET dim.dd_service = ifnull(dpt.service, 'Not Set'),
    dim.dd_subservice = ifnull(dpt.sub_service, 'Not Set'),
    dim.dd_site = ifnull(dpt.site, 'Not Set'),
    dim.dd_country = ifnull(dpt.country, 'Not Set'),
    dim.dd_departmenttype = ifnull(dpt.department, 'Not Set')
FROM dptmap dpt,
     dim_qualityusers dim
WHERE dim.department_number = dpt.department_number;


DROP TABLE IF EXISTS tmp_multipleIDs;
CREATE TABLE tmp_multipleIDs
AS
SELECT fn_ln as fn_ln,
       max(dim_qualityusersid) as dim_qualityusersid
FROM dim_qualityusers
WHERE 1 = 1
      AND fn_ln <> 'Anonymus'
GROUP BY fn_ln
HAVING count(*) > 1;

UPDATE dim_qualityusers dim
SET dim.rowiscurrent = 0
FROM dim_qualityusers dim,
     tmp_multipleIDs tmp
WHERE dim.fn_ln = tmp.fn_ln
      AND dim.dim_qualityusersid <> tmp.dim_qualityusersid;

DROP TABLE IF EXISTS tmp_multipleIDs;
