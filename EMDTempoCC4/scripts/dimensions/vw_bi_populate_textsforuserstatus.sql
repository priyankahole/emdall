INSERT INTO dim_textsforuserstatus(
	dim_textsforuserstatusid,
	client,
	statusprofile,
	userstatus,
	languageKey,
	individualstatusobjectshortform,
	objectstatus,
	longtextflag,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	1,
	'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    'Not Set',
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    ifnull((select s.dim_projectsourceid from dim_projectsource s),1)
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_textsforuserstatus WHERE dim_textsforuserstatusid = 1);

delete from number_fountain m where m.table_name = 'dim_textsforuserstatus';
   
insert into number_fountain
select 	
	'dim_textsforuserstatus',
	ifnull(max(d.dim_textsforuserstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_textsforuserstatus d
where d.dim_textsforuserstatusid <> 1;


INSERT INTO dim_textsforuserstatus(
	dim_textsforuserstatusid,
	client,
	statusprofile,
	userstatus,
	languageKey,
	individualstatusobjectshortform,
	objectstatus,
	longtextflag,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_textsforuserstatus') 
		+ row_number() over(order by ''),
	ifnull(TJ30T_MANDT,'Not Set'),
    ifnull(TJ30T_STSMA,'Not Set'),
    ifnull(TJ30T_ESTAT,'Not Set'),
    ifnull(TJ30T_SPRAS,'Not Set'),
    ifnull(TJ30T_TXT04,'Not Set'),
    ifnull(TJ30T_TXT30,'Not Set'),
    ifnull(TJ30T_LTEXT,'Not Set'),
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    ifnull((select s.dim_projectsourceid from dim_projectsource s),1)
FROM pma_TJ30T;