UPDATE    dim_MRPElement mrpe
   SET mrpe.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			mrpe.dw_update_date = current_timestamp
FROM DD07T t, dim_MRPElement mrpe
 WHERE mrpe.RowIsCurrent = 1
     AND    t.DD07T_DOMNAME = 'DELKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND mrpe.MRPElement = t.DD07T_DOMVALUE
;

INSERT INTO dim_MRPElement(dim_MRPElementId, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MRPElement
               WHERE dim_MRPElementId = 1);

delete from number_fountain m where m.table_name = 'dim_MRPElement';
   
insert into number_fountain
select 	'dim_MRPElement',
	ifnull(max(d.Dim_MRPElementID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MRPElement d
where d.Dim_MRPElementID <> 1; 

INSERT INTO dim_MRPElement(Dim_MRPElementID,
								Description,
                                MRPElement,
                                RowStartDate,
                                RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MRPElement') 
          + row_number() over(order by '') ,
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'DELKZ' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_MRPElement
                    WHERE MRPElement = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'DELKZ')
   ORDER BY 2;
