INSERT INTO dim_mainworkcenter(
	dim_mainworkcenterid,
	client,
    objecttype,
    objectid,
    workcenter,
    plant,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	1,
	'Not Set',
    'Not Set',
    0,
    'Not Set',
    'Not Set',
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    ifnull((select s.dim_projectsourceid from dim_projectsource s),1)
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_mainworkcenter WHERE dim_mainworkcenterid = 1);

delete from number_fountain m where m.table_name = 'dim_mainworkcenter';
   
insert into number_fountain
select 	
	'dim_mainworkcenter',
	ifnull(max(d.dim_mainworkcenterid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_mainworkcenter d
where d.dim_mainworkcenterid <> 1;


INSERT INTO dim_mainworkcenter(
	dim_mainworkcenterid,
	client,
    objecttype,
    objectid,
    workcenter,
    plant,
    rowstartdate,
    rowenddate,
    rowiscurrent,
    dw_insert_date,
    dw_update_date,
    projectsourceid
)
SELECT    
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_equipmentcategory') 
		+ row_number() over(order by ''),
	crhd_mandt,
    crhd_objty,
    crhd_objid,
    crhd_arbpl,
    crhd_werks,
    '1900-01-01',
    '2099-01-01',
    1,
    current_timestamp,
    current_timestamp,
    ifnull((select s.dim_projectsourceid from dim_projectsource s),1)
FROM PM_CRHD;