DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'dim_customercreditlimit';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'dim_customercreditlimit', ifnull(max(dim_customercreditlimitid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM dim_customercreditlimit;

update dim_customercreditlimit d
  set d.customercreditlimit = ifnull(case when t.T014_WAERS = 'EUR' then KNKK_KLIMK else (KNKK_KLIMK * csvo.exrate) end,0)
from dim_customercreditlimit d
  inner join KNKK k on k.KNKK_KUNNR	= d.customernumber and k.KNKK_KKBER	= d.creditcontrolarea
  inner join  T014 t on k.KNKK_KKBER = t.T014_KKBER
  left join csv_oprate csvo on t.T014_WAERS = csvo.FROMC;

insert into dim_customercreditlimit(DIM_CUSTOMERCREDITLIMITID,CUSTOMERNUMBER,CREDITCONTROLAREA,CUSTOMERCREDITLIMIT)
select
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customercreditlimit') + row_number() over(order by '') as dim_customercreditlimitid ,
	KNKK_KUNNR	as customernumber,
	KNKK_KKBER	as creditcontrolarea,
	ifnull((case when t.T014_WAERS = 'EUR' then KNKK_KLIMK else (KNKK_KLIMK * csvo.exrate) end),0)	as customercreditlimit
from KNKK k
  inner join  T014 t on k.KNKK_KKBER = t.T014_KKBER
  left join csv_oprate csvo on t.T014_WAERS = csvo.FROMC
where not exists(select 1 from dim_customercreditlimit d, KNKK where KNKK_KUNNR = customernumber and KNKK_KKBER = creditcontrolarea);
