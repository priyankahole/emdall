
insert into dim_OrderuserstatusPMNotif
	(
	dim_OrderuserstatusPMNotifid,
    ReadyForPlanning,
	PlanningInProgress,
	PlanningComplete,
	WaitingForMaterials,
	Scheduled,
	"hold",
	Cancelled 
	)
select 1, 'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set'
from ( SELECT 1 )  AS t
where not exists ( select 1 from dim_OrderuserstatusPMNotif where dim_OrderuserstatusPMNotifid = 1);

delete from number_fountain m where m.table_name = 'dim_OrderuserstatusPMNotif';

insert into number_fountain
select  'dim_OrderuserstatusPMNotif',
        ifnull(max(d.dim_OrderuserstatusPMNotifid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_OrderuserstatusPMNotif d
where d.dim_OrderuserstatusPMNotifid <> 1;

insert into dim_OrderuserstatusPMNotif
(	
	dim_OrderuserstatusPMNotifid,
	ReadyForPlanning,
	PlanningInProgress,
	PlanningComplete,
	WaitingForMaterials,
	Scheduled,
	"hold",
	Cancelled
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_OrderuserstatusPMNotif') + row_number() over(order by '') as dim_OrderuserstatusPMNotifid, 
	t.* from (select distinct 
		ReadyForPlanning,
		PlanningInProgress,
		PlanningComplete,
		WaitingForMaterials,
		Scheduled,
		sHold,
		Cancelled 
		from tmp_dim_notificationuserstatus3 t3
		where not exists (select 1 from dim_OrderuserstatusPMNotif pm
		where
		pm.ReadyForPlanning=t3.ReadyForPlanning and
		pm.PlanningInProgress=t3.PlanningInProgress and
		pm.PlanningComplete =t3.PlanningComplete and
		pm.WaitingForMaterials =t3.WaitingForMaterials and
		pm.Scheduled =t3.Scheduled and
		pm."hold" = t3.sHold and
		pm.Cancelled=t3.Cancelled
		/*and pm.userstatus=dd_userstatus  */
		) )
		 t;