

INSERT INTO dim_functionallocation(
		dim_functionallocationid,
		FLOCDescription ,
		AcctAssignment,
		FunctonalLocation ,
		SuperiorFuncLocation ,
		FLOCABCIndicator ,
		PlantSection ,
		FLOCSortField ,
		FLOCCostCenter ,
		FLOCLocationDesc ,
		Room ,
		ProductionWorkCenter ,
		ProdWorkCenterID,
		ProdWorkCenterDesc,
		FLOCLocation ,
		FLOCPlanningPlant,
		objectNumber,
		TechObjectAuthGroup,
		RowStartDate,
		RowIsCurrent )
SELECT    
		1,
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set', 
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		current_timestamp,
		1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_functionallocation WHERE dim_functionallocationId = 1);  

/* 06Jun2016 Update existing records - IFLOT fields */
update dim_functionallocation d
set
	FLOCPlanningPlant = ifnull(IFLOT_IWERK,'Not Set') ,
	dw_update_date = current_timestamp 
from PM_IFLOT ft 
	inner join dim_functionallocation d on d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
where FLOCPlanningPlant <> ifnull(IFLOT_IWERK,'Not Set') ;

update dim_functionallocation d
set
	SuperiorFuncLocation  = ifnull(IFLOT_TPLMA,'Not Set') , 
	dw_update_date = current_timestamp 
from PM_IFLOT ft 
	inner join dim_functionallocation d on d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')		
where SuperiorFuncLocation  <> ifnull(IFLOT_TPLMA,'Not Set') ;

update dim_functionallocation d
set
	objectnumber = ifnull(IFLOT_OBJNR,'Not Set') , 
	dw_update_date = current_timestamp
from PM_IFLOT ft 
	inner join dim_functionallocation d on d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
where objectnumber <> ifnull(IFLOT_OBJNR,'Not Set') ;

update dim_functionallocation d
set
	TechObjectAuthGroup = ifnull(IFLOT_BEGRU,'Not Set') ,
	dw_update_date = current_timestamp
from PM_IFLOT ft 
	inner join dim_functionallocation d on d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
where TechObjectAuthGroup <> ifnull(IFLOT_BEGRU,'Not Set');

/* ILOA fields */
update dim_functionallocation d
set
	FLOCABCIndicator  = ifnull(ILOA_ABCKZ,'Not Set') ,
	dw_update_Date = current_timestamp
from PM_ILOA il
	inner join dim_functionallocation d on d.AcctAssignment = ifnull(il.ILOA_ILOAN,'Not Set')
where FLOCABCIndicator  <> ifnull(ILOA_ABCKZ,'Not Set') ;

update dim_functionallocation d
set
	FLOCCostCenter  = ifnull(ILOA_KOSTL,'Not Set') ,
dw_update_Date = current_timestamp
from PM_ILOA il
	inner join dim_functionallocation d on d.AcctAssignment = ifnull(il.ILOA_ILOAN,'Not Set')
where FLOCCostCenter  <> ifnull(ILOA_KOSTL,'Not Set') ;

update dim_functionallocation d
set
	FLOCLocation  = ifnull(ILOA_STORT,'Not Set') ,
	dw_update_Date = current_timestamp
from PM_ILOA il
	inner join dim_functionallocation d on d.AcctAssignment = ifnull(il.ILOA_ILOAN,'Not Set')
where FLOCLocation  <> ifnull(ILOA_STORT,'Not Set');

update dim_functionallocation d
set
	FLOCSortField  = ifnull(ILOA_EQFNR,'Not Set') ,
	dw_update_Date = current_timestamp
from PM_ILOA il
	inner join dim_functionallocation d on d.AcctAssignment = ifnull(il.ILOA_ILOAN,'Not Set')
where FLOCSortField  <> ifnull(ILOA_EQFNR,'Not Set') ;
	
update dim_functionallocation d
set
	PlantSection  = ifnull(ILOA_BEBER,'Not Set') ,
	dw_update_Date = current_timestamp
from PM_ILOA il
	inner join dim_functionallocation d on d.AcctAssignment = ifnull(il.ILOA_ILOAN,'Not Set')
where PlantSection  <> ifnull(ILOA_BEBER,'Not Set');

update dim_functionallocation d
set
	ProdWorkCenterID = ifnull(convert(varchar(8),ILOA_PPSID),'Not Set') ,
	dw_update_Date = current_timestamp
from PM_ILOA il
	inner join dim_functionallocation d on d.AcctAssignment = ifnull(il.ILOA_ILOAN,'Not Set')
where ProdWorkCenterID <> ifnull(convert(varchar(8),ILOA_PPSID),'Not Set') ;	

update dim_functionallocation d
set
	Room  = ifnull(ILOA_MSGRP,'Not Set') ,
	dw_update_Date = current_timestamp
	from PM_ILOA il
	inner join dim_functionallocation d on d.AcctAssignment = ifnull(il.ILOA_ILOAN,'Not Set')
where Room  <> ifnull(ILOA_MSGRP,'Not Set');

/* End of Changes 06Jun2016 */

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_functionallocation';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_functionallocation',
                ifnull(max(d.dim_functionallocationId),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_functionallocation d
where d.dim_functionallocationId <> 1;


 INSERT INTO dim_functionallocation(
		dim_functionallocationid,
		AcctAssignment,
		FLOCABCIndicator ,
		FLOCCostCenter ,
		FLOCDescription ,
		FLOCLocation ,
		FLOCLocationDesc ,
		FLOCPlanningPlant,
		FLOCSortField ,
		FunctonalLocation ,
		PlantSection ,
		ProdWorkCenterID,
		Room ,
		SuperiorFuncLocation ,
		objectnumber,
		TechObjectAuthGroup,
		RowStartDate,
		RowIsCurrent
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_functionallocation') + row_number() over(order by '') as dim_functionallocationid,
		ifnull(ILOA_ILOAN,'Not Set') as AcctAssignment,
		ifnull(ILOA_ABCKZ,'Not Set') as FLOCABCIndicator ,
		ifnull(ILOA_KOSTL,'Not Set') as FLOCCostCenter ,
		'Not Set' as FLOCDescription ,/* 06May2016 - First consider E lang texts */
		ifnull(ILOA_STORT,'Not Set') as FLOCLocation ,
		'Not Set' as FLOCLocationDesc, /* 06May2016 */
		ifnull(IFLOT_IWERK,'Not Set') as FLOCPlanningPlant,
		ifnull(ILOA_EQFNR,'Not Set') as FLOCSortField ,
		ifnull(IFLOT_TPLNR,'Not Set') as FunctonalLocation ,
		ifnull(ILOA_BEBER,'Not Set') as PlantSection ,
		ifnull(convert(varchar(8),ILOA_PPSID),'Not Set') as ProdWorkCenterID,
		ifnull(ILOA_MSGRP,'Not Set') as Room ,
		ifnull(IFLOT_TPLMA,'Not Set') as SuperiorFuncLocation ,
		ifnull(IFLOT_OBJNR,'Not Set') as objectnumber,
		ifnull(IFLOT_BEGRU,'Not Set') as TechObjectAuthGroup,
		current_timestamp,
		1
FROM PM_IFLOT ft, PM_ILOA il
where ft.iflot_tplnr = il.ILOA_TPLNR
	and not exists (select 1 from dim_functionallocation d1
	where d1.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
	and d1.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set') );
	

update dim_functionallocation d
set d.FLOCDescription = ifnull(fx.IFLOTX_PLTXU,'Not Set')
	,dw_update_date = current_timestamp
from dim_functionallocation d,PM_IFLOT ft, PM_ILOA il,PM_IFLOTX fx
where ft.iflot_tplnr = il.ILOA_TPLNR
	and d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
	and d.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
	and ft.iflot_tplnr = fx.IFLOTX_tplnr 
	and fx.IFLOTX_SPRAS= 'E'
	and d.FLOCDescription <> ifnull(fx.IFLOTX_PLTXU,'Not Set');
	
update dim_functionallocation d
set d.FLOCLocationDesc = ifnull(t.T499S_KTEXT,'Not Set')
	,dw_update_date = current_timestamp
from dim_functionallocation d,PM_IFLOT ft, PM_ILOA il,PM_T499S t
where ft.iflot_tplnr = il.ILOA_TPLNR
	and d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
	and d.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
	and t.T499S_STAND = ILOA_STORT 
	and t.T499S_WERKS = IFLOT_IWERK
	and d.FLOCLocationDesc <> ifnull(t.T499S_KTEXT,'Not Set');	
	
				
/* 28Apr2016 - Logic to Populate the stats for functionallocation */
drop table if exists tmp_objstatus;

create table tmp_objstatus as
	select distinct objectnumber, t.jest_stat
	from dim_functionallocation f,PM_jest_iflot t
	where f.objectnumber = t.jest_objnr
		and jest_inact is null;

drop table if exists tmp_objlistagg;
	create table tmp_objlistagg as
	SELECT objectnumber,GROUP_CONCAT(jest_stat ORDER BY jest_stat) as stats
	FROM tmp_objstatus
	GROUP BY objectnumber;

	
update dim_functionallocation d
set 	
	flgsafetysiteprotection = case when t.stats like '%E0001%' then 'X' else 'Not Set' end 
	,flgenvprotection = case when t.stats like '%E0002%' then 'X' else 'Not Set' end 
	,flgcgxprelated = case when t.stats like '%E0003%' then 'X' else 'Not Set' end 
	,flgrepairasrequired = case when t.stats like '%E0004%' then 'X' else 'Not Set' end 
	,flgruntofailure = case when t.stats like '%E0005%' then 'X' else 'Not Set' end 
	,flgcgxpcritical = case when t.stats like '%E0006%' then 'X' else 'Not Set' end 
	,dw_update_date = current_timestamp
from tmp_objlistagg t 
	inner join dim_functionallocation d ON t.objectnumber =  d.objectnumber;

drop table if exists tmp_objstatus;
drop table if exists tmp_objlistagg;
/* End of changes 28Arp2016*/

/* 06May2016 - Update FLOC desc if description is not available in E */
update dim_functionallocation d
set d.FLOCDescription = ifnull(fx.IFLOTX_PLTXU,'Not Set' )
	,dw_update_date = current_timestamp
from PM_IFLOTX fx 
	inner join dim_functionallocation d ON  d.FunctonalLocation = fx.IFLOTX_tplnr 
where 
	d.FLOCDescription = 'Not Set' /* could not find E descriptions */
	and fx.IFLOTX_SPRAS<> 'E'
	and d.FLOCDescription <> ifnull(fx.IFLOTX_PLTXU,'Not Set' );

/*Update WorkCenter Code and desc from Work center Dimension */
drop table if exists tmp_dimwworkcenter_dstnct;
	create table tmp_dimwworkcenter_dstnct
	as
	select distinct objectid, workcenter, ktext_description
	from dim_workcenter;

/* Work Center code */
update dim_functionallocation f
set f.ProductionWorkCenter = ifnull(w.workcenter,'Not Set'), dw_update_date = current_timestamp
from tmp_dimwworkcenter_dstnct w 
	inner join dim_functionallocation f on f.prodworkcenterid = w.objectid
where f.ProductionWorkCenter <> ifnull(w.workcenter,'Not Set');

/* Work Center Desc */
update dim_functionallocation f
set f.prodworkcenterdesc = ifnull(w.ktext_Description,'Not Set'), dw_update_date = current_timestamp
from tmp_dimwworkcenter_dstnct w 
	inner join dim_functionallocation f on f.prodworkcenterid = w.objectid
where f.prodworkcenterdesc <> ifnull(w.ktext_Description,'Not Set');

/* Andrian - 19May2016 - Update Superior Floc Description */
update dim_functionallocation d
set d.superiroflocdesc = ifnull(fx.IFLOTX_PLTXU,'Not Set' )
	,dw_update_date = current_timestamp
from PM_IFLOTX fx 
	inner join dim_functionallocation d on d.superiorfunclocation = fx.IFLOTX_tplnr
where d.superiroflocdesc <> ifnull(fx.IFLOTX_PLTXU,'Not Set' );

drop table if exists tmp_dimwworkcenter_dstnct;

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_functionallocation';        

