
insert into Dim_BillingDocumentType(Dim_BillingDocumentTypeid,Description,Type,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason)
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from Dim_BillingDocumentType where Dim_BillingDocumentTypeid = 1);
               
UPDATE    dim_billingdocumenttype bdt
		SET bdt.Description = ifnull(t.TVFKT_VTEXT,'Not Set'),
			bdt.dw_update_date = current_timestamp
	    FROM
          dim_billingdocumenttype bdt, tvfkt t
    WHERE t.TVFKT_FKART = bdt.Type
   AND bdt.RowIsCurrent = 1;               

delete from number_fountain m where m.table_name = 'Dim_BillingDocumentType';

insert into number_fountain
select 	'Dim_BillingDocumentType',
	ifnull(max(d.Dim_BillingDocumentTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_BillingDocumentType d
where d.Dim_BillingDocumentTypeid <> 1;
   
INSERT INTO dim_billingdocumenttype(Dim_BillingDocumentTypeid,
						Type,
                        Description,
                        RowStartDate,
                        RowIsCurrent) 
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_BillingDocumentType') 
          + row_number() over(order by '') ,
		  TVFKT_FKART,
          ifnull(t.TVFKT_VTEXT,'Not Set'),
          current_timestamp,
          1
     FROM tvfkt t 
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_billingdocumenttype
                       WHERE Type = t.TVFKT_FKART);

delete from number_fountain m where m.table_name = 'Dim_BillingDocumentType';
					   