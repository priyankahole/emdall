
DROP TABLE IF EXISTS tmp_getalllines_ilsest;
CREATE TABLE tmp_getalllines_ilsest
AS
SELECT 
  IFNULL(s_sampleid,'Not Set') AS dd_sampleid, 
  IFNULL(batchid,'Not Set') AS dd_batchid, 
  IFNULL(u_sapbatchid,'Not Set') AS dd_sapbatchid, 
  IFNULL(u_lotnumber,'Not Set') AS dd_lotnumber, 
  IFNULL(materialname,'Not Set') AS dd_materialname, 
  IFNULL(u_materialfamily,'Not Set') AS dd_materialfamily,
  IFNULL(paramlistid,'Not Set') AS dd_paramlistid,
  IFNULL(paramlistversionid,'Not Set') AS dd_paramlistversionid,
  IFNULL(u_operationname,'Not Set') AS dd_operationname,
  IFNULL(paramid,'Not Set') AS dd_paramid,
  IFNULL(CAST(transformvalue AS VARCHAR(100)),'Not Set') AS dd_transformvalue,
  IFNULL(transformdt,'Not Set') AS dd_transformdate,
  IFNULL(displayunits,'Not Set') AS dd_displayunits,
  IFNULL(limittypeid,'Not Set') AS dd_limittypeid,
  IFNULL(condition1,'Not Set') AS dd_condition1,
  IFNULL(operator1,'Not Set') AS dd_operator1,
  IFNULL(CAST(value1num AS VARCHAR(100)),'Not Set') AS dd_value1num,
  IFNULL(operator2,'Not Set') AS dd_operator2,
  IFNULL(CAST(value2num AS VARCHAR(100)),'Not Set') AS dd_value2num,
  CAST('Not Set' AS VARCHAR(100)) AS dd_loweroos,
  CAST('Not Set' AS VARCHAR(100)) AS dd_loweroot,
  CAST('Not Set' AS VARCHAR(100)) AS dd_upperoos,
  CAST('Not Set' AS VARCHAR(100)) AS dd_upperoot,
  CAST(-999 AS DECIMAL(18,4)) AS ct_loweroos,
  CAST(-999 AS DECIMAL(18,4)) AS ct_loweroot,
  CAST(-999 AS DECIMAL(18,4)) AS ct_upperoos,
  CAST(-999 AS DECIMAL(18,4)) AS ct_upperoot,
  IFNULL(transformvalue, 0) AS ct_transformvalue
FROM V_MCOCKPIT_SUIVIRESULTANA
WHERE UPPER(paramid) = 'DATE ANALYSE';

INSERT INTO tmp_getalllines_ilsest
(
  dd_sampleid, 
  dd_batchid, 
  dd_sapbatchid, 
  dd_lotnumber, 
  dd_materialname, 
  dd_materialfamily,
  dd_paramlistid,
  dd_paramlistversionid,
  dd_operationname,
  dd_paramid,
  dd_transformvalue,
  dd_transformdate,
  dd_displayunits,
  dd_limittypeid,
  dd_condition1,
  dd_operator1,
  dd_value1num,
  dd_operator2,
  dd_value2num,
  dd_loweroos,
  dd_loweroot,
  dd_upperoos,
  dd_upperoot,
  ct_loweroos,
  ct_loweroot,
  ct_upperoos,
  ct_upperoot,
  ct_transformvalue
)
SELECT
  IFNULL(s.s_sampleid,'Not Set'), 
  IFNULL(s.batchid,'Not Set'), 
  IFNULL(s.u_sapbatchid,'Not Set'), 
  IFNULL(s.u_lotnumber,'Not Set'), 
  IFNULL(s.materialname,'Not Set'), 
  IFNULL(s.u_materialfamily,'Not Set'),
  IFNULL(s.paramlistid,'Not Set'),
  IFNULL(s.paramlistversionid,'Not Set'),
  IFNULL(s.u_operationname,'Not Set'),
  IFNULL(s.paramid,'Not Set'),
  IFNULL(CAST(s.transformvalue AS VARCHAR(100)),'Not Set'),
  IFNULL(s.transformdt,'Not Set'),
  IFNULL(s.displayunits,'Not Set'),
  IFNULL(s.limittypeid,'Not Set'),
  IFNULL(s.condition1,'Not Set'),
  IFNULL(s.operator1,'Not Set'),
  IFNULL(CAST(s.value1num AS VARCHAR(100)),'Not Set'),
  IFNULL(s.operator2,'Not Set'),
  IFNULL(CAST(s.value2num AS VARCHAR(100)),'Not Set'),
  'Not Set',
  'Not Set',
  'Not Set',
  'Not Set',
  -999,
  -999,
  -999,
  -999,
  IFNULL(s.transformvalue, 0) AS ct_transformvalue
FROM
  v_mcockpit_suiviresultana AS s
WHERE UPPER(s.paramid) <> 'DATE ANALYSE'
  AND UPPER(s.limittypeid) = 'IN SPEC'
;

UPDATE tmp_getalllines_ilsest AS t
SET dd_loweroos = IFNULL(CAST(s.value1num AS VARCHAR(100)), 'Not Set'),
  ct_loweroos = IFNULL(s.value1num, -999)
FROM v_mcockpit_suiviresultana AS s, tmp_getalllines_ilsest AS t
WHERE s.u_sapbatchid = t.dd_sapbatchid
  AND s.u_lotnumber = t.dd_lotnumber
  AND s.materialname = t.dd_materialname
  AND s.u_materialfamily = t.dd_materialfamily
  AND s.u_operationname = t.dd_operationname
  AND s.paramid = t.dd_paramid
  AND UPPER(s.limittypeid) = 'LOWER OOS';
  
UPDATE tmp_getalllines_ilsest AS t
SET dd_loweroot = IFNULL(CAST(s.value1num AS VARCHAR(100)), 'Not Set'),
  ct_loweroot = IFNULL(s.value1num, -999)
FROM v_mcockpit_suiviresultana AS s, tmp_getalllines_ilsest AS t
WHERE s.u_sapbatchid = t.dd_sapbatchid
  AND s.u_lotnumber = t.dd_lotnumber
  AND s.materialname = t.dd_materialname
  AND s.u_materialfamily = t.dd_materialfamily
  AND s.u_operationname = t.dd_operationname
  AND s.paramid = t.dd_paramid
  AND UPPER(s.limittypeid) = 'LOWER OOT';
  
UPDATE tmp_getalllines_ilsest AS t
SET dd_upperoos = IFNULL(CAST(s.value1num AS VARCHAR(100)), 'Not Set'),
  ct_upperoos = IFNULL(s.value1num, -999)
FROM v_mcockpit_suiviresultana AS s, tmp_getalllines_ilsest AS t
WHERE s.u_sapbatchid = t.dd_sapbatchid
  AND s.u_lotnumber = t.dd_lotnumber
  AND s.materialname = t.dd_materialname
  AND s.u_materialfamily = t.dd_materialfamily
  AND s.u_operationname = t.dd_operationname
  AND s.paramid = t.dd_paramid
  AND UPPER(s.limittypeid) = 'UPPER OOS';
  
UPDATE tmp_getalllines_ilsest AS t
SET dd_upperoot = IFNULL(CAST(s.value1num AS VARCHAR(100)), 'Not Set'),
  ct_upperoot = IFNULL(s.value1num, -999)
FROM v_mcockpit_suiviresultana AS s, tmp_getalllines_ilsest AS t
WHERE s.u_sapbatchid = t.dd_sapbatchid
  AND s.u_lotnumber = t.dd_lotnumber
  AND s.materialname = t.dd_materialname
  AND s.u_materialfamily = t.dd_materialfamily
  AND s.u_operationname = t.dd_operationname
  AND s.paramid = t.dd_paramid
  AND UPPER(s.limittypeid) = 'UPPER OOT';


DROP TABLE IF EXISTS number_fountain_fact_inspectionlotsemoyoperation;
CREATE TABLE number_fountain_fact_inspectionlotsemoyoperation 
LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoyoperation;
CREATE TABLE tmp_fact_inspectionlotsemoyoperation 
LIKE fact_inspectionlotsemoyoperation INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_inspectionlotsemoyoperation 
WHERE table_name = 'fact_inspectionlotsemoyoperation';	

INSERT INTO number_fountain_fact_inspectionlotsemoyoperation
SELECT 'fact_inspectionlotsemoyoperation', IFNULL(MAX(f.fact_inspectionlotsemoyoperationid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_inspectionlotsemoyoperation AS f;

INSERT INTO tmp_fact_inspectionlotsemoyoperation(
  fact_inspectionlotsemoyoperationid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, 
  dd_batchid, 
  dd_sapbatchid, 
  dd_lotnumber, 
  dd_materialname, 
  dd_materialfamily,
  dd_paramlistid,
  dd_paramlistversionid,
  dd_operationname,
  dd_paramid,
  dd_transformvalue,
  dd_transformdate,
  dim_transformdateid,
  dd_displayunits,
  dd_limittypeid,
  dd_condition1,
  dd_operator1,
  dd_value1num,
  dd_operator2,
  dd_value2num,
  dd_loweroos,
  dd_loweroot,
  dd_upperoos,
  dd_upperoot,
  ct_loweroos,
  ct_loweroot,
  ct_upperoos,
  ct_upperoot,
  ct_transformvalue
)
SELECT
  (SELECT max_id
    FROM number_fountain_fact_inspectionlotsemoyoperation 
    WHERE table_name = 'fact_inspectionlotsemoyoperation') + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoyoperationid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  CURRENT_TIMESTAMP AS dw_insert_date, 
  CURRENT_TIMESTAMP AS dw_update_date,
  dd_sampleid, 
  dd_batchid, 
  dd_sapbatchid, 
  dd_lotnumber, 
  dd_materialname, 
  dd_materialfamily,
  dd_paramlistid,
  dd_paramlistversionid,
  dd_operationname,
  dd_paramid,
  dd_transformvalue,
  dd_transformdate,
  CAST(1 AS BIGINT) AS dim_transformdateid,
  dd_displayunits,
  dd_limittypeid,
  dd_condition1,
  dd_operator1,
  dd_value1num,
  dd_operator2,
  dd_value2num,
  dd_loweroos,
  dd_loweroot,
  dd_upperoos,
  dd_upperoot,
  ct_loweroos,
  ct_loweroot,
  ct_upperoos,
  ct_upperoot,
  ct_transformvalue
FROM
  tmp_getalllines_ilsest
;

UPDATE tmp_fact_inspectionlotsemoyoperation AS f
SET f.dim_transformdateid = dd.dim_dateid
FROM tmp_fact_inspectionlotsemoyoperation AS f, dim_date AS dd 
WHERE LEFT(f.dd_transformdate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_transformdateid <> dd.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoyoperation AS f
SET f.dd_loweroos = f.dd_value1num,
  f.dd_upperoos = f.dd_value2num,
  f.ct_loweroos = CASE WHEN f.dd_value1num = 'Not Set' THEN -999 ELSE CAST(f.dd_value1num AS DECIMAL(18,4)) END,
  f.ct_upperoos = CASE WHEN f.dd_value2num = 'Not Set' THEN -999 ELSE CAST(f.dd_value2num AS DECIMAL(18,4)) END
WHERE f.dd_loweroos = 'Not Set' AND f.dd_upperoos = 'Not Set';

UPDATE tmp_fact_inspectionlotsemoyoperation
SET ct_loweroos = 0
WHERE ct_loweroos = -999;

UPDATE tmp_fact_inspectionlotsemoyoperation
SET ct_loweroot = 0
WHERE ct_loweroot = -999;

UPDATE tmp_fact_inspectionlotsemoyoperation
SET ct_upperoos = 0
WHERE ct_upperoos = -999;

UPDATE tmp_fact_inspectionlotsemoyoperation
SET ct_upperoot = 0
WHERE ct_upperoot = -999;

DELETE FROM fact_inspectionlotsemoyoperation;

INSERT INTO fact_inspectionlotsemoyoperation(
  fact_inspectionlotsemoyoperationid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, 
  dd_batchid, 
  dd_sapbatchid, 
  dd_lotnumber, 
  dd_materialname, 
  dd_materialfamily,
  dd_paramlistid,
  dd_paramlistversionid,
  dd_operationname,
  dd_paramid,
  dd_transformvalue,
  dd_transformdate,
  dim_transformdateid,
  dd_displayunits,
  dd_limittypeid,
  dd_condition1,
  dd_operator1,
  dd_value1num,
  dd_operator2,
  dd_value2num,
  dd_loweroos,
  dd_loweroot,
  dd_upperoos,
  dd_upperoot,
  ct_loweroos,
  ct_loweroot,
  ct_upperoos,
  ct_upperoot,
  ct_transformvalue
)
SELECT
  fact_inspectionlotsemoyoperationid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, 
  dd_batchid, 
  dd_sapbatchid, 
  dd_lotnumber, 
  dd_materialname, 
  dd_materialfamily,
  dd_paramlistid,
  dd_paramlistversionid,
  dd_operationname,
  dd_paramid,
  dd_transformvalue,
  dd_transformdate,
  dim_transformdateid,
  dd_displayunits,
  dd_limittypeid,
  dd_condition1,
  dd_operator1,
  dd_value1num,
  dd_operator2,
  dd_value2num,
  dd_loweroos,
  dd_loweroot,
  dd_upperoos,
  dd_upperoot,
  ct_loweroos,
  ct_loweroot,
  ct_upperoos,
  ct_upperoot,
  ct_transformvalue
FROM tmp_fact_inspectionlotsemoyoperation;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoyoperation;

DELETE FROM emd586.fact_inspectionlotsemoyoperation;

INSERT INTO emd586.fact_inspectionlotsemoyoperation
SELECT *
FROM fact_inspectionlotsemoyoperation;
