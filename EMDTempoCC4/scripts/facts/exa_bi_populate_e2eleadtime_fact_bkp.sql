/******************************************************************************************************************/
/*   Script         : exa_bi_populate_e2eleadtime_fact                                                            */
/*   Author         : Cristian Cleciu                                                                             */
/*   Created On     : 20 Sep 2017                                                                                 */
/*   Description    : Populating script of fact_e2eleadtime                                                       */
/*********************************************Change History*******************************************************/
/*   Date                By             		Version      Desc                                                 */
/*   20 Sep 2017         Cristian Cleciu        1.0          Creating the script.                                 */
/******************************************************************************************************************/


/* create inspection lot table with relevant lead time fields from QALS*/

drop table if exists tmp_fact_e2eleadtime_qals;

create table tmp_fact_e2eleadtime_qals as
select distinct 
qals_prueflos, 
qals_charg, 
qals_matnr, 
qals_werk, 
ifnull(qals_EBELN, 'Not Set') AS QALS_EBELN, 
IFNULL(qals_ebelp,'Not Set') AS qals_ebelp,
qals_bwart,
QALS_ENSTEHDAT,
QALS_ENTSTEZEIT, 
QALS_PAENDTERM, 
QALS_PASTRTERM, 
QALS_OBJNR
from qals_e2e q;

/* create temp table for canceled inspection lot status*/

DROP TABLE IF EXISTS tmp_fact_e2eleadtime_canceled_inspection;

create table tmp_fact_e2eleadtime_canceled_inspection AS
select distinct JEST_OBJNR
from jest_qals j
where j.JEST_STAT = 'I0224' 
AND j.JEST_INACT is NULL;

--remove canceled inspection lots
delete from tmp_fact_e2eleadtime_qals
where QALS_OBJNR in (select JEST_OBJNR from tmp_fact_e2eleadtime_canceled_inspection);

--create main temp table as base from Material Movement
drop table if exists tmp_fact_e2eleadtime_part1;
create table tmp_fact_e2eleadtime_part1 as 
select 
mseg_werks,
mseg_matnr,
mseg_charg,
mseg_lifnr,
min(mseg_ebeln) as mseg_ebeln,
min(mseg_ebelp) AS mseg_ebelp,
CONVERT(VARCHAR(50),'Not Set') as EBAN_BANFN,
CONVERT(VARCHAR(10),'Not Set') AS EBAN_BNFPO,
CONVERT(VARCHAR(12),'Not Set') AS qals_prueflos,
to_date('00010101','YYYYMMDD') AS EBAN_BADAT, -- PR Creation date
to_date('00010101','YYYYMMDD') AS EKKO_AEDAT, -- PO creation date
to_date('00010101','YYYYMMDD') AS NAST_DATVR, -- PO send date
to_date('00010101','YYYYMMDD') AS EKES_ERDAT, -- PO send date
to_date('00010101','YYYYMMDD') AS mkpf_cpudt, -- good receipt date
to_date('00010101','YYYYMMDD') AS qave_vdatum, -- usage decision date
to_date('00010101','YYYYMMDD') AS MKPF_BUDAT_first, -- first 261 date
to_date('00010101','YYYYMMDD') AS lab_reception_date,
to_date('00010101','YYYYMMDD') AS analysis_end_date,
to_date('00010101','YYYYMMDD') AS verification_end_date,
CONVERT(VARCHAR(10),'Not Set') AS qave_vcode
from mkpf_mseg_e2e
where
mseg_charg IS NOT NULL
and mseg_matnr IS NOT NULL
and mseg_ebeln IS NOT NULL
and mseg_bwart not in (351, 352)
group by 
mseg_werks,
mseg_matnr,
mseg_charg,
mseg_lifnr;

--find GR Date as first Posting Date for a 101 
drop table if exists tmp_fact_e2eleadtime_minGRdate;
create table tmp_fact_e2eleadtime_minGRdate as 
select 
mseg_werks,
mseg_matnr,
mseg_charg,
min(mkpf_cpudt) as mkpf_cpudt
from mkpf_mseg_e2e
where mseg_bwart  = 101
group by 
mseg_werks,
mseg_matnr,
mseg_charg;

update tmp_fact_e2eleadtime_part1 t1
set t1.mkpf_cpudt = IFNULL(m.mkpf_cpudt,TO_DATE('00010101','YYYYMMDD'))
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_minGRdate m
where t1.mseg_werks = m.mseg_werks
and t1.mseg_matnr = m.mseg_matnr
and t1.mseg_charg = m.mseg_charg
and t1.mkpf_cpudt <> IFNULL(m.mkpf_cpudt,TO_DATE('00010101','YYYYMMDD'));

-- find maximum Inspection Lot
drop table if exists tmp_fact_e2eleadtime_maxinspectionlot;
create table tmp_fact_e2eleadtime_maxinspectionlot as
select 
qals_werk,
qals_matnr,
qals_charg,
max(qals_prueflos) as qals_prueflos
from tmp_fact_e2eleadtime_qals
where 1=1
group by qals_werk,
qals_matnr,
qals_charg;

update tmp_fact_e2eleadtime_part1 t1
set t1.qals_prueflos = IFNULL(i.qals_prueflos, 'Not Set')
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_maxinspectionlot i
where 
t1.mseg_werks = i.qals_werk
and t1.mseg_matnr = i.qals_matnr
and t1.mseg_charg = i.qals_charg
and t1.qals_prueflos <> IFNULL(i.qals_prueflos, 'Not Set');

UPDATE tmp_fact_e2eleadtime_part1 t1
SET t1.qave_vcode = IFNULL(q.qave_vcode,'Not Set')
FROM tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_maxinspectionlot i, qave_e2e q
WHERE 
t1.mseg_werks = i.qals_werk
and t1.mseg_matnr = i.qals_matnr
and t1.mseg_charg = i.qals_charg
and i.qals_prueflos = q.qave_prueflos
AND t1.qave_vcode <> IFNULL(q.qave_vcode,'Not Set');

update tmp_fact_e2eleadtime_part1 t1
set t1.qave_vdatum = IFNULL(q.qave_vdatum,TO_DATE('00010101','YYYYMMDD'))
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_maxinspectionlot i, qave_e2e q
where 
t1.mseg_werks = i.qals_werk
and t1.mseg_matnr = i.qals_matnr
and t1.mseg_charg = i.qals_charg
and i.qals_prueflos = q.qave_prueflos
and q.qave_vcode in ('A0','A1','A2','R3','R7','R8')
and t1.qave_vdatum <> IFNULL(q.qave_vdatum,TO_DATE('00010101','YYYYMMDD'));


-- bring PO PR information from Purchasing
drop table if exists tmp_fact_e2eleadtime_POPR;

create table tmp_fact_e2eleadtime_POPR as 
select 
e.eban_werks,
e.eban_matnr,
e.eban_ebeln,
e.eban_ebelp,
min(e.eban_banfn) AS eban_banfn,
min(e.eban_bnfpo) as eban_bnfpo,
min(e.ekko_aedat) as ekko_aedat,
min(e.eban_badat) as eban_badat
from ekko_eban_e2e e
join tmp_fact_e2eleadtime_part1 t1
on e.eban_werks = t1.mseg_werks
and e.eban_matnr = t1.mseg_matnr 
and e.eban_ebeln = t1.mseg_ebeln
and e.eban_ebelp = t1.mseg_ebelp
group by 
e.eban_werks,
e.eban_matnr,
e.eban_ebeln,
e.eban_ebelp;

update tmp_fact_e2eleadtime_part1 t1
set t1.eban_banfn = ifnull(e.eban_banfn,'Not Set')
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
where 
e.eban_werks = t1.mseg_werks
and e.eban_matnr = t1.mseg_matnr 
and e.eban_ebeln = t1.mseg_ebeln
and e.eban_ebelp = t1.mseg_ebelp
and t1.eban_banfn <> ifnull(e.eban_banfn,'Not Set');

update tmp_fact_e2eleadtime_part1 t1
set t1.eban_bnfpo = ifnull(e.eban_bnfpo,'Not Set')
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
where 
e.eban_werks = t1.mseg_werks
and e.eban_matnr = t1.mseg_matnr 
and e.eban_ebeln = t1.mseg_ebeln
and e.eban_ebelp = t1.mseg_ebelp
and t1.eban_bnfpo <> ifnull(e.eban_bnfpo,'Not Set');

update tmp_fact_e2eleadtime_part1 t1
set t1.ekko_aedat = ifnull(e.ekko_aedat,TO_DATE('00010101','YYYYMMDD'))
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
where 
e.eban_werks = t1.mseg_werks
and e.eban_matnr = t1.mseg_matnr 
and e.eban_ebeln = t1.mseg_ebeln
and e.eban_ebelp = t1.mseg_ebelp
and t1.ekko_aedat <> ifnull(e.ekko_aedat,TO_DATE('00010101','YYYYMMDD'));

update tmp_fact_e2eleadtime_part1 t1
set t1.eban_badat = ifnull(e.eban_badat,TO_DATE('00010101','YYYYMMDD'))
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
where 
e.eban_werks = t1.mseg_werks
and e.eban_matnr = t1.mseg_matnr 
and e.eban_ebeln = t1.mseg_ebeln
and e.eban_ebelp = t1.mseg_ebelp
and t1.eban_badat <> ifnull(e.eban_badat,TO_DATE('00010101','YYYYMMDD'));

-- find first PO Send Date
drop table if exists tmp_fact_e2eleadtime_minPOSendDate;

create table tmp_fact_e2eleadtime_minPOSendDate AS
select  NAST_OBJKY, 
MIN(NAST_DATVR) AS NAST_DATVR, 
MIN(NAST_UHRVR) AS NAST_UHRVR
from EKKO_NAST_E2E e
join tmp_fact_e2eleadtime_POPR p on p.eban_ebeln = e.NAST_OBJKY
group by NAST_OBJKY;

update tmp_fact_e2eleadtime_part1 t1
set t1.NAST_DATVR = TO_DATE(ifnull(e.NAST_DATVR,'00010101'),'YYYYMMDD')
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_minPOSendDate e
where 
e.NAST_OBJKY = t1.mseg_ebeln
and t1.NAST_DATVR <> TO_DATE(ifnull(e.NAST_DATVR,'00010101'),'YYYYMMDD');

-- if PO Send Date is null then PO Send Date = PO Creation Date
update tmp_fact_e2eleadtime_part1 t1
set t1.NAST_DATVR = t1.EKKO_AEDAT
from tmp_fact_e2eleadtime_part1 t1
where 
t1.NAST_DATVR = TO_DATE('00010101','YYYYMMDD')
and t1.NAST_DATVR <> t1.EKKO_AEDAT;


drop table if exists tmp_fact_e2eleadtime_confreceived;

CREATE TABLE tmp_fact_e2eleadtime_confreceived AS
SELECT 
T1.MSEG_WERKS,
E.EKES_ebeln,
E.EKES_ebelp,
MIN(E.EKES_ERDAT) AS EKES_ERDAT
FROM EKES_E2E E
JOIN tmp_fact_e2eleadtime_part1 T1
ON e.EKES_ebeln = t1.mseg_ebeln
and e.EKES_ebelp = t1.mseg_ebelp
GROUP BY
T1.MSEG_WERKS,
E.EKES_ebeln,
E.EKES_ebelp;

UPDATE tmp_fact_e2eleadtime_part1 T1
SET T1.EKES_ERDAT = IFNULL(E.EKES_ERDAT,TO_DATE('00010101','YYYYMMDD'))
FROM tmp_fact_e2eleadtime_part1 T1, tmp_fact_e2eleadtime_confreceived E
WHERE e.EKES_ebeln = t1.mseg_ebeln
and e.EKES_ebelp = t1.mseg_ebelp
AND e.MSEG_WERKS = T1.MSEG_WERKS
AND T1.EKES_ERDAT <> IFNULL(E.EKES_ERDAT,TO_DATE('00010101','YYYYMMDD'));

DROP TABLE IF EXISTS tmp_fact_e2eleadtime_min261;

CREATE TABLE tmp_fact_e2eleadtime_min261 AS
SELECT 
	E.MSEG_MATNR, 
	E.MSEG_CHARG, 
	E.MSEG_WERKS,
	Min(E.MKPF_BUDAT) AS MKPF_BUDAT
FROM mkpf_mseg_e2e E
JOIN tmp_fact_e2eleadtime_part1 t1
ON T1.MSEG_MATNR = E.MSEG_MATNR
AND T1.MSEG_CHARG = E.MSEG_CHARG
AND T1.MSEG_WERKS = E.MSEG_WERKS
WHERE E.MSEG_BWART = '261'
GROUP BY 
	E.MSEG_MATNR, 
	E.MSEG_CHARG, 
	E.MSEG_WERKS;

UPDATE tmp_fact_e2eleadtime_part1 T1
	SET T1.MKPF_BUDAT_first = IFNULL(E.MKPF_BUDAT,TO_DATE('00010101','YYYYMMDD'))
FROM tmp_fact_e2eleadtime_part1 T1, tmp_fact_e2eleadtime_min261 E
WHERE T1.MSEG_MATNR = E.MSEG_MATNR
AND T1.MSEG_CHARG = E.MSEG_CHARG
AND T1.MSEG_WERKS = E.MSEG_WERKS
AND T1.MKPF_BUDAT_first <> IFNULL(E.MKPF_BUDAT,TO_DATE('00010101','YYYYMMDD'));

--update from LIMS data
UPDATE tmp_fact_e2eleadtime_part1 T1
SET T1.lab_reception_date = IFNULL(L.lab_reception_date,TO_DATE('00010101','YYYYMMDD'))
from aub_insplot_qc_dates L, tmp_fact_e2eleadtime_part1 T1
WHERE mseg_matnr = material_code 
and mseg_charg = batch_number
and qals_prueflos = ltrim(lot_number,0)
AND T1.lab_reception_date <> IFNULL(L.lab_reception_date,TO_DATE('00010101','YYYYMMDD'));

UPDATE tmp_fact_e2eleadtime_part1 T1
SET T1.analysis_end_date = IFNULL(L.analysis_end_date,TO_DATE('00010101','YYYYMMDD'))
from aub_insplot_qc_dates L, tmp_fact_e2eleadtime_part1 T1
WHERE mseg_matnr = material_code 
and mseg_charg = batch_number
and qals_prueflos = ltrim(lot_number,0)
AND T1.analysis_end_date <> IFNULL(L.analysis_end_date,TO_DATE('00010101','YYYYMMDD'));

UPDATE tmp_fact_e2eleadtime_part1 T1
SET T1.verification_end_date = IFNULL(L.verification_end_date,TO_DATE('00010101','YYYYMMDD'))
from aub_insplot_qc_dates L, tmp_fact_e2eleadtime_part1 T1
WHERE mseg_matnr = material_code 
and mseg_charg = batch_number
and qals_prueflos = ltrim(lot_number,0)
AND T1.verification_end_date <> IFNULL(L.verification_end_date,TO_DATE('00010101','YYYYMMDD'));

/* main temp table for integrating all sources*/

DROP TABLE IF EXISTS tmp_fact_e2eleadtime;
CREATE TABLE tmp_fact_e2eleadtime AS 	
SELECT  
	CONVERT(BIGINT,1) AS Dim_Plantid,
	t1.MSEG_WERKS AS MSEG_WERKS, -- dim_plant
    CONVERT(BIGINT,1) AS dim_partid,
	t1.MSEG_MATNR AS MSEG_MATNR,	-- dim_part
    ifnull(CONVERT(VARCHAR(50), t1.EBAN_BANFN),'Not Set')  AS dd_PurchaseReqNo,
    ifnull(CONVERT(VARCHAR(10), t1.EBAN_BNFPO),'Not Set')  AS dd_PurchaseReqItemNo,
    CONVERT(BIGINT, 1) AS dim_dateidPRCreation,
	t1.EBAN_BADAT AS EBAN_BADAT, -- dim_dateidPRCreation
    ifnull(CONVERT(VARCHAR(10), t1.MSEG_EBELN),'Not Set')  AS dd_PurchaseOrderNo,
    ifnull(CONVERT(VARCHAR(18), t1.MSEG_EBELP),'Not Set')  AS dd_PurchaseOrderItemNo,
    CONVERT (BIGINT, 1) AS dim_dateidPOCreation,
	t1.EKKO_AEDAT AS EKKO_AEDAT, -- dim_dateidPOCreation
    CONVERT(BIGINT, 1) AS dim_dateidProcessing,
	t1.NAST_DATVR AS NAST_DATVR, -- dim_dateidProcessing
    CONVERT(BIGINT, 1) AS dim_dateidConfReceived,
	t1.EKES_ERDAT AS EKES_ERDAT, -- dim_dateidConfReceived
    CONVERT(VARCHAR(10),'Not Set')  AS dd_MaterialDocNo,
    CONVERT(VARCHAR(18),'Not Set')  AS dd_MaterialDocItemNo,
    ifnull(CONVERT(VARCHAR(10), t1.MSEG_CHARG),'Not Set')  AS dd_BatchNumber,
	CONVERT(VARCHAR(12), 'Not Set') AS dd_OrderNumber,
    CONVERT(BIGINT, 1) AS dim_dateidDocEntry,
	t1.MKPF_CPUDT AS MKPF_CPUDT, -- dim_dateidDocEntry GR Date
    CONVERT(VARCHAR(10),'Not Set')  AS dd_TransferReqNo,
    CONVERT(VARCHAR(10),'Not Set')  AS dd_TrackingNo,
    CONVERT(VARCHAR(10),'Not Set')  AS dd_TransferOrderNo,
    CONVERT(VARCHAR(18),'Not Set')  AS dd_TransferOrderItem,
    CONVERT(BIGINT, 1) AS dim_dateidTOCreated,
	TO_DATE('00010101','YYYYMMDD') AS LTAK_BDATU, -- dim_dateidTOCreated
    CONVERT(BIGINT, 1) AS dim_dateidconfirmation,
	TO_DATE('00010101','YYYYMMDD') AS LTAP_QDATU, -- dim_dateidconfirmation
    ifnull(CONVERT(VARCHAR(12), t1.QALS_PRUEFLOS),'Not Set')  AS dd_inspectionlotno,
    CONVERT(BIGINT, 1) AS dim_dateidlotcreated,
	TO_DATE('00010101','YYYYMMDD') AS QALS_ENSTEHDAT, -- dim_dateidlotcreated
    TO_DATE('00010101','YYYYMMDD') AS QALS_PAENDTERM, -- Inspection lot end date
	CONVERT(BIGINT, 1) AS Dim_DateIdInspectionLotEnd,
	TO_DATE('00010101','YYYYMMDD') AS QALS_PASTRTERM, -- inspection lot start date
	CONVERT(BIGINT, 1) AS Dim_DateIdInspectionLotStart,
	CONVERT(BIGINT, 0) AS ct_StandardQCLT,
	CONVERT(BIGINT, 0) AS ct_StandardQCQALTv2,	
    CONVERT(VARCHAR(18),'Not Set')  AS dd_SampleDrawinNo,
    CONVERT(BIGINT, 1) AS dim_dateidsdopsrelease,	
    TO_DATE('00010101','YYYYMMDD') AS QPRN_FRGDT, -- dim_dateidsdopsrelease
    CONVERT(BIGINT, 1) AS Dim_DateIdUsageDecision,
	t1.qave_vdatum AS QAVE_VDATUM, -- Dim_DateIdUsageDecision 	
    CONVERT(BIGINT, 1) AS dim_dateidpostingdate,
    TO_DATE('00010101','YYYYMMDD') AS MKPF_BUDAT, --dim_dateidpostingdate
    CONVERT(BIGINT, 1) AS dim_dateidfirst261,
    T1.MKPF_BUDAT_first AS MKPF_BUDAT_first, --dim_dateidfirst261
    CONVERT(BIGINT, 1) AS amt_exhangerate,
    CONVERT(BIGINT, 1) AS amt_exchangerate_gbl,
    CONVERT(BIGINT, 1) AS dim_currencyid,
    CONVERT(BIGINT, 1) AS dim_currencyid_tra,
    CONVERT(BIGINT, 1) AS dim_currencyid_gbl,
    current_timestamp 	as DW_UPDATE_DATE,
    current_timestamp 	as DW_INSERT_DATE,
    CONVERT(BIGINT, 1) AS DIM_PROJECTSOURCEID,
	CONVERT(BIGINT, 0) AS ct_prCreationlt,	
	CONVERT(BIGINT, 0) as ct_poCreationlt,
	CONVERT(BIGINT, 0) AS ct_ptconfirmationlt,
	CONVERT(BIGINT, 0) as ct_supplierlt,
	CONVERT(BIGINT, 0) AS ct_tocreationlt,
	CONVERT(BIGINT, 0) AS ct_stockPlacementlt,
	CONVERT(BIGINT, 0) as ct_sampleDrawinglt,
	CONVERT(BIGINT, 0) as ct_qcqalt,	
	CONVERT(BIGINT, 0)	as ct_bachfirst,
	TO_DATE('00010101','YYYYMMDD') AS CAUFV_ERDAT,-- Process Order Creation Date
	CONVERT(BIGINT, 1) AS Dim_DateIdProcessOrderCreation,
	TO_DATE('00010101','YYYYMMDD') AS CAUFV_FTRMI,-- Process Order Release Date
	CONVERT(BIGINT, 1) AS Dim_DateIdProcessOrderRelease,
	TO_DATE('00010101','YYYYMMDD') AS CAUFV_GLTRI, -- Process order finish date
	CONVERT(BIGINT, 1) AS Dim_DateIdProcessOrderFinish,
	CONVERT(BIGINT, 0)	AS ct_PROPreparationLT,
	CONVERT(BIGINT, 0)	AS ct_MaterialStaggingLT,
	CONVERT(BIGINT, 0) AS ct_ProductionLT,
	CONVERT(BIGINT, 0) AS ct_PlannedDeliveryLT,
	CONVERT(BIGINT, 0)	AS ct_GoodRecepitProcessingLT,
	CONVERT(BIGINT, 1) AS dim_materialcodingschemeID,
	CONVERT(BIGINT, 0) AS dd_releaseperiod,
	CONVERT(VARCHAR(7), 'Not Set') AS MSEG_KZVBR, 
	CONVERT(VARCHAR(7), 'Not Set') AS MSEG_KZBEW, 
	CONVERT(VARCHAR(7), 'Not Set') AS MSEG_KZZUG, 
	CONVERT(VARCHAR(7), 'Not Set') AS MSEG_SOBKZ,
	CONVERT(BIGINT, 1) AS dim_inspectionlotstatusid,
	t1.MSEG_LIFNR AS MSEG_LIFNR,
	CONVERT(BIGINT, 1) AS Dim_vendorid,
	t1.lab_reception_date 		AS lab_reception_date,
    t1.analysis_end_date  		AS analysis_end_date,
    t1.verification_end_date	AS verification_end_date,
    CONVERT(BIGINT, 1) AS dim_dateid_lab_reception,
    CONVERT(BIGINT, 1) AS dim_dateid_analysis_end,
    CONVERT(BIGINT, 1) AS dim_dateid_verification_end,
    CONVERT(VARCHAR(100), 'Not Set') AS dd_SOPFamily,
	CONVERT(VARCHAR(100), 'Not Set') AS dd_SubFamily,
	t1.qave_vcode AS dd_UDCode
FROM tmp_fact_e2eleadtime_part1 t1;
	
/* calculating Lead Time Measures*/

--ct_supplierlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_supplierlt = MKPF_CPUDT-EKES_ERDAT
FROM tmp_fact_e2eleadtime
WHERE (MKPF_CPUDT <> TO_DATE('00010101','YYYYMMDD') AND EKES_ERDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_supplierlt <> MKPF_CPUDT-EKES_ERDAT;

--ct_tocreationlt	
UPDATE tmp_fact_e2eleadtime 
	SET ct_tocreationlt = LTAK_BDATU-MKPF_CPUDT
FROM tmp_fact_e2eleadtime
WHERE (LTAK_BDATU <> TO_DATE('00010101','YYYYMMDD') AND MKPF_CPUDT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_tocreationlt <> LTAK_BDATU-MKPF_CPUDT;

--ct_GoodRecepitProcessingLT
UPDATE tmp_fact_e2eleadtime 
	SET ct_GoodRecepitProcessingLT = QAVE_VDATUM - MKPF_CPUDT
FROM tmp_fact_e2eleadtime
WHERE (QAVE_VDATUM <> TO_DATE('00010101','YYYYMMDD') AND MKPF_CPUDT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_GoodRecepitProcessingLT <> QAVE_VDATUM - MKPF_CPUDT;

-- ct_bachfirst
UPDATE tmp_fact_e2eleadtime 
	SET ct_bachfirst = MKPF_BUDAT_first-QAVE_VDATUM
FROM tmp_fact_e2eleadtime
WHERE (MKPF_BUDAT_first <> TO_DATE('00010101','YYYYMMDD') AND QAVE_VDATUM <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_bachfirst <> MKPF_BUDAT_first-QAVE_VDATUM;
	
--	ct_MaterialStaggingLT,	
UPDATE tmp_fact_e2eleadtime 
	SET ct_MaterialStaggingLT = MKPF_BUDAT_first-CAUFV_FTRMI
FROM tmp_fact_e2eleadtime
WHERE (CAUFV_FTRMI <> TO_DATE('00010101','YYYYMMDD') AND MKPF_BUDAT_first <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_MaterialStaggingLT <> MKPF_BUDAT_first-CAUFV_FTRMI;

--	ct_ProductionLT,
UPDATE tmp_fact_e2eleadtime 
	SET ct_ProductionLT = CAUFV_GLTRI - MKPF_BUDAT_first
FROM tmp_fact_e2eleadtime
WHERE (CAUFV_GLTRI <> TO_DATE('00010101','YYYYMMDD') AND MKPF_BUDAT_first <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_ProductionLT <> CAUFV_GLTRI - MKPF_BUDAT_first;

--ct_PlannedDeliveryLT
UPDATE tmp_fact_e2eleadtime 
	SET ct_PlannedDeliveryLT = MKPF_CPUDT - NAST_DATVR
FROM tmp_fact_e2eleadtime
WHERE (NAST_DATVR <> TO_DATE('00010101','YYYYMMDD') AND MKPF_CPUDT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_PlannedDeliveryLT <> MKPF_CPUDT - NAST_DATVR;

--ct_poCreationlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_poCreationlt = NAST_DATVR - EKKO_AEDAT
FROM tmp_fact_e2eleadtime
WHERE (NAST_DATVR <> TO_DATE('00010101','YYYYMMDD') AND EKKO_AEDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_poCreationlt <> NAST_DATVR - EKKO_AEDAT;

--ct_ptconfirmationlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_ptconfirmationlt = EKES_ERDAT - NAST_DATVR
FROM tmp_fact_e2eleadtime
WHERE (NAST_DATVR <> TO_DATE('00010101','YYYYMMDD') AND EKES_ERDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_ptconfirmationlt <> EKES_ERDAT - NAST_DATVR;

--ct_StandardQCLT
UPDATE tmp_fact_e2eleadtime 
	SET ct_StandardQCLT = QALS_PAENDTERM - QALS_PASTRTERM
FROM tmp_fact_e2eleadtime
WHERE (QALS_PAENDTERM <> TO_DATE('00010101','YYYYMMDD') AND QALS_PASTRTERM <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_StandardQCLT <> QALS_PAENDTERM - QALS_PASTRTERM;

--ct_StandardQCQALTv2
UPDATE tmp_fact_e2eleadtime 
	SET ct_StandardQCQALTv2 = QAVE_VDATUM - QALS_PAENDTERM
FROM tmp_fact_e2eleadtime
WHERE (QAVE_VDATUM <> TO_DATE('00010101','YYYYMMDD') AND QALS_PAENDTERM <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_StandardQCQALTv2 <> QAVE_VDATUM - QALS_PAENDTERM;

--ct_prCreationlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_prCreationlt = EKKO_AEDAT-EBAN_BADAT
FROM tmp_fact_e2eleadtime
WHERE (EKKO_AEDAT <> TO_DATE('00010101','YYYYMMDD') AND EBAN_BADAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_prCreationlt <> EKKO_AEDAT-EBAN_BADAT;

--ct_stockPlacementlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_stockPlacementlt = LTAP_QDATU-LTAK_BDATU
FROM tmp_fact_e2eleadtime
WHERE (LTAP_QDATU <> TO_DATE('00010101','YYYYMMDD') AND LTAK_BDATU <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_stockPlacementlt <> LTAP_QDATU-LTAK_BDATU;
	
--ct_sampleDrawinglt
UPDATE tmp_fact_e2eleadtime 
	SET ct_sampleDrawinglt = QPRN_FRGDT-QALS_ENSTEHDAT
FROM tmp_fact_e2eleadtime
WHERE (QPRN_FRGDT <> TO_DATE('00010101','YYYYMMDD') AND QALS_ENSTEHDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_sampleDrawinglt <> QPRN_FRGDT-QALS_ENSTEHDAT;
	
--ct_qcqalt
UPDATE tmp_fact_e2eleadtime 
	SET ct_qcqalt = QAVE_VDATUM-QPRN_FRGDT
FROM tmp_fact_e2eleadtime
WHERE (QAVE_VDATUM <> TO_DATE('00010101','YYYYMMDD') AND QPRN_FRGDT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_qcqalt <> QAVE_VDATUM-QPRN_FRGDT;
	
--ct_PROPreparationLT
UPDATE tmp_fact_e2eleadtime 
	SET ct_PROPreparationLT = CAUFV_FTRMI-CAUFV_ERDAT
FROM tmp_fact_e2eleadtime
WHERE (CAUFV_FTRMI <> TO_DATE('00010101','YYYYMMDD') AND CAUFV_ERDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_PROPreparationLT <> CAUFV_FTRMI-CAUFV_ERDAT;
	
/* updating dimensions */

--S&OP Family 
UPDATE tmp_fact_e2eleadtime f
set dd_SOPFamily = ifnull(MARC_YYD_YSOP2,'Not Set')
from  tmp_fact_e2eleadtime f, MARC_E2E m
where mseg_matnr = marc_matnr
and mseg_werks = marc_werks
and dd_SOPFamily <> ifnull(MARC_YYD_YSOP2,'Not Set');

--SubFamily 
UPDATE tmp_fact_e2eleadtime f
set dd_SubFamily = ifnull(MARC_YYD_YSOP3,'Not Set')
from  tmp_fact_e2eleadtime f, MARC_E2E m
where mseg_matnr = marc_matnr
and mseg_werks = marc_werks
and dd_SubFamily <> ifnull(MARC_YYD_YSOP3,'Not Set');

--dim_venor	  
UPDATE tmp_fact_e2eleadtime f
SET f.dim_vendorid  = dv1.dim_vendorid
FROM dim_vendor dv1, tmp_fact_e2eleadtime f
WHERE    dv1.VendorNumber = ifnull(f.MSEG_LIFNR, 'Not Set')
     AND dv1.RowIsCurrent = 1
     AND f.dim_vendorid  <> dv1.dim_vendorid;

/*dim_materialcodingscheme*/	
UPDATE tmp_fact_e2eleadtime f
SET f.dim_materialcodingschemeid = IFNULL(d.dim_materialcodingschemeid,1)
FROM  tmp_fact_e2eleadtime f, dim_materialcodingscheme d
WHERE d.pos_1 = LEFT(IFNULL(f.MSEG_MATNR,'|'),1)
AND f.dim_materialcodingschemeid <> d.dim_materialcodingschemeid;

/*dim_partid*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_partid = IFNULL(pt.dim_partid, 1)
FROM tmp_fact_e2eleadtime f
LEFT JOIN dim_Part pt ON pt.PartNumber = f.MSEG_MATNR
		 AND pt.Plant      = f.MSEG_WERKS 
WHERE f.dim_partid <> IFNULL(pt.dim_partid, 1);

/*T436A_FREIZ dd_releaseperiod*/	
UPDATE tmp_fact_e2eleadtime f
SET f.dd_releaseperiod = IFNULL(v.V436A_FREIZ,0)
FROM  tmp_fact_e2eleadtime f, dim_part d, V436A_E2E v
WHERE f.dim_partid = d.dim_partid
AND d.schedmarginkey=v.V436A_FHORI
AND d.plant =v.V436A_WERKS
AND f.dd_releaseperiod <> IFNULL(v.V436A_FREIZ,0);

/*Dim_Plantid*/	
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_Plantid = IFNULL(pl.dim_plantid, 1)
FROM tmp_fact_e2eleadtime f
LEFT JOIN dim_plant pl ON pl.PlantCode = f.MSEG_WERKS 
WHERE f.Dim_Plantid <> IFNULL(pl.dim_plantid, 1);

/*dim_dateidPRCreation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidPRCreation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EBAN_BADAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidPRCreation <> ifnull(dd.dim_dateid,1);

/*dim_dateidPOCreation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidPOCreation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EKKO_AEDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidPOCreation <> ifnull(dd.dim_dateid,1);

/*dim_dateidProcessing*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidProcessing = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.NAST_DATVR
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidProcessing <> ifnull(dd.dim_dateid,1);

/*dim_dateidConfReceived*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidConfReceived = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EKES_ERDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidConfReceived <> ifnull(dd.dim_dateid,1);

/*dim_dateidDocEntry*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidDocEntry = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.MKPF_CPUDT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidDocEntry <> ifnull(dd.dim_dateid,1);

/*dim_dateidTOCreated*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidTOCreated = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.LTAK_BDATU
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidTOCreated <> ifnull(dd.dim_dateid,1);

/*dim_dateidconfirmation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidconfirmation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.LTAP_QDATU
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidconfirmation <> ifnull(dd.dim_dateid,1);

/*dim_dateidlotcreated*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidlotcreated = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QALS_ENSTEHDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidlotcreated <> ifnull(dd.dim_dateid,1);

/*dim_dateidsdopsrelease*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidsdopsrelease = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QPRN_FRGDT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidsdopsrelease <> ifnull(dd.dim_dateid,1);

/*Dim_DateIdUsageDecision*/
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdUsageDecision = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QAVE_VDATUM
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdUsageDecision <> ifnull(dd.dim_dateid,1);

/*dim_dateidfirst261*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidfirst261 = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.MKPF_BUDAT_first
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidfirst261 <> ifnull(dd.dim_dateid,1);

--dim_dateidpostingdate
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidpostingdate = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.MKPF_BUDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidpostingdate <> ifnull(dd.dim_dateid,1);

--Dim_DateIdInspectionLotEnd
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdInspectionLotEnd = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QALS_PAENDTERM
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdInspectionLotEnd <> ifnull(dd.dim_dateid,1);

--Dim_DateIdInspectionLotStart
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdInspectionLotStart = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QALS_PASTRTERM
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdInspectionLotStart <> ifnull(dd.dim_dateid,1);

--Dim_DateIdProcessOrderCreation
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdProcessOrderCreation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.CAUFV_ERDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdProcessOrderCreation <> ifnull(dd.dim_dateid,1);

--Dim_DateIdProcessOrderRelease
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdProcessOrderRelease = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.CAUFV_FTRMI
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdProcessOrderRelease <> ifnull(dd.dim_dateid,1);


--Dim_DateIdProcessOrderFinish
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdProcessOrderFinish = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.CAUFV_GLTRI
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdProcessOrderFinish <> ifnull(dd.dim_dateid,1);

--dim_dateid_lab_reception
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateid_lab_reception = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.lab_reception_date
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateid_lab_reception <> ifnull(dd.dim_dateid,1);


--dim_dateid_analysis_end
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateid_analysis_end = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.analysis_end_date
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateid_analysis_end <> ifnull(dd.dim_dateid,1);

--dim_dateid_verification_end
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateid_verification_end = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.verification_end_date
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateid_verification_end <> ifnull(dd.dim_dateid,1);



DROP TABLE IF EXISTS number_fountain_fact_e2eleadtime;
CREATE TABLE number_fountain_fact_e2eleadtime LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_e2eleadtime WHERE table_name = 'fact_e2eleadtime';

INSERT INTO number_fountain_fact_e2eleadtime
SELECT 'fact_e2eleadtime', ifnull(max(fact_e2eleadtimeid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_e2eleadtime;


DELETE FROM fact_e2eleadtime;

INSERT INTO fact_e2eleadtime(
fact_e2eLeadTimeid
,Dim_Plantid 			
,dim_partid				
,dd_PurchaseReqNo		
,dd_PurchaseReqItemNo	
,dim_dateidPRCreation	
,dd_PurchaseOrderNo		
,dd_PurchaseOrderItemNo
,dim_dateidPOCreation	
,dim_dateidProcessing	
,dim_dateidConfReceived	
,dd_MaterialDocNo		
,dd_MaterialDocItemNo	
,dd_BatchNumber			
,dim_dateidDocEntry		
,dd_TransferReqNo		
,dd_TrackingNo	
,dd_TransferOrderNo		
,dd_TransferOrderItem	
,dim_dateidTOCreated		
,dim_dateidconfirmation	
,dd_inspectionlotno		
,dim_dateidlotcreated	
,dd_SampleDrawinNo		
,dim_dateidsdopsrelease	
,Dim_DateIdUsageDecision	
,dim_dateidfirst261
,ct_prCreationlt			
,ct_poCreationlt			
,ct_ptconfirmationlt		
,ct_supplierlt			
,ct_tocreationlt			
,ct_stockPlacementlt		
,ct_sampleDrawinglt		
,ct_qcqalt				
,ct_bachfirst						
,amt_exhangerate			
,amt_exchangerate_gbl	
,dim_currencyid			
,dim_currencyid_tra		
,dim_currencyid_gbl		
,DW_UPDATE_DATE			
,DW_INSERT_DATE			
,DIM_PROJECTSOURCEID	
,dd_OrderNumber	
,Dim_DateIdInspectionLotEnd
,Dim_DateIdInspectionLotStart
,Dim_DateIdProcessOrderCreation
,Dim_DateIdProcessOrderRelease
,Dim_DateIdProcessOrderFinish
,ct_PROPreparationLT
,ct_MaterialStaggingLT
,ct_ProductionLT
,ct_StandardQCLT
,ct_StandardQCQALTv2	
,ct_PlannedDeliveryLT
,ct_GoodRecepitProcessingLT
,dim_materialcodingschemeID
,dd_releaseperiod
,dim_dateidpostingdate
,dim_inspectionlotstatusid
,dim_vendorid
,dim_dateid_lab_reception
,dim_dateid_analysis_end
,dim_dateid_verification_end
,dd_SOPFamily
,dd_SubFamily
,dd_UDCode

)
SELECT 
( SELECT max_id FROM number_fountain_fact_e2eleadtime WHERE table_name = 'fact_e2eleadtime') 
	+ row_number() over(order by '') AS fact_e2eLeadTimeid
,Dim_Plantid 			
,dim_partid				
,dd_PurchaseReqNo		
,dd_PurchaseReqItemNo	
,dim_dateidPRCreation	
,dd_PurchaseOrderNo		
,dd_PurchaseOrderItemNo	
,dim_dateidPOCreation	
,dim_dateidProcessing	
,dim_dateidConfReceived	
,dd_MaterialDocNo		
,dd_MaterialDocItemNo	
,dd_BatchNumber			
,dim_dateidDocEntry		
,dd_TransferReqNo		
,dd_TrackingNo	
,dd_TransferOrderNo		
,dd_TransferOrderItem	
,dim_dateidTOCreated		
,dim_dateidconfirmation	
,dd_inspectionlotno		
,dim_dateidlotcreated	
,dd_SampleDrawinNo		
,dim_dateidsdopsrelease	
,Dim_DateIdUsageDecision	
,dim_dateidfirst261
,ct_prCreationlt			
,ct_poCreationlt			
,ct_ptconfirmationlt		
,ct_supplierlt			
,ct_tocreationlt			
,ct_stockPlacementlt		
,ct_sampleDrawinglt		
,ct_qcqalt				
,ct_bachfirst						
,amt_exhangerate			
,amt_exchangerate_gbl	
,dim_currencyid			
,dim_currencyid_tra		
,dim_currencyid_gbl		
,DW_UPDATE_DATE			
,DW_INSERT_DATE			
,DIM_PROJECTSOURCEID	
,dd_OrderNumber	
,Dim_DateIdInspectionLotEnd
,Dim_DateIdInspectionLotStart
,Dim_DateIdProcessOrderCreation
,Dim_DateIdProcessOrderRelease
,Dim_DateIdProcessOrderFinish
,ct_PROPreparationLT
,ct_MaterialStaggingLT
,ct_ProductionLT
,ct_StandardQCLT
,ct_StandardQCQALTv2
,ct_PlannedDeliveryLT
,ct_GoodRecepitProcessingLT
,dim_materialcodingschemeID
,dd_releaseperiod
,dim_dateidpostingdate
,dim_inspectionlotstatusid
,dim_vendorid
,dim_dateid_lab_reception
,dim_dateid_analysis_end
,dim_dateid_verification_end
,dd_SOPFamily
,dd_SubFamily
,dd_UDCode
FROM tmp_fact_e2eleadtime;

DROP TABLE IF EXISTS tmp_fact_e2eleadtime;
DROP TABLE IF EXISTS tmp_maxGRdate;

DELETE FROM emd586.fact_e2eleadtime;

INSERT INTO emd586.fact_e2eleadtime
SELECT *
FROM fact_e2eleadtime;