/******************************************************************************************************************/
/*   Script         : bi_populate_hatbulkerbitux_fact                                                             */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_hatbulkerbitux                                                    */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   31 Jan 2017         CristianT       1.0               Creating the script.                                   */
/*   10 Feb 2017         CristianT       1.1               Calculating the AVG of previous years                  */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_hatbulkerbitux;
CREATE TABLE tmp_fact_hatbulkerbitux
AS
SELECT *
FROM fact_hatbulkerbitux
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_hatbulkerbitux';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_hatbulkerbitux', ifnull(max(fact_hatbulkerbituxid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_hatbulkerbitux;

INSERT INTO tmp_fact_hatbulkerbitux(
fact_hatbulkerbituxid,
dd_parameter_set_name,
dd_parameter_set_date,
dd_bf_batch_id,
amt_yieldhatbulk,
dim_dateidsetdate,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_hatbulkerbitux') + ROW_NUMBER() over(order by '') as fact_hatbulkerbituxid,
       ifnull(parameter_set_name, 'Not Set') as dd_parameter_set_name,
       ifnull(parameter_set_date, 0) as dd_parameter_set_date,
       ifnull(bf_batch_id, 'Not Set') as dd_bf_batch_id,
       ifnull(bf_conc_bulk_global_yield_dsp, 0) as amt_yieldhatbulk,
       1 as dim_dateidsetdate,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM yield_hat_bulk_erbitux
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR;

UPDATE tmp_fact_hatbulkerbitux tmp
SET tmp.dim_dateidsetdate =  dt.dim_dateid
FROM dim_date dt,
     tmp_fact_hatbulkerbitux tmp
WHERE dt.datevalue = substr(tmp.dd_parameter_set_date, 0, 10)
      AND dt.companycode = 'Not Set';

/* 10 Feb 2016 CristianT Start: Calculating the AVG of previous years */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_hatbulkerbitux';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_hatbulkerbitux', ifnull(max(fact_hatbulkerbituxid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_hatbulkerbitux;


INSERT INTO tmp_fact_hatbulkerbitux(
fact_hatbulkerbituxid,
dd_parameter_set_name,
dd_parameter_set_date,
dd_bf_batch_id,
amt_yieldhatbulk,
dim_dateidsetdate,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_hatbulkerbitux') + ROW_NUMBER() over(order by '') as fact_hatbulkerbituxid,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_parameter_set_name,
       current_date as dd_parameter_set_date,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_bf_batch_id,
       ifnull(AVG(bf_conc_bulk_global_yield_dsp), 0) as amt_yieldhatbulk,
       1 as dim_dateidsetdate,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM yield_hat_bulk_erbitux tmp
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
GROUP BY YEAR(substr(tmp.parameter_set_date, 0, 10));


UPDATE tmp_fact_hatbulkerbitux tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_hatbulkerbitux tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_parameter_set_date
      AND tmp.dim_dateidsetdate = 1;
/* 10 Feb 2016 CristianT End */

UPDATE tmp_fact_hatbulkerbitux tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_hatbulkerbitux tmp;

TRUNCATE TABLE fact_hatbulkerbitux;

INSERT INTO fact_hatbulkerbitux(
fact_hatbulkerbituxid,
dd_parameter_set_name,
dd_parameter_set_date,
dd_bf_batch_id,
amt_yieldhatbulk,
dim_dateidsetdate,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_hatbulkerbituxid,
       dd_parameter_set_name,
       dd_parameter_set_date,
       dd_bf_batch_id,
       amt_yieldhatbulk,
       dim_dateidsetdate,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM tmp_fact_hatbulkerbitux;

DROP TABLE IF EXISTS tmp_fact_hatbulkerbitux;
