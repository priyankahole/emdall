/*****************************************************************************************************************/
/*   Script         : exa_fact_forecastaccuracy                                                                  */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 07 Feb 2018                                                                                */
/*   Description    : Populating script of fact_forecastaccuracy                                                 */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   28 Feb 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/


DROP TABLE IF EXISTS tmp_fact_forecastaccuracy;
CREATE TABLE tmp_fact_forecastaccuracy
AS
SELECT *
FROM fact_forecastaccuracy
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_forecastaccuracy;
CREATE TABLE number_fountain_fact_forecastaccuracy LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_forecastaccuracy
SELECT 'fact_forecastaccuracyid', ifnull(max(fact_forecastaccuracyid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_forecastaccuracy;


/* Identify max load_date for the fo data set at timee level */
DROP TABLE IF EXISTS tmp_max_load_date_fach_fo;
CREATE TABLE tmp_max_load_date_fach_fo
AS
SELECT TIMEE,
       max(LOAD_DATE) as max_fo_load_date
FROM sap_ibp_fach_fo
GROUP BY TIMEE;

/* Identify max load_date for the cm data set at COUNTRYPLANNINGGROUP/BUSINESSDIVISION level */
DROP TABLE IF EXISTS tmp_max_load_date_fach_cm;
CREATE TABLE tmp_max_load_date_fach_cm
AS
SELECT COUNTRYPLANNINGGROUP,
       BUSINESSDIVISION,
       max(LOAD_DATE) as max_cm_load_date
FROM sap_ibp_fach_cm
GROUP BY COUNTRYPLANNINGGROUP,
         BUSINESSDIVISION;

/* Set the numeric columns to 0 when the string null appears */
UPDATE sap_ibp_fach_fo
SET sfaweight = case when sfaweight = 'null' then 0 else sfaweight end,
    kpiforecastacc = case when kpiforecastacc = 'null' then 0 else kpiforecastacc end,
    kpi1mbias = case when kpi1mbias = 'null' then 0 else kpi1mbias end,
    kpi3mbias = case when kpi3mbias = 'null' then 0 else kpi3mbias end,
    kpi6mbias = case when kpi6mbias = 'null' then 0 else kpi6mbias end,
    kpi3msystematicbias = case when kpi3msystematicbias = 'null' then 0 else kpi3msystematicbias end,
    kpi6msystematicbias = case when kpi6msystematicbias = 'null' then 0 else kpi6msystematicbias end;

INSERT INTO tmp_fact_forecastaccuracy(
fact_forecastaccuracyid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_countryplanninggroup,
dd_businesslinedesc,
dd_productcode,
dd_timee,
dim_dateidtime,
ct_sfaweight,
ct_kpiforecastacc,
ct_kpi1mbias,
ct_kpi3mbias,
ct_kpi6mbias,
ct_kpi3msystematicbias,
ct_kpi6msystematicbias,
ct_w_kpiforecastacc,
ct_w_kpi1mbias,
ct_w_kpi3mbias,
ct_w_kpi6mbias,
ct_w_kpi3msystematicbias,
ct_w_kpi6msystematicbias,
dd_businessdivision,
dd_ysfabiasdescr,
dd_countryid,
dd_blreportingregionlevel1,
dd_blreportingregionlevel2,
dd_blreportingregionlevel3,
dd_blreportingregionlevel4,
dd_blreportingregionlevel5,
dd_blreportingregionlevel6,
dd_blreportingregionlevel7,
dd_reportingregion,
dd_scmbdctryorg,
dd_scmorgsuborg,
dd_sdlevel
)
SELECT (SELECT max_id from number_fountain_fact_forecastaccuracy WHERE table_name = 'fact_forecastaccuracyid') + ROW_NUMBER() over(order by '') AS fact_forecastaccuracyid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(fo.countryplanninggroup, 'Not Set') as dd_countryplanninggroup,
       ifnull(fo.businesslinedesc, 'Not Set') as dd_businesslinedesc,
       ifnull(fo.productcode, 'Not Set') as dd_productcode,
       ifnull(fo.timee, '0001-01-01') as dd_timee,
       1 as dim_dateidtime,
       ifnull(fo.sfaweight, 0) as ct_sfaweight,
       ifnull(fo.kpiforecastacc, 0) as ct_kpiforecastacc,
       ifnull(fo.kpi1mbias, 0) as ct_kpi1mbias,
       ifnull(fo.kpi3mbias, 0) as ct_kpi3mbias,
       ifnull(fo.kpi6mbias, 0) as ct_kpi6mbias,
       ifnull(fo.kpi3msystematicbias, 0) as ct_kpi3msystematicbias,
       ifnull(fo.kpi6msystematicbias, 0) as ct_kpi6msystematicbias,
       ifnull(fo.sfaweight * fo.kpiforecastacc, 0) as ct_w_kpiforecastacc,
       ifnull(fo.sfaweight * fo.kpi1mbias, 0) as ct_w_kpi1mbias,
       ifnull(fo.sfaweight * fo.kpi3mbias, 0) as ct_w_kpi3mbias,
       ifnull(fo.sfaweight * fo.kpi6mbias, 0) as ct_w_kpi6mbias,
       ifnull(fo.sfaweight * fo.kpi3msystematicbias, 0) as ct_w_kpi3msystematicbias,
       ifnull(fo.sfaweight * fo.kpi6msystematicbias, 0) as ct_w_kpi6msystematicbias,
       ifnull(fo.businessdivision, 'Not Set') as dd_businessdivision,
       ifnull(fo.ysfabiasdescr, 'Not Set') as dd_ysfabiasdescr,
       'Not Set' as dd_countryid,
       ifnull(cm.blreportingregionlevel1, 'Not Set') as dd_blreportingregionlevel1,
       ifnull(cm.blreportingregionlevel2, 'Not Set') as dd_blreportingregionlevel2,
       ifnull(cm.blreportingregionlevel3, 'Not Set') as dd_blreportingregionlevel3,
       'Not Set' as dd_blreportingregionlevel4,
       'Not Set' as dd_blreportingregionlevel5,
       'Not Set' as dd_blreportingregionlevel6,
       'Not Set' as dd_blreportingregionlevel6,
       'Not Set' as dd_blreportingregionlevel7,
       'Not Set' as dd_scmbdctryorg,
       'Not Set' as dd_scmorgsuborg,
       'Not Set' as dd_sdlevel
FROM sap_ibp_fach_fo fo,
     tmp_max_load_date_fach_fo max_fo,
     (select distinct blreportingregionlevel1,blreportingregionlevel2,blreportingregionlevel3,countryplanninggroup,businessdivision,load_date
      from sap_ibp_fach_cm) cm,
     tmp_max_load_date_fach_cm max_cm
WHERE fo.load_date = max_fo.max_fo_load_date
      AND fo.timee = max_fo.timee
      AND fo.businessdivision = cm.businessdivision
      AND fo.countryplanninggroup = cm.countryplanninggroup
      AND cm.countryplanninggroup = max_cm.countryplanninggroup
      AND cm.businessdivision = max_cm.businessdivision
      AND cm.load_date = max_cm.max_cm_load_date;


UPDATE tmp_fact_forecastaccuracy f
SET f.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_forecastaccuracy f;

UPDATE tmp_fact_forecastaccuracy f
SET f.dim_dateidtime = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_forecastaccuracy f
WHERE dt.companycode = 'Not Set'
      AND substr(f.dd_timee, 0, 10) = dt.datevalue;

TRUNCATE TABLE fact_forecastaccuracy;
INSERT INTO fact_forecastaccuracy(
fact_forecastaccuracyid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_countryplanninggroup,
dd_businesslinedesc,
dd_productcode,
dd_timee,
dim_dateidtime,
ct_sfaweight,
ct_kpiforecastacc,
ct_kpi1mbias,
ct_kpi3mbias,
ct_kpi6mbias,
ct_kpi3msystematicbias,
ct_kpi6msystematicbias,
ct_w_kpiforecastacc,
ct_w_kpi1mbias,
ct_w_kpi3mbias,
ct_w_kpi6mbias,
ct_w_kpi3msystematicbias,
ct_w_kpi6msystematicbias,
dd_businessdivision,
dd_ysfabiasdescr,
dd_countryid,
dd_blreportingregionlevel1,
dd_blreportingregionlevel2,
dd_blreportingregionlevel3,
dd_blreportingregionlevel4,
dd_blreportingregionlevel5,
dd_blreportingregionlevel6,
dd_blreportingregionlevel7,
dd_reportingregion,
dd_scmbdctryorg,
dd_scmorgsuborg,
dd_sdlevel
)
SELECT fact_forecastaccuracyid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_countryplanninggroup,
       dd_businesslinedesc,
       dd_productcode,
       dd_timee,
       dim_dateidtime,
       ct_sfaweight,
       ct_kpiforecastacc,
       ct_kpi1mbias,
       ct_kpi3mbias,
       ct_kpi6mbias,
       ct_kpi3msystematicbias,
       ct_kpi6msystematicbias,
       ct_w_kpiforecastacc,
       ct_w_kpi1mbias,
       ct_w_kpi3mbias,
       ct_w_kpi6mbias,
       ct_w_kpi3msystematicbias,
       ct_w_kpi6msystematicbias,
       dd_businessdivision,
       dd_ysfabiasdescr,
       dd_countryid,
       dd_blreportingregionlevel1,
       dd_blreportingregionlevel2,
       dd_blreportingregionlevel3,
       dd_blreportingregionlevel4,
       dd_blreportingregionlevel5,
       dd_blreportingregionlevel6,
       dd_blreportingregionlevel7,
       dd_reportingregion,
       dd_scmbdctryorg,
       dd_scmorgsuborg,
       dd_sdlevel
FROM tmp_fact_forecastaccuracy;

DROP TABLE IF EXISTS tmp_fact_forecastaccuracy;

DELETE FROM emd586.fact_forecastaccuracy;
INSERT INTO emd586.fact_forecastaccuracy SELECT * FROM fact_forecastaccuracy;