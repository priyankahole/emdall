/*****************************************************************************************************************/
/*   Script         : exa_fact_writeoff                                                                          */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 25 Jan 2018                                                                                */
/*   Description    : Populating script of fact_writeoff                                                         */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   25 Jan 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/


DROP TABLE IF EXISTS tmp_fact_writeoff;
CREATE TABLE tmp_fact_writeoff
AS
SELECT *
FROM fact_writeoff
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_writeoff;
CREATE TABLE number_fountain_fact_writeoff LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_dsi
SELECT 'fact_writeoff', ifnull(max(fact_writeoffid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_writeoff;

INSERT INTO tmp_fact_writeoff(
fact_writeoffid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_repunit,
dd_position,
dd_product,
dd_auditid,
dd_version,
dd_calmonth,
dd_calyear,
dd_recordmode,
dd_calquarter,
amt_cs_trn_lc,
amt_cs_trn_gc,
amt_camntlc,
amt_camntgc,
amt_gcplr,
amt_cgcplr,
amt_gcpyr,
amt_cgcpyr,
amt_amtlyr,
amt_amtclyr,
amt_amtlcly,
amt_camtlyr,
dd_curkey_lc,
dd_curkey_gc,
dim_legalentityid,
dim_audithierarchyid,
dim_positionhierarchyid,
dim_ch_bwproducthierarchyid,
dim_dateid,
dd_country,
dd_region,
dd_countrycluster
)
SELECT (SELECT max_id from number_fountain_fact_dsi WHERE table_name = 'fact_dsi') + ROW_NUMBER() over(order by '') AS fact_dsiid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       bic_z_repunit as dd_repunit,
       bic_z_reppos as dd_position,
       bic_z_product as dd_product,
       bic_z_auditid as dd_auditid,
       bic_z_version as dd_version,
       calmonth as dd_calmonth,
       calyear as dd_calyear,
       recordmode as dd_recordmode,
       calquarter as dd_calquarter,
       cs_trn_lc as amt_cs_trn_lc,
       cs_trn_gc as amt_cs_trn_gc,
       bic_z_camntlc as amt_camntlc,
       bic_z_camntgc as amt_camntgc,
       bic_z_gcplr as amt_gcplr,
       bic_z_cgcplr as amt_cgcplr,
       bic_z_gcpyr as amt_gcpyr,
       bic_z_cgcpyr as amt_cgcpyr,
       bic_z_amtlyr as amt_amtlyr,
       bic_z_amtclyr as amt_amtclyr,
       bic_z_amtlcly as amt_amtlcly,
       bic_z_camtlyr as amt_camtlyr,
       curkey_lc as dd_curkey_lc,
       curkey_gc as dd_curkey_gc,
       1 as dim_legalentityid,
       1 as dim_audithierarchyid,
       1 as dim_positionhierarchyid,
       1 as dim_ch_bwproducthierarchyid,
       1 as dim_dateid,
       'Not Set' as dd_country,
       'Not Set' as dd_region,
       'Not Set' as dd_countrycluster
FROM BIC_AZCOFOPS200
WHERE BIC_Z_VERSION = 'A';


UPDATE tmp_fact_writeoff f
SET f.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_writeoff f;

DROP TABLE IF EXISTS tmp_writeoff_date;
CREATE TABLE tmp_writeoff_date
AS
SELECT dt.CALENDARMONTHID,
       min(dt.dim_dateid) as dim_dateid
FROM dim_date dt,
     tmp_fact_writeoff wo
WHERE dt.companycode = 'Not Set'
      AND dt.CALENDARMONTHID = wo.DD_CALMONTH
GROUP BY dt.CALENDARMONTHID;

UPDATE tmp_fact_writeoff wo
SET dim_dateid = ifnull(tmp.dim_dateid, 1)
FROM tmp_fact_writeoff wo,
     tmp_writeoff_date tmp
WHERE tmp.CALENDARMONTHID = wo.DD_CALMONTH;

DROP TABLE IF EXISTS tmp_writeoff_date;

UPDATE tmp_fact_writeoff tmp
SET dd_country = ifnull(rc.country_code, 'Not Set'),
    dd_region = ifnull(rc.region, 'Not Set'),
    dd_countrycluster = ifnull(rc.country_cluster, 'Not Set')
FROM tmp_fact_writeoff tmp,
     repunit_and_country rc
WHERE trim(leading '0' from tmp.dd_repunit) = trim(leading '0' from rc.reporting_unit);

/* Legal Entity */
UPDATE tmp_fact_writeoff f
SET f.dim_legalentityid = d.dim_legalentityid
FROM tmp_fact_writeoff f
     INNER JOIN dim_legalentity d ON d.le_code = f.dd_repunit
WHERE ifnull(f.dim_legalentityid,1) <> ifnull(d.dim_legalentityid,0);

/* Audit Hierarchy */
UPDATE tmp_fact_writeoff f
SET f.dim_audithierarchyid = d.dim_audithierarchyid
FROM tmp_fact_writeoff f
INNER JOIN dim_audithierarchy d ON d.nodename_l1 = f.dd_auditid
WHERE 1=1
      AND d.hieid = '4CKHZYP1PNYF0ZXMG9JCRMEDL'
      AND ifnull(f.dim_audithierarchyid,1) <> ifnull(d.dim_audithierarchyid,0);

/* Position Hierarchy */
UPDATE tmp_fact_writeoff f
SET f.dim_positionhierarchyid = d.dim_positionhierarchyid
FROM tmp_fact_writeoff f
     INNER JOIN dim_positionhierarchy d ON d.nodename_l1 = f.dd_position
WHERE d.HIEID = '5845BW8QVWD4NVGQU4MMHPC1O'
      AND ifnull(f.dim_positionhierarchyid,1) <> ifnull(d.dim_positionhierarchyid,0);

/* Product hierarchy */
UPDATE tmp_fact_writeoff f
SET f.dim_ch_bwproducthierarchyid = ifnull(d.dim_ch_bwproducthierarchyid,1)
FROM tmp_fact_writeoff f
     INNER JOIN dim_ch_bwproducthierarchy d
         ON d.businessline = f.dd_product
	        AND f.dd_calmonth between replace(substr(d.UPPERHIERSTARTDATE,1,7),'-','')
			AND replace(substr(d.UPPERHIERENDDATE,1,7),'-','')
WHERE ifnull(f.dim_ch_bwproducthierarchyid,1) <> ifnull(d.dim_ch_bwproducthierarchyid,0);

TRUNCATE TABLE fact_writeoff;
INSERT INTO fact_writeoff(
fact_writeoffid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_repunit,
dd_position,
dd_product,
dd_auditid,
dd_version,
dd_calmonth,
dd_calyear,
dd_recordmode,
dd_calquarter,
amt_cs_trn_lc,
amt_cs_trn_gc,
amt_camntlc,
amt_camntgc,
amt_gcplr,
amt_cgcplr,
amt_gcpyr,
amt_cgcpyr,
amt_amtlyr,
amt_amtclyr,
amt_amtlcly,
amt_camtlyr,
dd_curkey_lc,
dd_curkey_gc,
dim_legalentityid,
dim_audithierarchyid,
dim_positionhierarchyid,
dim_ch_bwproducthierarchyid,
dim_dateid,
dd_country,
dd_region,
dd_countrycluster)
SELECT fact_writeoffid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       ifnull(dd_repunit, 'Not Set'),
       ifnull(dd_position, 'Not Set'),
       ifnull(dd_product, 'Not Set'),
       ifnull(dd_auditid, 'Not Set'),
       ifnull(dd_version, 'Not Set'),
       ifnull(dd_calmonth, 'Not Set'),
       ifnull(dd_calyear, 'Not Set'),
       ifnull(dd_recordmode, 'Not Set'),
       ifnull(dd_calquarter, 'Not Set'),
       ifnull(amt_cs_trn_lc, 0),
       ifnull(amt_cs_trn_gc, 0),
       ifnull(amt_camntlc, 0),
       ifnull(amt_camntgc, 0),
       ifnull(amt_gcplr, 0),
       ifnull(amt_cgcplr, 0),
       ifnull(amt_gcpyr, 0),
       ifnull(amt_cgcpyr, 0),
       ifnull(amt_amtlyr, 0),
       ifnull(amt_amtclyr, 0),
       ifnull(amt_amtlcly, 0),
       ifnull(amt_camtlyr, 0),
       ifnull(dd_curkey_lc, 'Not Set'),
       ifnull(dd_curkey_gc, 'Not Set'),
       ifnull(dim_legalentityid, 1),
       ifnull(dim_audithierarchyid, 1),
       ifnull(dim_positionhierarchyid, 1),
       ifnull(dim_ch_bwproducthierarchyid, 1),
       ifnull(dim_dateid, 1),
       ifnull(dd_country, 'Not Set'),
       ifnull(dd_region, 'Not Set'),
       ifnull(dd_countrycluster, 'Not Set')
FROM tmp_fact_writeoff;

DROP TABLE IF EXISTS tmp_fact_writeoff;

/* 06112018 CarmenF change amt values according to the new exchange rate */

update fact_writeoff
  set
      amt_CS_TRN_GC = amt_CS_TRN_LC * usd_rate,
      amt_CAMNTGC = amt_CAMNTLC * usd_rate,
      amt_AMTCLYR = amt_AMTLYR * usd_rate,
      amt_CAMTLYR = amt_AMTLCLY * usd_rate,
      amt_GCPLR = amt_CS_TRN_LC * usd_rate,
      amt_CGCPLR = amt_CAMNTLC * usd_rate
  from fact_writeoff f, roda_fx_rates, dim_Date d
  where dd_CURKEY_LC = iso_code
  and period = dd_calmonth
  and d.dim_Dateid = f.dim_Dateid and d.datevalue >= '2019-01-01';

update fact_writeoff
 set dd_curkey_gc = 'USD'
from fact_writeoff f,  dim_Date d
where
 d.dim_Dateid = f.dim_Dateid and d.datevalue >= '2019-01-01';

/* END 06112018 CarmenF change amt values according to the new exchange rate */

DELETE FROM emd586.fact_writeoff;
INSERT INTO emd586.fact_writeoff SELECT * FROM fact_writeoff;