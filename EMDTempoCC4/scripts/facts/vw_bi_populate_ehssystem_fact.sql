/******************************************************************************************************************/
/*   Script         : bi_populate_ehssystem_fact                                                                  */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 10 Nov 2016                                                                                 */
/*   Description    : Populating script of fact_ehssystem                                                         */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   10 Nov 2016         Cristian T      1.0               Creating the script.                                   */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_ehssystem;
CREATE TABLE tmp_fact_ehssystem
AS
SELECT *
FROM fact_ehssystem
WHERE 1 = 0;

UPDATE ehs_system ehs
SET ehs.eventtype = 'Not Set'
WHERE ehs.eventtype = '';

UPDATE ehs_system ehs
SET ehs."domain" = 'Not Set'
WHERE ehs."domain" = '';

UPDATE ehs_system ehs
SET ehs.eventdate = '0001-01-01'
WHERE ehs.eventdate = '';

UPDATE ehs_system ehs
SET ehs.department = 'Not Set'
WHERE ehs.department = '';

UPDATE ehs_system ehs
SET ehs.sector = 'Not Set'
WHERE ehs.sector = '';

UPDATE ehs_system ehs
SET ehs.title = 'Not Set'
WHERE ehs.title = '';

UPDATE ehs_system ehs
SET ehs.reportingcategory = 'Not Set'
WHERE ehs.reportingcategory = '';

UPDATE ehs_system ehs
SET ehs.gravitylevel = 'Not Set'
WHERE ehs.gravitylevel = '';

UPDATE ehs_system ehs
SET ehs.link = 'Not Set'
WHERE ehs.link = '';

UPDATE ehs_system ehs
SET ehs.actionnumber = 'Not Set'
WHERE ehs.actionnumber = '';

UPDATE ehs_system ehs
SET ehs."year" = 'Not Set'
WHERE ehs."year" = '';

UPDATE ehs_system ehs
SET ehs.eventstatus = 'Not Set'
WHERE ehs.eventstatus = '';

UPDATE ehs_system ehs
SET ehs.communication = 'Not Set'
WHERE ehs.communication = '';

UPDATE ehs_system ehs
SET ehs.remark = 'Not Set'
WHERE ehs.remark = '';

INSERT INTO tmp_fact_ehssystem(
fact_ehssystemid,
dd_eventtype,
dd_domain,
dim_dateidevent,
dd_department,
dd_sector,
dd_title,
dd_reportingcategory,
dd_gravitylevel,
dd_link,
dd_actionnumber,
dd_detail,
dd_year,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dd_dangertype,
dd_actionstatus,
dd_efficacity_check_comment,
dd_efficacity_check,
dd_eventstatus,
dd_communication,
dd_remark
)
SELECT ROW_NUMBER() over(order by '') AS fact_ehssystemid,
       t.*
FROM (
SELECT DISTINCT ifnull(ehs.eventtype, 'Not Set') as dd_eventtype,
       ifnull(ehs."domain", 'Not Set') as dd_domain,
       ifnull(dd.dim_dateid, 1) as dim_dateidevent,
       ifnull(ehs.department, 'Not Set') as dd_department,
       ifnull(ehs.sector, 'Not Set') as dd_sector,
       ifnull(ehs.title, 'Not Set') as dd_title,
       ifnull(ehs.reportingcategory, 'Not Set') as dd_reportingcategory,
       ifnull(ehs.gravitylevel, 'Not Set') as dd_gravitylevel,
       ifnull(ehs.link, 'Not Set') as dd_link,
       ifnull(ehs.actionnumber, 'Not Set') as dd_actionnumber,
       'Not Set' as dd_detail,
       ifnull(ehs."year", 'Not Set') as dd_year,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       ifnull(danger_type, 'Not Set') as dd_dangertype,
       'Not Set' as dd_actionstatus,
       'Not Set' as dd_efficacity_check_comment,
       'Not Set' as dd_efficacity_check,
       ifnull(ehs.eventstatus, 'Not Set') as dd_eventstatus,
       ifnull(ehs.communication, 'Not Set') as dd_communication,
       ifnull(ehs.remark, 'Not Set') as dd_remark
FROM ehs_system ehs
     LEFT JOIN (SELECT dim_dateid, datevalue FROM dim_date WHERE companycode = 'Not Set') dd
         ON dd.datevalue = to_date(ehs.eventdate, 'MM/DD/YYYY')
      ) t;

UPDATE tmp_fact_ehssystem tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM tmp_fact_ehssystem tmp,
     dim_projectsource prj;

DELETE FROM fact_ehssystem;

INSERT INTO fact_ehssystem(
fact_ehssystemid,
dd_eventtype,
dd_domain,
dim_dateidevent,
dd_department,
dd_sector,
dd_title,
dd_reportingcategory,
dd_gravitylevel,
dd_link,
dd_actionnumber,
dd_detail,
dd_year,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dd_dangertype,
dd_actionstatus,
dd_efficacity_check_comment,
dd_efficacity_check,
dd_eventstatus,
dd_communication,
dd_remark
)
SELECT fact_ehssystemid,
       dd_eventtype,
       dd_domain,
       dim_dateidevent,
       dd_department,
       dd_sector,
       dd_title,
       dd_reportingcategory,
       dd_gravitylevel,
       dd_link,
       dd_actionnumber,
       dd_detail,
       dd_year,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dd_dangertype,
       dd_actionstatus,
       dd_efficacity_check_comment,
       dd_efficacity_check,
       dd_eventstatus,
       dd_communication,
       dd_remark
FROM tmp_fact_ehssystem;

DROP TABLE IF EXISTS tmp_fact_ehssystem;
