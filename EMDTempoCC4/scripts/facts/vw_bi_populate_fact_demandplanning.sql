/* ################################################################################################################## */
/* */
/*   Script         : bi_populate_fact_demandplanning */
/*   Author         : Roxana */
/*   Created On     : Feb 2017 */


delete from tmp_fact_demandplanning;

/* Insert OP data from excel files */

DROP TABLE IF EXISTS max_holder_701;
CREATE TABLE max_holder_701
AS
SELECT ifnull(max(fact_demandplanningid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) maxid
FROM fact_demandplanning;


insert into tmp_fact_demandplanning 
(fact_demandplanningid,
dd_Product,
dd_StrategicBusinessUnit,
dd_businesslinecode,
dd_BusinessUnit,
dd_businessfieldcode,
dd_KeyCountry,
dd_Calmonth,
dd_Currency,
dd_LocalCurrency,
dd_unitofmeasure,
ct_OperativePlan_GC,
ct_OperativePlan_LC,
ct_Quantity,
dd_datasource
)
select max_holder_701.maxid + row_number() over(order by '') as fact_demandplanningid,
	ifnull(trim (leading '0' from material),'Not Set') as dd_Product,
	ifnull(StrategicBusinessUnit,'Not Set') as dd_StrategicBusinessUnit,
	ifnull(PRODUCT_Z_WW020,'Not Set') as dd_businesslinecode,
	ifnull(PRODUCT_Z_WW025,'Not Set') as dd_BusinessUnit,
	ifnull(PRODUCT_Z_WW027,'Not Set') as dd_businessfieldcode,
	ifnull(KeyCountry,'Not Set') as dd_KeyCountry,
	ifnull(Calendar,'Not Set') as dd_Calmonth,
	ifnull(GroupCurrency,'Not Set') as dd_Currency,
	ifnull(LocalCurrency,'Not Set') as dd_LocalCurrency,
	ifnull(unitofmeasure,'Not Set') as dd_unitofmeasure,
	ifnull(OperativePlan_GC,0) as ct_OperativePlan_GC,
	ifnull(OperativePlan_LC,0) as ct_OperativePlan_LC,
	ifnull(Quantity,0) as ct_Quantity,
	'OP' as dd_datasource
from OP_Data_Upload_CSV
cross join max_holder_701;


/* MDG Part */


UPDATE tmp_fact_demandplanning f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM dim_mdg_part tmp, tmp_fact_demandplanning f
WHERE trim(leading '0' from f.dd_Product ) = trim (leading '0' from tmp.partnumber);


  
 update tmp_fact_demandplanning f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyapoid
from tmp_fact_demandplanning f, dim_mdg_part dp,  dim_bwproducthierarchyapo bw
where f.dim_mdg_partid = dp.dim_mdg_partid
and dp.glbproductgroup = right (bw.productgroup ,3)
and to_date('2017-12-28') between UPPERHIERSTARTDATE and UPPERHIERENDDATE; 
  
  
/* Insert data from APO */

DROP TABLE IF EXISTS max_holder_701;
CREATE TABLE max_holder_701
AS
SELECT ifnull(max(fact_demandplanningid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) maxid
FROM tmp_fact_demandplanning;


insert into  tmp_fact_demandplanning 
(fact_demandplanningid,
dd_Product,
dd_Calmonth,
dd_KeyCountry,
dd_BusinessUnit,
dd_KeyCustomer,
ct_OrderIntake,
ct_ConsensusPlan,
dim_mdg_partid,
dim_bwproducthierarchyid,
ct_OrderIntakeNV,
ct_consensusplanNV,
dd_BaseUnitofMeasure,
dim_snapshotdateid,
dd_datasource,
ct_OrderIntakeNVnew,
ct_AdjustedOrderIntake,
ct_ApprovedConsensusBaseline,
ct_ApprovedOneTimeEvent,
ct_ApprovedConsensusBaselineNV,
ct_StatisticalForecastBaseline,
ct_StatisticalForecastBaselineNV,
ct_RegionalBaseline,
ct_RegionalBaselineNV,
ct_SalesBaseline,
ct_SalesBaselineNV,
ct_MarketingBaseline,
ct_MarketingBaselineNV,
dd_Freetextattrib1,
dd_Freetextattrib2,
dd_Freetextattrib3,
dd_Freetextattrib4,
dd_ABCclassification,
dd_XYZclassification
)
select  max_holder_701.maxid + row_number() over(order by '') as fact_demandplanningid,
dd_Product,
dd_Calmonth,
dd_KeyCountry,
dd_BusinessUnit,
dd_KeyCustomer,
ct_OrderIntake,
ct_ConsensusPlan,
dim_mdg_partid,
dim_bwproducthierarchyid,
ct_OrderIntakeNV,
ct_consensusplanNV,
dd_BaseUnitofMeasure,
dim_snapshotdateid,
'APO' as dd_datasource,
ct_OrderIntakeNVnew,
ct_AdjustedOrderIntake,
ct_ApprovedConsensusBaseline,
ct_ApprovedOneTimeEvent,
ct_ApprovedConsensusBaselineNV,
ct_StatisticalForecastBaseline,
ct_StatisticalForecastBaselineNV,
ct_RegionalBaseline,
ct_RegionalBaselineNV,
ct_SalesBaseline,
ct_SalesBaselineNV,
ct_MarketingBaseline,
ct_MarketingBaselineNV,
dd_Freetextattrib1,
dd_Freetextattrib2,
dd_Freetextattrib3,
dd_Freetextattrib4,
dd_ABCclassification,
dd_XYZclassification
from EMD586.fact_APOForecastAnalytics
cross join max_holder_701
where dd_currentsnaphotdate = 'Yes';



/* Consensus Plan and Order Intake calculated in KG */

DELETE FROM tmp_MARM;
INSERT INTO tmp_MARM (MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH)
SELECT MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH FROM EMDEmerald1FD.MARM
UNION ALL 
SELECT MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH FROM EMDPhoenixDAD.MARM
UNION ALL 
SELECT MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH FROM EMDSpain8F2.MARM
UNION ALL 
SELECT MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH FROM EMDTempoCC4.MARM
UNION ALL 
SELECT MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH FROM EMDTempoLA57F.MARM
UNION ALL 
SELECT MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH FROM EMDTempoNA801.MARM
UNION ALL 
SELECT MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH FROM EMDTrio41B.MARM
UNION ALL 
SELECT MARM_MATNR,MARM_UMREN,MARM_UMREZ,MARM_MEINH FROM EMDQuattro51F.MARM;

DROP TABLE IF EXISTS tmp2_MARM;
CREATE TABLE tmp2_MARM AS SELECT DISTINCT MARM_MATNR,max(MARM_UMREN)MARM_UMREN,max(MARM_UMREZ) MARM_UMREZ ,MARM_MEINH FROM tmp_MARM
group by MARM_MATNR,MARM_MEINH;


UPDATE tmp_fact_demandplanning dp
SET ct_OrderIntakeKG =  ct_OrderIntake,					
    ct_consensusplanKG = ct_consensusplan						
FROM tmp_fact_demandplanning dp
WHERE dd_baseunitofmeasure = 'KG' ;


UPDATE tmp_fact_demandplanning dp
SET ct_OrderIntakeKG = (MARM_UMREN/MARM_UMREZ) * ct_OrderIntake,
    ct_consensusplanKG = (MARM_UMREN/MARM_UMREZ) * ct_consensusplan
FROM tmp_fact_demandplanning dp,tmp2_MARM tmp
WHERE tmp.MARM_MATNR = dp.dd_product
     AND tmp.MARM_MEINH = 'KG'
	and dd_baseunitofmeasure not in ('KG','Not Set');

	
UPDATE tmp_fact_demandplanning dp
SET ct_OrderIntakeKG = ct_OrderIntake / 1000,
    ct_consensusplanKG = ct_consensusplan / 1000
FROM tmp_fact_demandplanning dp
where dd_BaseUnitofMeasure = 'G';



/* Key Country Names Mapping */

update tmp_fact_demandplanning
set dd_KeyCountryDescription = ifnull(case when dd_KeyCountry = 'CN' then 'China'
									when dd_KeyCountry = 'HK' then 'Hong Kong'
									when dd_KeyCountry = 'IN' then 'India'
									when dd_KeyCountry = 'JP' then 'Japan'
									when dd_KeyCountry = 'KR' then 'South Korea'
									when dd_KeyCountry = 'TW' then 'Taiwan'
									when dd_KeyCountry = 'BR' then 'Brazil'
									when dd_KeyCountry = 'MX' then 'Mexico'
									when dd_KeyCountry = 'B1' then 'GSC Europe North'
									when dd_KeyCountry = 'B2' then 'GSC Europe South'
									when dd_KeyCountry = 'B3' then 'GSC Americas North'
									when dd_KeyCountry = 'B4' then 'ISC Europe'
									when dd_KeyCountry = 'B5' then 'ISC Americas'
									when dd_KeyCountry = 'B6' then 'ISC Near Middleeast'
									when dd_KeyCountry = 'B7' then 'ISC Africa'
									when dd_KeyCountry = 'B8' then 'ISC Asia Pacific'
									when dd_KeyCountry = 'B9' then 'ISC Africa Near Middleeast'
									when dd_KeyCountry = 'B10' then 'China/Hong Kong' 
								end,'Not Set');

	
	
/* Final insert */								
								
	
					
UPDATE fact_demandplanning dp           
SET dd_currentsnapshotdate = 'No'
WHERE dd_currentsnapshotdate <> 'No';

delete from fact_demandplanning where dd_currentsnapshotdate = 'No';
								

insert into fact_demandplanning 
(FACT_DEMANDPLANNINGID, 
	DD_PRODUCT, 
	DD_CURRENCY,
	dd_LocalCurrency,	
	DD_CALMONTH, 
	DD_SHIPFROMLOCATION, 
	DD_KEYCUSTOMER, 
	DD_KEYCOUNTRY, 
	DD_KEYCOUNTRYDESCRIPTION, 
	DD_BUSINESSUNIT, 
	DD_UNITOFMEASURE, 
	CT_OPERATIVEPLAN_GC, 
	CT_OPERATIVEPLAN_LC, 
	CT_QUANTITY, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	AMT_EXCHANGERATE, 
	AMT_EXCHANGERATE_GBL, 
	DIM_PROJECTSOURCEID, 
	CT_ORDERINTAKE, 
	CT_CONSENSUSPLAN, 
	DIM_MDG_PARTID, 
	DIM_BWPRODUCTHIERARCHYID, 
	CT_ORDERINTAKENV, 
	CT_CONSENSUSPLANNV, 
	DD_BASEUNITOFMEASURE, 
	CT_ORDERINTAKEKG, 
	CT_CONSENSUSPLANKG, 
	DIM_SNAPSHOTDATEID, 
	DD_CURRENTSNAPSHOTDATE,
	dd_StrategicBusinessUnit,
	dd_businesslinecode,
	dd_businessfieldcode,
	dd_datasource,
	ct_OrderIntakeNVnew,
	ct_AdjustedOrderIntake,
	ct_ApprovedConsensusBaseline,
	ct_ApprovedOneTimeEvent,
	ct_ApprovedConsensusBaselineNV,
	ct_StatisticalForecastBaseline,
	ct_StatisticalForecastBaselineNV,
	ct_RegionalBaseline,
	ct_RegionalBaselineNV,
	ct_SalesBaseline,
	ct_SalesBaselineNV,
	ct_MarketingBaseline,
	ct_MarketingBaselineNV,
	dd_Freetextattrib1,
	dd_Freetextattrib2,
	dd_Freetextattrib3,
	dd_Freetextattrib4,
	dd_ABCclassification,
	dd_XYZclassification
)
select 
FACT_DEMANDPLANNINGID as FACT_DEMANDPLANNINGID, 
	DD_PRODUCT, 
	DD_CURRENCY, 
	dd_LocalCurrency,
	DD_CALMONTH, 
	DD_SHIPFROMLOCATION, 
	DD_KEYCUSTOMER, 
	DD_KEYCOUNTRY, 
	DD_KEYCOUNTRYDESCRIPTION, 
	DD_BUSINESSUNIT, 
	DD_UNITOFMEASURE, 
	CT_OPERATIVEPLAN_GC, 
	CT_OPERATIVEPLAN_LC, 
	CT_QUANTITY, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	AMT_EXCHANGERATE, 
	AMT_EXCHANGERATE_GBL, 
	DIM_PROJECTSOURCEID, 
	CT_ORDERINTAKE, 
	CT_CONSENSUSPLAN, 
	DIM_MDG_PARTID, 
	DIM_BWPRODUCTHIERARCHYID, 
	CT_ORDERINTAKENV, 
	CT_CONSENSUSPLANNV, 
	DD_BASEUNITOFMEASURE, 
	CT_ORDERINTAKEKG, 
	CT_CONSENSUSPLANKG, 
	DIM_SNAPSHOTDATEID, 
	'Yes' as DD_CURRENTSNAPSHOTDATE,
	dd_StrategicBusinessUnit,
	dd_businesslinecode,
	dd_businessfieldcode,
	dd_datasource,
	ct_OrderIntakeNVnew,
	ct_AdjustedOrderIntake,
	ct_ApprovedConsensusBaseline,
	ct_ApprovedOneTimeEvent,
	ct_ApprovedConsensusBaselineNV,
	ct_StatisticalForecastBaseline,
	ct_StatisticalForecastBaselineNV,
	ct_RegionalBaseline,
	ct_RegionalBaselineNV,
	ct_SalesBaseline,
	ct_SalesBaselineNV,
	ct_MarketingBaseline,
	ct_MarketingBaselineNV,
	dd_Freetextattrib1,
	dd_Freetextattrib2,
	dd_Freetextattrib3,
	dd_Freetextattrib4,
	dd_ABCclassification,
	dd_XYZclassification
from tmp_fact_demandplanning ;



/* Insert into EMD586 */

	
UPDATE EMD586.fact_demandplanning dp           
SET dd_currentsnapshotdate = 'No'
WHERE dd_currentsnapshotdate <> 'No';

	
insert into EMD586.fact_demandplanning
(FACT_DEMANDPLANNINGID, 
	DD_PRODUCT, 
	DD_CURRENCY,
	dd_LocalCurrency,	
	DD_CALMONTH, 
	DD_SHIPFROMLOCATION, 
	DD_KEYCUSTOMER, 
	DD_KEYCOUNTRY, 
	DD_KEYCOUNTRYDESCRIPTION, 
	DD_BUSINESSUNIT, 
	DD_UNITOFMEASURE, 
	CT_OPERATIVEPLAN_GC, 
	CT_OPERATIVEPLAN_LC, 
	CT_QUANTITY, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	AMT_EXCHANGERATE, 
	AMT_EXCHANGERATE_GBL, 
	DIM_PROJECTSOURCEID, 
	CT_ORDERINTAKE, 
	CT_CONSENSUSPLAN, 
	DIM_MDG_PARTID, 
	DIM_BWPRODUCTHIERARCHYID, 
	CT_ORDERINTAKENV, 
	CT_CONSENSUSPLANNV, 
	DD_BASEUNITOFMEASURE, 
	CT_ORDERINTAKEKG, 
	CT_CONSENSUSPLANKG, 
	DIM_SNAPSHOTDATEID, 
	DD_CURRENTSNAPSHOTDATE,
	dd_StrategicBusinessUnit,
	dd_businesslinecode,
	dd_businessfieldcode,
	dd_datasource,
	ct_AdjustedOrderIntake,
	ct_ApprovedConsensusBaseline,
	ct_ApprovedOneTimeEvent,
	ct_ApprovedConsensusBaselineNV,
	ct_StatisticalForecastBaseline,
	ct_StatisticalForecastBaselineNV,
	ct_RegionalBaseline,
	ct_RegionalBaselineNV,
	ct_SalesBaseline,
	ct_SalesBaselineNV,
	ct_MarketingBaseline,
	ct_MarketingBaselineNV,
	ct_OrderIntakeNVnew,
	dd_Freetextattrib1,
	dd_Freetextattrib2,
	dd_Freetextattrib3,
	dd_Freetextattrib4,
	dd_ABCclassification,
	dd_XYZclassification)
	select 
	FACT_DEMANDPLANNINGID, 
	DD_PRODUCT, 
	DD_CURRENCY, 
	dd_LocalCurrency,
	DD_CALMONTH, 
	DD_SHIPFROMLOCATION, 
	DD_KEYCUSTOMER, 
	DD_KEYCOUNTRY, 
	DD_KEYCOUNTRYDESCRIPTION, 
	DD_BUSINESSUNIT, 
	DD_UNITOFMEASURE, 
	CT_OPERATIVEPLAN_GC, 
	CT_OPERATIVEPLAN_LC, 
	CT_QUANTITY, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	AMT_EXCHANGERATE, 
	AMT_EXCHANGERATE_GBL, 
	DIM_PROJECTSOURCEID, 
	CT_ORDERINTAKE, 
	CT_CONSENSUSPLAN, 
	DIM_MDG_PARTID, 
	DIM_BWPRODUCTHIERARCHYID, 
	CT_ORDERINTAKENV, 
	CT_CONSENSUSPLANNV, 
	DD_BASEUNITOFMEASURE, 
	CT_ORDERINTAKEKG, 
	CT_CONSENSUSPLANKG, 
	DIM_SNAPSHOTDATEID, 
	'Yes' as DD_CURRENTSNAPSHOTDATE,
	dd_StrategicBusinessUnit,
	dd_businesslinecode,
	dd_businessfieldcode,
	dd_datasource,
	ct_AdjustedOrderIntake,
	ct_ApprovedConsensusBaseline,
	ct_ApprovedOneTimeEvent,
	ct_ApprovedConsensusBaselineNV,
	ct_StatisticalForecastBaseline,
	ct_StatisticalForecastBaselineNV,
	ct_RegionalBaseline,
	ct_RegionalBaselineNV,
	ct_SalesBaseline,
	ct_SalesBaselineNV,
	ct_MarketingBaseline,
	ct_MarketingBaselineNV,
	ct_OrderIntakeNVnew,
	dd_Freetextattrib1,
	dd_Freetextattrib2,
	dd_Freetextattrib3,
	dd_Freetextattrib4,
	dd_ABCclassification,
	dd_XYZclassification
from tmp_fact_demandplanning;
	

