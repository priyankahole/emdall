/* ############################################################################# */
/*   Script         : bi_populate_mango_fact */
/*   Author         : Cornelia */
/*   Created On     : 10 Feb 2017 */
/*   Description    : Stored Proc bi_populate_mango_fact  */
/*   Change History */
/*   Date            By         Version           Desc */
/*  */
delete from NUMBER_FOUNTAIN where table_name = 'tmp_fact_MANGO';	


INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_MANGO',IFNULL(MAX(fact_MANGOID),0)
FROM tmp_fact_MANGO;

TRUNCATE TABLE tmp_fact_mango;


INSERT INTO tmp_fact_mango( FACT_MANGOID, 
	dim_qualityusersid,
    dim_datedocumentstatusid,
	DIM_DATEORIGINALCREATIONID, 
	DIM_DATEPERIODICREVIEWSTARTID, 
	DIM_DATEEFECTSTARTID, 
	DIM_DATEPERIODICREVIEWNOTIFYID, 
	DD_PERIODICREVIEWINTERVAL, 
	DIM_DATELASTREVIEWID, 
	DIM_DATENEXTREVIEWID, 
	DIM_DATEPLANNEDEFFECTIVEID, 
	DD_ENGLISHTITLE, 
	DD_AUTHORSITE, 
	DD_SYSTEMNAME, 
	DIM_DATEREVIEWID, 
	DD_AUTHORS, 
	DD_VERSIONLABEL, 
	AMT_EXCHANGERATE_GBL, 
	AMT_EXCHANGERATE, 
	DIM_CURRENCYID_TRA, 
	DIM_CURRENCYID_GBL, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	DIM_PROJECTSOURCEID, 
	DD_ROBJECTID, 
	DD_OBJECT_NAME, 
	DD_TITLE, 
	DD_DOCUMENT_TYPE, 
	DD_DOCUMENT_SUBTYPE, 
	DD_DOCUMENT_UNIT, 
	DD_LEGACY_DOCUMENT, 
	dd_original_creation_date,
    dd_DOCUMENT_STATUS_DATE,
	dd_PERIODIC_REVIEW_START_DATE,
	dd_EFFECT_START,
	dd_PERIODIC_REVIEW_NOTIFY_DATE,
	dd_LAST_REVIEW_DATE,
	dd_NEXT_REVIEW_DATE,
	dd_PLANNED_EFFECTIVE_DATE,
	dd_REVIEW_DATE,
    DD_DOCUMENT_STATUS,
	DD_STATUS,
	DD_SOURCE_VIEW)
SELECT 
IFNULL((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'tmp_fact_MANGO'),0)
          + row_number() over(order by '') fact_mangoID,
    1 dim_qualityusersid,
	1 dim_datedocumentstatusid,
	1 DIM_DATEORIGINALCREATIONID, 
	1 DIM_DATEPERIODICREVIEWSTARTID, 
	1 DIM_DATEEFECTSTARTID, 
	1 DIM_DATEPERIODICREVIEWNOTIFYID, 
	IFNULL(RW.PERIODIC_REVIEW_INTERVAL,'Not Set'), 
	1 DIM_DATELASTREVIEWID, 
	1 DIM_DATENEXTREVIEWID, 
	1 DIM_DATEPLANNEDEFFECTIVEID, 
	IFNULL(RW.ENGLISH_TITLE,'Not Set'), 
	IFNULL(RW.AUTHOR_SITE,'Not Set'), 
	IFNULL(RW.SYSTEM_NAME,'Not Set'), 
	1 DIM_DATEREVIEWID, 
	IFNULL(RW.AUTHORS,'Not Set'), 
	IFNULL(RW.R_VERSION_LABEL,'Not Set'), 
	1 AMT_EXCHANGERATE_GBL, 
	1 AMT_EXCHANGERATE, 
	1 DIM_CURRENCYID_TRA, 
	1 DIM_CURRENCYID_GBL, 
	current_date DW_INSERT_DATE, 
	current_date DW_UPDATE_DATE, 
	1, 
	IFNULL(M.R_OBJECT_ID,'Not Set'), 
	IFNULL(M.OBJECT_NAME,'Not Set'), 
	IFNULL(RW.TITLE,'Not Set'), 
	IFNULL(RW.DOCUMENT_TYPE,'Not Set'), 
	IFNULL(RW.DOCUMENT_SUBTYPE,'Not Set'), 
	IFNULL(RW.DOCUMENT_UNIT,'Not Set'), 
	IFNULL(RW.LEGACY_DOCUMENT,'Not Set'),
    IFNULL(RW.original_creation_date,'0001-01-01') dd_original_creation_date,
    IFNULL(RW.DOCUMENT_STATUS_DATE,'0001-01-01') dd_DOCUMENT_STATUS_DATE,
	IFNULL(RW.PERIODIC_REVIEW_START_DATE,'0001-01-01') dd_PERIODIC_REVIEW_START_DATE,
	IFNULL(RW.EFFECT_START,'0001-01-01') dd_EFFECT_START,
	IFNULL(RW.PERIODIC_REVIEW_NOTIFY_DATE,'0001-01-01') dd_PERIODIC_REVIEW_NOTIFY_DATE,
	IFNULL(RW.LAST_REVIEW_DATE,'0001-01-01') dd_LAST_REVIEW_DATE,
	IFNULL(RW.NEXT_REVIEW_DATE,'0001-01-01') dd_NEXT_REVIEW_DATE,
	IFNULL(RW.PLANNED_EFFECTIVE_DATE,'0001-01-01') dd_PLANNED_EFFECTIVE_DATE,
	IFNULL(RW.REVIEW_DATE,'0001-01-01') dd_REVIEW_DATE,
    IFNULL(RW.DOCUMENT_STATUS,'Not Set'),
	IFNULL(RW.STATUS,'Not Set'),
	'Pending For Review' AS DD_SOURCE_VIEW
FROM  MSV_MANGO_DOC_TO_REVIEW RW
LEFT JOIN  MSVT_DM_MAINVIEW_AUB_VEV m  ON 
RW.OBJECT_NAME  = M.OBJECT_NAME
and RW.authors  = M.authors
and RW.r_version_label  = M.r_version_label;

delete from NUMBER_FOUNTAIN where table_name = 'tmp_fact_MANGO';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_MANGO',IFNULL(MAX(fact_MANGOID),0)
FROM tmp_fact_MANGO;


INSERT INTO tmp_fact_mango( FACT_MANGOID, 
    dim_qualityusersid,
    dim_datedocumentstatusid,
	DIM_DATEORIGINALCREATIONID, 
	DIM_DATEPERIODICREVIEWSTARTID, 
	DIM_DATEEFECTSTARTID, 
	DIM_DATEPERIODICREVIEWNOTIFYID, 
	DD_PERIODICREVIEWINTERVAL, 
	DIM_DATELASTREVIEWID, 
	DIM_DATENEXTREVIEWID, 
	DIM_DATEPLANNEDEFFECTIVEID, 
	DD_ENGLISHTITLE, 
	DD_AUTHORSITE, 
	DD_SYSTEMNAME, 
	DIM_DATEREVIEWID, 
	DD_AUTHORS, 
	DD_VERSIONLABEL, 
	AMT_EXCHANGERATE_GBL, 
	AMT_EXCHANGERATE, 
	DIM_CURRENCYID_TRA, 
	DIM_CURRENCYID_GBL, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	DIM_PROJECTSOURCEID, 
	DD_ROBJECTID, 
	DD_OBJECT_NAME, 
	DD_TITLE, 
	DD_DOCUMENT_TYPE, 
	DD_DOCUMENT_SUBTYPE, 
	DD_DOCUMENT_UNIT, 
	DD_LEGACY_DOCUMENT, 
	dd_original_creation_date,
    dd_DOCUMENT_STATUS_DATE,
	dd_PERIODIC_REVIEW_START_DATE,
	dd_EFFECT_START,
	dd_PERIODIC_REVIEW_NOTIFY_DATE,
	dd_LAST_REVIEW_DATE,
	dd_NEXT_REVIEW_DATE,
	dd_PLANNED_EFFECTIVE_DATE,
	dd_REVIEW_DATE,
    DD_DOCUMENT_STATUS,
	DD_STATUS,
	DD_SOURCE_VIEW)
SELECT 
IFNULL((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'TMP_fact_mango'),0)
          + row_number() over(order by '') fact_mangoID,
    1 dim_qualityusersid,
    1 dim_datedocumentstatusid,
	1 DIM_DATEORIGINALCREATIONID, 
	1 DIM_DATEPERIODICREVIEWSTARTID, 
	1 DIM_DATEEFECTSTARTID, 
	1 DIM_DATEPERIODICREVIEWNOTIFYID, 
	IFNULL(LT.PERIODIC_REVIEW_INTERVAL,'Not Set'), 
	1 DIM_DATELASTREVIEWID, 
	1 DIM_DATENEXTREVIEWID, 
	1 DIM_DATEPLANNEDEFFECTIVEID, 
	IFNULL(LT.ENGLISH_TITLE,'Not Set'), 
	IFNULL(LT.AUTHOR_SITE,'Not Set'), 
	IFNULL(LT.SYSTEM_NAME,'Not Set'), 
	1 DIM_DATEREVIEWID, 
	IFNULL(LT.AUTHORS,'Not Set'), 
	IFNULL(LT.R_VERSION_LABEL,'Not Set'), 
	1 AMT_EXCHANGERATE_GBL, 
	1 AMT_EXCHANGERATE, 
	1 DIM_CURRENCYID_TRA, 
	1 DIM_CURRENCYID_GBL, 
	current_date DW_INSERT_DATE, 
	current_date DW_UPDATE_DATE, 
	1, 
	IFNULL(M.R_OBJECT_ID,'Not Set'), 
	IFNULL(M.OBJECT_NAME,'Not Set'), 
	IFNULL(LT.TITLE,'Not Set'), 
	IFNULL(LT.DOCUMENT_TYPE,'Not Set'), 
	IFNULL(LT.DOCUMENT_SUBTYPE,'Not Set'), 
	IFNULL(LT.DOCUMENT_UNIT,'Not Set'), 
	IFNULL(LT.LEGACY_DOCUMENT,'Not Set'),
    IFNULL(LT.original_creation_date,'0001-01-01') dd_original_creation_date,
    IFNULL(LT.DOCUMENT_STATUS_DATE,'0001-01-01') dd_DOCUMENT_STATUS_DATE,
	IFNULL(LT.PERIODIC_REVIEW_START_DATE,'0001-01-01') dd_PERIODIC_REVIEW_START_DATE,
	IFNULL(LT.EFFECT_START,'0001-01-01') dd_EFFECT_START,
	IFNULL(LT.PERIODIC_REVIEW_NOTIFY_DATE,'0001-01-01') dd_PERIODIC_REVIEW_NOTIFY_DATE,
	IFNULL(LT.LAST_REVIEW_DATE,'0001-01-01') dd_LAST_REVIEW_DATE,
	IFNULL(LT.NEXT_REVIEW_DATE,'0001-01-01') dd_NEXT_REVIEW_DATE,
	IFNULL(LT.PLANNED_EFFECTIVE_DATE,'0001-01-01') dd_PLANNED_EFFECTIVE_DATE,
	IFNULL(LT.REVIEW_DATE,'0001-01-01') dd_REVIEW_DATE,
    IFNULL(LT.DOCUMENT_STATUS,'Not Set'),
	IFNULL(LT.STATUS,'Not Set'),
	'LATE' AS DD_SOURCE_VIEW
FROM  MSV_MANGO_DOC_LATE LT
LEFT JOIN  MSVT_DM_MAINVIEW_AUB_VEV m  ON 
LT.OBJECT_NAME  = M.OBJECT_NAME
and lt.authors  = M.authors
and lt.r_version_label  = M.r_version_label;


-- updates for Date dimensions 

UPDATE tmp_FACT_MANGO F
SET F.DIM_qualityusersid = D.DIM_qualityusersid
FROM tmp_FACT_MANGO F
LEFT JOIN DIM_qualityusers D ON trim(upper(REPLACE(f.dd_authors,'(legacy)',''))) = trim(upper(replace(d.LN_FN,',','')))
WHERE F.DIM_qualityusersid  <> D.DIM_qualityusersid;


UPDATE tmp_FACT_MANGO F
SET F.DIM_DATEORIGINALCREATIONID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_original_creation_date) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEORIGINALCREATIONID  <> D.DIM_DATEID;


UPDATE tmp_FACT_MANGO F
SET F.DIM_DATEDOCUMENTSTATUSID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_DOCUMENT_STATUS_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEDOCUMENTSTATUSID  <> D.DIM_DATEID;

UPDATE tmp_FACT_MANGO F
SET F.DIM_DATEPERIODICREVIEWSTARTID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_PERIODIC_REVIEW_START_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEPERIODICREVIEWSTARTID  <> D.DIM_DATEID;

UPDATE tmp_FACT_MANGO F
SET F.DIM_DATEEFECTSTARTID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_EFFECT_START) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEEFECTSTARTID  <> D.DIM_DATEID;

UPDATE tmp_FACT_MANGO F
SET F.DIM_DATEPERIODICREVIEWNOTIFYID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_PERIODIC_REVIEW_NOTIFY_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEPERIODICREVIEWNOTIFYID  <> D.DIM_DATEID;

UPDATE tmp_FACT_MANGO F
SET F.DIM_DATELASTREVIEWID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_LAST_REVIEW_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATELASTREVIEWID  <> D.DIM_DATEID;

UPDATE tmp_FACT_MANGO F
SET F.DIM_DATENEXTREVIEWID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_NEXT_REVIEW_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATENEXTREVIEWID  <> D.DIM_DATEID;

UPDATE tmp_FACT_MANGO F
SET F.DIM_DATENEXTREVIEWID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_NEXT_REVIEW_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATENEXTREVIEWID  <> D.DIM_DATEID;

UPDATE tmp_FACT_MANGO F
SET F.DIM_DATEPLANNEDEFFECTIVEID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_PLANNED_EFFECTIVE_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEPLANNEDEFFECTIVEID  <> D.DIM_DATEID;

UPDATE tmp_FACT_MANGO F
SET F.DIM_DATEREVIEWID = D.DIM_DATEID
FROM tmp_FACT_MANGO F
JOIN DIM_DATE D ON to_date(f.dd_REVIEW_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEREVIEWID  <> D.DIM_DATEID;



delete from NUMBER_FOUNTAIN where table_name = 'fact_MANGO';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_MANGO',IFNULL(MAX(fact_MANGOID),0)
FROM fact_MANGO;

TRUNCATE TABLE fact_mango;

INSERT INTO FACT_mango
(
FACT_MANGOID, 
    dim_qualityusersid,
    dim_datedocumentstatusid,
	DIM_DATEORIGINALCREATIONID, 
	DIM_DATEPERIODICREVIEWSTARTID, 
	DIM_DATEEFECTSTARTID, 
	DIM_DATEPERIODICREVIEWNOTIFYID, 
	DD_PERIODICREVIEWINTERVAL, 
	DIM_DATELASTREVIEWID, 
	DIM_DATENEXTREVIEWID, 
	DIM_DATEPLANNEDEFFECTIVEID, 
	DD_ENGLISHTITLE, 
	DD_AUTHORSITE, 
	DD_SYSTEMNAME, 
	DIM_DATEREVIEWID, 
	DD_AUTHORS, 
	DD_VERSIONLABEL, 
	AMT_EXCHANGERATE_GBL, 
	AMT_EXCHANGERATE, 
	DIM_CURRENCYID_TRA, 
	DIM_CURRENCYID_GBL, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	DIM_PROJECTSOURCEID, 
	DD_ROBJECTID, 
	DD_OBJECT_NAME, 
	DD_TITLE, 
	DD_DOCUMENT_TYPE, 
	DD_DOCUMENT_SUBTYPE, 
	DD_DOCUMENT_UNIT, 
	DD_LEGACY_DOCUMENT, 
	dd_original_creation_date,
    dd_DOCUMENT_STATUS_DATE,
	dd_PERIODIC_REVIEW_START_DATE,
	dd_EFFECT_START,
	dd_PERIODIC_REVIEW_NOTIFY_DATE,
	dd_LAST_REVIEW_DATE,
	dd_NEXT_REVIEW_DATE,
	dd_PLANNED_EFFECTIVE_DATE,
	dd_REVIEW_DATE,
    DD_DOCUMENT_STATUS,
	DD_STATUS,
    DD_SOURCE_VIEW)
SELECT 
IFNULL((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_MANGO'),0)
          + row_number() over(order by '') fact_mangoID,
    dim_qualityusersid,
    dim_datedocumentstatusid,
	DIM_DATEORIGINALCREATIONID, 
	DIM_DATEPERIODICREVIEWSTARTID, 
	DIM_DATEEFECTSTARTID, 
	DIM_DATEPERIODICREVIEWNOTIFYID, 
	DD_PERIODICREVIEWINTERVAL, 
	DIM_DATELASTREVIEWID, 
	DIM_DATENEXTREVIEWID, 
	DIM_DATEPLANNEDEFFECTIVEID, 
	DD_ENGLISHTITLE, 
	DD_AUTHORSITE, 
	DD_SYSTEMNAME, 
	DIM_DATEREVIEWID, 
	DD_AUTHORS, 
	DD_VERSIONLABEL, 
	AMT_EXCHANGERATE_GBL, 
	AMT_EXCHANGERATE, 
	DIM_CURRENCYID_TRA, 
	DIM_CURRENCYID_GBL, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	DIM_PROJECTSOURCEID, 
	DD_ROBJECTID, 
	DD_OBJECT_NAME, 
	DD_TITLE, 
	DD_DOCUMENT_TYPE, 
	DD_DOCUMENT_SUBTYPE, 
	DD_DOCUMENT_UNIT, 
	DD_LEGACY_DOCUMENT, 
	dd_original_creation_date,
    dd_DOCUMENT_STATUS_DATE,
	dd_PERIODIC_REVIEW_START_DATE,
	dd_EFFECT_START,
	dd_PERIODIC_REVIEW_NOTIFY_DATE,
	dd_LAST_REVIEW_DATE,
	dd_NEXT_REVIEW_DATE,
	dd_PLANNED_EFFECTIVE_DATE,
	dd_REVIEW_DATE,
    DD_DOCUMENT_STATUS,
	DD_STATUS,
	DD_SOURCE_VIEW
FROM TMP_FACT_MANGO;

