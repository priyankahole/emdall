DELETE FROM BIC_PZBTAP1700_all;
INSERT INTO BIC_PZBTAP1700_all
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_1
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_2
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_3
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_4
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_5
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_6
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_7
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_8
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_9
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_10
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_11
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_12
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_13
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_14
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_15
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_16
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_17
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_18
UNION ALL
SELECT PRODUCT,BIC_Z_KEYCU,BIC_Z_KEYCO2, BIC_Z_APOPL, BIC_Z_SHIPF,CALQUARTER, CALYEAR, CALMONTH, LOAD_DATE, BIC_ZPCALMOL, BIC_Z_APRC, BIC_ZAOLDPRC, BIC_Z_QOTEC, BIC_Z_QOTEM, BIC_Z_QOTER, BIC_Z_QPCNB, BIC_Z_QPRGB, BIC_Z_QSTB, CURRENCY, BIC_Z_UNITPC, BIC_Z_QSTC, BIC_Z_QPRGC, BIC_Z_QPCNC, BIC_Z_QSTN, BIC_Z_QPRGN, BIC_Z_QPCNN, CURKEY_GC, BIC_Z_FROZON2, BIC_Z_FROZON1, BIC_Z_KEYRG, CALDAY, BIC_Z_QSTCLC, BIC_Z_QPRGCLC, BIC_Z_QPCNCLC, BIC_Z_QSTNLC, BIC_Z_QPRGNLC, BIC_Z_QPCNNLC, CURKEY_LC, BIC_Z_QUARTFZ, BIC_Z_APOGUID, BIC_Z_QPNBNN, BIC_Z_QNBNNLC, BIC_Z_QPNBNC, BIC_Z_QNBNCLC, BIC_Z_QPNABL
FROM BIC_PZBTAP1700_19;


DROP TABLE IF EXISTS tmp_fact_MM00;
CREATE TABLE tmp_fact_MM00
AS
SELECT *
FROM fact_MM00
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_MM00';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_MM00',IFNULL(MAX(fact_MM00id),0)
FROM tmp_fact_MM00;

INSERT INTO tmp_fact_MM00(
  fact_MM00id,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dw_insert_date,
  dw_update_date,
  dd_product,
  dd_keycustomer,
  dd_keycountry,
  dd_apoplanningversion,
  dd_shipfromlocation,
  dd_calendaryearquarter,
  dd_calendaryear,
  dd_calendaryearmonth,
  loadingdate,
  dim_loadingdateid,
  ct_price,
  ct_previousprice,
  ct_consensusonetimeevent,
  ct_globalonetimeevent,
  ct_regionalonetimeevent,
  ct_consensusbaseline,
  ct_regionalbaseline,
  ct_statisticalforecastbaseline,
  dd_currencykey,
  dd_pieces,
  ct_statisticalforecastsp,
  ct_regionalplansp,
  ct_consensusplansp,
  ct_statisticalforecastnv,
  ct_regionalplannv,
  ct_consensusplannv,
  dd_groupcurrency,
  dd_apofrozenzone1,
  dd_apofrozenzone2,
  dd_keyregion,
  calendarday,
  dim_calendardayid,
  ct_statisticalforecastsplc,
  ct_regionalplansplc,
  ct_consensusplansplc,
  ct_statisticalforecastnvlc,
  ct_regionalplannvlc,
  ct_consensusplannvinlc,
  dd_localcurrency,
  dd_apoquarterfrozenzone,
  dd_apoguid,
  ct_naiveplannv,
  ct_naiveplannvlc,
  ct_naiveplansp,
  ct_naiveplansplc,
  ct_naivebaseline
)
SELECT
  (select ifnull(max_id,1)
    from number_fountain
    where table_name = 'fact_MM00') + row_number() over(order by '') AS fact_MM00id,
  1 as amt_exchangerate,
  1 as amt_exchangerate_gbl,
  current_timestamp as DW_INSERT_DATE,
  current_timestamp as DW_UPDATE_DATE,
  ifnull(PRODUCT,'Not Set') as dd_product,
  ifnull(BIC_Z_KEYCU,'Not Set') as dd_keycustomer,
  ifnull(BIC_Z_KEYCO2,'Not Set') as dd_keycountry,
  ifnull(BIC_Z_APOPL,'Not Set') as dd_apoplanningversion,
  ifnull(BIC_Z_SHIPF,'Not Set') as dd_shipfromlocation,
  ifnull(CALQUARTER,'Not Set') as dd_calendaryearquarter,
  ifnull(CALYEAR,'Not Set') as dd_calendaryear,
  ifnull(CALMONTH,'Not Set') as dd_calendaryearmonth,
   LOAD_DATE  as loadingdate,
  1 as dim_loadingdateid,
  ifnull(BIC_Z_APRC,0) as ct_price,
  ifnull(BIC_ZAOLDPRC,0) as ct_previousprice,
  ifnull(BIC_Z_QOTEC,0) as ct_consensusonetimeevent,
  ifnull(BIC_Z_QOTEM,0) as ct_globalonetimeevent,
  ifnull(BIC_Z_QOTER,0) as ct_regionalonetimeevent,
  ifnull(BIC_Z_QPCNB,0) as ct_consensusbaseline,
  ifnull(BIC_Z_QPRGB,0) as ct_regionalbaseline,
  ifnull(BIC_Z_QSTB,0) as ct_statisticalforecastbaseline,
  ifnull(CURRENCY,'Not Set') as dd_currencykey,
  ifnull(BIC_Z_UNITPC,'Not Set') as dd_pieces,
  ifnull(BIC_Z_QSTC,0) as ct_statisticalforecastsp,
  ifnull(BIC_Z_QPRGC,0) as ct_regionalplansp,
  ifnull(BIC_Z_QPCNC,0) as ct_consensusplansp,
  ifnull(BIC_Z_QSTN,0) as ct_statisticalforecastnv,
  ifnull(BIC_Z_QPRGN,0) as ct_regionalplannv,
  ifnull(BIC_Z_QPCNN,0) as ct_consensusplannv,
  ifnull(CURKEY_GC,'Not Set') as dd_groupcurrency,
  ifnull(BIC_Z_FROZON2,0) as dd_apofrozenzone1,
  ifnull(BIC_Z_FROZON1,0) as dd_apofrozenzone2,
  ifnull(BIC_Z_KEYRG,'Not Set') as dd_keyregion,
  CALDAY  as calendarday,
  1 as dim_calendardayid,
  ifnull(BIC_Z_QSTCLC,0) as ct_statisticalforecastsplc,
  ifnull(BIC_Z_QPRGCLC,0) as ct_regionalplansplc,
  ifnull(BIC_Z_QPCNCLC,0) as ct_consensusplansplc,
  ifnull(BIC_Z_QSTNLC,0) as ct_statisticalforecastnvlc,
  ifnull(BIC_Z_QPRGNLC,0) as ct_regionalplannvlc,
  ifnull(BIC_Z_QPCNNLC,0) as ct_consensusplannvinlc,
  ifnull(CURKEY_LC,'Not Set') as dd_localcurrency,
  ifnull(BIC_Z_QUARTFZ,'Not Set') as dd_apoquarterfrozenzone,
  ifnull(BIC_Z_APOGUID,'Not Set') as dd_apoguid,
  ifnull(BIC_Z_QPNBNN,0) as ct_naiveplannv,
  ifnull(BIC_Z_QNBNNLC,0) as ct_naiveplannvlc,
  ifnull(BIC_Z_QPNBNC,0) as ct_naiveplansp,
  ifnull(BIC_Z_QNBNCLC,0) as ct_naiveplansplc,
  ifnull(BIC_Z_QPNABL,0) as ct_naivebaseline
FROM
  BIC_PZBTAP1700_all
;

UPDATE tmp_fact_MM00 f
SET f.dim_loadingdateid = d.DIM_DATEID
FROM  tmp_fact_MM00 f, DIM_DATE d
WHERE concat(left(f.loadingdate,4),'-',mid (loadingdate,5,2),'-',mid(loadingdate,7,2)) = d.datevalue
      AND d.companycode = 'Not Set';

UPDATE tmp_fact_MM00 f
SET f.dim_calendardayid = d.DIM_DATEID
FROM  tmp_fact_MM00 f, DIM_DATE d
WHERE concat(left(f.calendarday,4),'-',mid (calendarday,5,2),'-',mid(calendarday,7,2)) = d.datevalue
      AND d.companycode = 'Not Set'
      AND f.dim_calendardayid  <> d.DIM_DATEID;

UPDATE tmp_fact_MM00 f
SET f.CALENDARMONTHID = d.CALENDARMONTHID
FROM  tmp_fact_MM00 f, DIM_DATE d
WHERE  f.dim_calendardayid = d.DIM_DATEID;


UPDATE tmp_fact_MM00 f
SET f.dim_mdg_partid = ifnull(md.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM dim_mdg_part md, tmp_fact_MM00 f
WHERE  trim(leading '0' from f.DD_PRODUCT) = trim(leading '0' from md.partnumber );


update  tmp_fact_MM00 f
set dim_keycustomerid = ifnull(dim_keycustomerid,1)
from tmp_fact_MM00 f
   inner join dim_keycustomer dm on dm.KeyCustomer = f.dd_keycustomer;


update  tmp_fact_MM00 f
set dim_keycountryid = ifnull(dim_keycountryid,1)
from tmp_fact_MM00 f
   inner join dim_keycountry dm on dm.KeyCountry = f.dd_keycountry;

DELETE FROM fact_MM00;

INSERT INTO fact_MM00(
  fact_MM00id,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dw_insert_date,
  dw_update_date,
  dd_product,
  dd_keycustomer,
  dd_keycountry,
  dd_apoplanningversion,
  dd_shipfromlocation,
  dd_calendaryearquarter,
  dd_calendaryear,
  dd_calendaryearmonth,
  loadingdate,
  dim_loadingdateid,
  ct_price,
  ct_previousprice,
  ct_consensusonetimeevent,
  ct_globalonetimeevent,
  ct_regionalonetimeevent,
  ct_consensusbaseline,
  ct_regionalbaseline,
  ct_statisticalforecastbaseline,
  dd_currencykey,
  dd_pieces,
  ct_statisticalforecastsp,
  ct_regionalplansp,
  ct_consensusplansp,
  ct_statisticalforecastnv,
  ct_regionalplannv,
  ct_consensusplannv,
  dd_groupcurrency,
  dd_apofrozenzone1,
  dd_apofrozenzone2,
  dd_keyregion,
  calendarday,
  dim_calendardayid,
  ct_statisticalforecastsplc,
  ct_regionalplansplc,
  ct_consensusplansplc,
  ct_statisticalforecastnvlc,
  ct_regionalplannvlc,
  ct_consensusplannvinlc,
  dd_localcurrency,
  dd_apoquarterfrozenzone,
  dd_apoguid,
  ct_naiveplannv,
  ct_naiveplannvlc,
  ct_naiveplansp,
  ct_naiveplansplc,
  ct_naivebaseline,
  dim_mdg_partid,
  CALENDARMONTHID
)
SELECT
  fact_MM00id,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dw_insert_date,
  dw_update_date,
  dd_product,
  dd_keycustomer,
  dd_keycountry,
  dd_apoplanningversion,
  dd_shipfromlocation,
  dd_calendaryearquarter,
  dd_calendaryear,
  dd_calendaryearmonth,
  loadingdate,
  dim_loadingdateid,
  ct_price,
  ct_previousprice,
  ct_consensusonetimeevent,
  ct_globalonetimeevent,
  ct_regionalonetimeevent,
  ct_consensusbaseline,
  ct_regionalbaseline,
  ct_statisticalforecastbaseline,
  dd_currencykey,
  dd_pieces,
  ct_statisticalforecastsp,
  ct_regionalplansp,
  ct_consensusplansp,
  ct_statisticalforecastnv,
  ct_regionalplannv,
  ct_consensusplannv,
  dd_groupcurrency,
  dd_apofrozenzone1,
  dd_apofrozenzone2,
  dd_keyregion,
  calendarday,
  dim_calendardayid,
  ct_statisticalforecastsplc,
  ct_regionalplansplc,
  ct_consensusplansplc,
  ct_statisticalforecastnvlc,
  ct_regionalplannvlc,
  ct_consensusplannvinlc,
  dd_localcurrency,
  dd_apoquarterfrozenzone,
  dd_apoguid,
  ct_naiveplannv,
  ct_naiveplannvlc,
  ct_naiveplansp,
  ct_naiveplansplc,
  ct_naivebaseline,
  ifnull(dim_mdg_partid, 1 ) as dim_mdg_partid,
  CALENDARMONTHID
FROM
  TMP_FACT_MM00;

-- DROP TABLE IF EXISTS tmp_fact_MM00;


DELETE FROM EMD586.fact_MM00;

INSERT INTO EMD586.fact_MM00(
  fact_MM00id,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dw_insert_date,
  dw_update_date,
  dd_product,
  dd_keycustomer,
  dd_keycountry,
  dd_apoplanningversion,
  dd_shipfromlocation,
  dd_calendaryearquarter,
  dd_calendaryear,
  dd_calendaryearmonth,
  loadingdate,
  dim_loadingdateid,
  ct_price,
  ct_previousprice,
  ct_consensusonetimeevent,
  ct_globalonetimeevent,
  ct_regionalonetimeevent,
  ct_consensusbaseline,
  ct_regionalbaseline,
  ct_statisticalforecastbaseline,
  dd_currencykey,
  dd_pieces,
  ct_statisticalforecastsp,
  ct_regionalplansp,
  ct_consensusplansp,
  ct_statisticalforecastnv,
  ct_regionalplannv,
  ct_consensusplannv,
  dd_groupcurrency,
  dd_apofrozenzone1,
  dd_apofrozenzone2,
  dd_keyregion,
  calendarday,
  dim_calendardayid,
  ct_statisticalforecastsplc,
  ct_regionalplansplc,
  ct_consensusplansplc,
  ct_statisticalforecastnvlc,
  ct_regionalplannvlc,
  ct_consensusplannvinlc,
  dd_localcurrency,
  dd_apoquarterfrozenzone,
  dd_apoguid,
  ct_naiveplannv,
  ct_naiveplannvlc,
  ct_naiveplansp,
  ct_naiveplansplc,
  ct_naivebaseline,
  dim_mdg_partid,
  CALENDARMONTHID
)
SELECT
  fact_MM00id,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dw_insert_date,
  dw_update_date,
  dd_product,
  dd_keycustomer,
  dd_keycountry,
  dd_apoplanningversion,
  dd_shipfromlocation,
  dd_calendaryearquarter,
  dd_calendaryear,
  dd_calendaryearmonth,
  loadingdate,
  dim_loadingdateid,
  ct_price,
  ct_previousprice,
  ct_consensusonetimeevent,
  ct_globalonetimeevent,
  ct_regionalonetimeevent,
  ct_consensusbaseline,
  ct_regionalbaseline,
  ct_statisticalforecastbaseline,
  dd_currencykey,
  dd_pieces,
  ct_statisticalforecastsp,
  ct_regionalplansp,
  ct_consensusplansp,
  ct_statisticalforecastnv,
  ct_regionalplannv,
  ct_consensusplannv,
  dd_groupcurrency,
  dd_apofrozenzone1,
  dd_apofrozenzone2,
  dd_keyregion,
  calendarday,
  dim_calendardayid,
  ct_statisticalforecastsplc,
  ct_regionalplansplc,
  ct_consensusplansplc,
  ct_statisticalforecastnvlc,
  ct_regionalplannvlc,
  ct_consensusplannvinlc,
  dd_localcurrency,
  dd_apoquarterfrozenzone,
  dd_apoguid,
  ct_naiveplannv,
  ct_naiveplannvlc,
  ct_naiveplansp,
  ct_naiveplansplc,
  ct_naivebaseline,
   dim_mdg_partid,
  CALENDARMONTHID
FROM
  FACT_MM00;
