
/* EMD 586 Material movement data */
drop table if exists tmp_matmovvalues;
CREATE TABLE tmp_matmovvalues as
SELECT pd.CalendarMonthNumber,
       pd.CALENDARYEAR,
       prt.PartDescription ,
       uom.UOM ,
REPLACE (prt.PartNumber_NoLeadZero,'.','') PartNumber,
        SUM(f_mm.ct_Quantity/1000) Quantity
FROM EMD586.fact_materialmovement AS f_mm
INNER JOIN EMD586.Dim_Part AS prt ON f_mm.Dim_Partid = prt.Dim_Partid
INNER JOIN EMD586.dim_unitofmeasure AS uom ON f_mm.Dim_UnitOfMeasureid = uom.dim_unitofmeasureid
INNER JOIN EMD586.dim_MovementType AS mt ON f_mm.Dim_MovementTypeID = mt.dim_MovementTypeid
INNER JOIN EMD586.dim_projectsource AS prjs ON f_mm.DIM_PROJECTSOURCEID = prjs.dim_projectsourceid
INNER JOIN EMD586.dim_bwproducthierarchy AS bw ON f_mm.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
INNER JOIN EMD586.Dim_Date AS pd ON f_mm.dim_DateIDPostingDate = pd.Dim_Dateid
WHERE (lower (mt.MovementType) IN ('261','262'))
   AND (lower (prjs.projectdescription) IN (lower ('Quattro Instance'), lower ('Emerald Instance')))
   AND (lower (bw.businessunitdesc) = lower ('Display Materials'))
   AND (lower (prt.PartNumber_NoLeadZero) LIKE lower ('%1111'))
   AND (pd.DateValue >= '2016-11-01')
GROUP BY pd.CalendarMonthNumber,
         pd.CALENDARYEAR,
         prt.PartDescription,
         uom.UOM,
REPLACE (prt.PartNumber_NoLeadZero,'.','') ;
									   
	

/* APO Inventory data  */		

drop table if exists tmp_maxsnapshotdate;
create table tmp_maxsnapshotdate as
select max(datevalue) datevalue,CalendarMonthNumber,CALENDARYEAR from EMD586.fact_APOInventoryAnalytics f_apoia, EMD586.dim_date snpddt
where f_apoia.DIM_SNAPSHOTID = snpddt.Dim_Dateid
group by CalendarMonthNumber,CALENDARYEAR;
   



drop table if exists tmp_apovalues;
CREATE TABLE tmp_apovalues as 
SELECT 
               ddt.CalendarMonthNumber dateCalendarMonthNumber,
               ddt.CALENDARYEAR dateCALENDARYEAR,
               snpddt.CalendarMonthNumber snapshotCalendarMonthNumber,
               snpddt.CALENDARYEAR snapshotCALENDARYEAR,
                   f_apoia.dd_materialno MaterialNumber,
                   ROUND(SUM(ct_dependentdemand),2)DependentDemand,
                   ROUND(SUM(CT_DEPENDENTDEMANDBASEUNIT),2) DependentDemandBaseUnit,
                   ROUND(SUM(CT_DEPENDENTDEMANDKG),2) DependentDemandKG
            FROM EMD586.fact_APOInventoryAnalytics AS f_apoia
            INNER JOIN DIM_BWPRODUCTHIERARCHYAPO AS bwprh ON f_apoia.DIM_BWPRODUCTHIERARCHYID = bwprh.DIM_BWPRODUCTHIERARCHYAPOid
            INNER JOIN dim_mdg_part AS pmdg ON f_apoia.dim_mdg_partid = pmdg.dim_mdg_partid
            INNER JOIN Dim_Date AS snpddt ON f_apoia.DIM_SNAPSHOTID = snpddt.Dim_Dateid
            INNER JOIN Dim_Date AS ddt ON f_apoia.dim_dateid = ddt.Dim_Dateid
            INNER JOIN tmp_maxsnapshotdate tmp ON snpddt.datevalue = tmp.datevalue
            WHERE 
                  case when snpddt.CalendarMonthNumber = 12 then ddt.CalendarMonthNumber  = 1 and snpddt.CALENDARYEAR = ddt.CALENDARYEAR - 1
                           else snpddt.CalendarMonthNumber = ddt.CalendarMonthNumber - 1 and snpddt.CALENDARYEAR = ddt.CALENDARYEAR end
              AND f_apoia.dd_materialno LIKE lower('%0000')
              AND (lower(pmdg.aporelevance) IN (lower('2'),
                                                    lower('3'),
                                                    lower('4')))
              AND ((lower(pmdg.producthierarchy) LIKE lower ('LCS%')
                    OR lower(pmdg.producthierarchy) LIKE lower('LSP%')))
            GROUP BY 
                    ddt.CalendarMonthNumber,
               ddt.CALENDARYEAR,
               snpddt.CalendarMonthNumber,
               snpddt.CALENDARYEAR,
                   f_apoia.dd_materialno;

				   
/* Populate fact */
DELETE FROM fact_LCSinglesFcstAccuracy where dd_manualupload <> 'Yes';
INSERT INTO fact_LCSinglesFcstAccuracy
(
fact_LCSinglesFcstAccuracyid,
dd_CalendarMonthNumber,
dd_snapshotdate,
dd_MaterialNumberAPO,
dd_MaterialNumberMM,
ct_DependentDemand,
ct_Quantity,
ct_AbsDeviation,
ct_FcstError
)		   
SELECT row_number() over(order by '') as fact_LCSinglesFcstAccuracyid,
       mm.CalendarMonthNumber as dd_CalendarMonthNumber,
	   apo.snapshotCalendarMonthNumber as dd_snapshotdate,
       apo.MaterialNumber as dd_MaterialNumberAPO,
       mm.PartNumber as dd_MaterialNumberMM,
       apo.DependentDemand as ct_DependentDemand,
       mm.Quantity as ct_Quantity,
       abs(mm.Quantity - apo.DependentDemand) as ct_AbsDeviation,
       case when  mm.Quantity = 0 then 1 else abs(mm.Quantity - apo.DependentDemand) / mm.Quantity * (-1) end   ct_FcstError
FROM tmp_apovalues apo,
     tmp_matmovvalues mm
WHERE LEFT(apo.MaterialNumber, LEN(apo.MaterialNumber) - 4) = LEFT(mm.PartNumber, LEN(mm.PartNumber) - 4)
  AND  mm.CalendarMonthNumber = apo.dateCalendarMonthNumber  
  AND mm.CALENDARYEAR = apo.dateCALENDARYEAR ;