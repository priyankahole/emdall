/******************************************************************************************************************/
/*   Script         : bi_populate_outputerbitux_fact                                                              */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_outputerbitux                                                     */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   31 Jan 2017         CristianT       1.0               Creating the script.                                   */
/*   10 Feb 2017         CristianT       1.1               Calculating the AVG of previous years                  */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_outputerbitux;
CREATE TABLE tmp_fact_outputerbitux
AS
SELECT *
FROM fact_outputerbitux
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_outputerbitux';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_outputerbitux', ifnull(max(fact_outputerbituxid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_outputerbitux;

INSERT INTO tmp_fact_outputerbitux(
fact_outputerbituxid,
dd_parameter_set_name,
dd_parameter_Set_date,
dd_bf_batch_id,
amt_producedqty,
dim_dateidsetdate,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_outputerbitux') + ROW_NUMBER() over(order by '') as fact_outputerbituxid,
       ifnull(parameter_set_name, 'Not Set') as dd_parameter_set_name,
       ifnull(parameter_set_date, 0) as dd_parameter_Set_date,
       ifnull(bf_batch_id, 'Not Set') as dd_bf_batch_id,
       ifnull(bf_conc_bulk_protein_amount, 0) as amt_producedqty,
       1 as dim_dateidsetdate,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM output_erbitux_kg_run tmp
WHERE tmp.parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
      AND EXISTS (SELECT 1
              FROM yield_hat_bulk_erbitux h
              WHERE h.PARAMETER_SET_NAME = tmp.parameter_set_name);
      
UPDATE tmp_fact_outputerbitux tmp
SET tmp.dim_dateidsetdate =  dt.dim_dateid
FROM dim_date dt,
     tmp_fact_outputerbitux tmp
WHERE dt.datevalue = substr(tmp.dd_parameter_set_date, 0, 10)
      AND dt.companycode = 'Not Set';

/* 10 Feb 2016 CristianT Start: Calculating the AVG of previous years */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_outputerbitux';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_outputerbitux', ifnull(max(fact_outputerbituxid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_outputerbitux;

INSERT INTO tmp_fact_outputerbitux(
fact_outputerbituxid,
dd_parameter_set_name,
dd_parameter_Set_date,
dd_bf_batch_id,
amt_producedqty,
dim_dateidsetdate,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_outputerbitux') + ROW_NUMBER() over(order by '') as fact_outputerbituxid,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_parameter_set_name,
       current_date as dd_parameter_set_date,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_bf_batch_id,
       ifnull(AVG(bf_conc_bulk_protein_amount), 0) as amt_producedqty,
       1 as dim_dateidsetdate,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM Output_Erbitux_KG_run tmp
WHERE tmp.parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
      AND EXISTS (select 1 from yield_hat_bulk_erbitux h where h.PARAMETER_SET_NAME = tmp.parameter_set_name)
GROUP BY YEAR(substr(tmp.parameter_set_date, 0, 10));


UPDATE tmp_fact_outputerbitux tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_outputerbitux tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_parameter_set_date
      AND tmp.dim_dateidsetdate = 1;
/* 10 Feb 2016 CristianT End */

UPDATE tmp_fact_outputerbitux tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_outputerbitux tmp;

TRUNCATE TABLE fact_outputerbitux;
	  
INSERT INTO fact_outputerbitux(
fact_outputerbituxid,
dd_parameter_set_name,
dd_parameter_Set_date,
dd_bf_batch_id,
amt_producedqty,
dim_dateidsetdate,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_outputerbituxid,
       dd_parameter_set_name,
       dd_parameter_Set_date,
       dd_bf_batch_id,
       amt_producedqty,
       dim_dateidsetdate,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM tmp_fact_outputerbitux;

DROP TABLE IF EXISTS tmp_fact_outputerbitux;

