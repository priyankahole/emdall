/******************************************************************************************************************/
/*   Script         : bi_populate_ehss_fact                                                                       */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 02 Nov 2016                                                                                 */
/*   Description    : Populating script of fact_ehss                                                              */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   02 Nov 2016         CristianT       1.0               Creating the script.                                   */
/*   01 Feb 2017         CristianT       1.1               Adding snapshot functionality                          */
/*   17 Feb 2017         CristianT       1.2               Adding logic for dd_risklevel                          */
/*   20 Feb 2017         CristianT       1.3               I removed fact_ehss from the harmonization project and i added the delete/insert statements for emd586 inside the script */
/******************************************************************************************************************/

/* CristianT: Using ehsshistory_delete table in case we reprocess so we won't have duplicate data for same snapshot date in the historical table.
Historical data: fact_ehsshistory will preserve all rows, any alter made on fact_ehss we should make it on fact_ehsshistory aswell.
Reporting data: fact_ehss will keep only 1 day for each week of year. In case we have 1 full week in history table we should keep only Saturday. In case the week just started we should keep last processed day.
*/
DROP TABLE IF EXISTS ehsshistory_delete;
CREATE TABLE ehsshistory_delete
AS
SELECT fact_ehssid
FROM fact_ehsshistory
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM fact_ehsshistory
WHERE fact_ehssid IN (SELECT fact_ehssid FROM ehsshistory_delete);

DROP TABLE IF EXISTS ehsshistory_delete;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_ehsshistory';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ehsshistory', ifnull(max(fact_ehssid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_ehsshistory;

DROP TABLE IF EXISTS tmp_fact_ehss;
CREATE TABLE tmp_fact_ehss
AS
SELECT *
FROM fact_ehsshistory
WHERE 1 = 0;

INSERT INTO tmp_fact_ehss(
fact_ehssid,
dd_actionid,
dim_dateidcreated,
dim_dateidtarget,
dim_dateidmodified,
dd_actiontitle,
dd_actiontype,
dd_needapproval,
dd_actionstatus,
dd_aditionalinfo,
dd_actiondescription,
dd_externalreference,
dd_sector,
dd_subsector,
dd_scopetype,
dd_subjectlabel,
dd_situationdescription,
dim_ehssusersidassignedby,
dim_ehssusersidsupervisor,
dim_ehssusersidassignedto,
dd_controltype,
dim_dateidclosing,
dd_subsectorservice,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dim_qualityusersidassignedby,
dim_qualityuserssupervisor,
dim_qualityusersassignedto,
snapshotdate,
dim_dateidsnapshot,
dd_risklevel
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_ehsshistory') + ROW_NUMBER() over(order by '') AS fact_ehssid,
       t.*
FROM (
SELECT DISTINCT act.id as dd_actionid,
       1 as dim_dateidcreated,
	   1 as dim_dateidtarget,
	   1 as dim_dateidmodified,
       ifnull(act.title, 'Not Set') as dd_actiontitle,
	   ifnull(act.actiontype, 'Not Set') as dd_actiontype,
       case when act.needapproval = 1 then 'Y' when act.needapproval = 0 then 'N' else 'Not Set' end as dd_needapproval,
	   ifnull(act.status, 'Not Set') as dd_actionstatus,
       ifnull(act.actioncomment, 'Not Set') as dd_aditionalinfo,
       ifnull(act.description, 'Not Set') as dd_actiondescription,
       ifnull(act.auditreference, 'Not Set') as dd_externalreference,
       ifnull(sect.label, 'Not Set') as dd_sector,
       ifnull(subsect.label, 'Not Set') as dd_subsector,
       ifnull(scp.label, 'Not Set') as dd_scopetype,
       ifnull(sbj.label, 'Not Set') as dd_subjectlabel,
       ifnull(rsk.description, 'Not Set') as dd_situationdescription,       
       1 as dim_ehssusersidassignedby,
       1 as dim_ehssusersidsupervisor,
       1 as dim_ehssusersidassignedto,
       ifnull(cttype.label, 'Not Set') as dd_controltype,
       1 as dim_dateidclosing,
       'Not Set' as dd_subsectorservice,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       1 as dim_qualityusersidassignedby,
       1 as dim_qualityuserssupervisor,
       1 as dim_qualityusersassignedto,
       CASE WHEN extract(hour from current_timestamp) between 0 AND 18 THEN current_date - 1 ELSE current_date END as snapshotdate,
       1 as dim_dateidsnapshot,
       'Not Set' as dd_risklevel
FROM ehss_action act,
     ehss_sector sect,
     ehss_sector subsect,
     ehss_scope scp,
     ehss_assessmentaction assessct,
     ehss_assessment assess,
     ehss_risk rsk,
     ehss_control ctrl,
     ehss_subject sbj,
     ehss_controltype cttype
WHERE act.sectorid = sect.id
      AND act.subsectorid = subsect.id
      AND sect.scopeid = scp.id
      AND assessct.actionid = act.id
      AND assess.id = assessct.assessmentid
      AND assess.riskid = rsk.id
      AND rsk.controlid = ctrl.id
      AND ctrl.subjectid = sbj.id
      AND ctrl.controltypeid = cttype.id ) t;

UPDATE tmp_fact_ehss tmp
SET tmp.dim_dateidsnapshot = dt.dim_dateid
FROM tmp_fact_ehss tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = CASE WHEN extract(hour from current_timestamp) between 0 AND 18 THEN current_date - 1 ELSE current_date END;

UPDATE tmp_fact_ehss tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_ehss tmp;

UPDATE tmp_fact_ehss tmp
SET tmp.dim_dateidcreated = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     ehss_action act,
     tmp_fact_ehss tmp
WHERE tmp.dd_actionid = act.id
      AND dt.companycode = 'Not Set'
      AND act.createddate = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidcreated <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ehss tmp
SET tmp.dim_dateidtarget = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     ehss_action act,
     tmp_fact_ehss tmp
WHERE tmp.dd_actionid = act.id
      AND dt.companycode = 'Not Set'
      AND act.targetdate = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidtarget <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ehss tmp
SET tmp.dim_dateidmodified = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     ehss_action act,
     tmp_fact_ehss tmp
WHERE tmp.dd_actionid = act.id
      AND dt.companycode = 'Not Set'
      AND convert(DATE, act.modifieddate) = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmodified <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ehss tmp
SET tmp.dim_dateidclosing = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     ehss_action act,
     tmp_fact_ehss tmp
WHERE tmp.dd_actionid = act.id
      AND dt.companycode = 'Not Set'
      AND convert(DATE, act.closingdate) = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidclosing <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ehss tmp
SET tmp.dd_subsectorservice = 'USP'
WHERE upper(tmp.dd_subsector) in ('USP','USP 1','USP 2 + CAPTURE','USP 3+4','USP 4','USP 3');

UPDATE tmp_fact_ehss tmp
SET tmp.dd_subsectorservice = 'DSP'
WHERE upper(tmp.dd_subsector) in ('DSP','DSP1 + CAPTURE','DSP2','DSP3','DSP3 + CAPTURE','DSP4 + CAPTURE');

UPDATE tmp_fact_ehss tmp
SET tmp.dd_subsectorservice = 'MTS'
WHERE upper(tmp.dd_subsector) in ('MTS','USP DEV','DSP  DEV','MTS LAB', 'DSP DEV');

UPDATE tmp_fact_ehss tmp
SET tmp.dd_subsectorservice = 'PS'
WHERE upper(tmp.dd_subsector) in ('BP 12','BP 21','BP 34','DISPENSING + LAVERIE LSC','LAVERIE AUTOCLAVE 1','LAVERIE AUTOCLAVE 2','LAVERIE AUTOCLAVE 34','MP 20','MP 34','STAGING. MONT. MAN. WASH');

UPDATE tmp_fact_ehss tmp
SET tmp.dim_qualityusersidassignedby = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     ehss_action act,
     tmp_fact_ehss tmp
WHERE tmp.dd_actionid = act.id
      AND act.responsibleid = usr.merck_uid
      AND tmp.dim_qualityusersidassignedby <> ifnull(usr.dim_qualityusersid, 1);

UPDATE tmp_fact_ehss tmp
SET tmp.dim_qualityuserssupervisor = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     ehss_action act,
     tmp_fact_ehss tmp
WHERE tmp.dd_actionid = act.id
      AND act.accountableid = usr.merck_uid
      AND tmp.dim_qualityuserssupervisor <> ifnull(usr.dim_qualityusersid, 1);

UPDATE tmp_fact_ehss tmp
SET tmp.dim_qualityusersassignedto = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     ehss_action act,
     tmp_fact_ehss tmp
WHERE tmp.dd_actionid = act.id
      AND act.rpid = usr.merck_uid
      AND tmp.dim_qualityusersassignedto <> ifnull(usr.dim_qualityusersid, 1);

/* 17 Feb 2017 CristianT Start: Adding logic for dd_risklevel */
DROP TABLE IF EXISTS tmp_risklevel;
CREATE TABLE tmp_risklevel
AS
SELECT distinct id as dd_actionid,
       ifnull(NIVEAU_DE_RISQUE, 'Not Set') as dd_risklevel
FROM v_ehss_action_list;

UPDATE tmp_fact_ehss tmp
SET tmp.dd_risklevel = rsk.dd_risklevel
FROM tmp_fact_ehss tmp,
     tmp_risklevel rsk
WHERE tmp.dd_actionid = rsk.dd_actionid
      AND tmp.dd_risklevel <> rsk.dd_risklevel;

DROP TABLE IF EXISTS tmp_risklevel;

/* 17 Feb 2017 CristianT End */

INSERT INTO fact_ehsshistory (
fact_ehssid,
dd_actionid,
dim_dateidcreated,
dim_dateidtarget,
dim_dateidmodified,
dd_actiontitle,
dd_actiontype,
dd_needapproval,
dd_actionstatus,
dd_aditionalinfo,
dd_actiondescription,
dd_externalreference,
dd_sector,
dd_subsector,
dd_scopetype,
dd_subjectlabel,
dd_situationdescription,
dim_ehssusersidassignedby,
dim_ehssusersidsupervisor,
dim_ehssusersidassignedto,
dd_controltype,
dim_dateidclosing,
dd_subsectorservice,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dim_qualityusersidassignedby,
dim_qualityuserssupervisor,
dim_qualityusersassignedto,
snapshotdate,
dim_dateidsnapshot,
dd_risklevel
)
SELECT fact_ehssid,
       dd_actionid,
       dim_dateidcreated,
       dim_dateidtarget,
       dim_dateidmodified,
       dd_actiontitle,
       dd_actiontype,
       dd_needapproval,
       dd_actionstatus,
       dd_aditionalinfo,
       dd_actiondescription,
       dd_externalreference,
       dd_sector,
       dd_subsector,
       dd_scopetype,
       dd_subjectlabel,
       dd_situationdescription,
       dim_ehssusersidassignedby,
       dim_ehssusersidsupervisor,
       dim_ehssusersidassignedto,
       dd_controltype,
       dim_dateidclosing,
       dd_subsectorservice,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dim_qualityusersidassignedby,
       dim_qualityuserssupervisor,
       dim_qualityusersassignedto,
       snapshotdate,
       dim_dateidsnapshot,
       dd_risklevel
FROM tmp_fact_ehss;

DROP TABLE IF EXISTS tmp_fact_ehss;

DROP TABLE IF EXISTS ehss_delete;
CREATE TABLE ehss_delete
AS
SELECT fact_ehssid
FROM fact_ehss
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM fact_ehss
WHERE fact_ehssid IN (SELECT fact_ehssid FROM ehss_delete);

DROP TABLE IF EXISTS ehss_delete;

DROP TABLE IF EXISTS ehss_insert;
CREATE TABLE ehss_insert
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM fact_ehsshistory f,
     dim_date dt
WHERE dim_dateidsnapshot = dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_ehss';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_ehss', ifnull(max(fact_ehssid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_ehss;

INSERT INTO fact_ehss(
fact_ehssid,
dd_actionid,
dim_dateidcreated,
dim_dateidtarget,
dim_dateidmodified,
dd_actiontitle,
dd_actiontype,
dd_needapproval,
dd_actionstatus,
dd_aditionalinfo,
dd_actiondescription,
dd_externalreference,
dd_sector,
dd_subsector,
dd_scopetype,
dd_subjectlabel,
dd_situationdescription,
dim_ehssusersidassignedby,
dim_ehssusersidsupervisor,
dim_ehssusersidassignedto,
dd_controltype,
dim_dateidclosing,
dd_subsectorservice,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dim_qualityusersidassignedby,
dim_qualityuserssupervisor,
dim_qualityusersassignedto,
snapshotdate,
dim_dateidsnapshot,
dd_risklevel
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_ehss') + ROW_NUMBER() over(order by '') as fact_ehssid,
       h.dd_actionid,
       h.dim_dateidcreated,
       h.dim_dateidtarget,
       h.dim_dateidmodified,
       h.dd_actiontitle,
       h.dd_actiontype,
       h.dd_needapproval,
       h.dd_actionstatus,
       h.dd_aditionalinfo,
       h.dd_actiondescription,
       h.dd_externalreference,
       h.dd_sector,
       h.dd_subsector,
       h.dd_scopetype,
       h.dd_subjectlabel,
       h.dd_situationdescription,
       h.dim_ehssusersidassignedby,
       h.dim_ehssusersidsupervisor,
       h.dim_ehssusersidassignedto,
       h.dd_controltype,
       h.dim_dateidclosing,
       h.dd_subsectorservice,
       h.dim_projectsourceid,
       h.amt_exhangerate,
       h.amt_exchangerate_gbl,
       h.dim_currencyid,
       h.dim_currencyid_tra,
       h.dim_currencyid_gbl,
       h.dim_qualityusersidassignedby,
       h.dim_qualityuserssupervisor,
       h.dim_qualityusersassignedto,
       h.snapshotdate,
       h.dim_dateidsnapshot,
       h.dd_risklevel
FROM fact_ehsshistory h,
     ehss_insert ins
WHERE h.dim_dateidsnapshot = ins.dim_dateidsnapshot
      AND ins.rowno = 1
      AND NOT EXISTS (SELECT 1 FROM fact_ehss t WHERE t.dim_dateidsnapshot = ins.dim_dateidsnapshot);

DROP TABLE IF EXISTS ehss_insert;

DROP TABLE IF EXISTS tmp_delete;
CREATE TABLE tmp_delete
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM fact_ehss f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

MERGE INTO fact_ehss f
USING tmp_delete del ON f.dim_dateidsnapshot = del.dim_dateidsnapshot AND del.rowno > 1
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_delete;
