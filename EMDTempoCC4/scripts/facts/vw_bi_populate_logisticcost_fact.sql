drop table if exists tmp_logisticcost;
create table tmp_logisticcost as
select f.dim_dateidstartdate
	,f.dim_jda_locationid
	,f.dim_jda_productid
	,'Not Set' as dd_cool_ambient
	,case when f.dd_type = 'Consensus Forecast EURO' and date_trunc('YEAR',datevalue) = date_trunc('YEAR',current_date) then f.ct_packs else 0 end as amt_packs_cf
	,case when dc.c_type3 = 'OP EURO' and date_trunc('YEAR',datevalue) = date_trunc('YEAR',current_date) then f.ct_packs else 0 end as amt_packs_op
	,case when f.dd_type = 'Consensus Forecast QTY' then f.ct_packs else 0 end as ct_packs_cf
	,case when dc.c_type3 = 'OP' and date_trunc('YEAR',datevalue) = date_trunc('YEAR',current_date) then f.ct_packs else 0 end as ct_packs_op
	,ifnull(m.MICSTAG_LOCATION_CMG_MAPPING_cmg_id,0) as dd_cmg_id
	,cast(0 as decimal(18,4)) as ct_ambient_factor
	,cast(0 as decimal(18,4)) as ct_cool_factor
	,cast(0 as decimal(18,4)) as ct_freight_ratio
	,cast(0 as decimal(18,4)) as ct_wh_ratio
	,cast(0 as decimal(18,4)) as ct_ord_ratio
	,cast(0 as decimal(18,4)) as ct_packs_ambient_cf
	,cast(0 as decimal(18,4)) as ct_packs_cool_cf
	,cast(0 as decimal(18,4)) as ct_packs_ambient_op
	,cast(0 as decimal(18,4)) as ct_packs_cool_op
	,cast(0 as decimal(18,4)) as ct_eq_units_cf
	,cast(0 as decimal(18,4)) as ct_eq_units_op
	,cast('Not Set' as varchar(100)) as dd_devgroup_flag
from fact_jda_demandforecast f
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join dim_jda_location dl on f.dim_jda_locationid = dl.dim_jda_locationid
	inner join dim_jda_product dp on f.dim_jda_productid = dp.dim_jda_productid
	inner join dim_jda_component dc on f.dim_jda_componentid = dc.dim_jda_componentid
	left outer join MICSTAG_LOCATION_CMG_MAPPING m on dl.location_name = m.MICSTAG_LOCATION_CMG_MAPPING_dp_code
where (f.dd_type in ('Consensus Forecast QTY','Consensus Forecast EURO') or (dc.c_type3 in ('OP','OP EURO') AND f.dd_fcstlevel in ('111','711')) )
	and dp.GBU in ('77 Thyroids','A01 Oncology','A03 General Medicine','A04 Neurodegenerative Diseases','A05 AIID','A14 Fertility',
		'A15 Endocrinology','A16 Avelumab','B4 Diabetes','B5 Cardiovascular','B6 CMCare Local','OTHER','A08 3rd Party Business Production')
	and upper(dp.container) not in ('BULK','API BULK')
	and dl.udc_gauss_rep_region <> 'Not Set'
	and dl.region in ('Antarctica','Asia Pacific','Big 5','Central Europe','Intercontinental','Latin America','North America','Western Europe')
	and (dl.location_name not like 'REP%' or dl.location_name not like '%DISTR')
	and (year(datevalue) between year(current_date-INTERVAL '1' YEAR) and year(current_date))
	and m.MICSTAG_LOCATION_CMG_MAPPING_SPT_RELEVANT = 1;


--GAUSS Costs
drop table if exists tmp_lc_gausscost_bymonth;
create table tmp_lc_gausscost_bymonth as
select dim_dateid,calmonth, 'Freight' as ctype, b1.BIC_Z_REPUNIT, sum(amount)/(-1000) amount
	from BIC_AZFOFR00 b1, BIC_TZ_REPPOS b2, BIC_HZ_REPPOS b3, tmp_lc_hier h , dim_date dt
	where b1.BIC_Z_REPPOS = b2.BIC_Z_REPPOS
		and b3.hieid = h.hieid
		and left(b1.calmonth,4) = h.yearfrom
		and b3.nodename = b2.BIC_Z_REPPOS
		and to_date(to_char(CALMONTH),'YYYYMM') between b2.datefrom and b2.dateto
		and b1.BIC_Z_REPPOS like '311044%'
		and calmonth = dt.calendarmonthid and dt.companycode = 'Not Set' and dt.DAYOFMONTH = 1
		group by dim_dateid,calmonth,b1.BIC_Z_REPUNIT
UNION
select dim_dateid,calmonth, 'Warehousing and Distribution' as ctype,b1.BIC_Z_REPUNIT, sum(amount)/(-1000) amount
	from BIC_AZFOFR00 b1, BIC_TZ_REPPOS b2, BIC_HZ_REPPOS b3, tmp_lc_hier h , dim_date dt
	where b1.BIC_Z_REPPOS = b2.BIC_Z_REPPOS
		and b3.hieid = h.hieid
		and left(b1.calmonth,4) = h.yearfrom
		and b3.nodename = b2.BIC_Z_REPPOS
		and to_date(to_char(CALMONTH),'YYYYMM') between b2.datefrom and b2.dateto
		and b1.BIC_Z_REPPOS like '311042%'
		and calmonth = dt.calendarmonthid and dt.companycode = 'Not Set' and dt.DAYOFMONTH = 1
	group by dim_dateid,calmonth,b1.BIC_Z_REPUNIT
UNION
select dim_dateid,calmonth, 'Global Demand' as ctype,b1.BIC_Z_REPUNIT, sum(amount)/(-1000) amount
	from BIC_AZFOFR00 b1, BIC_TZ_REPPOS b2, BIC_HZ_REPPOS b3, tmp_lc_hier h ,dim_date dt
	where b1.BIC_Z_REPPOS = b2.BIC_Z_REPPOS
		and b3.hieid = h.hieid
		and left(b1.calmonth,4) = h.yearfrom
		and b3.nodename = b2.BIC_Z_REPPOS
		and to_date(to_char(CALMONTH),'YYYYMM') between b2.datefrom and b2.dateto
		and b1.BIC_Z_REPPOS like '311043%'
		and calmonth = dt.calendarmonthid and dt.companycode = 'Not Set' and dt.DAYOFMONTH = 1
	group by dim_dateid,calmonth,b1.BIC_Z_REPUNIT;

drop table if exists tmp_missing_le;
create table tmp_missing_le as
select distinct tg.dim_dateid as DIM_DATEIDSTARTDATE
	, trim(leading 0 from tg.BIC_Z_REPUNIT) as dd_cmg_id
from tmp_lc_gausscost_bymonth  tg
	left outer join tmp_logisticcost tl on trim(leading 0 from tg.BIC_Z_REPUNIT) = tl.dd_cmg_id and tg.dim_dateid = tl.dim_dateidstartdate
where tl.dd_cmg_id is null;

insert into tmp_logisticcost (DIM_DATEIDSTARTDATE,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_COOL_AMBIENT,AMT_PACKS_CF,AMT_PACKS_OP,CT_PACKS_CF,CT_PACKS_OP,
		DD_CMG_ID,CT_AMBIENT_FACTOR,CT_COOL_FACTOR,CT_FREIGHT_RATIO,CT_WH_RATIO,CT_ORD_RATIO,ct_packs_ambient_cf,ct_packs_cool_cf,
		ct_packs_ambient_op,ct_packs_cool_op,ct_eq_units_cf,ct_eq_units_op,DD_DEVGROUP_FLAG)
select t.DIM_DATEIDSTARTDATE
	,1 as dim_jda_locationid
	,1 as dim_jda_productid
	,'Cool' as dd_cool_ambient
	, 0 as amt_packs_cf
	, 0 as amt_packs_op
	, 0 as ct_packs_cf
	, 0 as ct_packs_op
	,t.DD_CMG_ID
	,cast(0 as decimal(18,4)) as ct_ambient_factor
	,cast(0 as decimal(18,4)) as ct_cool_factor
	,cast(0 as decimal(18,4)) as ct_freight_ratio
	,cast(0 as decimal(18,4)) as ct_wh_ratio
	,cast(0 as decimal(18,4)) as ct_ord_ratio
	,cast(0 as decimal(18,4)) as ct_packs_ambient_cf
	,cast(0 as decimal(18,4)) as ct_packs_cool_cf
	,cast(0 as decimal(18,4)) as ct_packs_ambient_op
	,cast(0 as decimal(18,4)) as ct_packs_cool_op
	,cast(0 as decimal(18,4)) as ct_eq_units_cf
	,cast(0 as decimal(18,4)) as ct_eq_units_op
	,cast('Not Set' as varchar(100)) as dd_devgroup_flag
from tmp_missing_le t;

drop table if exists tmp_missing_le_op;
create table tmp_missing_le_op as
select dim_dateid, csvop.*
from csv_lc_opcosts csvop, dim_date dt
where calmonth = dt.calendarmonthid and dt.companycode = 'Not Set' and dt.DAYOFMONTH = 1;

drop table if exists tmp_missing_le_op_insert;
create table tmp_missing_le_op_insert as
	select distinct tg.dim_dateid as DIM_DATEIDSTARTDATE
		,trim(leading 0 from tg.BIC_Z_REPUNIT) as dd_cmg_id
from tmp_missing_le_op  tg
	left outer join tmp_logisticcost tl on trim(leading 0 from tg.BIC_Z_REPUNIT) = tl.dd_cmg_id and tg.dim_dateid = tl.dim_dateidstartdate
where tl.dd_cmg_id is null;

insert into tmp_logisticcost (DIM_DATEIDSTARTDATE,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_COOL_AMBIENT,AMT_PACKS_CF,AMT_PACKS_OP,CT_PACKS_CF,CT_PACKS_OP,
		DD_CMG_ID,CT_AMBIENT_FACTOR,CT_COOL_FACTOR,CT_FREIGHT_RATIO,CT_WH_RATIO,CT_ORD_RATIO,ct_packs_ambient_cf,ct_packs_cool_cf,
		ct_packs_ambient_op,ct_packs_cool_op,ct_eq_units_cf,ct_eq_units_op,DD_DEVGROUP_FLAG)
select t.DIM_DATEIDSTARTDATE
	,1 as dim_jda_locationid
	,1 as dim_jda_productid
	,'Cool' as dd_cool_ambient
	, 0 as amt_packs_cf
	, 0 as amt_packs_op
	, 0 as ct_packs_cf
	, 0 as ct_packs_op
	,t.DD_CMG_ID
	,cast(0 as decimal(18,4)) as ct_ambient_factor
	,cast(0 as decimal(18,4)) as ct_cool_factor
	,cast(0 as decimal(18,4)) as ct_freight_ratio
	,cast(0 as decimal(18,4)) as ct_wh_ratio
	,cast(0 as decimal(18,4)) as ct_ord_ratio
	,cast(0 as decimal(18,4)) as ct_packs_ambient_cf
	,cast(0 as decimal(18,4)) as ct_packs_cool_cf
	,cast(0 as decimal(18,4)) as ct_packs_ambient_op
	,cast(0 as decimal(18,4)) as ct_packs_cool_op
	,cast(0 as decimal(18,4)) as ct_eq_units_cf
	,cast(0 as decimal(18,4)) as ct_eq_units_op
	,cast('Not Set' as varchar(100)) as dd_devgroup_flag
from tmp_missing_le_op_insert t;

--set Ambient_Cool flag
update tmp_logisticcost t
	set t.dd_cool_ambient = ifnull(cool_ambient,'Not Set')
from tmp_logisticcost t, dim_jda_product dp, csv_lc_cool_amb csvlc
where t.dim_jda_productid = dp.dim_jda_productid
	and upper(dp.BRANDDESCR) = upper(csvlc.BRAND)
	and t.dd_cool_ambient <> ifnull(cool_ambient,'Not Set');

---Ambient_Cool flag exceptions
update tmp_logisticcost t
	set dd_cool_ambient = cool_ambient
from tmp_logisticcost t, dim_jda_product dp, csv_lc_cool_amb_exception csvlc
where t.dim_jda_productid = dp.dim_jda_productid
and product_name = item;

update tmp_logisticcost t
set ct_packs_ambient_cf = case when DD_COOL_AMBIENT = 'Ambient' then ct_packs_cf
							else 0 end,
	ct_packs_cool_cf = case when DD_COOL_AMBIENT = 'Cool' then ct_packs_cf
						else 0 end,
	ct_packs_ambient_op = case when DD_COOL_AMBIENT = 'Ambient' then ct_packs_op
							else 0 end,
	ct_packs_cool_op = case when DD_COOL_AMBIENT = 'Cool' then ct_packs_op
						else 0 end;

/* add group developers */
drop table if exists tmp_group_dev1;
create table tmp_group_dev1 as
	select LE_CODE, product_name,location_name, stsc_sourcing_source
		,f.DIM_DATEIDSTARTDATE
		,f.DIM_JDA_LOCATIONID
		,f.DIM_JDA_PRODUCTID
		,f.DD_COOL_AMBIENT
		,SUM(f.AMT_PACKS_CF) as AMT_PACKS_CF
		,SUM(f.AMT_PACKS_OP) as AMT_PACKS_OP
		,SUM(f.CT_PACKS_CF) as CT_PACKS_CF
		,SUM(f.CT_PACKS_OP) as CT_PACKS_OP
		,f.DD_CMG_ID
		,SUM(f.CT_AMBIENT_FACTOR) as CT_AMBIENT_FACTOR
		,SUM(f.CT_COOL_FACTOR) as CT_COOL_FACTOR
		,SUM(f.CT_FREIGHT_RATIO) as CT_FREIGHT_RATIO
		,SUM(f.CT_WH_RATIO) as CT_WH_RATIO
		,SUM(f.CT_ORD_RATIO) as CT_ORD_RATIO
		,SUM(f.CT_PACKS_AMBIENT_CF) as CT_PACKS_AMBIENT_CF
		,SUM(f.CT_PACKS_COOL_CF) as CT_PACKS_COOL_CF
		,SUM(f.CT_PACKS_AMBIENT_OP) as CT_PACKS_AMBIENT_OP
		,SUM(f.CT_PACKS_COOL_OP) as CT_PACKS_COOL_OP
		,SUM(f.CT_EQ_UNITS_CF) as CT_EQ_UNITS_CF
		,SUM(f.CT_EQ_UNITS_OP) as CT_EQ_UNITS_OP
		,f.DD_DEVGROUP_FLAG
	from tmp_logisticcost f
		inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
		inner join dim_jda_product dp on f.dim_jda_productid = dp.dim_jda_productid
		inner join dim_jda_location dl on f.dim_jda_locationid = dl.dim_jda_locationid
		inner join stsc_sourcing ss on ss.stsc_sourcing_dest = location_name and ss.stsc_sourcing_item = product_name
		inner join csv_group_developers csvgd on JDA_CODES = stsc_sourcing_source
group by csvgd.LE_CODE, dp.product_name,dl.location_name, ss.stsc_sourcing_source ,f.DIM_DATEIDSTARTDATE
		,f.DIM_JDA_LOCATIONID ,f.DIM_JDA_PRODUCTID,f.DD_COOL_AMBIENT,f.DD_CMG_ID,f.DD_DEVGROUP_FLAG;

drop table if exists tmp_group_dev;
create table tmp_group_dev as
select
	row_number() over(partition by LE_CODE, dd_CMG_ID,location_name, product_name, DIM_DATEIDSTARTDATE order by '') as rowno, f.*
 from tmp_group_dev1 f;

insert into tmp_logisticcost (DIM_DATEIDSTARTDATE,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_COOL_AMBIENT,AMT_PACKS_CF,AMT_PACKS_OP,
		CT_PACKS_CF,CT_PACKS_OP,DD_CMG_ID,CT_AMBIENT_FACTOR,CT_COOL_FACTOR,CT_FREIGHT_RATIO,CT_WH_RATIO,CT_ORD_RATIO,
		ct_packs_ambient_cf,ct_packs_cool_cf,ct_packs_ambient_op,ct_packs_cool_op,ct_eq_units_cf,ct_eq_units_op,DD_DEVGROUP_FLAG)
	select t.DIM_DATEIDSTARTDATE
		,t.DIM_JDA_LOCATIONID
		,t.DIM_JDA_PRODUCTID
		,t.DD_COOL_AMBIENT
		,0 as AMT_PACKS_CF
		,0 as AMT_PACKS_OP
		,t.CT_PACKS_CF
		,t.CT_PACKS_OP
		,t.LE_CODE
		,t.CT_AMBIENT_FACTOR
		,t.CT_COOL_FACTOR
		,t.CT_FREIGHT_RATIO
		,t.CT_WH_RATIO
		,t.CT_ORD_RATIO
		,t.ct_packs_ambient_cf
		,t.ct_packs_cool_cf
		,t.ct_packs_ambient_op
		,t.ct_packs_cool_op
		,t.ct_eq_units_cf
		,t.ct_eq_units_op
		,concat('LEDest: ',t.DD_CMG_ID,', SSource: ',t.stsc_sourcing_source) as dd_devgroup_flag
	from tmp_group_dev t
	where t.DD_CMG_ID <> t.LE_CODE
	and t.rowno = 1 ;

drop table if exists tmp_ledemand;
create table tmp_ledemand as
	select distinct t.LE_CODE ,t.dd_cmg_id,t.dim_jda_productid , t.dim_jda_locationid
	from tmp_group_dev t;

update tmp_logisticcost f
	set f.dd_devgroup_flag = concat('LE Group Dev:',t.LE_CODE)
from tmp_logisticcost f, tmp_ledemand t
where f.dd_cmg_id = t.dd_cmg_id
		and f.dim_jda_productid = t.dim_jda_productid
		and f.dim_jda_locationid = t.dim_jda_locationid
		and f.dd_devgroup_flag = 'Not Set'
		and t.LE_CODE <> t.dd_cmg_id;

update tmp_logisticcost t
	set t.CT_AMBIENT_FACTOR = csvf.AMBIENT_FACTOR
		,t.CT_COOL_FACTOR = csvf.COOL_FACTOR
from tmp_logisticcost t, csv_lc_cool_amb_conv_factor csvf
where t.DD_CMG_ID = csvf.LE_CODE;

update tmp_logisticcost t
	set t.CT_FREIGHT_RATIO = csvr.FREIGHT_RATIO
		,t.CT_WH_RATIO = csvr.WH_RATIO
		,t.CT_ORD_RATIO = csvr.ORD_RATIO
	from tmp_logisticcost t, csv_lc_fix_var_cost_ratio csvr
	where t.DD_CMG_ID = csvr.LE_CODE;

update tmp_logisticcost t
	set ct_eq_units_cf = case when DD_COOL_AMBIENT = 'Ambient' then ct_packs_cf * ct_ambient_factor
							when DD_COOL_AMBIENT = 'Cool' then ct_packs_cf * ct_cool_factor
							else 0
						end,
		ct_eq_units_op = case when DD_COOL_AMBIENT = 'Ambient' then ct_packs_op * ct_ambient_factor
							when DD_COOL_AMBIENT = 'Cool' then ct_packs_op * ct_cool_factor
							else 0
						end;

delete from fact_logisticcost;
delete from number_fountain m where m.table_name = 'fact_logisticcost';
insert into number_fountain
	select 'fact_logisticcost',
		ifnull(max(d.fact_logisticcostid ),
		ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from fact_logisticcost d
	where d.fact_logisticcostid <> 1;

insert into fact_logisticcost (FACT_LOGISTICCOSTID,DIM_DATEIDSTARTDATE,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,
		DD_COOL_AMBIENT,CT_PACKS_CF,DD_CMG_ID,CT_EQ_UNITS_CF,CT_PACKS_AMBIENT_CF,CT_PACKS_COOL_CF,DIM_LEGALENTITYID,
		CT_AMBIENT_FACTOR,CT_COOL_FACTOR,CT_FREIGHT_RATIO,CT_WH_RATIO,CT_ORD_RATIO,DD_DEVGROUP_FLAG,CT_PACKS_OP,
		CT_EQ_UNITS_OP,CT_PACKS_AMBIENT_OP,CT_PACKS_COOL_OP,AMT_PACKS_CF,AMT_PACKS_OP)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_logisticcost') + row_number() over(order by '') as fact_logisticcostid
		,t.DIM_DATEIDSTARTDATE
		,t.DIM_JDA_LOCATIONID
		,t.DIM_JDA_PRODUCTID
		,t.DD_COOL_AMBIENT
		,sum(t.CT_PACKS_CF) as CT_PACKS_CF
		,t.DD_CMG_ID
		,sum(t.CT_EQ_UNITS_CF) as CT_EQ_UNITS_CF
		,sum(t.CT_PACKS_AMBIENT_CF) as CT_PACKS_AMBIENT_CF
		,sum(t.CT_PACKS_COOL_CF) as CT_PACKS_COOL_CF
		,ifnull(d.DIM_LEGALENTITYID,1) as DIM_LEGALENTITYID
		,max(t.CT_AMBIENT_FACTOR) as CT_AMBIENT_FACTOR
		,max(t.CT_COOL_FACTOR) as CT_COOL_FACTOR
		,max(t.CT_FREIGHT_RATIO) as CT_FREIGHT_RATIO
		,max(t.CT_WH_RATIO) as CT_WH_RATIO
		,max(t.CT_ORD_RATIO) as CT_ORD_RATIO
		,t.DD_DEVGROUP_FLAG
		,sum(t.CT_PACKS_OP) as CT_PACKS_OP
		,sum(t.CT_EQ_UNITS_OP) as CT_EQ_UNITS_OP
		,sum(t.CT_PACKS_AMBIENT_OP) as CT_PACKS_AMBIENT_OP
		,sum(t.CT_PACKS_COOL_OP) as CT_PACKS_COOL_OP
		,sum(t.AMT_PACKS_CF) as AMT_PACKS_CF
		,sum(t.AMT_PACKS_OP) as AMT_PACKS_OP
	from tmp_logisticcost t left outer join dim_legalentity d on trim(leading 0 from LE_Code) = to_char(t.dd_cmg_id)
	group by t.DIM_DATEIDSTARTDATE, t.DIM_JDA_LOCATIONID, t.DIM_JDA_PRODUCTID,t.DD_COOL_AMBIENT,t.DD_CMG_ID,t.DD_DEVGROUP_FLAG,ifnull(d.DIM_LEGALENTITYID,1);

/* calculate the last actual month and set YTD flag for all actual months */
update fact_logisticcost
	set dim_dateidlastactmth = (select dim_Dateid from dim_date
										inner join (select max(calmonth) as calmonth from BIC_AZFOFR00)
											on calendarmonthid = calmonth
										where companycode = 'Not Set' and DAYOFMONTH = 1);

update fact_logisticcost f
		set dd_ytdactual = 'YTD_ACT'
from fact_logisticcost f, dim_Date dt1, dim_date dt2
where dt1.dim_dateid = f.dim_dateidstartdate
		and f.dim_dateidlastactmth = dt2.dim_dateid
		and month(dt1.datevalue) <= month(dt2.datevalue);

drop table if exists tmp_fact_logisticcost_agglevelid;
create table tmp_fact_logisticcost_agglevelID as
select distinct DD_CMG_ID,  datevalue ,
 max(FACT_LOGISTICCOSTID) over (partition by f.DIM_LEGALENTITYID, f.DIM_DATEIDSTARTDATE) as FACT_LOGISTICCOSTID
from fact_logisticcost f
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join dIM_LEGALENTITY dl on f.dIM_LEGALENTITYid = dl.dIM_LEGALENTITYid
where DD_COOL_AMBIENT <> 'Not Set';

update fact_logisticcost f
		set f.DD_MAXIDFLAG = 1
from fact_logisticcost f, tmp_fact_logisticcost_agglevelID t
where f.FACT_LOGISTICCOSTID = t.FACT_LOGISTICCOSTID;

/* APP-6080 default with 1 all Equivalent Units(Actual and Future) UNLESS the volumes are consistently different from zero in the last 6 Actual months - Oana 26April2017 */
drop table if exists tmp_equnits_default1;
create table tmp_equnits_default1 as
select  dd_cmg_id
	,sum(checkrow) checkrow
from (select dd_cmg_id
		,sum(CT_EQ_UNITS_CF) as ct_eq_actual_total_cf
		,case when sum(CT_EQ_UNITS_CF) <> 0  then 1 else 0 end as checkrow
	from fact_logisticcost f
		inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
		inner join dim_date dt2 on f.dim_dateidlastactmth = dt2.dim_dateid
	where dt.datevalue between (dt2.datevalue - INTERVAL '5' MONTH) and dt2.datevalue
	group by dt.datevalue, dd_cmg_id)
group by dd_cmg_id;

update fact_logisticcost f
	set f.CT_EQ_UNITS_CF = 0
	from fact_logisticcost f, tmp_equnits_default1 t
	where f.dd_cmg_id = t.dd_cmg_id
		and checkrow < 6;

update fact_logisticcost f
	set f.CT_EQ_UNITS_CF = 1
from fact_logisticcost f, tmp_equnits_default1 t, tmp_fact_logisticcost_agglevelID tid
where f.dd_cmg_id = t.dd_cmg_id
	and f.fact_logisticcostid = tid.fact_logisticcostid
	and checkrow < 6;
/* END APP-6080 default with 1 all Equivalent Units(Actual and Future) UNLESS the volumes are consistently different from zero in the last 6 Actual months - Oana 26April2017 */

--monthly Gauss costs
update fact_logisticcost f
	set f.AMT_FREIGHT_MONTHLY_COST = t.amount
from fact_logisticcost f, tmp_lc_gausscost_bymonth t, tmp_fact_logisticcost_agglevelID tid
where t.ctype = 'Freight'
	and f.dim_dateidstartdate = t.dim_dateid
	and f.dd_cmg_id = trim(leading 0 from t.BIC_Z_REPUNIT)
	and f.fact_logisticcostid = tid.fact_logisticcostid;

update fact_logisticcost f
	set f.AMT_WH_MONTHLY_COST = t.amount
from fact_logisticcost f, tmp_lc_gausscost_bymonth t, tmp_fact_logisticcost_agglevelid tid
where t.ctype = 'Warehousing and Distribution'
	and f.dim_dateidstartdate = t.dim_dateid
	and f.dd_cmg_id = trim(leading 0 from t.BIC_Z_REPUNIT)
	and f.fact_logisticcostid = tid.fact_logisticcostid;

update fact_logisticcost f
	set f.AMT_DEMAND_MONTHLY_COST = t.amount
from fact_logisticcost f, tmp_lc_gausscost_bymonth t, tmp_fact_logisticcost_agglevelid tid
where t.ctype = 'Global Demand'
	and f.dim_dateidstartdate = t.dim_dateid
	and f.dd_cmg_id = trim(leading 0 from t.BIC_Z_REPUNIT)
	and f.fact_logisticcostid = tid.fact_logisticcostid;

--Actual Cost for the last 6 months
drop table if exists tmp_lc_gausscost;
    create table tmp_lc_gausscost as
    	select ctype
    		,BIC_Z_REPUNIT
    		,sum(amount) amount
    	from tmp_lc_gausscost_bymonth
    	where to_date(to_char(CALMONTH),'YYYYMM') between date_trunc('MONTH',current_date - INTERVAL '6' MONTH)
    		and date_trunc('MONTH',current_date - INTERVAL '1' MONTH)
    	group by ctype, BIC_Z_REPUNIT;

update fact_logisticcost f
    	set f.amt_freight_cost = t.amount
    from fact_logisticcost f, tmp_lc_gausscost t, tmp_fact_logisticcost_agglevelid tid
    where t.ctype = 'Freight'
    	and f.dd_cmg_id = trim(leading 0 from t.BIC_Z_REPUNIT)
		and f.fact_logisticcostid = tid.fact_logisticcostid;

update fact_logisticcost f
    	set f.amt_wh_cost = t.amount
    from fact_logisticcost f, tmp_lc_gausscost t, tmp_fact_logisticcost_agglevelid tid
    where t.ctype = 'Warehousing and Distribution'
    	and f.dd_cmg_id = trim(leading 0 from t.BIC_Z_REPUNIT)
		and f.fact_logisticcostid = tid.fact_logisticcostid;

update fact_logisticcost f
    	set f.amt_demand_cost = t.amount
    from fact_logisticcost f, tmp_lc_gausscost t, tmp_fact_logisticcost_agglevelid tid
    where t.ctype = 'Global Demand'
    	and f.dd_cmg_id = trim(leading 0 from t.BIC_Z_REPUNIT)
		and f.fact_logisticcostid = tid.fact_logisticcostid;

--OP costs
update fact_logisticcost f
    	set f.AMT_FREIGHT_OP_MONTHLY_COST = csvop.OP_COST*(-1)
from fact_logisticcost f, csv_lc_opcosts csvop , dim_date dt, tmp_fact_logisticcost_agglevelid tid
where f.dim_dateidstartdate = dt.dim_dateid
    	and csvop.BIC_Z_REPPOS = '3110440000'
    	and f.dd_cmg_id = trim(leading 0 from csvop.BIC_Z_REPUNIT)
    	and csvop.calmonth = dt.calendarmonthid
		and f.fact_logisticcostid = tid.fact_logisticcostid;

update fact_logisticcost f
    	set f.AMT_WH_OP_MONTHLY_COST = csvop.OP_COST*(-1)
from fact_logisticcost f, csv_lc_opcosts csvop , dim_date dt, tmp_fact_logisticcost_agglevelid tid
where f.dim_dateidstartdate = dt.dim_dateid
    	and csvop.BIC_Z_REPPOS = '3110420000'
    	and f.dd_cmg_id = trim(leading 0 from csvop.BIC_Z_REPUNIT)
    	and csvop.calmonth = dt.calendarmonthid
		and f.fact_logisticcostid = tid.fact_logisticcostid;

update fact_logisticcost f
    	set f.AMT_DEMAND_OP_MONTHLY_COST = csvop.OP_COST*(-1)
from fact_logisticcost f, csv_lc_opcosts csvop , dim_date dt, tmp_fact_logisticcost_agglevelid tid
where f.dim_dateidstartdate = dt.dim_dateid
    	and csvop.BIC_Z_REPPOS = '3110430000'
    	and f.dd_cmg_id = trim(leading 0 from csvop.BIC_Z_REPUNIT)
    	and csvop.calmonth = dt.calendarmonthid
		and f.fact_logisticcostid = tid.fact_logisticcostid;

--Equivalent Units for Actuals and OP
drop table if exists tmp_eq_actual_total;
create table tmp_eq_actual_total as
select dd_cmg_id
	,sum(CT_EQ_UNITS_CF) as ct_eq_actual_total_cf
	,sum(CT_EQ_UNITS_OP) as ct_eq_actual_total_op
from fact_logisticcost f
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join dim_date dt2 on f.dim_dateidlastactmth = dt2.dim_dateid
where dt.datevalue between (dt2.datevalue - INTERVAL '5' MONTH) and dt2.datevalue
group by dd_cmg_id;

update fact_logisticcost f
	set f.ct_eq_actual_total_cf = t.ct_eq_actual_total_cf,
		f.ct_eq_actual_total_op = t.ct_eq_actual_total_op
from fact_logisticcost f, tmp_eq_actual_total t, tmp_fact_logisticcost_agglevelid tid
	where f.dd_cmg_id = t.dd_cmg_id
		and f.fact_logisticcostid = tid.fact_logisticcostid;

drop table if exists tmp_eq_units_total;
	create table tmp_eq_units_total as
		select dim_dateidstartdate
				,dd_cmg_id
				,sum(CT_EQ_UNITS_CF) as ct_eq_units
		from fact_logisticcost f, dim_date dt
		where dt.dim_dateid = f.dim_dateidstartdate
		group by dim_dateidstartdate, dd_cmg_id;

update fact_logisticcost f
		set f.ct_eq_units = t.ct_eq_units
from fact_logisticcost f, tmp_eq_units_total t, tmp_fact_logisticcost_agglevelid tid
where f.dd_cmg_id = t.dd_cmg_id
		and f.dim_dateidstartdate = t.dim_dateidstartdate
		and f.fact_logisticcostid = tid.fact_logisticcostid;

drop table if exists tmp_expectedcost;
	create table tmp_expectedcost as
			select f.dim_dateidstartdate,f.dd_cmg_id
					,SUM(CASE WHEN dt.datevalue > dt2.datevalue  AND ct_eq_actual_total_cf <> 0
									THEN (amt_wh_cost*ct_wh_ratio/6) + (ct_eq_units * (amt_wh_cost - amt_wh_cost * ct_wh_ratio)/ ct_eq_actual_total_cf)
								ELSE 0 END) as amt_wh_expected
					,SUM(CASE WHEN dt.datevalue > dt2.datevalue AND ct_eq_actual_total_cf <> 0
									THEN (amt_freight_cost*ct_freight_ratio/6) + (ct_eq_units * (amt_freight_cost - amt_freight_cost * ct_freight_ratio)/ ct_eq_actual_total_cf)
								ELSE 0 END) as amt_freight_expected
					,SUM(CASE WHEN dt.datevalue > dt2.datevalue AND ct_eq_actual_total_cf <> 0
									THEN (amt_demand_cost*ct_ord_ratio/6) + (ct_eq_units * (amt_demand_cost - amt_demand_cost * ct_ord_ratio)/ ct_eq_actual_total_cf)
								ELSE 0 END) as amt_demand_expected
			from fact_logisticcost f, dim_date dt, dim_date dt2
			where dt.dim_dateid = f.dim_dateidstartdate
				and f.dim_dateidlastactmth = dt2.dim_dateid
			group by f.dim_dateidstartdate, f.dd_cmg_id;

update fact_logisticcost f
	set f.amt_wh_expected = case when t.amt_wh_expected > (1.5 * f.amt_wh_cost/6) then (1.5 * f.amt_wh_cost/6)
																when t.amt_wh_expected < (0.5 * f.amt_wh_cost/6) then (0.5 * f.amt_wh_cost/6)
																else t.amt_wh_expected
													end,
		 	f.amt_freight_expected = case when t.amt_freight_expected > (1.5 * f.amt_freight_cost/6) then (1.5 * f.amt_freight_cost/6)
																		when t.amt_freight_expected < (0.5 * f.amt_freight_cost/6) then (0.5 * f.amt_freight_cost/6)
																		else t.amt_freight_expected
															end,
		 	f.amt_demand_expected = case when t.amt_demand_expected > (1.5 * f.amt_demand_cost/6) then (1.5 * f.amt_demand_cost/6)
																		when t.amt_demand_expected < (0.5 * f.amt_demand_cost/6) then (0.5 * f.amt_demand_cost/6)
																		else t.amt_demand_expected
															end
from fact_logisticcost f, tmp_expectedcost t, dim_date dt1, dim_date dt2 , tmp_fact_logisticcost_agglevelid tid
where f.dd_cmg_id = t.dd_cmg_id
		and f.dim_dateidstartdate = t.dim_dateidstartdate
		and f.dim_dateidlastactmth = dt1.dim_dateid
		and t.dim_dateidstartdate = dt2.dim_dateid
		and dt2.datevalue > dt1.datevalue
		and f.fact_logisticcostid = tid.fact_logisticcostid;

/* APP-6261 - Expected Cost corrections from file */
update fact_logisticcost f
	set f.amt_freight_corrected = t.cost
from fact_logisticcost f, dim_date dt1, csv_lc_correction t, dim_date dt2 , tmp_fact_logisticcost_agglevelid tid
where f.dd_cmg_id = left(t.legal_entity,4)
		and t.datevalue = dt2.datevalue
		and f.dim_dateidstartdate = dt2.dim_dateid
		and f.dim_dateidlastactmth = dt1.dim_dateid
		and t.datevalue > dt1.datevalue
		and f.fact_logisticcostid = tid.fact_logisticcostid
		and left(t.cost_type,10) = '3110440000';

update fact_logisticcost f
	set f.amt_wh_corrected = t.cost
from fact_logisticcost f, dim_date dt1, csv_lc_correction t, dim_date dt2 , tmp_fact_logisticcost_agglevelid tid
where f.dd_cmg_id = left(t.legal_entity,4)
			and t.datevalue = dt2.datevalue
			and f.dim_dateidstartdate = dt2.dim_dateid
			and f.dim_dateidlastactmth = dt1.dim_dateid
			and t.datevalue > dt1.datevalue
			and f.fact_logisticcostid = tid.fact_logisticcostid
			and left(t.cost_type,10) = '3110420000';

update fact_logisticcost f
	set f.amt_demand_corrected = t.cost
from fact_logisticcost f, dim_date dt1, csv_lc_correction t, dim_date dt2 , tmp_fact_logisticcost_agglevelid tid
where f.dd_cmg_id = left(t.legal_entity,4)
		and t.datevalue = dt2.datevalue
		and f.dim_dateidstartdate = dt2.dim_dateid
		and f.dim_dateidlastactmth = dt1.dim_dateid
		and t.datevalue > dt1.datevalue
		and f.fact_logisticcostid = tid.fact_logisticcostid
		and left(t.cost_type,10) = '3110430000';

update fact_logisticcost f
	set f.amt_freight_corrected =  f.amt_freight_expected
from fact_logisticcost f
where f.amt_freight_corrected = 0;

update fact_logisticcost f
	set f.amt_wh_corrected = f.amt_wh_expected
from fact_logisticcost f
where f.amt_wh_corrected  = 0;

update fact_logisticcost f
	set f.amt_demand_corrected = f.amt_demand_expected
from fact_logisticcost f
where f.amt_demand_corrected = 0;
/* END APP-6261 - Expected Cost corrections from file */
