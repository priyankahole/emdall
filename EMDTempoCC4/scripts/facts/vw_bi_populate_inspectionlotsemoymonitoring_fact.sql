

DROP TABLE IF EXISTS number_fountain_fact_inspectionlotsemoymonitoring;
CREATE TABLE number_fountain_fact_inspectionlotsemoymonitoring 
LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoymonitoring;
CREATE TABLE tmp_fact_inspectionlotsemoymonitoring 
LIKE fact_inspectionlotsemoymonitoring INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_inspectionlotsemoymonitoring 
WHERE table_name = 'fact_inspectionlotsemoymonitoring';	

INSERT INTO number_fountain_fact_inspectionlotsemoymonitoring
SELECT 'fact_inspectionlotsemoymonitoring', IFNULL(max(f.fact_inspectionlotsemoymonitoringid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM tmp_fact_inspectionlotsemoymonitoring AS f;

INSERT INTO tmp_fact_inspectionlotsemoymonitoring(
  fact_inspectionlotsemoymonitoringid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, -- S_SAMPLEID 
  dd_requestid, -- REQUESTID
  dd_batchstageid, -- BATCHSTAGEID
  dd_batchid, -- BATCHID
  dd_usersampleid, -- U_USERSAMPLEID
  dd_materialid, -- U_SL_MATERIALID
  dd_materialtype, -- U_MATERIALTYPE
  dd_materialversionid, -- U_SL_MATERIALVERSIONID
  dd_securitydepartment, -- SECURITYDEPARTMENT
  dd_samplestatus, -- SAMPLESTATUS
  dd_samplecreatedt, -- SAMPLE_CREATEDT
  dim_samplecreatedateid,
  dd_samplecreateby, -- SAMPLE_CREATEBY
  dd_receiveddt, -- SAMPLE_RECEIVEDDT
  dim_receiveddateid,
  dd_batchstatus, -- BATCHSTATUS
  dd_lotnumber, -- U_LOTNUMBER
  dd_sapbatchno, -- SAP_BATCHNO
  dd_batchstagestatus, -- BATCHSTAGESTATUS
  dd_batchstagedesc, -- BATCHSTAGEDESC
  dd_stagename, -- STAGE_NAME
  dd_requestdesc, -- REQUESTDESC
  dd_requeststatus, -- REQUESTSTATUS
  dd_materialname, -- MATERIALNAME
  dd_materialfamily, -- U_MATERIALFAMILY
  dd_paramlistid, -- PARAMLISTID
  dd_paramlistversionid, -- PARAMLISTVERSIONID
  dd_paramlistvariantid, -- PARAMLIST_VARIANTID
  dd_datasetstatus, -- S_DATASETSTATUS
  dd_sdidataid, -- SDIDATAID
  dd_paramlistname, -- PARAMLIST_NAME
  dd_colournameflag,
  ct_avancement,
  dd_verificationcolournameflag,
  dd_headerrowforcalculations,
  dd_routineflag,
  dd_demandsupplydate, -- paendterm -- QALS -> Inspection End Date from SAP
  dim_demandsupplydateid,
  dd_mporvpflag,
  dd_articlebatch,
  ct_colournumberflag,
  dd_avancementpercentage,
  dd_hcdeviationdepartment,
  dd_nonroutinecheckflag,
  dd_aggregatedlevelstatusflag,
  dd_detailedlevelstatusflag,
  dd_cycletimevalue,
  dd_receiveddatepluscycletime,
  dim_receiveddatepluscycletimedateid,
  dd_dateqcflag,
  dd_categorystatusflag,
  ct_linecount,
  dd_dateqccolourflag,
  dd_nonroutinecolourflag,
  dd_trafficlightforparamlistname,
  dd_trafficlightforverificationaggregated,
  dd_trafficlightforverificationdetailed
)
SELECT
  (SELECT max_id 
    FROM number_fountain_fact_inspectionlotsemoymonitoring
    WHERE table_name = 'fact_inspectionlotsemoymonitoring') 
      + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoymonitoringid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  CURRENT_TIMESTAMP AS dw_insert_date, 
  CURRENT_TIMESTAMP AS dw_update_date,
  IFNULL(s_sampleid,'Not Set') AS dd_sampleid, -- S_SAMPLEID 
  IFNULL(requestid,'Not Set') AS dd_requestid, -- REQUESTID
  IFNULL(batchstageid,'Not Set') AS dd_batchstageid, -- BATCHSTAGEID
  IFNULL(batchid,'Not Set') AS dd_batchid, -- BATCHID
  IFNULL(u_usersampleid,'Not Set') AS dd_usersampleid, -- U_USERSAMPLEID
  IFNULL(u_sl_materialid,'Not Set') AS dd_materialid, -- U_SL_MATERIALID
  IFNULL(u_materialtype,'Not Set') AS dd_materialtype, -- U_MATERIALTYPE
  IFNULL(u_sl_materialversionid,'Not Set') AS dd_materialversionid, -- U_SL_MATERIALVERSIONID
  IFNULL(securitydepartment,'Not Set') AS dd_securitydepartment, -- SECURITYDEPARTMENT
  IFNULL(samplestatus,'Not Set') AS dd_samplestatus, -- SAMPLESTATUS
  IFNULL(sample_createdt,'0001-01-01') AS dd_samplecreatedt, -- SAMPLE_CREATEDT
  CAST(1 AS BIGINT) AS dim_samplecreatedateid,
  IFNULL(sample_createby,'Not Set') AS dd_samplecreateby, -- SAMPLE_CREATEBY
  IFNULL(sample_receiveddt,'0001-01-01') AS dd_receiveddt, -- SAMPLE_RECEIVEDDT
  CAST(1 AS BIGINT) AS dim_receiveddateid,
  IFNULL(batchstatus,'Not Set') AS dd_batchstatus, -- BATCHSTATUS
  IFNULL(u_lotnumber,'Not Set') AS dd_lotnumber, -- U_LOTNUMBER
  IFNULL(sap_batchno,'Not Set') AS dd_sapbatchno, -- SAP_BATCHNO
  IFNULL(batchstagestatus,'Not Set') AS dd_batchstagestatus, -- BATCHSTAGESTATUS
  IFNULL(batchstagedesc,'Not Set') AS dd_batchstagedesc, -- BATCHSTAGEDESC
  IFNULL(stage_name,'Not Set') AS dd_stagename, -- STAGE_NAME
  IFNULL(requestdesc,'Not Set') AS dd_requestdesc, -- REQUESTDESC
  IFNULL(requeststatus,'Not Set') AS dd_requeststatus, -- REQUESTSTATUS
  IFNULL(materialname,'Not Set') AS dd_materialname, -- MATERIALNAME
  IFNULL(u_materialfamily,'Not Set') AS dd_materialfamily, -- U_MATERIALFAMILY
  IFNULL(paramlistid,'Not Set') AS dd_paramlistid, -- PARAMLISTID
  IFNULL(paramlistversionid,'Not Set') AS dd_paramlistversionid, -- PARAMLISTVERSIONID
  IFNULL(paramlist_variantid,'Not Set') AS dd_paramlistvariantid, -- PARAMLIST_VARIANTID
  IFNULL(s_datasetstatus,'Not Set') AS dd_datasetstatus, -- S_DATASETSTATUS
  IFNULL(sdidataid,'Not Set') AS dd_sdidataid, -- SDIDATAID
  IFNULL(paramlist_name,'Not Set') AS dd_paramlistname, -- PARAMLIST_NAME
  CASE
    WHEN UPPER(s_datasetstatus) = 'INITIAL' THEN 'RED'
    WHEN UPPER(s_datasetstatus) IN ('DATAENTERED','INPROGRESS') THEN 'AMBER'
    WHEN UPPER(s_datasetstatus) IN ('RELEASED','COMPLETED') THEN 'GREEN'
    ELSE 'Not Set'
  END AS dd_colournameflag,
  0 AS ct_avancement,
  'Not Set' AS dd_verificationcolournameflag,
  'Not Set' AS dd_headerrowforcalculations,
  CASE
    WHEN requestdesc IS NOT NULL THEN 'Non Routine'
    ELSE 'Routine'
  END AS dd_routineflag,
  CAST('0001-01-01' AS DATE) AS dd_demandsupplydate,
  CAST(1 AS BIGINT) AS dim_demandsupplydateid,
  'Not Set' AS dd_mporvpflag,
  'Not Set' AS dd_articlebatch,
  CASE
    WHEN UPPER(s_datasetstatus) = 'INITIAL' THEN 3
    WHEN UPPER(s_datasetstatus) IN ('DATAENTERED','INPROGRESS') THEN 2
    WHEN UPPER(s_datasetstatus) IN ('RELEASED','COMPLETED') THEN 1
    ELSE 0
  END AS ct_colournumberflag,
  'Not Set' AS dd_avancementpercentage,
  'Not Set' AS dd_hcdeviationdepartment,
  'Not Set' AS dd_nonroutinecheckflag,
  'Not Set' AS dd_aggregatedlevelstatusflag,
  'Not Set' AS dd_detailedlevelstatusflag,
  0 AS dd_cycletimevalue,
  CAST('0001-01-01' AS DATE) AS dd_receiveddatepluscycletime,
  CAST(1 AS BIGINT) AS dim_receiveddatepluscycletimedateid,
  'Not Set' AS dd_dateqcflag,
  'Not Set' AS dd_categorystatusflag,
  1 AS ct_linecount,
  0 AS dd_dateqccolourflag,
  0 AS dd_nonroutinecolourflag,
  0 AS dd_trafficlightforparamlistname,
  0 AS dd_trafficlightforverificationaggregated,
  0 AS dd_trafficlightforverificationdetailed
FROM
  v_mcockpit_suiviavancementtest
WHERE
  UPPER(s_datasetstatus) NOT IN ('CANCELLED')
  --AND UPPER(requeststatus) NOT IN ('RELEASED','REJECTED')
  --AND UPPER(batchstagestatus) NOT IN ('INITIAL','CANCELLED')
;


/* BEGIN - update dim dates from fact */
UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dim_samplecreatedateid = IFNULL(dd.dim_dateid, 1)
FROM  tmp_fact_inspectionlotsemoymonitoring AS f, dim_date AS dd 
WHERE LEFT(f.dd_samplecreatedt, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_samplecreatedateid <> dd.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dim_receiveddateid = IFNULL(dd.dim_dateid, 1)
FROM  tmp_fact_inspectionlotsemoymonitoring AS f, dim_date AS dd 
WHERE LEFT(f.dd_receiveddt, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_receiveddateid <> dd.dim_dateid;
/* END - update dim dates from fact */

/* BEGIN - aggregated and detailed level status flag updates */
UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_detailedlevelstatusflag =
  CASE 
    WHEN UPPER(f.dd_datasetstatus) = 'COMPLETED' THEN 'GREEN'
    ELSE 'RED'
  END;
/* END - aggregated and detailed level status flag updates */

/* BEGIN - update avancement and verification fields logic */
DROP TABLE IF EXISTS tmp_getavancementpercentage_ilsem;
CREATE TABLE tmp_getavancementpercentage_ilsem
AS
SELECT
  MIN(fact_inspectionlotsemoymonitoringid) AS fact_inspectionlotsemoymonitoringid,
  dd_materialfamily,
  dd_materialname,
  dd_sapbatchno,
  dd_sampleid,
  CASE 
    WHEN SUM(CASE WHEN dd_colournameflag IN ('GREEN','RED','AMBER') THEN 1 ELSE 0 END) = 0 THEN 0 
    ELSE SUM(CASE WHEN dd_colournameflag = 'GREEN' THEN 1 ELSE 0 END) / SUM(CASE WHEN dd_colournameflag IN ('GREEN','RED','AMBER') THEN 1 ELSE 0 END) 
  END AS ct_avancement,
  CAST('Not Set' AS VARCHAR(100)) AS dd_verificationcolournameflag,
  SUM(CASE WHEN dd_detailedlevelstatusflag = 'GREEN' THEN 1 ELSE 0 END) as ct_countforaggregatedlevelstatus,
  SUM(1) as ct_totalcountforaggregatedlevelstatus
FROM 
  tmp_fact_inspectionlotsemoymonitoring
GROUP BY 
  dd_materialfamily,
  dd_materialname,
  dd_sapbatchno,
  dd_sampleid;

UPDATE tmp_getavancementpercentage_ilsem
SET dd_verificationcolournameflag = 
  CASE 
    WHEN ct_avancement = 1 THEN 'GREEN'
    WHEN ct_avancement > 0 AND ct_avancement < 1 THEN 'AMBER'
    WHEN ct_avancement = 0 THEN 'RED'
    ELSE 'Not Set'
  END;

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.ct_avancement = t.ct_avancement,
  f.dd_headerrowforcalculations = 'X'
FROM tmp_fact_inspectionlotsemoymonitoring AS f, tmp_getavancementpercentage_ilsem AS t
WHERE f.fact_inspectionlotsemoymonitoringid = t.fact_inspectionlotsemoymonitoringid;

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_avancementpercentage = CONCAT(CAST(CAST(100 * t.ct_avancement AS DECIMAL(6,0)) AS VARCHAR(100)), '%'),
  f.dd_verificationcolournameflag = t.dd_verificationcolournameflag,
  f.dd_aggregatedlevelstatusflag =
    CASE 
      WHEN t.ct_countforaggregatedlevelstatus = t.ct_totalcountforaggregatedlevelstatus THEN 'GREEN'
      WHEN t.ct_countforaggregatedlevelstatus >= 1 THEN 'AMBER'
      WHEN t.ct_countforaggregatedlevelstatus = 0 THEN 'RED'
      ELSE 'Not Set'
    END
FROM tmp_fact_inspectionlotsemoymonitoring AS f, tmp_getavancementpercentage_ilsem AS t
WHERE f.dd_materialfamily = t.dd_materialfamily
  AND f.dd_materialname = t.dd_materialname
  AND f.dd_sapbatchno = t.dd_sapbatchno
  AND f.dd_sampleid = t.dd_sampleid;
/* END - update avancement and verification fields logic */


/* BEGIN - article batch logic update */
UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_articlebatch = IFNULL(CONCAT(t.yyd_ygart,'-',f.dd_sapbatchno),'Not Set')
FROM tmp_fact_inspectionlotsemoymonitoring AS f, ilse_families_part2 AS t
WHERE f.dd_materialname = t.matnr;
/* END - article batch logic update */

/* BEGIN - MP vs VP logic */
UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_mporvpflag = 
  CASE 
    WHEN f.dd_materialfamily = 'EAU' THEN 'EAU'
    WHEN f.dd_materialfamily = 'MP' THEN 'MP'
    WHEN f.dd_materialfamily IS NULL THEN 'Not Set'
    ELSE 'VP'
  END;
/* END - MP vs VP logic */


/* BEGIN - Date Demand Supply (This is End of Inspection from SAP) logic */

-- first create a table that contains all the material and sapbatchno 
-- combination that have more lines due to the PAENDTERM date, 
-- after that update all MP logic

DROP TABLE IF EXISTS tmp_getmultiplematerialandbatch_ilsem;
CREATE TABLE tmp_getmultiplematerialandbatch_ilsem
AS 
SELECT DISTINCT 
  i.matnr AS dd_materialname, 
  i.charg AS dd_sapbatchno
FROM ilse_fr44_prio_lab AS i, ilse_fr44_prio_lab AS i1
WHERE i.matnr = i1.matnr
  and i.charg = i1.charg
  and i.paendterm <> i1.paendterm
  AND i.werk = 'FR44'
  AND i1.werk = 'FR44'
  AND UPPER(i.qmatauth) IN ('PHFR0A', 'PHFR1B', 'PHFR1E')
  AND UPPER(i1.qmatauth) IN ('PHFR0A', 'PHFR1B', 'PHFR1E')
  AND i.enstehdat >= '2016-01-01'
  AND i1.enstehdat >= '2016-01-01';

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_demandsupplydate = i.paendterm
FROM 
  tmp_fact_inspectionlotsemoymonitoring AS f, 
  (SELECT DISTINCT matnr, charg, paendterm 
  FROM ilse_fr44_prio_lab 
  WHERE UPPER(werk) = 'FR44'
    AND UPPER(qmatauth) IN ('PHFR0A', 'PHFR1B', 'PHFR1E')
    AND enstehdat >= '2016-01-01') AS i
WHERE 
  f.dd_materialname = i.matnr
  AND f.dd_sapbatchno = i.charg
  AND (f.dd_materialname, f.dd_sapbatchno) NOT IN (SELECT dd_materialname, dd_sapbatchno FROM tmp_getmultiplematerialandbatch_ilsem)
  AND f.dd_mporvpflag = 'MP';

-- VP LOGIC

DROP TABLE IF EXISTS tmp_ilse_fr44_prio_lab_with_articlenumber_ilsem;
CREATE TABLE tmp_ilse_fr44_prio_lab_with_articlenumber_ilsem
AS
SELECT DISTINCT 
  i.matnr AS dd_materialname, 
  i.charg AS dd_sapbatchno, 
  i.paendterm AS dd_demandsupplydate,
  IFNULL(CONCAT(t.yyd_ygart,'-',i.charg),'Not Set') AS dd_articlebatch
FROM ilse_fr44_prio_lab AS i, ilse_families_part2 AS t
WHERE UPPER(i.werk) = 'FR44'
  AND UPPER(i.qmatauth) IN ('PHFR0A', 'PHFR1B', 'PHFR1E')
  AND i.enstehdat >= '2016-01-01'
  AND i.matnr = t.matnr;

-- get the maximum date where we have more dates for VP

DROP TABLE IF EXISTS tmp_getmaxdatedemanduspplydate_ilsem;
CREATE TABLE tmp_getmaxdatedemanduspplydate_ilsem
AS
SELECT 
  dd_articlebatch, 
  MAX(dd_demandsupplydate) AS dd_demandsupplydate
FROM
  tmp_ilse_fr44_prio_lab_with_articlenumber_ilsem
GROUP BY dd_articlebatch;

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_demandsupplydate = md.dd_demandsupplydate
FROM 
  tmp_fact_inspectionlotsemoymonitoring AS f, 
  tmp_getmaxdatedemanduspplydate_ilsem AS md
WHERE 
  f.dd_mporvpflag <> 'MP'
  AND f.dd_articlebatch = md.dd_articlebatch;

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dim_demandsupplydateid = IFNULL(dd.dim_dateid, 1)
FROM tmp_fact_inspectionlotsemoymonitoring AS f, dim_date AS dd 
WHERE dd_demandsupplydate = dd.datevalue 
  AND dd.companycode = 'Not Set'
  AND f.dim_demandsupplydateid <> dd.dim_dateid;
/* END - Date Demand Supply (This is End of Inspection from SAP) logic */


/* BEGIN - routine with a check on non routine logic */
DROP TABLE IF EXISTS tmp_getnonroutinematerials_ilsem;
CREATE TABLE tmp_getnonroutinematerials_ilsem
AS
SELECT DISTINCT
  f.dd_usersampleid
FROM
  tmp_fact_inspectionlotsemoymonitoring AS f
WHERE f.dd_routineflag = 'Non Routine';

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_nonroutinecheckflag = 
  CASE
    WHEN f.dd_routineflag = 'Non Routine' THEN 'Not Set'
    ELSE 'No'
  END;

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_nonroutinecheckflag = 
  CASE
    WHEN f.dd_routineflag = 'Non Routine' THEN 'Not Set'
    ELSE 'Yes'
  END
FROM 
  tmp_fact_inspectionlotsemoymonitoring AS f,
  tmp_getnonroutinematerials_ilsem AS t
WHERE t.dd_usersampleid LIKE CONCAT(f.dd_materialname,'-',f.dd_sapbatchno,'%');
/* END - routine with a check on non routine logic */

/* BEGIN - update cycle time from extraction */

-- first add the time to the column dd_cycletimevalue

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_cycletimevalue = IFNULL(CAST(LEFT(t.cycle_time,LEN(t.cycle_time)-1) AS DECIMAL(18,0)),0)
FROM tmp_fact_inspectionlotsemoymonitoring AS f, tempscycleqc AS t
WHERE UPPER(f.dd_mporvpflag) = 'VP'
  AND UPPER(f.dd_materialfamily) = UPPER(t.famille)
  AND UPPER(t.vpormp) = 'VP';

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_cycletimevalue = IFNULL(CAST(LEFT(t.cycle_time,LEN(t.cycle_time)-1) AS DECIMAL(18,0)),0)
FROM tmp_fact_inspectionlotsemoymonitoring AS f, tempscycleqc AS t
WHERE UPPER(f.dd_mporvpflag) = 'EAU'
  AND UPPER(t.vpormp) = 'EAU';

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_cycletimevalue = IFNULL(CAST(LEFT(t.cycle_time,LEN(t.cycle_time)-1) AS DECIMAL(18,0)),0)
FROM tmp_fact_inspectionlotsemoymonitoring AS f, tempscycleqc AS t
WHERE UPPER(f.dd_mporvpflag) = 'MP'
  AND UPPER(t.vpormp) = 'MP'
  AND f.dd_materialname = t.code_article;

-- next add the working days 

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_receiveddatepluscycletime = dt.datevalue
FROM tmp_fact_inspectionlotsemoymonitoring AS f, dim_date AS dd, dim_date AS dt
WHERE LEFT(dd_receiveddt, 10) = dd.datevalue
  AND dd.companycode = 'Not Set'
  AND dd.businessdaysseqno + f.dd_cycletimevalue = dt.businessdaysseqno 
  AND UPPER(dt.isaweekendday2) = 'NO'
  AND dt.companycode = 'Not Set'
;

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dim_receiveddatepluscycletimedateid = IFNULL(dd.dim_dateid, 1)
FROM tmp_fact_inspectionlotsemoymonitoring AS f, dim_date AS dd 
WHERE dd_receiveddatepluscycletime = dd.datevalue 
  AND dd.companycode = 'Not Set'
  AND f.dim_receiveddatepluscycletimedateid <> dd.dim_dateid;

-- finally compare the two dates

UPDATE tmp_fact_inspectionlotsemoymonitoring AS f
SET f.dd_dateqcflag = 
  CASE 
    WHEN f.dd_demandsupplydate < f.dd_receiveddatepluscycletime THEN 'RED'
    ELSE 'GREEN'
  END
;

/* END - update cycle time from extraction */


/* BEGIN - update category status */

UPDATE tmp_fact_inspectionlotsemoymonitoring
SET dd_categorystatusflag =
  CASE 
    WHEN dd_avancementpercentage <> '100%' THEN 'EN ATTENTE ANALYSE'
    WHEN dd_avancementpercentage = '100%' AND dd_aggregatedlevelstatusflag <> 'GREEN'  THEN 'EN ATTENTE VÉRIFICATION'
    WHEN dd_avancementpercentage = '100%' AND dd_aggregatedlevelstatusflag = 'GREEN' THEN 'EN ATTENTE CONCLUSION ANALYTIQUE'
    ELSE 'Not Set'
  END;

/* END - update category status */

/* BEGIN - update flags from colour naming to numbers */

UPDATE tmp_fact_inspectionlotsemoymonitoring
SET dd_dateqccolourflag = 
  CASE 
    WHEN UPPER(dd_dateqcflag) = 'GREEN' THEN 1 
    WHEN UPPER(dd_dateqcflag) = 'RED' THEN 2
    ELSE 0
  END;

UPDATE tmp_fact_inspectionlotsemoymonitoring
SET dd_nonroutinecolourflag =
  CASE 
    WHEN UPPER(dd_nonroutinecheckflag) = 'AMBER' THEN 1
    WHEN UPPER(dd_nonroutinecheckflag) = 'GREEN' THEN 2
    WHEN UPPER(dd_nonroutinecheckflag) = 'RED' THEN 3
    ELSE 0
  END;

UPDATE tmp_fact_inspectionlotsemoymonitoring
SET dd_trafficlightforparamlistname =
  CASE 
    WHEN UPPER(dd_colournameflag) = 'GREEN' THEN 1 
    WHEN UPPER(dd_colournameflag) = 'AMBER' THEN 2
    WHEN UPPER(dd_colournameflag) = 'RED' THEN 3
    ELSE 0 
  END;

UPDATE tmp_fact_inspectionlotsemoymonitoring
SET dd_trafficlightforverificationaggregated = 
  CASE 
    WHEN UPPER(dd_aggregatedlevelstatusflag) = 'GREEN' THEN 1
    WHEN UPPER(dd_aggregatedlevelstatusflag) = 'AMBER' THEN 2
    WHEN UPPER(dd_aggregatedlevelstatusflag) = 'RED' THEN 3
    ELSE 0 
  END;

UPDATE tmp_fact_inspectionlotsemoymonitoring
SET dd_trafficlightforverificationdetailed =
  CASE 
    WHEN UPPER(dd_detailedlevelstatusflag) = 'GREEN' THEN 1
    WHEN UPPER(dd_detailedlevelstatusflag) = 'AMBER' THEN 2
    WHEN UPPER(dd_detailedlevelstatusflag) = 'RED' THEN 3
    ELSE 0 
  END;

/* END - update flags from colour naming to numbers */

DELETE FROM fact_inspectionlotsemoymonitoring;

INSERT INTO fact_inspectionlotsemoymonitoring(
  fact_inspectionlotsemoymonitoringid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, -- S_SAMPLEID 
  dd_requestid, -- REQUESTID
  dd_batchstageid, -- BATCHSTAGEID
  dd_batchid, -- BATCHID
  dd_usersampleid, -- U_USERSAMPLEID
  dd_materialid, -- U_SL_MATERIALID
  dd_materialtype, -- U_MATERIALTYPE
  dd_materialversionid, -- U_SL_MATERIALVERSIONID
  dd_securitydepartment, -- SECURITYDEPARTMENT
  dd_samplestatus, -- SAMPLESTATUS
  dd_samplecreatedt, -- SAMPLE_CREATEDT
  dim_samplecreatedateid,
  dd_samplecreateby, -- SAMPLE_CREATEBY
  dd_receiveddt, -- SAMPLE_RECEIVEDDT
  dim_receiveddateid,
  dd_batchstatus, -- BATCHSTATUS
  dd_lotnumber, -- U_LOTNUMBER
  dd_sapbatchno, -- SAP_BATCHNO
  dd_batchstagestatus, -- BATCHSTAGESTATUS
  dd_batchstagedesc, -- BATCHSTAGEDESC
  dd_stagename, -- STAGE_NAME
  dd_requestdesc, -- REQUESTDESC
  dd_requeststatus, -- REQUESTSTATUS
  dd_materialname, -- MATERIALNAME
  dd_materialfamily, -- U_MATERIALFAMILY
  dd_paramlistid, -- PARAMLISTID
  dd_paramlistversionid, -- PARAMLISTVERSIONID
  dd_paramlistvariantid, -- PARAMLIST_VARIANTID
  dd_datasetstatus, -- S_DATASETSTATUS
  dd_sdidataid, -- SDIDATAID
  dd_paramlistname, -- PARAMLIST_NAME
  dd_colournameflag,
  ct_avancement,
  dd_verificationcolournameflag,
  dd_headerrowforcalculations,
  dd_routineflag,
  dd_demandsupplydate,
  dim_demandsupplydateid,
  dd_mporvpflag,
  dd_articlebatch,
  ct_colournumberflag,
  dd_avancementpercentage,
  dd_hcdeviationdepartment,
  dd_nonroutinecheckflag,
  dd_aggregatedlevelstatusflag,
  dd_detailedlevelstatusflag,
  dd_cycletimevalue,
  dd_receiveddatepluscycletime,
  dim_receiveddatepluscycletimedateid,
  dd_dateqcflag,
  dd_categorystatusflag,
  ct_linecount,
  dd_dateqccolourflag,
  dd_nonroutinecolourflag,
  dd_trafficlightforparamlistname,
  dd_trafficlightforverificationaggregated,
  dd_trafficlightforverificationdetailed
)
SELECT
  fact_inspectionlotsemoymonitoringid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, -- S_SAMPLEID 
  dd_requestid, -- REQUESTID
  dd_batchstageid, -- BATCHSTAGEID
  dd_batchid, -- BATCHID
  dd_usersampleid, -- U_USERSAMPLEID
  dd_materialid, -- U_SL_MATERIALID
  dd_materialtype, -- U_MATERIALTYPE
  dd_materialversionid, -- U_SL_MATERIALVERSIONID
  dd_securitydepartment, -- SECURITYDEPARTMENT
  dd_samplestatus, -- SAMPLESTATUS
  dd_samplecreatedt, -- SAMPLE_CREATEDT
  dim_samplecreatedateid,
  dd_samplecreateby, -- SAMPLE_CREATEBY
  dd_receiveddt, -- SAMPLE_RECEIVEDDT
  dim_receiveddateid,
  dd_batchstatus, -- BATCHSTATUS
  dd_lotnumber, -- U_LOTNUMBER
  dd_sapbatchno, -- SAP_BATCHNO
  dd_batchstagestatus, -- BATCHSTAGESTATUS
  dd_batchstagedesc, -- BATCHSTAGEDESC
  dd_stagename, -- STAGE_NAME
  dd_requestdesc, -- REQUESTDESC
  dd_requeststatus, -- REQUESTSTATUS
  dd_materialname, -- MATERIALNAME
  dd_materialfamily, -- U_MATERIALFAMILY
  dd_paramlistid, -- PARAMLISTID
  dd_paramlistversionid, -- PARAMLISTVERSIONID
  dd_paramlistvariantid, -- PARAMLIST_VARIANTID
  dd_datasetstatus, -- S_DATASETSTATUS
  dd_sdidataid, -- SDIDATAID
  dd_paramlistname, -- PARAMLIST_NAME
  dd_colournameflag,
  ct_avancement,
  dd_verificationcolournameflag,
  dd_headerrowforcalculations,
  dd_routineflag,
  dd_demandsupplydate,
  dim_demandsupplydateid,
  dd_mporvpflag,
  dd_articlebatch,
  ct_colournumberflag,
  dd_avancementpercentage,
  dd_hcdeviationdepartment,
  dd_nonroutinecheckflag,
  dd_aggregatedlevelstatusflag,
  dd_detailedlevelstatusflag,
  dd_cycletimevalue,
  dd_receiveddatepluscycletime,
  dim_receiveddatepluscycletimedateid,
  dd_dateqcflag,
  dd_categorystatusflag,
  ct_linecount,
  dd_dateqccolourflag,
  dd_nonroutinecolourflag,
  dd_trafficlightforparamlistname,
  dd_trafficlightforverificationaggregated,
  dd_trafficlightforverificationdetailed
FROM tmp_fact_inspectionlotsemoymonitoring;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoymonitoring;

DELETE FROM emd586.fact_inspectionlotsemoymonitoring;

INSERT INTO emd586.fact_inspectionlotsemoymonitoring
SELECT *
FROM fact_inspectionlotsemoymonitoring;
