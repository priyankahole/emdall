/**********************************************************************************/
/* 19 Dec 2017    2.0     CristianT  Multiplying the rows for Vevey plant by bring additional data from AUFM table */
/* 06 Mar 2014            Cornelia   add ct_qtyuoe 	  */
/* 30 Dec 2013            Cornelia   update dim_customer 	  */
/* 24 Oct 2013    1.11    Issam      New fields AFKO.VORUE and AUFK.VAPLZ 	  */
/* 07 Sep 2013    1.5     Lokesh     Exchange rate, currency and amt changes */
/* 28 Aug 2013    1.4     Shanthi    New Field WIP Qty from AUFM		  */
/**********************************************************************************/
Drop table if exists pGlobalCurrency_po_43;

Create table pGlobalCurrency_po_43(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_43(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_43
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

/* 19 Dec 2017 CristianT: Added this delete to prevent multiplied rows to get any values */
DELETE FROM fact_productionorder WHERE dd_dataflag = 'AUFM';

Drop table if exists fact_productionorder_tmp_43;
Drop table if exists max_holder_43;

Create table fact_productionorder_tmp_43 as
SELECT dd_ordernumber,dd_orderitemno
FROM fact_productionorder ;

Create table max_holder_43
as Select ifnull(max(fact_productionorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_productionorder;

DROP TABLE IF EXISTS TMP_001_productionorder;
CREATE TABLE TMP_001_productionorder
AS SELECT p.*,convert(varchar(5),'') AUFK_WAERS from fact_productionorder p where 1=2;


INSERT INTO TMP_001_productionorder(
fact_productionorderid,
Dim_DateidRelease,
Dim_DateidBOMExplosion,
Dim_DateidLastScheduling,
Dim_DateidTechnicalCompletion,
Dim_DateidBasicStart,
Dim_DateidScheduledRelease,
Dim_DateidScheduledFinish,
Dim_DateidScheduledStart,
Dim_DateidActualStart,
Dim_DateidConfirmedOrderFinish,
Dim_DateidActualHeaderFinish,
Dim_DateidActualRelease,
Dim_DateidActualItemFinish,
Dim_DateidPlannedOrderDelivery,
Dim_DateidRoutingTransfer,
Dim_DateidBasicFinish,
Dim_ControllingAreaid,
Dim_ProfitCenterId,
Dim_tasklisttypeid,
Dim_PartidHeader,
Dim_bomstatusid,
Dim_bomusageid,
dim_productionschedulerid,
Dim_ProcurementID,
Dim_SpecialProcurementID,
Dim_UnitOfMeasureid,
Dim_PartidItem,
Dim_AccountCategoryid,
Dim_StockTypeid,
Dim_StorageLocationid,
Dim_Plantid,
dim_specialstockid,
Dim_ConsumptionTypeid,
Dim_SalesOrgid,
Dim_PurchaseOrgid,
Dim_Currencyid,
Dim_Companyid,
Dim_ordertypeid,
dim_productionorderstatusid,
dim_productionordermiscid,
Dim_BuildTypeid,
dd_ordernumber,
dd_orderitemno,
dd_bomexplosionno,
dd_plannedorderno,
dd_SalesOrderNo,
dd_SalesOrderItemNo,
dd_SalesOrderDeliveryScheduleNo,
ct_ConfirmedReworkQty,
ct_ScrapQty,
ct_OrderItemQty,
ct_TotalOrderQty,
ct_GRQty,
ct_GRProcessingTime,
amt_estimatedTotalCost,
amt_ValueGR,
dd_BOMLevel,
dd_sequenceno,
dd_ObjectNumber,
ct_ConfirmedScrapQty,
dd_OrderDescription,
Dim_MrpControllerId,
dim_currencyid_TRA,
dim_currencyid_GBL,
dd_OperationNumber,
dd_WorkCenter,
dim_customerid,
dd_dataflag,
AUFK_WAERS
)
SELECT max_holder_43.maxid + row_number() over(order by '') fact_productionorderid,
       1 as Dim_DateidRelease,
       1 as Dim_DateidBOMExplosion,
       1 as Dim_DateidLastScheduling,
       1 as Dim_DateidTechnicalCompletion,
       1 as Dim_DateidBasicStart,
       1 as Dim_DateidScheduledRelease,
       1 as Dim_DateidScheduledFinish,
       1 as Dim_DateidScheduledStart,
       1 as Dim_DateidActualStart,
       1 as Dim_DateidConfirmedOrderFinish,
       1 as Dim_DateidActualHeaderFinish,
       1 as Dim_DateidActualRelease,
       1 as Dim_DateidActualItemFinish,
       1 as Dim_DateidPlannedOrderDelivery,
       1 as Dim_DateidRoutingTransfer,
       1 as Dim_DateidBasicFinish,
       1 as Dim_ControllingAreaid,
       1 as Dim_ProfitCenterId,
       1 as Dim_tasklisttypeid,
       1 as Dim_PartidHeader,
       1 as Dim_bomstatusid,
       1 as Dim_bomusageid,
       1 as dim_productionschedulerid,
       1 as Dim_ProcurementID,
       1 as Dim_SpecialProcurementID,
       1 as Dim_UnitOfMeasureid,
       1 as Dim_PartidItem,
       1 as Dim_AccountCategoryid,
       1 as Dim_StockTypeid,
       1 as Dim_StorageLocationid,
       1 as Dim_Plantid,
       1 as dim_specialstockid,
       1 as Dim_ConsumptionTypeid,
       1 as Dim_SalesOrgid,
       1 as Dim_PurchaseOrgid,
       1 as Dim_Currencyid,
       1 as Dim_Companyid,
       1 as Dim_productionordertypeid,
       1 as dim_productionorderstatusid,
       1 as dim_productionordermiscid,
       1 as Dim_BuildTypeid,
       ifnull(AFKO_AUFNR,'Not Set') dd_ordernumber,
       AFPO_POSNR dd_orderitemno,
       ifnull(AFPO_SERNR, 'Not Set') dd_bomexplosionno,
       ifnull(AFPO_PLNUM,'Not Set') dd_plannedorderno,
       ifnull(AFPO_KDAUF,'Not Set') dd_SalesOrderNo,
       ifnull(AFPO_KDPOS,0) dd_SalesOrderItemNo,
       ifnull(AFPO_KDEIN,0) dd_SalesOrderDeliveryScheduleNo,
       AFKO_RMNGA ct_ConfirmedReworkQty,
       AFPO_PSAMG ct_ScrapQty,
       AFPO_PSMNG ct_OrderItemQty,
       AFKO_GAMNG ct_TotalOrderQty,
       AFPO_WEMNG ct_GRQty,
       AFPO_WEBAZ ct_GRProcessingTime,
       AUFK_USER4 amt_estimatedTotalCost,
       mhi.AFPO_WEWRT amt_ValueGR,
       AFKO_STUFE dd_BOMLevel,
       ifnull(AFKO_CY_SEQNR,'Not Set') dd_sequenceno,
       ifnull(AUFK_OBJNR,'Not Set') dd_ObjectNumber,
       ifnull(AFKO_IASMG,0.000) ct_ConfirmedScrapQty,
       ifnull(AUFK_KTEXT,'Not Set') dd_OrderDescription,
       1 Dim_MrpControllerId,
       1 Dim_Currencyid_TRA,
       1 dim_Currencyid_GBL,
       ifnull(AFKO_VORUE,'Not Set') dd_OperationNumber,
       ifnull(AUFK_VAPLZ,'Not Set') dd_WorkCenter,
       1 dim_customerid,
       'Not Set' as dd_dataflag,
       mhi.AUFK_WAERS -- used in Dim_Currencyid_TRA
FROM max_holder_43,
     AFKO_AFPO_AUFK mhi,
     pGlobalCurrency_po_43
WHERE NOT EXISTS (SELECT 1
                  FROM fact_productionorder_tmp_43 po
                  WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
                        AND po.dd_orderitemno = mhi.AFPO_POSNR);

UPDATE TMP_001_productionorder t1
SET t1.Dim_Currencyid_TRA = IFNULL(cur.Dim_Currencyid,1)
FROM TMP_001_productionorder t1 
		LEFT JOIN Dim_Currency cur ON		
			cur.CurrencyCode = t1.AUFK_WAERS;

UPDATE TMP_001_productionorder t1
SET t1.Dim_Currencyid_GBL = IFNULL(t2.Dim_Currencyid,1)
FROM TMP_001_productionorder t1 
		CROSS JOIN (SELECT cr.Dim_Currencyid 
		            FROM Dim_Currency cr 
							INNER JOIN pGlobalCurrency_po_43 p
                  on cr.CurrencyCode = p.pGlobalCurrency) t2;

INSERT INTO fact_productionorder(
fact_productionorderid,
Dim_DateidRelease,
Dim_DateidBOMExplosion,
Dim_DateidLastScheduling,
Dim_DateidTechnicalCompletion,
Dim_DateidBasicStart,
Dim_DateidScheduledRelease,
Dim_DateidScheduledFinish,
Dim_DateidScheduledStart,
Dim_DateidActualStart,
Dim_DateidConfirmedOrderFinish,
Dim_DateidActualHeaderFinish,
Dim_DateidActualRelease,
Dim_DateidActualItemFinish,
Dim_DateidPlannedOrderDelivery,
Dim_DateidRoutingTransfer,
Dim_DateidBasicFinish,
Dim_ControllingAreaid,
Dim_ProfitCenterId,
Dim_tasklisttypeid,
Dim_PartidHeader,
Dim_bomstatusid,
Dim_bomusageid,
dim_productionschedulerid,
Dim_ProcurementID,
Dim_SpecialProcurementID,
Dim_UnitOfMeasureid,
Dim_PartidItem,
Dim_AccountCategoryid,
Dim_StockTypeid,
Dim_StorageLocationid,
Dim_Plantid,
dim_specialstockid,
Dim_ConsumptionTypeid,
Dim_SalesOrgid,
Dim_PurchaseOrgid,
Dim_Currencyid,
Dim_Companyid,
Dim_ordertypeid,
dim_productionorderstatusid,
dim_productionordermiscid,
Dim_BuildTypeid,
dd_ordernumber,
dd_orderitemno,
dd_bomexplosionno,
dd_plannedorderno,
dd_SalesOrderNo,
dd_SalesOrderItemNo,
dd_SalesOrderDeliveryScheduleNo,
ct_ConfirmedReworkQty,
ct_ScrapQty,
ct_OrderItemQty,
ct_TotalOrderQty,
ct_GRQty,
ct_GRProcessingTime,
amt_estimatedTotalCost,
amt_ValueGR,
dd_BOMLevel,
dd_sequenceno,
dd_ObjectNumber,
ct_ConfirmedScrapQty,
dd_OrderDescription,
Dim_MrpControllerId,
dim_currencyid_TRA,
dim_currencyid_GBL,
dd_OperationNumber,
dd_WorkCenter,
dim_customerid,
dd_dataflag)
SELECT fact_productionorderid,
       Dim_DateidRelease,
       Dim_DateidBOMExplosion,
       Dim_DateidLastScheduling,
       Dim_DateidTechnicalCompletion,
       Dim_DateidBasicStart,
       Dim_DateidScheduledRelease,
       Dim_DateidScheduledFinish,
       Dim_DateidScheduledStart,
       Dim_DateidActualStart,
       Dim_DateidConfirmedOrderFinish,
       Dim_DateidActualHeaderFinish,
       Dim_DateidActualRelease,
       Dim_DateidActualItemFinish,
       Dim_DateidPlannedOrderDelivery,
       Dim_DateidRoutingTransfer,
       Dim_DateidBasicFinish,
       Dim_ControllingAreaid,
       Dim_ProfitCenterId,
       Dim_tasklisttypeid,
       Dim_PartidHeader,
       Dim_bomstatusid,
       Dim_bomusageid,
       dim_productionschedulerid,
       Dim_ProcurementID,
       Dim_SpecialProcurementID,
       Dim_UnitOfMeasureid,
       Dim_PartidItem,
       Dim_AccountCategoryid,
       Dim_StockTypeid,
       Dim_StorageLocationid,
       Dim_Plantid,
       dim_specialstockid,
       Dim_ConsumptionTypeid,
       Dim_SalesOrgid,
       Dim_PurchaseOrgid,
       Dim_Currencyid,
       Dim_Companyid,
       Dim_ordertypeid,
       dim_productionorderstatusid,
       dim_productionordermiscid,
       Dim_BuildTypeid,
       dd_ordernumber,
       dd_orderitemno,
       dd_bomexplosionno,
       dd_plannedorderno,
       dd_SalesOrderNo,
       dd_SalesOrderItemNo,
       dd_SalesOrderDeliveryScheduleNo,
       ct_ConfirmedReworkQty,
       ct_ScrapQty,
       ct_OrderItemQty,
       ct_TotalOrderQty,
       ct_GRQty,
       ct_GRProcessingTime,
       amt_estimatedTotalCost,
       amt_ValueGR,
       dd_BOMLevel,
       dd_sequenceno,
       dd_ObjectNumber,
       ct_ConfirmedScrapQty,
       dd_OrderDescription,
       Dim_MrpControllerId,
       dim_currencyid_TRA,
       dim_currencyid_GBL,
       dd_OperationNumber,
       dd_WorkCenter,
       dim_customerid,
       dd_dataflag
FROM TMP_001_productionorder;


Drop table if exists fact_productionorder_tmp_43;
Drop table if exists max_holder_43;

DELETE FROM fact_productionorder
WHERE EXISTS
          (SELECT 1
             FROM AFKO_AFPO_AUFK
            WHERE     AFKO_AUFNR = dd_ordernumber
                  AND AFPO_POSNR = dd_orderitemno
                  AND AFPO_XLOEK = 'X');
                  
DELETE FROM fact_productionorder
 WHERE EXISTS
          (SELECT 1
             FROM AFKO_AFPO_AUFK
            WHERE AFKO_AUFNR = dd_ordernumber AND AUFK_LOEKZ = 'X');


UPDATE fact_productionorder po
SET po.Dim_DateidRelease = ifnull(dr.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date dr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dr.DateValue = AUFK_IDAT1 AND mhi.AUFK_BUKRS = dr.CompanyCode
  AND po.Dim_DateidRelease <> ifnull(dr.dim_dateid, 1);

UPDATE fact_productionorder po
SET po.Dim_DateidBOMExplosion = ifnull(dr.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date dr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dr.DateValue = AFKO_AUFLD AND mhi.AUFK_BUKRS = dr.CompanyCode
  AND po.Dim_DateidBOMExplosion <> ifnull(dr.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidLastScheduling = ifnull(ls.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ls
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ls.DateValue = AFKO_TRMDT AND mhi.AUFK_BUKRS = ls.CompanyCode
  AND po.Dim_DateidLastScheduling <> ifnull(ls.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidTechnicalCompletion = ifnull(tc.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date tc
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND tc.DateValue = AUFK_IDAT2 AND mhi.AUFK_BUKRS = tc.CompanyCode
  AND po.Dim_DateidTechnicalCompletion <> ifnull(tc.dim_dateid, 1);

  UPDATE fact_productionorder po
   SET po.Dim_DateidBasicStart= ifnull(bst.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date bst
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bst.DateValue = AFKO_GSTRP AND mhi.AUFK_BUKRS = bst.CompanyCode
  AND po.Dim_DateidBasicStart <> ifnull(bst.dim_dateid, 1);

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledRelease = ifnull(sr.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date sr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sr.DateValue = AFKO_FTRMS AND mhi.AUFK_BUKRS = sr.CompanyCode
  AND po.Dim_DateidScheduledRelease <> ifnull(sr.dim_dateid, 1);

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledFinish = ifnull(sf.dim_dateid, 1)
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date sf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sf.DateValue = AFPO_DGLTS AND mhi.AUFK_BUKRS = sf.CompanyCode
  AND po.Dim_DateidScheduledFinish <> ifnull(sf.dim_dateid, 1);

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledStart = ifnull(ss.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ss
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ss.DateValue = AFKO_GSTRS AND mhi.AUFK_BUKRS = ss.CompanyCode
  AND po.Dim_DateidScheduledStart <> ifnull(ss.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualStart = ifnull(ast.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ast
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ast.DateValue = AFKO_GSTRI AND mhi.AUFK_BUKRS = ast.CompanyCode
  AND po.Dim_DateidActualStart <> ifnull(ast.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidConfirmedOrderFinish = ifnull(cof.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date cof
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND cof.DateValue = AFKO_GETRI AND mhi.AUFK_BUKRS = cof.CompanyCode
  AND po.Dim_DateidConfirmedOrderFinish <> ifnull(cof.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualHeaderFinish = ifnull(ahf.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ahf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ahf.DateValue = AFKO_GLTRI AND mhi.AUFK_BUKRS = ahf.CompanyCode
  AND po.Dim_DateidActualHeaderFinish <> ifnull(ahf.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualRelease = ifnull(ar.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ar
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ar.DateValue = AFKO_FTRMI AND mhi.AUFK_BUKRS = ar.CompanyCode
  AND po.Dim_DateidActualRelease <> ifnull(ar.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualItemFinish = ifnull(aif.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date aif
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND aif.DateValue = AFPO_LTRMI AND mhi.AUFK_BUKRS = aif.CompanyCode
  AND po.Dim_DateidActualItemFinish <> ifnull(aif.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidActualItemFinish = ifnull(aif.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date aif
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND aif.DateValue = AFPO_LTRMI AND mhi.AUFK_BUKRS = aif.CompanyCode
  AND po.Dim_DateidActualItemFinish <> ifnull(aif.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidPlannedOrderDelivery = ifnull(pod.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date pod
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pod.DateValue = AFPO_LTRMP AND mhi.AUFK_BUKRS = pod.CompanyCode
  AND po.Dim_DateidPlannedOrderDelivery <> ifnull(pod.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidRoutingTransfer = ifnull(rt.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date rt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND rt.DateValue = AFKO_PLAUF AND mhi.AUFK_BUKRS = rt.CompanyCode
  AND po.Dim_DateidRoutingTransfer <> ifnull(rt.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_DateidBasicFinish = ifnull(bf.dim_dateid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date bf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bf.DateValue = AFPO_DGLTP AND mhi.AUFK_BUKRS = bf.CompanyCode
  AND po.Dim_DateidBasicFinish <> ifnull(bf.dim_dateid, 1);

UPDATE fact_productionorder po
   SET po.Dim_ControllingAreaid = ifnull(ca.dim_controllingareaid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_ControllingArea ca
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ca.ControllingAreaCode = AUFK_KOKRS
  AND po.Dim_ControllingAreaid <> ifnull(ca.dim_controllingareaid, 1);

Drop table if exists dim_profitcenter_tmp_43;


Create table dim_profitcenter_tmp_43 as
Select  * from dim_profitcenter  ORDER BY ValidTo ASC;



UPDATE  fact_productionorder po
   SET po.Dim_ProfitCenterId = IFNULL(pc.Dim_ProfitCenterid,1)
FROM fact_productionorder po 
		INNER JOIN AFKO_AFPO_AUFK mhi ON po.dd_ordernumber = mhi.AFKO_AUFNR
									 AND po.dd_orderitemno = mhi.AFPO_POSNR
		LEFT JOIN dim_profitcenter_tmp_43 pc ON pc.ProfitCenterCode = AUFK_PRCTR
											AND pc.ControllingArea = AUFK_KOKRS
											AND pc.ValidTo >= AFKO_GSTRP;

drop table if exists dim_profitcenter_tmp_43;


UPDATE fact_productionorder po
   SET po.Dim_tasklisttypeid = ifnull(tlt.Dim_tasklisttypeid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_tasklisttype tlt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND tlt.TaskListTypeCode = AFKO_PLNTY 
  AND po.Dim_tasklisttypeid <> ifnull(tlt.Dim_tasklisttypeid, 1);

UPDATE fact_productionorder po
   SET po.Dim_PartidHeader = ifnull(phdr.Dim_Partid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part phdr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND phdr.PartNumber = AFKO_STLBEZ AND phdr.Plant = mhi.AUFK_WERKS
  AND po.Dim_PartidHeader <> ifnull(phdr.Dim_Partid, 1);

UPDATE fact_productionorder po
   SET po.Dim_bomstatusid = ifnull(bs.Dim_bomstatusid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_bomstatus bs
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bs.BOMStatusCode = AFKO_STLST 
  AND po.Dim_bomstatusid <> ifnull(bs.Dim_bomstatusid, 1);

UPDATE fact_productionorder po
   SET po.Dim_bomusageid = ifnull(bu.Dim_bomusageid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_bomusage bu
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bu.BOMUsageCode = AFKO_STLAN 
  aND po.Dim_bomusageid <> ifnull(bu.Dim_bomusageid, 1);

UPDATE fact_productionorder po
   SET po.dim_productionschedulerid = ifnull(ps.dim_productionschedulerid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_productionscheduler ps
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ps.ProductionScheduler = AFKO_FEVOR
  AND ps.Plant = mhi.AUFK_WERKS
  AND po.dim_productionschedulerid <> ifnull(ps.dim_productionschedulerid, 1);

UPDATE fact_productionorder po
   SET po.Dim_ProcurementID = ifnull(dpr.Dim_ProcurementID, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_procurement dpr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dpr.procurement = AFPO_BESKZ 
  AND po.Dim_ProcurementID <> ifnull(dpr.Dim_ProcurementID, 1);


UPDATE fact_productionorder po
   SET po.Dim_SpecialProcurementID = ifnull(spr.Dim_SpecialProcurementID, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_specialprocurement spr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND spr.specialprocurement = AFPO_PSOBS 
  AND po.Dim_SpecialProcurementID <> ifnull(spr.Dim_SpecialProcurementID,1);

UPDATE fact_productionorder po
   SET po.Dim_UnitOfMeasureid = ifnull(uom.Dim_UnitOfMeasureid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       Dim_UnitOfMeasure uom
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND uom.UOM = afpo_meins 
  AND po.Dim_UnitOfMeasureid <> ifnull(uom.Dim_UnitOfMeasureid,1);

UPDATE fact_productionorder po
   SET po.Dim_PartidItem = ifnull(pitem.Dim_Partid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part pitem
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pitem.PartNumber = mhi.AFPO_MATNR
       AND pitem.Plant = mhi.AFPO_DWERK 
       AND po.Dim_PartidItem <> ifnull(pitem.Dim_Partid,1);

UPDATE fact_productionorder po
   SET po.Dim_AccountCategoryid = ifnull(ac.Dim_AccountCategoryid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_accountcategory ac
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ac.Category = AFPO_KNTTP 
  AND po.Dim_AccountCategoryid <> ifnull(ac.Dim_AccountCategoryid,1);

UPDATE fact_productionorder po
   SET po.Dim_StockTypeid = ifnull(st.Dim_StockTypeid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_stocktype st
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND st.TypeCode = AFPO_INSMK 
  AND po.Dim_StockTypeid <> st.Dim_StockTypeid;

UPDATE fact_productionorder po
   SET po.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi, dim_storagelocation sl
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND sl.LocationCode = AFPO_LGORT
       AND sl.Plant = AFPO_DWERK
       AND po.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid,1);

UPDATE fact_productionorder po
   SET po.Dim_ConsumptionTypeid = ifnull(ct.Dim_ConsumptionTypeid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_consumptiontype ct
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ct.ConsumptionCode = AFPO_KZVBR 
  AND po.Dim_ConsumptionTypeid <> ifnull(ct.Dim_ConsumptionTypeid,1);

UPDATE fact_productionorder po
   SET po.dim_specialstockid = ifnull(spt.dim_specialstockid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_specialstock spt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND spt.specialstockindicator = AFPO_SOBKZ
  AND po.dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

UPDATE fact_productionorder po
   SET po.Dim_Plantid = ifnull(dp.Dim_Plantid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_plant dp
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dp.PlantCode = mhi.afpo_dwerk
  ANd  po.Dim_Plantid <> ifnull(dp.Dim_Plantid,1);

UPDATE fact_productionorder po
   SET po.dim_salesorgid = ifnull(so.dim_salesorgid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_salesorg so,
       dim_plant dp
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dp.PlantCode = mhi.afpo_dwerk
       AND so.SalesOrgCode = dp.SalesOrg
       AND  po.dim_salesorgid <> ifnull(so.dim_salesorgid,1);

UPDATE fact_productionorder po
   SET po.Dim_PurchaseOrgid = ifnull(porg.Dim_PurchaseOrgid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_purchaseorg porg,
       dim_plant dp
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND porg.PurchaseOrgCode = dp.PurchOrg
       AND dp.PlantCode = mhi.AUFK_WERKS
       AND po.Dim_PurchaseOrgid <> ifnull(porg.Dim_PurchaseOrgid, 1);

	   /*
UPDATE fact_productionorder po
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_Currency cur
   SET po.Dim_Currencyid = cur.Dim_Currencyid
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND cur.CurrencyCode = mhi.AUFK_WAERS */
       
UPDATE fact_productionorder po
   SET po.Dim_Companyid = ifnull(dc.Dim_Companyid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_Company dc
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dc.CompanyCode = mhi.AUFK_BUKRS
       AND po.Dim_Companyid <> ifnull(dc.Dim_Companyid, 1);
	   
/* Dim_Currencyid is now the local curr */	   
UPDATE fact_productionorder po
   SET po.Dim_Currencyid = ifnull(cur.Dim_Currencyid, 1)
FROM fact_productionorder po,dim_Company dc,
       dim_Currency cur
 WHERE     po.Dim_Companyid = dc.Dim_Companyid
 AND cur.CurrencyCode = dc.currency;	   


UPDATE fact_productionorder po
SET  ct_ConfirmedScrapQty = ifnull(AFKO_IASMG ,0.000)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND ct_ConfirmedScrapQty <> ifnull(AFKO_IASMG ,0.000);


UPDATE fact_productionorder po
   SET dd_OrderDescription = ifnull(AUFK_KTEXT,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dd_OrderDescription <>  ifnull(AUFK_KTEXT,'Not Set');
       
UPDATE fact_productionorder po
   SET po.dim_MrpControllerid = ifnull(mrp.dim_MrpControllerid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_MrpController mrp, dim_plant pl
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dim_plantid = pl.dim_plantid
       AND mhi.AFKO_DISPO IS  NOT NULL
       AND mrp.MRPController = mhi.AFKO_DISPO AND mrp.plant = pl.plantcode
       AND mrp.RowIsCurrent = 1
	AND po.dim_MrpControllerid <>  ifnull(mrp.dim_MrpControllerid, 1);
       
UPDATE fact_productionorder po
   SET po.dim_MrpControllerid = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND mhi.AFKO_DISPO IS  NULL
	AND  po.dim_MrpControllerid <> 1; 

UPDATE fact_productionorder po
   SET po.Dim_ordertypeid = ifnull(pot.Dim_productionordertypeid, 1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       Dim_productionordertype pot
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND pot.typecode = AFPO_DAUAT AND pot.RowIsCurrent = 1
       AND po.Dim_ordertypeid <> ifnull(pot.Dim_productionordertypeid,1);

UPDATE fact_productionorder po
   SET po.dim_productionordermiscid  = ifnull(misc.dim_productionordermiscid,1)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_productionordermisc misc
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND     CollectiveOrder = ifnull(AFKO_PRODNET, 'Not Set')
                  AND DeliveryComplete = ifnull(AFPO_ELIKZ, 'Not Set')
                  AND GRChangeIndicator = ifnull(AFPO_WEAED, 'Not Set')
                  AND GRIndicator = ifnull(AFPO_WEPOS, 'Not Set')
                  AND GRNonValue = ifnull(AFPO_WEUNB, 'Not Set')
                  AND NoAutoCost = ifnull(AFKO_NAUCOST, 'Not Set')
                  AND NoAutoSchedule = ifnull(AFKO_NAUTERM, 'Not Set')
                  AND NoCapacityRequirement = ifnull(AFKO_KBED, 'Not Set')
                  AND NonMRPMaterial = ifnull(AFPO_NDISR, 'Not Set')
                  AND NonMRPOrderItem = ifnull(AFPO_DNREL, 'Not Set')
                  AND NoPlannedCostCalculation = ifnull(AFKO_NOPCOST, 'Not Set')
                  AND OrderReleased = ifnull(AUFK_PHAS1, 'Not Set')
                  AND ReleasedToMRP = ifnull(AFPO_DFREI, 'Not Set')
                  AND SchedulingBreak = ifnull(AFKO_BREAKS, 'Not Set')
                  AND UnlimitedOverdelivery = ifnull(AFPO_UEBTK, 'Not Set')
                  AND ValueWorkRelevant = ifnull(AFKO_FLG_ARBEI, 'Not Set')
                  AND po.dim_productionordermiscid  <> ifnull(misc.dim_productionordermiscid,1);

UPDATE fact_productionorder po
SET dd_bomexplosionno = ifnull(AFPO_SERNR, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_bomexplosionno <>  ifnull(AFPO_SERNR, 'Not Set');

     


 UPDATE fact_productionorder po
   SET  dd_plannedorderno = ifnull(AFPO_PLNUM,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_plannedorderno <> ifnull(AFPO_PLNUM,'Not Set');



       UPDATE fact_productionorder po
   SET dd_SalesOrderNo = ifnull(AFPO_KDAUF,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_SalesOrderNo <> ifnull(AFPO_KDAUF,'Not Set');



       UPDATE fact_productionorder po
   SET dd_SalesOrderItemNo = ifnull(AFPO_KDPOS, 0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_SalesOrderItemNo <> ifnull(AFPO_KDPOS, 0);



       UPDATE fact_productionorder po
   SET dd_SalesOrderDeliveryScheduleNo = ifnull(AFPO_KDEIN, 0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_SalesOrderDeliveryScheduleNo <> ifnull(AFPO_KDEIN, 0);



       UPDATE fact_productionorder po
   SET ct_ConfirmedReworkQty = ifnull(AFKO_RMNGA,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_ConfirmedReworkQty <> ifnull(AFKO_RMNGA,0);



       UPDATE fact_productionorder po
   SET ct_ScrapQty = ifnull(AFPO_PSAMG,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_ScrapQty  <> ifnull(AFPO_PSAMG,0);



       UPDATE fact_productionorder po
   SET ct_OrderItemQty = ifnull(AFPO_PSMNG,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_OrderItemQty <> ifnull(AFPO_PSMNG,0);



       UPDATE fact_productionorder po
   SET ct_TotalOrderQty = ifnull(AFKO_GAMNG,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_TotalOrderQty  <> ifnull(AFKO_GAMNG,0);



       UPDATE fact_productionorder po
   SET ct_GRQty = ifnull(AFPO_WEMNG,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_GRQty  <> ifnull(AFPO_WEMNG,0);



       UPDATE fact_productionorder po
   SET ct_GRProcessingTime = ifnull(AFPO_WEBAZ,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_GRProcessingTime <> ifnull(AFPO_WEBAZ,0);



       UPDATE fact_productionorder po
   SET amt_estimatedTotalCost = ifnull(AUFK_USER4,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND amt_estimatedTotalCost <> ifnull(AUFK_USER4,0);


       UPDATE fact_productionorder po
   SET amt_ValueGR = ifnull(mhi.AFPO_WEWRT,0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND  amt_ValueGR <> ifnull(mhi.AFPO_WEWRT,0);



       UPDATE fact_productionorder po
   SET Dim_BuildTypeid = (CASE WHEN AFPO_KDAUF IS NOT NULL THEN 1 ELSE 2 END)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND  Dim_BuildTypeid <> (CASE WHEN AFPO_KDAUF IS NOT NULL THEN 1 ELSE 2 END);


       UPDATE fact_productionorder po
   SET dd_BOMLevel = ifnull(AFKO_STUFE, 0)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_BOMLevel <> ifnull(AFKO_STUFE, 0);


       UPDATE fact_productionorder po
   SET dd_sequenceno = ifnull(AFKO_CY_SEQNR,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_sequenceno <> ifnull(AFKO_CY_SEQNR,'Not Set');



UPDATE fact_productionorder po
   SET dd_ObjectNumber = ifnull(AUFK_OBJNR, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_ObjectNumber <> ifnull(AUFK_OBJNR, 'Not Set');


Drop table if exists subqry_po_001;

Create table subqry_po_001 as 
Select distinct dd_ObjectNumber,
CONVERT(varchar(20),'Not Set') Closed,
CONVERT(varchar(20),'Not Set') Delivered,
CONVERT(varchar(20),'Not Set') GoodsMovementPosted,
CONVERT(varchar(20),'Not Set') MaterialCommitted,
CONVERT(varchar(20),'Not Set') PreCosted,
CONVERT(varchar(20),'Not Set') SettlementRuleCreated, 
CONVERT(varchar(20),'Not Set') VariancesCalculated,
CONVERT(varchar(20),'Not Set') TechnicallyCompleted,
CONVERT(varchar(20),'Not Set') Created_col,
CONVERT(varchar(20),'Not Set') Released,
CONVERT(varchar(20),'Not Set') MaterialShortage,
CONVERT(varchar(20),'Not Set') PartPrinted,
CONVERT(varchar(20),'Not Set') PartiallyConfirmed,
CONVERT(varchar(20),'Not Set') InspectionLotAssigned,
CONVERT(varchar(20),'Not Set') MaterialAvailabilityNotChecked,
CONVERT(varchar(20),'Not Set') ResultsAnalysisCarriedOut
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
Where  po.dd_ordernumber = mhi.AFKO_AUFNR AND po.dd_orderitemno = mhi.AFPO_POSNR;


Update subqry_po_001 k
Set k.Closed = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0046' AND j.JEST_INACT is NULL;



Update subqry_po_001 k
Set k.Delivered = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0012' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.GoodsMovementPosted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0321' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialCommitted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0340' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PreCosted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0016' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.SettlementRuleCreated = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0028' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.VariancesCalculated = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0056' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.TechnicallyCompleted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0045' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.Created_col = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0001' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.Released = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0002' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialShortage = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0004' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PartPrinted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0008' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PartiallyConfirmed = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0010' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.InspectionLotAssigned = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0281' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialAvailabilityNotChecked = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0420' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.ResultsAnalysisCarriedOut = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0082' AND j.JEST_INACT is NULL;

UPDATE fact_productionorder po
SET po.dim_productionorderstatusid = ifnull(post.dim_productionorderstatusid, 1)
FROM fact_productionorder po,dim_productionorderstatus post,
     AFKO_AFPO_AUFK mhi,
     subqry_po_001 sq
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND sq.dd_ObjectNumber = po.dd_ObjectNumber
       AND post.Closed = sq.Closed
       AND post.Delivered = sq.Delivered
       AND post.GoodsMovementPosted = sq.GoodsMovementPosted
       AND post.MaterialCommitted =sq.MaterialCommitted
       AND post.PreCosted = sq.PreCosted
       AND post.SettlementRuleCreated =sq.SettlementRuleCreated 
       AND post.VariancesCalculated =sq.VariancesCalculated
       AND post.TechnicallyCompleted = sq.TechnicallyCompleted
       AND post."CREATED" = sq.Created_col
       AND post.Released = sq.Released
       AND post.MaterialShortage = sq.MaterialShortage
       AND post.PartPrinted = sq.PartPrinted
       AND post.PartiallyConfirmed = sq.PartiallyConfirmed
       AND post.InspectionLotAssigned = sq.InspectionLotAssigned
       AND post.MaterialAvailabilityNotChecked = sq.MaterialAvailabilityNotChecked
       AND post.ResultsAnalysisCarriedOut = sq.ResultsAnalysisCarriedOut;


UPDATE fact_productionorder po
SET amt_ExchangeRate = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
     dim_company dc
WHERE       po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS
AND ifnull(amt_ExchangeRate,-1) <> 1; 

UPDATE fact_productionorder po
SET amt_ExchangeRate = ifnull(ex.exchangeRate, 1)
FROM fact_productionorder po,dim_company dc,
     AFKO_AFPO_AUFK mhi,
	 tmp_getExchangeRate1 ex 
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS 
AND  pFromCurrency = AUFK_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = AUFK_AEDAT
and fact_script_name = 'bi_populate_prodorder_fact' 
AND amt_ExchangeRate <> ifnull(ex.exchangeRate, 1);


UPDATE fact_productionorder po
SET amt_ExchangeRate_GBL = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
     dim_company dc
WHERE       po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS
AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;  

UPDATE fact_productionorder po
SET amt_ExchangeRate_GBL = ifnull(ex.exchangeRate, 1)
FROM fact_productionorder po,dim_company dc,
     AFKO_AFPO_AUFK mhi,
	 tmp_getExchangeRate1 ex,
	 pGlobalCurrency_po_43
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS 
AND  pFromCurrency = AUFK_WAERS
and pToCurrency = pGlobalCurrency
and pFromExchangeRate = 0
and pDate =  current_date  /*AUFK_AEDAT - current date to be used for global rate*/
and fact_script_name = 'bi_populate_prodorder_fact'
AND amt_ExchangeRate <> ifnull(ex.exchangeRate, 1);

DROP TABLE IF EXISTS TMP_AUFM_UPDT;
CREATE TABLE TMP_AUFM_UPDT
AS
select AUFM_AUFNR, sum((case a.AUFM_SHKZG when 'S' then -1 else 1 end) * a.AUFM_DMBTR) sum_aufm
from AUFM a group by a.AUFM_AUFNR;

DROP TABLE IF EXISTS TMP_COSS_UPDT;
CREATE TABLE TMP_COSS_UPDT
AS
select COSS_OBJNR,sum(c.COSS_WOG001 + c.COSS_WOG002 + c.COSS_WOG003 + c.COSS_WOG004
                                    + c.COSS_WOG005 + c.COSS_WOG006 + c.COSS_WOG007 + c.COSS_WOG008
                                    + c.COSS_WOG009 + c.COSS_WOG010 + c.COSS_WOG011 + c.COSS_WOG012
                                    + c.COSS_WOG013 + c.COSS_WOG014 + c.COSS_WOG015 + c.COSS_WOG016) sum_cos
from coss c
where c.COSS_WRTTP = '04'
group by COSS_OBJNR;

UPDATE fact_productionorder po
SET po.amt_WIPBalance = ifnull(a.sum_aufm,0)
FROM fact_productionorder po
	LEFT JOIN TMP_AUFM_UPDT a ON po.dd_ordernumber = a.AUFM_AUFNR 
WHERE ifnull(amt_WIPBalance,-1) <> ifnull(a.sum_aufm,0);

UPDATE fact_productionorder po
SET po.amt_WIPBalance = ifnull(po.amt_WIPBalance,0) + ifnull(c.sum_cos,0)
FROM fact_productionorder po
	LEFT JOIN TMP_COSS_UPDT c ON 'OR' || po.dd_ordernumber = c.COSS_OBJNR
WHERE ifnull(amt_WIPBalance,-1) <> ifnull(po.amt_WIPBalance,0) + ifnull(c.sum_cos,0);

DROP TABLE IF EXISTS TMP_AUFM_UPDT;
DROP TABLE IF EXISTS TMP_COSS_UPDT;
						   
/* LK: 15 Sep 2013: aufm_dmbtr and coss amts are in local currency. So convert amts to transaction currency */
						   
UPDATE fact_productionorder po
SET amt_WIPBalance = ( amt_WIPBalance / amt_ExchangeRate )
WHERE amt_ExchangeRate <> 1;						   

DROP TABLE IF EXISTS FPO_TMP;
CREATE TABLE FPO_TMP
AS SELECT a.AUFM_AUFNR,sum(a.AUFM_MENGE) sm_aufmenge
from AUFM a
GROUP BY a.AUFM_AUFNR;

UPDATE fact_productionorder po
SET ct_WIPQty = ifnull(sm_aufmenge,0)
from fact_productionorder po, FPO_TMP a 
where a.AUFM_AUFNR = po.dd_ordernumber;
						 
UPDATE fact_productionorder po
SET dd_OperationNumber = ifnull(AFKO_VORUE, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_OperationNumber <>  ifnull(AFKO_VORUE, 'Not Set');

UPDATE fact_productionorder po
SET dd_WorkCenter = ifnull(AUFK_VAPLZ, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_WorkCenter <>  ifnull(AUFK_VAPLZ, 'Not Set');

DROP TABLE IF EXISTS tmp_salesorder;

CREATE TABLE tmp_salesorder AS
SELECT distinct dd_salesdocno,dd_Salesitemno,dim_customerid from fact_salesorder;

UPDATE fact_productionorder po
 SET po.Dim_customerid = ifnull(fso.Dim_customerid, 1)
FROM  fact_productionorder po,tmp_salesorder fso
WHERE     po.dd_salesorderno = fso.dd_salesdocno
     AND po.dd_salesorderitemno = fso.dd_salesitemno
     AND ifnull(po.Dim_customerid, -1) <> ifnull(fso.Dim_customerid, 1);
     
DROP TABLE IF EXISTS tmp_salesorder;
     
UPDATE fact_productionorder po
SET dd_OperationNumber = 'Not Set' WHERE dd_OperationNumber IS NULL;

UPDATE fact_productionorder po
SET dd_WorkCenter = 'Not Set' WHERE dd_WorkCenter IS NULL;

DROP TABLE IF EXISTS FPO_TMP2;
CREATE TABLE FPO_TMP2
AS SELECT r.RESB_AUFNR,sum(r.RESB_ERFMG) sm_resberfmg
from RESB r
GROUP BY r.RESB_AUFNR;

UPDATE fact_productionorder po
SET ct_qtyuoe = ifnull(sm_resberfmg,0)
FROM fact_productionorder po,FPO_TMP2 r
WHERE r.RESB_AUFNR = po.dd_ordernumber;

/* MDG Part */

DROP TABLE IF EXISTS tmp_dim_mdg_partid_prodo;
CREATE TABLE tmp_dim_mdg_partid_prodo as
SELECT DISTINCT pr.dim_partid, md.dim_mdg_partid, md.partnumber
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18);

UPDATE fact_productionorder f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM tmp_dim_mdg_partid_prodo tmp, fact_productionorder f
WHERE f.dim_partidheader = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

DROP TABLE IF EXISTS tmp_dim_mdg_partid_prodo;

/* 13 Dec 2017 CristianT Start: Added logic for Batch based on Vevey requierments */
UPDATE fact_productionorder po
SET dd_batch = ifnull(a.AFPO_CHARG,'Not Set')
FROM fact_productionorder po,
     AFKO_AFPO_AUFK a
WHERE a.AFKO_AUFNR = po.dd_ordernumber
      AND a.AFPO_POSNR = po.dd_orderitemno
      AND ifnull(dd_batch, 'X') <> ifnull(a.AFPO_CHARG,'Not Set');


/* 13 Dec 2017 CristianT End */

UPDATE fact_productionorder f
SET f.amt_ExchangeRate_GBL = 1
WHERE ifnull(f.amt_ExchangeRate_GBL,-1) <> 1;


UPDATE fact_productionorder f
SET f.amt_ExchangeRate_GBL = ifnull(r.exrate,1)
FROM fact_productionorder f, dim_currency dc, csv_oprate r
WHERE f.dim_currencyid = dc.dim_currencyid
	AND dc.CurrencyCode = r.fromc
	AND r.toc = (SELECT ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD'))
	AND ifnull(f.amt_ExchangeRate_GBL,-1) <> ifnull(r.exrate,1);


/* Update BW Hierarchy */
update fact_productionorder f
  set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_productionorder f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partidheader = dp.dim_partid
  and dp.producthierarchy = bw.lowerhierarchycode
  and dp.productgroupsbu = bw.upperhierarchycode
  and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
  and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);

/* BW Country Hierarchy - APP-7497 Oana 27Sept2017  */
update fact_productionorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_productionorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-32')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY04' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_productionorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_productionorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-31','DIV-35','DIV-34')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY05' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_productionorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_productionorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY07' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_productionorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_productionorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-87')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY08' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

/* APP-8176 - Oana 06Dec2017 */
drop table if exists tmp_po_buss_day;
create table tmp_po_buss_day
as
select companycode,BUSINESSDAYSSEQNO,max(datevalue) datevalue,cast(1 as bigint) as dim_dateid
from dim_date
group by companycode,BUSINESSDAYSSEQNO;

update tmp_po_buss_day t
	set t.dim_dateid = dd.dim_dateid
from tmp_po_buss_day t
	inner join dim_date dd on t.companycode = dd.companycode and t.datevalue = dd.datevalue;

update fact_productionorder f
set dim_DateidofAvailability = ifnull(dd2.dim_dateid,1)
from fact_productionorder f
    inner join dim_date  dd on f.dim_dateidbasicfinish= dd.dim_dateid
    inner join dim_part dim on f.dim_partiditem = dim.dim_partid
    inner join tmp_po_buss_day dd2 on dd2.companycode = dd.companycode and dd.BUSINESSDAYSSEQNO + GRProcessingTime = dd2.BUSINESSDAYSSEQNO;
/* END APP-8176 - Oana 06Dec2017 */

drop table if exists tmp_po_buss_day;

/* 19 Dec 2017 CristianT Start: Multiplying the rows for Vevey plant by bringing additional data from AUFM table */
/* This logic was requested by the users from Vevey Manufacturing Plant. They want to see specific details from AUFM table even if this its breaking the grain by multpliying the rows.
In order to make this work i created a flag column -> dd_dataflag. This column will hold the value 'Not Set' for all the original rows and 'AUFM' for the mulplied rows for Vevey.
All measure columns that are populated on the upper part of the script will be set to 0 in the multiplied rows. This way we ensure that the existing measures will not break.
This insert must be last part of the code from the script.
*/
/* Step 1 - Identify all production orders where Manufacturing Site = 'Vevey (SWITZ)' */
DROP TABLE IF EXISTS tmp_productionorder_noaufmdata;
CREATE TABLE tmp_productionorder_noaufmdata
AS
SELECT ifnull(dd_ordernumber, 'Not Set') as dd_ordernumber,
       ifnull(po.dim_controllingareaid, 1) as dim_controllingareaid,
       ifnull(po.dim_productionorderstatusid, 1) as dim_productionorderstatusid,
       ifnull(po.dim_dateidrelease, 1) as dim_dateidrelease,
       ifnull(po.dim_dateidtechnicalcompletion, 1) as dim_dateidtechnicalcompletion,
       0 as amt_estimatedtotalcost,
       ifnull(po.dim_dateidbasicstart, 1) as dim_dateidbasicstart,
       ifnull(po.dim_dateidscheduledrelease, 1) as dim_dateidscheduledrelease,
       ifnull(po.dim_dateidscheduledfinish, 1) as dim_dateidscheduledfinish,
       ifnull(po.dim_dateidscheduledstart, 1) as dim_dateidscheduledstart,
       ifnull(po.dim_dateidactualstart, 1) as dim_dateidactualstart,
       ifnull(po.dim_dateidconfirmedorderfinish, 1) as dim_dateidconfirmedorderfinish,
       ifnull(po.dim_dateidactualheaderfinish, 1) as dim_dateidactualheaderfinish,
       ifnull(po.dim_dateidactualrelease, 1) as dim_dateidactualrelease,
       0 as ct_totalorderqty,
       ifnull(po.dim_tasklisttypeid, 1) as dim_tasklisttypeid,
       ifnull(po.dim_dateidroutingtransfer, 1) as dim_dateidroutingtransfer,
       ifnull(po.dim_partidheader, 1) as dim_partidheader,
       ifnull(po.dim_bomstatusid, 1) as dim_bomstatusid,
       ifnull(po.dim_bomusageid, 1) as dim_bomusageid,
       ifnull(po.dim_dateidbomexplosion, 1) as dim_dateidbomexplosion,
       ifnull(po.dim_productionschedulerid, 1) as dim_productionschedulerid,
       ifnull(po.dim_dateidlastscheduling, 1) as dim_dateidlastscheduling,
       0 as ct_confirmedreworkqty,
       ifnull(po.dd_orderitemno, 0) as dd_orderitemno, 
       ifnull(po.dim_specialprocurementid, 1) as dim_specialprocurementid,
       ifnull(po.dd_plannedorderno, 'Not Set') as dd_plannedorderno,
       ifnull(po.dd_salesorderno, 'Not Set') as dd_salesorderno,
       ifnull(po.dd_salesorderitemno, 0) as dd_salesorderitemno,
       ifnull(po.dd_salesorderdeliveryscheduleno, 0) as dd_salesorderdeliveryscheduleno,
       ifnull(po.dim_procurementid, 1) as dim_procurementid,
       0 as ct_scrapqty,
       0 as ct_orderitemqty,
       0 as ct_grqty,
       ifnull(po.dim_unitofmeasureid, 1) as dim_unitofmeasureid,
       ifnull(po.dim_partiditem, 1) as dim_partiditem,
       ifnull(po.dim_accountcategoryid, 1) as dim_accountcategoryid,
       ifnull(po.dim_dateidactualitemfinish, 1) as dim_dateidactualitemfinish,
       ifnull(po.dim_dateidplannedorderdelivery, 1) as dim_dateidplannedorderdelivery,
       ifnull(po.dim_stocktypeid, 1) as dim_stocktypeid,
       ifnull(po.dim_storagelocationid, 1) as dim_storagelocationid,
       0 as ct_grprocessingtime,
       ifnull(po.dd_bomexplosionno, 'Not Set') as dd_bomexplosionno,
       ifnull(po.dim_plantid, 1) as dim_plantid,
       ifnull(po.dim_ordertypeid, 1) as dim_ordertypeid,
       ifnull(po.dim_dateidbasicfinish, 1) as dim_dateidbasicfinish,
       ifnull(po.dim_specialstockid, 1) as dim_specialstockid,
       ifnull(po.dim_buildtypeid, 1) as dim_buildtypeid,
       ifnull(po.dim_consumptiontypeid, 1) as dim_consumptiontypeid,
       0 as amt_valuegr,
       ifnull(po.dim_salesorgid, 1) as dim_salesorgid,
       ifnull(po.dim_purchaseorgid, 1) as dim_purchaseorgid,
       ifnull(po.dim_currencyid, 1) as dim_currencyid,
       ifnull(po.dim_companyid, 1) as dim_companyid,
       ifnull(po.dim_productionordermiscid, 1) as dim_productionordermiscid,
       ifnull(po.dim_customergroup1id, 1) as dim_customergroup1id,
       ifnull(po.dim_customerid, 1) as dim_customerid,
       ifnull(po.dim_documentcategoryid, 1) as dim_documentcategoryid,
       ifnull(po.dim_salesdocumenttypeid, 1) as dim_salesdocumenttypeid,
       1 as amt_exchangerate_gbl,
       1 as amt_exchangerate,
       ifnull(po.dim_profitcenterid, 1) as dim_profitcenterid,
       ifnull(po.dd_bomlevel, 0) as dd_bomlevel,
       ifnull(po.dd_sequenceno, 'Not Set') as dd_sequenceno,
       ifnull(po.dd_objectnumber, 'Not Set') as dd_objectnumber,
       0 as amt_wipbalance,
       0 as ct_confirmedscrapqty,
       ifnull(po.dd_orderdescription, 'Not Set') as dd_orderdescription,
       ifnull(po.dim_mrpcontrollerid, 1) as dim_mrpcontrollerid,
       0 as ct_wipqty,
       ifnull(po.dim_currencyid_tra, 1) as dim_currencyid_tra,
       ifnull(po.dim_currencyid_gbl, 1) as dim_currencyid_gbl,
       ifnull(po.dd_operationnumber, 'Not Set') as dd_operationnumber,
       ifnull(po.dd_workcenter, 'Not Set') as dd_workcenter,
       0 as ct_qtyuoe,
       ifnull(po.dd_routingoperationno, 'Not Set') as dd_routingoperationno,
       ifnull(po.dd_startdatetime, 'Not Set') as dd_startdatetime,
       ifnull(po.dd_finishdatetime, 'Not Set') as dd_finishdatetime,
       ifnull(po.dd_minlotsize, 'Not Set') as dd_minlotsize,
       ifnull(po.dd_batch, 'Not Set') as dd_batch,
       ifnull(po.dim_partheaderproducthierarchyid, 1) as dim_partheaderproducthierarchyid,
       ifnull(po.dim_partitemproducthierarchyid, 1) as dim_partitemproducthierarchyid,
       ifnull(po.dim_dateidactualheaderfinishdate_merck, 1) as dim_dateidactualheaderfinishdate_merck,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(po.dim_projectsourceid, 1) as dim_projectsourceid,
       ifnull(po.dd_cancelledorder, 'Not Set') as dd_cancelledorder,
       0 as ct_underdeliverytol_merck,
       0 as ct_overdeliverytol_merck,
       ifnull(po.dd_unimitedoverdelivery_merck, 'Not Set') as dd_unimitedoverdelivery_merck,
       ifnull(po.dim_dateidscheduledfinishheader, 1) as dim_dateidscheduledfinishheader,
       ifnull(po.dim_mdg_partid, 1) as dim_mdg_partid,
       0 as ct_sched_processing_time,
       0 as ct_underdeliverytol,
       0 as ct_overdeliverytol,
       ifnull(po.dd_unimitedoverdelivery, 'Not Set') as dd_unimitedoverdelivery,
       ifnull(po.dim_dateidactualheaderfinishdate, 1) as dim_dateidactualheaderfinishdate,
       0 as amt_stdunitprice,
       ifnull(po.dim_bwproducthierarchyid, 1) as dim_bwproducthierarchyid,
       ifnull(po.dim_bwhierarchycountryid, 1) as dim_bwhierarchycountryid,
       ifnull(po.dim_dateidofavailability, 1) as dim_dateidofavailability,
       ifnull(po.dim_hc_demandfulfillmentriskid, 1) as dim_hc_demandfulfillmentriskid,
       ifnull(po.dd_updatedate, '0001-01-01 00:00:00.000000') as dd_updatedate
FROM fact_productionorder po,
     dim_plant pl
WHERE po.dim_plantid = pl.dim_plantid
      AND pl.MANUFACTURINGSITE = 'Vevey (SWITZ)';

/* Step 2 - Prepare the AUFM data */
DROP TABLE IF EXISTS tmp_vevey_aufmdata;
CREATE TABLE tmp_vevey_aufmdata
AS
SELECT ifnull(MBLNR, 'Not Set') as dd_aufm_matdocno,
       ifnull(MJAHR, 'Not Set') as dd_aufm_matdocyr,
	   ifnull(ZEILE, 'Not Set') as dd_aufm_matdocitem,
	   ifnull(BWART, 'Not Set') as dd_aufm_mvt,
	   MATNR,
	   CHARG,
	   ifnull(SOBKZ, 'Not Set') as dd_aufm_specialstock,
	   ifnull(MEINS, 'Not Set') as dd_aufm_bun,
	   ifnull(AUFNR, 'Not Set') as dd_aufm_order,
	   ifnull(RSNUM, 'Not Set') as dd_aufm_reservno,
	   ifnull(RSPOS, 'Not Set') as dd_aufm_item,
	   ifnull(KZEAR, 'Not Set') as dd_aufm_fls,
	   ifnull(KZBEW, 'Not Set') as dd_aufm_mvtkzbew,
	   ifnull(SAKTO, 'Not Set') as dd_aufm_glacct,
	   ifnull(KOKRS, 'Not Set') as dd_aufm_coar,
	   ifnull(BLDAT, '0001-01-01') as BLDAT,
       CONVERT(BIGINT, 1) as dim_dateid_aufmdocdate,
	   ifnull(BUDAT, '0001-01-01') as BUDAT,
       CONVERT(BIGINT, 1) as dim_dateid_aufmpostedon,
	   ifnull(DMBTR, 0) as amt_aufm_loccurrency,
	   ifnull(MENGE, 0) as ct_aufm_menge,
	   ifnull(ERFMG, 0) as ct_aufm_erfmg
FROM PROD_ORDER_AUFM afm;

UPDATE tmp_vevey_aufmdata tmp
SET dim_dateid_aufmdocdate = ifnull(dt.dim_dateid, 1)
FROM tmp_vevey_aufmdata tmp,
     dim_date dt
WHERE tmp.BLDAT = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND dim_dateid_aufmdocdate <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_vevey_aufmdata tmp
SET dim_dateid_aufmpostedon = ifnull(dt.dim_dateid, 1)
FROM tmp_vevey_aufmdata tmp,
     dim_date dt
WHERE tmp.BUDAT = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND dim_dateid_aufmpostedon <> ifnull(dt.dim_dateid, 1);

/* Step 3 - Blend step 1 and 2 data into subject area */
DROP TABLE IF EXISTS number_fountain_fact_productionorder;
CREATE TABLE number_fountain_fact_productionorder LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_productionorder
SELECT 'fact_productionorder', ifnull(max(fact_productionorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_productionorder;

INSERT INTO fact_productionorder(
fact_productionorderid,
dd_ordernumber,
dim_controllingareaid,
dim_productionorderstatusid,
dim_dateidrelease,
dim_dateidtechnicalcompletion,
amt_estimatedtotalcost,
dim_dateidbasicstart,
dim_dateidscheduledrelease,
dim_dateidscheduledfinish,
dim_dateidscheduledstart,
dim_dateidactualstart,
dim_dateidconfirmedorderfinish,
dim_dateidactualheaderfinish,
dim_dateidactualrelease,
ct_totalorderqty,
dim_tasklisttypeid,
dim_dateidroutingtransfer,
dim_partidheader,
dim_bomstatusid,
dim_bomusageid,
dim_dateidbomexplosion,
dim_productionschedulerid,
dim_dateidlastscheduling,
ct_confirmedreworkqty,
dd_orderitemno,
dim_specialprocurementid,
dd_plannedorderno,
dd_salesorderno,
dd_salesorderitemno,
dd_salesorderdeliveryscheduleno,
dim_procurementid,
ct_scrapqty,
ct_orderitemqty,
ct_grqty,
dim_unitofmeasureid,
dim_partiditem,
dim_accountcategoryid,
dim_dateidactualitemfinish,
dim_dateidplannedorderdelivery,
dim_stocktypeid,
dim_storagelocationid,
ct_grprocessingtime,
dd_bomexplosionno,
dim_plantid,
dim_ordertypeid,
dim_dateidbasicfinish,
dim_specialstockid,
dim_buildtypeid,
dim_consumptiontypeid,
amt_valuegr,
dim_salesorgid,
dim_purchaseorgid,
dim_currencyid,
dim_companyid,
dim_productionordermiscid,
dim_customergroup1id,
dim_customerid,
dim_documentcategoryid,
dim_salesdocumenttypeid,
amt_exchangerate_gbl,
amt_exchangerate,
dim_profitcenterid,
dd_bomlevel,
dd_sequenceno,
dd_objectnumber,
amt_wipbalance,
ct_confirmedscrapqty,
dd_orderdescription,
dim_mrpcontrollerid,
ct_wipqty,
dim_currencyid_tra,
dim_currencyid_gbl,
dd_operationnumber,
dd_workcenter,
ct_qtyuoe,
dd_routingoperationno,
dd_startdatetime,
dd_finishdatetime,
dd_minlotsize,
dd_batch,
dim_partheaderproducthierarchyid,
dim_partitemproducthierarchyid,
dim_dateidactualheaderfinishdate_merck,
dw_insert_date,
dw_update_date,
dim_projectsourceid,
dd_cancelledorder,
ct_underdeliverytol_merck,
ct_overdeliverytol_merck,
dd_unimitedoverdelivery_merck,
dim_dateidscheduledfinishheader,
dim_mdg_partid,
ct_sched_processing_time,
ct_underdeliverytol,
ct_overdeliverytol,
dd_unimitedoverdelivery,
dim_dateidactualheaderfinishdate,
amt_stdunitprice,
dim_bwproducthierarchyid,
dim_bwhierarchycountryid,
dim_dateidofavailability,
dim_hc_demandfulfillmentriskid,
dd_updatedate,
dd_dataflag,
dd_aufm_matdocno,
dd_aufm_matdocyr,
dd_aufm_matdocitem,
dd_aufm_mvt,
dd_aufm_specialstock,
dd_aufm_bun,
dd_aufm_order,
dd_aufm_reservno,
dd_aufm_item,
dd_aufm_fls,
dd_aufm_mvtkzbew,
dd_aufm_glacct,
dd_aufm_coar,
dim_dateid_aufmdocdate,
dim_dateid_aufmpostedon,
amt_aufm_loccurrency,
ct_aufm_menge,
ct_aufm_erfmg
)
SELECT (SELECT max_id from number_fountain_fact_productionorder WHERE table_name = 'fact_productionorder') + ROW_NUMBER() over(order by '') AS fact_productionorderid,
       po.dd_ordernumber,
       po.dim_controllingareaid,
       po.dim_productionorderstatusid,
       po.dim_dateidrelease,
       po.dim_dateidtechnicalcompletion,
       po.amt_estimatedtotalcost,
       po.dim_dateidbasicstart,
       po.dim_dateidscheduledrelease,
       po.dim_dateidscheduledfinish,
       po.dim_dateidscheduledstart,
       po.dim_dateidactualstart,
       po.dim_dateidconfirmedorderfinish,
       po.dim_dateidactualheaderfinish,
       po.dim_dateidactualrelease,
       po.ct_totalorderqty,
       po.dim_tasklisttypeid,
       po.dim_dateidroutingtransfer,
       po.dim_partidheader,
       po.dim_bomstatusid,
       po.dim_bomusageid,
       po.dim_dateidbomexplosion,
       po.dim_productionschedulerid,
       po.dim_dateidlastscheduling,
       po.ct_confirmedreworkqty,
       po.dd_orderitemno,
       po.dim_specialprocurementid,
       po.dd_plannedorderno,
       po.dd_salesorderno,
       po.dd_salesorderitemno,
       po.dd_salesorderdeliveryscheduleno,
       po.dim_procurementid,
       po.ct_scrapqty,
       po.ct_orderitemqty,
       po.ct_grqty,
       po.dim_unitofmeasureid,
       po.dim_partiditem,
       po.dim_accountcategoryid,
       po.dim_dateidactualitemfinish,
       po.dim_dateidplannedorderdelivery,
       po.dim_stocktypeid,
       po.dim_storagelocationid,
       po.ct_grprocessingtime,
       po.dd_bomexplosionno,
       po.dim_plantid,
       po.dim_ordertypeid,
       po.dim_dateidbasicfinish,
       po.dim_specialstockid,
       po.dim_buildtypeid,
       po.dim_consumptiontypeid,
       po.amt_valuegr,
       po.dim_salesorgid,
       po.dim_purchaseorgid,
       po.dim_currencyid,
       po.dim_companyid,
       po.dim_productionordermiscid,
       po.dim_customergroup1id,
       po.dim_customerid,
       po.dim_documentcategoryid,
       po.dim_salesdocumenttypeid,
       po.amt_exchangerate_gbl,
       po.amt_exchangerate,
       po.dim_profitcenterid,
       po.dd_bomlevel,
       po.dd_sequenceno,
       po.dd_objectnumber,
       po.amt_wipbalance,
       po.ct_confirmedscrapqty,
       po.dd_orderdescription,
       po.dim_mrpcontrollerid,
       po.ct_wipqty,
       po.dim_currencyid_tra,
       po.dim_currencyid_gbl,
       po.dd_operationnumber,
       po.dd_workcenter,
       po.ct_qtyuoe,
       po.dd_routingoperationno,
       po.dd_startdatetime,
       po.dd_finishdatetime,
       po.dd_minlotsize,
       po.dd_batch,
       po.dim_partheaderproducthierarchyid,
       po.dim_partitemproducthierarchyid,
       po.dim_dateidactualheaderfinishdate_merck,
       po.dw_insert_date,
       po.dw_update_date,
       po.dim_projectsourceid,
       po.dd_cancelledorder,
       po.ct_underdeliverytol_merck,
       po.ct_overdeliverytol_merck,
       po.dd_unimitedoverdelivery_merck,
       po.dim_dateidscheduledfinishheader,
       po.dim_mdg_partid,
       po.ct_sched_processing_time,
       po.ct_underdeliverytol,
       po.ct_overdeliverytol,
       po.dd_unimitedoverdelivery,
       po.dim_dateidactualheaderfinishdate,
       po.amt_stdunitprice,
       po.dim_bwproducthierarchyid,
       po.dim_bwhierarchycountryid,
       po.dim_dateidofavailability,
       po.dim_hc_demandfulfillmentriskid,
       po.dd_updatedate,
       'AUFM' as dd_dataflag,
       afm.dd_aufm_matdocno,
       afm.dd_aufm_matdocyr,
       afm.dd_aufm_matdocitem,
       afm.dd_aufm_mvt,
       afm.dd_aufm_specialstock,
       afm.dd_aufm_bun,
       afm.dd_aufm_order,
       afm.dd_aufm_reservno,
       afm.dd_aufm_item,
       afm.dd_aufm_fls,
       afm.dd_aufm_mvtkzbew,
       afm.dd_aufm_glacct,
       afm.dd_aufm_coar,
       afm.dim_dateid_aufmdocdate,
       afm.dim_dateid_aufmpostedon,
       afm.amt_aufm_loccurrency,
       afm.ct_aufm_menge,
       afm.ct_aufm_erfmg
FROM tmp_productionorder_noaufmdata po,
     dim_mdg_part mdg,
     tmp_vevey_aufmdata afm
WHERE po.dim_mdg_partid = mdg.dim_mdg_partid
      AND mdg.MDGPartNumber_NoLeadZero = afm.MATNR
      AND po.dd_batch = afm.CHARG;

DROP TABLE IF EXISTS tmp_productionorder_noaufmdata;
DROP TABLE IF EXISTS tmp_vevey_aufmdata;
DROP TABLE IF EXISTS number_fountain_fact_productionorder;
/* 19 Dec 2017 CristianT End */

/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate;
create table tmp_updatedate as
select max(dw_update_date) dd_updatedate from  fact_productionorder;


update fact_productionorder f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_productionorder f, tmp_updatedate t
where f.dd_updatedate <> t.dd_updatedate;
