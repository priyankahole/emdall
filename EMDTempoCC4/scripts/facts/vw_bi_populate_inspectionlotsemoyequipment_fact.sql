

DROP TABLE IF EXISTS number_fountain_fact_inspectionlotsemoyequipment;
CREATE TABLE number_fountain_fact_inspectionlotsemoyequipment 
LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoyequipment;
CREATE TABLE tmp_fact_inspectionlotsemoyequipment 
LIKE fact_inspectionlotsemoyequipment INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_inspectionlotsemoyequipment 
WHERE table_name = 'fact_inspectionlotsemoyequipment';	

INSERT INTO number_fountain_fact_inspectionlotsemoyequipment
SELECT 'fact_inspectionlotsemoyequipment', IFNULL(max(f.fact_inspectionlotsemoyequipmentid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM tmp_fact_inspectionlotsemoyequipment AS f;

INSERT INTO tmp_fact_inspectionlotsemoyequipment(
  fact_inspectionlotsemoyequipmentid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, -- s_sampleid
  dd_createddate, -- createdt
  dim_createddateid,
  dd_batchid, -- batchid
  dd_materialid, -- u_sl_materialid
  dd_sapbatchid,  -- u_sapbatchid
  dd_batchstatus, -- batchstatus
  dd_lotnumber, -- u_lotnumber
  dd_requestdesc, -- requestdesc
  dd_requeststatus, -- requeststatus
  dd_materialname, -- materialname
  dd_materialfamily, -- u_materialfamily
  dd_paramlistid, -- paramlistid
  dd_paramlistversionid, -- paramlistversionid
  dd_variantid, -- variantid
  dd_operationname, -- u_operationname
  dd_paramid, -- paramid
  dd_transformtext, -- transformtext
  dd_transformdate, -- transformdt
  dim_tranformdateid,
  dd_instrumentmodelid, -- instrumentmodelid
  dd_routineflag,
  ct_counttest,
  ct_countatsapbatchlevel
)
SELECT
  (SELECT max_id 
    FROM number_fountain_fact_inspectionlotsemoyequipment
    WHERE table_name = 'fact_inspectionlotsemoyequipment') 
      + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoyequipmentid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  CURRENT_TIMESTAMP AS dw_insert_date, 
  CURRENT_TIMESTAMP AS dw_update_date,
  IFNULL(s_sampleid,'Not Set') AS dd_sampleid, -- s_sampleid
  IFNULL(createdt,'0001-01-01') AS dd_createddate, -- createdt
  CAST(1 AS BIGINT) AS dim_createddateid,
  IFNULL(batchid,'Not Set') AS dd_batchid, -- batchid
  IFNULL(u_sl_materialid,'Not Set') AS dd_materialid, -- u_sl_materialid
  IFNULL(u_sapbatchid,'Not Set') AS dd_sapbatchid,  -- u_sapbatchid
  IFNULL(batchstatus,'Not Set') AS dd_batchstatus, -- batchstatus
  IFNULL(u_lotnumber,'Not Set') AS dd_lotnumber, -- u_lotnumber
  IFNULL(requestdesc,'Not Set') AS dd_requestdesc, -- requestdesc
  IFNULL(requeststatus,'Not Set') AS dd_requeststatus, -- requeststatus
  IFNULL(materialname,'Not Set') AS dd_materialname, -- materialname
  IFNULL(u_materialfamily,'Not Set') AS dd_materialfamily, -- u_materialfamily
  IFNULL(paramlistid,'Not Set') AS dd_paramlistid, -- paramlistid
  IFNULL(paramlistversionid,'Not Set') AS dd_paramlistversionid, -- paramlistversionid
  IFNULL(variantid,'Not Set') AS dd_variantid, -- variantid
  IFNULL(u_operationname,'Not Set') AS dd_operationname, -- u_operationname
  IFNULL(paramid,'Not Set') AS dd_paramid, -- paramid
  IFNULL(transformtext,'Not Set') AS dd_transformtext, -- transformtext
  IFNULL(transformdt,'0001-01-01') AS dd_transformdate, -- transformdt
  CAST(1 AS BIGINT) AS dim_tranformdateid,
  IFNULL(instrumentmodelid,'Not Set') AS dd_instrumentmodelid, -- instrumentmodelid
  CASE
    WHEN requestdesc IS NOT NULL THEN 'Non Routine'
    ELSE 'Routine'
  END AS dd_routineflag,
  1 AS ct_counttest,
  0 AS ct_countatsapbatchlevel
FROM
  v_mcockpit_suivitauxequipement
;

/* BEGIN - update dim dates from fact */
UPDATE tmp_fact_inspectionlotsemoyequipment AS f
SET f.dim_createddateid = IFNULL(dd.dim_dateid, 1)
FROM tmp_fact_inspectionlotsemoyequipment AS f, dim_date AS dd 
WHERE LEFT(f.dd_createddate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_createddateid <> dd.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoyequipment AS f
SET f.dim_tranformdateid = IFNULL(dd.dim_dateid, 1)
FROM tmp_fact_inspectionlotsemoyequipment AS f, dim_date AS dd 
WHERE LEFT(f.dd_transformdate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_tranformdateid <> dd.dim_dateid;
/* END - update dim dates from fact */

/* BEGIN - count measure for sap batch id level */
DROP TABLE IF EXISTS tmp_unique_line_ilseq;
CREATE TABLE tmp_unique_line_ilseq
AS 
SELECT
  dd_operationname,
  dd_sapbatchid,
  SUM(1) AS ct_countatsapbatchlevel
FROM tmp_fact_inspectionlotsemoyequipment
WHERE dd_paramid <> 'Date Analyse'
GROUP BY 
  dd_operationname,
  dd_sapbatchid;

UPDATE tmp_fact_inspectionlotsemoyequipment AS f
SET f.ct_countatsapbatchlevel = t.ct_countatsapbatchlevel
FROM tmp_fact_inspectionlotsemoyequipment AS f, tmp_unique_line_ilseq AS t
WHERE f.dd_operationname = t.dd_operationname
  AND f.dd_sapbatchid = t.dd_sapbatchid
  AND f.dd_paramid = 'Date Analyse';
/* END - count measure for sap batch id level */

DELETE FROM fact_inspectionlotsemoyequipment;

INSERT INTO fact_inspectionlotsemoyequipment(
fact_inspectionlotsemoyequipmentid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, -- s_sampleid
  dd_createddate, -- createdt
  dim_createddateid,
  dd_batchid, -- batchid
  dd_materialid, -- u_sl_materialid
  dd_sapbatchid,  -- u_sapbatchid
  dd_batchstatus, -- batchstatus
  dd_lotnumber, -- u_lotnumber
  dd_requestdesc, -- requestdesc
  dd_requeststatus, -- requeststatus
  dd_materialname, -- materialname
  dd_materialfamily, -- u_materialfamily
  dd_paramlistid, -- paramlistid
  dd_paramlistversionid, -- paramlistversionid
  dd_variantid, -- variantid
  dd_operationname, -- u_operationname
  dd_paramid, -- paramid
  dd_transformtext, -- transformtext
  dd_transformdate, -- transformdt
  dim_tranformdateid,
  dd_instrumentmodelid, -- instrumentmodelid
  dd_routineflag,
  ct_counttest,
  ct_countatsapbatchlevel
)
SELECT
  fact_inspectionlotsemoyequipmentid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_sampleid, -- s_sampleid
  dd_createddate, -- createdt
  dim_createddateid,
  dd_batchid, -- batchid
  dd_materialid, -- u_sl_materialid
  dd_sapbatchid,  -- u_sapbatchid
  dd_batchstatus, -- batchstatus
  dd_lotnumber, -- u_lotnumber
  dd_requestdesc, -- requestdesc
  dd_requeststatus, -- requeststatus
  dd_materialname, -- materialname
  dd_materialfamily, -- u_materialfamily
  dd_paramlistid, -- paramlistid
  dd_paramlistversionid, -- paramlistversionid
  dd_variantid, -- variantid
  dd_operationname, -- u_operationname
  dd_paramid, -- paramid
  dd_transformtext, -- transformtext
  dd_transformdate, -- transformdt
  dim_tranformdateid,
  dd_instrumentmodelid, -- instrumentmodelid
  dd_routineflag,
  ct_counttest,
  ct_countatsapbatchlevel
FROM tmp_fact_inspectionlotsemoyequipment;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoyequipment;

DELETE FROM emd586.fact_inspectionlotsemoyequipment;

INSERT INTO emd586.fact_inspectionlotsemoyequipment
SELECT *
FROM fact_inspectionlotsemoyequipment;
