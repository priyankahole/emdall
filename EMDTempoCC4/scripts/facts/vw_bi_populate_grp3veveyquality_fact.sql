/*****************************************************************************************************************/
/*   Script         : bi_populate_veveyquality_fact                                                              */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 21 Nov 2016                                                                                */
/*   Description    : Populating script of fact_veveyquality for Brazil & Uruguay region                         */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   21 Nov 2016         CristianT      1.0          Creating the script.                                        */
/*   17 Jan 2017         CristianT      1.1          Adding logic for ct_countreccurent,ct_withrootcause,ct_withoutrootcause,ct_confirmedrootcause,ct_countrepetitve,ct_trackandtrendcause */
/*   02 Feb 2017         CristianT      1.2          Adding logic for dim_qualityusersidoriginator               */
/*   14 Feb 2017         CristianT      1.3          Adding logic for dim_dateidassessment                       */
/*   17 Feb 2017         CristianT      1.4          I removed this fact from the harmonization project and i added the delete/insert statements for emd586 inside the script */
/*   21 Feb 2017         CristianT      1.5          Improve the performance for the emd586 insert. Requested on BI-5539 */
/*   02 Mar 2017         CristianT      1.6          For records where dd_project = 'Deviation' we should calculate Due Date as Open Date + 30 */
/*   03 Mar 2017         CristianT      1.7          Investigation due and late logic                            */
/*   28 Mar 2017         CristianT      1.8          Adding previous week WIP in the current week for the WIP Delta 1 Week logic
                                                     Adding 12 week WIP in the current week for the WIP Delta 12 Week logic
                                                     Adding 12 week Late in the current week for the Count Late Delta 12 Week logic */
/*   29 Mar 2017         CristianT      1.9          Calculating Due Date for records where Project = Investigation as Open Date + 30 of the Parent Deviation */
/*   30 Mar 2017         CristianT      2.0          Logic for Recurrent Deviations if the Child Investigation is Reccurent */
/*   02 May 2017         CristianT      2.1          Adding dd_region. This attribute will be used in order to see from what region the data is comming */
/*   18 May 2017         CristianT      2.2          Adding new mapping logic with dim_qualityusers to use userid for people that are still using old ID in TW application */
/******************************************************************************************************************/

/* CristianT: Using veveyhistory_delete table in case we reprocess so we won't have duplicate data for same snapshot date in the historical table.
Historical data: fact_veveyqualityhistory will preserve all rows, any alter made on fact_veveyquality we should make it on fact_veveyqualityhistory aswell.
Reporting data: fact_veveyquality will keep only 1 day for each week of year. In case we have 1 full week in history table we should keep only Saturday. In case the week just started we should keep last processed day.
*/
DROP TABLE IF EXISTS veveyhistory_delete;
CREATE TABLE veveyhistory_delete
AS
SELECT fact_veveyqualityid
FROM fact_veveyqualityhistory
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
      AND dd_region = 'Brazil & Uruguay';

DELETE FROM fact_veveyqualityhistory
WHERE fact_veveyqualityid IN (SELECT fact_veveyqualityid FROM veveyhistory_delete);

DROP TABLE IF EXISTS veveyhistory_delete;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_veveyqualityhistory';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_veveyqualityhistory', ifnull(max(fact_veveyqualityid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_veveyqualityhistory;

DROP TABLE IF EXISTS tmp_fact_veveyquality;
CREATE TABLE tmp_fact_veveyquality
AS
SELECT *
FROM fact_veveyqualityhistory
WHERE 1 = 0;

INSERT INTO tmp_fact_veveyquality(
fact_veveyqualityid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_prid,
dd_title,
dd_parentid,
dd_project,
dd_site,
dd_recordstate,
dd_originator,
dd_assignedto,
dim_dateidopened,
dim_dateidclosed,
dim_dateidcurrentstate,
dim_dateiddue,
dd_priority,
dd_classifiedby,
dd_proposal_approved_for_imp,
dd_eventtype,
dd_localqa,
dd_assignedqa,
dd_deviation_main_categories,
dd_finalcomments,
dd_qaapprovedby,
dd_capatype,
dd_finedeviationcategories,
dd_effectivenesscheckneeded,
dd_temporarychange,
dim_dateidproposedstart,
dim_dateidproposedend,
dd_localmultisite,
dd_minormajor,
dd_classifiedon,
dd_rootcausecategory,
dd_neededforchangeeffective,
dd_changeeffectiveon,
dd_capaneeded,
dd_supervisorofreportingunit,
dd_qaapprovedon,
dd_assessmentdate,
dd_recurrence,
dd_changecontrolneeded,
dd_dateofdetection,
dd_finerootcausecategory,
dd_duedateeffectivenesscheck,
dd_capaeffective,
dd_batchdispositionimpact,
dim_qualityusersidassignedto,
dim_qualityusersidassignedqa,
snapshotdate,
dim_dateidsnapshot,
ct_countreccurent,
ct_withrootcause,
ct_withoutrootcause,
ct_confirmedrootcause,
ct_countrepetitve,
ct_trackandtrendcause,
dim_dateidpropapprovedforimp,
ct_orderby,
ct_orderbypriority,
dim_qualityusersidoriginator,
dim_dateidassessment,
dim_qualityusersidccporiginator,
dd_investigationstatus,
ct_1weekdelta,
ct_12weekdelta,
ct_late12weekdelta,
dd_recurrentchildinvestigation,
dd_region,
dd_originator_userid,
dd_assignedto_userid,
dd_classifiedby_userid,
dd_local_qa_userid,
dd_assigned_qa_userid,
dd_qa_approved_by_userid,
dd_superv_repunit_userid,
dd_division
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_veveyqualityhistory') + ROW_NUMBER() over(order by '') AS fact_veveyqualityid,
       t.*
FROM (
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(mv.main3_pr_id, 0) as dd_prid,
       ifnull(mv.main3_title, 'Not Set') as dd_title,
       ifnull(mv.main3_parent_id, 0) as dd_parentid,
       ifnull(mv.main3_project, 'Not Set') as dd_project,
       ifnull(mv.main3_site, 'Not Set') as dd_site,
       ifnull(mv.main3_record_state, 'Not Set') as dd_recordstate,
       ifnull(mv.main3_originator, 'Not Set') as dd_originator,
       ifnull(mv.main3_assignedto, 'Not Set') as dd_assignedto,
       1 as dim_dateidopened,
       1 as dim_dateidclosed,
       1 as dim_dateidcurrentstate,
       1 as dim_dateiddue,
       ifnull(mv.main3_priority, 'Not Set') as dd_priority,
       ifnull(mv.main3_classified_by, 'Not Set') as dd_classifiedby,
       ifnull(mv.main3_proposal_approved_for_imp, 'Not Set') as dd_proposal_approved_for_imp,
       ifnull(mv.main3_event_type, 'Not Set') as dd_eventtype,
       ifnull(mv.main3_local_qa, 'Not Set') as dd_localqa,
       ifnull(mv.main3_assigned_qa, 'Not Set') as dd_assignedqa,
       ifnull(mv.main3_deviation_main_categories, 'Not Set') as dd_deviation_main_categories,
       ifnull(mv.main3_final_comments, 'Not Set') as dd_finalcomments,
       ifnull(mv.main3_qa_approved_by, 'Not Set') as dd_qaapprovedby,
       ifnull(mv.main3_capa_type, 'Not Set') as dd_capatype,
       ifnull(mv.main3_fine_deviation_categories, 'Not Set') as dd_finedeviationcategories,
       ifnull(mv.main3_effectiveness_check_needed, 'Not Set') as dd_effectivenesscheckneeded,
       ifnull(mv.main3_temporary_change, 'Not Set') as dd_temporarychange,
       1 as dim_dateidproposedstart,
       1 as dim_dateidproposedend,
       ifnull(mv.main3_local_multisite, 'Not Set') as dd_localmultisite,
       ifnull(mv.main3_minor_major, 'Not Set') as dd_minormajor,
       ifnull(mv.main3_classified_on, 'Not Set') as dd_classifiedon,
       ifnull(mv.main3_root_cause_category, 'Not Set') as dd_rootcausecategory,
       ifnull(mv.main3_needed_for_change_effective, 'Not Set') as dd_neededforchangeeffective,
       ifnull(mv.main3_change_effective_on, 'Not Set') as dd_changeeffectiveon,
       ifnull(mv.main3_capa_needed, 'Not Set') as dd_capaneeded,
       ifnull(mv.main3_supervisorofreportingunit, 'Not Set') as dd_supervisorofreportingunit,
       ifnull(mv.main3_qa_approved_on, 'Not Set') as dd_qaapprovedon,
       ifnull(mv.main3_assessment_date, 'Not Set') as dd_assessmentdate,
       ifnull(mv.main3_recurrence, 'Not Set') as dd_recurrence,
       ifnull(mv.main3_changecontrol_needed, 'Not Set') as dd_changecontrolneeded,
       ifnull(mv.main3_date_of_detection, 'Not Set') as dd_dateofdetection,
       ifnull(mv.main3_fineroot_causecategory, 'Not Set') as dd_finerootcausecategory,
       ifnull(mv.main3_duedate_effectiveness_check, 'Not Set') as dd_duedateeffectivenesscheck,
       ifnull(mv.main3_capa_effective, 'Not Set') as dd_capaeffective,
       ifnull(mv.main3_batch_disposition_impact, 'Not Set') as dd_batchdispositionimpact,
       1 as dim_qualityusersidassignedto,
       1 as dim_qualityusersidassignedqa,
       CASE WHEN extract(hour from current_timestamp) between 0 AND 18 THEN current_date - 1 ELSE current_date END as snapshotdate,
       dt.dim_dateid as dim_dateidsnapshot,
       0 as ct_countreccurent,
       0 as ct_withrootcause,
       0 as ct_withoutrootcause,
       0 as ct_confirmedrootcause,
       0 as ct_countrepetitve,
       0 as ct_trackandtrendcause,
       1 as dim_dateidpropapprovedforimp,
       0 as ct_orderby,
       0 as ct_orderbypriority,
       1 as dim_qualityusersidoriginator,
       1 as dim_dateidassessment,
       1 as dim_qualityusersidccporiginator,
       'Not Set' as dd_investigationstatus,
       0 as ct_1weekdelta,
       0 as ct_12weekdelta,
       0 as ct_late12weekdelta,
       'Not Set' as dd_recurrentchildinvestigation,
       'Brazil & Uruguay' as dd_region,
       ifnull(mv.main3_originator_userid, 'Not Set') as dd_originator_userid,
       ifnull(mv.main3_assignedto_userid, 'Not Set') as dd_assignedto_userid,
       ifnull(mv.main3_classifiedby_userid, 'Not Set') as dd_classifiedby_userid,
       ifnull(mv.main3_local_qa_userid, 'Not Set') as dd_local_qa_userid,
       ifnull(mv.main3_assigned_qa_userid, 'Not Set') as dd_assigned_qa_userid,
       ifnull(mv.main3_qa_approved_by_userid, 'Not Set') as dd_qa_approved_by_userid,
       ifnull(mv.main3_superv_repunit_userid, 'Not Set') as dd_superv_repunit_userid,
       'Not Set' as dd_division
FROM mv_xtr_grp3_rpt_main mv
     INNER JOIN dim_date dt ON dt.companycode = 'Not Set' AND dt.datevalue = CASE WHEN extract(hour from current_timestamp) between 0 AND 18 THEN current_date - 1 ELSE current_date END
     ) t;

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_veveyquality tmp;

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateidopened = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     mv_xtr_grp3_rpt_main mv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = mv.main3_pr_id
      AND dt.companycode = 'Not Set'
      AND mv.main3_date_opened = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidopened <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateidclosed = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     mv_xtr_grp3_rpt_main mv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = mv.main3_pr_id
      AND dt.companycode = 'Not Set'
      AND mv.main3_date_closed = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidclosed <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateidcurrentstate = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     mv_xtr_grp3_rpt_main mv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = mv.main3_pr_id
      AND dt.companycode = 'Not Set'
      AND mv.main3_date_current_state = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidcurrentstate <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateiddue = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     mv_xtr_grp3_rpt_main mv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = mv.main3_pr_id
      AND dt.companycode = 'Not Set'
      AND mv.main3_date_due = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateiddue <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateidproposedstart = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     mv_xtr_grp3_rpt_main mv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = mv.main3_pr_id
      AND dt.companycode = 'Not Set'
      AND mv.main3_proposed_start_date = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidproposedstart <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateidproposedend = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     mv_xtr_grp3_rpt_main mv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = mv.main3_pr_id
      AND dt.companycode = 'Not Set'
      AND mv.main3_proposed_end_date = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidproposedend <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateidpropapprovedforimp = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     mv_xtr_grp3_rpt_main mv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = mv.main3_pr_id
      AND dt.companycode = 'Not Set'
      AND mv.main3_proposal_approved_for_imp = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidpropapprovedforimp <> ifnull(dt.dim_dateid, 1);

/* 14 Feb 2017 CristianT Start: Adding logic for dim_dateidassessment */
UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateidassessment = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     mv_xtr_grp3_rpt_main mv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = mv.main3_pr_id
      AND dt.companycode = 'Not Set'
      AND to_date(mv.main3_assessment_date, 'YYYY-MM-DD') = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidassessment <> ifnull(dt.dim_dateid, 1);

/* 14 Feb 2017 CristianT End */

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_qualityusersidassignedto = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_assignedto_userid = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dim_qualityusersidassignedto <> ifnull(usr.dim_qualityusersid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_qualityusersidassignedqa = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_assigned_qa_userid = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dim_qualityusersidassignedqa <> ifnull(usr.dim_qualityusersid, 1);

/* 02 Feb 2017 CristianT Start: Adding logic for dim_qualityusersidoriginator */
UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_qualityusersidoriginator = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_originator_userid = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dim_qualityusersidoriginator <> ifnull(usr.dim_qualityusersid, 1);

/* 02 Feb 2017 CristianT End */

/* 18 May 2017 CristianT Start: Adding new mapping logic with dim_qualityusers to use userid in case merck_uid it's not enough */
UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_qualityusersidassignedto = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_assignedto_userid = usr.userid
      AND usr.rowiscurrent = 1
      AND usr.userid <> 'Not Set'
      AND tmp.dim_qualityusersidassignedto <> ifnull(usr.dim_qualityusersid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_qualityusersidassignedqa = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_assigned_qa_userid = usr.userid
      AND usr.rowiscurrent = 1
      AND usr.userid <> 'Not Set'
      AND tmp.dim_qualityusersidassignedqa <> ifnull(usr.dim_qualityusersid, 1);

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_qualityusersidoriginator = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_originator_userid = usr.userid
      AND usr.rowiscurrent = 1
      AND usr.userid <> 'Not Set'
      AND tmp.dim_qualityusersidoriginator <> ifnull(usr.dim_qualityusersid, 1);
/* 18 May 2017 CristianT End */


/* 17 Jan 2017 CristianT Start: Adding logic for ct_countreccurent,ct_withrootcause,ct_withoutrootcause,ct_confirmedrootcause,ct_countrepetitve,ct_trackandtrendcause */
UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_countreccurent = 1
WHERE tmp.dd_finalcomments like '%ER12M%';

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_withrootcause = 1
WHERE tmp.dd_finalcomments like '%RCP%';

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_withoutrootcause = 1
WHERE tmp.dd_finalcomments like '%NRC%';

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_confirmedrootcause = 1
WHERE tmp.dd_finalcomments like '%RCC%';

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_countrepetitve = 1
WHERE tmp.dd_finalcomments like '%DR12M%';

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_trackandtrendcause = 1
WHERE tmp.dd_finalcomments like '%T&T%';

/* 17 Jan 2017 CristianT End */
UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_orderby = CASE
                       WHEN dd_recordstate = 'Open' THEN 1
                       WHEN dd_recordstate = 'Awaiting Assessment' THEN 2
                       WHEN dd_recordstate = 'Pending Investigation(s)' AND dd_priority = 'Low' THEN 3
                       WHEN dd_recordstate = 'Waiting for Investigation(s) and CA(s)' AND dd_priority = 'Low' THEN 3
                       WHEN dd_recordstate = 'Pending Investigation(s)' AND dd_priority = 'Medium' THEN 4
                       WHEN dd_recordstate = 'Waiting for Investigation(s) and CA(s)' AND dd_priority = 'Medium' THEN 4
                       WHEN dd_recordstate = 'Pending Investigation(s)' AND dd_priority = 'High' THEN 5
                       WHEN dd_recordstate = 'Waiting for Investigation(s) and CA(s)' AND dd_priority = 'High' THEN 5
                       WHEN dd_recordstate = 'Pending QA Closure' THEN 6
                       ELSE 7
                     END;

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_orderbypriority = CASE
                               WHEN dd_priority = 'Low' THEN 1
                               WHEN dd_priority = 'Medium' THEN 2
                               WHEN dd_priority = 'High' THEN 3
                               ELSE 4
                             END;

/* 24 Feb 2017 CristianT Start: Logic for CCP Originator */
DROP TABLE IF EXISTS tmp_ccporiginator;
CREATE TABLE tmp_ccporiginator
AS
SELECT f_veve.fact_veveyqualityid as fact_veveyqualityid,
       CASE
         WHEN assto.dd_departmenttype <> 'Not Set' then f_veve.dim_qualityusersidassignedto
         ELSE f_veve.dim_qualityusersidoriginator
       END as dim_qualityusersidccporiginator
FROM tmp_fact_veveyquality f_veve,
     dim_qualityusers assto,
     dim_qualityusers orig
WHERE 1 = 1
      AND f_veve.dim_qualityusersidassignedto = assto.dim_qualityusersid
      AND f_veve.dim_qualityusersidoriginator = orig.dim_qualityusersid
      AND f_veve.DD_PROJECT in ('Change Control','Secondary Change Control');

UPDATE tmp_fact_veveyquality f_veve
SET f_veve.dim_qualityusersidccporiginator = ifnull(tmp.dim_qualityusersidccporiginator, 1)
FROM tmp_fact_veveyquality f_veve,
     tmp_ccporiginator tmp
WHERE f_veve.fact_veveyqualityid = tmp.fact_veveyqualityid
      AND f_veve.dim_qualityusersidccporiginator <> ifnull(tmp.dim_qualityusersidccporiginator, 1);

DROP TABLE IF EXISTS tmp_ccporiginator;

/* 24 Feb 2017 CristianT End */

/* 02 Mar 2017 CristianT Start: For records where dd_project = 'Deviation' we should calculate Due Date as Open Date + 30 */
DROP TABLE IF EXISTS tmp_deviationduedate;
CREATE TABLE tmp_deviationduedate
AS
SELECT tmp.fact_veveyqualityid as fact_veveyqualityid,
       (opnd.datevalue + 30) as duedate
FROM tmp_fact_veveyquality tmp,
     dim_date opnd
WHERE tmp.dim_dateidopened = opnd.dim_dateid
      AND tmp.dim_dateidopened <> 1
      AND tmp.dim_dateiddue = 1
      AND tmp.dd_project = 'Deviation';

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateiddue = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_veveyquality tmp,
     dim_date dt,
     tmp_deviationduedate due
WHERE tmp.fact_veveyqualityid = due.fact_veveyqualityid
      AND dt.datevalue = due.duedate
      AND dt.companycode = 'Not Set'
      AND dt.projectsourceid = tmp.dim_projectsourceid
      AND tmp.dim_dateiddue <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_deviationduedate;

/* 02 Mar 2017 CristianT End */

/* 03 Mar 2017 CristianT Start: Investigation due and late logic */
DROP TABLE IF EXISTS tmp_parentdeviations;
CREATE TABLE tmp_parentdeviations
AS
SELECT tmp.dd_prid as parentid,
       opnd.datevalue as opendate
FROM tmp_fact_veveyquality tmp,
     dim_date opnd
WHERE tmp.dd_project = 'Deviation'
      AND tmp.dim_dateidclosed = 1
      AND tmp.dim_dateidopened = opnd.dim_dateid;

DROP TABLE IF EXISTS tmp_investigationstatus;
CREATE TABLE tmp_investigationstatus
AS
SELECT tmp.fact_veveyqualityid,
       CASE
          WHEN tmp.dim_dateidclosed <> 1 THEN 'Closed Investigation'
          WHEN tmp.dim_dateidclosed = 1 AND tmp.snapshotdate between (prt.opendate + 15) AND (prt.opendate + 20) THEN 'Open Investigation Due'
          WHEN tmp.dim_dateidclosed = 1 AND tmp.snapshotdate > (prt.opendate + 20) THEN 'Open Investigation Late'
		  ELSE 'Not Set'
       END as investigationstatus
FROM tmp_fact_veveyquality tmp,
     tmp_parentdeviations prt
WHERE tmp.dd_project = 'Investigation'
      AND convert(bigint, case when tmp.dd_parentid = 'Not Set' then '0' else tmp.dd_parentid end) = prt.parentid;

UPDATE tmp_fact_veveyquality tmp
SET tmp.dd_investigationstatus = inv.investigationstatus
FROM tmp_fact_veveyquality tmp,
     tmp_investigationstatus inv
WHERE tmp.fact_veveyqualityid = inv.fact_veveyqualityid;

DROP TABLE IF EXISTS tmp_investigationstatus;
DROP TABLE IF EXISTS tmp_parentdeviations;

/* 03 Mar 2017 CristianT End */

/* 28 Mar 2017 CristianT Start: Adding previous week WIP in the current week for the WIP Delta 1 Week logic */
DROP TABLE IF EXISTS tmp_prvweekid;
CREATE TABLE tmp_prvweekid
AS
SELECT to_char((snps.calendarweekyr - 1)) as prvweekid,
       dd_region
FROM tmp_fact_veveyquality tmp,
     dim_date snps
WHERE tmp.dim_dateidsnapshot = snps.dim_dateid
GROUP BY to_char((snps.calendarweekyr - 1)),
         dd_region;

DROP TABLE IF EXISTS tmp_previousweekwip;
CREATE TABLE tmp_previousweekwip
AS
SELECT f_veve.dd_prid as dd_prid
FROM fact_veveyquality f_veve,
     dim_date snps,
     dim_date opnd,
     tmp_prvweekid prv
WHERE 1 = 1
      AND f_veve.dd_region = prv.dd_region
      AND f_veve.dim_dateidsnapshot = snps.dim_dateid
      AND f_veve.dim_dateidopened = opnd.dim_dateid
      AND snps.calendarweekyr = prv.prvweekid
      AND case WHEN f_veve.dd_project NOT IN ('Corrective Action','Preventive Action') and f_veve.dd_recordstate not like 'Closed%' and opnd.calendarweekyr <= snps.calendarweekyr then 1 WHEN f_veve.dd_project IN ('Corrective Action','Preventive Action') AND f_veve.dd_recordstate IN ('Open','CAPAs in Progress','Pending QA Approval','Pending QA Review','Pending Supervisor Approval') and f_veve.dd_recordstate not like 'Closed%' and opnd.calendarweekyr <= snps.calendarweekyr then 1 else 0 end > 0;

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_1weekdelta = 1
FROM tmp_previousweekwip prv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = prv.dd_prid;

DROP TABLE IF EXISTS tmp_prvweekid;
DROP TABLE IF EXISTS tmp_previousweekwip;
/* 28 Mar 2017 CristianT End */

/* 28 Mar 2017 CristianT Start: Adding 12 week WIP in the current week for the WIP Delta 12 Week logic */
DROP TABLE IF EXISTS tmp_prv12weekid;
CREATE TABLE tmp_prv12weekid
AS
SELECT to_char((snps.calendarweekyr - 12)) as prvweekid,
       dd_region
FROM tmp_fact_veveyquality tmp,
     dim_date snps
WHERE tmp.dim_dateidsnapshot = snps.dim_dateid
GROUP BY to_char((snps.calendarweekyr - 12)),
         dd_region;

DROP TABLE IF EXISTS tmp_previous12weekwip;
CREATE TABLE tmp_previous12weekwip
AS
SELECT f_veve.dd_prid as dd_prid
FROM fact_veveyquality f_veve,
     dim_date snps,
     dim_date opnd,
     tmp_prv12weekid prv
WHERE 1 = 1
      AND f_veve.dd_region = prv.dd_region
      AND f_veve.dim_dateidsnapshot = snps.dim_dateid
      AND f_veve.dim_dateidopened = opnd.dim_dateid
      AND snps.calendarweekyr = prv.prvweekid
      AND case WHEN f_veve.dd_project NOT IN ('Corrective Action','Preventive Action') and f_veve.dd_recordstate not like 'Closed%' and opnd.calendarweekyr <= snps.calendarweekyr then 1 WHEN f_veve.dd_project IN ('Corrective Action','Preventive Action') AND f_veve.dd_recordstate IN ('Open','CAPAs in Progress','Pending QA Approval','Pending QA Review','Pending Supervisor Approval') and f_veve.dd_recordstate not like 'Closed%' and opnd.calendarweekyr <= snps.calendarweekyr then 1 else 0 end > 0;

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_12weekdelta = 1
FROM tmp_previous12weekwip prv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = prv.dd_prid;

DROP TABLE IF EXISTS tmp_prv12weekid;
DROP TABLE IF EXISTS tmp_previous12weekwip;
/* 28 Mar 2017 CristianT End */

/* 28 Mar 2017 CristianT Start: Adding 12 week Late in the current week for the Count Late Delta 12 Week logic */
DROP TABLE IF EXISTS tmp_prv12weekid;
CREATE TABLE tmp_prv12weekid
AS
SELECT to_char((snps.calendarweekyr - 12)) as prvweekid,
       dd_region
FROM tmp_fact_veveyquality tmp,
     dim_date snps
WHERE tmp.dim_dateidsnapshot = snps.dim_dateid
GROUP BY to_char((snps.calendarweekyr - 12)),
         dd_region;

DROP TABLE IF EXISTS tmp_previous12weeklate;
CREATE TABLE tmp_previous12weeklate
AS
SELECT f_veve.dd_prid as dd_prid
FROM fact_veveyquality f_veve,
     dim_date snps,
     dim_date opnd,
     dim_date clsd,
     dim_date dued,
     tmp_prv12weekid prv
WHERE 1 = 1
      AND f_veve.dd_region = prv.dd_region
      AND f_veve.dim_dateidsnapshot = snps.dim_dateid
      AND f_veve.dim_dateidopened = opnd.dim_dateid
      AND f_veve.dim_dateidclosed = clsd.dim_dateid
      AND f_veve.dim_dateiddue = dued.dim_dateid
      AND snps.calendarweekyr = prv.prvweekid
      AND CASE WHEN f_veve.dd_project NOT IN ('Corrective Action','Preventive Action') AND clsd.dim_dateid = 1 AND dued.dim_dateid <> 1 AND snps.datevalue > dued.datevalue THEN 1 WHEN f_veve.dd_project IN ('Corrective Action','Preventive Action') AND f_veve.dd_recordstate IN ('Open','CAPAs in Progress','Pending QA Approval','Pending QA Review','Pending Supervisor Approval') AND clsd.dim_dateid = 1 AND dued.dim_dateid <> 1 AND snps.datevalue > dued.datevalue THEN 1 ELSE 0 END > 0;

UPDATE tmp_fact_veveyquality tmp
SET tmp.ct_late12weekdelta = 1
FROM tmp_previous12weeklate prv,
     tmp_fact_veveyquality tmp
WHERE tmp.dd_prid = prv.dd_prid;

DROP TABLE IF EXISTS tmp_prv12weekid;
DROP TABLE IF EXISTS tmp_previous12weeklate;
/* 28 Mar 2017 CristianT End */

/* 29 Mar 2017 CristianT Start: Calculating Due Date for records where Project = Investigation as Open Date + 30 of the Parent Deviation */
DROP TABLE IF EXISTS tmp_parentdeviationsopendate;
CREATE TABLE tmp_parentdeviationsopendate
AS
SELECT tmp.dd_prid as parentid,
       opnd.datevalue as opendate
FROM tmp_fact_veveyquality tmp,
     dim_date opnd
WHERE tmp.dd_project = 'Deviation'
      AND tmp.dim_dateidopened = opnd.dim_dateid;

DROP TABLE IF EXISTS tmp_investigations;
CREATE TABLE tmp_investigations
AS
SELECT tmp.fact_veveyqualityid,
       prt.opendate + 30 as duedate
FROM tmp_fact_veveyquality tmp,
     tmp_parentdeviationsopendate prt
WHERE tmp.dd_project = 'Investigation'
      AND convert(bigint, case when tmp.dd_parentid = 'Not Set' then '0' else tmp.dd_parentid end) = prt.parentid;

UPDATE tmp_fact_veveyquality tmp
SET tmp.dim_dateiddue = ifnull(dued.dim_dateid, 1)
FROM tmp_fact_veveyquality tmp,
     tmp_investigations inv,
     dim_date dued
WHERE tmp.fact_veveyqualityid = inv.fact_veveyqualityid
      AND inv.duedate = dued.datevalue
      AND dued.companycode = 'Not Set'
      AND tmp.dim_dateiddue <> ifnull(dued.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_parentdeviationsopendate;
DROP TABLE IF EXISTS tmp_investigations;

/* 29 Mar 2017 CristianT End */

/* 30 Mar 2017 CristianT Start: Logic for Recurrent Deviations if the Child Investigation is Reccurent */
DROP TABLE IF EXISTS tmp_parentdeviations;
CREATE TABLE tmp_parentdeviations
AS
SELECT tmp.dd_prid as parentid
FROM tmp_fact_veveyquality tmp
WHERE tmp.dd_project = 'Deviation';


DROP TABLE IF EXISTS tmp_investigationrecurrence;
CREATE TABLE tmp_investigationrecurrence
AS
SELECT DISTINCT prt.parentid as parentid
FROM tmp_fact_veveyquality tmp,
     tmp_parentdeviations prt
WHERE tmp.dd_project = 'Investigation'
      AND tmp.dd_recurrence = 'Yes'
      AND convert(bigint, case when tmp.dd_parentid = 'Not Set' then '0' else tmp.dd_parentid end) = prt.parentid;

UPDATE tmp_fact_veveyquality tmp
SET tmp.dd_recurrentchildinvestigation = 'Yes'
FROM tmp_fact_veveyquality tmp,
     tmp_investigationrecurrence inv
WHERE tmp.dd_prid = inv.parentid;

DROP TABLE IF EXISTS tmp_investigationrecurrence;
DROP TABLE IF EXISTS tmp_parentdeviations;

/* 30 Mar 2017 CristianT End */

UPDATE tmp_fact_veveyquality
SET dd_site = CASE
                WHEN dd_site = 'Brazil' THEN 'Brazil, Rio'
                WHEN dd_site = 'France, Martillac' THEN 'France, Martillac'
                WHEN dd_site = 'France, Semoy' THEN 'France, Semoy'
                WHEN dd_site = 'Germany, Darmstadt Pharma' THEN 'Germany, Darmstadt'
                WHEN dd_site = 'Italy, Ardea' THEN 'Italy, Ardea'
                WHEN dd_site = 'Italy, Bari' THEN 'Italy, Bari'
                WHEN dd_site = 'Italy, Casilina' THEN 'Italy, Casilina'
                WHEN dd_site = 'Italy, RBM' THEN 'Italy, Ivrea'
                WHEN dd_site = 'Italy, Tiburtina' THEN 'Italy, Guidonia'
                WHEN dd_site = 'Mexico' THEN 'Mexico, Mexico City'
                WHEN dd_site = 'Nantong' THEN 'China, Nantong'
                WHEN dd_site = 'Spain, Mollet del Valles' THEN 'Spain, Mollet'
                WHEN dd_site = 'Spain, Tres Cantos' THEN 'Spain, Tres Cantos'
                WHEN dd_site = 'Switzerland, Aubonne' THEN 'Switzerland, Aubonne'
                WHEN dd_site = 'Switzerland, Vevey' THEN 'Switzerland, Vevey'
                WHEN dd_site = 'Uruguay' THEN 'Uruguay, Montevideo'
              END,
    dd_division = CASE
                    WHEN dd_site in ('Nantong', 'Uruguay') THEN 'Distribution'
                    WHEN dd_site in ('Brazil', 'France, Semoy', 'Germany, Darmstadt Pharma', 'Mexico', 'Spain, Mollet del Valles') THEN 'Pharma'
                    WHEN dd_site in ('France, Martillac', 'Italy, Ardea', 'Italy, Bari', 'Italy, Casilina', 'Italy, RBM', 'Italy, Tiburtina', 'Spain, Tres Cantos', 'Switzerland, Aubonne', 'Switzerland, Vevey') THEN 'Biotech'
                  END;

INSERT INTO fact_veveyqualityhistory (
fact_veveyqualityid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_prid,
dd_title,
dd_parentid,
dd_project,
dd_site,
dd_recordstate,
dd_originator,
dd_assignedto,
dim_dateidopened,
dim_dateidclosed,
dim_dateidcurrentstate,
dim_dateiddue,
dd_priority,
dd_classifiedby,
dd_proposal_approved_for_imp,
dd_eventtype,
dd_localqa,
dd_assignedqa,
dd_deviation_main_categories,
dd_finalcomments,
dd_qaapprovedby,
dd_capatype,
dd_finedeviationcategories,
dd_effectivenesscheckneeded,
dd_temporarychange,
dim_dateidproposedstart,
dim_dateidproposedend,
dd_localmultisite,
dd_minormajor,
dd_classifiedon,
dd_rootcausecategory,
dd_neededforchangeeffective,
dd_changeeffectiveon,
dd_capaneeded,
dd_supervisorofreportingunit,
dd_qaapprovedon,
dd_assessmentdate,
dd_recurrence,
dd_changecontrolneeded,
dd_dateofdetection,
dd_finerootcausecategory,
dd_duedateeffectivenesscheck,
dd_capaeffective,
dd_batchdispositionimpact,
dim_qualityusersidassignedto,
dim_qualityusersidassignedqa,
snapshotdate,
dim_dateidsnapshot,
ct_countreccurent,
ct_withrootcause,
ct_withoutrootcause,
ct_confirmedrootcause,
ct_countrepetitve,
ct_trackandtrendcause,
dim_dateidpropapprovedforimp,
ct_orderby,
ct_orderbypriority,
dim_qualityusersidoriginator,
dim_dateidassessment,
dim_qualityusersidccporiginator,
dd_investigationstatus,
ct_1weekdelta,
ct_12weekdelta,
ct_late12weekdelta,
dd_recurrentchildinvestigation,
dd_region,
dd_originator_userid,
dd_assignedto_userid,
dd_classifiedby_userid,
dd_local_qa_userid,
dd_assigned_qa_userid,
dd_qa_approved_by_userid,
dd_superv_repunit_userid,
dd_division
)
SELECT fact_veveyqualityid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_prid,
       dd_title,
       dd_parentid,
       dd_project,
       dd_site,
       dd_recordstate,
       dd_originator,
       dd_assignedto,
       dim_dateidopened,
       dim_dateidclosed,
       dim_dateidcurrentstate,
       dim_dateiddue,
       dd_priority,
       dd_classifiedby,
       dd_proposal_approved_for_imp,
       dd_eventtype,
       dd_localqa,
       dd_assignedqa,
       dd_deviation_main_categories,
       dd_finalcomments,
       dd_qaapprovedby,
       dd_capatype,
       dd_finedeviationcategories,
       dd_effectivenesscheckneeded,
       dd_temporarychange,
       dim_dateidproposedstart,
       dim_dateidproposedend,
       dd_localmultisite,
       dd_minormajor,
       dd_classifiedon,
       dd_rootcausecategory,
       dd_neededforchangeeffective,
       dd_changeeffectiveon,
       dd_capaneeded,
       dd_supervisorofreportingunit,
       dd_qaapprovedon,
       dd_assessmentdate,
       dd_recurrence,
       dd_changecontrolneeded,
       dd_dateofdetection,
       dd_finerootcausecategory,
       dd_duedateeffectivenesscheck,
       dd_capaeffective,
       dd_batchdispositionimpact,
       dim_qualityusersidassignedto,
       dim_qualityusersidassignedqa,
       snapshotdate,
       dim_dateidsnapshot,
       ct_countreccurent,
       ct_withrootcause,
       ct_withoutrootcause,
       ct_confirmedrootcause,
       ct_countrepetitve,
       ct_trackandtrendcause,
       dim_dateidpropapprovedforimp,
       ct_orderby,
       ct_orderbypriority,
       dim_qualityusersidoriginator,
       dim_dateidassessment,
       dim_qualityusersidccporiginator,
       dd_investigationstatus,
       ct_1weekdelta,
       ct_12weekdelta,
       ct_late12weekdelta,
       dd_recurrentchildinvestigation,
       dd_region,
       dd_originator_userid,
       dd_assignedto_userid,
       dd_classifiedby_userid,
       dd_local_qa_userid,
       dd_assigned_qa_userid,
       dd_qa_approved_by_userid,
       dd_superv_repunit_userid,
       dd_division
FROM tmp_fact_veveyquality;

DROP TABLE IF EXISTS tmp_fact_veveyquality;

DROP TABLE IF EXISTS veveyquality_delete;
CREATE TABLE veveyquality_delete
AS
SELECT fact_veveyqualityid
FROM fact_veveyquality
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
      AND dd_region = 'Brazil & Uruguay';

DELETE FROM fact_veveyquality
WHERE fact_veveyqualityid IN (SELECT fact_veveyqualityid FROM veveyquality_delete);

DROP TABLE IF EXISTS veveyquality_delete;


DROP TABLE IF EXISTS veveyquality_insert;
CREATE TABLE veveyquality_insert
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       f.dd_region as dd_region,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM fact_veveyqualityhistory f,
     dim_date dt
WHERE dim_dateidsnapshot = dim_dateid
      AND f.dd_region = 'Brazil & Uruguay'
GROUP BY f.dim_dateidsnapshot,
         f.dd_region,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_veveyquality';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_veveyquality', ifnull(max(fact_veveyqualityid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_veveyquality;


INSERT INTO fact_veveyquality (
fact_veveyqualityid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_prid,
dd_title,
dd_parentid,
dd_project,
dd_site,
dd_recordstate,
dd_originator,
dd_assignedto,
dim_dateidopened,
dim_dateidclosed,
dim_dateidcurrentstate,
dim_dateiddue,
dd_priority,
dd_classifiedby,
dd_proposal_approved_for_imp,
dd_eventtype,
dd_localqa,
dd_assignedqa,
dd_deviation_main_categories,
dd_finalcomments,
dd_qaapprovedby,
dd_capatype,
dd_finedeviationcategories,
dd_effectivenesscheckneeded,
dd_temporarychange,
dim_dateidproposedstart,
dim_dateidproposedend,
dd_localmultisite,
dd_minormajor,
dd_classifiedon,
dd_rootcausecategory,
dd_neededforchangeeffective,
dd_changeeffectiveon,
dd_capaneeded,
dd_supervisorofreportingunit,
dd_qaapprovedon,
dd_assessmentdate,
dd_recurrence,
dd_changecontrolneeded,
dd_dateofdetection,
dd_finerootcausecategory,
dd_duedateeffectivenesscheck,
dd_capaeffective,
dd_batchdispositionimpact,
dim_qualityusersidassignedto,
dim_qualityusersidassignedqa,
snapshotdate,
dim_dateidsnapshot,
ct_countreccurent,
ct_withrootcause,
ct_withoutrootcause,
ct_confirmedrootcause,
ct_countrepetitve,
ct_trackandtrendcause,
dim_dateidpropapprovedforimp,
ct_orderby,
ct_orderbypriority,
dim_qualityusersidoriginator,
dim_dateidassessment,
dim_qualityusersidccporiginator,
dd_investigationstatus,
ct_1weekdelta,
ct_12weekdelta,
ct_late12weekdelta,
dd_recurrentchildinvestigation,
dd_region,
dd_originator_userid,
dd_assignedto_userid,
dd_classifiedby_userid,
dd_local_qa_userid,
dd_assigned_qa_userid,
dd_qa_approved_by_userid,
dd_superv_repunit_userid,
dd_division
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_veveyquality') + ROW_NUMBER() over(order by ''),
       h.dim_projectsourceid,
       h.amt_exhangerate,
       h.amt_exchangerate_gbl,
       h.dim_currencyid,
       h.dim_currencyid_tra,
       h.dim_currencyid_gbl,
       h.dw_insert_date,
       h.dw_update_date,
       h.dd_prid,
       h.dd_title,
       h.dd_parentid,
       h.dd_project,
       h.dd_site,
       h.dd_recordstate,
       h.dd_originator,
       h.dd_assignedto,
       h.dim_dateidopened,
       h.dim_dateidclosed,
       h.dim_dateidcurrentstate,
       h.dim_dateiddue,
       h.dd_priority,
       h.dd_classifiedby,
       h.dd_proposal_approved_for_imp,
       h.dd_eventtype,
       h.dd_localqa,
       h.dd_assignedqa,
       h.dd_deviation_main_categories,
       h.dd_finalcomments,
       h.dd_qaapprovedby,
       h.dd_capatype,
       h.dd_finedeviationcategories,
       h.dd_effectivenesscheckneeded,
       h.dd_temporarychange,
       h.dim_dateidproposedstart,
       h.dim_dateidproposedend,
       h.dd_localmultisite,
       h.dd_minormajor,
       h.dd_classifiedon,
       h.dd_rootcausecategory,
       h.dd_neededforchangeeffective,
       h.dd_changeeffectiveon,
       h.dd_capaneeded,
       h.dd_supervisorofreportingunit,
       h.dd_qaapprovedon,
       h.dd_assessmentdate,
       h.dd_recurrence,
       h.dd_changecontrolneeded,
       h.dd_dateofdetection,
       h.dd_finerootcausecategory,
       h.dd_duedateeffectivenesscheck,
       h.dd_capaeffective,
       h.dd_batchdispositionimpact,
       h.dim_qualityusersidassignedto,
       h.dim_qualityusersidassignedqa,
       h.snapshotdate,
       h.dim_dateidsnapshot,
       h.ct_countreccurent,
       h.ct_withrootcause,
       h.ct_withoutrootcause,
       h.ct_confirmedrootcause,
       h.ct_countrepetitve,
       h.ct_trackandtrendcause,
       h.dim_dateidpropapprovedforimp,
       h.ct_orderby,
       h.ct_orderbypriority,
       h.dim_qualityusersidoriginator,
       h.dim_dateidassessment,
       h.dim_qualityusersidccporiginator,
       h.dd_investigationstatus,
       h.ct_1weekdelta,
       h.ct_12weekdelta,
       h.ct_late12weekdelta,
       h.dd_recurrentchildinvestigation,
       h.dd_region,
       h.dd_originator_userid,
       h.dd_assignedto_userid,
       h.dd_classifiedby_userid,
       h.dd_local_qa_userid,
       h.dd_assigned_qa_userid,
       h.dd_qa_approved_by_userid,
       h.dd_superv_repunit_userid,
       h.dd_division
FROM fact_veveyqualityhistory h,
     veveyquality_insert ins
WHERE h.dim_dateidsnapshot = ins.dim_dateidsnapshot
      AND h.dd_region = ins.dd_region
      AND ins.rowno = 1
      AND NOT EXISTS (SELECT 1 FROM fact_veveyquality t WHERE t.dim_dateidsnapshot = ins.dim_dateidsnapshot AND t.dd_region = ins.dd_region);

DROP TABLE IF EXISTS veveyquality_insert;

DROP TABLE IF EXISTS tmp_delete;
CREATE TABLE tmp_delete
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       f.dd_region as dd_region,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM fact_veveyquality f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
      AND f.dd_region = 'Brazil & Uruguay'
GROUP BY f.dim_dateidsnapshot,
         f.dd_region,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

MERGE INTO fact_veveyquality f
USING tmp_delete del ON f.dim_dateidsnapshot = del.dim_dateidsnapshot AND f.dd_region = del.dd_region AND del.rowno > 1
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_delete;

/* CristianT Start: Mask German people */
DROP TABLE IF EXISTS tmp_originatorgerman;
CREATE TABLE tmp_originatorgerman
AS
SELECT fact_veveyqualityid
FROM fact_veveyquality f_veve,
     dim_qualityusers dim
WHERE 1 = 1
      AND f_veve.dim_qualityusersidoriginator = dim.dim_qualityusersid
      AND dim.CMGNUMBER = '1500'
      AND f_veve.snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
      AND f_veve.dd_region = 'Brazil & Uruguay';

UPDATE fact_veveyquality fct
SET fct.dd_originator = 'Anonymous'
FROM tmp_originatorgerman tmp,
     fact_veveyquality fct
WHERE fct.fact_veveyqualityid = tmp.fact_veveyqualityid;

DROP TABLE IF EXISTS tmp_originatorgerman;

DROP TABLE IF EXISTS tmp_assignedtogerman;
CREATE TABLE tmp_assignedtogerman
AS
SELECT fact_veveyqualityid
FROM fact_veveyquality f_veve,
     dim_qualityusers dim
WHERE 1 = 1
      AND f_veve.dim_qualityusersidassignedto = dim.dim_qualityusersid
      AND dim.CMGNUMBER = '1500'
      AND f_veve.snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
      AND f_veve.dd_region = 'Brazil & Uruguay';

UPDATE fact_veveyquality fct
SET fct.dd_assignedto = 'Anonymous'
FROM tmp_assignedtogerman tmp,
     fact_veveyquality fct
WHERE fct.fact_veveyqualityid = tmp.fact_veveyqualityid;

DROP TABLE IF EXISTS tmp_assignedtogerman;

DROP TABLE IF EXISTS tmp_assignedqagerman;
CREATE TABLE tmp_assignedqagerman
AS
SELECT fact_veveyqualityid
FROM fact_veveyquality f_veve,
     dim_qualityusers dim
WHERE 1 = 1
      AND f_veve.dim_qualityusersidassignedqa = dim.dim_qualityusersid
      AND dim.CMGNUMBER = '1500'
      AND f_veve.snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
      AND f_veve.dd_region = 'Brazil & Uruguay';

UPDATE fact_veveyquality fct
SET fct.dd_assignedqa = 'Anonymous'
FROM tmp_assignedqagerman tmp,
     fact_veveyquality fct
WHERE fct.fact_veveyqualityid = tmp.fact_veveyqualityid;

DROP TABLE IF EXISTS tmp_assignedqagerman;
