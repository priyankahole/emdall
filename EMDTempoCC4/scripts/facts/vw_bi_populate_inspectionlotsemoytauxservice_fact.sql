

DROP TABLE IF EXISTS tmp_sap_data_ilsets;
CREATE TABLE tmp_sap_data_ilsets
AS
SELECT
  IFNULL(enstehdat,'0001-01-01') AS dd_datelotcreated,
  IFNULL(matnr,'Not Set') AS dd_part,
  IFNULL(charg,'Not Set') AS dd_batchno,
  IFNULL(werk,'Not Set') AS dd_plant,
  IFNULL(lmengeist,0) AS ct_actuallotqty,
  IFNULL(paendterm,'Not Set') AS dd_dateinspectionend,
  IFNULL(art,'Not Set') AS dd_inspectiontype,
  IFNULL(aufnr,'Not Set') AS dd_OrderNo,
  IFNULL(ktextlos,'Not Set') AS dd_shorttext,
  IFNULL(objnr,'Not Set') AS dd_objnr,
  CAST('Not Set' AS VARCHAR(7)) AS dd_usagedecisionflag,
  CAST(1 AS BIGINT) AS dim_plantid,
  CAST(1 AS BIGINT) AS dim_partid,
  CAST(1 AS BIGINT) AS dim_mdg_partid,
  IFNULL(qmatauth,'Not Set') AS dd_materialauthorizationgroup,
  IFNULL(prueflos,'Not Set') AS dd_inspectionlotno,
  CAST('Not Set' AS VARCHAR(100)) AS dd_articlenumberfromSAP,
  CAST('Not Set' AS VARCHAR(100)) AS dd_articlebatch,
  CAST('0001-01-01' AS DATE) AS dd_batchstageredate,
  CAST(1 AS BIGINT) AS dim_batchstageredateid,
  CAST('Not Set' AS VARCHAR(100)) AS dd_wherelineisfound,
  CAST('Not Set' AS VARCHAR(100)) AS dd_vpormp,
  'PRIO' AS dd_datasource,
  CAST('Not Set' AS VARCHAR(100)) AS dd_status
FROM
  ilse_fr44_prio_lab
WHERE 
  UPPER(werk) = 'FR44'
  AND UPPER(qmatauth) IN ('PHFR0A', 'PHFR1B', 'PHFR1E')
  AND enstehdat >= '2016-01-01'
;

INSERT INTO tmp_sap_data_ilsets
(
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_materialauthorizationgroup,
  dd_inspectionlotno,
  dd_articlenumberfromSAP,
  dd_articlebatch,
  dd_batchstageredate,
  dim_batchstageredateid,
  dd_wherelineisfound,
  dd_vpormp,
  dd_datasource,
  dd_status
)
SELECT
  IFNULL(enstehdat,'0001-01-01') AS dd_datelotcreated,
  IFNULL(matnr,'Not Set') AS dd_part,
  IFNULL(charg,'Not Set') AS dd_batchno,
  IFNULL(werk,'Not Set') AS dd_plant,
  IFNULL(lmengeist,0) AS ct_actuallotqty,
  IFNULL(paendterm,'Not Set') AS dd_dateinspectionend,
  IFNULL(art,'Not Set') AS dd_inspectiontype,
  IFNULL(aufnr,'Not Set') AS dd_OrderNo,
  IFNULL(ktextlos,'Not Set') AS dd_shorttext,
  IFNULL(objnr,'Not Set') AS dd_objnr,
  CAST('Not Set' AS VARCHAR(7)) AS dd_usagedecisionflag,
  CAST(1 AS BIGINT) AS dim_plantid,
  CAST(1 AS BIGINT) AS dim_partid,
  CAST(1 AS BIGINT) AS dim_mdg_partid,
  IFNULL(qmatauth,'Not Set') AS dd_materialauthorizationgroup,
  IFNULL(prueflos,'Not Set') AS dd_inspectionlotno,
  CAST('Not Set' AS VARCHAR(100)) AS dd_articlenumberfromSAP,
  CAST('Not Set' AS VARCHAR(100)) AS dd_articlebatch,
  CAST('0001-01-01' AS DATE) AS dd_batchstageredate,
  CAST(1 AS BIGINT) AS dim_batchstageredateid,
  CAST('Not Set' AS VARCHAR(100)) AS dd_wherelineisfound,
  CAST('Not Set' AS VARCHAR(100)) AS dd_vpormp,
  'LIBE' AS dd_datasource,
  CAST('Not Set' AS VARCHAR(100)) AS dd_status
FROM
  ilse_fr44_prio_lab
WHERE 
  UPPER(werk) = 'FR44'
  AND UPPER(matnr) NOT LIKE 'FR21L%' 
  AND UPPER(matnr) NOT LIKE 'FR29G%'
  AND UPPER(matnr) NOT LIKE 'FR29S%'
  AND UPPER(matnr) NOT LIKE 'FR29F%'
  AND UPPER(matnr) NOT LIKE 'FR29L%'
  AND (UPPER(qmatauth) LIKE 'PHFR1%'
    OR UPPER(qmatauth) = 'PHFR0A')
  AND (UPPER(art) LIKE 'YPH01%' 
    OR UPPER(art) LIKE 'YPH04%G'
    OR UPPER(art) LIKE 'YPH05%'
    OR UPPER(art) LIKE 'YPH09%'
    OR UPPER(art) LIKE 'YPH03%'
    OR UPPER(art) LIKE 'YPH08%')
  AND enstehdat >= '2016-01-01'
;

/* BEGIN - take maximum date for inspection end date from SAP for the same batch */

DROP TABLE IF EXISTS tmp_getmaxdateforbatch_ilsets;
CREATE TABLE tmp_getmaxdateforbatch_ilsets 
AS
SELECT
  dd_batchno, 
  MAX(dd_dateinspectionend) AS dd_dateinspectionend
FROM tmp_sap_data_ilsets
GROUP BY dd_batchno;

UPDATE tmp_sap_data_ilsets AS t
SET t.dd_dateinspectionend = md.dd_dateinspectionend
FROM tmp_sap_data_ilsets AS t, tmp_getmaxdateforbatch_ilsets AS md
WHERE t.dd_batchno = md.dd_batchno;

/* END - take maximum date for inspection end date from SAP for the same batch */


/* BEGIN - Usage Decision Flag Update*/
UPDATE tmp_sap_data_ilsets AS f
SET dd_usagedecisionflag = 'Yes'
FROM tmp_sap_data_ilsets AS f, ilse_jest AS j
WHERE f.dd_objnr = j.objnr
  AND j.stat = 'I0218'
  AND j.inact IS NULL;
/* END - Usage Decision Flag Update*/

/* BEGIN - plant update */
UPDATE tmp_sap_data_ilsets AS f 
SET f.dim_plantid = dp.dim_plantid
FROM tmp_sap_data_ilsets AS f,
  dim_plant AS dp
WHERE f.dd_plant = dp.plantcode
  AND dp.rowiscurrent = 1;
/* END - plant update */

/* BEGIN - part update */
UPDATE tmp_sap_data_ilsets AS f
SET f.dim_partid = dp.dim_partid
FROM tmp_sap_data_ilsets AS f,
  dim_part AS dp
WHERE f.dd_part = dp.partnumber
  AND f.dd_plant = dp.plant
  AND dp.rowiscurrent = 1;
/* END - part update */

/* BEGIN - mdg part update */
DROP TABLE IF EXISTS tmp_ilsets_getmdg_partid;
CREATE TABLE tmp_ilsets_getmdg_partid 
AS
SELECT pr.dim_partid, MAX(md.dim_mdg_partid) AS dim_mdg_partid
FROM dim_mdg_part AS md, 
  dim_part AS pr
WHERE right('000000000000000000' || md.partnumber, 18) = right('000000000000000000' || pr.partnumber, 18)
GROUP BY dim_partid;

UPDATE tmp_sap_data_ilsets AS f
SET f.dim_mdg_partid = tmp.dim_mdg_partid
FROM tmp_ilsets_getmdg_partid AS tmp, 
  tmp_sap_data_ilsets AS f
WHERE f.dim_partid = tmp.dim_partid
  AND f.dim_mdg_partid <> tmp.dim_mdg_partid;
/* END - mdg part update */

/* BEGIN - articlenumberfromSAP logic update */
UPDATE tmp_sap_data_ilsets AS t
SET t.dd_articlenumberfromSAP = IFNULL(il.yyd_ygart, 'Not Set')
FROM tmp_sap_data_ilsets AS t, (SELECT DISTINCT matnr, yyd_ygart FROM ilse_families_part2) AS il
WHERE t.dd_part = il.matnr;
/* END - articlenumberfromSAP logic update */

/* BEGIN - article batch Update */
UPDATE tmp_sap_data_ilsets AS f
SET f.dd_articlebatch = 
  CASE 
    WHEN f.dd_articlenumberfromSAP = 'Not Set' OR dd_batchno = 'Not Set' THEN 'Not Set' 
    ELSE CONCAT(f.dd_articlenumberfromSAP,'-',f.dd_batchno)
  END;
/* END - article batch Update */


/* BEGIN - Batch Stagere Date Logic */

-- MP Logic
-- ADD COMMENTS HERE

DROP TABLE IF EXISTS tmp_getmultiplematerialandbatch_suividelais_ilsets;
CREATE TABLE tmp_getmultiplematerialandbatch_suividelais_ilsets
AS 
SELECT DISTINCT 
  s.materialname AS dd_materialname, 
  s.textvalue AS dd_sapbatchno
FROM v_mcockpit_suividelais AS s, v_mcockpit_suividelais AS s1
WHERE s.materialname = s1.materialname
  AND s.textvalue = s1.textvalue
  AND s.batchstage_re <> s1.batchstage_re;

UPDATE tmp_sap_data_ilsets AS f
SET f.dd_wherelineisfound = 'Suividelais',
  f.dd_batchstageredate = LEFT(d.batchstage_re, 10),
  f.dd_vpormp = 'MP'
FROM 
  tmp_sap_data_ilsets AS f, 
  (SELECT DISTINCT materialname, textvalue, batchstage_re, u_materialfamily
  FROM v_mcockpit_suividelais
  WHERE batchstage_re IS NOT NULL
    AND u_materialfamily = 'MP') AS d
WHERE f.dd_part = d.materialname
  AND f.dd_batchno = d.textvalue
  AND (f.dd_part, f.dd_batchno) NOT IN 
      (SELECT g.dd_materialname, g.dd_sapbatchno FROM tmp_getmultiplematerialandbatch_suividelais_ilsets AS g);

-- VP Logic
-- ADD COMMENTS HERE

DROP TABLE IF EXISTS tmp_v_mcockpit_suividelais_with_articlebatch_ilsets;
CREATE TABLE tmp_v_mcockpit_suividelais_with_articlebatch_ilsets
AS 
SELECT DISTINCT
  s.materialname AS dd_materialname, 
  s.textvalue AS dd_sapbatchno,
  s.batchstage_re AS dd_batchstageredate,
  IFNULL(CONCAT(i.yyd_ygart,'-',s.textvalue),'Not Set') AS dd_articlebatch
FROM v_mcockpit_suividelais AS s, ilse_families_part2 AS i
WHERE s.materialname = i.matnr
  AND (s.materialname, s.textvalue) NOT IN
      (SELECT g.dd_materialname, g.dd_sapbatchno FROM tmp_getmultiplematerialandbatch_suividelais_ilsets AS g)
  AND s.batchstage_re IS NOT NULL;

-- get the maximum date where we have more dates for VP

DROP TABLE IF EXISTS tmp_getmaxdatedemandsupplydate_ilsets;
CREATE TABLE tmp_getmaxdatedemandsupplydate_ilsets
AS
SELECT 
  dd_articlebatch, 
  MAX(dd_batchstageredate) AS dd_batchstageredate
FROM
  tmp_v_mcockpit_suividelais_with_articlebatch_ilsets
GROUP BY dd_articlebatch;

UPDATE tmp_sap_data_ilsets AS f
SET f.dd_wherelineisfound = 'Suividelais',
  f.dd_batchstageredate = LEFT(d.dd_batchstageredate, 10),
  f.dd_vpormp = 'VP'
FROM 
  tmp_sap_data_ilsets AS f, 
  tmp_getmaxdatedemandsupplydate_ilsets as d
WHERE f.dd_articlebatch = d.dd_articlebatch
  AND f.dd_vpormp <> 'MP';

UPDATE tmp_sap_data_ilsets AS f
SET f.dim_batchstageredateid = IFNULL(dd.dim_dateid, 1)
FROM tmp_sap_data_ilsets AS f, dim_date AS dd 
WHERE dd_batchstageredate = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_batchstageredateid <> dd.dim_dateid;

/* END - Batch Stagere Date Logic */


/* BEGIN - logic for waiting list */

DROP TABLE IF EXISTS tmp_getwaitinglist_ilsets;
CREATE TABLE tmp_getwaitinglist_ilsets
AS
SELECT DISTINCT
  dd_batchno,
  CAST('Not Set' AS VARCHAR(100)) AS dd_status
FROM
  tmp_sap_data_ilsets
WHERE dd_vpormp = 'VP';

UPDATE tmp_getwaitinglist_ilsets AS t
SET dd_status = 'NOT IN WAITING LIST'
FROM 
  tmp_getwaitinglist_ilsets AS t, 
  (SELECT DISTINCT dd_batchno
   FROM tmp_sap_data_ilsets
   WHERE LEFT(dd_inspectionlotno,1) = '4'
     AND dd_vpormp = 'VP') AS f
WHERE t.dd_batchno = f.dd_batchno;

UPDATE tmp_sap_data_ilsets AS f
SET 
 dd_dateinspectionend = '0001-01-01',
 dd_status = 'IN WAITING LIST'
FROM tmp_sap_data_ilsets AS f, tmp_getwaitinglist_ilsets AS t
WHERE f.dd_batchno = t.dd_batchno
  AND f.dd_vpormp = 'VP'
  AND t.dd_status <> 'NOT IN WAITING LIST';

UPDATE tmp_sap_data_ilsets AS f
SET 
 dd_status = 'WITH INSPECTION END DATE'
WHERE
 dd_status <> 'IN WAITING LIST';


/* END - logic for waiting list */


DROP TABLE IF EXISTS number_fountain_fact_inspectionlotsemoytauxservice;
CREATE TABLE number_fountain_fact_inspectionlotsemoytauxservice 
  LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoytauxservice;
CREATE TABLE tmp_fact_inspectionlotsemoytauxservice 
  LIKE fact_inspectionlotsemoytauxservice INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_inspectionlotsemoytauxservice 
WHERE table_name = 'fact_inspectionlotsemoytauxservice';	

INSERT INTO number_fountain_fact_inspectionlotsemoytauxservice
SELECT 'fact_inspectionlotsemoytauxservice', IFNULL(MAX(f.fact_inspectionlotsemoytauxserviceid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM tmp_fact_inspectionlotsemoytauxservice AS f;

INSERT INTO tmp_fact_inspectionlotsemoytauxservice(
  fact_inspectionlotsemoytauxserviceid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_datelotcreated, -- QALS ENSTEHDAT
  dd_part, -- QALS MATNR
  dd_batchno,  -- QALS CHARG
  dd_plant, -- QALS WERK
  ct_actuallotqty, -- QALS LMENGEIST
  dd_dateinspectionend, -- QALS PAENDTERM
  dd_inspectiontype, -- QALS ART
  dd_OrderNo, -- QALS AUFNR
  dd_shorttext, -- QALS KTEXTLOS
  dd_objnr, -- QALS OBJNR
  dd_usagedecisionflag,  -- JEST 
  dim_plantid,
  dim_partid,
  dim_mdg_partid, 
  dd_materialauthorizationgroup, -- QALS QMATAUTH
  dd_inspectionlotno,  -- QALS PRUEFLOS
  dd_articlenumberfromSAP,  -- MARA YYD_YGART
  dd_articlebatch,
  dd_batchstageredate,
  dim_batchstageredateid,
  dd_wherelineisfound,
  dd_vpormp,
  dim_lotcreateddateid,
  dim_inspectionenddateid,
  dd_releasedcolournameflag,
  dd_releasedcolournumberflag,
  dd_status,
  ct_linecountdistinctbatch
)
SELECT
  (SELECT max_id 
    FROM number_fountain_fact_inspectionlotsemoytauxservice
    WHERE table_name = 'fact_inspectionlotsemoytauxservice') 
      + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoytauxserviceid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS dw_insert_date, 
  current_timestamp AS dw_update_date,
  dd_datelotcreated, -- QALS ENSTEHDAT
  dd_part, -- QALS MATNR
  dd_batchno,  -- QALS CHARG
  dd_plant, -- QALS WERK
  ct_actuallotqty, -- QALS LMENGEIST
  dd_dateinspectionend, -- QALS PAENDTERM
  dd_inspectiontype, -- QALS ART
  dd_OrderNo, -- QALS AUFNR
  dd_shorttext, -- QALS KTEXTLOS
  dd_objnr, -- QALS OBJNR
  dd_usagedecisionflag,  -- JEST 
  dim_plantid,
  dim_partid,
  dim_mdg_partid, 
  dd_materialauthorizationgroup, -- QALS QMATAUTH
  dd_inspectionlotno,  -- QALS PRUEFLOS
  dd_articlenumberfromSAP,  -- MARA YYD_YGART
  dd_articlebatch,
  dd_batchstageredate,
  dim_batchstageredateid,
  dd_wherelineisfound,
  dd_vpormp,
  CAST(1 AS BIGINT) AS dim_lotcreateddateid,
  CAST(1 AS BIGINT) AS dim_inspectionenddateid,
  CAST('Not Set' AS VARCHAR(100)) AS dd_releasedcolournameflag,
  CAST('Not Set' AS VARCHAR(100)) AS dd_releasedcolournumberflag,
  dd_status,
  0 AS ct_linecountdistinctbatch
FROM
  tmp_sap_data_ilsets
;

UPDATE tmp_fact_inspectionlotsemoytauxservice AS f
SET f.dim_lotcreateddateid = IFNULL(dd.dim_dateid, 1)
FROM  tmp_fact_inspectionlotsemoytauxservice AS f, dim_date AS dd 
WHERE dd_datelotcreated = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_lotcreateddateid <> dd.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoytauxservice AS f
SET f.dim_inspectionenddateid = IFNULL(dd.dim_dateid, 1)
FROM  tmp_fact_inspectionlotsemoytauxservice AS f, dim_date AS dd 
WHERE dd_dateinspectionend = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_inspectionenddateid <> dd.dim_dateid;


UPDATE tmp_fact_inspectionlotsemoytauxservice AS f
SET f.dd_releasedcolournameflag =
  CASE
    WHEN f.dd_batchstageredate <= f.dd_dateinspectionend THEN 'BLUE'
    WHEN f.dd_batchstageredate > f.dd_dateinspectionend THEN 'AMBER'
    WHEN f.dd_batchstageredate = '0001-01-01' AND f.dd_dateinspectionend < CURRENT_DATE THEN 'RED'
    ELSE 'Not Set'
  END
WHERE f.dd_vpormp <> 'Not Set';

UPDATE tmp_fact_inspectionlotsemoytauxservice AS f
SET f.dd_releasedcolournumberflag =
  CASE
    WHEN f.dd_releasedcolournameflag = 'BLUE' THEN '1'
    WHEN f.dd_releasedcolournameflag = 'AMBER' THEN '2'
    WHEN f.dd_releasedcolournameflag = 'RED' THEN '3'
    ELSE '0'
  END;

/* BEGIN - logic for counting once per batch */

DROP TABLE IF EXISTS tmp_getwaitinglist_ilsets;
CREATE TABLE tmp_getwaitinglist_ilsets
AS
SELECT
  dd_batchno,
  MAX(fact_inspectionlotsemoytauxserviceid) AS fact_inspectionlotsemoytauxserviceid
FROM tmp_fact_inspectionlotsemoytauxservice
GROUP BY dd_batchno;

UPDATE tmp_fact_inspectionlotsemoytauxservice AS f
SET f.ct_linecountdistinctbatch = 1
FROM tmp_fact_inspectionlotsemoytauxservice AS f, tmp_getwaitinglist_ilsets AS t
WHERE f.dd_batchno = t.dd_batchno
  AND f.fact_inspectionlotsemoytauxserviceid = t.fact_inspectionlotsemoytauxserviceid;

/* BEGIN - logic for counting once per batch */

DELETE FROM fact_inspectionlotsemoytauxservice;

INSERT INTO fact_inspectionlotsemoytauxservice(
  fact_inspectionlotsemoytauxserviceid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_datelotcreated, -- QALS ENSTEHDAT
  dd_part, -- QALS MATNR
  dd_batchno,  -- QALS CHARG
  dd_plant, -- QALS WERK
  ct_actuallotqty, -- QALS LMENGEIST
  dd_dateinspectionend, -- QALS PAENDTERM
  dd_inspectiontype, -- QALS ART
  dd_OrderNo, -- QALS AUFNR
  dd_shorttext, -- QALS KTEXTLOS
  dd_objnr, -- QALS OBJNR
  dd_usagedecisionflag,  -- JEST 
  dim_plantid,
  dim_partid,
  dim_mdg_partid, 
  dd_materialauthorizationgroup, -- QALS QMATAUTH
  dd_inspectionlotno,  -- QALS PRUEFLOS
  dd_articlenumberfromSAP,  -- MARA YYD_YGART
  dd_articlebatch,
  dd_batchstageredate,
  dim_batchstageredateid,
  dd_wherelineisfound,
  dd_vpormp,
  dim_lotcreateddateid,
  dim_inspectionenddateid,
  dd_releasedcolournameflag,
  dd_releasedcolournumberflag,
  dd_status,
  ct_linecountdistinctbatch
)
SELECT
  fact_inspectionlotsemoytauxserviceid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_datelotcreated, -- QALS ENSTEHDAT
  dd_part, -- QALS MATNR
  dd_batchno,  -- QALS CHARG
  dd_plant, -- QALS WERK
  ct_actuallotqty, -- QALS LMENGEIST
  dd_dateinspectionend, -- QALS PAENDTERM
  dd_inspectiontype, -- QALS ART
  dd_OrderNo, -- QALS AUFNR
  dd_shorttext, -- QALS KTEXTLOS
  dd_objnr, -- QALS OBJNR
  dd_usagedecisionflag,  -- JEST 
  dim_plantid,
  dim_partid,
  dim_mdg_partid, 
  dd_materialauthorizationgroup, -- QALS QMATAUTH
  dd_inspectionlotno,  -- QALS PRUEFLOS
  dd_articlenumberfromSAP,  -- MARA YYD_YGART
  dd_articlebatch,
  dd_batchstageredate,
  dim_batchstageredateid,
  dd_wherelineisfound,
  dd_vpormp,
  dim_lotcreateddateid,
  dim_inspectionenddateid,
  dd_releasedcolournameflag,
  dd_releasedcolournumberflag,
  dd_status,
  ct_linecountdistinctbatch
FROM tmp_fact_inspectionlotsemoytauxservice;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoytauxservice;

DELETE FROM emd586.fact_inspectionlotsemoytauxservice;

INSERT INTO emd586.fact_inspectionlotsemoytauxservice
SELECT *
FROM fact_inspectionlotsemoytauxservice;
