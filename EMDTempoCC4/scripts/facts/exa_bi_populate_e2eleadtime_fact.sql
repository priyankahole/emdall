/******************************************************************************************************************/
/*   Script         : exa_bi_populate_e2eleadtime_fact                                                            */
/*   Author         : Cristian Cleciu                                                                             */
/*   Created On     : 20 Sep 2017                                                                                 */
/*   Description    : Populating script of fact_e2eleadtime                                                       */
/*********************************************Change History*******************************************************/
/*   Date                By             		Version      Desc                                                 */
/*   20 Sep 2017         Cristian Cleciu        1.0          Creating the script.                                 */
/******************************************************************************************************************/


/* create inspection lot table with relevant lead time fields from QALS*/

drop table if exists tmp_fact_e2eleadtime_qals;

create table tmp_fact_e2eleadtime_qals as
select distinct 
dd_inspectionlotno AS QALS_PRUEFLOS, 
dd_batchno AS QALS_CHARG,
dim_partid,
CONVERT(VARCHAR(18),'Not Set') AS QALS_MATNR,
dim_plantid,
CONVERT(VARCHAR(7),'Not Set') AS QALS_WERK, 
ifnull(dd_documentno, 'Not Set') AS QALS_EBELN, 
IFNULL(dd_documentitemno,'Not Set') AS qals_ebelp,
dim_dateidlotcreated,
to_date('00010101','YYYYMMDD') AS QALS_ENSTEHDAT,
dim_dateidinspectionend,
to_date('00010101','YYYYMMDD') AS QALS_PAENDTERM, 
dim_dateidinspectionstart,
to_date('00010101','YYYYMMDD') AS QALS_PASTRTERM, 
dim_inspectionlotstatusid,
dim_inspusagedecisionid,
dim_dateidUsageDecisionMade
from fact_inspectionlot q;

--remove canceled inspection lots
delete from tmp_fact_e2eleadtime_qals
where dim_inspectionlotstatusid in (select dim_inspectionlotstatusid from dim_inspectionlotstatus where LotCancelled = 'X');

UPDATE tmp_fact_e2eleadtime_qals q
SET q.QALS_MATNR = dpa.PartNumber
FROM tmp_fact_e2eleadtime_qals q, dim_part dpa
WHERE dpa.dim_partid = q.dim_partid
AND q.QALS_MATNR <> dpa.PartNumber;

UPDATE tmp_fact_e2eleadtime_qals q
SET q.QALS_WERK = dp.plantcode
FROM tmp_fact_e2eleadtime_qals q, dim_plant dp
WHERE dp.dim_plantid = q.dim_plantid
AND q.QALS_WERK <> dp.plantcode;

UPDATE tmp_fact_e2eleadtime_qals q
SET q.QALS_ENSTEHDAT = dd.datevalue
FROM tmp_fact_e2eleadtime_qals q, dim_date dd
WHERE dd.dim_dateid = q.dim_dateidlotcreated
AND q.QALS_ENSTEHDAT <> dd.datevalue;

UPDATE tmp_fact_e2eleadtime_qals q
SET q.QALS_PAENDTERM = dd.datevalue
FROM tmp_fact_e2eleadtime_qals q, dim_date dd
WHERE dd.dim_dateid = q.dim_dateidinspectionend
AND q.QALS_PAENDTERM <> dd.datevalue;

UPDATE tmp_fact_e2eleadtime_qals q
SET q.QALS_PASTRTERM = dd.datevalue
FROM tmp_fact_e2eleadtime_qals q, dim_date dd
WHERE dd.dim_dateid = q.dim_dateidinspectionstart
AND q.QALS_PASTRTERM <> dd.datevalue;


--create main temp table as base from Material Movement
DROP TABLE IF EXISTS tmp_fact_e2eleadtime_part1;
CREATE TABLE tmp_fact_e2eleadtime_part1 as 
SELECT 
dim_plantid,
CONVERT(VARCHAR(7),'Not Set') AS mseg_werks,
dim_partid,
CONVERT(VARCHAR(18),'Not Set') AS mseg_matnr,
dd_BatchNumber AS mseg_charg,
Dim_Vendorid,
MIN(dd_DocumentNo) as mseg_ebeln,
MIN(dd_DocumentItemNo) AS mseg_ebelp,
CONVERT(VARCHAR(50),'Not Set') as EBAN_BANFN,
CONVERT(VARCHAR(10),'Not Set') AS EBAN_BNFPO,
CONVERT(VARCHAR(12),'Not Set') AS qals_prueflos,
to_date('00010101','YYYYMMDD') AS EBAN_BADAT, -- PR Creation date
to_date('00010101','YYYYMMDD') AS EKKO_AEDAT, -- PO creation date
to_date('00010101','YYYYMMDD') AS NAST_DATVR, -- PO send date
to_date('00010101','YYYYMMDD') AS EKES_ERDAT, -- PO send date
to_date('00010101','YYYYMMDD') AS mkpf_cpudt, -- good receipt date
to_date('00010101','YYYYMMDD') AS qave_vdatum, -- usage decision date
to_date('00010101','YYYYMMDD') AS MKPF_BUDAT_first, -- first 261 date
to_date('00010101','YYYYMMDD') AS lab_reception_date,
to_date('00010101','YYYYMMDD') AS analysis_end_date,
to_date('00010101','YYYYMMDD') AS verification_end_date,
CONVERT(VARCHAR(10),'Not Set') AS qave_vcode
FROM fact_materialmovement mm, Dim_MovementType mt
WHERE
mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
AND dd_BatchNumber IS NOT NULL
AND dim_partid <> 1
AND dd_DocumentNo IS NOT NULL
AND mt.MovementType NOT IN ('351', '352')
GROUP BY
dim_plantid,
dim_partid,
dd_BatchNumber,
Dim_Vendorid;

UPDATE tmp_fact_e2eleadtime_part1 t1
SET mseg_werks = plantcode
FROM tmp_fact_e2eleadtime_part1 t1, dim_plant dp
where t1.dim_plantid = dp.dim_plantid;

UPDATE tmp_fact_e2eleadtime_part1 t1
SET mseg_matnr = partnumber
FROM tmp_fact_e2eleadtime_part1 t1, dim_part dp
where t1.dim_partid = dp.dim_partid;

--find GR Date as first Posting Date for a 101 
DROP TABLE IF EXISTS tmp_fact_e2eleadtime_minGRdate;
CREATE TABLE tmp_fact_e2eleadtime_minGRdate as 
SELECT 
dim_plantid,
dim_partid,
dd_BatchNumber AS mseg_charg,
MIN(dd.datevalue) as mkpf_cpudt
FROM fact_materialmovement mm, dim_date dd, Dim_MovementType mt
WHERE 
mm.dim_DateIDPostingDate = dd.dim_dateid
AND mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
AND mt.MovementType  ='101'
GROUP BY 
dim_plantid,
dim_partid,
dd_BatchNumber;

UPDATE tmp_fact_e2eleadtime_part1 t1
SET t1.mkpf_cpudt = IFNULL(m.mkpf_cpudt,TO_DATE('00010101','YYYYMMDD'))
FROM tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_minGRdate m
WHERE t1.dim_plantid = m.dim_plantid
AND t1.dim_partid = m.dim_partid
AND t1.mseg_charg = m.mseg_charg
AND t1.mkpf_cpudt <> IFNULL(m.mkpf_cpudt,TO_DATE('00010101','YYYYMMDD'));

-- find maximum Inspection Lot
DROP TABLE IF EXISTS tmp_fact_e2eleadtime_maxinspectionlot;
CREATE TABLE tmp_fact_e2eleadtime_maxinspectionlot as
SELECT 
dim_plantid,
dim_partid,
qals_charg,
MAX(qals_prueflos) as qals_prueflos
FROM tmp_fact_e2eleadtime_qals
GROUP BY
dim_plantid,
dim_partid,
qals_charg;

UPDATE tmp_fact_e2eleadtime_part1 t1
SET t1.qals_prueflos = IFNULL(i.qals_prueflos, 'Not Set')
FROM tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_maxinspectionlot i
WHERE 
t1.dim_plantid = i.dim_plantid
AND t1.dim_partid = i.dim_partid
AND t1.mseg_charg = i.qals_charg
AND t1.qals_prueflos <> IFNULL(i.qals_prueflos, 'Not Set');

UPDATE tmp_fact_e2eleadtime_part1 t1
SET t1.qave_vcode = IFNULL(ct.code,'Not Set')
FROM tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_qals i, dim_CodeText ct
WHERE 
t1.dim_plantid = i.dim_plantid
AND t1.dim_partid = i.dim_partid
AND t1.mseg_charg = i.qals_charg
AND t1.qals_prueflos = i.qals_prueflos
AND i.dim_inspusagedecisionid = ct.dim_CodeTextid
AND t1.qave_vcode <> IFNULL(ct.code,'Not Set');

update tmp_fact_e2eleadtime_part1 t1
set t1.qave_vdatum = IFNULL(dd.datevalue,TO_DATE('00010101','YYYYMMDD'))
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_qals i, dim_date dd
where 
i.dim_dateidUsageDecisionMade = dd.dim_dateid 
and t1.dim_plantid = i.dim_plantid
AND t1.dim_partid = i.dim_partid
AND t1.mseg_charg = i.qals_charg
AND t1.qals_prueflos = i.qals_prueflos
and t1.qave_vcode in ('A0','A1','A2','R3','R7','R8')
and t1.qave_vdatum <> IFNULL(dd.datevalue,TO_DATE('00010101','YYYYMMDD'));


-- bring PO PR information from Purchasing
DROP TABLE IF EXISTS tmp_fact_e2eleadtime_POPR;

CREATE TABLE tmp_fact_e2eleadtime_POPR AS 
SELECT  
e.Dim_PlantidOrdering AS dim_plantid,
e.dim_partid,
e.dd_DocumentNo AS eban_ebeln,
e.dd_DocumentItemNo AS eban_ebelp,
MIN(e.dd_PurchaseReqNo) AS eban_banfn,
MIN(e.dd_PurchaseReqItemNo) AS eban_bnfpo,
MIN(ddc.datevalue) AS ekko_aedat,
MIN(ddr.datevalue) AS eban_badat,
MIN(dvc.datevalue) AS EKES_ERDAT
FROM fact_purchase e
JOIN tmp_fact_e2eleadtime_part1 t1 
ON e.Dim_PlantidOrdering = t1.dim_plantid
AND e.dim_partid = t1.dim_partid 
AND e.dd_DocumentNo = t1.mseg_ebeln
AND e.dd_DocumentItemNo = t1.mseg_ebelp
JOIN dim_date ddc ON e.dim_dateidcreate = ddc.dim_dateid
JOIN dim_date ddr ON e.Dim_DateIdRequistionDate = ddr.dim_dateid
JOIN dim_date dvc ON e.Dim_DateIdVendorConfirmation = dvc.dim_dateid
GROUP BY
e.Dim_PlantidOrdering,
e.dim_partid,
e.dd_DocumentNo,
e.dd_DocumentItemNo;

UPDATE tmp_fact_e2eleadtime_part1 t1
SET t1.eban_banfn = ifnull(e.eban_banfn,'Not Set')
FROM tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
WHERE 
e.dim_plantid = t1.dim_plantid
AND e.dim_partid = t1.dim_partid 
AND e.eban_ebeln = t1.mseg_ebeln
AND e.eban_ebelp = t1.mseg_ebelp
AND t1.eban_banfn <> ifnull(e.eban_banfn,'Not Set');

update tmp_fact_e2eleadtime_part1 t1
set t1.eban_bnfpo = ifnull(e.eban_bnfpo,'Not Set')
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
where 
e.dim_plantid = t1.dim_plantid
AND e.dim_partid = t1.dim_partid 
AND e.eban_ebeln = t1.mseg_ebeln
AND e.eban_ebelp = t1.mseg_ebelp
AND t1.eban_bnfpo <> ifnull(e.eban_bnfpo,'Not Set');

update tmp_fact_e2eleadtime_part1 t1
set t1.ekko_aedat = ifnull(e.ekko_aedat,TO_DATE('00010101','YYYYMMDD'))
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
where 
e.dim_plantid = t1.dim_plantid
AND e.dim_partid = t1.dim_partid 
AND e.eban_ebeln = t1.mseg_ebeln
AND e.eban_ebelp = t1.mseg_ebelp
AND t1.ekko_aedat <> ifnull(e.ekko_aedat,TO_DATE('00010101','YYYYMMDD'));

update tmp_fact_e2eleadtime_part1 t1
set t1.eban_badat = ifnull(e.eban_badat,TO_DATE('00010101','YYYYMMDD'))
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
where 
e.dim_plantid = t1.dim_plantid
AND e.dim_partid = t1.dim_partid 
AND e.eban_ebeln = t1.mseg_ebeln
AND e.eban_ebelp = t1.mseg_ebelp
AND t1.eban_badat <> ifnull(e.eban_badat,TO_DATE('00010101','YYYYMMDD'));

update tmp_fact_e2eleadtime_part1 t1
set t1.EKES_ERDAT = ifnull(e.EKES_ERDAT,TO_DATE('00010101','YYYYMMDD'))
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_POPR e
where 
e.dim_plantid = t1.dim_plantid
AND e.dim_partid = t1.dim_partid 
AND e.eban_ebeln = t1.mseg_ebeln
AND e.eban_ebelp = t1.mseg_ebelp
AND t1.EKES_ERDAT <> ifnull(e.EKES_ERDAT,TO_DATE('00010101','YYYYMMDD'));

-- find first PO Send Date
drop table if exists tmp_fact_e2eleadtime_minPOSendDate;

create table tmp_fact_e2eleadtime_minPOSendDate AS
select  NAST_OBJKY, 
MIN(NAST_DATVR) AS NAST_DATVR, 
MIN(NAST_UHRVR) AS NAST_UHRVR
from EKKO_NAST_E2E e
join tmp_fact_e2eleadtime_POPR p on p.eban_ebeln = e.NAST_OBJKY
group by NAST_OBJKY;

update tmp_fact_e2eleadtime_part1 t1
set t1.NAST_DATVR = TO_DATE(ifnull(e.NAST_DATVR,'00010101'),'YYYYMMDD')
from tmp_fact_e2eleadtime_part1 t1, tmp_fact_e2eleadtime_minPOSendDate e
where 
e.NAST_OBJKY = t1.mseg_ebeln
and t1.NAST_DATVR <> TO_DATE(ifnull(e.NAST_DATVR,'00010101'),'YYYYMMDD');

-- if PO Send Date is null then PO Send Date = PO Creation Date
update tmp_fact_e2eleadtime_part1 t1
set t1.NAST_DATVR = t1.EKKO_AEDAT
from tmp_fact_e2eleadtime_part1 t1
where 
t1.NAST_DATVR = TO_DATE('00010101','YYYYMMDD')
and t1.NAST_DATVR <> t1.EKKO_AEDAT;

-- first 261 date

DROP TABLE IF EXISTS tmp_fact_e2eleadtime_min261;

CREATE TABLE tmp_fact_e2eleadtime_min261 AS
SELECT 
	dim_plantid,
	dim_partid,
	dd_BatchNumber AS mseg_charg,
	Min(dd.datevalue) AS MKPF_BUDAT
FROM fact_materialmovement mm, dim_date dd, Dim_MovementType mt
WHERE 
mm.dim_DateIDPostingDate = dd.dim_dateid
AND mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
AND mt.MovementType = '261'
GROUP BY 
	dim_plantid,
	dim_partid,
	dd_BatchNumber;

UPDATE tmp_fact_e2eleadtime_part1 T1
	SET T1.MKPF_BUDAT_first = IFNULL(m.MKPF_BUDAT,TO_DATE('00010101','YYYYMMDD'))
FROM tmp_fact_e2eleadtime_part1 T1, tmp_fact_e2eleadtime_min261 m
WHERE t1.dim_plantid = m.dim_plantid
AND t1.dim_partid = m.dim_partid
AND t1.mseg_charg = m.mseg_charg
AND T1.MKPF_BUDAT_first <> IFNULL(m.MKPF_BUDAT,TO_DATE('00010101','YYYYMMDD'));

--update from LIMS data
UPDATE tmp_fact_e2eleadtime_part1 T1
SET T1.lab_reception_date = IFNULL(L.lab_reception_date,TO_DATE('00010101','YYYYMMDD'))
from aub_insplot_qc_dates L, tmp_fact_e2eleadtime_part1 T1
WHERE mseg_matnr = material_code 
and mseg_charg = batch_number
and qals_prueflos = ltrim(lot_number,0)
AND T1.lab_reception_date <> IFNULL(L.lab_reception_date,TO_DATE('00010101','YYYYMMDD'));

UPDATE tmp_fact_e2eleadtime_part1 T1
SET T1.analysis_end_date = IFNULL(L.analysis_end_date,TO_DATE('00010101','YYYYMMDD'))
from aub_insplot_qc_dates L, tmp_fact_e2eleadtime_part1 T1
WHERE mseg_matnr = material_code 
and mseg_charg = batch_number
and qals_prueflos = ltrim(lot_number,0)
AND T1.analysis_end_date <> IFNULL(L.analysis_end_date,TO_DATE('00010101','YYYYMMDD'));

UPDATE tmp_fact_e2eleadtime_part1 T1
SET T1.verification_end_date = IFNULL(L.verification_end_date,TO_DATE('00010101','YYYYMMDD'))
from aub_insplot_qc_dates L, tmp_fact_e2eleadtime_part1 T1
WHERE mseg_matnr = material_code 
and mseg_charg = batch_number
and qals_prueflos = ltrim(lot_number,0)
AND T1.verification_end_date <> IFNULL(L.verification_end_date,TO_DATE('00010101','YYYYMMDD'));

/* main temp table for integrating all sources*/

DROP TABLE IF EXISTS tmp_fact_e2eleadtime;
CREATE TABLE tmp_fact_e2eleadtime AS 	
SELECT  
	CONVERT(BIGINT,1) AS Dim_Plantid,
	t1.MSEG_WERKS AS MSEG_WERKS, -- dim_plant
    CONVERT(BIGINT,1) AS dim_partid,
	t1.MSEG_MATNR AS MSEG_MATNR,	-- dim_part
    ifnull(CONVERT(VARCHAR(50), t1.EBAN_BANFN),'Not Set')  AS dd_PurchaseReqNo,
    ifnull(CONVERT(VARCHAR(10), t1.EBAN_BNFPO),'Not Set')  AS dd_PurchaseReqItemNo,
    CONVERT(BIGINT, 1) AS dim_dateidPRCreation,
	t1.EBAN_BADAT AS EBAN_BADAT, -- dim_dateidPRCreation
    ifnull(CONVERT(VARCHAR(10), t1.MSEG_EBELN),'Not Set')  AS dd_PurchaseOrderNo,
    ifnull(CONVERT(VARCHAR(18), t1.MSEG_EBELP),'Not Set')  AS dd_PurchaseOrderItemNo,
    CONVERT (BIGINT, 1) AS dim_dateidPOCreation,
	t1.EKKO_AEDAT AS EKKO_AEDAT, -- dim_dateidPOCreation
    CONVERT(BIGINT, 1) AS dim_dateidProcessing,
	t1.NAST_DATVR AS NAST_DATVR, -- dim_dateidProcessing
    CONVERT(BIGINT, 1) AS dim_dateidConfReceived,
	t1.EKES_ERDAT AS EKES_ERDAT, -- dim_dateidConfReceived
    CONVERT(VARCHAR(10),'Not Set')  AS dd_MaterialDocNo,
    CONVERT(VARCHAR(18),'Not Set')  AS dd_MaterialDocItemNo,
    ifnull(CONVERT(VARCHAR(10), t1.MSEG_CHARG),'Not Set')  AS dd_BatchNumber,
	CONVERT(VARCHAR(12), 'Not Set') AS dd_OrderNumber,
    CONVERT(BIGINT, 1) AS dim_dateidDocEntry,
	t1.MKPF_CPUDT AS MKPF_CPUDT, -- dim_dateidDocEntry GR Date
    CONVERT(VARCHAR(10),'Not Set')  AS dd_TransferReqNo,
    CONVERT(VARCHAR(10),'Not Set')  AS dd_TrackingNo,
    CONVERT(VARCHAR(10),'Not Set')  AS dd_TransferOrderNo,
    CONVERT(VARCHAR(18),'Not Set')  AS dd_TransferOrderItem,
    CONVERT(BIGINT, 1) AS dim_dateidTOCreated,
	TO_DATE('00010101','YYYYMMDD') AS LTAK_BDATU, -- dim_dateidTOCreated
    CONVERT(BIGINT, 1) AS dim_dateidconfirmation,
	TO_DATE('00010101','YYYYMMDD') AS LTAP_QDATU, -- dim_dateidconfirmation
    ifnull(CONVERT(VARCHAR(12), t1.QALS_PRUEFLOS),'Not Set')  AS dd_inspectionlotno,
    CONVERT(BIGINT, 1) AS dim_dateidlotcreated,
	TO_DATE('00010101','YYYYMMDD') AS QALS_ENSTEHDAT, -- dim_dateidlotcreated
    TO_DATE('00010101','YYYYMMDD') AS QALS_PAENDTERM, -- Inspection lot end date
	CONVERT(BIGINT, 1) AS Dim_DateIdInspectionLotEnd,
	TO_DATE('00010101','YYYYMMDD') AS QALS_PASTRTERM, -- inspection lot start date
	CONVERT(BIGINT, 1) AS Dim_DateIdInspectionLotStart,
	CONVERT(BIGINT, 0) AS ct_StandardQCLT,
	CONVERT(BIGINT, 0) AS ct_StandardQCQALTv2,	
    CONVERT(VARCHAR(18),'Not Set')  AS dd_SampleDrawinNo,
    CONVERT(BIGINT, 1) AS dim_dateidsdopsrelease,	
    TO_DATE('00010101','YYYYMMDD') AS QPRN_FRGDT, -- dim_dateidsdopsrelease
    CONVERT(BIGINT, 1) AS Dim_DateIdUsageDecision,
	t1.qave_vdatum AS QAVE_VDATUM, -- Dim_DateIdUsageDecision 	
    CONVERT(BIGINT, 1) AS dim_dateidpostingdate,
    TO_DATE('00010101','YYYYMMDD') AS MKPF_BUDAT, --dim_dateidpostingdate
    CONVERT(BIGINT, 1) AS dim_dateidfirst261,
    T1.MKPF_BUDAT_first AS MKPF_BUDAT_first, --dim_dateidfirst261
    CONVERT(BIGINT, 1) AS amt_exhangerate,
    CONVERT(BIGINT, 1) AS amt_exchangerate_gbl,
    CONVERT(BIGINT, 1) AS dim_currencyid,
    CONVERT(BIGINT, 1) AS dim_currencyid_tra,
    CONVERT(BIGINT, 1) AS dim_currencyid_gbl,
    current_timestamp 	as DW_UPDATE_DATE,
    current_timestamp 	as DW_INSERT_DATE,
    CONVERT(BIGINT, 1) AS DIM_PROJECTSOURCEID,
	CONVERT(BIGINT, 0) AS ct_prCreationlt,	
	CONVERT(BIGINT, 0) as ct_poCreationlt,
	CONVERT(BIGINT, 0) AS ct_ptconfirmationlt,
	CONVERT(BIGINT, 0) as ct_supplierlt,
	CONVERT(BIGINT, 0) AS ct_tocreationlt,
	CONVERT(BIGINT, 0) AS ct_stockPlacementlt,
	CONVERT(BIGINT, 0) as ct_sampleDrawinglt,
	CONVERT(BIGINT, 0) as ct_qcqalt,	
	CONVERT(BIGINT, 0)	as ct_bachfirst,
	TO_DATE('00010101','YYYYMMDD') AS CAUFV_ERDAT,-- Process Order Creation Date
	CONVERT(BIGINT, 1) AS Dim_DateIdProcessOrderCreation,
	TO_DATE('00010101','YYYYMMDD') AS CAUFV_FTRMI,-- Process Order Release Date
	CONVERT(BIGINT, 1) AS Dim_DateIdProcessOrderRelease,
	TO_DATE('00010101','YYYYMMDD') AS CAUFV_GLTRI, -- Process order finish date
	CONVERT(BIGINT, 1) AS Dim_DateIdProcessOrderFinish,
	CONVERT(BIGINT, 0)	AS ct_PROPreparationLT,
	CONVERT(BIGINT, 0)	AS ct_MaterialStaggingLT,
	CONVERT(BIGINT, 0) AS ct_ProductionLT,
	CONVERT(BIGINT, 0) AS ct_PlannedDeliveryLT,
	CONVERT(BIGINT, 0)	AS ct_GoodRecepitProcessingLT,
	CONVERT(BIGINT, 1) AS dim_materialcodingschemeID,
	CONVERT(BIGINT, 0) AS dd_releaseperiod,
	CONVERT(VARCHAR(7), 'Not Set') AS MSEG_KZVBR, 
	CONVERT(VARCHAR(7), 'Not Set') AS MSEG_KZBEW, 
	CONVERT(VARCHAR(7), 'Not Set') AS MSEG_KZZUG, 
	CONVERT(VARCHAR(7), 'Not Set') AS MSEG_SOBKZ,
	CONVERT(BIGINT, 1) AS dim_inspectionlotstatusid,
	t1.Dim_vendorid,
	t1.lab_reception_date 		AS lab_reception_date,
    t1.analysis_end_date  		AS analysis_end_date,
    t1.verification_end_date	AS verification_end_date,
    CONVERT(BIGINT, 1) AS dim_dateid_lab_reception,
    CONVERT(BIGINT, 1) AS dim_dateid_analysis_end,
    CONVERT(BIGINT, 1) AS dim_dateid_verification_end,
    CONVERT(VARCHAR(100), 'Not Set') AS dd_SOPFamily,
	CONVERT(VARCHAR(100), 'Not Set') AS dd_SubFamily,
	t1.qave_vcode AS dd_UDCode,
	CONVERT(VARCHAR(8), 'N') AS dd_WorkCenter
FROM tmp_fact_e2eleadtime_part1 t1;
	
/* calculating Lead Time Measures*/

--ct_supplierlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_supplierlt = MKPF_CPUDT-EKES_ERDAT
FROM tmp_fact_e2eleadtime
WHERE (MKPF_CPUDT <> TO_DATE('00010101','YYYYMMDD') AND EKES_ERDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_supplierlt <> MKPF_CPUDT-EKES_ERDAT;

--ct_tocreationlt	
UPDATE tmp_fact_e2eleadtime 
	SET ct_tocreationlt = LTAK_BDATU-MKPF_CPUDT
FROM tmp_fact_e2eleadtime
WHERE (LTAK_BDATU <> TO_DATE('00010101','YYYYMMDD') AND MKPF_CPUDT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_tocreationlt <> LTAK_BDATU-MKPF_CPUDT;

--ct_GoodRecepitProcessingLT
UPDATE tmp_fact_e2eleadtime 
	SET ct_GoodRecepitProcessingLT = QAVE_VDATUM - MKPF_CPUDT
FROM tmp_fact_e2eleadtime
WHERE (QAVE_VDATUM <> TO_DATE('00010101','YYYYMMDD') AND MKPF_CPUDT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_GoodRecepitProcessingLT <> QAVE_VDATUM - MKPF_CPUDT;

-- ct_bachfirst
UPDATE tmp_fact_e2eleadtime 
	SET ct_bachfirst = MKPF_BUDAT_first-QAVE_VDATUM
FROM tmp_fact_e2eleadtime
WHERE (MKPF_BUDAT_first <> TO_DATE('00010101','YYYYMMDD') AND QAVE_VDATUM <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_bachfirst <> MKPF_BUDAT_first-QAVE_VDATUM;
	
--	ct_MaterialStaggingLT,	
UPDATE tmp_fact_e2eleadtime 
	SET ct_MaterialStaggingLT = MKPF_BUDAT_first-CAUFV_FTRMI
FROM tmp_fact_e2eleadtime
WHERE (CAUFV_FTRMI <> TO_DATE('00010101','YYYYMMDD') AND MKPF_BUDAT_first <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_MaterialStaggingLT <> MKPF_BUDAT_first-CAUFV_FTRMI;

--	ct_ProductionLT,
UPDATE tmp_fact_e2eleadtime 
	SET ct_ProductionLT = CAUFV_GLTRI - MKPF_BUDAT_first
FROM tmp_fact_e2eleadtime
WHERE (CAUFV_GLTRI <> TO_DATE('00010101','YYYYMMDD') AND MKPF_BUDAT_first <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_ProductionLT <> CAUFV_GLTRI - MKPF_BUDAT_first;

--ct_PlannedDeliveryLT
UPDATE tmp_fact_e2eleadtime 
	SET ct_PlannedDeliveryLT = MKPF_CPUDT - NAST_DATVR
FROM tmp_fact_e2eleadtime
WHERE (NAST_DATVR <> TO_DATE('00010101','YYYYMMDD') AND MKPF_CPUDT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_PlannedDeliveryLT <> MKPF_CPUDT - NAST_DATVR;

--ct_poCreationlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_poCreationlt = NAST_DATVR - EKKO_AEDAT
FROM tmp_fact_e2eleadtime
WHERE (NAST_DATVR <> TO_DATE('00010101','YYYYMMDD') AND EKKO_AEDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_poCreationlt <> NAST_DATVR - EKKO_AEDAT;

--ct_ptconfirmationlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_ptconfirmationlt = EKES_ERDAT - NAST_DATVR
FROM tmp_fact_e2eleadtime
WHERE (NAST_DATVR <> TO_DATE('00010101','YYYYMMDD') AND EKES_ERDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_ptconfirmationlt <> EKES_ERDAT - NAST_DATVR;

--ct_StandardQCLT
UPDATE tmp_fact_e2eleadtime 
	SET ct_StandardQCLT = QALS_PAENDTERM - QALS_PASTRTERM
FROM tmp_fact_e2eleadtime
WHERE (QALS_PAENDTERM <> TO_DATE('00010101','YYYYMMDD') AND QALS_PASTRTERM <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_StandardQCLT <> QALS_PAENDTERM - QALS_PASTRTERM;

--ct_StandardQCQALTv2
UPDATE tmp_fact_e2eleadtime 
	SET ct_StandardQCQALTv2 = QAVE_VDATUM - QALS_PAENDTERM
FROM tmp_fact_e2eleadtime
WHERE (QAVE_VDATUM <> TO_DATE('00010101','YYYYMMDD') AND QALS_PAENDTERM <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_StandardQCQALTv2 <> QAVE_VDATUM - QALS_PAENDTERM;

--ct_prCreationlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_prCreationlt = EKKO_AEDAT-EBAN_BADAT
FROM tmp_fact_e2eleadtime
WHERE (EKKO_AEDAT <> TO_DATE('00010101','YYYYMMDD') AND EBAN_BADAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_prCreationlt <> EKKO_AEDAT-EBAN_BADAT;

--ct_stockPlacementlt
UPDATE tmp_fact_e2eleadtime 
	SET ct_stockPlacementlt = LTAP_QDATU-LTAK_BDATU
FROM tmp_fact_e2eleadtime
WHERE (LTAP_QDATU <> TO_DATE('00010101','YYYYMMDD') AND LTAK_BDATU <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_stockPlacementlt <> LTAP_QDATU-LTAK_BDATU;
	
--ct_sampleDrawinglt
UPDATE tmp_fact_e2eleadtime 
	SET ct_sampleDrawinglt = QPRN_FRGDT-QALS_ENSTEHDAT
FROM tmp_fact_e2eleadtime
WHERE (QPRN_FRGDT <> TO_DATE('00010101','YYYYMMDD') AND QALS_ENSTEHDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_sampleDrawinglt <> QPRN_FRGDT-QALS_ENSTEHDAT;
	
--ct_qcqalt
UPDATE tmp_fact_e2eleadtime 
	SET ct_qcqalt = QAVE_VDATUM-QPRN_FRGDT
FROM tmp_fact_e2eleadtime
WHERE (QAVE_VDATUM <> TO_DATE('00010101','YYYYMMDD') AND QPRN_FRGDT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_qcqalt <> QAVE_VDATUM-QPRN_FRGDT;
	
--ct_PROPreparationLT
UPDATE tmp_fact_e2eleadtime 
	SET ct_PROPreparationLT = CAUFV_FTRMI-CAUFV_ERDAT
FROM tmp_fact_e2eleadtime
WHERE (CAUFV_FTRMI <> TO_DATE('00010101','YYYYMMDD') AND CAUFV_ERDAT <> TO_DATE('00010101','YYYYMMDD'))
	AND  ct_PROPreparationLT <> CAUFV_FTRMI-CAUFV_ERDAT;
	
/* updating dimensions */

--dd_workcenter

drop table if exists tmp_fact_e2eleadtime_limsflag;

create table tmp_fact_e2eleadtime_limsflag as
select distinct qals_prueflos,'LIMS' as workcenter  from QALS_PLPO_CRHD
where crhd_arbpl like '%LIMS'
order by qals_prueflos asc;

UPDATE tmp_fact_e2eleadtime f
SET f.dd_workcenter = 'Y'
FROM tmp_fact_e2eleadtime f, tmp_fact_e2eleadtime_limsflag q
WHERE f.dd_inspectionlotno = q.QALS_PRUEFLOS
AND f.dd_workcenter <> 'Y';

--S&OP Family 
UPDATE tmp_fact_e2eleadtime f
set dd_SOPFamily = ifnull(MARC_YYD_YSOP2,'Not Set')
from  tmp_fact_e2eleadtime f, MARC_E2E m
where mseg_matnr = marc_matnr
and mseg_werks = marc_werks
and dd_SOPFamily <> ifnull(MARC_YYD_YSOP2,'Not Set');

--SubFamily 
UPDATE tmp_fact_e2eleadtime f
set dd_SubFamily = ifnull(MARC_YYD_YSOP3,'Not Set')
from  tmp_fact_e2eleadtime f, MARC_E2E m
where mseg_matnr = marc_matnr
and mseg_werks = marc_werks
and dd_SubFamily <> ifnull(MARC_YYD_YSOP3,'Not Set');


/*dim_materialcodingscheme*/	
UPDATE tmp_fact_e2eleadtime f
SET f.dim_materialcodingschemeid = IFNULL(d.dim_materialcodingschemeid,1)
FROM  tmp_fact_e2eleadtime f, dim_materialcodingscheme d
WHERE d.pos_1 = LEFT(IFNULL(f.MSEG_MATNR,'|'),1)
AND f.dim_materialcodingschemeid <> d.dim_materialcodingschemeid;

/*dim_partid*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_partid = IFNULL(pt.dim_partid, 1)
FROM tmp_fact_e2eleadtime f
LEFT JOIN dim_Part pt ON pt.PartNumber = f.MSEG_MATNR
		 AND pt.Plant      = f.MSEG_WERKS 
WHERE f.dim_partid <> IFNULL(pt.dim_partid, 1);

/*T436A_FREIZ dd_releaseperiod*/	
UPDATE tmp_fact_e2eleadtime f
SET f.dd_releaseperiod = IFNULL(v.V436A_FREIZ,0)
FROM  tmp_fact_e2eleadtime f, dim_part d, V436A_E2E v
WHERE f.dim_partid = d.dim_partid
AND d.schedmarginkey=v.V436A_FHORI
AND d.plant =v.V436A_WERKS
AND f.dd_releaseperiod <> IFNULL(v.V436A_FREIZ,0);

/*Dim_Plantid*/	
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_Plantid = IFNULL(pl.dim_plantid, 1)
FROM tmp_fact_e2eleadtime f
LEFT JOIN dim_plant pl ON pl.PlantCode = f.MSEG_WERKS 
WHERE f.Dim_Plantid <> IFNULL(pl.dim_plantid, 1);

/*dim_dateidPRCreation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidPRCreation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EBAN_BADAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidPRCreation <> ifnull(dd.dim_dateid,1);

/*dim_dateidPOCreation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidPOCreation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EKKO_AEDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidPOCreation <> ifnull(dd.dim_dateid,1);

/*dim_dateidProcessing*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidProcessing = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.NAST_DATVR
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidProcessing <> ifnull(dd.dim_dateid,1);

/*dim_dateidConfReceived*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidConfReceived = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EKES_ERDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidConfReceived <> ifnull(dd.dim_dateid,1);

/*dim_dateidDocEntry*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidDocEntry = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.MKPF_CPUDT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidDocEntry <> ifnull(dd.dim_dateid,1);

/*dim_dateidTOCreated*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidTOCreated = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.LTAK_BDATU
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidTOCreated <> ifnull(dd.dim_dateid,1);

/*dim_dateidconfirmation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidconfirmation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.LTAP_QDATU
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidconfirmation <> ifnull(dd.dim_dateid,1);

/*dim_dateidlotcreated*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidlotcreated = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QALS_ENSTEHDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidlotcreated <> ifnull(dd.dim_dateid,1);

/*dim_dateidsdopsrelease*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidsdopsrelease = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QPRN_FRGDT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidsdopsrelease <> ifnull(dd.dim_dateid,1);

/*Dim_DateIdUsageDecision*/
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdUsageDecision = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QAVE_VDATUM
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdUsageDecision <> ifnull(dd.dim_dateid,1);

/*dim_dateidfirst261*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidfirst261 = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.MKPF_BUDAT_first
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidfirst261 <> ifnull(dd.dim_dateid,1);

--dim_dateidpostingdate
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidpostingdate = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.MKPF_BUDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateidpostingdate <> ifnull(dd.dim_dateid,1);

--Dim_DateIdInspectionLotEnd
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdInspectionLotEnd = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QALS_PAENDTERM
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdInspectionLotEnd <> ifnull(dd.dim_dateid,1);

--Dim_DateIdInspectionLotStart
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdInspectionLotStart = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QALS_PASTRTERM
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdInspectionLotStart <> ifnull(dd.dim_dateid,1);

--Dim_DateIdProcessOrderCreation
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdProcessOrderCreation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.CAUFV_ERDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdProcessOrderCreation <> ifnull(dd.dim_dateid,1);

--Dim_DateIdProcessOrderRelease
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdProcessOrderRelease = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.CAUFV_FTRMI
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdProcessOrderRelease <> ifnull(dd.dim_dateid,1);


--Dim_DateIdProcessOrderFinish
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdProcessOrderFinish = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.CAUFV_GLTRI
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.Dim_DateIdProcessOrderFinish <> ifnull(dd.dim_dateid,1);

--dim_dateid_lab_reception
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateid_lab_reception = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.lab_reception_date
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateid_lab_reception <> ifnull(dd.dim_dateid,1);


--dim_dateid_analysis_end
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateid_analysis_end = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.analysis_end_date
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateid_analysis_end <> ifnull(dd.dim_dateid,1);

--dim_dateid_verification_end
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateid_verification_end = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.verification_end_date
AND dd.CompanyCode = pl.CompanyCode
AND f.MSEG_WERKS = pl.PlantCode
AND f.dim_dateid_verification_end <> ifnull(dd.dim_dateid,1);



DROP TABLE IF EXISTS number_fountain_fact_e2eleadtime;
CREATE TABLE number_fountain_fact_e2eleadtime LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_e2eleadtime WHERE table_name = 'fact_e2eleadtime';

INSERT INTO number_fountain_fact_e2eleadtime
SELECT 'fact_e2eleadtime', ifnull(max(fact_e2eleadtimeid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_e2eleadtime;


DELETE FROM fact_e2eleadtime;

INSERT INTO fact_e2eleadtime(
fact_e2eLeadTimeid
,Dim_Plantid 			
,dim_partid				
,dd_PurchaseReqNo		
,dd_PurchaseReqItemNo	
,dim_dateidPRCreation	
,dd_PurchaseOrderNo		
,dd_PurchaseOrderItemNo
,dim_dateidPOCreation	
,dim_dateidProcessing		
,dim_dateidConfReceived	
,dd_MaterialDocNo		
,dd_MaterialDocItemNo	
,dd_BatchNumber			
,dim_dateidDocEntry		
,dd_TransferReqNo		
,dd_TrackingNo	
,dd_TransferOrderNo		
,dd_TransferOrderItem	
,dim_dateidTOCreated		
,dim_dateidconfirmation	
,dd_inspectionlotno		
,dim_dateidlotcreated	
,dd_SampleDrawinNo		
,dim_dateidsdopsrelease	
,Dim_DateIdUsageDecision	
,dim_dateidfirst261
,ct_prCreationlt			
,ct_poCreationlt			
,ct_ptconfirmationlt		
,ct_supplierlt			
,ct_tocreationlt			
,ct_stockPlacementlt		
,ct_sampleDrawinglt		
,ct_qcqalt				
,ct_bachfirst						
,amt_exhangerate			
,amt_exchangerate_gbl	
,dim_currencyid			
,dim_currencyid_tra		
,dim_currencyid_gbl		
,DW_UPDATE_DATE			
,DW_INSERT_DATE			
,DIM_PROJECTSOURCEID	
,dd_OrderNumber	
,Dim_DateIdInspectionLotEnd
,Dim_DateIdInspectionLotStart
,Dim_DateIdProcessOrderCreation
,Dim_DateIdProcessOrderRelease
,Dim_DateIdProcessOrderFinish
,ct_PROPreparationLT
,ct_MaterialStaggingLT
,ct_ProductionLT
,ct_StandardQCLT
,ct_StandardQCQALTv2	
,ct_PlannedDeliveryLT
,ct_GoodRecepitProcessingLT
,dim_materialcodingschemeID
,dd_releaseperiod
,dim_dateidpostingdate
,dim_inspectionlotstatusid
,dim_vendorid
,dim_dateid_lab_reception
,dim_dateid_analysis_end
,dim_dateid_verification_end
,dd_SOPFamily
,dd_SubFamily
,dd_UDCode
,dd_workcenter
)
SELECT 
( SELECT max_id FROM number_fountain_fact_e2eleadtime WHERE table_name = 'fact_e2eleadtime') 
	+ row_number() over(order by '') AS fact_e2eLeadTimeid
,Dim_Plantid 			
,dim_partid				
,dd_PurchaseReqNo		
,dd_PurchaseReqItemNo	
,dim_dateidPRCreation	
,dd_PurchaseOrderNo		
,dd_PurchaseOrderItemNo	
,dim_dateidPOCreation	
,dim_dateidProcessing	
,dim_dateidConfReceived	
,dd_MaterialDocNo		
,dd_MaterialDocItemNo	
,dd_BatchNumber			
,dim_dateidDocEntry		
,dd_TransferReqNo		
,dd_TrackingNo	
,dd_TransferOrderNo		
,dd_TransferOrderItem	
,dim_dateidTOCreated		
,dim_dateidconfirmation	
,dd_inspectionlotno		
,dim_dateidlotcreated	
,dd_SampleDrawinNo		
,dim_dateidsdopsrelease	
,Dim_DateIdUsageDecision	
,dim_dateidfirst261
,ct_prCreationlt			
,ct_poCreationlt			
,ct_ptconfirmationlt		
,ct_supplierlt			
,ct_tocreationlt			
,ct_stockPlacementlt		
,ct_sampleDrawinglt		
,ct_qcqalt				
,ct_bachfirst						
,amt_exhangerate			
,amt_exchangerate_gbl	
,dim_currencyid			
,dim_currencyid_tra		
,dim_currencyid_gbl		
,DW_UPDATE_DATE			
,DW_INSERT_DATE			
,DIM_PROJECTSOURCEID	
,dd_OrderNumber	
,Dim_DateIdInspectionLotEnd
,Dim_DateIdInspectionLotStart
,Dim_DateIdProcessOrderCreation
,Dim_DateIdProcessOrderRelease
,Dim_DateIdProcessOrderFinish
,ct_PROPreparationLT
,ct_MaterialStaggingLT
,ct_ProductionLT
,ct_StandardQCLT
,ct_StandardQCQALTv2
,ct_PlannedDeliveryLT
,ct_GoodRecepitProcessingLT
,dim_materialcodingschemeID
,dd_releaseperiod
,dim_dateidpostingdate
,dim_inspectionlotstatusid
,dim_vendorid
,dim_dateid_lab_reception
,dim_dateid_analysis_end
,dim_dateid_verification_end
,dd_SOPFamily
,dd_SubFamily
,dd_UDCode
,dd_workcenter
FROM tmp_fact_e2eleadtime;

DROP TABLE IF EXISTS tmp_fact_e2eleadtime;
DROP TABLE IF EXISTS tmp_maxGRdate;

DELETE FROM emd586.fact_e2eleadtime;

INSERT INTO emd586.fact_e2eleadtime
SELECT *
FROM fact_e2eleadtime;