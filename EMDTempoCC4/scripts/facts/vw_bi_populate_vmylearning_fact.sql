drop table if exists TMP_fact_vmylearning;
create table TMP_fact_vmylearning like fact_vmylearning INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain m
WHERE m.table_name = 'fact_vmylearning';

INSERT INTO number_fountain
SELECT 'fact_vmylearning',
       IFNULL(MAX(bw.fact_vmylearningid), IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_vmylearning bw
WHERE bw.fact_vmylearningid <> 1;

insert into TMP_fact_vmylearning(fact_vmylearningid
  ,dim_projectsourceid
  ,amt_exhangerate
  ,amt_exchangerate_gbl
  ,dim_currencyid
  ,dim_currencyid_tra
  ,dim_currencyid_gbl
  ,dd_item_id
  ,dim_qualityusersid
  ,dd_notactive
  ,dd_curricula
  ,dd_assignment_type
  ,dd_remaining_days
  ,dim_dateid_revision
  ,dim_dateid_required
  ,dim_dateid_completion
  ,dd_item_type)
select (SELECT IFNULL(m.max_id, 1)
  FROM number_fountain m
  WHERE m.table_name = 'fact_vmylearning') + ROW_NUMBER() OVER(ORDER BY '') AS fact_vmylearningid
  ,(select dim_projectsourceid from dim_projectsource) as dim_projectsourceid
  ,1.0000 as amt_exhangerate
  ,1.0000 as amt_exchangerate_gbl
  ,1 as dim_currencyid
  ,1 as dim_currencyid_tra
  ,1 as dim_currencyid_gbl
  ,ifnull(item_id,'Not Set') as dd_item_id
  ,ifnull(u.dim_qualityusersid,1) as dim_qualityusersid
  ,ifnull(not_active,'Not Set') as dd_notactive
  ,ifnull(curricula,'Not Set') as dd_curricula
  ,ifnull(assignment_type,'Not Set') as dd_assignment_type
  ,ifnull(remaining_days,'Not Set') as dd_remaining_days
  ,ifnull(d.dim_dateid,1) as dim_dateid_revision
  ,ifnull(d2.dim_dateid,1) as dim_dateid_required
  ,ifnull(d3.dim_dateid,1) as dim_dateid_completion
  ,ifnull(item_type,'Not Set') as dd_item_type
from MSVT_VEV_AUB_LEARNING_REPORT t
  left join dim_qualityusers u on u.merck_uid = t.user_id and u.rowiscurrent = 1
  left join dim_date d on d.companycode = 'Not Set' and d.datevalue = cast(left(t.required_date,10) as date)
  left join dim_date d2 on d2.companycode = 'Not Set' and d2.datevalue = cast(left(t.required_date,10) as date)
  left join dim_date d3 on d3.companycode = 'Not Set' and d3.datevalue = cast(left(t.completion_date,10) as date);

DELETE FROM fact_vmylearning;
INSERT INTO fact_vmylearning(fact_vmylearningid
  ,dim_projectsourceid
  ,amt_exhangerate
  ,amt_exchangerate_gbl
  ,dim_currencyid
  ,dim_currencyid_tra
  ,dim_currencyid_gbl
  ,dd_item_id
  ,dim_qualityusersid
  ,dd_notactive
  ,dd_curricula
  ,dd_assignment_type
  ,dd_remaining_days
  ,dim_dateid_revision
  ,dim_dateid_required
  ,dim_dateid_completion
  ,dd_item_type)
SELECT fact_vmylearningid
  ,dim_projectsourceid
  ,amt_exhangerate
  ,amt_exchangerate_gbl
  ,dim_currencyid
  ,dim_currencyid_tra
  ,dim_currencyid_gbl
  ,dd_item_id
  ,dim_qualityusersid
  ,dd_notactive
  ,dd_curricula
  ,dd_assignment_type
  ,dd_remaining_days
  ,dim_dateid_revision
  ,dim_dateid_required
  ,dim_dateid_completion
  ,dd_item_type
FROM TMP_fact_vmylearning;

drop table if exists TMP_fact_vmylearning;
