/******************************************************************************************************************/
/*   Script         : bi_populate_devperbatch_fact                                                                */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_devperbatch                                                       */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   16 Feb 2017         CristianT       1.0               Creating the script.                                   */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_devperbatch;
CREATE TABLE tmp_fact_devperbatch
AS
SELECT *
FROM fact_devperbatch
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_devperbatch';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_devperbatch', ifnull(max(fact_devperbatchid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_devperbatch;

/* Identify Erbitux Batches from MES data */

DROP TABLE IF EXISTS tmp_meserbituxbatches;
CREATE TABLE tmp_meserbituxbatches
AS
SELECT batch as dd_meschildbatchno,
       substr(batch,0, 3) as dd_mesbatchno
FROM EKE_MO_LIST
WHERE 1 = 1
      AND substr(BATCH, 0, 1) BETWEEN '0' AND '9'
      AND MATERIAL_ID like 'BN1%'
GROUP BY BATCH,substr(batch,0, 3);

/* Search for all deviations for Erbitux in TW data based on the batches from MES */
/* Commented this to have the example of initial approach
INSERT INTO tmp_fact_devperbatch(
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_meschildbatchno,
dd_mesbatchno,
dd_twprid,
dd_twseqno,
dd_twproduct,
dd_twitemcodeno,
dd_twbatchno,
dd_twlocationofbatches,
dd_twdispositionstatus
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_devperbatch') + ROW_NUMBER() over(order by '') as fact_devperbatchid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(mes.dd_meschildbatchno, 'Not Set') as dd_meschildbatchno,
       ifnull(mes.dd_mesbatchno, 'Not Set') as dd_mesbatchno,
       ifnull(tw.PR_ID, 0) as dd_twprid,
       ifnull(tw.seq_no, 0) as dd_twseqno,
       ifnull(tw.product, 'Not Set') as dd_twproduct,
       ifnull(tw.item_code_no, 'Not Set') as dd_twitemcodeno,
       ifnull(tw.batch_no, 'Not Set') as dd_twbatchno,
       ifnull(case when lower(tw.location_of_batches) = 'n/a' then 'Not Set' else tw.location_of_batches end, 'Not Set') as dd_twlocationofbatches,
       ifnull(case when lower(tw.disposition_status) = 'n/a' then 'Not Set' else tw.disposition_status end, 'Not Set') as dd_twdispositionstatus
FROM MV_XTR_VEVEY_AUBONNE_RPT_BATCH tw,
     tmp_meserbituxbatches mes
WHERE 1 = 1
      AND tw.product like 'Erbitux H%'
      AND tw.item_code_no like 'BN1%'
      AND tw.batch_no like '%' || mes.dd_meschildbatchno || '%'
*/

/* This insert is searching for single batch occurance inside batch_no column from mv_xtr_vevey_aubonne_rpt_batch table. Searching for batches with material id inside */
INSERT INTO tmp_fact_devperbatch(
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_meschildbatchno,
dd_mesbatchno,
dd_twprid,
dd_twseqno,
dd_twproduct,
dd_twitemcodeno,
dd_twbatchno,
dd_twlocationofbatches,
dd_twdispositionstatus
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_devperbatch') + ROW_NUMBER() over(order by '') as fact_devperbatchid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(mes.dd_meschildbatchno, 'Not Set') as dd_meschildbatchno,
       ifnull(mes.dd_mesbatchno, 'Not Set') as dd_mesbatchno,
       ifnull(tw.PR_ID, 0) as dd_twprid,
       ifnull(tw.seq_no, 0) as dd_twseqno,
       ifnull(tw.product, 'Not Set') as dd_twproduct,
       ifnull(tw.item_code_no, 'Not Set') as dd_twitemcodeno,
       ifnull(tw.batch_no, 'Not Set') as dd_twbatchno,
       ifnull(case when lower(tw.location_of_batches) = 'n/a' then 'Not Set' else tw.location_of_batches end, 'Not Set') as dd_twlocationofbatches,
       ifnull(case when lower(tw.disposition_status) = 'n/a' then 'Not Set' else tw.disposition_status end, 'Not Set') as dd_twdispositionstatus
FROM mv_xtr_vevey_aubonne_rpt_batch tw,
     tmp_meserbituxbatches mes
WHERE 1 = 1
      AND tw.product like 'Erbitux H%'
      AND tw.item_code_no like 'BN1%'
      AND tw.batch_no like '%BN1%'
      AND tw.batch_no not like '%,%'
      AND tw.batch_no like '%' || mes.dd_meschildbatchno;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_devperbatch';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_devperbatch', ifnull(max(fact_devperbatchid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_devperbatch;

/* This insert is searching for batch occurance inside batch_no column from mv_xtr_vevey_aubonne_rpt_batch table. Searching for batches without material id inside */
INSERT INTO tmp_fact_devperbatch(
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_meschildbatchno,
dd_mesbatchno,
dd_twprid,
dd_twseqno,
dd_twproduct,
dd_twitemcodeno,
dd_twbatchno,
dd_twlocationofbatches,
dd_twdispositionstatus
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_devperbatch') + ROW_NUMBER() over(order by '') as fact_devperbatchid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(mes.dd_meschildbatchno, 'Not Set') as dd_meschildbatchno,
       ifnull(mes.dd_mesbatchno, 'Not Set') as dd_mesbatchno,
       ifnull(tw.PR_ID, 0) as dd_twprid,
       ifnull(tw.seq_no, 0) as dd_twseqno,
       ifnull(tw.product, 'Not Set') as dd_twproduct,
       ifnull(tw.item_code_no, 'Not Set') as dd_twitemcodeno,
       ifnull(tw.batch_no, 'Not Set') as dd_twbatchno,
       ifnull(case when lower(tw.location_of_batches) = 'n/a' then 'Not Set' else tw.location_of_batches end, 'Not Set') as dd_twlocationofbatches,
       ifnull(case when lower(tw.disposition_status) = 'n/a' then 'Not Set' else tw.disposition_status end, 'Not Set') as dd_twdispositionstatus
FROM mv_xtr_vevey_aubonne_rpt_batch tw,
     tmp_meserbituxbatches mes
WHERE 1 = 1
      AND tw.product like 'Erbitux H%'
      AND tw.item_code_no like 'BN1%'
      AND tw.batch_no not like '%BN1%'
      AND is_number(replace(tw.batch_no, ',', '')) is true
      AND tw.batch_no like '%' || mes.dd_meschildbatchno;

/* This insert is searching for multiple batch occurance inside batch_no column from mv_xtr_vevey_aubonne_rpt_batch table. Searching for batches with material id inside  */
/* Creating a tmp table to search for all Erbitux rows from mv_xtr_vevey_aubonne_rpt_batch that have multiple batches in one line splited by , 
All rows with multple batches on same line will be splited in multiple lines for each batch
*/
DROP TABLE IF EXISTS tmp_multiplebatches;
CREATE TABLE tmp_multiplebatches
AS
WITH tmp_view AS (
SELECT pr_id,
       seq_no,
       product,
       item_code_no,
       location_of_batches,
       disposition_status,
       SUBSTR(BATCH_NO, 1, INSTR(BATCH_NO,',')-1) token1,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,1)+1, INSTR(BATCH_NO,',',1,2)-INSTR(BATCH_NO,',',1,1)-1) token2,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,2)+1, INSTR(BATCH_NO,',',1,3)-INSTR(BATCH_NO,',',1,2)-1) token3,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,3)+1, INSTR(BATCH_NO,',',1,4)-INSTR(BATCH_NO,',',1,3)-1) token4,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,4)+1, INSTR(BATCH_NO,',',1,5)-INSTR(BATCH_NO,',',1,4)-1) token5,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,5)+1, INSTR(BATCH_NO,',',1,6)-INSTR(BATCH_NO,',',1,5)-1) token6,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,6)+1, INSTR(BATCH_NO,',',1,7)-INSTR(BATCH_NO,',',1,6)-1) token7,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,7)+1, INSTR(BATCH_NO,',',1,8)-INSTR(BATCH_NO,',',1,7)-1) token8,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,8)+1, INSTR(BATCH_NO,',',1,9)-INSTR(BATCH_NO,',',1,8)-1) token9,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,9)+1, INSTR(BATCH_NO,',',1,10)-INSTR(BATCH_NO,',',1,9)-1) token10,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,10)+1, INSTR(BATCH_NO,',',1,11)-INSTR(BATCH_NO,',',1,10)-1) token11,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,11)+1, INSTR(BATCH_NO,',',1,12)-INSTR(BATCH_NO,',',1,11)-1) token12,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,12)+1, INSTR(BATCH_NO,',',1,13)-INSTR(BATCH_NO,',',1,12)-1) token13,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,13)+1, INSTR(BATCH_NO,',',1,14)-INSTR(BATCH_NO,',',1,13)-1) token14,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,14)+1, INSTR(BATCH_NO,',',1,15)-INSTR(BATCH_NO,',',1,14)-1) token15,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,15)+1, INSTR(BATCH_NO,',',1,16)-INSTR(BATCH_NO,',',1,15)-1) token16,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,16)+1, INSTR(BATCH_NO,',',1,17)-INSTR(BATCH_NO,',',1,16)-1) token17,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,17)+1, INSTR(BATCH_NO,',',1,18)-INSTR(BATCH_NO,',',1,17)-1) token18,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,18)+1, INSTR(BATCH_NO,',',1,19)-INSTR(BATCH_NO,',',1,18)-1) token19,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,19)+1, INSTR(BATCH_NO,',',1,20)-INSTR(BATCH_NO,',',1,19)-1) token20,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,20)+1, INSTR(BATCH_NO,',',1,21)-INSTR(BATCH_NO,',',1,20)-1) token21,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,21)+1, INSTR(BATCH_NO,',',1,22)-INSTR(BATCH_NO,',',1,21)-1) token22,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,22)+1, INSTR(BATCH_NO,',',1,23)-INSTR(BATCH_NO,',',1,22)-1) token23,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,23)+1, INSTR(BATCH_NO,',',1,24)-INSTR(BATCH_NO,',',1,23)-1) token24,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,24)+1, INSTR(BATCH_NO,',',1,25)-INSTR(BATCH_NO,',',1,24)-1) token25,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',1,25)+1, INSTR(BATCH_NO,',',1,26)-INSTR(BATCH_NO,',',1,25)-1) token26,
       SUBSTR(BATCH_NO, INSTR(BATCH_NO,',',-1)+1) tokenLAST
FROM mv_xtr_vevey_aubonne_rpt_batch m
WHERE 1 = 1
      AND product like 'Erbitux H%'
      AND item_code_no like 'BN1%'
      AND batch_no like '%BN1%'
      AND batch_no like '%,%')
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token1) as batch_no
FROM tmp_view
WHERE token1 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token2) as batch_no
FROM tmp_view
WHERE token2 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token3) as batch_no
FROM tmp_view
WHERE token3 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token4) as batch_no
FROM tmp_view
WHERE token4 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token5) as batch_no
FROM tmp_view
WHERE token5 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token6) as batch_no
FROM tmp_view
WHERE token6 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token7) as batch_no
FROM tmp_view
WHERE token7 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token8) as batch_no
FROM tmp_view
WHERE token8 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token9) as batch_no
FROM tmp_view
WHERE token9 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token10) as batch_no
FROM tmp_view
WHERE token10 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token11) as batch_no
FROM tmp_view
WHERE token11 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token12) as batch_no
FROM tmp_view
WHERE token12 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token13) as batch_no
FROM tmp_view
WHERE token13 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token14) as batch_no
FROM tmp_view
WHERE token14 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token15) as batch_no
FROM tmp_view
WHERE token15 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token16) as batch_no
FROM tmp_view
WHERE token16 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token17) as batch_no
FROM tmp_view
WHERE token17 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token18) as batch_no
FROM tmp_view
WHERE token18 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token19) as batch_no
FROM tmp_view
WHERE token19 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token20) as batch_no
FROM tmp_view
WHERE token20 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token21) as batch_no
FROM tmp_view
WHERE token21 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token22) as batch_no
FROM tmp_view
WHERE token22 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token23) as batch_no
FROM tmp_view
WHERE token23 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token24) as batch_no
FROM tmp_view
WHERE token24 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token25) as batch_no
FROM tmp_view
WHERE token25 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(token26) as batch_no
FROM tmp_view
WHERE token26 IS NOT NULL
UNION ALL
SELECT pr_id, seq_no,product,item_code_no,location_of_batches,disposition_status,trim(tokenLAST) as batch_no
FROM tmp_view
WHERE tokenLAST IS NOT NULL;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_devperbatch';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_devperbatch', ifnull(max(fact_devperbatchid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_devperbatch;

INSERT INTO tmp_fact_devperbatch(
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_meschildbatchno,
dd_mesbatchno,
dd_twprid,
dd_twseqno,
dd_twproduct,
dd_twitemcodeno,
dd_twbatchno,
dd_twlocationofbatches,
dd_twdispositionstatus
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_devperbatch') + ROW_NUMBER() over(order by '') as fact_devperbatchid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(mes.dd_meschildbatchno, 'Not Set') as dd_meschildbatchno,
       ifnull(mes.dd_mesbatchno, 'Not Set') as dd_mesbatchno,
       ifnull(tw.PR_ID, 0) as dd_twprid,
       ifnull(tw.seq_no, 0) as dd_twseqno,
       ifnull(tw.product, 'Not Set') as dd_twproduct,
       ifnull(tw.item_code_no, 'Not Set') as dd_twitemcodeno,
       ifnull(tw.batch_no, 'Not Set') as dd_twbatchno,
       ifnull(case when lower(tw.location_of_batches) = 'n/a' then 'Not Set' else tw.location_of_batches end, 'Not Set') as dd_twlocationofbatches,
       ifnull(case when lower(tw.disposition_status) = 'n/a' then 'Not Set' else tw.disposition_status end, 'Not Set') as dd_twdispositionstatus
FROM tmp_multiplebatches tw,
     tmp_meserbituxbatches mes
WHERE 1 = 1
      AND tw.batch_no like '%BN1%'
      AND tw.batch_no like '%' || mes.dd_meschildbatchno;

DROP TABLE IF EXISTS tmp_multiplebatches;

DROP TABLE IF EXISTS tmp_meserbituxbatches;
      
UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_devperbatch tmp;

/* Based on the discussion with Frederic Graf we should keep only one Deviation from TW at dd_mesbatchno/dd_twprid level. The logic is to keep min(dd_twseqno) */
DROP TABLE IF EXISTS tmp_multiplepridperbatch;
CREATE TABLE tmp_multiplepridperbatch
AS
SELECT DD_MESBATCHNO,
       DD_TWPRID,
       MIN(DD_TWSEQNO) as DD_TWSEQNO
FROM tmp_fact_devperbatch tmp
GROUP BY DD_MESBATCHNO,
         DD_TWPRID;

DROP TABLE IF EXISTS tmp_tobeldeleted;
CREATE TABLE tmp_tobeldeleted
AS
SELECT t.fact_devperbatchid as fact_devperbatchid 
FROM tmp_fact_devperbatch t
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_multiplepridperbatch del
				  WHERE del.DD_MESBATCHNO = t.DD_MESBATCHNO
				        AND del.DD_TWPRID = t.DD_TWPRID
				        AND del.DD_TWSEQNO = t.DD_TWSEQNO);

MERGE INTO tmp_fact_devperbatch t
USING tmp_tobeldeleted del ON t.fact_devperbatchid = del.fact_devperbatchid
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_multiplepridperbatch;
DROP TABLE IF EXISTS tmp_tobeldeleted;


TRUNCATE TABLE fact_devperbatch;
	  
INSERT INTO fact_devperbatch(
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_meschildbatchno,
dd_mesbatchno,
dd_twprid,
dd_twseqno,
dd_twproduct,
dd_twitemcodeno,
dd_twbatchno,
dd_twlocationofbatches,
dd_twdispositionstatus
)
SELECT fact_devperbatchid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_meschildbatchno,
       dd_mesbatchno,
       dd_twprid,
       dd_twseqno,
       dd_twproduct,
       dd_twitemcodeno,
       dd_twbatchno,
       dd_twlocationofbatches,
       dd_twdispositionstatus
FROM tmp_fact_devperbatch;

DROP TABLE IF EXISTS tmp_fact_devperbatch;

/* Keeping delete/insert statements for emd586 inside the script to prevent delays caused by harmonization */

TRUNCATE TABLE emd586.fact_devperbatch;

INSERT INTO emd586.fact_devperbatch(
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_meschildbatchno,
dd_mesbatchno,
dd_twprid,
dd_twseqno,
dd_twproduct,
dd_twitemcodeno,
dd_twbatchno,
dd_twlocationofbatches,
dd_twdispositionstatus
)
SELECT fact_devperbatchid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_meschildbatchno,
       dd_mesbatchno,
       dd_twprid,
       dd_twseqno,
       dd_twproduct,
       dd_twitemcodeno,
       dd_twbatchno,
       dd_twlocationofbatches,
       dd_twdispositionstatus
FROM fact_devperbatch;