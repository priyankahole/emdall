/*****************************************************************************************************************/
/*   Script         : exa_fact_netsales                                                                          */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 22 Jan 2018                                                                                */
/*   Description    : Populating script of fact_netsales                                                         */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   22 Jan 2018         CristianT      1.0          Creating the script based on the work of Veronica P         */
/******************************************************************************************************************/


/* Step 0: Preparing all 3 sources data by adding the coresponding description */
DROP TABLE IF EXISTS tmp_netsales;
CREATE TABLE tmp_netsales
AS
SELECT b.*,
       'Net Sales' AS Description
FROM BIC_AZCOFOPS500 b;

DROP TABLE IF EXISTS tmp_logisticscosts;
CREATE TABLE tmp_logisticscosts
AS
SELECT b.*,
       'Logistics Costs' AS Description
FROM BIC_AZCOFOPS100 b;

DROP TABLE IF EXISTS tmp_netinventory;
CREATE TABLE tmp_netinventory
AS
SELECT b.*,
       'Net Inventory' AS Description
FROM BIC_AZCOFOPS400 b;

/* Step 1: Insert data in tmp fact table from all previous tmp tables */
DROP TABLE IF EXISTS tmp_fact_netsales;
CREATE TABLE tmp_fact_netsales
AS
SELECT BIC_Z_AGRIND,
       BIC_Z_AMTCLYR,
       BIC_Z_AMTLCLY,
       BIC_Z_AMTLYR,
       BIC_Z_AUDITID,
       BIC_Z_CAMNTGC,
       BIC_Z_CAMNTLC,
       BIC_Z_CAMTLYR,
       BIC_Z_CGCPLR,
       BIC_Z_CGCPYR,
       BIC_Z_COUNTRY,
       BIC_Z_GCPLR,
       BIC_Z_GCPYR,
       BIC_Z_PRODUCT,
       BIC_Z_REPPOS,
       BIC_Z_REPUNIT,
       BIC_Z_RESTKEY,
       BIC_Z_VERSION,
       CALMONTH,
       CALQUARTER,
       CALYEAR,
       CS_TRN_GC,
       CS_TRN_LC,
       CURKEY_GC,
       CURKEY_LC,
       DESCRIPTION,
       RECORDMODE
FROM tmp_netsales
UNION ALL
SELECT null as BIC_Z_AGRIND,
       BIC_Z_AMTCLYR,
       BIC_Z_AMTLCLY,
       BIC_Z_AMTLYR,
       BIC_Z_AUDITID,
       BIC_Z_CAMNTGC,
       BIC_Z_CAMNTLC,
       BIC_Z_CAMTLYR,
       BIC_Z_CGCPLR,
       BIC_Z_CGCPYR,
       'Not Set' as BIC_Z_COUNTRY,
       BIC_Z_GCPLR,
       BIC_Z_GCPYR,
       BIC_Z_PRODUCT,
       BIC_Z_REPPOS,
       BIC_Z_REPUNIT,
       null as BIC_Z_RESTKEY,
       BIC_Z_VERSION,
       CALMONTH,
       CALQUARTER,
       CALYEAR,
       CS_TRN_GC,
       CS_TRN_LC,
       CURKEY_GC,
       CURKEY_LC,
       DESCRIPTION,
       RECORDMODE
FROM tmp_logisticscosts
UNION ALL
SELECT null as BIC_Z_AGRIND,
       BIC_Z_AMTCLYR,
       BIC_Z_AMTLCLY,
       BIC_Z_AMTLYR,
       BIC_Z_AUDITID,
       BIC_Z_CAMNTGC,
       BIC_Z_CAMNTLC,
       BIC_Z_CAMTLYR,
       BIC_Z_CGCPLR,
       BIC_Z_CGCPYR,
       'Not Set' as BIC_Z_COUNTRY,
       BIC_Z_GCPLR,
       BIC_Z_GCPYR,
       BIC_Z_PRODUCT,
       BIC_Z_REPPOS,
       BIC_Z_REPUNIT,
       null as BIC_Z_RESTKEY,
       BIC_Z_VERSION,
       CALMONTH,
       CALQUARTER,
       CALYEAR,
       CS_TRN_GC,
       CS_TRN_LC,
       CURKEY_GC,
       CURKEY_LC,
       DESCRIPTION,
       RECORDMODE
FROM tmp_netinventory;


/* Step 2: Insert data in fact table */
DELETE FROM number_fountain m where m.table_name = 'fact_netsales';
INSERT INTO number_fountain
SELECT 'fact_netsales',
	ifnull(max( f.fact_netsalesid ) , ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_netsales f
WHERE f.fact_netsalesid <> 1;


TRUNCATE TABLE fact_netsales;
INSERT INTO fact_netsales(
fact_netsalesid,
dd_quarter,
dd_calmonth,
dd_calyear,
dd_auditid,
dd_country,
dd_product,
dd_position,
dd_repunit,
dd_version,
dd_curkey_gc,
dd_curkey_lc,
amt_cs_trn_gc,
amt_cs_trn_lc,
amt_amtclyr,
amt_amtlcly,
amt_amtlyr,
amt_camntgc,
amt_camntlc,
amt_camtlyr,
amt_cgcplr,
amt_cgcpyr,
amt_gcplr,
amt_gcpyr,
dd_description,
dim_positionhierarchyid,
dim_legalentityid,
amt_exchangerate_gbl,
dim_audithierarchyid,
dim_ch_bwproducthierarchyid,
dim_projectsourceid,
amt_exhangerate,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dim_dateid,
dd_region,
dd_countrycluster,
amt_bogid_ytd
)
SELECT (select max_id from number_fountain where table_name = 'fact_netsales') + row_number() over(order by '') AS fact_netsalesid,
       CALQUARTER as dd_quarter ,
       CALMONTH as dd_CALMONTH ,
       CALYEAR as dd_CALYEAR ,
       BIC_Z_AUDITID as dd_auditid ,
       bic_z_country as dd_country ,
       BIC_Z_PRODUCT as dd_product ,
       BIC_Z_REPPOS as dd_position ,
       BIC_Z_REPUNIT as dd_repunit ,
       BIC_Z_VERSION as dd_version ,
       CURKEY_GC as dd_CURKEY_GC ,
       CURKEY_LC as dd_CURKEY_LC ,
       CS_TRN_GC as amt_CS_TRN_GC ,
       CS_TRN_LC as amt_CS_TRN_LC ,
       BIC_Z_AMTCLYR as amt_AMTCLYR ,
       BIC_Z_AMTLCLY as amt_AMTLCLY ,
       BIC_Z_AMTLYR as amt_AMTLYR ,
       BIC_Z_CAMNTGC as amt_CAMNTGC ,
       BIC_Z_CAMNTLC as amt_CAMNTLC ,
       BIC_Z_CAMTLYR as amt_CAMTLYR ,
       BIC_Z_CGCPLR as amt_CGCPLR ,
       BIC_Z_CGCPYR as amt_CGCPYR ,
       BIC_Z_GCPLR  as amt_GCPLR ,
       BIC_Z_GCPYR as amt_GCPYR ,
       Description as dd_description,
       1 as dim_positionhierarchyid,
       1 as dim_legalentityid,
       1 as amt_exchangerate_gbl,
       1 as dim_audithierarchyid,
       1 as dim_ch_bwproducthierarchyid,
       1 as dim_projectsourceid,
	   1 as amt_exhangerate,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as dim_dateid,
       'Not Set' as dd_region,
       'Not Set' as dd_countrycluster,
       0 as amt_bogid_ytd
FROM tmp_fact_netsales
WHERE BIC_Z_VERSION = 'A';

/* step3: updates dim_id */
UPDATE fact_netsales f
SET f.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     fact_netsales f;

DROP TABLE IF EXISTS tmp_netsales_date;
CREATE TABLE tmp_netsales_date
AS
SELECT dt.calendarmonthid,
       min(dt.dim_dateid) as dim_dateid
FROM dim_date dt,
     fact_netsales ns
WHERE dt.companycode = 'Not Set'
      AND dt.calendarmonthid = ns.dd_calmonth
GROUP BY dt.calendarmonthid;

UPDATE fact_netsales ns
SET dim_dateid = ifnull(tmp.dim_dateid, 1)
FROM fact_netsales ns,
     tmp_netsales_date tmp
WHERE tmp.calendarmonthid = ns.dd_calmonth;

DROP TABLE IF EXISTS tmp_netsales_date;

UPDATE fact_netsales tmp
SET dd_region = ifnull(rc.region, 'Not Set'),
    dd_countrycluster = ifnull(rc.country_cluster, 'Not Set')
FROM fact_netsales tmp,
     repunit_and_country rc
WHERE trim(leading '0' from tmp.dd_repunit) = trim(leading '0' from rc.reporting_unit)
      AND tmp.dd_Description = 'Net Inventory';

UPDATE fact_netsales tmp
SET dd_country = ifnull(rc.country_code, 'Not Set')
FROM fact_netsales tmp,
     repunit_and_country rc
WHERE tmp.dd_country = 'Not Set'
      AND trim(leading '0' from tmp.dd_repunit) = trim(leading '0' from rc.reporting_unit);

UPDATE fact_netsales tmp
SET dd_region = ifnull(rc.region, 'Not Set'),
    dd_countrycluster = ifnull(rc.country_cluster, 'Not Set')
FROM fact_netsales tmp,
     ns_lifr_bo_fa_lo rc
WHERE trim(leading '0' from tmp.dd_repunit) = trim(leading '0' from rc.reporting_unit)
      AND tmp.dd_country = rc.country_code
      AND tmp.dd_Description in ('Net Sales','Logistics Costs');

/* update legal entity */
UPDATE fact_netsales f
SET f.DIM_LEGALENTITYID = d.DIM_LEGALENTITYID
FROM fact_netsales f
     INNER JOIN dim_legalentity d ON d.le_code = f.dd_repunit
WHERE ifnull(f.DIM_LEGALENTITYID,1) <> ifnull(d.DIM_LEGALENTITYID,0);

/* update audit Hierarchy for net sales and logistics costs */
UPDATE fact_netsales f
SET f.DIM_AUDITHIERARCHYID = d.DIM_AUDITHIERARCHYID
FROM fact_netsales f
INNER JOIN dim_audithierarchy d ON d.nodename_l1 = f.dd_auditid
WHERE 1=1
      AND d.hieid = '4CKHZYP1PNYF0ZXMG9JCRMEDL'
      AND dd_Description in ('Net Sales','Logistics Costs')
      AND ifnull(f.DIM_AUDITHIERARCHYID,1) <> ifnull(d.DIM_AUDITHIERARCHYID,0);

/* update audit Hierarchy for net inventory*/
UPDATE fact_netsales f
SET f.DIM_AUDITHIERARCHYID = d.DIM_AUDITHIERARCHYID
FROM fact_netsales f
     INNER JOIN dim_audithierarchy d ON d.nodename_l1 = f.dd_auditid
WHERE 1=1
      AND d.hieid = '4CKI023Y51LRB0KT1MKT8HTU1'
      AND dd_Description in ('Net Inventory')
      AND ifnull(f.DIM_AUDITHIERARCHYID,1) <> ifnull(d.DIM_AUDITHIERARCHYID,0);

/* update position Hierarchy */
UPDATE fact_netsales f
SET f.DIM_POSITIONHIERARCHYID = d.DIM_POSITIONHIERARCHYID
FROM fact_netsales f
     INNER JOIN dim_positionhierarchy d ON d.nodename_l1 = f.dd_position
WHERE d.hieid = '5845BW8QVWD4NVGQU4MMHPC1O'
      AND ifnull(f.DIM_POSITIONHIERARCHYID,1) <> ifnull(d.DIM_POSITIONHIERARCHYID,0);

/* update product hierarchy */
UPDATE fact_netsales f
SET f.dim_ch_bwproducthierarchyid = ifnull(d.dim_ch_bwproducthierarchyid,1)
FROM fact_netsales f
     INNER JOIN dim_ch_bwproducthierarchy d
         ON d.businessline = f.dd_product
	        AND f.dd_calmonth between replace(substr(d.UPPERHIERSTARTDATE,1,7),'-','')
			AND replace(substr(d.UPPERHIERENDDATE,1,7),'-','')
WHERE ifnull(f.dim_ch_bwproducthierarchyid,1) <> ifnull(d.dim_ch_bwproducthierarchyid,0);

/* Calculate AMT_BODLV and AMT_BOGID */
/* Step 0: calculate MTD BO from fact_salesorder */
/*
DROP TABLE IF EXISTS tmp_bo_net
CREATE TABLE tmp_bo_net
AS
SELECT trim(leading 0 from c.company) as company,
       sum(amt_bogid) as bogid,
       sum(amt_bogid * amt_exchangerate_gbl) as bogid_exchangerate_gbl,
       sum(amt_bodlv) as bodlv,
       sum(amt_bodlv * amt_exchangerate_gbl) as bodlv_exchangerate_gbl,
       substr(gid.datevalue,0,7) as gidate,
       bw.businessline
FROM emd586.fact_salesorder f
     INNER JOIN emd586.dim_plant pl on pl.dim_plantid = f.dim_plantid
     INNER JOIN emd586.dim_company c on c.companycode = pl.companycode and f.dim_projectsourceid = c.projectsourceid
     INNER JOIN emd586.dim_bwproducthierarchy bw on bw.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
     INNER JOIN emd586.dim_date_factory_calendar gid on gid.dim_date_factory_calendarid = f.dim_dateidexpectedship_emd
-- to add left join with dim_date for customer request date filter
-- to add condition on gidate based on the platform
GROUP BY substr(gid.datevalue,0,7),
         bw.businessline,
         trim(leading 0 from c.company)

 Step 1: calculate MTD value from net sales - gauss 
DROP TABLE IF EXISTS tmp_net_bo
CREATE TABLE tmp_net_bo
AS
SELECT min(FACT_NETSALESID) as FACT_NETSALESID,
       dd_calmonth,
       dd_product,
       dd_repunit,
       sum(f.amt_GCPLR) as ns,
       sum(amt_GCPLR * amt_exchangerate_gbl) as ns_exchangerate_gbl,
       trim(leading 0 from c.company) as company,
       cast(0 as decimal(18,4)) as amt_bogid,
       cast(0 as decimal(18,4)) as amt_bodlv
FROM fact_netsales f
     INNER JOIN (SELECT distinct company, companycode FROM emd586.dim_company) c ON trim(leading 0 from c.company) = trim(leading 0 from f.dd_repunit)
WHERE dd_description = 'Net Sales'
GROUP BY dd_calmonth,
         dd_product,
         dd_repunit,
         trim(leading 0 from c.company)

 Step 2: update tmp table net sales with the value from sales order 
UPDATE tmp_net_bo a
SET a.amt_bogid = b.bogid_exchangerate_gbl,
    a.amt_bodlv = b.bodlv_exchangerate_gbl
FROM tmp_net_bo  a
     INNER JOIN tmp_bo_net b
          ON a.company = b.company
          AND replace(b.gidate,'-','') = a.dd_calmonth
          AND b.businessline = a.dd_product


 Step 3: update fact_netsales with the value from salesorder 
UPDATE fact_netsales f
SET f.amt_bogid = t.amt_bogid,
    f.amt_bodlv = t.amt_bodlv
FROM fact_netsales f
     INNER JOIN tmp_net_bo t on f.FACT_NETSALESID = t.FACT_NETSALESID

DROP TABLE IF EXISTS tmp_net_bo
DROP TABLE IF EXISTS tmp_bo_net
*/

DROP TABLE IF EXISTS tmp_consolidated_fact_lifrbo;
CREATE TABLE tmp_consolidated_fact_lifrbo
AS
SELECT dt.calendarmonthid,
       lf.dd_repunitno,
       dd_country, /* dd_bf*/
       sum(amt_bogidatevalue) as amt_bogidatevalue
FROM fact_lifrbo lf,
     dim_date dt
WHERE lf.dim_dateid = dt.dim_dateid
GROUP BY dt.calendarmonthid,
         lf.dd_repunitno,
         dd_country;

DROP TABLE IF EXISTS tmp_net_bo;
CREATE TABLE tmp_net_bo
AS
SELECT min(FACT_NETSALESID) as FACT_NETSALESID,
       dd_calmonth,
       dd_country, /* dd_product */
       dd_repunit
FROM fact_netsales  ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
WHERE dd_description = 'Net Sales'
GROUP BY dd_calmonth,
         dd_country,
         dd_repunit;

UPDATE fact_netsales f
SET f.amt_bogid = t.amt_bogidatevalue
FROM fact_netsales f,
     tmp_consolidated_fact_lifrbo t,
     tmp_net_bo min_id
WHERE f.dd_calmonth = t.calendarmonthid
      AND trim(leading '0' from f.dd_repunit) = trim(leading '0' from t.dd_repunitno)
      AND f.dd_country = t.dd_country
      AND f.dd_description = 'Net Sales'
      AND f.fact_netsalesid = min_id.fact_netsalesid;


DROP TABLE IF EXISTS tmp_consolidated_fact_lifrbo;

DROP TABLE IF EXISTS tmp_bogid_ytd;
CREATE TABLE tmp_bogid_ytd
AS
SELECT dd_repunit,
       dd_country,
       CALENDARMONTHID,
       calendaryear,
       sum(amt_bogid) over(partition by dd_repunit, dd_country, calendaryear order by CALENDARMONTHID) as amt_bogid_ytd
FROM (
SELECT dd_repunit,
       dd_country,
       dt.CALENDARMONTHID,
       dt.calendaryear,
       sum(amt_bogid) as amt_bogid
FROM fact_netsales ns
     INNER JOIN dim_date dt ON dt.dim_dateid = ns.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Net Sales'
GROUP BY dd_repunit,
         dd_country,
         dt.CALENDARMONTHID,
         dt.calendaryear
) t;

UPDATE fact_netsales f
SET f.amt_bogid_ytd = t.amt_bogid_ytd
FROM fact_netsales f,
     tmp_bogid_ytd t,
     tmp_net_bo min_id
WHERE f.dd_calmonth = t.calendarmonthid
      AND f.dd_repunit = t.dd_repunit
      AND f.dd_country = t.dd_country
      AND f.dd_description = 'Net Sales'
      AND f.fact_netsalesid = min_id.fact_netsalesid;

DROP TABLE IF EXISTS tmp_bogid_ytd;
DROP TABLE IF EXISTS tmp_net_bo;

/*
DROP TABLE IF EXISTS tmp_bogid_ytd
CREATE TABLE tmp_bogid_ytd
AS
SELECT calendaryear,
       calendarmonthid,
       dd_countrycluster,
       dd_region,
       amt_bogidatevalue,
       sum(amt_bogidatevalue) over(partition by dd_countrycluster,dd_region,calendaryear order by CALENDARMONTHID) as amt_bogid_ytd
FROM (
SELECT dt.calendaryear,
       dt.calendarmonthid,
       dd_countrycluster,
	   dd_region,
       sum(amt_bogidatevalue) as amt_bogidatevalue
FROM fact_lifrbo lf,
     dim_date dt
WHERE lf.dim_dateid = dt.dim_dateid
GROUP BY dt.calendaryear,
         dt.calendarmonthid,
         dd_countrycluster,
         dd_region
) t

DROP TABLE IF EXISTS tmp_min_net_bo_ytd
CREATE TABLE tmp_min_net_bo_ytd
AS
SELECT min(FACT_NETSALESID) as FACT_NETSALESID,
       dd_calmonth,
       dd_countrycluster,
       dd_region
FROM fact_netsales f
WHERE dd_description = 'Net Sales'
GROUP BY dd_calmonth,
         dd_countrycluster,
         dd_region

UPDATE fact_netsales f
SET f.amt_bogid_ytd = t.amt_bogid_ytd
FROM fact_netsales f,
     tmp_bogid_ytd t,
     tmp_min_net_bo_ytd min_id
WHERE f.dd_calmonth = t.calendarmonthid
      AND f.dd_countrycluster = t.dd_countrycluster
      AND f.dd_region = t.dd_region
      AND f.dd_description = 'Net Sales'
      AND f.fact_netsalesid = min_id.fact_netsalesid

DROP TABLE IF EXISTS tmp_min_net_bo_ytd
DROP TABLE IF EXISTS tmp_bogid_ytd
*/

/* 29102018 CarmenF change amt values according to the new exchange rate */

update fact_netsales
  set
      amt_CS_TRN_GC = amt_CS_TRN_LC * usd_rate,
      amt_CAMNTGC = amt_CAMNTLC * usd_rate,
      amt_AMTCLYR = amt_AMTLYR * usd_rate,
      amt_CAMTLYR = amt_AMTLCLY * usd_rate,
      amt_GCPLR = amt_CS_TRN_LC * usd_rate,
      amt_CGCPLR = amt_CAMNTLC * usd_rate
  from fact_netsales f, roda_fx_rates, dim_Date d
  where dd_CURKEY_LC = iso_code
  and period = dd_calmonth
  and d.dim_Dateid = f.dim_Dateid and d.datevalue >= '2019-01-01';

update fact_netsales
 set dd_curkey_gc = 'USD'
from fact_netsales f,  dim_Date d
where
 d.dim_Dateid = f.dim_Dateid and d.datevalue >= '2019-01-01';

/* END 29102018 CarmenF change amt values according to the new exchange rate */

DELETE FROM emd586.fact_netsales;
INSERT INTO emd586.fact_netsales SELECT * FROM fact_netsales;