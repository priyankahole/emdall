/*****************************************************************************************************************/
/*   Script         : exa_fact_lifrbo                                                                            */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 13 Feb 2018                                                                                */
/*   Description    : Populating script of fact_lifrbo                                                           */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   13 Feb 2018         CristianT      1.0          Creating the script                                         */
/*   18 Apr 2018         CristianT      1.1          Adding logic for amt_bogid from fact_salesorder_snapshot    */
/*   13 Aug 2018         CristianT      1.2          Adding logic for LIFR YTD values                            */
/*****************************************************************************************************************/


DROP TABLE IF EXISTS tmp_fact_lifrbo;
CREATE TABLE tmp_fact_lifrbo
AS
SELECT *
FROM fact_lifrbo
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_lifrbo;
CREATE TABLE number_fountain_fact_lifrbo LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_lifrbo
SELECT 'fact_lifrbo', ifnull(max(fact_lifrboid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_lifrbo;


/* Preparing the data from import_asia_ceemea file */
INSERT INTO tmp_fact_lifrbo(
fact_lifrboid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_country,
dd_region,
dd_repunitno,
dd_materialnumber,
dd_SBU,
dd_bf,
dd_bf_description,
dd_accordinggidate,
dim_dateid,
ct_totalorderedlines,
ct_ots,
amt_bogidatevalue,
dd_countrycluster,
amt_ytd_ots,
amt_ytd_totalorderedlines,
amt_ytd_bogidatevalue,
dd_lc_currecy_code,
amt_bogidatevalue_LC
)
SELECT (SELECT max_id from number_fountain_fact_lifrbo WHERE table_name = 'fact_lifrbo') + ROW_NUMBER() over(order by '') AS fact_lifrboid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(country, 'Not Set') as dd_country,
       ifnull(region, 'Not Set') as dd_region,
       ifnull(repunitno, 'Not Set') as dd_repunitno,
       ifnull(materialnumber, 'Not Set') as dd_materialnumber,
       ifnull(sbu, 'Not Set') as dd_sbu,
       'Not Set' as dd_bf,
       'Not Set' as dd_bf_description,
       ifnull(accordinggidate, '0001-01-01') as dd_accordinggidate,
       1 as dim_dateid,
       ifnull(totalorderedlines, 0) as ct_totalorderedlines,
       ifnull(ots, 0) as ct_ots,
       ifnull(bogidatevalue, 0) as amt_bogidatevalue,
       'Not Set' as dd_countrycluster,
       0 as amt_ytd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_bogidatevalue,
       'Not Set' as dd_lc_currecy_code,
       0 as amt_bogidatevalue_LC
FROM import_asia_ceemea;

DROP TABLE IF EXISTS tmp_lifrbo_businessfield;
CREATE TABLE tmp_lifrbo_businessfield
AS
SELECT sbu,
       businessline,
       businesslinedesc,
       max(UPPERHIERENDDATE) as max_date,
       ROW_NUMBER() over(partition by sbu, businessline order by max(UPPERHIERENDDATE) desc) as ro_no
FROM import_asia_ceemea
     INNER JOIN emd586.dim_bwproducthierarchy on substr(PRODUCTGROUP, 5, 7) = sbu and business in ('DIV-32', 'DIV.32')
GROUP BY sbu,businessline,businesslinedesc;

UPDATE tmp_fact_lifrbo tmp
SET dd_bf = ifnull(bf.businessline, 'Not Set'),
    dd_bf_description = ifnull(bf.businesslinedesc, 'Not Set')
FROM tmp_fact_lifrbo tmp,
     tmp_lifrbo_businessfield bf
WHERE tmp.dd_sbu = bf.sbu
      AND bf.ro_no = 1;

DROP TABLE IF EXISTS tmp_lifrbo_businessfield;

DELETE FROM number_fountain_fact_lifrbo;

INSERT INTO number_fountain_fact_lifrbo
SELECT 'fact_lifrbo', ifnull(max(fact_lifrboid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_lifrbo;

/* Preparing the data from fact_salesorder from the harmonized project for TempoEU, TempoLA, Lean and Phoenix platforms */
/* Step 1 - Use the correction files to change the values */
DROP TABLE IF EXISTS tmp_chbolifr_fact_salesorder;
CREATE TABLE tmp_chbolifr_fact_salesorder
AS
SELECT so.*
FROM emd586.fact_salesorder so,
     emd586.dim_bwproducthierarchy bw
WHERE so.dim_projectsourceid in (1,2,6,11)
      AND so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
      AND bw.business = 'DIV-32';

/* 18 Apr 2018 CristianT Start: Adding logic for amt_bogid from fact_salesorder_snapshot
This was requested because the BO gi date value might change for orders with GI Date in the past.
In order to have the bo gi date value as accurate as possible we will replace the value from fact_salesorder with the value from fact_salesorder_snapshot.
Logic is to get the value from last day of each snapshot month and map it to each gi date month at salesdocno/salesdocitem/scheduleno level.
*/
DROP TABLE IF EXISTS tmp_ch_bo_snapshot_emd586;
CREATE TABLE tmp_ch_bo_snapshot_emd586
AS
SELECT f.dim_dateidsnapshot,
       f.dim_projectsourceid,
       snps.calendarmonthid,
       snps.datevalue,
       ROW_NUMBER() over(PARTITION BY snps.calendarmonthid, f.dim_projectsourceid ORDER BY snps.datevalue DESC) as rowno
FROM emd586.CH_FACT_SALESORDER_SNAPSHOT f
     INNER JOIN emd586.dim_date snps ON f.dim_dateidsnapshot = snps.dim_dateid
     INNER JOIN emd586.dim_bwproducthierarchy uph ON f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
WHERE 1 = 1
      AND uph.business = 'DIV-32'
      AND f.dim_projectsourceid in (1,2,6,11)
GROUP BY f.dim_dateidsnapshot,
         f.dim_projectsourceid,
         snps.calendarmonthid,
         snps.datevalue;

DELETE FROM tmp_ch_bo_snapshot_emd586 WHERE rowno > 1;

DROP TABLE IF EXISTS tmp_bogivalue_snapshot_emd586;
CREATE TABLE tmp_bogivalue_snapshot_emd586
AS
SELECT distinct tmp.dim_dateidsnapshot,
       tmp.dim_projectsourceid,
       tmp.calendarmonthid,
       f.dd_salesdocno,
       f.dd_salesitemno,
       f.dd_scheduleno,
       f.amt_bogid
FROM tmp_ch_bo_snapshot_emd586 tmp
     INNER JOIN emd586.CH_FACT_SALESORDER_SNAPSHOT f
         ON f.dim_dateidsnapshot = tmp.dim_dateidsnapshot AND f.dim_projectsourceid = tmp.dim_projectsourceid
     INNER JOIN emd586.dim_bwproducthierarchy uph
         ON f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid AND uph.business = 'DIV-32';

UPDATE tmp_chbolifr_fact_salesorder so
SET so.amt_bogid = 0
FROM tmp_chbolifr_fact_salesorder so
     INNER JOIN emd586.dim_date_factory_calendar dt ON dt.dim_dateid = so.dim_newgidate
     INNER JOIN (select distinct calendarmonthid,dim_projectsourceid from tmp_bogivalue_snapshot_emd586) tmp
         ON tmp.calendarmonthid = dt.calendarmonthid
            AND so.dim_projectsourceid = tmp.dim_projectsourceid;

UPDATE tmp_chbolifr_fact_salesorder so
SET so.amt_bogid = ifnull(tmp.amt_bogid, 0)
FROM tmp_chbolifr_fact_salesorder so
     INNER JOIN emd586.dim_date_factory_calendar dt ON dt.dim_dateid = so.dim_newgidate
     INNER JOIN tmp_bogivalue_snapshot_emd586 tmp
         ON tmp.calendarmonthid = dt.calendarmonthid
            AND so.dim_projectsourceid = tmp.dim_projectsourceid
            AND so.dd_salesdocno = tmp.dd_salesdocno
            AND so.dd_salesitemno = tmp.dd_salesitemno
            AND so.dd_scheduleno = tmp.dd_scheduleno;

DROP TABLE IF EXISTS tmp_ch_bo_snapshot_emd586;
DROP TABLE IF EXISTS tmp_bogivalue_snapshot_emd586;

/* 18 Apr 2018 CristianT End */

DROP TABLE IF EXISTS tmp_bo_correction_tempoLA;
CREATE TABLE tmp_bo_correction_tempoLA
AS
SELECT so.fact_salesorderid as fact_salesorderid,
       bo.bogidatevalue as bogidatevalue,
       dt.dim_dateid as dim_newgidate
FROM bo_correction bo
     INNER JOIN tmp_chbolifr_fact_salesorder so ON trim(leading '0' from bo.salesdocno) = trim(leading '0' from so.dd_salesdocno) AND bo.salesdocitem = so.dd_salesitemno
     INNER JOIN emd586.dim_part prt ON so.dim_partid = prt.dim_partid
     LEFT JOIN dim_date dt ON bo.accordinggidate = dt.datevalue AND dt.companycode = 'Not Set'
WHERE 1 = 1
      AND trim(leading '0' from bo.materialnumber) = trim(leading '0' from prt.partnumber)
      AND so.dim_projectsourceid = 2
      AND bo.BO_INSTANCE = 'Tempo LA';

UPDATE tmp_chbolifr_fact_salesorder so
SET amt_bogid = ifnull(tmp.bogidatevalue, 0),
    dim_newgidate = ifnull(tmp.dim_newgidate, 1)
FROM tmp_chbolifr_fact_salesorder so,
     tmp_bo_correction_tempoLA tmp
WHERE so.fact_salesorderid = tmp.fact_salesorderid;

DROP TABLE IF EXISTS tmp_bo_correction_tempoLA;

DROP TABLE IF EXISTS tmp_lifr_correction_tempoLA;
CREATE TABLE tmp_lifr_correction_tempoLA
AS
SELECT so.fact_salesorderid,
       lfr.totalorderedlines as ct_lls,
       lfr.ots as ct_ots,
       dt.dim_dateid as dim_newgidate
FROM lifr_correction lfr
     INNER JOIN tmp_chbolifr_fact_salesorder so ON trim(leading '0' from lfr.salesdocno) = trim(leading '0' from so.dd_salesdocno) AND lfr.salesdocitem = so.dd_salesitemno
     INNER JOIN emd586.dim_part prt ON so.dim_partid = prt.dim_partid
     LEFT JOIN dim_date dt ON lfr.accordinggidate = dt.datevalue AND dt.companycode = 'Not Set'
WHERE 1 = 1
      AND trim(leading '0' from lfr.materialnumber) = trim(leading '0' from prt.partnumber)
      AND so.dim_projectsourceid = 2
      AND lfr.LIFR_INSTANCE = 'Tempo LA';

UPDATE tmp_chbolifr_fact_salesorder so
SET ct_lls = ifnull(tmp.ct_lls, 0),
    ct_ots = ifnull(tmp.ct_ots, 1)
FROM tmp_chbolifr_fact_salesorder so,
     tmp_lifr_correction_tempoLA tmp
WHERE so.fact_salesorderid = tmp.fact_salesorderid;

DROP TABLE IF EXISTS tmp_lifr_correction_tempoLA;

DROP TABLE IF EXISTS tmp_bo_correction_tempoEU;
CREATE TABLE tmp_bo_correction_tempoEU
AS
SELECT so.fact_salesorderid as fact_salesorderid,
       bo.bogidatevalue as bogidatevalue,
       dt.dim_dateid as dim_newgidate
FROM bo_correction bo
     INNER JOIN tmp_chbolifr_fact_salesorder so ON trim(leading '0' from bo.salesdocno) = trim(leading '0' from so.dd_salesdocno) AND bo.salesdocitem = so.dd_salesitemno
     INNER JOIN emd586.dim_part prt ON so.dim_partid = prt.dim_partid
     LEFT JOIN dim_date dt ON bo.accordinggidate = dt.datevalue AND dt.companycode = 'Not Set'
WHERE 1 = 1
      AND trim(leading '0' from bo.materialnumber) = trim(leading '0' from prt.partnumber)
      AND so.dim_projectsourceid = 1
      AND bo.BO_INSTANCE = 'Tempo EU';

UPDATE tmp_chbolifr_fact_salesorder so
SET amt_bogid = ifnull(tmp.bogidatevalue, 0),
    dim_newgidate = ifnull(tmp.dim_newgidate, 1)
FROM tmp_chbolifr_fact_salesorder so,
     tmp_bo_correction_tempoEU tmp
WHERE so.fact_salesorderid = tmp.fact_salesorderid;

DROP TABLE IF EXISTS tmp_bo_correction_tempoEU;

DROP TABLE IF EXISTS tmp_lifr_correction_tempoEU;
CREATE TABLE tmp_lifr_correction_tempoEU
AS
SELECT so.fact_salesorderid,
       lfr.totalorderedlines as ct_lls,
       lfr.ots as ct_ots,
       dt.dim_dateid as dim_newgidate
FROM lifr_correction lfr
     INNER JOIN tmp_chbolifr_fact_salesorder so ON trim(leading '0' from lfr.salesdocno) = trim(leading '0' from so.dd_salesdocno) AND lfr.salesdocitem = so.dd_salesitemno
     INNER JOIN emd586.dim_part prt ON so.dim_partid = prt.dim_partid
     LEFT JOIN dim_date dt ON lfr.accordinggidate = dt.datevalue AND dt.companycode = 'Not Set'
WHERE 1 = 1
      AND trim(leading '0' from lfr.materialnumber) = trim(leading '0' from prt.partnumber)
      AND so.dim_projectsourceid = 1
      AND lfr.LIFR_INSTANCE = 'Tempo EU';

UPDATE tmp_chbolifr_fact_salesorder so
SET ct_lls = ifnull(tmp.ct_lls, 0),
    ct_ots = ifnull(tmp.ct_ots, 1)
FROM tmp_chbolifr_fact_salesorder so,
     tmp_lifr_correction_tempoEU tmp
WHERE so.fact_salesorderid = tmp.fact_salesorderid;

DROP TABLE IF EXISTS tmp_lifr_correction_tempoEU;

DROP TABLE IF EXISTS tmp_bo_correction_Phoenix;
CREATE TABLE tmp_bo_correction_Phoenix
AS
SELECT so.fact_salesorderid as fact_salesorderid,
       bo.bogidatevalue as bogidatevalue,
       dt.dim_dateid as dim_newgidate
FROM bo_correction bo
     INNER JOIN tmp_chbolifr_fact_salesorder so ON trim(leading '0' from bo.salesdocno) = trim(leading '0' from so.dd_salesdocno) AND bo.salesdocitem = so.dd_salesitemno
     INNER JOIN emd586.dim_part prt ON so.dim_partid = prt.dim_partid
     LEFT JOIN dim_date dt ON bo.accordinggidate = dt.datevalue AND dt.companycode = 'Not Set'
WHERE 1 = 1
      AND trim(leading '0' from bo.materialnumber) = trim(leading '0' from prt.partnumber)
      AND so.dim_projectsourceid = 6
      AND bo.BO_INSTANCE = 'Phoenix';

UPDATE tmp_chbolifr_fact_salesorder so
SET amt_bogid = ifnull(tmp.bogidatevalue, 0),
    dim_newgidate = ifnull(tmp.dim_newgidate, 1)
FROM tmp_chbolifr_fact_salesorder so,
     tmp_bo_correction_Phoenix tmp
WHERE so.fact_salesorderid = tmp.fact_salesorderid;

DROP TABLE IF EXISTS tmp_bo_correction_Phoenix;

DROP TABLE IF EXISTS tmp_lifr_correction_Phoenix;
CREATE TABLE tmp_lifr_correction_Phoenix
AS
SELECT so.fact_salesorderid,
       lfr.totalorderedlines as ct_lls,
       lfr.ots as ct_ots,
       dt.dim_dateid as dim_newgidate
FROM lifr_correction lfr
     INNER JOIN tmp_chbolifr_fact_salesorder so ON trim(leading '0' from lfr.salesdocno) = trim(leading '0' from so.dd_salesdocno) AND lfr.salesdocitem = so.dd_salesitemno
     INNER JOIN emd586.dim_part prt ON so.dim_partid = prt.dim_partid
     LEFT JOIN dim_date dt ON lfr.accordinggidate = dt.datevalue AND dt.companycode = 'Not Set'
WHERE 1 = 1
      AND trim(leading '0' from lfr.materialnumber) = trim(leading '0' from prt.partnumber)
      AND so.dim_projectsourceid = 6
      AND lfr.LIFR_INSTANCE = 'Phoenix';

UPDATE tmp_chbolifr_fact_salesorder so
SET ct_lls = ifnull(tmp.ct_lls, 0),
    ct_ots = ifnull(tmp.ct_ots, 1)
FROM tmp_chbolifr_fact_salesorder so,
     tmp_lifr_correction_Phoenix tmp
WHERE so.fact_salesorderid = tmp.fact_salesorderid;

DROP TABLE IF EXISTS tmp_lifr_correction_Phoenix;

DROP TABLE IF EXISTS tmp_bo_correction_Lean;
CREATE TABLE tmp_bo_correction_Lean
AS
SELECT so.fact_salesorderid as fact_salesorderid,
       bo.bogidatevalue as bogidatevalue,
       dt.dim_dateid as dim_newgidate
FROM bo_correction bo
     INNER JOIN tmp_chbolifr_fact_salesorder so ON trim(leading '0' from bo.salesdocno) = trim(leading '0' from so.dd_salesdocno) AND bo.salesdocitem = so.dd_salesitemno
     INNER JOIN emd586.dim_part prt ON so.dim_partid = prt.dim_partid
     LEFT JOIN dim_date dt ON bo.accordinggidate = dt.datevalue AND dt.companycode = 'Not Set'
WHERE 1 = 1
      AND trim(leading '0' from bo.materialnumber) = trim(leading '0' from prt.partnumber)
      AND so.dim_projectsourceid = 11
      AND bo.BO_INSTANCE = 'Lean';

UPDATE tmp_chbolifr_fact_salesorder so
SET amt_bogid = ifnull(tmp.bogidatevalue, 0),
    dim_newgidate = ifnull(tmp.dim_newgidate, 1)
FROM tmp_chbolifr_fact_salesorder so,
     tmp_bo_correction_Lean tmp
WHERE so.fact_salesorderid = tmp.fact_salesorderid;

DROP TABLE IF EXISTS tmp_bo_correction_Lean;

DROP TABLE IF EXISTS tmp_lifr_correction_Lean;
CREATE TABLE tmp_lifr_correction_Lean
AS
SELECT so.fact_salesorderid,
       lfr.totalorderedlines as ct_lls,
       lfr.ots as ct_ots,
       dt.dim_dateid as dim_newgidate
FROM lifr_correction lfr
     INNER JOIN tmp_chbolifr_fact_salesorder so ON trim(leading '0' from lfr.salesdocno) = trim(leading '0' from so.dd_salesdocno) AND lfr.salesdocitem = so.dd_salesitemno
     INNER JOIN emd586.dim_part prt ON so.dim_partid = prt.dim_partid
     LEFT JOIN dim_date dt ON lfr.accordinggidate = dt.datevalue AND dt.companycode = 'Not Set'
WHERE 1 = 1
      AND trim(leading '0' from lfr.materialnumber) = trim(leading '0' from prt.partnumber)
      AND so.dim_projectsourceid = 11
      AND lfr.LIFR_INSTANCE = 'Lean';

UPDATE tmp_chbolifr_fact_salesorder so
SET ct_lls = ifnull(tmp.ct_lls, 0),
    ct_ots = ifnull(tmp.ct_ots, 1)
FROM tmp_chbolifr_fact_salesorder so,
     tmp_lifr_correction_Lean tmp
WHERE so.fact_salesorderid = tmp.fact_salesorderid;

DROP TABLE IF EXISTS tmp_lifr_correction_Lean;

/* Step 2 - Aggregate the data at the same granularity as in import_asia_ceemea table */
DROP TABLE IF EXISTS tmp_lifrbo_sapdata;
CREATE TABLE tmp_lifrbo_sapdata
AS
SELECT dt.CALENDARMONTHID,
       min(dt.datevalue) as dd_accordinggidate,
       bwch.nodehierarchydesclvl2 as dd_region,
       case when so.dim_projectsourceid = 6 then case when euoft.country = 'Not Set' then stp.country else euoft.country end else pl.Country end as dd_country,
       trim(leading '0' FROM cmp.company) AS dd_repunitno,
       prt.partnumber as dd_materialnumber,
       prt.productgroupsbu AS dd_sbu,
       bw.businessline as dd_bf,
       bw.businesslinedesc as dd_bf_description,
       SUM(so.ct_lls) as ct_totalorderedlines,
       SUM(so.ct_ots) as ct_ots,
       SUM(so.amt_bogid * so.amt_exchangerate_gbl) as amt_bogidatevalue,
       cur.currencycode as dd_lc_currecy_code,
       SUM(so.amt_bogid) as amt_bogidatevalue_LC
FROM tmp_chbolifr_fact_salesorder so,
     emd586.dim_date_factory_calendar dt,
     emd586.dim_bwproducthierarchy bw,
     emd586.dim_documentcategory dc,
     emd586.dim_company cmp,
     emd586.dim_part prt,
     emd586.dim_bwhierarchycountry bwch,
     emd586.dim_plant pl,
     emd586.dim_customer euoft,
     emd586.dim_customer stp,
     emd586.dim_currency cur
WHERE 1 = 1
      AND so.Dim_Currencyid_TRA = cur.Dim_Currencyid
      AND so.dim_newgidate = dt.dim_dateid
      AND so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid 
      AND bw.business = 'DIV-32' 
      AND so.dim_documentcategoryid = dc.dim_documentcategoryid
      AND dc.documentcategory = 'C'
      AND so.dim_companyid = cmp.dim_companyid
      AND cmp.companycode <> '0161'
      AND so.dim_partid = prt.dim_partid
      AND dt.datevalue between '2017-01-01' and current_date
      AND so.dim_bwhierarchycountryid = bwch.dim_bwhierarchycountryid
      AND so.dim_projectsourceid in (1,2,6,11)
      AND so.dim_plantid = pl.dim_plantid
      AND so.dim_customerenduserforftrade = euoft.dim_customerid
      AND so.dim_customerid = stp.dim_customerid
GROUP BY dt.CALENDARMONTHID,
         bwch.nodehierarchydesclvl2,
         case when so.dim_projectsourceid = 6 then case when euoft.country = 'Not Set' then stp.country else euoft.country end else pl.Country end,
         trim(leading '0' FROM cmp.company),
         prt.partnumber,
         prt.productgroupsbu,
         bw.businessline,
         bw.businesslinedesc,
         cur.currencycode;

UPDATE tmp_lifrbo_sapdata sap
SET amt_bogidatevalue = amt_bogidatevalue_LC * usd_rate
FROM roda_fx_rates fx,
     tmp_lifrbo_sapdata sap
WHERE sap.dd_lc_currecy_code = fx.iso_code
      AND substr(sap.CALENDARMONTHID, 0, 4) = fx.period
      AND sap.dd_accordinggidate >= '2019-01-01';

INSERT INTO tmp_fact_lifrbo(
fact_lifrboid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_country,
dd_region,
dd_repunitno,
dd_materialnumber,
dd_sbu,
dd_bf,
dd_bf_description,
dd_accordinggidate,
dim_dateid,
ct_totalorderedlines,
ct_ots,
amt_bogidatevalue,
dd_countrycluster,
amt_ytd_ots,
amt_ytd_totalorderedlines,
amt_ytd_bogidatevalue,
dd_lc_currecy_code,
amt_bogidatevalue_LC
)
SELECT (SELECT max_id from number_fountain_fact_lifrbo WHERE table_name = 'fact_lifrbo') + ROW_NUMBER() over(order by '') AS fact_lifrboid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(dd_country, 'Not Set') as dd_country,
       ifnull(dd_region, 'Not Set') as dd_region,
       ifnull(dd_repunitno, 'Not Set') as dd_repunitno,
       ifnull(dd_materialnumber, 'Not Set') as dd_materialnumber,
       ifnull(dd_sbu, 'Not Set') as dd_sbu,
       ifnull(dd_bf, 'Not Set') as dd_bf,
       ifnull(dd_bf_description, 'Not Set') as dd_bf_description,
       ifnull(dd_accordinggidate, '0001-01-01') as dd_accordinggidate,
       1 as dim_dateid,
       ifnull(ct_totalorderedlines, 0) as ct_totalorderedlines,
       ifnull(ct_ots, 0) as ct_ots,
       ifnull(amt_bogidatevalue, 0) as amt_bogidatevalue,
       'Not Set' as dd_countrycluster,
       0 as amt_ytd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_bogidatevalue,
       ifnull(dd_lc_currecy_code, 'Not Set') as dd_lc_currecy_code,
       ifnull(amt_bogidatevalue_LC, 0) as amt_bogidatevalue_LC
FROM tmp_lifrbo_sapdata;


UPDATE tmp_fact_lifrbo f
SET f.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_lifrbo f;

UPDATE tmp_fact_lifrbo f
SET f.dim_dateid = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lifrbo f
WHERE dt.companycode = 'Not Set'
      AND f.dd_accordinggidate = dt.datevalue;

UPDATE tmp_fact_lifrbo tmp
SET dd_countrycluster = ifnull(rc.country_cluster, 'Not Set'),
    dd_region = ifnull(rc.region, 'Not Set')
FROM tmp_fact_lifrbo tmp,
     ns_lifr_bo_fa_lo rc
WHERE trim(leading '0' from tmp.dd_repunitno) = trim(leading '0' from rc.reporting_unit)
       AND tmp.dd_country = rc.country_code;

/* 13 Aug 2018 CristianT Start: Adding logic for LIFR YTD values */
DROP TABLE IF EXISTS tmp_fact_lifrbo_ytd;
CREATE TABLE tmp_fact_lifrbo_ytd
AS
SELECT dd_country,
       dd_region,
       dd_repunitno,
       calendarmonthid,
       calendaryear,
       sum(amt_mtd_totalorderedlines) over(partition by dd_country, dd_region, dd_repunitno, calendaryear order by calendarmonthid) as amt_ytd_totalorderedlines,
       sum(amt_mtd_ots) over(partition by dd_country, dd_region, dd_repunitno, calendaryear order by calendarmonthid) as amt_ytd_ots,
       sum(amt_bogidatevalue) over(partition by dd_country, dd_region, dd_repunitno, calendaryear order by calendarmonthid) as amt_ytd_bogidatevalue
FROM (
SELECT dd_country,
       dd_region,
       dd_repunitno,
       dt.calendarmonthid,
       dt.calendaryear,
       sum(lfr.ct_totalorderedlines) as amt_mtd_totalorderedlines,
       sum(lfr.ct_ots) as amt_mtd_ots,
       sum(lfr.amt_bogidatevalue) as amt_bogidatevalue
FROM tmp_fact_lifrbo lfr
     INNER JOIN dim_date dt on dt.dim_dateid = lfr.dim_dateid
GROUP BY dd_country,
         dd_region,
         dd_repunitno,
         dt.calendarmonthid,
         dt.calendaryear
) t; 

DROP TABLE IF EXISTS tmp_fact_lifrbo_ytd_min;
CREATE TABLE tmp_fact_lifrbo_ytd_min
AS
SELECT min(fact_lifrboid) as fact_lifrboid,
       dd_country,
       dd_region,
       dd_repunitno,
       calendarmonthid
FROM tmp_fact_lifrbo lfr
     INNER JOIN dim_date dt on dt.dim_dateid = lfr.dim_dateid
GROUP BY dd_country,
         dd_region,
         dd_repunitno,
         calendarmonthid;

UPDATE tmp_fact_lifrbo f
SET amt_ytd_totalorderedlines = ifnull(t.amt_ytd_totalorderedlines, 0),
    amt_ytd_ots = ifnull(t.amt_ytd_ots, 0),
    amt_ytd_bogidatevalue = ifnull(t.amt_ytd_bogidatevalue, 0)	
FROM tmp_fact_lifrbo f,
     tmp_fact_lifrbo_ytd t,
     tmp_fact_lifrbo_ytd_min min_id
WHERE min_id.dd_country = t.dd_country
      AND min_id.dd_region = t.dd_region
      AND min_id.dd_repunitno = t.dd_repunitno
      AND min_id.calendarmonthid = t.calendarmonthid
      AND f.fact_lifrboid = min_id.fact_lifrboid;

DROP TABLE IF EXISTS tmp_fact_lifrbo_ytd;
DROP TABLE IF EXISTS tmp_fact_lifrbo_ytd_min;

/* 13 Aug 2018 CristianT End */

TRUNCATE TABLE fact_lifrbo;
INSERT INTO fact_lifrbo(
fact_lifrboid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_country,
dd_region,
dd_repunitno,
dd_materialnumber,
dd_SBU,
dd_bf,
dd_bf_description,
dd_accordinggidate,
dim_dateid,
ct_totalorderedlines,
ct_ots,
amt_bogidatevalue,
dd_countrycluster,
amt_ytd_ots,
amt_ytd_totalorderedlines,
amt_ytd_bogidatevalue,
dd_lc_currecy_code,
amt_bogidatevalue_LC
)
SELECT fact_lifrboid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_country,
       dd_region,
       dd_repunitno,
       dd_materialnumber,
       dd_SBU,
       dd_bf,
       dd_bf_description,
       dd_accordinggidate,
       dim_dateid,
       ct_totalorderedlines,
       ct_ots,
       amt_bogidatevalue,
       dd_countrycluster,
       amt_ytd_ots,
       amt_ytd_totalorderedlines,
       amt_ytd_bogidatevalue,
       dd_lc_currecy_code,
       amt_bogidatevalue_LC
FROM tmp_fact_lifrbo;

DROP TABLE IF EXISTS tmp_fact_lifrbo;

DELETE FROM fact_lifrbo
WHERE dd_accordinggidate >= '2018-10-01' OR dd_accordinggidate < '2018-01-01';

DELETE FROM emd586.fact_lifrbo;
INSERT INTO emd586.fact_lifrbo SELECT * FROM fact_lifrbo;