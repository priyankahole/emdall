

/******************************/
/* WORKING ON THE EXTRACTIONS */
/******************************/

/* BEGIN - Prepare CSV files to eliminate last carriage return when saving file 
as csv in Excel from Mac */

-- table: csv_families, last column: articlenumber

DROP TABLE IF EXISTS tmp_csv_families;
CREATE TABLE tmp_csv_families
AS 
SELECT * FROM csv_families;

UPDATE tmp_csv_families 
SET articlenumber = LEFT(articlenumber,LEN(articlenumber)-1);

/* END - Prepare CSV files to eliminate last carriage return when saving file 
as csv in Excel from Mac */

DROP TABLE IF EXISTS tmp_csv_baselv6;
CREATE TABLE tmp_csv_baselv6
AS 
SELECT 
  materialname AS dd_baselv6CodeArticle,
  u_materialtype AS dd_baselv6Libelle,
  sap_batchno AS dd_baselv6NoLotProduit,
  u_lotnumber AS dd_baselv6NoLotLIMS,
  CASE 
    WHEN sample_createdt IS NULL THEN '0001-01-01' 
    ELSE LEFT(sample_createdt, 10) 
  END 
  AS dd_baselv6creationdate,
  batchstagedesc AS dd_baselv6Categorie,
  stage_name AS dd_baselv6Stage,
  samplestatus AS dd_baselv6StatutEchantillon,
  'Not Set' AS dd_baselv6SpecCondition,
  '0001-01-01' AS dd_baselv6debutanalysedate,
  'Not Set' AS dd_baselv6enattentedepuis,
  category AS dd_baselv6comment,
  category AS dd_baselv6statutpourpriorite,
  category AS dd_baselv6statutpourbilan,
  u_materialfamily AS dd_baselv6family,
  ROW_NUMBER() OVER (PARTITION BY u_lotnumber ORDER BY u_lotnumber) AS dd_multiplierNoLotLims
FROM
  v_mcockpit_suiviencours
WHERE
  UPPER(securitydepartment) = 'SE'
;


/******************************************/
/* WORKING ON THE FIRST FILE FR44_PRIO_LAB*/
/******************************************/


DROP TABLE IF EXISTS tmp_fr44_prio_lab;
CREATE TABLE tmp_fr44_prio_lab
AS
SELECT
  IFNULL(enstehdat,'0001-01-01') AS dd_datelotcreated,
  IFNULL(matnr,'Not Set') AS dd_part,
  IFNULL(charg,'Not Set') AS dd_batchno,
  IFNULL(werk,'Not Set') AS dd_plant,
  IFNULL(lmengeist,0) AS ct_actuallotqty,
  IFNULL(paendterm,'Not Set') AS dd_dateinspectionend,
  IFNULL(art,'Not Set') AS dd_inspectiontype,
  IFNULL(aufnr,'Not Set') AS dd_OrderNo,
  IFNULL(ktextlos,'Not Set') AS dd_shorttext,
  IFNULL(objnr,'Not Set') AS dd_objnr,
  CAST('Not Set' AS VARCHAR(7)) AS dd_usagedecisionflag,
  CAST(1 AS BIGINT) AS dim_plantid,
  CAST(1 AS BIGINT) AS dim_partid,
  CAST(1 AS BIGINT) AS dim_mdg_partid,
  CAST('Not Set' AS VARCHAR(100)) AS dd_articlebatch,
  IFNULL(paendterm,'0001-01-01') AS dd_pfdatelibe,
  'P1' AS dd_priorite,
  CAST('Not Set' AS VARCHAR(100)) AS dd_pflok,
  'PRIO' AS dd_type,
  1 AS dd_multiplierarticlebatch,
  ROW_NUMBER() OVER (PARTITION BY charg ORDER BY charg) AS dd_multiplierbatchno
FROM
  ilse_fr44_prio_lab
WHERE 
  UPPER(werk) = 'FR44'
;

/* BEGIN - Usage Decision Flag Update*/
UPDATE tmp_fr44_prio_lab AS f
SET dd_usagedecisionflag = 'Yes'
FROM tmp_fr44_prio_lab AS f, ilse_jest AS j
WHERE f.dd_objnr = j.objnr
  AND j.stat = 'I0218'
  AND j.inact IS NULL;
/* END - Usage Decision Flag Update*/

/* BEGIN - take only inspection lots without a usage decision*/
DELETE FROM tmp_fr44_prio_lab
WHERE dd_usagedecisionflag = 'Yes';
/* END - take only inspection lots without a usage decision*/

/* BEGIN - plant update */
UPDATE tmp_fr44_prio_lab AS f 
SET f.dim_plantid = dp.dim_plantid
FROM tmp_fr44_prio_lab AS f,
  dim_plant AS dp
WHERE f.dd_plant = dp.plantcode
  AND dp.rowiscurrent = 1;
/* END - plant update */
  
/* BEGIN - part update */
UPDATE tmp_fr44_prio_lab AS f
SET f.dim_partid = dp.dim_partid
FROM tmp_fr44_prio_lab AS f,
  dim_part AS dp
WHERE f.dd_part = dp.partnumber
  AND f.dd_plant = dp.plant
  AND dp.rowiscurrent = 1;
/* END - part update */

/* BEGIN - mdg part update */
DROP TABLE IF EXISTS tmp_ilse_getmdg_partid;

CREATE TABLE tmp_ilse_getmdg_partid 
AS
SELECT pr.dim_partid, MAX(md.dim_mdg_partid) AS dim_mdg_partid
FROM dim_mdg_part AS md, 
  dim_part AS pr
WHERE right('000000000000000000' || md.partnumber, 18) = right('000000000000000000' || pr.partnumber, 18)
GROUP BY dim_partid;

UPDATE tmp_fr44_prio_lab AS f
SET f.dim_mdg_partid = tmp.dim_mdg_partid
FROM tmp_ilse_getmdg_partid AS tmp, 
  tmp_fr44_prio_lab AS f
WHERE f.dim_partid = tmp.dim_partid
  AND f.dim_mdg_partid <> tmp.dim_mdg_partid;
/* END - mdg part update */

/* BEGIN - pflok UPDATE */
UPDATE tmp_fr44_prio_lab AS f
SET f.dd_pflok = 
  CASE 
    WHEN UPPER(dd_shorttext) LIKE '%LOK%ASOK%' THEN 'LOK ASOK'
    WHEN UPPER(dd_shorttext) LIKE '%ASOK%LOK%' THEN 'LOK ASOK'
    WHEN UPPER(dd_shorttext) LIKE '%LOK%' 
      AND UPPER(dd_inspectiontype) LIKE '%QG%' THEN 'LOK AS'
    WHEN UPPER(dd_shorttext) LIKE '%LOK%' THEN 'LOK'
    ELSE 'LAB'
  END
;
/* END - pflok UPDATE */

/* BEGIN - article batch Update */
UPDATE tmp_fr44_prio_lab AS f
SET f.dd_articlebatch = 
  CASE 
    WHEN f.dd_batchno = 'Not Set' THEN '-' 
    ELSE CONCAT('-',f.dd_batchno)
  END;

UPDATE tmp_fr44_prio_lab AS f
SET f.dd_articlebatch = CONCAT(IFNULL(c.articlenumber,''),f.dd_articlebatch)
FROM tmp_fr44_prio_lab AS f
  INNER JOIN dim_mdg_part AS m ON f.dim_mdg_partid = m.dim_mdg_partid
  INNER JOIN tmp_csv_families AS c ON m.partnumber = c.material;
/* END - article batch Update */

/* BEGIN - generate multiplier on the article batch column */
DROP TABLE IF EXISTS tmp_fr44_prio_lab_step1;
CREATE TABLE tmp_fr44_prio_lab_step1
AS
SELECT
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_articlebatch,
  dd_pfdatelibe,
  dd_priorite,
  dd_pflok,
  dd_type,
  ROW_NUMBER() OVER (PARTITION BY dd_articlebatch ORDER BY dd_articlebatch) AS dd_multiplierarticlebatch,
  dd_multiplierbatchno
FROM
  tmp_fr44_prio_lab
;
/* END - generate multiplier on the article batch column */

/********************************************/
/* WORKING ON THE SECOND FILE FR44_LIBE_LAB */
/********************************************/


DROP TABLE IF EXISTS tmp_fr44_libe_lab;
CREATE TABLE tmp_fr44_libe_lab
AS
SELECT
  IFNULL(f.enstehdat,'0001-01-01') AS dd_datelotcreated,
  IFNULL(f.charg,'Not Set') AS dd_batchno,
  IFNULL(f.werk,'Not Set') AS dd_plant,
  CAST(1 AS BIGINT) AS dim_plantid,
  IFNULL(f.paendterm,'0001-01-01') AS dd_dateinspectionend,
  IFNULL(f.aufnr,'Not Set') AS dd_OrderNo,
  IFNULL(f.lmengeist,0) AS ct_actuallotqty,
  IFNULL(f.prueflos,'Not Set') AS dd_inspectionlotno,
  IFNULL(f.matnr,'Not Set') AS dd_part,
  CAST(1 AS BIGINT) AS dim_partid,
  CAST(1 AS BIGINT) AS dim_mdg_partid,
  CONCAT(IFNULL(f.matnr,''),'-',IFNULL(f.charg,'')) AS dd_concatenerpdp,
  IFNULL(f.ktextlos,'Not Set') AS dd_shorttext,
  IFNULL(f.objnr,'Not Set') AS dd_objnr,
  IFNULL(f.vbewertung,'Not Set') AS dd_codevaluation,
  IFNULL(b.dd_baselv6statutpourpriorite,'Not Set') AS dd_statutlabopourpriorite,
  IFNULL(b.dd_baselv6statutpourbilan,'Not Set') AS dd_statutlabopourbilan,
  IFNULL(b.dd_baselv6stage,'Not Set') AS dd_stage,
  CAST('Not Set' AS VARCHAR(100)) AS dd_pole,
  CAST('Not Set' AS VARCHAR(100)) AS dd_priorite,
  CAST('0001-01-01' AS DATE) AS dd_pfdatelibe,
  CAST('Not Set' AS VARCHAR(100)) AS dd_pfinsptype,
  CAST('Not Set' AS VARCHAR(100)) AS dd_pfshorttext,
  CAST('Not Set' AS VARCHAR(100)) AS dd_pflok,
  IFNULL(b.dd_baselv6family,'Not Set') AS dd_famille,
  CAST('Not Set' AS VARCHAR(100)) AS dd_sousfamille,
  CAST('Not Set' AS VARCHAR(100)) AS dd_articlebatch,
  'LIBE' AS dd_type,
  IFNULL(b.dd_multiplierNoLotLims, 1) AS dd_howmanytimes
FROM ilse_fr44_libe_lab AS f 
  LEFT JOIN tmp_csv_baselv6 AS b ON CONCAT('0',f.prueflos) = b.dd_baselv6nolotlims
WHERE 
  UPPER(f.werk) = 'FR44'
  AND UPPER(f.matnr) NOT LIKE 'FR21L%' 
  AND UPPER(f.matnr) NOT LIKE 'FR29G%'
  AND UPPER(f.matnr) NOT LIKE 'FR29S%'
  AND UPPER(f.matnr) NOT LIKE 'FR29F%'
  AND UPPER(f.matnr) NOT LIKE 'FR29L%'
;
  
  
/* BEGIN - plant update */
UPDATE tmp_fr44_libe_lab AS f 
SET f.dim_plantid = dp.dim_plantid
FROM tmp_fr44_libe_lab AS f,
  dim_plant AS dp
WHERE f.dd_plant = dp.plantcode
  AND dp.rowiscurrent = 1;
/* END - plant update */
  
/* BEGIN - part update */
UPDATE tmp_fr44_libe_lab AS f
SET f.dim_partid = dp.dim_partid
FROM tmp_fr44_libe_lab AS f,
  dim_part AS dp
WHERE f.dd_part = dp.partnumber
  AND f.dd_plant = dp.plant
  AND dp.rowiscurrent = 1;
/* END - part update */
  
/* BEGIN - MDG Part update */
UPDATE tmp_fr44_libe_lab AS f
SET f.dim_mdg_partid = tmp.dim_mdg_partid
FROM tmp_ilse_getmdg_partid AS tmp, 
  tmp_fr44_libe_lab AS f
WHERE f.dim_partid = tmp.dim_partid
  AND f.dim_mdg_partid <> tmp.dim_mdg_partid;

DROP TABLE IF EXISTS tmp_ilse_getmdg_partid;
/* END - MDG Part update */

UPDATE tmp_fr44_libe_lab AS f
SET f.dd_pole = IFNULL(c.pole,'Not Set')
FROM tmp_fr44_libe_lab AS f, tmp_csv_families AS c, dim_mdg_part AS m
WHERE f.dim_mdg_partid = m.dim_mdg_partid
  AND m.partnumber = c.material;

UPDATE tmp_fr44_libe_lab AS f
SET f.dd_sousfamille = IFNULL(c.sousfamille,'Not Set')
FROM tmp_fr44_libe_lab AS f, tmp_csv_families AS c, dim_mdg_part AS m
WHERE f.dim_mdg_partid = m.dim_mdg_partid
  AND m.partnumber = c.material;

/* BEGIN - article batch Update */
UPDATE tmp_fr44_libe_lab AS f
SET f.dd_articlebatch = 
  CASE 
    WHEN f.dd_batchno = 'Not Set' THEN '-' 
    ELSE CONCAT('-',f.dd_batchno)
  END;

UPDATE tmp_fr44_libe_lab AS f
SET f.dd_articlebatch = CONCAT(IFNULL(c.articlenumber,''),f.dd_articlebatch)
FROM tmp_fr44_libe_lab AS f
  INNER JOIN dim_mdg_part AS m ON f.dim_mdg_partid = m.dim_mdg_partid
  INNER JOIN tmp_csv_families AS c ON m.partnumber = c.material;
/* END - article batch Update */

/* BEGIN - priorite Update */
UPDATE tmp_fr44_libe_lab AS f
SET f.dd_priorite = 
  CASE 
    WHEN UPPER(dd_famille) = 'MP' 
      OR UPPER(dd_famille) = 'VRAC EXPORT' THEN 'MP-Vrac export'
    ELSE 'Not Set'
  END;

UPDATE tmp_fr44_libe_lab AS f
SET f.dd_priorite = 'P1'
WHERE f.dd_articlebatch IN
  (SELECT dd_articlebatch 
  FROM tmp_fr44_prio_lab);
/* END - priorite Update */


/* BEGIN - add all pf date libe */
DROP TABLE IF EXISTS tmp_fr44_libe_lab_step2;
CREATE TABLE tmp_fr44_libe_lab_step2
AS
SELECT
  l.dd_datelotcreated,
  l.dd_batchno,
  l.dd_plant,
  l.dim_plantid,
  l.dd_dateinspectionend,
  l.dd_OrderNo,
  l.ct_actuallotqty,
  l.dd_inspectionlotno,
  l.dd_part,
  l.dim_partid,
  l.dim_mdg_partid,
  l.dd_concatenerpdp,
  l.dd_shorttext,
  l.dd_objnr,
  l.dd_codevaluation,
  l.dd_statutlabopourpriorite,
  l.dd_statutlabopourbilan,
  l.dd_stage,
  l.dd_pole,
  l.dd_priorite,
  CASE 
    WHEN l.dd_priorite = 'Not Set' THEN '0001-01-01'
    WHEN p.dd_pfdatelibe IS NULL THEN '0001-01-01'
    ELSE p.dd_pfdatelibe 
  END 
  AS dd_pfdatelibe,
  l.dd_pfinsptype,
  l.dd_pfshorttext,
  l.dd_pflok,
  l.dd_famille,
  l.dd_sousfamille,
  l.dd_articlebatch,
  l.dd_type,
  l.dd_howmanytimes * IFNULL(p.dd_multiplierarticlebatch, 1) AS dd_howmanytimes
FROM tmp_fr44_libe_lab AS l 
  LEFT JOIN tmp_fr44_prio_lab_step1 AS p ON l.dd_articlebatch = p.dd_articlebatch
    AND l.dd_articlebatch <> '-'
    AND p.dd_articlebatch <> '-'
    AND l.dd_priorite <> 'Not Set'
;
/* END - add all pf date libe */


/* BEGIN - add all batches */

DROP TABLE IF EXISTS tmp_fr44_libe_lab_step3;
CREATE TABLE tmp_fr44_libe_lab_step3
AS
SELECT
  l.dd_datelotcreated,
  l.dd_batchno,
  l.dd_plant,
  l.dim_plantid,
  l.dd_dateinspectionend,
  l.dd_OrderNo,
  l.ct_actuallotqty,
  l.dd_inspectionlotno,
  l.dd_part,
  l.dim_partid,
  l.dim_mdg_partid,
  l.dd_concatenerpdp,
  l.dd_shorttext,
  l.dd_objnr,
  l.dd_codevaluation,
  l.dd_statutlabopourpriorite,
  l.dd_statutlabopourbilan,
  l.dd_stage,
  l.dd_pole,
  l.dd_priorite,
  l.dd_pfdatelibe,
  CASE 
    WHEN l.dd_priorite = 'Not Set' THEN 'Not Set'
    WHEN l.dd_priorite = 'MP-Vrac export' THEN 'Not Set'
    WHEN l.dd_priorite = 'P1' AND p.dd_inspectiontype IS NULL THEN 'Not Set'
    WHEN l.dd_priorite = 'P1' THEN p.dd_inspectiontype
    ELSE 'Not Set' 
  END
  AS dd_pfinsptype,
  CASE 
    WHEN l.dd_priorite = 'Not Set' THEN 'Not Set'
    WHEN l.dd_priorite = 'MP-Vrac export' THEN 'Not Set'
    WHEN l.dd_priorite = 'P1' AND p.dd_shorttext IS NULL THEN 'Not Set'
    WHEN l.dd_priorite = 'P1' THEN p.dd_shorttext
    ELSE 'Not Set' 
  END
  AS dd_pfshorttext,
  l.dd_pflok,
  l.dd_famille,
  l.dd_sousfamille,
  l.dd_articlebatch,
  l.dd_type,
  l.dd_howmanytimes * IFNULL(p.dd_multiplierbatchno, 1) AS dd_howmanytimes
FROM tmp_fr44_libe_lab_step2 AS l 
  LEFT JOIN tmp_fr44_prio_lab_step1 AS p ON l.dd_batchno = p.dd_batchno
;

/* END - add all batches */

/* BEGIN - pflok UPDATE */
UPDATE tmp_fr44_libe_lab_step3 AS f
SET f.dd_pflok = 
  CASE 
    WHEN UPPER(dd_shorttext) LIKE '%LOK%ASOK%' THEN 'LOK ASOK'
    WHEN UPPER(dd_shorttext) LIKE '%ASOK%LOK%' THEN 'LOK ASOK'
    WHEN UPPER(dd_shorttext) LIKE '%LOK%' 
      AND UPPER(dd_pfinsptype) LIKE '%QG%' THEN 'LOK AS'
    WHEN UPPER(dd_shorttext) LIKE '%LOK%' THEN 'LOK'
    ELSE 'LAB'
  END
;
/* END - pflok UPDATE */


/********************************************/
/*                FACT CREATION             */
/********************************************/


DROP TABLE IF EXISTS number_fountain_fact_inspectionlotsemoy;
CREATE TABLE number_fountain_fact_inspectionlotsemoy 
LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoy;
CREATE TABLE tmp_fact_inspectionlotsemoy 
LIKE fact_inspectionlotsemoy INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_inspectionlotsemoy 
WHERE table_name = 'fact_inspectionlotsemoy';	

INSERT INTO number_fountain_fact_inspectionlotsemoy
SELECT 'fact_inspectionlotsemoy', IFNULL(MAX(f.fact_inspectionlotsemoyid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_inspectionlotsemoy AS f;

-- first insert from the prio lab table

INSERT INTO tmp_fact_inspectionlotsemoy(
  fact_inspectionlotsemoyid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_articlebatch,
  dd_pfdatelibe,
  dd_priorite,
  dd_pflok,
  dd_type,
  dd_inspectionlotno,
  dd_concatenerpdp,
  dd_codevaluation,
  dd_statutlabopourpriorite,
  dd_statutlabopourbilan,
  dd_stage,
  dd_pole,
  dd_pfshorttext,
  dd_famille,
  dd_sousfamille,
  dim_dateidlotcreated,
  dim_dateidinspectionend,
  dim_pfdateidlibe,
  dd_howmanytimes,
  dd_statutlabopourbilanorder,
  dd_familleorder,
  dd_prioriteorder,
  dd_snapshotdate,
  dim_snapshotdateid,
  dd_statutlabopourbilanorderforchart,
  dd_statutlabopourbilanforchart,
  ct_avg,
  ct_min,
  ct_max,
  ct_avgtwicestddev,
  ct_avgthricestddev,
  dd_newpriorite,
  dd_newprioriteorder
)
SELECT
  (SELECT max_id
    FROM number_fountain_fact_inspectionlotsemoy 
    WHERE table_name = 'fact_inspectionlotsemoy') + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoyid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  CURRENT_TIMESTAMP AS dw_insert_date, 
  CURRENT_TIMESTAMP AS dw_update_date,
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_articlebatch,
  dd_pfdatelibe,
  dd_priorite,
  dd_pflok,
  dd_type,
  'Not Set' AS dd_inspectionlotno,
  'Not Set' AS dd_concatenerpdp,
  'Not Set' AS dd_codevaluation,
  'Not Set' AS dd_statutlabopourpriorite,
  'Not Set' AS dd_statutlabopourbilan,
  'Not Set' AS dd_stage,
  'Not Set' AS dd_pole,
  'Not Set' AS dd_pfshorttext,
  'Not Set' AS dd_famille,
  'Not Set' AS dd_sousfamille,
  1 AS dim_dateidlotcreated,
  1 AS dim_dateidinspectionend,
  1 AS dim_pfdateidlibe,
  1 AS dd_howmanytimes,
  0 AS dd_statutlabopourbilanorder,
  0 AS dd_familleorder,
  0 AS dd_prioriteorder,
  CASE 
    WHEN EXTRACT(hour FROM CURRENT_TIMESTAMP) BETWEEN 0 AND 18 THEN 
      CURRENT_DATE - 1 
    ELSE CURRENT_DATE 
  END AS dd_snapshotdate,
  1 AS dim_snapshotdateid,
  0 AS dd_statutlabopourbilanorderforchart,
  'Not Set' as dd_statutlabopourbilanforchart,
  0 AS ct_avg,
  0 AS ct_min,
  0 AS ct_max,
  0 AS ct_avgtwicestddev,
  0 AS ct_avgthricestddev,
  'Not Set' AS dd_newpriorite,
  1 AS dd_newprioriteorder
FROM
  tmp_fr44_prio_lab_step1
;


-- second insert from the libe lab table

DELETE FROM number_fountain_fact_inspectionlotsemoy 
WHERE table_name = 'fact_inspectionlotsemoy';	

INSERT INTO number_fountain_fact_inspectionlotsemoy
SELECT 'fact_inspectionlotsemoy', IFNULL(MAX(f.fact_inspectionlotsemoyid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM tmp_fact_inspectionlotsemoy AS f;

INSERT INTO tmp_fact_inspectionlotsemoy(
  fact_inspectionlotsemoyid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_articlebatch,
  dd_pfdatelibe,
  dd_priorite,
  dd_pflok,
  dd_type,
  dd_inspectionlotno,
  dd_concatenerpdp,
  dd_codevaluation,
  dd_statutlabopourpriorite,
  dd_statutlabopourbilan,
  dd_stage,
  dd_pole,
  dd_pfshorttext,
  dd_famille,
  dd_sousfamille,
  dim_dateidlotcreated,
  dim_dateidinspectionend,
  dim_pfdateidlibe,
  dd_howmanytimes,
  dd_statutlabopourbilanorder,
  dd_familleorder,
  dd_prioriteorder,
  dd_snapshotdate,
  dim_snapshotdateid,
  dd_statutlabopourbilanorderforchart,
  dd_statutlabopourbilanforchart,
  ct_avg,
  ct_min,
  ct_max,
  ct_avgtwicestddev,
  ct_avgthricestddev,
  dd_newpriorite,
  dd_newprioriteorder
)
SELECT
  (SELECT max_id
    FROM number_fountain_fact_inspectionlotsemoy 
    WHERE table_name = 'fact_inspectionlotsemoy') + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoyid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  CURRENT_TIMESTAMP AS dw_insert_date, 
  CURRENT_TIMESTAMP AS dw_update_date,
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_pfinsptype AS dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  'Not Set' AS dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_articlebatch,
  dd_pfdatelibe,
  dd_priorite,
  dd_pflok,
  dd_type,
  dd_inspectionlotno,
  dd_concatenerpdp,
  dd_codevaluation,
  dd_statutlabopourpriorite,
  dd_statutlabopourbilan,
  dd_stage,
  dd_pole,
  dd_pfshorttext,
  dd_famille,
  dd_sousfamille,
  1 AS dim_dateidlotcreated,
  1 AS dim_dateidinspectionend,
  1 AS dim_pfdateidlibe,
  dd_howmanytimes,
  0 AS dd_statutlabopourbilanorder,
  0 AS dd_familleorder,
  0 AS dd_prioriteorder,
  CASE 
    WHEN EXTRACT(hour FROM CURRENT_TIMESTAMP) BETWEEN 0 AND 18 THEN 
      CURRENT_DATE - 1 
    ELSE CURRENT_DATE 
  END AS dd_snapshotdate,
  1 AS dim_snapshotdateid,
  0 AS dd_statutlabopourbilanorderforchart,
  'Not Set' as dd_statutlabopourbilanforchart,
  0 AS ct_avg,
  0 AS ct_min,
  0 AS ct_max,
  0 AS ct_avgtwicestddev,
  0 AS ct_avgthricestddev,
  'Not Set' AS dd_newpriorite,
  1 AS dd_newprioriteorder
FROM
  tmp_fr44_libe_lab_step3
;


/* BEGIN - dim dates update */
UPDATE tmp_fact_inspectionlotsemoy AS f
SET f.dim_dateidlotcreated = dd.dim_dateid
FROM  tmp_fact_inspectionlotsemoy AS f, dim_date AS dd 
WHERE f.dd_datelotcreated = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_dateidlotcreated <> dd.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoy AS f
SET f.dim_dateidinspectionend = dd.dim_dateid
FROM  tmp_fact_inspectionlotsemoy AS f, dim_date AS dd 
WHERE f.dd_dateinspectionend = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_dateidinspectionend <> dd.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoy AS f
SET f.dim_pfdateidlibe = dd.dim_dateid
FROM  tmp_fact_inspectionlotsemoy AS f, dim_date AS dd 
WHERE f.dd_pfdatelibe = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_pfdateidlibe <> dd.dim_dateid;
/* END - dim dates update */


/* BEGIN - update order for UI fix */

UPDATE tmp_fact_inspectionlotsemoy AS f
SET dd_familleorder = 
  CASE 
    WHEN upper(dd_famille) = 'CYANOKIT' THEN 1
    WHEN upper(dd_famille) = 'CONTRÔLE SUP.' THEN 2
    WHEN upper(dd_famille) = 'GLUCOPHAGE JP' THEN 3
    WHEN upper(dd_famille) = 'CAMPRAL' THEN 4
    WHEN upper(dd_famille) = 'GLUCOPHAGE' THEN 5
    WHEN upper(dd_famille) = 'GLUCOPHAGE XR' THEN 6
    WHEN upper(dd_famille) = 'GLUCOVANCE' THEN 7
    WHEN upper(dd_famille) = 'VRAC EXPORT' THEN 8
    WHEN upper(dd_famille) = 'MP' THEN 9
    WHEN upper(dd_famille) = 'NOT SET' THEN 10
    ELSE 999
  END;

UPDATE tmp_fact_inspectionlotsemoy AS f
SET dd_prioriteorder = 
  CASE 
    WHEN dd_priorite = 'Not Set' THEN 1
    WHEN dd_priorite = 'MP-Vrac export' THEN 2
    WHEN dd_priorite = 'P1' THEN 3
    ELSE 999
  END;

UPDATE tmp_fact_inspectionlotsemoy AS f
SET dd_statutlabopourbilanforchart = dd_statutlabopourbilan;

/* END - update order for UI fix */


/* BEGIN - update snapshot dateid */
UPDATE tmp_fact_inspectionlotsemoy AS f
SET dim_snapshotdateid = dd.dim_dateid
FROM  tmp_fact_inspectionlotsemoy AS f, dim_date AS dd 
WHERE f.dd_snapshotdate = dd.datevalue 
  AND dd.CompanyCode = 'Not Set';
/* END - update snapshot dateid */

-- DELETE FROM fact_inspectionlotsemoy

/* BEGIN - snapshot logic */
DELETE FROM fact_inspectionlotsemoy
WHERE dd_snapshotdate = 
  CASE 
    WHEN EXTRACT(hour FROM CURRENT_TIMESTAMP) BETWEEN 0 AND 18 THEN 
      CURRENT_DATE - 1 
    ELSE CURRENT_DATE 
  END;
/* END - snapshot logic */


INSERT INTO fact_inspectionlotsemoy(
  fact_inspectionlotsemoyid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_articlebatch,
  dd_pfdatelibe,
  dd_priorite,
  dd_pflok,
  dd_type,
  dd_inspectionlotno,
  dd_concatenerpdp,
  dd_codevaluation,
  dd_statutlabopourpriorite,
  dd_statutlabopourbilan,
  dd_stage,
  dd_pole,
  dd_pfshorttext,
  dd_famille,
  dd_sousfamille,
  dim_dateidlotcreated,
  dim_dateidinspectionend,
  dim_pfdateidlibe,
  dd_howmanytimes,
  dd_statutlabopourbilanorder,
  dd_familleorder,
  dd_prioriteorder,
  dd_snapshotdate,
  dim_snapshotdateid,
  dd_statutlabopourbilanorderforchart,
  dd_statutlabopourbilanforchart,
  ct_avg,
  ct_min,
  ct_max,
  ct_avgtwicestddev,
  ct_avgthricestddev,
  dd_newpriorite,
  dd_newprioriteorder
)
SELECT
  fact_inspectionlotsemoyid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_articlebatch,
  dd_pfdatelibe,
  dd_priorite,
  dd_pflok,
  dd_type,
  dd_inspectionlotno,
  dd_concatenerpdp,
  dd_codevaluation,
  dd_statutlabopourpriorite,
  dd_statutlabopourbilan,
  dd_stage,
  dd_pole,
  dd_pfshorttext,
  dd_famille,
  dd_sousfamille,
  dim_dateidlotcreated,
  dim_dateidinspectionend,
  dim_pfdateidlibe,
  dd_howmanytimes,
  dd_statutlabopourbilanorder,
  dd_familleorder,
  dd_prioriteorder,
  dd_snapshotdate,
  dim_snapshotdateid,
  dd_statutlabopourbilanorderforchart,
  dd_statutlabopourbilanforchart,
  ct_avg,
  ct_min,
  ct_max,
  ct_avgtwicestddev,
  ct_avgthricestddev,
  dd_newpriorite,
  dd_newprioriteorder
FROM
  tmp_fact_inspectionlotsemoy
;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoy;


/* BEGIN - logic for AVG,MIN,MAX,STDDEV */

DELETE FROM fact_inspectionlotsemoy as f
WHERE f.dd_statutlabopourbilan = 'Total';

UPDATE fact_inspectionlotsemoy as f
SET 
  f.ct_avg = 0,
  f.ct_min = 0,
  f.ct_max = 0,
  f.ct_avgtwicestddev = 0,
  f.ct_avgthricestddev = 0;

DROP TABLE IF EXISTS tmp_getavgminmaxstg;
CREATE TABLE tmp_getavgminmaxstg
AS
SELECT 
  dd_statutlabopourbilan,
  avg(numberOfbatches) AS ct_avg,
  min(numberOfbatches) AS ct_min,
  max(numberOfbatches) AS ct_max,
  avg(numberOfbatches) + 2 * stddev(numberOfbatches) AS ct_avgtwicestddev,
  avg(numberOfbatches) + 3 * stddev(numberOfbatches) AS ct_avgthricestddev
FROM
  (SELECT dd_statutlabopourbilan, dd_snapshotdate, count(*) AS numberOfbatches
  FROM fact_inspectionlotsemoy AS f 
    INNER JOIN dim_part AS dp ON f.dim_partid = dp.dim_partid
  WHERE upper(f.dd_type) = 'LIBE'
    AND f.dd_howmanytimes = 1
    AND upper(f.dd_famille) IN ('CYANOKIT','CONTRÔLE SUP.','GLUCOPHAGE JP','CAMPRAL',
      'GLUCOPHAGE','GLUCOPHAGE XR','GLUCOVANCE','VRAC EXPORT',
      'MP','PF SANS LIMS')
    AND f.dd_plant = 'FR44'
    AND f.dd_statutlabopourbilan <> 'Not Set'
    AND dp.partnumber NOT LIKE 'FR21L%'
    AND dp.partnumber NOT LIKE 'FR29G%'
    AND dp.partnumber NOT LIKE 'FR29S%'
    AND dp.partnumber NOT LIKE 'FR29F%'
    AND dp.partnumber NOT LIKE 'FR29L%'
  GROUP BY 
    f.dd_statutlabopourbilan,
    f.dd_snapshotdate)
GROUP BY dd_statutlabopourbilan
;

INSERT INTO tmp_getavgminmaxstg(
  dd_statutlabopourbilan,
  ct_avg,
  ct_min,
  ct_max,
  ct_avgtwicestddev,
  ct_avgthricestddev
)
SELECT 
  'Total' AS dd_statutlabopourbilan,
  avg(numberOfbatches) AS ct_avg,
  min(numberOfbatches) AS ct_min,
  max(numberOfbatches) AS ct_max,
  avg(numberOfbatches) + 2 * stddev(numberOfbatches) AS ct_avgtwicestddev,
  avg(numberOfbatches) + 3 * stddev(numberOfbatches) AS ct_avgthricestddev
FROM
  (SELECT dd_statutlabopourbilan,count(*) AS numberOfbatches
  FROM fact_inspectionlotsemoy AS f 
    INNER JOIN dim_part AS dp ON f.dim_partid = dp.dim_partid
  WHERE upper(f.dd_type) = 'LIBE'
    AND f.dd_howmanytimes = 1
    AND upper(f.dd_famille) IN ('CYANOKIT','CONTRÔLE SUP.','GLUCOPHAGE JP','CAMPRAL',
      'GLUCOPHAGE','GLUCOPHAGE XR','GLUCOVANCE','VRAC EXPORT','MP',
      'PF SANS LIMS')
    AND f.dd_plant = 'FR44'
    AND f.dd_statutlabopourbilan <> 'Not Set'
    AND dp.partnumber NOT LIKE 'FR21L%'
    AND dp.partnumber NOT LIKE 'FR29G%'
    AND dp.partnumber NOT LIKE 'FR29S%'
    AND dp.partnumber NOT LIKE 'FR29F%'
    AND dp.partnumber NOT LIKE 'FR29L%'
  GROUP BY 
    f.dd_statutlabopourbilan)
;

DELETE FROM number_fountain_fact_inspectionlotsemoy 
WHERE table_name = 'fact_inspectionlotsemoy';	

INSERT INTO number_fountain_fact_inspectionlotsemoy
SELECT 'fact_inspectionlotsemoy', IFNULL(MAX(f.fact_inspectionlotsemoyid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_inspectionlotsemoy AS f;

INSERT INTO fact_inspectionlotsemoy(
  fact_inspectionlotsemoyid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_datelotcreated,
  dd_part,
  dd_batchno,
  dd_plant,
  ct_actuallotqty,
  dd_dateinspectionend,
  dd_inspectiontype,
  dd_OrderNo,
  dd_shorttext,
  dd_objnr,
  dd_usagedecisionflag,
  dim_plantid,
  dim_partid,
  dim_mdg_partid,
  dd_articlebatch,
  dd_pfdatelibe,
  dd_priorite,
  dd_pflok,
  dd_type,
  dd_inspectionlotno,
  dd_concatenerpdp,
  dd_codevaluation,
  dd_statutlabopourpriorite,
  dd_statutlabopourbilan,
  dd_stage,
  dd_pole,
  dd_pfshorttext,
  dd_famille,
  dd_sousfamille,
  dim_dateidlotcreated,
  dim_dateidinspectionend,
  dim_pfdateidlibe,
  dd_howmanytimes,
  dd_statutlabopourbilanorder,
  dd_familleorder,
  dd_prioriteorder,
  dd_snapshotdate,
  dim_snapshotdateid,
  dd_statutlabopourbilanorderforchart,
  dd_statutlabopourbilanforchart,
  ct_avg,
  ct_min,
  ct_max,
  ct_avgtwicestddev,
  ct_avgthricestddev,
  dd_newpriorite,
  dd_newprioriteorder
)
SELECT
  (SELECT max_id
    FROM number_fountain_fact_inspectionlotsemoy 
    WHERE table_name = 'fact_inspectionlotsemoy') + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoyid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  CURRENT_TIMESTAMP AS dw_insert_date, 
  CURRENT_TIMESTAMP AS dw_update_date,
  '0001-01-01' AS dd_datelotcreated,
  'Not Set' AS dd_part,
  'Not Set' AS dd_batchno,
  'FR44' AS dd_plant,
  0 AS ct_actuallotqty,
  '0001-01-01' AS dd_dateinspectionend,
  'Not Set' AS dd_inspectiontype,
  'Not Set' AS dd_OrderNo,
  'Not Set' AS dd_shorttext,
  'Not Set' AS dd_objnr,
  'Not Set' AS dd_usagedecisionflag,
  (SELECT dim_plantid FROM dim_plant WHERE plantcode = 'FR44') AS dim_plantid,
  1 AS dim_partid,
  1 AS dim_mdg_partid,
  'Not Set' AS dd_articlebatch,
  '0001-01-01' AS dd_pfdatelibe,
  'Not Set' AS dd_priorite,
  'Not Set' AS dd_pflok,
  'LIBE' AS dd_type,
  'Not Set' AS dd_inspectionlotno,
  'Not Set' AS dd_concatenerpdp,
  'Not Set' AS dd_codevaluation,
  'Not Set' AS dd_statutlabopourpriorite,
  'Total' AS dd_statutlabopourbilan,
  'Not Set' AS dd_stage,
  'Not Set' AS dd_pole,
  'Not Set' AS dd_pfshorttext,
  'CYANOKIT' AS dd_famille,
  'Not Set' AS dd_sousfamille,
  1 AS dim_dateidlotcreated,
  1 AS dim_dateidinspectionend,
  1 AS dim_pfdateidlibe,
  1 AS dd_howmanytimes,
  0 AS dd_statutlabopourbilanorder,
  0 AS dd_familleorder,
  0 AS dd_prioriteorder,
  CASE 
    WHEN EXTRACT(hour FROM CURRENT_TIMESTAMP) BETWEEN 0 AND 18 THEN 
      CURRENT_DATE - 1 
    ELSE CURRENT_DATE 
  END AS dd_snapshotdate,
  1 AS dim_snapshotdateid,
  0 AS dd_statutlabopourbilanorderforchart,
  'Not Set' as dd_statutlabopourbilanforchart,
  0 AS ct_avg,
  0 AS ct_min,
  0 AS ct_max,
  0 AS ct_avgtwicestddev,
  0 AS ct_avgthricestddev,
  'Not Set' AS dd_newpriorite,
  1 AS dd_newprioriteorder  
FROM (SELECT 1) AS dummyTable
;

DROP TABLE IF EXISTS tmp_getmaxidforupdatesemoy;
CREATE TABLE tmp_getmaxidforupdatesemoy
AS
SELECT
  dd_statutlabopourbilan AS dd_statutlabopourbilan,
  MAX(fact_inspectionlotsemoyid) AS fact_inspectionlotsemoyid
FROM fact_inspectionlotsemoy AS f 
  INNER JOIN dim_part AS dp ON f.dim_partid = dp.dim_partid
WHERE upper(f.dd_type) = 'LIBE'
  AND f.dd_howmanytimes = 1
  AND upper(f.dd_famille) IN ('CYANOKIT','CONTRÔLE SUP.','GLUCOPHAGE JP','CAMPRAL',
    'GLUCOPHAGE','GLUCOPHAGE XR','GLUCOVANCE','VRAC EXPORT','MP',
    'PF SANS LIMS')
  AND f.dd_plant = 'FR44'
  AND f.dd_statutlabopourbilan <> 'Not Set'
  AND dp.partnumber NOT LIKE 'FR21L%'
  AND dp.partnumber NOT LIKE 'FR29G%'
  AND dp.partnumber NOT LIKE 'FR29S%'
  AND dp.partnumber NOT LIKE 'FR29F%'
  AND dp.partnumber NOT LIKE 'FR29L%'
GROUP BY dd_statutlabopourbilan
;

UPDATE fact_inspectionlotsemoy AS f 
SET 
  f.ct_avg = ta.ct_avg,
  f.ct_min = ta.ct_min,
  f.ct_max = ta.ct_max,
  f.ct_avgtwicestddev = ta.ct_avgtwicestddev,
  f.ct_avgthricestddev = ta.ct_avgthricestddev
FROM 
  fact_inspectionlotsemoy AS f, 
  tmp_getmaxidforupdatesemoy AS t,
  tmp_getavgminmaxstg AS ta
WHERE f.fact_inspectionlotsemoyid = t.fact_inspectionlotsemoyid
  AND t.dd_statutlabopourbilan = ta.dd_statutlabopourbilan;

/* END - logic for AVG,MIN,MAX,STDDEV */

/* BEGIN - update logic for UI fix */

UPDATE fact_inspectionlotsemoy AS f
SET dd_statutlabopourbilanorder = 
  CASE 
    WHEN dd_statutlabopourbilan = 'Attente libération lot' THEN 1
    WHEN dd_statutlabopourbilan = 'Attente conclusion analytique' THEN 2
    WHEN dd_statutlabopourbilan = 'Attente vérification' THEN 3
    WHEN dd_statutlabopourbilan = 'Attente fin analyse' THEN 4
    WHEN dd_statutlabopourbilan = 'Attente prise en charge' THEN 5
    WHEN dd_statutlabopourbilan = 'Not Set' THEN 6
    WHEN dd_statutlabopourbilan = 'Total' THEN 7
    ELSE 999
  END;

UPDATE fact_inspectionlotsemoy AS f
SET dd_statutlabopourbilanorderforchart = 
  CASE 
    WHEN dd_statutlabopourbilan = 'Attente libération lot' THEN 1
    WHEN dd_statutlabopourbilan = 'Attente conclusion analytique' THEN 2
    WHEN dd_statutlabopourbilan = 'Attente vérification' THEN 3
    WHEN dd_statutlabopourbilan = 'Attente fin analyse' THEN 4
    WHEN dd_statutlabopourbilan = 'Attente prise en charge' THEN 5
    WHEN dd_statutlabopourbilan = 'Not Set' THEN 6
    WHEN dd_statutlabopourbilan = 'Total' THEN 7
    ELSE 999
  END;

UPDATE fact_inspectionlotsemoy AS f
SET dd_newpriorite = 
  CASE
    WHEN dd_priorite = 'Not Set' THEN 'Priorité 2'
    WHEN dd_priorite = 'P1' THEN 'Priorité 1'
    WHEN dd_priorite = 'MP-Vrac export' THEN 'MP'
    ELSE 'Not Set'
  END
;

UPDATE fact_inspectionlotsemoy AS f
SET dd_newprioriteorder = 
  CASE
    WHEN dd_newpriorite = 'Priorité 2' THEN 1
    WHEN dd_newpriorite = 'Priorité 1' THEN 2
    WHEN dd_newpriorite = 'MP' THEN 3
    ELSE 1
  END
;

/* END - update logic for UI fix */

DELETE FROM emd586.fact_inspectionlotsemoy;

INSERT INTO emd586.fact_inspectionlotsemoy
SELECT *
FROM fact_inspectionlotsemoy;
