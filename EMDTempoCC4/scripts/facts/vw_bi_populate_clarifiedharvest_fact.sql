/******************************************************************************************************************/
/*   Script         : bi_populate_clarifiedharvest_fact                                                           */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_clarifiedharvest                                                  */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   31 Jan 2017         CristianT       1.0               Creating the script.                                   */
/*   10 Feb 2017         CristianT       1.1               Calculating the AVG of previous years                  */
/******************************************************************************************************************/


DROP TABLE IF EXISTS tmp_fact_clarifiedharvest;
CREATE TABLE tmp_fact_clarifiedharvest
AS
SELECT *
FROM fact_clarifiedharvest
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_clarifiedharvest';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_clarifiedharvest', ifnull(max(fact_clarifiedharvestid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_clarifiedharvest;

INSERT INTO tmp_fact_clarifiedharvest(
fact_clarifiedharvestid,
dd_harvest_batch_id,
amt_harvest_hat_pa_hplc,
amt_harvest_hat_yield_cla_params_volume1,
amt_quantity_total,
dim_dateidsetdate,
dd_parameter_set_date,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_clarifiedharvest') + ROW_NUMBER() over(order by '') as fact_clarifiedharvestid,
       ifnull(harvest_batch_id, 'Not Set') as dd_harvest_batch_id,
       ifnull(harvest_hat_analytics_pa_hplc, 0) as amt_harvest_hat_pa_hplc,
       ifnull(harvesT_hat_yield_cla_params_volume1, 0) as amt_harvest_hat_yield_cla_params_volume1,
       ifnull((harvest_hat_analytics_pa_hplc * harvesT_hat_yield_cla_params_volume1 / 1000), 0) as amt_quantity_total,
       1 as dim_dateidsetdate,
       ifnull(parameter_set_date, 'Not Set') as dd_parameter_set_date,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM clarified_harvest
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
      AND harvest_batch_id is not null;

UPDATE tmp_fact_clarifiedharvest tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_clarifiedharvest tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = substr(tmp.dd_parameter_set_date, 0,10);


/* 10 Feb 2016 CristianT Start: Calculating the AVG of previous years */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_clarifiedharvest';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_clarifiedharvest', ifnull(max(fact_clarifiedharvestid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_clarifiedharvest;


INSERT INTO tmp_fact_clarifiedharvest(
fact_clarifiedharvestid,
dd_harvest_batch_id,
amt_harvest_hat_pa_hplc,
amt_harvest_hat_yield_cla_params_volume1,
amt_quantity_total,
dim_dateidsetdate,
dd_parameter_set_date,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_clarifiedharvest') + ROW_NUMBER() over(order by '') as fact_clarifiedharvestid,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_harvest_batch_id,
       ifnull(AVG(harvest_hat_analytics_pa_hplc), 0) as amt_harvest_hat_pa_hplc,
       ifnull(AVG(harvesT_hat_yield_cla_params_volume1), 0) as amt_harvest_hat_yield_cla_params_volume1,
       ifnull(AVG(harvest_hat_analytics_pa_hplc * harvesT_hat_yield_cla_params_volume1 / 1000), 0) as amt_quantity_total,
       1 as dim_dateidsetdate,
       current_date as dd_parameter_set_date,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM clarified_harvest tmp
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
GROUP BY YEAR(substr(tmp.parameter_set_date, 0, 10));


UPDATE tmp_fact_clarifiedharvest tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_clarifiedharvest tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_parameter_set_date
      AND tmp.dim_dateidsetdate = 1;
/* 10 Feb 2016 CristianT End */

UPDATE tmp_fact_clarifiedharvest tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_clarifiedharvest tmp;

TRUNCATE TABLE fact_clarifiedharvest;

INSERT INTO fact_clarifiedharvest(
fact_clarifiedharvestid,
dd_harvest_batch_id,
amt_harvest_hat_pa_hplc,
amt_harvest_hat_yield_cla_params_volume1,
amt_quantity_total,
dim_dateidsetdate,
dd_parameter_set_date,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_clarifiedharvestid,
       dd_harvest_batch_id,
       amt_harvest_hat_pa_hplc,
       amt_harvest_hat_yield_cla_params_volume1,
       amt_quantity_total,
       dim_dateidsetdate,
       dd_parameter_set_date,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM tmp_fact_clarifiedharvest;

DROP TABLE IF EXISTS tmp_fact_clarifiedharvest;

