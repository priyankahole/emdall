
/* Processing Script */

delete from fact_inventoryprojection;
insert into fact_inventoryprojection(
  fact_inventoryprojectionid
  ,amt_exchangerate
  ,amt_exchangerate_GBL
  ,dim_projectsourceid
  ,dim_partid
  ,dim_plantid
  ,dim_dateidmonth
  ,dd_planingsegment
  ,dd_periodofsegment
  ,dd_periodindicator
  ,ct_receipts
  ,ct_availableqty
)
select
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1) + row_number() over(order by '') fact_inventoryprojectionid
  ,1 amt_exchangerate
  ,1 amt_exchangerate_GBL
  ,(select s.dim_projectsourceid from dim_projectsource s) dim_projectsourceid
  ,ifnull(prt.dim_partid,1) dim_partid
  ,ifnull(plt.dim_plantid,1) dim_plantid
  ,1 dim_dateidmonth
  ,ifnull(m.MDSU_PLAAB, 'Not Set') dd_planingsegment
  ,ifnull(m.MDSU_EXTSU, 'Not Set') dd_periodofsegment
  ,ifnull(m.MDSU_PERKZ, 'Not Set') dd_periodindicator
  ,ifnull(m.MDSU_MNG03, 0) ct_receipts
  ,ifnull(m.MDSU_MNG04, 0) ct_availableqty
from MD04 m
  inner join dim_part prt on m.MDKP_MATNR = prt.partnumber and prt.plant = m.MDKP_PLWRK
  inner join dim_plant plt on m.MDKP_PLWRK = plt.plantcode
where ifnull(m.MDSU_PLAAB, 'Not Set') = '2' and (upper(ifnull(m.MDSU_EXTSU, 'Not Set')) = 'STOCK' or ifnull(m.MDSU_PERKZ, 'Not Set') like 'M%')
 and concat(right(MDSU_EXTSU,4), mid(MDSU_EXTSU,3,2)) >= concat(left(current_date,4), mid(current_date,6,2))
 and (concat(right(MDSU_EXTSU,4), mid(MDSU_EXTSU,3,2)) < concat(year(current_date+interval '2' year) ,'00') or  MDSU_EXTSU ='Stock');

drop table if exists tmp_dateupdate;
create table tmp_dateupdate
  as
  select fact_inventoryprojectionid, dd_periodofsegment, cast(1 as bigint) dim_dateidmonth from fact_inventoryprojection where dd_periodindicator = 'M';

update tmp_dateupdate f
set f.dim_dateidmonth = dd.dim_dateid
from tmp_dateupdate f
  inner join dim_date dd on dd.datevalue = cast(concat(right(trim(dd_periodofsegment),4),'-',left(right(trim(dd_periodofsegment),7),2),'-01') as date)
    and dd.companycode = 'Not Set';

update fact_inventoryprojection f
  set f.dim_dateidmonth = t.dim_dateidmonth
  from fact_inventoryprojection f
    inner join tmp_dateupdate t on f.fact_inventoryprojectionid = t.fact_inventoryprojectionid;


/* set available cty to 0 for 'Not Set' before current date' */
/*update fact_inventoryprojection
set ct_availableqty = 0 
from fact_inventoryprojection f, dim_date dd
where f.dim_dateidmonth = dd.dim_dateid
and DD_PERIODOFSEGMENT <> 'Stock'
and concat(left(datevalue,4), mid(datevalue,6,2)) < concat(left(current_date,4), mid(current_date,6,2))*/




drop table if exists tmp_dateupdate;

/* Add missing months */

/* create table with min and max date at the part/plant level */
drop table if exists tmp_min_max_date;
create table tmp_min_max_date
as
select f.dim_partid
  ,f.dim_plantid
  ,least(date_trunc('year',current_date),min(case when dd.datevalue ='0001-01-01' then current_date else dd.datevalue end)) - interval '1' month min_date
  ,(date_trunc('year',current_date) + interval '2' year) - interval '1' month max_date
  ,'Not Set' dummyrow
from fact_inventoryprojection f
  inner join dim_date dd on f.dim_dateidmonth = dd.dim_dateid
group by f.dim_partid,f.dim_plantid;

/* create table with all the first days of the month from dim_date*/
drop table if exists tmp_dateinterval;
create table tmp_dateinterval
as
select dd.dim_dateid, dd.datevalue,projectsourceid
from dim_date dd, dim_projectsource
where dd.dayofmonth = 1
  and dd.datevalue between (select min(t1.min_date) md from tmp_min_max_date t1) and (select max(t2.max_date) md from tmp_min_max_date t2)
  and dd.companycode = 'Not Set';

/* join the two tmp tables to add all the misssing dates */
drop table if exists tmp_dates_exploded;
create table tmp_dates_exploded
as
select t1.dim_partid
  ,t1.dim_plantid
  ,t1.min_date
  ,t1.max_date
  ,case when f.fact_inventoryprojectionid is null then 'X' else t1.dummyrow end dummyrow
  ,t2.dim_dateid
  ,t2.datevalue
  ,t2.projectsourceid
  ,ifnull(f.ct_availableqty,0) ct_availableqty
  ,ifnull(f.dd_planingsegment, 'Not Set') dd_planingsegment
  ,ifnull(f.dd_periodofsegment, 'Not Set') dd_periodofsegment
  ,ifnull(f.dd_periodindicator, 'Not Set') dd_periodindicator
from tmp_min_max_date t1
  inner join tmp_dateinterval t2 on t2.datevalue between t1.min_date and t1.max_date
  left join fact_inventoryprojection f on f.dim_partid = t1.dim_partid and f.dim_plantid = t1.dim_plantid and f.dim_dateidmonth = t2.dim_dateid;

drop table if exists tmp_min_max_date;
drop table if exists tmp_dateinterval;

/* get the previous valid date */
drop table if exists tmp_final;
create table tmp_final
as
select
  t.dim_partid
 ,t.dim_plantid
 ,t.min_date
 ,t.max_date
 ,t.dummyrow
 ,t.dim_dateid
 ,t.datevalue
 ,t.projectsourceid
 ,t.ct_availableqty
 ,t.dd_planingsegment
 ,t.dd_periodofsegment
 ,t.dd_periodindicator
 ,max(case when t.dummyrow = 'X' then '0001-01-01' else t.datevalue end) over (partition by t.dim_partid,t.dim_plantid order by t.datevalue rows between unbounded preceding and current row) prev_valid_date
from tmp_dates_exploded t;

drop table if exists tmp_dates_exploded;

drop table if exists tmp_begining_stock;
create table tmp_begining_stock
as
select
  dim_partid
  ,dim_plantid
  ,ct_availableqty
from fact_inventoryprojection where upper(trim(dd_periodofsegment)) = 'STOCK';



/*update the values with the stock for the dummy rows*/
update tmp_final t
set t.ct_availableqty = ifnull(t2.ct_availableqty,bs.ct_availableqty)
  ,t.dd_planingsegment = ifnull(t2.dd_planingsegment,'2')
  ,t.dd_periodofsegment = CONCAT('M ',to_char(t.datevalue,'MM/YYYY'))
  ,t.dd_periodindicator = ifnull(t2.dd_periodindicator,'M')
from tmp_final t
  inner join tmp_begining_stock bs on t.dim_partid = bs.dim_partid and t.dim_plantid = bs.dim_plantid
  left join tmp_final t2 on t.dim_partid = t2.dim_partid and t.dim_plantid = t2.dim_plantid and t.prev_valid_date = t2.datevalue;


  

drop table if exists tmp_begining_stock;

delete from tmp_final where dummyrow <> 'X';

DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'fact_inventoryprojection';
INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryprojection',ifnull(max(fact_inventoryprojectionid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_inventoryprojection;

insert into fact_inventoryprojection(
  fact_inventoryprojectionid
  ,amt_exchangerate
  ,amt_exchangerate_GBL
  ,dim_projectsourceid
  ,dim_partid
  ,dim_plantid
  ,dim_dateidmonth
  ,dd_planingsegment
  ,dd_periodofsegment
  ,dd_periodindicator
  ,ct_receipts
  ,ct_availableqty
  ,dd_dummyrow
)
select
  ifnull((select s.max_id from NUMBER_FOUNTAIN s where s.table_name = 'fact_inventoryprojection'),1) + row_number() over(order by '') fact_inventoryprojectionid
  ,1 amt_exchangerate
  ,1 amt_exchangerate_GBL
  ,(select s.dim_projectsourceid from dim_projectsource s) dim_projectsourceid
  ,ifnull(t.dim_partid,1) dim_partid
  ,ifnull(t.dim_plantid,1) dim_plantid
  ,ifnull(t.dim_dateid,1) dim_dateidmonth
  ,ifnull(t.dd_planingsegment, 'Not Set') dd_planingsegment
  ,ifnull(t.dd_periodofsegment, 'Not Set') dd_periodofsegment
  ,ifnull(t.dd_periodindicator, 'Not Set') dd_periodindicator
  ,0 ct_receipts
  ,ifnull(t.ct_availableqty, 0) ct_availableqty
  ,'X' dd_dummyrow
from tmp_final t;

drop table if exists tmp_final;






  /* Adjust available qty for dummy rows ( all values for avaliable qty should be 0 for dd_dummyrow = 'X' 
  rows before the first dd_dummyrow='Not Set' excepting DD_PERIODOFSEGMENT = 'Stock' ) */

drop table if exists availableqtyadj;
create table  availableqtyadj as 
select  
f.dim_partid,f.dim_plantid,min(dd.datevalue) min_date
from fact_inventoryprojection f, dim_date dd
where dd_dummyrow = 'Not Set'
and f.dim_dateidmonth <> 1
and f.dim_dateidmonth = dd.dim_dateid
group by f.dim_partid,f.dim_plantid;

update fact_inventoryprojection
set ct_availableqty = 0 
from fact_inventoryprojection f, availableqtyadj tmp ,dim_date dd
where f.dim_dateidmonth = dd.dim_dateid
  and f.dim_partid = tmp.dim_partid
  and f.dim_plantid = tmp.dim_plantid
  and dd.datevalue < tmp.min_date
  and DD_PERIODOFSEGMENT <> 'Stock';
  
  
/* Adjust available qty for dummy rows ( all values for avaliable qty should be 0 where DD_PERIODOFSEGMENT = 'Stock' 
    and no other original columns are available -no dummy inserted ) */
drop table if exists partwithnodata;
create table  partwithnodata as 
select   f.dim_partid,f.dim_plantid
from fact_inventoryprojection f
where DD_PERIODOFSEGMENT <> 'Stock'
and dd_dummyrow = 'Not Set'
group by f.dim_partid,f.dim_plantid;



update fact_inventoryprojection
set ct_availableqty = 0 
from fact_inventoryprojection f
where  concat(f.dim_partid , f.dim_plantid)  not in (select  concat(tmp.dim_partid , tmp.dim_plantid) from partwithnodata tmp)
AND dd_dummyrow ='X';



  
  /* update 'Dummy Row' = Actuals for all  'Dummy Row' = 'X' before current month */
  
  update fact_inventoryprojection f
set dd_dummyrow = 'Actuals'
from fact_inventoryprojection f, dim_date dd
where f.dim_dateidmonth = dd.dim_dateid
  and dd_dummyrow = 'X'
  and concat(left(datevalue,4), mid(datevalue,6,2))< concat(left(current_date,4), mid(current_date,6,2))
  and DD_PERIODOFSEGMENT <> 'Stock';
  
  
   /* update 'Dummy Row' = Actuals for all  'Dummy Row' = 'X' before current month */
   
   update fact_inventoryprojection f
set dd_dummyrow = 'Projected'
from fact_inventoryprojection f, dim_date dd
where f.dim_dateidmonth = dd.dim_dateid
  and dd_dummyrow = 'X'
  and concat(left(datevalue,4), mid(datevalue,6,2)) >= concat(left(current_date,4), mid(current_date,6,2))
  and DD_PERIODOFSEGMENT <> 'Stock';
  





/* update where dd_dummyrow equal Actuals with the DD_PERIODOFSEGMENT equal 'Stock' for current month */
drop table if exists tmp_notupdatableparts;
create table tmp_notupdatableparts as 
select concat(f.dim_partid , f.dim_plantid) partidplant from fact_inventoryprojection f, dim_date dd
where f.dim_dateidmonth = dd.dim_dateid
and dd_dummyrow ='Not Set' and concat(left(datevalue,4), mid(datevalue,6,2)) = concat(left(current_date,4), mid(current_date,6,2));

drop table if exists tmp_stockvalue;
create table  tmp_stockvalue as 
select   f.dim_partid,f.dim_plantid,f.ct_availableqty,f.ct_receipts
from fact_inventoryprojection f
where DD_PERIODOFSEGMENT = 'Stock'
 and concat(f.dim_partid , f.dim_plantid) not in (select  partidplant from tmp_notupdatableparts);

update fact_inventoryprojection f
set  f.ct_availableqty = f2.ct_availableqty
from fact_inventoryprojection f,tmp_stockvalue f2, dim_date dd
where f.dim_dateidmonth = dd.dim_dateid
and f.dd_dummyrow = 'Projected'
and concat(left(datevalue,4), mid(datevalue,6,2)) >= concat(left(current_date,4), mid(current_date,6,2))
and concat(f.dim_partid , f.dim_plantid)  = concat(f2.dim_partid , f2.dim_plantid);

update fact_inventoryprojection f
set   f.ct_receipts  = f2.ct_receipts
from fact_inventoryprojection f,tmp_stockvalue f2, dim_date dd
where f.dim_dateidmonth = dd.dim_dateid
and f.dd_dummyrow = 'Projected'
and concat(left(datevalue,4), mid(datevalue,6,2)) = concat(left(current_date,4), mid(current_date,6,2))
and concat(f.dim_partid , f.dim_plantid)  = concat(f2.dim_partid , f2.dim_plantid);

/* update Projected values with last values from 'Not Set' */
drop table if exists tmp_final2;
create table tmp_final2
as
select
  t.fact_inventoryprojectionid
 ,t.dim_partid
 ,t.dim_plantid
 ,t.dd_dummyrow
 ,dd.datevalue
 ,t.ct_availableqty
 ,t.dd_planingsegment
 ,t.dd_periodofsegment
 ,t.dd_periodindicator
 ,max(case when t.dd_dummyrow ='Projected' then '0001-01-01' else dd.datevalue end) over (partition by t.dim_partid,t.dim_plantid order by dd.datevalue rows between unbounded preceding and current row) prev_valid_date
from fact_inventoryprojection t
 inner join dim_date dd on t.dim_dateidmonth = dd.dim_dateid
where concat(left(datevalue,4), mid(datevalue,6,2)) > concat(left(current_date,4), mid(current_date,6,2));



update tmp_final2 t
set t.ct_availableqty = t2.ct_availableqty
from tmp_final2 t
  inner join tmp_final2 t2 on t.dim_partid = t2.dim_partid and t.dim_plantid = t2.dim_plantid and t.prev_valid_date = t2.datevalue;

update fact_inventoryprojection f
set f.ct_availableqty = t.ct_availableqty
from fact_inventoryprojection f,tmp_final2 t 
 where  f.fact_inventoryprojectionid = t.fact_inventoryprojectionid
  and f.dd_dummyrow <> 'Not Set';


/* Set Safety Stock and TotalPlantStock as measures */
update fact_inventoryprojection f
set ct_availableSafetyStock = CT_AVAILABLEQTY + dp.SafetyStock,
    f.ct_SafetyStock = dp.SafetyStock,
    f.amt_TotalPlantStock = dp.TotalPlantStockAmt
from fact_inventoryprojection f,dim_part dp
where  f.dim_partid = dp.dim_partid;

/* get date from inventory history */

/* get the max dates populated for each month */
drop table if exists maxdates;
create table maxdates as
select max(datevalue)dats,CALENDARMONTHID from fact_inventoryhistory f,dim_date dd
where f.Dim_DateidSnapshot = dd.dim_dateid
and datevalue >= date_trunc('year',current_date) - interval '1' year
group by CALENDARMONTHID;

drop table if exists tmp_fact_inventoryagingdata;
create table tmp_fact_inventoryagingdata as 
select  f.dim_partid,dd.CALENDARMONTHID,
sum(ct_TotalRestrictedStock)ct_TotalRestrictedStock,
sum(ct_BlockedStock)ct_BlockedStock,
sum(ct_StockInTransfer) ct_StockInTransfer,
sum(ct_StockQty)ct_StockQty,
sum(ct_StockInQInsp)ct_StockInQInsp,
sum(ct_StockInTransit) ct_StockInTransit,
sum(amt_OnHand) amt_OnHand,
max(amt_StdUnitPrice) amt_StdUnitPrice,
max(amt_ExchangeRate_GBL) amt_ExchangeRate_GBL,
sum(ct_baseuomratioPCcustom)ct_baseuomratioPCcustom
from fact_inventoryhistory f ,dim_date dd, maxdates mx
where ct_TotalRestrictedStock + ct_BlockedStock+ ct_StockInTransfer + ct_StockQty + ct_StockInQInsp+ ct_StockInTransit  <> 0
and f.Dim_DateidSnapshot = dd.dim_dateid
and dd.datevalue = mx.dats
group by f.dim_partid,dd.CALENDARMONTHID;




update fact_inventoryprojection f
set f.ct_TotalRestrictedStock = tmp.ct_TotalRestrictedStock,
    f.ct_BlockedStock = tmp.ct_BlockedStock,
    f.ct_StockInTransfer = tmp.ct_StockInTransfer,
    f.ct_onhandpre = ct_StockQty + ct_StockInQInsp+ ct_StockInTransit,
    f.amt_OnHand = tmp.amt_OnHand,
    f.amt_StdUnitPrice = tmp.amt_StdUnitPrice,
    f.amt_ExchangeRate_GBL = tmp.amt_ExchangeRate_GBL,
    f.ct_baseuomratioPCcustom = tmp.ct_baseuomratioPCcustom
from fact_inventoryprojection f,tmp_fact_inventoryagingdata tmp, dim_date d1
where  f.dim_partid = tmp.dim_partid
  and  f.dim_dateidmonth = d1.dim_dateid
  and  concat(left(d1.datevalue,4), mid(d1.datevalue,6,2)) = tmp.CALENDARMONTHID;

/* Carry the last available from inv history SS, RS, BL, IT for future months - all rows */
drop table if exists tmp_stockvalue;
create table  tmp_stockvalue as 
select   f.dim_partid,f.dim_plantid,f.ct_TotalRestrictedStock,f.ct_BlockedStock,f.ct_StockInTransfer,f.amt_StdUnitPrice,f.amt_ExchangeRate_GBL
from fact_inventoryprojection f, dim_date d1
where f.dim_dateidmonth = d1.dim_dateid
 and concat(left(datevalue,4), mid(datevalue,6,2)) = concat(left(current_date,4), mid(current_date,6,2)); 

update fact_inventoryprojection f
set      f.ct_TotalRestrictedStock = f2.ct_TotalRestrictedStock,
     f.ct_BlockedStock = f2.ct_BlockedStock,
     f.ct_StockInTransfer = f2.ct_StockInTransfer,
     f.amt_StdUnitPrice = f2.amt_StdUnitPrice,
     f.amt_ExchangeRate_GBL =f2.amt_ExchangeRate_GBL
from fact_inventoryprojection f,tmp_stockvalue f2, dim_date dd
where f.dim_dateidmonth = dd.dim_dateid
and concat(left(datevalue,4), mid(datevalue,6,2)) >= concat(left(current_date,4), mid(current_date,6,2))
and concat(f.dim_partid , f.dim_plantid)  = concat(f2.dim_partid , f2.dim_plantid);

update fact_inventoryprojection f
set  ct_availablerestricted = ct_availableSafetyStock + ct_TotalRestrictedStock,
     ct_availableblocked = ct_availableSafetyStock + ct_BlockedStock + ct_StockInTransfer + ct_TotalRestrictedStock,
     ct_onhand = ct_TotalRestrictedStock + ct_BlockedStock+ ct_StockInTransfer + ct_onhandpre;


/* Calculate Glidepath */
update fact_inventoryprojection f
set ct_Glidepath = case when concat(left(datevalue,4), mid(datevalue,6,2)) < concat(left(current_date,4), mid(current_date,6,2))
                            then ct_onhand
                            else CT_AVAILABLEQTY+ ct_SafetyStock +  ct_TotalRestrictedStock + ct_BlockedStock + ct_StockInTransfer end
from  fact_inventoryprojection f, dim_date dd
where f.dim_dateidmonth = dd.dim_dateid;

/* update Glidepath values with previous values from Glidepath */
drop table if exists tmp_final3;
create table tmp_final3
as
select
  t.fact_inventoryprojectionid
 ,t.dim_partid
 ,t.dim_plantid
 ,t.dd_dummyrow
 ,dd.datevalue
 ,t.ct_Glidepath
 ,t.dd_planingsegment
 ,t.dd_periodofsegment
 ,t.dd_periodindicator
 ,max(case when t.dd_dummyrow ='Projected' then '0001-01-01' else dd.datevalue end) over (partition by t.dim_partid,t.dim_plantid order by dd.datevalue rows between unbounded preceding and current row) prev_valid_date
from fact_inventoryprojection t
 inner join dim_date dd on t.dim_dateidmonth = dd.dim_dateid;


update tmp_final3 t
set t.ct_Glidepath = t2.ct_Glidepath
from tmp_final3 t
  inner join tmp_final3 t2 on t.dim_partid = t2.dim_partid and t.dim_plantid = t2.dim_plantid and t.prev_valid_date = t2.datevalue;

update fact_inventoryprojection f
set f.ct_Glidepath = t.ct_Glidepath
from fact_inventoryprojection f,tmp_final3 t 
 where  f.fact_inventoryprojectionid = t.fact_inventoryprojectionid;


/*  Glidepath Max Zero */
update fact_inventoryprojection f
set ct_Glidepathmax0 = case when ct_Glidepath < 0 
                             then 0 else ct_Glidepath 
                             end;




/* Calculate CT_AVAILABLEQTY and Glidepath for previous month */
drop table if exists tmp_availabilityprevmonth;
create table tmp_availabilityprevmonth as 
select  partnumber, plant ,datevalue, fact_inventoryprojectionid, CT_AVAILABLEQTY, cast (0 as decimal (18,4)) as ct_PREVAVAILABLEQTY,
ct_Glidepath, cast (0 as decimal (18,4)) as ct_preGlidepath,ct_Glidepathmax0, cast (0 as decimal (18,4)) as ct_PREGlidepathmax0
from fact_inventoryprojection f, dim_part dp , dim_date dd
where f.dim_partid = dp.dim_partid
and f.dim_dateidmonth = dd.dim_dateid
and datevalue <> '0001-01-01'
order by partnumber, plant ,datevalue;


update tmp_availabilityprevmonth t1
set t1.ct_PREVAVAILABLEQTY =  t1.CT_AVAILABLEQTY - t2.CT_AVAILABLEQTY,
    t1.ct_preGlidepath     =  t1.ct_Glidepath - t2.ct_Glidepath,
    t1.ct_PREGlidepathmax0 =  t1.ct_Glidepathmax0 - t2.ct_Glidepathmax0
from tmp_availabilityprevmonth t1, tmp_availabilityprevmonth t2
where t1.partnumber = t2.partnumber
and t1.plant = t2.plant 
and t1.datevalue - interval '1' month = t2.datevalue;


update fact_inventoryprojection f
  set f.ct_deltaVAILABLEQTY = t.ct_PREVAVAILABLEQTY,
      f.ct_DeltaGlidepath = t.ct_preGlidepath,
      f.ct_DeltaGlidepathmax0 =t.ct_PREGlidepathmax0
  from fact_inventoryprojection f,tmp_availabilityprevmonth t
  where f.fact_inventoryprojectionid = t.fact_inventoryprojectionid;

/* Calculate all amounts */

update fact_inventoryprojection
set amt_GLIDEPATH = amt_StdUnitPrice * CT_GLIDEPATH,
    amt_DELTAGLIDEPATH = amt_StdUnitPrice * ct_DELTAGLIDEPATH,
    amt_AVAILABLEQTY  =  amt_StdUnitPrice * CT_AVAILABLEQTY, 
    amt_AVAILABLERESTRICTED = amt_StdUnitPrice * CT_AVAILABLERESTRICTED,
    amt_AVAILABLESAFETYSTOCK = amt_StdUnitPrice * CT_AVAILABLESAFETYSTOCK, 
    amt_AVAILABLEBLOCKED = amt_StdUnitPrice * CT_AVAILABLEBLOCKED,
    amt_DeltaGlidepathmax0 = amt_StdUnitPrice * ct_DeltaGlidepathmax0,
    amt_Glidepathmax0 = amt_StdUnitPrice * ct_Glidepathmax0 ;


/* Update bwproducthierarchy */
update fact_inventoryprojection f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_inventoryprojection f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and bw.dim_bwproducthierarchyid <> '10000032744';
/* delete all data before current year */
delete from fact_inventoryprojection f
where   f.dim_dateidmonth in (select  dd.dim_dateid from dim_date dd  where year(dd.datevalue) < year(current_date))
  and DD_PERIODOFSEGMENT <> 'Stock';


/* MDG Part */
DROP TABLE IF EXISTS tmp_dim_mdg_partid;
CREATE TABLE tmp_dim_mdg_partid as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;


UPDATE fact_inventoryprojection f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM tmp_dim_mdg_partid tmp, fact_inventoryprojection f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

DROP TABLE IF EXISTS tmp_dim_mdg_partid;



update fact_inventoryprojection f
set f.amt_cogsfixedplanrate_emd = ifnull(c.Z_GCPLFF,0)
from fact_inventoryprojection f, csv_cogs c, dim_part dp , dim_plant dpl ,dim_company cd
where  f.dim_partid = dp.dim_partid
  and f.dim_plantid = dpl.dim_plantid
  and dpl.companycode = cd.companycode
  and trim(leading '0' from PRODUCT) = trim(leading '0' from dp.partnumber)
  and trim(leading '0' from c.Z_REPUNIT) = trim(leading '0' from cd.company);

/* Replace 'Not Set' with 'MD04' */
update fact_inventoryprojection f
set dd_dummyrow = 'MD04'
where dd_dummyrow ='Not Set';

/* Update the date when the project was refreshed */
update fact_inventoryprojection
  set dim_refreshdateid = dd.dim_dateid 
  from fact_inventoryprojection f, dim_date dd 
  where  datevalue = current_timestamp 
    and companycode = 'Not Set';
    
    
update fact_inventoryprojection
set DD_DUMMYROW2 = case when DD_DUMMYROW = 'Actuals' then 'Actuals'
                        when DD_DUMMYROW in ('MD04','Projected') then 'Forecast' 
                  else 'Not Set' end ;
/* Update target values */

/*update  FACT_INVENTORYPROJECTION f
set f.amt_target = md.target
from FACT_INVENTORYPROJECTION f
    inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
    inner join dim_part dp  on f.dim_partid = dp.dim_partid
    inner join dim_date dd on f.dim_dateidmonth = dd.dim_dateid
    inner join EMDTEMPOCC4.MD04_targets md on dpl.MANUFACTURINGSITE = md.ManufacturingSites
                               and dp.OWNERSHIP = md.Ownership
where left(dd.datevalue,4) = md.year1*/

/*
drop table if exists tmp_minparttarget
create table tmp_minparttarget as
select distinct min(dp.partnumber) as partnumber,min(dpl.plantcode)plantcode, dpl.MANUFACTURINGSITE,dp.OWNERSHIP,left(dd.datevalue,4)year1,md.target
from FACT_INVENTORYPROJECTION f
    inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
    inner join dim_part dp  on f.dim_partid = dp.dim_partid
    inner join dim_date dd on f.dim_dateidmonth = dd.dim_dateid
    inner join EMDTEMPOCC4.MD04_targets md on dpl.MANUFACTURINGSITE = md.ManufacturingSites
                               and dp.OWNERSHIP = md.Ownership
where left(dd.datevalue,4) <> '0001'
  and left(dd.datevalue,4) = md.year1
group by dpl.MANUFACTURINGSITE,dp.OWNERSHIP,left(dd.datevalue,4),md.target
order by dpl.MANUFACTURINGSITE,dp.OWNERSHIP

update  FACT_INVENTORYPROJECTION f
set f.amt_target = tmp.target
from FACT_INVENTORYPROJECTION f
    inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
    inner join dim_part dp  on f.dim_partid = dp.dim_partid
    inner join dim_date dd on f.dim_dateidmonth = dd.dim_dateid
    inner join tmp_minparttarget tmp on dpl.MANUFACTURINGSITE = tmp.MANUFACTURINGSITE
                               and dp.OWNERSHIP = tmp.Ownership
                               and tmp.partnumber = dp.partnumber
                               and tmp.plantcode  = dpl.plantcode
where left(dd.datevalue,4) = tmp.year1 */
/* APP-7897 - Oana 03Nov2017 */
drop table if exists tmp_minparttarget;
create table tmp_minparttarget as
select max(FACT_INVENTORYPROJECTIONid) FACT_INVENTORYPROJECTIONID, dpl.MANUFACTURINGSITE,dp.OWNERSHIP,dd.datevalue,md.target
from FACT_INVENTORYPROJECTION f
    inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
    inner join dim_part dp  on f.dim_partid = dp.dim_partid
    inner join dim_date dd on f.dim_dateidmonth = dd.dim_dateid
    inner join EMDTEMPOCC4.MD04_targets md on dpl.MANUFACTURINGSITE = md.ManufacturingSites
                               and dp.OWNERSHIP = md.Ownership and left(dd.datevalue,4) = md.year1
	inner join dim_bwproducthierarchy bw on f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
where bw.BUSINESSSECTOR = 'BS-01'
group by dpl.MANUFACTURINGSITE,dp.OWNERSHIP,dd.datevalue,md.target
order by dpl.MANUFACTURINGSITE,dp.OWNERSHIP;

update  FACT_INVENTORYPROJECTION f
set f.amt_target =  tmp.target
from FACT_INVENTORYPROJECTION f
    inner join tmp_minparttarget tmp on f.FACT_INVENTORYPROJECTIONID = tmp.FACT_INVENTORYPROJECTIONID;
/* END APP-7897 - Oana 03Nov2017 */

/*  APP-8137 MRKHC In-Quality-Review  */
drop table if exists tmp_inspectiondata;
create table tmp_inspectiondata as 
select dp.partnumber, dpl.plantcode , 
CALENDARMONTHID InspectionEndDate,
sum(f.ct_postedqty) ct_postedqty
from fact_inspectionlot f
  inner join dim_part dp on f.dim_partid = dp.dim_partid
  inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
  inner join dim_date dd  on dd.dim_dateid = f.dim_dateidinspectionend
  inner join dim_inspectionlotstatus ins on f.dim_inspectionlotstatusid  = ins.dim_inspectionlotstatusid
where ins.LotCancelled = 'Not Set' 
  and ins.UsageDecisionMade = 'Not Set'
  and f.dim_dateidusagedecisionmade  = 1 
  and f.ct_postedqty > 0 
  and CALENDARMONTHID not in (select  case when month(current_date) <10   then concat(year(current_date),0, month(current_date))
                                                             else concat(year(current_date), month(current_date))
                                                             end dat )
group by dp.partnumber, dpl.plantcode ,CALENDARMONTHID;


insert into  tmp_inspectiondata
(
PARTNUMBER, PLANTCODE, InspectionEndDate, ct_postedqty
)
select PARTNUMBER, PLANTCODE, CALENDARMONTHID as InspectionEndDate,0 as ct_postedqty
from FACT_INVENTORYPROJECTION f
   inner join dim_part dp on f.dim_partid = dp.dim_partid
   inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
   inner join dim_date dd on dd.dim_dateid = f.DIM_DATEIDMONTH
where concat (PARTNUMBER, PLANTCODE, CALENDARMONTHID) not in (select concat (PARTNUMBER, PLANTCODE, InspectionEndDate) from tmp_inspectiondata);

drop table if exists tmp_previousvalues;
create table tmp_previousvalues as 
select PARTNUMBER, PLANTCODE, max(InspectionEndDate) InspectionEndDate,sum (ct_postedqty) as ct_postedqty
from tmp_inspectiondata
where InspectionEndDate <  (select case when month(current_date) <10   then concat(year(current_date),0, month(current_date))
                                                             else concat(year(current_date), month(current_date))
                                                             end dat)
group by PARTNUMBER, PLANTCODE;


drop table if exists tmp_inspectiondatapre;
create table tmp_inspectiondatapre as 
select  PARTNUMBER, PLANTCODE,INSPECTIONENDDATE , 
sum(CT_POSTEDQTY) over (partition by PARTNUMBER, PLANTCODE order by cast (INSPECTIONENDDATE as int)desc) as CT_POSTEDQTY,row_number() over(partition by PARTNUMBER, PLANTCODE order by cast (INSPECTIONENDDATE as int)) rowno 
from tmp_inspectiondata;


update tmp_inspectiondatapre tmp 
set  tmp.CT_POSTEDQTY = tmp.CT_POSTEDQTY + tmp2.CT_POSTEDQTY
from tmp_inspectiondatapre tmp, tmp_previousvalues tmp2
  where tmp.PARTNUMBER = tmp2.PARTNUMBER
   and  tmp.PLANTCODE = tmp2.PLANTCODE;




update FACT_INVENTORYPROJECTION f
set f.ct_MRKHCInQualityReview =  tmp.ct_postedqty
from FACT_INVENTORYPROJECTION f
   inner join dim_part dp on f.dim_partid = dp.dim_partid
   inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
   inner join dim_date dd on dd.dim_dateid = f.DIM_DATEIDMONTH
   inner join tmp_inspectiondatapre tmp on  dp.partnumber = tmp.partnumber
         and  dpl.plantcode = tmp.plantcode
         and  dd.CALENDARMONTHID = tmp.InspectionEndDate
         and dd.CALENDARMONTHID >=(select case when month(current_date) <10   then concat(year(current_date),0, month(current_date))
                                                             else concat(year(current_date), month(current_date))
                                                             end dat);


delete from emd586.FACT_INVENTORYPROJECTION where dim_projectsourceid in (select DIM_PROJECTSOURCEID from dim_projectsource);
insert into emd586.FACT_INVENTORYPROJECTION
(
FACT_INVENTORYPROJECTIONID, 
	AMT_EXCHANGERATE, 
	AMT_EXCHANGERATE_GBL, 
	DIM_PROJECTSOURCEID, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	DIM_PARTID, 
	DIM_PLANTID, 
	DIM_DATEIDMONTH, 
	DD_PLANINGSEGMENT, 
	DD_PERIODOFSEGMENT, 
	DD_PERIODINDICATOR, 
	CT_RECEIPTS, 
	CT_AVAILABLEQTY, 
	DD_DUMMYROW, 
	CT_AVAILABLESAFETYSTOCK, 
	CT_AVAILABLERESTRICTED, 
	CT_AVAILABLEBLOCKED, 
	DIM_MDG_PARTID, 
	CT_TOTALRESTRICTEDSTOCK, 
	CT_BLOCKEDSTOCK, 
	CT_STOCKINTRANSFER, 
	CT_DELTAVAILABLEQTY, 
	CT_ONHAND, 
	AMT_COGSFIXEDPLANRATE_EMD, 
	CT_ONHANDPRE, 
	CT_SAFETYSTOCK, 
	AMT_TOTALPLANTSTOCK, 
	CT_GLIDEPATH, 
	CT_DELTAGLIDEPATH, 
	DIM_REFRESHDATEID, 
	AMT_ONHAND, 
	AMT_STDUNITPRICE, 
	CT_BASEUOMRATIOPCCUSTOM, 
	AMT_GLIDEPATH, 
	AMT_DELTAGLIDEPATH, 
	AMT_AVAILABLEQTY, 
	AMT_AVAILABLERESTRICTED, 
	AMT_AVAILABLESAFETYSTOCK, 
	AMT_AVAILABLEBLOCKED, 
	DIM_BWPRODUCTHIERARCHYID,
    DD_DUMMYROW2,
    amt_target,
    ct_Glidepathmax0,
    ct_DeltaGlidepathmax0,
    amt_DeltaGlidepathmax0,
    amt_Glidepathmax0,
    ct_MRKHCInQualityReview
)
SELECT
	FACT_INVENTORYPROJECTIONID, 
	AMT_EXCHANGERATE, 
	AMT_EXCHANGERATE_GBL, 
	DIM_PROJECTSOURCEID, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	DIM_PARTID, 
	DIM_PLANTID, 
	DIM_DATEIDMONTH, 
	DD_PLANINGSEGMENT, 
	DD_PERIODOFSEGMENT, 
	DD_PERIODINDICATOR, 
	CT_RECEIPTS, 
	CT_AVAILABLEQTY, 
	DD_DUMMYROW, 
	CT_AVAILABLESAFETYSTOCK, 
	CT_AVAILABLERESTRICTED, 
	CT_AVAILABLEBLOCKED, 
	DIM_MDG_PARTID, 
	CT_TOTALRESTRICTEDSTOCK, 
	CT_BLOCKEDSTOCK, 
	CT_STOCKINTRANSFER, 
	CT_DELTAVAILABLEQTY, 
	CT_ONHAND, 
	AMT_COGSFIXEDPLANRATE_EMD, 
	CT_ONHANDPRE, 
	CT_SAFETYSTOCK, 
	AMT_TOTALPLANTSTOCK, 
	CT_GLIDEPATH, 
	CT_DELTAGLIDEPATH, 
	DIM_REFRESHDATEID, 
	AMT_ONHAND, 
	AMT_STDUNITPRICE, 
	CT_BASEUOMRATIOPCCUSTOM, 
	AMT_GLIDEPATH, 
	AMT_DELTAGLIDEPATH, 
	AMT_AVAILABLEQTY, 
	AMT_AVAILABLERESTRICTED, 
	AMT_AVAILABLESAFETYSTOCK, 
	AMT_AVAILABLEBLOCKED, 
	DIM_BWPRODUCTHIERARCHYID,
    DD_DUMMYROW2,
    amt_target,
    ct_Glidepathmax0,
    ct_DeltaGlidepathmax0,
    amt_DeltaGlidepathmax0,
    amt_Glidepathmax0,
    ct_MRKHCInQualityReview
FROM
	FACT_INVENTORYPROJECTION;
