/******************************************************************************************************************/
/*   Script         : bi_populate_gamed_fact                                                                      */
/*   Author         : Cornelia                                                                                    */
/*   Created On     : 07 Mar 2017                                                                                 */
/*   Description    : Populating script of fact_gamed                                                             */
/*********************************************Change History*******************************************************/
/*   Date                By             Version      Desc                                                         */
/*   07 Mar 2017         Cornelia       1.0          Creating the script.                                         */
/*   04 Apr 2017         CristianT      1.1          Fix for Total Qty, Scrap Qty, Duration Prod AIM              */
/*   06 Apr 2017         CristianT      1.2          Adding dummy rows for Rate & Quality loss status             */
/*   13 Apr 2017         Cornelia       1.3          Add prdsts_duration in Hours:Minutes:Secconds                */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_gamed;
CREATE TABLE tmp_fact_gamed
AS
SELECT *
FROM fact_gamed
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_gamed';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_gamed',IFNULL(MAX(fact_gamedID),0)
FROM tmp_fact_gamed;

INSERT INTO tmp_fact_gamed( 
fact_gamedid,
dd_pshift_id,
dd_site_name,
dd_machine_name,
dd_machine_id,
dd_dati_shift,
dd_status_cls_name ,
dd_status_cls_desc,
dd_prdsts_dati_to,
ct_prdsts_duration,
ct_use_loss_duration_stg,
dd_shift_nr,
dd_prdsts_dati_from,
ct_production_duration_stg,
dd_status_name,
dd_status_desc,
dd_loss_type,
ct_avail_loss_duration_stg,
dd_shift_begin,
dd_shift_end,
ct_duration_prod_aim,
dd_tool_id,
ct_total_quantity,
ct_scrap_quantity,
dd_part_id,
amt_exchangerate_gbl,
amt_exchangerate,
dw_insert_date,
dw_update_date,
dim_projectsourceid,
dim_date_dati_shiftid,
dim_date_prdsts_dati_toid,
dim_prdsts_dati_fromid,
dim_shift_beginid,
dim_shift_endid,
dd_all_losses,
dd_availability_losses,
ct_order_count_batch,
ct_pulse_sum
)
SELECT 
(select ifnull(max_id,1) 
 from number_fountain 
 where table_name = 'tmp_fact_gamed') + row_number() over(order by '') AS fact_gamedID,
ifnull(substr(st.pshift_id,0,6),0) as dd_pshift_id ,
ifnull(st.site_name,'Not Set') as dd_site_name,
ifnull(st.machine_name,'Not Set')as dd_machine_name ,
ifnull(substr(st.machine_id,0,6),0) as dd_machine_id ,
ifnull(st.dati_shift,'0001-01-01') as dd_dati_shift,
ifnull(st.status_cls_name ,'Not Set') as dd_status_cls_name  ,
ifnull(st.status_cls_desc,'Not Set') as dd_status_cls_desc ,
ifnull(st.prdsts_dati_to, '0001-01-01') as dd_prdsts_dati_to,
ifnull(st.prdsts_duration ,0) as ct_prdsts_duration,
ifnull(st.use_loss_duration ,0) as ct_use_loss_duration_stg,
ifnull(st.shift_nr ,0) as dd_shift_nr,
ifnull(st.prdsts_dati_from ,'0001-01-01') as dd_prdsts_dati_from,
ifnull(st.production_duration ,0) as ct_production_duration_stg,
ifnull(st.status_name ,'Not Set') as dd_status_name,
ifnull(st.status_desc ,'Not Set') as dd_status_desc,
ifnull(st.loss_type,'Not Set') as dd_loss_type,
ifnull(st.avail_loss_duration,0) as ct_avail_loss_duration_stg,
'0001-01-01' as dd_shift_begin, /* su */
'0001-01-01' as  dd_shift_end, /* su */
0 as ct_duration_prod_aim, /* su */
0 as  dd_tool_id, /* su */
0 as  ct_total_quantity, /* su */
0 as ct_scrap_quantity, /* su */
0 as dd_part_id, /* su */
1 as amt_exchangerate_gbl,
1 as amt_exchangerate,
current_timestamp as DW_INSERT_DATE, 
current_timestamp as DW_UPDATE_DATE, 
1 as dim_projectsourceid,
1 as dim_date_dati_shiftid,
1 as dim_date_prdsts_dati_toid,
1 as dim_prdsts_dati_fromid,
1 as dim_shift_beginid ,
1 as dim_shift_endid,
ifnull(st.all_losses,'Not Set') as  dd_all_losses,
ifnull(st.availability_losses,'Not Set') as dd_availability_losses,
0 as ct_order_count_batch,
0 as ct_pulse_sum
FROM  oee_prod_status_extract st ;

-- updates for Date dimensions 

UPDATE tmp_FACT_GAMED F
SET F.dim_date_dati_shiftid = D.DIM_DATEID
FROM  tmp_FACT_GAMED F, DIM_DATE D 
WHERE to_date(f.dd_dati_shift) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_date_dati_shiftid  <> D.DIM_DATEID;

UPDATE tmp_FACT_GAMED F
SET F.dim_date_prdsts_dati_toid = D.DIM_DATEID
FROM  tmp_FACT_GAMED F, DIM_DATE D 
WHERE to_date(f.dd_prdsts_dati_to) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_date_prdsts_dati_toid  <> D.DIM_DATEID;

UPDATE tmp_FACT_GAMED F
SET F.dim_prdsts_dati_fromid = D.DIM_DATEID
FROM  tmp_FACT_GAMED F, DIM_DATE D 
WHERE to_date(f.dd_prdsts_dati_from) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_prdsts_dati_fromid  <> D.DIM_DATEID;

DELETE FROM fact_gamed;

INSERT INTO fact_gamed(
fact_gamedid,
dd_pshift_id,
dd_site_name,
dd_machine_name,
dd_machine_id,
dd_dati_shift,
dd_status_cls_name,
dd_status_cls_desc,
dd_prdsts_dati_to,
ct_prdsts_duration,
ct_use_loss_duration_stg,
dd_shift_nr,
dd_prdsts_dati_from,
ct_production_duration_stg,
dd_status_name,
dd_status_desc,
dd_loss_type,
ct_avail_loss_duration_stg,
dd_shift_begin,
dd_shift_end,
ct_duration_prod_aim,
dd_tool_id,
ct_total_quantity,
ct_scrap_quantity,
dd_part_id,
amt_exchangerate_gbl,
amt_exchangerate,
dw_insert_date,
dw_update_date,
dim_projectsourceid,
dim_date_dati_shiftid,
dim_date_prdsts_dati_toid,
dim_prdsts_dati_fromid,
dim_shift_beginid,
dim_shift_endid,
dd_all_losses,
dd_availability_losses,
ct_order_count_batch,
ct_pulse_sum)
SELECT fact_gamedid,
       dd_pshift_id,
       dd_site_name,
       dd_machine_name,
       dd_machine_id,
       dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       dd_prdsts_dati_to,
       ct_prdsts_duration,
       ct_use_loss_duration_stg,
       dd_shift_nr,
       dd_prdsts_dati_from,
       ct_production_duration_stg,
       dd_status_name,
       dd_status_desc,
       dd_loss_type,
       ct_avail_loss_duration_stg,
       dd_shift_begin,
       dd_shift_end,
       ct_duration_prod_aim,
       dd_tool_id,
       ct_total_quantity,
       ct_scrap_quantity,
       dd_part_id,
       amt_exchangerate_gbl,
       amt_exchangerate,
       dw_insert_date,
       dw_update_date,
       dim_projectsourceid,
       dim_date_dati_shiftid,
       dim_date_prdsts_dati_toid,
       dim_prdsts_dati_fromid,
       dim_shift_beginid,
       dim_shift_endid,
       dd_all_losses,
       dd_availability_losses,
	   ct_order_count_batch,
       ct_pulse_sum
FROM TMP_FACT_gamed;

DROP TABLE IF EXISTS tmp_fact_gamed;

/* Losses calculation */
DROP TABLE IF EXISTS Losses_calculation;
CREATE TABLE Losses_calculation
AS 
SELECT dd_site_name,
       dd_machine_name,
       dim_date_dati_shiftid,
       dd_shift_nr,
       dd_status_cls_name , 
       dd_status_cls_desc ,
       sum(ct_prdsts_duration) as DurationInSeconds
FROM fact_gamed f,
     dim_date dt
WHERE f.dim_date_dati_shiftid = dt.dim_dateid
GROUP BY dd_site_name,dd_machine_name,dim_date_dati_shiftid,dd_shift_nr,dd_status_cls_name, dd_status_cls_desc, dt.datevalue;


UPDATE fact_gamed f
SET f.ct_DurationInSeconds = ifnull(l.DurationInSeconds,0)
FROM fact_gamed f, Losses_calculation l
WHERE f.dd_site_name = l.dd_site_name
      AND f.dd_machine_name = l.dd_machine_name
      AND f.dim_date_dati_shiftid = l.dim_date_dati_shiftid
      AND f.dd_shift_nr = l.dd_shift_nr
      AND f.dd_status_cls_name = l.dd_status_cls_name
      AND f.dd_status_cls_desc = l.dd_status_cls_desc
      AND f.ct_DurationInSeconds <> ifnull(l.DurationInSeconds,0);


/* 06 Apr 2017 CristianT Start: Adding dummy rows for Rate & Quality loss status */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_gamed', ifnull(max(fact_gamedid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_gamed;

DROP TABLE IF EXISTS tmp_distinctmachinerows;
CREATE TABLE tmp_distinctmachinerows
AS
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed') + ROW_NUMBER() over(order by '') AS fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dim_date_dati_shiftid,
       'Rate & Quality Loss' as dd_status_cls_name,
       'Rate & Quality Loss' as dd_status_cls_desc
FROM fact_gamed
GROUP BY dd_site_name,
         dd_machine_id,
         dd_machine_name,
         dd_pshift_id,
         dd_shift_nr,
         dd_dati_shift,
         dim_date_dati_shiftid;

INSERT INTO fact_gamed(
fact_gamedid,
dd_pshift_id,
dd_site_name,
dd_machine_name,
dd_machine_id,
dd_dati_shift,
dd_status_cls_name,
dd_status_cls_desc,
dd_prdsts_dati_to,
ct_prdsts_duration,
ct_use_loss_duration_stg,
dd_shift_nr,
dd_prdsts_dati_from,
ct_production_duration_stg,
dd_status_name,
dd_status_desc,
dd_loss_type,
ct_avail_loss_duration_stg,
dd_shift_begin,
dd_shift_end,
ct_duration_prod_aim,
dd_tool_id,
ct_total_quantity,
ct_scrap_quantity,
dd_part_id,
amt_exchangerate_gbl,
amt_exchangerate,
dw_insert_date,
dw_update_date,
dim_projectsourceid,
dim_date_dati_shiftid,
dim_date_prdsts_dati_toid,
dim_prdsts_dati_fromid,
dim_shift_beginid,
dim_shift_endid,
dd_all_losses,
dd_availability_losses,
ct_order_count_batch,
ct_pulse_sum
)
SELECT fact_gamedid,
       dd_pshift_id,
       dd_site_name,
       dd_machine_name,
       dd_machine_id,
       dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       '0001-01-01' as dd_prdsts_dati_to,
       0 as ct_prdsts_duration,
       0 as ct_use_loss_duration_stg,
       dd_shift_nr,
       '0001-01-01' as dd_prdsts_dati_from,
       0 as ct_production_duration_stg,
       'Not Set' as dd_status_name,
       'Not Set' as dd_status_desc,
       'Not Set' as dd_loss_type,
       0 as ct_avail_loss_duration_stg,
       '0001-01-01' as dd_shift_begin,
       '0001-01-01' as dd_shift_end,
       0 as ct_duration_prod_aim,
       0 as dd_tool_id,
       0 as ct_total_quantity,
       0 as ct_scrap_quantity,
       1 as dd_part_id,
       0 as amt_exchangerate_gbl,
       0 as amt_exchangerate,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as dim_projectsourceid,
       dim_date_dati_shiftid,
       1 as dim_date_prdsts_dati_toid,
       1 as dim_prdsts_dati_fromid,
       1 as dim_shift_beginid,
       1 as dim_shift_endid,
       'All Losses' as dd_all_losses,
       'Availability Losses' as dd_availability_losses,
	   0 as ct_order_count_batch,
       0 as ct_pulse_sum
FROM tmp_distinctmachinerows;

DROP TABLE IF EXISTS tmp_distinctmachinerows;

/* Adding dummy rows for dd_status_cls_name = Production when this are not available for a date/site/machine/shift. This is to fix Duration% with Rate & Loss measure */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_gamed', ifnull(max(fact_gamedid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_gamed;
DROP TABLE IF EXISTS tmp_gamed_dummyproductionrows;
CREATE TABLE tmp_gamed_dummyproductionrows
AS 
SELECT dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dim_date_dati_shiftid
FROM fact_gamed tmp
WHERE tmp.dd_status_cls_desc = 'Production'
      AND tmp.dd_status_cls_name = 'PROD'
      AND tmp.dd_all_losses = 'All Losses'
      AND tmp.dd_availability_losses = 'Availability Losses'
GROUP BY dd_site_name,
         dd_machine_id,
         dd_machine_name,
         dd_pshift_id,
         dd_shift_nr,
         dd_dati_shift,
         dim_date_dati_shiftid;


drop table if exists tmp_dummyproductionrows;
CREATE TABLE tmp_dummyproductionrows
AS
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed') + ROW_NUMBER() over(order by '') AS fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dim_date_dati_shiftid,
       'Production' as dd_status_cls_desc,
       'PROD' as dd_status_cls_name,
       'All Losses' as dd_all_losses,
       'Availability Losses' as dd_availability_losses
FROM fact_gamed fg
WHERE 1 = 1
      AND NOT EXISTS (SELECT 1
                      FROM tmp_gamed_dummyproductionrows tmp
                      WHERE tmp.dd_site_name = fg.dd_site_name
                            AND tmp.dd_machine_id = fg.dd_machine_id
                            AND tmp.dd_machine_name = fg.dd_machine_name
                            AND tmp.dd_pshift_id = fg.dd_pshift_id
                            AND tmp.dd_shift_nr = fg.dd_shift_nr
                            AND tmp.dd_dati_shift = fg.dd_dati_shift
                            AND tmp.dim_date_dati_shiftid = fg.dim_date_dati_shiftid)
GROUP BY dd_site_name,
         dd_machine_id,
         dd_machine_name,
         dd_pshift_id,
         dd_shift_nr,
         dd_dati_shift,
         dim_date_dati_shiftid;


INSERT INTO fact_gamed(
fact_gamedid,
dd_pshift_id,
dd_site_name,
dd_machine_name,
dd_machine_id,
dd_dati_shift,
dd_status_cls_name,
dd_status_cls_desc,
dd_prdsts_dati_to,
ct_prdsts_duration,
ct_use_loss_duration_stg,
dd_shift_nr,
dd_prdsts_dati_from,
ct_production_duration_stg,
dd_status_name,
dd_status_desc,
dd_loss_type,
ct_avail_loss_duration_stg,
dd_shift_begin,
dd_shift_end,
ct_duration_prod_aim,
dd_tool_id,
ct_total_quantity,
ct_scrap_quantity,
dd_part_id,
amt_exchangerate_gbl,
amt_exchangerate,
dw_insert_date,
dw_update_date,
dim_projectsourceid,
dim_date_dati_shiftid,
dim_date_prdsts_dati_toid,
dim_prdsts_dati_fromid,
dim_shift_beginid,
dim_shift_endid,
dd_all_losses,
dd_availability_losses,
ct_order_count_batch,
ct_pulse_sum
)
SELECT fact_gamedid,
       dd_pshift_id,
       dd_site_name,
       dd_machine_name,
       dd_machine_id,
       dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       '0001-01-01' as dd_prdsts_dati_to,
       0 as ct_prdsts_duration,
       0 as ct_use_loss_duration_stg,
       dd_shift_nr,
       '0001-01-01' as dd_prdsts_dati_from,
       0 as ct_production_duration_stg,
       'Not Set' as dd_status_name,
       'Not Set' as dd_status_desc,
       'Not Set' as dd_loss_type,
       0 as ct_avail_loss_duration_stg,
       '0001-01-01' as dd_shift_begin,
       '0001-01-01' as dd_shift_end,
       0 as ct_duration_prod_aim,
       0 as dd_tool_id,
       0 as ct_total_quantity,
       0 as ct_scrap_quantity,
       1 as dd_part_id,
       0 as amt_exchangerate_gbl,
       0 as amt_exchangerate,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as dim_projectsourceid,
       dim_date_dati_shiftid,
       1 as dim_date_prdsts_dati_toid,
       1 as dim_prdsts_dati_fromid,
       1 as dim_shift_beginid,
       1 as dim_shift_endid,
       dd_all_losses,
       dd_availability_losses,
	   0 as ct_order_count_batch,
       0 as ct_pulse_sum
FROM tmp_dummyproductionrows;

DROP TABLE IF EXISTS tmp_dummyproductionrows;

/* 06 Apr 2017 CristianT End */

/* 04 Apr 2017 CristianT Start: Fix for Total Qty, Scrap Qty, Duration Prod AIM */
/* All this values will be populated on rows with rate & quality status */
DROP TABLE IF EXISTS tmp_oee_groupedvalues;
CREATE TABLE tmp_oee_groupedvalues
AS
SELECT site_name,
       machine_id,
       pshift_id,
       dati_shift,
       sum(ifnull(total_quantity, 0)) as total_quantity,
       sum(ifnull(scrap_quantity, 0)) as scrap_quantity,
       sum(ifnull(duration_prod_aim, 0)) as duration_prod_aim,
	   sum(ifnull(order_count,0)) as ct_order_count_batch,
	   sum(ifnull(pulse_sum,0)) as pulse_sum
FROM oee_prod_sum_extract
GROUP BY site_name,
         machine_id,
         pshift_id,
         dati_shift;

DROP TABLE IF EXISTS tmp_gamed_minid;
CREATE TABLE tmp_gamed_minid
AS
SELECT fct.dd_site_name as dd_site_name,
       fct.dd_machine_id as dd_machine_id,
       fct.dd_pshift_id as dd_pshift_id,
       fct.dim_date_dati_shiftid as dim_date_dati_shiftid,
       dt.datevalue as datevalue,
       min(fct.fact_gamedid) as fact_gamedid
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
      AND fct.dd_status_cls_desc = 'Rate & Quality Loss'
GROUP BY fct.dd_site_name,
         fct.dd_machine_id,
         fct.dd_pshift_id,
         fct.dim_date_dati_shiftid,
         dt.datevalue;

UPDATE fact_gamed f
SET f.ct_scrap_quantity = ifnull(qt.scrap_quantity,0)
FROM fact_gamed f,
	 tmp_oee_groupedvalues qt,
     tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND f.ct_scrap_quantity <> ifnull(qt.scrap_quantity,0);	

UPDATE fact_gamed f
SET f.ct_total_quantity = ifnull(qt.total_quantity,0)
FROM fact_gamed f,
		tmp_oee_groupedvalues qt,
     	tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND f.ct_total_quantity <> ifnull(qt.total_quantity,0);	

UPDATE fact_gamed f
SET f.ct_duration_prod_aim = ifnull(qt.duration_prod_aim,0)
FROM fact_gamed f, 
	tmp_oee_groupedvalues qt,
    tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND f.ct_duration_prod_aim <> ifnull(qt.duration_prod_aim,0);	
/* 04 Apr 2017 CristianT End */

/* 29 May 2017 Cornelia */
UPDATE fact_gamed f
SET f.ct_order_count_batch = ifnull(qt.ct_order_count_batch,0)
FROM fact_gamed f,
	 tmp_oee_groupedvalues qt,
     tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND f.ct_order_count_batch <> ifnull(qt.ct_order_count_batch,0);	

UPDATE fact_gamed f
SET f.ct_pulse_sum = ifnull(qt.pulse_sum,0)
FROM fact_gamed f,
	 tmp_oee_groupedvalues qt,
     tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND f.ct_pulse_sum <> ifnull(qt.pulse_sum,0);	
/* END 29 May 2017 Cornelia */	  


/* 07 Apr 2017 CristianT Start: Change of logic to be applied for Duration % measure when dd_status_cls_desc = Production or dd_status_cls_desc = Rate and Quality */
/* CristianT Details of this logic: Based on the feedback from Chandu and Priyanka Duration % measure for dd_status_cls_desc = Production should match the OEE % value.
In order to achieve this i created 3 aditional columns to get the values from oee_prod_sum_extract table and to reproduce the OEE logic
*/
DROP TABLE IF EXISTS tmp_prod_minid;
CREATE TABLE tmp_prod_minid
AS
SELECT fct.dd_site_name as dd_site_name,
       fct.dd_machine_id as dd_machine_id,
       fct.dd_pshift_id as dd_pshift_id,
       fct.dim_date_dati_shiftid as dim_date_dati_shiftid,
       dt.datevalue as datevalue,
       min(fct.fact_gamedid) as fact_gamedid
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
      AND fct.dd_status_cls_desc = 'Production'
      AND fct.dd_availability_losses <> 'Not Set'
GROUP BY fct.dd_site_name,
         fct.dd_machine_id,
         fct.dd_pshift_id,
         fct.dim_date_dati_shiftid,
         dt.datevalue;

UPDATE fact_gamed f
SET f.ct_scrap_quantity_prod_duration = ifnull(qt.scrap_quantity,0)
FROM fact_gamed f,
		tmp_oee_groupedvalues qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND f.ct_scrap_quantity_prod_duration <> ifnull(qt.scrap_quantity,0);	

UPDATE fact_gamed f
SET f.ct_total_quantity_prod_duration = ifnull(qt.total_quantity,0)
FROM fact_gamed f,
		tmp_oee_groupedvalues qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND f.ct_total_quantity_prod_duration <> ifnull(qt.total_quantity,0);	

UPDATE fact_gamed f
SET f.ct_aim_prod_duration = ifnull(qt.duration_prod_aim,0)
FROM fact_gamed f,
		tmp_oee_groupedvalues qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND f.ct_aim_prod_duration <> ifnull(qt.duration_prod_aim,0);	

/* Calculating the total of ct_avail_loss_duration_stg and ct_production_duration_stg at site/machine/shift/date level to use in the Duration % logic when dd_status_cls_desc = Production */
DROP TABLE IF EXISTS tmp_aggregated_sum;
CREATE TABLE tmp_aggregated_sum
AS
SELECT fct.dd_site_name as dd_site_name,
       fct.dd_machine_id as dd_machine_id,
       fct.dd_pshift_id as dd_pshift_id,
       fct.dim_date_dati_shiftid as dim_date_dati_shiftid,
       dt.datevalue as datevalue,
       sum(ct_avail_loss_duration_stg) as ct_total_avail_loss_duration,
       sum(ct_production_duration_stg) as ct_total_production_duration
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
GROUP BY fct.dd_site_name,
         fct.dd_machine_id,
         fct.dd_pshift_id,
         fct.dim_date_dati_shiftid,
         dt.datevalue;

UPDATE fact_gamed f
SET f.ct_total_avail_loss_duration = ifnull(qt.ct_total_avail_loss_duration,0)
FROM fact_gamed f,
		tmp_aggregated_sum qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND f.ct_total_avail_loss_duration <> ifnull(qt.ct_total_avail_loss_duration,0);	

UPDATE fact_gamed f
SET f.ct_total_production_duration = ifnull(qt.ct_total_production_duration,0)
FROM fact_gamed f,
		tmp_aggregated_sum qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND f.ct_total_production_duration <> ifnull(qt.ct_total_production_duration,0);

/* For Rate and Loss status Duration % measure logic should be (Duration% when dd_status_cls_desc = Production) - OEE% value */
/* Added ct_prdsts_duration_rateloss to get the value from ct_prdsts_duration when dd_status_cls_desc = Production and put it on Rate and Loss lines */
/* Calculating the total of ct_avail_loss_duration_stg and ct_production_duration_stg at site/machine/shift/date level to use in the Duration % logic when dd_status_cls_desc = Rate and loss */
DROP TABLE IF EXISTS tmp_prod_rateloss;
CREATE TABLE tmp_prod_rateloss
AS
SELECT fct.dd_site_name as dd_site_name,
       fct.dd_machine_id as dd_machine_id,
       fct.dd_pshift_id as dd_pshift_id,
       fct.dim_date_dati_shiftid as dim_date_dati_shiftid,
       dt.datevalue as datevalue,
       sum(fct.ct_prdsts_duration) as ct_prdsts_duration_rateloss
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
      AND fct.dd_status_cls_desc = 'Production'
GROUP BY fct.dd_site_name,
         fct.dd_machine_id,
         fct.dd_pshift_id,
         fct.dim_date_dati_shiftid,
         dt.datevalue;

UPDATE fact_gamed f
SET f.ct_prdsts_duration_rateloss = ifnull(qt.ct_prdsts_duration_rateloss,0)
FROM fact_gamed f,
		tmp_prod_rateloss qt,
     	tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND f.ct_prdsts_duration_rateloss <> ifnull(qt.ct_prdsts_duration_rateloss,0);

UPDATE fact_gamed f
SET f.ct_total_avail_loss_duration_rateloss = ifnull(qt.ct_total_avail_loss_duration,0)
FROM fact_gamed f,
		tmp_aggregated_sum qt,
     	tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND f.ct_total_avail_loss_duration_rateloss <> ifnull(qt.ct_total_avail_loss_duration,0);

UPDATE fact_gamed f
SET f.ct_total_production_duration_rateloss = ifnull(qt.ct_total_production_duration,0)
FROM fact_gamed f,
		tmp_aggregated_sum qt,
     	tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND f.ct_total_production_duration_rateloss <> ifnull(qt.ct_total_production_duration,0);
	  
DROP TABLE IF EXISTS tmp_prod_rateloss;
DROP TABLE IF EXISTS tmp_aggregated_sum;	  
DROP TABLE IF EXISTS tmp_prod_minid;
DROP TABLE IF EXISTS tmp_oee_groupedvalues;
DROP TABLE IF EXISTS tmp_gamed_minid;
/* 07 Apr 2017 CristianT End */



/* Total duration percents by machine */
DROP TABLE IF EXISTS total_duration;   
CREATE TABLE total_duration
AS 
SELECT dd_machine_name,dd_machine_id,dim_date_dati_shiftid,
      sum(ct_prdsts_duration) as total_duration,
	  count(dd_machine_name) as ct_no_machine
FROM fact_gamed,
    dim_date dt
WHERE 1 = 1
     and dt.dim_dateid = dim_date_dati_shiftid
GROUP BY dd_machine_name,dd_machine_id,dim_date_dati_shiftid;


UPDATE fact_gamed f 
SET f.ct_prdts_duration_total = t.total_duration
FROM fact_gamed f, total_duration t
WHERE f.dd_machine_name = t.dd_machine_name
      AND f.dd_machine_id = t.dd_machine_id
      AND f.dim_date_dati_shiftid = t.dim_date_dati_shiftid;

DROP TABLE IF EXISTS duration_by_machine;
CREATE TABLE duration_by_machine
AS 
SELECT f.dd_machine_name,
       f.dd_machine_id,
       f.dim_date_dati_shiftid,
       sum(case when t.ct_no_machine = 0 then 0 else t.total_duration/t.ct_no_machine end) as duration_by_machine
FROM fact_gamed f, total_duration t
WHERE f.dd_machine_name = t.dd_machine_name
      AND f.dd_machine_id = t.dd_machine_id
      AND f.dim_date_dati_shiftid = t.dim_date_dati_shiftid
GROUP BY f.dd_machine_name,f.dd_machine_id,f.dim_date_dati_shiftid;


UPDATE fact_gamed f 
SET f.ct_duration_by_machine = t.duration_by_machine
FROM fact_gamed f , duration_by_machine t
WHERE f.dd_machine_name = t.dd_machine_name
      AND f.dd_machine_id = t.dd_machine_id
      AND f.dim_date_dati_shiftid = t.dim_date_dati_shiftid;

DROP TABLE IF EXISTS duration_by_machine;
DROP TABLE IF EXISTS total_duration;   

/* 13 Apr 2017 Cornelia - add prdsts_duration in Hours:Minutes:Secconds */
/*update fact_gamed 
set dd_prdsts_duration_hms = CAST( CONVERT(int,(ct_prdsts_duration / (60 * 60))) as varchar) + ':' +
       CAST((mod(ct_prdsts_duration , 60 * 60)) / 60 as varchar) + ':' +
       CAST(mod(mod(ct_prdsts_duration , 60 * 60 * 60), 60) as varchar)
where dd_prdsts_duration_hms <> CAST( CONVERT(int,(ct_prdsts_duration / (60 * 60))) as varchar) + ':' +
       CAST((mod(ct_prdsts_duration , 60 * 60)) / 60 as varchar) + ':' +
       CAST(mod(mod(ct_prdsts_duration , 60 * 60 * 60), 60) as varchar) */
	   
/* call vectorwise (combine 'fact_gamed') */

/* 9 Jun 2017 Cristian Cleciu - new calculation and field Setup Mean Time */

DROP TABLE IF EXISTS tmp_prdsts_dur_minid;

CREATE TABLE tmp_prdsts_dur_minid AS
SELECT 	MIN(fact_gamedid) AS fact_gamedid,
	   	dd_site_name,
	   	dd_machine_id,
	   	dd_machine_name,
		dd_pshift_id,
		dd_dati_shift,
		dd_status_name,
		dd_status_cls_name,
		SUM(ct_prdsts_duration) AS sum_prdsts_duration
FROM fact_gamed
GROUP BY
dd_site_name,
dd_machine_id,
dd_machine_name,
dd_pshift_id,
dd_dati_shift,
dd_status_name,
dd_status_cls_name;

DROP TABLE IF EXISTS tmp_total_order_count;

CREATE TABLE tmp_total_order_count AS
SELECT	dd_site_name,
		dd_machine_id,
		dd_machine_name,
		dd_pshift_id,
		dd_dati_shift,
		SUM(ct_order_count_batch)  AS sum_order_count_batch
FROM fact_gamed 
GROUP BY 
dd_site_name,
dd_machine_id,
dd_machine_name,
dd_pshift_id,
dd_dati_shift;

DROP TABLE IF EXISTS tmp_setupmeantime;

CREATE TABLE tmp_setupmeantime AS
SELECT 	min.fact_gamedid,
		min.dd_site_name,
		min.dd_machine_id,
		min.dd_machine_name,
		min.dd_pshift_id,
		min.dd_dati_shift,
		min.dd_status_name,
		min.dd_status_cls_name,
		min.sum_prdsts_duration,
		ord.sum_order_count_batch,
		(CASE WHEN ord.sum_order_count_batch = 0 
				THEN 0.0000 
				ELSE min.sum_prdsts_duration / ord.sum_order_count_batch
				END) SetupMeanTime
FROM tmp_prdsts_dur_minid min
JOIN tmp_total_order_count ord
ON min.dd_site_name  = ord.dd_site_name
AND min.dd_machine_id  = ord.dd_machine_id
AND min.dd_dati_shift  = ord.dd_dati_shift
AND min.dd_pshift_id  = ord.dd_pshift_id;

UPDATE fact_gamed f
SET f.ct_setupmeantime = tmp.setupmeantime
FROM 	fact_gamed f, 
		tmp_setupmeantime tmp
WHERE 	f.fact_gamedid = tmp.fact_gamedid
		AND f.ct_setupmeantime <> tmp.setupmeantime;

DROP TABLE IF EXISTS tmp_prdsts_dur_minid;
DROP TABLE IF EXISTS tmp_total_order_count;
DROP TABLE IF EXISTS tmp_setupmeantime;
/* END 9 Jun 2017 Cristian Cleciu - new calculation and field Setup Mean Time */

/* 13 Jun 2017 Cristian Cleciu - new calculation and field Duration daily*/

DROP TABLE IF EXISTS tmp_dur_daily_minid;
CREATE TABLE tmp_dur_daily_minid
AS
SELECT 
       dt.datevalue as datevalue,
       fct.dd_status_cls_desc,
       min(fct.fact_gamedid) as fact_gamedid
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
      AND fct.dd_status_cls_desc <> 'Rate & Quality Loss'
GROUP  BY dt.datevalue,
		  fct.dd_status_cls_desc;


DROP TABLE IF EXISTS tmp_dur_daily_agg;
CREATE TABLE tmp_dur_daily_agg
AS         
SELECT dt.datevalue as datevalue,
       sum(fct.ct_prdsts_duration) as ct_prdsts_duration_daily
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
      AND fct.dd_status_cls_desc <> 'Rate & Quality Loss'
GROUP BY dt.datevalue;

DROP TABLE IF EXISTS tmp_dur_daily_upd;
CREATE TABLE tmp_dur_daily_upd
AS         
SELECT x.*,
       y.ct_prdsts_duration_daily
FROM tmp_dur_daily_minid x
LEFT JOIN tmp_dur_daily_agg y
ON x.datevalue = y.datevalue;

UPDATE fact_gamed f
SET f.ct_prdsts_duration_daily = ifnull(qt.ct_prdsts_duration_daily,0)
FROM fact_gamed f,
	 tmp_dur_daily_agg qt,
     tmp_dur_daily_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.datevalue = qt.datevalue
      AND f.ct_prdsts_duration_daily <> ifnull(qt.ct_prdsts_duration_daily,0); 

DROP TABLE IF EXISTS tmp_dur_daily_minid;
DROP TABLE IF EXISTS tmp_dur_daily_agg;
DROP TABLE IF EXISTS tmp_dur_daily_upd;

/* END 13 Jun 2017 Cristian Cleciu - new calculation and field Duration daily*/

DELETE FROM emd586.fact_gamed;

INSERT INTO emd586.fact_gamed
SELECT *
FROM fact_gamed;