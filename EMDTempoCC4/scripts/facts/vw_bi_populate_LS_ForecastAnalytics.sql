 drop table if exists tmp_FACT_LSFORECASTACCURACY;
 create table tmp_FACT_LSFORECASTACCURACY
 like  FACT_LSFORECASTACCURACY including defaults including identity;

alter table tmp_FACT_LSFORECASTACCURACY
add column CALENDARMONTHID int;




/*alter table FACT_LSFORECASTACCURACY
CREATE TABLE FACT_LSFORECASTACCURACY (
    FACT_LSFORECASTACCURACYID                    DECIMAL(36,0) DEFAULT 1 NOT NULL PRIMARY key,
    dd_globalmaterial VARCHAR(18) UTF8 DEFAULT 'Not Set' NOT NULL ENABLE,
    dd_keycustomer VARCHAR(32) UTF8 DEFAULT 'Not Set' NOT NULL ENABLE,
    dd_keycountry VARCHAR(7) UTF8 DEFAULT 'Not Set' NOT NULL ENABLE,
    dd_frozen1 varchar (7) DEFAULT 'Not Set' NOT NULL ENABLE,  
    AMT_EXCHANGERATE               DECIMAL(18,4) DEFAULT 1 NOT NULL ENABLE,
    AMT_EXCHANGERATE_GBL           DECIMAL(18,4) DEFAULT 1 NOT NULL ENABLE,
    DW_INSERT_DATE                 TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL ENABLE,
    DW_UPDATE_DATE                 TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL ENABLE,
    CT_NAIVEPLANNV                 DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE,
    CT_NAIVEPLANSP                 DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE,
    CT_NAIVEBASELINE               DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE,   
    CT_CONSENSUSBASELINE  DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE,   
    CT_CONSENSUSPLANNV DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE,   
    CT_CONSENSUSPLANSP DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE,   
    CT_STATISTICALFORECASTBASELINE DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE, 
    CT_STATISTICALFORECASTNV DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE, 
    CT_STATISTICALFORECASTSP DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE, 
    ct_quantityinpieces  DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE, 
    ct_quantity DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE, 
    amt_periodicvaluegc  DECIMAL(18,4) DEFAULT 0 NOT NULL ENABLE, 
    DIM_MDG_PARTID                 DECIMAL(36,0) DEFAULT 1 NOT NULL ENABLE,
    DIM_KEYCUSTOMER                DECIMAL(36,0) DEFAULT 1 NOT NULL ENABLE,
    DIM_KEYCUSTOMERID              DECIMAL(36,0) DEFAULT 1 NOT NULL ENABLE,
    DIM_KEYCOUNTRYID               DECIMAL(36,0) DEFAULT 1 NOT NULL ENABLE,
    DIM_CALENDARMONTHID DECIMAL(36,0) DEFAULT 1 NOT NULL ENABLE
    
);*/


drop table if exists tmp_preloaddata300;
create table tmp_preloaddata300 as 
--insert into dd_globalmaterial,dd_keycustomer,dd_keycountry,ct_quantity, CALENDARMONTHID
select dd_globalmaterial,dd_keycustomer,dd_keycountry,CALENDARMONTHID,sum(ct_orderintakesp) ct_orderintakesp,sum(ct_quantity)ct_quantity,sum(amt_periodicvaluegc) amt_periodicvaluegc,
sum(ct_quantityinpieces) ct_quantityinpieces,'1' as frozen1
from fact_APO300
 inner join dim_date dd on dim_requesteddeldateid = dim_dateid 
where CALENDARMONTHID >= 201702 and dd_intercompanyflag in ('Not Set','E')
group by dd_globalmaterial,dd_keycustomer,dd_keycountry, CALENDARMONTHID
union all 
select dd_globalmaterial,dd_keycustomer,dd_keycountry,CALENDARMONTHID,sum(ct_orderintakesp) ct_orderintakesp,sum(ct_quantity)ct_quantity,sum(amt_periodicvaluegc) amt_periodicvaluegc,
sum(ct_quantityinpieces) ct_quantityinpieces,'3' as frozen1
from fact_APO300
 inner join dim_date dd on dim_requesteddeldateid = dim_dateid 
where CALENDARMONTHID >= 201702 and dd_intercompanyflag in ('Not Set','E')
group by dd_globalmaterial,dd_keycustomer,dd_keycountry, CALENDARMONTHID
union all 
select dd_globalmaterial,dd_keycustomer,dd_keycountry,CALENDARMONTHID,sum(ct_orderintakesp) ct_orderintakesp,sum(ct_quantity)ct_quantity,sum(amt_periodicvaluegc) amt_periodicvaluegc,
sum(ct_quantityinpieces) ct_quantityinpieces,'6' as frozen1
from fact_APO300
 inner join dim_date dd on dim_requesteddeldateid = dim_dateid 
where CALENDARMONTHID >= 201702 and dd_intercompanyflag in ('Not Set','E')
group by dd_globalmaterial,dd_keycustomer,dd_keycountry, CALENDARMONTHID;


drop table if exists tmp_preloaddataMM;
create table tmp_preloaddataMM as 
    select m.dd_product,m.dd_keycustomer,m.dd_keycountry,m.dd_apofrozenzone1,m.CALENDARMONTHID,
    sum(ifnull(m.CT_NAIVEPLANNV,0)) as CT_NAIVEPLANNV,                
    sum(ifnull(m.CT_NAIVEPLANSP,0)) as CT_NAIVEPLANSP,               
    sum(ifnull(m.CT_NAIVEBASELINE,0)) as CT_NAIVEBASELINE,  
    sum(ifnull(m.CT_CONSENSUSBASELINE,0)) as CT_CONSENSUSBASELINE,  
    sum(ifnull(m.CT_CONSENSUSPLANNV,0)) as CT_CONSENSUSPLANNV,
    sum(ifnull(m.CT_CONSENSUSPLANSP,0)) as CT_CONSENSUSPLANSP,
    sum(ifnull(m.CT_STATISTICALFORECASTBASELINE,0)) as CT_STATISTICALFORECASTBASELINE,
    sum(ifnull(m.CT_STATISTICALFORECASTNV,0)) as CT_STATISTICALFORECASTNV,
    sum(ifnull(m.CT_STATISTICALFORECASTSP,0)) as CT_STATISTICALFORECASTSP,
    sum(ifnull(m.CT_REGIONALBASELINE,0)) as CT_REGIONALBASELINE,
    sum(ifnull(m.CT_REGIONALPLANNV,0)) as CT_REGIONALPLANNV,
    sum(ifnull(m.CT_REGIONALPLANSP,0)) as CT_REGIONALPLANSP
    from fact_MM00 m
    where m.CALENDARMONTHID >= 201702 
      and m.dd_apofrozenzone1 in ('1','3','6')
group by m.dd_product,m.dd_keycustomer,m.dd_keycountry,m.dd_apofrozenzone1,m.CALENDARMONTHID;



--select * from tmp_preloaddata
-- delete from FACT_LSFORECASTACCURACY;
insert into tmp_FACT_LSFORECASTACCURACY
(
dd_globalmaterial,
dd_keycustomer,
dd_keycountry,
CALENDARMONTHID,
dd_frozen1,
CT_NAIVEPLANNV,
CT_NAIVEPLANSP,
CT_NAIVEBASELINE,
CT_CONSENSUSBASELINE,
CT_CONSENSUSPLANNV,
CT_CONSENSUSPLANSP,
CT_STATISTICALFORECASTBASELINE,
CT_STATISTICALFORECASTNV,
CT_STATISTICALFORECASTSP,
CT_REGIONALBASELINE,
CT_REGIONALPLANNV,
CT_REGIONALPLANSP,
ct_quantityinpieces,
ct_quantity,
amt_periodicvaluegc,
ct_orderintakesp,
DIM_MDG_PARTID,
    DIM_KEYCUSTOMER,
    DIM_KEYCUSTOMERID,
    DIM_KEYCOUNTRYID  
)
select 
    m.dd_product,  
    m.dd_keycustomer, 
    m.dd_keycountry, 
    m.CALENDARMONTHID,
    m.dd_apofrozenzone1 as dd_frozen1,    
    sum(ifnull(m.CT_NAIVEPLANNV,0)) as CT_NAIVEPLANNV,                
    sum(ifnull(m.CT_NAIVEPLANSP,0)) as CT_NAIVEPLANSP,               
    sum(ifnull(m.CT_NAIVEBASELINE,0)) as CT_NAIVEBASELINE,  
    sum(ifnull(m.CT_CONSENSUSBASELINE,0)) as CT_CONSENSUSBASELINE,  
    sum(ifnull(m.CT_CONSENSUSPLANNV,0)) as CT_CONSENSUSPLANNV,
    sum(ifnull(m.CT_CONSENSUSPLANSP,0)) as CT_CONSENSUSPLANSP,
    sum(ifnull(m.CT_STATISTICALFORECASTBASELINE,0)) as CT_STATISTICALFORECASTBASELINE,
    sum(ifnull(m.CT_STATISTICALFORECASTNV,0)) as CT_STATISTICALFORECASTNV,
    sum(ifnull(m.CT_STATISTICALFORECASTSP,0)) as CT_STATISTICALFORECASTSP,
    sum(ifnull(m.CT_REGIONALBASELINE,0)) as CT_REGIONALBASELINE,
    sum(ifnull(m.CT_REGIONALPLANNV,0)) as CT_REGIONALPLANNV,
    sum(ifnull(m.CT_REGIONALPLANSP,0)) as CT_REGIONALPLANSP,
    0 as ct_quantityinpieces,
    0 as ct_quantity,
    0 as amt_periodicvaluegc,
    0 as ct_orderintakesp,
    cast(1 as bigint) as DIM_MDG_PARTID,
    cast(1 as bigint) as DIM_KEYCUSTOMER,
    cast(1 as bigint) as DIM_KEYCUSTOMERID,
    cast(1 as bigint) as DIM_KEYCOUNTRYID   
from tmp_preloaddataMM m 
group by m.dd_product,  
    m.dd_keycustomer, 
    m.dd_keycountry, 
    m.CALENDARMONTHID,
    m.dd_apofrozenzone1
union all
select 
    t.dd_globalmaterial,  
    t.dd_keycustomer, 
    t.dd_keycountry, 
    t.CALENDARMONTHID,
    t.frozen1 as dd_frozen1,    
    0 as CT_NAIVEPLANNV,                
    0 as CT_NAIVEPLANSP,               
    0 as CT_NAIVEBASELINE,  
    0 as CT_CONSENSUSBASELINE,  
    0 as CT_CONSENSUSPLANNV,
    0 as CT_CONSENSUSPLANSP,
    0 as CT_STATISTICALFORECASTBASELINE,
    0 as CT_STATISTICALFORECASTNV,
    0 as CT_STATISTICALFORECASTSP,
    0 as CT_REGIONALBASELINE,
    0 as CT_REGIONALPLANNV,
    0 as CT_REGIONALPLANSP,
    sum(ifnull(t.ct_quantityinpieces,0)) ct_quantityinpieces,
    sum(ifnull(t.ct_quantity,0)) ct_quantity,
    sum(ifnull(t.amt_periodicvaluegc,0)) amt_periodicvaluegc,
    sum(ifnull(t.ct_orderintakesp,0))ct_orderintakesp,
    cast(1 as bigint) as DIM_MDG_PARTID,
    cast(1 as bigint) as DIM_KEYCUSTOMER,
    cast(1 as bigint) as DIM_KEYCUSTOMERID,
    cast(1 as bigint) as DIM_KEYCOUNTRYID   
from tmp_preloaddata300 t
group by t.dd_globalmaterial,  
    t.dd_keycustomer, 
    t.dd_keycountry, 
    t.CALENDARMONTHID,
    t.frozen1;

update tmp_FACT_LSFORECASTACCURACY t
    set t.dim_calendarmonthid = dd.dim_dateid 
from tmp_FACT_LSFORECASTACCURACY t,dim_date dd 
where t.CALENDARMONTHID = dd.CALENDARMONTHID
  and dd.companycode = 'Not Set'
  and concat(datevalue,t.CALENDARMONTHID) in (select concat (min (datevalue),CALENDARMONTHID) from dim_date  where companycode = 'Not Set' group by CALENDARMONTHID);



/* Mdg */

UPDATE tmp_FACT_LSFORECASTACCURACY tmp
SET tmp.dim_mdg_partid = ifnull(md.dim_mdg_partid, 1),
    tmp.dw_update_date = current_timestamp
FROM dim_mdg_part md, tmp_FACT_LSFORECASTACCURACY tmp
WHERE  trim(leading '0' from tmp.dd_globalmaterial) = trim(leading '0' from md.partnumber )
  AND tmp.dim_mdg_partid <>  md.dim_mdg_partid; 

/* BW Product Hierarchy */
drop table if exists tmp_maxversion;
create table tmp_maxversion as
select max(right (versiondesc,4))versiondesc , productgroup  from dim_bwproducthierarchy  where right (versiondesc,4) ='2017'
group by productgroup;



drop table if exists tmp_dim_bwproducthierarchy;
create table tmp_dim_bwproducthierarchy as
select max(dbw.dim_bwproducthierarchyid)dim_bwproducthierarchyid, dbw.productgroup
from dim_bwproducthierarchy dbw ,tmp_maxversion tmp
where concat(right (dbw.versiondesc,4),dbw.productgroup) = concat(tmp.versiondesc,tmp.productgroup)
group by dbw.productgroup;

/*drop table if exists tmp_sbu;
create table tmp_sbu as 
select  distinct productgroupsbu,partnumber  from dim_part dp;


select  distinct glbproductgroup as productgroupsbu,partnumber  from dim_mdg_part dp;

update tmp_FACT_LSFORECASTACCURACY tmp
set tmp.dim_bwproducthierarchyid =  ifnull(dbw.dim_bwproducthierarchyid,1)
from tmp_FACT_LSFORECASTACCURACY tmp, tmp_sbu dp, tmp_dim_bwproducthierarchy dbw, DIM_MDG_PART mdg
where  mdg.DIM_MDG_PARTID  =  tmp.DIM_MDG_PARTID 
  and  mdg.partnumber = dp.partnumber 
  and  dp.productgroupsbu = REPLACE(dbw.productgroup,'SBU-','');*/
  
update tmp_FACT_LSFORECASTACCURACY tmp
set tmp.dim_bwproducthierarchyid =  ifnull(dbw.dim_bwproducthierarchyid,1)
from tmp_FACT_LSFORECASTACCURACY tmp,   tmp_dim_bwproducthierarchy dbw, DIM_MDG_PART mdg
where  mdg.DIM_MDG_PARTID  =  tmp.DIM_MDG_PARTID 
  and  mdg.glbproductgroup = REPLACE(dbw.productgroup,'SBU-',''); 


/* Update Key country */
update tmp_FACT_LSFORECASTACCURACY f 
set f.dim_keycountryid = ifnull(dm.dim_keycountryid,1)
from tmp_FACT_LSFORECASTACCURACY f
   inner join dim_keycountry dm on trim(dm.KeyCountry) = trim(f.dd_keycountry)
and f.dim_keycountryid <> ifnull(dm.dim_keycountryid,1);


update tmp_FACT_LSFORECASTACCURACY f 
set f.dim_keycustomerid = ifnull(dm.dim_keycustomerid,1)
from tmp_FACT_LSFORECASTACCURACY f
   inner join dim_keycustomer dm on rtrim(dm.KeyCustomer) = f.dd_keycustomer
and f.dim_keycustomerid <> ifnull(dm.dim_keycustomerid,1);

delete from FACT_LSFORECASTACCURACY;
insert into FACT_LSFORECASTACCURACY
(
FACT_LSFORECASTACCURACYID, 
dd_globalmaterial,
dd_keycustomer,
dd_keycountry,
dd_frozen1,
CT_NAIVEPLANNV,
CT_NAIVEPLANSP,
CT_NAIVEBASELINE,
CT_CONSENSUSBASELINE,
CT_CONSENSUSPLANNV,
CT_CONSENSUSPLANSP,
CT_STATISTICALFORECASTBASELINE,
CT_STATISTICALFORECASTNV,
CT_STATISTICALFORECASTSP,
CT_REGIONALBASELINE,
CT_REGIONALPLANNV,
CT_REGIONALPLANSP,
ct_quantityinpieces,
ct_quantity,
amt_periodicvaluegc,
ct_orderintakesp,
DIM_MDG_PARTID,
    DIM_KEYCUSTOMER,
    DIM_KEYCUSTOMERID,
    DIM_KEYCOUNTRYID,
    dim_calendarmonthid,
    dim_bwproducthierarchyid
)
select 
row_number() over(order by '') as FACT_LSFORECASTACCURACYID, 
dd_globalmaterial,
dd_keycustomer,
dd_keycountry,
dd_frozen1,
sum(CT_NAIVEPLANNV) as CT_NAIVEPLANNV,
sum(CT_NAIVEPLANSP) as CT_NAIVEPLANSP,
sum(CT_NAIVEBASELINE) as CT_NAIVEBASELINE,
sum(CT_CONSENSUSBASELINE) as CT_CONSENSUSBASELINE,
sum(CT_CONSENSUSPLANNV) as CT_CONSENSUSPLANNV,
sum(CT_CONSENSUSPLANSP) as CT_CONSENSUSPLANSP,
sum(CT_STATISTICALFORECASTBASELINE) as CT_STATISTICALFORECASTBASELINE,
sum(CT_STATISTICALFORECASTNV) as CT_STATISTICALFORECASTNV,
sum(CT_STATISTICALFORECASTSP) as CT_STATISTICALFORECASTSP,
sum(CT_REGIONALBASELINE) as CT_REGIONALBASELINE,
sum(CT_REGIONALPLANNV) as CT_REGIONALPLANNV,
sum(CT_REGIONALPLANSP) as CT_REGIONALPLANSP,
sum(ct_quantityinpieces) as ct_quantityinpieces,
sum(ct_quantity) as ct_quantity,
sum(amt_periodicvaluegc) as amt_periodicvaluegc,
sum(ct_orderintakesp) as ct_orderintakesp,
DIM_MDG_PARTID,
    DIM_KEYCUSTOMER,
    DIM_KEYCUSTOMERID,
    DIM_KEYCOUNTRYID,
    dim_calendarmonthid,
    dim_bwproducthierarchyid
from tmp_FACT_LSFORECASTACCURACY
group by dd_globalmaterial,
dd_keycustomer,
dd_keycountry,
dd_frozen1,
DIM_MDG_PARTID,
    DIM_KEYCUSTOMER,
    DIM_KEYCUSTOMERID,
    DIM_KEYCOUNTRYID,
    dim_calendarmonthid,
    dim_bwproducthierarchyid;
/*create table tmp_missing as 
select dd_product,
  dd_keycustomer,
  dd_keycountry from  fact_MM00 f
  inner join dim_date dd on  f.dim_calendardayid = dd.DIM_DATEID
     and CALENDARMONTHID >= 201702
where concat (dd_product,dd_keycustomer,dd_keycountry)  
   not in (select concat(dd_globalmaterial,dd_keycustomer,dd_keycountry) from fact_APO300)

select dd_product,
  dd_keycustomer,
  dd_keycountry from  fact_MM00 f
  inner join dim_date dd on  f.dim_calendardayid = dd.DIM_DATEID
     and CALENDARMONTHID >= 201702
where  dd_product not in (select  dd_globalmaterial  from fact_APO300)*/

/* Mean absolute error material level */
drop table if exists tmp_globalmaterial;
create table tmp_globalmaterial
  as select min(FACT_LSFORECASTACCURACYid)as id ,  dd_globalmaterial,DD_FROZEN1,min(dim_calendarmonthid)dim_calendarmonthid,abs ( sum ( (CT_ORDERINTAKESP) - (CT_CONSENSUSPLANSP) ) )  ct_meanabserror,
case when sum (CT_ORDERINTAKESP)  = 0 and sum(CT_CONSENSUSPLANSP)= 0  then 100 
     when sum (CT_ORDERINTAKESP)  = 0 and sum(CT_CONSENSUSPLANSP)<> 0  then 0 
     when sum (CT_ORDERINTAKESP)  <> 0 then 1- abs ( sum ( (CT_ORDERINTAKESP) - (CT_CONSENSUSPLANSP) ) ) 
else 0 end ct_mixskumaterial
from FACT_LSFORECASTACCURACY
group by dd_globalmaterial,DD_FROZEN1,dim_calendarmonthid;


update FACT_LSFORECASTACCURACY f
set f.ct_meanabserror = t.ct_meanabserror,
    f.ct_mixskumaterial = t.ct_mixskumaterial
from FACT_LSFORECASTACCURACY f,tmp_globalmaterial t
where f.dd_globalmaterial = t.dd_globalmaterial
 and f.DD_FROZEN1 = t.DD_FROZEN1
 and f.dim_calendarmonthid = t.dim_calendarmonthid
 and f.FACT_LSFORECASTACCURACYid  = t.id;
 
 
 /* Mean absolute error country level */

drop table if exists tmp_KEYCOUNTRY;
create table tmp_KEYCOUNTRY
  as select min(FACT_LSFORECASTACCURACYid)as id , dd_globalmaterial, KEYCOUNTRY,KEYREGION,DD_KEYCUSTOMER, DD_FROZEN1,min(dim_calendarmonthid)dim_calendarmonthid,abs ( sum ( (CT_ORDERINTAKESP) - (CT_CONSENSUSPLANSP) ) )  ct_meanabserrorcntry
from FACT_LSFORECASTACCURACY f
  left join dim_keycountry dc on f.dim_keycountryid = dc.dim_keycountryid
group by  dd_globalmaterial, KEYCOUNTRY,KEYREGION,DD_KEYCUSTOMER, DD_FROZEN1,dim_calendarmonthid;


update FACT_LSFORECASTACCURACY f
set f.ct_meanabserrorcntry = t.ct_meanabserrorcntry
from FACT_LSFORECASTACCURACY f,tmp_KEYCOUNTRY t,dim_keycountry dc 
where f.dim_keycountryid = dc.dim_keycountryid
 and dc.KEYCOUNTRY = t.KEYCOUNTRY
 and dc.KEYREGION = t.KEYREGION
 and f.DD_KEYCUSTOMER = t.DD_KEYCUSTOMER
 and f.dd_globalmaterial = t.dd_globalmaterial
 and f.DD_FROZEN1 = t.DD_FROZEN1
 and f.dim_calendarmonthid = t.dim_calendarmonthid
 and f.FACT_LSFORECASTACCURACYid  = t.id; 
 
/* Mean absolute error region level */

drop table if exists tmp_KEYREGION;
create table tmp_KEYREGION
  as select min(FACT_LSFORECASTACCURACYid)as id , dd_globalmaterial, KEYREGION,DD_FROZEN1,min(dim_calendarmonthid)dim_calendarmonthid,abs ( sum ( (CT_ORDERINTAKESP) - (CT_CONSENSUSPLANSP) ) )  ct_meanabserrorreg
from FACT_LSFORECASTACCURACY f
    left join dim_keycountry dc on f.dim_keycountryid = dc.dim_keycountryid
group by dd_globalmaterial,KEYREGION,DD_FROZEN1,dim_calendarmonthid;


update FACT_LSFORECASTACCURACY f
set f.ct_meanabserrorreg = t.ct_meanabserrorreg
from FACT_LSFORECASTACCURACY f,tmp_KEYREGION t,dim_keycountry dc 
where f.dim_keycountryid = dc.dim_keycountryid
 and dc.KEYREGION = t.KEYREGION
 and f.dd_globalmaterial = t.dd_globalmaterial
 and f.DD_FROZEN1 = t.DD_FROZEN1
 and f.dim_calendarmonthid = t.dim_calendarmonthid
 and f.FACT_LSFORECASTACCURACYid  = t.id;
 

 
update FACT_LSFORECASTACCURACY
set dd_globalmaterial = trim (leading '0' from dd_globalmaterial);

delete from EMD586.FACT_LSFORECASTACCURACY;
insert into EMD586.FACT_LSFORECASTACCURACY
(
FACT_LSFORECASTACCURACYID, 
dd_globalmaterial,
dd_keycustomer,
dd_keycountry,
dd_frozen1,
CT_NAIVEPLANNV,
CT_NAIVEPLANSP,
CT_NAIVEBASELINE,
CT_CONSENSUSBASELINE,
CT_CONSENSUSPLANNV,
CT_CONSENSUSPLANSP,
CT_STATISTICALFORECASTBASELINE,
CT_STATISTICALFORECASTNV,
CT_STATISTICALFORECASTSP,
CT_REGIONALBASELINE,
CT_REGIONALPLANNV,
CT_REGIONALPLANSP,
ct_quantityinpieces,
ct_quantity,
amt_periodicvaluegc,
ct_orderintakesp,
DIM_MDG_PARTID,
    DIM_KEYCUSTOMER,
    DIM_KEYCUSTOMERID,
    DIM_KEYCOUNTRYID,
    dim_calendarmonthid,
    dim_bwproducthierarchyid,
    ct_meanabserror,
    ct_meanabserrorcntry,
    ct_meanabserrorreg,
    ct_mixskumaterial
)
select 
FACT_LSFORECASTACCURACYID, 
dd_globalmaterial,
dd_keycustomer,
dd_keycountry,
dd_frozen1,
CT_NAIVEPLANNV,
CT_NAIVEPLANSP,
CT_NAIVEBASELINE,
CT_CONSENSUSBASELINE,
CT_CONSENSUSPLANNV,
CT_CONSENSUSPLANSP,
CT_STATISTICALFORECASTBASELINE,
CT_STATISTICALFORECASTNV,
CT_STATISTICALFORECASTSP,
CT_REGIONALBASELINE,
CT_REGIONALPLANNV,
CT_REGIONALPLANSP,
ct_quantityinpieces,
ct_quantity,
amt_periodicvaluegc,
ct_orderintakesp,
DIM_MDG_PARTID,
    DIM_KEYCUSTOMER,
    DIM_KEYCUSTOMERID,
    DIM_KEYCOUNTRYID,
    dim_calendarmonthid,
    dim_bwproducthierarchyid,
    ct_meanabserror,
    ct_meanabserrorcntry,
    ct_meanabserrorreg,
    ct_mixskumaterial
from FACT_LSFORECASTACCURACY;
