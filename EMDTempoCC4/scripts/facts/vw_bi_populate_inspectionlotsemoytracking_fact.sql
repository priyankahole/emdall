
DROP TABLE IF EXISTS number_fountain_fact_inspectionlotsemoytracking;
CREATE TABLE number_fountain_fact_inspectionlotsemoytracking 
  LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoytracking;
CREATE TABLE tmp_fact_inspectionlotsemoytracking 
  LIKE fact_inspectionlotsemoytracking INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_inspectionlotsemoytracking 
WHERE table_name = 'fact_inspectionlotsemoytracking';	

INSERT INTO number_fountain_fact_inspectionlotsemoytracking
SELECT 'fact_inspectionlotsemoytracking', IFNULL(MAX(f.fact_inspectionlotsemoytrackingid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM tmp_fact_inspectionlotsemoytracking AS f;

INSERT INTO tmp_fact_inspectionlotsemoytracking(
  fact_inspectionlotsemoytrackingid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_codearticle,
  dd_libelle,
  dd_NoLotProduit,
  dd_NoLotLIMS,
  dd_datedecreation,
  dd_Categorie,
  dd_stage,
  dd_StatutEchantillon,
  dd_SpecCondition,
  dd_datedebutanalyse,
  dd_enattentedepuis,
  dd_comment,
  dim_DateidDeCreation,
  dim_dateiddebutanalyse
)
SELECT
  (SELECT max_id 
    FROM number_fountain_fact_inspectionlotsemoytracking
    WHERE table_name = 'fact_inspectionlotsemoytracking') 
      + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoytrackingid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS dw_insert_date, 
  current_timestamp AS dw_update_date,
  IFNULL(materialname,'Not Set') AS dd_codearticle,
  IFNULL(u_materialtype,'Not Set') AS dd_libelle,
  IFNULL(sap_batchno,'Not Set') AS dd_NoLotProduit,
  IFNULL(u_lotnumber,'Not Set') AS dd_NoLotLIMS,
  CASE 
    WHEN sample_createdt IS NULL THEN '0001-01-01' 
    ELSE LEFT(sample_createdt, 10) 
  END 
  AS dd_datedecreation,
  IFNULL(batchstagedesc,'Not Set') AS dd_Categorie,
  IFNULL(stage_name,'Not Set') AS dd_stage,
  IFNULL(samplestatus,'Not Set') AS dd_StatutEchantillon,
  'Not Set' AS dd_SpecCondition,
  '0001-01-01' AS dd_datedebutanalyse,
  'Not Set' AS dd_enattentedepuis,
  IFNULL(category,'Not Set') AS dd_comment,
  1 AS dim_DateidDeCreation,
  1 AS dim_dateiddebutanalyse
FROM
  v_mcockpit_suiviencours
WHERE
  UPPER(securitydepartment) = 'SE'
;


/* BEGIN - update dim dates */
UPDATE tmp_fact_inspectionlotsemoytracking AS f
SET f.dim_DateidDeCreation = dd.dim_dateid
FROM  tmp_fact_inspectionlotsemoytracking AS f, dim_date AS dd 
WHERE f.dd_datedecreation = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_DateidDeCreation <> dd.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoytracking AS f
SET f.dim_dateiddebutanalyse = dd.dim_dateid
FROM  tmp_fact_inspectionlotsemoytracking AS f, dim_date AS dd 
WHERE f.dd_datedebutanalyse = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_dateiddebutanalyse <> dd.dim_dateid;
/* END - update dim dates */

DELETE FROM fact_inspectionlotsemoytracking;

INSERT INTO fact_inspectionlotsemoytracking(
  fact_inspectionlotsemoytrackingid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_codearticle,
  dd_libelle,
  dd_NoLotProduit,
  dd_NoLotLIMS,
  dd_datedecreation,
  dd_Categorie,
  dd_stage,
  dd_StatutEchantillon,
  dd_SpecCondition,
  dd_datedebutanalyse,
  dd_enattentedepuis,
  dd_comment,
  dim_DateidDeCreation,
  dim_dateiddebutanalyse
)
SELECT
  fact_inspectionlotsemoytrackingid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_codearticle,
  dd_libelle,
  dd_NoLotProduit,
  dd_NoLotLIMS,
  dd_datedecreation,
  dd_Categorie,
  dd_stage,
  dd_StatutEchantillon,
  dd_SpecCondition,
  dd_datedebutanalyse,
  dd_enattentedepuis,
  dd_comment,
  dim_DateidDeCreation,
  dim_dateiddebutanalyse
FROM tmp_fact_inspectionlotsemoytracking;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoytracking;

DELETE FROM emd586.fact_inspectionlotsemoytracking;

INSERT INTO emd586.fact_inspectionlotsemoytracking
SELECT *
FROM fact_inspectionlotsemoytracking;