



/* Removed START OF PROC */

 DROP TABLE IF EXISTS tmp_pGlobalCurrency_planorder;
 CREATE TABLE tmp_pGlobalCurrency_planorder ( pGlobalCurrency CHAR(3) NULL);

 INSERT INTO tmp_pGlobalCurrency_planorder VALUES ( 'USD' );

update tmp_pGlobalCurrency_planorder
SET pGlobalCurrency = ifnull(s.property_value, 'USD')
FROM tmp_pGlobalCurrency_planorder tmp
 		CROSS JOIN (select property_value from systemproperty WHERE property = 'customer.global.currency') s;

			   

/*Inactive old plan orders - Q1a*/
UPDATE fact_planorder po
   SET po.Dim_ActionStateid = 3
 WHERE NOT EXISTS
          (SELECT 1
             FROM PLAF p
            WHERE p.PLAF_PLNUM = po.dd_PlanOrderNo)
AND po.Dim_ActionStateid <> 3;

/*Inactive old plan orders - Q1b*/
UPDATE fact_planorder po
   SET po.ct_Completed = 1
 WHERE NOT EXISTS
          (SELECT 1
             FROM PLAF p
            WHERE p.PLAF_PLNUM = po.dd_PlanOrderNo)
AND po.ct_Completed <> 1;

/*Update plan order*/
DROP TABLE IF EXISTS tmp_fpo_mbew_no_bwtar;
CREATE TABLE tmp_fpo_mbew_no_bwtar AS SELECT max(ifnull((CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0)) calcPrice,
                        sp.BWKEY,
                        sp.MATNR
                FROM mbew_no_bwtar sp
               WHERE  ((sp.LFGJA * 100) + sp.LFMON) =
                            (SELECT max((x.LFGJA * 100) + x.LFMON)
                               FROM mbew_no_bwtar x
                              WHERE x.MATNR = sp.MATNR AND x.BWKEY = sp.BWKEY
                                    AND ((x.VPRSV = 'S' AND x.STPRS > 0)
                                         OR (x.VPRSV = 'V' AND x.VERPR > 0)))
                     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0)
                          OR (sp.VPRSV = 'V' AND sp.VERPR > 0))
				 group by  
				  sp.BWKEY,
                  sp.MATNR;

/* Q2 - column 1 - Dim_Companyid*/

UPDATE fact_planorder po
SET po.Dim_Companyid = ifnull(dc.Dim_Companyid, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Companyid <> dc.Dim_Companyid;		


/* Q2 - column 2 - Dim_Partid */
UPDATE fact_planorder po
SET po.Dim_Partid = ifnull(dp.Dim_Partid, 1)
FROM plaf p,
	dim_plant pl,
	dim_company dc,
	dim_currency c,
	dim_part dp,
	dim_unitofmeasure uom,
	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Partid <> dp.Dim_Partid;

/* Q2 - column 3 - Dim_mpnid */
UPDATE fact_planorder po
SET po.Dim_mpnid = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	fact_planorder po
		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_mpnid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_mpnid = ifnull(mpn.dim_partid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_part mpn, fact_planorder po
	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND CONVERT( varchar (7), uom.UOM) = CONVERT (varchar (7), p.plaf_meins)
AND dc.Currency = c.CurrencyCode
AND mpn.PartNumber = p.PLAF_EMATN
AND mpn.Plant = p.PLAF_PLWRK
AND mpn.RowIsCurrent = 1;


/* Q2 - column 4 - Dim_StorageLocationid */

UPDATE fact_planorder po
SET po.Dim_StorageLocationid = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_StorageLocationid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_storagelocation sl, fact_planorder po

WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sl.LocationCode = p.plaf_lgort
AND sl.Plant = p.PLAF_PLWRK
AND sl.RowIsCurrent = 1;



/* Q2 - column 5 - Dim_Plantid */

UPDATE fact_planorder po
SET po.Dim_Plantid = ifnull(pl.Dim_Plantid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Plantid  <> pl.Dim_Plantid;


/* Q2 - column6 - Dim_UnitOfMeasureid */
UPDATE fact_planorder po
SET po.Dim_UnitOfMeasureid = ifnull(uom.Dim_UnitOfMeasureid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_UnitOfMeasureid  <> uom.Dim_UnitOfMeasureid;


/* Q2 - column7 - Dim_PurchaseOrgid */
UPDATE fact_planorder po
SET po.Dim_PurchaseOrgid = ifnull(pog.Dim_PurchaseOrgid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,dim_purchaseorg pog, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pog.PurchaseOrgCode = pl.PurchOrg
AND po.Dim_PurchaseOrgid  <> pog.Dim_PurchaseOrgid;



/* Q2 - column8 - Dim_Vendorid */
UPDATE fact_planorder po
SET po.Dim_Vendorid = 1		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Vendorid <> 1;

UPDATE fact_planorder po
SET po.Dim_Vendorid = ifnull(dv.Dim_Vendorid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_vendor dv, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dv.VendorNumber = p.PLAF_EMLIF;


/* Q2 - column9 - Dim_FixedVendorid */

UPDATE fact_planorder po
SET po.Dim_FixedVendorid = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, 
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_FixedVendorid <> 1;


UPDATE fact_planorder po
SET po.Dim_FixedVendorid = ifnull(fv.Dim_Vendorid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_vendor fv, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND fv.VendorNumber = p.PLAF_FLIEF;


/* Q2 - column10 - Dim_SpecialProcurementid */
UPDATE fact_planorder po
SET po.Dim_SpecialProcurementid = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_SpecialProcurementid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_SpecialProcurementid = ifnull(sp.Dim_SpecialProcurementid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_specialprocurement sp, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sp.specialprocurement = p.PLAF_SOBES
AND ifnull(po.Dim_SpecialProcurementid,-1) <> ifnull(sp.Dim_SpecialProcurementid,-2);

/* Q2 - column11 - Dim_ConsumptionTypeid */
UPDATE fact_planorder po
SET po.Dim_ConsumptionTypeid = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po 
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_ConsumptionTypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_ConsumptionTypeid = ifnull(dcp.Dim_ConsumptionTypeid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_consumptiontype dcp, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dcp.ConsumptionCode = p.PLAF_KZVBR
AND ifnull(po.Dim_ConsumptionTypeid,-1) <> ifnull(dcp.Dim_ConsumptionTypeid,-2);

/* Q2 - column12 - Dim_AccountCategoryid */
UPDATE fact_planorder po
SET po.Dim_AccountCategoryid = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_AccountCategoryid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_AccountCategoryid = ifnull(ac.Dim_AccountCategoryid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_accountcategory ac, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ac.Category = p.plaf_knttp
AND ifnull(po.Dim_AccountCategoryid,-1) <> ifnull(ac.Dim_AccountCategoryid,-2);


/* Q2 - column13 - Dim_SpecialStockid */

UPDATE fact_planorder po
SET po.Dim_SpecialStockid = 1		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_SpecialStockid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_SpecialStockid = ifnull(st.Dim_SpecialStockid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_specialstock st,fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND st.specialstockindicator = p.PLAF_SOBKZ
AND ifnull(po.Dim_SpecialStockid,-1) <> ifnull(st.Dim_SpecialStockid,-2);

/* Q2 - column14 - dim_bomstatusid */
UPDATE fact_planorder po
SET po.dim_bomstatusid = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
 		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_bomstatusid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_bomstatusid = ifnull(bs.dim_bomstatusid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_bomstatus bs, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND bs.BOMStatusCode = p.plaf_ststa
AND ifnull(po.dim_bomstatusid,-1) <> ifnull(bs.dim_bomstatusid,-2);

/* Q2 - column15 - dim_bomusageid */
UPDATE fact_planorder po
SET po.dim_bomusageid = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_bomusageid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_bomusageid = ifnull(bu.dim_bomusageid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_bomusage bu, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND bu.BOMUsageCode = p.plaf_stlan
AND ifnull(po.dim_bomusageid,-1) <> ifnull(bu.dim_bomusageid,-2);

/* Q2 - column16 - dim_objecttypeid */
UPDATE fact_planorder po
SET po.dim_objecttypeid = 1		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_objecttypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_objecttypeid = ifnull(ot.dim_objecttypeid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_objecttype ot, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ot.ObjectType = p.PLAF_OBART
AND ifnull(po.dim_objecttypeid,-1) <> ifnull(ot.dim_objecttypeid,-2);


/* Q2 - column17 - dim_productionschedulerid */
UPDATE fact_planorder po
SET po.dim_productionschedulerid = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_productionschedulerid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_productionschedulerid = ifnull(ps.dim_productionschedulerid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_productionscheduler ps, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ps.ProductionScheduler = p.PLAF_PLGRP
AND ps.Plant = p.PLAF_PLWRK
AND ifnull(po.dim_productionschedulerid,-1) <> ifnull(ps.dim_productionschedulerid,-2);


/* Q2 - column18 - dim_schedulingerrorid */
UPDATE fact_planorder po
SET po.dim_schedulingerrorid = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_schedulingerrorid,-1) <> 1;



UPDATE fact_planorder po
SET po.dim_schedulingerrorid = ifnull(se.dim_schedulingerrorid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_schedulingerror se, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND se.SchedulingErrorCode = p.PLAF_TRMER
AND ifnull(po.dim_schedulingerrorid,-1) <> ifnull(se.dim_schedulingerrorid,-2);


/* Q2 - column19 - dim_tasklisttypeid */
UPDATE fact_planorder po
SET po.dim_tasklisttypeid = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_tasklisttypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_tasklisttypeid = ifnull(tst.dim_tasklisttypeid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_tasklisttype tst,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND tst.TaskListTypeCode = p.PLAF_PLNTY
AND ifnull(po.dim_tasklisttypeid,-1) <> ifnull(tst.dim_tasklisttypeid,-2);


/* Q2 - column20 - dim_ordertypeid */
UPDATE fact_planorder po
SET po.dim_ordertypeid = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_ordertypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_ordertypeid = ifnull(ordt.dim_ordertypeid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_ordertype ordt, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ordt.OrderTypeCode = p.PLAF_PAART
AND ifnull(po.dim_ordertypeid,-1) <> ifnull(ordt.dim_ordertypeid,-2);



/* Q2 - column21 - Dim_dateidStart */
UPDATE fact_planorder po
SET po.Dim_dateidStart = 1		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidStart,-1) <> 1;



UPDATE fact_planorder po
SET po.Dim_dateidStart = ifnull(ds.dim_dateid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date ds, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ds.DateValue = p.PLAF_PSTTR
AND pl.CompanyCode = ds.CompanyCode
AND ifnull(po.Dim_dateidStart,-1) <> ifnull(ds.dim_dateid,-2);


/* Q2 - column22 - Dim_dateidFinish */
UPDATE fact_planorder po
SET po.Dim_dateidFinish = 1		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidFinish,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_dateidFinish = ifnull(df.dim_dateid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_PEDTR
AND pl.CompanyCode = df.CompanyCode
AND p.PLAF_PEDTR IS NOT NULL
AND ifnull(po.Dim_dateidFinish,-1) <> ifnull(df.dim_dateid,-2);



/* Q2 - column23 - Dim_dateidOpening */
UPDATE fact_planorder po
SET po.Dim_dateidOpening = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidOpening,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_dateidOpening = ifnull(dop.dim_dateid,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date dop, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dop.DateValue = p.PLAF_PERTR
AND pl.CompanyCode = dop.CompanyCode
AND p.PLAF_PERTR IS NOT NULL
AND ifnull(po.Dim_dateidOpening,-1) <> ifnull(dop.dim_dateid,-2);


/* Q2 - column24 - ct_QtyTotal */
UPDATE fact_planorder po
SET po.ct_QtyTotal = ifnull(p.PLAF_GSMNG,0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.ct_QtyTotal <> p.PLAF_GSMNG;



/* Q2 - column25 - amt_ExtendedPrice : LK - This query can be tuned further if required */
	DROP TABLE IF EXISTS tmp_fact_planorder_t001;
	create table tmp_fact_planorder_t001 as
	select  distinct sp.calcPrice, p.plaf_plnum
	FROM 	plaf p,
			dim_plant pl,        
			dim_part dp,       
			tmp_fpo_mbew_no_bwtar sp
	WHERE pl.PlantCode = p.PLAF_PLWRK
	AND dp.PartNumber = p.PLAF_MATNR
	AND dp.Plant = p.PLAF_PLWRK
	AND sp.MATNR = dp.PartNumber 
	AND sp.BWKEY = pl.ValuationArea;

	UPDATE fact_planorder po
	SET po.amt_ExtendedPrice = ifnull(tmp.calcPrice,0)* p.PLAF_GSMNG
	FROM fact_planorder po
			INNER JOIN plaf p ON po.dd_PlanOrderNo = p.plaf_plnum
			LEFT JOIN tmp_fact_planorder_t001 tmp ON tmp.plaf_plnum = po.dd_PlanOrderNo			
	WHERE po.Dim_ActionStateid = 2
	AND po.amt_ExtendedPrice <> ifnull(tmp.calcPrice,0)* p.PLAF_GSMNG;

	DROP TABLE IF EXISTS tmp_fact_planorder_t001;
	/* Original 
	
				UPDATE fact_planorder po
			   FROM plaf p,
					dim_plant pl,
					dim_company dc,
					dim_currency c,
					dim_part dp,
					dim_unitofmeasure uom
			SET        amt_ExtendedPrice =
					  (ifnull((SELECT calcPrice 
							FROM tmp_fpo_mbew_no_bwtar sp
						   WHERE sp.MATNR = dp.PartNumber AND sp.BWKEY = pl.ValuationArea),
						 0)
					   * p.PLAF_GSMNG)
			WHERE     po.dd_PlanOrderNo = p.plaf_plnum
			AND po.Dim_ActionStateid = 2
			AND pl.PlantCode = p.PLAF_PLWRK
			AND dc.CompanyCode = pl.CompanyCode
			AND dp.PartNumber = p.PLAF_MATNR
			AND dp.Plant = p.PLAF_PLWRK
			AND uom.UOM = p.plaf_meins
			AND dc.Currency = c.CurrencyCode*/

/* Q2 - column26 - dd_bomexplosionno */
UPDATE fact_planorder po
SET po.dd_bomexplosionno = ifnull(p.PLAF_SERNR, 'Not Set')
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_bomexplosionno,'xx') <> ifnull(p.PLAF_SERNR,'yy');


/* Q2 - column27 - ct_QtyReduced */
UPDATE fact_planorder po
SET po.ct_QtyReduced = ifnull(p.plaf_ABMNG, 0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyReduced,-1) <> ifnull(p.plaf_ABMNG,-2);


/* Q2 - column28 - Dim_DateidProductionStart */
UPDATE fact_planorder po
SET po.Dim_DateidProductionStart = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidProductionStart,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_DateidProductionStart = ifnull(df.dim_dateid, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_TERST
AND pl.CompanyCode = df.CompanyCode
AND ifnull(po.Dim_DateidProductionStart,-1) <> ifnull(df.dim_dateid,-2);


/* Q2 - column29 - Dim_DateidProductionFinish */
UPDATE fact_planorder po
SET po.Dim_DateidProductionFinish = 1		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidProductionFinish,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_DateidProductionFinish = ifnull(df.dim_dateid, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_TERED
AND pl.CompanyCode = df.CompanyCode
AND ifnull(po.Dim_DateidProductionFinish,-1) <> ifnull(df.dim_dateid,-2);

/* Q2 - column30 - Dim_DateidExplosion */
UPDATE fact_planorder po
SET po.Dim_DateidExplosion = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidExplosion,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_DateidExplosion = ifnull(df.dim_dateid, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_PALTR
AND pl.CompanyCode = df.CompanyCode
AND ifnull(po.Dim_DateidExplosion,-1) <> ifnull(df.dim_dateid,-2);


/* Q2 - column31 - Dim_DateidAction */
UPDATE fact_planorder po
SET po.Dim_DateidAction = 1		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidAction,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_DateidAction = ifnull(df.dim_dateid, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_MDACD
AND pl.CompanyCode = df.CompanyCode
AND ifnull(po.Dim_DateidAction,-1) <> ifnull(df.dim_dateid,-2);


/* Q2 - column32 - dim_scheduletypeid */
UPDATE fact_planorder po
SET po.dim_scheduletypeid = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_scheduletypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_scheduletypeid = ifnull(st.dim_scheduletypeid, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_scheduletype st, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND st.ScheduleTypeCode = p.PLAF_LVSCH
AND ifnull(po.dim_scheduletypeid,-1) <> ifnull(st.dim_scheduletypeid,-2);



/* Q2 - column33 - dim_availabilityconfirmationid */
UPDATE fact_planorder po
SET po.dim_availabilityconfirmationid = 1		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_availabilityconfirmationid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_availabilityconfirmationid = ifnull(acf.dim_availabilityconfirmationid, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_availabilityconfirmation acf,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND acf.AvailabilityConfirmationCode = p.PLAF_MDPBV
AND ifnull(po.dim_availabilityconfirmationid,-1) <> ifnull(acf.dim_availabilityconfirmationid,-2);


/* Q2 - column34 - Dim_ProcurementId */
UPDATE fact_planorder po
SET po.Dim_ProcurementId = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	    fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_ProcurementId,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_ProcurementId = ifnull(pc.dim_procurementid, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, Dim_Procurement pc, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pc.procurement = p.PLAF_BESKZ
AND ifnull(po.Dim_ProcurementId,-1) <> ifnull(pc.Dim_ProcurementId,-2);



/* Q2 - column35 - Dim_Currencyid */
UPDATE fact_planorder po
SET po.Dim_Currencyid = ifnull(c.Dim_Currencyid	, 1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_Currencyid,-1) <> ifnull(c.Dim_Currencyid,-2);



/* Q2 - column36 - dd_SalesOrderNo */
UPDATE fact_planorder po
SET po.dd_SalesOrderNo = ifnull(p.PLAF_KDAUF, 'Not Set')
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderNo,'xx') <> ifnull(p.PLAF_KDAUF,'yy');



/* Q2 - column37 - dd_SalesOrderItemNo */
UPDATE fact_planorder po
SET po.dd_SalesOrderItemNo = ifnull(p.PLAF_KDPOS, 0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderItemNo,-1) <> ifnull(p.PLAF_KDPOS,-2);


/* Q2 - column38 - dd_SalesOrderScheduleNo */
UPDATE fact_planorder po
SET po.dd_SalesOrderScheduleNo = ifnull(p.PLAF_KDEIN, 0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderScheduleNo,-1) <> ifnull(p.PLAF_KDEIN,-2);


/* Q2 - column39 - ct_GRProcessingTime */
UPDATE fact_planorder po
SET po.ct_GRProcessingTime = ifnull(p.PLAF_WEBAZ,0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_GRProcessingTime,-1) <> ifnull(p.PLAF_WEBAZ,-2);


/* Q2 - column40 - ct_QtyIssued */
UPDATE fact_planorder po
SET po.ct_QtyIssued = ifnull(p.PLAF_WAMNG,0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyIssued,-1) <> ifnull(p.PLAF_WAMNG,-2);


/* Q2 - column41 - ct_QtyCommitted */
UPDATE fact_planorder po
SET po.ct_QtyCommitted = ifnull(p.PLAF_VFMNG,0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyCommitted,-1) <> ifnull(p.PLAF_VFMNG,-2);


/* Q2 - column42 - dd_SequenceNo */
UPDATE fact_planorder po
SET po.dd_SequenceNo = ifnull(p.PLAF_SEQNR, 'Not Set')
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SequenceNo,'xx') <> ifnull(p.PLAF_SEQNR, 'Not Set');


/* Q2 - column43 - dd_PlannScenario */
UPDATE fact_planorder po
SET po.dd_PlannScenario = ifnull(p.PLAF_PLSCN, 'Not Set')
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_PlannScenario,'xx') <> ifnull(p.PLAF_VFMNG,'Not Set');


/* Q2 - column44 - amt_StdUnitPrice */
UPDATE fact_planorder po
SET po.amt_StdUnitPrice = 0		
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.amt_StdUnitPrice,-1) <> 0;


UPDATE fact_planorder po
SET po.amt_StdUnitPrice = ifnull(sp.calcPrice,0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        tmp_fpo_mbew_no_bwtar sp,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sp.MATNR = dp.PartNumber 
AND sp.BWKEY = pl.ValuationArea
AND ifnull(po.amt_StdUnitPrice,-1) <> ifnull(sp.calcPrice,-2);


/* Q2 - column45 - Dim_MRPControllerId */
UPDATE fact_planorder po
SET po.Dim_MRPControllerId = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_MRPControllerId,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_MRPControllerId = ifnull(mc.Dim_MRPControllerId,1)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,Dim_MRPController mc,  fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND mc.MRPController = PLAF_DISPO AND mc.Plant = PLAF_PLWRK
AND ifnull(po.Dim_MRPControllerId,-1) <> ifnull(mc.Dim_MRPControllerId,-2);



/* Q2 - column46 - dd_AgreementNo */
UPDATE fact_planorder po
SET po.dd_AgreementNo = ifnull(p.PLAF_KONNR, 'Not Set')
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_AgreementNo,'xx') <> ifnull(p.PLAF_KONNR,'Not Set');

/* Q2 - column47 - dd_AgreementItemNo */
UPDATE fact_planorder po
SET po.dd_AgreementItemNo = ifnull(p.PLAF_KTPNR, 0)
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_AgreementItemNo,-1) <> ifnull(p.PLAF_KTPNR,0);


/* Q2 - column48 - dim_dateidscheduledstart */
UPDATE fact_planorder po
SET po.dim_dateidscheduledstart = 1	
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_dateidscheduledstart,-1) <> 1;


drop table if exists tmp_fact_planorder_temp2;
create  table  tmp_fact_planorder_temp2
as select distinct (dd.dim_dateid) dim_dateidscheduledstart, po.fact_planorderid, row_number() 
		OVER(partition by po.fact_planorderid ORDER BY dim_dateidscheduledstart DESC) rownumber
FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
	kbko k,
        dim_unitofmeasure uom, dim_date dd,
        fact_planorder po
WHERE  po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND k.kbko_gstrs = dd.DateValue
AND p.plaf_plnum = k.kbko_plnum
AND pl.CompanyCode = dd.CompanyCode;

update fact_planorder po
set po.dim_dateidscheduledstart = ifnull(tmp.dim_dateidscheduledstart,1)
from fact_planorder po, tmp_fact_planorder_temp2 tmp
where po.fact_planorderid = tmp.fact_planorderid
AND ifnull(po.dim_dateidscheduledstart,-1) <> ifnull(tmp.dim_dateidscheduledstart,-2)
AND rownumber = 1;

drop table if exists tmp_fact_planorder_temp2;


/* Q2 - column49 - dim_dateidscheduledfinish */
UPDATE fact_planorder po
SET po.dim_dateidscheduledfinish = 1
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_dateidscheduledfinish,-1) <> 1;


DROP TABLE IF EXISTS tmp_fact_planorder_temp2;
CREATE TABLE tmp_fact_planorder_temp2
AS SELECT DISTINCT(dd.dim_dateid) dim_dateidscheduledfinish, fact_planorderid, row_number() 
		OVER(partition by po.fact_planorderid ORDER BY dim_dateidscheduledfinish DESC) rownumber
FROM fact_planorder po,
     plaf p,
     dim_plant pl,
     dim_company dc,
     dim_currency c,
     dim_part dp,
	 kbko k,
     dim_unitofmeasure uom, dim_date dd
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pl.CompanyCode = dd.CompanyCode
AND k.kbko_gltrs = dd.DateValue
AND p.plaf_plnum = k.kbko_plnum;

UPDATE fact_planorder po
SET po.dim_dateidscheduledfinish = ifnull(tmp.dim_dateidscheduledfinish,1)
FROM fact_planorder po, tmp_fact_planorder_temp2 tmp
WHERE po.fact_planorderid = tmp.fact_planorderid
AND ifnull(po.dim_dateidscheduledfinish,-1) <> ifnull(tmp.dim_dateidscheduledfinish,-2)
AND rownumber = 1;

DROP TABLE IF EXISTS tmp_fact_planorder_temp2;








/*Insert into the fact table*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_planorder';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_planorder',ifnull(max(fact_planorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_planorder;


drop table if exists fact_planorder_tmp;
create table fact_planorder_tmp as select po.dd_PlanOrderNo FROM fact_planorder po;


/* Insert Statement Optimized */


drop table if exists fact_planorder_di09;
Create table fact_planorder_di09 as 
Select  a.*,
	convert( varchar(18), null) PARTNUMBER_upd,
    	convert( varchar(20), null) COMPANYCODE_upd,
 	convert( varchar(20), null) VALUATIONAREA_upd,
	convert( varchar(20), null) PLAF_DISPO_upd,
	convert( varchar(20), null) PLAF_EMATN_upd,
	convert( varchar(20), null) PLAF_EMLIF_upd,
	convert( varchar(20), null) PLAF_FLIEF_upd,
   	convert( char(1), null) PLAF_KNTTP_upd,
   	convert( char(1), null) PLAF_KZVBR_upd,
	convert( varchar(4), null) PLAF_LGORT_upd,
	convert( char(1), null) PLAF_LVSCH_upd,
        convert( date, null) PLAF_MDACD_upd,
 	convert( char(1), null) PLAF_MDPBV_upd,
        convert( char(1), null) PLAF_OBART_upd,
	convert( varchar(10), null) PLAF_PAART_upd,
        convert( date, null) PLAF_PALTR_upd,
        convert( date, null) PLAF_PEDTR_upd,
 	convert( date, null) PLAF_PERTR_upd,
        convert( varchar(3), null) PLAF_PLGRP_upd,
	convert( char(1), null) PLAF_PLNTY_upd,
	convert( varchar(4), null) PLAF_PLWRK_upd,
	convert( varchar(1), null) PLAF_SOBES_upd,
	convert( char(1), null) PLAF_SOBKZ_upd,
	convert( char(1), null) PLAF_STLAN_upd,
	convert( DECIMAL (18,0), null) PLAF_STSTA_upd,
	convert( date, null) PLAF_TERED_upd,
	convert( date, null) PLAF_TERST_upd,
	convert( varchar(2), null) PLAF_TRMER_upd,
	convert( varchar(20), null) PlantCode_upd,
	convert( varchar(18), null) PLAF_MATNR_upd,
	convert( DECIMAL (9,0),null) CalendarYear_upd,
	convert( DECIMAL (9,0),null) FinancialMonthNumber_upd,
	convert( decimal (18,4),null) PLAF_UMREZ_upd,
	convert( decimal (18,4),null) PLAF_UMREN_upd,
	convert( decimal (18,4),null) PLAF_GSMNG_upd,
	convert( varchar(1), null) PLAF_BESKZ_upd
FROM fact_planorder a 
where 1=2;

/* Optimize NOT EXISTS */


DROP TABLE IF EXISTS tmp_ins_fact_planorder;
CREATE TABLE tmp_ins_fact_planorder
AS
SELECT p.plaf_plnum dd_PlanOrderNo
    from plaf p;


/* Need exactly the same table structure for combine to work */
DROP TABLE IF EXISTS tmp_del_fact_planorder;
CREATE TABLE tmp_del_fact_planorder
AS
SELECT * FROM tmp_ins_fact_planorder where 1 = 2;

INSERT INTO tmp_del_fact_planorder
SELECT po.dd_PlanOrderNo
FROM fact_planorder_tmp po;

delete from tmp_ins_fact_planorder 
where dd_PlanOrderNo in (Select distinct dd_PlanOrderNo from tmp_del_fact_planorder);


DROP TABLE IF EXISTS tmp_ins_plaf_fact_planorder;
CREATE TABLE tmp_ins_plaf_fact_planorder
AS
SELECT p.*
FROM plaf p, tmp_ins_fact_planorder t
WHERE p.plaf_plnum = t.dd_PlanOrderNo;

INSERT INTO fact_planorder_di09(fact_planorderid,
                           Dim_ActionStateid,
                           Dim_Companyid,
                           Dim_Partid,
                           Dim_mpnid,
                           Dim_StorageLocationid,
                           Dim_Plantid,
                           Dim_UnitOfMeasureid,
                           Dim_PurchaseOrgid,
                           Dim_Vendorid,
                           Dim_FixedVendorid,
                           Dim_SpecialProcurementid,
                           Dim_ConsumptionTypeid,
                           Dim_AccountCategoryid,
                           Dim_SpecialStockid,
                           dim_bomstatusid,
                           dim_bomusageid,
                           dim_objecttypeid,
                           dim_productionschedulerid,
                           dim_schedulingerrorid,
                           dim_tasklisttypeid,
                           dim_ordertypeid,
                           Dim_dateidStart,
                           Dim_dateidFinish,
                           Dim_dateidOpening,
                           ct_QtyTotal,
                           ct_Completed,
                           amt_ExtendedPrice,
                           dd_PlanOrderNo,
                           dd_bomexplosionno,
                           ct_QtyReduced,
                           Dim_PlanOrderStatusid,
                           Dim_DateidConversionDate,
                           Dim_DateidProductionStart,
                           Dim_DateidProductionFinish,
                           Dim_DateidExplosion,
                           Dim_DateidAction,
                           dim_scheduletypeid,
                           dim_availabilityconfirmationid,
                           dim_currencyid,
						   	dim_currencyid_TRA,
							dim_currencyid_GBL,
							amt_ExchangeRate,
							amt_ExchangeRate_GBL,										   
                           Dim_ProcurementId,
                           dd_SalesOrderNo,
                           dd_SalesOrderItemNo,
                           dd_SalesOrderScheduleNo,
                           ct_GRProcessingTime,
                           ct_QtyIssued,
                           ct_QtyCommitted,
                           dd_SequenceNo,
                           dd_PlannScenario,
                           amt_StdUnitPrice,
                           Dim_MRPControllerId,
                           dd_AgreementNo,
                           dd_AgreementItemNo,
			PARTNUMBER_upd,
			COMPANYCODE_upd,
			VALUATIONAREA_upd,
			PLAF_DISPO_upd,
			PLAF_EMATN_upd,
			PLAF_EMLIF_upd,
			PLAF_FLIEF_upd,
			PLAF_KNTTP_upd,
			PLAF_KZVBR_upd,
			PLAF_LGORT_upd,
			PLAF_LVSCH_upd,
			PLAF_MDACD_upd,
			PLAF_MDPBV_upd,
			PLAF_OBART_upd,
			PLAF_PAART_upd,
			PLAF_PALTR_upd,
			PLAF_PEDTR_upd,
			PLAF_PERTR_upd,
			PLAF_PLGRP_upd,
			PLAF_PLNTY_upd,
			PLAF_PLWRK_upd,
			PLAF_SOBES_upd,
			PLAF_SOBKZ_upd,
			PLAF_STLAN_upd,
			PLAF_STSTA_upd,
			PLAF_TERED_upd,
			PLAF_TERST_upd,
			PLAF_TRMER_upd,
			PlantCode_upd,
			PLAF_MATNR_upd,
			CalendarYear_upd,
			FinancialMonthNumber_upd,
			PLAF_UMREZ_upd,
			PLAF_UMREN_upd,
			PLAF_GSMNG_upd,
			PLAF_BESKZ_upd)
   SELECT IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN 
WHERE TABLE_NAME = 'fact_planorder' ), 1) + ROW_NUMBER() OVER(order by '') 
fact_planorderid,
	2 Dim_ActionStateid,
	Dim_Companyid,
	dp.Dim_Partid,
	1 Dim_mpnid,
	1 Dim_StorageLocationid,
	Dim_Plantid,
	Dim_UnitOfMeasureid,
	Dim_PurchaseOrgid,
	1 Dim_Vendorid,
	1 Dim_FixedVendorid,
	1 Dim_SpecialProcurementid,
	1 Dim_ConsumptionTypeid,
	1 Dim_AccountCategoryid,
	1 Dim_SpecialStockid,
	1 dim_bomstatusid,
	1 dim_bomusageid,
	1 dim_objecttypeid,
	1 dim_productionschedulerid,
	1 dim_schedulingerrorid,
	1 dim_tasklisttypeid,
	1 dim_ordertypeid,
	ds.dim_dateid Dim_dateidStart,
	1 Dim_dateidFinish,
	1 Dim_dateidOpening,
	p.PLAF_GSMNG ct_QtyTotal,
	0 ct_Completed,
	0 amt_ExtendedPrice,
	p.PLAF_PLNUM dd_PlanOrderNo,
	p.PLAF_SERNR dd_bomexplosionno,
	p.plaf_ABMNG ct_QtyReduced,
	1 Dim_PlanOrderStatusid,
	1 Dim_DateidConversionDate,
	1 Dim_DateidProductionStart,
	1 Dim_DateidProductionFinish,
	1 Dim_DateidExplosion,
	1 Dim_DateidAction,
	1 dim_scheduletypeid,
	1 dim_availabilityconfirmationid,
	c.dim_currencyid,
	c.dim_currencyid dim_currencyid_TRA,  /*Default */		  
        convert(bigint, 1) dim_currencyid_GBL,
        1 amt_ExchangeRate,           /*Default */
        convert(decimal (18,4), 0) amt_ExchangeRate_GBL,   /* Order finish date used to calculate exchg rate*/	
	1 Dim_ProcurementId,
	p.PLAF_KDAUF dd_SalesOrderNo,
	p.PLAF_KDPOS dd_SalesOrderItemNo,
	p.PLAF_KDEIN dd_SalesOrderScheduleNo,
	p.PLAF_WEBAZ ct_GRProcessingTime,
	p.PLAF_WAMNG ct_QtyIssued,
	p.PLAF_VFMNG ct_QtyCommitted,
	ifnull(p.PLAF_SEQNR, 'Not Set'),
	ifnull(p.PLAF_PLSCN, 'Not Set'),
	0 amt_StdUnitPrice,
	1 Dim_MRPControllerId,
        ifnull(PLAF_KONNR,'Not Set') dd_AgreementNo,
        ifnull(PLAF_KTPNR,0) dd_AgreementItemNo,
	dp.PARTNUMBER PARTNUMBER_upd,
	pl.COMPANYCODE COMPANYCODE_upd,
	pl.VALUATIONAREA VALUATIONAREA_upd,
	p.PLAF_DISPO PLAF_DISPO_upd,
	p.PLAF_EMATN PLAF_EMATN_upd,
	p.PLAF_EMLIF PLAF_EMLIF_upd,
	p.PLAF_FLIEF PLAF_FLIEF_upd,
	p.PLAF_KNTTP PLAF_KNTTP_upd,
	p.PLAF_KZVBR PLAF_KZVBR_upd,
	p.PLAF_LGORT PLAF_LGORT_upd,
	p.PLAF_LVSCH PLAF_LVSCH_upd,
	p.PLAF_MDACD PLAF_MDACD_upd,
	p.PLAF_MDPBV PLAF_MDPBV_upd,
	p.PLAF_OBART PLAF_OBART_upd,
	p.PLAF_PAART PLAF_PAART_upd,
	p.PLAF_PALTR PLAF_PALTR_upd,
	p.PLAF_PEDTR PLAF_PEDTR_upd,
	p.PLAF_PERTR PLAF_PERTR_upd,
	p.PLAF_PLGRP PLAF_PLGRP_upd,
	p.PLAF_PLNTY PLAF_PLNTY_upd,
	p.PLAF_PLWRK PLAF_PLWRK_upd,
	p.PLAF_SOBES PLAF_SOBES_upd,
	p.PLAF_SOBKZ PLAF_SOBKZ_upd,
	p.PLAF_STLAN PLAF_STLAN_upd,
	p.PLAF_STSTA PLAF_STSTA_upd,
	p.PLAF_TERED PLAF_TERED_upd,
	p.PLAF_TERST PLAF_TERST_upd,
	p.PLAF_TRMER PLAF_TRMER_upd,
	pl.PlantCode PlantCode_upd,
        p.PLAF_MATNR PLAF_MATNR_upd,
        ds.CalendarYear CalendarYear_upd,
        ds.FinancialMonthNumber FinancialMonthNumber_upd,
        p.PLAF_UMREZ PLAF_UMREZ_upd,
        p.PLAF_UMREN PLAF_UMREN_upd,
        p.PLAF_GSMNG PLAF_GSMNG_upd,
        p.PLAF_BESKZ PLAF_BESKZ_upd
FROM tmp_ins_plaf_fact_planorder p
          INNER JOIN dim_plant pl
             ON pl.PlantCode = p.PLAF_PLWRK
          INNER JOIN dim_company dc
             ON dc.CompanyCode = pl.CompanyCode
          INNER JOIN Dim_Currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date ds
             ON ds.DateValue = p.PLAF_PSTTR
                AND pl.CompanyCode = ds.CompanyCode
          INNER JOIN dim_part dp
             ON     dp.PartNumber = p.PLAF_MATNR
                AND dp.Plant = p.PLAF_PLWRK
          INNER JOIN dim_unitofmeasure uom
             ON CONVERT(varchar (7), uom.UOM) = CONVERT(varchar (7), p.plaf_meins)
          INNER JOIN dim_purchaseorg pog
             ON pog.PurchaseOrgCode = pl.PurchOrg;
	     
	     
update fact_planorder_di09 f
set Dim_Currencyid_GBL = ifnull(t.Dim_Currencyid, 1)
from fact_planorder_di09 f 
		CROSS JOIN (SELECT c.Dim_Currencyid 
					FROM dim_currency c, tmp_pGlobalCurrency_planorder tc
                    WHERE c.CurrencyCode = tc.pGlobalCurrency ) t
where Dim_Currencyid_GBL <> ifnull(t.Dim_Currencyid, 1);

update fact_planorder_di09 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_planorder_di09 f
      INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmp_pGlobalCurrency_planorder tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_planorder_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);

   /* WHERE NOT EXISTS
             (SELECT 1
                FROM fact_planorder_tmp po
               WHERE po.dd_PlanOrderNo = p.plaf_plnum)*/

Update fact_planorder_di09
SET Dim_mpnid= IFNULL( mpn.dim_partid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN  dim_part mpn ON mpn.PartNumber = f.PLAF_EMATN_upd 
						AND mpn.Plant = f.PLAF_PLWRK_upd;
		

Update fact_planorder_di09 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = plaf_lgort_upd
						 AND sl.Plant = f.PLAF_PLWRK_upd;
		
Update fact_planorder_di09 f 
Set f.Dim_Vendorid=ifnull( dv.Dim_Vendorid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_vendor dv ON  dv.VendorNumber = f.PLAF_EMLIF_upd;



Update fact_planorder_di09 f 
Set f.Dim_FixedVendorid=ifnull( fv.Dim_Vendorid,1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_vendor fv ON fv.VendorNumber = f.PLAF_FLIEF_upd;



Update fact_planorder_di09 f
Set f.Dim_SpecialProcurementid = ifnull(sp.Dim_SpecialProcurementid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_specialprocurement sp ON sp.specialprocurement = f.PLAF_SOBES_upd;



Update fact_planorder_di09 f
Set f.Dim_ConsumptionTypeid = ifnull(dcp.Dim_ConsumptionTypeid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_consumptiontype dcp ON dcp.ConsumptionCode = f.PLAF_KZVBR_upd;



Update fact_planorder_di09 f
Set f.Dim_AccountCategoryid = ifnull( ac.Dim_AccountCategoryid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_accountcategory ac ON ac.Category = f.plaf_knttp_upd;


Update fact_planorder_di09 f
Set f.Dim_SpecialStockid=ifnull( st.Dim_SpecialStockid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_specialstock st ON st.specialstockindicator = f.PLAF_SOBKZ_upd;


Update fact_planorder_di09 f
Set f.dim_bomstatusid=ifnull( bs.dim_bomstatusid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_bomstatus bs ON bs.BOMStatusCode = f.plaf_ststa_upd;


Update fact_planorder_di09 f
Set f.dim_bomusageid = ifnull( bu.dim_bomusageid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_bomusage bu ON bu.BOMUsageCode = f.plaf_stlan_upd;


Update fact_planorder_di09 f
Set f.dim_objecttypeid = ifnull( ot.dim_objecttypeid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_objecttype ot ON ot.ObjectType = f.PLAF_OBART_upd;


Update fact_planorder_di09 f
Set f.dim_productionschedulerid = ifnull(ps.dim_productionschedulerid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_productionscheduler ps ON ps.ProductionScheduler = f.PLAF_PLGRP_upd
							AND ps.Plant = f.PLAF_PLWRK_upd;

Update fact_planorder_di09 f
Set f.dim_schedulingerrorid = ifnull( se.dim_schedulingerrorid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_schedulingerror se ON se.SchedulingErrorCode = f.PLAF_TRMER_upd;


Update fact_planorder_di09 f
Set f.dim_tasklisttypeid=ifnull( tst.dim_tasklisttypeid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_tasklisttype tst ON tst.TaskListTypeCode = f.PLAF_PLNTY_upd;


Update fact_planorder_di09 f
Set f.dim_ordertypeid = ifnull( ordt.dim_ordertypeid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_ordertype ordt ON ordt.OrderTypeCode = PLAF_PAART_upd;


Update fact_planorder_di09 f
Set f.Dim_dateidFinish=ifnull(df.dim_dateid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_date df ON df.DateValue = f.PLAF_PEDTR_upd 
		                      AND df.CompanyCode = CompanyCode_upd;



Update fact_planorder_di09 f
Set f.Dim_dateidOpening = ifnull( dop.dim_dateid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_date dop ON dop.DateValue = f.PLAF_PERTR_upd 
					AND CompanyCode_upd = dop.CompanyCode;



Update fact_planorder_di09 f
Set f.Dim_DateidProductionStart = ifnull( df.dim_dateid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_date df ON df.DateValue = f.PLAF_TERST_upd
					AND CompanyCode_upd = df.CompanyCode;

Update fact_planorder_di09 f
Set f.Dim_DateidProductionFinish=ifnull(df.dim_dateid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_date df ON df.DateValue = PLAF_TERED_upd
					AND CompanyCode_upd = df.CompanyCode;


Update fact_planorder_di09 f
Set f.Dim_DateidExplosion = ifnull(df.dim_dateid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_date df ON df.DateValue = f.PLAF_PALTR_upd 
					AND CompanyCode_upd = df.CompanyCode;

Update fact_planorder_di09 f
Set Dim_DateidAction = ifnull(df.dim_dateid,1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_date df ON df.DateValue = f.PLAF_MDACD_upd
					AND CompanyCode_upd = df.CompanyCode;


Update fact_planorder_di09 f
Set f.dim_scheduletypeid = ifnull(st.dim_scheduletypeid, 1)
FROM   fact_planorder_di09 f
		LEFT JOIN dim_scheduletype st ON st.ScheduleTypeCode = f.PLAF_LVSCH_upd;


Update fact_planorder_di09 f
Set f.dim_availabilityconfirmationid= ifnull(acf.dim_availabilityconfirmationid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_availabilityconfirmation acf ON acf.AvailabilityConfirmationCode = PLAF_MDPBV_upd;


Update fact_planorder_di09
Set amt_StdUnitPrice = ifnull(
                        (CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0)
FROM  fact_planorder_di09 
     LEFT JOIN  mbew_no_bwtar sp
                ON sp.MATNR = PartNumber_upd AND sp.BWKEY = ValuationArea_upd
WHERE  ((sp.LFGJA * 100) + sp.LFMON) = (SELECT max((x.LFGJA * 100) + x.LFMON)
							       		FROM mbew_no_bwtar x
							       		WHERE    x.MATNR = sp.MATNR 
											 AND x.BWKEY = sp.BWKEY
							                 AND (   (x.VPRSV = 'S' AND x.STPRS > 0)
									              OR (x.VPRSV = 'V' AND x.VERPR > 0)
												 )
                                        )
	AND (   (sp.VPRSV = 'S' AND sp.STPRS > 0)
		 OR (sp.VPRSV = 'V' AND sp.VERPR > 0)) ;
		 

/*  ORIGINAL

Update fact_planorder_di09
Set amt_StdUnitPrice = ifnull(
             (SELECT ifnull(
                        (CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0)
                FROM mbew_no_bwtar sp
               WHERE sp.MATNR = PartNumber_upd AND sp.BWKEY = ValuationArea_upd
                     AND ((sp.LFGJA * 100) + sp.LFMON) =
                            (SELECT max((x.LFGJA * 100) + x.LFMON)
                               FROM mbew_no_bwtar x
                              WHERE x.MATNR = sp.MATNR AND x.BWKEY = sp.BWKEY
                                    AND ((x.VPRSV = 'S' AND x.STPRS > 0)
                                         OR (x.VPRSV = 'V' AND x.VERPR > 0)))
                     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0)
                          OR (sp.VPRSV = 'V' AND sp.VERPR > 0)) ), 0) */

Update fact_planorder_di09 f
Set f.Dim_MRPControllerId = ifnull(mc.Dim_MRPControllerId, 1)
FROM fact_planorder_di09 f
		LEFT JOIN Dim_MRPController mc ON mc.MRPController = f.PLAF_DISPO_upd AND mc.Plant = PLAF_PLWRK_upd;




UPDATE fact_planorder_di09
SET amt_ExtendedPrice = ifnull(standardprice, 0)* PLAF_GSMNG_upd
FROM fact_planorder_di09 f
                LEFT JOIN (select max(STANDARDPRICE)STANDARDPRICE,pCompanyCode,pPlant,pMaterialNo,pFiYear,pPeriod,vUMREZ,vUMREN from tmp_getStdPrice where fact_script_name = 'bi_populate_planorder_fact'
                 group by pCompanyCode,pPlant,pMaterialNo,pFiYear,pPeriod,vUMREZ,vUMREN) tmp ON tmp.pCompanyCode = f.COMPANYCODE_upd AND tmp.pPlant = f.PlantCode_upd
        AND pMaterialNo = f.PLAF_MATNR_upd
        AND tmp.pFiYear = f.CalendarYear_upd
        AND tmp.pPeriod = f.FinancialMonthNumber_upd
        AND vUMREZ = CASE WHEN PLAF_UMREZ_upd = 0 THEN 1 ELSE PLAF_UMREZ_upd END
        AND vUMREN = CASE WHEN PLAF_UMREN_upd = 0 THEN 1 ELSE PLAF_UMREN_upd END;




Update fact_planorder_di09 f
Set f.Dim_ProcurementId = ifnull( pc.dim_procurementid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN Dim_Procurement pc ON pc.procurement = f.PLAF_BESKZ_upd;





INSERT INTO fact_planorder(fact_planorderid,
                           Dim_ActionStateid,
                           Dim_Companyid,
                           Dim_Partid,
                           Dim_mpnid,
                           Dim_StorageLocationid,
                           Dim_Plantid,
                           Dim_UnitOfMeasureid,
                           Dim_PurchaseOrgid,
                           Dim_Vendorid,
                           Dim_FixedVendorid,
                           Dim_SpecialProcurementid,
                           Dim_ConsumptionTypeid,
                           Dim_AccountCategoryid,
                           Dim_SpecialStockid,
                           dim_bomstatusid,
                           dim_bomusageid,
                           dim_objecttypeid,
                           dim_productionschedulerid,
                           dim_schedulingerrorid,
                           dim_tasklisttypeid,
                           dim_ordertypeid,
                           Dim_dateidStart,
                           Dim_dateidFinish,
                           Dim_dateidOpening,
                           ct_QtyTotal,
                           ct_Completed,
                           amt_ExtendedPrice,
                           dd_PlanOrderNo,
                           dd_bomexplosionno,
                           ct_QtyReduced,
                           Dim_PlanOrderStatusid,
                           Dim_DateidConversionDate,
                           Dim_DateidProductionStart,
                           Dim_DateidProductionFinish,
                           Dim_DateidExplosion,
                           Dim_DateidAction,
                           dim_scheduletypeid,
                           dim_availabilityconfirmationid,
                           dim_currencyid,
						   	dim_currencyid_TRA,
							dim_currencyid_GBL,
							amt_ExchangeRate,
							amt_ExchangeRate_GBL,								   
                           Dim_ProcurementId,
			   dd_SalesOrderNo,
                           dd_SalesOrderItemNo,
                           dd_SalesOrderScheduleNo,
                           ct_GRProcessingTime,
                           ct_QtyIssued,
                           ct_QtyCommitted,
                           dd_SequenceNo,
                           dd_PlannScenario,
                           amt_StdUnitPrice,
                           Dim_MRPControllerId,
                           dd_AgreementNo,
                           dd_AgreementItemNo)
Select fact_planorderid,
                           Dim_ActionStateid,
                           Dim_Companyid,
                           Dim_Partid,
                           Dim_mpnid,
                           Dim_StorageLocationid,
                           Dim_Plantid,
                           Dim_UnitOfMeasureid,
                           Dim_PurchaseOrgid,
                           Dim_Vendorid,
                           Dim_FixedVendorid,
                           Dim_SpecialProcurementid,
                           Dim_ConsumptionTypeid,
                           Dim_AccountCategoryid,
                           Dim_SpecialStockid,
                           dim_bomstatusid,
                           dim_bomusageid,
                           dim_objecttypeid,
                           dim_productionschedulerid,
                           dim_schedulingerrorid,
                           dim_tasklisttypeid,
                           dim_ordertypeid,
                           Dim_dateidStart,
                           Dim_dateidFinish,
                           Dim_dateidOpening,
                           ct_QtyTotal,
                           ct_Completed,
                           amt_ExtendedPrice,
                           dd_PlanOrderNo,
                           dd_bomexplosionno,
                           ct_QtyReduced,
                           Dim_PlanOrderStatusid,
                           Dim_DateidConversionDate,
                           Dim_DateidProductionStart,
                           Dim_DateidProductionFinish,
                           Dim_DateidExplosion,
                           Dim_DateidAction,
                           dim_scheduletypeid,
                           dim_availabilityconfirmationid,
                           dim_currencyid,
						   	dim_currencyid_TRA,
							dim_currencyid_GBL,
							amt_ExchangeRate,
							amt_ExchangeRate_GBL,								   
                           Dim_ProcurementId,
			   dd_SalesOrderNo,
                           dd_SalesOrderItemNo,
                           dd_SalesOrderScheduleNo,
                           ct_GRProcessingTime,
                           ct_QtyIssued,
                           ct_QtyCommitted,
                           dd_SequenceNo,
                           dd_PlannScenario,
                           amt_StdUnitPrice,
                           Dim_MRPControllerId,
                           dd_AgreementNo,
                           dd_AgreementItemNo
from fact_planorder_di09;


drop table if exists fact_planorder_di09;


/*Update schedule start and end date*/
drop table if exists tmp_kbko_updt;
create table tmp_kbko_updt
as
select row_number() over(partition by kbko_plnum order by kbko_gstrs desc)rono,kbko_plnum,kbko_gstrs
from kbko;

UPDATE fact_planorder po
SET dim_dateidscheduledstart = ifnull(dd.dim_dateid, 1)
FROM
        tmp_kbko_updt k,
        dim_date dd,
        dim_company dc,
        fact_planorder po
WHERE dd_PlanOrderNo = k.kbko_plnum AND k.kbko_gstrs = dd.DateValue
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND dd.CompanyCode = dc.CompanyCode
        AND k.rono = 1
        AND ifnull(dim_dateidscheduledstart,-1) <> ifnull(dd.dim_dateid,-2);


drop table if exists tmp_kbko_updt;
create table tmp_kbko_updt
as
select row_number() over(partition by kbko_plnum order by kbko_gltrs desc)rono,kbko_plnum,kbko_gltrs
from kbko;

UPDATE fact_planorder po
SET dim_dateidscheduledfinish = ifnull(dd.dim_dateid, 1)
FROM 
	 tmp_kbko_updt k,
	dim_date dd,
	dim_company dc, 
	fact_planorder po
WHERE dd_PlanOrderNo = k.kbko_plnum AND k.kbko_gltrs = dd.DateValue
	AND po.Dim_Companyid = dc.Dim_Companyid
	AND dd.CompanyCode = dc.CompanyCode
	AND k.rono = 1
	AND ifnull(dim_dateidscheduledfinish,-1) <> ifnull(dd.dim_dateid,-2);

drop table if exists tmp_kbko_updt;

UPDATE fact_planorder po
SET po.dim_profitcenterid = ifnull(pc.dim_profitcenterid, 1)
FROM 
	dim_part pt, 
	dim_profitcenter pc, 
	fact_planorder po
WHERE po.dim_PartId = pt.Dim_PartId
 	AND pc.ProfitCenterCode = pt.ProfitCenterCode
 	AND pc.validto >= current_date
 	AND pc.RowIsCurrent = 1
	AND ifnull(po.dim_profitcenterid,1) <> ifnull(pc.dim_profitcenterid,1);


UPDATE fact_planorder po
SET po.dim_profitcenterid = 1
WHERE po.dim_profitcenterid IS NULL;

/*Update plan order miscellaneous dimension*/
UPDATE fact_planorder po
SET po.Dim_PlanOrderMiscid = ifnull(m.Dim_PlanOrderMiscid, 1)
FROM
       dim_planordermisc m,
       plaf p,
       fact_planorder po
 WHERE     po.dd_PlanOrderNo = ifnull(p.PLAF_PLNUM, 'Not Set')
       AND ProductionDateScheduling = ifnull(p.PLAF_PRSCH, 'Not Set')
       AND PlanningWithoutFinalAssembly = ifnull(p.PLAF_VRPLA, 'Not Set')
       AND SubcontractingVendor = ifnull(p.PLAF_LBLKZ, 'Not Set')
       AND ConversionIndicator = ifnull(p.PLAF_UMSKZ, 'Not Set')
       AND FirmingIndicator = ifnull(p.PLAF_AUFFX, 'Not Set')
       AND FixingIndicator = ifnull(p.PLAF_STLFX, 'Not Set')
       AND SchedulingIndicator = ifnull(p.PLAF_TRMKZ, 'Not Set')
       AND AssemblyOrderProcedures = ifnull(p.PLAF_MONKZ, 'Not Set')
       AND LeadingOrder = ifnull(p.PLAF_PRNKZ, 'Not Set');


/* Update currencies and exchange rates from fact_productionorder */


UPDATE    fact_planorder po
SET po.dim_currencyID = ifnull(pod.dim_currencyID, 1)
FROM  fact_productionorder pod,  fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.dim_currencyID <> ifnull(pod.dim_currencyID, 1);


 
UPDATE    fact_planorder po
SET po.dim_currencyID_TRA = ifnull(pod.dim_currencyID_TRA, 1)
FROM fact_productionorder pod, fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.dim_currencyID_TRA <> ifnull(pod.dim_currencyID_TRA, 1);



UPDATE    fact_planorder po
SET po.amt_ExchangeRate = ifnull(pod.amt_ExchangeRate, 1)
FROM fact_productionorder pod, fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.amt_ExchangeRate <> ifnull(pod.amt_ExchangeRate, 1); 

 

UPDATE    fact_planorder po
SET po.amt_ExtendedPrice_GBL = ifnull(pod.amt_ExchangeRate_GBL, 0)
FROM fact_productionorder pod,  fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.amt_ExtendedPrice_GBL <> ifnull(pod.amt_ExchangeRate_GBL, 0);

   

UPDATE    fact_planorder po
SET po.amt_ExtendedPrice_GBL = ifnull(pod.amt_ExchangeRate_GBL, 0)
FROM fact_productionorder pod, fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.amt_ExtendedPrice_GBL <> ifnull(pod.amt_ExchangeRate_GBL, 0);

/* Update amounts to transaction currencies where applicable */
UPDATE  fact_planorder po
SET  po.amt_ExtendedPrice = amt_ExtendedPrice /   amt_ExchangeRate            /* As extended price is in local curr, change that to tran curr */
 WHERE   dim_currencyID <> dim_currencyID_TRA;


UPDATE   fact_planorder po
SET po.amt_StdUnitPrice = amt_StdUnitPrice /   amt_ExchangeRate              /* As amt_StdUnitPrice is in local curr, change that to tran curr */
WHERE   dim_currencyID <> dim_currencyID_TRA;


UPDATE   fact_planorder po
SET po.amt_ExtendedPrice_GBL = amt_ExtendedPrice *   amt_ExchangeRate_GBL
WHERE  dim_currencyID_TRA <> dim_currencyID_GBL;

/* MDG Part */

DROP TABLE IF EXISTS tmp_dim_mdg_partid_plano;
CREATE TABLE tmp_dim_mdg_partid_plano as
SELECT DISTINCT pr.dim_partid, md.dim_mdg_partid, md.partnumber
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18);

UPDATE fact_planorder f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM tmp_dim_mdg_partid_plano tmp,fact_planorder f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

/* Update BW Product and Country Hierarchy - APP-7497 - Oana 10Nov2017 */
merge into fact_planorder f
using (select f.fact_planorderid ,max(bw.dim_bwproducthierarchyid) dim_bwproducthierarchyid
		from fact_planorder f, dim_part dp, dim_bwproducthierarchy bw
		where f.dim_partid = dp.dim_partid
			and dp.producthierarchy = bw.lowerhierarchycode
			and dp.productgroupsbu = bw.upperhierarchycode
			and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
		group by f.fact_planorderid) m
on f.fact_planorderid = m.fact_planorderid
when matched then update set  f.dim_bwproducthierarchyid = m.dim_bwproducthierarchyid
where f.dim_bwproducthierarchyid <> m.dim_bwproducthierarchyid;

update fact_planorder f
set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_planorder f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);

update fact_planorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_planorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-32')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY04' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_planorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_planorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-31','DIV-35','DIV-34')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY05' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_planorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_planorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY07' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_planorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_planorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-87')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY08' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;
/* END Update BW Product and Country Hierarchy - APP-7497 - Oana 10Nov2017 */
/* APP-8176 - Oana 05Dec2017 */
drop table if exists tmp_plannedord_buss_day;
create table tmp_plannedord_buss_day
as
select companycode,BUSINESSDAYSSEQNO,max(datevalue) datevalue,cast(1 as bigint) as dim_dateid
from dim_date
group by companycode,BUSINESSDAYSSEQNO;

update tmp_plannedord_buss_day t
	set t.dim_dateid = dd.dim_dateid
from tmp_plannedord_buss_day t
	inner join dim_date dd on t.companycode = dd.companycode and t.datevalue = dd.datevalue;

update fact_planorder f
set dim_DateidofAvailability = ifnull(dd2.dim_dateid,1)
from fact_planorder f
    inner join dim_date  dd on f.dim_dateidfinish= dd.dim_dateid
    inner join dim_part dim on f.dim_partid = dim.dim_partid
    inner join tmp_plannedord_buss_day dd2 on dd2.companycode = dd.companycode and dd.BUSINESSDAYSSEQNO + GRProcessingTime = dd2.BUSINESSDAYSSEQNO;
/* END APP-8176 - Oana 05Dec2017 */

DROP TABLE IF EXISTS tmp_dim_mdg_partid_plano;

drop table if exists fact_planorder_tmp;
DROP TABLE IF EXISTS tmp_ins_plaf_fact_planorder;
DROP TABLE IF EXISTS tmp_ins_fact_planorder;
DROP TABLE IF EXISTS tmp_del_fact_planorder;



/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate_planorder;
create table tmp_updatedate_planorder as
select max(dw_update_date) dd_updatedate from  fact_planorder;


update fact_planorder f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_planorder f, tmp_updatedate_planorder t
where f.dd_updatedate <> t.dd_updatedate;
