/*****************************************************************************************************************/
/*   Script         : exa_fact_chradar                                                                           */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 21 Feb 2018                                                                                */
/*   Description    : Populating script of fact_chradar                                                          */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   21 Feb 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/


DROP TABLE IF EXISTS tmp_fact_chradar;
CREATE TABLE tmp_fact_chradar
AS
SELECT *
FROM fact_chradar
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_chradar;
CREATE TABLE number_fountain_fact_chradar LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;


/* Step 1 - Preparing the data for each KPI */
-- Unconsolidated Write Off
DROP TABLE IF EXISTS tmp_unconsolidated_writeoff;
CREATE TABLE tmp_unconsolidated_writeoff
AS
SELECT 'Write Offs' as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       audh.type as dd_type,
       ifnull(ns.dd_country, rc.country_code) as dd_country,
       dd_region as dd_region,
       dd_countrycluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(AMT_GCPLR) as amt_mtd_writeoff, -- GC Plan Rate MTD
	   sum(AMT_CGCPLR) as amt_ytd_writeoff -- CUM GC Plan Rate YTD
FROM fact_writeoff ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type = 'Unconsolidated'
     LEFT JOIN repunit_and_country rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit)
	 INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
GROUP BY dd_repunit,
         audh.type,
         ifnull(ns.dd_country, rc.country_code),
         dd_region,
         dd_countrycluster,
         dt.calendarmonthid;

-- Consolidated Write Off
/*DROP TABLE IF EXISTS tmp_consolidated_writeoff
CREATE TABLE tmp_consolidated_writeoff
AS
SELECT 'Write Offs' as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Consolidated' as dd_type,
       ifnull(ns.dd_country, rc.country_code) as dd_country,
       dd_region as dd_region,
       dd_countrycluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(AMT_GCPLR) as amt_mtd_writeoff, -- GC Plan Rate MTD
	   sum(AMT_CGCPLR) as amt_ytd_writeoff -- CUM GC Plan Rate YTD
FROM fact_writeoff ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN repunit_and_country rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit)
	 INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
GROUP BY dd_repunit,
         ifnull(ns.dd_country, rc.country_code),
         dd_region,
         dd_countrycluster,
         dt.calendarmonthid */

-- Unconsolidated Avg DSI
DROP TABLE IF EXISTS tmp_unconsolidated_dsi;
CREATE TABLE tmp_unconsolidated_dsi
AS
SELECT 'DSI' as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       audh.type as dd_type,
       ifnull(ds.dd_country, rc.country_code) as dd_country,
       dd_region as dd_region,
       dd_countrycluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(case when dd_position = '5790062000' then amt_camntgc else 0 end) as amt_dsi_5790062000, -- Cum Amount in GC for 5790062000
	   sum(case when dd_position = '5790061000' then amt_camntgc else 0 end) as amt_dsi_5790061000 -- Cum Amount in GC for 5790061000
FROM fact_dsi ds
     INNER JOIN dim_audithierarchy audh ON ds.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type = 'Unconsolidated'
     LEFT JOIN repunit_and_country rc ON trim(leading '0' from ds.dd_repunit) = trim(leading '0' from rc.reporting_unit)
	 INNER JOIN dim_date dt ON ds.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ds.dd_position in ('5790062000','5790061000')
      AND ds.dd_version = 'A'
GROUP BY dd_repunit,
         audh.type,
         ifnull(ds.dd_country, rc.country_code),
         dd_region,
         dd_countrycluster,
         dt.calendarmonthid;

-- Consolidated Avg DSI
/*DROP TABLE IF EXISTS tmp_consolidated_dsi
CREATE TABLE tmp_consolidated_dsi
AS
SELECT 'DSI' as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Consolidated' as dd_type,
       ifnull(ds.dd_country, rc.country_code) as dd_country,
       dd_region as dd_region,
       dd_countrycluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(case when dd_position = '5790062000' then amt_camntgc else 0 end) as amt_dsi_5790062000, -- Cum Amount in GC for 5790062000
	   sum(case when dd_position = '5790061000' then amt_camntgc else 0 end) as amt_dsi_5790061000 -- Cum Amount in GC for 5790061000
FROM fact_dsi ds
     INNER JOIN dim_audithierarchy audh ON ds.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Unconsolidated', 'Consolidated')
     LEFT JOIN repunit_and_country rc ON trim(leading '0' from ds.dd_repunit) = trim(leading '0' from rc.reporting_unit)
     INNER JOIN dim_date dt ON ds.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ds.dd_position in ('5790062000','5790061000')
      AND ds.dd_version = 'A'
GROUP BY dd_repunit,
         ifnull(ds.dd_country, rc.country_code),
         dd_region,
         dd_countrycluster,
         dt.calendarmonthid */

-- Unconsolidated Net Inventory
DROP TABLE IF EXISTS tmp_unconsolidated_netinventory;
CREATE TABLE tmp_unconsolidated_netinventory
AS
SELECT dd_description as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       audh.type as dd_type,
       ifnull(ns.dd_country, rc.country_code) as dd_country,
       dd_region as dd_region,
       dd_countrycluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(AMT_CGCPLR) as AMT_mtd_netinventory, -- Cum GC Plan Rate
	   sum(AMT_CGCPLR) as AMT_ytd_netinventory -- Cum GC Plan Rate
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type = 'Unconsolidated'
     LEFT JOIN repunit_and_country rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit)
     INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Net Inventory'
GROUP BY dd_description,
         dd_repunit,
         audh.type,
         ifnull(ns.dd_country, rc.country_code),
         dd_region,
         dd_countrycluster,
         dt.calendarmonthid;
		 
-- Consolidated Net Inventory
/*DROP TABLE IF EXISTS tmp_consolidated_netinventory
CREATE TABLE tmp_consolidated_netinventory
AS
SELECT dd_description as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Consolidated' as dd_type,
       ifnull(ns.dd_country, rc.country_code) as dd_country,
       dd_region as dd_region,
       dd_countrycluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(AMT_CGCPLR) as amt_mtd_netinventory, -- Cum GC Plan Rate
	   sum(AMT_CGCPLR) as amt_ytd_netinventory -- Cum GC Plan Rate
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN repunit_and_country rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit)
     INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Net Inventory'
GROUP BY dd_description,
         dd_repunit,
         ifnull(ns.dd_country, rc.country_code),
         dd_region,
         dd_countrycluster,
         dt.calendarmonthid */

-- Unconsolidated Net Sales
DROP TABLE IF EXISTS tmp_unconsolidated_netsales;
CREATE TABLE tmp_unconsolidated_netsales
AS
SELECT dd_description as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Unconsolidated' as dd_type,
       ns.dd_country as dd_country,
       rc.region as dd_region,
       rc.country_cluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(AMT_GCPLR) as amt_mtd_netsales, -- GC Plan Rate MTD
	   sum(AMT_CGCPLR) as amt_ytd_netsales -- CUM GC Plan Rate YTD
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit) AND ns.dd_country = rc.country_code
     INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Net Sales'
GROUP BY dd_description,
         dd_repunit,
         ns.dd_country,
         rc.region,
         rc.country_cluster,
         dt.calendarmonthid;
		 
-- Consolidated Net Sales
/*DROP TABLE IF EXISTS tmp_consolidated_netsales
CREATE TABLE tmp_consolidated_netsales
AS
SELECT dd_description as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Consolidated' as dd_type,
       ns.dd_country as dd_country,
       rc.region as dd_region,
       rc.country_cluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(AMT_GCPLR) as amt_mtd_netsales, -- GC Plan Rate MTD
	   sum(AMT_CGCPLR) as amt_ytd_netsales -- CUM GC Plan Rate YTD
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit) AND ns.dd_country = rc.country_code
     INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Net Sales'
GROUP BY dd_description,
         dd_repunit,
         ns.dd_country,
         rc.region,
         rc.country_cluster,
         dt.calendarmonthid */

-- Unconsolidated Logistics Costs
DROP TABLE IF EXISTS tmp_unconsolidated_logisticscosts;
CREATE TABLE tmp_unconsolidated_logisticscosts
AS
SELECT dd_description as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       audh.type as dd_type,
       ns.dd_country as dd_country,
       rc.region as dd_region,
       rc.country_cluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(AMT_GCPLR) as amt_mtd_logisticscosts, -- GC Plan Rate MTD
	   sum(AMT_CGCPLR) as amt_ytd_logisticscosts -- CUM GC Plan Rate YTD
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type = 'Unconsolidated'
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit) AND ns.dd_country = rc.country_code
     INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Logistics Costs'
GROUP BY dd_description,
         dd_repunit,
         audh.type,
         ns.dd_country,
         rc.region,
         rc.country_cluster,
         dt.calendarmonthid;
		 
-- Consolidated Logistics Costs
/*DROP TABLE IF EXISTS tmp_consolidated_logisticscosts
CREATE TABLE tmp_consolidated_logisticscosts
AS
SELECT dd_description as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Consolidated' as dd_type,
       ns.dd_country as dd_country,
       rc.region as dd_region,
       rc.country_cluster as dd_countrycluster,
       dt.calendarmonthid,
       sum(AMT_GCPLR) as amt_mtd_logisticscosts, -- GC Plan Rate MTD
	   sum(AMT_CGCPLR) as amt_ytd_logisticscosts -- CUM GC Plan Rate YTD
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit) AND ns.dd_country = rc.country_code
     INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Logistics Costs'
GROUP BY dd_description,
         dd_repunit,
         ns.dd_country,
         rc.region,
         rc.country_cluster,
         dt.calendarmonthid */

-- Unconsolidated % LIFR
DROP TABLE IF EXISTS tmp_unconsolidated_perclifr;
CREATE TABLE tmp_unconsolidated_perclifr
AS
SELECT dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       CALENDARMONTHID,
       calendaryear,
       amt_mtd_totalorderedlines,
       amt_mtd_ots,
       sum(amt_mtd_totalorderedlines) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_totalorderedlines,
       sum(amt_mtd_ots) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_ots
FROM (
SELECT '% LIFR' as dd_description,
       trim(leading '0' from lfr.DD_REPUNITNO) as dd_repunit,
       'Unconsolidated' as dd_type,
       lfr.dd_country  as dd_country,
       ifnull(rc.REGION, 'Not Set') as dd_region,
       ifnull(rc.country_cluster, 'Not Set') as dd_countrycluster,
       dt.CALENDARMONTHID,
       dt.calendaryear,
       sum(lfr.CT_TOTALORDEREDLINES) as amt_mtd_totalorderedlines,
       sum(lfr.CT_OTS) as amt_mtd_ots
FROM fact_lifrbo lfr
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from lfr.DD_REPUNITNO) = trim(leading '0' from rc.reporting_unit) AND lfr.dd_country = rc.country_code
     INNER JOIN dim_date dt on dt.dim_dateid = lfr.dim_dateid
WHERE CT_TOTALORDEREDLINES <> 0
GROUP BY lfr.DD_REPUNITNO,
         lfr.dd_country,
         ifnull(rc.REGION, 'Not Set'),
         ifnull(rc.country_cluster, 'Not Set'),
         dt.CALENDARMONTHID,
         dt.calendaryear
) t;

-- Consolidated % LIFR
/*DROP TABLE IF EXISTS tmp_consolidated_perclifr
CREATE TABLE tmp_consolidated_perclifr
AS
SELECT dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       CALENDARMONTHID,
       calendaryear,
       amt_mtd_totalorderedlines,
       amt_mtd_ots,
       sum(amt_mtd_totalorderedlines) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_totalorderedlines,
       sum(amt_mtd_ots) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_ots
FROM (
SELECT '% LIFR' as dd_description,
       trim(leading '0' from lfr.DD_REPUNITNO) as dd_repunit,
       'Consolidated' as dd_type,
       lfr.dd_country  as dd_country,
       ifnull(rc.REGION, 'Not Set') as dd_region,
       ifnull(rc.country_cluster, 'Not Set') as dd_countrycluster,
       dt.CALENDARMONTHID,
       dt.calendaryear,
       sum(lfr.CT_TOTALORDEREDLINES) as amt_mtd_totalorderedlines,
       sum(lfr.CT_OTS) as amt_mtd_ots
FROM fact_lifrbo lfr
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from lfr.DD_REPUNITNO) = trim(leading '0' from rc.reporting_unit) AND lfr.dd_country = rc.country_code
     INNER JOIN dim_date dt on dt.dim_dateid = lfr.dim_dateid
WHERE CT_TOTALORDEREDLINES <> 0
GROUP BY lfr.DD_REPUNITNO,
         lfr.dd_country,
         ifnull(rc.REGION, 'Not Set'),
         ifnull(rc.country_cluster, 'Not Set'),
         dt.CALENDARMONTHID,
         dt.calendaryear
) t */
		 
-- Unconsolidated % Logistics Costs
DROP TABLE IF EXISTS tmp_unconsolidated_perclogisticscosts;
CREATE TABLE tmp_unconsolidated_perclogisticscosts
AS
SELECT '% Logistics Costs' as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Unconsolidated' as dd_type,
       ns.dd_country as dd_country,
       rc.region as dd_region,
       rc.country_cluster as dd_countrycluster,
       dt.CALENDARMONTHID,
       sum(case when dd_description = 'Logistics Costs' AND audh.type = 'Unconsolidated' AND dd_position in ('3110441100','3110441200','3110442100','3110442200','3110442900','3110420000','3110430000') then amt_GCPLR else 0 end) as amt_mtd_perclc_logisticscosts, -- Logistics Costs MTD
	   sum(case when dd_description = 'Net Sales' AND dd_position in ('3010000000','3011000000','3011100000','3011110000','3011120000','3011130000','3011140000','3011150000','3011160000','3011170000','3011180000','3011250000','3011200000','3011210000','3011000000','3012201000','3012200000','3012210000','3012220000') then amt_GCPLR else 0 end) as amt_mtd_perclc_netsales, -- Net Sales MTD
       sum(case when dd_description = 'Logistics Costs' AND audh.type = 'Unconsolidated' AND dd_position in ('3110441100','3110441200','3110442100','3110442200','3110442900','3110420000','3110430000') then AMT_CGCPLR else 0 end) as amt_ytd_perclc_logisticscosts, -- Logistics Costs YTD
	   sum(case when dd_description = 'Net Sales' AND dd_position in ('3010000000','3011000000','3011100000','3011110000','3011120000','3011130000','3011140000','3011150000','3011160000','3011170000','3011180000','3011250000','3011200000','3011210000','3011000000','3012201000','3012200000','3012210000','3012220000') then AMT_CGCPLR else 0 end) as amt_ytd_perclc_netsales -- Net Sales YTD
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit) AND ns.dd_country = rc.country_code
     INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ns.dd_description in ('Logistics Costs', 'Net Sales')
      AND ns.dd_version = 'A'
GROUP BY trim(leading '0' from dd_repunit),
         ns.dd_country,
         rc.region,
         rc.country_cluster,
         dt.CALENDARMONTHID;

-- Consolidated % Logistics Costs
/* DROP TABLE IF EXISTS tmp_consolidated_perclogisticscosts
CREATE TABLE tmp_consolidated_perclogisticscosts
AS
SELECT '% Logistics Costs' as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Consolidated' as dd_type,
       ns.dd_country as dd_country,
       rc.region as dd_region,
       rc.country_cluster as dd_countrycluster,
       dt.CALENDARMONTHID,
       sum(case when dd_description = 'Logistics Costs' AND dd_position in ('3110441100','3110441200','3110442100','3110442200','3110442900','3110420000','3110430000') then amt_GCPLR else 0 end) as amt_mtd_perclc_logisticscosts, -- Logistics Costs MTD
	   sum(case when dd_description = 'Net Sales' AND dd_position in ('3010000000','3011000000','3011100000','3011110000','3011120000','3011130000','3011140000','3011150000','3011160000','3011170000','3011180000','3011250000','3011200000','3011210000','3011000000','3012201000','3012200000','3012210000','3012220000') then amt_GCPLR else 0 end) as amt_mtd_perclc_netsales, -- Net Sales MTD
       sum(case when dd_description = 'Logistics Costs' AND dd_position in ('3110441100','3110441200','3110442100','3110442200','3110442900','3110420000','3110430000') then AMT_CGCPLR else 0 end) as amt_ytd_perclc_logisticscosts, -- Logistics Costs YTD
	   sum(case when dd_description = 'Net Sales' AND dd_position in ('3010000000','3011000000','3011100000','3011110000','3011120000','3011130000','3011140000','3011150000','3011160000','3011170000','3011180000','3011250000','3011200000','3011210000','3011000000','3012201000','3012200000','3012210000','3012220000') then AMT_CGCPLR else 0 end) as amt_ytd_perclc_netsales -- Net Sales YTD
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit) AND ns.dd_country = rc.country_code
     INNER JOIN dim_date dt ON ns.dim_dateid = dt.dim_dateid
WHERE 1 = 1
      AND ns.dd_description in ('Logistics Costs', 'Net Sales')
      AND ns.dd_version = 'A'
GROUP BY trim(leading '0' from dd_repunit),
         ns.dd_country,
         rc.region,
         rc.country_cluster,
         dt.CALENDARMONTHID 
         */
		 
-- Unconsolidated % Forecast Accuracy
DROP TABLE IF EXISTS tmp_unconsolidated_percforecastacc;
CREATE TABLE tmp_unconsolidated_percforecastacc
AS
SELECT dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       calendarmonthid,
       calendaryear,
       amt_mtd_sfaweight,
       amt_mtd_kpiforecastacc,
       sum(amt_mtd_sfaweight) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_sfaweight,
       sum(amt_mtd_kpiforecastacc) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_kpiforecastacc
FROM (
SELECT '% Forecast Accuracy' as dd_description,
       'Not Set' as dd_repunit,
       'Unconsolidated' as dd_type,
       upper(CASE WHEN fc.DD_COUNTRYPLANNINGGROUP = 'FRENCH SPEAKING AFRICA' THEN 'FS' ELSE substr(fc.DD_COUNTRYPLANNINGGROUP, 0, 2) END) as dd_country,
       ifnull(rc.REGION, 'Not Set') as dd_region,
       ifnull(rc.COUNTRY_CLUSTER, 'Not Set') as dd_countrycluster,
       dt.calendarmonthid,
       dt.calendaryear,
       sum(fc.ct_sfaweight) as amt_mtd_sfaweight,
       sum(fc.ct_w_kpiforecastacc) as amt_mtd_kpiforecastacc
FROM fact_forecastaccuracy fc
     LEFT JOIN (select distinct COUNTRY_CODE,REGION,COUNTRY_CLUSTER
                from repunit_and_country) rc ON rc.COUNTRY_CODE = upper(CASE WHEN fc.DD_COUNTRYPLANNINGGROUP = 'FRENCH SPEAKING AFRICA' THEN 'FS' ELSE substr(fc.DD_COUNTRYPLANNINGGROUP, 0, 2) END)
     INNER JOIN dim_date dt ON fc.dim_dateidtime = dt.dim_dateid
WHERE fc.dd_businessdivision = '32'
GROUP BY upper(CASE WHEN fc.DD_COUNTRYPLANNINGGROUP = 'FRENCH SPEAKING AFRICA' THEN 'FS' ELSE substr(fc.DD_COUNTRYPLANNINGGROUP, 0, 2) END),
         ifnull(rc.REGION, 'Not Set'),
         ifnull(rc.COUNTRY_CLUSTER, 'Not Set'),
         dt.calendarmonthid,
         dt.calendaryear
) t;

-- Consolidated % Forecast Accuracy
/*DROP TABLE IF EXISTS tmp_consolidated_percforecastacc
CREATE TABLE tmp_consolidated_percforecastacc
AS
SELECT dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       calendarmonthid,
       calendaryear,
       amt_mtd_sfaweight,
       amt_mtd_kpiforecastacc,
       sum(amt_mtd_sfaweight) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_sfaweight,
       sum(amt_mtd_kpiforecastacc) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_kpiforecastacc
FROM (
SELECT '% Forecast Accuracy' as dd_description,
       'Not Set' as dd_repunit,
       'Consolidated' as dd_type,
       upper(CASE WHEN fc.DD_COUNTRYPLANNINGGROUP = 'FRENCH SPEAKING AFRICA' THEN 'FS' ELSE substr(fc.DD_COUNTRYPLANNINGGROUP, 0, 2) END) as dd_country,
       ifnull(rc.REGION, 'Not Set') as dd_region,
       ifnull(rc.COUNTRY_CLUSTER, 'Not Set') as dd_countrycluster,
       dt.calendarmonthid,
       dt.calendaryear,
       sum(fc.ct_sfaweight) as amt_mtd_sfaweight,
       sum(fc.ct_w_kpiforecastacc) as amt_mtd_kpiforecastacc
FROM fact_forecastaccuracy fc
     LEFT JOIN (select distinct COUNTRY_CODE,REGION,COUNTRY_CLUSTER
                from repunit_and_country) rc ON rc.COUNTRY_CODE = upper(CASE WHEN fc.DD_COUNTRYPLANNINGGROUP = 'FRENCH SPEAKING AFRICA' THEN 'FS' ELSE substr(fc.DD_COUNTRYPLANNINGGROUP, 0, 2) END)
     INNER JOIN dim_date dt ON fc.dim_dateidtime = dt.dim_dateid
WHERE fc.dd_businessdivision = '32'
GROUP BY upper(CASE WHEN fc.DD_COUNTRYPLANNINGGROUP = 'FRENCH SPEAKING AFRICA' THEN 'FS' ELSE substr(fc.DD_COUNTRYPLANNINGGROUP, 0, 2) END),
         ifnull(rc.REGION, 'Not Set'),
         ifnull(rc.COUNTRY_CLUSTER, 'Not Set'),
         dt.calendarmonthid,
         dt.calendaryear
) t
*/

-- Unconsolidated % BO
DROP TABLE IF EXISTS tmp_unconsolidated_percbo;
CREATE TABLE tmp_unconsolidated_percbo
AS
SELECT dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       CALENDARMONTHID,
       calendaryear,
       amt_mtd_percbo_bogid,
       amt_mtd_percbo_gcplr,
       sum(amt_mtd_percbo_bogid) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_percbo_bogid,
       amt_ytd_percbo_gcplr as amt_ytd_percbo_gcplr
FROM (
SELECT '% BO' as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Unconsolidated' as dd_type,
       ns.dd_country as dd_country,
       rc.region as dd_region,
       rc.country_cluster as dd_countrycluster,
       dt.CALENDARMONTHID,
       dt.calendaryear,
       sum(amt_bogid) as amt_mtd_percbo_bogid, -- BO GI value MTD
	   sum(amt_gcplr) as amt_mtd_percbo_gcplr, -- GC Plan Rate MTD
       sum(AMT_CGCPLR) as amt_ytd_percbo_gcplr -- CUM GC Plan Rate YTD
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit) AND ns.dd_country = rc.country_code
     INNER JOIN dim_date dt ON dt.dim_dateid = ns.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Net Sales'
GROUP BY dd_repunit,
         ns.dd_country,
         rc.region,
         rc.country_cluster,
         dt.CALENDARMONTHID,
         dt.calendaryear
) t;
		 
-- Consolidated % BO
/*DROP TABLE IF EXISTS tmp_consolidated_percbo
CREATE TABLE tmp_consolidated_percbo
AS
SELECT dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       CALENDARMONTHID,
       calendaryear,
       amt_mtd_percbo_bogid,
       amt_mtd_percbo_gcplr,
       sum(amt_mtd_percbo_bogid) over(partition by dd_description,dd_repunit, dd_type, dd_country, dd_region, dd_countrycluster,calendaryear order by CALENDARMONTHID) as amt_ytd_percbo_bogid,
       amt_ytd_percbo_gcplr as amt_ytd_percbo_gcplr
FROM (
SELECT '% BO' as dd_description,
       trim(leading '0' from dd_repunit) as dd_repunit,
       'Consolidated' as dd_type,
       ns.dd_country as dd_country,
       rc.region as dd_region,
       rc.country_cluster as dd_countrycluster,
       dt.CALENDARMONTHID,
       dt.calendaryear,
       sum(amt_bogid) as amt_mtd_percbo_bogid, -- BO GI value MTD
	   sum(amt_gcplr) as amt_mtd_percbo_gcplr, -- GC Plan Rate MTD
       sum(AMT_CGCPLR) as amt_ytd_percbo_gcplr -- CUM GC Plan Rate YTD
FROM fact_netsales ns
     INNER JOIN dim_audithierarchy audh ON ns.dim_audithierarchyid = audh.dim_audithierarchyid AND audh.type in('Consolidated','Unconsolidated')
     LEFT JOIN ns_lifr_bo_fa_lo rc ON trim(leading '0' from ns.dd_repunit) = trim(leading '0' from rc.reporting_unit) AND ns.dd_country = rc.country_code
     INNER JOIN dim_date dt ON dt.dim_dateid = ns.dim_dateid
WHERE 1 = 1
      AND ns.dd_description = 'Net Sales'
GROUP BY dd_repunit,
         ns.dd_country,
         rc.region,
         rc.country_cluster,
         dt.CALENDARMONTHID,
         dt.calendaryear
) t */

/* Step 2 - Blend the data into the tmp table */
DROP TABLE IF EXISTS tmp_mindate_month;
CREATE TABLE tmp_mindate_month
AS
SELECT dt.calendarmonthid,
       min(dt.dim_dateid) as dim_dateid
FROM dim_date dt
WHERE dt.companycode = 'Not Set'
GROUP BY dt.calendarmonthid;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       amt_mtd_writeoff,
       amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_writeoff tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       amt_mtd_writeoff,
       amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_consolidated_writeoff tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       amt_dsi_5790062000,
       amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_dsi tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       amt_dsi_5790062000,
       amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_consolidated_dsi tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       amt_mtd_netinventory,
       amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_netinventory tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       amt_mtd_netinventory,
       amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_consolidated_netinventory tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       amt_mtd_netsales,
       amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_netsales tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       amt_mtd_netsales,
       amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_consolidated_netsales tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       amt_mtd_logisticscosts,
       amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_logisticscosts tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       amt_mtd_logisticscosts,
       amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_consolidated_logisticscosts tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       amt_mtd_totalorderedlines,
       amt_mtd_ots,
       amt_ytd_totalorderedlines,
       amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_perclifr tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       amt_mtd_totalorderedlines,
       amt_mtd_ots,
       amt_ytd_totalorderedlines,
       amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_consolidated_perclifr tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       amt_mtd_perclc_logisticscosts,
       amt_mtd_perclc_netsales,
       amt_ytd_perclc_logisticscosts,
       amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_perclogisticscosts tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       amt_mtd_perclc_logisticscosts,
       amt_mtd_perclc_netsales,
       amt_ytd_perclc_logisticscosts,
       amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_consolidated_perclogisticscosts tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       amt_mtd_sfaweight,
       amt_mtd_kpiforecastacc,
       amt_ytd_sfaweight,
       amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_percforecastacc tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       amt_mtd_sfaweight,
       amt_mtd_kpiforecastacc,
       amt_ytd_sfaweight,
       amt_ytd_kpiforecastacc,
       0 as amt_mtd_percbo_bogid,
       0 as amt_mtd_percbo_gcplr,
       0 as amt_ytd_percbo_bogid,
       0 as amt_ytd_percbo_gcplr
FROM tmp_consolidated_percforecastacc tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       amt_mtd_percbo_bogid,
       amt_mtd_percbo_gcplr,
       amt_ytd_percbo_bogid,
       amt_ytd_percbo_gcplr
FROM tmp_unconsolidated_percbo tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid;

/*DELETE FROM number_fountain_fact_chradar

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar */

/*INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       dd_description,
       dd_repunit,
       dd_type,
       dd_country,
       dd_region,
       dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       tmp.calendarmonthid,
       0 as amt_mtd_writeoff,
       0 as amt_ytd_writeoff,
       0 as amt_dsi_5790062000,
       0 as amt_dsi_5790061000,
       0 as amt_mtd_netinventory,
       0 as amt_ytd_netinventory,
       0 as amt_mtd_netsales,
       0 as amt_ytd_netsales,
       0 as amt_mtd_logisticscosts,
       0 as amt_ytd_logisticscosts,
       0 as amt_mtd_totalorderedlines,
       0 as amt_mtd_ots,
       0 as amt_ytd_totalorderedlines,
       0 as amt_ytd_ots,
       0 as amt_mtd_perclc_logisticscosts,
       0 as amt_mtd_perclc_netsales,
       0 as amt_ytd_perclc_logisticscosts,
       0 as amt_ytd_perclc_netsales,
       0 as amt_mtd_sfaweight,
       0 as amt_mtd_kpiforecastacc,
       0 as amt_ytd_sfaweight,
       0 as amt_ytd_kpiforecastacc,
       amt_mtd_percbo_bogid,
       amt_mtd_percbo_gcplr,
       amt_ytd_percbo_bogid,
       amt_ytd_percbo_gcplr
FROM tmp_consolidated_percbo tmp
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = tmp.calendarmonthid */


UPDATE tmp_fact_chradar fct
SET dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_chradar fct;

/* Step 3 - Get the MTD and YTD targets from ch_radar_target */
DROP TABLE IF EXISTS tmp_writeoff_target;
CREATE TABLE tmp_writeoff_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       WRITE_OFFS as amt_mtd_writeoff_target,
       sum(WRITE_OFFS) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_writeoff_target
FROM ch_radar_target where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_mtd_writeoff_target = ifnull(trg.amt_mtd_writeoff_target, 0),
    amt_ytd_writeoff_target = ifnull(trg.amt_ytd_writeoff_target, 0)
FROM tmp_fact_chradar tmp,
     tmp_writeoff_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = 'Write Offs';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff_target,
amt_ytd_writeoff_target
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Write Offs' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_mtd_writeoff_target, 0) as amt_mtd_writeoff_target,
       ifnull(trg.amt_ytd_writeoff_target, 0) as amt_ytd_writeoff_target
FROM tmp_writeoff_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = 'Write Offs');

DROP TABLE IF EXISTS tmp_writeoff_target;

DROP TABLE IF EXISTS tmp_net_inventory_target;
CREATE TABLE tmp_net_inventory_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       net_inventory as amt_mtd_netinventory_target,
       net_inventory as amt_ytd_netinventory_target
FROM ch_radar_target where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_mtd_netinventory_target = ifnull(trg.amt_mtd_netinventory_target, 0),
    amt_ytd_netinventory_target = ifnull(trg.amt_ytd_netinventory_target, 0)
FROM tmp_fact_chradar tmp,
     tmp_net_inventory_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = 'Net Inventory';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_netinventory_target,
amt_ytd_netinventory_target
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Net Inventory' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_mtd_netinventory_target, 0) as amt_mtd_netinventory_target,
       ifnull(trg.amt_ytd_netinventory_target, 0) as amt_ytd_netinventory_target
FROM tmp_net_inventory_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = 'Net Inventory');

DROP TABLE IF EXISTS tmp_net_inventory_target;

DROP TABLE IF EXISTS tmp_netsales_target;
CREATE TABLE tmp_netsales_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       net_sales as amt_mtd_netsales_target,
       sum(net_sales) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_netsales_target
FROM ch_radar_target where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_mtd_netsales_target = ifnull(trg.amt_mtd_netsales_target, 0),
    amt_ytd_netsales_target = ifnull(trg.amt_ytd_netsales_target, 0)
FROM tmp_fact_chradar tmp,
     tmp_netsales_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = 'Net Sales';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_netsales_target,
amt_ytd_netsales_target
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Net Sales' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_mtd_netsales_target, 0) as amt_mtd_netsales_target,
       ifnull(trg.amt_ytd_netsales_target, 0) as amt_ytd_netsales_target
FROM tmp_netsales_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = 'Net Sales');

DROP TABLE IF EXISTS tmp_netsales_target;

DROP TABLE IF EXISTS tmp_logistics_costs_target;
CREATE TABLE tmp_logistics_costs_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       logistics_costs as amt_mtd_logisticscosts_target,
       sum(logistics_costs) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_logisticscosts_target
FROM ch_radar_target  where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_mtd_logisticscosts_target = ifnull(trg.amt_mtd_logisticscosts_target, 0),
    amt_ytd_logisticscosts_target = ifnull(trg.amt_ytd_logisticscosts_target, 0)
FROM tmp_fact_chradar tmp,
     tmp_logistics_costs_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = 'Logistics Costs';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_logisticscosts_target,
amt_ytd_logisticscosts_target
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Logistics Costs' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_mtd_logisticscosts_target, 0) as amt_mtd_logisticscosts_target,
       ifnull(trg.amt_ytd_logisticscosts_target, 0) as amt_ytd_logisticscosts_target
FROM tmp_logistics_costs_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = 'Logistics Costs');

DROP TABLE IF EXISTS tmp_logistics_costs_target;

DROP TABLE IF EXISTS tmp_perc_logistics_costs_target;
CREATE TABLE tmp_perc_logistics_costs_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       logistics_costs as amt_mtd_perclc_logisticscosts_target,
       net_sales as amt_mtd_perclc_netsales_target,
       sum(logistics_costs) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_Ytd_perclc_logisticscosts_target,
       sum(net_sales) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_Ytd_perclc_netsales_target
FROM ch_radar_target  where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_mtd_perclc_logisticscosts_target = ifnull(trg.amt_mtd_perclc_logisticscosts_target, 0),
    amt_mtd_perclc_netsales_target = ifnull(trg.amt_mtd_perclc_netsales_target, 0),
    amt_Ytd_perclc_logisticscosts_target = ifnull(trg.amt_Ytd_perclc_logisticscosts_target, 0),
    amt_Ytd_perclc_netsales_target = ifnull(trg.amt_Ytd_perclc_netsales_target, 0)
FROM tmp_fact_chradar tmp,
     tmp_perc_logistics_costs_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = '% Logistics Costs';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_perclc_logisticscosts_target,
amt_mtd_perclc_netsales_target,
amt_Ytd_perclc_logisticscosts_target,
amt_Ytd_perclc_netsales_target
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       '% Logistics Costs' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_mtd_perclc_logisticscosts_target, 0) as amt_mtd_perclc_logisticscosts_target,
       ifnull(trg.amt_mtd_perclc_netsales_target, 0) as amt_mtd_perclc_netsales_target,
       ifnull(trg.amt_Ytd_perclc_logisticscosts_target, 0) as amt_Ytd_perclc_logisticscosts_target,
       ifnull(trg.amt_Ytd_perclc_netsales_target, 0) as amt_Ytd_perclc_netsales_target
FROM tmp_perc_logistics_costs_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = '% Logistics Costs');

DROP TABLE IF EXISTS tmp_perc_logistics_costs_target;

DROP TABLE IF EXISTS tmp_dsi_target;
CREATE TABLE tmp_dsi_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       avg_inv_dsi as amt_dsi_5790062000_target_colQ,
       avg_cogs_dsi as amt_dsi_5790061000_target_colP
FROM ch_radar_target  where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_dsi_5790062000_target_colQ = ifnull(trg.amt_dsi_5790062000_target_colQ, 0),
    amt_dsi_5790061000_target_colP = ifnull(trg.amt_dsi_5790061000_target_colP, 0)
FROM tmp_fact_chradar tmp,
     tmp_dsi_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = 'DSI';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_dsi_5790062000_target_colQ,
amt_dsi_5790061000_target_colP
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'DSI' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_dsi_5790062000_target_colQ, 0) as amt_dsi_5790062000_target_colQ,
       ifnull(trg.amt_dsi_5790061000_target_colP, 0) as amt_dsi_5790061000_target_colP
FROM tmp_dsi_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = 'DSI');

DROP TABLE IF EXISTS tmp_dsi_target;

DROP TABLE IF EXISTS tmp_perc_forecaststaccuracy_target;
CREATE TABLE tmp_perc_forecaststaccuracy_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       sfa_weight * fcstaccuracy_perc as amt_mtd_fcstaccuracy_perc_target,
       sfa_weight as amt_mtd_sfa_weight_target,
       sum(sfa_weight * fcstaccuracy_perc) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_fcstaccuracy_perc_target,
       sum(sfa_weight) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_sfa_weight_target
FROM ch_radar_target  where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_mtd_fcstaccuracy_perc_target = ifnull(trg.amt_mtd_fcstaccuracy_perc_target, 0),
    amt_mtd_sfa_weight_target = ifnull(trg.amt_mtd_sfa_weight_target, 0),
    amt_ytd_fcstaccuracy_perc_target = ifnull(trg.amt_ytd_fcstaccuracy_perc_target, 0),
    amt_ytd_sfa_weight_target = ifnull(trg.amt_ytd_sfa_weight_target, 0)
FROM tmp_fact_chradar tmp,
     tmp_perc_forecaststaccuracy_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = '% Forecast Accuracy';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_fcstaccuracy_perc_target,
amt_mtd_sfa_weight_target,
amt_ytd_fcstaccuracy_perc_target,
amt_ytd_sfa_weight_target
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       '% Forecast Accuracy' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_mtd_fcstaccuracy_perc_target, 0) as amt_mtd_fcstaccuracy_perc_target,
       ifnull(trg.amt_mtd_sfa_weight_target, 0) as amt_mtd_sfa_weight_target,
       ifnull(trg.amt_ytd_fcstaccuracy_perc_target, 0) as amt_ytd_fcstaccuracy_perc_target,
       ifnull(trg.amt_ytd_sfa_weight_target, 0) as amt_ytd_sfa_weight_target
FROM tmp_perc_forecaststaccuracy_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = '% Forecast Accuracy');

DROP TABLE IF EXISTS tmp_perc_forecaststaccuracy_target;

DROP TABLE IF EXISTS tmp_perc_bo_target;
CREATE TABLE tmp_perc_bo_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       sfa_weight * bo_perc as amt_mtd_percbo_bo_perc_target,
       sfa_weight as amt_mtd_percbo_sfa_weight_target,
       sum(sfa_weight * bo_perc) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_percbo_bo_perc_target,
       sum(sfa_weight) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_percbo_sfa_weight_target
FROM ch_radar_target  where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_mtd_percbo_bo_perc_target = ifnull(trg.amt_mtd_percbo_bo_perc_target, 0),
    amt_mtd_percbo_sfa_weight_target = ifnull(trg.amt_mtd_percbo_sfa_weight_target, 0),
    amt_ytd_percbo_bo_perc_target = ifnull(trg.amt_ytd_percbo_bo_perc_target, 0),
    amt_ytd_percbo_sfa_weight_target = ifnull(trg.amt_ytd_percbo_sfa_weight_target, 0)
FROM tmp_fact_chradar tmp,
     tmp_perc_bo_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = '% BO';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_percbo_bo_perc_target,
amt_mtd_percbo_sfa_weight_target,
amt_ytd_percbo_bo_perc_target,
amt_ytd_percbo_sfa_weight_target
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       '% BO' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_mtd_percbo_bo_perc_target, 0) as amt_mtd_percbo_bo_perc_target,
       ifnull(trg.amt_mtd_percbo_sfa_weight_target, 0) as amt_mtd_percbo_sfa_weight_target,
       ifnull(trg.amt_ytd_percbo_bo_perc_target, 0) as amt_ytd_percbo_bo_perc_target,
       ifnull(trg.amt_ytd_percbo_sfa_weight_target, 0) as amt_ytd_percbo_sfa_weight_target
FROM tmp_perc_bo_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = '% BO');

DROP TABLE IF EXISTS tmp_perc_bo_target;

DROP TABLE IF EXISTS tmp_perc_lifr_target;
CREATE TABLE tmp_perc_lifr_target
AS
SELECT Rep_Unit,
       Country,
       region,
       Country_cluster,
       substr(period,0,4) as period_year,
       Period,
       Type,
       lifr_weight * lifr_perc as amt_mtd_lifr_perc_target,
       lifr_weight as amt_mtd_lifr_weight_target,
       sum(convert(numeric(18,4), lifr_weight) * convert(numeric(18,4), lifr_perc)) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_lifr_perc_target,
       sum(convert(numeric(18,4), lifr_weight)) over(partition by Rep_Unit,Country, region, Country_cluster,substr(period,0,4),type  order by Period) as amt_ytd_lifr_weight_target
FROM ch_radar_target  where type = 'Unconsolidated';

UPDATE tmp_fact_chradar tmp
SET amt_mtd_lifr_perc_target = ifnull(trg.amt_mtd_lifr_perc_target, 0),
    amt_mtd_lifr_weight_target = ifnull(trg.amt_mtd_lifr_weight_target, 0),
    amt_ytd_lifr_perc_target = ifnull(trg.amt_ytd_lifr_perc_target, 0),
    amt_ytd_lifr_weight_target = ifnull(trg.amt_ytd_lifr_weight_target, 0)
FROM tmp_fact_chradar tmp,
     tmp_perc_lifr_target trg
WHERE trg.Rep_Unit = tmp.dd_repunit
      AND trg.Country = tmp.dd_country
      AND trg.region = tmp.dd_region
      AND trg.Country_cluster = tmp.dd_countrycluster
      AND trg.Period = tmp.dd_calendarmonthid
      AND trg.Type = tmp.dd_type
      AND tmp.dd_description = '% LIFR';

DELETE FROM number_fountain_fact_chradar;

INSERT INTO number_fountain_fact_chradar
SELECT 'fact_chradar', ifnull(max(fact_chradarid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_chradar;

INSERT INTO tmp_fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_lifr_perc_target,
amt_mtd_lifr_weight_target,
amt_ytd_lifr_perc_target,
amt_ytd_lifr_weight_target
)
SELECT (SELECT max_id from number_fountain_fact_chradar WHERE table_name = 'fact_chradar') + ROW_NUMBER() over(order by '') AS fact_chradarid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       '% LIFR' as dd_description,
       trg.Rep_Unit as dd_repunit,
       trg.type as dd_type,
       trg.Country as dd_country,
       trg.region as dd_region,
       trg.Country_cluster as dd_countrycluster,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       trg.Period,
       ifnull(trg.amt_mtd_lifr_perc_target, 0) as amt_mtd_lifr_perc_target,
       ifnull(trg.amt_mtd_lifr_weight_target, 0) as amt_mtd_lifr_weight_target,
       ifnull(trg.amt_ytd_lifr_perc_target, 0) as amt_ytd_lifr_perc_target,
       ifnull(trg.amt_ytd_lifr_weight_target, 0) as amt_ytd_lifr_weight_target
FROM tmp_perc_lifr_target trg
     INNER JOIN tmp_mindate_month dt ON dt.calendarmonthid = trg.Period
WHERE NOT EXISTS (SELECT 1
                  FROM tmp_fact_chradar tmp
                  WHERE trg.Rep_Unit = tmp.dd_repunit
                        AND trg.Country = tmp.dd_country
                        AND trg.region = tmp.dd_region
                        AND trg.Country_cluster = tmp.dd_countrycluster
                        AND trg.Period = tmp.dd_calendarmonthid
                        AND trg.Type = tmp.dd_type
                        AND tmp.dd_description = '% LIFR');

DROP TABLE IF EXISTS tmp_perc_lifr_target;

/* Calculate previous months values to be used in the Trend */
DROP TABLE IF EXISTS tmp_fact_chradar_trend_previousmonth;
CREATE TABLE tmp_fact_chradar_trend_previousmonth
AS
SELECT ch.dd_description,
       ch.dd_repunit,
       ch.dd_type,
       ch.dd_country,
       ch.dd_region,
       ch.dd_countrycluster,
       ch.dd_calendarmonthid,
       case when substr(ch.dd_calendarmonthid, 5, 2) = '12' then convert(varchar(100),substr(ch.dd_calendarmonthid, 0, 4) +1) || convert(varchar(100),'01') else ch.dd_calendarmonthid + 1 end prev_month,
       amt_mtd_percbo_bogid as amt_mtd_percbo_bogid_prev,
       amt_mtd_netinventory as amt_mtd_netinventory_prev,
       amt_mtd_logisticscosts as amt_mtd_logisticscosts_prev,
       amt_mtd_netsales as amt_mtd_netsales_prev,
       amt_mtd_writeoff as amt_mtd_writeoff_prev,
       amt_dsi_5790062000 as amt_dsi_5790062000_prev,
       amt_dsi_5790061000 as amt_dsi_5790061000_prev,
       amt_mtd_percbo_gcplr as amt_mtd_percbo_gcplr_prev,
       amt_mtd_kpiforecastacc as amt_mtd_kpiforecastacc_prev,
       amt_mtd_sfaweight as amt_mtd_sfaweight_prev,
       amt_mtd_ots as amt_mtd_ots_prev,
       amt_mtd_totalorderedlines as amt_mtd_totalorderedlines_prev,
       amt_mtd_perclc_netsales as amt_mtd_perclc_netsales_prev,
       amt_mtd_perclc_logisticscosts as amt_mtd_perclc_logisticscosts_prev
FROM tmp_fact_chradar ch;

UPDATE tmp_fact_chradar tmp
SET amt_mtd_percbo_bogid_prev = ifnull(trend.amt_mtd_percbo_bogid_prev, 0),
    amt_mtd_netinventory_prev = ifnull(trend.amt_mtd_netinventory_prev, 0),
    amt_mtd_logisticscosts_prev = ifnull(trend.amt_mtd_logisticscosts_prev, 0),
    amt_mtd_netsales_prev = ifnull(trend.amt_mtd_netsales_prev, 0),
    amt_mtd_writeoff_prev = ifnull(trend.amt_mtd_writeoff_prev, 0),
    amt_dsi_5790062000_prev = ifnull(trend.amt_dsi_5790062000_prev, 0),
    amt_dsi_5790061000_prev = ifnull(trend.amt_dsi_5790061000_prev, 0),
    amt_mtd_percbo_gcplr_prev = ifnull(trend.amt_mtd_percbo_gcplr_prev, 0),
    amt_mtd_kpiforecastacc_prev = ifnull(trend.amt_mtd_kpiforecastacc_prev, 0),
    amt_mtd_sfaweight_prev = ifnull(trend.amt_mtd_sfaweight_prev, 0),
    amt_mtd_ots_prev = ifnull(trend.amt_mtd_ots_prev, 0),
    amt_mtd_totalorderedlines_prev = ifnull(trend.amt_mtd_totalorderedlines_prev, 0),
    amt_mtd_perclc_netsales_prev = ifnull(trend.amt_mtd_perclc_netsales_prev, 0),
    amt_mtd_perclc_logisticscosts_prev = ifnull(trend.amt_mtd_perclc_logisticscosts_prev, 0)
FROM tmp_fact_chradar tmp,
     tmp_fact_chradar_trend_previousmonth trend
WHERE tmp.dd_description = trend.dd_description
      AND tmp.dd_repunit = trend.dd_repunit
      AND tmp.dd_type = trend.dd_type
      AND tmp.dd_country = trend.dd_country
      AND tmp.dd_region = trend.dd_region
      AND tmp.dd_countrycluster = trend.dd_countrycluster
      AND tmp.dd_calendarmonthid = trend.prev_month;

DROP TABLE IF EXISTS tmp_fact_chradar_trend_previousmonth;

TRUNCATE TABLE fact_chradar;
INSERT INTO fact_chradar(
fact_chradarid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_description,
dd_repunit,
dd_type,
dd_country,
dd_region,
dd_countrycluster,
dim_dateid,
dd_calendarmonthid,
amt_mtd_writeoff,
amt_ytd_writeoff,
amt_dsi_5790062000,
amt_dsi_5790061000,
amt_mtd_netinventory,
amt_ytd_netinventory,
amt_mtd_netsales,
amt_ytd_netsales,
amt_mtd_logisticscosts,
amt_ytd_logisticscosts,
amt_mtd_totalorderedlines,
amt_mtd_ots,
amt_ytd_totalorderedlines,
amt_ytd_ots,
amt_mtd_perclc_logisticscosts,
amt_mtd_perclc_netsales,
amt_ytd_perclc_logisticscosts,
amt_ytd_perclc_netsales,
amt_mtd_sfaweight,
amt_mtd_kpiforecastacc,
amt_ytd_sfaweight,
amt_ytd_kpiforecastacc,
amt_mtd_percbo_bogid,
amt_mtd_percbo_gcplr,
amt_ytd_percbo_bogid,
amt_ytd_percbo_gcplr,
amt_mtd_writeoff_target,
amt_ytd_writeoff_target,
amt_mtd_netinventory_target,
amt_ytd_netinventory_target,
amt_mtd_netsales_target,
amt_ytd_netsales_target,
amt_mtd_logisticscosts_target,
amt_ytd_logisticscosts_target,
amt_mtd_perclc_logisticscosts_target,
amt_mtd_perclc_netsales_target,
amt_ytd_perclc_logisticscosts_target,
amt_ytd_perclc_netsales_target,
amt_dsi_5790062000_target_colQ,
amt_dsi_5790061000_target_colP,
amt_mtd_fcstaccuracy_perc_target,
amt_mtd_sfa_weight_target,
amt_ytd_fcstaccuracy_perc_target,
amt_ytd_sfa_weight_target,
amt_mtd_percbo_bo_perc_target,
amt_mtd_percbo_sfa_weight_target,
amt_ytd_percbo_bo_perc_target,
amt_ytd_percbo_sfa_weight_target,
amt_mtd_lifr_perc_target,
amt_mtd_lifr_weight_target,
amt_ytd_lifr_perc_target,
amt_ytd_lifr_weight_target,
amt_mtd_percbo_bogid_prev,
amt_mtd_netinventory_prev,
amt_mtd_logisticscosts_prev,
amt_mtd_netsales_prev,
amt_mtd_writeoff_prev,
amt_dsi_5790062000_prev,
amt_dsi_5790061000_prev,
amt_mtd_percbo_gcplr_prev,
amt_mtd_kpiforecastacc_prev,
amt_mtd_sfaweight_prev,
amt_mtd_ots_prev,
amt_mtd_totalorderedlines_prev,
amt_mtd_perclc_netsales_prev,
amt_mtd_perclc_logisticscosts_prev
)
SELECT fact_chradarid,
       dim_projectsourceid,
       ifnull(amt_exhangerate, 1),
       ifnull(amt_exchangerate_gbl, 1),
       ifnull(dim_currencyid, 1),
       ifnull(dim_currencyid_tra, 1),
       ifnull(dim_currencyid_gbl, 1),
       ifnull(dw_insert_date, current_timestamp),
       ifnull(dw_update_date, current_timestamp),
       dd_description,
       dd_repunit,
       dd_type,
       ifnull(dd_country, 'Not Set'),
       ifnull(dd_region, 'Not Set'),
       ifnull(dd_countrycluster, 'Not Set'),
       dim_dateid,
       dd_calendarmonthid,
       ifnull(amt_mtd_writeoff, 0),
       ifnull(amt_ytd_writeoff, 0),
       ifnull(amt_dsi_5790062000, 0),
       ifnull(amt_dsi_5790061000, 0),
       ifnull(amt_mtd_netinventory, 0),
       ifnull(amt_ytd_netinventory, 0),
       ifnull(amt_mtd_netsales, 0),
       ifnull(amt_ytd_netsales, 0),
       ifnull(amt_mtd_logisticscosts, 0),
       ifnull(amt_ytd_logisticscosts, 0),
       ifnull(amt_mtd_totalorderedlines, 0),
       ifnull(amt_mtd_ots, 0),
       ifnull(amt_ytd_totalorderedlines, 0),
       ifnull(amt_ytd_ots, 0),
       ifnull(amt_mtd_perclc_logisticscosts, 0),
       ifnull(amt_mtd_perclc_netsales, 0),
       ifnull(amt_ytd_perclc_logisticscosts, 0),
       ifnull(amt_ytd_perclc_netsales, 0),
       ifnull(amt_mtd_sfaweight, 0),
       ifnull(amt_mtd_kpiforecastacc, 0),
       ifnull(amt_ytd_sfaweight, 0),
       ifnull(amt_ytd_kpiforecastacc, 0),
       ifnull(amt_mtd_percbo_bogid, 0),
       ifnull(amt_mtd_percbo_gcplr, 0),
       ifnull(amt_ytd_percbo_bogid, 0),
       ifnull(amt_ytd_percbo_gcplr, 0),
       ifnull(amt_mtd_writeoff_target, 0),
       ifnull(amt_ytd_writeoff_target, 0),
       ifnull(amt_mtd_netinventory_target, 0),
       ifnull(amt_ytd_netinventory_target, 0),
       ifnull(amt_mtd_netsales_target, 0),
       ifnull(amt_ytd_netsales_target, 0),
       ifnull(amt_mtd_logisticscosts_target, 0),
       ifnull(amt_ytd_logisticscosts_target, 0),
       ifnull(amt_mtd_perclc_logisticscosts_target, 0),
       ifnull(amt_mtd_perclc_netsales_target, 0),
       ifnull(amt_ytd_perclc_logisticscosts_target, 0),
       ifnull(amt_ytd_perclc_netsales_target, 0),
       ifnull(amt_dsi_5790062000_target_colQ, 0),
       ifnull(amt_dsi_5790061000_target_colP, 0),
       ifnull(amt_mtd_fcstaccuracy_perc_target, 0),
       ifnull(amt_mtd_sfa_weight_target, 0),
       ifnull(amt_ytd_fcstaccuracy_perc_target, 0),
       ifnull(amt_ytd_sfa_weight_target, 0),
       ifnull(amt_mtd_percbo_bo_perc_target, 0),
       ifnull(amt_mtd_percbo_sfa_weight_target, 0),
       ifnull(amt_ytd_percbo_bo_perc_target, 0),
       ifnull(amt_ytd_percbo_sfa_weight_target, 0),
       ifnull(amt_mtd_lifr_perc_target, 0),
       ifnull(amt_mtd_lifr_weight_target, 0),
       ifnull(amt_ytd_lifr_perc_target, 0),
       ifnull(amt_ytd_lifr_weight_target, 0),
       ifnull(amt_mtd_percbo_bogid_prev, 0),
       ifnull(amt_mtd_netinventory_prev, 0),
       ifnull(amt_mtd_logisticscosts_prev, 0),
       ifnull(amt_mtd_netsales_prev, 0),
       ifnull(amt_mtd_writeoff_prev, 0),
       ifnull(amt_dsi_5790062000_prev, 0),
       ifnull(amt_dsi_5790061000_prev, 0),
       ifnull(amt_mtd_percbo_gcplr_prev, 0),
       ifnull(amt_mtd_kpiforecastacc_prev, 0),
       ifnull(amt_mtd_sfaweight_prev, 0),
       ifnull(amt_mtd_ots_prev, 0),
       ifnull(amt_mtd_totalorderedlines_prev, 0),
       ifnull(amt_mtd_perclc_netsales_prev, 0),
       ifnull(amt_mtd_perclc_logisticscosts_prev, 0)
FROM tmp_fact_chradar;

DROP TABLE IF EXISTS tmp_mindate_month;
DROP TABLE IF EXISTS tmp_fact_chradar;

DELETE FROM emd586.fact_chradar;
INSERT INTO emd586.fact_chradar SELECT * FROM fact_chradar;