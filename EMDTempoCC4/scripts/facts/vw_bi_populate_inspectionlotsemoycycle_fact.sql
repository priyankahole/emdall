
DROP TABLE IF EXISTS number_fountain_fact_inspectionlotsemoycycle;
CREATE TABLE number_fountain_fact_inspectionlotsemoycycle 
LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoycycle;
CREATE TABLE tmp_fact_inspectionlotsemoycycle 
LIKE fact_inspectionlotsemoycycle INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_inspectionlotsemoycycle 
WHERE table_name = 'fact_inspectionlotsemoycycle';	

INSERT INTO number_fountain_fact_inspectionlotsemoycycle
SELECT 'fact_inspectionlotsemoycycle', IFNULL(max(f.fact_inspectionlotsemoycycleid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM tmp_fact_inspectionlotsemoycycle AS f;

INSERT INTO tmp_fact_inspectionlotsemoycycle(
  fact_inspectionlotsemoycycleid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_materialname,  -- v_mcockpit_suividelais, materialname
  dd_securitydepartment, -- v_mcockpit_suividelais, securitydepartment 
  dd_lotnumber, -- v_mcockpit_suividelais, u_lotnumber
  dd_textvalue, -- v_mcockpit_suividelais, textvalue
  dd_fabrecpdate, -- v_mcockpit_suividelais, date_fab_recp
  dim_fabrecpdateid,
  dd_receiveddate, -- v_mcockpit_suividelais, receiveddt
  dim_receiveddateid,
  dd_analysedate, -- v_mcockpit_suividelais, date_analyse
  dim_analysedateid,
  dd_maxdatasetreleaseddate, -- v_mcockpit_suividelais, u_maxdatasetreleaseddt
  dim_maxdatasetreleaseddateid,
  dd_completeddate, -- v_mcockpit_suividelais, completedt
  dim_completeddateid,
  dd_batchredate, -- v_mcockpit_suividelais, batch_re 
  dim_batchredateid,
  dd_batchstageredate, -- v_mcockpit_suividelais, batchstage_re
  dim_batchstageredateid,
  dd_batchstagedesc, -- v_mcockpit_suividelais, batchstagedesc
  dd_famillevpfamille, -- v_mcockpit_suiviavancementtest, famille
  dd_famillempfamille, -- v_mcockpit_suiviavancementtest, famille
  ct_delay1, -- Delai 1 : Temps prise en charge
  ct_delay2, -- Delai 2 : Temps analyse
  ct_tempsdetraitementanalyse, -- Temps de traitement analyse
  ct_delay3, -- Delai 3 : Temps verification
  ct_delay4, -- Delai 4 : Temps liberation
  ct_tempsdecycle -- Temps de cycle : Reception > Liberation
)
SELECT
  (SELECT max_id 
    FROM number_fountain_fact_inspectionlotsemoycycle
    WHERE table_name = 'fact_inspectionlotsemoycycle') 
      + ROW_NUMBER() OVER(ORDER BY '') AS fact_inspectionlotsemoycycleid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS dw_insert_date, 
  current_timestamp AS dw_update_date,
  IFNULL(materialname,'Not Set') AS dd_materialname,  -- v_mcockpit_suividelais, materialname
  IFNULL(securitydepartment,'Not Set') AS dd_securitydepartment, -- v_mcockpit_suividelais, securitydepartment 
  IFNULL(u_lotnumber,'Not Set') AS dd_lotnumber, -- v_mcockpit_suividelais, u_lotnumber
  IFNULL(textvalue,'Not Set') AS dd_textvalue, -- v_mcockpit_suividelais, textvalue
  IFNULL(date_fab_recp,'0001-01-01') AS dd_fabrecpdate, -- v_mcockpit_suividelais, date_fab_recp
  CAST(1 AS BIGINT) AS dim_fabrecpdateid,
  IFNULL(receiveddt,'0001-01-01') AS dd_receiveddate, -- v_mcockpit_suividelais, receiveddt
  CAST(1 AS BIGINT) AS dim_receiveddateid,
  IFNULL(date_analyse,'0001-01-01') AS dd_analysedate, -- v_mcockpit_suividelais, date_analyse
  CAST(1 AS BIGINT) AS dim_analysedateid,
  IFNULL(u_maxdatasetreleaseddt,'0001-01-01') AS dd_maxdatasetreleaseddate, -- v_mcockpit_suividelais, u_maxdatasetreleaseddt
  CAST(1 AS BIGINT) AS dim_maxdatasetreleaseddateid,
  IFNULL(completedt,'0001-01-01') AS dd_completeddate, -- v_mcockpit_suividelais, completedt
  CAST(1 AS BIGINT) AS dim_completeddateid,
  IFNULL(batch_re,'0001-01-01') AS dd_batchredate, -- v_mcockpit_suividelais, batch_re 
  CAST(1 AS BIGINT) AS dim_batchredateid,
  IFNULL(batchstage_re,'0001-01-01') AS dd_batchstageredate, -- v_mcockpit_suividelais, batchstage_re
  CAST(1 AS BIGINT) AS dim_batchstageredateid,
  IFNULL(batchstagedesc,'Not Set') AS dd_batchstagedesc, -- v_mcockpit_suividelais, batchstagedesc
  'Not Set' AS dd_famillevpfamille, -- csv_famillevp, famille
  'Not Set' AS dd_famillempfamille, -- csv_famillemp, famille
  0 AS ct_delay1, -- Delai 1 : Temps prise en charge
  0 AS ct_delay2, -- Delai 2 : Temps analyse
  0 as ct_tempsdetraitementanalyse, -- Temps de traitement analyse
  0 AS ct_delay3, -- Delai 3 : Temps verification
  0 AS ct_delay4, -- Delai 4 : Temps liberation
  0 AS ct_tempsdecycle -- Temps de cycle : Reception > Liberation
FROM
  v_mcockpit_suividelais
;

/* BEGIN - update dim dates from fact */
UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_fabrecpdateid = IFNULL(dd.dim_dateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f, dim_date AS dd 
WHERE LEFT(f.dd_fabrecpdate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_fabrecpdateid <> dd.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_receiveddateid = IFNULL(dd.dim_dateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f, dim_date AS dd 
WHERE LEFT(f.dd_receiveddate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_receiveddateid <> dd.dim_dateid;

-- update date value to the previous column where it is blank
UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_receiveddateid = IFNULL(f.dim_fabrecpdateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f
WHERE f.dim_receiveddateid = 1 
  AND f.dim_fabrecpdateid <> 1;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_analysedateid = IFNULL(dd.dim_dateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f, dim_date AS dd 
WHERE LEFT(f.dd_analysedate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_analysedateid <> dd.dim_dateid;

-- update date value to the previous column where it is blank
UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_analysedateid = IFNULL(f.dim_receiveddateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f
WHERE f.dim_analysedateid = 1 
  AND f.dim_receiveddateid <> 1;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_maxdatasetreleaseddateid = IFNULL(dd.dim_dateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f, dim_date AS dd 
WHERE LEFT(f.dd_maxdatasetreleaseddate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_maxdatasetreleaseddateid <> dd.dim_dateid;

-- update date value to the previous column where it is blank
UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_maxdatasetreleaseddateid = IFNULL(f.dim_analysedateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f
WHERE f.dim_maxdatasetreleaseddateid = 1 
  AND f.dim_analysedateid <> 1;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_completeddateid = IFNULL(dd.dim_dateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f, dim_date AS dd 
WHERE LEFT(f.dd_completeddate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_completeddateid <> dd.dim_dateid;

-- update date value to the previous column where it is blank
UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_completeddateid = IFNULL(f.dim_maxdatasetreleaseddateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f
WHERE f.dim_completeddateid = 1 
  AND f.dim_maxdatasetreleaseddateid <> 1;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_batchredateid = IFNULL(dd.dim_dateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f, dim_date AS dd 
WHERE LEFT(f.dd_batchredate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_batchredateid <> dd.dim_dateid;

-- update date value to the previous column where it is blank
UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_batchredateid = IFNULL(f.dim_completeddateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f
WHERE f.dim_batchredateid = 1 
  AND f.dim_completeddateid <> 1;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_batchstageredateid = IFNULL(dd.dim_dateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f, dim_date AS dd 
WHERE LEFT(f.dd_batchstageredate, 10) = dd.datevalue 
  AND dd.CompanyCode = 'Not Set'
  AND f.dim_batchstageredateid <> dd.dim_dateid;

-- update date value to the previous column where it is blank
UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dim_batchstageredateid = IFNULL(f.dim_batchredateid,1)
FROM  tmp_fact_inspectionlotsemoycycle AS f
WHERE f.dim_batchstageredateid = 1 
  AND f.dim_batchredateid <> 1;

/* END - update dim dates from fact */

/* BEGIN - update elements from the families files */

DROP TABLE IF EXISTS tmp_getdistinctmaterialsforfamilyupdate;
CREATE TABLE tmp_getdistinctmaterialsforfamilyupdate
AS 
SELECT 
  DISTINCT materialname, 
  u_materialfamily 
FROM v_mcockpit_suiviavancementtest;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dd_famillempfamille = IFNULL(t.u_materialfamily,'Not Set')
FROM tmp_fact_inspectionlotsemoycycle AS f, 
  tmp_getdistinctmaterialsforfamilyupdate as t
WHERE f.dd_materialname = t.materialname;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.dd_famillevpfamille = IFNULL(t.u_materialfamily,'Not Set')
FROM tmp_fact_inspectionlotsemoycycle AS f, 
  tmp_getdistinctmaterialsforfamilyupdate as t
WHERE f.dd_materialname = t.materialname;

/* END - update elements from the families files */

/* BEGIN - update delay periods */

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.ct_delay1 = IFNULL(d2.businessdaysseqno - d1.businessdaysseqno,1)
FROM tmp_fact_inspectionlotsemoycycle AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_receiveddateid = d1.dim_dateid
  AND f.dim_analysedateid = d2.dim_dateid; 

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.ct_delay2 = IFNULL(d2.businessdaysseqno - d1.businessdaysseqno,1)
FROM tmp_fact_inspectionlotsemoycycle AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_analysedateid = d1.dim_dateid
  AND f.dim_maxdatasetreleaseddateid = d2.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoycycle
SET ct_tempsdetraitementanalyse = IFNULL(ct_delay1 + ct_delay2,1)
FROM tmp_fact_inspectionlotsemoycycle;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.ct_delay3 = IFNULL(d2.businessdaysseqno - d1.businessdaysseqno,1)
FROM tmp_fact_inspectionlotsemoycycle AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_maxdatasetreleaseddateid = d1.dim_dateid
  AND f.dim_completeddateid = d2.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.ct_delay4 = IFNULL(d2.businessdaysseqno - d1.businessdaysseqno,1)
FROM tmp_fact_inspectionlotsemoycycle AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_completeddateid = d1.dim_dateid
  AND f.dim_batchstageredateid = d2.dim_dateid;

UPDATE tmp_fact_inspectionlotsemoycycle AS f
SET f.ct_tempsdecycle = IFNULL(d2.businessdaysseqno - d1.businessdaysseqno,1)
FROM tmp_fact_inspectionlotsemoycycle AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_receiveddateid = d1.dim_dateid
  AND f.dim_batchstageredateid = d2.dim_dateid;


/* END - update delay periods */

DELETE FROM fact_inspectionlotsemoycycle;

INSERT INTO fact_inspectionlotsemoycycle(
  fact_inspectionlotsemoycycleid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_materialname,  -- v_mcockpit_suividelais, materialname
  dd_securitydepartment, -- v_mcockpit_suividelais, securitydepartment 
  dd_lotnumber, -- v_mcockpit_suividelais, u_lotnumber
  dd_textvalue, -- v_mcockpit_suividelais, textvalue
  dd_fabrecpdate, -- v_mcockpit_suividelais, date_fab_recp
  dim_fabrecpdateid,
  dd_receiveddate, -- v_mcockpit_suividelais, receiveddt
  dim_receiveddateid,
  dd_analysedate, -- v_mcockpit_suividelais, date_analyse
  dim_analysedateid,
  dd_maxdatasetreleaseddate, -- v_mcockpit_suividelais, u_maxdatasetreleaseddt
  dim_maxdatasetreleaseddateid,
  dd_completeddate, -- v_mcockpit_suividelais, completedt
  dim_completeddateid,
  dd_batchredate, -- v_mcockpit_suividelais, batch_re 
  dim_batchredateid,
  dd_batchstageredate, -- v_mcockpit_suividelais, batchstage_re
  dim_batchstageredateid,
  dd_batchstagedesc, -- v_mcockpit_suividelais, batchstagedesc
  dd_famillevpfamille, -- csv_famillevp, famille
  dd_famillempfamille, -- csv_famillemp, famille
  ct_delay1, -- Delai 1 : Temps prise en charge
  ct_delay2, -- Delai 2 : Temps analyse
  ct_tempsdetraitementanalyse, -- Temps de traitement analyse
  ct_delay3, -- Delai 3 : Temps verification
  ct_delay4, -- Delai 4 : Temps liberation
  ct_tempsdecycle -- Temps de cycle : Reception > Liberation
)
SELECT
  fact_inspectionlotsemoycycleid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_materialname,  -- v_mcockpit_suividelais, materialname
  dd_securitydepartment, -- v_mcockpit_suividelais, securitydepartment 
  dd_lotnumber, -- v_mcockpit_suividelais, u_lotnumber
  dd_textvalue, -- v_mcockpit_suividelais, textvalue
  dd_fabrecpdate, -- v_mcockpit_suividelais, date_fab_recp
  dim_fabrecpdateid,
  dd_receiveddate, -- v_mcockpit_suividelais, receiveddt
  dim_receiveddateid,
  dd_analysedate, -- v_mcockpit_suividelais, date_analyse
  dim_analysedateid,
  dd_maxdatasetreleaseddate, -- v_mcockpit_suividelais, u_maxdatasetreleaseddt
  dim_maxdatasetreleaseddateid,
  dd_completeddate, -- v_mcockpit_suividelais, completedt
  dim_completeddateid,
  dd_batchredate, -- v_mcockpit_suividelais, batch_re 
  dim_batchredateid,
  dd_batchstageredate, -- v_mcockpit_suividelais, batchstage_re
  dim_batchstageredateid,
  dd_batchstagedesc, -- v_mcockpit_suividelais, batchstagedesc
  dd_famillevpfamille, -- v_mcockpit_suiviavancementtest, famille
  dd_famillempfamille, -- v_mcockpit_suiviavancementtest, famille
  ct_delay1, -- Delai 1 : Temps prise en charge
  ct_delay2, -- Delai 2 : Temps analyse
  ct_tempsdetraitementanalyse, -- Temps de traitement analyse
  ct_delay3, -- Delai 3 : Temps verification
  ct_delay4, -- Delai 4 : Temps liberation
  ct_tempsdecycle -- Temps de cycle : Reception > Liberation
FROM tmp_fact_inspectionlotsemoycycle;

DROP TABLE IF EXISTS tmp_fact_inspectionlotsemoycycle;

DELETE FROM emd586.fact_inspectionlotsemoycycle;

INSERT INTO emd586.fact_inspectionlotsemoycycle
SELECT *
FROM fact_inspectionlotsemoycycle;
