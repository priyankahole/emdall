/* dim_jda_location */

insert into dim_jda_location (dim_jda_locationid,
                              location_id)
select 1, -99999999
from (select 1) a
where not exists ( select 'x' from dim_jda_location where dim_jda_locationid = 1); 

drop table if exists tmp_dw_location;
create table tmp_dw_location as
select   nl.nwmgr_location_location_id		as location_id	 /* taking this ID as base for further table joins */
	    ,nl.nwmgr_location_name				as location_name /* lowest level drill */
		,sl.stsc_loc_loc					as loc			 /* this field can contain Null values, so further joins are preferable to be performed on previous - nwmgr_location_name */
		,sl.stsc_loc_udc_country 			as country
		,sl.stsc_loc_descr					as locdescr
		,sl.stsc_loc_udc_market				as market
		,sl.stsc_loc_udc_region				as region
		,sl.stsc_loc_udc_tpregion			as tpregion
		,sl.stsc_loc_udc_lrpregion			as locgrouping
		,sl.stsc_loc_cust					as dpplanner
		,sl.stsc_loc_udc_scmanager			as scmanager
		,sl.stsc_loc_udc_respcmg			as respcmg
		,sl.stsc_loc_udc_regulflex			as regulflex
		,sl.stsc_loc_udc_reporting_country	as reporting_country
		,sl.stsc_loc_udc_reporting_region	as reporting_region
		,nl.nwmgr_location_uda_exchrate		as exchrate	
		,sl.stsc_loc_udc_gauss_rep_region	as udc_gauss_rep_region
		,nl.nwmgr_location_uda_currency_name as uda_currency_name 
from nwmgr_location nl
		left join stsc_loc sl on ifnull(nwmgr_location_name,'Not Set') = ifnull(stsc_loc_loc, 'Not Set');

update dim_jda_location djl
set djl.country = ifnull(t.country,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.country <> ifnull(t.country,'Not Set');

update dim_jda_location djl
set djl.locdescr = ifnull(t.locdescr,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.locdescr <> ifnull(t.locdescr,'Not Set');
	  
update dim_jda_location djl
set djl.market = ifnull(t.market,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.market <> ifnull(t.market,'Not Set');
	  
update dim_jda_location djl
set djl.region = ifnull(t.region,'Not Set'),
    dw_update_date = current_timestamp
from tmp_dw_location t, dim_jda_location djl
where     djl.location_id = t.location_id
	 and djl.region <> ifnull(t.region,'Not Set');

update dim_jda_location djl
set djl.tpregion = ifnull(t.tpregion,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.tpregion <> ifnull(t.tpregion,'Not Set');

update dim_jda_location djl
set djl.locgrouping = ifnull(t.locgrouping,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.locgrouping <> ifnull(t.locgrouping,'Not Set');

update dim_jda_location djl
set djl.dpplanner = ifnull(t.dpplanner,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.dpplanner <> ifnull(t.dpplanner,'Not Set');

update dim_jda_location djl
set djl.scmanager = ifnull(t.scmanager,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.scmanager <> ifnull(t.scmanager,'Not Set');

update dim_jda_location djl
set djl.respcmg = ifnull(t.respcmg,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.respcmg <> ifnull(t.respcmg,'Not Set');

update dim_jda_location djl
set djl.regulflex = ifnull(t.regulflex,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.regulflex <> ifnull(t.regulflex,'Not Set');

update dim_jda_location djl
set djl.reporting_country = ifnull(t.reporting_country,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.reporting_country <> ifnull(t.reporting_country,'Not Set');

update dim_jda_location djl
set djl.reporting_region = ifnull(t.reporting_region,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.reporting_region <> ifnull(t.reporting_region,'Not Set');
	  
update dim_jda_location djl
set djl.exchrate = ifnull(t.exchrate,0),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where  djl.location_id = t.location_id
	 and ifnull(djl.exchrate,0) <> ifnull(t.exchrate,0);
	  
update dim_jda_location djl
set djl.udc_gauss_rep_region = ifnull(t.udc_gauss_rep_region,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.udc_gauss_rep_region <> ifnull(t.udc_gauss_rep_region,'Not Set');	  
	 
update dim_jda_location djl
set djl.uda_currency_name = ifnull(t.uda_currency_name,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	 and djl.uda_currency_name <> ifnull(t.uda_currency_name,'Not Set');
	  
delete from number_fountain m where m.table_name = 'dim_jda_location';
insert into number_fountain
select 'dim_jda_location',
            ifnull(max(d.dim_jda_locationid ), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_location d
where d.dim_jda_locationid <> 1;

insert into dim_jda_location(
					 dim_jda_locationid	
					,location_id		
					,location_name		
					,loc				
					,country			
					,locdescr			
					,market				
					,region				
					,tpregion			
					,locgrouping		
					,dpplanner			
					,scmanager			
					,respcmg			
					,regulflex			
					,reporting_country	
					,reporting_region	
					,plannig_item_flag
					,exchrate
					,udc_gauss_rep_region
					,uda_currency_name)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_location') + row_number()  over(order by '') AS dim_jda_locationid
		,location_id		
		,ifnull(location_name,'Not Set') 		as location_name		
		,ifnull(loc,'Not Set') 					as loc
		,ifnull(country,'Not Set') 				as country
		,ifnull(locdescr,'Not Set') 			as locdescr
		,ifnull(market,'Not Set') 				as market
		,ifnull(region,'Not Set') 				as region
		,ifnull(tpregion,'Not Set') 			as tpregion
		,ifnull(locgrouping,'Not Set') 			as locgrouping
		,ifnull(dpplanner,'Not Set') 			as dpplanner
		,ifnull(scmanager,'Not Set') 			as scmanager
		,ifnull(respcmg,'Not Set') 				as respcmg
		,ifnull(regulflex,'Not Set')			as regulflex
		,ifnull(reporting_country,'Not Set') 	as reporting_country
		,ifnull(reporting_region,'Not Set')  	as reporting_region
				,case when loc is null and location_name is not null 
				then 'Yes' 
			   else 'Not Set'
		 end as plannig_item_flag
		,ifnull(exchrate,0)						as exchrate
		,ifnull(udc_gauss_rep_region,'Not Set') as udc_gauss_rep_region
		,ifnull(uda_currency_name,'Not Set') 	as uda_currency_name
from tmp_dw_location ds
where not exists (select 'x' from dim_jda_location dc where dc.location_id = ds.location_id);

/* PVE exchange rate for Actual vs Actual Analysis -- Oana 15 March 2017 */
update dim_jda_location dl
set dl.OPRATES_PY_01 = csvar.OPRATES_PY_01,
	dl.OPRATES_PY_02 = csvar.OPRATES_PY_02,
	dl.OPRATES_PY_03 = csvar.OPRATES_PY_03,
	dl.OPRATES_PY_04 = csvar.OPRATES_PY_04,
	dl.OPRATES_PY_05 = csvar.OPRATES_PY_05,
	dl.OPRATES_PY_06 = csvar.OPRATES_PY_06,
	dl.OPRATES_PY_07 = csvar.OPRATES_PY_07,
	dl.OPRATES_PY_08 = csvar.OPRATES_PY_08,
	dl.OPRATES_PY_09 = csvar.OPRATES_PY_09,
	dl.OPRATES_PY_10 = csvar.OPRATES_PY_10,
	dl.OPRATES_PY_11 = csvar.OPRATES_PY_11,
	dl.OPRATES_PY_12 = csvar.OPRATES_PY_12
from dim_jda_location dl , csv_pve_py_actualrate csvar
where  dl.uda_currency_name  = csvar.currency_name;

update dim_jda_location dl
set ACTUALRATE_PY_YTD = case when month(current_date) = 2 then csvar.OPRATES_PY_01
							when month(current_date) = 3 then csvar.OPRATES_PY_02
							when month(current_date) = 4 then csvar.OPRATES_PY_03
							when month(current_date) = 5 then csvar.OPRATES_PY_04
							when month(current_date) = 6 then csvar.OPRATES_PY_05
							when month(current_date) = 7 then csvar.OPRATES_PY_06
							when month(current_date) = 8 then csvar.OPRATES_PY_07
							when month(current_date) = 9 then csvar.OPRATES_PY_08
							when month(current_date) = 10 then csvar.OPRATES_PY_09
							when month(current_date) = 11 then csvar.OPRATES_PY_10
							when month(current_date) = 12 then csvar.OPRATES_PY_11
						end
from dim_jda_location dl , csv_pve_py_actualrate csvar
where  dl.uda_currency_name  = csvar.currency_name;
/* END - PVE exchange rate for Actual vs Actual Analysis -- Oana 15 March 2017 */
