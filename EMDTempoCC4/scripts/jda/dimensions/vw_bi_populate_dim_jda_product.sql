/* dim_jda_product */

insert into dim_jda_product (dim_jda_productid,
                             product_id)
select 1, -99999999
from (select 1) a
where not exists ( select 'x' from dim_jda_product where dim_jda_productid = 1); 

drop table if exists tmp_dw_product;
create table tmp_dw_product as
select	distinct  np.nwmgr_product_product_id 	as product_id		/* taking this ID as base for further table joins */
	    ,np.nwmgr_product_name		 	as product_name		/* lowest level drill */
		,du.dmdunit_dmdunit			 	as itemglobalcode	/* this field can contain Null values, so further joins are preferable to be performed on previous - nwmgr_product_name */
	   	,du.dmdunit_udc_strength		as gbu
		,du.dmdunit_udc_ta				as sbu
		,du.dmdunit_udc_tadescr			as sbudescr
		,du.dmdunit_udc_branddescr		as branddescr
		,du.dmdunit_udc_strengthdescr	as strengthdescr
		,du.dmdunit_udc_pharmaform		as pharmaform
		,du.dmdunit_udc_container		as container
		,du.dmdunit_udc_suppliername	as suppliername
		,du.dmdunit_udc_grouping		as itemgrouping
		,du.dmdunit_descr				as itemdescr
		,du.dmdunit_udc_sopsw			as sopsw
		,du.dmdunit_udc_familygrpdescr	as familygroupdescr
		,du.dmdunit_udc_secpackperbox	as secpackperbox
		,du.dmdunit_udc_gaussglobalcode as gaussglobalcode
		,du.dmdunit_udc_packermfg		as packer
		,du.dmdunit_udc_boxtodoses		as packsize
		,du.dmdunit_udc_sizetobrand		as brandtosize
		,du.dmdunit_udc_dosetomcg		as dosetomcg
		,du.dmdunit_udc_dosetoiu		as dosetoiu
		,du.dmdunit_udc_gps				as gps
		,np.nwmgr_product_uda_boxtodoses  	as uda_boxtodoses
		,np.nwmgr_product_uda_sizetobrand 	as uda_sizetobrand
		,np.nwmgr_product_uda_dosetomcg   	as uda_dosetomcg
		,np.nwmgr_product_uda_dosetoiu    	as uda_dosetoiu
		,du.dmdunit_udc_sbu_desc_yplus1		as udc_sbu_desc_yplus1
		,du.dmdunit_udc_business_line		as udc_business_line
		,np.nwmgr_product_uda_gph			as uda_gph
		,np.nwmgr_product_uda_gphdescr		as uda_gphdescr
		,np.NWMGR_PRODUCT_UDA_GPSNAME		as uda_gpsname
from nwmgr_product np
		left join dmdunit du on ifnull(np.nwmgr_product_name,'Not Set') = ifnull(du.dmdunit_dmdunit,'Not Set');

update dim_jda_product djp
set djp.itemglobalcode = ifnull(t.itemglobalcode,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.itemglobalcode <> ifnull(t.itemglobalcode,'Not Set');

update dim_jda_product djp
set djp.gbu = ifnull(t.gbu,'Not Set'),
    dw_update_date = current_timestamp
from tmp_dw_product t,dim_jda_product djp
where     djp.product_id = t.product_id
	  and djp.gbu <> ifnull(t.gbu,'Not Set');

update dim_jda_product djp
set djp.sbu = ifnull(t.sbu,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.sbu <> ifnull(t.sbu,'Not Set');

update dim_jda_product djp
set djp.sbudescr = ifnull(t.sbudescr,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.sbudescr <> ifnull(t.sbudescr,'Not Set');	  

update dim_jda_product djp
set djp.branddescr = ifnull(t.branddescr,'Not Set'),
    dw_update_date = current_timestamp
from tmp_dw_product t,dim_jda_product djp
where     djp.product_id = t.product_id
	  and djp.branddescr <> ifnull(t.branddescr,'Not Set');	 
	  
update dim_jda_product djp
set djp.strengthdescr = ifnull(t.strengthdescr,'Not Set'),
    dw_update_date = current_timestamp
from  dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.strengthdescr <> ifnull(t.strengthdescr,'Not Set');	 

update dim_jda_product djp
set djp.pharmaform = ifnull(t.pharmaform,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.pharmaform <> ifnull(t.pharmaform,'Not Set');	 

update dim_jda_product djp
set djp.container = ifnull(t.container,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.container <> ifnull(t.container,'Not Set');	 
	  
update dim_jda_product djp
set djp.suppliername = ifnull(t.suppliername,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.suppliername <> ifnull(t.suppliername,'Not Set');
	  
update dim_jda_product djp
set djp.itemgrouping = ifnull(t.itemgrouping,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.itemgrouping <> ifnull(t.itemgrouping,'Not Set');
	  
update dim_jda_product djp
set djp.itemdescr = ifnull(t.itemdescr,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.itemdescr <> ifnull(t.itemdescr,'Not Set');
	  
update dim_jda_product djp
set djp.sopsw = ifnull(t.sopsw,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.sopsw <> ifnull(t.sopsw,0);

update dim_jda_product djp
set djp.familygroupdescr = ifnull(t.familygroupdescr,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.familygroupdescr <> ifnull(t.familygroupdescr,'Not Set');
	  
update dim_jda_product djp
set djp.secpackperbox = ifnull(t.secpackperbox,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.secpackperbox <> ifnull(t.secpackperbox,0);
	  
update dim_jda_product djp
set djp.gaussglobalcode = ifnull(t.gaussglobalcode,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.gaussglobalcode <> ifnull(t.gaussglobalcode,'Not Set');


update dim_jda_product djp
set djp.packer = ifnull(t.packer,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.packer <> ifnull(t.packer,'Not Set');

update dim_jda_product djp
set djp.packsize = ifnull(t.packsize,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.packsize <> ifnull(t.packsize,0);
	  
update dim_jda_product djp
set djp.gps = ifnull(t.gps,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.gps <> ifnull(t.gps,'Not Set');	

/* update Oana 22Aug2016 */
update dim_jda_product djp
set djp.brandtosize = ifnull(t.brandtosize,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.brandtosize <> ifnull(t.brandtosize,0);		
	  
update dim_jda_product djp
set djp.dosetomcg = ifnull(t.dosetomcg,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.dosetomcg <> ifnull(t.dosetomcg,0);	
	  
update dim_jda_product djp
set djp.dosetoiu = ifnull(t.dosetoiu,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.dosetoiu <> ifnull(t.dosetoiu,0);	  	  
/* END update Oana 22Aug2016*/	  
	  
update dim_jda_product djp
set djp.uda_boxtodoses = ifnull(t.uda_boxtodoses,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.uda_boxtodoses <> ifnull(t.uda_boxtodoses,0);

update dim_jda_product djp
set djp.uda_sizetobrand = ifnull(t.uda_sizetobrand,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.uda_sizetobrand <> ifnull(t.uda_sizetobrand,0);	  

update dim_jda_product djp
set djp.uda_dosetomcg = ifnull(t.uda_dosetomcg,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.uda_dosetomcg <> ifnull(t.uda_dosetomcg,0);	

update dim_jda_product djp
set djp.uda_dosetoiu = ifnull(t.uda_dosetoiu,0),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.uda_dosetoiu <> ifnull(t.uda_dosetoiu,0);		

update dim_jda_product djp
set djp.udc_sbu_desc_yplus1 = ifnull(t.udc_sbu_desc_yplus1,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.udc_sbu_desc_yplus1 <> ifnull(t.udc_sbu_desc_yplus1,'Not Set');	  
	  
update dim_jda_product djp
set djp.udc_business_line = ifnull(t.udc_business_line,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.udc_business_line <> ifnull(t.udc_business_line,'Not Set');	
	  
update dim_jda_product djp
set djp.uda_gph = ifnull(t.uda_gph,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.uda_gph <> ifnull(t.uda_gph,'Not Set');	
	  
update dim_jda_product djp
set djp.uda_gphdescr = ifnull(t.uda_gphdescr,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.uda_gphdescr <> ifnull(t.uda_gphdescr,'Not Set');	

update dim_jda_product djp
set djp.uda_gpsname = ifnull(t.uda_gpsname,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_product djp,tmp_dw_product t
where     djp.product_id = t.product_id
	  and djp.uda_gpsname <> ifnull(t.uda_gpsname,'Not Set');	
	  
	  
delete from number_fountain m where m.table_name = 'dim_jda_product';
insert into number_fountain
select 'dim_jda_product',
            ifnull(max(d.dim_jda_productid ), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_product d
where d.dim_jda_productid <> 1;

insert into dim_jda_product(
					 dim_jda_productid	
					,product_id								 
					,product_name							 
					,itemglobalcode							 
					,gbu									 
					,sbu									 
					,sbudescr								 
					,branddescr								 
					,strengthdescr							 
					,pharmaform								 
					,container								 
					,suppliername							 
					,itemgrouping		
					,itemdescr			
					,sopsw				
					,familygroupdescr	
					,secpackperbox		
					,gaussglobalcode	
					,packer				
					,packsize			
					,brandtosize		
					,dosetomcg			
					,dosetoiu			
					,gps					 
					,plannig_item_flag
					,uda_boxtodoses
					,uda_sizetobrand
					,uda_dosetomcg
					,uda_dosetoiu
					,udc_sbu_desc_yplus1
					,udc_business_line
					,uda_gph
					,uda_gphdescr
					,uda_gpsname
					)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_product') + row_number() over(order by '') AS dim_jda_productid
		,product_id								 
		,ifnull(product_name,'Not Set')		as product_name						 
		,ifnull(itemglobalcode,'Not Set')	as itemglobalcode	 
		,ifnull(gbu,'Not Set')			 	as gbu
		,ifnull(sbu,'Not Set')				as sbu
		,ifnull(sbudescr,'Not Set')			as sbudescr		 
		,ifnull(branddescr,'Not Set')		as branddescr			 
		,ifnull(strengthdescr,'Not Set')	as strengthdescr				 
		,ifnull(pharmaform,'Not Set')		as pharmaform		 
		,ifnull(container,'Not Set')		as container			 
		,ifnull(suppliername,'Not Set')		as suppliername			 
		,ifnull(itemgrouping,'Not Set')		as itemgrouping
		,ifnull(itemdescr,'Not Set')		as itemdescr
		,ifnull(sopsw,0)					as sopsw
		,ifnull(familygroupdescr,'Not Set')	as familygroupdescr
		,ifnull(secpackperbox,0)			as secpackperbox
		,ifnull(gaussglobalcode,'Not Set')	as gaussglobalcode
		,ifnull(packer,'Not Set')			as packer
		,ifnull(packsize,0)					as packsize
		,ifnull(brandtosize,0)				as brandtosize
		,ifnull(dosetomcg,0)				as dosetomcg
		,ifnull(dosetoiu,0)					as dosetoiu
		,ifnull(gps,'Not Set')				as gps
		,case when itemglobalcode is null and product_name is not null 
				then 'Yes' 
			   else 'Not Set'
		 end 									as plannig_item_flag
		,ifnull(uda_boxtodoses,0)  				as uda_boxtodoses
		,ifnull(uda_sizetobrand,0) 				as uda_sizetobrand
		,ifnull(uda_dosetomcg,0)   				as uda_dosetomcg
		,ifnull(uda_dosetoiu,0)	   				as uda_dosetoiu	
		,ifnull(udc_sbu_desc_yplus1,'Not Set') 	as udc_sbu_desc_yplus1
		,ifnull(udc_business_line,'Not Set')  	as udc_business_line
		,ifnull(uda_gph,'Not Set') 				as uda_gph
		,ifnull(uda_gphdescr, 'Not Set') 		as uda_gphdescr
		,ifnull(uda_gpsname,'Not Set')			as uda_gpsname
from tmp_dw_product ds
where not exists (select 'x' from dim_jda_product dc where dc.product_id = ds.product_id);

/* PVE enhancement - Oana 13 Jan 2017 */
update dim_jda_product dp
	set final_sbu = case when sbu in ('309','312','521','753','764','912','971') then SBU else 'No' end;
	
update dim_jda_product dp
	set final_aggregation = case when Final_SBU = 'No' then itemglobalcode else uda_gph end;	
/* END PVE enhancement - Oana 13 Jan 2017 */

update dim_jda_product dp
  set product_name_descr = concat(PRODUCT_NAME,' - ',itemdescr);
  
/* Expression test  */

update dim_jda_product
set PRODUCT_NAME2 = product_name||' - '||itemdescr
where PRODUCT_NAME2 <> product_name||' - '||itemdescr;
