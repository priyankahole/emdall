/* dim_jda_skudemandparam */

insert into dim_jda_skudemandparam(DIM_JDA_SKUDEMANDPARAMID,ITEM,LOC,MONTHLYFCST,DW_INSERT_DATE,DW_UPDATE_DATE)
	values(1,'Not Set','Not Set',0,current_timestamp,current_timestamp);

delete from number_fountain m where m.table_name = 'dim_jda_skudemandparam';
insert into number_fountain
select 'dim_jda_skudemandparam',
            ifnull(max(d.dim_jda_skudemandparamid), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_skudemandparam d
where d.dim_jda_skudemandparamid <> 1;


drop table if exists tmp_skudemandparam;
create table tmp_skudemandparam as 
	select sdp.STSC_SKUDEMANDPARAM_ITEM 			as item
		,sdp.STSC_SKUDEMANDPARAM_LOC				as loc
		,sdp.STSC_SKUDEMANDPARAM_UDC_MONTHLYFCST	as monthlyfcst
	from stsc_skudemandparam sdp;

update dim_jda_skudemandparam dsdp
	set dsdp.monthlyfcst = ifnull(tsdp.monthlyfcst,0)
from dim_jda_skudemandparam dsdp,tmp_skudemandparam tsdp
where dsdp.item	= tsdp.item and dsdp.loc	= tsdp.loc;

insert into dim_jda_skudemandparam(DIM_JDA_SKUDEMANDPARAMID,ITEM,LOC,MONTHLYFCST,DW_INSERT_DATE,DW_UPDATE_DATE)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_skudemandparam') + row_number()  over(order by '') as dim_jda_skudemandparamid
		,tsdp.item
		,tsdp.loc
		,ifnull(tsdp.monthlyfcst,0) as monthlyfcst
		,current_timestamp				as dw_insert_date
		,current_timestamp				as dw_update_date
	from tmp_skudemandparam tsdp
	where not exists (select 'x' from dim_jda_skudemandparam dsdp where dsdp.item	= tsdp.item and dsdp.loc	= tsdp.loc)
		and tsdp.item is not null;

drop table if exists tmp_skudemandparam;


