/* dim_jda_item */
insert into dim_jda_item(DIM_JDA_ITEMID,ITEM,ITEMDESCR,DW_INSERT_DATE,DW_UPDATE_DATE,GBU,GBUDESCR,SBU,SBUDESCR,BRAND,BRANDDESCR)
	select 1,'Not Set','Not Set',current_timestamp,current_timestamp,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set'
	from (select 1) where not exists (select 'x' from dim_jda_item where dim_jda_itemid = 1);

delete from number_fountain m where m.table_name = 'dim_jda_item';
insert into number_fountain
select 'dim_jda_item'
        ,ifnull(max(d.dim_jda_itemid)
        ,ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_item d
where d.dim_jda_itemid <> 1;

drop table if exists tmp_jda_item;
create table tmp_jda_item as
	select i.stsc_item_item 				as item
		,i.stsc_item_descr				as itemdescr
		,i.stsc_item_udc_strength		as gbu
		,i.stsc_item_udc_strengthdescr	as gbudescr
		,i.stsc_item_udc_ta				as sbu
		,i.stsc_item_udc_tadescr			as sbudescr
		,i.stsc_item_udc_brand			as brand
		,i.stsc_item_udc_branddescr		as branddescr
		,i.stsc_item_udc_packsize as packsize
		,i.stsc_item_udc_producttype as producttype
		,i.stsc_item_udc_factor as factor
		,du.dmdunit_udc_business_line		as udc_business_line
		,i.stsc_item_udc_container as container
		,i.stsc_item_udc_activeprinciple as activeprinciple
		,i.stsc_item_udc_pharmaform as pharmaform
		,i.stsc_item_udc_equnits as equnits
	from stsc_item i
		left outer join dmdunit du on ifnull(stsc_item_item,'Not Set') = ifnull(du.dmdunit_dmdunit,'Not Set');

update dim_jda_item d
	set d.itemdescr = ifnull(t.itemdescr,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item 
	and d.itemdescr <> ifnull(t.itemdescr,'Not Set');
	
update dim_jda_item d
	set d.gbu = ifnull(t.gbu,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item 
	and d.gbu <> ifnull(t.gbu,'Not Set');	
	
update dim_jda_item d
	set d.gbudescr = ifnull(t.gbudescr,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item 
	and d.gbudescr <> ifnull(t.gbudescr,'Not Set');	
	
update dim_jda_item d
	set d.sbu = ifnull(t.sbu,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item 
	and d.sbu <> ifnull(t.sbu,'Not Set');	
	
update dim_jda_item d
	set d.sbudescr = ifnull(t.sbudescr,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item 
	and d.sbudescr <> ifnull(t.sbudescr,'Not Set');	
	
update dim_jda_item d
	set d.brand = ifnull(t.brand,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item 
	and d.brand <> ifnull(t.brand,'Not Set');	
	
update dim_jda_item d
	set d.branddescr = ifnull(t.branddescr,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item
	and d.branddescr <> ifnull(t.branddescr,'Not Set');

update dim_jda_item d
	set d.producttype = ifnull(t.producttype,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item
	and d.producttype <> ifnull(t.producttype,'Not Set');

update dim_jda_item d
	set d.factor = ifnull(t.factor,0),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item
	and d.factor <> ifnull(t.factor,0);

update dim_jda_item d
	set d.udc_business_line = ifnull(t.udc_business_line,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item
	and d.udc_business_line <> ifnull(t.udc_business_line,'Not Set');

update dim_jda_item d
	set d.container = ifnull(t.container,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item
	and d.container <> ifnull(t.container,'Not Set');

update dim_jda_item d
	set d.activeprinciple = ifnull(t.activeprinciple,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item
	and d.activeprinciple <> ifnull(t.activeprinciple,'Not Set');

update dim_jda_item d
	set d.pharmaform = ifnull(t.pharmaform,'Not Set'),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item
	and d.pharmaform <> ifnull(t.pharmaform,'Not Set');

update dim_jda_item d
	set d.equnits = ifnull(t.equnits,0),
		dw_update_date = current_timestamp
from dim_jda_item d,tmp_jda_item t
where d.item = t.item
	and d.equnits <> ifnull(t.equnits,0);

insert into dim_jda_item(DIM_JDA_ITEMID,ITEM,ITEMDESCR,GBU,GBUDESCR,SBU,SBUDESCR,BRAND,BRANDDESCR,PRODUCTTYPE,FACTOR,udc_business_line,CONTAINER,ACTIVEPRINCIPLE,PHARMAFORM,EQUNITS)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_item') + row_number()  over(order by '') as dim_jda_itemid
		,t.item							as item
		,ifnull(t.itemdescr,'Not Set') 	as itemdescr
		,ifnull(t.gbu,'Not Set')		as gbu
		,ifnull(t.gbudescr,'Not Set')	as gbudescr
		,ifnull(t.sbu,'Not Set')		as sbu
		,ifnull(t.sbudescr,'Not Set')	as sbudescr
		,ifnull(t.brand,'Not Set')		as brand
		,ifnull(t.branddescr,'Not Set')	as branddescr
		,ifnull(t.producttype,'Not Set') as prodcuttype
		,ifnull(t.factor,0) as factor
		,ifnull(t.udc_business_line, 'Not Set') as udc_business_line
		,ifnull(t.container, 'Not Set') as container
		,ifnull(t.activeprinciple, 'Not Set') as activeprinciple
		,ifnull(t.pharmaform, 'Not Set') as pharmaform
		,ifnull(t.equnits, 0) as equnits
	from tmp_jda_item t
	where not exists (select 'x' from dim_jda_item d where d.item = t.item)
		and t.item is not null;
		
drop table if exists tmp_jda_item;