/* dim_jda_sku */
insert into dim_jda_sku (DIM_JDA_SKUID,SKU_ITEM,SKU_LOC,REPLENTYPE,OHPOST,SKUSOURCE,FCSTSW,ACTIVESW,CUSTORDERCOPYSW,DW_INSERT_DATE,DW_UPDATE_DATE)
	select 1,'Not Set','Not Set',0,'0001-01-01','Not Set',0,0,'0',current_timestamp,current_timestamp
		from (select 1) where not exists(select 'x' from dim_jda_sku where dim_jda_skuid = 1);

delete from number_fountain m where m.table_name = 'dim_jda_sku';
insert into number_fountain
select 'dim_jda_sku',
            ifnull(max(d.dim_jda_skuid), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_sku d
where d.dim_jda_skuid <> 1;

drop table if exists tmp_jda_sku;
create table tmp_jda_sku as
	select ss.stsc_sku_item					as item
		,ss.stsc_sku_loc					as loc
		,ss.stsc_sku_replentype				as replentype
		,ss.stsc_sku_ohpost					as ohpost
		,ss.stsc_sku_udc_skusource			as skusource
		,ss.stsc_sku_udc_fcstsw				as fcstsw
		,ss.stsc_sku_udc_activesw			as activesw
		,ss.stsc_sku_udc_custordercopysw	as custordercopysw
	from stsc_sku ss;

update dim_jda_sku d
	set d.replentype = ifnull(t.replentype,'Not Set')
from dim_jda_sku d, tmp_jda_sku t
where d.sku_item = t.item and d.sku_loc = t.loc
	and d.replentype <> t.replentype;

update dim_jda_sku d
	set d.ohpost = ifnull(t.ohpost,'0001-01-01')
from dim_jda_sku d, tmp_jda_sku t
where d.sku_item = t.item and d.sku_loc = t.loc
	and d.ohpost <> t.ohpost;

update dim_jda_sku d
	set d.skusource = ifnull(t.skusource,'Not Set')
from dim_jda_sku d, tmp_jda_sku t
where d.sku_item = t.item and d.sku_loc = t.loc
	and d.skusource <> t.skusource;

update dim_jda_sku d
	set d.fcstsw = ifnull(t.fcstsw,0)
from dim_jda_sku d, tmp_jda_sku t
where d.sku_item = t.item and d.sku_loc = t.loc
	and d.fcstsw <> t.fcstsw;

update dim_jda_sku d
	set d.activesw = ifnull(t.activesw,0)
from dim_jda_sku d, tmp_jda_sku t
where d.sku_item = t.item and d.sku_loc = t.loc
	and d.activesw <> t.activesw;

update dim_jda_sku d
	set d.custordercopysw = ifnull(t.custordercopysw,'0')
from dim_jda_sku d, tmp_jda_sku t
where d.sku_item = t.item and d.sku_loc = t.loc
	and d.custordercopysw <> t.custordercopysw;

insert into dim_jda_sku (DIM_JDA_SKUID,SKU_ITEM,SKU_LOC,REPLENTYPE,OHPOST,SKUSOURCE,FCSTSW,ACTIVESW,CUSTORDERCOPYSW)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_sku') + row_number()  over(order by '') as DIM_JDA_SKUID
		,item	as sku_item
		,loc	as sku_loc
		,ifnull(t.replentype,'Not Set')	as replentype
		,ifnull(t.ohpost,'0001-01-01') 	as ohpost
		,ifnull(t.skusource,'Not Set')	as skusource
		,ifnull(t.fcstsw,0)				as fcstsw
		,ifnull(t.activesw,0)				as activesw
		,ifnull(t.custordercopysw,'0')	as custordercopysw
	from tmp_jda_sku t 
	where not exists (select 'x' from dim_jda_sku d where d.sku_item = t.item and d.sku_loc = t.loc )
		and t.item is not null and t.loc is not null;
		
drop table if exists tmp_jda_sku;

