/* fact_jda_demandforecast */

/* build 1 row table with current dates */
drop table if exists tmp_jda_dateholder;
create table tmp_jda_dateholder as 
select current_date	 			  as currentdate, /* current_date */
	   date_trunc('MONTH', current_date) 	 			  as last_saturday_month,
	   add_months(date_trunc('MONTH', current_date), - 1) as LAG0_month,
	   add_months(date_trunc('MONTH', current_date), - 2) as LAG1_month,
	   max(datevalue) as LASTDAY
	   from dim_date dd
       where dd.calendaryear = year(current_date)
            and dd.calendarmonthnumber = month(current_date)
            and dd.companycode = 'Not Set'; 

	/* populate last_saturday of the current date */
	drop table if exists tmp_last_saturday;
	create table tmp_last_saturday as
	select min(d.datevalue) last_saturday
	from dim_date d, tmp_jda_dateholder t
	where    d.datevalue  >  LASTDAY - 7 
		 and d.datevalue  <= LASTDAY
	  	 and d.companycode = 'Not Set'
	  	 and trim(d.weekdayname) = 'Saturday';
	  	 	   
	update tmp_jda_dateholder t
	set t.last_saturday_month = ls.last_saturday
	from tmp_jda_dateholder t,tmp_last_saturday ls;
	
	drop table if exists tmp_last_saturday;
	
	/* LAG0_month */
	drop table if exists tmp_lag0_month;
	create table tmp_lag0_month as 
	select case when currentdate > last_saturday_month
		   		  then add_months(date_trunc('MONTH', last_saturday_month + 10), -1)
		   		else   add_months(date_trunc('MONTH', currentdate)			   , -1)
		   end LAG0_month
	from tmp_jda_dateholder;

	update tmp_jda_dateholder t
	set t.LAG0_month = l.LAG0_month
	from tmp_jda_dateholder t,tmp_lag0_month l;
	
	drop table if exists tmp_lag0_month;
	
	/* LAG1_month */
	drop table if exists tmp_lag1_month;
	create table tmp_lag1_month as 
	select case when currentdate > last_saturday_month
		   		  then add_months(date_trunc('MONTH', last_saturday_month + 10), -2)
		   		else   add_months(date_trunc('MONTH', currentdate)			   , -2)
		   end LAG1_month
	from tmp_jda_dateholder;

	update tmp_jda_dateholder t
	set t.LAG1_month = l.LAG1_month
	from tmp_jda_dateholder t,tmp_lag1_month l;
	
	drop table if exists tmp_lag1_month;
	
	delete from dim_jda_dateholder;
	insert into dim_jda_dateholder(dim_jda_dateholderid, currentdate, last_saturday_month, lag0_month, lag1_month)
	select 10000000001, currentdate, last_saturday_month, lag0_month, lag1_month
	from tmp_jda_dateholder;


/* get planning item merged values */

	/* append additional values to STSC_DFUVIEW table from other ones */
	drop table if exists tmp_STSC_DFUVIEW_group;
	create table tmp_STSC_DFUVIEW_group as
	select t1.*,
		   t2.stsc_dc_dfuclass_class
	from STSC_DFUVIEW t1
			left join (select * 
					   from STSC_DC_DFUCLASS 
					   where stsc_dc_dfuclass_histstream = 'SHIPMENTS') t2 on   t1.stsc_dfuview_dmdunit = t2.stsc_dc_dfuclass_dmdunit
											and t1.stsc_dfuview_loc = t2.stsc_dc_dfuclass_loc
											and t1.stsc_dfuview_dmdgroup = t2.stsc_dc_dfuclass_dmdgroup;

	drop table if exists tmp_dw_planningitem;
	create table tmp_dw_planningitem as
	select  pi.nwmgr_planning_item_planning_item_id as planning_item_id	/* taking this id for further joins to nwmgr_version table */
		   ,pi.enterprise_id
	       ,pi.dim_jda_productid  										/* part of lowest level drill, unique key combination */
	       ,pi.dim_jda_locationid										/* part of lowest level drill, unique key combination */
	       ,ifnull(pi.pi_dmdgroup,'Not Set')   as pi_dmdgroup			/* part of lowest level drill, unique key combination */
	       ,ifnull(pi.product_name,'Not Set')  as product_name
	       ,ifnull(pi.location_name,'Not Set') as location_name
		   ,ifnull(sd.stsc_dfuview_dmdunit,'Not Set')  as stsc_dfuview_dmdunit		
		   ,ifnull(sd.stsc_dfuview_dmdgroup,'Not Set') as stsc_dfuview_dmdgroup 	
		   ,ifnull(sd.stsc_dfuview_loc,'Not Set')	   as stsc_dfuview_loc 		
		   ,ifnull(sd.stsc_dfuview_udc_countryabc, 'Not Set')	as abc_class 		/* varchar(50) */
		   ,ifnull(sd.stsc_dfuview_udc_localbrand, 'Not Set')	as localbrand		/* varchar(100) */
		   ,ifnull(sd.stsc_dfuview_udc_prodlocalid, 'Not Set')	as itemlocalcode	/* varchar(50) */
		   ,ifnull(sd.stsc_dfuview_udc_dfumodel, 'Not Set')		as model			/* varchar(18) */
		   ,ifnull(sd.stsc_dfuview_udc_fcstlevel, 'Not Set')	as fcstlevel		/* varchar(50) */
		   ,ifnull(sd.STSC_DFUVIEW_UDC_DFULIFECYCLE, 'Not Set')	as DFULIFECYCLE		/* VARCHAR(2) -> goes to 7 */
		   ,ifnull(sd.STSC_DFUVIEW_UDC_GAUSSITEMCODE, 'Not Set') as GAUSSITEMCODE	/* varchar(50) */
		   ,ifnull(sd.STSC_DFUVIEW_UDC_AVGSALESPMONTH, 0)	as AVGSALESPMONTH		/* DECIMAL(29,9) */
		   ,ifnull(sd.STSC_DFUVIEW_UDC_AVGFCST, 0)			as AVGFCST		/* DECIMAL(29,9) */
		   ,ifnull(sd.STSC_DFUVIEW_UDC_OP_EURO, 0)			as OP_EURO		/* DECIMAL(15,2) */
		   ,ifnull(sd.stsc_dc_dfuclass_class, 'Not Set') 	as dfuclass		/* VARCHAR(5) -> goes to 7 */
		   ,ifnull(STSC_DFUVIEW_UDC_AVG3M_BIAS1M, 0) 		as AVG3M_BIAS1M
		   ,ifnull(STSC_DFUVIEW_UDC_AVG6M_BIAS1M, 0) 		as AVG6M_BIAS1M
		   ,ifnull(STSC_DFUVIEW_UDC_AVG_MKTSFA_12M, 0) 		as AVG_MKTSFA_12M
		   ,ifnull(STSC_DFUVIEW_UDC_AVG_STATSFA_12M, 0) 	as AVG_STATSFA_12M
		   ,ifnull(STSC_DFUVIEW_UDC_STATSFA, 0) 			as STATSFA
		   ,ifnull(STSC_DFUVIEW_UDC_MKTSFA, 0) 				as MKTSFA
		   ,ifnull(STSC_DFUVIEW_UDC_SYSTBIAS3M, 'Not Set') 	as SYSTBIAS3M
		   ,ifnull(STSC_DFUVIEW_UDC_SYSTBIAS6M, 'Not Set') 	as SYSTBIAS6M
		   ,ifnull(STSC_DFUVIEW_UDC_VOLATILITY, 0)		   	as VOLATILITY
		   ,ifnull(STSC_DFUVIEW_UDC_SEGMENTATION, 'Not Set') as SEGMENTATION
		   ,case when sd.stsc_dfuview_dmdunit is null and pi.product_name is not null 
					then 'Yes' 
			   	 else 'Not Set'
		 	end as plannig_item_flag
	from (
			select t1.nwmgr_planning_item_planning_item_id,
				   t1.nwmgr_planning_item_planning_item_enterprise_id enterprise_id,
				   t1.nwmgr_planning_item_dmdgroup as pi_dmdgroup,
				   t2.dim_jda_locationid,
				   t2.location_name,
				   t3.dim_jda_productid,
				   t3.product_name
			from NWMGR_PLANNING_ITEM t1, 
				 dim_jda_location t2,
				 dim_jda_product t3
			where     t1.nwmgr_planning_item_location_id = t2.location_id
				  and t1.nwmgr_planning_item_product_id  = t3.product_id
		 ) pi
			left join tmp_STSC_DFUVIEW_group sd on    ifnull(pi.product_name,'Not Set')  = ifnull(sd.stsc_dfuview_dmdunit,'Not Set')
												  and ifnull(pi.pi_dmdgroup,'Not Set')   = ifnull(sd.stsc_dfuview_dmdgroup,'Not Set')
												  and ifnull(pi.location_name,'Not Set') = ifnull(sd.stsc_dfuview_loc,'Not Set');
										 

/* Build tmp tables splitted by TYPE */
	/* LAG0 and LAG1 */
		/* Due to join on stsc_histfcst based on location_name and product_name, which in planning_item can have duplicate we create a tmp table with first ID */
		drop table if exists tmp_dw_planningitem_histfcst;
		create table tmp_dw_planningitem_histfcst as
		select pi.*
		from (
				select tpi.*,
					   row_number()over(partition by tpi.pi_dmdgroup, tpi.product_name, tpi.location_name order by planning_item_id) as RN
				from tmp_dw_planningitem tpi
			 ) pi
		where RN = 1;
		
		/* LAG0 */
		
		
         ALTER TABLE tmp_jda_dateholder
         ADD COLUMN lag0_addmonth timestamp ;

         update tmp_jda_dateholder dh
         set lag0_addmonth = CASE 
			  				     WHEN dh.currentdate > ( dh.last_saturday_month )
									 THEN ADD_MONTHS (date_trunc('MONTH', dh.currentdate), 0)
						 			 ELSE     ADD_MONTHS (date_trunc('MONTH', dh.currentdate),-1) 
						 		 END;
								 
		drop table if exists tmp_dw_histfcst_lag0;
		create table tmp_dw_histfcst_lag0 as
		select  sh.stsc_histfcst_basefcst + sh.stsc_histfcst_nonbasefcst as packs
			   ,sh.stsc_histfcst_startdate
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,-99999999 as dim_jda_componentid	/* 'FCST_LAG0' AS TYPE */
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
				,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from stsc_histfcst sh,
			 tmp_dw_planningitem_histfcst pi,
			 tmp_jda_dateholder dh
		where     sh.stsc_histfcst_dmdgroup = pi.pi_dmdgroup
			  and sh.stsc_histfcst_dmdunit  = pi.product_name
			  and sh.stsc_histfcst_loc      = pi.location_name
			  and sh.stsc_histfcst_fcstdate = dh.lag0_addmonth
				AND pi.fcstlevel IN ('111', '711', '141');	 
		
		/* LAG1 */
		 ALTER TABLE tmp_jda_dateholder
         ADD COLUMN lag1_addmonth timestamp ;

         update tmp_jda_dateholder dh
         set lag1_addmonth = case 
			  				    WHEN dh.currentdate > ( dh.last_saturday_month )
									THEN ADD_MONTHS (date_trunc('MONTH', dh.currentdate), -1)
						 			ELSE ADD_MONTHS (date_trunc('MONTH', dh.currentdate), -2) 
						 		END;
		drop table if exists tmp_dw_histfcst_lag1;
		create table tmp_dw_histfcst_lag1 as
		select  sh.stsc_histfcst_basefcst + sh.stsc_histfcst_nonbasefcst as packs
			   ,sh.stsc_histfcst_startdate
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,-99999997 as dim_jda_componentid 										/* 'FCST_LAG1' AS TYPE */
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag			   
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from stsc_histfcst sh,
			 tmp_dw_planningitem_histfcst pi,
			 tmp_jda_dateholder dh
		where     sh.stsc_histfcst_dmdgroup = pi.pi_dmdgroup
			  and sh.stsc_histfcst_dmdunit  = pi.product_name
			  and sh.stsc_histfcst_loc      = pi.location_name
			  and sh.stsc_histfcst_fcstdate = lag1_addmonth
				AND pi.fcstlevel IN ('111', '711', '141');

	/* MKTFCS, TENDER, ADJHIST, LRP, OPLAN, OPLAN_EURO, SPP, STATFCST, STAT231, SOPFRAM */
		/* main tmp_table as source for all TYPE from Version-Cell combination  */
		--drop table if exists dw_version_cell;
		--create table dw_version_cell as
		truncate table dw_version_cell; 
		insert into dw_version_cell(NWMGR_VERSION_VERSION_ID,NWMGR_VERSION_COMPONENT_ID,NWMGR_VERSION_ENTERPRISE_ID,NWMGR_VERSION_PLANNING_ITEM_ID,
			NWMGR_VERSION_VERSION_NUMBER,NWMGR_CELL_START_DATE,NWMGR_CELL_QUANTITY)
		select  nv.nwmgr_version_version_id
			   ,nv.nwmgr_version_component_id
			   ,nv.nwmgr_version_enterprise_id
			   ,nv.nwmgr_version_planning_item_id
			   ,nv.nwmgr_version_version_number
			   ,nc.nwmgr_cell_start_date
			   ,nc.nwmgr_cell_quantity
		from NWMGR_VERSION nv,
			 NWMGR_CELL nc
		where nv.nwmgr_version_version_id = nc.nwmgr_cell_version_id;

		/* MKTFCS */
		--drop table if exists dw_version_cell_mktfcst;
		--create table dw_version_cell_mktfcst as
		truncate table dw_version_cell_mktfcst;
		insert into dw_version_cell_mktfcst (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
			ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
			AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'MKTFCST' AS TYPE */
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
			 tmp_jda_dateholder dh
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
			  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1
			  and pi.fcstlevel IN ('111', '711', '141')	  
			  and nc.c_type = 'MKTFCST'													/* substr(nc.nwmgr_component_description, 1, 6) IN ('MKTFCS') */ 
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;
		
		/* TENDER */
		--drop table if exists dw_version_cell_tender;
		--create table dw_version_cell_tender as
		truncate table dw_version_cell_tender;
		insert into dw_version_cell_tender (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
			ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
			AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'TENDER' AS TYPE */
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
			 tmp_jda_dateholder dh
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
			  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1
			  and pi.fcstlevel IN ('111', '711', '141')	  
			  and nc.c_type = 'TENDER'													/* substr(nc.nwmgr_component_description, 1, 6) IN ('TENDER') */
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;	
		
		/* ADJHIST */
		--drop table if exists dw_version_cell_adjhist;
		--create table dw_version_cell_adjhist as
		truncate table dw_version_cell_adjhist;
		insert into dw_version_cell_adjhist (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
			ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
			AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'ADJHIST' AS TYPE */	   	   	   
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
			 tmp_jda_dateholder dh
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
			  and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR',  dh.currentdate),-24)
			  and vc.nwmgr_cell_start_date <= ADD_MONTHS (date_trunc('MONTH', dh.currentdate), -1)
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1
			  and pi.fcstlevel IN ('111', '711', '141')	  
			  and nc.c_type = 'ADJHIST'													/* nc.nwmgr_component_name = 'Adj Hist' */
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;	
		
		/* LRP no filter dates required */
		--drop table if exists dw_version_cell_lrp;
		--create table dw_version_cell_lrp as
		truncate table dw_version_cell_lrp;
		insert into dw_version_cell_lrp (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
			ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
			AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'LRP' AS TYPE */
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc 														/* NWMGR_COMPONENT nc */
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1	  
			  and nc.c_type = 'LRP'														/* nc.nwmgr_component_name = 'LRP Demand' */
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;
		
		/* OPLAN */
		--drop table if exists dw_version_cell_oplan;
		--create table dw_version_cell_oplan as
		truncate table dw_version_cell_oplan;
		insert into dw_version_cell_oplan (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
			ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
			AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'OPERATING PLAN' AS TYPE */
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
			 tmp_jda_dateholder dh
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
			  and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
			  and vc.nwmgr_cell_start_date <  ADD_MONTHS (date_trunc('YEAR', dh.currentdate),  36)	  
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1	  
			  and nc.c_type = 'OPERATING PLAN'														/* nc.nwmgr_component_description like 'OPQ%' */
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

		/* OPLAN_EURO */
		--drop table if exists dw_version_cell_oplaneuro;
		--create table dw_version_cell_oplaneuro as
		truncate table dw_version_cell_oplaneuro;
		insert into dw_version_cell_oplaneuro (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
			ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
			AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'OPERATING PLAN EURO' AS TYPE */	
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
			 tmp_jda_dateholder dh
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
			  and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
			  and vc.nwmgr_cell_start_date <  ADD_MONTHS (date_trunc('YEAR', dh.currentdate),  36)	  
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1	  
			  and nc.c_type = 'OPERATING PLAN EURO'										/* nc.nwmgr_component_description like 'OPVGC%' */
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

		/* SPP */
		--drop table if exists dw_version_cell_spp;
		--create table dw_version_cell_spp as
		truncate table dw_version_cell_spp;
		insert into dw_version_cell_spp (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
			ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
			AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'SPP' AS TYPE */
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
			 tmp_jda_dateholder dh
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
			  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1	  
			  and nc.c_type = 'SPP'														/* nc.nwmgr_component_description like 'SPPQ%' */
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

		/* STATFCST 
		drop table if exists dw_version_cell_statfcst
		create table dw_version_cell_statfcst as
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'STATFCST' AS TYPE 
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, 
			 tmp_jda_dateholder dh
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id 
			  and pi.fcstlevel IN ('111')
			  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1
			  and nc.c_type = 'STATFCST'												/* nc.nwmgr_component_name = 'Stat Forecast' 
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass 
				 */
		
		/* STAT231 */
		--drop table if exists dw_version_cell_stat231;
		--create table dw_version_cell_stat231 as
		truncate table dw_version_cell_stat231;
		insert into dw_version_cell_stat231 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
			ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
			AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
		select  sum(vc.nwmgr_cell_quantity) as packs
			   ,vc.nwmgr_cell_start_date
			   ,pi.planning_item_id
			   ,pi.dim_jda_locationid
			   ,pi.dim_jda_productid
			   ,nc.dim_jda_componentid													/* 'STAT231' AS TYPE */
			   ,pi.abc_class
			   ,pi.localbrand
			   ,pi.itemlocalcode
			   ,pi.model
			   ,pi.fcstlevel
			   ,pi.plannig_item_flag
			   ,pi.DFULIFECYCLE
			   ,pi.GAUSSITEMCODE
			   ,pi.AVGSALESPMONTH
			   ,pi.AVGFCST
			   ,pi.OP_EURO
			   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
		from dw_version_cell vc,
			 tmp_dw_planningitem pi, 
			 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
			 tmp_jda_dateholder dh
		where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
			  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
			  and pi.fcstlevel IN ('231')
			  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
			  and vc.nwmgr_version_version_number = 0
			  and vc.nwmgr_version_enterprise_id = 1
			  and nc.c_type = 'STAT231'													/* nc.nwmgr_component_description like 'STAT231%' */
		group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;		

	/* SOPFRAM */
	--drop table if exists dw_version_cell_sopfram;
	--create table dw_version_cell_sopfram as
	truncate table dw_version_cell_sopfram;
	insert into dw_version_cell_sopfram (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'SOP FRAMING' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'SOP FRAMING'												/* nc.nwmgr_component_description like 'SOP%' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

				 

/* F.O.C Hist / F.O.C. Fcst */
	--drop table if exists dw_version_cell_fochistfcst;
	--create table dw_version_cell_fochistfcst as
	truncate table dw_version_cell_fochistfcst;
	insert into dw_version_cell_fochistfcst (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'SOP FRAMING' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'F.O.C Hist / F.O.C. Fcst'												/* nc.nwmgr_component_description like 'SOP%' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

				 				 
/* Oana V - 11 July 2016 - JDA Demand for IBP */
/* F.O.C. Hist */
	--drop table if exists dw_version_cell_comp1103;
	--create table dw_version_cell_comp1103 as
	truncate table dw_version_cell_comp1103;
	insert into dw_version_cell_comp1103 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'F.O.C. Hist' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			,pi.AVG3M_BIAS1M
			,pi.AVG6M_BIAS1M
			,pi.AVG_MKTSFA_12M
			,pi.AVG_STATSFA_12M
			,pi.STATSFA
			,pi.MKTSFA
			,pi.SYSTBIAS3M
			,pi.SYSTBIAS6M
			,pi.VOLATILITY
			,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'F.O.C. Hist'												/* nc.nwmgr_component_name = 'F.O.C. Hist' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* Actual Hist */
	--drop table if exists dw_version_cell_comp1600;
	--create table dw_version_cell_comp1600 as
	truncate table dw_version_cell_comp1600;
	insert into dw_version_cell_comp1600(PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'Actual Hist' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			,pi.AVG3M_BIAS1M
			,pi.AVG6M_BIAS1M
			,pi.AVG_MKTSFA_12M
			,pi.AVG_STATSFA_12M
			,pi.STATSFA
			,pi.MKTSFA
			,pi.SYSTBIAS3M
			,pi.SYSTBIAS6M
			,pi.VOLATILITY
			,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'Actual Hist'												/* nc.nwmgr_component_name = 'Actual Hist' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* Demand Adj / Supply Risk */
	--drop table if exists dw_version_cell_comp6400;
	--create table dw_version_cell_comp6400 as
	truncate table dw_version_cell_comp6400;
	insert into dw_version_cell_comp6400 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'Actual Hist' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			,pi.AVG3M_BIAS1M
			,pi.AVG6M_BIAS1M
			,pi.AVG_MKTSFA_12M
			,pi.AVG_STATSFA_12M
			,pi.STATSFA
			,pi.MKTSFA
			,pi.SYSTBIAS3M
			,pi.SYSTBIAS6M
			,pi.VOLATILITY
			,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'Demand Adj/Supply Risk'												/* nc.nwmgr_component_name = 'Demand Adjustment / Supply Risk' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* Demand Adj/Business Risk & Opportunities */
	--drop table if exists dw_version_cell_comp6500;
	--create table dw_version_cell_comp6500 as
	truncate table dw_version_cell_comp6500;
	insert into dw_version_cell_comp6500 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'Demand Adj/Business Risk & Opportunities' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'Demand Adj/Business Risk & Opportunities'				/* nc.nwmgr_component_name = 'Demand Adjustment / Business Risk & Opportunities' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* Tender Demand Adj/Supply Risk */
	--drop table if exists dw_version_cell_comp6501 ;
	--create table dw_version_cell_comp6501 as
	truncate table dw_version_cell_comp6501;
	insert into dw_version_cell_comp6501(PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'Tender Demand Adj/Supply Risk' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'Tender Demand Adj/Supply Risk'				/* nc.nwmgr_component_name = 'Tender Demand Adjustment / Supply Risk' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* Tender Demand Adj/Business Risk & Opportunities */
	--drop table if exists dw_version_cell_comp6502;
	--create table dw_version_cell_comp6502 as
	truncate table dw_version_cell_comp6502;
	insert into dw_version_cell_comp6502(PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'Tender Demand Adj/Business Risk & Opportunities' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'Tender Demand Adj/Business Risk & Opportunities'				/* nc.nwmgr_component_name = 'Tender Demand Adjustment / Business Risk & Opportunities' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* Consensus Forecast LC */
	--drop table if exists dw_version_cell_comp6801;
	--create table dw_version_cell_comp6801 as
	truncate table dw_version_cell_comp6801;
	insert into dw_version_cell_comp6801(PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'Consensus Forecast LC' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)   
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'Consensus Forecast LC'				/* nc.nwmgr_component_name = 'Consensus Forecast LC' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

drop table if exists tmp_insertnewrows;
create table tmp_insertnewrows as
		select distinct cast( 0 as decimal(36,9)) as PACKS
				,datevalue as NWMGR_CELL_START_DATE
				,PLANNING_ITEM_ID
					 	,DIM_JDA_LOCATIONID
					 	,DIM_JDA_PRODUCTID
					 	,DIM_JDA_COMPONENTID
					 	,ABC_CLASS
					 	,LOCALBRAND
					 	,ITEMLOCALCODE
					 	,MODEL
					 	,FCSTLEVEL
					 	,PLANNIG_ITEM_FLAG
					 	,DFULIFECYCLE
					 	,GAUSSITEMCODE
					 	,cast( 0 as decimal(29,9)) as AVGSALESPMONTH
					 	,cast( 0 as decimal(29,9)) as AVGFCST
					 	,cast( 0 as decimal(15,2)) as OP_EURO
					 	,DFUCLASS
					 	,cast( 0 as decimal(18,4)) as AVG3M_BIAS1M
					 	,cast( 0 as decimal(18,4)) as AVG6M_BIAS1M
					 	,cast( 0 as decimal(18,4)) as AVG_MKTSFA_12M
					 	,cast( 0 as decimal(18,4)) as AVG_STATSFA_12M
					 	,cast( 0 as decimal(18,4)) as STATSFA
					 	,cast( 0 as decimal(18,4)) as MKTSFA
					 	,cast( 0 as VARCHAR(7)) as SYSTBIAS3M
					 	,cast( 0 as VARCHAR(7)) as SYSTBIAS6M
					 	,cast( 0 as decimal(18,4)) as VOLATILITY
					 	,SEGMENTATION
			from
				(select PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,LOCALBRAND
					 		,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
					 		AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION
				from dw_version_cell_comp6801
				where year(nwmgr_cell_start_date) between year(current_date-INTERVAL '1' YEAR) and year(current_date)) t1
				full outer join (select datevalue, dim_dateid from dim_date where dayofmonth = 1 and companycode = 'Not Set' and year(datevalue)  between year(current_date-INTERVAL '1' YEAR) and year(current_date)) d1
					 	on year(t1.nwmgr_cell_start_date) = year(datevalue);

merge into dw_version_cell_comp6801 t1
	using tmp_insertnewrows t2
		on t1.NWMGR_CELL_START_DATE = t2.NWMGR_CELL_START_DATE
			and t1.PLANNING_ITEM_ID = t2.PLANNING_ITEM_ID
			and t1.DIM_JDA_LOCATIONID = t2.DIM_JDA_LOCATIONID
			and t1.DIM_JDA_PRODUCTID = t2.DIM_JDA_PRODUCTID
			and t1.DIM_JDA_COMPONENTID = t2.DIM_JDA_COMPONENTID
		when not matched then insert values (t2.PACKS,t2.NWMGR_CELL_START_DATE,t2.PLANNING_ITEM_ID,t2.DIM_JDA_LOCATIONID,
							t2.DIM_JDA_PRODUCTID,t2.DIM_JDA_COMPONENTID,t2.ABC_CLASS,t2.LOCALBRAND,t2.ITEMLOCALCODE,t2.MODEL,t2.FCSTLEVEL,
							t2.PLANNIG_ITEM_FLAG,t2.DFULIFECYCLE,t2.GAUSSITEMCODE,t2.AVGSALESPMONTH,t2.AVGFCST,t2.OP_EURO,t2.DFUCLASS,t2.AVG3M_BIAS1M,
							t2.AVG6M_BIAS1M,t2.AVG_MKTSFA_12M,t2.AVG_STATSFA_12M,t2.STATSFA,t2.MKTSFA,t2.SYSTBIAS3M,t2.SYSTBIAS6M,t2.VOLATILITY,
							t2.SEGMENTATION);


/* Consensus Forecast_V QTY */
	--drop table if exists dw_version_cell_comp9700;
	--create table dw_version_cell_comp9700 as
	truncate table dw_version_cell_comp9700;
	insert into dw_version_cell_comp9700 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,ABC_CLASS,
		LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,AVG3M_BIAS1M,
		AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'Consensus Forecast_V QTY' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag	   	   	   
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and nc.c_type = 'Consensus Forecast_V QTY'				/* nc.nwmgr_component_name = 'Consensus Forecast (in Qty)_V' */
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
				 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
				 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
				 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;
/* END Oana V - 11 July 2016 - JDA Demand for IBP */
/* APP-6201 EMD - Changes in FusionOps to comply with SAP IBP - Oana 22 May 2017 */
/* 7900 Late Sales Recognition (in LC) */
truncate table dw_version_cell_comp7900;
insert into dw_version_cell_comp7900 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'Late Sales Recognition (in LC)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'Late Sales Recognition (in LC)'														/* nc.nwmgr_component_description like 'Late Sales Recognition (in LC)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
			 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* 7901 Commission Income (in LC) */
truncate table dw_version_cell_comp7901;
insert into dw_version_cell_comp7901 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'Commission Income (in LC)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
			,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
			,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'Commission Income (in LC)'														/* nc.nwmgr_component_description like 'Commission Income (in LC)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
			 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* 7902 Business Risk Late Sales Reco (in LC) */
truncate table dw_version_cell_comp7902;
insert into dw_version_cell_comp7902 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'Business Risk Late Sales Reco (in LC)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
			,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
			,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'Business Risk Late Sales Reco (in LC)'														/* nc.nwmgr_component_description like 'Business Risk Late Sales Reco (in LC)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* 8100 Supply Risk Global from IBP (in LC) */
truncate table dw_version_cell_comp8100;
insert into dw_version_cell_comp8100 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'Supply Risk Global from IBP (in LC)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
			,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
			,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'Supply Risk Global from IBP (in LC)'														/* nc.nwmgr_component_description like 'Business Risk Late Sales Reco (in LC)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

/* 8101 - Supply Risk Global from IBP (in Qty) */
truncate table dw_version_cell_comp8101;
insert into dw_version_cell_comp8101 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'Supply Risk Global from IBP (in Qty)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
			,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
			,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'Supply Risk Global from IBP (in Qty)'														/* nc.nwmgr_component_description like 'Supply Risk Global from IBP (in Qty)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;
/* END APP-6201 EMD - Changes in FusionOps to comply with SAP IBP - Oana 22 May 2017 */
/* APP-6477 New Components for F1, F2, F3 and OP - Oana 9 June 2017  */
truncate table dw_version_cell_comp7500;
insert into dw_version_cell_comp7500 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'F1 Consensus Forecast Frozen (in Qty)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'F1 Consensus Forecast Frozen (in Qty)'														/* nc.nwmgr_component_description like 'Late Sales Recognition (in LC)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

truncate table dw_version_cell_comp7502;
insert into dw_version_cell_comp7502 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'F1 Consensus Forecast Frozen (in LC)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'F1 Consensus Forecast Frozen (in LC)'														/* nc.nwmgr_component_description like 'F1 Consensus Forecast Frozen (in LC)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

truncate table dw_version_cell_comp8200;
insert into dw_version_cell_comp8200 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'F2 Consensus Forecast Frozen (in LC)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'F2 Consensus Forecast Frozen (in LC)'														/* nc.nwmgr_component_description like 'F2 Consensus Forecast Frozen (in LC)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

truncate table dw_version_cell_comp8201;
insert into dw_version_cell_comp8201 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'F3 Consensus Forecast Frozen (in LC)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'F3 Consensus Forecast Frozen (in LC)'														/* nc.nwmgr_component_description like 'F3 Consensus Forecast Frozen (in LC)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

truncate table dw_version_cell_comp8202;
insert into dw_version_cell_comp8202 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'F2 Consensus Forecast Frozen (in Qty)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'F2 Consensus Forecast Frozen (in Qty)'														/* nc.nwmgr_component_description like 'F2 Consensus Forecast Frozen (in Qty)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

truncate table dw_version_cell_comp8203;
insert into dw_version_cell_comp8203 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'F3 Consensus Forecast Frozen (in Qty)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'F3 Consensus Forecast Frozen (in Qty)'														/* nc.nwmgr_component_description like 'F3 Consensus Forecast Frozen (in Qty)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;


truncate table dw_version_cell_comp8204;
insert into dw_version_cell_comp8204 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'OP Consensus Forecast Frozen (in Qty)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'OP Consensus Forecast Frozen (in Qty)'														/* nc.nwmgr_component_description like 'OP Consensus Forecast Frozen (in Qty)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;

truncate table dw_version_cell_comp8205;
insert into dw_version_cell_comp8205 (PACKS,NWMGR_CELL_START_DATE,PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,
	ABC_CLASS,LOCALBRAND,ITEMLOCALCODE,MODEL,FCSTLEVEL,PLANNIG_ITEM_FLAG,DFULIFECYCLE,GAUSSITEMCODE,AVGSALESPMONTH,AVGFCST,OP_EURO,DFUCLASS,
	AVG3M_BIAS1M,AVG6M_BIAS1M,AVG_MKTSFA_12M,AVG_STATSFA_12M,STATSFA,MKTSFA,SYSTBIAS3M,SYSTBIAS6M,VOLATILITY,SEGMENTATION)
select sum(vc.nwmgr_cell_quantity) as packs
		 ,vc.nwmgr_cell_start_date
		 ,pi.planning_item_id
		 ,pi.dim_jda_locationid
		 ,pi.dim_jda_productid
		 ,nc.dim_jda_componentid													/* 'OP Consensus Forecast Frozen (in Qty)' AS TYPE */
		 ,pi.abc_class
		 ,pi.localbrand
		 ,pi.itemlocalcode
		 ,pi.model
		 ,pi.fcstlevel
		 ,pi.plannig_item_flag
		 ,pi.DFULIFECYCLE
		 ,pi.GAUSSITEMCODE
		 ,pi.AVGSALESPMONTH
		 ,pi.AVGFCST
		 ,pi.OP_EURO
		 ,pi.dfuclass
		,pi.AVG3M_BIAS1M
		,pi.AVG6M_BIAS1M
		,pi.AVG_MKTSFA_12M
		,pi.AVG_STATSFA_12M
		,pi.STATSFA
		,pi.MKTSFA
		,pi.SYSTBIAS3M
		,pi.SYSTBIAS6M
		,pi.VOLATILITY
		,pi.SEGMENTATION
from dw_version_cell vc,
	 tmp_dw_planningitem pi,
	 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
	 tmp_jda_dateholder dh
where   vc.nwmgr_version_planning_item_id = pi.planning_item_id
		and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		and vc.nwmgr_cell_start_date >= ADD_MONTHS (date_trunc('YEAR', dh.currentdate), -24)
		and vc.nwmgr_version_version_number = 0
		and vc.nwmgr_version_enterprise_id = 1
		and nc.c_type = 'OP Consensus Forecast Frozen (in LC)'														/* nc.nwmgr_component_description like 'OP Consensus Forecast Frozen (in Qty)' */
group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
		 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
		 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
		 ,pi.dfuclass
		 ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;


/* END APP-6477 New Components for F1, F2, F3 and OP - Oana 9 June 2017 */



	/* Changing logic to ALL data */												/* Actual Hist / Market Forecast + Tender Forecast */
	drop table if exists dw_version_cell_acthist_mkfcst_tdfcst;
	create table dw_version_cell_acthist_mkfcst_tdfcst as
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,nc.dim_jda_componentid													/* 'STATFCST_12M' AS TYPE */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag
		   ,pi.DFULIFECYCLE
		   ,pi.GAUSSITEMCODE
		   ,pi.AVGSALESPMONTH
		   ,pi.AVGFCST
		   ,pi.OP_EURO
		   ,pi.dfuclass
			    ,pi.AVG3M_BIAS1M
				,pi.AVG6M_BIAS1M
			   	,pi.AVG_MKTSFA_12M
				,pi.AVG_STATSFA_12M
				,pi.STATSFA
				,pi.MKTSFA
				,pi.SYSTBIAS3M
				,pi.SYSTBIAS6M
				,pi.VOLATILITY
				,pi.SEGMENTATION
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 dim_jda_component nc, 														/* NWMGR_COMPONENT nc, */
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.component_id					/* vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id */
		  /* and pi.fcstlevel IN ('111')
		  and vc.nwmgr_cell_start_date >= date_trunc('MONTH', dh.currentdate) 
		  and vc.nwmgr_cell_start_date <=  ADD_MONTHS (date_trunc('MONTH', dh.currentdate),  11) */	  
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1	  
		  and (   (nc.c_type in ('STATFCST', 'OPERATING PLAN LC', 
		                         'F1 Euro', 'F2 Euro', 'F3 Euro', 
								 'F1 LC', 'F2 LC', 'F3 LC', 
								 'F1 QTY', 'F2 QTY', 'F3 QTY', 
								 'Actual Values LC', 'Actual Values Euro',
								 'Price LC', 'Price Tender LC', 'CurrRate'))
		       /* OR (nc.component_name = 'Tender Forecast') */
			   )
	group by vc.nwmgr_cell_start_date, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, nc.dim_jda_componentid
			 ,pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag
			 ,pi.DFULIFECYCLE, pi.GAUSSITEMCODE, pi.AVGSALESPMONTH, pi.AVGFCST, pi.OP_EURO
			 ,pi.dfuclass
			     ,pi.AVG3M_BIAS1M, pi.AVG6M_BIAS1M, pi.AVG_MKTSFA_12M, pi.AVG_STATSFA_12M, pi.STATSFA, pi.MKTSFA, pi.SYSTBIAS3M, pi.SYSTBIAS6M, pi.VOLATILITY, pi.SEGMENTATION;
				 
/* UNION ALL tmp tables with their specific header */
	drop table if exists tmp_prefact_demand;
	create table tmp_prefact_demand(dim_dateidstartdate bigint, dd_planning_item_id integer, dim_jda_locationid bigint, dim_jda_productid bigint, dd_type varchar(50), dd_abc_class varchar(50),
									dd_localbrand varchar(100), dd_itemlocalcode varchar(50), dd_model varchar(18), dd_fcstlevel varchar(50), dd_plannig_item_flag varchar(7),
									ct_packs decimal(30,9), ct_units decimal(30,9), ct_eq_unit decimal(30,9), ct_mcg decimal(30,9), ct_iu decimal(30,9),
									DFULIFECYCLE varchar(7), GAUSSITEMCODE varchar(50), AVGSALESPMONTH decimal(29,9), AVGFCST decimal(29,9), OP_EURO decimal(15,2),
									dfuclass varchar(7),
									dim_jda_componentid bigint,
									ct_price decimal(30,9), ct_currRate decimal(30,9)
									,AVG3M_BIAS1M decimal(18,4), AVG6M_BIAS1M decimal(18,4), AVG_MKTSFA_12M decimal(18,4), AVG_STATSFA_12M decimal(18,4), STATSFA decimal(18,4), MKTSFA decimal(18,4), SYSTBIAS3M varchar(7), 
									SYSTBIAS6M varchar(7), VOLATILITY decimal(18,4), SEGMENTATION varchar(100),startdate timestamp);									
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dd.dim_dateid from dim_date dd where dd.datevalue = lag0.stsc_histfcst_startdate and dd.companycode = 'Not Set'),1) dim_dateidstartdate
		    1 as dim_dateidstartdate
		   ,lag0.planning_item_id
		   ,lag0.dim_jda_locationid
		   ,lag0.dim_jda_productid
		   ,'FCST_LAG0' as dd_type
		   ,lag0.abc_class, lag0.localbrand, lag0.itemlocalcode, lag0.model, lag0.fcstlevel, lag0.plannig_item_flag /* coming from planning_item */
		   ,lag0.packs 
		   ,lag0.packs * pr.packsize 				  AS units 
		   ,lag0.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,lag0.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,lag0.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,lag0.DFULIFECYCLE, lag0.GAUSSITEMCODE, lag0.AVGSALESPMONTH, lag0.AVGFCST, lag0.OP_EURO, lag0.dfuclass
		   ,lag0.dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,lag0.stsc_histfcst_startdate as startdate
	from tmp_dw_histfcst_lag0 lag0,
		 dim_jda_product pr
	where lag0.dim_jda_productid = pr.dim_jda_productid;
	
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dd.dim_dateid from dim_date dd where dd.datevalue = lag1.stsc_histfcst_startdate and dd.companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,lag1.planning_item_id
		   ,lag1.dim_jda_locationid
		   ,lag1.dim_jda_productid
		   ,'FCST_LAG1' as dd_type
		   ,lag1.abc_class, lag1.localbrand, lag1.itemlocalcode, lag1.model, lag1.fcstlevel, lag1.plannig_item_flag /* coming from planning_item */
		   ,lag1.packs 
		   ,lag1.packs * pr.packsize 				  AS units 
		   ,lag1.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,lag1.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,lag1.packs * pr.packsize * pr.dosetoiu    AS iu	 
		   ,lag1.DFULIFECYCLE, lag1.GAUSSITEMCODE, lag1.AVGSALESPMONTH, lag1.AVGFCST, lag1.OP_EURO, lag1.dfuclass
		   ,lag1.dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,stsc_histfcst_startdate  as startdate
	from tmp_dw_histfcst_lag1 lag1,
		 dim_jda_product pr
	where lag1.dim_jda_productid = pr.dim_jda_productid;
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = mf.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,mf.planning_item_id
		   ,mf.dim_jda_locationid
		   ,mf.dim_jda_productid
		   ,'MKTFCST' as dd_type
		   ,mf.abc_class, mf.localbrand, mf.itemlocalcode, mf.model, mf.fcstlevel, mf.plannig_item_flag /* coming from planning_item */
		   ,mf.packs 
		   ,mf.packs * pr.packsize 				  	AS units 
		   ,mf.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,mf.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,mf.packs * pr.packsize * pr.dosetoiu    AS iu	
		   ,mf.DFULIFECYCLE, mf.GAUSSITEMCODE, mf.AVGSALESPMONTH, mf.AVGFCST, mf.OP_EURO, mf.dfuclass
		   ,mf.dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,mf.nwmgr_cell_start_date as startdate
	from dw_version_cell_mktfcst mf,
		 dim_jda_product pr
	where mf.dim_jda_productid = pr.dim_jda_productid;
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = t.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,t.planning_item_id
		   ,t.dim_jda_locationid
		   ,t.dim_jda_productid
		   ,'TENDER' as dd_type
		   ,t.abc_class, t.localbrand, t.itemlocalcode, t.model, t.fcstlevel, t.plannig_item_flag /* coming from planning_item */
		   ,t.packs 
		   ,t.packs * pr.packsize 				   AS units 
		   ,t.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,t.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,t.packs * pr.packsize * pr.dosetoiu    AS iu	  
		   ,t.DFULIFECYCLE, t.GAUSSITEMCODE, t.AVGSALESPMONTH, t.AVGFCST, t.OP_EURO, t.dfuclass
		   ,t.dim_jda_componentid,
								 AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,t.nwmgr_cell_start_date as startdate
	from dw_version_cell_tender t,
		 dim_jda_product pr
	where t.dim_jda_productid = pr.dim_jda_productid;
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'ADJHIST' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_adjhist ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = lrp.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,lrp.planning_item_id
		   ,lrp.dim_jda_locationid
		   ,lrp.dim_jda_productid
		   ,'LRP' as dd_type
		   ,lrp.abc_class, lrp.localbrand, lrp.itemlocalcode, lrp.model, lrp.fcstlevel, lrp.plannig_item_flag /* coming from planning_item */
		   ,ROUND(DECODE(pr.brandtosize * pr.packsize, 0, 0, lrp.packs / (pr.brandtosize * pr.packsize) ),0) AS packs  /* 1.49 > 1 also ? 1.5 > 2 standard  */
		   ,DECODE(pr.brandtosize, 0, 0, lrp.packs / pr.brandtosize) 								AS units									
		   ,lrp.packs 																				AS eq_unit									
		   ,DECODE(pr.brandtosize, 0, 0, (lrp.packs * pr.dosetomcg) / pr.brandtosize ) 				AS mcg
		   ,DECODE(pr.brandtosize, 0, 0, (lrp.packs * pr.dosetoiu) / pr.brandtosize )  				AS iu
		   ,lrp.DFULIFECYCLE, lrp.GAUSSITEMCODE, lrp.AVGSALESPMONTH, lrp.AVGFCST, lrp.OP_EURO, lrp.dfuclass
		   ,lrp.dim_jda_componentid,
								   AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, lrp.nwmgr_cell_start_date as startdate
	from dw_version_cell_lrp lrp,
		 dim_jda_product pr
	where lrp.dim_jda_productid = pr.dim_jda_productid;
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = op.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,op.planning_item_id
		   ,op.dim_jda_locationid
		   ,op.dim_jda_productid
		   ,'OPERATING PLAN' as dd_type
		   ,op.abc_class, op.localbrand, op.itemlocalcode, op.model, op.fcstlevel, op.plannig_item_flag /* coming from planning_item */
		   ,op.packs 								AS packs
		   ,op.packs * pr.packsize 				    AS units 
		   ,convert(decimal (30,9),(op.packs * pr.packsize)) * pr.brandtosize AS eq_unit 
		   ,convert(decimal (30,9),(op.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		   ,convert(decimal (30,9),(op.packs * pr.packsize ))* pr.dosetoiu    AS iu		   
		   ,op.DFULIFECYCLE, op.GAUSSITEMCODE, op.AVGSALESPMONTH, op.AVGFCST, op.OP_EURO, op.dfuclass
		   ,op.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, op.nwmgr_cell_start_date as startdate
	from dw_version_cell_oplan op,
		 dim_jda_product pr
	where op.dim_jda_productid = pr.dim_jda_productid;
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ope.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ope.planning_item_id
		   ,ope.dim_jda_locationid
		   ,ope.dim_jda_productid
		   ,'OPERATING PLAN EURO' as dd_type
		   ,ope.abc_class, ope.localbrand, ope.itemlocalcode, ope.model, ope.fcstlevel, ope.plannig_item_flag /* coming from planning_item */
		   ,ope.packs 								 AS packs
		   ,ope.packs * pr.packsize 				 AS units 
		    ,convert(decimal (30,9),(ope.packs * pr.packsize)) * pr.brandtosize AS eq_unit 
		    ,convert(decimal (30,9),(ope.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		    ,convert(decimal (30,9),(ope.packs * pr.packsize)) * pr.dosetoiu    AS iu  
		   ,ope.DFULIFECYCLE, ope.GAUSSITEMCODE, ope.AVGSALESPMONTH, ope.AVGFCST, ope.OP_EURO, ope.dfuclass
		   ,ope.dim_jda_componentid,
								   AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ope.nwmgr_cell_start_date as startdate
	from dw_version_cell_oplaneuro ope,
		 dim_jda_product pr
	where ope.dim_jda_productid = pr.dim_jda_productid;
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = spp.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,spp.planning_item_id
		   ,spp.dim_jda_locationid
		   ,spp.dim_jda_productid
		   ,'SPP' as dd_type
		   ,spp.abc_class, spp.localbrand, spp.itemlocalcode, spp.model, spp.fcstlevel, spp.plannig_item_flag /* coming from planning_item */
		   ,ROUND(DECODE(pr.brandtosize * pr.packsize, 0, 0, spp.packs / (pr.brandtosize * pr.packsize) ),0) AS packs
		   ,DECODE(pr.brandtosize, 0, 0, spp.packs / pr.brandtosize) 								AS units
		   ,spp.packs 																				AS eq_unit
		   ,DECODE(pr.brandtosize, 0, 0, (spp.packs * pr.dosetomcg) / pr.brandtosize ) 				AS mcg
		   ,DECODE(pr.brandtosize, 0, 0, (spp.packs * pr.dosetoiu) / pr.brandtosize )  				AS iu
		   ,spp.DFULIFECYCLE, spp.GAUSSITEMCODE, spp.AVGSALESPMONTH, spp.AVGFCST, spp.OP_EURO, spp.dfuclass
		   ,spp.dim_jda_componentid,
								   AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,spp.nwmgr_cell_start_date as startdate
	from dw_version_cell_spp spp,
		 dim_jda_product pr
	where spp.dim_jda_productid = pr.dim_jda_productid;

	/* it was declared in all fields
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid)
	select  ifnull((select dim_dateid from dim_date where datevalue = stf.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   ,stf.planning_item_id
		   ,stf.dim_jda_locationid
		   ,stf.dim_jda_productid
		   ,'STATFCST' as dd_type
		   ,stf.abc_class, stf.localbrand, stf.itemlocalcode, stf.model, stf.fcstlevel, stf.plannig_item_flag /* coming from planning_item 
		   ,stf.packs 								 AS packs
		   ,stf.packs * pr.packsize 				 AS units 
		   ,stf.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,stf.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,stf.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,stf.DFULIFECYCLE, stf.GAUSSITEMCODE, stf.AVGSALESPMONTH, stf.AVGFCST, stf.OP_EURO, stf.dfuclass
		   ,stf.dim_jda_componentid
	from dw_version_cell_statfcst stf,
		 dim_jda_product pr
	where stf.dim_jda_productid = pr.dim_jda_productid 
	*/
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = st2.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,st2.planning_item_id
		   ,st2.dim_jda_locationid
		   ,st2.dim_jda_productid
		   ,'STAT231' as dd_type
		   ,st2.abc_class, st2.localbrand, st2.itemlocalcode, st2.model, st2.fcstlevel, st2.plannig_item_flag /* coming from planning_item */
		   ,ROUND(DECODE(pr.brandtosize * pr.packsize, 0, 0, st2.packs / (pr.brandtosize * pr.packsize) ),0) AS packs
		   ,DECODE(pr.brandtosize, 0, 0, st2.packs / pr.brandtosize) 								AS units
		   ,st2.packs 																				AS eq_unit
		   ,DECODE(pr.brandtosize, 0, 0, (st2.packs * pr.dosetomcg) / pr.brandtosize ) 				AS mcg
		   ,DECODE(pr.brandtosize, 0, 0, (st2.packs * pr.dosetoiu) / pr.brandtosize )  				AS iu	   
		   ,st2.DFULIFECYCLE, st2.GAUSSITEMCODE, st2.AVGSALESPMONTH, st2.AVGFCST, st2.OP_EURO, st2.dfuclass
		   ,st2.dim_jda_componentid,
								   AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,st2.nwmgr_cell_start_date as startdate
	from dw_version_cell_stat231 st2,
		 dim_jda_product pr
	where st2.dim_jda_productid = pr.dim_jda_productid;
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = sof.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,sof.planning_item_id
		   ,sof.dim_jda_locationid
		   ,sof.dim_jda_productid
		   ,'SOP FRAMING' as dd_type
		   ,sof.abc_class, sof.localbrand, sof.itemlocalcode, sof.model, sof.fcstlevel, sof.plannig_item_flag /* coming from planning_item */
		   ,ROUND(DECODE(pr.brandtosize * pr.packsize, 0, 0, sof.packs / (pr.brandtosize * pr.packsize) ),0) AS packs
		   ,DECODE(pr.brandtosize, 0, 0, sof.packs / pr.brandtosize) 								AS units
		   ,sof.packs 																				AS eq_unit
		   ,DECODE(pr.brandtosize, 0, 0, (sof.packs * pr.dosetomcg) / pr.brandtosize ) 				AS mcg
		   ,DECODE(pr.brandtosize, 0, 0, (sof.packs * pr.dosetoiu) / pr.brandtosize )  				AS iu		   
		   ,sof.DFULIFECYCLE, sof.GAUSSITEMCODE, sof.AVGSALESPMONTH, sof.AVGFCST, sof.OP_EURO, sof.dfuclass
		   ,sof.dim_jda_componentid,
								   AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,sof.nwmgr_cell_start_date  as startdate
	from dw_version_cell_sopfram sof,
		 dim_jda_product pr
	where sof.dim_jda_productid = pr.dim_jda_productid;
	
	
	-------------------------
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = foc.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,foc.planning_item_id
		   ,foc.dim_jda_locationid
		   ,foc.dim_jda_productid
		   ,'F.O.C Hist / F.O.C. Fcst' as dd_type
		   ,foc.abc_class, foc.localbrand, foc.itemlocalcode, foc.model, foc.fcstlevel, foc.plannig_item_flag /* coming from planning_item */
		   ,foc.packs 
		   ,foc.packs * pr.packsize 				  	AS units 
		   ,foc.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,foc.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,foc.packs * pr.packsize * pr.dosetoiu    AS iu	
		   ,foc.DFULIFECYCLE, foc.GAUSSITEMCODE, foc.AVGSALESPMONTH, foc.AVGFCST, foc.OP_EURO, foc.dfuclass
		   ,foc.dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,foc.nwmgr_cell_start_date as startdate
	from dw_version_cell_fochistfcst foc,
		 dim_jda_product pr
	where foc.dim_jda_productid = pr.dim_jda_productid;
	
/* Oana V - 11 July 2016 - JDA Demand for IBP */	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'F.O.C. Hist' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp1103 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Actual Hist' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp1600 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;	
	

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Demand Adj/Supply Risk' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp6400 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Demand Adj/Business Risk & Opportunities' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp6500 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Tender Demand Adj/Supply Risk' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp6501 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Tender Demand Adj/Business Risk & Opportunities' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp6502 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Consensus Forecast LC' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize			  		AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize  AS eq_unit 
		   ,cast(ah.packs * pr.packsize as decimal(30,9)) * pr.dosetomcg    AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp6801 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Consensus Forecast_V QTY' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units 
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit 
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu		   
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp9700 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
/* END Oana V - 11 July 2016 - JDA Demand for IBP */
/* APP-6201 Changes in FusionOps to comply with SAP IBP - Oana 22 May 2017 */
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Late Sales Recognition (in LC)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp7900 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Commission Income (in LC)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp7901 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Business Risk Late Sales Reco (in LC)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp7902 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;


insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Supply Risk Global from IBP (in LC)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp8100 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'Supply Risk Global from IBP (in Qty)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp8101 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
/* END - APP-6201 Changes in FusionOps to comply with SAP IBP - Oana 22 May 2017 */
/*  APP-6477 New Components for F1, F2, F3 and OP - Oana 9 June 2017  */
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'F1 Consensus Forecast Frozen (in Qty)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
		   ,ah.packs * pr.packsize 				    AS units
		   ,ah.packs * pr.packsize * pr.brandtosize AS eq_unit
		   ,ah.packs * pr.packsize * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp7500 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'F1 Consensus Forecast Frozen (in LC)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
			 ,convert(decimal (30,9),(ah.packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp7502 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'F2 Consensus Forecast Frozen (in LC)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
			 ,convert(decimal (30,9),(ah.packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp8200 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'F3 Consensus Forecast Frozen (in LC)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
			 ,convert(decimal (30,9),(ah.packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp8201 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'F2 Consensus Forecast Frozen (in Qty)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
			 ,convert(decimal (30,9),(ah.packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp8202 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'F3 Consensus Forecast Frozen (in Qty)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
			 ,convert(decimal (30,9),(ah.packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp8203 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'OP Consensus Forecast Frozen (in Qty)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
			 ,convert(decimal (30,9),(ah.packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp8204 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,ah.planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,'OP Consensus Forecast Frozen (in LC)' as dd_type
		   ,ah.abc_class, ah.localbrand, ah.itemlocalcode, ah.model, ah.fcstlevel, ah.plannig_item_flag  /* coming from planning_item */
		   ,ah.packs 								AS packs
			 ,convert(decimal (30,9),(ah.packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,convert(decimal (30,9),(ah.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		   ,ah.packs * pr.packsize * pr.dosetoiu    AS iu
		   ,ah.DFULIFECYCLE, ah.GAUSSITEMCODE, ah.AVGSALESPMONTH, ah.AVGFCST, ah.OP_EURO, ah.dfuclass
		   ,ah.dim_jda_componentid,
								  AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,ah.nwmgr_cell_start_date as startdate
	from dw_version_cell_comp8205 ah,
		 dim_jda_product pr
	where ah.dim_jda_productid = pr.dim_jda_productid;
/* END APP-6477 New Components for F1, F2, F3 and OP - Oana 9 June 2017  */
	----------------------------------
	
	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = amtf.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
	        1 as dim_dateidstartdate
		   ,amtf.planning_item_id dd_planning_item_id
		   ,amtf.dim_jda_locationid dim_jda_locationid
		   ,amtf.dim_jda_productid dim_jda_productid
		   ,'Not Set' as dd_type
		   ,amtf.abc_class dd_abc_class,  
		   amtf.localbrand dd_localbrand, 
		   amtf.itemlocalcode dd_itemlocalcode, 
		   amtf.model dd_model, 
		   amtf.fcstlevel dd_fcstlevel, 
		   amtf.plannig_item_flag dd_plannig_item_flag/* coming from planning_item */
		   ,amtf.packs 								 AS ct_packs
		   ,amtf.packs * pr.packsize 				 AS units 
		    ,convert(decimal (30,9),(amtf.packs * pr.packsize)) * pr.brandtosize AS eq_unit 
		   ,convert(decimal (30,9) ,(amtf.packs * pr.packsize)) * pr.dosetomcg   AS mcg
		    ,convert(decimal (30,9),(amtf.packs * pr.packsize)) * pr.dosetoiu    AS iu
		   ,amtf.DFULIFECYCLE, amtf.GAUSSITEMCODE, amtf.AVGSALESPMONTH, amtf.AVGFCST, amtf.OP_EURO, amtf.dfuclass
		   ,amtf.dim_jda_componentid,
								    AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION,amtf.nwmgr_cell_start_date as startdate
	from dw_version_cell_acthist_mkfcst_tdfcst amtf,
		 dim_jda_product pr
	where amtf.dim_jda_productid = pr.dim_jda_productid;
	
	update tmp_prefact_demand tmp
	set tmp.dim_dateidstartdate = d.dim_dateid
	from tmp_prefact_demand tmp, dim_date d 
	where d.datevalue = tmp.startdate
	and companycode = 'Not Set';
	
/* Adding Prices to MKTFCST and Tender Forecast */
	drop table if exists tmp_jda_mapping_price_01;
	create table tmp_jda_mapping_price_01 as
	select t1.dd_planning_item_id, t1.dim_jda_locationid, t1.dim_jda_productid, t1.dim_dateidstartdate, t1.ct_packs,
		   t2.c_type
	from tmp_prefact_demand t1
			inner join dim_jda_component t2 on t1.dim_jda_componentid = t2.dim_jda_componentid
	where t2.c_type3 in ('Price LC');

	drop table if exists tmp_jda_mapping_price_02;
	create table tmp_jda_mapping_price_02 as
	select t1.dd_planning_item_id, t1.dim_jda_locationid, t1.dim_jda_productid, t1.dim_dateidstartdate, t1.ct_packs,
		   t2.c_type
	from tmp_prefact_demand t1
			inner join dim_jda_component t2 on t1.dim_jda_componentid = t2.dim_jda_componentid
	where t2.c_type3 in ('Price Tender LC');

	drop table if exists tmp_jda_mapping_price_03;
	create table tmp_jda_mapping_price_03 as
	select t1.dd_planning_item_id, t1.dim_jda_locationid, t1.dim_jda_productid, t1.dim_dateidstartdate, t1.ct_packs,
		   t2.c_type
	from tmp_prefact_demand t1
			inner join dim_jda_component t2 on t1.dim_jda_componentid = t2.dim_jda_componentid
	where t2.c_type3 in ('CurrRate');
	
	/* Price for MKTFCST */
	update tmp_prefact_demand dst
	set dst.ct_price = src.ct_packs
	from tmp_prefact_demand dst,
	     tmp_jda_mapping_price_01 src,
		 dim_jda_component dco
	where     dst.dd_planning_item_id = src.dd_planning_item_id
		  and dst.dim_jda_locationid = src.dim_jda_locationid
		  and dst.dim_jda_productid = src.dim_jda_productid
		  and dst.dim_dateidstartdate = src.dim_dateidstartdate
		  and dst.dim_jda_componentid = dco.dim_jda_componentid
		  and dco.c_type in ('MKTFCST','F.O.C Hist / F.O.C. Fcst','F.O.C. Hist','Actual Hist','Demand Adj/Supply Risk','Demand Adj/Business Risk & Opportunities',
'Consensus Forecast LC','Consensus Forecast_V QTY');

	/* Price for Tender */
	update tmp_prefact_demand dst
	set dst.ct_price = src.ct_packs
	from tmp_prefact_demand dst,
	     tmp_jda_mapping_price_02 src,
		 dim_jda_component dco
	where     dst.dd_planning_item_id = src.dd_planning_item_id
		  and dst.dim_jda_locationid = src.dim_jda_locationid
		  and dst.dim_jda_productid = src.dim_jda_productid
		  and dst.dim_dateidstartdate = src.dim_dateidstartdate
		  and dst.dim_jda_componentid = dco.dim_jda_componentid
		  and dco.component_name in ('Tender Forecast','Tender Demand Adjustment / Supply Risk','Tender Demand Adjustment / Business Risk & Opportunities');
	
	/* Currency Rate for MKTFCST and Tender */
	update tmp_prefact_demand dst
	set dst.ct_currRate = src.ct_packs
	from tmp_prefact_demand dst,
	     tmp_jda_mapping_price_03 src,
		 dim_jda_component dco
	where     dst.dd_planning_item_id = src.dd_planning_item_id
		  and dst.dim_jda_locationid = src.dim_jda_locationid
		  and dst.dim_jda_productid = src.dim_jda_productid
		  and dst.dim_dateidstartdate = src.dim_dateidstartdate
		  and dst.dim_jda_componentid = dco.dim_jda_componentid
		  and (dco.c_type = 'MKTFCST' OR dco.component_name = 'Tender Forecast' OR dco.c_type = 'F.O.C Hist / F.O.C. Fcst'
				OR dco.component_name = 'Tender Demand Adjustment / Supply Risk' OR dco.component_name = 'Tender Demand Adjustment / Business Risk & Opportunities'
				OR dco.c_type = 'F.O.C. Hist' OR dco.c_type = 'Actual Hist' OR dco.c_type = 'Demand Adj/Supply Risk' OR dco.c_type = 'Demand Adj/Business Risk & Opportunities'
				OR dco.c_type = 'Consensus Forecast LC' OR dco.c_type = 'Consensus Forecast_V QTY' OR dco.component_name ='Late Sales Recognition (in LC)'
				OR dco.component_name ='Commission Income (in LC)' OR dco.component_name ='Business Risk Late Sales Reco (in LC)' OR dco.component_name ='Supply Risk Global from IBP (in LC)');

/* JDA Demand for IBP - Calculated Measures - Oana */
/* 6702 Best Estimate Standard in LC */
--drop table if exists dw_version_cell_comp6702 ;
--create table dw_version_cell_comp6702 as 
truncate table dw_version_cell_comp6702;
insert into dw_version_cell_comp6702 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Best Estimate Standard Business LC' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'MKTFCST' THEN ifnull((f.ct_packs * f.ct_price),0)
				WHEN f.dd_type = 'Demand Adj/Business Risk & Opportunities' THEN ifnull((f.ct_packs * f.ct_price),0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull((- f.ct_packs * f.ct_price),0)
				WHEN djc.component_name ='Actual Values (in LC)' THEN ifnull(f.ct_packs,0) --'Actual Values LC'
				WHEN f.dd_type ='Late Sales Recognition (in LC)' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type ='Commission Income (in LC)' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type ='Business Risk Late Sales Reco (in LC)' THEN ifnull(f.ct_packs,0)
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6702) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_jda_component djc
where f.dim_jda_componentid = djc.dim_jda_componentid
and (f.dd_type in ('MKTFCST','Demand Adj/Business Risk & Opportunities','F.O.C Hist / F.O.C. Fcst','Late Sales Recognition (in LC)','Commission Income (in LC)','Business Risk Late Sales Reco (in LC)') OR djc.component_name ='Actual Values (in LC)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6702 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* 6701 Best Estimate Tender in LC */
--drop table if exists dw_version_cell_comp6701 
--create table dw_version_cell_comp6701 as
truncate table dw_version_cell_comp6701;
insert into dw_version_cell_comp6701 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
			'Best Estimate Tender LC' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand,
			max(f.dd_itemlocalcode) as dd_itemlocalcode,
			max(f.dd_model) as dd_model,
			max(f.dd_fcstlevel) as dd_fcstlevel,
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull((ct_packs * ct_price),0)) as ct_packs,
			0 as ct_units,
			0 as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE,
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 6701) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			max(SYSTBIAS3M) as dd_SYSTBIAS3M,
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f
where dd_type in ('TENDER','Tender Demand Adj/Business Risk & Opportunities')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;	

	insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6701 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* 6703 Best Estimate in LC */	
--drop table if exists dw_version_cell_comp6703 ;
--create table dw_version_cell_comp6703 as 
truncate table dw_version_cell_comp6703;
insert into dw_version_cell_comp6703 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select ifnull(dw1.dim_dateidstartdate,dw2.dim_dateidstartdate) as dim_dateidstartdate, 
			ifnull(dw1.dd_planning_item_id,dw2.dd_planning_item_id) as dd_planning_item_id, 
			ifnull(dw1.dim_jda_locationid,dw2.dim_jda_locationid) as dim_jda_locationid, 
			ifnull(dw1.dim_jda_productid,dw2.dim_jda_productid) as dim_jda_productid, 
			'Best Estimate LC' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0) + ifnull(dw2.ct_packs,0)) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 6703) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from dw_version_cell_comp6702 dw1
	full outer join dw_version_cell_comp6701 dw2
	on 		dw1.dim_dateidstartdate = dw2.dim_dateidstartdate
		and dw1.dd_planning_item_id = dw2.dd_planning_item_id 
		and dw1.dim_jda_locationid 	= dw2.dim_jda_locationid
		and dw1.dim_jda_productid	= dw2.dim_jda_productid;
		
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6703 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
		
/* 6700 Best Estimate in Qty */
--drop table if exists dw_version_cell_comp6700 ;
--create table dw_version_cell_comp6700 as 
truncate table dw_version_cell_comp6700;
insert into dw_version_cell_comp6700 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Best Estimate QTY' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull(ct_packs,0)) as ct_packs,
			sum(ifnull(ct_units,0)) as ct_units,	
			sum(ifnull(ct_eq_unit,0)) as ct_eq_unit, 
			sum(ifnull(ct_mcg,0)) as ct_mcg,	
			sum(ifnull(ct_iu,0)) as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6700) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f
where dd_type IN ('Tender Demand Adj/Business Risk & Opportunities','Demand Adj/Business Risk & Opportunities','TENDER','MKTFCST','Actual Hist')  -- 29 Jul 2016 Oana: added 'Actual Hist' as 'MKTFCST' was set o bgring only data for future
group by dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6700 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* 6705 Best Estimate in Euro */	
--drop table if exists dw_version_cell_comp6705 ;
--create table dw_version_cell_comp6705 as 
truncate table dw_version_cell_comp6705;
insert into dw_version_cell_comp6705 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Best Estimate EURO' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'MKTFCST' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'Demand Adj/Business Risk & Opportunities' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull((- f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'TENDER' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'Tender Demand Adj/Business Risk & Opportunities' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN djc.component_name ='Actual Values (in LC)' THEN ifnull((f.ct_packs * ct_currRate),0) + ifnull((f.ct_packs * EXCHRATE),0) --'Actual Values LC'
				WHEN f.dd_type ='Late Sales Recognition (in LC)' THEN ifnull(f.ct_packs * ct_currRate,0)
				WHEN f.dd_type ='Commission Income (in LC)' THEN ifnull(f.ct_packs * ct_currRate,0)
				WHEN f.dd_type ='Business Risk Late Sales Reco (in LC)' THEN ifnull(f.ct_packs * ct_currRate,0)
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6705) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component djc, dim_jda_location dl
where f.dim_jda_componentid = djc.dim_jda_componentid
and f.dim_jda_locationid 	= dl.dim_jda_locationid
and (f.dd_type in ('MKTFCST','Demand Adj/Business Risk & Opportunities','F.O.C Hist / F.O.C. Fcst','TENDER',
	'Tender Demand Adj/Business Risk & Opportunities','Late Sales Recognition (in LC)','Commission Income (in LC)','Business Risk Late Sales Reco (in LC)')
	OR djc.component_name ='Actual Values (in LC)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6705 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6803 Consensus Forecast in Euro */
--drop table if exists dw_version_cell_comp6803 ;
--create table dw_version_cell_comp6803 as 
truncate table dw_version_cell_comp6803;
insert into dw_version_cell_comp6803 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Consensus Forecast EURO' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE
				WHEN dd_type = 'Consensus Forecast LC' and date_trunc('month',dt.datevalue) >= date_trunc('month',current_date) THEN ifnull(ct_packs * ct_currRate,0)
				WHEN djc.component_name ='Actual Values (in LC)' and date_trunc('month',dt.datevalue) < date_trunc('month',current_date) then ifnull(ct_packs * EXCHRATE,0)
			END) as ct_packs,
			0 as ct_units,
			0 as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 6803) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,  
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_date dt, dim_jda_location dl, dim_jda_component djc
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_componentid = djc.dim_jda_componentid
	and (dd_type = 'Consensus Forecast LC' OR djc.component_name ='Actual Values (in LC)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6803 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 6901 Consensus Forecast in Qty */	
--drop table if exists dw_version_cell_comp6901;
--create table dw_version_cell_comp6901 as 
truncate table dw_version_cell_comp6901;
insert into dw_version_cell_comp6901(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Consensus Forecast QTY' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull(ct_packs,0)) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6901) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f
where f.dd_type IN ('MKTFCST','TENDER','Consensus Forecast_V QTY','Actual Hist')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

drop table if exists tmp_insertnewrows;
create table tmp_insertnewrows as
select distinct d1.dim_dateid as DIM_DATEIDSTARTDATE
	,DD_PLANNING_ITEM_ID
	,DIM_JDA_LOCATIONID
	,DIM_JDA_PRODUCTID
	,DD_TYPE
	,DD_ABC_CLASS
	,DD_LOCALBRAND
	,DD_ITEMLOCALCODE
	,DD_MODEL
	,DD_FCSTLEVEL
	,DD_PLANNIG_ITEM_FLAG
	,0 as CT_PACKS
	,0 as CT_UNITS
	,0 as CT_EQ_UNIT
	,0 as CT_MCG
	,0 as CT_IU
	,DD_DFULIFECYCLE
	,DD_GAUSSITEMCODE
	,0  as CT_AVGSALESPMONTH
	,0  as CT_AVGFCST
	,0  as CT_OP_EURO
	,DD_DFUCLASS
	,DIM_JDA_COMPONENTID
	,0 as CT_PRICE
	,0 as CT_CURRRATE
	,0 as CT_AVG3M_BIAS1M
	,0 as CT_AVG6M_BIAS1M
	,0 as CT_AVG_MKTSFA_12M
	,0 as CT_AVG_STATSFA_12M
	,0 as CT_STATSFA
	,0 as CT_MKTSFA
	,DD_SYSTBIAS3M
	,DD_SYSTBIAS6M
	,0 as CT_VOLATILITY
	,DD_SEGMENTATION
from (select dt.datevalue ,dt.DIM_DATEID as DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
	DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,
	DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,
	CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,
	DD_SEGMENTATION
from dw_version_cell_comp6901 d
	inner join dim_date dt on d.DIM_DATEIDSTARTDATE = dt.dim_dateid
where year(datevalue) between year(current_date-INTERVAL '1' YEAR) and year(current_date)) t1
full outer join (select datevalue, dim_dateid from dim_date where dayofmonth = 1 and companycode = 'Not Set' and year(datevalue)  between year(current_date-INTERVAL '1' YEAR) and year(current_date)) d1
	on year(t1.datevalue) = year(d1.datevalue);

merge into dw_version_cell_comp6901 t1
	using tmp_insertnewrows t2
		on t1.DIM_DATEIDSTARTDATE = t2.DIM_DATEIDSTARTDATE
		and t1.DD_PLANNING_ITEM_ID = t2.DD_PLANNING_ITEM_ID
		and t1.DIM_JDA_LOCATIONID = t2.DIM_JDA_LOCATIONID
		and t1.DIM_JDA_PRODUCTID = t2.DIM_JDA_PRODUCTID
		and t1.DIM_JDA_COMPONENTID = t2.DIM_JDA_COMPONENTID
	when not matched then insert values (t2.DIM_DATEIDSTARTDATE,t2.DD_PLANNING_ITEM_ID,t2.DIM_JDA_LOCATIONID,t2.DIM_JDA_PRODUCTID,t2.DD_TYPE,
		t2.DD_ABC_CLASS,t2.DD_LOCALBRAND,t2.DD_ITEMLOCALCODE,t2.DD_MODEL,t2.DD_FCSTLEVEL,t2.DD_PLANNIG_ITEM_FLAG,t2.CT_PACKS,t2.CT_UNITS,
		t2.CT_EQ_UNIT,t2.CT_MCG,t2.CT_IU,t2.DD_DFULIFECYCLE,t2.DD_GAUSSITEMCODE,t2.CT_AVGSALESPMONTH,t2.CT_AVGFCST,t2.CT_OP_EURO,
		t2.DD_DFUCLASS,t2.DIM_JDA_COMPONENTID,t2.CT_PRICE,t2.CT_CURRRATE,t2.CT_AVG3M_BIAS1M,t2.CT_AVG6M_BIAS1M,t2.CT_AVG_MKTSFA_12M,
		t2.CT_AVG_STATSFA_12M,t2.CT_STATSFA,t2.CT_MKTSFA,t2.DD_SYSTBIAS3M,t2.DD_SYSTBIAS6M,t2.CT_VOLATILITY,t2.DD_SEGMENTATION);

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6901 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 6503 Constrained Actual / Mkt Fcst Value (in LC) wo Tender */
--drop table if exists dw_version_cell_comp6503 ;
--create table dw_version_cell_comp6503 as 
truncate table dw_version_cell_comp6503;
insert into dw_version_cell_comp6503 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
	'Constrained Actual/Mkt Fcst Value wo Tender LC' as dd_type, 
	max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'MKTFCST' THEN ifnull((f.ct_packs * f.ct_price),0)	
				WHEN f.dd_type = 'Demand Adj/Supply Risk' THEN ifnull((f.ct_packs * f.ct_price),0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull((- f.ct_packs * f.ct_price),0)
				WHEN f.dd_type = 'F.O.C. Hist' THEN ifnull((f.ct_packs * f.ct_price),0)
				WHEN djc.component_name ='Actual Values (in LC)' THEN ifnull(f.ct_packs,0) --'Actual Values LC'
				WHEN f.dd_type ='Late Sales Recognition (in LC)' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type ='Commission Income (in LC)' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type ='Supply Risk Global from IBP (in LC)' THEN ifnull(f.ct_packs,0)
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6503) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_jda_component djc
where f.dim_jda_componentid = djc.dim_jda_componentid
and (f.dd_type in ('MKTFCST','Demand Adj/Supply Risk','F.O.C Hist / F.O.C. Fcst','Late Sales Recognition (in LC)','Commission Income (in LC)','Supply Risk Global from IBP (in LC)') OR djc.component_name ='Actual Values (in LC)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6503 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6504	Constrained Tender or Launch Value (in LC) */	
--drop table if exists dw_version_cell_comp6504 ;
--create table dw_version_cell_comp6504 as 
truncate table dw_version_cell_comp6504;
insert into dw_version_cell_comp6504 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Constrained Tender or Launch Value LC' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull((ct_packs * ct_price),0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 6504) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type in ('Tender Demand Adj/Supply Risk','TENDER');

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6504 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6508 Constrained Actual / Mkt & Tender Fcst Value (in LC)*/
--drop table if exists dw_version_cell_comp6508 ;
--create table dw_version_cell_comp6508 as 
truncate table dw_version_cell_comp6508;
insert into dw_version_cell_comp6508 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Constrained Actual/Mkt & Tender Fcst Value LC' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'MKTFCST' THEN ifnull((f.ct_packs * f.ct_price),0)	
				WHEN f.dd_type = 'Demand Adj/Supply Risk' THEN ifnull((f.ct_packs * f.ct_price),0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull((- f.ct_packs * f.ct_price),0)
				WHEN djc.component_name ='Actual Values (in LC)' THEN ifnull(f.ct_packs,0) --'Actual Values LC'
				WHEN f.dd_type ='Late Sales Recognition (in LC)' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type ='Commission Income (in LC)' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type ='Supply Risk Global from IBP (in LC)' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type = 'Tender Demand Adj/Supply Risk' THEN ifnull((f.ct_packs * f.ct_price),0)
				WHEN f.dd_type = 'TENDER' THEN ifnull((f.ct_packs * f.ct_price),0)
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6508) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component djc
where f.dim_jda_componentid = djc.dim_jda_componentid
and (f.dd_type in ('MKTFCST','Demand Adj/Supply Risk','F.O.C Hist / F.O.C. Fcst','Tender Demand Adj/Supply Risk','TENDER','Late Sales Recognition (in LC)','Commission Income (in LC)','Supply Risk Global from IBP (in LC)') OR djc.component_name ='Actual Values (in LC)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6508 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6511 Constrained Tender or Launch Value (in Euro) */
--drop table if exists dw_version_cell_comp6511 ;
--create table dw_version_cell_comp6511 as 
truncate table dw_version_cell_comp6511;
insert into dw_version_cell_comp6511 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Constrained Tender or Launch Value EURO' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull((cast(cast(ct_packs as decimal(36,9))*cast(ct_price as decimal(36,9)) as decimal(36,9))*cast(ct_currRate as decimal(36,9))),0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 6511) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type in ('Tender Demand Adj/Supply Risk','TENDER');

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6511 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6509 Constrained Market Fcst Val (in Euro) wo Tender */
--drop table if exists dw_version_cell_comp6509;
--create table dw_version_cell_comp6509 as 
truncate table dw_version_cell_comp6509;
insert into dw_version_cell_comp6509 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Constrained Market Fcst Val wo Tender EURO' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'MKTFCST' THEN ifnull(f.ct_packs * f.ct_price * ct_currRate,0)
				--WHEN f.dd_type = 'Actual Hist' THEN (- f.ct_packs * f.ct_price * ct_currRate)
				WHEN f.dd_type = 'Demand Adj/Supply Risk' THEN ifnull(f.ct_packs * f.ct_price * ct_currRate,0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull(- f.ct_packs * f.ct_price * ct_currRate,0)
				--WHEN f.dd_type = 'F.O.C. Hist' THEN (f.ct_packs * f.ct_price * ct_currRate)
				WHEN djc.component_name ='Actual Values (in LC)' THEN (ifnull(f.ct_packs * ct_currRate,0) - ifnull(f.ct_packs * EXCHRATE,0)) --'Actual Values LC'
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6509) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_jda_component djc, dim_jda_location dl
where 	f.dim_jda_componentid = djc.dim_jda_componentid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and (f.dd_type  IN ('MKTFCST','Demand Adj/Supply Risk','F.O.C Hist / F.O.C. Fcst') OR djc.component_name ='Actual Values (in LC)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6509 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6510 Constrained Actual / Mkt Fcst Val (in Euro) wo Tender */
--drop table if exists dw_version_cell_comp6510;
--create table dw_version_cell_comp6510 as 
truncate table dw_version_cell_comp6510;
insert into dw_version_cell_comp6510 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Constrained Actual/Mkt Fcst Val wo Tender  EURO' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'MKTFCST' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'Demand Adj/Supply Risk' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull((- f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN djc.component_name ='Actual Values (in LC)' THEN ifnull((f.ct_packs * ct_currRate),0)
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6510) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_jda_component djc
where f.dim_jda_componentid = djc.dim_jda_componentid
and (f.dd_type  IN ('MKTFCST','Demand Adj/Supply Risk','F.O.C Hist / F.O.C. Fcst') OR djc.component_name ='Actual Values (in LC)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6510 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6512 Constrained Actual / Mkt & Tender Fcst Values (in Euro) */	
--drop table if exists dw_version_cell_comp6512;
--create table dw_version_cell_comp6512 as 
truncate table dw_version_cell_comp6512;
insert into dw_version_cell_comp6512 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Constrained Actual/Mkt & Tender Fcst Value EURO' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'MKTFCST' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'Demand Adj/Supply Risk' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull((- f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN djc.component_name ='Actual Values (in LC)' THEN ifnull((f.ct_packs * exchrate),0)
				WHEN f.dd_type = 'Late Sales Recognition (in LC)' THEN ifnull(f.ct_packs * ct_currRate,0)
				WHEN f.dd_type = 'Commission Income (in LC)' THEN ifnull(f.ct_packs * ct_currRate,0)
				WHEN f.dd_type = 'Supply Risk Global from IBP (in LC)' THEN ifnull(f.ct_packs * ct_currRate,0)
				WHEN f.dd_type = 'Tender Demand Adj/Supply Risk' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
				WHEN f.dd_type = 'TENDER' THEN ifnull((f.ct_packs * f.ct_price * ct_currRate),0)
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6512) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component djc, dim_jda_location dl
where f.dim_jda_componentid = djc.dim_jda_componentid
and f.dim_jda_locationid 	= dl.dim_jda_locationid
and (f.dd_type  IN ('MKTFCST','Demand Adj/Supply Risk','F.O.C Hist / F.O.C. Fcst','Tender Demand Adj/Supply Risk','TENDER',
	'Late Sales Recognition (in LC)','Commission Income (in LC)','Supply Risk Global from IBP (in LC)') OR djc.component_name ='Actual Values (in LC)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6512 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6402 Constrained Actual Hist / Mkt Fcst /Tender (in Qty) */
--drop table if exists dw_version_cell_comp6402 ;
--create table dw_version_cell_comp6402 as 
truncate table dw_version_cell_comp6402;
insert into dw_version_cell_comp6402 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid,
			'Constrained Actual Hist/Mkt Fcst/Tender QTY' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand,
			max(f.dd_itemlocalcode) as dd_itemlocalcode,
			max(f.dd_model) as dd_model,
			max(f.dd_fcstlevel) as dd_fcstlevel,
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull(ct_packs,0)) as ct_packs,
			0 as ct_units,
			0 as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE,
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 6402) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			max(SYSTBIAS3M) as dd_SYSTBIAS3M,
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f
where f.dd_type in ('MKTFCST','Actual Hist','TENDER','Demand Adj/Supply Risk','Tender Demand Adj/Supply Risk','Supply Risk Global from IBP (in Qty)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6402 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6513	Constrained Adj Hist / Mkt Fcst / Tender (in Qty) */
--drop table if exists dw_version_cell_comp6513 ;
--create table dw_version_cell_comp6513 as 
truncate table dw_version_cell_comp6513;
insert into dw_version_cell_comp6513 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Constrained Adj Hist/Mkt Fcst/Tender QTY' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull(f.ct_packs,0)) as ct_packs,
			sum(ifnull(f.ct_units,0)) as ct_units,	
			sum(ifnull(f.ct_eq_unit,0)) as ct_eq_unit, 
			sum(ifnull(f.ct_mcg,0)) as ct_mcg,	
			sum(ifnull(f.ct_iu,0)) as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6513) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_jda_component djc
where f.dim_jda_componentid = djc.dim_jda_componentid
and f.dd_type  in ('MKTFCST','TENDER','Demand Adj/Supply Risk','Tender Demand Adj/Supply Risk','ADJHIST') 
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6513 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6403 Demand Adjustment Value / Supply Risk (in LC) */
--drop table if exists dw_version_cell_comp6403 ;
--create table dw_version_cell_comp6403 as 
truncate table dw_version_cell_comp6403;
insert into dw_version_cell_comp6403 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Value/Supply Risk LC' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs * ct_price,0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 6403) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type = 'Demand Adj/Supply Risk' ;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6403 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6506 Demand Adjustment Value Tender / Supply Risk (in LC) */
--drop table if exists dw_version_cell_comp6506 ;
--create table dw_version_cell_comp6506 as 
truncate table dw_version_cell_comp6506;
insert into dw_version_cell_comp6506 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Value Tender/Supply Risk LC' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs * ct_price,0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 6506) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type = 'Tender Demand Adj/Supply Risk';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6506 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8001 Demand Adjustment Supply Risk Total in LC */
--drop table if exists dw_version_cell_comp8001 ;
--create table dw_version_cell_comp8001 as 
truncate table dw_version_cell_comp8001;
insert into dw_version_cell_comp8001 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Supply Risk Total LC' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs * ct_price,0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 8001) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type in ('Demand Adj/Supply Risk','Tender Demand Adj/Supply Risk');

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8001 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8002 Demand Adjustment Value / Supply Risk (in Euro) */
--drop table if exists dw_version_cell_comp8002 ;
--create table dw_version_cell_comp8002 as 
truncate table dw_version_cell_comp8002;
insert into dw_version_cell_comp8002 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Value/Supply Risk EURO' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs * ct_price * ct_currRate,0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 8002) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type = 'Demand Adj/Supply Risk';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8002 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8003 Demand Adjustment Value Tender / Supply Risk (in Euro) */
--drop table if exists dw_version_cell_comp8003 ;
--create table dw_version_cell_comp8003 as 
truncate table dw_version_cell_comp8003;
insert into dw_version_cell_comp8003 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Value Tender/Supply Risk EURO' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull((ct_packs * ct_price * ct_currRate),0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 8003) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type = 'Tender Demand Adj/Supply Risk';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8003 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8004 Demand Adjustment Supply Risk Total in Euro */
--drop table if exists dw_version_cell_comp8004 ;
--create table dw_version_cell_comp8004 as 
truncate table dw_version_cell_comp8004;
insert into dw_version_cell_comp8004 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Supply Risk Total EURO' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs * ct_price * ct_currRate,0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 8004) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type in ('Demand Adj/Supply Risk','Tender Demand Adj/Supply Risk');

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8004 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6505	Demand Adjustment Value / Business Risk & Opportunities (in LC) */
--drop table if exists dw_version_cell_comp6505 ;
--create table dw_version_cell_comp6505 as 
truncate table dw_version_cell_comp6505;
insert into dw_version_cell_comp6505 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Value/Business Risk & Opp LC' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull((ct_packs * ct_price),0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 6505) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type = 'Demand Adj/Business Risk & Opportunities';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6505 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
			
/* 6507	Demand Adjustment Value Tender / Business Risk & Opportunities (in LC) */
--drop table if exists dw_version_cell_comp6507 ;
--create table dw_version_cell_comp6507 as 
truncate table dw_version_cell_comp6507;
insert into dw_version_cell_comp6507 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Value Tender/Business Risk & Opp LC' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull((ct_packs * ct_price),0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 6507) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type = 'Tender Demand Adj/Business Risk & Opportunities';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6507 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8005 Demand Adjustment Value / Business Risk & Opportunities (in Euro) */
--drop table if exists dw_version_cell_comp8005 ;
--create table dw_version_cell_comp8005 as 
truncate table dw_version_cell_comp8005;
insert into dw_version_cell_comp8005 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Value/Business Risk & Opp EURO' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull((ct_packs * ct_price * ct_currRate),0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 8005) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type = 'Demand Adj/Business Risk & Opportunities';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8005 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8006 Demand Adjustment Value Tender / Business Risk & Opportunities (in Euro) */
--drop table if exists dw_version_cell_comp8006 ;
--create table dw_version_cell_comp8006 as 
truncate table dw_version_cell_comp8006;
insert into dw_version_cell_comp8006 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Adj Value Tender/Business Risk & Opp EURO' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs * ct_price * ct_currRate,0) as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 8006) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION
from tmp_prefact_demand
where dd_type = 'Tender Demand Adj/Business Risk & Opportunities';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 								AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8006 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 1203 Actual / Mkt & Tender Fcst Value (in LC) */
--drop table if exists dw_version_cell_comp1203 ;
--create table dw_version_cell_comp1203 as 
truncate table dw_version_cell_comp1203;
insert into dw_version_cell_comp1203 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Actual/Mkt & Tender Fcst Value LC' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN djc.component_name ='Actual Values (in LC)' THEN ifnull(f.ct_packs,0) --'Actual Values LC'
				WHEN f.dd_type = 'MKTFCST' THEN ifnull((f.ct_packs * f.ct_price),0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull((- f.ct_packs * f.ct_price),0)
				WHEN djc.component_name = 'Tender Forecast' THEN ifnull((f.ct_packs * f.ct_price),0)
				WHEN djc.c_type ='Late Sales Recognition (in LC)' THEN ifnull(f.ct_packs,0)
				WHEN djc.c_type ='Commission Income (in LC)' THEN ifnull(f.ct_packs,0)
			END) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 1203) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component djc
where f.dim_jda_componentid = djc.dim_jda_componentid
and (f.dd_type in ('MKTFCST','F.O.C Hist / F.O.C. Fcst','Late Sales Recognition (in LC)','Commission Income (in LC)') OR djc.component_name in ('Actual Values (in LC)','Tender Forecast'))
and f.dd_fcstlevel IN ('111', '711')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 																AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 																	AS mcg
		   ,ah.ct_iu							    									AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp1203 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
	and ah.dim_jda_productid = pr.dim_jda_productid;

/* 1204 Actual / Mkt & Tender Fcst Value (in Euro) */
--drop table if exists dw_version_cell_comp1204 ;
--create table dw_version_cell_comp1204 as 
truncate table dw_version_cell_comp1204;
insert into dw_version_cell_comp1204 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Actual/Mkt & Tender Fcst Value EURO' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN djc.component_name ='Actual Values (in LC)' THEN ifnull((f.ct_packs * EXCHRATE),0) --'Actual Values LC'
				WHEN f.dd_type = 'MKTFCST' THEN ifnull((f.ct_packs * f.ct_price * ct_currrate),0)
				WHEN f.dd_type = 'F.O.C Hist / F.O.C. Fcst' THEN ifnull((- f.ct_packs * f.ct_price * ct_currrate),0)
				WHEN djc.component_name = 'Tender Forecast' THEN ifnull((f.ct_packs * f.ct_price * ct_currrate),0)
				WHEN djc.c_type ='Late Sales Recognition (in LC)' THEN ifnull(f.ct_packs * ct_currrate,0)
				WHEN djc.c_type ='Commission Income (in LC)' THEN ifnull(f.ct_packs * ct_currrate,0)
			END) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 1204) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid 
from tmp_prefact_demand f, dim_jda_component djc, dim_jda_location dl
where f.dim_jda_componentid = djc.dim_jda_componentid
and f.dim_jda_locationid 	= dl.dim_jda_locationid
and (f.dd_type in ('MKTFCST','F.O.C Hist / F.O.C. Fcst','Late Sales Recognition (in LC)','Commission Income (in LC)') OR djc.component_name in ('Actual Values (in LC)','Tender Forecast'))
and f.dd_fcstlevel IN ('111', '711')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp1204 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8010 Demand Risks & Business Opportunities ( in LC ) */
--drop table if exists dw_version_cell_comp8010 ;
--create table dw_version_cell_comp8010 as 
truncate table dw_version_cell_comp8010;
insert into dw_version_cell_comp8010 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Risks & Business Opportunities LC' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Best Estimate LC' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type = 'Actual/Mkt & Tender Fcst Value LC' THEN ifnull((- f.ct_packs),0)
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8010) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f
where f.dd_type in ('Best Estimate LC','Actual/Mkt & Tender Fcst Value LC')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8010 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8011 Demand Risks & Business Opportunities ( in Euro ) */
--drop table if exists dw_version_cell_comp8011 ;
--create table dw_version_cell_comp8011 as 
truncate table dw_version_cell_comp8011;
insert into dw_version_cell_comp8011 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Risks & Business Opportunities Euro' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Best Estimate EURO' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type = 'Actual/Mkt & Tender Fcst Value EURO' THEN ifnull((- f.ct_packs ),0)
			END) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8011) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component djc
where f.dim_jda_componentid = djc.dim_jda_componentid
and f.dd_type in ('Actual/Mkt & Tender Fcst Value EURO','Best Estimate EURO')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8011 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8012 Supply Risk (in LC)*/
--drop table if exists dw_version_cell_comp8012 ;
--create table dw_version_cell_comp8012 as 
truncate table dw_version_cell_comp8012;
insert into dw_version_cell_comp8012 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Supply Risk LC' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Constrained Actual/Mkt & Tender Fcst Value LC' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type = 'Actual/Mkt & Tender Fcst Value LC' THEN ifnull((- f.ct_packs),0)
			END) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8012) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f
where f.dd_type in ('Constrained Actual/Mkt & Tender Fcst Value LC','Actual/Mkt & Tender Fcst Value LC')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8012 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8013 Supply Risk (in Euro) */
--drop table if exists dw_version_cell_comp8013;
--create table dw_version_cell_comp8013 as 
truncate table dw_version_cell_comp8013;
insert into dw_version_cell_comp8013 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Supply Risk EURO' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Constrained Actual/Mkt & Tender Fcst Value EURO' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type ='Actual/Mkt & Tender Fcst Value EURO' THEN ifnull((- f.ct_packs),0)
			END) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8013) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f
where f.dd_type  IN ('Constrained Actual/Mkt & Tender Fcst Value EURO','Actual/Mkt & Tender Fcst Value EURO') 
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8013 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8014 Actual / Mkt & Tender Forecast [in LC] vs. OP */	
--drop table if exists dw_version_cell_comp8014;
--create table dw_version_cell_comp8014 as 
truncate table dw_version_cell_comp8014;
insert into dw_version_cell_comp8014 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Actual/Mkt & Tender Forecast vs. OP LC' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Actual/Mkt & Tender Fcst Value LC' THEN ifnull(f.ct_packs,0)
				WHEN dc.c_type3 = 'OP LC' THEN ifnull((- f.ct_packs),0)	
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8014) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component dc
where f.dim_jda_componentid = dc.dim_jda_componentid
	and (f.dd_type in ('Actual/Mkt & Tender Fcst Value LC') OR dc.c_type3 = 'OP LC')
	and dd_fcstlevel IN ('111', '711')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8014 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* 8015 Actual / Mkt & Tender Forecast [in Euro] vs. OP */	
--drop table if exists dw_version_cell_comp8015;
--create table dw_version_cell_comp8015 as 
truncate table dw_version_cell_comp8015;
insert into dw_version_cell_comp8015 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Actual/Mkt & Tender Forecast vs. OP EURO' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Actual/Mkt & Tender Fcst Value EURO' THEN ifnull(f.ct_packs,0)
				WHEN dc.c_type3 = 'OP EURO' THEN ifnull((- f.ct_packs),0)	
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8015) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION,
				1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component dc
where f.dim_jda_componentid = dc.dim_jda_componentid
and (f.dd_type in ('Actual/Mkt & Tender Fcst Value EURO') OR dc.c_type3 = 'OP EURO')
and dd_fcstlevel IN ('111', '711')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8015 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* 7000 F1 (in Euro) @ OP rate */
--drop table if exists dw_version_cell_comp7000 ;
--create table dw_version_cell_comp7000 as 
truncate table dw_version_cell_comp7000;
insert into dw_version_cell_comp7000 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid,
			'F1 (in Euro) @ OP rate' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand,
			max(f.dd_itemlocalcode) as dd_itemlocalcode,
			max(f.dd_model) as dd_model,
			max(f.dd_fcstlevel) as dd_fcstlevel,
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			SUM(ifnull((ct_packs * dl.EXCHRATE),0))  as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE,
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 7000) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			max(SYSTBIAS3M) as dd_SYSTBIAS3M,
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			max(SEGMENTATION) as dd_SEGMENTATION,
				1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component dc, dim_jda_location dl
where 	f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_componentid = dc.dim_jda_componentid
	and dd_fcstlevel IN ('111', '711')
	and dc.c_type3 	 = 'F1 LC'
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

/* APP-6159 - F1 import for file */
insert into dw_version_cell_comp7000 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dt.dim_dateid as dim_dateidstartdate,
			0 as dd_planning_item_id,
			dl.dim_jda_locationid,
			dp.dim_jda_productid,
			'F1 (in Euro) @ OP rate' as dd_type,
			'Not Set' as dd_abc_class,
			'Not Set' as dd_localbrand,
			'Not Set' as dd_itemlocalcode,
			'Not Set' as dd_model,
			'Not Set' as dd_fcstlevel,
			'Not Set' as dd_plannig_item_flag,
			sum(csvf1.amt_f1Euro)  as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			'Not Set' as DD_DFULIFECYCLE,
			'Not Set' as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			'Not Set' as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 7000) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			'Not Set' as dd_SYSTBIAS3M,
			'Not Set' as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			'Not Set' as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from csv_ibp_importf1 csvf1, dim_jda_location dl, dim_jda_product dp, dim_date dt
where csvf1.location_name= dl.location_name
	and	csvf1.product_name = dp.product_name
	and	csvf1.startdate = dt.datevalue
	and	dt.companycode = 'Not Set'
	and dl.uda_currency_name <> 'Not Set'
	group by dt.dim_dateid ,dl.dim_jda_locationid, dp.dim_jda_productid;
/* END APP-6159 - F1 import frokm file */


--drop table if exists dw_version_cell_comp7000_nextyear ;
--create table dw_version_cell_comp7000_nextyear as 
truncate table dw_version_cell_comp7000_nextyear;
insert into dw_version_cell_comp7000_nextyear (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,
	DD_LOCALBRAND,DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
	CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
	CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,DIM_JDA_DATEHOLDERID,DATEVALUE)
	select dim_dateidstartdate, 
			dd_planning_item_id, 
			dim_jda_locationid, 
			dim_jda_productid, 
			dd_type, 
			dd_abc_class, 
			dd_localbrand, 
			dd_itemlocalcode, 
			dd_model, 
			dd_fcstlevel, 
			dd_plannig_item_flag,
			ct_packs, 
			ct_units,	
			ct_eq_unit, 
			ct_mcg,
			ct_iu,
			DD_DFULIFECYCLE, 
			DD_GAUSSITEMCODE, 
			CT_AVGSALESPMONTH, 
			CT_AVGFCST, 
			CT_OP_EURO,
			dd_dfuclass, 
			dim_jda_componentid, 
			ct_price, 
			ct_currRate,
			ct_AVG3M_BIAS1M, 
			ct_AVG6M_BIAS1M, 
			ct_AVG_MKTSFA_12M, 
			ct_AVG_STATSFA_12M, 
			ct_STATSFA, 
			ct_MKTSFA, 
			dd_SYSTBIAS3M, 
			dd_SYSTBIAS6M, 
			ct_VOLATILITY, 
			dd_SEGMENTATION,
			dim_jda_dateholderid,
			dt.datevalue as datevalue
from dw_version_cell_comp7000 f, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_date);

update dw_version_cell_comp7000_nextyear f
	set f.dim_dateidstartdate = dl.dim_dateid,
		f.datevalue = dl.datevalue
from dim_date dl, dw_version_cell_comp7000_nextyear f
	where dl.datevalue = (f.datevalue + interval '1' year)
	and dl.companycode = 'Not Set';

insert into dw_version_cell_comp7000 (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ct_packs,ct_units,ct_eq_unit,ct_mcg,ct_iu,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,
			dd_dfuclass,dim_jda_componentid,ct_price,ct_currRate,ct_AVG3M_BIAS1M,ct_AVG6M_BIAS1M,ct_AVG_MKTSFA_12M,ct_AVG_STATSFA_12M,
			ct_STATSFA,ct_MKTSFA,dd_SYSTBIAS3M, dd_SYSTBIAS6M,ct_VOLATILITY,dd_SEGMENTATION,dim_jda_dateholderid)
	select ah.dim_dateidstartdate, 
			ah.dd_planning_item_id, 
			ah.dim_jda_locationid, 
			ah.dim_jda_productid, 
			ah.dd_type, 
			ah.dd_abc_class, 
			ah.dd_localbrand, 
			ah.dd_itemlocalcode, 
			ah.dd_model, 
			ah.dd_fcstlevel, 
			ah.dd_plannig_item_flag,
			ah.ct_packs, 
			ah.ct_units,
			ah.ct_eq_unit, 
			ah.ct_mcg,
			ah.ct_iu,
			ah.DD_DFULIFECYCLE, 
			ah.DD_GAUSSITEMCODE, 
			ah.CT_AVGSALESPMONTH, 
			ah.CT_AVGFCST, 
			ah.CT_OP_EURO,
			ah.dd_dfuclass, 
			ah.dim_jda_componentid, 
			ah.ct_price, 
			ah.ct_currRate,
			ah.ct_AVG3M_BIAS1M, 
			ah.ct_AVG6M_BIAS1M, 
			ah.ct_AVG_MKTSFA_12M, 
			ah.ct_AVG_STATSFA_12M, 
			ah.ct_STATSFA, 
			ah.ct_MKTSFA, 
			ah.dd_SYSTBIAS3M, 
			ah.dd_SYSTBIAS6M, 
			ah.ct_VOLATILITY, 
			ah.dd_SEGMENTATION,
			ah.dim_jda_dateholderid
	from dw_version_cell_comp7000_nextyear ah;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp7000 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 7001 F2 (in Euro) @ OP rate */
--drop table if exists dw_version_cell_comp7001 ;
--create table dw_version_cell_comp7001 as 
truncate table dw_version_cell_comp7001;
insert into dw_version_cell_comp7001 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'F2 (in Euro) @ OP rate' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull((ct_packs * dl.EXCHRATE),0)  as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 7001) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION,
				1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component dc, dim_jda_location dl
where 	f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_componentid = dc.dim_jda_componentid
	and dd_fcstlevel 		  IN ('111', '711') 
	and dc.c_type3 			  = 'F2 LC';
	
--drop table if exists dw_version_cell_comp7001_nextyear ;
--create table dw_version_cell_comp7001_nextyear as 
truncate table dw_version_cell_comp7001_nextyear;
insert into dw_version_cell_comp7001_nextyear (dim_dateidstartdate, 
			dd_planning_item_id, 
			dim_jda_locationid, 
			dim_jda_productid, 
			dd_type, 
			dd_abc_class, 
			dd_localbrand, 
			dd_itemlocalcode, 
			dd_model, 
			dd_fcstlevel, 
			dd_plannig_item_flag,
			ct_packs, 
			ct_units,	
			ct_eq_unit, 
			ct_mcg,
			ct_iu,
			DD_DFULIFECYCLE, 
			DD_GAUSSITEMCODE, 
			CT_AVGSALESPMONTH, 
			CT_AVGFCST, 
			CT_OP_EURO,
			dd_dfuclass, 
			dim_jda_componentid, 
			ct_price, 
			ct_currRate,
			ct_AVG3M_BIAS1M, 
			ct_AVG6M_BIAS1M, 
			ct_AVG_MKTSFA_12M, 
			ct_AVG_STATSFA_12M, 
			ct_STATSFA, 
			ct_MKTSFA, 
			dd_SYSTBIAS3M, 
			dd_SYSTBIAS6M, 
			ct_VOLATILITY, 
			dd_SEGMENTATION,
			dim_jda_dateholderid,
			datevalue)
	select dim_dateidstartdate, 
			dd_planning_item_id, 
			dim_jda_locationid, 
			dim_jda_productid, 
			dd_type, 
			dd_abc_class, 
			dd_localbrand, 
			dd_itemlocalcode, 
			dd_model, 
			dd_fcstlevel, 
			dd_plannig_item_flag,
			ct_packs, 
			ct_units,	
			ct_eq_unit, 
			ct_mcg,
			ct_iu,
			DD_DFULIFECYCLE, 
			DD_GAUSSITEMCODE, 
			CT_AVGSALESPMONTH, 
			CT_AVGFCST, 
			CT_OP_EURO,
			dd_dfuclass, 
			dim_jda_componentid, 
			ct_price, 
			ct_currRate,
			ct_AVG3M_BIAS1M, 
			ct_AVG6M_BIAS1M, 
			ct_AVG_MKTSFA_12M, 
			ct_AVG_STATSFA_12M, 
			ct_STATSFA, 
			ct_MKTSFA, 
			dd_SYSTBIAS3M, 
			dd_SYSTBIAS6M, 
			ct_VOLATILITY, 
			dd_SEGMENTATION,
			dim_jda_dateholderid,
			dt.datevalue as datevalue
from dw_version_cell_comp7001 f, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_date);

update dw_version_cell_comp7001_nextyear f
	set f.dim_dateidstartdate = dl.dim_dateid,
		f.datevalue = dl.datevalue
from dim_date dl, dw_version_cell_comp7001_nextyear f
	where dl.datevalue = (f.datevalue + interval '1' year)
	and dl.companycode = 'Not Set';

insert into dw_version_cell_comp7001 (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ct_packs,ct_units,ct_eq_unit,ct_mcg,ct_iu,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,
			dd_dfuclass,dim_jda_componentid,ct_price,ct_currRate,ct_AVG3M_BIAS1M,ct_AVG6M_BIAS1M,ct_AVG_MKTSFA_12M,ct_AVG_STATSFA_12M,
			ct_STATSFA,ct_MKTSFA,dd_SYSTBIAS3M, dd_SYSTBIAS6M,ct_VOLATILITY,dd_SEGMENTATION,dim_jda_dateholderid)
	select ah.dim_dateidstartdate, 
			ah.dd_planning_item_id, 
			ah.dim_jda_locationid, 
			ah.dim_jda_productid, 
			ah.dd_type, 
			ah.dd_abc_class, 
			ah.dd_localbrand, 
			ah.dd_itemlocalcode, 
			ah.dd_model, 
			ah.dd_fcstlevel, 
			ah.dd_plannig_item_flag,
			ah.ct_packs, 
			ah.ct_units,
			ah.ct_eq_unit, 
			ah.ct_mcg,
			ah.ct_iu,
			ah.DD_DFULIFECYCLE, 
			ah.DD_GAUSSITEMCODE, 
			ah.CT_AVGSALESPMONTH, 
			ah.CT_AVGFCST, 
			ah.CT_OP_EURO,
			ah.dd_dfuclass, 
			ah.dim_jda_componentid, 
			ah.ct_price, 
			ah.ct_currRate,
			ah.ct_AVG3M_BIAS1M, 
			ah.ct_AVG6M_BIAS1M, 
			ah.ct_AVG_MKTSFA_12M, 
			ah.ct_AVG_STATSFA_12M, 
			ah.ct_STATSFA, 
			ah.ct_MKTSFA, 
			ah.dd_SYSTBIAS3M, 
			ah.dd_SYSTBIAS6M, 
			ah.ct_VOLATILITY, 
			ah.dd_SEGMENTATION,
			dim_jda_dateholderid
	from dw_version_cell_comp7001_nextyear ah;
	
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp7001 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 7002 F3 (in Euro) @ OP rate */
--drop table if exists dw_version_cell_comp7002 ;
--create table dw_version_cell_comp7002 as 
truncate table dw_version_cell_comp7002;
insert into dw_version_cell_comp7002 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'F3 (in Euro) @ OP rate' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull((ct_packs * dl.EXCHRATE),0)  as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass,  
			(select dim_jda_componentid from dim_jda_component where component_id = 7002) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_jda_component dc, dim_jda_location dl
where 	f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_componentid = dc.dim_jda_componentid
and 	dd_fcstlevel 		  IN ('111', '711') 
and 	dc.c_type3 			  = 'F3 LC';

--drop table if exists dw_version_cell_comp7002_nextyear ;
--create table dw_version_cell_comp7002_nextyear as 
truncate table dw_version_cell_comp7002_nextyear;
insert into dw_version_cell_comp7002_nextyear (dim_dateidstartdate, 
			dd_planning_item_id, 
			dim_jda_locationid, 
			dim_jda_productid, 
			dd_type, 
			dd_abc_class, 
			dd_localbrand, 
			dd_itemlocalcode, 
			dd_model, 
			dd_fcstlevel, 
			dd_plannig_item_flag,
			ct_packs, 
			ct_units,	
			ct_eq_unit, 
			ct_mcg,
			ct_iu,
			DD_DFULIFECYCLE, 
			DD_GAUSSITEMCODE, 
			CT_AVGSALESPMONTH, 
			CT_AVGFCST, 
			CT_OP_EURO,
			dd_dfuclass, 
			dim_jda_componentid, 
			ct_price, 
			ct_currRate,
			ct_AVG3M_BIAS1M, 
			ct_AVG6M_BIAS1M, 
			ct_AVG_MKTSFA_12M, 
			ct_AVG_STATSFA_12M, 
			ct_STATSFA, 
			ct_MKTSFA, 
			dd_SYSTBIAS3M, 
			dd_SYSTBIAS6M, 
			ct_VOLATILITY, 
			dd_SEGMENTATION,
			dim_jda_dateholderid,
			datevalue)
	select dim_dateidstartdate, 
			dd_planning_item_id, 
			dim_jda_locationid, 
			dim_jda_productid, 
			dd_type, 
			dd_abc_class, 
			dd_localbrand, 
			dd_itemlocalcode, 
			dd_model, 
			dd_fcstlevel, 
			dd_plannig_item_flag,
			ct_packs, 
			ct_units,	
			ct_eq_unit, 
			ct_mcg,
			ct_iu,
			DD_DFULIFECYCLE, 
			DD_GAUSSITEMCODE, 
			CT_AVGSALESPMONTH, 
			CT_AVGFCST, 
			CT_OP_EURO,
			dd_dfuclass, 
			dim_jda_componentid, 
			ct_price, 
			ct_currRate,
			ct_AVG3M_BIAS1M, 
			ct_AVG6M_BIAS1M, 
			ct_AVG_MKTSFA_12M, 
			ct_AVG_STATSFA_12M, 
			ct_STATSFA, 
			ct_MKTSFA, 
			dd_SYSTBIAS3M, 
			dd_SYSTBIAS6M, 
			ct_VOLATILITY, 
			dd_SEGMENTATION,
			dim_jda_dateholderid,
			dt.datevalue as datevalue
from dw_version_cell_comp7002 f, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_date);

update dw_version_cell_comp7002_nextyear f
	set f.dim_dateidstartdate = dl.dim_dateid,
		f.datevalue = dl.datevalue
from dim_date dl, dw_version_cell_comp7002_nextyear f
	where dl.datevalue = (f.datevalue + interval '1' year)
	and dl.companycode = 'Not Set';

insert into dw_version_cell_comp7002 (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ct_packs,ct_units,ct_eq_unit,ct_mcg,ct_iu,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,
			dd_dfuclass,dim_jda_componentid,ct_price,ct_currRate,ct_AVG3M_BIAS1M,ct_AVG6M_BIAS1M,ct_AVG_MKTSFA_12M,ct_AVG_STATSFA_12M,
			ct_STATSFA,ct_MKTSFA,dd_SYSTBIAS3M, dd_SYSTBIAS6M,ct_VOLATILITY,dd_SEGMENTATION,dim_jda_dateholderid)
	select ah.dim_dateidstartdate, 
			ah.dd_planning_item_id, 
			ah.dim_jda_locationid, 
			ah.dim_jda_productid, 
			ah.dd_type, 
			ah.dd_abc_class, 
			ah.dd_localbrand, 
			ah.dd_itemlocalcode, 
			ah.dd_model, 
			ah.dd_fcstlevel, 
			ah.dd_plannig_item_flag,
			ah.ct_packs, 
			ah.ct_units,
			ah.ct_eq_unit, 
			ah.ct_mcg,
			ah.ct_iu,
			ah.DD_DFULIFECYCLE, 
			ah.DD_GAUSSITEMCODE, 
			ah.CT_AVGSALESPMONTH, 
			ah.CT_AVGFCST, 
			ah.CT_OP_EURO,
			ah.dd_dfuclass, 
			ah.dim_jda_componentid, 
			ah.ct_price, 
			ah.ct_currRate,
			ah.ct_AVG3M_BIAS1M, 
			ah.ct_AVG6M_BIAS1M, 
			ah.ct_AVG_MKTSFA_12M, 
			ah.ct_AVG_STATSFA_12M, 
			ah.ct_STATSFA, 
			ah.ct_MKTSFA, 
			ah.dd_SYSTBIAS3M, 
			ah.dd_SYSTBIAS6M, 
			ah.ct_VOLATILITY, 
			ah.dd_SEGMENTATION,
			dim_jda_dateholderid
	from dw_version_cell_comp7002_nextyear ah;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp7002 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* insert current year value as next year value for F1/F2/F3 in LC - Oana V - 06 Sept 2016 */
--drop table if exists dw_version_cell_f1lc_nextyear ;
--create table dw_version_cell_f1lc_nextyear as 
truncate table dw_version_cell_f1lc_nextyear;
insert into dw_version_cell_f1lc_nextyear (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,datevalue)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Not Set' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs,0)  as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where c_type3 = 'F1 LC') as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION,
			dt.datevalue
from tmp_prefact_demand f, dim_jda_component dc, dim_date dt
where 	f.dim_jda_componentid = dc.dim_jda_componentid	
	and f.dim_dateidstartdate = dt.dim_Dateid	
	and dd_fcstlevel IN ('111', '711') 
	and dc.c_type3 	 = 'F1 LC'
	and date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date);

update dw_version_cell_f1lc_nextyear f
	set f.dim_dateidstartdate = dl.dim_dateid,
		f.datevalue = dl.datevalue
from dim_date dl, dw_version_cell_f1lc_nextyear f
	where dl.datevalue = (f.datevalue + interval '1' year)
	and dl.companycode = 'Not Set';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,ah.datevalue as startdate
	from dw_version_cell_f1lc_nextyear ah;
	
--drop table if exists dw_version_cell_f2lc_nextyear ;
--create table dw_version_cell_f2lc_nextyear as 
truncate table dw_version_cell_f2lc_nextyear;
insert into dw_version_cell_f2lc_nextyear (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,datevalue)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Not Set' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs,0)  as ct_packs, 
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where c_type3 = 'F2 LC') as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION,
			dt.datevalue
from tmp_prefact_demand f, dim_jda_component dc, dim_date dt
where 	f.dim_jda_componentid = dc.dim_jda_componentid	
	and f.dim_dateidstartdate = dt.dim_Dateid	
	and dd_fcstlevel IN ('111', '711') 
	and dc.c_type3 	 = 'F2 LC'
	and date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date);

update dw_version_cell_f2lc_nextyear f
	set f.dim_dateidstartdate = dl.dim_dateid,
		f.datevalue = dl.datevalue
from dim_date dl, dw_version_cell_f2lc_nextyear f
	where dl.datevalue = (f.datevalue + interval '1' year)
	and dl.companycode = 'Not Set';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,ah.datevalue as startdate
	from dw_version_cell_f2lc_nextyear ah;
	
--drop table if exists dw_version_cell_f3lc_nextyear ;
--create table dw_version_cell_f3lc_nextyear as 
truncate table dw_version_cell_f3lc_nextyear;
insert into dw_version_cell_f3lc_nextyear (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,datevalue)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid, 
			'Not Set' as dd_type, 
			dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
			ifnull(ct_packs,0)  as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			DFULIFECYCLE as DD_DFULIFECYCLE, 
			GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			dfuclass as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where c_type3 = 'F3 LC') as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			SYSTBIAS3M as dd_SYSTBIAS3M, 
			SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			SEGMENTATION as dd_SEGMENTATION,
			dt.datevalue
from tmp_prefact_demand f, dim_jda_component dc, dim_date dt
where 	f.dim_jda_componentid = dc.dim_jda_componentid	
	and f.dim_dateidstartdate = dt.dim_Dateid	
	and dd_fcstlevel IN ('111', '711') 
	and dc.c_type3 	 = 'F3 LC'
	and date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date);

update dw_version_cell_f3lc_nextyear f
	set f.dim_dateidstartdate = dl.dim_dateid,
		f.datevalue = dl.datevalue
from dim_date dl, dw_version_cell_f3lc_nextyear f
	where dl.datevalue = (f.datevalue + interval '1' year)
	and dl.companycode = 'Not Set';

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,ah.datevalue as startdate
	from dw_version_cell_f3lc_nextyear ah;
	
/* END insert next year value for F1/F2/F3 in LC - Oana V - 06Sept */
		
/* 5304 Actual Hist / Mkt Fcst /Tender */
--drop table if exists dw_version_cell_comp5304 ;
--create table dw_version_cell_comp5304 as 
truncate table dw_version_cell_comp5304;
insert into dw_version_cell_comp5304 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
			'Actual Hist / Mkt Fcst /Tender' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand,
			max(f.dd_itemlocalcode) as dd_itemlocalcode,
			max(f.dd_model) as dd_model,
			max(f.dd_fcstlevel) as dd_fcstlevel,
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ct_packs) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE,
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 5304) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			max(SYSTBIAS3M) as dd_SYSTBIAS3M,
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f
where dd_type IN ('TENDER','MKTFCST','Actual Hist')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;


insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp5304 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8042 Breathing Space (in Qty) */
--drop table if exists dw_version_cell_comp8042 ;
--create table dw_version_cell_comp8042 as 
truncate table dw_version_cell_comp8042;
insert into dw_version_cell_comp8042 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select ifnull(dw1.dim_dateidstartdate,dw2.dim_dateidstartdate) as dim_dateidstartdate, 
			ifnull(dw1.dd_planning_item_id,dw2.dd_planning_item_id) as dd_planning_item_id, 
			ifnull(dw1.dim_jda_locationid,dw2.dim_jda_locationid) as dim_jda_locationid, 
			ifnull(dw1.dim_jda_productid,dw2.dim_jda_productid) as dim_jda_productid, 
			'Breathing Space (in Qty)' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			ifnull(dw1.ct_packs,0) - ifnull(dw2.ct_packs,0) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE,
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 8042) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6901 dw1
	full outer join dw_version_cell_comp5304 dw2
	on 		dw1.dim_dateidstartdate = dw2.dim_dateidstartdate
		and dw1.dd_planning_item_id = dw2.dd_planning_item_id 
		and dw1.dim_jda_locationid 	= dw2.dim_jda_locationid
		and dw1.dim_jda_productid	= dw2.dim_jda_productid;
		
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8042 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8043 Breathing Space (in LC)*/
--drop table if exists dw_version_cell_comp8043;
--create table dw_version_cell_comp8043 as 
truncate table dw_version_cell_comp8043;
insert into dw_version_cell_comp8043 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select  ifnull(dw2.dim_dateidstartdate,dt.dim_dateid) as dim_dateidstartdate, 
			ifnull(dw2.dd_planning_item_id,dw1.planning_item_id) as dd_planning_item_id, 
			ifnull(dw2.dim_jda_locationid,dw1.dim_jda_locationid) as dim_jda_locationid, 
			ifnull(dw2.dim_jda_productid,dw1.dim_jda_productid) as dim_jda_productid, 
			'Breathing Space (in LC)' as dd_type, 
			ifnull(dw2.dd_abc_class, dw1.abc_class) as dd_abc_class,
			ifnull(dw2.dd_localbrand,dw1.localbrand) as dd_localbrand, 
			ifnull(dw2.dd_itemlocalcode,dw1.itemlocalcode) as dd_itemlocalcode, 
			ifnull(dw2.dd_model,dw1.model) as dd_model, 
			ifnull(dw2.dd_fcstlevel,dw1.fcstlevel) as dd_fcstlevel, 
			ifnull(dw2.dd_plannig_item_flag,dw1.plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(packs,0) - ifnull(ct_packs,0)) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			ifnull(dw2.DD_DFULIFECYCLE,dw1.DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw2.DD_GAUSSITEMCODE,dw1.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw2.dd_dfuclass,dw1.dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8043) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw2.dd_SYSTBIAS3M,dw1.SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw2.dd_SYSTBIAS6M,dw1.SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw2.dd_SEGMENTATION,dw1.SEGMENTATION) as dd_SEGMENTATION,
				1 as dim_jda_dateholderid
from dw_version_cell_comp6801 dw1 
	inner join dim_date dt
		on 	dt.datevalue 	= dw1.nwmgr_cell_start_date
		and dt.companycode 	= 'Not Set'
	full outer join dw_version_cell_comp1203 dw2
		on 	dt.dim_dateid			= dw2.dim_dateidstartdate
		and dw2.dd_planning_item_id = dw1.planning_item_id
		and dw2.dim_jda_locationid	= dw1.dim_jda_locationid
		and dw2.dim_jda_productid	= dw1.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8043 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8044 Breathing Space (in Euro) */
--drop table if exists dw_version_cell_comp8044 ;
--create table dw_version_cell_comp8044 as 
truncate table dw_version_cell_comp8044;
insert into dw_version_cell_comp8044 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select ifnull(dw1.dim_dateidstartdate,dw2.dim_dateidstartdate) as dim_dateidstartdate, 
			ifnull(dw1.dd_planning_item_id,dw2.dd_planning_item_id) as dd_planning_item_id, 
			ifnull(dw1.dim_jda_locationid,dw2.dim_jda_locationid) as dim_jda_locationid, 
			ifnull(dw1.dim_jda_productid,dw2.dim_jda_productid) as dim_jda_productid, 
			'Breathing Space (in Euro)' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0) - ifnull(dw2.ct_packs,0)) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select dim_jda_componentid from dim_jda_component where component_id = 8044) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6803 dw1
	full outer join dw_version_cell_comp1204 dw2
	on 		dw1.dim_dateidstartdate = dw2.dim_dateidstartdate
		and dw1.dd_planning_item_id = dw2.dd_planning_item_id 
		and dw1.dim_jda_locationid 	= dw2.dim_jda_locationid
		and dw1.dim_jda_productid	= dw2.dim_jda_productid;
		
insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8044 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8063 Actual / Mkt & Tender Fcst Value (in Qty) */
--drop table if exists dw_version_cell_comp8063 ;
--create table dw_version_cell_comp8063 as 
truncate table dw_version_cell_comp8063;
insert into dw_version_cell_comp8063 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Actual / Mkt & Tender Fcst Value (in Qty)' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ct_packs) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8063) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_jda_component djc
where f.dim_jda_componentid = djc.dim_jda_componentid
and f.dd_type = 'Actual Hist / Mkt Fcst /Tender'
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8063 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8064 Demand Risks & Business Opportunities (in Qty) */
--drop table if exists dw_version_cell_comp8064 ;
--create table dw_version_cell_comp8064 as 
truncate table dw_version_cell_comp8064;
insert into dw_version_cell_comp8064 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Demand Risks & Business Opportunities (in Qty)' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Best Estimate QTY' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type = 'Actual / Mkt & Tender Fcst Value (in Qty)' THEN ifnull((- f.ct_packs),0)
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8064) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f
where f.dd_type in ('Best Estimate QTY','Actual / Mkt & Tender Fcst Value (in Qty)')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8064 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8065 Supply Risk (in Qty) */
--drop table if exists dw_version_cell_comp8065;
--create table dw_version_cell_comp8065 as 
truncate table dw_version_cell_comp8065;
insert into dw_version_cell_comp8065 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Supply Risk (in Qty)' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Constrained Actual Hist/Mkt Fcst/Tender QTY' THEN ifnull(f.ct_packs,0)
				WHEN f.dd_type = 'Actual / Mkt & Tender Fcst Value (in Qty)' THEN ifnull((- f.ct_packs),0)
			END) as ct_packs,
			cast(0 as decimal(18,9)) as ct_units,
			cast(0 as decimal(18,9)) as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8065) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f
where f.dd_type  IN ('Constrained Actual Hist/Mkt Fcst/Tender QTY','Actual / Mkt & Tender Fcst Value (in Qty)') 
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8065 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8066 Actual / Mkt & Tender Forecast vs. OP (in Qty) */
--drop table if exists dw_version_cell_comp8066;
--create table dw_version_cell_comp8066 as 
truncate table dw_version_cell_comp8066;
insert into dw_version_cell_comp8066 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Actual / Mkt & Tender Forecast vs. OP (in Qty)' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(CASE 	
				WHEN f.dd_type = 'Actual / Mkt & Tender Fcst Value (in Qty)' THEN ifnull(f.ct_packs,0)
				WHEN dc.c_type3 = 'OP' THEN ifnull((- f.ct_packs),0)	
			END) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8066) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_jda_component dc
where f.dim_jda_componentid = dc.dim_jda_componentid
	and (f.dd_type in ('Actual / Mkt & Tender Fcst Value (in Qty)') OR dc.c_type3 = 'OP')
	and dd_fcstlevel IN ('111', '711')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8066 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* F1 Consensus Forecast Frozen (in Euro) */
truncate table dw_version_cell_comp8078;
insert into dw_version_cell_comp8078 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid,
			'F1 Consensus Forecast Frozen (in Euro)' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand,
			max(f.dd_itemlocalcode) as dd_itemlocalcode,
			max(f.dd_model) as dd_model,
			max(f.dd_fcstlevel) as dd_fcstlevel,
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull(ct_packs * EXCHRATE,0)) as ct_packs,
			0 as ct_units,
			0 as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE,
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 8078) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			max(SYSTBIAS3M) as dd_SYSTBIAS3M,
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_date dt, dim_jda_location dl
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dd_type = 'F1 Consensus Forecast Frozen (in LC)'
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8078 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* F2 Consensus Forecast Frozen (in Euro) */
truncate table dw_version_cell_comp8079;
insert into dw_version_cell_comp8079 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid,
			'F2 Consensus Forecast Frozen (in Euro)' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand,
			max(f.dd_itemlocalcode) as dd_itemlocalcode,
			max(f.dd_model) as dd_model,
			max(f.dd_fcstlevel) as dd_fcstlevel,
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull(ct_packs * EXCHRATE,0)) as ct_packs,
			0 as ct_units,
			0 as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE,
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 8079) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			max(SYSTBIAS3M) as dd_SYSTBIAS3M,
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_date dt, dim_jda_location dl
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dd_type = 'F2 Consensus Forecast Frozen (in LC)'
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8079 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* F3 Consensus Forecast Frozen (in Euro) */
truncate table dw_version_cell_comp8080;
insert into dw_version_cell_comp8080 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid,
			'F3 Consensus Forecast Frozen (in Euro)' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand,
			max(f.dd_itemlocalcode) as dd_itemlocalcode,
			max(f.dd_model) as dd_model,
			max(f.dd_fcstlevel) as dd_fcstlevel,
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull(ct_packs * EXCHRATE,0)) as ct_packs,
			0 as ct_units,
			0 as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE,
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 8080) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			max(SYSTBIAS3M) as dd_SYSTBIAS3M,
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_date dt, dim_jda_location dl
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dd_type = 'F3 Consensus Forecast Frozen (in LC)'
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8080 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* OP Consensus Forecast Frozen (in Euro) */
truncate table dw_version_cell_comp8081;
insert into dw_version_cell_comp8081 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION,dim_jda_dateholderid)
	select dim_dateidstartdate, dd_planning_item_id, f.dim_jda_locationid, dim_jda_productid,
			'OP Consensus Forecast Frozen (in Euro)' as dd_type,
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand,
			max(f.dd_itemlocalcode) as dd_itemlocalcode,
			max(f.dd_model) as dd_model,
			max(f.dd_fcstlevel) as dd_fcstlevel,
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum(ifnull(ct_packs * EXCHRATE,0)) as ct_packs,
			0 as ct_units,
			0 as ct_eq_unit,
			0 as ct_mcg,
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE,
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE,
			0 as CT_AVGSALESPMONTH,
			0 as CT_AVGFCST,
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass,
			(select dim_jda_componentid from dim_jda_component where component_id = 8081) as dim_jda_componentid,
			0 as ct_price,
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M,
			0 as ct_AVG6M_BIAS1M,
			0 as ct_AVG_MKTSFA_12M,
			0 as ct_AVG_STATSFA_12M,
			0 as ct_STATSFA,
			0 as ct_MKTSFA,
			max(SYSTBIAS3M) as dd_SYSTBIAS3M,
			max(SYSTBIAS6M) as dd_SYSTBIAS6M,
			0 as ct_VOLATILITY,
			max(SEGMENTATION) as dd_SEGMENTATION,
			1 as dim_jda_dateholderid
from tmp_prefact_demand f, dim_date dt, dim_jda_location dl
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dd_type = 'OP Consensus Forecast Frozen (in LC)'
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize))			  				AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize 	AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8081 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* Freeze Components - Oana 6 Dec 2016 - DON'T DELETE COMMENTED SECTION BELOW */
/*
truncate table tmp_jda_freezedcomp
insert into tmp_jda_freezedcomp (dim_dateidstartdate,dd_planning_item_id,dim_jda_locationid,dim_jda_productid,dd_type,dd_abc_class,dd_localbrand,dd_itemlocalcode,dd_model,
				dd_fcstlevel,dd_plannig_item_flag  ,ct_packs ,ct_units ,ct_eq_unit,ct_mcg ,ct_iu,dd_DFULIFECYCLE,dd_GAUSSITEMCODE,CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,
				dd_dfuclass,dim_jda_componentid,ct_AVG3M_BIAS1M,ct_AVG6M_BIAS1M,ct_AVG_MKTSFA_12M,ct_AVG_STATSFA_12M,ct_STATSFA,ct_MKTSFA ,dd_SYSTBIAS3M,dd_SYSTBIAS6M,
				ct_VOLATILITY,dd_SEGMENTATION)
	select f.dim_dateidstartdate,f.dd_planning_item_id,f.dim_jda_locationid,f.dim_jda_productid,f.dd_type,f.dd_abc_class,f.dd_localbrand,f.dd_itemlocalcode,f.dd_model,
		f.dd_fcstlevel,f.dd_plannig_item_flag  ,f.ct_packs ,f.ct_units ,f.ct_eq_unit,f.ct_mcg ,f.ct_iu,f.dd_DFULIFECYCLE,f.dd_GAUSSITEMCODE,f.CT_AVGSALESPMONTH,f.CT_AVGFCST,
		f.CT_OP_EURO,f.dd_dfuclass,f.dim_jda_componentid,f.ct_AVG3M_BIAS1M,f.ct_AVG6M_BIAS1M,f.ct_AVG_MKTSFA_12M,f.ct_AVG_STATSFA_12M,f.ct_STATSFA,f.ct_MKTSFA ,f.dd_SYSTBIAS3M,
		f.dd_SYSTBIAS6M,f.ct_VOLATILITY,f.dd_SEGMENTATION
	from fact_jda_demandforecast f , dim_date dt
	where f.dim_dateidstartdate = dt.dim_dateid
		and dd_type in ('Best Estimate EURO','Breathing Space (in Euro)','Consensus Forecast EURO','Constrained Actual/Mkt & Tender Fcst Value EURO',
			'Constrained Actual/Mkt Fcst Val wo Tender  EURO','Demand Adj Supply Risk Total EURO','Demand Adj Value/Business Risk & Opp EURO',
			'Demand Adj Value/Supply Risk EURO','Demand Adj Value Tender/Business Risk & Opp EURO','Demand Adj Value Tender/Supply Risk EURO',
			'Demand Risks & Business Opportunities Euro','Supply Risk EURO','Actual / Mkt & Tender Fcst Value (in Qty)',
			'MKTFCST','Best Estimate QTY','Breathing Space (in Qty)','Consensus Forecast QTY','Constrained Actual Hist/Mkt Fcst/Tender QTY',
			'Demand Adj/Supply Risk','Tender Demand Adj/Supply Risk','Demand Adj/Business Risk & Opportunities',
			'Tender Demand Adj/Business Risk & Opportunities','Actual Hist','Actual/Mkt & Tender Fcst Value EURO','Supply Risk (in Qty)',
			'Consensus Forecast (in Qty) Cum','Const Actual Hist/Mkt Fcst/Tender (in Qty) Cum','Best Estimate (in Qty) Cum',
			'Actual / Mkt & Tender Fcst Value (in Qty) Cum','Demand Risks & Business Opportunities Euro Cum',
			'Constr Act/Mkt & Tender Fcst Value (in Euro) Cum','Consensus Forecast (in Euro) Cum','Best Estimate (in Euro) Cum',
			'Actual / Mkt & Tender Fcst Value (in Euro) Cum','Constrained Actual Hist / Mkt Fcst /Tender (in Qty) Cum',
			'Demand Risks & Business Opportunities (in Qty) Cum','Supply Risk (in Qty) Cum','Supply Risk (in Euro) Cum','Consensus Forecast LC','Actual Hist / Mkt Fcst /Tender')
		and monthyear = to_char(current_date,'Mon YYYY')

--- to be executed after 4:00PM CET data load in the last day of the month
delete from tmp_prefact_demand
where dd_type in ('Best Estimate EURO','Breathing Space (in Euro)','Consensus Forecast EURO','Constrained Actual/Mkt & Tender Fcst Value EURO',
			'Constrained Actual/Mkt Fcst Val wo Tender  EURO','Demand Adj Supply Risk Total EURO','Demand Adj Value/Business Risk & Opp EURO',
			'Demand Adj Value/Supply Risk EURO','Demand Adj Value Tender/Business Risk & Opp EURO','Demand Adj Value Tender/Supply Risk EURO',
			'Demand Risks & Business Opportunities Euro','Supply Risk EURO','Actual / Mkt & Tender Fcst Value (in Qty)',
			'MKTFCST','Best Estimate QTY','Breathing Space (in Qty)','Consensus Forecast QTY','Constrained Actual Hist/Mkt Fcst/Tender QTY',
			'Demand Adj/Supply Risk','Tender Demand Adj/Supply Risk','Demand Adj/Business Risk & Opportunities',
			'Tender Demand Adj/Business Risk & Opportunities','Actual Hist','Actual/Mkt & Tender Fcst Value EURO','Supply Risk (in Qty)','Consensus Forecast LC','Actual Hist / Mkt Fcst /Tender')
	and startdate = (select LAG0_month from  dim_jda_dateholder)

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,
	dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
	ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
	DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
	AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
select  ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units
		   ,ah.ct_eq_unit							AS eq_unit
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from tmp_jda_freezedcomp ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate
	and dd_type in ('Best Estimate EURO','Breathing Space (in Euro)','Consensus Forecast EURO','Constrained Actual/Mkt & Tender Fcst Value EURO',
			'Constrained Actual/Mkt Fcst Val wo Tender  EURO','Demand Adj Supply Risk Total EURO','Demand Adj Value/Business Risk & Opp EURO',
			'Demand Adj Value/Supply Risk EURO','Demand Adj Value Tender/Business Risk & Opp EURO','Demand Adj Value Tender/Supply Risk EURO',
			'Demand Risks & Business Opportunities Euro','Supply Risk EURO','Actual / Mkt & Tender Fcst Value (in Qty)',
			'MKTFCST','Best Estimate QTY','Breathing Space (in Qty)','Consensus Forecast QTY','Constrained Actual Hist/Mkt Fcst/Tender QTY',
			'Demand Adj/Supply Risk','Tender Demand Adj/Supply Risk','Demand Adj/Business Risk & Opportunities',
			'Tender Demand Adj/Business Risk & Opportunities','Actual Hist','Actual/Mkt & Tender Fcst Value EURO','Supply Risk (in Qty)', 'Consensus Forecast LC','Actual Hist / Mkt Fcst /Tender')
*/
/* END Freeze Components - Oana 6 Dec 2016 */

/* Cumulative values */	
/* 2201 Actual / Mkt & Tender Fcst Value (in LC) Cum */
--drop table if exists dw_version_cell_comp2201_1;
--create table dw_version_cell_comp2201_1 as
truncate table dw_version_cell_comp2201_1;
insert into dw_version_cell_comp2201_1 (yearstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,dd_abc_class,dd_localbrand, 
		dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,DD_DFULIFECYCLE, DD_GAUSSITEMCODE, dd_dfuclass, 
		dim_jda_dateholderid,dd_SYSTBIAS3M, dd_SYSTBIAS6M, dd_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dd_dfuclass) as dd_dfuclass, 	
	max(f.dim_jda_dateholderid) as dim_jda_dateholderid, 
	max(f.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp1203 f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp2201_2;
--create table dw_version_cell_comp2201_2 as
truncate table dw_version_cell_comp2201_2;
insert into dw_version_cell_comp2201_2 (dim_Dateid,yearstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,dd_abc_class,dd_localbrand, dd_itemlocalcode, 
		dd_model, dd_fcstlevel, dd_plannig_item_flag,DD_DFULIFECYCLE, DD_GAUSSITEMCODE, dd_dfuclass, dim_jda_dateholderid, 
		dd_SYSTBIAS3M, dd_SYSTBIAS6M, dd_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp2201_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp2201_3;
--create table dw_version_cell_comp2201_3 as
truncate table dw_version_cell_comp2201_3;
insert into dw_version_cell_comp2201_3 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,
		DD_LOCALBRAND,DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,
		DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,
		CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Actual / Mkt & Tender Fcst Value (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 2201) as dim_jda_componentid,
			ifnull(dw1.dim_jda_dateholderid,dw2.dim_jda_dateholderid) as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp2201_2	dw2
left join dw_version_cell_comp1203 dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid;

--drop table if exists dw_version_cell_comp2201;
--create table dw_version_cell_comp2201 as 
truncate table dw_version_cell_comp2201;
insert into dw_version_cell_comp2201 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,CT_AVGSALESPMONTH,
		CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,dim_jda_dateholderid,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp2201_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp2201 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

		
/* 5201 Actual / Mkt & Tender Fcst Value (in Euro) Cum */
--drop table  if exists dw_version_cell_comp5201_1;
--create table dw_version_cell_comp5201_1 as
truncate table dw_version_cell_comp5201_1;
insert into dw_version_cell_comp5201_1 (yearstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,dd_abc_class,dd_localbrand, 
		dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,DD_DFULIFECYCLE, DD_GAUSSITEMCODE, dd_dfuclass, 
		dim_jda_dateholderid,dd_SYSTBIAS3M, dd_SYSTBIAS6M, dd_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	1 as dim_jda_dateholderid, 
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and dd_type = 'Actual/Mkt & Tender Fcst Value EURO'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp5201_2;
--create table dw_version_cell_comp5201_2 as
truncate table dw_version_cell_comp5201_2;
insert into dw_version_cell_comp5201_2(dim_Dateid,yearstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid,dd_abc_class,dd_localbrand, 
		dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,DD_DFULIFECYCLE, DD_GAUSSITEMCODE, dd_dfuclass, 
		dim_jda_dateholderid,dd_SYSTBIAS3M, dd_SYSTBIAS6M, dd_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp5201_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp5201_3;
--create table dw_version_cell_comp5201_3 as
truncate table dw_version_cell_comp5201_3;
insert into dw_version_cell_comp5201_3 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,
		CT_AVG_MKTSFA_12M,CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Actual / Mkt & Tender Fcst Value (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 5201) as dim_jda_componentid,
			1 as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp5201_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Actual/Mkt & Tender Fcst Value EURO';

--drop table if exists dw_version_cell_comp5201;
--create table dw_version_cell_comp5201 as
truncate table dw_version_cell_comp5201;
insert into dw_version_cell_comp5201(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,
		CT_AVG_MKTSFA_12M,CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp5201_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp5201 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 5202 Operating Plan (in LC) Cum */
--drop table if exists dw_version_cell_comp5202_1;
--create table dw_version_cell_comp5202_1 as
truncate table dw_version_cell_comp5202_1;
insert into dw_version_cell_comp5202_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,
			DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt, dim_jda_component djc
where dt.dim_dateid				= f.dim_dateidstartdate
	and djc.dim_jda_componentid	= f.dim_jda_componentid
	and f.dd_fcstlevel			IN ('111', '711')
	and djc.c_type3				= 'OP LC'
group by extract(year from datevalue),	f.dd_planning_item_id, 	f.dim_jda_locationid, 	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp5202_2;
--create table dw_version_cell_comp5202_2 as
truncate table dw_version_cell_comp5202_2;
insert into dw_version_cell_comp5202_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,
			DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION) 
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp5202_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp5202_3;
--create table dw_version_cell_comp5202_3 as
truncate table dw_version_cell_comp5202_3;
insert into dw_version_cell_comp5202_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Operating Plan (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 5202) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp5202_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
inner join dim_jda_component djc
	on	dw1.dim_jda_componentid	= djc.dim_jda_componentid
	and dw1.dd_fcstlevel		IN ('111', '711')
	and djc.c_type3 			= 'OP LC' ;

--drop table if exists dw_version_cell_comp5202;
--create table dw_version_cell_comp5202 as
truncate table dw_version_cell_comp5202;
insert into dw_version_cell_comp5202(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp5202_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp5202 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 5203	Operating Plan (in Euro) Cum */	
--drop table if exists dw_version_cell_comp5203_1;
--create table dw_version_cell_comp5203_1 as
truncate table dw_version_cell_comp5203_1;
insert into dw_version_cell_comp5203_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt, dim_jda_component djc
where dt.dim_dateid				= f.dim_dateidstartdate
	and djc.dim_jda_componentid	= f.dim_jda_componentid
	and f.dd_fcstlevel			IN ('111', '711')
	and djc.c_type3				= 'OP EURO'
group by extract(year from datevalue),	f.dd_planning_item_id, 	f.dim_jda_locationid, 	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp5203_2;
--create table dw_version_cell_comp5203_2 as
truncate table dw_version_cell_comp5203_2;
insert into dw_version_cell_comp5203_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp5203_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp5203_3;
--create table dw_version_cell_comp5203_3 as
truncate table dw_version_cell_comp5203_3;
insert into dw_version_cell_comp5203_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Operating Plan (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 5203) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp5203_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
inner join dim_jda_component djc
	on	dw1.dim_jda_componentid	= djc.dim_jda_componentid
	and dw1.dd_fcstlevel		IN ('111', '711')
	and djc.c_type3 			= 'OP EURO' ;

--drop table if exists dw_version_cell_comp5203;
--create table dw_version_cell_comp5203 as
truncate table dw_version_cell_comp5203;
insert into dw_version_cell_comp5203 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp5203_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp5203 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6200	F1 (in LC) Cum */	
--drop table if exists dw_version_cell_comp6200_1;
--create table dw_version_cell_comp6200_1 as
truncate table dw_version_cell_comp6200_1;
insert into dw_version_cell_comp6200_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,DD_ITEMLOCALCODE,
		DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt, dim_jda_component djc
where dt.dim_dateid				= f.dim_dateidstartdate
	and djc.dim_jda_componentid	= f.dim_jda_componentid
	and f.dd_fcstlevel			IN ('111', '711')
	and djc.c_type3				= 'F1 LC'
group by extract(year from datevalue),	f.dd_planning_item_id, 	f.dim_jda_locationid, 	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6200_2;
--create table dw_version_cell_comp6200_2 as
truncate table dw_version_cell_comp6200_2;
insert into dw_version_cell_comp6200_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6200_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6200_3;
--create table dw_version_cell_comp6200_3 as
truncate table dw_version_cell_comp6200_3;
insert into dw_version_cell_comp6200_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'F1 (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6200) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6200_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
inner join dim_jda_component djc
	on	dw1.dim_jda_componentid	= djc.dim_jda_componentid
	and dw1.dd_fcstlevel		IN ('111', '711')
	and djc.c_type3 			= 'F1 LC' ;

--drop table if exists dw_version_cell_comp6200;
--create table dw_version_cell_comp6200 as
truncate table dw_version_cell_comp6200;
insert into dw_version_cell_comp6200 (DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6200_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6200 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6203	F2 (in LC) Cum */
--drop table if exists dw_version_cell_comp6203_1;
--create table dw_version_cell_comp6203_1 as
truncate table dw_version_cell_comp6203_1;
insert into dw_version_cell_comp6203_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	 
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt, dim_jda_component djc
where dt.dim_dateid				= f.dim_dateidstartdate
	and djc.dim_jda_componentid	= f.dim_jda_componentid
	and f.dd_fcstlevel			IN ('111', '711')
	and djc.c_type3				= 'F2 LC'
group by extract(year from datevalue),	f.dd_planning_item_id, 	f.dim_jda_locationid, 	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6203_2;
--create table dw_version_cell_comp6203_2 as
truncate table dw_version_cell_comp6203_2;
insert into dw_version_cell_comp6203_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6203_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6203_3;
--create table dw_version_cell_comp6203_3 as
truncate table dw_version_cell_comp6203_3;
insert into dw_version_cell_comp6203_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'F2 (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6203) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6203_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
inner join dim_jda_component djc
	on	dw1.dim_jda_componentid	= djc.dim_jda_componentid
	and dw1.dd_fcstlevel		IN ('111', '711')
	and djc.c_type3 			= 'F2 LC' ;

--drop table if exists dw_version_cell_comp6203;
--create table dw_version_cell_comp6203 as
truncate table dw_version_cell_comp6203;
insert into dw_version_cell_comp6203(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6203_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6203 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6204	F3 (in LC) Cum */	
--drop table if exists dw_version_cell_comp6204_1;
--create table dw_version_cell_comp6204_1 as
truncate table dw_version_cell_comp6204_1;
insert into dw_version_cell_comp6204_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt, dim_jda_component djc
where dt.dim_dateid				= f.dim_dateidstartdate
	and djc.dim_jda_componentid	= f.dim_jda_componentid
	and f.dd_fcstlevel			IN ('111', '711')
	and djc.c_type3				= 'F3 LC'
group by extract(year from datevalue),	f.dd_planning_item_id, 	f.dim_jda_locationid, 	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6204_2;
--create table dw_version_cell_comp6204_2 as
truncate table dw_version_cell_comp6204_2;
insert into dw_version_cell_comp6204_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6204_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6204_3;
--create table dw_version_cell_comp6204_3 as
truncate table dw_version_cell_comp6204_3;
insert into dw_version_cell_comp6204_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'F3 (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6204) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6204_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
inner join dim_jda_component djc
	on	dw1.dim_jda_componentid	= djc.dim_jda_componentid
	and dw1.dd_fcstlevel		IN ('111', '711')
	and djc.c_type3 			= 'F3 LC' ;

--drop table if exists dw_version_cell_comp6204;
--create table dw_version_cell_comp6204 as
truncate table dw_version_cell_comp6204;
insert into dw_version_cell_comp6204(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6204_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6204 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6205	F1 (in Euro) Cum */
--drop table if exists  dw_version_cell_comp6205_1;
--create table dw_version_cell_comp6205_1 as
truncate table dw_version_cell_comp6205_1;
insert into dw_version_cell_comp6205_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,dim_jda_dateholderid,DD_SYSTBIAS3M,
		DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dd_dfuclass) as dd_dfuclass, 	
	max(f.dim_jda_dateholderid) as dim_jda_dateholderid, 
	max(f.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp7000 f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6205_2;
--create table dw_version_cell_comp6205_2 as
truncate table dw_version_cell_comp6205_2;
insert into dw_version_cell_comp6205_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,
		DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6205_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6205_3;
--create table dw_version_cell_comp6205_3 as
truncate table dw_version_cell_comp6205_3;
insert into dw_version_cell_comp6205_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,
		CT_AVG_MKTSFA_12M,CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'F1 (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6205) as dim_jda_componentid,
			ifnull(dw1.dim_jda_dateholderid,dw2.dim_jda_dateholderid) as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6205_2	dw2
left join dw_version_cell_comp7000 dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid;

--drop table if exists dw_version_cell_comp6205;
--create table dw_version_cell_comp6205 as
truncate table dw_version_cell_comp6205;
insert into dw_version_cell_comp6205(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,dim_jda_dateholderid,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6205_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6205 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* 6206	F2 (in Euro) Cum */	
--drop table if exists dw_version_cell_comp6206_1;
--create table dw_version_cell_comp6206_1 as
truncate table dw_version_cell_comp6206_1;
insert into dw_version_cell_comp6206_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,dim_jda_dateholderid,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dd_dfuclass) as dd_dfuclass, 	
	max(f.dim_jda_dateholderid) as dim_jda_dateholderid, 
	max(f.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp7001 f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6206_2;
--create table dw_version_cell_comp6206_2 as
truncate table dw_version_cell_comp6206_2;
insert into dw_version_cell_comp6206_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
	DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,
	DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6206_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6206_3;
--create table dw_version_cell_comp6206_3 as
truncate table dw_version_cell_comp6206_3;
insert into dw_version_cell_comp6206_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,dim_jda_dateholderid,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'F2 (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6206) as dim_jda_componentid,
			ifnull(dw1.dim_jda_dateholderid,dw2.dim_jda_dateholderid) as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6206_2	dw2
left join dw_version_cell_comp7001 dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid;

--drop table if exists dw_version_cell_comp6206;
--create table dw_version_cell_comp6206 as
truncate table dw_version_cell_comp6206;
insert into dw_version_cell_comp6206(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,dim_jda_dateholderid,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6206_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6206 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6207	F3 (in Euro) Cum */	
--drop table if exists dw_version_cell_comp6207_1;
--create table dw_version_cell_comp6207_1 as
truncate table dw_version_cell_comp6207_1;
insert into dw_version_cell_comp6207_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,dim_jda_dateholderid,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dd_dfuclass) as dd_dfuclass, 	
	max(f.dim_jda_dateholderid) as dim_jda_dateholderid, 
	max(f.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp7002 f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6207_2;
--create table dw_version_cell_comp6207_2 as
truncate table dw_version_cell_comp6207_2;
insert into dw_version_cell_comp6207_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
	DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,
	DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6207_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6207_3;
--create table dw_version_cell_comp6207_3 as
truncate table dw_version_cell_comp6207_3;
insert into dw_version_cell_comp6207_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'F3 (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6207) as dim_jda_componentid,
			ifnull(dw1.dim_jda_dateholderid,dw2.dim_jda_dateholderid) as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6207_2	dw2
left join dw_version_cell_comp7002 dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid;

--drop table if exists dw_version_cell_comp6207;
--create table dw_version_cell_comp6207 as
truncate table dw_version_cell_comp6207;
insert into dw_version_cell_comp6207(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6207_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6207 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* 6604	Constrained Actual / Mkt & Tender Fcst Value (in LC) Cum */
--drop table if exists dw_version_cell_comp6604_1;
--create table dw_version_cell_comp6604_1 as
truncate table dw_version_cell_comp6604_1;
insert into dw_version_cell_comp6604_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dd_dfuclass) as dd_dfuclass, 	
	max(f.dim_jda_dateholderid) as dim_jda_dateholderid, 
	max(f.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.dd_SEGMENTATION) as dd_SEGMENTATION	
from dw_version_cell_comp6508 f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6604_2;
--create table dw_version_cell_comp6604_2 as
truncate table dw_version_cell_comp6604_2;
insert into dw_version_cell_comp6604_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,
		DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6604_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6604_3;
--create table dw_version_cell_comp6604_3 as
truncate table dw_version_cell_comp6604_3;
insert into dw_version_cell_comp6604_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Constr Act/Mkt & Tender Fcst Value (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6604) as dim_jda_componentid,
			ifnull(dw1.dim_jda_dateholderid,dw2.dim_jda_dateholderid) as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6604_2	dw2
left join dw_version_cell_comp6508 dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid;

--drop table if exists dw_version_cell_comp6604;
--create table dw_version_cell_comp6604 as
truncate table dw_version_cell_comp6604;
insert into dw_version_cell_comp6604(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6604_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6604 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6605	Constrained Actual / Mkt & Tender Fcst Values (in Euro) Cum */
--drop table if exists  dw_version_cell_comp6605_1;
--create table dw_version_cell_comp6605_1 as
truncate table dw_version_cell_comp6605_1;
insert into dw_version_cell_comp6605_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	1 as dim_jda_dateholderid, 
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Constrained Actual/Mkt & Tender Fcst Value EURO'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6605_2;
--create table dw_version_cell_comp6605_2 as
truncate table dw_version_cell_comp6605_2;
insert into dw_version_cell_comp6605_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,
		DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6605_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6605_3;
--create table dw_version_cell_comp6605_3 as
truncate table dw_version_cell_comp6605_3;
insert into dw_version_cell_comp6605_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Constr Act/Mkt & Tender Fcst Value (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6605) as dim_jda_componentid,
			1 as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6605_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Constrained Actual/Mkt & Tender Fcst Value EURO';

--drop table if exists dw_version_cell_comp6605;
--create table dw_version_cell_comp6605 as
truncate table dw_version_cell_comp6605;
insert into dw_version_cell_comp6605(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6605_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6605 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6704	Best Estimate (in LC) Cum */
--drop table if exists dw_version_cell_comp6704_1;
--create table dw_version_cell_comp6704_1 as
truncate table dw_version_cell_comp6704_1;
insert into dw_version_cell_comp6704_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dd_dfuclass) as dd_dfuclass, 	
	max(f.dim_jda_dateholderid) as dim_jda_dateholderid, 
	max(f.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6703 f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6704_2;
--create table dw_version_cell_comp6704_2 as
truncate table dw_version_cell_comp6704_2;
insert into dw_version_cell_comp6704_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6704_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6704_3;
--create table dw_version_cell_comp6704_3 as
truncate table dw_version_cell_comp6704_3;
insert into dw_version_cell_comp6704_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Best Estimate (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6704) as dim_jda_componentid,
			ifnull(dw1.dim_jda_dateholderid,dw2.dim_jda_dateholderid) as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6704_2	dw2
left join dw_version_cell_comp6703 dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid;

--drop table if exists dw_version_cell_comp6704;
--create table dw_version_cell_comp6704 as
truncate table dw_version_cell_comp6704;
insert into dw_version_cell_comp6704(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6704_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6704 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6706	Best Estimate (in Euro) Cum */
--drop table if exists dw_version_cell_comp6706_1;
--create table dw_version_cell_comp6706_1 as
truncate table dw_version_cell_comp6706_1;
insert into dw_version_cell_comp6706_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	1 as dim_jda_dateholderid, 
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and dd_type = 'Best Estimate EURO'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6706_2;
--create table dw_version_cell_comp6706_2 as
truncate table dw_version_cell_comp6706_2;
insert into dw_version_cell_comp6706_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6706_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6706_3;
--create table dw_version_cell_comp6706_3 as
truncate table dw_version_cell_comp6706_3;
insert into dw_version_cell_comp6706_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Best Estimate (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6706) as dim_jda_componentid,
			1 as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6706_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Best Estimate EURO';

--drop table if exists dw_version_cell_comp6706;
--create table dw_version_cell_comp6706 as
truncate table dw_version_cell_comp6706;
insert into dw_version_cell_comp6706(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6706_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6706 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6802	Consensus Forecast (in LC) Cum */
--drop table if exists dw_version_cell_comp6802_1;
--create table dw_version_cell_comp6802_1 as
truncate table dw_version_cell_comp6802_1;
insert into dw_version_cell_comp6802_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass,
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
and f.dd_type				= 'Consensus Forecast LC'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6802_2;
--create table dw_version_cell_comp6802_2 as
truncate table dw_version_cell_comp6802_2;
insert into dw_version_cell_comp6802_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6802_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6802_3;
--create table dw_version_cell_comp6802_3 as
truncate table dw_version_cell_comp6802_3;
insert into dw_version_cell_comp6802_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Consensus Forecast (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6802) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6802_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Consensus Forecast LC';

--drop table if exists dw_version_cell_comp6802;
--create table dw_version_cell_comp6802 as
truncate table dw_version_cell_comp6802;
insert into dw_version_cell_comp6802(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6802_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6802 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 6804	Consensus Forecast (in Euro) Cum */
--drop table if exists dw_version_cell_comp6804_1;
--create table dw_version_cell_comp6804_1 as
truncate table dw_version_cell_comp6804_1;
insert into dw_version_cell_comp6804_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	1 as dim_jda_dateholderid, 
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Consensus Forecast EURO'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp6804_2;
--create table dw_version_cell_comp6804_2 as
truncate table dw_version_cell_comp6804_2;
insert into dw_version_cell_comp6804_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp6804_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp6804_3;
--create table dw_version_cell_comp6804_3 as
truncate table dw_version_cell_comp6804_3;
insert into dw_version_cell_comp6804_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Consensus Forecast (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 6804) as dim_jda_componentid,
			1 as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp6804_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Consensus Forecast EURO';

--drop table if exists dw_version_cell_comp6804;
--create table dw_version_cell_comp6804 as
truncate table dw_version_cell_comp6804;
insert into dw_version_cell_comp6804(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp6804_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp6804 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8024	Demand Risks & Business Opportunities ( in LC ) Cum */
--drop table if exists dw_version_cell_comp8024_1;
--create table dw_version_cell_comp8024_1 as
truncate table dw_version_cell_comp8024_1;
insert into dw_version_cell_comp8024_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dd_dfuclass) as dd_dfuclass, 	
	max(f.dim_jda_dateholderid) as dim_jda_dateholderid, 
	max(f.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8010 f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8024_2;
--create table dw_version_cell_comp8024_2 as
truncate table dw_version_cell_comp8024_2;
insert into dw_version_cell_comp8024_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8024_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8024_3;
--create table dw_version_cell_comp8024_3 as
truncate table dw_version_cell_comp8024_3;
insert into dw_version_cell_comp8024_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Demand Risks & Business Opportunities LC Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8024) as dim_jda_componentid,
			ifnull(dw1.dim_jda_dateholderid,dw2.dim_jda_dateholderid) as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8024_2	dw2
left join dw_version_cell_comp8010 dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid;

--drop table if exists dw_version_cell_comp8024;
--create table dw_version_cell_comp8024 as
truncate table dw_version_cell_comp8024;
insert into dw_version_cell_comp8024(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8024_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8024 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
		
/* 8025	Demand Risks & Business Opportunities ( in Euro) Cum */
--drop table if exists dw_version_cell_comp8025_1;
--create table dw_version_cell_comp8025_1 as
truncate table dw_version_cell_comp8025_1;
insert into dw_version_cell_comp8025_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	1 as dim_jda_dateholderid, 
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and dd_type = 'Demand Risks & Business Opportunities Euro'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8025_2;
--create table dw_version_cell_comp8025_2 as
truncate table dw_version_cell_comp8025_2;
insert into dw_version_cell_comp8025_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8025_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8025_3;
--create table dw_version_cell_comp8025_3 as
truncate table dw_version_cell_comp8025_3;
insert into dw_version_cell_comp8025_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Demand Risks & Business Opportunities Euro Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8025) as dim_jda_componentid,
			1 as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8025_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Demand Risks & Business Opportunities Euro';

--drop table if exists dw_version_cell_comp8025;
--create table dw_version_cell_comp8025 as
truncate table dw_version_cell_comp8025;
insert into dw_version_cell_comp8025(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8025_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8025 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8026	Supply Risk (in LC) Cum */
--drop table if exists dw_version_cell_comp8026_1;
--create table dw_version_cell_comp8026_1 as
truncate table dw_version_cell_comp8026_1;
insert into dw_version_cell_comp8026_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dd_dfuclass) as dd_dfuclass, 	
	max(f.dim_jda_dateholderid) as dim_jda_dateholderid, 
	max(f.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8012 f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8026_2;
--create table dw_version_cell_comp8026_2 as
truncate table dw_version_cell_comp8026_2;
insert into dw_version_cell_comp8026_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8026_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8026_3;
--create table dw_version_cell_comp8026_3 as
truncate table dw_version_cell_comp8026_3;
insert into dw_version_cell_comp8026_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Supply Risk (in LC) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DD_DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.DD_GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dd_dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8026) as dim_jda_componentid,
			ifnull(dw1.dim_jda_dateholderid,dw2.dim_jda_dateholderid) as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.dd_SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.dd_SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.dd_SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8026_2	dw2
left join dw_version_cell_comp8012 dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid;

--drop table if exists dw_version_cell_comp8026;
--create table dw_version_cell_comp8026 as
truncate table dw_version_cell_comp8026;
insert into dw_version_cell_comp8026(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8026_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8026 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8027	Supply Risk (in Euro) Cum */
--drop table if exists dw_version_cell_comp8027_1;
--create table dw_version_cell_comp8027_1 as
truncate table dw_version_cell_comp8027_1;
insert into dw_version_cell_comp8027_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	1 as dim_jda_dateholderid, 
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Supply Risk EURO'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8027_2;
--create table dw_version_cell_comp8027_2 as
truncate table dw_version_cell_comp8027_2;
insert into dw_version_cell_comp8027_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DIM_JDA_DATEHOLDERID,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8027_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8027_3;
--create table dw_version_cell_comp8027_3 as
truncate table dw_version_cell_comp8027_3;
insert into dw_version_cell_comp8027_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Supply Risk (in Euro) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8027) as dim_jda_componentid,
			1 as dim_jda_dateholderid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8027_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Supply Risk EURO';

--drop table if exists dw_version_cell_comp8027;
--create table dw_version_cell_comp8027 as
truncate table dw_version_cell_comp8027;
insert into dw_version_cell_comp8027(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			f2.dim_jda_dateholderid as dim_jda_dateholderid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8027_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8027 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8028	Actual / Mkt & Tender Forecast [in LC] vs. OP Cum */
--drop table if exists dw_version_cell_comp8028;
--create table dw_version_cell_comp8028 as 
truncate table dw_version_cell_comp8028;
insert into dw_version_cell_comp8028(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Actual/Mkt & Tender Forecast vs. OP LC Cum' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum (case when f.dd_type = 'Actual / Mkt & Tender Fcst Value (in LC) Cum' then ifnull(ct_packs,0)
					  when f.dd_type = 'Operating Plan (in LC) Cum'  then ifnull((- ct_packs),0)
				end
			)as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8028) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f
where f.dd_type 				in ('Actual / Mkt & Tender Fcst Value (in LC) Cum','Operating Plan (in LC) Cum')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8028 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8029	Actual / Mkt & Tender Forecast [in Euro] vs. OP Cum */
--drop table if exists dw_version_cell_comp8029;
--create table dw_version_cell_comp8029 as 
truncate table dw_version_cell_comp8029;
insert into dw_version_cell_comp8029(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
	select dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
			'Actual/Mkt & Tender Forecast vs. OP EURO Cum' as dd_type, 
			max(f.dd_abc_class) as dd_abc_class,
			max(f.dd_localbrand) as dd_localbrand, 
			max(f.dd_itemlocalcode) as dd_itemlocalcode, 
			max(f.dd_model) as dd_model, 
			max(f.dd_fcstlevel) as dd_fcstlevel, 
			max(f.dd_plannig_item_flag) as dd_plannig_item_flag,
			sum (case when f.dd_type = 'Actual / Mkt & Tender Fcst Value (in Euro) Cum' then ifnull(ct_packs,0)
					  when f.dd_type = 'Operating Plan (in Euro) Cum'  then ifnull((- ct_packs),0)
				end
			)as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			max(DFULIFECYCLE) as DD_DFULIFECYCLE, 
			max(GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			max(dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8029) as dim_jda_componentid, 
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			max(SYSTBIAS3M) as dd_SYSTBIAS3M, 
			max(SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			max(SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f
where 	f.dd_type 				in ('Actual / Mkt & Tender Fcst Value (in Euro) Cum','Operating Plan (in Euro) Cum')
group by f.dim_dateidstartdate, f.dd_planning_item_id, f.dim_jda_locationid, f.dim_jda_productid;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8029 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8071	Best Estimate (in Qty) Cum */
--drop table if exists dw_version_cell_comp8071_1;
--create table dw_version_cell_comp8071_1 as
truncate table dw_version_cell_comp8071_1;
insert into dw_version_cell_comp8071_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Best Estimate QTY'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8071_2;
--create table dw_version_cell_comp8071_2 as
truncate table dw_version_cell_comp8071_2;
insert into dw_version_cell_comp8071_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8071_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8071_3;
--create table dw_version_cell_comp8071_3 as
truncate table dw_version_cell_comp8071_3;
insert into dw_version_cell_comp8071_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Best Estimate (in Qty) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8071) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8071_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Best Estimate QTY';

--drop table if exists dw_version_cell_comp8071;
--create table dw_version_cell_comp8071 as
truncate table dw_version_cell_comp8071;
insert into dw_version_cell_comp8071(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8071_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8071 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8070	Demand Risks & Business Opportunities (in Qty) Cum*/
--drop table if exists dw_version_cell_comp8070_1;
--create table dw_version_cell_comp8070_1 as
truncate table dw_version_cell_comp8070_1;
insert into dw_version_cell_comp8070_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Demand Risks & Business Opportunities (in Qty)'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8070_2;
--create table dw_version_cell_comp8070_2 as
truncate table dw_version_cell_comp8070_2;
insert into dw_version_cell_comp8070_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8070_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8070_3;
--create table dw_version_cell_comp8070_3 as
truncate table dw_version_cell_comp8070_3;
insert into dw_version_cell_comp8070_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Demand Risks & Business Opportunities (in Qty) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8070) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8070_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Demand Risks & Business Opportunities (in Qty)';

--drop table if exists dw_version_cell_comp8070;
--create table dw_version_cell_comp8070 as
truncate table dw_version_cell_comp8070;
insert into dw_version_cell_comp8070(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8070_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8070 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;

/* 8069 Actual / Mkt & Tender Fcst Value (in Qty) Cum */
--drop table if exists dw_version_cell_comp8069_1;
--create table dw_version_cell_comp8069_1 as
truncate table dw_version_cell_comp8069_1;
insert into dw_version_cell_comp8069_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Actual / Mkt & Tender Fcst Value (in Qty)'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8069_2;
--create table dw_version_cell_comp8069_2 as
truncate table dw_version_cell_comp8069_2;
insert into dw_version_cell_comp8069_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8069_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8069_3;
--create table dw_version_cell_comp8069_3 as
truncate table dw_version_cell_comp8069_3;
insert into dw_version_cell_comp8069_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Actual / Mkt & Tender Fcst Value (in Qty) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8069) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8069_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Actual / Mkt & Tender Fcst Value (in Qty)';

--drop table if exists dw_version_cell_comp8069;
--create table dw_version_cell_comp8069 as
truncate table dw_version_cell_comp8069;
insert into dw_version_cell_comp8069(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8069_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,ah.ct_packs * pr.packsize 				    AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize AS eq_unit
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.dosetomcg   AS mcg
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize ))* pr.dosetoiu    AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8069 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8068 Operating Plan (in Qty) Cum */
--drop table if exists dw_version_cell_comp8068_1;
--create table dw_version_cell_comp8068_1 as
truncate table dw_version_cell_comp8068_1;
insert into dw_version_cell_comp8068_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt, dim_jda_component djc
where dt.dim_dateid				= f.dim_dateidstartdate
	and djc.dim_jda_componentid	= f.dim_jda_componentid
	and f.dd_fcstlevel			IN ('111', '711')
	and djc.c_type3				= 'OP'
group by extract(year from datevalue),	f.dd_planning_item_id, 	f.dim_jda_locationid, 	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8068_2;
--create table dw_version_cell_comp8068_2 as
truncate table dw_version_cell_comp8068_2;
insert into dw_version_cell_comp8068_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8068_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8068_3;
--create table dw_version_cell_comp8068_3 as
truncate table dw_version_cell_comp8068_3;
insert into dw_version_cell_comp8068_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Operating Plan (in Qty) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8068) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8068_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
inner join dim_jda_component djc
	on	dw1.dim_jda_componentid	= djc.dim_jda_componentid
	and dw1.dd_fcstlevel		IN ('111', '711')
	and djc.c_type3 			= 'OP' ;

--drop table if exists dw_version_cell_comp8068;
--create table dw_version_cell_comp8068 as
truncate table dw_version_cell_comp8068;
insert into dw_version_cell_comp8068(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8068_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,ah.ct_packs * pr.packsize 				    AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize AS eq_unit
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.dosetomcg   AS mcg
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize ))* pr.dosetoiu    AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8068 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;

/* 8072	Supply Risk (in Qty) Cum */
--drop table if exists dw_version_cell_comp8072_1;
--create table dw_version_cell_comp8072_1 as
truncate table dw_version_cell_comp8072_1;
insert into dw_version_cell_comp8072_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Supply Risk (in Qty)'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8072_2;
--create table dw_version_cell_comp8072_2 as
truncate table dw_version_cell_comp8072_2;
insert into dw_version_cell_comp8072_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8072_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8072_3;
--create table dw_version_cell_comp8072_3 as
truncate table dw_version_cell_comp8072_3;
insert into dw_version_cell_comp8072_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Supply Risk (in Qty) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8072) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8072_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Supply Risk (in Qty)';

--drop table if exists dw_version_cell_comp8072;
--create table dw_version_cell_comp8072 as
truncate table dw_version_cell_comp8072;
insert into dw_version_cell_comp8072(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8072_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8072 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8073	Constrained Actual Hist / Mkt Fcst /Tender (in Qty) Cum */
--drop table if exists dw_version_cell_comp8073_1;
--create table dw_version_cell_comp8073_1 as
truncate table dw_version_cell_comp8073_1;
insert into dw_version_cell_comp8073_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Constrained Actual Hist/Mkt Fcst/Tender QTY'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8073_2;
--create table dw_version_cell_comp8073_2 as
truncate table dw_version_cell_comp8073_2;
insert into dw_version_cell_comp8073_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8073_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8073_3;
--create table dw_version_cell_comp8073_3 as
truncate table dw_version_cell_comp8073_3;
insert into dw_version_cell_comp8073_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Const Actual Hist/Mkt Fcst/Tender (in Qty) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8073) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8073_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Constrained Actual Hist/Mkt Fcst/Tender QTY';

--drop table if exists dw_version_cell_comp8073;
--create table dw_version_cell_comp8073 as
truncate table dw_version_cell_comp8073;
insert into dw_version_cell_comp8073(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8073_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
		   ,ah.ct_units 		 				    AS units 
		   ,ah.ct_eq_unit							AS eq_unit 
		   ,ah.ct_mcg 								AS mcg
		   ,ah.ct_iu							    AS iu		   
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA 
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8073 ah, dim_date dc
	where dc.dim_dateid = ah.dim_dateidstartdate;
	
/* 8074	Consensus Forecast (in Qty) Cum */
--drop table if exists dw_version_cell_comp8074_1;
--create table dw_version_cell_comp8074_1 as
truncate table dw_version_cell_comp8074_1;
insert into dw_version_cell_comp8074_1(YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select extract(year from dt.datevalue) as yearstartdate, 
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid,
    max(f.dd_abc_class) as dd_abc_class,
	max(f.dd_localbrand) as dd_localbrand, 
	max(f.dd_itemlocalcode) as dd_itemlocalcode, 
	max(f.dd_model) as dd_model, 
	max(f.dd_fcstlevel) as dd_fcstlevel, 
	max(f.dd_plannig_item_flag)as dd_plannig_item_flag,
	max(f.DFULIFECYCLE) as DD_DFULIFECYCLE, 
	max(f.GAUSSITEMCODE) as DD_GAUSSITEMCODE, 
	max(f.dfuclass) as dd_dfuclass, 	
	max(f.SYSTBIAS3M) as dd_SYSTBIAS3M, 
	max(f.SYSTBIAS6M) as dd_SYSTBIAS6M, 
	max(f.SEGMENTATION) as dd_SEGMENTATION
from tmp_prefact_demand f, dim_date dt
where dt.dim_dateid 			= f.dim_dateidstartdate
	and f.dd_type = 'Consensus Forecast QTY'
group by extract(year from datevalue),
	f.dd_planning_item_id, 
	f.dim_jda_locationid, 
	f.dim_jda_productid;

--drop table if exists dw_version_cell_comp8074_2;
--create table dw_version_cell_comp8074_2 as
truncate table dw_version_cell_comp8074_2;
insert into dw_version_cell_comp8074_2(DIM_DATEID,YEARSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,DD_DFUCLASS,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_SEGMENTATION)
select 
	dim_Dateid,
	f.*
from dw_version_cell_comp8074_1 f, dim_date
	where calendaryear = yearstartdate
	and companycode = 'Not Set'
	and DAYSINMONTHSOFAR = 1;

--drop table if exists dw_version_cell_comp8074_3;
--create table dw_version_cell_comp8074_3 as
truncate table dw_version_cell_comp8074_3;
insert into dw_version_cell_comp8074_3(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select dw2.dim_dateid as dim_dateidstartdate, 
		dw2.dd_planning_item_id, dw2.dim_jda_locationid,dw2.dim_jda_productid, 
			'Consensus Forecast (in Qty) Cum' as dd_type, 
			ifnull(dw1.dd_abc_class,dw2.dd_abc_class) as dd_abc_class,
			ifnull(dw1.dd_localbrand,dw2.dd_localbrand) as dd_localbrand,
			ifnull(dw1.dd_itemlocalcode,dw2.dd_itemlocalcode) as dd_itemlocalcode,
			ifnull(dw1.dd_model,dw2.dd_model) as dd_model,
			ifnull(dw1.dd_fcstlevel,dw2.dd_fcstlevel) as dd_fcstlevel, 
			ifnull(dw1.dd_plannig_item_flag,dw2.dd_plannig_item_flag) as dd_plannig_item_flag,
			(ifnull(dw1.ct_packs,0)) as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			ifnull(dw1.DFULIFECYCLE,dw2.DD_DFULIFECYCLE) as DD_DFULIFECYCLE, 
			ifnull(dw1.GAUSSITEMCODE,dw2.DD_GAUSSITEMCODE) as DD_GAUSSITEMCODE,  
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			ifnull(dw1.dfuclass,dw2.dd_dfuclass) as dd_dfuclass, 
			(select a.dim_jda_componentid from dim_jda_component a where a.component_id = 8074) as dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			ifnull(dw1.SYSTBIAS3M,dw2.dd_SYSTBIAS3M) as dd_SYSTBIAS3M, 
			ifnull(dw1.SYSTBIAS6M,dw2.dd_SYSTBIAS6M) as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			ifnull(dw1.SEGMENTATION,dw2.dd_SEGMENTATION) as dd_SEGMENTATION
from dw_version_cell_comp8074_2	dw2
left join tmp_prefact_demand dw1
	on	dw1.dim_dateidstartdate = dw2.dim_dateid 
	and dw1.dd_planning_item_id = dw2.dd_planning_item_id
	and	dw1.dim_jda_locationid = dw2.dim_jda_locationid 
	and	dw1.dim_jda_productid = dw2.dim_jda_productid
	and dw1.dd_type = 'Consensus Forecast QTY';

--drop table if exists dw_version_cell_comp8074;
--create table dw_version_cell_comp8074 as
truncate table dw_version_cell_comp8074;
insert into dw_version_cell_comp8074(DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DD_TYPE,DD_ABC_CLASS,DD_LOCALBRAND,
		DD_ITEMLOCALCODE,DD_MODEL,DD_FCSTLEVEL,DD_PLANNIG_ITEM_FLAG,CT_PACKS,CT_UNITS,CT_EQ_UNIT,CT_MCG,CT_IU,DD_DFULIFECYCLE,DD_GAUSSITEMCODE,
		CT_AVGSALESPMONTH,CT_AVGFCST,CT_OP_EURO,DD_DFUCLASS,DIM_JDA_COMPONENTID,CT_PRICE,CT_CURRRATE,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVG_MKTSFA_12M,
		CT_AVG_STATSFA_12M,CT_STATSFA,CT_MKTSFA,DD_SYSTBIAS3M,DD_SYSTBIAS6M,CT_VOLATILITY,DD_SEGMENTATION)
select 		f2.dim_dateidstartdate, 
			f2.dd_planning_item_id, 
			f2.dim_jda_locationid, 
			f2.dim_jda_productid, 
			f2.dd_type, 
			f2.dd_abc_class as dd_abc_class,
			f2.dd_localbrand as dd_localbrand, 
			f2.dd_itemlocalcode as dd_itemlocalcode, 
			f2.dd_model as dd_model, 
			f2.dd_fcstlevel as dd_fcstlevel, 
			f2.dd_plannig_item_flag as dd_plannig_item_flag, 
			sum(ct_packs) over(partition by date_trunc('YEAR', dt.datevalue),dd_planning_item_id, dim_jda_locationid, dim_jda_productid order by dt.datevalue)  as ct_packs,
			0 as ct_units,	
			0 as ct_eq_unit, 
			0 as ct_mcg,	
			0 as ct_iu,
			f2.DD_DFULIFECYCLE as DD_DFULIFECYCLE, 
			f2.DD_GAUSSITEMCODE as DD_GAUSSITEMCODE, 
			0 as CT_AVGSALESPMONTH, 
			0 as CT_AVGFCST, 
			0 as CT_OP_EURO,
			f2.dd_dfuclass as dd_dfuclass, 
			f2.dim_jda_componentid,
			0 as ct_price, 
			0 as ct_currRate,
			0 as ct_AVG3M_BIAS1M, 
			0 as ct_AVG6M_BIAS1M, 
			0 as ct_AVG_MKTSFA_12M, 
			0 as ct_AVG_STATSFA_12M, 
			0 as ct_STATSFA, 
			0 as ct_MKTSFA, 
			f2.dd_SYSTBIAS3M as dd_SYSTBIAS3M, 
			f2.dd_SYSTBIAS6M as dd_SYSTBIAS6M, 
			0 as ct_VOLATILITY, 
			f2.dd_SEGMENTATION as dd_SEGMENTATION
from dw_version_cell_comp8074_3 f2, dim_Date dt
where dt.dim_dateid 			= f2.dim_dateidstartdate;

insert into tmp_prefact_demand (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DFULIFECYCLE, GAUSSITEMCODE, AVGSALESPMONTH, AVGFCST, OP_EURO, dfuclass, dim_jda_componentid,
									AVG3M_BIAS1M, AVG6M_BIAS1M, AVG_MKTSFA_12M, AVG_STATSFA_12M, STATSFA, MKTSFA, SYSTBIAS3M, SYSTBIAS6M, VOLATILITY, SEGMENTATION, startdate)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = ah.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
			ah.dim_dateidstartdate
		   ,ah.dd_planning_item_id
		   ,ah.dim_jda_locationid
		   ,ah.dim_jda_productid
		   ,ah.dd_type
		   ,ah.dd_abc_class, ah.dd_localbrand, ah.dd_itemlocalcode, ah.dd_model, ah.dd_fcstlevel, ah.dd_plannig_item_flag  /* coming from planning_item */
		   ,ah.ct_packs 							AS packs
			 ,ah.ct_packs * pr.packsize 				    AS units
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.brandtosize AS eq_unit
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize)) * pr.dosetomcg   AS mcg
			 ,convert(decimal (30,9),(ah.ct_packs * pr.packsize ))* pr.dosetoiu    AS iu
		   ,ah.dd_DFULIFECYCLE, ah.dd_GAUSSITEMCODE, ah.CT_AVGSALESPMONTH, ah.CT_AVGFCST, ah.CT_OP_EURO, ah.dd_dfuclass
		   ,ah.dim_jda_componentid
		   ,ah.ct_AVG3M_BIAS1M, ah.ct_AVG6M_BIAS1M, ah.ct_AVG_MKTSFA_12M, ah.ct_AVG_STATSFA_12M, ah.ct_STATSFA, ah.ct_MKTSFA
		   ,ah.dd_SYSTBIAS3M, ah.dd_SYSTBIAS6M, ah.ct_VOLATILITY, ah.dd_SEGMENTATION,dc.datevalue as startdate
	from dw_version_cell_comp8074 ah, dim_date dc, dim_jda_product pr
	where dc.dim_dateid = ah.dim_dateidstartdate
		and ah.dim_jda_productid = pr.dim_jda_productid;
/* END Cumulative values */
/* END JDA Demand for IBP - Calculated Measures - Oana */

/* fact_jda_demandforecast populating */
	delete from fact_jda_demandforecast;
	
	delete from number_fountain m where m.table_name = 'fact_jda_demandforecast';
	insert into number_fountain
	select 'fact_jda_demandforecast', ifnull(max(f.fact_jda_demandforecastid ), 
									 ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from fact_jda_demandforecast f;
	 
	insert into fact_jda_demandforecast(
									fact_jda_demandforecastid, dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu,
									DD_DFULIFECYCLE, DD_GAUSSITEMCODE, CT_AVGSALESPMONTH, CT_AVGFCST, CT_OP_EURO, dd_dfuclass, dim_jda_componentid,
									dim_jda_dateholderid, ct_price, ct_currRate,
									ct_AVG3M_BIAS1M, ct_AVG6M_BIAS1M, ct_AVG_MKTSFA_12M, ct_AVG_STATSFA_12M, ct_STATSFA, ct_MKTSFA, dd_SYSTBIAS3M, dd_SYSTBIAS6M, ct_VOLATILITY, dd_SEGMENTATION)
	select (select max_id from number_fountain where table_name = 'fact_jda_demandforecast') + row_number() over(order by '') as fact_jda_demandforecastid 
		   ,ifnull(dim_dateidstartdate, 1) 			AS dim_dateidstartdate
		   ,ifnull(dd_planning_item_id, -999999)	AS dd_planning_item_id
		   ,ifnull(dim_jda_locationid, 1)			AS dim_jda_locationid
		   ,ifnull(dim_jda_productid, 1)			AS dim_jda_productid 
		   ,ifnull(dd_type, 'Not Set') 				AS dd_type
		   ,ifnull(dd_abc_class, 'Not Set') 		AS dd_abc_class
		   ,ifnull(dd_localbrand, 'Not Set') 		AS dd_localbrand
		   ,ifnull(dd_itemlocalcode, 'Not Set') 	AS dd_itemlocalcode
		   ,ifnull(dd_model, 'Not Set') 			AS dd_model
		   ,ifnull(dd_fcstlevel, 'Not Set') 		AS dd_fcstlevel
		   ,ifnull(dd_plannig_item_flag, 'Not Set') AS dd_plannig_item_flag 
		   ,ifnull(ct_packs, 0)		AS ct_packs
		   ,ifnull(ct_units, 0)		AS ct_units
		   ,ifnull(ct_eq_unit, 0)	AS ct_eq_unit
		   ,ifnull(ct_mcg, 0)		AS ct_mcg
		   ,ifnull(ct_iu, 0)		AS ct_iu
		   ,ifnull(DFULIFECYCLE, 'Not Set')		as DD_DFULIFECYCLE
		   ,ifnull(GAUSSITEMCODE, 'Not Set')	as DD_GAUSSITEMCODE
		   ,ifnull(AVGSALESPMONTH, 0.00)	as CT_AVGSALESPMONTH 
		   ,ifnull(AVGFCST, 0.00)			as CT_AVGFCST
		   ,ifnull(OP_EURO, 0.00)			as CT_OP_EURO
		   ,ifnull(dfuclass, 'Not Set') 	as dd_dfuclass
		   ,ifnull(dim_jda_componentid, 1)  as dim_jda_componentid
		   ,10000000001 as dim_jda_dateholderid
		   ,ifnull(ct_price, 0.00)	  as ct_price
		   ,ifnull(ct_currRate, 0.00) as ct_currRate
		   ,ifnull(AVG3M_BIAS1M, 0.00) 		as ct_AVG3M_BIAS1M  
		   ,ifnull(AVG6M_BIAS1M, 0.00) 		as ct_AVG6M_BIAS1M  
		   ,ifnull(AVG_MKTSFA_12M, 0.00) 	as ct_AVG_MKTSFA_12M
		   ,ifnull(AVG_STATSFA_12M, 0.00) 	as ct_AVG_STATSFA_12M
		   ,ifnull(STATSFA, 0.00) 			as ct_STATSFA
		   ,ifnull(MKTSFA, 0.00) 			as ct_MKTSFA
		   ,ifnull(SYSTBIAS3M, 'Not Set') 	as dd_SYSTBIAS3M
		   ,ifnull(SYSTBIAS6M, 'Not Set') 	as dd_SYSTBIAS6M
		   ,ifnull(VOLATILITY, 0.00) 		as ct_VOLATILITY
		   ,ifnull(SEGMENTATION, 'Not Set') as dd_SEGMENTATION
	from tmp_prefact_demand ds;
	
/* build additional helping fields */	
	/* TYPE_2 */
	update fact_jda_demandforecast t
	set dd_type_2 = case when dd_type = 'ADJHIST' OR dd_type = 'MKTFCST' OR dd_type = 'TENDER'
							then 'FCST'
				 		 else dd_type end
	where dd_type_2 <> case when dd_type = 'ADJHIST' OR dd_type = 'MKTFCST' OR dd_type = 'TENDER'
							  then 'FCST'
				 		    else dd_type end;

	/* TYPE_3 */
	update fact_jda_demandforecast t
	set dd_type_3 = case when dd_type = 'ADJHIST' OR dd_type = 'FCST_LAG0' OR dd_type = 'FCST_LAG1'
							then 'FCST'
						 when dd_type = 'OPERATING PLAN'
						 	then 'OP'
						 when dd_type = 'OPERATING PLAN EURO'
						 	then 'OP EURO'
						 else 'Not Set' end
	where dd_type_3 <> case when dd_type = 'ADJHIST' OR dd_type = 'FCST_LAG0' OR dd_type = 'FCST_LAG1'
							then 'FCST'
						 when dd_type = 'OPERATING PLAN'
						 	then 'OP'
						 when dd_type = 'OPERATING PLAN EURO'
						 	then 'OP EURO'
						 else 'Not Set' end;
				 		    	
	/* PACKS_COR */
	update fact_jda_demandforecast t
	set t.ct_packs_cor = case when upper(p.container) = 'BULK' OR upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
							    then 0
				 			  else t.ct_packs end
    from fact_jda_demandforecast t,dim_jda_product p
	where     t.dim_jda_productid = p.dim_jda_productid
		  and t.ct_packs_cor <> case when upper(p.container) = 'BULK' OR upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
							  		   then 0
				 					 else t.ct_packs end;

	/* UNITS_COR */
	update fact_jda_demandforecast t
	set t.ct_units_cor = case when upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
							    then 0
				 			  else t.ct_units end
	from fact_jda_demandforecast t,dim_jda_product p
	where     t.dim_jda_productid = p.dim_jda_productid
		  and t.ct_units_cor <> case when upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
							  		   then 0
				 					 else t.ct_units end;		  

	/* LAG0_PACKS_COR */
	drop table if exists tmp_jda_lag0_pack_1;
	create table tmp_jda_lag0_pack_1 as 
	select case when (t.dd_type = 'ADJHIST' AND dds.datevalue >= dh.lag0_month) OR t.dd_type = 'FCST_LAG1' 
				  then 0
				when upper(p.container) = 'BULK' OR upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
				  then 0
				else t.ct_packs 
		   end as lag0_packs_cor
		   ,t.fact_jda_demandforecastid	
	from fact_jda_demandforecast t,
		 dim_date dds,
		 dim_jda_product p,
		 tmp_jda_dateholder dh
	where     t.dim_dateidstartdate = dds.dim_dateid
		  and t.dim_jda_productid = p.dim_jda_productid;
	
	update fact_jda_demandforecast t
	set t.ct_lag0_packs_cor = ds.lag0_packs_cor
	from fact_jda_demandforecast t,tmp_jda_lag0_pack_1 ds
	where     t.fact_jda_demandforecastid = ds.fact_jda_demandforecastid
		  and t.ct_lag0_packs_cor <> ds.lag0_packs_cor;
	
	drop table if exists tmp_jda_lag0_pack_1;
	
	/* LAG0_UNITS_COR */
	drop table if exists tmp_jda_lag0_unit_1;
	create table tmp_jda_lag0_unit_1 as 
	select case when (t.dd_type = 'ADJHIST' AND dds.datevalue >= dh.lag0_month) OR t.dd_type = 'FCST_LAG1' 
				  then 0
				when upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
				  then 0
				else t.ct_units 
		   end as lag0_units_cor
		   ,t.fact_jda_demandforecastid	
	from fact_jda_demandforecast t,
		 dim_date dds,
		 dim_jda_product p,
		 tmp_jda_dateholder dh
	where     t.dim_dateidstartdate = dds.dim_dateid
		  and t.dim_jda_productid = p.dim_jda_productid;
	
	update fact_jda_demandforecast t
	set t.ct_lag0_units_cor = ds.lag0_units_cor
	from fact_jda_demandforecast t,tmp_jda_lag0_unit_1 ds
	where     t.fact_jda_demandforecastid = ds.fact_jda_demandforecastid
		  and t.ct_lag0_units_cor <> ds.lag0_units_cor;
	
	drop table if exists tmp_jda_lag0_unit_1;	

	/* LAG0_EQ_UNIT */
	drop table if exists tmp_jda_lag0_eq_unit_1;
	create table tmp_jda_lag0_eq_unit_1 as 
	select case when (t.dd_type = 'ADJHIST' AND dds.datevalue >= dh.lag0_month) OR t.dd_type = 'FCST_LAG1' 
				  then 0
				-- when upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
				--  then 0
				else t.ct_eq_unit 
		   end as lag0_eq_unit
		   ,t.fact_jda_demandforecastid	
	from fact_jda_demandforecast t,
		 dim_date dds,
		 dim_jda_product p,
		 tmp_jda_dateholder dh
	where     t.dim_dateidstartdate = dds.dim_dateid
		  and t.dim_jda_productid = p.dim_jda_productid;
	
	update fact_jda_demandforecast t
	set t.ct_lag0_eq_unit = ds.lag0_eq_unit
	from  fact_jda_demandforecast t,tmp_jda_lag0_eq_unit_1 ds
	where     t.fact_jda_demandforecastid = ds.fact_jda_demandforecastid
		  and t.ct_lag0_eq_unit <> ds.lag0_eq_unit;
	
	drop table if exists tmp_jda_lag0_eq_unit_1;	
	
	/* LAG1_PACKS_COR */
	drop table if exists tmp_jda_lag1_pack_1;
	create table tmp_jda_lag1_pack_1 as 
	select case when (t.dd_type = 'ADJHIST' AND dds.datevalue >= dh.lag1_month) OR t.dd_type = 'FCST_LAG0' 
				  then 0
				when upper(p.container) = 'BULK' OR upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
				  then 0
				else t.ct_packs 
		   end as lag1_packs_cor
		   ,t.fact_jda_demandforecastid	
	from fact_jda_demandforecast t,
		 dim_date dds,
		 dim_jda_product p,
		 tmp_jda_dateholder dh
	where     t.dim_dateidstartdate = dds.dim_dateid
		  and t.dim_jda_productid = p.dim_jda_productid;
	
	update fact_jda_demandforecast t
	set t.ct_lag1_packs_cor = ds.lag1_packs_cor
	from fact_jda_demandforecast t,tmp_jda_lag1_pack_1 ds
	where     t.fact_jda_demandforecastid = ds.fact_jda_demandforecastid
		  and t.ct_lag1_packs_cor <> ds.lag1_packs_cor;
	
	drop table if exists tmp_jda_lag1_pack_1;
	
	/* LAG1_UNITS_COR */
	drop table if exists tmp_jda_lag1_unit_1;
	create table tmp_jda_lag1_unit_1 as 
	select case when (t.dd_type = 'ADJHIST' AND dds.datevalue >= dh.lag1_month) OR t.dd_type = 'FCST_LAG0' 
				  then 0
				when upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
				  then 0
				else t.ct_units 
		   end as lag1_units_cor
		   ,t.fact_jda_demandforecastid	
	from fact_jda_demandforecast t,
		 dim_date dds,
		 dim_jda_product p,
		 tmp_jda_dateholder dh
	where     t.dim_dateidstartdate = dds.dim_dateid
		  and t.dim_jda_productid = p.dim_jda_productid;
	
	update fact_jda_demandforecast t
	set t.ct_lag1_units_cor = ds.lag1_units_cor
	from fact_jda_demandforecast t,tmp_jda_lag1_unit_1 ds
	where     t.fact_jda_demandforecastid = ds.fact_jda_demandforecastid
		  and t.ct_lag1_units_cor <> ds.lag1_units_cor;
	
	drop table if exists tmp_jda_lag1_unit_1;
	
	/* LAG1_EQ_UNIT */
	drop table if exists tmp_jda_lag1_eq_unit_1;
	create table tmp_jda_lag1_eq_unit_1 as 
	select case when (t.dd_type = 'ADJHIST' AND dds.datevalue >= dh.lag1_month) OR t.dd_type = 'FCST_LAG0' 
				  then 0
				--when upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
				--  then 0
				else t.ct_eq_unit
		   end as lag1_eq_unit
		   ,t.fact_jda_demandforecastid	
	from fact_jda_demandforecast t,
		 dim_date dds,
		 dim_jda_product p,
		 tmp_jda_dateholder dh
	where     t.dim_dateidstartdate = dds.dim_dateid
		  and t.dim_jda_productid = p.dim_jda_productid;
	
	update fact_jda_demandforecast t
	set t.ct_lag1_eq_unit = ds.lag1_eq_unit
	from fact_jda_demandforecast t,tmp_jda_lag1_eq_unit_1 ds
	where     t.fact_jda_demandforecastid = ds.fact_jda_demandforecastid
		  and t.ct_lag1_eq_unit <> ds.lag1_eq_unit;
	
	drop table if exists tmp_jda_lag1_eq_unit_1;
	drop table if exists tmp_STSC_DFUVIEW_group;

	
/* LAG0 and LAG1 custom calculation */
drop table if exists tmp_lag0lag1;
create table tmp_lag0lag1 as 

select current_date	 			  as currentdate,
      max(dd2.datevalue) as thisweeklastsaturday,
      max(dd3.datevalue) as prevmonthlastsaturday,
      max(dd4.datevalue) as prevlast2monthsaturday
from dim_date dd1,  dim_date dd2,dim_date dd3,dim_date dd4
where   dd1.datevalue = current_date	
    and dd1.companycode = 'Not Set'
    and dd2.companycode = 'Not Set'
    and dd3.companycode = 'Not Set'
    and dd4.companycode = 'Not Set' 
    and dd3.weekdaynumber = 7
    and dd2.weekdaynumber = 7
    and dd4.weekdaynumber = 7   
    and dd1.calendarmonthid = dd2.calendarmonthid 
    and dd1.calendarmonthid = dd3.calendarmonthid + 1 
    and dd1.calendarmonthid = dd4.calendarmonthid + 2
group by dd1.weekdaynumber;


insert into tmp_lag0lag1
select current_date	 			  as currentdate,
      max(dd2.datevalue) as thisweeklastsaturday,
      max(dd3.datevalue) as prevmonthlastsaturday,
      max(dd4.datevalue) as prevlast2monthsaturday
from dim_date dd1,  dim_date dd2,dim_date dd3,dim_date dd4
where   dd1.datevalue = current_date
    and dd1.companycode = 'Not Set'
    and dd2.companycode = 'Not Set'
    and dd3.companycode = 'Not Set'
    and dd4.companycode = 'Not Set' 
    and dd3.weekdaynumber = 7
    and dd2.weekdaynumber = 7
    and dd4.weekdaynumber = 7   
    and dd1.calendarmonthid = dd2.calendarmonthid 
    and dd1.calendarmonthid = dd3.calendarmonthid + 89 
    and dd1.calendarmonthid = dd4.calendarmonthid + 90;

insert into tmp_lag0lag1
select current_date	 			  as currentdate,
      max(dd2.datevalue) as thisweeklastsaturday,
      max(dd3.datevalue) as prevmonthlastsaturday,
      max(dd4.datevalue) as prevlast2monthsaturday
from dim_date dd1,  dim_date dd2,dim_date dd3,dim_date dd4
where   dd1.datevalue = current_date
    and dd1.companycode = 'Not Set'
    and dd2.companycode = 'Not Set'
    and dd3.companycode = 'Not Set'
    and dd4.companycode = 'Not Set' 
    and dd3.weekdaynumber = 7
    and dd2.weekdaynumber = 7
    and dd4.weekdaynumber = 7   
    and dd1.calendarmonthid = dd2.calendarmonthid 
    and dd1.calendarmonthid = dd3.calendarmonthid + 90 
    and dd1.calendarmonthid = dd4.calendarmonthid + 91;

delete from  tmp_lag0lag1 where thisweeklastsaturday is null; 


drop table if exists tmp_lag0lag1final;
create table tmp_lag0lag1final as 
select 
currentdate,
case when currentdate > thisweeklastsaturday then thisweeklastsaturday
     else  prevmonthlastsaturday end as LastSaturdayCPM,
case when currentdate > thisweeklastsaturday then prevmonthlastsaturday
     else  prevlast2monthsaturday end as ndLastSaturdayCPM
from tmp_lag0lag1;


update fact_jda_demandforecast
 set dd_currentdate = currentdate
 from fact_jda_demandforecast,(select max(currentdate) currentdate from tmp_lag0lag1final);

update fact_jda_demandforecast
set dd_LastSaturdayCPM = LastSaturdayCPM
from fact_jda_demandforecast,(select max(LastSaturdayCPM) LastSaturdayCPM from tmp_lag0lag1final);

update fact_jda_demandforecast
set dd_ndLastSaturdayCPM = ndLastSaturdayCPM
from fact_jda_demandforecast,(select max(ndLastSaturdayCPM) ndLastSaturdayCPM from tmp_lag0lag1final);

DROP TABLE IF EXISTS tmp_date_lag;
CREATE TABLE tmp_date_lag AS
SELECT d.dim_dateid from dim_jda_dateholder jd, dim_date d
where jd.lag0_month=d.datevalue
and d.companycode='Not Set';

/*update fact_jda_demandforecast f
set f.dim_lag0_monthid  = d.dim_dateid
from  dim_date d, dim_jda_dateholder jd,fact_jda_demandforecast f
where 
 jd.lag0_month = d.datevalue
and d.companycode = 'Not Set'*/

update fact_jda_demandforecast f
set f.dim_lag0_monthid  = d.dim_dateid
from  fact_jda_demandforecast f,tmp_date_lag d;

DROP TABLE IF EXISTS tmp_date_lag1;
CREATE TABLE tmp_date_lag1 AS
SELECT d.dim_dateid from dim_jda_dateholder jd, dim_date d
where jd.lag1_month=d.datevalue
and d.companycode='Not Set';

/*update fact_jda_demandforecast f
set f.dim_lag1_monthid  = d.dim_dateid
from  dim_date d, dim_jda_dateholder jd,fact_jda_demandforecast f
where 
 jd.lag1_month = d.datevalue
and d.companycode = 'Not Set'*/

update fact_jda_demandforecast f
set f.dim_lag1_monthid  = d.dim_dateid
from  fact_jda_demandforecast f,tmp_date_lag1 d;

DROP TABLE IF EXISTS tmp_date_hold;
CREATE TABLE tmp_date_hold AS
SELECT d.dim_dateid from dim_jda_dateholder jd, dim_date d
where jd.currentdate=d.datevalue
and d.companycode='Not Set';

/*update fact_jda_demandforecast f
set f.dim_dateholderid  = d.dim_dateid
from  dim_date d, dim_jda_dateholder jd,fact_jda_demandforecast f
where 
 jd.currentdate = d.datevalue
and d.companycode = 'Not Set'*/

update fact_jda_demandforecast f
set f.dim_dateholderid  = d.dim_dateid
from fact_jda_demandforecast f,tmp_date_hold d;


update fact_jda_demandforecast f
set f.dim_ndlastsaturdaycpmid = d.dim_dateid
from dim_date d,fact_jda_demandforecast f
   where f.dd_ndlastsaturdaycpm = d.datevalue
   and   d.companycode = 'Not Set';
   
   
update fact_jda_demandforecast f
set f.dim_lastsaturdaycpmid = d.dim_dateid
from dim_date d,fact_jda_demandforecast f
   where f.dd_lastsaturdaycpm = d.datevalue
   and   d.companycode = 'Not Set';
	

/*Prod Location logic */	
drop table if exists tmp_prodlocation;
create table tmp_prodlocation as 
select 
d_loc.region,
fjd.dd_segmentation,
d_prd.product_name,
d_loc.location_name,
CASE 
     WHEN Avg( ct_avg_statsfa_12m * 100 ) >  Avg( ct_avg_mktsfa_12m * 100 )
                                             THEN  1  ELSE 0 
                                             END ct_prodlocation
from fact_jda_demandforecast AS fjd
 INNER JOIN dim_jda_location AS d_loc  ON fjd.dim_jda_locationid =  d_loc.dim_jda_locationid 
 INNER JOIN dim_jda_product AS d_prd  ON fjd.dim_jda_productid =  d_prd.dim_jda_productid 
group by d_loc.region,
fjd.dd_segmentation,
d_prd.product_name,
d_loc.location_name;


drop table if exists tmp2_prodlocation;
create table tmp2_prodlocation
as select  
region,
dd_segmentation,
sum(ct_prodlocation) as ct_prodlocation
from tmp_prodlocation
group by region,
dd_segmentation;




update  fact_jda_demandforecast f
set f.ct_prodlocation = tmp.ct_prodlocation
from fact_jda_demandforecast f , tmp2_prodlocation tmp,dim_jda_location  d_loc
where 
f.dim_jda_locationid =  d_loc.dim_jda_locationid and
d_loc.region = tmp.region and
f.dd_segmentation = tmp.dd_segmentation;

	
	

	
/* ******************************************************************************************************************************** */

/* FORECAST EVOLUTION FACT - separate processing */
	/* generate last 12 months including current month - WD */
	/* generate all months starting with January of prev year and till 2 years in advance - CD */
	/* cross join 2 tables, which will result for each workdate will have 12 cycledate dates */
	drop table if exists tmp_jda_workdate_cycledate;
	create table tmp_jda_workdate_cycledate as 
	select wd.workdate, cd.cycledate
	from (
			select distinct monthstartdate as workdate 
			from dim_date d
			where d.datevalue between date_trunc('YEAR', add_months(current_date,-12)) and  add_months(current_date,24)	
		 ) wd,
		 (
		 	select distinct monthstartdate as cycledate 
			from dim_date d
			where d.datevalue between add_months(current_date,-11) and current_date
		 ) cd
	order by wd.workdate, cd.cycledate;
	
	/* FORECAST EVOLUTION EQUI */
	drop table if exists dw_version_cell_fcst_evol_equi;
	create table dw_version_cell_fcst_evol_equi as
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,woc.cycledate
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,'FCST' AS TYPE	/* 'FCST EQUI' initially it was conevrted to FCST on UI level, but due to dummy rows it was applied here */
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 NWMGR_COMPONENT nc,
		 tmp_jda_workdate_cycledate woc,
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and pi.fcstlevel IN ('111', '711', '141')	  
		  and vc.nwmgr_cell_start_date = woc.workdate
		  AND woc.cycledate <= woc.workdate
		  AND woc.cycledate = TRUNC(dh.currentdate, 'MM')
		  and (nc.nwmgr_component_description like 'MKTFCST%' OR nc.nwmgr_component_description like 'TENDER%')
	group by vc.nwmgr_cell_start_date, woc.cycledate, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, 
		     pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag;	

	/* FORECAST EVOLUTION HIST */
	drop table if exists dw_version_cell_fcst_evol_hist;
	create table dw_version_cell_fcst_evol_hist as
	select  sum(vc.nwmgr_cell_quantity) as packs
		   ,vc.nwmgr_cell_start_date
		   ,woc.cycledate
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,'HIST' AS TYPE	
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag
	from dw_version_cell vc,
		 tmp_dw_planningitem pi, 
		 NWMGR_COMPONENT nc,
		 tmp_jda_workdate_cycledate woc,
		 tmp_jda_dateholder dh
	where     vc.nwmgr_version_planning_item_id = pi.planning_item_id
		  and vc.nwmgr_version_component_id     = nc.nwmgr_component_component_id
		  and vc.nwmgr_version_version_number = 0
		  and vc.nwmgr_version_enterprise_id = 1
		  and pi.fcstlevel IN ('111', '711', '141')	  
		  and vc.nwmgr_cell_start_date = woc.workdate
		  AND woc.cycledate > woc.workdate
		  and nc.nwmgr_component_name = 'Adj Hist'
	group by vc.nwmgr_cell_start_date, woc.cycledate, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, 
		     pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag;			 

	/* FORECAST EVOLUTION FCST */
	drop table if exists tmp_dw_histfcst_fcst_evol_fcst;
	create table tmp_dw_histfcst_fcst_evol_fcst as
	select  sum(sh.stsc_histfcst_basefcst + sh.stsc_histfcst_nonbasefcst) as packs
		   ,sh.stsc_histfcst_startdate
		   ,woc.cycledate
		   ,pi.planning_item_id
		   ,pi.dim_jda_locationid
		   ,pi.dim_jda_productid
		   ,'FCST' AS TYPE
		   ,pi.abc_class
		   ,pi.localbrand
		   ,pi.itemlocalcode
		   ,pi.model
		   ,pi.fcstlevel
		   ,pi.plannig_item_flag			   
	from stsc_histfcst sh,
		 tmp_dw_planningitem_histfcst pi,
		 tmp_jda_workdate_cycledate woc,
		 tmp_jda_dateholder dh
	where     sh.stsc_histfcst_dmdgroup  = pi.pi_dmdgroup
		  and sh.stsc_histfcst_dmdunit   = pi.product_name
		  and sh.stsc_histfcst_loc       = pi.location_name
		  and sh.stsc_histfcst_fcstdate  = woc.cycledate
		  and sh.stsc_histfcst_startdate = woc.workdate
		  and woc.workdate >= woc.cycledate
		  and woc.cycledate <> dh.currentdate
		  AND pi.fcstlevel IN ('111', '711', '141')
		  and pi.enterprise_id = 1
	group by sh.stsc_histfcst_startdate, woc.cycledate, pi.planning_item_id, pi.dim_jda_locationid, pi.dim_jda_productid, 
		     pi.abc_class, pi.localbrand, pi.itemlocalcode, pi.model, pi.fcstlevel, pi.plannig_item_flag;	

/* UNION ALL tmp tables with their specific header */
	drop table if exists tmp_prefact_demand_fe;
	create table tmp_prefact_demand_fe(dim_dateidstartdate bigint, dd_planning_item_id integer, dim_jda_locationid bigint, dim_jda_productid bigint, dd_type varchar(50), dd_abc_class varchar(50),
									dd_localbrand varchar(100), dd_itemlocalcode varchar(50), dd_model varchar(18), dd_fcstlevel varchar(50), dd_plannig_item_flag varchar(7),
									ct_packs bigint, ct_units decimal(30,9), ct_eq_unit decimal(30,9), ct_mcg decimal(30,9), ct_iu decimal(30,9), dim_dateidcycledate bigint, startdate timestamp, cycledates timestamp);
	delete from  tmp_prefact_demand_fe;
	
	/* Forecast Evolution EQUI */
	insert into tmp_prefact_demand_fe (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									   dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									   ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu, dim_dateidcycledate,startdate,cycledates)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = stf.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		   1 as dim_dateidstartdate
		   ,stf.planning_item_id
		   ,stf.dim_jda_locationid
		   ,stf.dim_jda_productid
		   ,stf.TYPE
		   ,stf.abc_class, stf.localbrand, stf.itemlocalcode, stf.model, stf.fcstlevel, stf.plannig_item_flag /* coming from planning_item */
		   ,stf.packs 					  AS packs
		   ,stf.packs * pr.uda_boxtodoses AS units 
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_sizetobrand AS eq_unit 
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_dosetomcg   AS mcg
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_dosetoiu    AS iu
		   -- ,ifnull((select dim_dateid from dim_date where datevalue = stf.cycledate and companycode = 'Not Set'),1) dim_dateidcycledate
		   ,1 as dim_dateidcycledate
		   ,stf.nwmgr_cell_start_date as startdate
		   ,stf.cycledate as cycledates
	from dw_version_cell_fcst_evol_equi stf,
		 dim_jda_product pr
	where stf.dim_jda_productid = pr.dim_jda_productid;		
	
	/* Forecast Evolution HIST */
	insert into tmp_prefact_demand_fe (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									   dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									   ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu, dim_dateidcycledate,startdate,cycledates)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = stf.nwmgr_cell_start_date and companycode = 'Not Set'),1) dim_dateidstartdate
		    1 as dim_dateidstartdate
		   ,stf.planning_item_id
		   ,stf.dim_jda_locationid
		   ,stf.dim_jda_productid
		   ,stf.TYPE
		   ,stf.abc_class, stf.localbrand, stf.itemlocalcode, stf.model, stf.fcstlevel, stf.plannig_item_flag /* coming from planning_item */
		   ,stf.packs 					  AS packs
		   ,stf.packs * pr.uda_boxtodoses AS units 
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_sizetobrand AS eq_unit 
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_dosetomcg   AS mcg
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_dosetoiu    AS iu
		   -- ,ifnull((select dim_dateid from dim_date where datevalue = stf.cycledate and companycode = 'Not Set'),1) dim_dateidcycledate
		   ,1 as dim_dateidcycledate
		   ,stf.nwmgr_cell_start_date as startdate
		   ,stf.cycledate as cycledates
	from dw_version_cell_fcst_evol_hist stf,
		 dim_jda_product pr
	where stf.dim_jda_productid = pr.dim_jda_productid;			

	/* Forecast Evolution FCST */
	insert into tmp_prefact_demand_fe (dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									   dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									   ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu, dim_dateidcycledate,startdate,cycledates)
	select  -- ifnull((select dim_dateid from dim_date where datevalue = stf.stsc_histfcst_startdate and companycode = 'Not Set'),1) dim_dateidstartdate
		    1 as dim_dateidstartdate
		   ,stf.planning_item_id
		   ,stf.dim_jda_locationid
		   ,stf.dim_jda_productid
		   ,stf.TYPE
		   ,stf.abc_class, stf.localbrand, stf.itemlocalcode, stf.model, stf.fcstlevel, stf.plannig_item_flag /* coming from planning_item */
		   ,stf.packs 					  AS packs
		   ,stf.packs * pr.uda_boxtodoses AS units 
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_sizetobrand AS eq_unit 
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_dosetomcg   AS mcg
		   ,stf.packs * pr.uda_boxtodoses * pr.uda_dosetoiu    AS iu
		   --,ifnull((select dim_dateid from dim_date where datevalue = stf.cycledate and companycode = 'Not Set'),1) dim_dateidcycledate
		   ,1 as dim_dateidcycledate
		   ,stf.stsc_histfcst_startdate as startdate
		   ,stf.cycledate as cycledates
	from tmp_dw_histfcst_fcst_evol_fcst stf,
		 dim_jda_product pr
	where stf.dim_jda_productid = pr.dim_jda_productid;		
									   
									  
    update tmp_prefact_demand_fe tmp
	set tmp.dim_dateidstartdate = d.dim_dateid
	from tmp_prefact_demand_fe tmp, dim_date d 
	where d.datevalue = tmp.startdate  
	and d.companycode = 'Not Set';
	
	update tmp_prefact_demand_fe tmp
	set tmp.dim_dateidcycledate = d.dim_dateid
	from tmp_prefact_demand_fe tmp, dim_date d 
	where d.datevalue = tmp.cycledates  
	and d.companycode = 'Not Set';
									  
/* fact_jda_demandforecastevol populating */
	delete from  fact_jda_demandforecastevol;
	
	delete from number_fountain m where m.table_name = 'fact_jda_demandforecastevol';
	insert into number_fountain
	select 'fact_jda_demandforecastevol', ifnull(max(f.fact_jda_demandforecastevolid ), 
									 ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from fact_jda_demandforecastevol f;
	 
	insert into fact_jda_demandforecastevol(
									fact_jda_demandforecastevolid, dim_dateidstartdate, dim_dateidcycledate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, 
									dd_type, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel, dd_plannig_item_flag,
									ct_packs, ct_units,	ct_eq_unit, ct_mcg,	ct_iu)
	select (select max_id from number_fountain where table_name = 'fact_jda_demandforecastevol') + row_number() over(order by '') as fact_jda_demandforecastevolid 
		   ,ifnull(dim_dateidstartdate, 1) 			AS dim_dateidstartdate
		   ,ifnull(dim_dateidcycledate, 1)			AS dim_dateidcycledate
		   ,ifnull(dd_planning_item_id, -999999)	AS dd_planning_item_id
		   ,ifnull(dim_jda_locationid, 1)			AS dim_jda_locationid
		   ,ifnull(dim_jda_productid, 1)			AS dim_jda_productid 
		   ,ifnull(dd_type, 'Not Set') 				AS dd_type
		   ,ifnull(dd_abc_class, 'Not Set') 		AS dd_abc_class
		   ,ifnull(dd_localbrand, 'Not Set') 		AS dd_localbrand
		   ,ifnull(dd_itemlocalcode, 'Not Set') 	AS dd_itemlocalcode
		   ,ifnull(dd_model, 'Not Set') 			AS dd_model
		   ,ifnull(dd_fcstlevel, 'Not Set') 		AS dd_fcstlevel
		   ,ifnull(dd_plannig_item_flag, 'Not Set') AS dd_plannig_item_flag 
		   ,ifnull(ct_packs, 0)		AS ct_packs
		   ,ifnull(ct_units, 0)		AS ct_units
		   ,ifnull(ct_eq_unit, 0)	AS ct_eq_unit
		   ,ifnull(ct_mcg, 0)		AS ct_mcg
		   ,ifnull(ct_iu, 0)		AS ct_iu
	from tmp_prefact_demand_fe ds;									   
									   

   /* PACKS_COR in Evolution */
	update fact_jda_demandforecastevol t
	set t.ct_packs_cor = case when upper(p.container) = 'BULK' OR upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
							    then 0
				 			  else t.ct_packs end
	from fact_jda_demandforecastevol t,dim_jda_product p
	where     t.dim_jda_productid = p.dim_jda_productid
		  and t.ct_packs_cor <> case when upper(p.container) = 'BULK' OR upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
							  		   then 0
				 					 else t.ct_packs end;

	/* UNITS_COR  in Evolution */
	update fact_jda_demandforecastevol t
	set t.ct_units_cor = case when upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
							    then 0
				 			  else t.ct_units end
	from fact_jda_demandforecastevol t,dim_jda_product p
	where     t.dim_jda_productid = p.dim_jda_productid
		  and t.ct_units_cor <> case when upper(p.pharmaform) = 'ACTIV INGREDIENT (API)'
							  		   then 0
				 					 else t.ct_units end;

/* ************** WATERFALL CHART ********************************** */
/* in order to build waterfall chart we need dummy rows generation */
	/* build all startdates */
		drop table if exists tmp_minmax_dates;
		create table tmp_minmax_dates as
		select  min(sd.datevalue) min_startdate, max(sd.datevalue) max_startdate
		from tmp_prefact_demand_fe fdf,
			 dim_date sd
		where fdf.dim_dateidstartdate = sd.dim_dateid;
		
		drop table if exists tmp_full_dates;
		create table tmp_full_dates as
		select datevalue full_dates
		from dim_date dd,
			 tmp_minmax_dates td
		where dd.datevalue >= td.min_startdate and 
			  dd.datevalue <= td.max_startdate and
			  dd.companycode = 'Not Set' and
			  dd.dayofmonth = 1;

	/* join with unique rows and depict only missing one */
		drop table if exists tmp_distinct_grain;
		create table tmp_distinct_grain as
		select distinct dd_planning_item_id, case when dd_type = 'FCST EQUI' then 'FCST' else dd_type end as dd_type
		from tmp_prefact_demand_fe fdf;	
		
		drop table if exists tmp_distinct_grain_with_dates;
		create table tmp_distinct_grain_with_dates as
		select dd_planning_item_id, dd_type, full_dates
		from tmp_distinct_grain t1,
			 tmp_full_dates;
		
		drop table if exists tmp_grain_with_startdate_and_cycle;
		create table tmp_grain_with_startdate_and_cycle as
		select dd_planning_item_id, dd_type, full_dates as startdate, cycledate
		from tmp_distinct_grain_with_dates t1,
			 tmp_jda_workdate_cycledate t2
		where t1.full_dates = t2.workdate;
				 
		drop table if exists tmp_distinct_grain_with_startdate;
		create table tmp_distinct_grain_with_startdate as
		select distinct sd.datevalue startdate, cd.datevalue cycledate, dd_planning_item_id, case when dd_type = 'FCST EQUI' then 'FCST' else dd_type end as dd_type
		from tmp_prefact_demand_fe fdf,
			 dim_date sd,
			 dim_date cd
		where fdf.dim_dateidstartdate = sd.dim_dateid
			  and fdf.dim_dateidcycledate = cd.dim_dateid;
		
		drop table if exists tmp_dummy_rows_startdate;
		create table tmp_dummy_rows_startdate as
		select t1.dd_planning_item_id, t1.dd_type, t1.startdate, t1.cycledate
		from tmp_grain_with_startdate_and_cycle t1
				left join tmp_distinct_grain_with_startdate t2 on t1.dd_planning_item_id = t2.dd_planning_item_id and 
																  t1.dd_type = t2.dd_type and 
																  t1.startdate = t2.startdate and 
																  t1.cycledate = t2.cycledate
		where t2.startdate is null;
	

	/* insert dommy rows */
		delete from fact_jda_demandforecastevol where dd_dummy_wtrfl = 'X';
		
		delete from number_fountain m where m.table_name = 'fact_jda_demandforecastevol';
		insert into number_fountain
		select 'fact_jda_demandforecastevol', ifnull(max(f.fact_jda_demandforecastevolid ), 
										 ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
		from fact_jda_demandforecastevol f;

		alter table fact_jda_demandforecastevol add column startdate timestamp;
		alter table fact_jda_demandforecastevol add column cycledate timestamp;
		insert into fact_jda_demandforecastevol(
										fact_jda_demandforecastevolid, dim_dateidstartdate, dim_dateidcycledate, dd_planning_item_id, dd_type, dd_dummy_wtrfl,startdate,cycledate)
		select (select max_id from number_fountain where table_name = 'fact_jda_demandforecastevol') + row_number() over(order by '') as fact_jda_demandforecastevolid, 
				-- ifnull((select dim_dateid from dim_date where datevalue = t1.startdate and companycode = 'Not Set'),1) dim_dateidstartdate,
				-- ifnull((select dim_dateid from dim_date where datevalue = t1.cycledate and companycode = 'Not Set'),1)  dim_dateidcycledate,
				1 as dim_dateidstartdate,
				1 as dim_dateidcycledate,
				t1.dd_planning_item_id,
				t1.dd_type,
				'X' dd_dummy_wtrfl,
				t1.startdate as startdate,
				t1.cycledate as cycledate
		from tmp_dummy_rows_startdate t1;
		
		update fact_jda_demandforecastevol f
		set f.dim_dateidstartdate = d.dim_dateid
		from fact_jda_demandforecastevol f,dim_date d
		where d.datevalue = f.startdate 
		and companycode = 'Not Set';
		
		update fact_jda_demandforecastevol f
		set f.dim_dateidcycledate = d.dim_dateid
		from fact_jda_demandforecastevol f,dim_date d
		where d.datevalue = f.cycledate 
		and companycode = 'Not Set';
		
		alter table fact_jda_demandforecastevol drop column startdate ;
		alter table fact_jda_demandforecastevol drop column cycledate ;

	/* due to the fact that dummy rows are not mapped to dim_ids, we populate them from existing records from pre_fact */
		drop table if exists tmp_ids_for_def_val;
		create table tmp_ids_for_def_val as 
		select distinct dd_planning_item_id, dim_jda_locationid, dim_jda_productid, dd_abc_class, dd_localbrand, dd_itemlocalcode, dd_model, dd_fcstlevel
		from tmp_prefact_demand_fe;
		
		update fact_jda_demandforecastevol t1
		set t1.dim_jda_locationid = t2.dim_jda_locationid, 
			t1.dim_jda_productid  = t2.dim_jda_productid, 
			t1.dd_abc_class 	= t2.dd_abc_class, 
			t1.dd_localbrand 	= t2.dd_localbrand, 
			t1.dd_itemlocalcode = t2.dd_itemlocalcode, 
			t1.dd_model 		= t2.dd_model, 
			t1.dd_fcstlevel 	= t2.dd_fcstlevel
		from tmp_ids_for_def_val t2, fact_jda_demandforecastevol t1
		where     t1.dd_planning_item_id = t2.dd_planning_item_id
			  and t1.dd_dummy_wtrfl = 'X'; 	
	
	/* build previous values for all packs_cor */
		drop table if exists tmp_jda_lag_forecastevol;
		create table tmp_jda_lag_forecastevol as 
		select fact_jda_demandforecastevolid,
			   dcd.datevalue,
			   lag(ct_packs_cor, 1, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag1,
			   lag(ct_packs_cor, 2, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag2,
			   lag(ct_packs_cor, 3, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag3,
			   lag(ct_packs_cor, 4, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag4,
			   lag(ct_packs_cor, 5, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag5,
			   lag(ct_packs_cor, 6, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag6,
			   lag(ct_packs_cor, 7, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag7,
			   lag(ct_packs_cor, 8, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag8,
			   lag(ct_packs_cor, 9, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag9,
			   lag(ct_packs_cor, 10, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag10,
			   lag(ct_packs_cor, 11, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) packs_cor_lag11
		from fact_jda_demandforecastevol fdf,
			 dim_date dcd,
			 dim_date sd,
			 dim_jda_product djp
		where fdf.dim_dateidcycledate = dcd.dim_dateid and
			  fdf.dim_dateidstartdate = sd.dim_dateid and
			  fdf.dim_jda_productid   = djp.dim_jda_productid;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag1 = ds.packs_cor_lag1
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag1 <> ds.packs_cor_lag1;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag2 = ds.packs_cor_lag2
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag2 <> ds.packs_cor_lag2;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag3 = ds.packs_cor_lag3
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag3 <> ds.packs_cor_lag3;	  
		
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag4 = ds.packs_cor_lag4
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag4 <> ds.packs_cor_lag4;
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag5 = ds.packs_cor_lag5
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag5 <> ds.packs_cor_lag5;	  
		
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag6 = ds.packs_cor_lag6
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag6 <> ds.packs_cor_lag6;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag7 = ds.packs_cor_lag7
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag7 <> ds.packs_cor_lag7;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag8 = ds.packs_cor_lag8
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag8 <> ds.packs_cor_lag8;	
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag9 = ds.packs_cor_lag9
		from  fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag9 <> ds.packs_cor_lag9;	
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag10 = ds.packs_cor_lag10
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag10 <> ds.packs_cor_lag10;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_packs_cor_lag11 = ds.packs_cor_lag11
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_packs_cor_lag11 <> ds.packs_cor_lag11;	

	/* build previous values for all units_cor */
		drop table if exists tmp_jda_lag_forecastevol_unitscor;
		create table tmp_jda_lag_forecastevol_unitscor as 
		select fact_jda_demandforecastevolid,
			   dcd.datevalue,
			   lag(ct_units_cor, 1, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag1,
			   lag(ct_units_cor, 2, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag2,
			   lag(ct_units_cor, 3, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag3,
			   lag(ct_units_cor, 4, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag4,
			   lag(ct_units_cor, 5, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag5,
			   lag(ct_units_cor, 6, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag6,
			   lag(ct_units_cor, 7, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag7,
			   lag(ct_units_cor, 8, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag8,
			   lag(ct_units_cor, 9, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag9,
			   lag(ct_units_cor, 10, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag10,
			   lag(ct_units_cor, 11, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) units_cor_lag11
		from fact_jda_demandforecastevol fdf,
			 dim_date dcd,
			 dim_date sd,
			 dim_jda_product djp
		where fdf.dim_dateidcycledate = dcd.dim_dateid and
			  fdf.dim_dateidstartdate = sd.dim_dateid and
			  fdf.dim_jda_productid   = djp.dim_jda_productid;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag1 = ds.units_cor_lag1
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag1 <> ds.units_cor_lag1;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag2 = ds.units_cor_lag2
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag2 <> ds.units_cor_lag2;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag3 = ds.units_cor_lag3
		from fact_jda_demandforecastevol fje, tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag3 <> ds.units_cor_lag3;	  
		
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag4 = ds.units_cor_lag4
		from fact_jda_demandforecastevol fje, tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag4 <> ds.units_cor_lag4;
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag5 = ds.units_cor_lag5
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag5 <> ds.units_cor_lag5;	  
		
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag6 = ds.units_cor_lag6
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag6 <> ds.units_cor_lag6;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag7 = ds.units_cor_lag7
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag7 <> ds.units_cor_lag7;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag8 = ds.units_cor_lag8
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag8 <> ds.units_cor_lag8;	
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag9 = ds.units_cor_lag9
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag9 <> ds.units_cor_lag9;	
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag10 = ds.units_cor_lag10
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag10 <> ds.units_cor_lag10;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_units_cor_lag11 = ds.units_cor_lag11
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_unitscor ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_units_cor_lag11 <> ds.units_cor_lag11;	

	/* build previous values for all eq_unit */
		drop table if exists tmp_jda_lag_forecastevol_equnit;
		create table tmp_jda_lag_forecastevol_equnit as 
		select fact_jda_demandforecastevolid,
			   dcd.datevalue,
			   lag(ct_eq_unit, 1, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag1,
			   lag(ct_eq_unit, 2, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag2,
			   lag(ct_eq_unit, 3, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag3,
			   lag(ct_eq_unit, 4, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag4,
			   lag(ct_eq_unit, 5, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag5,
			   lag(ct_eq_unit, 6, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag6,
			   lag(ct_eq_unit, 7, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag7,
			   lag(ct_eq_unit, 8, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag8,
			   lag(ct_eq_unit, 9, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag9,
			   lag(ct_eq_unit, 10, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag10,
			   lag(ct_eq_unit, 11, 0)over(partition by dd_planning_item_id, dd_type, sd.datevalue order by dcd.datevalue) equnit_lag11
		from fact_jda_demandforecastevol fdf,
			 dim_date dcd,
			 dim_date sd,
			 dim_jda_product djp
		where fdf.dim_dateidcycledate = dcd.dim_dateid and
			  fdf.dim_dateidstartdate = sd.dim_dateid and
			  fdf.dim_jda_productid   = djp.dim_jda_productid;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag1 = ds.equnit_lag1
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag1 <> ds.equnit_lag1;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag2 = ds.equnit_lag2
		from  fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag2 <> ds.equnit_lag2;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag3 = ds.equnit_lag3
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag3 <> ds.equnit_lag3;	  
		
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag4 = ds.equnit_lag4
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag4 <> ds.equnit_lag4;
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag5 = ds.equnit_lag5
		from  fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag5 <> ds.equnit_lag5;	  
		
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag6 = ds.equnit_lag6
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag6 <> ds.equnit_lag6;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag7 = ds.equnit_lag7
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag7 <> ds.equnit_lag7;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag8 = ds.equnit_lag8
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag8 <> ds.equnit_lag8;	
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag9 = ds.equnit_lag9
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag9 <> ds.equnit_lag9;	
			  
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag10 = ds.equnit_lag10
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag10 <> ds.equnit_lag10;
		
		update fact_jda_demandforecastevol fje
		set fje.ct_equnit_lag11 = ds.equnit_lag11
		from fact_jda_demandforecastevol fje,tmp_jda_lag_forecastevol_equnit ds
		where     fje.fact_jda_demandforecastevolid = ds.fact_jda_demandforecastevolid
			  and fje.ct_equnit_lag11 <> ds.equnit_lag11;				  

/* clean-up useless dummy records */
delete from fact_jda_demandforecastevol
where ct_eq_unit + ct_equnit_lag1 + ct_equnit_lag10 + ct_equnit_lag11 + ct_equnit_lag2 + ct_equnit_lag3 + ct_equnit_lag4 + ct_equnit_lag5 +
	  ct_equnit_lag6 + ct_equnit_lag7 + ct_equnit_lag8 + ct_equnit_lag9 +
	  ct_packs_cor + ct_packs_cor_lag1 + ct_packs_cor_lag10 + ct_packs_cor_lag11 + ct_packs_cor_lag2 + ct_packs_cor_lag3 + ct_packs_cor_lag4 + ct_packs_cor_lag5 +
	  ct_packs_cor_lag6 + ct_packs_cor_lag7 + ct_packs_cor_lag8 + ct_packs_cor_lag9 +
	  ct_units_cor + ct_units_cor_lag1 + ct_units_cor_lag10 + ct_units_cor_lag11 + ct_units_cor_lag2 +  ct_units_cor_lag3 +  ct_units_cor_lag4 + ct_units_cor_lag5 +
	  ct_units_cor_lag6 + ct_units_cor_lag7 + ct_units_cor_lag8 + ct_units_cor_lag9 = 0
	  and dd_dummy_wtrfl = 'X';

/* ************** END WATERFALL CHART ********************************** */			  

/* Oana V - 13 July 2016 - JDA Demand for IBP */
update fact_jda_demandforecast f
	set f.ct_UDA_EXCHRATE = dc.EXCHRATE
from fact_jda_demandforecast f, dim_jda_location dc
where f.DIM_JDA_LOCATIONID = dc.DIM_JDA_LOCATIONID
and f.ct_UDA_EXCHRATE <> dc.EXCHRATE;
/* END Oana V - 13 July 2016 - JDA Demand for IBP */


/* WCOGS */
/* Liviu Ionescu 20170408 (weekend suport) - add max MICSTAG_LOCATION_CMG_MAPPING_CMG_ID due to ambiguous replace */
update fact_jda_demandforecast f
	set f.AMT_COGSFIXEDPLANRATE_EMD = ifnull(x.Z_GCPLFF,0)
from fact_jda_demandforecast f
	inner join DIM_JDA_PRODUCT p on f.DIM_JDA_PRODUCTID = p.DIM_JDA_PRODUCTID
	inner join DIM_JDA_LOCATION lc on f.DIM_JDA_LOCATIONID = lc.DIM_JDA_LOCATIONID
    inner join stsc_loc l on l.stsc_loc_loc = lc.loc
	left join (SELECT DISTINCT max(a.MICSTAG_LOCATION_CMG_MAPPING_CMG_ID) MICSTAG_LOCATION_CMG_MAPPING_CMG_ID, a.MICSTAG_LOCATION_CMG_MAPPING_DP_CODE
					FROM MICSTAG_LOCATION_CMG_MAPPING a
					WHERE (a.MICSTAG_LOCATION_CMG_MAPPING_SPT_RELEVANT = 1) group by a.MICSTAG_LOCATION_CMG_MAPPING_DP_CODE) yy
			on lc.LOCATION_NAME = yy.MICSTAG_LOCATION_CMG_MAPPING_DP_CODE
	inner join csv_cogs x on trim(leading 0 from x.Z_REPUNIT) = yy.MICSTAG_LOCATION_CMG_MAPPING_CMG_ID and trim(leading 0 from x.PRODUCT) = p.PRODUCT_NAME
where  l.stsc_loc_udc_loclevel = 1
	and l.stsc_loc_udc_dfuloc = 1
	and f.DD_FCSTLEVEL IN ('111', '711')
	and lc.LOCATION_NAME not like '%DISTR'
	and p.gbu not like 'CH%';

update fact_jda_demandforecast f
	set f.dd_cmg_id = yy.MICSTAG_LOCATION_CMG_MAPPING_CMG_ID
from fact_jda_demandforecast f
	inner join DIM_JDA_LOCATION lc on f.DIM_JDA_LOCATIONID = lc.DIM_JDA_LOCATIONID
	inner join (SELECT DISTINCT max(a.MICSTAG_LOCATION_CMG_MAPPING_CMG_ID) MICSTAG_LOCATION_CMG_MAPPING_CMG_ID, a.MICSTAG_LOCATION_CMG_MAPPING_DP_CODE
					FROM MICSTAG_LOCATION_CMG_MAPPING a
					WHERE (a.MICSTAG_LOCATION_CMG_MAPPING_SPT_RELEVANT = 1) group by a.MICSTAG_LOCATION_CMG_MAPPING_DP_CODE) yy
			on lc.LOCATION_NAME = yy.MICSTAG_LOCATION_CMG_MAPPING_DP_CODE;

/* Marius 3 nov 2916 WCOGS for JDA */

drop table if exists tmp_jda_cogs;
create table tmp_jda_cogs
as
select distinct
	dm.DMDUNIT_dmdunit Global_prd
	,df.STSC_DFUVIEW_UDC_prodlocalid Local_prd
	,m.MICSTAG_LOCATION_CMG_MAPPING_dp_code Location
	,m.MICSTAG_LOCATION_CMG_MAPPING_cmg_id GlobalCompany
	,l.STSC_LOC_udc_cmg_id LocalCompany
	,dts.STSC_DFUTOSKU_skuloc SKULocation
	,lll.STSC_LOC_udc_cmg_id SKUCompany
	,cast(0 as decimal(18,4)) cogs_price_cy
	,cast(0 as decimal(18,4)) cogs_price_op_cy
	,cast(0 as decimal(18,4)) cogs_price_ny
	,cast(0 as decimal(18,4)) cogs_price_op_ny
	,cast(0 as decimal(18,4)) cf_value_cy
	,cast(0 as decimal(18,4)) cf_qty_cy
	,cast(0 as decimal(18,4)) op_value_cy
	,cast(0 as decimal(18,4)) op_qty_cy
	,cast(0 as decimal(18,4)) cf_value_ny
	,cast(0 as decimal(18,4)) cf_qty_ny
	,cast(0 as decimal(18,4)) op_value_ny
	,cast(0 as decimal(18,4)) op_qty_ny
	,cast(0 as decimal(18,4)) pctg
	,cast('Not Set' as varchar(40)) MDG_Flag_Global
	,cast('Not Set' as varchar(40)) MDG_Flag_Local
	,cast('Not Set' as varchar(40)) cogssource
	,cast('Not Set' as varchar(40)) cogssource_op
from DMDUNIT dm
	inner join STSC_DFUVIEW df on dm.DMDUNIT_dmdunit = df.STSC_DFUVIEW_dmdunit
	inner join STSC_LOC l on df.STSC_DFUVIEW_loc = l.STSC_LOC_loc
	inner join MICSTAG_LOCATION_CMG_MAPPING m on df.STSC_DFUVIEW_loc = m.MICSTAG_LOCATION_CMG_MAPPING_dp_code
	left outer join STSC_DFUTOSKU dts on dm.DMDUNIT_dmdunit = dts.STSC_DFUTOSKU_dmdunit and df.STSC_DFUVIEW_loc = dts.STSC_DFUTOSKU_dfuloc
		and dts.STSC_DFUTOSKU_EFF <= SYSDATE AND dts.STSC_DFUTOSKU_DISC > SYSDATE
	left outer join STSC_LOC lll on dts.STSC_DFUTOSKU_skuloc = lll.STSC_LOC_loc
where df.STSC_DFUVIEW_udc_fcstlevel IN ('111', '711') and l.STSC_LOC_loc not like '%distr' /*and df.STSC_DFUVIEW_udc_fcstsw = 1*/
	and dm.DMDUNIT_udc_strength not like 'CH%' and m.MICSTAG_LOCATION_CMG_MAPPING_spt_relevant = 1;

/* update MDG Flag */

update tmp_jda_cogs t
set t.MDG_Flag_Global = COGSICMINDICATOR
from tmp_jda_cogs t, dim_mdg_part m
where t.Global_prd = m.partnumber and t.MDG_Flag_Global <> COGSICMINDICATOR;

update tmp_jda_cogs t
set t.MDG_Flag_Local = COGSICMINDICATOR
from tmp_jda_cogs t, dim_mdg_part m
where t.Local_prd = m.partnumber and t.Global_prd <> t.Local_prd and t.MDG_Flag_Local <> COGSICMINDICATOR;

/* update COGS from Gauss*/

update tmp_jda_cogs t
set t.cogs_price_cy = c.Z_GCPLFF, t.cogs_price_ny = c.Z_GCPLFF, t.cogssource = 'Gauss GLBL Part - GLBL Comp', t.cogs_price_op_cy = c.Z_GCPLFF, t.cogs_price_op_ny = c.Z_GCPLFF, t.cogssource_op = 'Gauss GLBL Part - GLBL Comp'
from tmp_jda_cogs t, csv_cogs c
where trim(leading '0' from t.Global_prd) = trim(leading '0' from c.PRODUCT)
	and trim(leading '0' from t.GlobalCompany) = trim(leading '0' from c.Z_REPUNIT)
	and t.MDG_Flag_Global = 'X' and t.cogs_price_cy = 0;

update tmp_jda_cogs t
set t.cogs_price_cy = c.Z_GCPLFF, t.cogs_price_ny = c.Z_GCPLFF, t.cogssource = 'Gauss GLBL Part - SKU Comp', t.cogs_price_op_cy = c.Z_GCPLFF, t.cogs_price_op_ny = c.Z_GCPLFF, t.cogssource_op = 'Gauss GLBL Part - GLBL Comp'
from tmp_jda_cogs t, csv_cogs c
where trim(leading '0' from t.Global_prd) = trim(leading '0' from c.PRODUCT)
	and trim(leading '0' from t.SKUCompany) = trim(leading '0' from c.Z_REPUNIT)
	and t.MDG_Flag_Global = 'X' and t.cogs_price_cy = 0;

update tmp_jda_cogs t
set t.cogs_price_cy = c.Z_GCPLFF, t.cogs_price_ny = c.Z_GCPLFF, t.cogssource = 'Gauss LCL Part - GLBL Comp', t.cogs_price_op_cy = c.Z_GCPLFF, t.cogs_price_op_ny = c.Z_GCPLFF, t.cogssource_op = 'Gauss GLBL Part - GLBL Comp'
from tmp_jda_cogs t, csv_cogs c
where trim(leading '0' from t.Local_prd) = trim(leading '0' from c.PRODUCT)
	and trim(leading '0' from t.GlobalCompany) = trim(leading '0' from c.Z_REPUNIT)
	and t.MDG_Flag_Local = 'X' and t.cogs_price_cy = 0;

update tmp_jda_cogs t
set t.cogs_price_cy = c.Z_GCPLFF, t.cogs_price_ny = c.Z_GCPLFF, t.cogssource = 'Gauss LCL Part - SKU Comp', t.cogs_price_op_cy = c.Z_GCPLFF, t.cogs_price_op_ny = c.Z_GCPLFF, t.cogssource_op = 'Gauss GLBL Part - GLBL Comp'
from tmp_jda_cogs t, csv_cogs c
where trim(leading '0' from t.Local_prd) = trim(leading '0' from c.PRODUCT)
	and trim(leading '0' from t.SKUCompany) = trim(leading '0' from c.Z_REPUNIT)
	and t.MDG_Flag_Local = 'X' and t.cogs_price_cy = 0;

/* update cogs for MDG parts using percent of sale */

drop table if exists tmp_wcogs_calculation_pctg;
create table tmp_wcogs_calculation_pctg
as
select
	p.product_name
	,f.DD_CMG_ID
	,sd.calendaryear
	,SUM(CASE WHEN f.dd_type = 'Consensus Forecast QTY' THEN f.ct_packs ELSE 0 END) CF_QTY
	,SUM(CASE WHEN f.dd_type = 'Consensus Forecast EURO' THEN f.ct_packs ELSE 0 END) CF_VALUE
from fact_jda_demandforecast f
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
where sd.calendaryear in (year(current_date), year(current_date)+1)
group by p.product_name,f.DD_CMG_ID,sd.calendaryear;

drop table if exists tmp_wcogs_calculation_pctg_op;
create table tmp_wcogs_calculation_pctg_op
as
select
	p.product_name
	,f.DD_CMG_ID
	,sd.calendaryear
	,SUM(case when jcmp.c_type3 = 'OP' then f.ct_packs else 0 end) OP_QTY
	,SUM(case when (jcmp.c_type3) = 'OP EURO' AND (f.dd_fcstlevel) IN ('111', '711') THEN (f.ct_packs) ELSE 0 END) OP_VALUE
from fact_jda_demandforecast f
	inner join dim_jda_component jcmp on f.dim_jda_componentid = jcmp.dim_jda_componentid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
where sd.calendaryear in (year(current_date), year(current_date)+1)
group by p.product_name,f.DD_CMG_ID,sd.calendaryear;

update tmp_jda_cogs c
set c.CF_VALUE_CY = t.CF_VALUE , c.CF_QTY_CY = t.CF_QTY
from tmp_jda_cogs c
	inner join tmp_wcogs_calculation_pctg t on t.product_name = c.GLOBAL_PRD and t.DD_CMG_ID = c.GLOBALCOMPANY and t.calendaryear = year(current_date)
where c.MDG_FLAG_GLOBAL = 'X' and c.COGS_PRICE_CY = 0;

update tmp_jda_cogs c
set c.CF_VALUE_CY = t.CF_VALUE , c.CF_QTY_CY = t.CF_QTY
from tmp_jda_cogs c
	inner join tmp_wcogs_calculation_pctg t on t.product_name = c.LOCAL_PRD and t.DD_CMG_ID = c.GLOBALCOMPANY and t.calendaryear = year(current_date)
where c.MDG_FLAG_LOCAL = 'X' and c.COGS_PRICE_CY = 0;

update tmp_jda_cogs c
set c.OP_VALUE_CY = t.OP_VALUE , c.OP_QTY_CY = t.OP_QTY
from tmp_jda_cogs c
	inner join tmp_wcogs_calculation_pctg_op t on t.product_name = c.GLOBAL_PRD and t.DD_CMG_ID = c.GLOBALCOMPANY and t.calendaryear = year(current_date)
where c.MDG_FLAG_GLOBAL = 'X' and c.COGS_PRICE_OP_CY = 0;

update tmp_jda_cogs c
set c.OP_VALUE_CY = t.OP_VALUE , c.OP_QTY_CY = t.OP_QTY
from tmp_jda_cogs c
	inner join tmp_wcogs_calculation_pctg_op t on t.product_name = c.LOCAL_PRD and t.DD_CMG_ID = c.GLOBALCOMPANY and t.calendaryear = year(current_date)
where c.MDG_FLAG_LOCAL = 'X' and c.COGS_PRICE_OP_CY = 0;

update tmp_jda_cogs c
set c.CF_VALUE_NY = t.CF_VALUE , c.CF_QTY_NY = t.CF_QTY
from tmp_jda_cogs c
	inner join tmp_wcogs_calculation_pctg t on t.product_name = c.GLOBAL_PRD and t.DD_CMG_ID = c.GLOBALCOMPANY and t.calendaryear = year(current_date) + 1
where c.MDG_FLAG_GLOBAL = 'X' and c.COGS_PRICE_NY = 0;

update tmp_jda_cogs c
set c.CF_VALUE_NY = t.CF_VALUE , c.CF_QTY_NY = t.CF_QTY
from tmp_jda_cogs c
	inner join tmp_wcogs_calculation_pctg t on t.product_name = c.LOCAL_PRD and t.DD_CMG_ID = c.GLOBALCOMPANY and t.calendaryear = year(current_date) + 1
where c.MDG_FLAG_LOCAL = 'X' and c.COGS_PRICE_NY = 0;

update tmp_jda_cogs c
set c.OP_VALUE_NY = t.OP_VALUE , c.OP_QTY_NY = t.OP_QTY
from tmp_jda_cogs c
	inner join tmp_wcogs_calculation_pctg_op t on t.product_name = c.GLOBAL_PRD and t.DD_CMG_ID = c.GLOBALCOMPANY and t.calendaryear = year(current_date) + 1
where c.MDG_FLAG_GLOBAL = 'X' and c.COGS_PRICE_OP_NY = 0;

update tmp_jda_cogs c
set c.OP_VALUE_NY = t.OP_VALUE , c.OP_QTY_NY = t.OP_QTY
from tmp_jda_cogs c
	inner join tmp_wcogs_calculation_pctg_op t on t.product_name = c.LOCAL_PRD and t.DD_CMG_ID = c.GLOBALCOMPANY and t.calendaryear = year(current_date) + 1
where c.MDG_FLAG_LOCAL = 'X' and c.COGS_PRICE_OP_NY = 0;

/* get pctg from Livia file */

update tmp_jda_cogs c
set c.pctg = t.val
from tmp_jda_cogs c
	inner join csv_wcogs_pctg t on t.repunit = c.GLOBALCOMPANY and t.item = GLOBAL_PRD
where (c.MDG_FLAG_GLOBAL = 'X' or c.MDG_FLAG_LOCAL = 'X') and c.COGS_PRICE_CY = 0;

/* update price based on CF/OP for the year and the pctg */

update tmp_jda_cogs c
set c.COGS_PRICE_CY = (case when c.CF_QTY_CY = 0 then 0 else cast(c.CF_VALUE_CY * c.PCTG as decimal(18,4)) / c.CF_QTY_CY end)
	,c.COGSSOURCE = 'PCTG GLBL Part'
where c.MDG_FLAG_GLOBAL = 'X' and c.COGS_PRICE_CY = 0;

update tmp_jda_cogs c
set c.COGS_PRICE_CY = (case when c.CF_QTY_CY = 0 then 0 else cast(c.CF_VALUE_CY * c.PCTG as decimal(18,4)) / c.CF_QTY_CY end)
	,c.COGSSOURCE = 'PCTG LCL Part'
where c.MDG_FLAG_LOCAL = 'X' and c.COGS_PRICE_CY = 0;

update tmp_jda_cogs c
set c.COGSSOURCE_OP = 'PCTG GLBL Part',
	c.COGS_PRICE_OP_CY = (case when c.OP_QTY_CY = 0 then 0 else cast(c.OP_VALUE_CY * c.PCTG as decimal(18,4)) / c.OP_QTY_CY end)
where c.MDG_FLAG_GLOBAL = 'X' and c.COGS_PRICE_OP_CY = 0;

update tmp_jda_cogs c
set c.COGSSOURCE_OP = 'PCTG LCL Part',
	c.COGS_PRICE_OP_CY = (case when c.OP_QTY_CY = 0 then 0 else cast(c.OP_VALUE_CY * c.PCTG as decimal(18,4)) / c.OP_QTY_CY end)
where c.MDG_FLAG_LOCAL = 'X' and c.COGS_PRICE_OP_CY = 0;

update tmp_jda_cogs c
set c.COGS_PRICE_NY = (case when c.CF_QTY_NY = 0 then 0 else cast(c.CF_VALUE_NY * c.PCTG as decimal(18,4)) / c.CF_QTY_NY end)
	,c.COGSSOURCE = 'PCTG GLBL Part'
where c.MDG_FLAG_GLOBAL = 'X' and c.COGS_PRICE_NY = 0;

update tmp_jda_cogs c
set c.COGS_PRICE_NY = (case when c.CF_QTY_NY = 0 then 0 else cast(c.CF_VALUE_NY * c.PCTG as decimal(18,4)) / c.CF_QTY_NY end)
	,c.COGSSOURCE = 'PCTG LCL Part'
where c.MDG_FLAG_LOCAL = 'X' and c.COGS_PRICE_NY = 0;

update tmp_jda_cogs c
set c.COGSSOURCE_OP = 'PCTG GLBL Part',
	c.COGS_PRICE_OP_NY = (case when c.OP_QTY_NY = 0 then 0 else cast(c.OP_VALUE_NY * c.PCTG as decimal(18,4)) / c.OP_QTY_NY end)
where c.MDG_FLAG_GLOBAL = 'X' and c.COGS_PRICE_OP_NY = 0;

update tmp_jda_cogs c
set c.COGSSOURCE_OP = 'PCTG LCL Part',
	c.COGS_PRICE_OP_NY = (case when c.OP_QTY_NY = 0 then 0 else cast(c.OP_VALUE_NY * c.PCTG as decimal(18,4)) / c.OP_QTY_NY end)
where c.MDG_FLAG_LOCAL = 'X' and c.COGS_PRICE_OP_NY = 0;

/* update COGS based on standard price */

drop table if exists tmp_inventory_price;
create table tmp_inventory_price
as
select dp.partnumber, dc.company, max(f.amt_StdUnitPrice * f.amt_exchangerate_gbl) stdprice
from emd586.fact_inventoryhistory f
	inner join emd586.dim_part dp on f.dim_partid = dp.dim_partid
	inner join emd586.dim_company dc on f.dim_companyid = dc.dim_companyid
	inner join emd586.dim_date s on f.dim_dateidsnapshot = s.dim_dateid
where s.datevalue >= current_date - interval '3' month
group by dp.partnumber, dc.company;

update tmp_jda_cogs j
set j.COGS_PRICE_CY = t.stdprice, j.COGS_PRICE_NY = t.stdprice, j.cogssource = 'SAP GLBL Part - GLBL Comp'
from tmp_jda_cogs j
	inner join tmp_inventory_price t on trim(leading '0' from j.Global_prd) = trim(leading '0' from t.partnumber)
		and trim(leading '0' from j.GlobalCompany) = trim(leading '0' from t.company)
		and j.MDG_Flag_Global <> 'X' and j.MDG_FLAG_LOCAL <> 'X' and j.cogs_price_cy = 0;

update tmp_jda_cogs j
set j.COGS_PRICE_CY = t.stdprice, j.COGS_PRICE_NY = t.stdprice, j.cogssource = 'SAP GLBL Part - SKU Comp'
from tmp_jda_cogs j
	inner join tmp_inventory_price t on trim(leading '0' from j.Global_prd) = trim(leading '0' from t.partnumber)
		and trim(leading '0' from j.SKUCOMPANY) = trim(leading '0' from t.company)
			and j.MDG_Flag_Global <> 'X' and j.MDG_FLAG_LOCAL <> 'X' and j.cogs_price_cy = 0;

update tmp_jda_cogs j
set j.COGS_PRICE_CY = t.stdprice, j.COGS_PRICE_NY = t.stdprice, j.cogssource = 'SAP LCL Part - GLBL Comp'
from tmp_jda_cogs j
	inner join tmp_inventory_price t on trim(leading '0' from j.LOCAL_PRD) = trim(leading '0' from t.partnumber)
		and trim(leading '0' from j.GlobalCompany) = trim(leading '0' from t.company)
		and j.MDG_Flag_Global <> 'X' and j.MDG_FLAG_LOCAL <> 'X' and j.cogs_price_cy = 0;

update tmp_jda_cogs j
set j.COGS_PRICE_CY = t.stdprice, j.COGS_PRICE_NY = t.stdprice, j.cogssource = 'SAP LCL Part - SKU Comp'
from tmp_jda_cogs j
	inner join tmp_inventory_price t on trim(leading '0' from j.LOCAL_PRD) = trim(leading '0' from t.partnumber)
		and trim(leading '0' from j.SKUCOMPANY) = trim(leading '0' from t.company)
		and j.MDG_Flag_Global <> 'X' and j.MDG_FLAG_LOCAL <> 'X' and j.cogs_price_cy = 0;

/* OP */

update tmp_jda_cogs j
set j.COGS_PRICE_OP_CY = t.stdprice, j.COGS_PRICE_OP_NY = t.stdprice, j.cogssource_op = 'SAP GLBL Part - GLBL Comp'
from tmp_jda_cogs j
	inner join tmp_inventory_price t on trim(leading '0' from j.Global_prd) = trim(leading '0' from t.partnumber)
		and trim(leading '0' from j.GlobalCompany) = trim(leading '0' from t.company)
where j.MDG_Flag_Global <> 'X' and j.MDG_FLAG_LOCAL <> 'X' and j.COGS_PRICE_OP_CY = 0;

update tmp_jda_cogs j
set j.COGS_PRICE_OP_CY = t.stdprice, j.COGS_PRICE_OP_NY = t.stdprice, j.cogssource_op = 'SAP GLBL Part - SKU Comp'
from tmp_jda_cogs j
	inner join tmp_inventory_price t on trim(leading '0' from j.Global_prd) = trim(leading '0' from t.partnumber)
		and trim(leading '0' from j.SKUCOMPANY) = trim(leading '0' from t.company)
where j.MDG_Flag_Global <> 'X' and j.MDG_FLAG_LOCAL <> 'X' and j.COGS_PRICE_OP_CY = 0;

update tmp_jda_cogs j
set j.COGS_PRICE_OP_CY = t.stdprice, j.COGS_PRICE_OP_NY = t.stdprice, j.cogssource_op = 'SAP LCL Part - GLBL Comp'
from tmp_jda_cogs j
	inner join tmp_inventory_price t on trim(leading '0' from j.LOCAL_PRD) = trim(leading '0' from t.partnumber)
		and trim(leading '0' from j.GlobalCompany) = trim(leading '0' from t.company)
where j.MDG_Flag_Global <> 'X' and j.MDG_FLAG_LOCAL <> 'X' and j.COGS_PRICE_OP_CY = 0;

update tmp_jda_cogs j
set j.COGS_PRICE_OP_CY = t.stdprice, j.COGS_PRICE_OP_NY = t.stdprice, j.cogssource_op = 'SAP LCL Part - SKU Comp'
from tmp_jda_cogs j
	inner join tmp_inventory_price t on trim(leading '0' from j.LOCAL_PRD) = trim(leading '0' from t.partnumber)
		and trim(leading '0' from j.SKUCOMPANY) = trim(leading '0' from t.company)
where j.MDG_Flag_Global <> 'X' and j.MDG_FLAG_LOCAL <> 'X' and j.COGS_PRICE_OP_CY = 0;

/* Final updates */

update tmp_jda_cogs
set COGSSOURCE = 'Not Set' where COGS_PRICE_CY = 0 and COGS_PRICE_NY = 0;

update tmp_jda_cogs
set COGSSOURCE_OP = 'Not Set' where COGS_PRICE_OP_CY = 0 and COGS_PRICE_OP_NY = 0;

drop table if exists tmp_updt_jda_cogs;
create table tmp_updt_jda_cogs
as
select Global_prd, GlobalCompany, cogs_price_cy, cogssource, row_number() over(partition by Global_prd, GlobalCompany order by cogs_price_cy desc) rono
from tmp_jda_cogs;

update fact_jda_demandforecast f
set f.amt_jdacogs = t.cogs_price_cy, f.dd_cogssource = t.cogssource
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join tmp_updt_jda_cogs t on p.product_name = t.Global_prd and f.DD_CMG_ID = t.GlobalCompany and t.rono = 1
where sd.calendaryear <= year(current_date);

drop table if exists tmp_updt_jda_cogs;
create table tmp_updt_jda_cogs
as
select Global_prd, GlobalCompany, cogs_price_ny, cogssource, row_number() over(partition by Global_prd, GlobalCompany order by cogs_price_ny desc) rono
from tmp_jda_cogs;

update fact_jda_demandforecast f
set f.amt_jdacogs = t.cogs_price_ny, f.dd_cogssource = t.cogssource
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join tmp_updt_jda_cogs t on p.product_name = t.Global_prd and f.DD_CMG_ID = t.GlobalCompany and t.rono = 1
where sd.calendaryear >= year(current_date) + 1;

drop table if exists tmp_updt_jda_cogs;

drop table if exists tmp_updt_jda_cogs_op;
create table tmp_updt_jda_cogs_op
as
select Global_prd, GlobalCompany, cogs_price_op_cy, cogssource_op, row_number() over(partition by Global_prd, GlobalCompany order by cogs_price_op_cy desc) rono
from tmp_jda_cogs;

update fact_jda_demandforecast f
set f.amt_jdacogs_op = t.cogs_price_op_cy, f.dd_cogssource_op = t.cogssource_op
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join tmp_updt_jda_cogs_op t on p.product_name = t.Global_prd and f.DD_CMG_ID = t.GlobalCompany and t.rono = 1
where sd.calendaryear <= year(current_date);

drop table if exists tmp_updt_jda_cogs_op;
create table tmp_updt_jda_cogs_op
as
select Global_prd, GlobalCompany, cogs_price_op_ny, cogssource_op, row_number() over(partition by Global_prd, GlobalCompany order by cogs_price_op_ny desc) rono
from tmp_jda_cogs;

update fact_jda_demandforecast f
set f.amt_jdacogs_op = t.cogs_price_op_ny, f.dd_cogssource_op = t.cogssource_op
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join tmp_updt_jda_cogs_op t on p.product_name = t.Global_prd and f.DD_CMG_ID = t.GlobalCompany and t.rono = 1
where sd.calendaryear >= year(current_date) + 1;

drop table if exists tmp_updt_jda_cogs_op;

update fact_jda_demandforecast f
set f.dd_cogssource = 'Not Set'
where f.amt_jdacogs = 0 and f.dd_cogssource <> 'Not Set';

update fact_jda_demandforecast f
set f.dd_cogssource_op = 'Not Set'
where f.amt_jdacogs_op = 0 and f.dd_cogssource_op <> 'Not Set';

update fact_jda_demandforecast f
set f.amt_jdacogs = (t.CF_VALUE * c.wcogs_pctg) / t.CF_QTY, f.dd_cogssource = 'PCTG Business Line'
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join tmp_wcogs_calculation_pctg t on p.product_name = t.product_name and f.DD_CMG_ID = t.DD_CMG_ID and t.calendaryear = year(current_date)
	inner join csv_pctg_cogs c on trim(leading '0' from f.DD_CMG_ID) = trim(leading '0' from c.company) and p.UDC_BUSINESS_LINE = c.bussinesline
where f.dd_cogssource = 'Not Set' and t.CF_QTY <> 0 and sd.calendaryear <= year(current_date) and (t.CF_VALUE * c.wcogs_pctg) / t.CF_QTY <> 0;

update fact_jda_demandforecast f
set f.amt_jdacogs = (t.CF_VALUE * c.wcogs_pctg) / t.CF_QTY, f.dd_cogssource = 'PCTG Business Line'
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join tmp_wcogs_calculation_pctg t on p.product_name = t.product_name and f.DD_CMG_ID = t.DD_CMG_ID and t.calendaryear = year(current_date) + 1
	inner join csv_pctg_cogs c on trim(leading '0' from f.DD_CMG_ID) = trim(leading '0' from c.company) and p.UDC_BUSINESS_LINE = c.bussinesline
where f.dd_cogssource = 'Not Set' and t.CF_QTY <> 0 and sd.calendaryear >= year(current_date) + 1 and (t.CF_VALUE * c.wcogs_pctg) / t.CF_QTY <> 0;

drop table if exists tmp_wcogs_calculation_pctg;

update fact_jda_demandforecast f
set f.amt_jdacogs_op = (t.OP_VALUE * c.wcogs_pctg) / t.OP_QTY, f.dd_cogssource_op = 'PCTG Business Line'
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join tmp_wcogs_calculation_pctg_op t on p.product_name = t.product_name and f.DD_CMG_ID = t.DD_CMG_ID and t.calendaryear = year(current_date)
	inner join csv_pctg_cogs c on trim(leading '0' from f.DD_CMG_ID) = trim(leading '0' from c.company) and p.UDC_BUSINESS_LINE = c.bussinesline
where f.dd_cogssource_op = 'Not Set' and t.OP_QTY <> 0 and sd.calendaryear <= year(current_date) and (t.OP_VALUE * c.wcogs_pctg) / t.OP_QTY <> 0;

update fact_jda_demandforecast f
set f.amt_jdacogs_op = (t.OP_VALUE * c.wcogs_pctg) / t.OP_QTY, f.dd_cogssource_op = 'PCTG Business Line'
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join tmp_wcogs_calculation_pctg_op t on p.product_name = t.product_name and f.DD_CMG_ID = t.DD_CMG_ID and t.calendaryear = year(current_date) + 1
	inner join csv_pctg_cogs c on trim(leading '0' from f.DD_CMG_ID) = trim(leading '0' from c.company) and p.UDC_BUSINESS_LINE = c.bussinesline
where f.dd_cogssource_op = 'Not Set' and t.OP_QTY <> 0 and sd.calendaryear >= year(current_date) + 1 and (t.OP_VALUE * c.wcogs_pctg) / t.OP_QTY <> 0;

drop table if exists tmp_wcogs_calculation_pctg_op;

update fact_jda_demandforecast f
set f.dd_wcogspctg = 'X'
from fact_jda_demandforecast f
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join csv_pctg_cogs c on trim(leading '0' from f.DD_CMG_ID) = trim(leading '0' from c.company) and p.UDC_BUSINESS_LINE = c.bussinesline;

/* Egypt Metformin exception */

update fact_jda_demandforecast f
set f.amt_jdacogs_op = f.amt_jdacogs_op / 85
from fact_jda_demandforecast f
	inner join dim_date sd on f.dim_dateidstartdate = sd.dim_dateid
	inner join dim_jda_product p on f.dim_jda_productid = p.dim_jda_productid
	inner join dim_jda_location l on f.dim_jda_locationid = l.dim_jda_locationid
where year(sd.datevalue) = 2016 and p.product_name = 'FR21030390085' and l.UDC_GAUSS_REP_REGION = 'EG EGYPT' and f.dd_cogssource_op = 'Gauss GLBL Part - GLBL Comp';

/* Marius end COGS JDA */

/* Demand Alert today vs yesterday - Oana 18 Nov 2016 */
update fact_jda_demandforecast f
	set f.ct_units_cor_yesterday = f1.ct_units_cor,
		f.dim_dateidsnapshot = f1.dim_dateidsnapshot
from fact_jda_demandforecast f,fact_jda_demandalertsnapshot f1
	where f.DIM_DATEIDSTARTDATE 	= f1.DIM_DATEIDSTARTDATE
		and f.DD_PLANNING_ITEM_ID	= f1.DD_PLANNING_ITEM_ID
		and f.DIM_JDA_LOCATIONID 	= f1.DIM_JDA_LOCATIONID
		and f.DIM_JDA_PRODUCTID 	= f1.DIM_JDA_PRODUCTID
		and f.DIM_JDA_COMPONENTID 	= f1.DIM_JDA_COMPONENTID;
/* END Demand Alert today vs yesterday - Oana 18 Nov 2016 */

/* Corrections - Oana 29 March 2017 */
update fact_jda_demandforecast f
	set f.ct_packs_tocorrect = csvc.ct_packs_tocorrect
from fact_jda_demandforecast f, dim_jda_component djc, dim_date dt, dim_jda_product dp, dim_jda_location dl, csv_pve_correction csvc
	where djc.dim_jda_componentid = f.dim_jda_componentid
	and dt.dim_dateid = f.dim_dateidstartdate
	and dp.dim_jda_productid = f.dim_jda_productid
	and dl.dim_jda_locationid = f.dim_jda_locationid
	and trim(csvc.product_name) = dp.product_name
	and trim(csvc.loc) = dl.loc
	and csvc.datevalue = dt.datevalue
	and csvc.component_type = djc.component_name
	and csvc.sbu =  dp.sbu;
/* END - Corrections - Oana 29 March 2017 */

/* PVE - Oana 17 Jan 2017 */
update fact_jda_demandforecast f
	set f.dim_mdg_partid = m.dim_mdg_partid
from fact_jda_demandforecast f, dim_jda_product dp , dim_mdg_part m 
where f.dim_jda_productid = dp.dim_jda_productid
	and product_name = m.partnumber
	and f.dim_mdg_partid <> m.dim_mdg_partid;

drop table if exists tmp_product_toexclude;
create table tmp_product_toexclude as
	select dim_jda_productid, product_name,itemdescr
	from dim_jda_product
	where (upper(itemdescr) like '%DEMO%'
		or upper(itemdescr) like '%DMO%'
		or upper(itemdescr) like '%SAMPLE'
		or upper(itemdescr) like '%SPL%'
		or product_name in ('U0U19003','U1211202'))
		and (upper(itemdescr) not like '%KIT%'  OR upper(itemdescr) like '%DEMOKIT%');

---- Consensus Forecast PY vs CY -----
drop table if exists  tmp_pve_beforeFiltering;
create table tmp_pve_beforeFiltering as
select datevalue
		 ,f.dim_dateidstartdate
		 ,f.dd_planning_item_id
		 ,f.dim_jda_locationid
		 ,f.dim_jda_productid
		 ,sum(case when f.dd_type = 'Consensus Forecast QTY' and dp.FINAL_SBU <> 'No' then ((ct_packs+ct_packs_tocorrect) * dmp.COMPLETECONTENTOFTHEPACKAGE)
					when f.dd_type = 'Consensus Forecast QTY' and dp.FINAL_SBU = 'No' then (ct_packs+ct_packs_tocorrect)
					else 0 end) as ct_cfcst_qty
		,sum(case when dd_type = 'Consensus Forecast LC' then ct_packs else 0 end) as amt_cfcst_euro
		,sum(case when dd_type = 'Consensus Forecast EURO' then ct_packs else 0 end) as amt_cfcst_euro_zero
		,sum(case when dd_type = 'Consensus Forecast LC' then ct_packs * dl.ACTUALRATE_PY_YTD else 0 end) as amt_cfcst_ytd_lc
		,sum(case when f.dd_type = 'Consensus Forecast QTY' then ct_packs_tocorrect else 0 end) as ct_cfcst_qty_corrected
	from fact_jda_demandforecast f, dim_Date dt, dim_jda_location dl, dim_mdg_part dmp, dim_jda_product dp
	where dt.dim_dateid = f.dim_dateidstartdate
		and dl.dim_jda_locationid = f.dim_jda_locationid
		and dp.dim_jda_productid = f.dim_jda_productid
		and dmp.dim_mdg_partid = f.dim_mdg_partid
		and date_trunc('YEAR',datevalue) between date_trunc('YEAR',current_date-INTERVAL '1' Year) and date_trunc('YEAR',current_date)
		and f.dd_type in ('Consensus Forecast QTY','Consensus Forecast LC','Consensus Forecast EURO')
		and dl.UDC_GAUSS_REP_REGION not in ('VE VENEZUELA')
		--and dp.branddescr not in ('KUVAN-MD','KUVAN')
		and (location_name not like 'REP%' and location_name not like '%DISTR' and dl.location_name <> '350PK')
		and f.dd_cmg_id not in (1056,1755)
	group by datevalue	,f.dim_dateidstartdate	,f.dd_planning_item_id	,f.dim_jda_locationid	,f.dim_jda_productid;

drop table if exists tmp_pve_beforeFiltering_agg;
create table tmp_pve_beforeFiltering_agg as
select f.dim_jda_locationid
	,f.dim_jda_productid
	,sum(CT_CFCST_QTY) CT_CFCST_QTY
	,sum(amt_cfcst_euro_zero) amt_cfcst_euro_zero
from  tmp_pve_beforeFiltering f
group by f.dim_jda_locationid,f.dim_jda_productid;

drop table if exists tmp_pve_filterout;
create table tmp_pve_filterout as
select f.dim_jda_productid , f.dim_jda_locationid
from  tmp_pve_beforeFiltering_agg f
where (trunc(CT_CFCST_QTY) >= 0
	and trunc(amt_cfcst_euro_zero) = 0
	and f.dim_jda_productid in (select tdp.dim_jda_productid from tmp_product_toexclude tdp));

drop table if exists tmp_pve_calculate_price;
create table tmp_pve_calculate_price as
	select f.*
	from tmp_pve_beforeFiltering f
	left outer join tmp_pve_filterout t on f.dim_jda_productid = t.dim_jda_productid and f.dim_jda_locationid = t.dim_jda_locationid
	where  t.dim_jda_productid is null
		and t.dim_jda_locationid is null;

--- Quarter ---
drop table if exists tmp_pve_quarter;
create table tmp_pve_quarter as
select calendaryear,
	CALENDARQUARTER,
	CALENDARQUARTERID,
	DIM_JDA_LOCATIONID,
	DIM_JDA_PRODUCTID,
	sum(CT_CFCST_QTY) over( partition by calendaryear,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID order by CALENDARQUARTERID) as CT_CFCST_QTY,
	sum(AMT_CFCST_EURO) over( partition by calendaryear,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID order by CALENDARQUARTERID) as  AMT_CFCST_EURO
from
(select dt.calendaryear,
	dt.CALENDARQUARTER,
	dt.CALENDARQUARTERID,
	t.DIM_JDA_LOCATIONID,
	t.DIM_JDA_PRODUCTID,
	sum(CT_CFCST_QTY) as CT_CFCST_QTY,
	sum(AMT_CFCST_EURO) as AMT_CFCST_EURO
from  tmp_pve_calculate_price t, dim_jda_product dp, dim_jda_location dl, dim_date dt
where t.dim_jda_productid = dp.dim_jda_productid
	and t.dim_jda_locationid = dl.dim_jda_locationid
	and t.dim_dateidstartdate = dt.dim_dateid
group by calendaryear,CALENDARQUARTER,CALENDARQUARTERID, t.DIM_JDA_LOCATIONID,t.DIM_JDA_PRODUCTID);

drop table if exists tmp_pve_flags;
create table tmp_pve_flags as
select CALENDARQUARTER
	,final_aggregation
	,UDC_GAUSS_REP_REGION
	,sum(case when calendaryear = year(current_date - INTERVAL '1' YEAR) then CT_CFCST_QTY else 0 end) as CT_CFCST_QTY_PY
	,sum(case when calendaryear = year(current_date - INTERVAL '1' YEAR) and calendarquarter = 1  then (AMT_CFCST_EURO * dl.oprates_py_03)
						when calendaryear = year(current_date - INTERVAL '1' YEAR) and calendarquarter = 2  then (AMT_CFCST_EURO * dl.oprates_py_06)
						when calendaryear = year(current_date - INTERVAL '1' YEAR) and calendarquarter = 3  then (AMT_CFCST_EURO * dl.oprates_py_09)
						when calendaryear = year(current_date - INTERVAL '1' YEAR) and calendarquarter = 4  then (AMT_CFCST_EURO * dl.oprates_py_12)
						else 0
			end) as AMT_CFCST_EURO_PY
	,sum(case when calendaryear = year(current_date) then CT_CFCST_QTY else 0 end) as CT_CFCST_QTY_CY
	,sum(case when calendaryear = year(current_date) and calendarquarter = 1  then (AMT_CFCST_EURO * dl.oprates_py_03)
						when calendaryear = year(current_date) and calendarquarter = 2  then (AMT_CFCST_EURO * dl.oprates_py_06)
						when calendaryear = year(current_date) and calendarquarter = 3  then (AMT_CFCST_EURO * dl.oprates_py_09)
						when calendaryear = year(current_date) and calendarquarter = 4  then (AMT_CFCST_EURO * dl.oprates_py_12)
						else 0
				end) as AMT_CFCST_EURO_CY
	,cast('No' as varchar(7)) as dd_NPI_EOL
	,cast('No' as varchar(7)) as dd_value_w_o_qty
	,cast(0 as decimal(36,9)) as amt_pve_PY_euro_price
	,cast(0 as decimal(36,9)) as ct_volume_eff_q
	,cast(0 as decimal(36,9)) as ct_volume_eff_pq
from tmp_pve_quarter t,dim_jda_location dl, dim_jda_product dp
where  dl.dim_jda_locationid = t.dim_jda_locationid
	and t.dim_jda_productid = dp.dim_jda_productid
group by CALENDARQUARTER ,final_aggregation	,UDC_GAUSS_REP_REGION;

update tmp_pve_flags
	set dd_NPI_EOL = case when (((trunc(CT_CFCST_QTY_CY) <> 0) AND (trunc(CT_CFCST_QTY_PY) = 0)) AND ((trunc(AMT_CFCST_EURO_CY) <> 0) AND (trunc(AMT_CFCST_EURO_PY) = 0)))
						OR (((trunc(CT_CFCST_QTY_PY) <> 0) AND (trunc(CT_CFCST_QTY_CY) = 0)) AND ((trunc(AMT_CFCST_EURO_PY) <> 0) AND (trunc(AMT_CFCST_EURO_CY) = 0)))  then 'Yes'
					else 'No' end,
		dd_value_w_o_qty = case when ((trunc(CT_CFCST_QTY_CY) = 0) AND (trunc(AMT_CFCST_EURO_CY) <> 0)) OR ((trunc(CT_CFCST_QTY_PY) = 0) AND (trunc(AMT_CFCST_EURO_PY) <> 0)) then 'Yes'
							else 'No' end;

update tmp_pve_flags t
	set amt_pve_PY_euro_price = case when ifnull(CT_CFCST_QTY_PY,0)<>0  then AMT_CFCST_EURO_PY/CT_CFCST_QTY_PY else 0 end;

update tmp_pve_flags t
	set t.ct_volume_eff_q = case when (dd_NPI_EOL = 'Yes' or dd_value_w_o_qty = 'Yes') then (AMT_CFCST_EURO_CY - AMT_CFCST_EURO_PY)
								else (CT_CFCST_QTY_CY - CT_CFCST_QTY_PY)*amt_pve_PY_euro_price end;

/* APP-6440 - Exclude KUVAN from PVE - Oana 06 June 2017 */
update tmp_pve_flags t
	set t.ct_volume_eff_q =  (AMT_CFCST_EURO_CY - AMT_CFCST_EURO_PY)
from tmp_pve_flags t,dim_jda_product dp
where t.final_aggregation = dp.final_aggregation
	and dp.branddescr in ('KUVAN-MD','KUVAN');
/* END APP-6440 - Oana 06 June 2017 */

update tmp_pve_flags t
	set t.ct_volume_eff_pq = t2.ct_volume_eff_q
from tmp_pve_flags t, tmp_pve_flags t2
where t.CALENDARQUARTER= (t2.CALENDARQUARTER+1)
	and t.final_aggregation = t2.final_aggregation
	and t.UDC_GAUSS_REP_REGION = t2.UDC_GAUSS_REP_REGION;

update fact_jda_demandforecast f
	set f.ct_pve_volume_eff = t.ct_volume_eff_q - t.ct_volume_eff_pq
	from fact_jda_demandforecast f, tmp_pve_flags t , dim_jda_location dl, dim_jda_product dp, dim_date dt
	where  f.dim_jda_locationid = dl.dim_jda_locationid
		and f.dim_jda_productid = dp.dim_jda_productid
		and f.dim_dateidstartdate = dt.dim_dateid
		and t.final_aggregation = dp.final_aggregation
		and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
		and t.calendarquarter = dt.calendarquarter
		and (date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date - INTERVAL '1' YEAR) OR date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date));

update fact_jda_demandforecast f
	set f.ct_pve_q_qty = case when year(dt.datevalue)= year(current_Date - INTERVAL '1' YEAR) then t.CT_CFCST_QTY_PY
														else t.CT_CFCST_QTY_CY  end
from fact_jda_demandforecast f, tmp_pve_flags t, dim_jda_location dl, dim_jda_product dp, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and dt.CALENDARQUARTER = t.CALENDARQUARTER
	and (date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date - INTERVAL '1' YEAR) OR date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date))
	and f.dd_type = 'Consensus Forecast QTY';

update fact_jda_demandforecast f
	set f.ct_pve_prevq_qty = case when year(dt.datevalue)= year(current_Date - INTERVAL '1' YEAR) then t.CT_CFCST_QTY_PY
							else t.CT_CFCST_QTY_CY  end
from fact_jda_demandforecast f, tmp_pve_flags t, dim_jda_location dl, dim_jda_product dp, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and (dt.CALENDARQUARTER - 1) = t.CALENDARQUARTER
	and (date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date - INTERVAL '1' YEAR) OR date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date))
	and f.dd_type = 'Consensus Forecast QTY';

update fact_jda_demandforecast f
	set f.amt_pve_q_euro = case when year(dt.datevalue)= year(current_Date - INTERVAL '1' YEAR) then t.AMT_CFCST_EURO_PY
													else t.AMT_CFCST_EURO_CY  end
from fact_jda_demandforecast f, tmp_pve_flags t, dim_jda_location dl, dim_jda_product dp, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and dt.CALENDARQUARTER = t.CALENDARQUARTER
	and (date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date - INTERVAL '1' YEAR) OR date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date))
	and f.dd_type = 'Consensus Forecast EURO';

update fact_jda_demandforecast f
	set f.amt_pve_prevq_euro = case when year(dt.datevalue)= year(current_Date - INTERVAL '1' YEAR) then t.AMT_CFCST_EURO_PY
															else t.AMT_CFCST_EURO_CY  end
from fact_jda_demandforecast f, tmp_pve_flags t, dim_jda_location dl, dim_jda_product dp, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and (dt.CALENDARQUARTER - 1) = t.CALENDARQUARTER
	and (date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date - INTERVAL '1' YEAR) OR date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date))
	and f.dd_type = 'Consensus Forecast EURO';

update fact_jda_demandforecast f
	set f.amt_pve_py_q_euro_price =	t.amt_pve_PY_euro_price
from fact_jda_demandforecast f, tmp_pve_flags t, dim_jda_location dl, dim_jda_product dp, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and (dt.CALENDARQUARTER ) = t.CALENDARQUARTER
	and (year(dt.datevalue) = year(current_Date - INTERVAL '1' YEAR) OR year(dt.datevalue) = year(current_Date))
	and f.dd_type = 'Consensus Forecast QTY';

update fact_jda_demandforecast f
	set f.amt_pve_py_prevq_euro_price = t.amt_pve_PY_euro_price
from fact_jda_demandforecast f, tmp_pve_flags t, dim_jda_location dl, dim_jda_product dp, dim_date dt
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and (dt.CALENDARQUARTER - 1 ) = t.CALENDARQUARTER
	and (year(dt.datevalue) = year(current_Date - INTERVAL '1' YEAR) OR year(dt.datevalue) = year(current_Date))
	and f.dd_type = 'Consensus Forecast QTY';

update fact_jda_demandforecast f
	set f.dd_value_w_o_qty = t.dd_value_w_o_qty,
		f.dd_NPI_EOL = t.dd_NPI_EOL
	from fact_jda_demandforecast f, tmp_pve_flags t , dim_jda_location dl, dim_jda_product dp, dim_date dt
	where  f.dim_jda_locationid = dl.dim_jda_locationid
		and f.dim_jda_productid = dp.dim_jda_productid
		and f.dim_dateidstartdate = dt.dim_dateid
		and t.final_aggregation = dp.final_aggregation
		and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
		and t.calendarquarter = dt.calendarquarter
		and (date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date - INTERVAL '1' YEAR) OR date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date));

------- YTD -------
drop table if exists tmp_pve_flags_ytd;
create table tmp_pve_flags_ytd as
	select t1.dd_month
		,t1.final_aggregation
		,t1.UDC_GAUSS_REP_REGION
		,sum(CT_CFCST_QTY_CY) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by dd_month) as CT_CFCST_QTY_CY
		,sum(AMT_CFCST_EURO_CY) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by dd_month) as AMT_CFCST_EURO_CY
		,sum(CT_CFCST_QTY_PY) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by dd_month) as CT_CFCST_QTY_PY
		,sum(AMT_CFCST_EURO_PY) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by dd_month) as AMT_CFCST_EURO_PY
		,t1.DD_NPI_EOL_YTD
		,t1.DD_VALUE_W_O_QTY_YTD
	from (select month(datevalue) as dd_month
					,final_aggregation
					,UDC_GAUSS_REP_REGION
					,trunc(sum(case when date_trunc('YEAR',datevalue) = date_trunc('YEAR',current_date) then (CT_CFCST_QTY) else 0 end )) as CT_CFCST_QTY_CY
					,trunc(sum(case when date_trunc('YEAR',datevalue) = date_trunc('YEAR',current_date) then (amt_cfcst_ytd_lc)else 0 end )) as AMT_CFCST_EURO_CY
					,trunc(sum(case when date_trunc('YEAR',datevalue) = date_trunc('YEAR',current_date-INTERVAL '1' Year) then (CT_CFCST_QTY) else 0 end)) as CT_CFCST_QTY_PY
					,trunc(sum(case when date_trunc('YEAR',datevalue) = date_trunc('YEAR',current_date-INTERVAL '1' Year) then (amt_cfcst_ytd_lc) else 0 end)) as AMT_CFCST_EURO_PY
					,cast('Yes' as varchar(7)) as dd_NPI_EOL_YTD
					,cast('Yes' as varchar(7)) as dd_value_w_o_qty_ytd
		from tmp_pve_calculate_price t, dim_jda_product dp, dim_jda_location dl
		where t.dim_jda_productid = dp.dim_jda_productid
				and t.dim_jda_locationid = dl.dim_jda_locationid
				and month(datevalue) <  month(current_date)
				and year(datevalue) between year(current_date - interval '1' year) and year(current_date)
		group by month(datevalue),final_aggregation,UDC_GAUSS_REP_REGION) t1;

update tmp_pve_flags_ytd
	set dd_NPI_EOL_YTD = case when (((CT_CFCST_QTY_CY <> 0) AND (CT_CFCST_QTY_PY = 0)) AND ((AMT_CFCST_EURO_CY <> 0) AND (AMT_CFCST_EURO_PY = 0)))
									OR (((CT_CFCST_QTY_PY <> 0) AND (CT_CFCST_QTY_CY = 0)) AND ((AMT_CFCST_EURO_PY <> 0) AND (AMT_CFCST_EURO_CY = 0)))  then 'Yes'
						 	else 'No' end,
		dd_value_w_o_qty_ytd = case when ((CT_CFCST_QTY_CY = 0) AND (AMT_CFCST_EURO_CY <> 0)) OR ((CT_CFCST_QTY_PY = 0) AND (AMT_CFCST_EURO_PY <> 0)) then 'Yes'
									else 'No' end;

------- YTD -------
drop table if exists tmp_price_ytd;
create table tmp_price_ytd as
	select t1.datevalue
		,t1.final_aggregation
		,t1.UDC_GAUSS_REP_REGION
		,sum(ct_cfcst_qty_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as ct_cfcst_qty_ytd
		,sum(amt_cfcst_lc_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as amt_cfcst_lc_ytd
		,sum(ct_cfcst_qty_ytd_corrected) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as ct_cfcst_qty_ytd_corrected
	from (select datevalue
					,dp.final_aggregation
					,dl.UDC_GAUSS_REP_REGION
					,sum(ct_cfcst_qty) as ct_cfcst_qty_ytd
					,sum(amt_cfcst_ytd_lc) as amt_cfcst_lc_ytd
					,sum(ct_cfcst_qty_corrected) as ct_cfcst_qty_ytd_corrected
			from tmp_pve_calculate_price f, dim_jda_location dl, dim_jda_product dp
			where f.dim_jda_productid = dp.DIM_JDA_PRODUCTID
				and dl.dim_jda_locationid = f.dim_jda_locationid
				and date_trunc('MONTH',f.datevalue) <  date_trunc('MONTH',current_date) and date_trunc('YEAR',f.datevalue) =  date_trunc('YEAR',current_date)
			group by datevalue,dp.final_aggregation,dl.UDC_GAUSS_REP_REGION) t1
union
select t2.datevalue
		,t2.final_aggregation
		,t2.UDC_GAUSS_REP_REGION
		,sum(ct_cfcst_qty_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as ct_cfcst_qty_ytd
		,sum(amt_cfcst_lc_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as amt_cfcst_lc_ytd
		,sum(ct_cfcst_qty_ytd_corrected) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as ct_cfcst_qty_ytd_corrected
from (select datevalue
				,dp.final_aggregation
				,dl.UDC_GAUSS_REP_REGION
				,sum(ct_cfcst_qty) as ct_cfcst_qty_ytd
				,sum(amt_cfcst_ytd_lc) as amt_cfcst_lc_ytd
				,sum(ct_cfcst_qty_corrected) as ct_cfcst_qty_ytd_corrected
			from tmp_pve_calculate_price f, dim_jda_location dl, dim_jda_product dp
			where f.dim_jda_productid = dp.DIM_JDA_PRODUCTID
				and dl.dim_jda_locationid = f.dim_jda_locationid
				and date_trunc('MONTH',f.datevalue) <  date_trunc('MONTH',current_date - interval '1' year) and date_trunc('YEAR',f.datevalue) =  date_trunc('YEAR',current_date - interval '1' year)
			group by datevalue,dp.final_aggregation,dl.UDC_GAUSS_REP_REGION) t2;

update fact_jda_demandforecast f
	set f.ct_pve_ytd_qty = t.ct_cfcst_qty_ytd,
			f.CT_PACKS_TOCORRECT_YTD = t.ct_cfcst_qty_ytd_corrected
from fact_jda_demandforecast f
	inner join dim_jda_product dp on f.dim_jda_productid = dp.dim_jda_productid
	inner join dim_jda_location dl on f.dim_jda_locationid = dl.dim_jda_locationid
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join tmp_price_ytd t on dt.datevalue = (t.datevalue + interval '1' month)
			and dp.final_aggregation = t.final_aggregation
			and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
where f.dd_type = 'Consensus Forecast QTY'
	and year(dt.datevalue) between year(current_date - interval '1' year) and year(current_date);

update fact_jda_demandforecast f
	set f.amt_pve_ytd_euro = amt_cfcst_lc_ytd
from fact_jda_demandforecast f
	inner join dim_jda_product dp on f.dim_jda_productid = dp.dim_jda_productid
	inner join dim_jda_location dl on f.dim_jda_locationid = dl.dim_jda_locationid
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join tmp_price_ytd t on  dt.datevalue = (t.datevalue + interval '1' month)
			and dp.final_aggregation = t.final_aggregation
			and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
where f.dd_type = 'Consensus Forecast EURO'
	and year(dt.datevalue) between year(current_date - interval '1' year) and year(current_date);

update fact_jda_demandforecast f
	set f.amt_pve_ytd_euro_price = case when ifnull(ct_cfcst_qty_ytd,0) <> 0 then ifnull(t.amt_cfcst_lc_ytd,0)/ifnull(ct_cfcst_qty_ytd,0)
																		else 0 end
from fact_jda_demandforecast f
inner join dim_jda_product dp on f.dim_jda_productid = dp.dim_jda_productid
	inner join dim_jda_location dl on f.dim_jda_locationid = dl.dim_jda_locationid
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join tmp_price_ytd t on month(dt.datevalue) = month(t.datevalue + interval '1' month)
			and dp.final_aggregation = t.final_aggregation
			and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
			and year(t.datevalue) = year(current_Date - INTERVAL '1' YEAR)
where year(dt.datevalue) between year(current_Date - INTERVAL '1' YEAR) and year(current_Date)
	and f.dd_type = 'Consensus Forecast QTY';

---count
drop table if exists tmp_pve_count;
create table tmp_pve_count as
select f.dim_dateidstartdate
	,f.dd_planning_item_id 
	,f.dim_jda_locationid 
	,f.dim_jda_productid
	,dd_type
	,count(CALENDARQUARTERID) over(partition by  CALENDARQUARTERID,UDC_GAUSS_REP_REGION,final_aggregation,dd_type) as ct_count
	,count(datevalue) over(partition by  datevalue,UDC_GAUSS_REP_REGION,final_aggregation,dd_type) as ct_count_mth_ytd
	,count(distinct calendaryear) over (partition by UDC_GAUSS_REP_REGION,final_aggregation,dd_type) as ct_count_years
	,count(distinct dd_type) over(partition by  CALENDARQUARTERID,UDC_GAUSS_REP_REGION,final_aggregation) as ct_count_type
from fact_jda_demandforecast f,dim_jda_location dl, dim_jda_product dp, dim_date dt, dim_jda_component dc
where  f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and f.dim_jda_componentid = dc.dim_jda_componentid
	and dl.location_name not like 'REP%' 
	and dl.location_name not like '%DISTR'
	and (date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date - INTERVAL '1' YEAR) OR date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date))
	and (dd_type in ('Consensus Forecast EURO','Consensus Forecast QTY') or c_type3 in ('OP EURO','OP'));

update fact_jda_demandforecast f
	set f.ct_count = t.ct_count
from fact_jda_demandforecast f,tmp_pve_count t
where f.dim_dateidstartdate = t.dim_dateidstartdate
	and f.dd_planning_item_id = t.dd_planning_item_id 
	and f.dim_jda_locationid = t.dim_jda_locationid
	and f.dim_jda_productid = t.dim_jda_productid
	and f.dd_type = t.dd_type;	
	
update fact_jda_demandforecast f
	set f.ct_count_mth_ytd = t.ct_count_mth_ytd
from fact_jda_demandforecast f,tmp_pve_count t
where f.dim_dateidstartdate = t.dim_dateidstartdate
	and f.dd_planning_item_id = t.dd_planning_item_id
	and f.dim_jda_locationid = t.dim_jda_locationid
	and f.dim_jda_productid = t.dim_jda_productid
	and f.dd_type = t.dd_type;

update fact_jda_demandforecast f
	set f.ct_count_type = t.ct_count_type
from fact_jda_demandforecast f,tmp_pve_count t
where f.dim_dateidstartdate = t.dim_dateidstartdate
	and f.dd_planning_item_id = t.dd_planning_item_id 
	and f.dim_jda_locationid = t.dim_jda_locationid
	and f.dim_jda_productid = t.dim_jda_productid
	and f.dd_type = t.dd_type;

update fact_jda_demandforecast f
	set f.ct_pve_count_years = t.ct_count_years
from fact_jda_demandforecast f,tmp_pve_count t
where f.dim_dateidstartdate = t.dim_dateidstartdate
	and f.dd_planning_item_id = t.dd_planning_item_id
	and f.dim_jda_locationid = t.dim_jda_locationid
	and f.dim_jda_productid = t.dim_jda_productid
	and f.dd_type = t.dd_type;

update fact_jda_demandforecast f
		set f.ct_pve_alternative_qty = t.ct_cfcst_qty
	from fact_jda_demandforecast f, tmp_pve_calculate_price t
	where  f.dim_dateidstartdate = t.dim_dateidstartdate
		and f.dd_planning_item_id = t.dd_planning_item_id 
		and f.dim_jda_locationid = t.dim_jda_locationid
		and f.dim_jda_productid = t.dim_jda_productid
		and f.dd_type = 'Consensus Forecast QTY';

update fact_jda_demandforecast f
	set f.dd_value_w_o_qty_ytd = t.dd_value_w_o_qty_ytd,
			f.dd_NPI_EOL_YTD = t.dd_NPI_EOL_YTD
	from fact_jda_demandforecast f, tmp_pve_flags_YTD t , dim_jda_location dl, dim_jda_product dp, dim_date dt
	where f.dim_dateidstartdate = dt.dim_dateid
		and f.dim_jda_locationid = dl.dim_jda_locationid
		and f.dim_jda_productid = dp.dim_jda_productid
		and t.final_aggregation = dp.final_aggregation
		and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
		and  month(dt.datevalue) = (dd_month + 1)
		and (year(dt.datevalue) between year(current_Date - INTERVAL '1' YEAR) and year(current_Date));

----- Consensus Forecast vs. OP ------
drop table if exists tmp_pve_cfop_beforeFiltering;
create table tmp_pve_cfop_beforeFiltering as
select datevalue
		 ,f.dim_dateidstartdate
		 ,f.dd_planning_item_id
		 ,f.dim_jda_locationid
		 ,f.dim_jda_productid
		 ,sum(case when f.dd_type = 'Consensus Forecast QTY' and dp.FINAL_SBU <> 'No' then ((ct_packs+ct_packs_tocorrect) * dmp.COMPLETECONTENTOFTHEPACKAGE)
					when f.dd_type = 'Consensus Forecast QTY' and dp.FINAL_SBU = 'No' then (ct_packs+ct_packs_tocorrect)
					else 0 end) as ct_cf_qty
		,sum(case when f.dd_type = 'Consensus Forecast LC' then (ct_packs * EXCHRATE) else 0 end) as amt_cf_euro
		,sum(case when dc.c_type3 = 'OP' and dp.FINAL_SBU <> 'No' then ((ct_packs+ct_packs_tocorrect) * dmp.COMPLETECONTENTOFTHEPACKAGE)
					when dc.c_type3 = 'OP' and dp.FINAL_SBU = 'No' then (ct_packs+ct_packs_tocorrect)
					else 0 end) as ct_op_qty
		,sum(case when c_type3 = 'OP EURO' then (ct_packs+ct_packs_tocorrect)  else 0 end) as amt_op_euro
		,sum(case when f.dd_type = 'Consensus Forecast QTY' then ct_packs_tocorrect else 0 end) as ct_cf_qty_corrected
		,sum(case when dc.c_type3 = 'OP'then ct_packs_tocorrect else 0 end) as ct_op_qty_corrected
	from fact_jda_demandforecast f, dim_Date dt, dim_jda_location dl, dim_mdg_part dmp, dim_jda_component dc, dim_jda_product dp
	where dt.dim_dateid = f.dim_dateidstartdate
		and dl.dim_jda_locationid = f.dim_jda_locationid
		and dp.dim_jda_productid = f.dim_jda_productid
		and dmp.dim_mdg_partid = f.dim_mdg_partid
		and f.dim_jda_componentid = dc.dim_jda_componentid
		and date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_date)
		and (f.dd_type in ('Consensus Forecast QTY','Consensus Forecast LC') or dc.c_type3 in ('OP EURO','OP'))
		and dl.UDC_GAUSS_REP_REGION not in ('VE VENEZUELA')
		---and dp.branddescr not in ('KUVAN-MD','KUVAN')
		and (location_name not like 'REP%' and location_name not like '%DISTR' and dl.location_name <> '350PK')
		and f.dd_cmg_id not in (1056,1755)
	group by datevalue	,f.dim_dateidstartdate	,f.dd_planning_item_id	,f.dim_jda_locationid	,f.dim_jda_productid;

drop table if exists tmp_pve_cfop_beforeFiltering_agg;
create table tmp_pve_cfop_beforeFiltering_agg as
select f.dim_jda_locationid
	,f.dim_jda_productid
	,sum(CT_CF_QTY) CT_CF_QTY
	,sum(AMT_CF_EURO) AMT_CF_EURO
from  tmp_pve_cfop_beforeFiltering f
group by f.dim_jda_locationid
	,f.dim_jda_productid;

drop table if exists tmp_pve_cfop_filterout;
create table tmp_pve_cfop_filterout as
select distinct f.dim_jda_productid , dim_jda_locationid
from  tmp_pve_cfop_beforeFiltering_agg f
where (trunc(ct_cf_qty) >= 0
	and trunc(amt_cf_euro) = 0
	and f.dim_jda_productid in (select tdp.dim_jda_productid from tmp_product_toexclude tdp));

drop table if exists tmp_pve_cfop_calculate_price;
create table tmp_pve_cfop_calculate_price as
	select f.*
	from tmp_pve_cfop_beforeFiltering f
	 	left outer join tmp_pve_cfop_filterout t on f.dim_jda_productid = t.dim_jda_productid
				and  f.dim_jda_locationid = t.dim_jda_locationid
	where  t.dim_jda_productid is null
		and t.dim_jda_locationid is null;

----- Consensus Forecast vs. OP - YTD ---------
drop table if exists tmp_pve_cfop_flags_ytd;
create table tmp_pve_cfop_flags_ytd as
select t1.DATEVALUE
	,t1.FINAL_AGGREGATION
	,t1.UDC_GAUSS_REP_REGION
	,sum(CT_CF_QTY_YTD) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as CT_CF_QTY_YTD
	,sum(AMT_CFCST_EURO_YTD)  over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as AMT_CFCST_EURO_YTD
	,sum(CT_OP_QTY_YTD) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as CT_OP_QTY_YTD
	,sum(AMT_OP_EURO_YTD) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as AMT_OP_EURO_YTD
	,DD_NPI_EOL_CFOP_YTD
	,DD_VALUE_W_O_QTY_CFOP_YTD
from( select datevalue
		,final_aggregation
		,UDC_GAUSS_REP_REGION
		,trunc(sum(CT_CF_QTY)) as ct_cf_qty_ytd
		,trunc(sum(AMT_CF_EURO)) as amt_cfcst_euro_ytd
		,trunc(sum(CT_OP_QTY)) as ct_op_qty_ytd
		,trunc(sum(AMT_OP_EURO)) as amt_op_euro_ytd
		,cast('No' as varchar(7)) as dd_NPI_EOL_cfop_YTD
		,cast('No' as varchar(7)) as dd_value_w_o_qty_cfop_ytd
	from tmp_pve_cfop_calculate_price t, dim_jda_product dp, dim_jda_location dl
	where t.dim_jda_productid = dp.dim_jda_productid
		and t.dim_jda_locationid = dl.dim_jda_locationid
		and date_trunc('MONTH',datevalue) <  date_trunc('MONTH',current_date) and date_trunc('YEAR',datevalue) =  date_trunc('YEAR',current_date)
	group by datevalue, final_aggregation,UDC_GAUSS_REP_REGION) t1;

update tmp_pve_cfop_flags_ytd
	set dd_NPI_EOL_cfop_YTD = case when (((ct_cf_qty_ytd <> 0) AND (ct_op_qty_ytd = 0)) AND ((amt_cfcst_euro_ytd <> 0) AND (amt_op_euro_ytd = 0)))
									OR (((ct_op_qty_ytd <> 0) AND (ct_cf_qty_ytd = 0)) AND ((amt_op_euro_ytd <> 0) AND (amt_cfcst_euro_ytd = 0)))  then 'Yes'
						 	else 'No' end,
		dd_value_w_o_qty_cfop_ytd = case when ((ct_cf_qty_ytd = 0) AND (amt_cfcst_euro_ytd <> 0)) OR ((ct_op_qty_ytd = 0) AND (amt_op_euro_ytd <> 0)) then 'Yes'
									else 'No' end;

update fact_jda_demandforecast f
	set f.dd_NPI_EOL_cfop_YTD = t.dd_NPI_EOL_cfop_YTD,
		f.dd_value_w_o_qty_cfop_ytd = t.dd_value_w_o_qty_cfop_ytd
	from fact_jda_demandforecast f, tmp_pve_cfop_flags_ytd t , dim_jda_location dl, dim_jda_product dp, dim_date dt, dim_jda_component dc
	where f.dim_dateidstartdate = dt.dim_dateid
		and f.dim_jda_locationid = dl.dim_jda_locationid
		and f.dim_jda_productid = dp.dim_jda_productid
		and f.dim_jda_componentid = dc.dim_jda_componentid
		and t.final_aggregation = dp.final_aggregation
		and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
		and (t.datevalue + interval '1' month) = dt.datevalue
		and (dd_type in ('Consensus Forecast QTY','Consensus Forecast EURO') or c_type3 in ('OP EURO','OP'));

drop table if exists tmp_price_cf_op_ytd;
create table tmp_price_cf_op_ytd as
	select t1.datevalue
	,t1.final_aggregation
	,t1.UDC_GAUSS_REP_REGION
	,sum(ct_cfcst_qty_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as ct_cfcst_qty_ytd
	,sum(amt_cfcst_euro_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as amt_cfcst_euro_ytd
	,sum(ct_op_qty_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as ct_op_qty_ytd
	,sum(amt_op_euro_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as amt_op_euro_ytd
	,sum(ct_cf_qty_corrected_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as ct_cf_qty_corrected_ytd
	,sum(ct_op_qty_corrected_ytd) over(partition by UDC_GAUSS_REP_REGION,final_aggregation order by datevalue) as ct_op_qty_corrected_ytd
	from (select datevalue
			,dp.final_aggregation
			,dl.UDC_GAUSS_REP_REGION
			,sum(ct_cf_qty) as ct_cfcst_qty_ytd
			,sum(amt_cf_euro) as amt_cfcst_euro_ytd
			,sum(ct_op_qty) as ct_op_qty_ytd
			,sum(amt_op_euro) as amt_op_euro_ytd
			,sum(ct_cf_qty_corrected) as ct_cf_qty_corrected_ytd
			,sum(ct_op_qty_corrected) as ct_op_qty_corrected_ytd
			from tmp_pve_cfop_calculate_price f, dim_jda_location dl, dim_jda_product dp
			where f.dim_jda_productid = dp.DIM_JDA_PRODUCTID
				and dl.dim_jda_locationid = f.dim_jda_locationid
				and date_trunc('MONTH',f.datevalue) <  date_trunc('MONTH',current_date) and date_trunc('YEAR',f.datevalue) =  date_trunc('YEAR',current_date)
			group by datevalue,dp.final_aggregation,dl.UDC_GAUSS_REP_REGION)t1;

update fact_jda_demandforecast f
	set f.ct_pve_cfop_ytd_qty = (case when f.dd_type = 'Consensus Forecast QTY' then ifnull(t.ct_cfcst_qty_ytd,0)
									when dc.c_type3 = 'OP' then ifnull(t.ct_op_qty_ytd,0)
									else 0 end),
			f.CT_PACKS_TOCORRECT_YTD = 	(case when f.dd_type = 'Consensus Forecast QTY' then ifnull(t.ct_cf_qty_corrected_ytd,0)
											when dc.c_type3 = 'OP' then ifnull(t.ct_op_qty_corrected_ytd,0)
											else 0 end)
from fact_jda_demandforecast f
	inner join dim_jda_product dp on f.dim_jda_productid = dp.dim_jda_productid
	inner join dim_jda_location dl on f.dim_jda_locationid = dl.dim_jda_locationid
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join dim_jda_component dc on f.dim_jda_componentid = dc.dim_jda_componentid
	inner join tmp_price_cf_op_ytd t	on dt.datevalue = t.datevalue + interval '1' month
		and t.final_aggregation = dp.final_aggregation
		and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
where (dd_type = 'Consensus Forecast QTY'  or dc.c_type3 = 'OP')
		and year(dt.datevalue) = year(current_date);

update fact_jda_demandforecast f
	set f.amt_pve_cfop_ytd_euro = (case when f.dd_type = 'Consensus Forecast EURO' then ifnull(t.amt_cfcst_euro_ytd,0)
									when dc.c_type3 = 'OP EURO' then ifnull(t.amt_op_euro_ytd,0)
									else 0 end)
from fact_jda_demandforecast f
	inner join dim_jda_product dp on f.dim_jda_productid = dp.dim_jda_productid
	inner join dim_jda_location dl on f.dim_jda_locationid = dl.dim_jda_locationid
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join dim_jda_component dc on f.dim_jda_componentid = dc.dim_jda_componentid
	inner join tmp_price_cf_op_ytd t	on dt.datevalue = t.datevalue + interval '1' month
		and t.final_aggregation = dp.final_aggregation
		and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
where (f.dd_type = 'Consensus Forecast EURO' or dc.c_type3 = 'OP EURO')
	and year(dt.datevalue) = year(current_date);


update fact_jda_demandforecast f
	set f.amt_pve_cfop_ytd_euro_price = (case when ifnull(t.ct_op_qty_ytd,0) <>0 then ifnull(t.amt_op_euro_ytd,0)/ifnull(t.ct_op_qty_ytd,0)  else 0 end )
from fact_jda_demandforecast f
	inner join dim_jda_product dp on f.dim_jda_productid = dp.dim_jda_productid
	inner join dim_jda_location dl on f.dim_jda_locationid = dl.dim_jda_locationid
	inner join dim_date dt on f.dim_dateidstartdate = dt.dim_dateid
	inner join dim_jda_component dc on f.dim_jda_componentid = dc.dim_jda_componentid
	inner join tmp_price_cf_op_ytd t	on dt.datevalue = t.datevalue + interval '1' month
		and t.final_aggregation = dp.final_aggregation
		and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
where (dd_type = 'Consensus Forecast QTY'  or dc.c_type3 = 'OP')
		and year(dt.datevalue) = year(current_date);

----- Consensus Forecast vs. OP - Quarter ---------
drop table if exists tmp_pve_price_cf_op_quarter;
create table tmp_pve_price_cf_op_quarter as
select CALENDARQUARTERID
	,final_aggregation
	,UDC_GAUSS_REP_REGION
	,sum(qty_cf_quarter) over(partition by calendaryear,final_aggregation, UDC_GAUSS_REP_REGION order by CALENDARQUARTERID) as qty_cf_quarter
	,sum(euro_cf_quarter)  over(partition by calendaryear,final_aggregation, UDC_GAUSS_REP_REGION order by CALENDARQUARTERID) as euro_cf_quarter
	,sum(qty_op_quarter) over(partition by calendaryear,final_aggregation, UDC_GAUSS_REP_REGION order by CALENDARQUARTERID) as qty_op_quarter
	,sum(euro_op_quarter) over(partition by calendaryear,final_aggregation, UDC_GAUSS_REP_REGION order by CALENDARQUARTERID) as euro_op_quarter
	,cast('No' as varchar(7)) as dd_NPI_EOL_cfop_q
	,cast('No' as varchar(7)) as dd_value_w_o_qty_cfop_q
	,cast(0 as decimal(36,9)) as amt_pve_po_euro_price
	,cast(0 as decimal(36,9)) as ct_volume_eff_q
	,cast(0 as decimal(36,9)) as ct_volume_eff_pq
from (
select calendaryear
	,dt.CALENDARQUARTERID
	,dp.final_aggregation
	,dl.UDC_GAUSS_REP_REGION
	,sum(ct_cf_qty) as qty_cf_quarter
	,sum(amt_cf_euro) as euro_cf_quarter
	,sum(ct_op_qty) as qty_op_quarter
	,sum(amt_op_euro) as euro_op_quarter
from tmp_pve_cfop_calculate_price f, dim_jda_location dl, dim_jda_product dp, dim_date dt
where f.dim_jda_productid = dp.DIM_JDA_PRODUCTID
	and dl.dim_jda_locationid = f.dim_jda_locationid
	and f.dim_dateidstartdate = dt.dim_dateid
group by  calendaryear, CALENDARQUARTERID, final_aggregation, UDC_GAUSS_REP_REGION);

update tmp_pve_price_cf_op_quarter
	set dd_NPI_EOL_cfop_q = case when (((trunc(qty_cf_quarter) <> 0) AND (trunc(qty_op_quarter) = 0)) AND ((trunc(euro_cf_quarter) <> 0) AND (trunc(euro_op_quarter) = 0)))
									OR (((trunc(qty_op_quarter) <> 0) AND (trunc(qty_cf_quarter) = 0)) AND ((trunc(euro_op_quarter) <> 0) AND (trunc(euro_cf_quarter) = 0)))  then 'Yes'
						 	else 'No' end,
		dd_value_w_o_qty_cfop_q = case when ((trunc(qty_cf_quarter) = 0) AND (trunc(euro_cf_quarter) <> 0)) OR ((trunc(qty_op_quarter) = 0) AND (trunc(euro_op_quarter) <> 0)) then 'Yes'
									else 'No' end;

update tmp_pve_price_cf_op_quarter t
	set amt_pve_po_euro_price = case when qty_op_quarter<>0  then euro_op_quarter/qty_op_quarter else 0 end;

update tmp_pve_price_cf_op_quarter t
	set t.ct_volume_eff_q = case when (dd_NPI_EOL_cfop_q = 'Yes' or dd_value_w_o_qty_cfop_q = 'Yes') then (euro_cf_quarter - euro_op_quarter)
								else (qty_cf_quarter - qty_op_quarter)*amt_pve_po_euro_price end;

update tmp_pve_price_cf_op_quarter t
	set t.ct_volume_eff_q =  (euro_cf_quarter - euro_op_quarter)
from tmp_pve_price_cf_op_quarter t,dim_jda_product dp
where t.final_aggregation = dp.final_aggregation
	and dp.branddescr in ('KUVAN-MD','KUVAN');

update tmp_pve_price_cf_op_quarter t
	set t.ct_volume_eff_pq = t2.ct_volume_eff_q
from tmp_pve_price_cf_op_quarter t, tmp_pve_price_cf_op_quarter t2
where t.CALENDARQUARTERID= (t2.CALENDARQUARTERID+1)
	and t.final_aggregation = t2.final_aggregation
	and t.UDC_GAUSS_REP_REGION = t2.UDC_GAUSS_REP_REGION;

update fact_jda_demandforecast f
	set f.ct_pve_cfop_volume_eff = t.ct_volume_eff_q - t.ct_volume_eff_pq
from fact_jda_demandforecast f, tmp_pve_price_cf_op_quarter t , dim_jda_location dl, dim_jda_product dp, dim_date dt
where  f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and f.dim_dateidstartdate = dt.dim_dateid
	and t.final_aggregation = dp.final_aggregation
	and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
	and t.calendarquarterid = dt.calendarquarterid
	and date_trunc('YEAR',dt.datevalue) = date_trunc('YEAR',current_Date);

update fact_jda_demandforecast f
	set f.ct_pve_cfop_q_qty = case when f.dd_type = 'Consensus Forecast QTY' then t.qty_cf_quarter
								when dc.c_type3 = 'OP' then t.qty_op_quarter
							end,
			f.amt_pve_cfop_q_euro_price =  t.amt_pve_po_euro_price
from fact_jda_demandforecast f, tmp_pve_price_cf_op_quarter t, dim_jda_location dl, dim_jda_product dp, dim_date dt, dim_jda_component dc
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and f.dim_jda_componentid = dc.dim_jda_componentid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and dt.CALENDARQUARTERID = t.CALENDARQUARTERID
	and (f.dd_type = 'Consensus Forecast QTY' or dc.c_type3 = 'OP');

update fact_jda_demandforecast f
	set f.amt_pve_cfop_q_euro  = case when f.dd_type = 'Consensus Forecast EURO' then t.euro_cf_quarter
								when dc.c_type3 = 'OP EURO' then t.euro_op_quarter
							end
from fact_jda_demandforecast f, tmp_pve_price_cf_op_quarter t, dim_jda_location dl, dim_jda_product dp, dim_date dt,dim_jda_component dc
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and f.dim_jda_componentid = dc.dim_jda_componentid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and dt.CALENDARQUARTERID = t.CALENDARQUARTERID
	and (f.dd_type = 'Consensus Forecast EURO' or dc.c_type3 = 'OP EURO');

update fact_jda_demandforecast f
	set f.ct_pve_cfop_prevq_qty = case when f.dd_type = 'Consensus Forecast QTY' then ifnull(t.qty_cf_quarter,0)
										when dc.c_type3 = 'OP' then ifnull(t.qty_op_quarter,0)
									end,
			f.amt_pve_cfop_prevq_euro_price =  t.amt_pve_po_euro_price
from fact_jda_demandforecast f, tmp_pve_price_cf_op_quarter t, dim_jda_location dl, dim_jda_product dp, dim_date dt, dim_jda_component dc
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and f.dim_jda_componentid = dc.dim_jda_componentid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and t.CALENDARQUARTERID = (dt.CALENDARQUARTERID-1)
	and (f.dd_type = 'Consensus Forecast QTY' or dc.c_type3 = 'OP');

update fact_jda_demandforecast f
	set f.amt_pve_cfop_prevq_euro = case when dd_type = 'Consensus Forecast EURO' then ifnull(t.euro_cf_quarter,0)
										when dc.c_type3 = 'OP EURO' then ifnull(t.euro_op_quarter,0)
									end
from fact_jda_demandforecast f, tmp_pve_price_cf_op_quarter t, dim_jda_location dl, dim_jda_product dp, dim_date dt, dim_jda_component dc
where f.dim_dateidstartdate = dt.dim_dateid
	and f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and f.dim_jda_componentid = dc.dim_jda_componentid
	and dp.final_aggregation = t.final_aggregation
	and dl.UDC_GAUSS_REP_REGION = t.UDC_GAUSS_REP_REGION
	and t.CALENDARQUARTERID = (dt.CALENDARQUARTERID-1)
	and (f.dd_type = 'Consensus Forecast EURO' or dc.c_type3 = 'OP EURO');

update fact_jda_demandforecast f
		set f.ct_pve_alternative_qty = t.ct_op_qty
from fact_jda_demandforecast f, tmp_pve_cfop_calculate_price t, dim_jda_component dc
where  f.dim_dateidstartdate = t.dim_dateidstartdate
		and f.dd_planning_item_id = t.dd_planning_item_id
		and f.dim_jda_locationid = t.dim_jda_locationid
		and f.dim_jda_productid = t.dim_jda_productid
		and f.dim_jda_componentid = dc.dim_jda_componentid
		and dc.c_type3 = 'OP';

update fact_jda_demandforecast f
	set f.dd_NPI_EOL_cfop_q = t.dd_NPI_EOL_cfop_q,
			f.dd_value_w_o_qty_cfop_q = t.dd_value_w_o_qty_cfop_q
from fact_jda_demandforecast f, tmp_pve_price_cf_op_quarter t , dim_jda_location dl, dim_jda_product dp, dim_date dt, dim_jda_component dc
where f.dim_dateidstartdate = dt.dim_dateid
		and f.dim_jda_locationid = dl.dim_jda_locationid
		and f.dim_jda_productid = dp.dim_jda_productid
		and f.dim_jda_componentid = dc.dim_jda_componentid
		and t.final_aggregation = dp.final_aggregation
		and t.UDC_GAUSS_REP_REGION = dl.UDC_GAUSS_REP_REGION
		and t.CALENDARQUARTERID = dt.CALENDARQUARTERID
		and (dd_type in ('Consensus Forecast QTY','Consensus Forecast EURO') or c_type3 in ('OP EURO','OP'));
/* END PVE - Oana 17 Jan 2017 */

update fact_jda_demandforecast
	set dw_update_date = current_timestamp;


/* Temporary fix for JDA harmonization - Oana 04 March 2017 */
update EMD586.dim_jda_dateholder d1
set d1.CURRENTDATE = d2.CURRENTDATE,
	d1.DIM_JDA_DATEHOLDERID = d2.DIM_JDA_DATEHOLDERID,
	d1.LAG0_MONTH = d2.LAG0_MONTH,
	d1.LAG1_MONTH = d2.LAG1_MONTH,
	d1.LAST_SATURDAY_MONTH = d2.LAST_SATURDAY_MONTH,
	d1.PROJECTSOURCEID = d2.PROJECTSOURCEID
from EMD586.dim_jda_dateholder d1, EMDTEMPOCC4.dim_jda_dateholder d2
where d1.dim_jda_dateholderid = d2.dim_jda_dateholderid;

insert into  EMD586.dim_jda_location (COUNTRY,DIM_JDA_LOCATIONID,DPPLANNER,DW_INSERT_DATE,DW_UPDATE_DATE,LOC,LOCATION_ID,LOCATION_NAME,LOCDESCR,LOCGROUPING,
	MARKET,PLANNIG_ITEM_FLAG,PROJECTSOURCEID,REGION,REGULFLEX,REPORTING_COUNTRY,REPORTING_REGION,RESPCMG,ROWCHANGEREASON,
	ROWENDDATE,ROWISCURRENT,ROWSTARTDATE,SCMANAGER,TPREGION,EXCHRATE,UDC_GAUSS_REP_REGION,UDA_CURRENCY_NAME,OPRATES_PY_01,
	OPRATES_PY_02,OPRATES_PY_03,OPRATES_PY_04,OPRATES_PY_05,OPRATES_PY_06,OPRATES_PY_07,OPRATES_PY_08,OPRATES_PY_09,
	OPRATES_PY_10,OPRATES_PY_11,OPRATES_PY_12,ACTUALRATE_PY_YTD)
select COUNTRY,
		DIM_JDA_LOCATIONID,
		DPPLANNER,
		DW_INSERT_DATE,
		DW_UPDATE_DATE,
		LOC,
		LOCATION_ID,
		LOCATION_NAME,
		LOCDESCR,
		LOCGROUPING,
		MARKET,
		PLANNIG_ITEM_FLAG,
		PROJECTSOURCEID,
		REGION,
		REGULFLEX,
		REPORTING_COUNTRY,
		REPORTING_REGION,
		RESPCMG,
		ROWCHANGEREASON,
		ROWENDDATE,
		ROWISCURRENT,
		ROWSTARTDATE,
		SCMANAGER,
		TPREGION,
		EXCHRATE,
		UDC_GAUSS_REP_REGION,
		UDA_CURRENCY_NAME,
		OPRATES_PY_01,
		OPRATES_PY_02,
		OPRATES_PY_03,
		OPRATES_PY_04,
		OPRATES_PY_05,
		OPRATES_PY_06,
		OPRATES_PY_07,
		OPRATES_PY_08,
		OPRATES_PY_09,
		OPRATES_PY_10,
		OPRATES_PY_11,
		OPRATES_PY_12,
		ACTUALRATE_PY_YTD
from EMDTEMPOCC4.dim_jda_location d1
where not exists(select 'x' from EMD586.dim_jda_location d2 where d1.DIM_JDA_LOCATIONID = d2.DIM_JDA_LOCATIONID);

INSERT INTO EMD586.dim_jda_product (BRANDDESCR,BRANDTOSIZE,CONTAINER,DIM_JDA_PRODUCTID,DOSETOIU,DOSETOMCG,
	DW_INSERT_DATE,DW_UPDATE_DATE,FAMILYGROUPDESCR,GAUSSGLOBALCODE,GBU,GPS,ITEMDESCR,ITEMGLOBALCODE,ITEMGROUPING,
	PACKER,PACKSIZE,PHARMAFORM,PLANNIG_ITEM_FLAG,PRODUCT_ID,PRODUCT_NAME,PROJECTSOURCEID,ROWCHANGEREASON,ROWENDDATE,
	ROWISCURRENT,ROWSTARTDATE,SBU,SBUDESCR,SECPACKPERBOX,SOPSW,STRENGTHDESCR,SUPPLIERNAME,UDA_BOXTODOSES,UDA_DOSETOIU,
	UDA_DOSETOMCG,UDA_SIZETOBRAND,UDC_SBU_DESC_YPLUS1,UDC_BUSINESS_LINE,UDA_GPH,UDA_GPHDESCR,FINAL_SBU,FINAL_AGGREGATION,
	UDA_GPSNAME)
SELECT BRANDDESCR,
		BRANDTOSIZE,
		CONTAINER,
		DIM_JDA_PRODUCTID,
		DOSETOIU,
		DOSETOMCG,
		DW_INSERT_DATE,
		DW_UPDATE_DATE,
		FAMILYGROUPDESCR,
		GAUSSGLOBALCODE,
		GBU,
		GPS,
		ITEMDESCR,
		ITEMGLOBALCODE,
		ITEMGROUPING,
		PACKER,
		PACKSIZE,
		PHARMAFORM,
		PLANNIG_ITEM_FLAG,
		PRODUCT_ID,
		PRODUCT_NAME,
		PROJECTSOURCEID,
		ROWCHANGEREASON,
		ROWENDDATE,
		ROWISCURRENT,
		ROWSTARTDATE,
		SBU,
		SBUDESCR,
		SECPACKPERBOX,
		SOPSW,
		STRENGTHDESCR,
		SUPPLIERNAME,
		UDA_BOXTODOSES,
		UDA_DOSETOIU,
		UDA_DOSETOMCG,
		UDA_SIZETOBRAND,
		UDC_SBU_DESC_YPLUS1,
		UDC_BUSINESS_LINE,
		UDA_GPH,
		UDA_GPHDESCR,
		FINAL_SBU,
		FINAL_AGGREGATION,
		UDA_GPSNAME
FROM EMDTEMPOCC4.dim_jda_product d1
WHERE not exists( select 'x' from EMD586.dim_jda_product d2 where d1.DIM_JDA_PRODUCTID = d2.DIM_JDA_PRODUCTID);

update EMDTEMPOCC4.fact_jda_demandforecast set dim_projectsourceid = 1 where ifnull(dim_projectsourceid,-1) <> ifnull(1,-2);
drop table if exists emd586.fact_jda_demandforecast_hmn_new_table;
create table emd586.fact_jda_demandforecast_hmn_new_table AS
	select AMT_PVE_PRICE_EURO_Q_PREV_YEAR,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVGFCST,CT_AVGSALESPMONTH,CT_AVG_MKTSFA_12M,CT_AVG_STATSFA_12M,CT_CURRRATE,CT_EQ_UNIT,CT_IU,CT_LAG0_EQ_UNIT,CT_LAG0_PACKS_COR,CT_LAG0_UNITS_COR,CT_LAG1_EQ_UNIT,CT_LAG1_PACKS_COR,CT_LAG1_UNITS_COR,CT_MCG,CT_MKTSFA,CT_OP_EURO,CT_PACKS,CT_PACKS_COR,CT_PRICE,CT_PRICE_EURO_AVG_QUARTER,CT_PRICE_EURO_AVG_YEAR,CT_PRODLOCATION,CT_STATSFA,CT_UDA_EXCHRATE,CT_UNITS,CT_UNITS_COR,CT_VOLATILITY,DD_ABC_CLASS,DD_CURRENTDATE,DD_DFUCLASS,DD_DFULIFECYCLE,DD_FCSTLEVEL,DD_GAUSSITEMCODE,DD_ITEMLOCALCODE,DD_LASTSATURDAYCPM,DD_LOCALBRAND,DD_MODEL,DD_NDLASTSATURDAYCPM,DD_PLANNIG_ITEM_FLAG,DD_PLANNING_ITEM_ID,DD_SEGMENTATION,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_TYPE,DD_TYPE_2,DD_TYPE_3,DIM_DATEHOLDERID,DIM_DATEIDSTARTDATE,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_LAG0_MONTHID,DIM_LAG1_MONTHID,DIM_LASTSATURDAYCPMID,DIM_NDLASTSATURDAYCPMID,DIM_PROJECTSOURCEID,DW_INSERT_DATE,DW_UPDATE_DATE,FACT_JDA_DEMANDFORECASTID,AMT_PVE_PRICE_EURO_YTD_PREV_YEAR,AMT_COGSFIXEDPLANRATE_EMD,DD_CMG_ID,AMT_JDACOGS,DD_COGSSOURCE,AMT_JDACOGS_OP,DD_COGSSOURCE_OP,DD_WCOGSPCTG,CT_UNITS_COR_YESTERDAY,DIM_DATEIDSNAPSHOT,AMT_PVE_CFCST_EURO,DD_NPI_EOL,DD_VALUE_W_O_QTY,AMT_PVE_PRICE_EURO_QUARTER,AMT_PVE_PRICE_EURO_YTD,CT_PVE_Q_QTY,AMT_PVE_PY_Q_EURO_PRICE,AMT_PVE_PY_PREVQ_EURO_PRICE,CT_PVE_PREVQ_QTY,AMT_PVE_PREVQ_EURO,AMT_PVE_Q_EURO,CT_COUNT_MTH,CT_COUNT,CT_QTY_PREVQ,CT_PVE_YTD_QTY,AMT_PVE_YTD_EURO,AMT_PVE_YTD_EURO_PRICE,DIM_MDG_PARTID,CT_PVE_ALTERNATIVE_QTY,AMT_PVE_CFCST_YTD_EURO,DD_NPI_EOL_YTD,DD_VALUE_W_O_QTY_YTD,CT_COUNT_MTH_YTD,CT_PVE_CFOP_YTD_QTY,AMT_PVE_CFOP_YTD_EURO,AMT_PVE_CFOP_YTD_EURO_PRICE,DD_NPI_EOL_CFOP_YTD,DD_VALUE_W_O_QTY_CFOP_YTD,CT_PVE_CFOP_Q_QTY,CT_PVE_CFOP_PREVQ_QTY,AMT_PVE_CFOP_Q_EURO,AMT_PVE_CFOP_PREVQ_EURO,AMT_PVE_CFOP_Q_EURO_PRICE,AMT_PVE_CFOP_PREVQ_EURO_PRICE,DD_NPI_EOL_CFOP_Q,DD_VALUE_W_O_QTY_CFOP_Q,CT_PACKS_TOCORRECT,CT_PACKS_TOCORRECT_YTD
from emd586.fact_jda_demandforecast;

rename emd586.fact_jda_demandforecast to emd586.fact_jda_demandforecast_hmn_working;
rename emd586.fact_jda_demandforecast_hmn_new_table to emd586.fact_jda_demandforecast;
rename emd586.fact_jda_demandforecast_hmn_working to emd586.fact_jda_demandforecast_hmn_new_table;
delete from emd586.fact_jda_demandforecast_hmn_new_table where dim_projectsourceid = 1;

merge into emd586.fact_jda_demandforecast_hmn_new_table a
using EMDTEMPOCC4.fact_jda_demandforecast b
	on a.fact_jda_demandforecastid = b.fact_jda_demandforecastid
when not matched then insert
       (AMT_PVE_PRICE_EURO_Q_PREV_YEAR,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVGFCST,CT_AVGSALESPMONTH,CT_AVG_MKTSFA_12M,CT_AVG_STATSFA_12M,CT_CURRRATE,CT_EQ_UNIT,CT_IU,CT_LAG0_EQ_UNIT,CT_LAG0_PACKS_COR,CT_LAG0_UNITS_COR,CT_LAG1_EQ_UNIT,CT_LAG1_PACKS_COR,CT_LAG1_UNITS_COR,CT_MCG,CT_MKTSFA,CT_OP_EURO,CT_PACKS,CT_PACKS_COR,CT_PRICE,CT_PRICE_EURO_AVG_QUARTER,CT_PRICE_EURO_AVG_YEAR,CT_PRODLOCATION,CT_STATSFA,CT_UDA_EXCHRATE,CT_UNITS,CT_UNITS_COR,CT_VOLATILITY,DD_ABC_CLASS,DD_CURRENTDATE,DD_DFUCLASS,DD_DFULIFECYCLE,DD_FCSTLEVEL,DD_GAUSSITEMCODE,DD_ITEMLOCALCODE,DD_LASTSATURDAYCPM,DD_LOCALBRAND,DD_MODEL,DD_NDLASTSATURDAYCPM,DD_PLANNIG_ITEM_FLAG,DD_PLANNING_ITEM_ID,DD_SEGMENTATION,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_TYPE,DD_TYPE_2,DD_TYPE_3,DIM_DATEHOLDERID,DIM_DATEIDSTARTDATE,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_LAG0_MONTHID,DIM_LAG1_MONTHID,DIM_LASTSATURDAYCPMID,DIM_NDLASTSATURDAYCPMID,DIM_PROJECTSOURCEID,DW_INSERT_DATE,DW_UPDATE_DATE,FACT_JDA_DEMANDFORECASTID,AMT_PVE_PRICE_EURO_YTD_PREV_YEAR,AMT_COGSFIXEDPLANRATE_EMD,DD_CMG_ID,AMT_JDACOGS,DD_COGSSOURCE,AMT_JDACOGS_OP,DD_COGSSOURCE_OP,DD_WCOGSPCTG,CT_UNITS_COR_YESTERDAY,DIM_DATEIDSNAPSHOT,AMT_PVE_CFCST_EURO,DD_NPI_EOL,DD_VALUE_W_O_QTY,AMT_PVE_PRICE_EURO_QUARTER,AMT_PVE_PRICE_EURO_YTD,CT_PVE_Q_QTY,AMT_PVE_PY_Q_EURO_PRICE,AMT_PVE_PY_PREVQ_EURO_PRICE,CT_PVE_PREVQ_QTY,AMT_PVE_PREVQ_EURO,AMT_PVE_Q_EURO,CT_COUNT_MTH,CT_COUNT,CT_QTY_PREVQ,CT_PVE_YTD_QTY,AMT_PVE_YTD_EURO,AMT_PVE_YTD_EURO_PRICE,DIM_MDG_PARTID,CT_PVE_ALTERNATIVE_QTY,AMT_PVE_CFCST_YTD_EURO,DD_NPI_EOL_YTD,DD_VALUE_W_O_QTY_YTD,CT_COUNT_MTH_YTD,CT_PVE_CFOP_YTD_QTY,AMT_PVE_CFOP_YTD_EURO,AMT_PVE_CFOP_YTD_EURO_PRICE,DD_NPI_EOL_CFOP_YTD,DD_VALUE_W_O_QTY_CFOP_YTD,CT_PVE_CFOP_Q_QTY,CT_PVE_CFOP_PREVQ_QTY,AMT_PVE_CFOP_Q_EURO,AMT_PVE_CFOP_PREVQ_EURO,AMT_PVE_CFOP_Q_EURO_PRICE,AMT_PVE_CFOP_PREVQ_EURO_PRICE,DD_NPI_EOL_CFOP_Q,DD_VALUE_W_O_QTY_CFOP_Q,CT_PACKS_TOCORRECT,CT_PACKS_TOCORRECT_YTD,ct_pve_volume_eff,ct_pve_count_years,ct_pve_cfop_volume_eff,ct_count_type)
values (AMT_PVE_PRICE_EURO_Q_PREV_YEAR,CT_AVG3M_BIAS1M,CT_AVG6M_BIAS1M,CT_AVGFCST,CT_AVGSALESPMONTH,CT_AVG_MKTSFA_12M,CT_AVG_STATSFA_12M,CT_CURRRATE,CT_EQ_UNIT,CT_IU,CT_LAG0_EQ_UNIT,CT_LAG0_PACKS_COR,CT_LAG0_UNITS_COR,CT_LAG1_EQ_UNIT,CT_LAG1_PACKS_COR,CT_LAG1_UNITS_COR,CT_MCG,CT_MKTSFA,CT_OP_EURO,CT_PACKS,CT_PACKS_COR,CT_PRICE,CT_PRICE_EURO_AVG_QUARTER,CT_PRICE_EURO_AVG_YEAR,CT_PRODLOCATION,CT_STATSFA,CT_UDA_EXCHRATE,CT_UNITS,CT_UNITS_COR,CT_VOLATILITY,DD_ABC_CLASS,DD_CURRENTDATE,DD_DFUCLASS,DD_DFULIFECYCLE,DD_FCSTLEVEL,DD_GAUSSITEMCODE,DD_ITEMLOCALCODE,DD_LASTSATURDAYCPM,DD_LOCALBRAND,DD_MODEL,DD_NDLASTSATURDAYCPM,DD_PLANNIG_ITEM_FLAG,DD_PLANNING_ITEM_ID,DD_SEGMENTATION,DD_SYSTBIAS3M,DD_SYSTBIAS6M,DD_TYPE,DD_TYPE_2,DD_TYPE_3,DIM_DATEHOLDERID,DIM_DATEIDSTARTDATE,DIM_JDA_COMPONENTID,DIM_JDA_DATEHOLDERID,DIM_JDA_LOCATIONID,DIM_JDA_PRODUCTID,DIM_LAG0_MONTHID,DIM_LAG1_MONTHID,DIM_LASTSATURDAYCPMID,DIM_NDLASTSATURDAYCPMID,DIM_PROJECTSOURCEID,DW_INSERT_DATE,DW_UPDATE_DATE,FACT_JDA_DEMANDFORECASTID,AMT_PVE_PRICE_EURO_YTD_PREV_YEAR,AMT_COGSFIXEDPLANRATE_EMD,DD_CMG_ID,AMT_JDACOGS,DD_COGSSOURCE,AMT_JDACOGS_OP,DD_COGSSOURCE_OP,DD_WCOGSPCTG,CT_UNITS_COR_YESTERDAY,DIM_DATEIDSNAPSHOT,AMT_PVE_CFCST_EURO,DD_NPI_EOL,DD_VALUE_W_O_QTY,AMT_PVE_PRICE_EURO_QUARTER,AMT_PVE_PRICE_EURO_YTD,CT_PVE_Q_QTY,AMT_PVE_PY_Q_EURO_PRICE,AMT_PVE_PY_PREVQ_EURO_PRICE,CT_PVE_PREVQ_QTY,AMT_PVE_PREVQ_EURO,AMT_PVE_Q_EURO,CT_COUNT_MTH,CT_COUNT,CT_QTY_PREVQ,CT_PVE_YTD_QTY,AMT_PVE_YTD_EURO,AMT_PVE_YTD_EURO_PRICE,DIM_MDG_PARTID,CT_PVE_ALTERNATIVE_QTY,AMT_PVE_CFCST_YTD_EURO,DD_NPI_EOL_YTD,DD_VALUE_W_O_QTY_YTD,CT_COUNT_MTH_YTD,CT_PVE_CFOP_YTD_QTY,AMT_PVE_CFOP_YTD_EURO,AMT_PVE_CFOP_YTD_EURO_PRICE,DD_NPI_EOL_CFOP_YTD,DD_VALUE_W_O_QTY_CFOP_YTD,CT_PVE_CFOP_Q_QTY,CT_PVE_CFOP_PREVQ_QTY,AMT_PVE_CFOP_Q_EURO,AMT_PVE_CFOP_PREVQ_EURO,AMT_PVE_CFOP_Q_EURO_PRICE,AMT_PVE_CFOP_PREVQ_EURO_PRICE,DD_NPI_EOL_CFOP_Q,DD_VALUE_W_O_QTY_CFOP_Q,CT_PACKS_TOCORRECT,CT_PACKS_TOCORRECT_YTD,ct_pve_volume_eff,ct_pve_count_years,ct_pve_cfop_volume_eff,ct_count_type);

drop table if exists emd586.fact_jda_demandforecast_previous_EMDTEMPOCC4;
rename emd586.fact_jda_demandforecast to emd586.fact_jda_demandforecast_previous_EMDTEMPOCC4;
rename emd586.fact_jda_demandforecast_hmn_new_table to emd586.fact_jda_demandforecast;

drop table if exists emd586.fact_jda_demandforecast_previous_EMDTEMPOCC4;
drop table if exists TMP_PREFACT_DEMAND;
drop table if exists TMP_JDA_LAG_FORECASTEVOL_UNITSCOR;
drop table if exists TMP_JDA_LAG_FORECASTEVOL_EQUNIT;

/* Merck HC Control Tower avg_price from JDA*/
drop table if exists tmp_processing_day;
create table tmp_processing_day as select case when hour(current_timestamp) between 0 and 12 then current_date - interval '1' day else current_date end tdy from dual;

drop table if exists tmp_get_avg_price;
create table tmp_get_avg_price
as
select p.itemglobalcode
  ,l.location_name
  ,d.datevalue
  ,SUM(CASE WHEN jcmp.component_name = 'Actual / Mkt & Tender Fcst Values (in Euro)' THEN ct_packs ELSE 0 END) fcst_value
  ,SUM(CASE WHEN fjd.dd_type = 'Actual Hist / Mkt Fcst /Tender' THEN fjd.ct_packs ELSE 0 END) fcst_qty
from fact_jda_demandforecast fjd
  inner join dim_jda_component jcmp on fjd.dim_jda_componentid = jcmp.dim_jda_componentid
  inner join dim_jda_product p on fjd.dim_jda_productid = p.dim_jda_productid
  inner join dim_jda_location l on fjd.dim_jda_locationid = l.dim_jda_locationid
  inner join dim_date d on fjd.dim_dateidstartdate = d.dim_dateid
where p.GBU <> 'CHC' and d.datevalue between date_trunc('month',(select tdy from tmp_processing_day)) and date_trunc('month',(select tdy from tmp_processing_day)) + interval '12' month
group by p.itemglobalcode, l.location_name, d.datevalue;

drop table if exists tmp_get_itemplant;
create table tmp_get_itemplant
as
select STSC_DFUVIEW_DMDUNIT,STSC_DFUVIEW_LOC,max(STSC_DFUVIEW_UDC_PLANT) STSC_DFUVIEW_UDC_PLANT from STSC_DFUVIEW group by STSC_DFUVIEW_DMDUNIT,STSC_DFUVIEW_LOC;

delete from csv_averagesaleprice;
insert into csv_averagesaleprice(MATNR,PERIOD,WERKS,PRICE)
select a.itemglobalcode MATNR
  ,a.datevalue PERIOD
  ,p.STSC_DFUVIEW_UDC_PLANT WERKS
  ,CASE WHEN SUM(a.fcst_qty) <> 0 THEN CAST(SUM(a.fcst_value) / SUM(a.fcst_qty) AS DECIMAL(18,4)) ELSE 0 END PRICE
from tmp_get_avg_price a
  inner join tmp_get_itemplant p on a.itemglobalcode = p.STSC_DFUVIEW_DMDUNIT and a.location_name = p.STSC_DFUVIEW_LOC
group by a.itemglobalcode,a.datevalue,p.STSC_DFUVIEW_UDC_PLANT;

/* APP-6855 - Oana 6 July 2017 */
update fact_jda_demandforecast f
	set f.dd_PLANT = ifnull(t.STSC_DFUVIEW_UDC_PLANT,'Not Set')
from fact_jda_demandforecast f, dim_jda_location dl, dim_jda_product dp, tmp_get_itemplant t
where f.dim_jda_locationid = dl.dim_jda_locationid
	and f.dim_jda_productid = dp.dim_jda_productid
	and ifnull(dp.product_name,'Not Set')  = ifnull(t.stsc_dfuview_dmdunit,'Not Set')
	and ifnull(dl.location_name,'Not Set') = ifnull(t.stsc_dfuview_loc,'Not Set');
/* END APP-6855 - Oana 6 July 2017 */

drop table if exists tmp_get_avg_price;
drop table if exists tmp_get_itemplant;
drop table if exists tmp_processing_day;

