drop table if exists tmp_monthlyforecast;
create table tmp_monthlyforecast as
	select sps.stsc_skuprojstatic_item as item
		,sps.stsc_skuprojstatic_loc as loc
		,sps.stsc_skuprojstatic_startdate as startdate 
		,sum(stsc_dfutoskufcst_totfcst) as totfcst
	from stsc_skuprojstatic sps 
		left outer join stsc_dfutoskufcst dsf on (sps.stsc_skuprojstatic_item = dsf.stsc_dfutoskufcst_item 
				and sps.stsc_skuprojstatic_loc = dsf.stsc_dfutoskufcst_skuloc
				and sps.stsc_skuprojstatic_startdate = dsf.stsc_dfutoskufcst_startdate)
		inner join  dim_jda_sku djs on (sps.stsc_skuprojstatic_item = djs.sku_item and sps.stsc_skuprojstatic_loc = djs.sku_loc)
		inner join dim_jda_loc djl on sps.stsc_skuprojstatic_loc = djl.loc
	where  djl.loc_type in (3,5) 
		and djs.CUSTORDERCOPYSW <> 1
		and to_char(sps.stsc_skuprojstatic_startdate,'dd') = '01'
	group by sps.stsc_skuprojstatic_item,sps.stsc_skuprojstatic_loc,sps.stsc_skuprojstatic_startdate;

drop table if exists tmp_totdmd;
create table tmp_totdmd as 
	select 
		STSC_SKUPROJSTATIC_item	as item
		,STSC_SKUPROJSTATIC_LOC	as loc
		,date_trunc('MONTH',STSC_SKUPROJSTATIC_STARTDATE)	as startdate
		,sum(STSC_SKUPROJSTATIC_TOTDMD) as totdmd
	from STSC_SKUPROJSTATIC
	group by STSC_SKUPROJSTATIC_ITEM ,STSC_SKUPROJSTATIC_LOC,date_trunc('MONTH',STSC_SKUPROJSTATIC_STARTDATE);

update tmp_monthlyforecast t
	set totfcst = totdmd
from tmp_monthlyforecast t, tmp_totdmd t2
where t.item = t2.item
	and t.loc = t2.loc
	AND t.startdate = t2.startdate
	and totfcst is null;

/* SaftyStock calculation */
drop table if exists tmp_saftystock;
create table tmp_saftystock as 
	select sssp2.item
		,sssp2.loc
		,startdate 
		,greatest(ssq2,minss) as ssqty
	from dim_jda_skusafetystockparam sssp2,
		(select sssp1.item 
			,sssp1.loc
			,startdate 
			,least(ssq1,maxss) as ssq2
		from dim_jda_skusafetystockparam sssp1,
			(select sssp.item
				,sssp.loc
				,startdate 
				,(sscov/30 * totfcst) as ssq1
			from dim_jda_skusafetystockparam sssp, tmp_monthlyforecast tmf
			where sssp.item = tmf.item and sssp.loc = tmf.loc) sq1
		where sssp1.item = sq1.item  and sssp1.loc = sq1.loc)sq2
	where sssp2.item = sq2.item  and sssp2.loc = sq2.loc;

/* MaxD calculation */
drop table if exists tmp_maxd;
create table tmp_maxd as
	select spp2.item
		,spp2.loc
		,startdate
		,spp2.mindrpqty
		,drp1.drp2
		,(drp1.drp2 - mindrpqty) as drp2rest
		,greatest(spp2.incdrpqty,1) as max_incdrpqty
		,spp2.incdrpqty
	from dim_jda_skuplanningparam spp2,
		(select spp1.item
			,spp1.loc
			,startdate
			,greatest(drp.drp1,spp1.mindrpqty) as drp2
		from dim_jda_skuplanningparam spp1,
			(select spp.item
				,spp.loc
				,mthf.startdate
				,(spp.drpcovdur/30 * mthf.totfcst)  as drp1
			from dim_jda_skuplanningparam spp, tmp_monthlyforecast mthf	
			where spp.item = mthf.item  and spp.loc = mthf.loc) drp
		where spp1.item = drp.item and spp1.loc= drp.loc) drp1
	where spp2.item = drp1.item and spp2.loc = drp1.loc;

/* get SAP Std Price */
drop table if exists tmp_inventory_stdprice;
create table tmp_inventory_stdprice
as
select dp.partnumber, dc.company, max(f.amt_StdUnitPrice * f.amt_exchangerate_gbl) stdprice
from emd586.fact_inventoryhistory f
	inner join emd586.dim_part dp on f.dim_partid = dp.dim_partid
	inner join emd586.dim_company dc on f.dim_companyid = dc.dim_companyid
	inner join emd586.dim_date s on f.dim_dateidsnapshot = s.dim_dateid
where s.datevalue >= current_date - interval '3' month
group by dp.partnumber, dc.company;

/* get Business Line */
drop table if exists tmp_upperhier;
create table tmp_upperhier as 
	select distinct partnumber,businessline, max(dim_bwproducthierarchyid) as dim_bwproducthierarchyid , row_number() over(partition by partnumber order by businessline ) as rowno
		from EMD586.dim_part dp, EMD586.dim_bwproducthierarchy bw
	 	where dp.producthierarchy = bw.lowerhierarchycode
			and dp.productgroupsbu = bw.upperhierarchycode
			and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
			and businessline <> 'Not Set'
		group by partnumber,businessline;

drop table if exists tmp_projectedonhandinv;
create table tmp_projectedonhandinv as
	select STSC_SKUPROJSTATIC_ITEM as item
		,STSC_SKUPROJSTATIC_LOC as loc
		,sps.STSC_SKUPROJSTATIC_STARTDATE as startdate 
		,djl.cmg_id as company
		,dim_jda_itemid
		,dim_jda_locid
		,dim_jda_skuid
		,dim_dateid as dim_dateidstartdate
		,cast(1 as decimal(36,0)) as dim_jda_skuplanningparamid
		,cast(1 as decimal(36,0)) as dim_jda_skusafetystockparamid
		,cast(0 as decimal(18,4)) as ct_saftystock
		,cast(0 as decimal(18,4)) as ct_replenqty 
		,STSC_SKUPROJSTATIC_CONSTRPROJOH as ct_constrprojoh
		,STSC_SKUPROJSTATIC_CONSTRCOVDUR as ct_constrcovdur
		,cast(0 as decimal(18,4)) as amt_cogsfixedplanrate
		,cast(0 as decimal(18,4)) as ct_monthlyfcst
		,ifnull(mdg.COGSICMINDICATOR,'Not Set') as mdgflag
		,cast('Not Set' as varchar(50)) as cogssource
	from stsc_skuprojstatic sps
			inner join  dim_jda_item dji on STSC_SKUPROJSTATIC_ITEM = dji.item
			inner join  dim_jda_loc djl on STSC_SKUPROJSTATIC_LOC = djl.loc
			inner join  dim_jda_sku djs on (STSC_SKUPROJSTATIC_ITEM = djs.sku_item and STSC_SKUPROJSTATIC_LOC = djs.sku_loc)
			inner join  dim_date dt on STSC_SKUPROJSTATIC_STARTDATE = dt.datevalue and companycode = 'Not Set'
			left outer join dim_mdg_part mdg on STSC_SKUPROJSTATIC_ITEM = mdg.partnumber
			--left join csv_cogs csvg on (djl.cmg_id = trim(leading 0 from csvg.Z_REPUNIT) and STSC_SKUPROJSTATIC_ITEM = trim(leading 0 from csvg.PRODUCT))
	where djl.loc_type in (3,5) 
		and djs.CUSTORDERCOPYSW <> 1
		and to_char(sps.stsc_skuprojstatic_startdate,'dd') = '01';

/* Std Cost */		
update tmp_projectedonhandinv t
	set t.amt_cogsfixedplanrate = t1.stdprice,
		t.cogssource = 'Local Std. Cost (SAP)'
from tmp_projectedonhandinv t, tmp_inventory_stdprice t1
where t.item = t1.partnumber 
	and t.company = trim(leading 0 from t1.company)
	and mdgflag <> 'X';

update tmp_projectedonhandinv t
	set t.amt_cogsfixedplanrate = csvg.Z_GCPLFF,
		t.cogssource = 'World Std. Cost'
from tmp_projectedonhandinv t,  csv_cogs csvg
where t.company = trim(leading 0 from csvg.Z_REPUNIT) 
	and t.item = trim(leading 0 from csvg.PRODUCT)
	and mdgflag = 'X';

update tmp_projectedonhandinv t
	set t.amt_cogsfixedplanrate = (PE_APPLYED*ti.stdprice),
		t.cogssource = 'Group Reporting PE'
from tmp_projectedonhandinv t 
	inner join tmp_upperhier tu on t.item = tu.partnumber
	inner join csv_pe_pctg csvpe on (t.company = csvpe.company and csvpe.BWPRODHIER_BL = tu.businessline)
	inner join tmp_inventory_stdprice ti on  t.item = ti.partnumber and t.company = trim(leading 0 from ti.company)
where amt_cogsfixedplanrate = 0
	and mdgflag = 'X'
	and rowno = 1;
	
/* get Std. Cost whithout considering the company - only those cases without yet a cost */
drop table if exists tmp_sap_avgstdprice;
create table tmp_sap_avgstdprice as
select t.item, avg(stdprice) as stdprice
from tmp_projectedonhandinv t, tmp_inventory_stdprice t1
where t.item = t1.partnumber 
group by t.item;

drop table if exists tmp_cogs_avgprice;
create table tmp_cogs_avgprice as 
select item, avg(csvg.Z_GCPLFF) as Z_GCPLFF
from tmp_projectedonhandinv t,  csv_cogs csvg
where t.item = trim(leading 0 from csvg.PRODUCT)
group by item;

update tmp_projectedonhandinv t
	set t.amt_cogsfixedplanrate = t1.stdprice,
		t.cogssource = 'Local Std. Cost (SAP) Avg.'
from tmp_projectedonhandinv t, tmp_sap_avgstdprice t1
where t.item = t1.item
	and mdgflag <> 'X'
	and t.cogssource = 'Not Set';

update tmp_projectedonhandinv t
	set t.amt_cogsfixedplanrate = csvg.Z_GCPLFF,
		t.cogssource = 'World Std. Cost Avg.'
from tmp_projectedonhandinv t,  tmp_cogs_avgprice csvg
where t.item = csvg.item
	and mdgflag = 'X'
	and t.cogssource = 'Not Set';

update tmp_projectedonhandinv t
	set t.amt_cogsfixedplanrate = (PE_APPLYED*ti.stdprice),
		t.cogssource = 'Group Reporting PE Avg.'
from tmp_projectedonhandinv t 
	inner join tmp_upperhier tu on t.item = tu.partnumber
	inner join csv_pe_pctg csvpe on (t.company = csvpe.company and csvpe.BWPRODHIER_BL = tu.businessline)
	inner join tmp_sap_avgstdprice ti on  t.item = ti.item
where mdgflag = 'X'
	and t.cogssource = 'Not Set'
	and rowno = 1;
	
update tmp_projectedonhandinv t
	set t.amt_cogsfixedplanrate = (ti.stdprice),
		t.cogssource = 'Group Reporting PE Avg.'
from tmp_projectedonhandinv t , tmp_sap_avgstdprice ti
where t.item = ti.item
	and mdgflag = 'X'
	and t.cogssource = 'Not Set';	
/* END Std Cost */
	
update tmp_projectedonhandinv t
	set t.dim_jda_skuplanningparamid = spp.dim_jda_skuplanningparamid
from tmp_projectedonhandinv t, dim_jda_skuplanningparam spp
where t.item = spp.item and t.loc = spp.loc;

update tmp_projectedonhandinv t
	set t.dim_jda_skusafetystockparamid = sssp.dim_jda_skusafetystockparamid
from tmp_projectedonhandinv t, dim_jda_skusafetystockparam sssp
where t.item = sssp.item and t.loc = sssp.loc;

update tmp_projectedonhandinv t
	set t.ct_saftystock = ts.ssqty
from tmp_projectedonhandinv t, tmp_saftystock ts
where t.item = ts.item  and t.loc = ts.loc and t.startdate = ts.startdate;

update tmp_projectedonhandinv t
	set t.ct_monthlyfcst = tmf.totfcst
from tmp_projectedonhandinv t, tmp_monthlyforecast tmf
where t.item = tmf.item  and t.loc = tmf.loc and t.startdate = tmf.startdate;

update tmp_projectedonhandinv t
	set t.ct_replenqty = case 	when drp2rest = 0 then tm.drp2
							when drp2rest > 0 then (tm.mindrpqty + round(tm.drp2rest/tm.max_incdrpqty) * tm.incdrpqty)
							else 0
						end
from tmp_projectedonhandinv t, tmp_maxd tm
where t.item = tm.item and t.loc = tm.loc and t.startdate = tm.startdate;	

truncate table fact_jda_projectedonhandinv;
delete from number_fountain m where m.table_name = 'fact_jda_projectedonhandinv';
insert into number_fountain
select 'fact_jda_projectedonhandinv',
            ifnull(max(d.fact_jda_projectedonhandinvid), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_jda_projectedonhandinv d
where d.fact_jda_projectedonhandinvid <> 1;

insert into fact_jda_projectedonhandinv (fact_jda_projectedonhandinvid,dim_jda_itemid,dim_jda_locid,dim_jda_skuid,dim_dateidstartdate,
	dim_jda_skuplanningparamid,dim_jda_skusafetystockparamid,ct_saftystock,ct_constrprojoh,ct_constrcovdur,
	amt_cogsfixedplanrate,dw_insert_date,dw_update_date,ct_replenqty,ct_monthlyfcst,dd_mdg_flag,dd_cogssource)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_jda_projectedonhandinv') + row_number()  over(order by '') as fact_jda_projectedonhandinvid
	,ifnull(dim_jda_itemid,1) as dim_jda_itemid
	,ifnull(dim_jda_locid,1) as dim_jda_locid
	,ifnull(dim_jda_skuid,1) as dim_jda_skuid
	,ifnull(dim_dateidstartdate,1) as dim_dateidstartdate
	,ifnull(dim_jda_skuplanningparamid,1) as dim_jda_skuplanningparamid
	,ifnull(dim_jda_skusafetystockparamid,1) as dim_jda_skusafetystockparamid
	,ifnull(ct_saftystock,0) as ct_saftystock
	,ifnull(ct_constrprojoh,0) as ct_constrprojoh
	,ifnull(ct_constrcovdur,0) as ct_constrcovdur
	,ifnull(amt_cogsfixedplanrate,0) as amt_cogsfixedplanrate
	,current_timestamp  as dw_insert_date
	,current_timestamp  as dw_update_date
	,ifnull(t.ct_replenqty,0) as ct_replenqty
	,ifnull(ct_monthlyfcst,0) as ct_monthlyfcst
	,ifnull(mdgflag,'Not Set') as dd_mdg_flag
	,ifnull(cogssource, 'Not Set') as dd_cogssource
from tmp_projectedonhandinv t;

update fact_jda_projectedonhandinv f
	set f.dim_Dateiddisplay = ifnull(dt2.dim_Dateid,1)
from fact_jda_projectedonhandinv f
	inner join (select distinct dt.datevalue - interval '1' month as datevalue, dim_dateid ,companycode from dim_Date dt,fact_jda_projectedonhandinv 
					where dim_dateidstartdate = dt.dim_dateid) dt1 on f.dim_dateidstartdate = dt1.dim_dateid
	inner join dim_date dt2 on dt2.datevalue = dt1.datevalue  and dt2.companycode = dt1.companycode;

/* Calculate Delta Bucket vs MaxD */
drop table if exists tmp_deltabucketmaxd;
create table tmp_deltabucketmaxd as 
	select dim_jda_itemid
		,dim_jda_locid
		,dim_dateidstartdate
	 	,sum(f.ct_constrprojoh * amt_cogsfixedplanrate) as bucket
		,sum((f.ct_saftystock + 1.5 * f.ct_replenqty)*amt_cogsfixedplanrate) as maxD
	from fact_jda_projectedonhandinv f
	group by dim_jda_itemid,dim_jda_locid,dim_jda_skuid,dim_dateidstartdate;

update fact_jda_projectedonhandinv  f
	set f.amt_deltabuckettomaxd = case when bucket > maxD then (bucket - maxD) else 0 end,
		f.DD_FLAGMAXD = case when bucket > maxD then 1 else 0 end
from fact_jda_projectedonhandinv f, tmp_deltabucketmaxd t
	where f.dim_jda_itemid = t.dim_jda_itemid
		and f.dim_jda_locid = t.dim_jda_locid
		and f.dim_dateidstartdate = t.dim_dateidstartdate;
		
/* set link to dim_bwproducthierarchy */
update fact_jda_projectedonhandinv f
	set f.dim_bwproducthierarchyid = ifnull(t.dim_bwproducthierarchyid,1)
from fact_jda_projectedonhandinv f, dim_jda_sku djs , tmp_upperhier t
where f.dim_jda_skuid = djs.dim_jda_skuid
	and djs.sku_item = t.partnumber
	and rowno = 1;

	
drop table if exists tmp_projectedonhandinv;