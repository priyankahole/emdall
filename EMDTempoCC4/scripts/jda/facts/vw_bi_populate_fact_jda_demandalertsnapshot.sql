/* fact_jda_demandalertsnapshot */

truncate table fact_jda_demandalertsnapshot;
insert into fact_jda_demandalertsnapshot (FACT_JDA_DEMANDFORECASTID,DIM_DATEIDSTARTDATE,DD_PLANNING_ITEM_ID,DIM_JDA_LOCATIONID,
	DIM_JDA_PRODUCTID,DIM_JDA_COMPONENTID,DD_TYPE_2,DD_TYPE_3,CT_PACKS_COR,CT_UNITS_COR)
	select f.FACT_JDA_DEMANDFORECASTID,
		f.DIM_DATEIDSTARTDATE,
		f.DD_PLANNING_ITEM_ID,
		f.DIM_JDA_LOCATIONID,
		f.DIM_JDA_PRODUCTID,
		f.DIM_JDA_COMPONENTID,
		f.DD_TYPE_2,
		f.DD_TYPE_3,
		f.CT_PACKS_COR,
		f.CT_UNITS_COR
	from fact_jda_demandforecast f
		where f.dd_type_2 = 'FCST';

update fact_jda_demandalertsnapshot f
	set f.dim_dateidsnapshot  = (select dim_dateid from  dim_date where datevalue = current_Date and companycode = 'Not Set');
