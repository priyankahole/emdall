UPDATE    fact_planorder po
   SET po.amt_ExtendedPrice_GBL = amt_ExtendedPrice * so.amt_ExchangeRate_GBL
FROM fact_salesorder so, fact_planorder po
   WHERE   so.dd_SalesDocNo = po.dd_SalesOrderNo
          AND so.dd_SalesItemNo = po.dd_SalesOrderItemNo
          AND so.dd_ScheduleNo = po.dd_SalesOrderScheduleNo;

UPDATE fact_planorder fp
   SET fp.Dim_CustomerGroup1id = s.Dim_CustomerGroup1id,
       fp.Dim_CustomerID = s.Dim_CustomerID,
       fp.Dim_DocumentCategoryid = s.Dim_DocumentCategoryid,
       fp.Dim_SalesDocumentTypeid = s.Dim_SalesDocumentTypeid
FROM fact_salesorder s, fact_planorder fp
 WHERE  fp.dd_SalesOrderNo = s.dd_SalesDocNo
       AND fp.dd_SalesOrderItemNo = s.dd_SalesItemNo
       AND fp.dd_SalesOrderScheduleNo = s.dd_ScheduleNo;

UPDATE fact_productionorder fp
   SET fp.Dim_CustomerGroup1id = s.Dim_CustomerGroup1id,
       fp.Dim_CustomerID = s.Dim_CustomerID,
       fp.Dim_DocumentCategoryid = s.Dim_DocumentCategoryid,
       fp.Dim_SalesDocumentTypeid = s.Dim_SalesDocumentTypeid,
       fp.amt_ExchangeRate = s.amt_ExchangeRate,
       fp.amt_ExchangeRate_GBL = s.amt_ExchangeRate_GBL
FROM   (select distinct dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,Dim_CustomerGroup1id,
Dim_CustomerID,Dim_DocumentCategoryid,Dim_SalesDocumentTypeid,amt_ExchangeRate,amt_ExchangeRate_GBL from
    fact_salesorder ) s, fact_productionorder fp
 WHERE     fp.dd_SalesOrderNo = s.dd_SalesDocNo
       AND fp.dd_SalesOrderItemNo = s.dd_SalesItemNo
       AND fp.dd_SalesOrderDeliveryScheduleNo = s.dd_ScheduleNo;