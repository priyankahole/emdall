/* ##################################################################################################################

   Script         : bi_populate_so_itemstatus_dim
   Author         : Ashu
   Created On     : 17 Jan 2013


   Description    : Stored Proc bi_populate_so_itemstatus_dim from MySQL to Vectorwise syntax

   Change History
   Date            By        Version           Desc
   17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
   7 July 2014     George    1.1               Added GeneralIncompletionStatusCode
   5 Feb 2015      Liviu Ionescu  1.2            Add Combine for table VBUP , dim_salesorderitemstatus
#################################################################################################################### */

INSERT INTO dim_salesorderitemstatus(
          dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
		  ItemDeliveryBlockStatus,
		  Customerres1Itemstatus
          )
		  
  SELECT 1,
          'Not Set',
          0,
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
		  'Not Set' Customerres1Itemstatus
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorderitemstatus
               WHERE dim_salesorderitemstatusid = 1);		  

 /*DELETE FROM VBUP
 WHERE NOT EXISTS
              (SELECT 1
                 FROM VBAK_VBAP
                WHERE VBUP_VBELN = VBAP_VBELN AND VBUP_POSNR = VBAP_POSNR)
       AND NOT EXISTS
                  (SELECT 1
                     FROM fact_salesorder
                    WHERE dd_SalesDocNo = VBUP_VBELN
                          AND dd_SalesItemNo = VBUP_POSNR)

*/
    
UPDATE dim_salesorderitemstatus sois
SET OverallReferenceStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_RFGSA
AND OverallReferenceStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET ConfirmationStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_BESTA
AND  ConfirmationStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET DeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFSTA
AND DeliveryStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET OverallDeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LFGSA
AND OverallDeliveryStatus <>  ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET BillingStatusDeliveryRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSTA
AND BillingStatusDeliveryRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET BillingStatusOrderRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_FKSAA
AND  BillingStatusOrderRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET ItemRejectionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_ABSTA
AND ItemRejectionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET OverallProcessingStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_GBSTA
AND OverallProcessingStatus <> ifnull( t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET GeneralIncompletionStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL
AND  GeneralIncompletionStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET DelayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_DCSTA
AND DelayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET PODStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_PDSTA
AND  PODStatus  <> ifnull( t.DD07T_DDTEXT ,'Not Set') ;

UPDATE dim_salesorderitemstatus sois
SET GoodsMovementStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_WBSTA
AND  GoodsMovementStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set');

UPDATE dim_salesorderitemstatus sois
SET PickingPutawayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_KOSTA
AND PickingPutawayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus sois
Set ItemDeliveryBlockStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP,dd07t t
Where SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_LSSTA
AND  ItemDeliveryBlockStatus <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_salesorderitemstatus sois
   SET    ItemDataforDeliv = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP,dd07t t
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVVLK
  AND ItemDataforDeliv <> ifnull(t.DD07T_DDTEXT,'Not Set');
  
UPDATE dim_salesorderitemstatus sois
   SET    GeneralIncompletionStatusCode = ifnull(t.DD07T_DOMVALUE,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP,dd07t t
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVALL
  AND  GeneralIncompletionStatusCode <> ifnull(t.DD07T_DOMVALUE ,'Not Set');
  
UPDATE dim_salesorderitemstatus sois
   SET    GeneralIncompletionProcessingStatusOfItem = ifnull(t.TVBST_BEZEI,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois, VBUP, TVBST t
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND CONVERT(varchar(1),VBUP_UVALL) = CONVERT(varchar(1),t.TVBST_STATU) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'
  AND  GeneralIncompletionProcessingStatusOfItem <> ifnull(t.TVBST_BEZEI ,'Not Set');

 UPDATE dim_salesorderitemstatus sois
   SET    Customerres1Itemstatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP,dd07t t
  WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = VBUP_UVP01
  AND Customerres1Itemstatus <> ifnull(t.DD07T_DDTEXT,'Not Set'); 
  
delete from number_fountain m where m.table_name = 'dim_salesorderitemstatus';

insert into number_fountain
select 	'dim_salesorderitemstatus',
	ifnull(max(d.dim_salesorderitemstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemstatus d
where d.dim_salesorderitemstatusid <> 1; 
  
INSERT INTO dim_salesorderitemstatus(
          dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
	  ItemDeliveryBlockStatus,
	  ItemDataforDeliv,
	  GeneralIncompletionStatusCode,
	  GeneralIncompletionProcessingStatusOfItem,
	  Customerres1Itemstatus
          )
  SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by dtbl1.VBUP_VBELN,dtbl1.VBUP_POSNR)),dtbl1.*
 FROM (SELECT  distinct
          VBUP_VBELN,
          VBUP_POSNR,
         'Not Set' OverallReferenceStatus,
         'Not Set' ConfirmationStatus,
         'Not Set' DeliveryStatus,
         'Not Set' OverallDeliveryStatus,
         'Not Set' BillingStatusDeliveryRelated,
         'Not Set' BillingStatusOrderRelated,
         'Not Set' ItemRejectionStatus,
         'Not Set' OverallProcessingStatus,
         'Not Set' GeneralIncompletionStatus,
         'Not Set' DelayStatus,
         'Not Set' PODStatus,
         'Not Set' GoodsMovementStatus,
         'Not Set' PickingPutawayStatus,
	  	 'Not Set' ItemDeliveryBlockStatus,
	 	 'Not Set' ItemDataforDeliv,
	     'Not Set' GeneralIncompletionStatusCode,
	     'Not Set' GeneralIncompletionProcessingStatusOfItem,
		 'Not Set' Customerres1Itemstatus
FROM VBUP inner join VBAK_VBAP_VBEP on VBAK_VBELN = VBUP_VBELN and VBAP_POSNR = VBUP_POSNR
WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR)) dtbl1;

DROP TABLE IF EXISTS TMP_VBAK_VBAP_VBEP_SOIS;
CREATE TABLE TMP_VBAK_VBAP_VBEP_SOIS
AS
SELECT DISTINCT VBAK_VBELN, VBAP_POSNR FROM VBAK_VBAP_VBEP;

UPDATE dim_salesorderitemstatus sois
SET OverallReferenceStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_RFGSA
AND OverallReferenceStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET ConfirmationStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_BESTA
AND  ConfirmationStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET DeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_LFSTA
AND DeliveryStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET OverallDeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_LFGSA
AND OverallDeliveryStatus <>  ifnull(t.DD07T_DDTEXT ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET BillingStatusDeliveryRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_FKSTA
AND BillingStatusDeliveryRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET BillingStatusOrderRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_FKSAA
AND  BillingStatusOrderRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET ItemRejectionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_ABSTA
AND ItemRejectionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET OverallProcessingStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_GBSTA
AND OverallProcessingStatus <> ifnull( t.DD07T_DDTEXT ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET GeneralIncompletionStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_UVALL
AND  GeneralIncompletionStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET DelayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_DCSTA
AND DelayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET PODStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_PDSTA
AND  PODStatus  <> ifnull( t.DD07T_DDTEXT ,'Not Set')  and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET GoodsMovementStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_WBSTA
AND  GoodsMovementStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET PickingPutawayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_KOSTA
AND PickingPutawayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
Set ItemDeliveryBlockStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
Where SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_LSSTA
AND  ItemDeliveryBlockStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
   SET    ItemDataforDeliv = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
  WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_UVVLK
  AND ItemDataforDeliv <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;
  
UPDATE dim_salesorderitemstatus sois
   SET    GeneralIncompletionStatusCode = ifnull(t.DD07T_DOMVALUE,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
  WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_UVALL
  AND  GeneralIncompletionStatusCode <> ifnull(t.DD07T_DOMVALUE ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;
  
UPDATE dim_salesorderitemstatus sois
   SET    GeneralIncompletionProcessingStatusOfItem = ifnull(t.TVBST_BEZEI,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois, VBUP v, TVBST t, TMP_VBAK_VBAP_VBEP_SOIS vp
  WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
  AND CONVERT(varchar(1),v.VBUP_UVALL) = CONVERT(varchar(1),t.TVBST_STATU) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'
  AND  GeneralIncompletionProcessingStatusOfItem <> ifnull(t.TVBST_BEZEI ,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois  
SET Customerres1Itemstatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
    dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP v,dd07t t, TMP_VBAK_VBAP_VBEP_SOIS vp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_UVP01
AND Customerres1Itemstatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAK_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

  
  
DROP TABLE IF EXISTS TMP_VBAK_VBAP_VBEP_SOIS;



delete from number_fountain m where m.table_name = 'dim_salesorderitemstatus';

insert into number_fountain
select 	'dim_salesorderitemstatus',
	ifnull(max(d.dim_salesorderitemstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderitemstatus d
where d.dim_salesorderitemstatusid <> 1; 

      
  INSERT
  INTO dim_salesorderitemstatus(
	  dim_salesorderitemstatusid,
          SalesDocumentNumber,
          SalesItemNumber,
          OverallReferenceStatus,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDeliveryStatus,
          BillingStatusDeliveryRelated,
          BillingStatusOrderRelated,
          ItemRejectionStatus,
          OverallProcessingStatus,
          GeneralIncompletionStatus,
          DelayStatus,
          PODStatus,
          GoodsMovementStatus,
          PickingPutawayStatus,
          ItemDeliveryBlockStatus,
	  ItemDataforDeliv,
	  GeneralIncompletionStatusCode,
	  GeneralIncompletionProcessingStatusOfItem,
	  Customerres1Itemstatus
          )
  SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderitemstatus') + row_number() over (order by dtbl1.VBUP_VBELN,dtbl1.VBUP_POSNR)),dtbl1.*
 FROM (SELECT  distinct
          VBUP_VBELN,
          VBUP_POSNR,
          'Not Set' OverallReferenceStatus,
          'Not Set' ConfirmationStatus,
          'Not Set' DeliveryStatus,
          'Not Set' OverallDeliveryStatus,
          'Not Set' BillingStatusDeliveryRelated,
          'Not Set' BillingStatusOrderRelated,
          'Not Set' ItemRejectionStatus,
          'Not Set' OverallProcessingStatus,
          'Not Set' GeneralIncompletionStatus,
          'Not Set' DelayStatus,
          'Not Set' PODStatus,
          'Not Set' GoodsMovementStatus,
          'Not Set' PickingPutawayStatus,
          'Not Set' ItemDeliveryBlockStatus,
	      'Not Set' ItemDataforDeliv,
	      'Not Set' GeneralIncompletionStatusCode,
	      'Not Set' GeneralIncompletionProcessingStatusOfItem,
		  'Not Set' Customerres1Itemstatus
  FROM VBUP inner join VBAK_VBAP on VBAP_VBELN = VBUP_VBELN and VBAP_POSNR = VBUP_POSNR
  WHERE not exists (select 1 from dim_salesorderitemstatus s where s.SalesDocumentNumber = VBUP_VBELN and s.SalesItemNumber = VBUP_POSNR)) dtbl1;


UPDATE dim_salesorderitemstatus sois
SET OverallReferenceStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_RFGSA
AND OverallReferenceStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET ConfirmationStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = VBUP_VBELN and SalesItemNumber = VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_BESTA
AND  ConfirmationStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET DeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_LFSTA
AND DeliveryStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET OverallDeliveryStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_LFGSA
AND OverallDeliveryStatus <>  ifnull(t.DD07T_DDTEXT ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET BillingStatusDeliveryRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_FKSTA
AND BillingStatusDeliveryRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET BillingStatusOrderRelated = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_FKSAA
AND  BillingStatusOrderRelated <> ifnull( t.DD07T_DDTEXT ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET ItemRejectionStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_ABSTA
AND ItemRejectionStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET OverallProcessingStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_GBSTA
AND OverallProcessingStatus <> ifnull( t.DD07T_DDTEXT ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET GeneralIncompletionStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_UVALL
AND  GeneralIncompletionStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET DelayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_DCSTA
AND DelayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET PODStatus = ifnull( t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_PDSTA
AND  PODStatus  <> ifnull( t.DD07T_DDTEXT ,'Not Set')  and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET GoodsMovementStatus = ifnull(t.DD07T_DDTEXT ,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_WBSTA
AND  GoodsMovementStatus <> ifnull(t.DD07T_DDTEXT ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
SET PickingPutawayStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_KOSTA
AND PickingPutawayStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
Set ItemDeliveryBlockStatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 from dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
Where SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_LSSTA
AND  ItemDeliveryBlockStatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;

UPDATE dim_salesorderitemstatus sois
   SET    ItemDataforDeliv = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
  WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_UVVLK
  AND ItemDataforDeliv <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;
  
UPDATE dim_salesorderitemstatus sois
   SET    GeneralIncompletionStatusCode = ifnull(t.DD07T_DOMVALUE,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
  WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
  AND  t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_UVALL
  AND  GeneralIncompletionStatusCode <> ifnull(t.DD07T_DOMVALUE ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;
  
UPDATE dim_salesorderitemstatus sois
   SET    GeneralIncompletionProcessingStatusOfItem = ifnull(t.TVBST_BEZEI,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois, VBUP v, TVBST t, VBAK_VBAP vp
  WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
  AND CONVERT(varchar(1),v.VBUP_UVALL) = CONVERT(varchar(1),t.TVBST_STATU) and t.TVBST_TBNAM = 'VBUP' and t.TVBST_FDNAM = 'UVALL'
  AND  GeneralIncompletionProcessingStatusOfItem <> ifnull(t.TVBST_BEZEI ,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;
  
  
UPDATE dim_salesorderitemstatus sois
   SET    Customerres1Itemstatus = ifnull(t.DD07T_DDTEXT,'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_salesorderitemstatus sois,VBUP v,dd07t t, VBAK_VBAP vp
  WHERE SalesDocumentNumber = v.VBUP_VBELN and SalesItemNumber = v.VBUP_POSNR
  AND t.DD07T_DOMNAME = 'STATV' and t.DD07T_DOMVALUE = v.VBUP_UVP01
  AND Customerres1Itemstatus <> ifnull(t.DD07T_DDTEXT,'Not Set') and vp.VBAP_VBELN = v.VBUP_VBELN and vp.VBAP_POSNR = v.VBUP_POSNR;  
  
  