

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   22 Dec 2015      Alex D.   1.0               Script in exasol */
/******************************************************************************************************************/

drop table if exists pGlobalCurrency_tbl_consg;
create table pGlobalCurrency_tbl_consg ( pGlobalCurrency varchar(3));

Insert into pGlobalCurrency_tbl_consg values('USD');

Update pGlobalCurrency_tbl_consg
SET pGlobalCurrency =
        ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');
				
DELETE FROM fact_materialmovement
WHERE EXISTS (SELECT 1
		FROM MKPF_MSEG_RKWA m
		WHERE dd_MaterialDocNo = m.MSEG_MBLNR
                         AND dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = dd_MaterialDocYear);



Drop table if exists max_holder_789;

CREATE TABLE max_holder_789 AS
SELECT ifnull(max(fact_materialmovementid), 
	   ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
FROM fact_materialmovement;


drop table if exists fact_materialmovement_789tmp;
create table fact_materialmovement_789tmp as 
--select max_holder_789.maxid + row_number() over() rowno, bb.* from max_holder_789,
SELECT 
          m.MSEG_MBLNR dd_MaterialDocNo,
          m.MSEG_ZEILE dd_MaterialDocItemNo,
          m.MSEG_MJAHR dd_MaterialDocYear,
          m.MSEG_EBELN dd_DocumentNo,
          m.MSEG_EBELP dd_DocumentItemNo,
          m.MSEG_KDAUF dd_SalesOrderNo,
          m.MSEG_KDPOS dd_SalesOrderItemNo,
          m.MSEG_KDEIN dd_SalesOrderDlvrNo,
          m.MSEG_SAKTO dd_GLAccountNo,
          CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          m.MSEG_AUFNR dd_productionordernumber, 
          m.MSEG_AUFPS dd_productionorderitemno,
          m.MSEG_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG_CHARG, 'Not Set') dd_BatchNumber,
          m.MSEG_BWTAR dd_ValuationType,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_DMBTR amt_LocalCurrAmt,
          m.MSEG_DMBTR / (case when m.MSEG_ERFMG =0 then null else  m.MSEG_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BUALT amt_AltPriceControl,
          m.MSEG_SALK3 amt_OnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_MENGE ct_Quantity,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BSTMG ct_QtyGROrdUnit,
          m.MSEG_LBKUM ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BPMNG ct_QtyOrdPriceUnit,
          cast(1 as bigint) as  dim_MovementTypeid,
          cast(1 as bigint) as  dim_Companyid,
          cast(1 as bigint) as  dim_Currencyid,
          cast(1 as bigint) as  dim_Currencyid_TRA,
          cast(1 as bigint) as  Dim_Currencyid_GBL,				   
	      cast(1 as decimal (18,4)) as amt_ExchangeRate,
	      cast(1 as decimal (18,4)) as amt_ExchangeRate_GBL,				   
		  cast(1 as bigint)  as Dim_Partid,
          dp.Dim_Plantid as Dim_Plantid,
          cast(1 as bigint) as Dim_StorageLocationid,
          cast(1 as bigint) as Dim_Vendorid,
          cast(1 as bigint) as dim_DateIDMaterialDocDate,
          cast(1 as bigint) as dim_DateIDPostingDate,
          cast(1 as bigint) as dim_producthierarchyid,
          cast(1 as bigint) as dim_MovementIndicatorid,
          cast(1 as bigint) as dim_Customerid,
          cast(1 as bigint) as dim_CostCenterid,
          cast(1 as bigint) as dim_specialstockid,
          cast(1 as bigint) as dim_unitofmeasureid,
          cast(1 as bigint) as dim_controllingareaid,
          cast(1 as bigint) as dim_consumptiontypeid,
          cast(1 as bigint) as dim_stocktypeid,
	      cast(1 as bigint) as Dim_PurchaseGroupid,
          cast(1 as bigint) as Dim_materialgroupid,
          cast(1 as bigint) as dd_ConsignmentFlag,
          m.MSEG_BWART, -- used in Dim_MovementTypeid
		  m.MSEG_KZVBR, -- used in Dim_MovementTypeid
		  m.MSEG_KZBEW, -- used in Dim_MovementTypeid,dim_MovementIndicatorid
		  m.MSEG_KZZUG, -- used in Dim_MovementTypeid
		  m.MSEG_SOBKZ, -- used in Dim_MovementTypeid,dim_specialstockid
		  m.MSEG_BUKRS, -- used in dim_Companyid,Dim_Currencyid,dim_MaterialDocDate,dim_PostingDate
		  m.MSEG_WAERS, -- used in dim_Currencyid_TRA,Dim_Currencyid_GBL,amt_ExchangeRate,amt_ExchangeRate_GBL
		  m.MKPF_BUDAT, -- used in amt_ExchangeRate,dim_PostingDate
		  m.MSEG_WERKS, -- used in Dim_Partid,Dim_StorageLocationid,dim_producthierarchyid,Dim_PurchaseGroupid,Dim_materialgroupid
		  m.MSEG_MATNR, -- used in Dim_Partid,dim_producthierarchyid,Dim_PurchaseGroupid,Dim_materialgroupid
		  m.MSEG_LGORT, -- used in Dim_StorageLocationid
		  m.MSEG_LIFNR, -- used in Dim_Vendorid
		  m.MSEG_KUNNR, -- used in dim_Customerid
		  m.MSEG_MEINS, -- used in dim_unitofmeasureid
		  m.MSEG_KOKRS, -- used in dim_controllingareaid
		  m.MSEG_INSMK, -- used in dim_stocktypeid
		  m.MKPF_BLDAT, -- used in dim_MaterialDocDate
		  p.pGlobalCurrency,
		  'Not Set' as dc_Currency  -- used in amt_ExchangeRate
		  
     FROM MKPF_MSEG_RKWA m
          INNER JOIN dim_plant dp ON m.MSEG_WERKS = dp.PlantCode 
		                         AND dp.RowIsCurrent = 1
		  CROSS JOIN pGlobalCurrency_tbl_consg p
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear);
                    
UPDATE fact_materialmovement_789tmp m
SET m.dim_MovementTypeid = ifnull(mt.Dim_MovementTypeid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_movementtype mt 
     ON mt.MovementType = m.MSEG_BWART
     AND mt.ConsumptionIndicator = ifnull(m.MSEG_KZVBR, 'Not Set')
     AND mt.MovementIndicator = ifnull(m.MSEG_KZBEW, 'Not Set')
     AND mt.ReceiptIndicator = ifnull(m.MSEG_KZZUG, 'Not Set')
     AND mt.SpecialStockIndicator = ifnull(m.MSEG_SOBKZ, 'Not Set')
     AND mt.RowIsCurrent = 1;
	 
UPDATE fact_materialmovement_789tmp m	 
SET  m.dim_Companyid = IFNULL(dc.dim_Companyid,1),
     m.dc_Currency = IFNULL(dc.Currency,'Not Set')
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_company dc ON dc.CompanyCode = m.MSEG_BUKRS 
                         AND dc.RowIsCurrent = 1;
	 

UPDATE fact_materialmovement_789tmp m	 
SET  m.dim_Currencyid = IFNULL(dcr.Dim_Currencyid,1)
FROM   fact_materialmovement_789tmp m
INNER JOIN dim_company dc ON m.dim_Companyid = dc.dim_Companyid
LEFT JOIN dim_currency dcr ON dcr.CurrencyCode = dc.currency;
	 
	 
UPDATE fact_materialmovement_789tmp m	 
SET  m.dim_Currencyid_TRA = IFNULL(dcr.Dim_Currencyid,1)
FROM   fact_materialmovement_789tmp m
LEFT JOIN dim_currency dcr ON dcr.CurrencyCode = m.MSEG_WAERS;

UPDATE fact_materialmovement_789tmp m	 
SET  m.Dim_Currencyid_GBL = IFNULL(dcr.Dim_Currencyid,1)
FROM   fact_materialmovement_789tmp m
LEFT JOIN dim_currency dcr ON dcr.CurrencyCode = m.pGlobalCurrency;

UPDATE fact_materialmovement_789tmp m	
SET  m.amt_ExchangeRate	= IFNULL(z.exchangeRate,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN tmp_getExchangeRate1 z ON  z.pFromCurrency  = m.MSEG_WAERS 
	                              AND z.fact_script_name = 'bi_populate_materialmovement_fact' 
	                              AND z.pToCurrency = m.dc_Currency 
	                              AND z.pDate = m.MKPF_BUDAT;
	


UPDATE fact_materialmovement_789tmp m	
SET m.amt_ExchangeRate_GBL =  CASE WHEN MSEG_WAERS = pGlobalCurrency 
                              THEN 1.00 
							  ELSE z.exchangeRate 
							  END
FROM fact_materialmovement_789tmp m,tmp_getExchangeRate1 z 
WHERE    z.pFromCurrency  = m.MSEG_WAERS 
     AND z.fact_script_name = 'bi_populate_materialmovement_fact' 
	 AND z.pToCurrency = pGlobalCurrency 
	 AND z.pDate = current_date;


UPDATE fact_materialmovement_789tmp m	
SET m.Dim_Partid = IFNULL(dpr.Dim_Partid,1) 
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_part dpr ON     dpr.PartNumber = m.MSEG_MATNR
                          AND dpr.Plant = m.MSEG_WERKS
                          AND dpr.RowIsCurrent = 1;
     
    
	
UPDATE fact_materialmovement_789tmp m
SET m.Dim_StorageLocationid = IFNULL(Dim_StorageLocationid,1) 
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_storagelocation dsl ON dsl.LocationCode = m.MSEG_LGORT
                                  AND dsl.Plant = m.MSEG_WERKS
                                  AND dsl.RowIsCurrent = 1;	 
					
					
UPDATE fact_materialmovement_789tmp m
SET m.Dim_Vendorid = IFNULL(dv.Dim_Vendorid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_vendor dv ON dv.VendorNumber = m.MSEG_LIFNR
                        AND dv.RowIsCurrent = 1;


UPDATE fact_materialmovement_789tmp m
SET m.dim_DateIDMaterialDocDate = IFNULL(mdd.Dim_Dateid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_date mdd ON mdd.CompanyCode = m.MSEG_BUKRS 
                       AND m.MKPF_BLDAT = mdd.DateValue;
   
UPDATE fact_materialmovement_789tmp m
SET m.dim_DateIDPostingDate = IFNULL(pd.Dim_Dateid,1)
FROM  fact_materialmovement_789tmp m
LEFT JOIN dim_date pd ON pd.DateValue = m.MKPF_BUDAT 
                      AND pd.CompanyCode = m.MSEG_BUKRS;


UPDATE fact_materialmovement_789tmp m
SET m.dim_producthierarchyid = IFNULL(dim_producthierarchyid,1)
FROM fact_materialmovement_789tmp m
INNER JOIN dim_part dpr ON m.Dim_Partid = dpr.Dim_Partid
LEFT JOIN dim_producthierarchy dph  ON dph.ProductHierarchy = dpr.ProductHierarchy
                                    AND dph.RowIsCurrent = 1;
                                                   

						  
UPDATE fact_materialmovement_789tmp m
SET m.dim_MovementIndicatorid = IFNULL(dmi.dim_MovementIndicatorid,1)
FROM fact_materialmovement_789tmp m 
LEFT JOIN  dim_movementindicator dmi ON dmi.TypeCode = m.MSEG_KZBEW
                                     AND dmi.RowIsCurrent = 1;			


UPDATE fact_materialmovement_789tmp m
SET m.dim_Customerid = 	IFNULL(dcu.dim_Customerid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_customer dcu ON dcu.CustomerNumber = m.MSEG_KUNNR 
                           AND dcu.RowIsCurrent = 1;
					  
						
UPDATE fact_materialmovement_789tmp m
SET m.dim_specialstockid = IFNULL(spt.dim_specialstockid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_specialstock spt ON spt.specialstockindicator = MSEG_SOBKZ
                               AND spt.RowIsCurrent = 1;


UPDATE fact_materialmovement_789tmp m
SET m.dim_unitofmeasureid =  IFNULL(uom.Dim_UnitOfMeasureid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_unitofmeasure uom ON uom.UOM = MSEG_MEINS 
                                AND uom.RowIsCurrent = 1;
	 
				   
UPDATE fact_materialmovement_789tmp m
SET dim_controllingareaid = IFNULL(ca.dim_controllingareaid,1)
FROM  fact_materialmovement_789tmp m
LEFT JOIN  dim_controllingarea ca ON ca.ControllingAreaCode = MSEG_KOKRS;


UPDATE fact_materialmovement_789tmp m
SET m.dim_consumptiontypeid = IFNULL(ct.Dim_ConsumptionTypeid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_consumptiontype ct ON ct.ConsumptionCode = MSEG_KZVBR 
                                 AND ct.RowIsCurrent = 1;

				   
UPDATE fact_materialmovement_789tmp m
SET m.dim_stocktypeid = IFNULL(st.Dim_StockTypeid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_stocktype st ON st.TypeCode = MSEG_INSMK 
                           AND st.RowIsCurrent = 1;
	 

UPDATE fact_materialmovement_789tmp m
SET m.Dim_PurchaseGroupid = IFNULL(pg.Dim_PurchaseGroupid,1)
FROM fact_materialmovement_789tmp m
INNER JOIN dim_part dpr ON m.Dim_Partid = dpr.Dim_Partid
LEFT JOIN dim_purchasegroup pg ON dpr.PurchaseGroupCode = pg.PurchaseGroup;	
				

						

UPDATE fact_materialmovement_789tmp m
SET m.Dim_materialgroupid =  IFNULL(mg.Dim_materialgroupid,1)
FROM fact_materialmovement_789tmp m
INNER JOIN dim_part dpr ON m.Dim_Partid = dpr.Dim_Partid
LEFT JOIN  dim_materialgroup mg ON mg.MaterialGroupCode = dpr.MaterialGroup 
                                AND mg.RowIsCurrent = 1;








drop table if exists dim_costcenter_789;
create table dim_costcenter_789 as select * from dim_costcenter;

DROP TABLE IF EXISTS cstcent_tmp;

CREATE TABLE cstcent_tmp as 
SELECT distinct dim_CostCenterid,dcc.Code,dcc.validTo,dcc.ControllingArea
                            FROM dim_costcenter_789 dcc,MKPF_MSEG_RKWA m
                            WHERE     dcc.Code = m.MSEG_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG_KOKRS = dcc.ControllingArea
                                AND dcc.RowIsCurrent = 1;

UPDATE fact_materialmovement_789tmp fmt
SET dim_CostCenterid= ifnull(dim_CostCenterid,1)
FROM fact_materialmovement_789tmp fmt,cstcent_tmp dcc,MKPF_MSEG_RKWA m
        INNER JOIN dim_plant dp
                  ON m.MSEG_WERKS = dp.PlantCode AND dp.RowIsCurrent = 1
WHERE  NOT EXISTS
               (SELECT 1
                  FROM fact_materialmovement ms
                  WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                       AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                       AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)
AND fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND dcc.Code = m.MSEG_KOSTL
AND dcc.validTo >= m.MKPF_BUDAT
AND m.MSEG_KOKRS = dcc.ControllingArea
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR;

drop table if exists dim_costcenter_789;

INSERT INTO fact_materialmovement(fact_materialmovementid,
  			                   dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
							   Dim_Currencyid_TRA,
							   Dim_Currencyid_GBL,
							   amt_exchangerate,
							   amt_exchangerate_GBL,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               dd_ConsignmentFlag)
SELECT  max_holder_789.maxid + row_number() over(order by '') fact_materialmovementid, x.* FROM 
(SELECT DISTINCT 
dd_MaterialDocNo,
dd_MaterialDocItemNo,
dd_MaterialDocYear,
dd_DocumentNo,
dd_DocumentItemNo,
dd_SalesOrderNo,
dd_SalesOrderItemNo,
dd_SalesOrderDlvrNo,
dd_GLAccountNo,
dd_debitcreditid,
dd_productionordernumber,
dd_productionorderitemno,
dd_GoodsMoveReason,
dd_BatchNumber,
dd_ValuationType,
amt_LocalCurrAmt,
amt_StdUnitPrice,
amt_DeliveryCost,
amt_AltPriceControl,
amt_OnHand_ValStock,
ct_Quantity,
ct_QtyEntryUOM,
ct_QtyGROrdUnit,
ct_QtyOnHand_ValStock,
ct_QtyOrdPriceUnit,
Dim_MovementTypeid,
dim_Companyid,
Dim_Currencyid,
Dim_Currencyid_TRA,
Dim_Currencyid_GBL,
amt_exchangerate,
amt_exchangerate_GBL,
Dim_Partid,
Dim_Plantid,
Dim_StorageLocationid,
Dim_Vendorid,
dim_DateIDMaterialDocDate,
dim_DateIDPostingDate,
dim_producthierarchyid,
dim_MovementIndicatorid,
dim_Customerid,
dim_CostCenterid,
dim_specialstockid,
dim_unitofmeasureid,
dim_controllingareaid,
dim_consumptiontypeid,
dim_stocktypeid,
Dim_PurchaseGroupid,
dim_materialgroupid,
dd_ConsignmentFlag
FROM fact_materialmovement_789tmp) x
CROSS JOIN max_holder_789;
                               
drop table if exists max_holder_789;
DROP TABLE IF EXISTS fact_materialmovement_789tmp;

UPDATE fact_materialmovement ms
SET ms.dim_purchasegroupid = po.dim_purchasegroupid 
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE  ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	 AND po.Dim_Vendorid = v.Dim_Vendorid
     AND ms.Dim_Plantid = pl.Dim_Plantid
	 AND  po.dim_purchasegroupid <>1 
	 AND  ms.dim_purchasegroupid <> po.dim_purchasegroupid;


UPDATE fact_materialmovement ms
SET   ms.dim_purchaseorgid = po.dim_purchaseorgid
	 ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
      AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
      AND po.Dim_Vendorid = v.Dim_Vendorid
      AND ms.Dim_Plantid = pl.Dim_Plantid
      AND  ms.dim_purchaseorgid <> po.dim_purchaseorgid;


UPDATE fact_materialmovement ms
SET ms.dim_itemcategoryid = po.dim_itemcategoryid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND po.Dim_Vendorid = v.Dim_Vendorid
     AND ms.Dim_Plantid = pl.Dim_Plantid
     AND ms.dim_itemcategoryid <> po.dim_itemcategoryid;



UPDATE fact_materialmovement ms
SET ms.dim_Termid = po.dim_Termid
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND po.Dim_Vendorid = v.Dim_Vendorid
     AND ms.Dim_Plantid = pl.Dim_Plantid
     AND  ms.dim_Termid <> po.dim_Termid;


UPDATE fact_materialmovement ms
SET ms.dim_documenttypeid = po.dim_documenttypeid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
      AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
      AND po.Dim_Vendorid = v.Dim_Vendorid
      AND ms.Dim_Plantid = pl.Dim_Plantid
      AND  ms.dim_documenttypeid <> po.dim_documenttypeid;

UPDATE fact_materialmovement ms
SET ms.dim_accountcategoryid = po.dim_accountcategoryid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND po.Dim_Vendorid = v.Dim_Vendorid
     AND ms.Dim_Plantid = pl.Dim_Plantid
     AND  ms.dim_accountcategoryid <>  po.dim_accountcategoryid;

UPDATE fact_materialmovement ms
SET ms.dim_itemstatusid = po.dim_itemstatusid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND po.Dim_Vendorid = v.Dim_Vendorid
     AND ms.Dim_Plantid = pl.Dim_Plantid
     AND  ms.dim_itemstatusid <>  po.dim_itemstatusid;

DROP TABLE IF EXISTS tmp_upd_po_to_mm;
CREATE TABLE tmp_upd_po_to_mm
AS
SELECT po.*
FROM fact_purchase po,
(SELECT fp.dd_DocumentNo,fp.dd_DocumentItemNo,min(dd_scheduleno) dd_scheduleno
FROM fact_purchase fp GROUP BY fp.dd_DocumentNo,fp.dd_DocumentItemNo) t
WHERE po.dd_DocumentNo = t.dd_DocumentNo
AND po.dd_DocumentItemNo = t.dd_DocumentItemNo
AND po.dd_scheduleno = t.dd_scheduleno;

UPDATE fact_materialmovement ms
SET ms.Dim_DateIDDocCreation = po.Dim_DateidCreate
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_po_to_mm po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND po.Dim_Vendorid = v.Dim_Vendorid
     AND ms.Dim_Plantid = pl.Dim_Plantid
     AND ms.Dim_DateIDDocCreation <> po.Dim_DateidCreate;

UPDATE fact_materialmovement ms
SET ms.Dim_DateIDOrdered = po.Dim_DateidOrder
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_po_to_mm po,dim_vendor v, dim_plant pl
WHERE  ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND po.Dim_Vendorid = v.Dim_Vendorid
     AND ms.Dim_Plantid = pl.Dim_Plantid
     AND ms.Dim_DateIDOrdered <> po.Dim_DateidOrder;

UPDATE fact_materialmovement ms
SET ms.Dim_MaterialGroupid = po.Dim_MaterialGroupid
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_po_to_mm po,dim_vendor v, dim_plant pl
WHERE  ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND po.Dim_Vendorid = v.Dim_Vendorid
     AND ms.Dim_Plantid = pl.Dim_Plantid
     AND  ms.Dim_MaterialGroupid <> po.Dim_MaterialGroupid;

DROP TABLE IF EXISTS tmp_upd_po_to_mm;

DROP TABLE IF EXISTS tmp_upd_so_to_mm;
CREATE TABLE tmp_upd_so_to_mm
AS
SELECT po.*
FROM fact_salesorder po,
(SELECT fp.dd_SalesDocNo,fp.dd_SalesItemNo,min(dd_scheduleno) dd_scheduleno
FROM fact_salesorder fp GROUP BY fp.dd_SalesDocNo,fp.dd_SalesItemNo) t
WHERE po.dd_SalesDocNo = t.dd_SalesDocNo
AND po.dd_SalesItemNo = t.dd_SalesItemNo
AND po.dd_scheduleno = t.dd_scheduleno;
  
UPDATE fact_materialmovement ms
SET ms.dim_salesorderheaderstatusid = so.dim_salesorderheaderstatusid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesorderheaderstatusid <> so.dim_salesorderheaderstatusid;



UPDATE fact_materialmovement ms
SET  ms.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
	 ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesorderitemstatusid <> so.dim_salesorderitemstatusid;

UPDATE fact_materialmovement ms
SET ms.dim_salesdocumenttypeid = so.dim_salesdocumenttypeid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesdocumenttypeid <> so.dim_salesdocumenttypeid;

UPDATE fact_materialmovement ms
SET ms.dim_salesorderrejectreasonid = so.dim_salesorderrejectreasonid
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND ms.dim_salesorderrejectreasonid <> so.dim_salesorderrejectreasonid;

UPDATE fact_materialmovement ms
SET ms.dim_salesgroupid = so.dim_salesgroupid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesgroupid <> so.dim_salesgroupid;

UPDATE fact_materialmovement ms
SET ms.dim_salesorgid = so.dim_salesorgid
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesorgid <> so.dim_salesorgid;

UPDATE fact_materialmovement ms
SET ms.dim_customergroup1id = so.dim_customergroup1id
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo;

UPDATE fact_materialmovement ms
SET ms.dim_documentcategoryid = so.dim_documentcategoryid
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
    AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
    AND ms.dim_documentcategoryid <>  so.dim_documentcategoryid;

UPDATE fact_materialmovement ms
SET ms.Dim_DateIDDocCreation = so.Dim_DateidSalesOrderCreated
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND ms.Dim_DateIDDocCreation <> so.Dim_DateidSalesOrderCreated;

UPDATE fact_materialmovement ms
SET ms.Dim_DateIDOrdered = so.Dim_DateidSalesOrderCreated
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND ms.Dim_DateIDOrdered <> so.Dim_DateidSalesOrderCreated;
  
UPDATE fact_materialmovement ms
SET ms.dim_productionorderstatusid = po.dim_productionorderstatusid
   ,ms.dw_update_date = current_timestamp
FROM   fact_materialmovement ms,fact_productionorder po
WHERE  ms.dd_productionordernumber = po.dd_ordernumber
     AND ms.dd_productionorderitemno = po.dd_orderitemno
     AND ms.dim_productionorderstatusid<>  po.dim_productionorderstatusid;

UPDATE fact_materialmovement ms
SET  ms.dim_productionordertypeid = po.Dim_ordertypeid
    ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_productionorder po
WHERE  ms.dd_productionordernumber = po.dd_ordernumber
     AND ms.dd_productionorderitemno = po.dd_orderitemno
     AND  ms.dim_productionordertypeid <> po.Dim_ordertypeid;

 


