DROP TABLE IF EXISTS mdtb_pi00;

CREATE TABLE mdtb_pi00 AS 
SELECT MDTB_DELNR, MDTB_DTNUM, IFNULL(MDTB_UMDAT, MDTB_DAT01) DATEvalue_upd
FROM mdtb 
WHERE IFNULL(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL;

DROP TABLE IF EXISTS plaf_pi00;

create table plaf_pi00 as 
Select PLAF_UMREZ,
	PLAF_UMREN,
	PLAF_PLNUM,
	ifnull(PLAF_EMLIF, 'Not Set') PLAF_EMLIF, 
	ifnull(PLAF_FLIEF, 'Not Set') PLAF_FLIEF,
	ifnull(PLAF_KZVBR, 'Not Set') PLAF_KZVBR
from plaf
where ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL;

/*MERGE INTO fact_excessandshortage fact
USING(SELECT m.fact_excessandshortageid, IFNULL(z.StandardPrice,0) StandardPrice
	  FROM fact_excessandshortage m 
	   INNER JOIN plaf_pi00 po ON m.dd_DocumentNo = po.PLAF_PLNUM
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR 
	   INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
	   INNER JOIN dim_part p ON p.Dim_Partid = m.Dim_Partid
	   INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant
	   INNER JOIN Dim_Date dd ON to_date(dd.DateValue) = to_date(t.datevalue_upd) AND dd.Dim_Dateid <> 1
			 AND dd.CompanyCode = pl.CompanyCode 
       LEFT JOIN tmp_getStdPrice z
			            ON  z.pCompanyCode = pl.CompanyCode
						AND z.pPlant = pl.PlantCode
						AND z.pMaterialNo =  k.MDKP_MATNR
						AND z.pFiYear = dd.CalendarYear
						AND z.pPeriod =  dd.FinancialMonthNumber
						AND z.vUMREZ = PLAF_UMREZ
						AND z.vUMREN =   PLAF_UMREN
						AND z.fact_script_name = 'fact_excessandshortage'
						AND z.PONumber IS NULL
                        AND m.Dim_ActionStateid = 2
			           AND z.pUnitPrice =  0) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.StandardPrice*/

MERGE INTO fact_excessandshortage fact 
USING(SELECT m.fact_excessandshortageid, IFNULL(max(z.StandardPrice),0) StandardPrice 
FROM fact_excessandshortage m INNER JOIN plaf_pi00 po 
ON m.dd_DocumentNo = po.PLAF_PLNUM INNER JOIN mdtb_pi00 t 
ON m.dd_DocumentNo = t.MDTB_DELNR INNER JOIN mdkp k 
ON k.MDKP_DTNUM = t.MDTB_DTNUM INNER JOIN dim_part p 
ON p.Dim_Partid = m.Dim_Partid INNER JOIN dim_plant pl 
ON pl.PlantCode = p.Plant INNER JOIN Dim_Date dd 
ON to_date(dd.DateValue) = to_date(t.datevalue_upd) 
AND dd.Dim_Dateid <> 1 
AND dd.CompanyCode = pl.CompanyCode
LEFT JOIN tmp_getStdPrice z ON z.pCompanyCode = pl.CompanyCode 
AND z.pPlant = pl.PlantCode 
AND z.pMaterialNo = k.MDKP_MATNR 
AND z.pFiYear = dd.CalendarYear 
AND z.pPeriod = dd.FinancialMonthNumber 
AND z.vUMREZ = PLAF_UMREZ 
AND z.vUMREN = PLAF_UMREN 
AND z.fact_script_name = 'fact_excessandshortage' 
AND z.PONumber IS NULL 
AND m.Dim_ActionStateid = 2 
AND z.pUnitPrice = 0
group by m.fact_excessandshortageid) src 
ON fact.fact_excessandshortageid = src.fact_excessandshortageid 
WHEN MATCHED THEN 
UPDATE SET fact.amt_UnitPrice = src.StandardPrice;	

drop table if exists mdtb_pi00;
drop table if exists plaf_pi00;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,
			  p.Dim_ItemCategoryid,
			  p.Dim_Vendorid,
			  p.Dim_DocumentTypeid,
			  ifnull((p.amt_DeliveryTotal / (case when p.ct_BaseUOMQty = 0 
			                                      then null 
												  else p.ct_BaseUOMQty 
										     end ))
				     ,0) as amt_UnitPrice,
			  ifnull((p.amt_DeliveryTotal / (case when p.ct_BaseUOMQty = 0 
			                                      then null 
												  else p.ct_BaseUOMQty 
										     end))
					 ,0) * p.amt_ExchangeRate_GBL as amt_UnitPrice_GBL,
			  (ct_DeliveryQty - ct_ReceivedQty) as ct_QtyOpenOrder,
			   p.Dim_ConsumptionTypeid 
	   FROM  fact_excessandshortage m
	         INNER JOIN fact_purchase p ON m.dd_DocumentNo = p.dd_DocumentNo
										AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
										AND m.dd_ScheduleNo = p.dd_ScheduleNo
			 INNER JOIN dim_vendor v ON v.Dim_Vendorid = p.Dim_Vendorid
             INNER JOIN dim_date dt ON p.Dim_DateidDelivery = dt.Dim_Dateid
		WHERE  m.Dim_ActionStateid = 2
               AND dt.Dim_Dateid <> 1) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_ItemCategoryid = src.Dim_ItemCategoryid,
	fact.Dim_Vendorid = src.Dim_Vendorid,
	fact.Dim_DocumentTypeid = src.Dim_DocumentTypeid,
	fact.amt_UnitPrice = src.amt_UnitPrice,
	fact.amt_UnitPrice_GBL = src.amt_UnitPrice_GBL,
	fact.ct_QtyOpenOrder = src.ct_QtyOpenOrder,
	fact.dim_consumptiontypeid = src.Dim_ConsumptionTypeid;	

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.Dim_SalesDocumentTypeid, 1)) Dim_SalesDocumentTypeid
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
           AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_SalesDocumentTypeid = src.Dim_SalesDocumentTypeid
WHERE fact.Dim_SalesDocumentTypeid <> src.Dim_SalesDocumentTypeid;
			 
MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.Dim_CustomerGroup1id, 1)) Dim_CustomerGroup1id
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
             AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_CustomerGroup1id = src.Dim_CustomerGroup1id
WHERE fact.Dim_CustomerGroup1id <> src.Dim_CustomerGroup1id;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.Dim_CustomerID, 1)) Dim_CustomerID
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
             AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_CustomerID = src.Dim_CustomerID
WHERE fact.Dim_CustomerID <> src.Dim_CustomerID;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.Dim_SalesOrderRejectReasonid, 1)) Dim_SalesOrderRejectReasonid
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
           AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_SalesOrderRejectReasonid = src.Dim_SalesOrderRejectReasonid
WHERE fact.Dim_SalesOrderRejectReasonid <> src.Dim_SalesOrderRejectReasonid;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.Dim_SalesOrderItemStatusid, 1)) Dim_SalesOrderItemStatusid
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
            AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_SalesOrderItemStatusid = src.Dim_SalesOrderItemStatusid
WHERE fact.Dim_SalesOrderItemStatusid <> src.Dim_SalesOrderItemStatusid;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.Dim_SalesOrderHeaderStatusid, 1)) Dim_SalesOrderHeaderStatusid
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
          AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY  m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_SalesOrderHeaderStatusid = src.Dim_SalesOrderHeaderStatusid
WHERE fact.Dim_SalesOrderHeaderStatusid <> src.Dim_SalesOrderHeaderStatusid;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.Dim_DocumentCategoryid, 1)) Dim_DocumentCategoryid
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
             AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_DocumentCategoryid = src.Dim_DocumentCategoryid
WHERE fact.Dim_DocumentCategoryid <> src.Dim_DocumentCategoryid;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.amt_UnitPrice,0)/(case when p.ct_PriceUnit = 0 
                                                                        then null 
																		else p.ct_PriceUnit 
																		end)) amt_UnitPrice
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
            AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.amt_UnitPrice
WHERE fact.amt_UnitPrice <> src.amt_UnitPrice;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(ifnull(p.amt_UnitPrice,0)/(case when p.ct_PriceUnit = 0 
                                                                        then null 
																		else p.ct_PriceUnit 
																		end)
										   * p.amt_ExchangeRate_GBL) amt_UnitPrice_GBL
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
            AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice_GBL = src.amt_UnitPrice_GBL
WHERE fact.amt_UnitPrice_GBL <> src.amt_UnitPrice_GBL;


MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,max(CASE
											 WHEN (ct_ScheduleQtySalesUnit - ct_DeliveredQty) < 0 THEN 0
											 ELSE (ct_ScheduleQtySalesUnit - ct_DeliveredQty)
										  END) ct_QtyOpenOrder
	   FROM  fact_excessandshortage m
	         INNER JOIN fact_salesorder p ON  m.dd_DocumentNo = p.dd_SalesDocNo
										  AND m.dd_DocumentItemNo = p.dd_SalesItemNo
										  AND m.dd_ScheduleNo = p.dd_ScheduleNo
	    WHERE m.Dim_ActionStateid = 2
            AND  m.dd_DocumentNo <> 'Not Set'
		GROUP BY m.fact_excessandshortageid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.ct_QtyOpenOrder = src.ct_QtyOpenOrder
WHERE fact.ct_QtyOpenOrder <> src.ct_QtyOpenOrder;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,
			  ifnull((p.amt_estimatedTotalCost/(case when p.ct_TotalOrderQty = 0 
													 then null 
													 else p.ct_TotalOrderQty 
													 end))
			  , 0) amt_UnitPrice
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_productionorder p ON  m.dd_DocumentNo = p.dd_ordernumber
										  AND m.dd_DocumentItemNo = p.dd_orderitemno
	    WHERE m.Dim_ActionStateid = 2
             AND  m.dd_DocumentNo <> 'Not Set') src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.amt_UnitPrice
WHERE fact.amt_UnitPrice <> src.amt_UnitPrice;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,
			  ifnull((p.amt_estimatedTotalCost/(case when p.ct_TotalOrderQty = 0 
													 then null 
													 else p.ct_TotalOrderQty 
													 end))
			  , 0) * p.amt_ExchangeRate_GBL amt_UnitPrice_GBL
	   FROM  fact_excessandshortage m
	         LEFT JOIN fact_productionorder p ON  m.dd_DocumentNo = p.dd_ordernumber
										  AND m.dd_DocumentItemNo = p.dd_orderitemno
	    WHERE m.Dim_ActionStateid = 2
              AND  m.dd_DocumentNo <> 'Not Set') src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice_GBL = src.amt_UnitPrice_GBL
WHERE fact.amt_UnitPrice_GBL <> src.amt_UnitPrice_GBL;

drop table if exists mdtb_pi00;

create table mdtb_pi00 as 
Select MDTB_DELNR,MDTB_DELPS,MDTB_DTNUM from mdtb 
where ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL and MDTB_DAT02 IS NOT NULL;

MERGE INTO fact_excessandshortage fact
USING (SELECT ic.Dim_ItemCategoryid, m.fact_excessandshortageid 
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
	    WHERE m.Dim_ActionStateid = 2 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_ItemCategoryid = src.Dim_ItemCategoryid
WHERE fact.Dim_ItemCategoryid <> src.Dim_ItemCategoryid;

MERGE INTO fact_excessandshortage fact
USING (SELECT v.Dim_Vendorid, m.fact_excessandshortageid 
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
	    WHERE m.Dim_ActionStateid = 2 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_Vendorid = src.Dim_Vendorid
WHERE fact.Dim_Vendorid <> src.Dim_Vendorid;

MERGE INTO fact_excessandshortage fact
USING (SELECT fv.Dim_Vendorid, m.fact_excessandshortageid 
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
		WHERE m.Dim_ActionStateid = 2 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_Vendorid = src.Dim_Vendorid
WHERE fact.Dim_Vendorid <> src.Dim_Vendorid;

MERGE INTO fact_excessandshortage fact
USING (SELECT dt.Dim_DocumentTypeid, m.fact_excessandshortageid 
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
		WHERE m.Dim_ActionStateid = 2 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_DocumentTypeid = src.Dim_DocumentTypeid
WHERE fact.Dim_DocumentTypeid <> src.Dim_DocumentTypeid;

MERGE INTO fact_excessandshortage fact
USING (SELECT ct.Dim_ConsumptionTypeid, m.fact_excessandshortageid 
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
		WHERE m.Dim_ActionStateid = 2 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_ConsumptionTypeid = src.Dim_ConsumptionTypeid
WHERE fact.Dim_ConsumptionTypeid <> src.Dim_ConsumptionTypeid;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,
	   ifnull(EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 
										then null 
										else EBAN_PEINH 
										end
								   ), 1), 0) amt_UnitPrice
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
		WHERE m.Dim_ActionStateid = 2 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.amt_UnitPrice
WHERE fact.amt_UnitPrice <> src.amt_UnitPrice;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,
	   ifnull(EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 
										then null 
										else EBAN_PEINH 
										end
								   ), 1), 0) * IFNULL(ex.exchangeRate,1) as amt_UnitPrice_GBL
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
	   LEFT JOIN (SELECT exchangeRate,pFromCurrency, pDate
				  FROM tmp_getExchangeRate1 t1 CROSS JOIN systemproperty sp
				  WHERE t1.pFromExchangeRate is null
						and t1.fact_script_name = 'bi_populate_excessandshortage_fact'
						and t1.pToCurrency = IFNULL(sp.property_value,'USD')
						and sp.property = 'customer.global.currency') ex  
			ON ex.pFromCurrency = pr.EBAN_WAERS and ex.pDate = pr.eban_BADAT
       WHERE m.Dim_ActionStateid = 2 
			 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
								) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice_GBL = src.amt_UnitPrice_GBL
WHERE fact.amt_UnitPrice_GBL <> src.amt_UnitPrice_GBL;

MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,
			  MAX(b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end)) as amt_UnitPrice
	   FROM   fact_excessandshortage m
	          INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid 
	          INNER JOIN dim_plant p ON p.Dim_Plantid = m.Dim_Plantid
			  INNER JOIN mbew b ON b.MATNR = dp.PartNumber
								AND b.BWKEY = p.ValuationArea
	    WHERE b.BWTAR IS NULL
			  AND b.MBEW_LBKUM > 0
			  AND m.Dim_ActionStateid = 2
		GROUP BY m.fact_excessandshortageid
	    ) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.amt_UnitPrice
WHERE fact.amt_UnitPrice <> src.amt_UnitPrice AND ifnull(fact.amt_UnitPrice, 0) = 0;

Drop table if exists pGlobalCurrency_po_41;
Drop table if exists mdtb_pi00;















































