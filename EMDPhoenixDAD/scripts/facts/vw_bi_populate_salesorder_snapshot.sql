/* fact_salesorder_snapshot - 25 may 2016 - Marius C.   */
/* SA created to preserve Backorders history for EMD LS */

drop table if exists tmp_dayofprocessing_var_snp;
create table tmp_dayofprocessing_var_snp
as
select case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end as processing_date from (select 1) a;

delete from fact_salesorder_snapshot
where dim_dateidsnapshot = (select dim_dateid from dim_date where companycode = 'Not Set' and datevalue = (select processing_date from tmp_dayofprocessing_var_snp));


delete from number_fountain m where m.table_name = 'fact_salesorder_snapshot';

insert into number_fountain
select 	'fact_salesorder_snapshot',
	ifnull(max(d.fact_salesorder_snapshotid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_salesorder_snapshot d;

insert into fact_salesorder_snapshot (
FACT_SALESORDER_SNAPSHOTID
,DD_SALESDOCNO
,DD_SALESITEMNO
,DD_SCHEDULENO
,CT_SCHEDULEQTYSALESUNIT
,CT_CONFIRMEDQTY
,CT_CORRECTEDQTY
,CT_DELIVEREDQTY
,CT_ONHANDCOVEREDQTY
,AMT_UNITPRICE
,CT_PRICEUNIT
,AMT_SCHEDULETOTAL
,AMT_STDCOST
,AMT_TARGETVALUE
,CT_TARGETQTY
,AMT_EXCHANGERATE
,CT_OVERDLVRTOLERANCE
,CT_UNDERDLVRTOLERANCE
,DIM_DATEIDSALESORDERCREATED
,DIM_DATEIDSCHEDDELIVERYREQ
,DIM_DATEIDSCHEDDLVRREQPREV
,DIM_DATEIDSCHEDDELIVERY
,DIM_DATEIDGOODSISSUE
,DIM_DATEIDMTRLAVAIL
,DIM_DATEIDLOADING
,DIM_DATEIDTRANSPORT
,DIM_CURRENCYID
,DIM_PRODUCTHIERARCHYID
,DIM_PLANTID
,DIM_COMPANYID
,DIM_STORAGELOCATIONID
,DIM_SALESDIVISIONID
,DIM_SHIPRECEIVEPOINTID
,DIM_DOCUMENTCATEGORYID
,DIM_SALESDOCUMENTTYPEID
,DIM_SALESORGID
,DIM_CUSTOMERID
,DIM_DATEIDVALIDFROM
,DIM_DATEIDVALIDTO
,DIM_SALESGROUPID
,DIM_COSTCENTERID
,DIM_CONTROLLINGAREAID
,DIM_BILLINGBLOCKID
,DIM_TRANSACTIONGROUPID
,DIM_SALESORDERREJECTREASONID
,DIM_PARTID
,DIM_PARTSALESID
,DIM_SALESORDERHEADERSTATUSID
,DIM_SALESORDERITEMSTATUSID
,DIM_CUSTOMERGROUP1ID
,DIM_CUSTOMERGROUP2ID
,DIM_SALESORDERITEMCATEGORYID
,AMT_EXCHANGERATE_GBL
,CT_FILLQTY
,CT_ONHANDQTY
,DIM_DATEIDFIRSTDATE
,DIM_SCHEDULELINECATEGORYID
,DIM_SALESMISCID
,DD_ITEMRELFORDELV
,DIM_DATEIDSHIPMENTDELIVERY
,DIM_DATEIDACTUALGI
,DIM_DATEIDSHIPDLVRFILL
,DIM_DATEIDACTUALGIFILL
,DIM_PROFITCENTERID
,DIM_CUSTOMERIDSHIPTO
,DIM_DATEIDGUARANTEEDATE
,DIM_UNITOFMEASUREID
,DIM_DISTRIBUTIONCHANNELID
,DD_BATCHNO
,DD_CREATEDBY
,DIM_CUSTOMPARTNERFUNCTIONID
,DIM_DATEIDLOADINGFILL
,DIM_PAYERPARTNERFUNCTIONID
,DIM_BILLTOPARTYPARTNERFUNCTIONID
,CT_SHIPPEDAGNSTORDERQTY
,CT_CMLQTYRECEIVED
,DIM_CUSTOMPARTNERFUNCTIONID1
,DIM_CUSTOMPARTNERFUNCTIONID2
,DIM_CUSTOMERGROUPID
,DIM_SALESOFFICEID
,DIM_HIGHERCUSTOMERID
,DIM_HIGHERSALESORGID
,DIM_HIGHERDISTCHANNELID
,DIM_HIGHERSALESDIVID
,DD_SOLDTOHIERARCHYLEVEL
,DD_HIERARCHYTYPE
,DIM_TOPLEVELCUSTOMERID
,DIM_TOPLEVELDISTCHANNELID
,DIM_TOPLEVELSALESDIVID
,DIM_TOPLEVELSALESORGID
,CT_AFSUNALLOCATEDQTY
,AMT_AFSUNALLOCATEDVALUE
,CT_AFSALLOCATIONRQTY
,CT_CUMCONFIRMEDQTY
,CT_AFSALLOCATIONFQTY
,CT_CUMORDERQTY
,AMT_AFSONDELIVERYVALUE
,CT_SHIPPEDORBILLEDQTY
,AMT_SHIPPEDORBILLED
,DIM_CREDITREPRESENTATIVEID
,DD_CUSTOMERPONO
,CT_INCOMPLETEQTY
,AMT_INCOMPLETE
,DIM_DELIVERYBLOCKID
,DD_AFSSTOCKCATEGORY
,DD_AFSSTOCKTYPE
,DIM_DATEIDAFSCANCELDATE
,AMT_AFSNETPRICE
,AMT_AFSNETVALUE
,AMT_BASEPRICE
,DIM_MATERIALPRICEGROUP1ID
,AMT_CREDITHOLDVALUE
,AMT_DELIVERYBLOCKVALUE
,DIM_AFSSIZEID
,DIM_AFSREJECTIONREASONID
,DIM_DATEIDDLVRDOCCREATED
,AMT_CUSTOMEREXPECTEDPRICE
,CT_AFSALLOCATEDQTY
,CT_AFSONDELIVERYQTY
,DIM_OVERALLSTATUSCREDITCHECKID
,DIM_SALESDISTRICTID
,DIM_ACCOUNTASSIGNMENTGROUPID
,DIM_MATERIALGROUPID
,DIM_SALESDOCORDERREASONID
,AMT_SUBTOTAL3
,AMT_SUBTOTAL4
,DIM_DATEIDAFSREQDELIVERY
,AMT_DICOUNTACCRUALNETPRICE
,CT_AFSOPENQTY
,DIM_PURCHASEORDERTYPEID
,DIM_DATEIDQUOTATIONVALIDFROM
,DIM_DATEIDPURCHASEORDER
,DIM_DATEIDQUOTATIONVALIDTO
,DIM_AFSSEASONID
,DD_AFSDEPARTMENT
,DIM_DATEIDSOCREATED
,DIM_DATEIDSODOCUMENT
,DD_SUBSEQUENTDOCNO
,DD_SUBSDOCITEMNO
,DD_SUBSSCHEDULENO
,DIM_SUBSDOCCATEGORYID
,CT_AFSTOTALDRAWN
,DIM_DATEIDASLASTDATE
,DD_REFERENCEDOCUMENTNO
,DD_REQUIREMENTTYPE
,AMT_STDPRICE
,DIM_DATEIDNEXTDATE
,AMT_TAX
,DIM_DATEIDEARLIESTSOCANCELDATE
,DIM_DATEIDLATESTCUSTPOAGI
,DD_BUSINESSCUSTOMERPONO
,DIM_ROUTEID
,DIM_BILLINGDATEID
,DIM_CUSTOMERPAYMENTTERMSID
,DIM_SALESRISKCATEGORYID
,DD_CREDITREP
,DD_CREDITLIMIT
,DIM_CUSTOMERRISKCATEGORYID
,CT_FILLQTY_CRD
,CT_FILLQTY_PDD
,DIM_H1CUSTOMERID
,DIM_DATEIDSOITEMCHANGEDON
,DIM_SHIPPINGCONDITIONID
,DD_AFSALLOCATIONGROUPNO
,DIM_DATEIDREJECTION
,DD_CUSTOMERMATERIALNO
,DIM_BASEUOMID
,DIM_SALESUOMID
,AMT_UNITPRICEUOM
,DIM_DATEIDFIXEDVALUE
,DIM_PAYMENTTERMSID
,DIM_DATEIDNETDUEDATE
,DIM_MATERIALPRICEGROUP4ID
,DIM_MATERIALPRICEGROUP5ID
,AMT_SUBTOTAL3_ORDERQTY
,AMT_SUBTOTAL3INCUSTCONFIG_BILLING
,DIM_CUSTOMERMASTERSALESID
,DD_SALESORDERBLOCKED
,DD_SOCREATETIME
,DD_REQDELIVERYTIME
,DD_SOLINECREATETIME
,DD_DELIVERYTIME
,DD_PLANNEDGITIME
,DIM_DATEIDARPOSTING
,DIM_CURRENCYID_TRA
,DIM_CURRENCYID_GBL
,DIM_CURRENCYID_STAT
,AMT_EXCHANGERATE_STAT
,DD_ARUNNUMBER
,DIM_AVAILABILITYCHECKID
,DD_IDOCNUMBER
,DIM_INCOTERMID
,DD_INTERNATIONALARTICLENO
,DD_RELEASERULE
,DIM_DELIVERYPRIORITYID
,DD_REFERENCEDOCITEMNO
,DIM_SALESORDERPARTNERID
,DD_PURCHASEREQUISITION
,DD_ITEMOFREQUISITION
,CT_AFSCALCULATEDOPENQTY
,AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE
,DD_DOCUMENTCONDITIONNO
,AMT_SUBTOTAL1
,AMT_SUBTOTAL2
,AMT_SUBTOTAL5
,AMT_SUBTOTAL6
,DD_HIGHLEVELITEM
,DD_PODOCUMENTNO
,DD_PODOCUMENTITEMNO
,DD_POSCHEDULENO
,DD_PRODORDERNO
,DD_PRODORDERITEMNO
,DIM_CUSTOMERCONDITIONGROUPS1ID
,DIM_CUSTOMERCONDITIONGROUPS2ID
,DIM_CUSTOMERCONDITIONGROUPS3ID
,CT_AFSOPENPOQTY
,CT_AFSINTRANSITQTY
,DIM_SCHEDULEDELIVERYBLOCKID
,DIM_CUSTOMERGROUP4ID
,DD_OPENCONFIRMATIONSEXISTS
,DD_CONDITIONNO
,DD_CREDITMGR
,DD_CLEAREDBLOCKEDSTS
,CT_AFSINRDDUNITS
,CT_AFSAFTERRDDUNITS
,DIM_AGREEMENTSID
,DIM_CHARGEBACKCONTRACTID
,DD_RO_MATERIALDESC
,DIM_CUSTOMERIDCBOWNER
,CT_COMMITTEDQTY
,DIM_DATEIDREQDELIVLINE
,DIM_MATERIALPRICINGGROUPID
,DIM_BILLINGBLOCKSALESDOCLVLID
,DD_DELIVERYINDICATOR
,DD_SHIPTOPARTYSTREET
,DIM_CUSTOMERCONDITIONGROUPS4ID
,DIM_CUSTOMERCONDITIONGROUPS5ID
,DD_PURCHASEORDERITEM
,DIM_MATERIALPRICEGROUP2ID
,DIM_PROJECTSOURCEID
,DD_BILLING_NO
,DIM_MDG_PARTID
,DIM_BWPRODUCTHIERARCHYID
,CT_FIRSTNONZEROCONFIRMQTY
,CT_VOLUME
,CT_NOTCONFIRMEDQTY
,DD_CUSTOMCUSTEXPPRICEINC
,DD_DOCUMENTFLOWGROUP
,DD_ORDERBY
,DD_CUSTOMPRICEMISSINGINC
,DIM_CUSTOMERENDUSERFORFTRADE
,DD_PACKHOLDFLAG
,DD_PURCHASEREQ
,AMT_NETVALUE
,DIM_DATEIDREQUESTED_EMD
,DIM_DATEIDEXPECTEDSHIP_EMD
,DIM_DATEIDCONFIRMEDDELIVERY_EMD
,DIM_ACCORDINGGIDATEMTO_EMD
,CT_BASEUOMRATIO
,DD_MERCKLSBACKORDERFILTER
,DD_OPENQTYCVRDBYMAT
,AMT_EXCHANGERATE_CUSTOM
,DD_INTERCOMPFLAG
,DD_LSINTERCOMPFLAG
,DIM_COMMERCIALVIEWID
,CT_BASEUOMRATIOKG
,DIM_CLUSTERID
,DD_OPENQTYCVRDBYPLANT
,DIM_DATEIDSNAPSHOT
,CT_COUNTSALESDOCITEM
,dim_countryhierpsid
,dim_countryhierarid
,dim_gsamapimportid
,dim_customer_shipto
,dim_productionschedulerid
,dd_inco1
,dd_inco2
,dd_ship_to_party
,dd_mtomts
,DIM_ADDRESSID
,dd_CompleteInSingleDelivery
,dd_batch_split
,dd_delivery_group
,DIM_BLOCKEDBACKORDERBYPLANTID
,amt_openorderpm
,ct_openorder
,amt_order
,amt_openorder
)
select
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_salesorder_snapshot')
          + row_number()
             over(order by '') FACT_SALESORDER_SNAPSHOTID
,ifnull(f.DD_SALESDOCNO,'Not Set')
,ifnull(f.DD_SALESITEMNO,0)
,ifnull(f.DD_SCHEDULENO,0)
,ifnull(f.CT_SCHEDULEQTYSALESUNIT,0)
,ifnull(f.CT_CONFIRMEDQTY,0)
,ifnull(f.CT_CORRECTEDQTY,0)
,ifnull(f.CT_DELIVEREDQTY,0)
,ifnull(f.CT_ONHANDCOVEREDQTY,0)
,ifnull(f.AMT_UNITPRICE,0)
,ifnull(f.CT_PRICEUNIT,1)
,ifnull(f.AMT_SCHEDULETOTAL,0)
,ifnull(f.AMT_STDCOST,0)
,ifnull(f.AMT_TARGETVALUE,0)
,ifnull(f.CT_TARGETQTY,0)
,ifnull(f.AMT_EXCHANGERATE,1)
,ifnull(f.CT_OVERDLVRTOLERANCE,0)
,ifnull(f.CT_UNDERDLVRTOLERANCE,0)
,ifnull(f.DIM_DATEIDSALESORDERCREATED,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERYREQ,1)
,ifnull(f.DIM_DATEIDSCHEDDLVRREQPREV,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERY,1)
,ifnull(f.DIM_DATEIDGOODSISSUE,1)
,ifnull(f.DIM_DATEIDMTRLAVAIL,1)
,ifnull(f.DIM_DATEIDLOADING,1)
,ifnull(f.DIM_DATEIDTRANSPORT,1)
,ifnull(f.DIM_CURRENCYID,1)
,ifnull(f.DIM_PRODUCTHIERARCHYID,1)
,ifnull(f.DIM_PLANTID,1)
,ifnull(f.DIM_COMPANYID,1)
,ifnull(f.DIM_STORAGELOCATIONID,1)
,ifnull(f.DIM_SALESDIVISIONID,1)
,ifnull(f.DIM_SHIPRECEIVEPOINTID,1)
,ifnull(f.DIM_DOCUMENTCATEGORYID,1)
,ifnull(f.DIM_SALESDOCUMENTTYPEID,1)
,ifnull(f.DIM_SALESORGID,1)
,ifnull(f.DIM_CUSTOMERID,1)
,ifnull(f.DIM_DATEIDVALIDFROM,1)
,ifnull(f.DIM_DATEIDVALIDTO,1)
,ifnull(f.DIM_SALESGROUPID,0)
,ifnull(f.DIM_COSTCENTERID,1)
,ifnull(f.DIM_CONTROLLINGAREAID,0)
,ifnull(f.DIM_BILLINGBLOCKID,0)
,ifnull(f.DIM_TRANSACTIONGROUPID,0)
,ifnull(f.DIM_SALESORDERREJECTREASONID,0)
,ifnull(f.DIM_PARTID,1)
,ifnull(f.DIM_PARTSALESID,1)
,ifnull(f.DIM_SALESORDERHEADERSTATUSID,0)
,ifnull(f.DIM_SALESORDERITEMSTATUSID,0)
,ifnull(f.DIM_CUSTOMERGROUP1ID,0)
,ifnull(f.DIM_CUSTOMERGROUP2ID,0)
,ifnull(f.DIM_SALESORDERITEMCATEGORYID,1)
,ifnull(f.AMT_EXCHANGERATE_GBL,1)
,ifnull(f.CT_FILLQTY,0)
,ifnull(f.CT_ONHANDQTY,0)
,ifnull(f.DIM_DATEIDFIRSTDATE,1)
,ifnull(f.DIM_SCHEDULELINECATEGORYID,1)
,ifnull(f.DIM_SALESMISCID,1)
,ifnull(f.DD_ITEMRELFORDELV,'Not Set')
,ifnull(f.DIM_DATEIDSHIPMENTDELIVERY,1)
,ifnull(f.DIM_DATEIDACTUALGI,1)
,ifnull(f.DIM_DATEIDSHIPDLVRFILL,1)
,ifnull(f.DIM_DATEIDACTUALGIFILL,1)
,ifnull(f.DIM_PROFITCENTERID,1)
,ifnull(f.DIM_CUSTOMERIDSHIPTO,1)
,ifnull(f.DIM_DATEIDGUARANTEEDATE,1)
,ifnull(f.DIM_UNITOFMEASUREID,1)
,ifnull(f.DIM_DISTRIBUTIONCHANNELID,1)
,ifnull(f.DD_BATCHNO,'Not Set')
,ifnull(f.DD_CREATEDBY,'Not Set')
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID,1)
,ifnull(f.DIM_DATEIDLOADINGFILL,1)
,ifnull(f.DIM_PAYERPARTNERFUNCTIONID,1)
,ifnull(f.DIM_BILLTOPARTYPARTNERFUNCTIONID,1)
,ifnull(f.CT_SHIPPEDAGNSTORDERQTY,0)
,ifnull(f.CT_CMLQTYRECEIVED,0)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID1,1)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID2,1)
,ifnull(f.DIM_CUSTOMERGROUPID,1)
,ifnull(f.DIM_SALESOFFICEID,1)
,ifnull(f.DIM_HIGHERCUSTOMERID,1)
,ifnull(f.DIM_HIGHERSALESORGID,1)
,ifnull(f.DIM_HIGHERDISTCHANNELID,1)
,ifnull(f.DIM_HIGHERSALESDIVID,1)
,ifnull(f.DD_SOLDTOHIERARCHYLEVEL,0)
,ifnull(f.DD_HIERARCHYTYPE,'Not Set')
,ifnull(f.DIM_TOPLEVELCUSTOMERID,1)
,ifnull(f.DIM_TOPLEVELDISTCHANNELID,1)
,ifnull(f.DIM_TOPLEVELSALESDIVID,1)
,ifnull(f.DIM_TOPLEVELSALESORGID,1)
,ifnull(f.CT_AFSUNALLOCATEDQTY,0)
,ifnull(f.AMT_AFSUNALLOCATEDVALUE,0)
,ifnull(f.CT_AFSALLOCATIONRQTY,0)
,ifnull(f.CT_CUMCONFIRMEDQTY,0)
,ifnull(f.CT_AFSALLOCATIONFQTY,0)
,ifnull(f.CT_CUMORDERQTY,0)
,ifnull(f.AMT_AFSONDELIVERYVALUE,0)
,ifnull(f.CT_SHIPPEDORBILLEDQTY,0)
,ifnull(f.AMT_SHIPPEDORBILLED,0)
,ifnull(f.DIM_CREDITREPRESENTATIVEID,1)
,ifnull(f.DD_CUSTOMERPONO,'Not Set')
,ifnull(f.CT_INCOMPLETEQTY,0)
,ifnull(f.AMT_INCOMPLETE,0)
,ifnull(f.DIM_DELIVERYBLOCKID,1)
,ifnull(f.DD_AFSSTOCKCATEGORY,'Not Set')
,ifnull(f.DD_AFSSTOCKTYPE,'Not Set')
,ifnull(f.DIM_DATEIDAFSCANCELDATE,1)
,ifnull(f.AMT_AFSNETPRICE,0)
,ifnull(f.AMT_AFSNETVALUE,0)
,ifnull(f.AMT_BASEPRICE,0)
,ifnull(f.DIM_MATERIALPRICEGROUP1ID,1)
,ifnull(f.AMT_CREDITHOLDVALUE,0)
,ifnull(f.AMT_DELIVERYBLOCKVALUE,0)
,ifnull(f.DIM_AFSSIZEID,1)
,ifnull(f.DIM_AFSREJECTIONREASONID,1)
,ifnull(f.DIM_DATEIDDLVRDOCCREATED,1)
,ifnull(f.AMT_CUSTOMEREXPECTEDPRICE,0)
,ifnull(f.CT_AFSALLOCATEDQTY,0)
,ifnull(f.CT_AFSONDELIVERYQTY,0)
,ifnull(f.DIM_OVERALLSTATUSCREDITCHECKID,1)
,ifnull(f.DIM_SALESDISTRICTID,1)
,ifnull(f.DIM_ACCOUNTASSIGNMENTGROUPID,1)
,ifnull(f.DIM_MATERIALGROUPID,1)
,ifnull(f.DIM_SALESDOCORDERREASONID,1)
,ifnull(f.AMT_SUBTOTAL3,0)
,ifnull(f.AMT_SUBTOTAL4,0)
,ifnull(f.DIM_DATEIDAFSREQDELIVERY,1)
,ifnull(f.AMT_DICOUNTACCRUALNETPRICE,0)
,ifnull(f.CT_AFSOPENQTY,0)
,ifnull(f.DIM_PURCHASEORDERTYPEID,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDFROM,1)
,ifnull(f.DIM_DATEIDPURCHASEORDER,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDTO,1)
,ifnull(f.DIM_AFSSEASONID,1)
,ifnull(f.DD_AFSDEPARTMENT,'Not Set')
,ifnull(f.DIM_DATEIDSOCREATED,1)
,ifnull(f.DIM_DATEIDSODOCUMENT,1)
,ifnull(f.DD_SUBSEQUENTDOCNO,'Not Set')
,ifnull(f.DD_SUBSDOCITEMNO,0)
,ifnull(f.DD_SUBSSCHEDULENO,0)
,ifnull(f.DIM_SUBSDOCCATEGORYID,1)
,ifnull(f.CT_AFSTOTALDRAWN,0)
,ifnull(f.DIM_DATEIDASLASTDATE,1)
,ifnull(f.DD_REFERENCEDOCUMENTNO,'Not Set')
,ifnull(f.DD_REQUIREMENTTYPE,'Not Set')
,ifnull(f.AMT_STDPRICE,0)
,ifnull(f.DIM_DATEIDNEXTDATE,1)
,ifnull(f.AMT_TAX,0)
,ifnull(f.DIM_DATEIDEARLIESTSOCANCELDATE,1)
,ifnull(f.DIM_DATEIDLATESTCUSTPOAGI,1)
,ifnull(f.DD_BUSINESSCUSTOMERPONO,'Not Set')
,ifnull(f.DIM_ROUTEID,1)
,ifnull(f.DIM_BILLINGDATEID,1)
,ifnull(f.DIM_CUSTOMERPAYMENTTERMSID,1)
,ifnull(f.DIM_SALESRISKCATEGORYID,1)
,ifnull(f.DD_CREDITREP,'Not Set')
,ifnull(f.DD_CREDITLIMIT,0)
,ifnull(f.DIM_CUSTOMERRISKCATEGORYID,1)
,ifnull(f.CT_FILLQTY_CRD,0)
,ifnull(f.CT_FILLQTY_PDD,0)
,ifnull(f.DIM_H1CUSTOMERID,1)
,ifnull(f.DIM_DATEIDSOITEMCHANGEDON,1)
,ifnull(f.DIM_SHIPPINGCONDITIONID,1)
,ifnull(f.DD_AFSALLOCATIONGROUPNO,'Not Set')
,ifnull(f.DIM_DATEIDREJECTION,1)
,ifnull(f.DD_CUSTOMERMATERIALNO,'Not Set')
,ifnull(f.DIM_BASEUOMID,1)
,ifnull(f.DIM_SALESUOMID,1)
,ifnull(f.AMT_UNITPRICEUOM,0)
,ifnull(f.DIM_DATEIDFIXEDVALUE,1)
,ifnull(f.DIM_PAYMENTTERMSID,1)
,ifnull(f.DIM_DATEIDNETDUEDATE,1)
,ifnull(f.DIM_MATERIALPRICEGROUP4ID,1)
,ifnull(f.DIM_MATERIALPRICEGROUP5ID,1)
,ifnull(f.AMT_SUBTOTAL3_ORDERQTY,0)
,ifnull(f.AMT_SUBTOTAL3INCUSTCONFIG_BILLING,0)
,ifnull(f.DIM_CUSTOMERMASTERSALESID,1)
,ifnull(f.DD_SALESORDERBLOCKED,'Not Set')
,ifnull(f.DD_SOCREATETIME,'Not Set')
,ifnull(f.DD_REQDELIVERYTIME,'Not Set')
,ifnull(f.DD_SOLINECREATETIME,'Not Set')
,ifnull(f.DD_DELIVERYTIME,'Not Set')
,ifnull(f.DD_PLANNEDGITIME,'Not Set')
,ifnull(f.DIM_DATEIDARPOSTING,1)
,ifnull(f.DIM_CURRENCYID_TRA,1)
,ifnull(f.DIM_CURRENCYID_GBL,1)
,ifnull(f.DIM_CURRENCYID_STAT,1)
,ifnull(f.AMT_EXCHANGERATE_STAT,1)
,ifnull(f.DD_ARUNNUMBER,'Not Set')
,ifnull(f.DIM_AVAILABILITYCHECKID,1)
,ifnull(f.DD_IDOCNUMBER,'Not Set')
,ifnull(f.DIM_INCOTERMID,1)
,ifnull(f.DD_INTERNATIONALARTICLENO,'Not Set')
,ifnull(f.DD_RELEASERULE,'Not Set')
,ifnull(f.DIM_DELIVERYPRIORITYID,1)
,ifnull(f.DD_REFERENCEDOCITEMNO,0)
,ifnull(f.DIM_SALESORDERPARTNERID,1)
,ifnull(f.DD_PURCHASEREQUISITION,'Not Set')
,ifnull(f.DD_ITEMOFREQUISITION,0)
,ifnull(f.CT_AFSCALCULATEDOPENQTY,0)
,ifnull(f.AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE,0)
,ifnull(f.DD_DOCUMENTCONDITIONNO,'Not Set')
,ifnull(f.AMT_SUBTOTAL1,0)
,ifnull(f.AMT_SUBTOTAL2,0)
,ifnull(f.AMT_SUBTOTAL5,0)
,ifnull(f.AMT_SUBTOTAL6,0)
,ifnull(f.DD_HIGHLEVELITEM,0)
,ifnull(f.DD_PODOCUMENTNO,'Not Set')
,ifnull(f.DD_PODOCUMENTITEMNO,0)
,ifnull(f.DD_POSCHEDULENO,0)
,ifnull(f.DD_PRODORDERNO,'Not Set')
,ifnull(f.DD_PRODORDERITEMNO,0)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS1ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS2ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS3ID,1)
,ifnull(f.CT_AFSOPENPOQTY,0)
,ifnull(f.CT_AFSINTRANSITQTY,0)
,ifnull(f.DIM_SCHEDULEDELIVERYBLOCKID,1)
,ifnull(f.DIM_CUSTOMERGROUP4ID,1)
,ifnull(f.DD_OPENCONFIRMATIONSEXISTS,'Not Set')
,ifnull(f.DD_CONDITIONNO,'Not Set')
,ifnull(f.DD_CREDITMGR,'Not Set')
,ifnull(f.DD_CLEAREDBLOCKEDSTS,'Not Set')
,ifnull(f.CT_AFSINRDDUNITS,0)
,ifnull(f.CT_AFSAFTERRDDUNITS,0)
,ifnull(f.DIM_AGREEMENTSID,1)
,ifnull(f.DIM_CHARGEBACKCONTRACTID,1)
,ifnull(f.DD_RO_MATERIALDESC,'Not Set')
,ifnull(f.DIM_CUSTOMERIDCBOWNER,1)
,ifnull(f.CT_COMMITTEDQTY,0)
,ifnull(f.DIM_DATEIDREQDELIVLINE,1)
,ifnull(f.DIM_MATERIALPRICINGGROUPID,1)
,ifnull(f.DIM_BILLINGBLOCKSALESDOCLVLID,1)
,ifnull(f.DD_DELIVERYINDICATOR,'Not Set')
,ifnull(f.DD_SHIPTOPARTYSTREET,'Not Set')
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS4ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS5ID,1)
,ifnull(f.DD_PURCHASEORDERITEM,'0')
,ifnull(f.DIM_MATERIALPRICEGROUP2ID,1)
,ifnull(f.DIM_PROJECTSOURCEID,1)
,ifnull(f.DD_BILLING_NO,'Not Set')
,ifnull(f.DIM_MDG_PARTID,1)
,ifnull(f.DIM_BWPRODUCTHIERARCHYID,1)
,ifnull(f.CT_FIRSTNONZEROCONFIRMQTY,0)
,ifnull(f.CT_VOLUME,0)
,ifnull(f.CT_NOTCONFIRMEDQTY,0)
,ifnull(f.DD_CUSTOMCUSTEXPPRICEINC,'Not Set')
,ifnull(f.DD_DOCUMENTFLOWGROUP,'Not Set')
,ifnull(f.DD_ORDERBY,'Not Set')
,ifnull(f.DD_CUSTOMPRICEMISSINGINC,'Not Set')
,ifnull(f.DIM_CUSTOMERENDUSERFORFTRADE,1)
,ifnull(f.DD_PACKHOLDFLAG,'Not Set')
,ifnull(f.DD_PURCHASEREQ,'Not Set')
,ifnull(f.AMT_NETVALUE,'0')
,ifnull(f.DIM_DATEIDREQUESTED_EMD,1)
,ifnull(f.DIM_DATEIDEXPECTEDSHIP_EMD,1)
,ifnull(f.DIM_DATEIDCONFIRMEDDELIVERY_EMD,1)
,ifnull(f.DIM_ACCORDINGGIDATEMTO_EMD,1)
,ifnull(f.CT_BASEUOMRATIO,0)
,ifnull(f.DD_MERCKLSBACKORDERFILTER,'Not Set')
,ifnull(f.DD_OPENQTYCVRDBYMAT,'Not Set')
,ifnull(f.AMT_EXCHANGERATE_CUSTOM,1)
,ifnull(f.DD_INTERCOMPFLAG,'Not Set')
,ifnull(f.DD_LSINTERCOMPFLAG,'Not Set')
,ifnull(f.DIM_COMMERCIALVIEWID,1)
,ifnull(f.CT_BASEUOMRATIOKG,0)
,ifnull(f.DIM_CLUSTERID,1)
,ifnull(f.DD_OPENQTYCVRDBYPLANT,'Not Set')
,ifnull((select dim_dateid from dim_date where companycode = 'Not Set' and datevalue = (select processing_date from tmp_dayofprocessing_var_snp)),1) DIM_DATEIDSNAPSHOT
,ifnull(f.CT_COUNTSALESDOCITEM,0)
,ifnull(f.dim_countryhierpsid,1)
,ifnull(f.dim_countryhierarid,1)
,ifnull(f.dim_gsamapimportid,1)
,ifnull(f.dim_customer_shipto,1)
,ifnull(f.dim_productionschedulerid,1)
,ifnull(f.dd_inco1,'Not Set')
,ifnull(f.dd_inco2,'Not Set')
,ifnull(f.dd_ship_to_party,'Not Set')
,ifnull(prt.mtomts,'Not Set')
,IFNULL(F.DIM_ADDRESSID,1)
,ifnull(f.dd_CompleteInSingleDelivery,'Not Set')
,ifnull(f.dd_batch_split,'Not Set')
,ifnull(f.dd_delivery_group,'Not Set')
,DIM_BLOCKEDBACKORDERBYPLANTID
,amt_openorderpm
,ct_openorder
,amt_order
,amt_openorder
from fact_salesorder f
	inner join dim_date_factory_calendar mts on f.dim_dateidexpectedship_emd = mts.dim_dateid
	inner join dim_date_factory_calendar mto on f.dim_accordinggidatemto_emd = mto.dim_dateid
	inner join dim_date rdemd on f.dim_dateidrequested_emd = rdemd.dim_dateid
	inner join dim_date cddemd on f.dim_dateidconfirmeddelivery_emd = cddemd.dim_dateid
	inner join dim_part prt on f.dim_partid = prt.dim_partid
	inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
	inner join dim_salesorderitemstatus its on f.dim_salesorderitemstatusid = its.dim_salesorderitemstatusid
	inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
where
CASE
    WHEN UPPER(uph.businessdesc) LIKE '%RESEARCH SOLUTION%'
	    	THEN
						 CASE
						      WHEN DECODE( mts.DateValue ,'0001-01-01', rdemd.DateValue , mts.DateValue ) < current_date
							     AND hs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
							     AND its.OverallProcessingStatus not in ('Completely processed' , 'Tratado completamente')
						      THEN 'Yes'
							  ELSE 'No'
						 END
		ELSE
				case
  when DECODE( mts.DateValue ,'0001-01-01', rdemd.DateValue , mts.DateValue ) < current_date
		 and prt.mtomts = 'MTS'
		 and hs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
		 and its.OverallProcessingStatus   not in ('Completely processed' , 'Tratado completamente')
		 and DECODE(mts.DateValue||rdemd.DateValue,'0001-01-010001-01-01','No','Yes') = 'Yes'
	  then 'Yes'
  when DECODE( mto.DateValue , '0001-01-01', cddemd.DateValue , mto.DateValue ) < current_date
		 and prt.mtomts = 'MTO'
		 and hs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
		 and its.OverallProcessingStatus   not in ('Completely processed' , 'Tratado completamente')
		 and DECODE(mto.DateValue||cddemd.DateValue,'0001-01-010001-01-01','No','Yes') = 'Yes'
      then  'Yes'
else 'No'
end
END = 'Yes'
and f.dd_mercklsbackorderfilter = 'X'
and uph.businesssector = 'BS-02';

/*Marius 2 SEP 2016 Calculate Backorder Variance */

DROP TABLE IF EXISTS TMP_MAX_SNP;
CREATE TABLE TMP_MAX_SNP
AS
SELECT max(snp.datevalue) max_snp
FROM fact_salesorder_snapshot f_sosnp
	INNER JOIN dim_date snp ON f_sosnp.dim_dateidsnapshot = snp.dim_dateid
UNION
SELECT max(snp.datevalue)-1 max_snp
FROM fact_salesorder_snapshot f_sosnp
	INNER JOIN dim_date snp ON f_sosnp.dim_dateidsnapshot = snp.dim_dateid
UNION
SELECT max(snp.datevalue)-7 max_snp
FROM fact_salesorder_snapshot f_sosnp
	INNER JOIN dim_date snp ON f_sosnp.dim_dateidsnapshot = snp.dim_dateid
UNION
SELECT add_months(max(snp.datevalue),-1) max_snp
FROM fact_salesorder_snapshot f_sosnp
	INNER JOIN dim_date snp ON f_sosnp.dim_dateidsnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_BACKORDER_VARIANCE;
CREATE TABLE TMP_BACKORDER_VARIANCE
AS
select snp.datevalue
	,uph.businessdesc
	,SUM(cast((case when f_sosnp.Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_sosnp.dd_ItemRelForDelv = 'X' then (case when (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived)) < 0 then 0.0000 else (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived)) end * f_sosnp.amt_UnitPriceUoM/(CASE WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit ELSE 1 END)) else (case when (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty) < 0 then 0.0000 else (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty) end * f_sosnp.amt_UnitPriceUoM/(CASE WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit ELSE 1 END)) end * amt_ExchangeRate_GBL) as decimal(18,4))) OpenAmt
from fact_salesorder_snapshot f_sosnp
	inner join dim_date snp on f_sosnp.dim_dateidsnapshot = snp.dim_dateid
	inner join dim_bwproducthierarchy uph on f_sosnp.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
	inner join dim_salesdocumenttype sdt on f_sosnp.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
WHERE snp.datevalue IN (SELECT max_snp FROM TMP_MAX_SNP)
group by snp.datevalue,uph.businessdesc;

DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT snp.datevalue
	,uph.businessdesc
	,MAX(fact_salesorder_snapshotid) fact_salesorder_snapshotid
FROM fact_salesorder_snapshot f_sosnp
	inner join dim_date snp on f_sosnp.dim_dateidsnapshot = snp.dim_dateid
	inner join dim_bwproducthierarchy uph on f_sosnp.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
WHERE snp.datevalue IN (SELECT max(max_snp) FROM TMP_MAX_SNP)
GROUP BY snp.datevalue
	,uph.businessdesc;

MERGE INTO fact_salesorder_snapshot f_sosnp
USING (SELECT f.fact_salesorder_snapshotid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt
	FROM fact_salesorder_snapshot f
		INNER JOIN dim_date snp ON f.dim_dateidsnapshot = snp.dim_dateid
		INNER JOIN dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
		INNER JOIN TMP_MAX_ID t ON f.fact_salesorder_snapshotid = t.fact_salesorder_snapshotid
		LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.datevalue = snp.datevalue AND a.businessdesc = uph.businessdesc
		LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.datevalue = snp.datevalue - 1 AND b.businessdesc = uph.businessdesc
	WHERE f.amt_openqtyvalue1daychange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x
ON f_sosnp.fact_salesorder_snapshotid = x.fact_salesorder_snapshotid
WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1daychange = x.OpnAmt;

MERGE INTO fact_salesorder_snapshot f_sosnp
USING (SELECT f.fact_salesorder_snapshotid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt
	FROM fact_salesorder_snapshot f
		INNER JOIN dim_date snp ON f.dim_dateidsnapshot = snp.dim_dateid
		INNER JOIN dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
		INNER JOIN TMP_MAX_ID t ON f.fact_salesorder_snapshotid = t.fact_salesorder_snapshotid
		LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.datevalue = snp.datevalue AND a.businessdesc = uph.businessdesc
		LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.datevalue = snp.datevalue - 7 AND b.businessdesc = uph.businessdesc
	WHERE f.amt_openqtyvalue1weekchange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x
ON f_sosnp.fact_salesorder_snapshotid = x.fact_salesorder_snapshotid
WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1weekchange = x.OpnAmt;

MERGE INTO fact_salesorder_snapshot f_sosnp
USING (SELECT f.fact_salesorder_snapshotid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt
	FROM fact_salesorder_snapshot f
		INNER JOIN dim_date snp ON f.dim_dateidsnapshot = snp.dim_dateid
		INNER JOIN dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
		INNER JOIN TMP_MAX_ID t ON f.fact_salesorder_snapshotid = t.fact_salesorder_snapshotid
		LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.datevalue = snp.datevalue AND a.businessdesc = uph.businessdesc
		LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.datevalue = add_months(snp.datevalue,-1) AND b.businessdesc = uph.businessdesc
	WHERE f.amt_openqtyvalue1monthchange <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x
ON f_sosnp.fact_salesorder_snapshotid = x.fact_salesorder_snapshotid
WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1monthchange = x.OpnAmt;

DROP TABLE IF EXISTS TMP_MAX_SNP;
DROP TABLE IF EXISTS TMP_BACKORDER_VARIANCE;
DROP TABLE IF EXISTS TMP_MAX_ID;


DROP TABLE IF EXISTS TMP_MAX_SNP;
CREATE TABLE TMP_MAX_SNP
AS
SELECT max(snp.datevalue) max_snp
FROM fact_salesorder_snapshot f_sosnp
	INNER JOIN dim_date snp ON f_sosnp.dim_dateidsnapshot = snp.dim_dateid
UNION
SELECT max(snp.datevalue)-1 max_snp
FROM fact_salesorder_snapshot f_sosnp
	INNER JOIN dim_date snp ON f_sosnp.dim_dateidsnapshot = snp.dim_dateid
UNION
SELECT max(snp.datevalue)-7 max_snp
FROM fact_salesorder_snapshot f_sosnp
	INNER JOIN dim_date snp ON f_sosnp.dim_dateidsnapshot = snp.dim_dateid
UNION
SELECT add_months(max(snp.datevalue),-1) max_snp
FROM fact_salesorder_snapshot f_sosnp
	INNER JOIN dim_date snp ON f_sosnp.dim_dateidsnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_BACKORDER_VARIANCE;
CREATE TABLE TMP_BACKORDER_VARIANCE
AS
select snp.datevalue
	,prt.partnumber
  ,f_sosnp.DD_INTERCOMPFLAG
	,SUM(cast((case when f_sosnp.Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_sosnp.dd_ItemRelForDelv = 'X' then (case when (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived)) < 0 then 0.0000 else (f_sosnp.ct_ScheduleQtySalesUnit - (f_sosnp.ct_ShippedAgnstOrderQty - f_sosnp.ct_CmlQtyReceived)) end * f_sosnp.amt_UnitPriceUoM/(CASE WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit ELSE 1 END)) else (case when (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty) < 0 then 0.0000 else (f_sosnp.ct_ScheduleQtySalesUnit - f_sosnp.ct_ShippedAgnstOrderQty) end * f_sosnp.amt_UnitPriceUoM/(CASE WHEN f_sosnp.ct_PriceUnit <> 0 THEN f_sosnp.ct_PriceUnit ELSE 1 END)) end * amt_ExchangeRate_GBL) as decimal(18,4))) OpenAmt
from fact_salesorder_snapshot f_sosnp
	inner join dim_part prt on f_sosnp.dim_partid = prt.dim_partid
	inner join dim_date snp on f_sosnp.dim_dateidsnapshot = snp.dim_dateid
	inner join dim_salesdocumenttype sdt on f_sosnp.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
WHERE snp.datevalue IN (SELECT max_snp FROM TMP_MAX_SNP)
group by snp.datevalue,prt.partnumber,f_sosnp.DD_INTERCOMPFLAG;

DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT snp.datevalue
	,prt.partnumber
  ,f_sosnp.DD_INTERCOMPFLAG
	,MAX(fact_salesorder_snapshotid) fact_salesorder_snapshotid
FROM fact_salesorder_snapshot f_sosnp
	inner join dim_part prt on f_sosnp.dim_partid = prt.dim_partid
	inner join dim_date snp on f_sosnp.dim_dateidsnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_snp) FROM TMP_MAX_SNP)
GROUP BY snp.datevalue
	,prt.partnumber
  ,f_sosnp.DD_INTERCOMPFLAG;

MERGE INTO fact_salesorder_snapshot f_sosnp
USING (SELECT f.fact_salesorder_snapshotid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt
	FROM fact_salesorder_snapshot f
		INNER JOIN dim_date snp ON f.dim_dateidsnapshot = snp.dim_dateid
		INNER JOIN dim_part prt on f.dim_partid = prt.dim_partid
		INNER JOIN TMP_MAX_ID t ON f.fact_salesorder_snapshotid = t.fact_salesorder_snapshotid
		LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.datevalue = snp.datevalue AND a.partnumber = prt.partnumber and a.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG
		LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.datevalue = snp.datevalue - 1 AND b.partnumber = prt.partnumber and b.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG
	WHERE f.amt_openqtyvalue1daychangepart <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x
ON f_sosnp.fact_salesorder_snapshotid = x.fact_salesorder_snapshotid
WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1daychangepart = x.OpnAmt;

MERGE INTO fact_salesorder_snapshot f_sosnp
USING (SELECT f.fact_salesorder_snapshotid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt
	FROM fact_salesorder_snapshot f
		INNER JOIN dim_date snp ON f.dim_dateidsnapshot = snp.dim_dateid
		INNER JOIN dim_part prt on f.dim_partid = prt.dim_partid
		INNER JOIN TMP_MAX_ID t ON f.fact_salesorder_snapshotid = t.fact_salesorder_snapshotid
		LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.datevalue = snp.datevalue AND a.partnumber = prt.partnumber and a.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG
		LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.datevalue = snp.datevalue - 7 AND b.partnumber = prt.partnumber and b.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG
	WHERE f.amt_openqtyvalue1weekchangepart <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x
ON f_sosnp.fact_salesorder_snapshotid = x.fact_salesorder_snapshotid
WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1weekchangepart = x.OpnAmt;

MERGE INTO fact_salesorder_snapshot f_sosnp
USING (SELECT f.fact_salesorder_snapshotid, ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0) OpnAmt
	FROM fact_salesorder_snapshot f
		INNER JOIN dim_date snp ON f.dim_dateidsnapshot = snp.dim_dateid
		INNER JOIN dim_part prt on f.dim_partid = prt.dim_partid
		INNER JOIN TMP_MAX_ID t ON f.fact_salesorder_snapshotid = t.fact_salesorder_snapshotid
		LEFT JOIN TMP_BACKORDER_VARIANCE a ON a.datevalue = snp.datevalue AND a.partnumber = prt.partnumber and a.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG
		LEFT JOIN TMP_BACKORDER_VARIANCE b ON b.datevalue = add_months(snp.datevalue,-1) AND b.partnumber = prt.partnumber and b.DD_INTERCOMPFLAG = f.DD_INTERCOMPFLAG
	WHERE f.amt_openqtyvalue1monthchangepart <> ifnull(a.OpenAmt,0) - ifnull(b.OpenAmt,0)) x
ON f_sosnp.fact_salesorder_snapshotid = x.fact_salesorder_snapshotid
WHEN MATCHED THEN UPDATE SET f_sosnp.amt_openqtyvalue1monthchangepart = x.OpnAmt;


/*BI-5021 incorporate CCT logic with commercial view*/

drop table if exists tmp_upd_sales;
create table tmp_upd_sales as
select  distinct f.fact_salesorder_snapshotid,f.dim_customerid non_cct
,ff.dim_customerid cct_customerid
 from fact_salesorder_snapshot f
 inner join dim_customer c on f.dim_customerid = c.dim_customerid
                                                and  c.customernumber in
('0000169177'
,'0000172329'
,'0000183611'
,'0000197474'
,'0000241105'
,'0000241786'
,'0000241787'
,'0000242176'
,'0000242177'
,'0000243046'
,'0000243047'
,'0000243048')
 inner join emdtempocc4.fact_salesorder ff on right(f.dd_businesscustomerpono,instr(f.dd_businesscustomerpono,'/',1)-1 ) = ff.dd_salesdocno
 where right(f.dd_businesscustomerpono,instr(f.dd_businesscustomerpono,'/',1)-1 ) is not null;

update fact_salesorder_snapshot f
set f.dim_customerid = t.cct_customerid
from fact_salesorder_snapshot f
,tmp_upd_sales t, dim_bwproducthierarchy ph
where t.fact_salesorder_snapshotid =f.fact_salesorder_snapshotid
and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
and t.non_cct = f.dim_customerid
and f.dim_customerid <> t.cct_customerid;

update fact_salesorder_snapshot f
	set f.dim_commercialviewid=ds.dim_commercialviewid
	from dim_commercialview ds, emdtempocc4.dim_customer a, dim_bwproducthierarchy b, fact_salesorder_snapshot f
	where ds.SOLDTOCOUNTRY=a.COUNTRY
	and ds.UPPERPRODHIER=b.BUSINESS
	and f.DIM_BWPRODUCTHIERARCHYID=b.DIM_BWPRODUCTHIERARCHYID
	and f.dim_customerid=a.dim_customerid
	and f.dim_commercialviewid <> ds.dim_commercialviewid;

drop table if exists tmp_upd_sales;
DROP TABLE IF EXISTS TMP_BACKORDER_VARIANCE;
DROP TABLE IF EXISTS TMP_MAX_ID;
DROP TABLE IF EXISTS TMP_MAX_SNP;

/* Marius 13 jan add backorder aging categ */

update fact_salesorder_snapshot f
set dd_mercklsbocateg =
	CASE
	WHEN (CASE
			WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' AND snpsd.datevalue >= '2016-12-05'
				THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
			ELSE
				CASE
					WHEN trim(prt.mtomts) = 'MTO'
						THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(acgidmto.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),acgidmto.DateValue) - 1 - (CASE WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isapublicholiday,acgidmto.isapublicholiday) = 1 THEN 1 WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isaweekendday,acgidmto.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
					ELSE DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
				END
		END) BETWEEN 1 AND 7 THEN '01 to 07 days'
	WHEN (CASE
			WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' AND snpsd.datevalue >= '2016-12-05'
				THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
			ELSE
				CASE
					WHEN trim(prt.mtomts) = 'MTO'
						THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(acgidmto.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),acgidmto.DateValue) - 1 - (CASE WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isapublicholiday,acgidmto.isapublicholiday) = 1 THEN 1 WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isaweekendday,acgidmto.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
					ELSE DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
				END
		END) BETWEEN 8 AND 14 THEN '08 to 14 days'
	WHEN (CASE
			WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' AND snpsd.datevalue >= '2016-12-05'
				THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
			ELSE
				CASE
					WHEN trim(prt.mtomts) = 'MTO'
						THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(acgidmto.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),acgidmto.DateValue) - 1 - (CASE WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isapublicholiday,acgidmto.isapublicholiday) = 1 THEN 1 WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isaweekendday,acgidmto.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
					ELSE DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
				END
		END) BETWEEN 15 AND 21 THEN '15 to 21 days'
	WHEN (CASE
			WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' AND snpsd.datevalue >= '2016-12-05'
				THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
			ELSE
				CASE
					WHEN trim(prt.mtomts) = 'MTO'
						THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(acgidmto.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),acgidmto.DateValue) - 1 - (CASE WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isapublicholiday,acgidmto.isapublicholiday) = 1 THEN 1 WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isaweekendday,acgidmto.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
					ELSE DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
				END
		END) BETWEEN 22 AND 28 THEN '22 to 28 days'
	WHEN (CASE
			WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' AND snpsd.datevalue >= '2016-12-05'
				THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
			ELSE
				CASE
					WHEN trim(prt.mtomts) = 'MTO'
						THEN DAYS_BETWEEN(snpsd.datevalue, DECODE(acgidmto.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),acgidmto.DateValue) - 1 - (CASE WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isapublicholiday,acgidmto.isapublicholiday) = 1 THEN 1 WHEN DECODE(acgidmto.DateValue,'0001-01-01',rdemd.isaweekendday,acgidmto.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
					ELSE DAYS_BETWEEN(snpsd.datevalue, DECODE(esdemd.DateValue,'0001-01-01',DECODE(rdemd.DateValue,'0001-01-01',NULL,rdemd.DateValue),esdemd.DateValue) - 1 - (CASE WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isapublicholiday,esdemd.isapublicholiday) = 1 THEN 1 WHEN DECODE(esdemd.DateValue,'0001-01-01',rdemd.isaweekendday,esdemd.isaweekendday) = 1 THEN 1 ELSE 0 END ) )
				END
		END) > 28 THEN '28+ days'
	ELSE 'Not Set'
	END
from fact_salesorder_snapshot f
	inner join dim_bwproducthierarchy bw on f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
	inner join dim_part prt on f.dim_partid  = prt.dim_partid
	inner join dim_date snpsd on f.dim_dateidsnapshot = snpsd.dim_dateid
	inner join dim_date_factory_calendar esdemd on f.dim_dateidexpectedship_emd = esdemd.dim_dateid
	inner join dim_date rdemd on f.dim_dateidrequested_emd = rdemd.dim_dateid
	inner join dim_date_factory_calendar acgidmto on f.dim_accordinggidatemto_emd = acgidmto.dim_dateid
where f.DIM_DATEIDSNAPSHOT = ifnull((select x.dim_dateid from dim_date x where x.companycode = 'Not Set' and x.datevalue = (select t.processing_date from tmp_dayofprocessing_var_snp t)),1);

/* Roxana - 23rd of January - Add PM data in Snapshot SA */

delete from number_fountain m where m.table_name = 'fact_salesorder_snapshot';

insert into number_fountain
select 	'fact_salesorder_snapshot',
	ifnull(max(d.fact_salesorder_snapshotid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_salesorder_snapshot d;

insert into fact_salesorder_snapshot (
FACT_SALESORDER_SNAPSHOTID
,DD_SALESDOCNO
,DD_SALESITEMNO
,DD_SCHEDULENO
,CT_SCHEDULEQTYSALESUNIT
,CT_CONFIRMEDQTY
,CT_CORRECTEDQTY
,CT_DELIVEREDQTY
,CT_ONHANDCOVEREDQTY
,AMT_UNITPRICE
,CT_PRICEUNIT
,AMT_SCHEDULETOTAL
,AMT_STDCOST
,AMT_TARGETVALUE
,CT_TARGETQTY
,AMT_EXCHANGERATE
,CT_OVERDLVRTOLERANCE
,CT_UNDERDLVRTOLERANCE
,DIM_DATEIDSALESORDERCREATED
,DIM_DATEIDSCHEDDELIVERYREQ
,DIM_DATEIDSCHEDDLVRREQPREV
,DIM_DATEIDSCHEDDELIVERY
,DIM_DATEIDGOODSISSUE
,DIM_DATEIDMTRLAVAIL
,DIM_DATEIDLOADING
,DIM_DATEIDTRANSPORT
,DIM_CURRENCYID
,DIM_PRODUCTHIERARCHYID
,DIM_PLANTID
,DIM_COMPANYID
,DIM_STORAGELOCATIONID
,DIM_SALESDIVISIONID
,DIM_SHIPRECEIVEPOINTID
,DIM_DOCUMENTCATEGORYID
,DIM_SALESDOCUMENTTYPEID
,DIM_SALESORGID
,DIM_CUSTOMERID
,DIM_DATEIDVALIDFROM
,DIM_DATEIDVALIDTO
,DIM_SALESGROUPID
,DIM_COSTCENTERID
,DIM_CONTROLLINGAREAID
,DIM_BILLINGBLOCKID
,DIM_TRANSACTIONGROUPID
,DIM_SALESORDERREJECTREASONID
,DIM_PARTID
,DIM_PARTSALESID
,DIM_SALESORDERHEADERSTATUSID
,DIM_SALESORDERITEMSTATUSID
,DIM_CUSTOMERGROUP1ID
,DIM_CUSTOMERGROUP2ID
,DIM_SALESORDERITEMCATEGORYID
,AMT_EXCHANGERATE_GBL
,CT_FILLQTY
,CT_ONHANDQTY
,DIM_DATEIDFIRSTDATE
,DIM_SCHEDULELINECATEGORYID
,DIM_SALESMISCID
,DD_ITEMRELFORDELV
,DIM_DATEIDSHIPMENTDELIVERY
,DIM_DATEIDACTUALGI
,DIM_DATEIDSHIPDLVRFILL
,DIM_DATEIDACTUALGIFILL
,DIM_PROFITCENTERID
,DIM_CUSTOMERIDSHIPTO
,DIM_DATEIDGUARANTEEDATE
,DIM_UNITOFMEASUREID
,DIM_DISTRIBUTIONCHANNELID
,DD_BATCHNO
,DD_CREATEDBY
,DIM_CUSTOMPARTNERFUNCTIONID
,DIM_DATEIDLOADINGFILL
,DIM_PAYERPARTNERFUNCTIONID
,DIM_BILLTOPARTYPARTNERFUNCTIONID
,CT_SHIPPEDAGNSTORDERQTY
,CT_CMLQTYRECEIVED
,DIM_CUSTOMPARTNERFUNCTIONID1
,DIM_CUSTOMPARTNERFUNCTIONID2
,DIM_CUSTOMERGROUPID
,DIM_SALESOFFICEID
,DIM_HIGHERCUSTOMERID
,DIM_HIGHERSALESORGID
,DIM_HIGHERDISTCHANNELID
,DIM_HIGHERSALESDIVID
,DD_SOLDTOHIERARCHYLEVEL
,DD_HIERARCHYTYPE
,DIM_TOPLEVELCUSTOMERID
,DIM_TOPLEVELDISTCHANNELID
,DIM_TOPLEVELSALESDIVID
,DIM_TOPLEVELSALESORGID
,CT_AFSUNALLOCATEDQTY
,AMT_AFSUNALLOCATEDVALUE
,CT_AFSALLOCATIONRQTY
,CT_CUMCONFIRMEDQTY
,CT_AFSALLOCATIONFQTY
,CT_CUMORDERQTY
,AMT_AFSONDELIVERYVALUE
,CT_SHIPPEDORBILLEDQTY
,AMT_SHIPPEDORBILLED
,DIM_CREDITREPRESENTATIVEID
,DD_CUSTOMERPONO
,CT_INCOMPLETEQTY
,AMT_INCOMPLETE
,DIM_DELIVERYBLOCKID
,DD_AFSSTOCKCATEGORY
,DD_AFSSTOCKTYPE
,DIM_DATEIDAFSCANCELDATE
,AMT_AFSNETPRICE
,AMT_AFSNETVALUE
,AMT_BASEPRICE
,DIM_MATERIALPRICEGROUP1ID
,AMT_CREDITHOLDVALUE
,AMT_DELIVERYBLOCKVALUE
,DIM_AFSSIZEID
,DIM_AFSREJECTIONREASONID
,DIM_DATEIDDLVRDOCCREATED
,AMT_CUSTOMEREXPECTEDPRICE
,CT_AFSALLOCATEDQTY
,CT_AFSONDELIVERYQTY
,DIM_OVERALLSTATUSCREDITCHECKID
,DIM_SALESDISTRICTID
,DIM_ACCOUNTASSIGNMENTGROUPID
,DIM_MATERIALGROUPID
,DIM_SALESDOCORDERREASONID
,AMT_SUBTOTAL3
,AMT_SUBTOTAL4
,DIM_DATEIDAFSREQDELIVERY
,AMT_DICOUNTACCRUALNETPRICE
,CT_AFSOPENQTY
,DIM_PURCHASEORDERTYPEID
,DIM_DATEIDQUOTATIONVALIDFROM
,DIM_DATEIDPURCHASEORDER
,DIM_DATEIDQUOTATIONVALIDTO
,DIM_AFSSEASONID
,DD_AFSDEPARTMENT
,DIM_DATEIDSOCREATED
,DIM_DATEIDSODOCUMENT
,DD_SUBSEQUENTDOCNO
,DD_SUBSDOCITEMNO
,DD_SUBSSCHEDULENO
,DIM_SUBSDOCCATEGORYID
,CT_AFSTOTALDRAWN
,DIM_DATEIDASLASTDATE
,DD_REFERENCEDOCUMENTNO
,DD_REQUIREMENTTYPE
,AMT_STDPRICE
,DIM_DATEIDNEXTDATE
,AMT_TAX
,DIM_DATEIDEARLIESTSOCANCELDATE
,DIM_DATEIDLATESTCUSTPOAGI
,DD_BUSINESSCUSTOMERPONO
,DIM_ROUTEID
,DIM_BILLINGDATEID
,DIM_CUSTOMERPAYMENTTERMSID
,DIM_SALESRISKCATEGORYID
,DD_CREDITREP
,DD_CREDITLIMIT
,DIM_CUSTOMERRISKCATEGORYID
,CT_FILLQTY_CRD
,CT_FILLQTY_PDD
,DIM_H1CUSTOMERID
,DIM_DATEIDSOITEMCHANGEDON
,DIM_SHIPPINGCONDITIONID
,DD_AFSALLOCATIONGROUPNO
,DIM_DATEIDREJECTION
,DD_CUSTOMERMATERIALNO
,DIM_BASEUOMID
,DIM_SALESUOMID
,AMT_UNITPRICEUOM
,DIM_DATEIDFIXEDVALUE
,DIM_PAYMENTTERMSID
,DIM_DATEIDNETDUEDATE
,DIM_MATERIALPRICEGROUP4ID
,DIM_MATERIALPRICEGROUP5ID
,AMT_SUBTOTAL3_ORDERQTY
,AMT_SUBTOTAL3INCUSTCONFIG_BILLING
,DIM_CUSTOMERMASTERSALESID
,DD_SALESORDERBLOCKED
,DD_SOCREATETIME
,DD_REQDELIVERYTIME
,DD_SOLINECREATETIME
,DD_DELIVERYTIME
,DD_PLANNEDGITIME
,DIM_DATEIDARPOSTING
,DIM_CURRENCYID_TRA
,DIM_CURRENCYID_GBL
,DIM_CURRENCYID_STAT
,AMT_EXCHANGERATE_STAT
,DD_ARUNNUMBER
,DIM_AVAILABILITYCHECKID
,DD_IDOCNUMBER
,DIM_INCOTERMID
,DD_INTERNATIONALARTICLENO
,DD_RELEASERULE
,DIM_DELIVERYPRIORITYID
,DD_REFERENCEDOCITEMNO
,DIM_SALESORDERPARTNERID
,DD_PURCHASEREQUISITION
,DD_ITEMOFREQUISITION
,CT_AFSCALCULATEDOPENQTY
,AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE
,DD_DOCUMENTCONDITIONNO
,AMT_SUBTOTAL1
,AMT_SUBTOTAL2
,AMT_SUBTOTAL5
,AMT_SUBTOTAL6
,DD_HIGHLEVELITEM
,DD_PODOCUMENTNO
,DD_PODOCUMENTITEMNO
,DD_POSCHEDULENO
,DD_PRODORDERNO
,DD_PRODORDERITEMNO
,DIM_CUSTOMERCONDITIONGROUPS1ID
,DIM_CUSTOMERCONDITIONGROUPS2ID
,DIM_CUSTOMERCONDITIONGROUPS3ID
,CT_AFSOPENPOQTY
,CT_AFSINTRANSITQTY
,DIM_SCHEDULEDELIVERYBLOCKID
,DIM_CUSTOMERGROUP4ID
,DD_OPENCONFIRMATIONSEXISTS
,DD_CONDITIONNO
,DD_CREDITMGR
,DD_CLEAREDBLOCKEDSTS
,CT_AFSINRDDUNITS
,CT_AFSAFTERRDDUNITS
,DIM_AGREEMENTSID
,DIM_CHARGEBACKCONTRACTID
,DD_RO_MATERIALDESC
,DIM_CUSTOMERIDCBOWNER
,CT_COMMITTEDQTY
,DIM_DATEIDREQDELIVLINE
,DIM_MATERIALPRICINGGROUPID
,DIM_BILLINGBLOCKSALESDOCLVLID
,DD_DELIVERYINDICATOR
,DD_SHIPTOPARTYSTREET
,DIM_CUSTOMERCONDITIONGROUPS4ID
,DIM_CUSTOMERCONDITIONGROUPS5ID
,DD_PURCHASEORDERITEM
,DIM_MATERIALPRICEGROUP2ID
,DIM_PROJECTSOURCEID
,DD_BILLING_NO
,DIM_MDG_PARTID
,DIM_BWPRODUCTHIERARCHYID
,CT_FIRSTNONZEROCONFIRMQTY
,CT_VOLUME
,CT_NOTCONFIRMEDQTY
,DD_CUSTOMCUSTEXPPRICEINC
,DD_DOCUMENTFLOWGROUP
,DD_ORDERBY
,DD_CUSTOMPRICEMISSINGINC
,DIM_CUSTOMERENDUSERFORFTRADE
,DD_PACKHOLDFLAG
,DD_PURCHASEREQ
,AMT_NETVALUE
,DIM_DATEIDREQUESTED_EMD
,DIM_DATEIDEXPECTEDSHIP_EMD
,DIM_DATEIDCONFIRMEDDELIVERY_EMD
,DIM_ACCORDINGGIDATEMTO_EMD
,CT_BASEUOMRATIO
,DD_MERCKLSBACKORDERFILTER
,DD_OPENQTYCVRDBYMAT
,AMT_EXCHANGERATE_CUSTOM
,DD_INTERCOMPFLAG
,DD_LSINTERCOMPFLAG
,DIM_COMMERCIALVIEWID
,CT_BASEUOMRATIOKG
,DIM_CLUSTERID
,DD_OPENQTYCVRDBYPLANT
,DIM_DATEIDSNAPSHOT
,CT_COUNTSALESDOCITEM
,dim_countryhierpsid
,dim_countryhierarid
,dim_gsamapimportid
,dim_customer_shipto
,dim_productionschedulerid
,dd_inco1
,dd_inco2
,Dim_Addressid
,DD_PMDELIVERYSERVICEFLAG
,dd_durationcategory
,CT_DEVGIDTVSACCGICONFDT
,CT_DELSERAGACCGIDT
,DD_SHIP_TO_PARTY
,dd_mtomts
,DIM_BLOCKEDBACKORDERBYPLANTID
,amt_openorderpm
,ct_openorder
,amt_order
,amt_openorder
)
select
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_salesorder_snapshot')
          + row_number()
             over(order by '') FACT_SALESORDER_SNAPSHOTID
,ifnull(f.DD_SALESDOCNO,'Not Set')
,ifnull(f.DD_SALESITEMNO,0)
,ifnull(f.DD_SCHEDULENO,0)
,ifnull(f.CT_SCHEDULEQTYSALESUNIT,0)
,ifnull(f.CT_CONFIRMEDQTY,0)
,ifnull(f.CT_CORRECTEDQTY,0)
,ifnull(f.CT_DELIVEREDQTY,0)
,ifnull(f.CT_ONHANDCOVEREDQTY,0)
,ifnull(f.AMT_UNITPRICE,0)
,ifnull(f.CT_PRICEUNIT,1)
,ifnull(f.AMT_SCHEDULETOTAL,0)
,ifnull(f.AMT_STDCOST,0)
,ifnull(f.AMT_TARGETVALUE,0)
,ifnull(f.CT_TARGETQTY,0)
,ifnull(f.AMT_EXCHANGERATE,1)
,ifnull(f.CT_OVERDLVRTOLERANCE,0)
,ifnull(f.CT_UNDERDLVRTOLERANCE,0)
,ifnull(f.DIM_DATEIDSALESORDERCREATED,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERYREQ,1)
,ifnull(f.DIM_DATEIDSCHEDDLVRREQPREV,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERY,1)
,ifnull(f.DIM_DATEIDGOODSISSUE,1)
,ifnull(f.DIM_DATEIDMTRLAVAIL,1)
,ifnull(f.DIM_DATEIDLOADING,1)
,ifnull(f.DIM_DATEIDTRANSPORT,1)
,ifnull(f.DIM_CURRENCYID,1)
,ifnull(f.DIM_PRODUCTHIERARCHYID,1)
,ifnull(f.DIM_PLANTID,1)
,ifnull(f.DIM_COMPANYID,1)
,ifnull(f.DIM_STORAGELOCATIONID,1)
,ifnull(f.DIM_SALESDIVISIONID,1)
,ifnull(f.DIM_SHIPRECEIVEPOINTID,1)
,ifnull(f.DIM_DOCUMENTCATEGORYID,1)
,ifnull(f.DIM_SALESDOCUMENTTYPEID,1)
,ifnull(f.DIM_SALESORGID,1)
,ifnull(f.DIM_CUSTOMERID,1)
,ifnull(f.DIM_DATEIDVALIDFROM,1)
,ifnull(f.DIM_DATEIDVALIDTO,1)
,ifnull(f.DIM_SALESGROUPID,0)
,ifnull(f.DIM_COSTCENTERID,1)
,ifnull(f.DIM_CONTROLLINGAREAID,0)
,ifnull(f.DIM_BILLINGBLOCKID,0)
,ifnull(f.DIM_TRANSACTIONGROUPID,0)
,ifnull(f.DIM_SALESORDERREJECTREASONID,0)
,ifnull(f.DIM_PARTID,1)
,ifnull(f.DIM_PARTSALESID,1)
,ifnull(f.DIM_SALESORDERHEADERSTATUSID,0)
,ifnull(f.DIM_SALESORDERITEMSTATUSID,0)
,ifnull(f.DIM_CUSTOMERGROUP1ID,0)
,ifnull(f.DIM_CUSTOMERGROUP2ID,0)
,ifnull(f.DIM_SALESORDERITEMCATEGORYID,1)
,ifnull(f.AMT_EXCHANGERATE_GBL,1)
,ifnull(f.CT_FILLQTY,0)
,ifnull(f.CT_ONHANDQTY,0)
,ifnull(f.DIM_DATEIDFIRSTDATE,1)
,ifnull(f.DIM_SCHEDULELINECATEGORYID,1)
,ifnull(f.DIM_SALESMISCID,1)
,ifnull(f.DD_ITEMRELFORDELV,'Not Set')
,ifnull(f.DIM_DATEIDSHIPMENTDELIVERY,1)
,ifnull(f.DIM_DATEIDACTUALGI,1)
,ifnull(f.DIM_DATEIDSHIPDLVRFILL,1)
,ifnull(f.DIM_DATEIDACTUALGIFILL,1)
,ifnull(f.DIM_PROFITCENTERID,1)
,ifnull(f.DIM_CUSTOMERIDSHIPTO,1)
,ifnull(f.DIM_DATEIDGUARANTEEDATE,1)
,ifnull(f.DIM_UNITOFMEASUREID,1)
,ifnull(f.DIM_DISTRIBUTIONCHANNELID,1)
,ifnull(f.DD_BATCHNO,'Not Set')
,ifnull(f.DD_CREATEDBY,'Not Set')
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID,1)
,ifnull(f.DIM_DATEIDLOADINGFILL,1)
,ifnull(f.DIM_PAYERPARTNERFUNCTIONID,1)
,ifnull(f.DIM_BILLTOPARTYPARTNERFUNCTIONID,1)
,ifnull(f.CT_SHIPPEDAGNSTORDERQTY,0)
,ifnull(f.CT_CMLQTYRECEIVED,0)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID1,1)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID2,1)
,ifnull(f.DIM_CUSTOMERGROUPID,1)
,ifnull(f.DIM_SALESOFFICEID,1)
,ifnull(f.DIM_HIGHERCUSTOMERID,1)
,ifnull(f.DIM_HIGHERSALESORGID,1)
,ifnull(f.DIM_HIGHERDISTCHANNELID,1)
,ifnull(f.DIM_HIGHERSALESDIVID,1)
,ifnull(f.DD_SOLDTOHIERARCHYLEVEL,0)
,ifnull(f.DD_HIERARCHYTYPE,'Not Set')
,ifnull(f.DIM_TOPLEVELCUSTOMERID,1)
,ifnull(f.DIM_TOPLEVELDISTCHANNELID,1)
,ifnull(f.DIM_TOPLEVELSALESDIVID,1)
,ifnull(f.DIM_TOPLEVELSALESORGID,1)
,ifnull(f.CT_AFSUNALLOCATEDQTY,0)
,ifnull(f.AMT_AFSUNALLOCATEDVALUE,0)
,ifnull(f.CT_AFSALLOCATIONRQTY,0)
,ifnull(f.CT_CUMCONFIRMEDQTY,0)
,ifnull(f.CT_AFSALLOCATIONFQTY,0)
,ifnull(f.CT_CUMORDERQTY,0)
,ifnull(f.AMT_AFSONDELIVERYVALUE,0)
,ifnull(f.CT_SHIPPEDORBILLEDQTY,0)
,ifnull(f.AMT_SHIPPEDORBILLED,0)
,ifnull(f.DIM_CREDITREPRESENTATIVEID,1)
,ifnull(f.DD_CUSTOMERPONO,'Not Set')
,ifnull(f.CT_INCOMPLETEQTY,0)
,ifnull(f.AMT_INCOMPLETE,0)
,ifnull(f.DIM_DELIVERYBLOCKID,1)
,ifnull(f.DD_AFSSTOCKCATEGORY,'Not Set')
,ifnull(f.DD_AFSSTOCKTYPE,'Not Set')
,ifnull(f.DIM_DATEIDAFSCANCELDATE,1)
,ifnull(f.AMT_AFSNETPRICE,0)
,ifnull(f.AMT_AFSNETVALUE,0)
,ifnull(f.AMT_BASEPRICE,0)
,ifnull(f.DIM_MATERIALPRICEGROUP1ID,1)
,ifnull(f.AMT_CREDITHOLDVALUE,0)
,ifnull(f.AMT_DELIVERYBLOCKVALUE,0)
,ifnull(f.DIM_AFSSIZEID,1)
,ifnull(f.DIM_AFSREJECTIONREASONID,1)
,ifnull(f.DIM_DATEIDDLVRDOCCREATED,1)
,ifnull(f.AMT_CUSTOMEREXPECTEDPRICE,0)
,ifnull(f.CT_AFSALLOCATEDQTY,0)
,ifnull(f.CT_AFSONDELIVERYQTY,0)
,ifnull(f.DIM_OVERALLSTATUSCREDITCHECKID,1)
,ifnull(f.DIM_SALESDISTRICTID,1)
,ifnull(f.DIM_ACCOUNTASSIGNMENTGROUPID,1)
,ifnull(f.DIM_MATERIALGROUPID,1)
,ifnull(f.DIM_SALESDOCORDERREASONID,1)
,ifnull(f.AMT_SUBTOTAL3,0)
,ifnull(f.AMT_SUBTOTAL4,0)
,ifnull(f.DIM_DATEIDAFSREQDELIVERY,1)
,ifnull(f.AMT_DICOUNTACCRUALNETPRICE,0)
,ifnull(f.CT_AFSOPENQTY,0)
,ifnull(f.DIM_PURCHASEORDERTYPEID,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDFROM,1)
,ifnull(f.DIM_DATEIDPURCHASEORDER,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDTO,1)
,ifnull(f.DIM_AFSSEASONID,1)
,ifnull(f.DD_AFSDEPARTMENT,'Not Set')
,ifnull(f.DIM_DATEIDSOCREATED,1)
,ifnull(f.DIM_DATEIDSODOCUMENT,1)
,ifnull(f.DD_SUBSEQUENTDOCNO,'Not Set')
,ifnull(f.DD_SUBSDOCITEMNO,0)
,ifnull(f.DD_SUBSSCHEDULENO,0)
,ifnull(f.DIM_SUBSDOCCATEGORYID,1)
,ifnull(f.CT_AFSTOTALDRAWN,0)
,ifnull(f.DIM_DATEIDASLASTDATE,1)
,ifnull(f.DD_REFERENCEDOCUMENTNO,'Not Set')
,ifnull(f.DD_REQUIREMENTTYPE,'Not Set')
,ifnull(f.AMT_STDPRICE,0)
,ifnull(f.DIM_DATEIDNEXTDATE,1)
,ifnull(f.AMT_TAX,0)
,ifnull(f.DIM_DATEIDEARLIESTSOCANCELDATE,1)
,ifnull(f.DIM_DATEIDLATESTCUSTPOAGI,1)
,ifnull(f.DD_BUSINESSCUSTOMERPONO,'Not Set')
,ifnull(f.DIM_ROUTEID,1)
,ifnull(f.DIM_BILLINGDATEID,1)
,ifnull(f.DIM_CUSTOMERPAYMENTTERMSID,1)
,ifnull(f.DIM_SALESRISKCATEGORYID,1)
,ifnull(f.DD_CREDITREP,'Not Set')
,ifnull(f.DD_CREDITLIMIT,0)
,ifnull(f.DIM_CUSTOMERRISKCATEGORYID,1)
,ifnull(f.CT_FILLQTY_CRD,0)
,ifnull(f.CT_FILLQTY_PDD,0)
,ifnull(f.DIM_H1CUSTOMERID,1)
,ifnull(f.DIM_DATEIDSOITEMCHANGEDON,1)
,ifnull(f.DIM_SHIPPINGCONDITIONID,1)
,ifnull(f.DD_AFSALLOCATIONGROUPNO,'Not Set')
,ifnull(f.DIM_DATEIDREJECTION,1)
,ifnull(f.DD_CUSTOMERMATERIALNO,'Not Set')
,ifnull(f.DIM_BASEUOMID,1)
,ifnull(f.DIM_SALESUOMID,1)
,ifnull(f.AMT_UNITPRICEUOM,0)
,ifnull(f.DIM_DATEIDFIXEDVALUE,1)
,ifnull(f.DIM_PAYMENTTERMSID,1)
,ifnull(f.DIM_DATEIDNETDUEDATE,1)
,ifnull(f.DIM_MATERIALPRICEGROUP4ID,1)
,ifnull(f.DIM_MATERIALPRICEGROUP5ID,1)
,ifnull(f.AMT_SUBTOTAL3_ORDERQTY,0)
,ifnull(f.AMT_SUBTOTAL3INCUSTCONFIG_BILLING,0)
,ifnull(f.DIM_CUSTOMERMASTERSALESID,1)
,ifnull(f.DD_SALESORDERBLOCKED,'Not Set')
,ifnull(f.DD_SOCREATETIME,'Not Set')
,ifnull(f.DD_REQDELIVERYTIME,'Not Set')
,ifnull(f.DD_SOLINECREATETIME,'Not Set')
,ifnull(f.DD_DELIVERYTIME,'Not Set')
,ifnull(f.DD_PLANNEDGITIME,'Not Set')
,ifnull(f.DIM_DATEIDARPOSTING,1)
,ifnull(f.DIM_CURRENCYID_TRA,1)
,ifnull(f.DIM_CURRENCYID_GBL,1)
,ifnull(f.DIM_CURRENCYID_STAT,1)
,ifnull(f.AMT_EXCHANGERATE_STAT,1)
,ifnull(f.DD_ARUNNUMBER,'Not Set')
,ifnull(f.DIM_AVAILABILITYCHECKID,1)
,ifnull(f.DD_IDOCNUMBER,'Not Set')
,ifnull(f.DIM_INCOTERMID,1)
,ifnull(f.DD_INTERNATIONALARTICLENO,'Not Set')
,ifnull(f.DD_RELEASERULE,'Not Set')
,ifnull(f.DIM_DELIVERYPRIORITYID,1)
,ifnull(f.DD_REFERENCEDOCITEMNO,0)
,ifnull(f.DIM_SALESORDERPARTNERID,1)
,ifnull(f.DD_PURCHASEREQUISITION,'Not Set')
,ifnull(f.DD_ITEMOFREQUISITION,0)
,ifnull(f.CT_AFSCALCULATEDOPENQTY,0)
,ifnull(f.AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE,0)
,ifnull(f.DD_DOCUMENTCONDITIONNO,'Not Set')
,ifnull(f.AMT_SUBTOTAL1,0)
,ifnull(f.AMT_SUBTOTAL2,0)
,ifnull(f.AMT_SUBTOTAL5,0)
,ifnull(f.AMT_SUBTOTAL6,0)
,ifnull(f.DD_HIGHLEVELITEM,0)
,ifnull(f.DD_PODOCUMENTNO,'Not Set')
,ifnull(f.DD_PODOCUMENTITEMNO,0)
,ifnull(f.DD_POSCHEDULENO,0)
,ifnull(f.DD_PRODORDERNO,'Not Set')
,ifnull(f.DD_PRODORDERITEMNO,0)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS1ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS2ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS3ID,1)
,ifnull(f.CT_AFSOPENPOQTY,0)
,ifnull(f.CT_AFSINTRANSITQTY,0)
,ifnull(f.DIM_SCHEDULEDELIVERYBLOCKID,1)
,ifnull(f.DIM_CUSTOMERGROUP4ID,1)
,ifnull(f.DD_OPENCONFIRMATIONSEXISTS,'Not Set')
,ifnull(f.DD_CONDITIONNO,'Not Set')
,ifnull(f.DD_CREDITMGR,'Not Set')
,ifnull(f.DD_CLEAREDBLOCKEDSTS,'Not Set')
,ifnull(f.CT_AFSINRDDUNITS,0)
,ifnull(f.CT_AFSAFTERRDDUNITS,0)
,ifnull(f.DIM_AGREEMENTSID,1)
,ifnull(f.DIM_CHARGEBACKCONTRACTID,1)
,ifnull(f.DD_RO_MATERIALDESC,'Not Set')
,ifnull(f.DIM_CUSTOMERIDCBOWNER,1)
,ifnull(f.CT_COMMITTEDQTY,0)
,ifnull(f.DIM_DATEIDREQDELIVLINE,1)
,ifnull(f.DIM_MATERIALPRICINGGROUPID,1)
,ifnull(f.DIM_BILLINGBLOCKSALESDOCLVLID,1)
,ifnull(f.DD_DELIVERYINDICATOR,'Not Set')
,ifnull(f.DD_SHIPTOPARTYSTREET,'Not Set')
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS4ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS5ID,1)
,ifnull(f.DD_PURCHASEORDERITEM,'0')
,ifnull(f.DIM_MATERIALPRICEGROUP2ID,1)
,ifnull(f.DIM_PROJECTSOURCEID,1)
,ifnull(f.DD_BILLING_NO,'Not Set')
,ifnull(f.DIM_MDG_PARTID,1)
,ifnull(f.DIM_BWPRODUCTHIERARCHYID,1)
,ifnull(f.CT_FIRSTNONZEROCONFIRMQTY,0)
,ifnull(f.CT_VOLUME,0)
,ifnull(f.CT_NOTCONFIRMEDQTY,0)
,ifnull(f.DD_CUSTOMCUSTEXPPRICEINC,'Not Set')
,ifnull(f.DD_DOCUMENTFLOWGROUP,'Not Set')
,ifnull(f.DD_ORDERBY,'Not Set')
,ifnull(f.DD_CUSTOMPRICEMISSINGINC,'Not Set')
,ifnull(f.DIM_CUSTOMERENDUSERFORFTRADE,1)
,ifnull(f.DD_PACKHOLDFLAG,'Not Set')
,ifnull(f.DD_PURCHASEREQ,'Not Set')
,ifnull(f.AMT_NETVALUE,'0')
,ifnull(f.DIM_DATEIDREQUESTED_EMD,1)
,ifnull(f.DIM_DATEIDEXPECTEDSHIP_EMD,1)
,ifnull(f.DIM_DATEIDCONFIRMEDDELIVERY_EMD,1)
,ifnull(f.DIM_ACCORDINGGIDATEMTO_EMD,1)
,ifnull(f.CT_BASEUOMRATIO,0)
,ifnull(f.DD_MERCKLSBACKORDERFILTER,'Not Set')
,ifnull(f.DD_OPENQTYCVRDBYMAT,'Not Set')
,ifnull(f.AMT_EXCHANGERATE_CUSTOM,1)
,ifnull(f.DD_INTERCOMPFLAG,'Not Set')
,ifnull(f.DD_LSINTERCOMPFLAG,'Not Set')
,ifnull(f.DIM_COMMERCIALVIEWID,1)
,ifnull(f.CT_BASEUOMRATIOKG,0)
,ifnull(f.DIM_CLUSTERID,1)
,ifnull(f.DD_OPENQTYCVRDBYPLANT,'Not Set')
,ifnull((select dim_dateid from dim_date where companycode = 'Not Set' and datevalue = (select processing_date from tmp_dayofprocessing_var_snp)),1) DIM_DATEIDSNAPSHOT
,ifnull(f.CT_COUNTSALESDOCITEM,0)
,ifnull(f.dim_countryhierpsid,1)
,ifnull(f.dim_countryhierarid,1)
,ifnull(f.dim_gsamapimportid,1)
,ifnull(f.dim_customer_shipto,1)
,ifnull(f.dim_productionschedulerid,1)
,ifnull(f.dd_inco1,'Not Set')
,ifnull(f.dd_inco2,'Not Set')
,ifnull(f.Dim_Addressid,1)
,ifnull(f.DD_PMDELIVERYSERVICEFLAG,'Not Set')
,ifnull(case
when TO_DATE ( mts.DateValue,'DD MON YYYY') between current_date - interval '7' day and current_date - interval '1' day THEN '01 to 07 days'
when TO_DATE ( mts.DateValue,'DD MON YYYY') between current_date - interval '14' day and current_date - interval '7' day THEN '08 to 14 days'
when TO_DATE ( mts.DateValue,'DD MON YYYY') between current_date - interval '21' day and current_date - interval '14' day THEN '15 to 21 days'
when TO_DATE ( mts.DateValue,'DD MON YYYY') between current_date - interval '28' day and current_date - interval '21' day THEN '22 to 28 days'
when TO_DATE ( mts.DateValue,'DD MON YYYY') < current_date - interval '28' day THEN '28+ days'
ELSE 'Not Set' end,'Not Set')
,ifnull(f.CT_DEVGIDTVSACCGICONFDT,0)
,ifnull(f.CT_DELSERAGACCGIDT,0)
,ifnull(f.DD_SHIP_TO_PARTY,'Not Set')
,ifnull(prt.mtomts,'Not Set')
,DIM_BLOCKEDBACKORDERBYPLANTID
,amt_openorderpm
,ct_openorder
,amt_order
,amt_openorder
from fact_salesorder f
	inner join dim_date_factory_calendar mts on f.dim_dateidexpectedship_emd = mts.dim_dateid
	inner join dim_part prt on f.dim_partid = prt.dim_partid
	inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
where  uph.businesssector = 'BS-03';

/*Disabled 2017-10-27 Roxana D - as requested 
update fact_salesorder_snapshot  f
set f.dim_clusterid = dc.dim_clusterid
from fact_salesorder_snapshot  f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> dc.dim_clusterid

update emd586.fact_salesorder_snapshot f
set f.dim_clusterid = ff.dim_clusterid
from emd586.fact_salesorder_snapshot f,EMDPhoenixDAD.fact_salesorder_snapshot ff
where f.fact_salesorder_snapshotid = ff.fact_salesorder_snapshotid
	and f.dim_projectsourceid=6
	and f.dim_clusterid <> ff.dim_clusterid
	
	*/
	
/*Add dd_primaryproductionlocation Roxana D 2017-11-01*/

update FACT_SALESORDER_Snapshot f
set f.DD_PRIMARYPRODUCTIONLOCATION = mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
FROM  FACT_SALESORDER_Snapshot f
inner join  dim_cluster dc on dc.dim_clusterid = f.dim_clusterid
inner join dim_bwproducthierarchy ph on f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
   and businesssector = 'BS-02'
inner join (select distinct primary_production_location,primary_production_location_name  
from dim_mdg_part ) mdg on mdg.primary_production_location = dc.primary_manufacturing_site
where f.DD_PRIMARYPRODUCTIONLOCATION <> mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
and dim_dateidsnapshot = (select dim_dateid from dim_date where companycode = 'Not Set' and datevalue = (select processing_date from tmp_dayofprocessing_var_snp));


/*End*/
update fact_salesorder_snapshot f_so
set dim_mercklsconforreqdateid = mlsrd.dim_dateid
from fact_salesorder_snapshot f_so,dim_date_factory_calendar mlsrd, dim_part prt, dim_bwproducthierarchy bw
where f_so.Dim_Partid = prt.Dim_Partid
and f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
and
CASE WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN f_so.dim_dateidexpectedship_emd ELSE
  CASE WHEN trim(prt.mtomts) = 'MTO' THEN f_so.dim_accordinggidatemto_emd
      ELSE f_so.dim_dateidexpectedship_emd END END=mlsrd.dim_dateid
and dim_mercklsconforreqdateid <> mlsrd.dim_dateid;



update fact_salesorder_snapshot f
set f.dd_CustomerPONo=ff.dd_CustomerPONo
from fact_salesorder_snapshot f,fact_salesorder ff
where f.dd_salesdocno=ff.dd_salesdocno and f.dd_salesitemno=ff.dd_salesitemno
and f.dd_scheduleno=ff.dd_scheduleno
and f.dd_CustomerPONo='Not Set'
and f.dd_CustomerPONo<>ff.dd_CustomerPONo;

update emd586.fact_salesorder_snapshot f
set  f.dd_CustomerPONo=ff.dd_CustomerPONo
from emd586.fact_salesorder_snapshot f,fact_salesorder_snapshot ff
where f.fact_salesorder_snapshotid=ff.fact_salesorder_snapshotid
and f.dd_salesdocno=ff.dd_salesdocno and to_char(f.dd_salesitemno)=to_char(ff.dd_salesitemno)
and f.dd_scheduleno=ff.dd_scheduleno and f.dim_dateidsnapshot=ff.dim_dateidsnapshot
and f.dd_CustomerPONo='Not Set'
and f.dd_CustomerPONo<>ff.dd_CustomerPONo;


/* Roxana H - 16 Oct 2017 - Add Order Bucket Logic in Backend */

update fact_salesorder_snapshot f
set dd_orderbucket = ifnull(case 
when TO_DATE (esdemd.DateValue , 'DD MON YYYY') = '0001-01-01' and
(sois.OverallProcessingStatus = 'Completely processed' or
sois.OverallProcessingStatus = 'Tratado completamente') then 'Closed Order'
when TO_DATE ( esdemd.DateValue ,'DD MON YYYY') = '0001-01-01' then 'No Requested Date'
when sois.OverallProcessingStatus <> 'Completely processed'
and sohs.OverallProcessStatusItem <> 'Completely processed'
and sorr.Description = 'Not Set'
and TO_DATE( esdemd.DateValue ,'DD MON YYYY') > TO_DATE( t.processing_date ,'DD MON YYYY') +29
then 'Future Order'
when sois.OverallProcessingStatus <> 'Completely processed'
and sohs.OverallProcessStatusItem <> 'Completely processed'
and sorr.Description = 'Not Set'
and TO_DATE( esdemd.DateValue,'DD MON YYYY') <= TO_DATE( t.processing_date ,'DD MON YYYY') +29
and TO_DATE( esdemd.DateValue,'DD MON YYYY') > TO_DATE( t.processing_date ,'DD MON YYYY')
then 'Open Order'
when sois.OverallProcessingStatus <> 'Completely processed' 
and sohs.OverallProcessStatusItem <> 'Completely processed'
and sorr.Description = 'Not Set'
and TO_DATE( esdemd.DateValue,'DD MON YYYY') <= TO_DATE( t.processing_date ,'DD MON YYYY')
then 'Backorder'
when sois.OverallProcessingStatus = 'Completely processed' 
or sohs.OverallProcessStatusItem = 'Tratado completamente'
or sois.OverallDeliveryStatus = 'Completely processed'
or sois.OverallDeliveryStatus = 'Tratado completamente'	
then 'Closed Order'
else 'Open Rejected'
end,'Not Set')
from fact_salesorder_snapshot f, dim_salesorderrejectreason sorr, dim_salesorderheaderstatus sohs, dim_salesorderitemstatus sois, dim_date_factory_calendar esdemd, tmp_dayofprocessing_var_snp t
where f.dim_salesorderrejectreasonid = sorr.dim_salesorderrejectreasonid
and f.dim_salesorderheaderstatusid = sohs.dim_salesorderheaderstatusid
and f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
and dim_dateidexpectedship_emd = esdemd.dim_dateid
and f.dim_dateidsnapshot = (select dim_dateid from dim_date where companycode = 'Not Set' and datevalue = (select processing_date from tmp_dayofprocessing_var_snp));


/* 2017-10-10 fix ShipToHeader Roxana D */


drop table if exists tmp_snaps;
create table tmp_snaps as 
select max(dim_customer_shipto) as dim_customer_shipto ,dd_salesdocno , dim_projectsourceid
from fact_salesorder
group by dd_salesdocno, dim_projectsourceid;

update FACT_SALESORDER_SNAPSHOT f
set f.dim_customer_shipto = s.dim_customer_shipto
from FACT_SALESORDER_SNAPSHOT f, tmp_snaps s
where f.dd_salesdocno = s.dd_salesdocno
and f.dim_projectsourceid = s.dim_projectsourceid
and f.dim_customer_shipto = 1;

update emd586.fact_salesorder_SNAPSHOT f
set f.dim_customer_shipto = s.dim_customer_shipto
from emd586.fact_salesorder_SNAPSHOT f, tmp_snaps s
where f.dd_salesdocno = s.dd_salesdocno
	and f.dim_projectsourceid = s.dim_projectsourceid
	and f.dim_customer_shipto = 1;


drop table if exists tmp_snaps;

/*End Of ShipToHeader */


drop table if exists tmp_dayofprocessing_var_snp;

/*2017-12-13 Add dimension for BIFILTERS_PRE_PRD Roxana D*/

update fact_salesorder_snapshot f
set f.dim_BIFILTERS_PRE_PRDId = ifnull(d.DIM_BIFILTERS_PRE_PRDid, 1)
from fact_salesorder_snapshot f, DIM_BIFILTERS_PRE_PRD d
where f.dim_projectsourceid = d.dim_projectsourceid
and case when f.DD_BIFILTERS_PRE_PRD = 'X' then 'Yes' else 'No' end = TRIM(d.BIFILTERS_PRE_PRD_CODE)
AND f.dim_BIFILTERS_PRE_PRDId <> ifnull(d.DIM_BIFILTERS_PRE_PRDid, 1);

/**/
