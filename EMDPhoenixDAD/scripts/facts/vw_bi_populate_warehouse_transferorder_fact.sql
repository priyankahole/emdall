/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : unknown */
/*   Created On     : 23 June 2015  */
/*   Description    : Brought from AM interface to SCRIPT model  */

/* Eliminate duplicates <ltak_lgnum, ltap_tanum, ltap_tapos> from LTAK_LTAP */
/* When we have duplicates on the key <ltak_lgnum, ltap_tanum, ltap_tapos>, we remove the ones that have NULL values for user ("ltap_qname" column)*/
drop table if exists ltak_ltap_duplicates;

create table ltak_ltap_duplicates as
select st.ltak_lgnum, st.ltap_tanum, st.ltap_tapos
from ltak_ltap st
group by st.ltak_lgnum, st.ltap_tanum, st.ltap_tapos
having count(*) > 1;

delete from ltak_ltap st
where exists (select 1 from ltak_ltap_duplicates z where st.ltak_lgnum = z.ltak_lgnum and st.ltap_tanum = z.ltap_tanum and st.ltap_tapos = z.ltap_tapos)
      and ltap_qname is null;
/* Eliminate duplicates <ltak_lgnum, ltap_tanum, ltap_tapos> from LTAK_LTAP */
drop table if exists fact_wmtransferorder_tmp;

CREATE TABLE fact_wmtransferorder_tmp 
AS 
select * from fact_wmtransferorder where 1 = 2;

/*initialize NUMBER_FOUNTAIN*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_wmtransferorder';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_wmtransferorder',ifnull(max(f.fact_wmtransferorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) 
FROM fact_wmtransferorder f;

DROP TABLE IF EXISTS fact_wmtransferorder_t1;
CREATE TABLE fact_wmtransferorder_t1
AS
SELECT row_number() over (order by '') rid,
        wn.dim_warehousenumberid,
			ifnull(st.LTAP_TANUM,'Not Set') dd_transferorderno,
			ifnull(st.LTAP_TAPOS,0) dd_transferorderitem,
			CONVERT(BIGINT, 1)  dim_movementtypeid ,
			CONVERT(BIGINT, 1) dim_dateidtransferordercreated,
			ifnull(st.LTAK_MBLNR,'Not Set') dd_materialdocno,
			CONVERT(BIGINT, 1)  dim_partid,
			pl.Dim_Plantid,
			ifnull(st.LTAP_QNAME,'Not Set') dd_username,
			ifnull(st.LTAP_VSOLM, 0) ct_sourcetargetqty,
			CONVERT(BIGINT, 1) dim_sourcestoragebinid,
			CONVERT(BIGINT, 1) dim_sourcestoragetypeid,                   
			CONVERT(BIGINT, 1) dim_destinationstoragebinid,
			CONVERT(BIGINT, 1) dim_destinationstoragetypeid,
			ifnull(st.LTAK_VBELN,'Not Set') dd_sddocumentno,
			CONVERT(BIGINT, 1) dim_dateidconfirmation,
			CONVERT(BIGINT, 1) dim_dateidplannedexecution ,
			CONVERT(BIGINT, 1) dim_dateidstartto,
			CONVERT(BIGINT, 1)  dim_dateidendto,
			CONVERT(BIGINT, 1) dim_rfqueueid,
			CONVERT(BIGINT, 1) dim_wmmovementtypeid,
			CONVERT(BIGINT, 1)  dim_dateiditemconfirmation,
			CONVERT(BIGINT, 1) dim_dateidgoodsreceipt,
			CONVERT(BIGINT, 1)  dim_dateidpickconfirmation ,
			ifnull(st.LTAP_UMREZ, 0) dd_conversionnum,
			ifnull(st.LTAP_UMREN, 0) dd_conversiondenom,
			CONVERT(BIGINT, 1) dim_wmstorageunittypeid,
			CONVERT(BIGINT, 1)  dim_inventoryindicatorid,
			ifnull(st.LTAP_MBPOS,0) dd_materialdocitemno,
			ifnull(st.LTAP_WENUM,'Not Set') dd_goodsreceiptno,
			ifnull(st.LTAP_WEPOS,0) dd_goodsreceiptitemno,
			ifnull(st.LTAP_VLQNR,'Not Set') dd_sourcequant,
			ifnull(st.LTAP_NLQNR,'Not Set') dd_destinationquant,
			ifnull(st.LTAP_CHARG,'Not Set') dd_batchnumber,
			CONVERT(BIGINT, 1) dim_baseunitofmeasureid,
			CONVERT(BIGINT, 1)  dim_alternativeunitofmeasureid,
		    TO_TIMEstamp(st.LTAP_QZEIT,'HH24MISS') as dd_timeofconfirmation,
			ifnull(st.LTAP_BRGEW, 0) ct_itemgrossweight,
			ifnull(st.LTAP_VORGA,'Not Set') dd_transferprocedure,
			CONVERT(BIGINT, 1)   dim_wmstockcategoryid,
			CONVERT(BIGINT, 1)  dim_specialstockid,
			ifnull(st.LTAP_VISTM, 0) ct_sourceactualqty,
			ifnull(st.LTAP_VDIFM, 0) ct_sourcedifferenceqty,
			ifnull(st.LTAP_VSOLA, 0) ct_sourcetargetalternateqty,
			ifnull(st.LTAP_VISTA, 0) ct_sourceactualalternateqty,
			ifnull(st.LTAP_VDIFA, 0) ct_sourcedifferencealternateqty,
			ifnull(st.LTAP_NSOLM, 0) ct_destinationtargetqty,
			ifnull(st.LTAP_NISTM, 0) ct_destinationactualqty,
			ifnull(st.LTAP_NDIFM, 0) ct_destinationdifferenceqty,
			ifnull(st.LTAP_NSOLA, 0) ct_destinationtargetalternateqty,
			ifnull(st.LTAP_NISTA, 0) ct_destinationactualalternateqty,
			ifnull(st.LTAP_NDIFA, 0) ct_destinationdifferencealternateqty,
			CONVERT(BIGINT, 1)  Dim_WMTransferOrderMiscId ,	   
			CONVERT(BIGINT, 1) dim_wmtransferorderitemtypeid,
			st.LTAK_BWART LTAK_BWART,
			st.LTAK_BDATU LTAK_BDATU,
			pl.CompanyCode CompanyCode,
			st.LTAP_MATNR LTAP_MATNR,
			st.LTAP_WERKS LTAP_WERKS,
			st.LTAP_VLPLA LTAP_VLPLA,
			st.LTAP_VLTYP LTAP_VLTYP,
			st.LTAK_LGNUM LTAK_LGNUM,
			st.LTAP_NLPLA LTAP_NLPLA,
			st.LTAP_NLTYP LTAP_NLTYP,
			st.LTAK_QDATU LTAK_QDATU,
			st.LTAK_PLDAT LTAK_PLDAT,
			st.LTAK_STDAT LTAK_STDAT,
			st.LTAK_ENDAT LTAK_ENDAT,
			st.LTAK_QUEUE LTAK_QUEUE,
			st.LTAK_BWLVS LTAK_BWLVS,
			st.LTAP_QDATU LTAP_QDATU,
			st.LTAP_WDATU LTAP_WDATU,
			st.LTAP_EDATU LTAP_EDATU,
			st.LTAP_LETYP LTAP_LETYP,
			st.LTAP_KZINV LTAP_KZINV,
			st.LTAP_MEINS LTAP_MEINS,
			st.LTAP_ALTME LTAP_ALTME,
			st.LTAP_BESTQ LTAP_BESTQ,
			st.LTAP_SOBKZ LTAP_SOBKZ,
			st.LTAK_KQUIT LTAK_KQUIT,
			st.LTAK_MINWM LTAK_MINWM,
			st.LTAP_KZQUI LTAP_KZQUI,
			st.LTAP_PQUIT LTAP_PQUIT,
			st.LTAP_POSTY LTAP_POSTY
     FROM LTAK_LTAP st,
          dim_plant pl,
		  dim_warehousenumber wn		 
    WHERE st.LTAP_WERKS = pl.PlantCode
		 AND wn.warehousecode = st.LTAK_LGNUM
         AND NOT EXISTS (SELECT 1 FROM fact_wmtransferorder fto
                         WHERE wn.dim_warehousenumberid = fto.dim_warehousenumberid
                               AND  st.LTAP_TANUM = fto.dd_transferorderno
                               AND  st.LTAP_TAPOS = fto.dd_transferorderitem);
							   
UPDATE 	fact_wmtransferorder_t1 t
SET dim_movementtypeid = ifnull( mt.dim_movementtypeid, 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_movementtype mt
		     ON  mt.movementtype = t.LTAK_BWART;	

UPDATE 	fact_wmtransferorder_t1 t
SET   dim_dateidtransferordercreated = ifnull ( tocdt.dim_dateid, 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_date tocdt
		     ON tocdt.DateValue = t.LTAK_BDATU
              AND tocdt.CompanyCode = t.CompanyCode ;	

UPDATE 	fact_wmtransferorder_t1 t
FROM fact_wmtransferorder_t1  t
SET  dim_partid  = ifnull(dim_partid, 1)
        LEFT JOIN dim_part dp
		     ON dp.PartNumber = t.LTAP_MATNR AND dp.plant = t.LTAP_WERKS;	
			 
UPDATE 	fact_wmtransferorder_t1 t
SET   dim_sourcestoragebinid = ifnull(  src.dim_storagebinid, 1)
FROM fact_wmtransferorder_t1 t 
        LEFT JOIN dim_storagebin src
		     ON src.storagebin = t.LTAP_VLPLA
                           AND src.storagetype = t.LTAP_VLTYP
						   AND src.WarehouseNumber = t.LTAK_LGNUM;	

			 
UPDATE 	fact_wmtransferorder_t1 t
SET   dim_sourcestoragetypeid = ifnull(  srctype.dim_wmstoragetypeid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_wmstoragetype srctype
		     ON srctype.storagetype = t.LTAP_VLTYP
					AND srctype.WarehouseNumber = t.LTAK_LGNUM	 ;
			 
			 
UPDATE 	fact_wmtransferorder_t1 t
SET  dim_destinationstoragebinid = ifnull( dest.dim_storagebinid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_storagebin dest
		     ON dest.storagebin = t.LTAP_NLPLA
                           AND dest.storagetype = t.LTAP_NLTYP
						   AND dest.WarehouseNumber = t.LTAK_LGNUM;			 
			 
			 
			 
UPDATE 	fact_wmtransferorder_t1 t
SET   dim_destinationstoragetypeid = ifnull( desttype.dim_wmstoragetypeid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_wmstoragetype desttype
		     ON desttype.storagetype = t.LTAP_VLTYP
			AND desttype.WarehouseNumber = t.LTAK_LGNUM	;

			 
						 
UPDATE 	fact_wmtransferorder_t1 t
SET   dim_dateidconfirmation = ifnull( confdt.dim_dateid  , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_date confdt
		     ON confdt.DateValue = t.LTAK_QDATU
						   AND confdt.CompanyCode = t.CompanyCode; 	

UPDATE 	fact_wmtransferorder_t1 t
SET   dim_dateidplannedexecution = ifnull(  pedt.dim_dateid, 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_date pedt
		     ON pedt.DateValue = t.LTAK_PLDAT
			AND pedt.CompanyCode = t.CompanyCode  ;	

UPDATE 	fact_wmtransferorder_t1 t
SET   dim_dateidstartto = ifnull( stdt.dim_dateid  , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_date stdt
		     ON stdt.DateValue = t.LTAK_STDAT
			 AND stdt.CompanyCode = t.CompanyCode	;

UPDATE 	fact_wmtransferorder_t1 t
SET   dim_dateidendto = ifnull(  endt.dim_dateid, 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_date endt
		     ON endt.DateValue = t.LTAK_ENDAT
			 AND endt.CompanyCode = t.CompanyCode ;	

UPDATE 	fact_wmtransferorder_t1 t
SET   dim_rfqueueid = ifnull( rfq.dim_rfqueueid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_rfqueue rfq
		     ON  rfq.Queue = t.LTAK_QUEUE
			 AND rfq.WarehouseNumber = t.LTAK_LGNUM	;

UPDATE 	fact_wmtransferorder_t1 t
SET  dim_wmmovementtypeid  = ifnull( wmmt.dim_wmmovementtypeid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_wmmovementtype wmmt
		     ON wmmt.MovementType = t.LTAK_BWLVS
			 AND wmmt.WarehouseNumber = t.LTAK_LGNUM;	

UPDATE 	fact_wmtransferorder_t1 t
SET  dim_dateiditemconfirmation = ifnull( iconfdt.dim_dateid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_date iconfdt
		     ON iconfdt.DateValue = t.LTAP_QDATU 
		     AND iconfdt.CompanyCode = t.CompanyCode;

UPDATE 	fact_wmtransferorder_t1 t
SET  dim_dateidgoodsreceipt = ifnull( grdt.dim_dateid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_date grdt
		     ON grdt.DateValue = t.LTAP_WDATU
			 AND grdt.CompanyCode = t.CompanyCode;	

UPDATE 	fact_wmtransferorder_t1 t
SET   dim_dateidpickconfirmation = ifnull( pcdt.dim_dateid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_date pcdt
		     ON pcdt.DateValue = t.LTAP_EDATU
			 AND pcdt.CompanyCode = t.CompanyCode	;

UPDATE 	fact_wmtransferorder_t1 t
SET   dim_wmstorageunittypeid = ifnull(  wmsut.dim_wmstorageunittypeid  , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_wmstorageunittype wmsut
		     ON wmsut.StorageUnitType= t.LTAP_LETYP
			 AND wmsut.WarehouseNumber = t.LTAK_LGNUM	;
			 
UPDATE 	fact_wmtransferorder_t1 t
SET  dim_inventoryindicatorid  = ifnull( ii.dim_inventoryindicatorid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_inventoryindicator ii
		     ON ii.InventoryIndicator = t.LTAP_KZINV;	

UPDATE 	fact_wmtransferorder_t1 t
SET  dim_baseunitofmeasureid  = ifnull( um.dim_unitofmeasureid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_unitofmeasure um
		     ON um.uom = t.LTAP_MEINS;	

UPDATE 	fact_wmtransferorder_t1 t
SET  dim_alternativeunitofmeasureid  = ifnull( aum.dim_unitofmeasureid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_unitofmeasure aum
		     ON aum.uom = t.LTAP_ALTME;	

UPDATE 	fact_wmtransferorder_t1 t
SET  dim_wmstockcategoryid  = ifnull( wmsc.dim_wmstockcategoryid , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_wmstockcategory wmsc
		     ON wmsc.WMStockCategory = t.LTAP_BESTQ;	

UPDATE 	fact_wmtransferorder_t1 t
SET  dim_specialstockid  = ifnull( ss.dim_specialstockid  , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_specialstock ss
		     ON ss.specialstockindicator = t.LTAP_SOBKZ;	

UPDATE 	fact_wmtransferorder_t1 t
SET  Dim_WMTransferOrderMiscId  = ifnull(  wmtomisc.Dim_WMTransferOrderMiscId, 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN dim_wmtransferordermiscellaneous wmtomisc
		     ON wmtomisc.Confirmation = ifnull(t.LTAK_KQUIT, 'Not Set')
		                   AND wmtomisc.NoRealStorage = ifnull(t.LTAK_MINWM, 'Not Set')
		                   AND wmtomisc.ConfirmationRequired = ifnull(t.LTAP_KZQUI, 'Not Set')
		                   AND wmtomisc.ConfirmationComplete = ifnull(t.LTAP_PQUIT, 'Not Set')		;

UPDATE 	fact_wmtransferorder_t1 t
SET  dim_wmtransferorderitemtypeid  = ifnull(  wmtoit.dim_wmtransferorderitemtypeid   , 1)
FROM fact_wmtransferorder_t1 t
        LEFT JOIN  dim_wmtransferorderitemtype wmtoit
		     ON wmtoit.WMTransferOrderItemType = t.LTAP_POSTY;	

			 
INSERT INTO fact_wmtransferorder_tmp(fact_wmtransferorderid,
								dim_warehousenumberid,
								dd_transferorderno,
								dd_transferorderitem,								
								dim_movementtypeid,
								dim_dateidtransferordercreated ,
								dd_materialdocno,
								dim_partid,
								dim_plantid,
								dd_username,
								ct_sourcetargetqty,
								dim_sourcestoragebinid,
								dim_sourcestoragetypeid,
								dim_destinationstoragebinid,
								dim_destinationstoragetypeid,
								dd_sddocumentno,
								dim_dateidconfirmation,
								dim_dateidplannedexecution,
								dim_dateidstartto,
								dim_dateidendto,
								dim_rfqueueid,
								dim_wmmovementtypeid,
								dim_dateiditemconfirmation,
								dim_dateidgoodsreceipt,
								dim_dateidpickconfirmation,
								dd_conversionnum,
								dd_conversiondenom,
								dim_wmstorageunittypeid,
								dim_inventoryindicatorid,
								dd_materialdocitemno,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dd_sourcequant,
								dd_destinationquant,
								dd_batchnumber,
								dim_baseunitofmeasureid,
								dim_alternativeunitofmeasureid,
								dd_timeofconfirmation,
								ct_itemgrossweight,
								dd_transferprocedure,
								dim_wmstockcategoryid,
								dim_specialstockid,
								ct_sourceactualqty,
								ct_sourcedifferenceqty,
								ct_sourcetargetalternateqty,
								ct_sourceactualalternateqty,
								ct_sourcedifferencealternateqty,
								ct_destinationtargetqty,
								ct_destinationactualqty,
								ct_destinationdifferenceqty,
								ct_destinationtargetalternateqty,
								ct_destinationactualalternateqty,
								ct_destinationdifferencealternateqty,
								Dim_WMTransferOrderMiscId,
								dim_wmtransferorderitemtypeid)			
								
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN 
   WHERE table_name = 'fact_wmtransferorder') 
   + row_number() over (order by '') ) fact_wmtransferorderid,
        a.*
        FROM ( SELECT DISTINCT 	
								dim_warehousenumberid,
								dd_transferorderno,
								dd_transferorderitem,								
								dim_movementtypeid,
								dim_dateidtransferordercreated ,
								dd_materialdocno,
								dim_partid,
								dim_plantid,
								dd_username,
								ct_sourcetargetqty,
								dim_sourcestoragebinid,
								dim_sourcestoragetypeid,
								dim_destinationstoragebinid,
								dim_destinationstoragetypeid,
								dd_sddocumentno,
								dim_dateidconfirmation,
								dim_dateidplannedexecution,
								dim_dateidstartto,
								dim_dateidendto,
								dim_rfqueueid,
								dim_wmmovementtypeid,
								dim_dateiditemconfirmation,
								dim_dateidgoodsreceipt,
								dim_dateidpickconfirmation,
								dd_conversionnum,
								dd_conversiondenom,
								dim_wmstorageunittypeid,
								dim_inventoryindicatorid,
								dd_materialdocitemno,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dd_sourcequant,
								dd_destinationquant,
								dd_batchnumber,
								dim_baseunitofmeasureid,
								dim_alternativeunitofmeasureid,
								dd_timeofconfirmation,
								ct_itemgrossweight,
								dd_transferprocedure,
								dim_wmstockcategoryid,
								dim_specialstockid,
								ct_sourceactualqty,
								ct_sourcedifferenceqty,
								ct_sourcetargetalternateqty,
								ct_sourceactualalternateqty,
								ct_sourcedifferencealternateqty,
								ct_destinationtargetqty,
								ct_destinationactualqty,
								ct_destinationdifferenceqty,
								ct_destinationtargetalternateqty,
								ct_destinationactualalternateqty,
								ct_destinationdifferencealternateqty,
								Dim_WMTransferOrderMiscId,
								dim_wmtransferorderitemtypeid
			FROM fact_wmtransferorder_t1  ) a;
	
/*   UPDATE Movement TYPE   */


drop table if exists tmp_dim_movementtype;

CREATE TABLE tmp_dim_movementtype AS
select mt.*,
       row_number() over (PARTITION BY movementtype ORDER BY dim_movementtypeid) rowno
from dim_movementtype mt;

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_movementtypeid = ifnull(mt.dim_movementtypeid, 1)
FROM tmp_dim_movementtype mt, 
     LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
     fact_wmtransferorder_tmp fto
WHERE mt.movementtype = st.LTAK_BWART
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND mt.rowno = 1
AND fto.dim_movementtypeid <> ifnull(mt.dim_movementtypeid, 1);

drop table if exists tmp_dim_movementtype;

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_dateidtransferordercreated = ifnull(tocdt.dim_dateid, 1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date tocdt,
	fact_wmtransferorder_tmp fto
WHERE tocdt.DateValue = st.LTAK_BDATU
AND tocdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidtransferordercreated <> ifnull(tocdt.dim_dateid, 1);

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_materialdocno = ifnull(st.LTAK_MBLNR,'Not Set')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_materialdocno <> ifnull(st.LTAK_MBLNR,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_plantid = ifnull(pl.Dim_Plantid,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_plantid <> ifnull(pl.Dim_Plantid,1);

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_username = ifnull(st.LTAP_QNAME,'Not Set')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_username <> ifnull(st.LTAP_QNAME,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_sourcetargetqty = ifnull(st.LTAP_VSOLM,0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND ct_sourcetargetqty <> ifnull(st.LTAP_VSOLM,0);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_sourcestoragebinid = ifnull(src.dim_storagebinid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_storagebin src,
	 fact_wmtransferorder_tmp fto
WHERE src.storagebin = st.LTAP_VLPLA
AND src.storagetype = st.LTAP_VLTYP
AND src.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_sourcestoragebinid <> ifnull(src.dim_storagebinid ,1);	


UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_sourcestoragetypeid = ifnull(srctype.dim_wmstoragetypeid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmstoragetype srctype,
	 fact_wmtransferorder_tmp fto
WHERE srctype.storagetype = st.LTAP_VLTYP
AND srctype.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_sourcestoragetypeid <> ifnull(srctype.dim_wmstoragetypeid ,1);


UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_destinationstoragebinid = ifnull(dest.dim_storagebinid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_storagebin dest,
	 fact_wmtransferorder_tmp fto
WHERE dest.storagebin = st.LTAP_NLPLA
AND dest.storagetype = st.LTAP_NLTYP
AND dest.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_destinationstoragebinid <> ifnull(dest.dim_storagebinid ,1);


UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_destinationstoragetypeid = ifnull(desttype.dim_wmstoragetypeid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmstoragetype desttype,
	 fact_wmtransferorder_tmp fto
WHERE desttype.storagetype = st.LTAP_NLTYP
AND desttype.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_destinationstoragetypeid <> ifnull(desttype.dim_wmstoragetypeid ,1);


UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_sddocumentno = ifnull(st.LTAK_VBELN,'Not Set')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_sddocumentno <> ifnull(st.LTAK_VBELN,'Not Set');


UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_dateidconfirmation = ifnull(confdt.dim_dateid,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date confdt,
	 fact_wmtransferorder_tmp fto
WHERE confdt.DateValue = st.LTAK_QDATU
AND confdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidconfirmation <> ifnull(confdt.dim_dateid,1);
	
UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_dateidplannedexecution = ifnull(pedt.dim_dateid,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date pedt,
	 fact_wmtransferorder_tmp fto
WHERE pedt.DateValue = st.LTAK_PLDAT
AND pedt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidplannedexecution <> ifnull(pedt.dim_dateid,1);

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_dateidstartto = ifnull( stdt.dim_dateid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date stdt,
	 fact_wmtransferorder_tmp fto
WHERE stdt.DateValue = st.LTAK_STDAT 
AND stdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidstartto <> ifnull( stdt.dim_dateid ,1);

	
UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_dateidendto = ifnull( endt.dim_dateid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date endt,
	 fact_wmtransferorder_tmp fto
WHERE endt.DateValue = st.LTAK_ENDAT
AND endt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidendto <> ifnull( endt.dim_dateid ,1);	


UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_rfqueueid = ifnull( rfq.dim_rfqueueid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_rfqueue rfq,
	 fact_wmtransferorder_tmp fto
WHERE  rfq.Queue = st.LTAK_QUEUE
AND rfq.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_rfqueueid <> ifnull( rfq.dim_rfqueueid ,1);

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_wmmovementtypeid = ifnull(wmmt.dim_wmmovementtypeid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmmovementtype wmmt,
	  fact_wmtransferorder_tmp fto
WHERE wmmt.MovementType = st.LTAK_BWLVS
AND wmmt.WarehouseNumber = st.LTAK_LGNUM
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_wmmovementtypeid <> ifnull(wmmt.dim_wmmovementtypeid ,1);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_dateiditemconfirmation = ifnull(iconfdt.dim_dateid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date iconfdt,
	 fact_wmtransferorder_tmp fto
WHERE iconfdt.DateValue = st.LTAP_QDATU
AND iconfdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateiditemconfirmation <>ifnull(iconfdt.dim_dateid ,1);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_dateiditemconfirmation = ifnull(iconfdt.dim_dateid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date iconfdt,
	  fact_wmtransferorder_tmp fto
WHERE iconfdt.DateValue = st.LTAP_QDATU
AND iconfdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateiditemconfirmation <>ifnull(iconfdt.dim_dateid ,1);	


UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_dateidpickconfirmation = ifnull(pcdt.dim_dateid ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_date pcdt,
	 fact_wmtransferorder_tmp fto
WHERE pcdt.DateValue = st.LTAP_EDATU
AND pcdt.CompanyCode = pl.CompanyCode
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_dateidpickconfirmation <> ifnull(pcdt.dim_dateid ,1);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_conversionnum  = ifnull(st.LTAP_UMREZ, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_conversionnum <> ifnull(st.LTAP_UMREZ, 0);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_conversiondenom  = ifnull(st.LTAP_UMREN, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_conversiondenom <> ifnull(st.LTAP_UMREN, 0);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_wmstorageunittypeid  = ifnull( wmsut.dim_wmstorageunittypeid, 1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmstorageunittype wmsut,
	  fact_wmtransferorder_tmp fto
WHERE wmsut.StorageUnitType= st.LTAP_LETYP
AND wmsut.WarehouseNumber = st.LTAK_LGNUM 
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_wmstorageunittypeid <> ifnull( wmsut.dim_wmstorageunittypeid, 1);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_inventoryindicatorid  = ifnull(ii.dim_inventoryindicatorid, 1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_inventoryindicator ii,
	 fact_wmtransferorder_tmp fto
WHERE ii.InventoryIndicator = st.LTAP_KZINV
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_inventoryindicatorid <> ifnull(ii.dim_inventoryindicatorid, 1);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_materialdocitemno  = ifnull(st.LTAP_MBPOS,0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_materialdocitemno <> ifnull(st.LTAP_MBPOS,0);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_goodsreceiptno =  ifnull(st.LTAP_WENUM,'Not Set')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_goodsreceiptno <> ifnull(st.LTAP_WENUM,'Not Set');	

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_goodsreceiptitemno =  ifnull(st.LTAP_WEPOS,0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	  fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_goodsreceiptitemno <> ifnull(st.LTAP_WEPOS,0);

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_sourcequant  = ifnull(st.LTAP_VLQNR,'Not Set')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_sourcequant <> ifnull(st.LTAP_VLQNR,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_destinationquant  = ifnull(st.LTAP_NLQNR,'Not Set')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_destinationquant <> ifnull(st.LTAP_NLQNR,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_batchnumber  = ifnull(st.LTAP_CHARG,'Not Set')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_batchnumber <> ifnull(st.LTAP_CHARG,'Not Set');

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_baseunitofmeasureid  = ifnull(um.dim_unitofmeasureid , 1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_unitofmeasure um,
	 fact_wmtransferorder_tmp fto
WHERE um.uom = st.LTAP_MEINS
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_baseunitofmeasureid <> ifnull(um.dim_unitofmeasureid , 1);



UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_alternativeunitofmeasureid  = ifnull(aum.dim_unitofmeasureid  , 1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_unitofmeasure aum,
	 fact_wmtransferorder_tmp fto
WHERE aum.uom = st.LTAP_ALTME
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_alternativeunitofmeasureid <> ifnull(aum.dim_unitofmeasureid  , 1);

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_timeofconfirmation  = TO_TIMESTAMP(st.LTAP_QZEIT,'HH24MISS')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_timeofconfirmation <> TO_TIMESTAMP(st.LTAP_QZEIT,'HH24MISS');


UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_itemgrossweight  = ifnull(st.LTAP_BRGEW ,1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_itemgrossweight <> ifnull(st.LTAP_BRGEW ,1);

UPDATE fact_wmtransferorder_tmp fto
SET fto.dd_transferprocedure  = ifnull(st.LTAP_VORGA,'Not Set')
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dd_transferprocedure <> ifnull(st.LTAP_VORGA,'Not Set');
	
UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_wmstockcategoryid  = ifnull(wmsc.dim_wmstockcategoryid , 1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmstockcategory wmsc,
	 fact_wmtransferorder_tmp fto
WHERE wmsc.WMStockCategory = LTAP_BESTQ
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_wmstockcategoryid  <> ifnull(wmsc.dim_wmstockcategoryid , 1);


UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_specialstockid  = ifnull(ss.dim_specialstockid , 1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_specialstock ss,
	 fact_wmtransferorder_tmp fto
WHERE ss.specialstockindicator = st.LTAP_SOBKZ
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_specialstockid <> ifnull(ss.dim_specialstockid , 1);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_sourceactualqty = ifnull(st.LTAP_VISTM, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourceactualqty <> ifnull(st.LTAP_VISTM, 0);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_sourcedifferenceqty = ifnull(st.LTAP_VDIFM, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourcedifferenceqty <> ifnull(st.LTAP_VDIFM, 0);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_sourcetargetalternateqty = ifnull(st.LTAP_VSOLA, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourcetargetalternateqty <> ifnull(st.LTAP_VSOLA, 0);	

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_sourceactualalternateqty = ifnull(st.LTAP_VISTA, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourceactualalternateqty <> ifnull(st.LTAP_VISTA, 0);

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_sourcedifferencealternateqty = ifnull(st.LTAP_VDIFA, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_sourcedifferencealternateqty <> ifnull(st.LTAP_VDIFA, 0);

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_destinationtargetqty  = 	ifnull(st.LTAP_NSOLM, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationtargetqty  <>	ifnull(st.LTAP_NSOLM, 0);

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_destinationactualqty = ifnull(st.LTAP_NISTM, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 	
AND fto.ct_destinationactualqty  <>	ifnull(st.LTAP_NISTM, 0);

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_destinationdifferenceqty  = 	ifnull(st.LTAP_NDIFM, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationdifferenceqty <> ifnull(st.LTAP_NDIFM, 0);

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_destinationtargetalternateqty  = ifnull(st.LTAP_NSOLA, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationtargetalternateqty <> ifnull(st.LTAP_NSOLA, 0);


UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_destinationactualalternateqty  = ifnull(st.LTAP_NISTA, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationactualalternateqty <> ifnull(st.LTAP_NISTA, 0);

UPDATE fact_wmtransferorder_tmp fto
SET fto.ct_destinationdifferencealternateqty  = ifnull(st.LTAP_NDIFA, 0)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 fact_wmtransferorder_tmp fto
WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.ct_destinationdifferencealternateqty  <> ifnull(st.LTAP_NDIFA, 0);

UPDATE fact_wmtransferorder_tmp fto
SET fto.Dim_WMTransferOrderMiscId = ifnull(wmtomisc.Dim_WMTransferOrderMiscId, 1)	
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmtransferordermiscellaneous wmtomisc,
	 fact_wmtransferorder_tmp fto

WHERE st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND wmtomisc.Confirmation = ifnull(st.LTAK_KQUIT, 'Not Set')
AND wmtomisc.NoRealStorage = ifnull(st.LTAK_MINWM, 'Not Set')
AND wmtomisc.ConfirmationRequired = ifnull(st.LTAP_KZQUI, 'Not Set')
AND wmtomisc.ConfirmationComplete = ifnull(st.LTAP_PQUIT, 'Not Set')	
AND fto.Dim_WMTransferOrderMiscId <> ifnull(wmtomisc.Dim_WMTransferOrderMiscId, 1);

UPDATE fact_wmtransferorder_tmp fto
SET fto.dim_wmtransferorderitemtypeid = ifnull(wmtoit.dim_wmtransferorderitemtypeid, 1)
FROM LTAK_LTAP st,
	 dim_plant pl,
	 dim_warehousenumber wn,
	 dim_wmtransferorderitemtype wmtoit,
	 fact_wmtransferorder_tmp fto

WHERE wmtoit.WMTransferOrderItemType = st.LTAP_POSTY
AND st.LTAP_WERKS = pl.PlantCode
AND wn.warehousecode = st.LTAK_LGNUM
AND wn.dim_warehousenumberid = fto.dim_warehousenumberid
AND st.LTAP_TANUM = fto.dd_transferorderno
AND st.LTAP_TAPOS = fto.dd_transferorderitem 
AND fto.dim_wmtransferorderitemtypeid <> ifnull(wmtoit.dim_wmtransferorderitemtypeid, 1);


insert into fact_wmtransferorder
(fact_wmtransferorderid,
								dim_warehousenumberid,
								dd_transferorderno,
								dd_transferorderitem,								
								dim_movementtypeid,
								dim_dateidtransferordercreated ,
								dd_materialdocno,
								dim_partid,
								dim_plantid,
								dd_username,
								ct_sourcetargetqty,
								dim_sourcestoragebinid,
								dim_sourcestoragetypeid,
								dim_destinationstoragebinid,
								dim_destinationstoragetypeid,
								dd_sddocumentno,
								dim_dateidconfirmation,
								dim_dateidplannedexecution,
								dim_dateidstartto,
								dim_dateidendto,
								dim_rfqueueid,
								dim_wmmovementtypeid,
								dim_dateiditemconfirmation,
								dim_dateidgoodsreceipt,
								dim_dateidpickconfirmation,
								dd_conversionnum,
								dd_conversiondenom,
								dim_wmstorageunittypeid,
								dim_inventoryindicatorid,
								dd_materialdocitemno,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dd_sourcequant,
								dd_destinationquant,
								dd_batchnumber,
								dim_baseunitofmeasureid,
								dim_alternativeunitofmeasureid,
								dd_timeofconfirmation,
								ct_itemgrossweight,
								dd_transferprocedure,
								dim_wmstockcategoryid,
								dim_specialstockid,
								ct_sourceactualqty,
								ct_sourcedifferenceqty,
								ct_sourcetargetalternateqty,
								ct_sourceactualalternateqty,
								ct_sourcedifferencealternateqty,
								ct_destinationtargetqty,
								ct_destinationactualqty,
								ct_destinationdifferenceqty,
								ct_destinationtargetalternateqty,
								ct_destinationactualalternateqty,
								ct_destinationdifferencealternateqty,
								Dim_WMTransferOrderMiscId,
								dim_wmtransferorderitemtypeid
)
select fact_wmtransferorderid,
								dim_warehousenumberid,
								dd_transferorderno,
								dd_transferorderitem,								
								dim_movementtypeid,
								dim_dateidtransferordercreated ,
								dd_materialdocno,
								dim_partid,
								dim_plantid,
								dd_username,
								ct_sourcetargetqty,
								dim_sourcestoragebinid,
								dim_sourcestoragetypeid,
								dim_destinationstoragebinid,
								dim_destinationstoragetypeid,
								dd_sddocumentno,
								dim_dateidconfirmation,
								dim_dateidplannedexecution,
								dim_dateidstartto,
								dim_dateidendto,
								dim_rfqueueid,
								dim_wmmovementtypeid,
								dim_dateiditemconfirmation,
								dim_dateidgoodsreceipt,
								dim_dateidpickconfirmation,
								dd_conversionnum,
								dd_conversiondenom,
								dim_wmstorageunittypeid,
								dim_inventoryindicatorid,
								dd_materialdocitemno,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dd_sourcequant,
								dd_destinationquant,
								dd_batchnumber,
								dim_baseunitofmeasureid,
								dim_alternativeunitofmeasureid,
								dd_timeofconfirmation,
								ct_itemgrossweight,
								dd_transferprocedure,
								dim_wmstockcategoryid,
								dim_specialstockid,
								ct_sourceactualqty,
								ct_sourcedifferenceqty,
								ct_sourcetargetalternateqty,
								ct_sourceactualalternateqty,
								ct_sourcedifferencealternateqty,
								ct_destinationtargetqty,
								ct_destinationactualqty,
								ct_destinationdifferenceqty,
								ct_destinationtargetalternateqty,
								ct_destinationactualalternateqty,
								ct_destinationdifferencealternateqty,
								Dim_WMTransferOrderMiscId,
								dim_wmtransferorderitemtypeid
from fact_wmtransferorder_tmp;

drop table if exists fact_wmtransferorder_tmp;

