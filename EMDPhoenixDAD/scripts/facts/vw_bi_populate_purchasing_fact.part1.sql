
/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 20 Jul 2013 */
/*   Description    : Stored Proc bi_populate_purchasing_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   17 Apr 2014      Simona    1.38              Added field dd_OurReference                                    */
/*   13 Jan 2014      Lokesh	1.37              Additional fields dim_DateIdInvoiceDay,dd_PoLineNBR,dd_PurchFrom,ct_CatalogLeadTime */
/*   10 Dec 2013      Issam  	1.32		  Added dd_DeletionIndicator						  */
/*   14 Nov 2013      Issam  	1.30		  Dim_ValuationClassId, dd_DeliveryNumber, dd_POCreatedBy		  */
/* 				  		dim_ValuationTypeId, amt_POEXRate	  */
/*   05 Nov 2013      Issam  	1.26		  Added EKKO_FRGKE and EKES_XBLNR		   		 */
/*   12 Sep 2013      Lokesh	1.12		 Currency changes Tran/Local/GBL					  */
/*   1 Aug 2013       Lokesh    1.11              Sync up with AFS script. Issue was found with amt_DeliveryTotal, amt_DeliveryPPV */
/*                                                Now ABS is used while retrieving data from tmp_getExchangeRate1 e.g z.pFromExchangeRate = abs(EKKO_WKURS) */
/*   29 Jul 2013      Lokesh    1.10              Reverted Shanthi's ct_QtyReduced changes as they are not going into prod   */
/*   28 Jul 2013      Lokesh    1.9               int overflow in aei when i.EINA_UMREN = 0                       */
/*                                                Note that mysql proc was written expecting this to be non-zero  */
/*   28 Jul 2013      Lokesh    1.8               dd_ConfirmEdi fix. Bug in mysql, caught in honda validation. int to char update.            */
/*							 Changed version numbering to match CVS. 					*/
/*   24 Jul 2013      Lokesh    1.2               Merge Mircea's changes for QtyConversion_EqualTo,QtyConversion_Denom	*/
/*							 PriceConversion_EqualTo and PriceConversion_Denom		  */
/*   24 Jul 2013      Lokesh    1.1               Merge Shanthi's changes for ct_qtyreduced                       */
/*   20 Jul 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/

/* Note that this needs to be split into part1 and part 2 ( divided at the point where it updates the stdunitprice and costing date */
/* Also note that some initial part is in the exchange rate script as it populates tables required by exch rate call */
/* Use columbia99(testVW) for testing and validation */

delete from NUMBER_FOUNTAIN where table_name = 'fact_purchase';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_purchase',ifnull(max(fact_purchaseid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
FROM fact_purchase;

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
select 'processinglog',ifnull(max(processinglogid),0)
FROM processinglog;


DROP TABLE IF EXISTS tmp_var_purchasing_fact;

CREATE table tmp_var_purchasing_fact AS
Select CONVERT(varchar(3), ifnull((SELECT property_value
                                   FROM systemproperty
                                   WHERE property = 'customer.global.currency'),
                                  'USD')) as pGlobalCurrency,
	   CONVERT(varchar(10), ifnull((SELECT property_value
                                    FROM systemproperty
                                    WHERE property = 'custom.vendor.partnerfunction.key1'),
                                   'Not Set')) as pCustomPartnerFunction1,
	   CONVERT(varchar(10), ifnull((SELECT property_value
                 					FROM systemproperty
               						WHERE property = 'custom.vendor.partnerfunction.key2'),
              					   'Not Set')) as pCustomPartnerFunction2,
	   CONVERT(varchar(10), ifnull((SELECT property_value
                 					FROM systemproperty
                					WHERE property = 'custom.vendor.partnerfunction.key3'),
              					   'Not Set')) as pCustomPartnerFunction3,
	   CONVERT(varchar(10), ifnull((SELECT property_value
                 					FROM systemproperty
                					WHERE property = 'custom.vendor.partnerfunction.key4'),
					               'Not Set')) as pCustomPartnerFunction4;

INSERT INTO ekko_ekpo_eket
SELECT distinct * FROM ekko_ekpo_eket_mseg a
WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);

/* DELETE FROM ekko_ekpo_eket_mseg */
/*Georgiana Changes 11 Jun 2015- enumerating the columns for insert*/
INSERT INTO ekko_ekpo_eket (ekpo_ebeln,
							ekpo_ebelp,
							eket_etenr,
							eket_banfn,
							eket_menge,
							eket_eindt,
							eket_bedat,
							eket_wemng,
							eket_slfdt,
							ekko_aedat,
							ekko_bukrs,
							ekko_bstyp,
							ekko_bsart,
							ekko_ernam,
							ekko_ekorg,
							ekko_lifnr,
							ekko_waers,
							ekko_bedat,
							ekko_ekgrp,
							ekko_wkurs,
							ekko_loekz,
							ekko_statu,
							ekko_reswk,
							ekko_autlf,
							ekko_fixpo,
							ekko_kufix,
							ekko_frgzu,
							ekko_zterm,
							ekko_kalsm,
							ekpo_matnr,
							ekpo_werks,
							ekpo_meins,
							ekpo_netpr,
							ekpo_loekz,
							ekpo_bprme,
							ekpo_peinh,
							ekpo_menge,
							ekpo_matkl,
							ekpo_mtart,
							ekpo_knttp,
							ekpo_netwr,
							ekpo_pstyp,
							ekpo_elikz,
							ekpo_retpo,
							ekpo_umrez,
							ekpo_umren,
							ekpo_lmein,
							ekpo_insmk,
							ekpo_bwtar,
							ekpo_infnr,
							ekpo_webaz,
							ekpo_uebto,
							ekpo_untto,
							ekpo_idnlf,
							ekpo_aedat,
							ekpo_prdat,
							ekpo_kzvbr,
							ekpo_inco1,
							ekpo_status,
							ekpo_adrnr,
							ekpo_lgort,
							ekpo_mwskz,
							ekpo_vrtkz,
							ekpo_erekz,
							ekpo_wepos,
							ekpo_weunb,
							ekpo_j_1bownpro,
							ekpo_abskz,
							ekpo_bnfpo,
							ekpo_bpumz,
							ekpo_bpumn,
							ekpo_effwr,
							eket_sernr,
							ekpo_adrn2,
							ekko_absgr,
							ekpo_packno,
							ekpo_etfz1,
							ekpo_lgbzo,
							ekpo_lgbzo_b,
							ekpo_plifz,
							ekpo_inco2,
							ekko_inco1,
							ekko_inco2,
							ekpo_revlv,
							ekko_ihrez,
							ekpo_ko_prctr,
							eket_wamng,
							ekpo_reslo,
							ekko_kdatb,
							ekko_kdate,
							ekpo_stapo,
							ekpo_txz01,
							eket_bnfpo,
							ekpo_banfn,
							ekpo_bukrs,
							ekko_knumv,
							ekpo_brtwr,
							ekpo_kzwi1,
							ekpo_kzwi2,
							ekpo_kzwi3,
							ekpo_kzwi4,
							ekpo_kzwi5,
							ekpo_kzwi6,
							ekpo_j_3aexfcm,
							ekpo_route,
							ekpo_tsbed,
							eket_j_3anetp,
							eket_j_3anetw,
							eket_j_4kscat,
							eket_j_3asize,
							ekpo_j_3asean,
							ekpo_j_3adat,
							ekpo_afs_collection,
							ekpo_afs_theme,
							eket_j_3aefda,
							eket_dabmg,
							ekko_frgke,
							eket_j_3aelikz,
							eket_j_3aetenr,
							ekpo_zzppv_rc,
							ekpo_zzppv_rct,
							ekko_lponr,
							ekpo_ltsnr,
							ekpo_zzprior,
							ekpo_uebtk,
							ekko_unsez,
							eket_j_3auanr,
							eket_j_3aupos,
							eket_j_3aetenv,
							ekko_exnum,
							eket_mbdat,
							eket_charg,
							eket_licha,
							ekpo_j_3aexfcp,
            EKPO_EVERS)
SELECT distinct ekpo_ebeln,
				ekpo_ebelp,
				eket_etenr,
				eket_banfn,
				eket_menge,
				eket_eindt,
				eket_bedat,
				eket_wemng,
				eket_slfdt,
				ekko_aedat,
				ekko_bukrs,
				ekko_bstyp,
				ekko_bsart,
				ekko_ernam,
				ekko_ekorg,
				ekko_lifnr,
				ekko_waers,
				ekko_bedat,
				ekko_ekgrp,
				ekko_wkurs,
				ekko_loekz,
				ekko_statu,
				ekko_reswk,
				ekko_autlf,
				ekko_fixpo,
				ekko_kufix,
				ekko_frgzu,
				ekko_zterm,
				ekko_kalsm,
				ekpo_matnr,
				ekpo_werks,
				ekpo_meins,
				ekpo_netpr,
				ekpo_loekz,
				ekpo_bprme,
				ekpo_peinh,
				ekpo_menge,
				ekpo_matkl,
				ekpo_mtart,
				ekpo_knttp,
				ekpo_netwr,
				ekpo_pstyp,
				ekpo_elikz,
				ekpo_retpo,
				ekpo_umrez,
				ekpo_umren,
				ekpo_lmein,
				ekpo_insmk,
				ekpo_bwtar,
				ekpo_infnr,
				ekpo_webaz,
				ekpo_uebto,
				ekpo_untto,
				ekpo_idnlf,
				ekpo_aedat,
				ekpo_prdat,
				ekpo_kzvbr,
				ekpo_inco1,
				ekpo_status,
				ekpo_adrnr,
				ekpo_lgort,
				ekpo_mwskz,
				ekpo_vrtkz,
				ekpo_erekz,
				ekpo_wepos,
				ekpo_weunb,
				ekpo_j_1bownpro,
				ekpo_abskz,
				ekpo_bnfpo,
				ekpo_bpumz,
				ekpo_bpumn,
				ekpo_effwr,
				eket_sernr,
				ekpo_adrn2,
				ekko_absgr,
				ekpo_packno,
				ekpo_etfz1,
				ekpo_lgbzo,
				ekpo_lgbzo_b,
				ekpo_plifz,
				ekpo_inco2,
				ekko_inco1,
				ekko_inco2,
				ekpo_revlv,
				ekko_ihrez,
				ekpo_ko_prctr,
				eket_wamng,
				ekpo_reslo,
				ekko_kdatb,
				ekko_kdate,
				ekpo_stapo,
				ekpo_txz01,
				eket_bnfpo,
				ekpo_banfn,
				ekpo_bukrs,
				ekko_knumv,
				ekpo_brtwr,
				ekpo_kzwi1,
				ekpo_kzwi2,
				ekpo_kzwi3,
				ekpo_kzwi4,
				ekpo_kzwi5,
				ekpo_kzwi6,
				ekpo_j_3aexfcm,
				ekpo_route,
				ekpo_tsbed,
				eket_j_3anetp,
				eket_j_3anetw,
				eket_j_4kscat,
				eket_j_3asize,
				ekpo_j_3asean,
				ekpo_j_3adat,
				ekpo_afs_collection,
				ekpo_afs_theme,
				eket_j_3aefda,
				eket_dabmg,
				ekko_frgke,
				eket_j_3aelikz,
				eket_j_3aetenr,
				ekpo_zzppv_rc,
				ekpo_zzppv_rct,
				ekko_lponr,
				ekpo_ltsnr,
				ekpo_zzprior,
				ekpo_uebtk,
				ekko_unsez,
				eket_j_3auanr,
				eket_j_3aupos,
				eket_j_3aetenv,
				ekko_exnum,
				eket_mbdat,
				eket_charg,
				eket_licha,
				ekpo_j_3aexfcp, /*End Changes 11 Jun 2015*/
        EKPO_EVERS
FROM ekko_ekpo_eket_ekbe a
WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);

/*  DELETE FROM ekko_ekpo_eket_ekbe */
UPDATE ekko_ekpo_eket SET EKPO_BWTAR = IFNULL(EKPO_BWTAR,'0'), EKPO_BUKRS = IFNULL(EKPO_BUKRS,EKKO_BUKRS);

INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',CURRENT_TIMESTAMP, 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;
  
INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact',CURRENT_TIMESTAMP, 'UPDATE ekko_ekpo_eket, getExchangeRate',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 2 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';  

/* Update 1 - split for each column */
UPDATE fact_purchase fpr
SET dd_PurchaseReqNo = ifnull(ifnull(dm_di_ds.EKET_BANFN, dm_di_ds.EKPO_BANFN),'Not Set')
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND  ifnull(dd_PurchaseReqNo,'xx') <> ifnull(ifnull(dm_di_ds.EKET_BANFN, dm_di_ds.EKPO_BANFN),'Not Set');

UPDATE fact_purchase fpr
SET dd_PurchaseReqItemNo = ifnull(ifnull(EKET_BNFPO, EKPO_BNFPO),0)
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_PurchaseReqItemNo,-1) <> ifnull(ifnull(EKET_BNFPO, EKPO_BNFPO),0);

UPDATE fact_purchase fpr
SET dd_VendorMaterialNo = ifnull(EKPO_IDNLF, '')
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_VendorMaterialNo,'xx') <> ifnull(EKPO_IDNLF, '');

UPDATE fact_purchase fpr
SET dd_inforecordno = ifnull(EKPO_INFNR,'Not Set')
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_inforecordno,'xx') <> ifnull(EKPO_INFNR,'yy');

UPDATE fact_purchase fpr
SET dd_bomexplosionno = ifnull(EKET_SERNR,'Not Set')
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_bomexplosionno,'xx') <> ifnull(EKET_SERNR,'yy');

UPDATE fact_purchase fpr
SET dd_packagingno = ifnull(EKPO_PACKNO,0)
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_packagingno,-1) <> ifnull(EKPO_PACKNO,-2);

UPDATE fact_purchase fpr
SET dd_incoterms2 = ifnull(ifnull(EKPO_INCO2,EKKO_INCO2),'Not Set')
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_incoterms2,'xx') <> ifnull(ifnull(EKPO_INCO2,EKKO_INCO2),'Not Set');

UPDATE fact_purchase fpr
SET dd_RevisionLevel = ifnull(EKPO_REVLV,'Not Set')
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(dd_RevisionLevel,'xx') <> ifnull(EKPO_REVLV,'Not Set');

UPDATE fact_purchase fpr
SET amt_SalesTax = 0
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_SalesTax is null;

UPDATE fact_purchase fpr
SET amt_invoice = 0
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_invoice is null;

UPDATE fact_purchase fpr
SET amt_Paid = 0
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_Paid is null;

merge into fact_purchase fact
using (select t1.fact_purchaseid, ifnull(t2.exchangeRate, 1) ct_ExchangeRate
	   from (select fact_purchaseid, 
					dm_di_ds.EKKO_WAERS, dm_di_ds.EKKO_WKURS, dm_di_ds.EKKO_BEDAT, 
					dc.Currency
		     from fact_purchase fpr
					inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
														 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
														 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
					inner join dim_company dc on dc.companycode = dm_di_ds.EKPO_BUKRS ) t1
				left join (select z.exchangeRate, z.pFromCurrency, z.pFromExchangeRate, z.pDate, z.pToCurrency 
					       from tmp_getExchangeRate1 z
						   where z.fact_script_name = 'bi_populate_purchasing_fact') t2
						on    t2.pFromCurrency  = t1.EKKO_WAERS 
						  AND t2.pFromExchangeRate = t1.EKKO_WKURS
						  AND t2.pDate = t1.EKKO_BEDAT
						  AND t2.pToCurrency = t1.Currency) src
on fact.fact_purchaseid = src.fact_purchaseid
when matched then update set fact.ct_ExchangeRate = src.ct_ExchangeRate
where fact.ct_ExchangeRate <> src.ct_ExchangeRate;

UPDATE fact_purchase fpr
SET	 ct_ExchangeRate = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND EKKO_WAERS = dc.Currency
AND ct_ExchangeRate is null;

/* ct_exchangerate is EKKO_WAERS->dc.currency */

UPDATE fact_purchase fpr
SET amt_ExchangeRate = ct_ExchangeRate
WHERE amt_ExchangeRate <> ct_ExchangeRate;
 
merge into fact_purchase fact
using (select t1.fact_purchaseid, ifnull(exchangeRate, 1) amt_ExchangeRate_GBL
	   from (select fpr.fact_purchaseid,
				    dm_di_ds.EKKO_WAERS
			 from fact_purchase fpr
					inner join ekko_ekpo_eket dm_di_ds ON    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
														 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
														 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR) t1
			left join (select z.exchangeRate, z.pFromCurrency
			           from tmp_getExchangeRate1 z cross join tmp_var_purchasing_fact v
					   where    z.pToCurrency = v.pGlobalCurrency
						    and z.pFromExchangeRate = 0
							and z.pDate = current_date
							and z.fact_script_name = 'bi_populate_purchasing_fact') t2
					  on t2.pFromCurrency = t1.EKKO_WAERS /* LK 12 Sep 2013: This was earlier dc.currency */
					  )  src
on fact.fact_purchaseid = src.fact_purchaseid
when matched then update set fact.amt_ExchangeRate_GBL = src.amt_ExchangeRate_GBL
where fact.amt_ExchangeRate_GBL <> src.amt_ExchangeRate_GBL;


UPDATE fact_purchase fpr
SET	 amt_ExchangeRate_GBL = 1
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,tmp_var_purchasing_fact, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND pGlobalCurrency = EKKO_WAERS
AND amt_ExchangeRate_GBL is null;
 
UPDATE fact_purchase fpr
SET ct_DeliveryQty =  ifnull((CASE WHEN EKKO_AUTLF IS NOT NULL OR EKKO_LOEKZ IS NOT NULL OR EKPO_ELIKZ IS NOT NULL OR EKPO_LOEKZ IS NOT NULL THEN EKET_WEMNG ELSE EKET_MENGE END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
FROM  ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_DeliveryQty <>  ifnull((CASE WHEN EKKO_AUTLF IS NOT NULL OR EKKO_LOEKZ IS NOT NULL OR EKPO_ELIKZ IS NOT NULL OR EKPO_LOEKZ IS NOT NULL THEN EKET_WEMNG ELSE EKET_MENGE END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END;

UPDATE fact_purchase fpr
SET ct_ReceivedQty = ifnull(EKET_WEMNG, 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_ReceivedQty <> ifnull(EKET_WEMNG, 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END;

UPDATE fact_purchase fpr
SET ct_ReturnQty = 0
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_ReturnQty is null;

UPDATE fact_purchase fpr
SET ct_RejectQty = 0
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_RejectQty is null;

UPDATE fact_purchase fpr
SET ct_GRProcessingTime = ifnull(EKPO_WEBAZ, 0)
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_GRProcessingTime <> ifnull(EKPO_WEBAZ, 0);

UPDATE fact_purchase fpr
SET ct_OverDeliveryTolerance = ifnull(EKPO_UEBTO, 0)
FROM  ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_OverDeliveryTolerance <> ifnull(EKPO_UEBTO, 0);

UPDATE fact_purchase fpr
SET ct_UnderDeliveryTolerance = ifnull(EKPO_UNTTO, 0)
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_UnderDeliveryTolerance <> ifnull(EKPO_UNTTO, 0);

/* 27 July 2017 - Roxana - add Committed Qty */

UPDATE fact_purchase fpr
SET ct_CommittedQty = ifnull(EKET_MNG02, 0)
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_CommittedQty <> ifnull(EKET_MNG02, 0) ;

/* end changes 27 July 2017 */

 
/* Sync Shanthi's changes  - 24 Jul */ 
UPDATE fact_purchase fpr
SET /*ct_BaseUOMQty = EKET_MENGE * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END*/
         ct_BaseUOMQty = ifnull((CASE WHEN  EKKO_AUTLF IS NOT NULL
                                        OR EKKO_LOEKZ IS NOT NULL
                                        OR EKPO_ELIKZ IS NOT NULL
                                        OR EKPO_LOEKZ IS NOT NULL
                                    THEN EKET_WEMNG
                                    ELSE EKET_MENGE
                                  END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
                        * (EKPO_UMREZ / case when EKPO_UMREN = 0 then 1 else EKPO_UMREN end)
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_BaseUOMQty <> ifnull((CASE WHEN  EKKO_AUTLF IS NOT NULL
                                        OR EKKO_LOEKZ IS NOT NULL
                                        OR EKPO_ELIKZ IS NOT NULL
                                        OR EKPO_LOEKZ IS NOT NULL
                                    THEN EKET_WEMNG
                                    ELSE EKET_MENGE
                                  END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
                        * (EKPO_UMREZ / case when EKPO_UMREN = 0 then 1 else EKPO_UMREN end);


UPDATE fact_purchase fpr
SET ct_BaseUOMQty = ifnull((CASE WHEN   EKKO_AUTLF IS NOT NULL
                                        OR EKKO_LOEKZ IS NOT NULL
                                        OR EKPO_ELIKZ IS NOT NULL
                                        OR EKPO_LOEKZ IS NOT NULL
                                    THEN EKET_WEMNG
                                    ELSE EKET_MENGE
                                  END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
		* ifnull(( i.EINA_UMREZ / case i.EINA_UMREN when 0 then 1 else i.EINA_UMREN end  ),1)
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,EINA i, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND i.INFNR = EKPO_INFNR AND i.EINA_UMREN <> 0
AND ct_BaseUOMQty <> ifnull((CASE WHEN   EKKO_AUTLF IS NOT NULL
                                        OR EKKO_LOEKZ IS NOT NULL
                                        OR EKPO_ELIKZ IS NOT NULL
                                        OR EKPO_LOEKZ IS NOT NULL
                                    THEN EKET_WEMNG
                                    ELSE EKET_MENGE
                                  END), 0) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
		* ifnull(( i.EINA_UMREZ / case i.EINA_UMREN when 0 then 1 else i.EINA_UMREN end  ),1);


UPDATE fact_purchase fpr
SET Dim_StorageLocationid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_StorageLocationid is null;

	drop table if exists tmp_updt_dim_StorageLocation;
	create table tmp_updt_dim_StorageLocation
	as
	select LocationCode,Plant,max(Dim_StorageLocationid) Dim_StorageLocationid from dim_StorageLocation group by LocationCode,Plant;
	

UPDATE fact_purchase fpr
SET Dim_StorageLocationid = sl.Dim_StorageLocationid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,tmp_updt_dim_StorageLocation sl, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND sl.LocationCode = EKPO_LGORT
 AND sl.Plant = EKPO_WERKS
AND fpr.Dim_StorageLocationid <> sl.Dim_StorageLocationid;

	drop table if exists tmp_updt_dim_StorageLocation;


UPDATE fact_purchase fpr
SET Dim_ItemStatusid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ItemStatusid is null;

UPDATE fact_purchase fpr
SET Dim_ItemStatusid = its.Dim_ItemStatusid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_ItemStatus its, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND its.Status = EKPO_STATUS
AND fpr.Dim_ItemStatusid <> its.Dim_ItemStatusid;


UPDATE fact_purchase fpr
SET Dim_DocumentStatusid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DocumentStatusid is null;

UPDATE fact_purchase fpr
SET Dim_DocumentStatusid = dst.Dim_DocumentStatusid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_DocumentStatus dst, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dst.Status = EKKO_STATU
AND fpr.Dim_DocumentStatusid <> dst.Dim_DocumentStatusid;

UPDATE fact_purchase fpr
SET Dim_DocumentTypeid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DocumentTypeid is null;

UPDATE fact_purchase fpr
SET Dim_DocumentTypeid = dtp.Dim_DocumentTypeid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_DocumentType dtp, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dtp.Type = EKKO_BSART AND dtp.Category = EKKO_BSTYP
AND fpr.Dim_DocumentTypeid <> dtp.Dim_DocumentTypeid;


/* Update Dim_UnitOfMeasureid */

UPDATE fact_purchase fpr
SET Dim_UnitOfMeasureid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_UnitOfMeasureid is null;

UPDATE fact_purchase fpr
SET Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_UnitOfMeasure uom, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND uom.UOM = EKPO_MEINS
AND fpr.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;


/* Update Dim_StockTypeid */

UPDATE fact_purchase fpr
SET Dim_StockTypeid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_StockTypeid is null;


UPDATE fact_purchase fpr
SET Dim_StockTypeid = st.Dim_StockTypeid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_StockType st, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND st.TypeCode = EKPO_INSMK
AND fpr.Dim_StockTypeid <> st.Dim_StockTypeid;

/* Update Dim_ConsumptionTypeid */

UPDATE fact_purchase fpr
SET Dim_ConsumptionTypeid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ConsumptionTypeid is null;


UPDATE fact_purchase fpr
SET Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_ConsumptionType ct, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND ct.ConsumptionCode = EKPO_KZVBR
AND fpr.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;

/* Update Dim_ItemCategoryid */

UPDATE fact_purchase fpr
SET Dim_ItemCategoryid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ItemCategoryid is null;


UPDATE fact_purchase fpr
SET Dim_ItemCategoryid = ic.Dim_ItemCategoryid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_ItemCategory ic, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND ic.CategoryCode = EKPO_PSTYP
AND fpr.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;

/* Update Dim_AccountCategoryid */

UPDATE fact_purchase fpr
SET Dim_AccountCategoryid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_AccountCategoryid is null;


UPDATE fact_purchase fpr
SET Dim_AccountCategoryid = ac.Dim_AccountCategoryid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_AccountCategory ac, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND ac.Category = EKPO_KNTTP
AND fpr.Dim_AccountCategoryid <> ac.Dim_AccountCategoryid;

/* Update Dim_IncoTerm1id */

UPDATE fact_purchase fpr
SET Dim_IncoTerm1id = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_IncoTerm1id is null;


UPDATE fact_purchase fpr
SET Dim_IncoTerm1id = it.Dim_IncoTermid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_IncoTerm it, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND it.IncoTermCode = ifnull(EKPO_INCO1,EKKO_INCO1)
AND fpr.Dim_IncoTerm1id <> it.Dim_IncoTermid;


/* Update Dim_Termid */

UPDATE fact_purchase fpr
SET Dim_Termid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_Termid is null;

/* Note that this was using LIMIT 1 without order by in mysql. Updating it like other columns here. It will update the first value of dtm.Dim_Termid it finds in Dim_Term */

UPDATE fact_purchase fpr
SET Dim_Termid = dtm.Dim_Termid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Term dtm, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dtm.TermCode = EKKO_ZTERM
AND fpr.Dim_Termid <> dtm.Dim_Termid;

/* Update Dim_DateidDelivery */

UPDATE fact_purchase fpr
SET Dim_DateidDelivery = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidDelivery is null;


UPDATE fact_purchase fpr
SET Dim_DateidDelivery = dsd.Dim_Dateid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dsd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dsd.DateValue = EKET_SLFDT AND dsd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidDelivery <> dsd.Dim_Dateid;


UPDATE fact_purchase fpr
SET Dim_DateidDelivery = dd.Dim_Dateid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = EKET_EINDT AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidDelivery <> dd.Dim_Dateid;


/* Update Dim_DateIdValidityEnd */

UPDATE fact_purchase fpr
SET Dim_DateIdValidityEnd = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateIdValidityEnd is null;


UPDATE fact_purchase fpr
SET Dim_DateIdValidityEnd = dd.Dim_Dateid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = EKKO_KDATE AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateIdValidityEnd <> dd.Dim_Dateid;

/* Update Dim_DateIdValidityStart */

UPDATE fact_purchase fpr
SET Dim_DateIdValidityStart = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateIdValidityStart is null;


UPDATE fact_purchase fpr
SET Dim_DateIdValidityStart = dd.Dim_Dateid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = EKKO_KDATB AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateIdValidityStart <> dd.Dim_Dateid;


/* Update Dim_DateidShip */

UPDATE fact_purchase fpr
SET Dim_DateidShip = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidShip is null;


UPDATE fact_purchase fpr
SET Dim_DateidShip = dsd.Dim_Dateid                                                             
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dsd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dsd.DateValue = ( EKET_SLFDT - cast(EKPO_PLIFZ as integer))  AND dsd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidShip <> dsd.Dim_Dateid;


UPDATE fact_purchase fpr
SET Dim_DateidShip = dd.Dim_Dateid                                                             
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = (EKET_EINDT - cast(EKPO_PLIFZ as integer)) AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidShip <> dd.Dim_Dateid;



/* Update Dim_DateidStatDelivery */

UPDATE fact_purchase fpr
SET Dim_DateidStatDelivery = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidStatDelivery is null;


UPDATE fact_purchase fpr
SET Dim_DateidStatDelivery = dsd.Dim_Dateid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dsd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dsd.DateValue = EKET_EINDT AND dsd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidStatDelivery <> dsd.Dim_Dateid;


UPDATE fact_purchase fpr
SET Dim_DateidStatDelivery = dd.Dim_Dateid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dd.DateValue = EKET_SLFDT AND dd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidStatDelivery <> dd.Dim_Dateid;

/* Update Dim_DateidInvoice */

UPDATE fact_purchase fpr
SET Dim_DateidInvoice = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidInvoice is null;

/* Update Dim_DateidPaid */

UPDATE fact_purchase fpr
SET Dim_DateidPaid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidPaid is null;


/* Update Dim_DateidPrice */

UPDATE fact_purchase fpr
SET Dim_DateidPrice = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidPrice is null;


UPDATE fact_purchase fpr
SET Dim_DateidPrice = dprd.Dim_Dateid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dprd, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dprd.DateValue = EKPO_PRDAT AND dprd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidPrice <> dprd.Dim_Dateid;

/* Update Dim_PurchaseGroupid */

UPDATE fact_purchase fpr
SET Dim_PurchaseGroupid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_PurchaseGroupid is null;


UPDATE fact_purchase fpr
SET Dim_PurchaseGroupid = pg.Dim_PurchaseGroupid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_PurchaseGroup pg, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND pg.PurchaseGroup = EKKO_EKGRP
AND fpr.Dim_PurchaseGroupid <> pg.Dim_PurchaseGroupid;

/* Update Dim_PurchaseOrgid */

UPDATE fact_purchase fpr
SET Dim_PurchaseOrgid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_PurchaseOrgid is null;


UPDATE fact_purchase fpr
SET Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_PurchaseOrg po, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND po.PurchaseOrgCode = EKKO_EKORG
AND fpr.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;




/* Update Dim_PlantidOrdering */

UPDATE fact_purchase fpr
SET Dim_PlantidOrdering = Dim_Plantid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_PlantidOrdering <> Dim_Plantid;


/* Update Dim_PlantidSupplying */

UPDATE fact_purchase fpr
SET Dim_PlantidSupplying = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_PlantidSupplying is null;


UPDATE fact_purchase fpr
SET Dim_PlantidSupplying = dps.Dim_Plantid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Plant dps, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dps.PlantCode = EKKO_RESWK
AND fpr.Dim_PlantidSupplying <> dps.Dim_Plantid;

/* Update Dim_Currencyid */

UPDATE fact_purchase fpr
SET Dim_Currencyid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_Currencyid is null;


UPDATE fact_purchase fpr
SET Dim_Currencyid = dcr.Dim_Currencyid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Currency dcr, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dcr.CurrencyCode = dc.currency
AND fpr.Dim_Currencyid <> dcr.Dim_Currencyid;

/* Update Dim_POCurrencyid */

UPDATE fact_purchase fpr
SET Dim_POCurrencyid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_POCurrencyid is null;


UPDATE fact_purchase fpr
SET Dim_POCurrencyid = dcr.Dim_Currencyid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Currency dcr, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dcr.CurrencyCode = EKKO_WAERS
AND fpr.Dim_POCurrencyid <> dcr.Dim_Currencyid;

/* Update Tran currency ( taken as PO currency ) */

UPDATE fact_purchase fpr
SET Dim_Currencyid_TRA = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_Currencyid_TRA is null;


UPDATE fact_purchase fpr
SET Dim_Currencyid_TRA = dcr.Dim_Currencyid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Currency dcr, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dcr.CurrencyCode = EKKO_WAERS
AND fpr.Dim_Currencyid_TRA <> dcr.Dim_Currencyid;

/* Update Global currency */

UPDATE fact_purchase fpr
SET Dim_Currencyid_GBL = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_Currencyid_GBL is null;


UPDATE fact_purchase fpr
SET Dim_Currencyid_GBL = dcr.Dim_Currencyid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Currency dcr,tmp_var_purchasing_fact, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dcr.CurrencyCode = pGlobalCurrency
AND fpr.Dim_Currencyid_GBL <> dcr.Dim_Currencyid;

/* Update Dim_ShipToAddressid */

UPDATE fact_purchase fpr
SET Dim_ShipToAddressid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_ShipToAddressid is null;

/* LK: This used LIMIT 1 without order by in mysql */
UPDATE fact_purchase fpr
SET Dim_ShipToAddressid = sta.Dim_Addressid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_Address sta, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  sta.AddressNumber = EKPO_ADRNR                     
AND fpr.Dim_ShipToAddressid <> sta.Dim_Addressid;

/* Update Dim_DelivAddressId */

UPDATE fact_purchase fpr
SET Dim_DelivAddressId = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DelivAddressId is null;

/* LIMIT 1 */
UPDATE fact_purchase fpr
SET Dim_DelivAddressId = sta.Dim_Addressid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_Address sta, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  sta.AddressNumber = EKPO_ADRN2                      
AND fpr.Dim_DelivAddressId <> sta.Dim_Addressid;

/* Update Dim_Partid */

UPDATE fact_purchase fpr
SET Dim_Partid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_Partid is null;


UPDATE fact_purchase fpr
SET Dim_Partid = dp.Dim_Partid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Part dp, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dp.PartNumber = EKPO_MATNR AND dp.Plant = EKPO_WERKS
AND fpr.Dim_Partid <> dp.Dim_Partid;

/* Update dim_producthierarchyid */

UPDATE fact_purchase fpr
SET dim_producthierarchyid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND dim_producthierarchyid is null;


UPDATE fact_purchase fpr
SET dim_producthierarchyid = dph.dim_producthierarchyid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_producthierarchy dph, Dim_Part p, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND      dph.ProductHierarchy = p.ProductHierarchy AND p.PartNumber = EKPO_MATNR AND p.Plant = EKPO_WERKS
AND fpr.dim_producthierarchyid <> dph.dim_producthierarchyid;

/* Update Dim_MaterialGroupid */

UPDATE fact_purchase fpr
SET Dim_MaterialGroupid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_MaterialGroupid is null;


UPDATE fact_purchase fpr
SET Dim_MaterialGroupid = mg.Dim_MaterialGroupid
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_MaterialGroup mg, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  mg.MaterialGroupCode = EKPO_MATKL
AND fpr.Dim_MaterialGroupid <> mg.Dim_MaterialGroupid;

/* Update Dim_DateIdSchedOrder */

UPDATE fact_purchase fpr
SET Dim_DateIdSchedOrder = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateIdSchedOrder is null;


UPDATE fact_purchase fpr
SET Dim_DateIdSchedOrder = dc0.Dim_Dateid
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dc0, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dc0.DateValue = EKET_BEDAT AND dc0.CompanyCode = EKPO_BUKRS
AND fpr.Dim_DateIdSchedOrder <> dc0.Dim_Dateid;


/* Update dd_ConfirmEdi */

UPDATE fact_purchase fpr
SET dd_ConfirmEdi = ifnull(cast(EKKO_ABSGR as varchar(10)),'Not Set')
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_ConfirmEdi <> ifnull(cast(EKKO_ABSGR as varchar(10)),'Not Set');

/* Update ct_FirmZone */

UPDATE fact_purchase fpr
SET ct_FirmZone = ifnull(EKPO_ETFZ1,0)
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_FirmZone <> ifnull(EKPO_ETFZ1,0);


/* Update Dim_MSZoneId */

UPDATE fact_purchase fpr
SET Dim_MSZoneId = 1
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_MSZoneId is null;

/* LIMIT 1 */

UPDATE fact_purchase fpr
SET Dim_MSZoneId = ms.Dim_MaterialStagingZoneId
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_MaterialStagingZone ms, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  ms.StagingArea = EKPO_LGBZO
AND fpr.Dim_MSZoneId <> ms.Dim_MaterialStagingZoneId;


/* Update dd_ReferenceNo */

UPDATE fact_purchase fpr
SET dd_ReferenceNo = ifnull(EKKO_IHREZ,'Not Set')
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_ReferenceNo <> ifnull(EKKO_IHREZ,'Not Set');

/* Update ct_FirmZone */

UPDATE fact_purchase fpr
SET ct_QtyIssued = ifnull(EKET_WAMNG,0)
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_QtyIssued <> ifnull(EKET_WAMNG,0);

/* Update Dim_ProfitCenterid */
/* Note that ifnull and subquery are required in this query. As it involves updating the first record from tmp table can't use that in join */

drop table if exists tmp_dim_pc_purchasing_fact;
create table tmp_dim_pc_purchasing_fact 
as select * from dim_profitcenter order by ValidTo desc;

UPDATE fact_purchase fpr
SET fpr.dim_profitcenterid = ifnull(pc.Dim_ProfitCenterid, 1)	
from fact_purchase fpr
		inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
											 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
											 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
		left join tmp_dim_pc_purchasing_fact pc on pc.ProfitCenterCode = dm_di_ds.EKPO_KO_PRCTR
where fpr.dim_profitcenterid <> ifnull(pc.Dim_ProfitCenterid, 1);

/* Update Dim_IssuStorageLocid */

UPDATE fact_purchase fpr
SET Dim_IssuStorageLocid = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_IssuStorageLocid is null;


UPDATE fact_purchase fpr
SET Dim_IssuStorageLocid = sl.Dim_StorageLocationid
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,dim_StorageLocation sl, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND sl.LocationCode = EKPO_RESLO AND sl.Plant = EKKO_RESWK
AND fpr.Dim_IssuStorageLocid <> sl.Dim_StorageLocationid;

/* Update dd_ShortText */

UPDATE fact_purchase fpr
SET dd_ShortText = ifnull(EKPO_TXZ01,'Not Set')
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND  dd_ShortText <> ifnull(EKPO_TXZ01,'Not Set');

/* Update Dim_DateidCreate */

UPDATE fact_purchase fpr
SET Dim_DateidCreate = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidCreate is null;


UPDATE fact_purchase fpr
SET Dim_DateidCreate = dc0.Dim_Dateid
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dc0, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dc0.DateValue = EKKO_AEDAT AND dc0.CompanyCode = EKPO_BUKRS
AND fpr.Dim_DateidCreate <> dc0.Dim_Dateid;

/* Update Dim_DateidOrder */

UPDATE fact_purchase fpr
SET Dim_DateidOrder = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidOrder is null;


UPDATE fact_purchase fpr
SET Dim_DateidOrder = dc0.Dim_Dateid
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dc0, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dc0.DateValue = EKKO_BEDAT AND dc0.CompanyCode = EKPO_BUKRS
AND fpr.Dim_DateidOrder <> dc0.Dim_Dateid;

/* Update Dim_DateidItemCreate */

UPDATE fact_purchase fpr
SET Dim_DateidItemCreate = 1
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND Dim_DateidItemCreate is null;


UPDATE fact_purchase fpr
SET Dim_DateidItemCreate = dcrd.Dim_Dateid
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dcrd, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND dcrd.DateValue = EKPO_AEDAT AND dcrd.CompanyCode = pl.CompanyCode
AND fpr.Dim_DateidItemCreate <> dcrd.Dim_Dateid;


/* Update amt_GrossOrderValue */
UPDATE fact_purchase fpr
SET amt_GrossOrderValue = ifnull(EKPO_BRTWR, 0)
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_GrossOrderValue <> ifnull(EKPO_BRTWR, 0);

/* Update ct_ItemQty */
UPDATE fact_purchase fpr
SET ct_ItemQty = ct_DeliveryQty -- EKPO_MENGE
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_ItemQty <> ct_DeliveryQty;

/* Update amt_ItemNetValue */
UPDATE fact_purchase fpr
SET amt_ItemNetValue = EKPO_NETWR
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_ItemNetValue <> EKPO_NETWR;

/* Update amt_PricingSbt1 */
UPDATE fact_purchase fpr
SET amt_PricingSbt1 = ifnull(EKPO_KZWI1, 0)
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt1 <> ifnull(EKPO_KZWI1, 0);

/* Update amt_PricingSbt2 */
UPDATE fact_purchase fpr
SET amt_PricingSbt2 = EKPO_KZWI2
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt2 <> EKPO_KZWI2;

/* Update amt_PricingSbt3 */
UPDATE fact_purchase fpr
SET amt_PricingSbt3 = EKPO_KZWI3
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt3 <> EKPO_KZWI3;

/* Update amt_PricingSbt4 */
UPDATE fact_purchase fpr
SET amt_PricingSbt4 = EKPO_KZWI4
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt4 <> EKPO_KZWI4;

/* Update amt_PricingSbt5 */
UPDATE fact_purchase fpr
SET amt_PricingSbt5 = EKPO_KZWI5
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt5 <> EKPO_KZWI5;

/* Update amt_PricingSbt6 */
UPDATE fact_purchase fpr
SET amt_PricingSbt6 = EKPO_KZWI6
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_PricingSbt6 <> EKPO_KZWI6;

/* Update fpr.Dim_CompanyId */
UPDATE fact_purchase fpr
SET fpr.Dim_CompanyId = dc.dim_companyid
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND fpr.Dim_CompanyId <> dc.dim_companyid;


/* Update fpr.QtyConversion_EqualTo and 3 other fields - Mircea changes 24 Jul */
UPDATE fact_purchase fpr
SET fpr.QtyConversion_EqualTo = EKPO_BPUMZ
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(QtyConversion_EqualTo,-1) <> EKPO_BPUMZ;

UPDATE fact_purchase fpr
SET fpr.QtyConversion_Denom = EKPO_BPUMN
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(QtyConversion_Denom,-1) <> EKPO_BPUMN;

UPDATE fact_purchase fpr
SET fpr.PriceConversion_EqualTo = EKPO_UMREZ
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(PriceConversion_EqualTo,-1) <> EKPO_UMREZ;

UPDATE fact_purchase fpr
SET fpr.PriceConversion_Denom = EKPO_UMREN
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ifnull(PriceConversion_Denom,-1) <> EKPO_UMREN;

/*   Begin 05 Nov 2013 */
UPDATE fact_purchase fpr
SET dim_ReleaseIndicatorId =  1
WHERE dim_ReleaseIndicatorId is null;

UPDATE fact_purchase fpr
SET dim_ReleaseIndicatorId = d.dim_ReleaseIndicatorId
FROM     ekko_ekpo_eket e, dim_ReleaseIndicator d, fact_purchase fpr
WHERE  fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_scheduleno = EKET_ETENR
AND ReleaseIndicatorCode = ifnull(EKKO_FRGKE, 'Not Set')
AND fpr.dim_ReleaseIndicatorId <> d.dim_ReleaseIndicatorId;

/*   End 05 Nov 2013 */	

/*   Begin 14 Nov 2013 */
/*dd_DeliveryNumber*/
DROP TABLE IF EXISTS tmp_upd_fpr_ekes_maxeten;
CREATE TABLE tmp_upd_fpr_ekes_maxeten
AS
SELECT DISTINCT EKPO_EBELN,EKPO_EBELP,
			    FIRST_VALUE(EKES_VBLEN) OVER(PARTITION BY EKPO_EBELN,EKPO_EBELP ORDER BY EKES_ETENS DESC) EKES_VBLEN,
				FIRST_VALUE(EKES_XBLNR) OVER(PARTITION BY EKPO_EBELN,EKPO_EBELP ORDER BY EKES_ETENS DESC) EKES_XBLNR
FROM ekko_ekpo_ekes;

UPDATE fact_purchase fpr
SET dd_DeliveryNumber =  'Not Set'
WHERE dd_DeliveryNumber is null;

UPDATE fact_purchase fpr
SET dd_DeliveryNumber = ifnull(EKES_VBLEN, 'Not Set')
FROM tmp_upd_fpr_ekes_maxeten e, fact_purchase fpr
WHERE  fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND dd_DeliveryNumber <> ifnull(EKES_VBLEN, 'Not Set');

/*dd_POCreatedBy*/
UPDATE fact_purchase fpr
SET dd_POCreatedBy =  'Not Set'
WHERE dd_POCreatedBy is null;

UPDATE fact_purchase fpr
SET dd_POCreatedBy = ifnull(EKKO_ERNAM, 'Not Set')
FROM     ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_POCreatedBy <> ifnull(EKKO_ERNAM, 'Not Set');

/*dim_ValuationTypeId*/
UPDATE fact_purchase fpr
SET dim_ValuationTypeId = 1
FROM     ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND dim_ValuationTypeId is null;

UPDATE fact_purchase fpr
SET dim_ValuationTypeId = dvt.dim_ValuationTypeId
FROM   ekko_ekpo_eket e, dim_ValuationType dvt, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND    dvt.ValuationType = ifnull(EKPO_BWTAR, 'Not Set')
AND fpr.dim_ValuationTypeId <> dvt.dim_ValuationTypeId;

/*amt_POEXRate*/
UPDATE fact_purchase fpr
SET amt_POEXRate = ifnull(EKKO_WKURS, 0)
FROM ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_POEXRate <> ifnull(EKKO_WKURS, 0);

/*   End 14 Nov 2013 */	

/*   Begin 10 Dec 2013 */	
UPDATE fact_purchase fpr
SET dd_DeletionIndicator = ifnull(EKPO_LOEKZ, 'Not Set')
FROM     ekko_ekpo_eket dm_di_ds, fact_purchase fpr
WHERE fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND ifnull(dd_DeletionIndicator,'-1') <> ifnull(EKPO_LOEKZ, 'Not Set');
/*   End 10 Dec 2013 */	

/*   Begin 14 Apr 2014 : dd_OurReference */
UPDATE fact_purchase fpr
SET dd_OurReference =  'Not Set'
WHERE dd_OurReference is null;

UPDATE fact_purchase fpr
SET dd_OurReference = ifnull(EKKO_UNSEZ, 'Not Set')
FROM     ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_OurReference <> ifnull(EKKO_UNSEZ, 'Not Set');
/*   End 14 Apr 2014: dd_OurReference */



/* Update dim_DateIdInvoiceDay */
/* So in inoviceday update queries, there are no joins on schedule no and bukrs */
/* Using max DateValue */

DROP TABLE IF EXISTS tmp_fact_purchase_dim_DateIdInvoiceDay;
CREATE TABLE tmp_fact_purchase_dim_DateIdInvoiceDay
AS
SELECT EKBE_WERKS,EKBE_EBELN,EKBE_EBELP,max(EKBE_CPUDT) max_EKBE_CPUDT
FROM ekbe_purch
GROUP BY  EKBE_WERKS,EKBE_EBELN,EKBE_EBELP;


UPDATE fact_purchase fpr
SET dim_DateIdInvoiceDay = dd.Dim_Dateid
FROM     tmp_fact_purchase_dim_DateIdInvoiceDay dm_di_ds, dim_plant pl, Dim_Date dd, fact_purchase fpr
WHERE     pl.PlantCode = EKBE_WERKS
AND fpr.Dim_PlantidOrdering = pl.dim_plantid
 AND fpr.dd_DocumentNo = EKBE_EBELN
 AND fpr.dd_DocumentItemNo = EKBE_EBELP
 AND dd.DateValue = max_EKBE_CPUDT AND dd.CompanyCode = pl.CompanyCode
AND fpr.dim_DateIdInvoiceDay <> dd.Dim_Dateid;


/* dd_PoLineNBR */
UPDATE fact_purchase fpr
SET dd_PoLineNBR = ifnull(EKKO_LPONR,0)
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_PoLineNBR <> ifnull(EKKO_LPONR,0);


/* dd_PurchFrom */

UPDATE fact_purchase fpr
SET dd_PurchFrom = ifnull(EKPO_LTSNR,'Not Set')
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_PurchFrom <> ifnull(EKPO_LTSNR,'Not Set');

/* dd_CatalogLeadTime */

UPDATE fact_purchase fpr
SET ct_CatalogLeadTime = ifnull(EKPO_PLIFZ,0)
FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_CatalogLeadTime <> ifnull(EKPO_PLIFZ,0);


/* Update 1 - Ends */

/* Insert 1 - Starts */ 

drop table if exists fact_purchase_tmp_forinsert;
create table fact_purchase_tmp_forinsert
as (
SELECT 	EKPO_EBELN dd_DocumentNo,
		EKPO_EBELP dd_DocumentItemNo,
		EKET_ETENR dd_ScheduleNo,
		ifnull(ifnull(dm_di_ds.EKET_BANFN, dm_di_ds.EKPO_BANFN),'Not Set') dd_PurchaseReqNo,
		ifnull(ifnull(EKET_BNFPO, EKPO_BNFPO),0) dd_PurchaseReqItemNo,
		ifnull(EKPO_IDNLF, 'Not Set') dd_VendorMaterialNo,
		ifnull(EKPO_INFNR,'Not Set') dd_inforecordno,
		ifnull(convert(varchar(10), EKKO_ABSGR),'Not Set') dd_ConfirmEdi,
		ifnull(ifnull(EKPO_INCO2,EKKO_INCO2),'Not Set') dd_incoterms2,
		convert(decimal (18,4), 0) amt_SalesTax ,
		convert(decimal (18,4), 0) amt_invoice,
		convert(decimal (18,4), 0) amt_Paid,
		/* CASE WHEN EKKO_WAERS = dc.Currency THEN 1.00 ELSE getExchangeRate(EKKO_WAERS, dc.Currency, EKKO_WKURS, EKKO_BEDAT) END,
		   CASE WHEN dc.Currency = t1row.pGlobalCurrency THEN 1.00 ELSE getExchangeRate(dc.Currency, t1row.pGlobalCurrency, 0,EKKO_BEDAT) END, */
		CASE WHEN EKKO_WAERS = dc.Currency THEN 1.00 ELSE -5 END ct_ExchangeRate,
		convert(decimal (18,4), 1) as amt_ExchangeRate_GBL, 	-- CASE WHEN EKKO_WAERS = t1row.pGlobalCurrency THEN 1.00 ELSE -5 END amt_ExchangeRate_GBL, -- t1row = tmp_var_purchasing_fact
		( CASE WHEN EKKO_AUTLF IS NOT NULL OR EKKO_LOEKZ IS NOT NULL OR EKPO_ELIKZ IS NOT NULL OR EKPO_LOEKZ IS NOT NULL THEN EKET_WEMNG ELSE EKET_MENGE END) * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END ct_DeliveryQty,
		EKET_WEMNG * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END ct_ReceivedQty,
		convert(decimal (18,4), 0) ct_ReturnQty,
		convert(decimal (18,4), 0) ct_RejectQty,
		EKPO_WEBAZ ct_GRProcessingTime, 				-- used in ct_TotalLeadTime processing
		EKPO_UEBTO ct_OverDeliveryTolerance,
		EKPO_UNTTO ct_UnderDeliveryTolerance,
		/* getLeadTime(EKPO_MATNR,EKPO_WERKS,EKKO_LIFNR,EKPO_EBELN,EKPO_EBELP,'PO') leadTime,*/
		-99999 ct_leadtime,
		/* ifnull(datediff(EKET_EINDT, (select min(x.EKET_BEDAT) from ekko_ekpo_eket x where x.EKPO_EBELN = dm_di_ds.EKPO_EBELN and x.EKPO_EBELP = dm_di_ds.EKPO_EBELP)), 0) -
		   ifnull((getLeadTime(EKPO_MATNR, EKPO_WERKS, EKKO_LIFNR, EKPO_EBELN, EKPO_EBELP, 'PO')), 0) leadTime_Variance, */
		-99999 ct_LeadTimeVariance,
		convert(decimal (18,4), 0) as ct_baseuomqty, -- (CASE WHEN EKKO_AUTLF IS NOT NULL OR EKKO_LOEKZ IS NOT NULL OR EKPO_ELIKZ IS NOT NULL OR EKPO_LOEKZ IS NOT NULL THEN EKET_WEMNG ELSE EKET_MENGE END) * 
								-- CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END * 
								-- ifnull((SELECT i.EINA_UMREZ / case i.EINA_UMREN when 0 then 1 else i.EINA_UMREN end FROM EINA i WHERE i.INFNR = EKPO_INFNR), 1) ct_baseuomqty,
		convert(bigint, 1) as Dim_StorageLocationid, -- ifnull((SELECT Dim_StorageLocationid FROM dim_StorageLocation sl WHERE sl.LocationCode = EKPO_LGORT AND sl.Plant = EKPO_WERKS),1) Dim_StorageLocationid,
		convert(bigint, 1) as Dim_ShipToAddressid, 	-- ifnull((SELECT min(Dim_Addressid) FROM Dim_Address sta WHERE sta.AddressNumber = EKPO_ADRNR), 1) Dim_ShipToAddressid,
		convert(bigint, 1) as Dim_DelivAddressid, 	-- ifnull((SELECT min(Dim_Addressid) FROM dim_Address sta WHERE sta.AddressNumber = EKPO_ADRN2), 1) Dim_DelivAddressid,
		convert(bigint, 1) as Dim_ItemStatusid, 		-- ifnull((SELECT Dim_ItemStatusid FROM Dim_ItemStatus its WHERE its.Status = EKPO_STATUS), 1) Dim_ItemStatusid,
		convert(bigint, 1) as Dim_DocumentStatusid, 	-- ifnull((SELECT Dim_DocumentStatusid FROM Dim_DocumentStatus dst WHERE dst.Status = EKKO_STATU), 1) Dim_DocumentStatusid, 
		convert(bigint, 1) as Dim_DocumentTypeid, 	-- ifnull((SELECT Dim_DocumentTypeid FROM Dim_DocumentType dtp WHERE dtp.Type = EKKO_BSART AND dtp.Category = EKKO_BSTYP), 1) Dim_DocumentTypeid,
		convert(bigint, 1) as Dim_UnitOfMeasureid, 	-- ifnull((SELECT Dim_UnitOfMeasureid FROM Dim_UnitOfMeasure uom WHERE uom.UOM = EKPO_MEINS), 1) Dim_UnitOfMeasureid, 
		convert(bigint, 1) as Dim_StockTypeid, 		-- ifnull((SELECT Dim_StockTypeid FROM Dim_StockType st WHERE st.TypeCode = EKPO_INSMK), 1) Dim_StockTypeid, 
		convert(bigint, 1) as Dim_ConsumptionTypeid, -- ifnull((SELECT Dim_ConsumptionTypeid FROM Dim_ConsumptionType ct WHERE ct.ConsumptionCode = EKPO_KZVBR),1) Dim_ConsumptionTypeid,
		convert(bigint, 1) as Dim_ItemCategoryid, 	-- ifnull((SELECT Dim_ItemCategoryid FROM Dim_ItemCategory ic WHERE ic.CategoryCode = EKPO_PSTYP), 1) Dim_ItemCategoryid, 
		convert(bigint, 1) as Dim_AccountCategoryid, -- ifnull((SELECT Dim_AccountCategoryid FROM Dim_AccountCategory ac WHERE ac.Category = EKPO_KNTTP), 1) Dim_AccountCategoryid,
		convert(bigint, 1) as Dim_IncoTerm1id, 		-- ifnull((SELECT Dim_IncoTermid FROM Dim_IncoTerm it WHERE it.IncoTermCode = ifnull(EKPO_INCO1,EKKO_INCO1)), 1) Dim_IncoTerm1id, 
		convert(bigint, 1) as Dim_Termid, 			-- ifnull((SELECT min(Dim_Termid) FROM Dim_Term dtm WHERE dtm.TermCode = EKKO_ZTERM ), 1) Dim_Termid, 
		convert(bigint, 1) as Dim_Vendorid, 		-- ifnull((select v.Dim_Vendorid from dim_vendor v where v.VendorNumber = EKKO_LIFNR),1) Dim_Vendorid, 
		convert(bigint, 1) as Dim_DateidOrder,		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dcrd WHERE dcrd.DateValue = EKKO_BEDAT AND dcrd.CompanyCode = pl.CompanyCode), 1) Dim_DateidOrder,
		convert(bigint, 1) as Dim_DateidCreate, 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = EKKO_AEDAT AND dd.CompanyCode = pl.CompanyCode), 1) Dim_DateidCreate,
		convert(bigint, 1) as Dim_DateidDelivery,	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = EKET_EINDT AND dd.CompanyCode = pl.CompanyCode), 
														--	ifnull((SELECT Dim_Dateid FROM Dim_Date dsd WHERE dsd.DateValue = EKET_SLFDT AND dsd.CompanyCode = pl.CompanyCode),1)) Dim_DateidDelivery,
		convert(bigint, 1) as Dim_DateidStatDelivery, 	--	ifnull((SELECT Dim_Dateid FROM Dim_Date dsd WHERE dsd.DateValue = EKET_SLFDT AND dsd.CompanyCode = pl.CompanyCode), 
															-- ifnull((SELECT Dim_Dateid FROM Dim_Date dsd WHERE dsd.DateValue = EKET_EINDT AND dsd.CompanyCode = pl.CompanyCode),1)) Dim_DateidStatDelivery,
		convert(bigint, 1) as Dim_DateidShip, 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ( EKET_EINDT - int(EKPO_PLIFZ) ) AND dd.CompanyCode = pl.CompanyCode),
														-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = (EKET_SLFDT - int(EKPO_PLIFZ) ) AND dd.CompanyCode = pl.CompanyCode),1)) Dim_DateidShip,
		convert(bigint, 1) as Dim_DateidItemCreate, 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dcrd WHERE dcrd.DateValue = EKPO_AEDAT AND dcrd.CompanyCode = pl.CompanyCode),1) Dim_DateidItemCreate,
		convert(bigint, 1) Dim_DateidInvoice,
		convert(bigint, 1) Dim_DateidPaid,
		convert(bigint, 1) as Dim_DateidPrice, 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dprd WHERE dprd.DateValue = EKPO_PRDAT AND dprd.CompanyCode = pl.CompanyCode), 1) Dim_DateidPrice,
		convert(bigint, 1) as Dim_DateIdValidityEnd, -- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = EKKO_KDATE AND dd.CompanyCode = pl.CompanyCode), 1) Dim_DateIdValidityEnd, 
		convert(bigint, 1) as Dim_DateIdValidityStart, -- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = EKKO_KDATB AND dd.CompanyCode = pl.CompanyCode), 1) Dim_DateIdValidityStart,
		convert(bigint, 1) as Dim_Partid, 			-- ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber = EKPO_MATNR AND dp.Plant = EKPO_WERKS), 1) Dim_Partid,
		convert(bigint, 1) as Dim_PurchaseGroupid, 	-- ifnull((SELECT Dim_PurchaseGroupid FROM Dim_PurchaseGroup pg WHERE pg.PurchaseGroup = EKKO_EKGRP), 1) Dim_PurchaseGroupid,
		convert(bigint, 1) as Dim_PurchaseOrgid, 	-- ifnull((SELECT Dim_PurchaseOrgid FROM Dim_PurchaseOrg po WHERE po.PurchaseOrgCode = EKKO_EKORG), 1) Dim_PurchaseOrgid,
		Dim_Plantid Dim_PlantidOrdering,
		convert(bigint, 1) as Dim_PlantidSupplying, 	-- ifnull((SELECT Dim_Plantid FROM Dim_Plant dps WHERE dps.PlantCode = EKKO_RESWK), 1) Dim_PlantidSupplying,
		convert(bigint, 1) as Dim_Currencyid, 		-- ifnull((SELECT Dim_Currencyid FROM Dim_Currency dcr WHERE dcr.CurrencyCode = dc.currency), 1) Dim_Currencyid,
		convert(bigint, 1) as Dim_POCurrencyid, 		-- ifnull((SELECT Dim_Currencyid FROM Dim_Currency dcr WHERE dcr.CurrencyCode = EKKO_WAERS), 1) Dim_POCurrencyid,
		convert(bigint, 1) Dim_PurchaseMiscid,
		convert(bigint, 1) as dim_producthierarchyid, -- ifnull((SELECT dim_producthierarchyid FROM dim_producthierarchy dph, Dim_Part p 
														-- WHERE dph.ProductHierarchy = p.ProductHierarchy AND p.PartNumber = EKPO_MATNR AND p.Plant = EKPO_WERKS AND dph.RowIsCurrent = 1),1) dim_producthierarchyid,
		convert(bigint, 1) as Dim_MaterialGroupid, 	-- ifnull((SELECT Dim_MaterialGroupid FROM Dim_MaterialGroup mg WHERE mg.MaterialGroupCode = EKPO_MATKL), 1) Dim_MaterialGroupid, 
		ifnull(EKET_SERNR,'Not Set') dd_bomexplosionno, 
		ifnull(EKPO_PACKNO,0) dd_packagingno, 
		convert(bigint, 1) as Dim_DateIdSchedOrder, 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dcrd WHERE dcrd.DateValue = EKET_BEDAT AND dcrd.CompanyCode = pl.CompanyCode),1) Dim_DateIdSchedOrder,
		ifnull(EKPO_ETFZ1,0) ct_FirmZone,
		convert(bigint, 1) as Dim_MSZoneId, 			-- ifnull(( SELECT min(Dim_MaterialStagingZoneId) FROM Dim_MaterialStagingZone ms WHERE ms.StagingArea = EKPO_LGBZO),1) Dim_MSZoneId, 
		convert(decimal (18,4), 0) as ct_TotalLeadTime, 	-- ifnull((SELECT ( LeadTime + EKPO_WEBAZ + ProcessingTime) FROM dim_Part pt WHERE pt.PartNumber = dm_di_ds.EKPO_MATNR AND pt.Plant = dm_di_ds.EKPO_WERKS),0) ct_TotalLeadTime,
		convert(decimal (18,4), 0) as ct_TotalRLTime, 		-- ifnull((SELECT TotalReplenishmentLeadTime FROM dim_Part pt WHERE pt.PartNumber = dm_di_ds.EKPO_MATNR AND pt.Plant = dm_di_ds.EKPO_WERKS),0) ct_TotalRLTime,
		ifnull(EKPO_REVLV,'Not Set') dd_RevisionLevel, 
		ifnull(EKKO_IHREZ,'Not Set') dd_ReferenceNo, 
		ifnull(EKET_WAMNG,0) ct_QtyIssued, 
		/* ifnull((select pc.Dim_ProfitCenterid from dim_profitcenter pc where pc.ProfitCenterCode = EKPO_KO_PRCTR order by pc.ValidTo desc limit 1),1) Dim_ProfitCenterid,*/
		convert(bigint, -1) Dim_ProfitCenterid, 
		convert(bigint, 1) as Dim_IssuStorageLocid, -- ifnull((SELECT Dim_StorageLocationid FROM dim_StorageLocation sl WHERE sl.LocationCode = EKPO_RESLO AND sl.Plant = EKKO_RESWK),1) Dim_IssuStorageLocid,
		ifnull(EKPO_TXZ01,'Not Set') dd_ShortText, 
		ifnull(EKPO_BRTWR, 0) amt_GrossOrderValue,
		ifnull(EKET_MENGE,0) ct_ItemQty,
		ifnull(EKPO_NETWR,0) amt_ItemNetValue,
		ifnull(EKPO_KZWI1, 0) amt_PricingSbt1,
		ifnull(EKPO_KZWI2, 0) amt_PricingSbt2,
		ifnull(EKPO_KZWI3, 0) amt_PricingSbt3,
		ifnull(EKPO_KZWI4, 0) amt_PricingSbt4,
		ifnull(EKPO_KZWI5, 0) amt_PricingSbt5,
		ifnull(EKPO_KZWI6, 0) amt_PricingSbt6,
		dc.dim_companyid Dim_CompanyId,
		convert(bigint, 1) dim_ReleaseIndicatorId,
		convert(bigint, 1) Dim_ValuationClassId,
		'Not Set' dd_DeliveryNumber,
		'Not Set' dd_POCreatedBy,
		convert(bigint, 1) dim_ValuationTypeId,
		convert(decimal (18,4), 0) amt_POEXRate,
		ifnull(EKPO_LOEKZ, 'Not Set') dd_DeletionIndicator,
		ifnull(EKKO_UNSEZ, 'Not Set') dd_OurReference,
		ifnull(EKPO_BPUMZ,0) QtyConversion_EqualTo,
		ifnull(EKPO_BPUMN,0) QtyConversion_Denom,
		ifnull(EKPO_UMREZ,0) PriceConversion_EqualTo,
		ifnull(EKPO_UMREN,0) PriceConversion_Denom,
		(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_purchase')+ row_number() over (order by '')  fact_purchaseid,
		convert(bigint, 1) as Dim_Currencyid_TRA, -- ifnull((SELECT Dim_Currencyid FROM Dim_Currency dcr WHERE dcr.CurrencyCode = EKKO_WAERS), 1) Dim_Currencyid_TRA, 
		convert(bigint, 1) as Dim_Currencyid_GBL, -- ifnull((SELECT Dim_Currencyid FROM Dim_Currency dcr WHERE dcr.CurrencyCode = t1row.pGlobalCurrency ), 1) Dim_Currencyid_GBL , -- t1row = tmp_var_purchasing_fact
		CASE WHEN EKKO_WAERS = dc.Currency THEN 1.00 ELSE -5 END amt_ExchangeRate,
		convert(bigint, 1) dim_DateIdInvoiceDay,
		ifnull(EKKO_LPONR,0) dd_PoLineNBR,
		ifnull(EKPO_LTSNR,'Not Set') dd_PurchFrom,
		ifnull(EKPO_PLIFZ,0) ct_CatalogLeadTime, -- used in Dim_DateidShip
		1 dirtyrow,
		/* Exasol migration: use next columns for updates */
		EKKO_WAERS,												-- for amt_ExchangeRate_GBL, Dim_POCurrencyid, Dim_Currencyid_TRA
		EKKO_AUTLF, EKKO_LOEKZ, EKPO_ELIKZ, EKPO_LOEKZ, 
			EKET_WEMNG, EKET_MENGE, EKPO_RETPO, EKPO_INFNR, 	-- for ct_baseuomqty
		EKPO_LGORT, EKPO_WERKS,									-- for Dim_StorageLocationid
		EKPO_ADRNR,												-- for Dim_ShipToAddressid
		EKPO_ADRN2,												-- for Dim_DelivAddressid
		EKPO_STATUS,											-- for Dim_ItemStatusid
		EKKO_STATU,												-- for Dim_DocumentStatusid
		EKKO_BSART, EKKO_BSTYP,									-- for Dim_DocumentTypeid
		EKPO_MEINS,												-- for Dim_UnitOfMeasureid
		EKPO_INSMK,												-- for Dim_StockTypeid
		EKPO_KZVBR,												-- for Dim_ConsumptionTypeid
		EKPO_PSTYP,												-- for Dim_ItemCategoryid
		EKPO_KNTTP,												-- for Dim_AccountCategoryid
		EKPO_INCO1, EKKO_INCO1,									-- for Dim_IncoTerm1id
		EKKO_ZTERM,												-- for Dim_Termid
		EKKO_LIFNR,												-- for Dim_Vendorid
		EKKO_BEDAT,												-- for Dim_DateidOrder
		EKKO_AEDAT,												-- for Dim_DateidCreate
		EKET_EINDT, EKET_SLFDT,									-- for Dim_DateidDelivery, Dim_DateidStatDelivery
		EKPO_AEDAT,												-- for Dim_DateidItemCreate
		EKPO_PRDAT,												-- for Dim_DateidPrice
		EKKO_KDATE,												-- for Dim_DateIdValidityEnd
		EKKO_KDATB,												-- for Dim_DateIdValidityStart
		EKPO_MATNR,												-- for Dim_PartId, dim_producthierarchyid, ct_TotalLeadTime, ct_TotalRLTime
		EKKO_EKGRP,												-- for Dim_PurchaseGroupid
		EKKO_EKORG,												-- for Dim_PurchaseOrgid
		EKKO_RESWK,												-- for Dim_PlantidSupplying
		EKPO_BUKRS,												-- for Dim_Currencyid
		EKPO_MATKL,												-- for Dim_MaterialGroupid
		EKET_BEDAT,												-- for Dim_DateIdSchedOrder
		EKPO_LGBZO,												-- for Dim_MSZoneId
		EKPO_RESLO,			 									-- for Dim_IssuStorageLocid 
		ifnull(EKET_MNG02,0) ct_CommittedQty
FROM ekko_ekpo_eket dm_di_ds
		INNER JOIN dim_plant pl ON pl.PlantCode = dm_di_ds.EKPO_WERKS
		INNER JOIN dim_company dc ON dc.companycode = dm_di_ds.EKPO_BUKRS
WHERE NOT EXISTS (SELECT 1
				  FROM fact_purchase fp
				  WHERE     fp.dd_DocumentNo = dm_di_ds.EKPO_EBELN
				        AND fp.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
						AND fp.dd_ScheduleNo = dm_di_ds.EKET_ETENR));
						
/* amt_ExchangeRate_GBL */
	update fact_purchase_tmp_forinsert t1
	set amt_ExchangeRate_GBL = CASE WHEN t1.EKKO_WAERS = t1row.pGlobalCurrency THEN 1.00 ELSE -5 END 
	from tmp_var_purchasing_fact t1row, fact_purchase_tmp_forinsert t1
	where amt_ExchangeRate_GBL <> CASE WHEN t1.EKKO_WAERS = t1row.pGlobalCurrency THEN 1.00 ELSE -5 END;

/* Dim_Currencyid_GBL */					
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_Currencyid_GBL = ifnull(t2.Dim_Currencyid, 1)
	from fact_purchase_tmp_forinsert t1
			cross join (select Dim_Currencyid
						FROM Dim_Currency dcr 
								inner join tmp_var_purchasing_fact t1row 
										on dcr.CurrencyCode = pGlobalCurrency) t2
	where t1.Dim_Currencyid_GBL <> ifnull(t2.Dim_Currencyid, 1);

/* Dim_Currencyid_TRA */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_Currencyid_TRA = ifnull(dcr.Dim_Currencyid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_Currency dcr on dcr.CurrencyCode = t1.EKKO_WAERS
	where t1.Dim_Currencyid_TRA <> ifnull(dcr.Dim_Currencyid, 1);
	
/* ct_baseuomqty */
	update fact_purchase_tmp_forinsert t1
	set ct_baseuomqty = CASE WHEN EKKO_AUTLF IS NOT NULL OR EKKO_LOEKZ IS NOT NULL OR EKPO_ELIKZ IS NOT NULL OR EKPO_LOEKZ IS NOT NULL THEN EKET_WEMNG ELSE EKET_MENGE END * 
						CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END *
						ifnull((i.EINA_UMREZ / case i.EINA_UMREN when 0 then 1 else i.EINA_UMREN end), 1)	
	from fact_purchase_tmp_forinsert t1
			left join EINA i on i.INFNR = t1.EKPO_INFNR;

/* Dim_StorageLocationid */
	drop table if exists tmp_updt_dim_StorageLocation;
	create table tmp_updt_dim_StorageLocation
	as
	select LocationCode,Plant,max(Dim_StorageLocationid) Dim_StorageLocationid from dim_StorageLocation group by LocationCode,Plant;
	
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
	from fact_purchase_tmp_forinsert t1
			left join tmp_updt_dim_StorageLocation sl on    sl.LocationCode = t1.EKPO_LGORT 
											   AND sl.Plant = t1.EKPO_WERKS
	where t1.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);
	
	drop table if exists tmp_updt_dim_StorageLocation;

/* Dim_ShipToAddressid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_ShipToAddressid = ifnull(t2.Dim_Addressid, 1)
	from fact_purchase_tmp_forinsert t1
			left join (select min(sta.Dim_Addressid) as Dim_Addressid,
							  sta.AddressNumber
					   from Dim_Address sta
					   group by sta.AddressNumber) t2
					 on t1.EKPO_ADRNR = t2.AddressNumber
	where t1.Dim_ShipToAddressid <> ifnull(t2.Dim_Addressid, 1);

/* Dim_DelivAddressid */ 
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_ShipToAddressid = ifnull(t2.Dim_Addressid, 1)
	from fact_purchase_tmp_forinsert t1
			left join (select min(sta.Dim_Addressid) as Dim_Addressid,
							  sta.AddressNumber
					   from Dim_Address sta
					   group by sta.AddressNumber) t2
					 on t1.EKPO_ADRN2 = t2.AddressNumber
	where t1.Dim_ShipToAddressid <> ifnull(t2.Dim_Addressid, 1);
	
/* Dim_ItemStatusid */ 
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_ItemStatusid = ifnull(its.Dim_ItemStatusid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_ItemStatus its on its.Status = t1.EKPO_STATUS
	where t1.Dim_ItemStatusid <> ifnull(its.Dim_ItemStatusid, 1);

/* Dim_DocumentStatusid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_DocumentStatusid = ifnull(dst.Dim_DocumentStatusid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_DocumentStatus dst on dst.Status = t1.EKKO_STATU
	where t1.Dim_DocumentStatusid <> ifnull(dst.Dim_DocumentStatusid, 1);

/* Dim_DocumentTypeid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_DocumentTypeid = ifnull(dtp.Dim_DocumentTypeid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_DocumentType dtp on dtp.Type = t1.EKKO_BSART and dtp.Category = t1.EKKO_BSTYP
	where t1.Dim_DocumentTypeid <> ifnull(dtp.Dim_DocumentTypeid, 1);

/* Dim_UnitOfMeasureid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_UnitOfMeasureid = ifnull(uom.Dim_UnitOfMeasureid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_UnitOfMeasure uom on uom.UOM = t1.EKPO_MEINS
	where t1.Dim_UnitOfMeasureid <> ifnull(uom.Dim_UnitOfMeasureid, 1);

/* Dim_StockTypeid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_StockTypeid = ifnull(st.Dim_StockTypeid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_StockType st on st.TypeCode = t1.EKPO_INSMK
	where t1.Dim_StockTypeid <> ifnull(st.Dim_StockTypeid, 1);
	
/* Dim_ConsumptionTypeid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_ConsumptionTypeid = ifnull(ct.Dim_ConsumptionTypeid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_ConsumptionType ct on ct.ConsumptionCode = t1.EKPO_KZVBR
	where t1.Dim_ConsumptionTypeid <> ifnull(ct.Dim_ConsumptionTypeid, 1);

/* Dim_ItemCategoryid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_ItemCategoryid = ifnull(ic.Dim_ItemCategoryid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_ItemCategory ic on ic.CategoryCode = t1.EKPO_PSTYP
	where t1.Dim_ItemCategoryid <> ifnull(ic.Dim_ItemCategoryid, 1);

/* Dim_AccountCategoryid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_AccountCategoryid = ifnull(ac.Dim_AccountCategoryid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_AccountCategory ac on ac.Category = t1.EKPO_KNTTP
	where t1.Dim_AccountCategoryid <> ifnull(ac.Dim_AccountCategoryid, 1);

/* Dim_IncoTerm1id */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_IncoTerm1id = ifnull(it.Dim_IncoTermid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_IncoTerm it on it.IncoTermCode = ifnull(t1.EKPO_INCO1, t1.EKKO_INCO1)
	where t1.Dim_IncoTerm1id <> ifnull(it.Dim_IncoTermid, 1);

/* Dim_Termid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_Termid = ifnull(t2.Dim_Termid, 1)
	from fact_purchase_tmp_forinsert t1
			left join (select min(Dim_Termid) Dim_Termid, dtm.TermCode
					   from Dim_Term dtm
					   group by dtm.TermCode) t2
					 on t2.TermCode = t1.EKKO_ZTERM
	where t1.Dim_Termid <> ifnull(t2.Dim_Termid, 1);
	
/* Dim_Vendorid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_Vendorid = ifnull(v.Dim_Vendorid, 1)
	from fact_purchase_tmp_forinsert t1
			left join dim_vendor v on v.VendorNumber = t1.EKKO_LIFNR
	where t1.Dim_Vendorid <> ifnull(v.Dim_Vendorid, 1);
	
/* Dim_DateidOrder, Dim_DateidCreate, Dim_DateidDelivery, Dim_DateidStatDelivery, Dim_DateidShip, 
   Dim_DateidItemCreate, Dim_DateidPrice, Dim_DateIdValidityEnd, Dim_DateIdValidityStart, Dim_DateIdSchedOrder */
	merge into fact_purchase_tmp_forinsert fact
	using (select t2.fact_purchaseid, 
				  ifnull(dcrd.dim_dateid, 1) Dim_DateidOrder,
				  ifnull(ddcr.dim_dateid, 1) Dim_DateidCreate,
				  ifnull(ifnull(dddl.dim_dateid, ddsd.dim_dateid), 1) Dim_DateidDelivery,
				  ifnull(ifnull(ddsd.dim_dateid, dddl.dim_dateid), 1) Dim_DateidStatDelivery,
				  ifnull(ifnull(dsh1.dim_dateid, dsh2.dim_dateid), 1) Dim_DateidShip,
				  ifnull(ddic.dim_dateid, 1) Dim_DateidItemCreate,
				  ifnull(ddpr.dim_dateid, 1) Dim_DateidPrice,
				  ifnull(ddve.dim_dateid, 1) Dim_DateIdValidityEnd,
				  ifnull(ddvs.dim_dateid, 1) Dim_DateIdValidityStart,
				  ifnull(ddso.dim_dateid, 1) Dim_DateIdSchedOrder
		   from (select t1.fact_purchaseid, pl.CompanyCode,
					    t1.EKKO_BEDAT,	-- Dim_DateidOrder
						t1.EKKO_AEDAT,	-- Dim_DateidCreate
						t1.EKET_EINDT,	-- Dim_DateidDelivery / Dim_DateidStatDelivery
						t1.EKET_SLFDT,	-- Dim_DateidStatDelivery / Dim_DateidDelivery
						t1.ct_CatalogLeadTime, -- Dim_DateidShip
						t1.EKPO_AEDAT,	-- Dim_DateidItemCreate
						t1.EKPO_PRDAT,	-- Dim_DateidPrice
						t1.EKKO_KDATE,	-- Dim_DateIdValidityEnd
						t1.EKKO_KDATB,	-- Dim_DateIdValidityStart
						t1.EKET_BEDAT	-- Dim_DateIdSchedOrder
				 from fact_purchase_tmp_forinsert t1
						inner join dim_plant pl on t1.Dim_PlantidOrdering = pl.dim_plantid) t2
					left join dim_date dcrd on    dcrd.CompanyCode = t2.CompanyCode
										      and dcrd.DateValue = t2.EKKO_BEDAT
					left join dim_date ddcr on    ddcr.CompanyCode = t2.CompanyCode
										      and ddcr.DateValue = t2.EKKO_AEDAT
					left join dim_date dddl on    dddl.CompanyCode = t2.CompanyCode
										      and dddl.DateValue = t2.EKET_EINDT
					left join dim_date ddsd on    ddsd.CompanyCode = t2.CompanyCode
										      and ddsd.DateValue = t2.EKET_SLFDT
					left join dim_date dsh1 on    dsh1.CompanyCode = t2.CompanyCode
										      and dsh1.DateValue = (t2.EKET_EINDT - t2.ct_CatalogLeadTime)
					left join dim_date dsh2 on    dsh2.CompanyCode = t2.CompanyCode
										      and dsh2.DateValue = (t2.EKET_SLFDT - t2.ct_CatalogLeadTime)
					left join dim_date ddic on    ddic.CompanyCode = t2.CompanyCode
										      and ddic.DateValue = t2.EKPO_AEDAT
					left join dim_date ddpr on    ddpr.CompanyCode = t2.CompanyCode
										      and ddpr.DateValue = t2.EKPO_PRDAT
					left join dim_date ddve on    ddve.CompanyCode = t2.CompanyCode
										      and ddve.DateValue = t2.EKKO_KDATE
					left join dim_date ddvs on    ddvs.CompanyCode = t2.CompanyCode
										      and ddvs.DateValue = t2.EKKO_KDATB
					left join dim_date ddso on    ddso.CompanyCode = t2.CompanyCode
										      and ddso.DateValue = t2.EKET_BEDAT
		   ) src
	on fact.fact_purchaseid = src.fact_purchaseid
	when matched then update set fact.Dim_DateidOrder = src.Dim_DateidOrder,
								 fact.Dim_DateidCreate = src.Dim_DateidCreate,
								 fact.Dim_DateidDelivery = src.Dim_DateidDelivery,
								 fact.Dim_DateidStatDelivery = src.Dim_DateidStatDelivery,
								 fact.Dim_DateidShip = src.Dim_DateidShip,
								 fact.Dim_DateidItemCreate = src.Dim_DateidItemCreate,
								 fact.Dim_DateidPrice = src.Dim_DateidPrice,
								 fact.Dim_DateIdValidityEnd = src.Dim_DateIdValidityEnd,
								 fact.Dim_DateIdValidityStart = src.Dim_DateIdValidityStart,
								 fact.Dim_DateIdSchedOrder = src.Dim_DateIdSchedOrder;
		  
/* Dim_Partid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_Partid = ifnull(dp.dim_partid, 1)
	from fact_purchase_tmp_forinsert t1
			left join dim_part dp on    dp.Plant = t1.EKPO_WERKS
									AND dp.PartNumber = t1.EKPO_MATNR
	where t1.Dim_Partid <> ifnull(dp.dim_partid, 1);
		  
/* Dim_PurchaseGroupid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_PurchaseGroupid = ifnull(pg.Dim_PurchaseGroupid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_PurchaseGroup pg on pg.PurchaseGroup = t1.EKKO_EKGRP
	where t1.Dim_PurchaseGroupid <> ifnull(pg.Dim_PurchaseGroupid, 1);
		  
/* Dim_PurchaseOrgid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_PurchaseOrgid = ifnull(po.Dim_PurchaseOrgid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_PurchaseOrg po on po.PurchaseOrgCode = t1.EKKO_EKORG
	where t1.Dim_PurchaseOrgid <> ifnull(po.Dim_PurchaseOrgid, 1);
	
/* Dim_PlantidSupplying */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_PlantidSupplying = ifnull(dps.Dim_Plantid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_Plant dps on dps.PlantCode = t1.EKKO_RESWK
	where t1.Dim_PlantidSupplying <> ifnull(dps.Dim_Plantid, 1);
	
/* Dim_Currencyid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_Currencyid = ifnull(dcr.Dim_Currencyid, 1)
	from fact_purchase_tmp_forinsert t1
			inner join dim_company dc on t1.dim_companyid = dc.dim_companyid
			left join Dim_Currency dcr on dcr.CurrencyCode = dc.currency
	where t1.Dim_Currencyid <> ifnull(dcr.Dim_Currencyid, 1);

/* Dim_POCurrencyid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_POCurrencyid = ifnull(dcr.Dim_Currencyid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_Currency dcr on dcr.CurrencyCode = t1.EKKO_WAERS
	where t1.Dim_POCurrencyid <> ifnull(dcr.Dim_Currencyid, 1);
	
/* dim_producthierarchyid */
	update fact_purchase_tmp_forinsert t1
	set t1.dim_producthierarchyid = ifnull(t2.dim_producthierarchyid, 1)
	FROM fact_purchase_tmp_forinsert t1
			left join (select dph.dim_producthierarchyid, p.PartNumber, p.Plant
					   from dim_producthierarchy dph, Dim_Part p
					   where    dph.ProductHierarchy = p.ProductHierarchy 
							AND dph.RowIsCurrent = 1) t2
					on t2.PartNumber = t1.EKPO_MATNR AND t2.Plant = t1.EKPO_WERKS 
	where t1.dim_producthierarchyid <> ifnull(t2.dim_producthierarchyid, 1);
			  
/* Dim_MaterialGroupid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_MaterialGroupid = ifnull(mg.Dim_MaterialGroupid, 1)
	from fact_purchase_tmp_forinsert t1
			left join Dim_MaterialGroup mg on mg.MaterialGroupCode = t1.EKPO_MATKL
	where t1.Dim_MaterialGroupid <> ifnull(mg.Dim_MaterialGroupid, 1);

/* Dim_MSZoneId */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_MSZoneId = ifnull(t2.Dim_MaterialStagingZoneId, 1)
	from fact_purchase_tmp_forinsert t1
			left join (select min(ms.Dim_MaterialStagingZoneId) as Dim_MaterialStagingZoneId, ms.StagingArea
					   from Dim_MaterialStagingZone ms
					   group by ms.StagingArea) t2
					on t2.StagingArea = t1.EKPO_LGBZO
	where t1.Dim_MSZoneId <> ifnull(t2.Dim_MaterialStagingZoneId, 1);

/* ct_TotalLeadTime, ct_TotalRLTime */
	update fact_purchase_tmp_forinsert t1
	set t1.ct_TotalLeadTime = ifnull(dp.LeadTime + dp.ProcessingTime + t1.ct_GRProcessingTime, 0), -- LeadTime + ProcessingTime + EKPO_WEBAZ
	    t1.ct_TotalRLTime = ifnull(dp.TotalReplenishmentLeadTime, 0)
	from fact_purchase_tmp_forinsert t1
			left join dim_part dp on dp.Plant = t1.EKPO_WERKS AND dp.PartNumber = t1.EKPO_MATNR
	where (  t1.ct_TotalLeadTime <> ifnull(dp.LeadTime + dp.ProcessingTime + t1.ct_GRProcessingTime, 0)
		  OR t1.ct_TotalRLTime <> ifnull(dp.TotalReplenishmentLeadTime, 0));
		  
/* Dim_IssuStorageLocid */
	update fact_purchase_tmp_forinsert t1
	set t1.Dim_IssuStorageLocid = ifnull(sl.Dim_StorageLocationid, 1)
	from fact_purchase_tmp_forinsert t1
			left join dim_StorageLocation sl on sl.LocationCode = t1.EKPO_RESLO AND sl.Plant = t1.EKKO_RESWK
	where t1.Dim_IssuStorageLocid <> ifnull(sl.Dim_StorageLocationid, 1);

INSERT INTO fact_purchase(dd_DocumentNo,
						dd_DocumentItemNo,
						dd_ScheduleNo,
						dd_PurchaseReqNo,
						dd_PurchaseReqItemNo,
						dd_VendorMaterialNo,
						dd_inforecordno,
						dd_ConfirmEdi,
						dd_incoterms2,
						amt_SalesTax,
						amt_invoice,
						amt_Paid,
						ct_ExchangeRate,
						amt_ExchangeRate_GBL,
						ct_DeliveryQty,
						ct_ReceivedQty,
						ct_ReturnQty,
						ct_RejectQty,
						ct_GRProcessingTime,
						ct_OverDeliveryTolerance,
						ct_UnderDeliveryTolerance,
						ct_leadtime,
						ct_LeadTimeVariance,
						ct_BaseUOMQty,
						Dim_StorageLocationid,
						Dim_ShipToAddressid,
						Dim_DelivAddressId,
						Dim_ItemStatusid,
						Dim_DocumentStatusid,
						Dim_DocumentTypeid,
						Dim_UnitOfMeasureid,
						Dim_StockTypeid,
						Dim_ConsumptionTypeid,
						Dim_ItemCategoryid,
						Dim_AccountCategoryid,
						Dim_IncoTerm1id,
						Dim_Termid,
						Dim_Vendorid,
						Dim_DateidOrder,
						Dim_DateidCreate,
						Dim_DateidDelivery,
						Dim_DateidStatDelivery,
						Dim_DateidShip,
						Dim_DateidItemCreate,
						Dim_DateidInvoice,
						Dim_DateidPaid,
						Dim_DateidPrice,
						Dim_DateidValidityEnd,
						Dim_DateidValidityStart,
						Dim_Partid,
						Dim_PurchaseGroupid,
						Dim_PurchaseOrgid,
						Dim_PlantidOrdering,
						Dim_PlantidSupplying,
						Dim_Currencyid,
						Dim_POCurrencyid,
						Dim_PurchaseMiscid,
						dim_producthierarchyid,
						Dim_MaterialGroupid,
						dd_bomexplosionno,
						dd_packagingno,
						Dim_DateIdSchedOrder,
						ct_FirmZone,
						Dim_MSZoneId,
						ct_TotalLeadTime,
						ct_TotalRLTime,
						dd_RevisionLevel,
						dd_ReferenceNo,
						ct_QtyIssued,
						Dim_ProfitCenterid,
						Dim_IssuStorageLocid,
						dd_ShortText,
						amt_GrossOrderValue,
						ct_ItemQty,
						amt_ItemNetValue,
						amt_PricingSbt1,
						amt_PricingSbt2,
						amt_PricingSbt3,
						amt_PricingSbt4,
						amt_PricingSbt5,
						amt_PricingSbt6,
						Dim_CompanyId,
						dim_ReleaseIndicatorId,
						dim_ValuationClassId,
						dd_DeliveryNumber,
						dd_POCreatedBy,
						dim_ValuationTypeId,
						amt_POEXRate,
						dd_DeletionIndicator,
						dd_OurReference,
						QtyConversion_EqualTo,
						QtyConversion_Denom,
						PriceConversion_EqualTo,
						PriceConversion_Denom,							
						fact_purchaseid,
						Dim_Currencyid_TRA,
						Dim_Currencyid_GBL, 
						amt_ExchangeRate,
						dim_DateIdInvoiceDay,
						dd_PoLineNBR,
						dd_PurchFrom,
						ct_CatalogLeadTime,
						dirtyrow,
						ct_CommittedQty
				) 
select  dd_documentno,
		dd_documentitemno,
		dd_scheduleno,
		dd_purchasereqno,
		dd_purchasereqitemno,
		dd_vendormaterialno,
		dd_inforecordno,
		dd_confirmedi,
		dd_incoterms2,
		amt_salestax,
		amt_invoice,
		amt_paid,
		ct_exchangerate,
		amt_exchangerate_gbl,
		ct_deliveryqty,
		ct_receivedqty,
		ct_returnqty,
		ct_rejectqty,
		ct_grprocessingtime,
		ct_overdeliverytolerance,
		ct_underdeliverytolerance,
		ct_leadtime,
		ct_leadtimevariance,
		ct_baseuomqty,
		dim_storagelocationid,
		dim_shiptoaddressid,
		dim_delivaddressid,
		dim_itemstatusid,
		dim_documentstatusid,
		dim_documenttypeid,
		dim_unitofmeasureid,
		dim_stocktypeid,
		dim_consumptiontypeid,
		dim_itemcategoryid,
		dim_accountcategoryid,
		dim_incoterm1id,
		dim_termid,
		dim_vendorid,
		dim_dateidorder,
		dim_dateidcreate,
		dim_dateiddelivery,
		dim_dateidstatdelivery,
		dim_dateidship,
		dim_dateiditemcreate,
		dim_dateidinvoice,
		dim_dateidpaid,
		dim_dateidprice,
		dim_dateidvalidityend,
		dim_dateidvaliditystart,
		dim_partid,
		dim_purchasegroupid,
		dim_purchaseorgid,
		dim_plantidordering,
		dim_plantidsupplying,
		dim_currencyid,
		dim_pocurrencyid,
		dim_purchasemiscid,
		dim_producthierarchyid,
		dim_materialgroupid,
		dd_bomexplosionno,
		dd_packagingno,
		dim_dateidschedorder,
		ct_firmzone,
		dim_mszoneid,
		ct_totalleadtime,
		ct_totalrltime,
		dd_revisionlevel,
		dd_referenceno,
		ct_qtyissued,
		dim_profitcenterid,
		dim_issustoragelocid,
		dd_shorttext,
		amt_grossordervalue,
		ct_itemqty,
		amt_itemnetvalue,
		amt_pricingsbt1,
		amt_pricingsbt2,
		amt_pricingsbt3,
		amt_pricingsbt4,
		amt_pricingsbt5,
		amt_pricingsbt6,
		dim_companyid,
		dim_releaseindicatorid,
		dim_valuationclassid,
		dd_deliverynumber,
		dd_pocreatedby,
		dim_valuationtypeid,
		amt_poexrate,
		dd_deletionindicator,
		dd_ourreference,
		qtyconversion_equalto,
		qtyconversion_denom,
		priceconversion_equalto,
		priceconversion_denom,
		fact_purchaseid,
		dim_currencyid_tra,
		dim_currencyid_gbl,
		amt_exchangerate,
		dim_dateidinvoiceday,
		dd_polinenbr,
		dd_purchfrom,
		ct_catalogleadtime,
		dirtyrow,
		ct_CommittedQty
from fact_purchase_tmp_forinsert;

/*End Changes 03 Jun 2015*/

/* Update dim_DateIdInvoiceDay separately  */
	UPDATE fact_purchase fpr
	SET dim_DateIdInvoiceDay = dd.Dim_Dateid
	FROM tmp_fact_purchase_dim_DateIdInvoiceDay dm_di_ds, dim_plant pl, Dim_Date dd, fact_purchase fpr
	WHERE     pl.PlantCode          = EKBE_WERKS
	      AND fpr.Dim_PlantidOrdering = pl.dim_plantid
		  AND fpr.dd_DocumentNo     = EKBE_EBELN
		  AND fpr.dd_DocumentItemNo = EKBE_EBELP
		  AND dd.DateValue          = max_EKBE_CPUDT 
		  AND dd.CompanyCode        = pl.CompanyCode
		  AND fpr.dim_DateIdInvoiceDay <> dd.Dim_Dateid;
				
/* Update ct_ExchangeRate and amt_ExchangeRate_GBL */
	merge into fact_purchase fact
	using (select distinct fpr.fact_purchaseid, ifnull(exchangeRate, 1) exchangeRate
		   from fact_purchase fpr
					inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
														 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
														 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
					inner join dim_company dc on fpr.dim_companyid = dc.dim_companyid
					left join (select z.exchangeRate,
									  z.pFromCurrency, z.pToCurrency, z.pFromExchangeRate, z.pDate
							   from tmp_getExchangeRate1 z
							   where z.fact_script_name = 'bi_populate_purchasing_fact') t2
							on    t2.pToCurrency       = dc.Currency
							  AND t2.pFromCurrency     = dm_di_ds.EKKO_WAERS 
							  AND t2.pFromExchangeRate = dm_di_ds.EKKO_WKURS
							  AND t2.pDate 		      = dm_di_ds.EKKO_BEDAT
		   where fpr.ct_ExchangeRate = -5 ) src
	on fact.fact_purchaseid = src.fact_purchaseid
	when matched then update set fact.ct_ExchangeRate = src.exchangeRate;
		
			/* original
			 UPDATE fact_purchase fpr
			 FROM tmp_var_purchasing_fact, ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
			 SET fpr.ct_ExchangeRate =
			  ifnull(( select z.exchangeRate 
								from tmp_getExchangeRate1 z
								 where z.pFromCurrency  = EKKO_WAERS 
								 AND z.pToCurrency = dc.Currency
								 AND z.pFromExchangeRate = EKKO_WKURS
								 AND z.pDate = EKKO_BEDAT
								 and z.fact_script_name = 'bi_populate_purchasing_fact'),1)	              
			 WHERE pl.PlantCode = EKPO_WERKS
			 AND dc.companycode = EKPO_BUKRS
			 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
			 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
			 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
			 AND  fpr.ct_ExchangeRate = -5 */
	 
/* Local exchange rate */	 
	UPDATE fact_purchase fpr
	SET amt_ExchangeRate   =  ct_ExchangeRate
	WHERE amt_ExchangeRate <> ct_ExchangeRate;
	
	merge into fact_purchase fact
	using (select fpr.fact_purchaseid, ifnull(t2.exchangeRate, 1) exchangeRate
		   from fact_purchase fpr
					inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
														 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
														 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
					left join (select z.exchangeRate, z.pFromCurrency
							   from tmp_getExchangeRate1 z
										inner join tmp_var_purchasing_fact t1row 
													on z.pToCurrency = t1row.pGlobalCurrency
							   where z.pFromExchangeRate = 0
								 AND z.pDate = current_date
								 and z.fact_script_name = 'bi_populate_purchasing_fact'
							  ) t2 on t2.pFromCurrency  = dm_di_ds.EKKO_WAERS
		   where fpr.amt_ExchangeRate_GBL = -5) src
	on fact.fact_purchaseid = src.fact_purchaseid
	when matched then update set fact.amt_ExchangeRate_GBL = src.exchangeRate
	where fact.amt_ExchangeRate_GBL <> src.exchangeRate;
	
			 /* original
			 UPDATE fact_purchase fpr
			 FROM tmp_var_purchasing_fact,  ekko_ekpo_eket dm_di_ds ,dim_plant pl,dim_company dc
			 SET  fpr.amt_ExchangeRate_GBL =
			  ifnull(( select z.exchangeRate 
								from tmp_getExchangeRate1 z
								 where z.pFromCurrency  = EKKO_WAERS
								 AND z.pToCurrency = pGlobalCurrency
								 AND z.pFromExchangeRate = 0
								 AND z.pDate = ANSIDATE(LOCAL_TIMESTAMP)
								 and z.fact_script_name = 'bi_populate_purchasing_fact'),1)	              
			 WHERE pl.PlantCode = EKPO_WERKS
			 AND dc.companycode = EKPO_BUKRS
			 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
			 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
			 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR	 
			 AND  fpr.amt_ExchangeRate_GBL = -5 */
	 
/* Update lead time and lead time variance */
	merge into fact_purchase fact
	using (select fpr.fact_purchaseid, ifnull(t2.v_leadTime, 0) v_leadTime
		   from fact_purchase fpr
					inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
														 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
														 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
					left join (select v_leadTime, 
									  pPart, pPlant, pVendor, DocumentNumber, DocumentLineNumber
							   from tmp_getLeadTime t
							   where    t.DocumentType =  'PO'
								    AND t.fact_script_name = 'bi_populate_purchasing_fact') t2 
							on t2.pPart = dm_di_ds.EKPO_MATNR and  
							   t2.pPlant = dm_di_ds.EKPO_WERKS and  
							   t2.pVendor = dm_di_ds.EKKO_LIFNR and
							   t2.DocumentNumber = dm_di_ds.EKPO_EBELN and 
							   t2.DocumentLineNumber = dm_di_ds.EKPO_EBELP
		   where fpr.ct_leadtime = -99999) src
	on fact.fact_purchaseid = src.fact_purchaseid
	when matched then update set fact.ct_leadtime = src.v_leadTime
	where fact.ct_leadtime <> src.v_leadTime;
	
			/* original
			UPDATE fact_purchase fpr
			FROM ekko_ekpo_eket dm_di_ds ,dim_plant pl,dim_company dc              
			SET ct_leadtime =    ifnull((SELECT v_leadTime 
								  FROM tmp_getLeadTime 
								  WHERE pPart = EKPO_MATNR AND pPlant = EKPO_WERKS
								  AND pVendor = EKKO_LIFNR AND DocumentNumber = EKPO_EBELN
								  AND DocumentLineNumber =  EKPO_EBELP AND DocumentType =  'PO'
								  AND fact_script_name = 'bi_populate_purchasing_fact'  )                              
								  ,0)    
			 WHERE pl.PlantCode = EKPO_WERKS
			 AND dc.companycode = EKPO_BUKRS
			 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
			 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
			 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR	 
			 AND  fpr.ct_leadtime = -99999 */				

	merge into fact_purchase fact
	using (select fpr.fact_purchaseid, 
				  ifnull(dm_di_ds.EKET_EINDT - t2.EKET_BEDAT, 0) - fpr.ct_leadtime as ct_LeadTimeVariance
		   from fact_purchase fpr
					inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
														 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
														 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
					left join (select min(x.EKET_BEDAT) EKET_BEDAT, x.EKPO_EBELN, x.EKPO_EBELP
							   from ekko_ekpo_eket x
							   group by x.EKPO_EBELN, x.EKPO_EBELP) t2 on    t2.EKPO_EBELN = dm_di_ds.EKPO_EBELN 
																		 and t2.EKPO_EBELP = dm_di_ds.EKPO_EBELP
		   where fpr.ct_LeadTimeVariance = -99999) src
	on fact.fact_purchaseid = src.fact_purchaseid
	when matched then update set fact.ct_LeadTimeVariance = src.ct_LeadTimeVariance
	where fact.ct_LeadTimeVariance <> src.ct_LeadTimeVariance;	
	
			/* original
			 UPDATE fact_purchase fpr
			 FROM ekko_ekpo_eket dm_di_ds ,dim_plant pl,dim_company dc   
			 SET ct_LeadTimeVariance
						= ifnull(EKET_EINDT - (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
												  where x.EKPO_EBELN = dm_di_ds.EKPO_EBELN and x.EKPO_EBELP = dm_di_ds.EKPO_EBELP),0)
						  - ct_leadtime
			 WHERE pl.PlantCode = EKPO_WERKS
			 AND dc.companycode = EKPO_BUKRS
			 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
			 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
			 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR	 
			 AND  fpr.ct_LeadTimeVariance = -99999   */

/* Update dim_profitcenterid */
	update fact_purchase fpr
	set dim_profitcenterid = ifnull(pc.Dim_ProfitCenterid, 1)
	from fact_purchase fpr
			inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN 
												 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP 
												 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
			left join tmp_dim_pc_purchasing_fact pc on dm_di_ds.EKPO_KO_PRCTR = pc.ProfitCenterCode
	where fpr.Dim_ProfitCenterid = -1;
	
			/* original
			UPDATE fact_purchase fpr
			FROM     ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc
			SET     dim_profitcenterid = ifnull((SELECT  pc.Dim_ProfitCenterid
											FROM tmp_dim_pc_purchasing_fact pc
											WHERE  pc.ProfitCenterCode = EKPO_KO_PRCTR ),1)	
			WHERE     pl.PlantCode = EKPO_WERKS
			 AND dc.companycode = EKPO_BUKRS
			 AND fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
			 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
			 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
			 AND fpr.Dim_ProfitCenterid = -1 */ 
	 
/* Insert 1 - Ends */  
							
update NUMBER_FOUNTAIN 
set max_id = ifnull(( select max(fact_purchaseid) from fact_purchase), 0)
where table_name = 'fact_purchase';							
 
/* Update 2 */

/* Update lead time and lead time variance where leadtime is 0 */
	merge into fact_purchase fact
	using (select fpr.fact_purchaseid, ifnull(t2.v_leadTime, 0) v_leadTime
		   from fact_purchase fpr
					inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
														 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
														 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
					inner join dim_date dddl on fpr.Dim_DateidDelivery = dddl.Dim_Dateid
					left join (select v_leadTime, 
									  pPart, pPlant, pVendor, DocumentNumber, DocumentLineNumber
							   from tmp_getLeadTime t
							   where    t.DocumentType =  'PO'
								    AND t.fact_script_name = 'bi_populate_purchasing_fact') t2 
							on t2.pPart = dm_di_ds.EKPO_MATNR and  
							   t2.pPlant = dm_di_ds.EKPO_WERKS and  
							   t2.pVendor = dm_di_ds.EKKO_LIFNR and
							   t2.DocumentNumber = dm_di_ds.EKPO_EBELN and 
							   t2.DocumentLineNumber = dm_di_ds.EKPO_EBELP
		   where     fpr.ct_leadtime = 0
			     and dddl.Dim_Dateid <> 1
				 and dm_di_ds.EKET_EINDT IS NOT NULL) src
	on fact.fact_purchaseid = src.fact_purchaseid
	when matched then update set fact.ct_leadtime = src.v_leadTime
	where fact.ct_leadtime <> src.v_leadTime;
	
			/* original
			 UPDATE fact_purchase fp
			 FROM ekko_ekpo_eket dm_di_ds, dim_date dt      
			 SET ct_leadtime =    ifnull((SELECT v_leadTime 
								  FROM tmp_getLeadTime 
								  WHERE pPart = EKPO_MATNR AND pPlant = EKPO_WERKS
								  AND pVendor = EKKO_LIFNR AND DocumentNumber = EKPO_EBELN
								  AND DocumentLineNumber =  EKPO_EBELP AND DocumentType =  'PO'
								  AND fact_script_name = 'bi_populate_purchasing_fact'  )                              
								  ,0)    
			 WHERE  fp.dd_DocumentNo = dm_di_ds.EKPO_EBELN
				 AND fp.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
				 AND fp.dd_ScheduleNo = dm_di_ds.EKET_ETENR
				 AND fp.Dim_DateidDelivery = dt.Dim_Dateid
				 AND EKET_EINDT IS NOT NULL
				 AND dt.Dim_Dateid <> 1
				 AND fp.ct_leadtime = 0 */	

	merge into fact_purchase fact
	using (select fpr.fact_purchaseid, 
				  ifnull(dm_di_ds.EKET_EINDT - t2.EKET_BEDAT, 0) - fpr.ct_leadtime as ct_LeadTimeVariance
		   from fact_purchase fpr
					inner join ekko_ekpo_eket dm_di_ds on    fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN
														 AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
														 AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
					inner join dim_date dddl on fpr.Dim_DateidDelivery = dddl.Dim_Dateid
					left join (select min(x.EKET_BEDAT) EKET_BEDAT, x.EKPO_EBELN, x.EKPO_EBELP
							   from ekko_ekpo_eket x
							   group by x.EKPO_EBELN, x.EKPO_EBELP) t2 on    t2.EKPO_EBELN = dm_di_ds.EKPO_EBELN 
																		 and t2.EKPO_EBELP = dm_di_ds.EKPO_EBELP
		   where     fpr.ct_leadtime = 0
			     and dddl.Dim_Dateid <> 1
				 and dm_di_ds.EKET_EINDT IS NOT NULL) src
	on fact.fact_purchaseid = src.fact_purchaseid
	when matched then update set fact.ct_LeadTimeVariance = src.ct_LeadTimeVariance
	where fact.ct_LeadTimeVariance <> src.ct_LeadTimeVariance;				 

			/* 
			 UPDATE fact_purchase fp
			 FROM ekko_ekpo_eket dm_di_ds, dim_date dt      
			 SET ct_LeadTimeVariance
						= ifnull(EKET_EINDT - (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
												  where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo),0)
						  - ct_leadtime
			 WHERE  fp.dd_DocumentNo = dm_di_ds.EKPO_EBELN
				 AND fp.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP
				 AND fp.dd_ScheduleNo = dm_di_ds.EKET_ETENR
				 AND fp.Dim_DateidDelivery = dt.Dim_Dateid
				 AND EKET_EINDT IS NOT NULL
				 AND dt.Dim_Dateid <> 1
				 AND fp.ct_leadtime = 0 */		

/*   Begin 05 Nov 2013 */

UPDATE fact_purchase fpr
SET dd_ReferenceDocument = 'Not Set'
WHERE dd_ReferenceDocument is null;

UPDATE fact_purchase fpr
SET dd_ReferenceDocument = ifnull(EKES_XBLNR, 'Not Set')
FROM     tmp_upd_fpr_ekes_maxeten e, fact_purchase fpr
WHERE  fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND dd_ReferenceDocument <> ifnull(EKES_XBLNR, 'Not Set');

UPDATE fact_purchase fpr
SET dim_ReleaseIndicatorId = d.dim_ReleaseIndicatorId
FROM     ekko_ekpo_eket e, dim_ReleaseIndicator d, fact_purchase fpr
WHERE  fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
  AND fpr.dd_ScheduleNo = EKET_ETENR
AND ReleaseIndicatorCode = ifnull(EKKO_FRGKE, 'Not Set')
AND fpr.dim_ReleaseIndicatorId <> d.dim_ReleaseIndicatorId; 

/*   End 05 Nov 2013 */

/*   Begin 14 Nov 2013 */
/*dd_DeliveryNumber*/
UPDATE fact_purchase fpr
SET dd_DeliveryNumber =  'Not Set'
WHERE dd_DeliveryNumber is null;

UPDATE fact_purchase fpr
SET dd_DeliveryNumber = ifnull(EKES_VBLEN, 'Not Set')
FROM     tmp_upd_fpr_ekes_maxeten e, fact_purchase fpr
WHERE  fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND dd_DeliveryNumber <> ifnull(EKES_VBLEN, 'Not Set');

/*dd_POCreatedBy*/
UPDATE fact_purchase fpr
SET dd_POCreatedBy =  'Not Set'
WHERE dd_POCreatedBy is null;

UPDATE fact_purchase fpr
SET dd_POCreatedBy = ifnull(EKKO_ERNAM, 'Not Set')
FROM     ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_POCreatedBy <> ifnull(EKKO_ERNAM, 'Not Set');

/*dim_ValuationTypeId*/
UPDATE fact_purchase fpr
SET dim_ValuationTypeId = 1
FROM     ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND dim_ValuationTypeId is null;

UPDATE fact_purchase fpr
SET dim_ValuationTypeId = dvt.dim_ValuationTypeId
FROM   ekko_ekpo_eket e, dim_ValuationType dvt, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND    dvt.ValuationType = ifnull(EKPO_BWTAR, 'Not Set')
AND fpr.dim_ValuationTypeId <> dvt.dim_ValuationTypeId;

/*amt_POEXRate*/
UPDATE fact_purchase fpr
SET amt_POEXRate = ifnull(EKKO_WKURS, 0)
FROM ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND amt_POEXRate <> ifnull(EKKO_WKURS, 0);

/*   End 14 Nov 2013 */

/*   Begin 14 Apr 2014 : dd_OurReference */
UPDATE fact_purchase fpr
SET dd_OurReference =  'Not Set'
WHERE dd_OurReference is null;

UPDATE fact_purchase fpr
SET dd_OurReference = ifnull(EKKO_UNSEZ, 'Not Set')
FROM     ekko_ekpo_eket e, fact_purchase fpr
WHERE   fpr.dd_DocumentNo = EKPO_EBELN
		AND fpr.dd_DocumentItemNo = EKPO_EBELP
		AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_OurReference <> ifnull(EKKO_UNSEZ, 'Not Set');
/*   End 14 Apr 2014: dd_OurReference */

/*   Begin 10 Dec 2013 */	
UPDATE fact_purchase fpr
SET dd_DeletionIndicator = ifnull(EKPO_LOEKZ, 'Not Set')
FROM     ekko_ekpo_eket dm_di_ds, fact_purchase fpr
WHERE fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND ifnull(dd_DeletionIndicator,'-1') <> ifnull(EKPO_LOEKZ, 'Not Set');

/* End of Update 2 */

DROP TABLE IF EXISTS tmp_fact_purchase_dim_DateIdInvoiceDay;
drop table if exists fact_purchase_tmp_forinsert;



/* Roxana - 24 Aug 2017 - Calculate In Transit Qty for Stock Analytics */


drop table if exists tmp_intransit;
create table tmp_intransit as 
select  EKBE_EBELN,EKBE_EBELP,EKBE_MENGE,EKBE_BEWTP,EKBE_BWART,EKKO_BSART,EKBE_GJAHR,EKBE_BELNR,EKBE_BUZEI,
	cast (0 as decimal (18,4)) as menge_101,cast (0 as decimal (18,4)) as menge_102,cast (0 as decimal (18,4)) as menge_641
from  EKBE_NEW
where EKKO_BSART in ('YUB','YTB');


drop table if exists tmp_itemcount;
create table tmp_itemcount as
select EKBE_EBELN,count(*) countall
from tmp_intransit 
group by EKBE_EBELN;


drop table if exists tmp_bwart;
create table tmp_bwart as
select t.EKBE_EBELN,t.EKBE_EBELP,EKBE_BWART,EKBE_BEWTP
from tmp_intransit t, tmp_itemcount c
where t.EKBE_EBELN = c.EKBE_EBELN
and EKBE_BWART = '101'
and countall >1; 


update tmp_intransit
set menge_641 = EKBE_MENGE
from tmp_intransit
where EKBE_BWART = '641' AND  EKBE_BEWTP ='U';


update tmp_intransit
set menge_101 = EKBE_MENGE
from tmp_intransit
where EKBE_BWART = '101' AND  EKBE_BEWTP ='E';


update tmp_intransit
set menge_102 = EKBE_MENGE
from tmp_intransit
where EKBE_BWART = '102' AND  EKBE_BEWTP ='E';


drop table if exists tmp_finalquantities;
create table tmp_finalquantities as
select case when sum(menge_641) + sum(menge_102) < sum(menge_101) then 0
		else sum(menge_641) + sum(menge_102) - sum(menge_101) end as mengefinal, EKBE_EBELN,EKBE_EBELP
from tmp_intransit 
group by EKBE_EBELN,EKBE_EBELP;



drop table if exists tmp_minscheduleno;
create table tmp_minscheduleno as
select distinct dd_documentno,dd_documentitemno,min(dd_Scheduleno) dd_Scheduleno from fact_purchase
group by dd_documentno,dd_documentitemno;


update fact_purchase fpr
set fpr.ct_intransit = ifnull(mengefinal,0)
from fact_purchase fpr,  tmp_finalquantities tmp, tmp_minscheduleno sch
where fpr.dd_documentno = tmp.EKBE_EBELN
and fpr.dd_documentitemno = tmp.EKBE_EBELP
and fpr.dd_documentno = sch.dd_documentno
and fpr.dd_documentitemno = sch.dd_documentitemno 
and fpr.dd_Scheduleno = sch.dd_Scheduleno
and fpr.ct_intransit <> ifnull(mengefinal,0);


/* end */

drop table if exists tmp_intransit;
drop table if exists tmp_itemcount;
drop table if exists tmp_bwart;
drop table if exists tmp_finalquantities;
drop table if exists tmp_minscheduleno;