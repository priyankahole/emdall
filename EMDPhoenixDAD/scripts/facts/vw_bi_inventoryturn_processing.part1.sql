 /**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Lokesh */
/*   Created On     : 13 Aug 2013 */
/*   Description    : Stored Proc bi_inventoryturn_processing migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   13 Aug 2013      Lokesh    1.0               Existing code migrated to Vectorwise.                           */
/******************************************************************************************************************/


  DROP TABLE IF EXISTS tmp_ivturn_globalvars;
  CREATE TABLE tmp_ivturn_globalvars
  AS
  select convert(varchar (7),ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD' )) pGlobalCurrency;
/*    Use this table only when getExchangeRate1 is joined with pGlobalCurrency */
  DROP TABLE IF EXISTS tmp_getExchangeRate1_with_globalvar;
  CREATE TABLE tmp_getExchangeRate1_with_globalvar AS
  SELECT er.*, igv.pGlobalCurrency
  FROM 
  tmp_getExchangeRate1 er
  INNER JOIN tmp_ivturn_globalvars igv ON er.pToCurrency = igv.pGlobalCurrency
  ;
  
  DELETE FROM fact_inventory_tmp;

-- Revise exchange_rate temp table and join it with tmp_ivturn_globalvars before the insert statements

  INSERT INTO fact_inventory_tmp
      (Fact_Inventoryid,
       amt_OpenStockValue,
       amt_CloseStockValue,
       amt_COGS,
       amt_COGS_GBL,
       amt_AvgInventoryValue,
       ct_InventoryTurn,
       Plant_code,
       Company_code,
       Part_no,
       FiYear,
       Period,
       PeriodStartDt,
       PeriodEndDt
       )
   SELECT Fact_Inventoryid,
          amt_OpenStockValue,
          amt_CloseStockValue,
          amt_COGS,
          amt_COGS * 1,
          amt_AvgInventoryValue,
          ct_InventoryTurn,
          pl.PlantCode,
          c.CompanyCode,
          p.PartNumber,
          dd.FinancialYear,
          dd.FinancialMonthNumber,
          dd.dateValue,
          dd1.dateValue
     FROM fact_inventory inv
          inner join dim_company c on inv.Dim_Companyid = c.Dim_Companyid
          inner join dim_plant pl on inv.Dim_Plantid = pl.Dim_Plantid
          inner join dim_part p on inv.Dim_Partid = p.Dim_Partid
          inner join dim_date dd on inv.Dim_DateidRecorded = dd.Dim_Dateid
          inner join dim_date dd1 on inv.Dim_DateidRecordedEnd = dd1.Dim_Dateid
     WHERE exists (SELECT 1 FROM S031 a
                   WHERE a.MATNR = p.PartNumber and a.WERKS = pl.PlantCode
                         and a.SPMON_YEAR = dd1.CalendarYear and a.SPMON_MONTH = dd1.CalendarMonthNumber);

DROP TABLE IF EXISTS fact_inventory_tmp_ins;	
CREATE TABLE fact_inventory_tmp_ins AS
SELECT Fact_Inventoryid, z.exchangeRate
FROM fact_inventory inv
		inner join dim_company c on inv.Dim_Companyid = c.Dim_Companyid
		LEFT join tmp_getExchangeRate1_with_globalvar z on ( z.pFromCurrency =  c.Currency AND z.pDate = current_date AND z.fact_script_name = 'bi_inventoryturn_processing' );

UPDATE fact_inventory_tmp t0
SET t0.amt_COGS_GBL = t0.amt_COGS_GBL * ifnull(t1.exchangeRate,1)
FROM fact_inventory_tmp t0
		INNER JOIN fact_inventory_tmp_ins t1 ON t0.Fact_Inventoryid = t1.Fact_Inventoryid	
WHERE t0.amt_COGS_GBL <> t0.amt_COGS_GBL * ifnull(t1.exchangeRate,1);

DROP TABLE fact_inventory_tmp_ins;
						 
DROP TABLE IF EXISTS tmp_inv_t_cursor;
CREATE TABLE    tmp_inv_t_cursor
(
 pFact_Inventoryid INT,
   pCompCode CHAR(4),
   pPlant CHAR(4),
   pMaterialNo CHAR(18),
   pFiYear INT,
   pprevFisYear INT,
   pFisYear INT,
   pPeriodFrom INT,
   pPeriodTo INT,
   pPer1 INT,
   pprevPer1 INT,
   pPer2 INT,
   pPer3 INT,
   pPer4 INT,
   pFromYear INT,
   pFromMonth INT,
   pToYear INT,
   pToMonth INT,
   pPeriodMth CHAR(3),
   pPeriodDates Char(53),
   pPeriodFromDate date,
   pPeriodToDate date,
   pPeriodMidDate date,
   pCurrency VARCHAR(10),
   TranStock1 decimal(20,6),
   TranStock2 decimal(20,6),
   ClosingStock decimal(20,6),
   OpeningStock decimal(20,6),
   OpCount Int,
   OpCount1 Int,
   ClCount Int,
   AverageInv decimal(20,6),
   CostOpen decimal(20,6),
   CostClose decimal(20,6),
   CostOfGoodsSold decimal(20,6),
   CostOfGoodsSold_GBL decimal(20,6),
   TargetTranCount1 INT,
   TargetTranCount2 INT,
   NoOfDays int,
   InvTurn decimal(20,6),
   pDates Char(45),
   pDates1 Char(45),
   pFromDateA date,
   pFromDate date,
   pTempDate1 date,
   pTempDate2 date,
   pTempDate3 date,
   pTempDate4 date,
   pToDate date,
   pToDateCalc date,
   pCurrDateCalc date,
   pMon Char(4),
   vNoOfDays INT,
   maxDays date,
   noOfInvDays int,
   pGlobalCurrency varchar(3),
   min_pPer4_flag int /* Required only for the 2nd tmp table */
   );


  INSERT INTO tmp_inv_t_cursor
  ( pFact_Inventoryid, pCompCode, pPlant,pFiYear, pPeriodFrom, pPeriodTo, pMaterialNo, pFromDate, pToDate, pCurrency )
  SELECT 
  Fact_Inventoryid pFact_Inventoryid, 
  Company_code pCompCode, 
  Plant_code pPlant, 
  FiYear pFiYear, 
  Period pPeriodFrom, 
  Period pPeriodTo,
  Part_no pMaterialNo, 
  PeriodStartDt pFromDate, 
  PeriodEndDt pToDate, 
  dc.Currency pCurrency
  FROM fact_inventory_tmp 
  INNER JOIN dim_Company dc on dc.companycode = Company_Code and dc.RowIsCurrent = 1
  ORDER BY Plant_code, FiYear, Period, Part_no;

 UPDATE tmp_inv_t_cursor t0
 SET pGlobalCurrency = ifnull(t1.property_value,'USD')
 FROM
 tmp_inv_t_cursor t0
CROSS JOIN (select t1.property_value 
            from systemproperty t1
	    where  t1.property = 'customer.global.currency' ) t1;



        DROP TABLE IF EXISTS tmp_ivt_opcount0;
        CREATE table tmp_ivt_opcount0
        AS
        SELECT BWKEY,MATNR,LFGJA,LFMON,BWTAR,COUNT(*) cnt
        FROM MBEWH
        GROUP BY BWKEY,MATNR,LFGJA,LFMON,BWTAR;

        DROP TABLE IF EXISTS tmp_ivt_opcount1;
        CREATE table tmp_ivt_opcount1
        AS
        SELECT BWKEY,MATNR,LFGJA,LFMON,COUNT(*) cnt
        FROM MBEWH
        GROUP BY BWKEY,MATNR,LFGJA,LFMON;


        DROP TABLE IF EXISTS tmp_inventoryturn_mbewh_t1;
        CREATE TABLE tmp_inventoryturn_mbewh_t1
        AS
        SELECT BWKEY,MATNR,LFGJA,LFMON,BWTAR,sum(SALK3) sum_SALK3
        from MBEWH
        GROUP BY BWKEY,MATNR,LFGJA,LFMON,BWTAR;

        DROP TABLE IF EXISTS tmp_inventoryturn_mbewh_t2;
        CREATE TABLE tmp_inventoryturn_mbewh_t2
        AS
        SELECT BWKEY,MATNR,LFGJA,LFMON,sum(SALK3) sum_SALK3
        from MBEWH
        GROUP BY BWKEY,MATNR,LFGJA,LFMON;


        DROP TABLE IF EXISTS tmp_inventoryturn_mbew_t1;
        CREATE TABLE tmp_inventoryturn_mbew_t1
        AS
        SELECT BWKEY,MATNR,LFGJA,LFMON,BWTAR,sum(SALK3) sum_SALK3
        from MBEW
        GROUP BY BWKEY,MATNR,LFGJA,LFMON,BWTAR;

        DROP TABLE IF EXISTS tmp_inventoryturn_mbew_t2;
        CREATE TABLE tmp_inventoryturn_mbew_t2
        AS
        SELECT BWKEY,MATNR,LFGJA,LFMON,sum(SALK3) sum_SALK3
        from MBEW
        GROUP BY BWKEY,MATNR,LFGJA,LFMON;


UPDATE  tmp_inv_t_cursor
SET vNoOfDays = pToDate -       pFromDate + 1;

UPDATE  tmp_inv_t_cursor
  set TranStock1 = 0;
  UPDATE        tmp_inv_t_cursor
  set ClosingStock = 0;
  UPDATE        tmp_inv_t_cursor
  set TranStock2 = 0;

  UPDATE        tmp_inv_t_cursor
  set OpeningStock = 0;

  UPDATE        tmp_inv_t_cursor
  set AverageInv = 0;


  /* Note : According to the proc in mysql, pPeriodFrom and pPeriodTo are always same ( Period ). So, the outer while loop is not actually needed */

  UPDATE        tmp_inv_t_cursor
  set pPer1 = pPeriodFrom;

  UPDATE        tmp_inv_t_cursor
  set pPer2 = pPeriodTo;

  UPDATE        tmp_inv_t_cursor
  set pPer3 = 12;

  UPDATE        tmp_inv_t_cursor
  set pFisYear = pFiYear;


   UPDATE       tmp_inv_t_cursor
   set pprevFisYear = pFisYear - 1,pprevPer1 = 12
   where pPer1 = 1;

   UPDATE       tmp_inv_t_cursor
   set pprevFisYear = pFisYear,pprevPer1 = pPer1 - 1
   where IFNULL(pPer1,-1) <> 1;


   UPDATE       tmp_inv_t_cursor
  set OpeningStock = 0;

   UPDATE       tmp_inv_t_cursor
  set CLCount = 0;

   UPDATE       tmp_inv_t_cursor
  set OpCount = 0;

   UPDATE       tmp_inv_t_cursor
  set OpCount1 = 0;

   UPDATE       tmp_inv_t_cursor
  set TranStock1 = 0;



        UPDATE  tmp_inv_t_cursor
		set CLCount = cnt
        FROM 
		tmp_inv_t_cursor,
		tmp_ivt_opcount1 a
        where a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and a.LFGJA = pprevFisYear
        and a.LFMON = pprevPer1;



  UPDATE        tmp_inv_t_cursor
  SET   OpeningStock = sum_SALK3
  FROM 
  tmp_inv_t_cursor,
  tmp_inventoryturn_mbewh_t1 a
  where a.BWKEY = pPlant
  and   a.MATNR = pMaterialNo
  and   a.LFGJA = pprevFisYear
  and   a.LFMON = pprevPer1
  and ifnull(a.BWTAR, '0') = '0'
  AND CLCount > 0;


  UPDATE        tmp_inv_t_cursor
  SET   OpeningStock = sum_SALK3
  FROM 
  tmp_inv_t_cursor,
  tmp_inventoryturn_mbewh_t2 a
  where a.BWKEY = pPlant
  and   a.MATNR = pMaterialNo
  and   a.LFGJA = pprevFisYear
  and   a.LFMON = pprevPer1
  AND CLCount > 0 and OpeningStock = 0;

    UPDATE  tmp_inv_t_cursor
        set pPer4 = pPer1 + 1;

        UPDATE tmp_inv_t_cursor
        set opcount = cnt
        FROM tmp_inv_t_cursor,tmp_ivt_opcount1 a   
        where a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and a.LFGJA = pFisYear
        and a.LFMON = pPer1;

        DROP TABLE IF EXISTS tmp_inv_t_cursor_innerwhile_OpCount_zero;

        CREATE TABLE tmp_inv_t_cursor_innerwhile_OpCount_zero
        AS
        SELECT *
        from tmp_inv_t_cursor
        WHERE OpCount = 0;

        DELETE FROM tmp_inv_t_cursor
        WHERE OpCount = 0;


/*  if(OpCount > 0) then                                   */

        UPDATE tmp_inv_t_cursor
        set TranStock1 = sum_SALK3
        FROM tmp_inv_t_cursor,tmp_inventoryturn_mbewh_t1 a     
        WHERE a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and  a.LFGJA = pFisYear
        and  a.LFMON = pPer1
        and  ifnull(a.BWTAR, '0') = '0'
        and OpCount > 0;


        UPDATE tmp_inv_t_cursor
		set TranStock1 = sum_SALK3
        FROM tmp_inv_t_cursor,tmp_inventoryturn_mbewh_t2 a     
        WHERE a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and  a.LFGJA = pFisYear
        and  a.LFMON = pPer1
        and OpCount > 0 and TranStock1 = 0;

        UPDATE tmp_inv_t_cursor
        set TranStock1 = sum_SALK3
        FROM tmp_inv_t_cursor,tmp_inventoryturn_mbew_t1 a    
        WHERE a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and  a.LFGJA = pFisYear
        and  a.LFMON = pPer1
        and  ifnull(a.BWTAR, '0') = '0'
        and OpCount > 0 and TranStock1 = 0;


        UPDATE tmp_inv_t_cursor
        set TranStock1 = sum_SALK3
        FROM tmp_inv_t_cursor,tmp_inventoryturn_mbew_t2 a      
        WHERE a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and  a.LFGJA = pFisYear
        and  a.LFMON = pPer1
        and OpCount > 0 and TranStock1 = 0;

        DROP TABLE IF EXISTS min_pPer4_tmp;
        CREATE TABLE min_pPer4_tmp
        AS
        SELECT BWKEY pPlant,MATNR pMaterialNo,LFGJA pFisYear,min(LFMON) min_pPer4_flag
        FROM MBEWH t2
        WHERE EXISTS ( SELECT 1 FROM tmp_inv_t_cursor_innerwhile_OpCount_zero t
                                        where t.pPlant = t2.BWKEY
                                        and t.pMaterialNo = t2.MATNR
                                        and t.pFisYear = t2.LFGJA
                                        AND LFMON >= pPer4
                                        AND LFMON <= pPer3 )
        GROUP BY BWKEY,MATNR,LFGJA;

        UPDATE tmp_inv_t_cursor_innerwhile_OpCount_zero t
        set min_pPer4_flag = t2.min_pPer4_flag
        FROM tmp_inv_t_cursor_innerwhile_OpCount_zero t, min_pPer4_tmp t2      
        where t.pPlant = t2.pPlant
        and t.pMaterialNo = t2.pMaterialNo
        and t.pFisYear = t2.pFisYear
        AND t2.min_pPer4_flag >= pPer4
        AND t2.min_pPer4_flag <= pPer3;


        UPDATE tmp_inv_t_cursor_innerwhile_OpCount_zero
        SET OpCount1 = cnt
        FROM tmp_inv_t_cursor_innerwhile_OpCount_zero,tmp_ivt_opcount1 a      
        where a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and a.LFGJA = pFisYear
        and a.LFMON = min_pPer4_flag;


        UPDATE tmp_inv_t_cursor_innerwhile_OpCount_zero
        set TranStock1 = sum_SALK3
        FROM tmp_inv_t_cursor_innerwhile_OpCount_zero,tmp_inventoryturn_mbewh_t1 a
        WHERE a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and  a.LFGJA = pFisYear
        and  a.LFMON = min_pPer4_flag
        and  ifnull(a.BWTAR, '0') = '0'
        and min_pPer4_flag <> 0;


        UPDATE tmp_inv_t_cursor_innerwhile_OpCount_zero
        set TranStock1 = sum_SALK3
        FROM tmp_inv_t_cursor_innerwhile_OpCount_zero,tmp_inventoryturn_mbewh_t2 a
        WHERE a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and  a.LFGJA = pFisYear
        and  a.LFMON = min_pPer4_flag
        and min_pPer4_flag <> 0 AND TranStock1 = 0;

        UPDATE tmp_inv_t_cursor_innerwhile_OpCount_zero
        set TranStock1 = sum_SALK3
        FROM tmp_inv_t_cursor_innerwhile_OpCount_zero,tmp_inventoryturn_mbew_t1 a  
        WHERE a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and  a.LFGJA = pFisYear
        and  a.LFMON = min_pPer4_flag
        and  ifnull(a.BWTAR, '0') = '0'
        and min_pPer4_flag <> 0 AND TranStock1 = 0;

        UPDATE tmp_inv_t_cursor_innerwhile_OpCount_zero
		set TranStock1 = sum_SALK3
        FROM tmp_inv_t_cursor_innerwhile_OpCount_zero,tmp_inventoryturn_mbew_t2 a       
        WHERE a.BWKEY = pPlant
        and a.MATNR = pMaterialNo
        and  a.LFGJA = pFisYear
        and  a.LFMON = min_pPer4_flag
        and min_pPer4_flag <> 0 AND TranStock1 = 0;


        INSERT INTO tmp_inv_t_cursor
        SELECT * FROM tmp_inv_t_cursor_innerwhile_OpCount_zero;

        UPDATE tmp_inv_t_cursor
    set ClosingStock = ClosingStock + TranStock1;

        UPDATE tmp_inv_t_cursor
        set OpeningStock = TranStock1,CLCount = 1
        WHERE CLCount = 0 and (OpCount > 0 or OpCount1 > 0);


  UPDATE tmp_inv_t_cursor
  set AverageInv = ((ClosingStock + OpeningStock)/((pPeriodTo - pPeriodFrom) + 2));

  /* Populate the fiscal year table. No need to create a custom script for populating this table */
DELETE FROM tmp_funct_fiscal_year
where fact_script_name = 'bi_inventoryturn_processing';

/* After the following insert, call the fiscal year standard script */
INSERT INTO tmp_funct_fiscal_year
(pCompanyCode,FiscalYear,Period,fact_script_name)
SELECT DISTINCT pCompCode, pFiYear, pPeriodFrom,'bi_inventoryturn_processing'
FROM tmp_inv_t_cursor t;

DROP TABLE tmp_getExchangeRate1_with_globalvar;