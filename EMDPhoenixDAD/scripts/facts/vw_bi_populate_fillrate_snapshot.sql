
drop table if exists tmp_dayofprocessing_var_fill;
create table tmp_dayofprocessing_var_fill
as
select case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end as processing_date from (select 1) a;

delete from number_fountain m where m.table_name = 'fact_fillrate_snapshot';

insert into number_fountain
select 	'fact_fillrate_snapshot',
	ifnull(max(fact_fillrate_snapshotid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_fillrate_snapshot d ;

/* drop table if exists tmp_loaded_datevalues
create table tmp_loaded_datevalues
as
select distinct(fc.dim_dateid) as dim_dateid
from fact_fillrate_snapshot ws inner join dim_date fc on ws.dim_dateidsnapshot = fc.dim_Dateid
where to_date(fc.datevalue) = to_Date(case when extract(hour from current_timestamp) between 0 and 16 then sysdate - 1 else sysdate end)
DELETE FROM fact_fillrate_snapshot t
where t.dim_dateidsnapshot in (select dim_dateid from tmp_loaded_datevalues ) */

insert into fact_fillrate_snapshot (
fact_fillrate_snapshotID
,DD_SALESDOCNO
,DD_SALESITEMNO
,DD_SCHEDULENO
,CT_SCHEDULEQTYSALESUNIT
,CT_CONFIRMEDQTY
,CT_CORRECTEDQTY
,CT_DELIVEREDQTY
,CT_ONHANDCOVEREDQTY
,AMT_UNITPRICE
,CT_PRICEUNIT
,AMT_SCHEDULETOTAL
,AMT_STDCOST
,AMT_TARGETVALUE
,CT_TARGETQTY
,AMT_EXCHANGERATE
,CT_OVERDLVRTOLERANCE
,CT_UNDERDLVRTOLERANCE
,DIM_DATEIDSALESORDERCREATED
,DIM_DATEIDSCHEDDELIVERYREQ
,DIM_DATEIDSCHEDDLVRREQPREV
,DIM_DATEIDSCHEDDELIVERY
,DIM_DATEIDGOODSISSUE
,DIM_DATEIDMTRLAVAIL
,DIM_DATEIDLOADING
,DIM_DATEIDTRANSPORT
,DIM_CURRENCYID
,DIM_PRODUCTHIERARCHYID
,DIM_PLANTID
,DIM_COMPANYID
,DIM_STORAGELOCATIONID
,DIM_SALESDIVISIONID
,DIM_SHIPRECEIVEPOINTID
,DIM_DOCUMENTCATEGORYID
,DIM_SALESDOCUMENTTYPEID
,DIM_SALESORGID
,DIM_CUSTOMERID
,DIM_DATEIDVALIDFROM
,DIM_DATEIDVALIDTO
,DIM_SALESGROUPID
,DIM_COSTCENTERID
,DIM_CONTROLLINGAREAID
,DIM_BILLINGBLOCKID
,DIM_TRANSACTIONGROUPID
,DIM_SALESORDERREJECTREASONID
,DIM_PARTID
,DIM_PARTSALESID
,DIM_SALESORDERHEADERSTATUSID
,DIM_SALESORDERITEMSTATUSID
,DIM_CUSTOMERGROUP1ID
,DIM_CUSTOMERGROUP2ID
,DIM_SALESORDERITEMCATEGORYID
,AMT_EXCHANGERATE_GBL
,CT_FILLQTY
,CT_ONHANDQTY
,DIM_DATEIDFIRSTDATE
,DIM_SCHEDULELINECATEGORYID
,DIM_SALESMISCID
,DD_ITEMRELFORDELV
,DIM_DATEIDSHIPMENTDELIVERY
,DIM_DATEIDACTUALGI
,DIM_DATEIDSHIPDLVRFILL
,DIM_DATEIDACTUALGIFILL
,DIM_PROFITCENTERID
,DIM_CUSTOMERIDSHIPTO
,DIM_DATEIDGUARANTEEDATE
,DIM_UNITOFMEASUREID
,DIM_DISTRIBUTIONCHANNELID
,DD_BATCHNO
,DD_CREATEDBY
,DIM_CUSTOMPARTNERFUNCTIONID
,DIM_DATEIDLOADINGFILL
,DIM_PAYERPARTNERFUNCTIONID
,DIM_BILLTOPARTYPARTNERFUNCTIONID
,CT_SHIPPEDAGNSTORDERQTY
,CT_CMLQTYRECEIVED
,DIM_CUSTOMPARTNERFUNCTIONID1
,DIM_CUSTOMPARTNERFUNCTIONID2
,DIM_CUSTOMERGROUPID
,DIM_SALESOFFICEID
,DIM_HIGHERCUSTOMERID
,DIM_HIGHERSALESORGID
,DIM_HIGHERDISTCHANNELID
,DIM_HIGHERSALESDIVID
,DD_SOLDTOHIERARCHYLEVEL
,DD_HIERARCHYTYPE
,DIM_TOPLEVELCUSTOMERID
,DIM_TOPLEVELDISTCHANNELID
,DIM_TOPLEVELSALESDIVID
,DIM_TOPLEVELSALESORGID
,CT_AFSUNALLOCATEDQTY
,AMT_AFSUNALLOCATEDVALUE
,CT_AFSALLOCATIONRQTY
,CT_CUMCONFIRMEDQTY
,CT_AFSALLOCATIONFQTY
,CT_CUMORDERQTY
,AMT_AFSONDELIVERYVALUE
,CT_SHIPPEDORBILLEDQTY
,AMT_SHIPPEDORBILLED
,DIM_CREDITREPRESENTATIVEID
,DD_CUSTOMERPONO
,CT_INCOMPLETEQTY
,AMT_INCOMPLETE
,DIM_DELIVERYBLOCKID
,DD_AFSSTOCKCATEGORY
,DD_AFSSTOCKTYPE
,DIM_DATEIDAFSCANCELDATE
,AMT_AFSNETPRICE
,AMT_AFSNETVALUE
,AMT_BASEPRICE
,DIM_MATERIALPRICEGROUP1ID
,AMT_CREDITHOLDVALUE
,AMT_DELIVERYBLOCKVALUE
,DIM_AFSSIZEID
,DIM_AFSREJECTIONREASONID
,DIM_DATEIDDLVRDOCCREATED
,AMT_CUSTOMEREXPECTEDPRICE
,CT_AFSALLOCATEDQTY
,CT_AFSONDELIVERYQTY
,DIM_OVERALLSTATUSCREDITCHECKID
,DIM_SALESDISTRICTID
,DIM_ACCOUNTASSIGNMENTGROUPID
,DIM_MATERIALGROUPID
,DIM_SALESDOCORDERREASONID
,AMT_SUBTOTAL3
,AMT_SUBTOTAL4
,DIM_DATEIDAFSREQDELIVERY
,AMT_DICOUNTACCRUALNETPRICE
,CT_AFSOPENQTY
,DIM_PURCHASEORDERTYPEID
,DIM_DATEIDQUOTATIONVALIDFROM
,DIM_DATEIDPURCHASEORDER
,DIM_DATEIDQUOTATIONVALIDTO
,DIM_AFSSEASONID
,DD_AFSDEPARTMENT
,DIM_DATEIDSOCREATED
,DIM_DATEIDSODOCUMENT
,DD_SUBSEQUENTDOCNO
,DD_SUBSDOCITEMNO
,DD_SUBSSCHEDULENO
,DIM_SUBSDOCCATEGORYID
,CT_AFSTOTALDRAWN
,DIM_DATEIDASLASTDATE
,DD_REFERENCEDOCUMENTNO
,DD_REQUIREMENTTYPE
,AMT_STDPRICE
,DIM_DATEIDNEXTDATE
,AMT_TAX
,DIM_DATEIDEARLIESTSOCANCELDATE
,DIM_DATEIDLATESTCUSTPOAGI
,DD_BUSINESSCUSTOMERPONO
,DIM_ROUTEID
,DIM_BILLINGDATEID
,DIM_CUSTOMERPAYMENTTERMSID
,DIM_SALESRISKCATEGORYID
,DD_CREDITREP
,DD_CREDITLIMIT
,DIM_CUSTOMERRISKCATEGORYID
,CT_FILLQTY_CRD
,CT_FILLQTY_PDD
,DIM_H1CUSTOMERID
,DIM_DATEIDSOITEMCHANGEDON
,DIM_SHIPPINGCONDITIONID
,DD_AFSALLOCATIONGROUPNO
,DIM_DATEIDREJECTION
,DD_CUSTOMERMATERIALNO
,DIM_BASEUOMID
,DIM_SALESUOMID
,AMT_UNITPRICEUOM
,DIM_DATEIDFIXEDVALUE
,DIM_PAYMENTTERMSID
,DIM_DATEIDNETDUEDATE
,DIM_MATERIALPRICEGROUP4ID
,DIM_MATERIALPRICEGROUP5ID
,AMT_SUBTOTAL3_ORDERQTY
,AMT_SUBTOTAL3INCUSTCONFIG_BILLING
,DIM_CUSTOMERMASTERSALESID
,DD_SALESORDERBLOCKED
,DD_SOCREATETIME
,DD_REQDELIVERYTIME
,DD_SOLINECREATETIME
,DD_DELIVERYTIME
,DD_PLANNEDGITIME
,DIM_DATEIDARPOSTING
,DIM_CURRENCYID_TRA
,DIM_CURRENCYID_GBL
,DIM_CURRENCYID_STAT
,AMT_EXCHANGERATE_STAT
,DD_ARUNNUMBER
,DIM_AVAILABILITYCHECKID
,DD_IDOCNUMBER
,DIM_INCOTERMID
,DD_INTERNATIONALARTICLENO
,DD_RELEASERULE
,DIM_DELIVERYPRIORITYID
,DD_REFERENCEDOCITEMNO
,DIM_SALESORDERPARTNERID
,DD_PURCHASEREQUISITION
,DD_ITEMOFREQUISITION
,CT_AFSCALCULATEDOPENQTY
,AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE
,DD_DOCUMENTCONDITIONNO
,AMT_SUBTOTAL1
,AMT_SUBTOTAL2
,AMT_SUBTOTAL5
,AMT_SUBTOTAL6
,DD_HIGHLEVELITEM
,DD_PODOCUMENTNO
,DD_PODOCUMENTITEMNO
,DD_POSCHEDULENO
,DD_PRODORDERNO
,DD_PRODORDERITEMNO
,DIM_CUSTOMERCONDITIONGROUPS1ID
,DIM_CUSTOMERCONDITIONGROUPS2ID
,DIM_CUSTOMERCONDITIONGROUPS3ID
,CT_AFSOPENPOQTY
,CT_AFSINTRANSITQTY
,DIM_SCHEDULEDELIVERYBLOCKID
,DIM_CUSTOMERGROUP4ID
,DD_OPENCONFIRMATIONSEXISTS
,DD_CONDITIONNO
,DD_CREDITMGR
,DD_CLEAREDBLOCKEDSTS
,CT_AFSINRDDUNITS
,CT_AFSAFTERRDDUNITS
,DIM_AGREEMENTSID
,DIM_CHARGEBACKCONTRACTID
,DD_RO_MATERIALDESC
,DIM_CUSTOMERIDCBOWNER
,CT_COMMITTEDQTY
,DIM_DATEIDREQDELIVLINE
,DIM_MATERIALPRICINGGROUPID
,DIM_BILLINGBLOCKSALESDOCLVLID
,DD_DELIVERYINDICATOR
,DD_SHIPTOPARTYSTREET
,DIM_CUSTOMERCONDITIONGROUPS4ID
,DIM_CUSTOMERCONDITIONGROUPS5ID
,DD_PURCHASEORDERITEM
,DIM_MATERIALPRICEGROUP2ID
,DIM_PROJECTSOURCEID
,DD_BILLING_NO
,DIM_MDG_PARTID
,DIM_BWPRODUCTHIERARCHYID
,CT_FIRSTNONZEROCONFIRMQTY
,CT_VOLUME
,CT_NOTCONFIRMEDQTY
,DD_CUSTOMCUSTEXPPRICEINC
,DD_DOCUMENTFLOWGROUP
,DD_ORDERBY
,DD_CUSTOMPRICEMISSINGINC
,DIM_CUSTOMERENDUSERFORFTRADE
,DIM_DATEIDSNAPSHOT
,dim_accordinggidatemto_emd
,dim_dateidexpectedship_emd
,dim_dateidrequested_emd
,ct_baseuomratio
,ct_baseuomratiokg
,ct_countsalesdocitem
,ct_deliveryisfull
,ct_deliveryontime
,ct_deliveryontimecasual
,dd_ace_openorders
,dd_backorderflag
,dd_backorderreason
,dd_backorder_responsibility
,dd_deliveryisfull
,dd_deliveryontime
,dd_deliveryontimecasual
,dd_intercompflag
,dd_lsintercompflag
,dd_mercklsbackorderfilter
,dd_ora_active_hold_info
,dd_ora_attribute11
,dd_ora_line_on_hold
,dd_ora_tradsales_flag
,dim_backorder_dateid
,dim_commercialviewid
,dim_dateidactualgimerckls
,dim_dateidconfirmeddelivery_emd
,AMT_EXCHANGERATE_CUSTOM
,DD_OPENQTYCVRDBYMAT
,DD_OPENQTYCVRDBYPLANT
,DIM_CLUSTERID
,DW_INSERT_DATE
,DW_UPDATE_DATE
,dim_countryhierpsid
,dim_countryhierarid
,dd_gsa_code
,dim_date_enddateloadingid
,fact_salesorderid
,dim_customer_shipto
,dim_productionschedulerid
,dim_gsamapimportid
,dd_inco1
,dd_inco2
,dd_ship_to_party
,dd_mtomts
,DIM_ADDRESSID
,dd_CompleteInSingleDelivery
,dd_batch_split
,dd_delivery_group
)
select
(select ifnull(max(m.max_id), 1) from number_fountain m where m.table_name = 'fact_fillrate_snapshot')
          + row_number()
             over(order by '') fact_fillrate_snapshotID
,ifnull(f.DD_SALESDOCNO,'Not Set')
,ifnull(f.DD_SALESITEMNO,0)
,ifnull(f.DD_SCHEDULENO,0)
,ifnull(f.CT_SCHEDULEQTYSALESUNIT,0)
,ifnull(f.CT_CONFIRMEDQTY,0)
,ifnull(f.CT_CORRECTEDQTY,0)
,ifnull(f.CT_DELIVEREDQTY,0)
,ifnull(f.CT_ONHANDCOVEREDQTY,0)
,ifnull(f.AMT_UNITPRICE,0)
,ifnull(f.CT_PRICEUNIT,1)
,ifnull(f.AMT_SCHEDULETOTAL,0)
,ifnull(f.AMT_STDCOST,0)
,ifnull(f.AMT_TARGETVALUE,0)
,ifnull(f.CT_TARGETQTY,0)
,ifnull(f.AMT_EXCHANGERATE,1)
,ifnull(f.CT_OVERDLVRTOLERANCE,0)
,ifnull(f.CT_UNDERDLVRTOLERANCE,0)
,ifnull(f.DIM_DATEIDSALESORDERCREATED,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERYREQ,1)
,ifnull(f.DIM_DATEIDSCHEDDLVRREQPREV,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERY,1)
,ifnull(f.DIM_DATEIDGOODSISSUE,1)
,ifnull(f.DIM_DATEIDMTRLAVAIL,1)
,ifnull(f.DIM_DATEIDLOADING,1)
,ifnull(f.DIM_DATEIDTRANSPORT,1)
,ifnull(f.DIM_CURRENCYID,1)
,ifnull(f.DIM_PRODUCTHIERARCHYID,1)
,ifnull(f.DIM_PLANTID,1)
,ifnull(f.DIM_COMPANYID,1)
,ifnull(f.DIM_STORAGELOCATIONID,1)
,ifnull(f.DIM_SALESDIVISIONID,1)
,ifnull(f.DIM_SHIPRECEIVEPOINTID,1)
,ifnull(f.DIM_DOCUMENTCATEGORYID,1)
,ifnull(f.DIM_SALESDOCUMENTTYPEID,1)
,ifnull(f.DIM_SALESORGID,1)
,ifnull(f.DIM_CUSTOMERID,1)
,ifnull(f.DIM_DATEIDVALIDFROM,1)
,ifnull(f.DIM_DATEIDVALIDTO,1)
,ifnull(f.DIM_SALESGROUPID,0)
,ifnull(f.DIM_COSTCENTERID,1)
,ifnull(f.DIM_CONTROLLINGAREAID,0)
,ifnull(f.DIM_BILLINGBLOCKID,0)
,ifnull(f.DIM_TRANSACTIONGROUPID,0)
,ifnull(f.DIM_SALESORDERREJECTREASONID,0)
,ifnull(f.DIM_PARTID,1)
,ifnull(f.DIM_PARTSALESID,1)
,ifnull(f.DIM_SALESORDERHEADERSTATUSID,0)
,ifnull(f.DIM_SALESORDERITEMSTATUSID,0)
,ifnull(f.DIM_CUSTOMERGROUP1ID,0)
,ifnull(f.DIM_CUSTOMERGROUP2ID,0)
,ifnull(f.DIM_SALESORDERITEMCATEGORYID,1)
,ifnull(f.AMT_EXCHANGERATE_GBL,1)
,ifnull(f.CT_FILLQTY,0)
,ifnull(f.CT_ONHANDQTY,0)
,ifnull(f.DIM_DATEIDFIRSTDATE,1)
,ifnull(f.DIM_SCHEDULELINECATEGORYID,1)
,ifnull(f.DIM_SALESMISCID,1)
,ifnull(f.DD_ITEMRELFORDELV,'Not Set')
,ifnull(f.DIM_DATEIDSHIPMENTDELIVERY,1)
,ifnull(f.DIM_DATEIDACTUALGI,1)
,ifnull(f.DIM_DATEIDSHIPDLVRFILL,1)
,ifnull(f.DIM_DATEIDACTUALGIFILL,1)
,ifnull(f.DIM_PROFITCENTERID,1)
,ifnull(f.DIM_CUSTOMERIDSHIPTO,1)
,ifnull(f.DIM_DATEIDGUARANTEEDATE,1)
,ifnull(f.DIM_UNITOFMEASUREID,1)
,ifnull(f.DIM_DISTRIBUTIONCHANNELID,1)
,ifnull(f.DD_BATCHNO,'Not Set')
,ifnull(f.DD_CREATEDBY,'Not Set')
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID,1)
,ifnull(f.DIM_DATEIDLOADINGFILL,1)
,ifnull(f.DIM_PAYERPARTNERFUNCTIONID,1)
,ifnull(f.DIM_BILLTOPARTYPARTNERFUNCTIONID,1)
,ifnull(f.CT_SHIPPEDAGNSTORDERQTY,0)
,ifnull(f.CT_CMLQTYRECEIVED,0)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID1,1)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID2,1)
,ifnull(f.DIM_CUSTOMERGROUPID,1)
,ifnull(f.DIM_SALESOFFICEID,1)
,ifnull(f.DIM_HIGHERCUSTOMERID,1)
,ifnull(f.DIM_HIGHERSALESORGID,1)
,ifnull(f.DIM_HIGHERDISTCHANNELID,1)
,ifnull(f.DIM_HIGHERSALESDIVID,1)
,ifnull(f.DD_SOLDTOHIERARCHYLEVEL,0)
,ifnull(f.DD_HIERARCHYTYPE,'Not Set')
,ifnull(f.DIM_TOPLEVELCUSTOMERID,1)
,ifnull(f.DIM_TOPLEVELDISTCHANNELID,1)
,ifnull(f.DIM_TOPLEVELSALESDIVID,1)
,ifnull(f.DIM_TOPLEVELSALESORGID,1)
,ifnull(f.CT_AFSUNALLOCATEDQTY,0)
,ifnull(f.AMT_AFSUNALLOCATEDVALUE,0)
,ifnull(f.CT_AFSALLOCATIONRQTY,0)
,ifnull(f.CT_CUMCONFIRMEDQTY,0)
,ifnull(f.CT_AFSALLOCATIONFQTY,0)
,ifnull(f.CT_CUMORDERQTY,0)
,ifnull(f.AMT_AFSONDELIVERYVALUE,0)
,ifnull(f.CT_SHIPPEDORBILLEDQTY,0)
,ifnull(f.AMT_SHIPPEDORBILLED,0)
,ifnull(f.DIM_CREDITREPRESENTATIVEID,1)
,ifnull(f.DD_CUSTOMERPONO,'Not Set')
,ifnull(f.CT_INCOMPLETEQTY,0)
,ifnull(f.AMT_INCOMPLETE,0)
,ifnull(f.DIM_DELIVERYBLOCKID,1)
,ifnull(f.DD_AFSSTOCKCATEGORY,'Not Set')
,ifnull(f.DD_AFSSTOCKTYPE,'Not Set')
,ifnull(f.DIM_DATEIDAFSCANCELDATE,1)
,ifnull(f.AMT_AFSNETPRICE,0)
,ifnull(f.AMT_AFSNETVALUE,0)
,ifnull(f.AMT_BASEPRICE,0)
,ifnull(f.DIM_MATERIALPRICEGROUP1ID,1)
,ifnull(f.AMT_CREDITHOLDVALUE,0)
,ifnull(f.AMT_DELIVERYBLOCKVALUE,0)
,ifnull(f.DIM_AFSSIZEID,1)
,ifnull(f.DIM_AFSREJECTIONREASONID,1)
,ifnull(f.DIM_DATEIDDLVRDOCCREATED,1)
,ifnull(f.AMT_CUSTOMEREXPECTEDPRICE,0)
,ifnull(f.CT_AFSALLOCATEDQTY,0)
,ifnull(f.CT_AFSONDELIVERYQTY,0)
,ifnull(f.DIM_OVERALLSTATUSCREDITCHECKID,1)
,ifnull(f.DIM_SALESDISTRICTID,1)
,ifnull(f.DIM_ACCOUNTASSIGNMENTGROUPID,1)
,ifnull(f.DIM_MATERIALGROUPID,1)
,ifnull(f.DIM_SALESDOCORDERREASONID,1)
,ifnull(f.AMT_SUBTOTAL3,0)
,ifnull(f.AMT_SUBTOTAL4,0)
,ifnull(f.DIM_DATEIDAFSREQDELIVERY,1)
,ifnull(f.AMT_DICOUNTACCRUALNETPRICE,0)
,ifnull(f.CT_AFSOPENQTY,0)
,ifnull(f.DIM_PURCHASEORDERTYPEID,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDFROM,1)
,ifnull(f.DIM_DATEIDPURCHASEORDER,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDTO,1)
,ifnull(f.DIM_AFSSEASONID,1)
,ifnull(f.DD_AFSDEPARTMENT,'Not Set')
,ifnull(f.DIM_DATEIDSOCREATED,1)
,ifnull(f.DIM_DATEIDSODOCUMENT,1)
,ifnull(f.DD_SUBSEQUENTDOCNO,'Not Set')
,ifnull(f.DD_SUBSDOCITEMNO,0)
,ifnull(f.DD_SUBSSCHEDULENO,0)
,ifnull(f.DIM_SUBSDOCCATEGORYID,1)
,ifnull(f.CT_AFSTOTALDRAWN,0)
,ifnull(f.DIM_DATEIDASLASTDATE,1)
,ifnull(f.DD_REFERENCEDOCUMENTNO,'Not Set')
,ifnull(f.DD_REQUIREMENTTYPE,'Not Set')
,ifnull(f.AMT_STDPRICE,0)
,ifnull(f.DIM_DATEIDNEXTDATE,1)
,ifnull(f.AMT_TAX,0)
,ifnull(f.DIM_DATEIDEARLIESTSOCANCELDATE,1)
,ifnull(f.DIM_DATEIDLATESTCUSTPOAGI,1)
,ifnull(f.DD_BUSINESSCUSTOMERPONO,'Not Set')
,ifnull(f.DIM_ROUTEID,1)
,ifnull(f.DIM_BILLINGDATEID,1)
,ifnull(f.DIM_CUSTOMERPAYMENTTERMSID,1)
,ifnull(f.DIM_SALESRISKCATEGORYID,1)
,ifnull(f.DD_CREDITREP,'Not Set')
,ifnull(f.DD_CREDITLIMIT,0)
,ifnull(f.DIM_CUSTOMERRISKCATEGORYID,1)
,ifnull(f.CT_FILLQTY_CRD,0)
,ifnull(f.CT_FILLQTY_PDD,0)
,ifnull(f.DIM_H1CUSTOMERID,1)
,ifnull(f.DIM_DATEIDSOITEMCHANGEDON,1)
,ifnull(f.DIM_SHIPPINGCONDITIONID,1)
,ifnull(f.DD_AFSALLOCATIONGROUPNO,'Not Set')
,ifnull(f.DIM_DATEIDREJECTION,1)
,ifnull(f.DD_CUSTOMERMATERIALNO,'Not Set')
,ifnull(f.DIM_BASEUOMID,1)
,ifnull(f.DIM_SALESUOMID,1)
,ifnull(f.AMT_UNITPRICEUOM,0)
,ifnull(f.DIM_DATEIDFIXEDVALUE,1)
,ifnull(f.DIM_PAYMENTTERMSID,1)
,ifnull(f.DIM_DATEIDNETDUEDATE,1)
,ifnull(f.DIM_MATERIALPRICEGROUP4ID,1)
,ifnull(f.DIM_MATERIALPRICEGROUP5ID,1)
,ifnull(f.AMT_SUBTOTAL3_ORDERQTY,0)
,ifnull(f.AMT_SUBTOTAL3INCUSTCONFIG_BILLING,0)
,ifnull(f.DIM_CUSTOMERMASTERSALESID,1)
,ifnull(f.DD_SALESORDERBLOCKED,'Not Set')
,ifnull(f.DD_SOCREATETIME,'Not Set')
,ifnull(f.DD_REQDELIVERYTIME,'Not Set')
,ifnull(f.DD_SOLINECREATETIME,'Not Set')
,ifnull(f.DD_DELIVERYTIME,'Not Set')
,ifnull(f.DD_PLANNEDGITIME,'Not Set')
,ifnull(f.DIM_DATEIDARPOSTING,1)
,ifnull(f.DIM_CURRENCYID_TRA,1)
,ifnull(f.DIM_CURRENCYID_GBL,1)
,ifnull(f.DIM_CURRENCYID_STAT,1)
,ifnull(f.AMT_EXCHANGERATE_STAT,1)
,ifnull(f.DD_ARUNNUMBER,'Not Set')
,ifnull(f.DIM_AVAILABILITYCHECKID,1)
,ifnull(f.DD_IDOCNUMBER,'Not Set')
,ifnull(f.DIM_INCOTERMID,1)
,ifnull(f.DD_INTERNATIONALARTICLENO,'Not Set')
,ifnull(f.DD_RELEASERULE,'Not Set')
,ifnull(f.DIM_DELIVERYPRIORITYID,1)
,ifnull(f.DD_REFERENCEDOCITEMNO,0)
,ifnull(f.DIM_SALESORDERPARTNERID,1)
,ifnull(f.DD_PURCHASEREQUISITION,'Not Set')
,ifnull(f.DD_ITEMOFREQUISITION,0)
,ifnull(f.CT_AFSCALCULATEDOPENQTY,0)
,ifnull(f.AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE,0)
,ifnull(f.DD_DOCUMENTCONDITIONNO,'Not Set')
,ifnull(f.AMT_SUBTOTAL1,0)
,ifnull(f.AMT_SUBTOTAL2,0)
,ifnull(f.AMT_SUBTOTAL5,0)
,ifnull(f.AMT_SUBTOTAL6,0)
,ifnull(f.DD_HIGHLEVELITEM,0)
,ifnull(f.DD_PODOCUMENTNO,'Not Set')
,ifnull(f.DD_PODOCUMENTITEMNO,0)
,ifnull(f.DD_POSCHEDULENO,0)
,ifnull(f.DD_PRODORDERNO,'Not Set')
,ifnull(f.DD_PRODORDERITEMNO,0)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS1ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS2ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS3ID,1)
,ifnull(f.CT_AFSOPENPOQTY,0)
,ifnull(f.CT_AFSINTRANSITQTY,0)
,ifnull(f.DIM_SCHEDULEDELIVERYBLOCKID,1)
,ifnull(f.DIM_CUSTOMERGROUP4ID,1)
,ifnull(f.DD_OPENCONFIRMATIONSEXISTS,'Not Set')
,ifnull(f.DD_CONDITIONNO,'Not Set')
,ifnull(f.DD_CREDITMGR,'Not Set')
,ifnull(f.DD_CLEAREDBLOCKEDSTS,'Not Set')
,ifnull(f.CT_AFSINRDDUNITS,0)
,ifnull(f.CT_AFSAFTERRDDUNITS,0)
,ifnull(f.DIM_AGREEMENTSID,1)
,ifnull(f.DIM_CHARGEBACKCONTRACTID,1)
,ifnull(f.DD_RO_MATERIALDESC,'Not Set')
,ifnull(f.DIM_CUSTOMERIDCBOWNER,1)
,ifnull(f.CT_COMMITTEDQTY,0)
,ifnull(f.DIM_DATEIDREQDELIVLINE,1)
,ifnull(f.DIM_MATERIALPRICINGGROUPID,1)
,ifnull(f.DIM_BILLINGBLOCKSALESDOCLVLID,1)
,ifnull(f.DD_DELIVERYINDICATOR,'Not Set')
,ifnull(f.DD_SHIPTOPARTYSTREET,'Not Set')
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS4ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS5ID,1)
,ifnull(f.DD_PURCHASEORDERITEM,'0')
,ifnull(f.DIM_MATERIALPRICEGROUP2ID,1)
,ifnull(f.DIM_PROJECTSOURCEID,1)
,ifnull(f.DD_BILLING_NO,'Not Set')
,ifnull(f.DIM_MDG_PARTID,1)
,ifnull(f.DIM_BWPRODUCTHIERARCHYID,1)
,ifnull(f.CT_FIRSTNONZEROCONFIRMQTY,0)
,ifnull(f.CT_VOLUME,0)
,ifnull(f.CT_NOTCONFIRMEDQTY,0)
,ifnull(f.DD_CUSTOMCUSTEXPPRICEINC,'Not Set')
,ifnull(f.DD_DOCUMENTFLOWGROUP,'Not Set')
,ifnull(f.DD_ORDERBY,'Not Set')
,ifnull(f.DD_CUSTOMPRICEMISSINGINC,'Not Set')
,ifnull(f.DIM_CUSTOMERENDUSERFORFTRADE,1)
,ifnull((select dim_dateid from dim_date where companycode = 'Not Set'   and datevalue = (select processing_date from tmp_dayofprocessing_var_fill)),1) DIM_DATEIDSNAPSHOT
,ifnull(dim_accordinggidatemto_emd,1)
,ifnull(dim_dateidexpectedship_emd,1)
,ifnull(dim_dateidrequested_emd,1)
,ifnull(ct_baseuomratio,0)
,ifnull(ct_baseuomratiokg,0)
,ifnull(ct_countsalesdocitem,0)
,ct_deliveryisfull
,ct_deliveryontime
,ct_deliveryontimecasual
,dd_ace_openorders
,ifnull(dd_backorderflag,'Not Set')
,dd_backorderreason
,dd_backorder_responsibility
,dd_deliveryisfull
,dd_deliveryontime
,dd_deliveryontimecasual
,dd_intercompflag
,dd_lsintercompflag
,dd_mercklsbackorderfilter
,ifnull(dd_ora_active_hold_info,0)
,ifnull(dd_ora_attribute11,0)
,ifnull(dd_ora_line_on_hold,0)
,ifnull(dd_ora_tradsales_flag,0)
,ifnull(dim_backorder_dateid,1)
,dim_commercialviewid
,dim_dateidactualgimerckls
,dim_dateidconfirmeddelivery_emd
,AMT_EXCHANGERATE_CUSTOM
,DD_OPENQTYCVRDBYMAT
,DD_OPENQTYCVRDBYPLANT
,ifnull(dim_clusterid,1)
,F.DW_INSERT_DATE
,F.DW_UPDATE_DATE
,ifnull(f.dim_countryhierpsid,1)
,ifnull(f.dim_countryhierarid,1)
,ifnull(f.dd_gsa_code,'Not Set')
,ifnull(f.dim_date_enddateloadingid,1)
,ifnull(f.fact_salesorderid,1)
,ifnull(f.dim_customer_shipto,1)
,ifnull(f.dim_productionschedulerid,1)
,ifnull(f.dim_gsamapimportid,1)
,ifnull(f.dd_inco1,'Not Set')
,ifnull(f.dd_inco2,'Not Set')
,ifnull(f.dd_ship_to_party,'Not Set')
,ifnull(prt.mtomts,'Not Set')
,IFNULL(F.DIM_ADDRESSID,1)
,ifnull(f.dd_CompleteInSingleDelivery,'Not Set')
,ifnull(f.dd_batch_split,'Not Set')
,ifnull(f.dd_delivery_group,'Not Set')
from fact_salesorder f
inner join dim_part prt on f.dim_partid = prt.dim_partid
inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
inner join dim_date_factory_calendar mlsrd on (
CASE
  WHEN UPPER(uph.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN decode(f.dim_dateidexpectedship_emd,1,f.dim_dateidrequested_emd,f.dim_dateidexpectedship_emd)
  when trim(prt.mtomts) = 'MTO' AND UPPER(uph.businessdesc) NOT LIKE '%RESEARCH SOLUTION%' then decode(f.dim_accordinggidatemto_emd,1,f.dim_dateidconfirmeddelivery_emd,f.dim_accordinggidatemto_emd)
  else decode(f.dim_dateidexpectedship_emd,1,f.dim_dateidrequested_emd,f.dim_dateidexpectedship_emd)
end) = mlsrd.dim_dateid
inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
inner join dim_salesorderitemstatus its on f.dim_salesorderitemstatusid = its.dim_salesorderitemstatusid
where
 TO_DATE(mlsrd.datevalue) = (select processing_date from tmp_dayofprocessing_var_fill)
 AND mlsrd.datevalue  <> '0001-01-01'
     AND f.DD_ACE_OPENORDERS = 'X'
     AND uph.businesssector = 'BS-02'
	 AND f.dd_fillratesnapshot_flag = 'Not Set' and concat(f.dd_salesdocno,f.dd_salesitemno) not in (select distinct concat(dd_salesdocno,dd_salesitemno) from fact_fillrate_snapshot)
     AND f.dd_scheduleno=dd_min_scheduleno;

/*Update flag for fillrate snapshot in Sales analysis*/

drop table if exists fillratesnapshot_flag_update;
create table fillratesnapshot_flag_update
as select distinct dd_salesdocno,dd_salesitemno
from fact_fillrate_snapshot;

update fact_salesorder f
set dd_fillratesnapshot_flag = 'Y'
from fact_salesorder f,fillratesnapshot_flag_update ff
where f.dd_salesdocno=ff.dd_salesdocno and f.dd_salesitemno=ff.dd_salesitemno
and ifnull(f.dd_fillratesnapshot_flag,'Not Set') = 'Not Set';

/*BI-5021 incorporate CCT logic with commercial view*/

drop table if exists tmp_upd_sales;
create table tmp_upd_sales as
select  distinct f.fact_fillrate_snapshotid,f.dim_customerid non_cct
,ff.dim_customerid cct_customerid
 from fact_fillrate_snapshot f
 inner join dim_customer c on f.dim_customerid = c.dim_customerid
                                                and  c.customernumber in
('0000169177'
,'0000172329'
,'0000183611'
,'0000197474'
,'0000241105'
,'0000241786'
,'0000241787'
,'0000242176'
,'0000242177'
,'0000243046'
,'0000243047'
,'0000243048')
 inner join emdtempocc4.fact_salesorder ff on right(f.dd_businesscustomerpono,instr(f.dd_businesscustomerpono,'/',1)-1 ) = ff.dd_salesdocno
 where right(f.dd_businesscustomerpono,instr(f.dd_businesscustomerpono,'/',1)-1 ) is not null;

update fact_fillrate_snapshot f
set f.dim_customerid = t.cct_customerid
from fact_fillrate_snapshot f
,tmp_upd_sales t, dim_bwproducthierarchy ph
where t.fact_fillrate_snapshotid =f.fact_fillrate_snapshotid
and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
and t.non_cct = f.dim_customerid
and f.dim_customerid <> t.cct_customerid;

update fact_fillrate_snapshot f
	set f.dim_commercialviewid=ds.dim_commercialviewid
	from dim_commercialview ds, emdtempocc4.dim_customer a, dim_bwproducthierarchy b, fact_fillrate_snapshot f
	where ds.SOLDTOCOUNTRY=a.COUNTRY
	and ds.UPPERPRODHIER=b.BUSINESS
	and f.DIM_BWPRODUCTHIERARCHYID=b.DIM_BWPRODUCTHIERARCHYID
	and f.dim_customerid=a.dim_customerid
	and f.dim_commercialviewid <> ds.dim_commercialviewid;

/*@Catalin recalculation for coveredby plant flag*/

drop table if exists fact_fillrate_snapshot_history;
create table fact_fillrate_snapshot_history as select a.* from fact_fillrate_snapshot a
 inner join dim_date_factory_calendar b on b.dim_Dateid = a.dim_dateidsnapshot  and b.datevalue > (select processing_date-1 from tmp_dayofprocessing_var_fill);

 update fact_fillrate_snapshot_history f
 set dd_openqtycvrdbyplant = 'Not Set'
 where ifnull(dd_openqtycvrdbyplant,'Not Set') <> 'Not Set';

drop table if exists tmp_diff;
create table tmp_diff as select dd_salesdocno, dd_salesitemno, dim_Dateidsnapshot,sum(ct_ScheduleQtySalesUnit)-sum(ct_DeliveredQty) diff
from fact_fillrate_snapshot_history
group by 1,2,3;

update fact_fillrate_snapshot_history a
set a.dd_openqtycvrdbyplant = 'X'
from fact_fillrate_snapshot_history a, tmp_diff b
where a.dd_salesdocno = b.dd_salesdocno
and a.dd_salesitemno = b.dd_salesitemno
and a.dim_Dateidsnapshot = b.dim_dateidsnapshot
and b.diff = 0;

drop table if exists tmp_sales_openqty_1;
create table tmp_sales_openqty_1
as
select
	dd1.datevalue
	,f_so.dd_salesdocno
	,f_so.dd_salesitemno
	,dp.partnumber
	,pl.PLANTCODE
	,d.deliverypriority
	, case when  UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' then DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue )
else (case when dp.mtomts = 'MTS' then DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue ) else DECODE( acgidmto.DateValue , '0001-01-01', cddemd.DateValue , acgidmto.DateValue ) end) end dim_dateidrequested_emd

	,sum(case
	         when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
			 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X'
			     then (case
				           when (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 then 0.0000
						   else (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
					   end)
			 when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000
			 else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
	      end) openqty
from fact_fillrate_snapshot_history f_so
	inner join dim_part dp on f_so.dim_partid = dp.dim_partid
	inner join dim_deliverypriority d on f_so.dim_deliverypriorityId = d.dim_deliverypriorityId
	inner join dim_date dd on f_so.dim_dateidrequested_emd = dd.dim_dateid
	inner join dim_date cddemd on f_so.dim_dateidconfirmeddelivery_emd = cddemd.dim_dateid
	inner join dim_salesdocumenttype sdt on f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
	inner join dim_salesorderheaderstatus sohs on f_so.dim_salesorderheaderstatusid = sohs.dim_salesorderheaderstatusid
	inner join dim_salesorderitemstatus sois on f_so.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
	inner join dim_date_factory_calendar esdemd on f_so.dim_dateidexpectedship_emd = esdemd.dim_dateid
	inner join dim_date_factory_calendar acgidmto on f_so.dim_accordinggidatemto_emd = acgidmto.dim_dateid
	inner join dim_bwproducthierarchy bw on f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
	inner join dim_plant pl on f_so.dim_plantid = pl.dim_plantid
inner join dim_date_factory_calendar dd1 on f_so.dim_dateidsnapshot = dd1.dim_Dateid
where dd_openqtycvrdbyplant <>'X'
group by dd1.datevalue,dd_salesdocno, dd_salesitemno, dp.partnumber, pl.PLANTCODE, d.deliverypriority, case when  UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' then DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue )
else (case when dp.mtomts = 'MTS' then DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue ) else DECODE( acgidmto.DateValue , '0001-01-01', cddemd.DateValue , acgidmto.DateValue ) end) end

having sum(case
	         when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
			 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X'
			     then (case
				           when (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 then 0.0000
						   else (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
					   end)
			 when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000
			 else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
	      end) > 0;

drop table if exists tmp_sales_openqty;
create table tmp_sales_openqty
as
select
	row_number() over(order by dim_dateidrequested_emd asc, dd_salesdocno asc,datevalue asc) proc_order
	,dd_salesdocno
	,dd_salesitemno
	,datevalue
	,partnumber
	,PLANTCODE
	,openqty
from tmp_sales_openqty_1;

drop table if exists tmp_sales_openqty_cum;
create table tmp_sales_openqty_cum
as
select
	 a.datevalue
	,a.proc_order
	,a.dd_salesdocno
	,a.dd_salesitemno
	,a.partnumber
	,a.PLANTCODE
	,a.openqty
	,'Not Set' cvrd_by_plant
	,sum(a.openqty) over(partition by partnumber,PLANTCODE,datevalue order by a.proc_order rows between unbounded preceding and current row) cum_openqty
from tmp_sales_openqty a
group by a.proc_order, a.dd_salesdocno, a.dd_salesitemno, a.partnumber, a.PLANTCODE, a.openqty,a.datevalue;

drop table if exists tmp_uprestrictedstock;
create table tmp_uprestrictedstock
as
select
	dp.partnumber
	,pl.PLANTCODE
	,sum(f.ct_StockQty) ct_StockQty
	,dd.datevalue
from fact_inventoryhistory f
	inner join dim_part dp on dp.dim_partid = f.dim_partid
	inner join dim_plant pl on f.dim_plantid = pl.dim_plantid
inner join dim_Date dd on f.dim_dateidsnapshot = dd.dim_dateid
group by dp.partnumber, pl.PLANTCODE,dd.datevalue;

update tmp_sales_openqty_cum a
set a.cvrd_by_plant = 'X'
from tmp_sales_openqty_cum a, tmp_uprestrictedstock b
where a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and a.cum_openqty <= b.ct_StockQty and a.datevalue = b.datevalue;

/* check if the remaining stock can be allocated */

drop table if exists tmp_unalocatedstock;
create table tmp_unalocatedstock
as
select a.partnumber,a.PLANTCODE,max(b.ct_StockQty) - max(a.cum_openqty) remaining_stock ,a.datevalue
from tmp_sales_openqty_cum a
	inner join tmp_uprestrictedstock b on a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and a.datevalue = b.datevalue
where a.cvrd_by_plant = 'X'
group by a.partnumber,a.PLANTCODE,a.datevalue
having max(b.ct_StockQty) - max(a.cum_openqty) > 0;

/* add materials that did not manage to cover any order but have stock */

drop table if exists tmp_materialsnotcovered;
create table tmp_materialsnotcovered
as
select distinct a.partnumber,a.PLANTCODE,a.datevalue from tmp_sales_openqty_cum a where not exists
 (select 1 from tmp_sales_openqty_cum b where a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and b.cvrd_by_plant = 'X' and a.datevalue = b.datevalue);

insert into tmp_unalocatedstock(partnumber, PLANTCODE, remaining_stock,datevalue)
select partnumber, PLANTCODE, ct_StockQty,datevalue from tmp_uprestrictedstock where (partnumber,PLANTCODE,datevalue) in (select partnumber,PLANTCODE,datevalue from tmp_materialsnotcovered) and ct_StockQty > 0;

drop table if exists tmp_sowithunalocatedstock;
create table tmp_sowithunalocatedstock
as
select a.*
from tmp_sales_openqty_cum a
	inner join tmp_unalocatedstock b on a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and a.datevalue = b.datevalue
where a.openqty <= b.remaining_stock
	and a.cvrd_by_plant = 'Not Set';
 execute script emd586.emd_covered_by_plant_history('tmp_sowithunalocatedstock','tmp_unalocatedstock','tmp_sales_openqty_cum');
/* end check if the remaining stock can be allocated */

update fact_fillrate_snapshot_history f
set f.dd_openqtycvrdbyplant = trim(t.cvrd_by_plant)
from fact_fillrate_snapshot_history f inner join dim_date dd on f.dim_dateidsnapshot = dd.dim_dateid, tmp_sales_openqty_cum t
where f.dd_salesdocno = t.dd_salesdocno and f.dd_salesitemno = t.dd_salesitemno and dd.datevalue = t.datevalue
	and f.dd_openqtycvrdbyplant <> trim(t.cvrd_by_plant) ;

drop table if exists tmp_sales_openqty_1;
drop table if exists tmp_sales_openqty;
 drop table if exists tmp_sales_openqty_cum;
 drop table if exists tmp_unalocatedstock;
 drop table if exists tmp_sowithunalocatedstock;
drop table if exists tmp_materialsnotcovered;

update fact_fillrate_snapshot_history ffs
set  ffs.dd_openqtycvrdbyplant  = 'X'
from fact_fillrate_snapshot_history ffs
 inner join dim_date_factory_calendar d1 on d1.dim_dateid = ffs.dim_dateidactualgi
 inner join dim_date_factory_calendar d2 on d2.dim_dateid = ffs.dim_dateidsnapshot
 inner join dim_salesdocumenttype sdt on ffs.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
where
d1.datevalue <= d2.datevalue
and (case when ffs.Dim_SalesOrderRejectReasonid <>
1 then 0.0000 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')
AND ffs.dd_ItemRelForDelv = 'X' then (case when (ffs.ct_ScheduleQtySalesUnit - (ffs.ct_ShippedAgnstOrderQty - ffs.ct_CmlQtyReceived))
< 0 then 0.0000 else (ffs.ct_ScheduleQtySalesUnit - (ffs.ct_ShippedAgnstOrderQty - ffs.ct_CmlQtyReceived)) end)
 when (ffs.ct_ScheduleQtySalesUnit - ffs.ct_ShippedAgnstOrderQty) < 0
 then 0.0000 else (ffs.ct_ScheduleQtySalesUnit - ffs.ct_ShippedAgnstOrderQty) end) = 0
and ffs.ct_scheduleqtysalesunit <>0
and ffs.dd_openqtycvrdbyplant <> 'X';

drop table if exists tmp_distinct;
create table tmp_distinct as select distinct a.fact_fillrate_snapshotid aa,a.dd_openqtycvrdbyplant
from  fact_fillrate_snapshot_history a inner join fact_fillrate_snapshot_history b
on a.dd_Salesdocno = b.dd_salesdocno
and a.dd_salesitemno = b.dd_salesitemno
and a.dim_partid = b.dim_partid
and a.fact_fillrate_snapshotid <> b.fact_fillrate_snapshotid
and a.dim_Dateidsnapshot = b.dim_dateidsnapshot and a.dd_openqtycvrdbyplant<>b.dd_openqtycvrdbyplant
where a.ct_scheduleqtysalesunit = 0;

update fact_fillrate_snapshot_history a
set a.dd_openqtycvrdbyplant = b.dd_openqtycvrdbyplant
from fact_fillrate_snapshot_history a
    ,tmp_distinct b
where a.fact_fillrate_snapshotid = b.aa
and a.dd_openqtycvrdbyplant <> 'X';

drop table if exists tmp_tbp;
create table tmp_tbp as
select distinct a.fact_fillrate_snapshotid , b.dd_openqtycvrdbyplant,a.dim_dateidsnapshot,a.dd_scheduleno from fact_fillrate_snapshot_history a,fact_fillrate_snapshot_history b
where a.dd_salesdocno = b.dd_salesdocno
and a.dd_Salesitemno = b.dd_salesitemno
and a.dd_scheduleno <> b.dd_scheduleno
and a.dim_dateidsnapshot = b.dim_Dateidsnapshot
and a.ct_scheduleqtysalesunit = 0
and b.ct_scheduleqtysalesunit <>0
and a.dd_openqtycvrdbyplant <> b.dd_openqtycvrdbyplant;

update fact_fillrate_snapshot_history a
set a.dd_openqtycvrdbyplant  = b.dd_openqtycvrdbyplant
from fact_fillrate_snapshot_history a,tmp_tbp b
where a.facT_fillrate_snapshotid = b.facT_fillrate_snapshotid
and a.dim_dateidsnapshot = b.dim_dateidsnapshot
and a.ct_scheduleqtysalesunit = 0
and a.dd_Scheduleno = b.dd_scheduleno
and b.dd_openqtycvrdbyplant ='X'
and a.dd_openqtycvrdbyplant <> b.dd_openqtycvrdbyplant;

update fact_fillrate_snapshot a
set a.dd_openqtycvrdbyplant = b.dd_openqtycvrdbyplant
from fact_fillrate_snapshot a, fact_fillrate_snapshot_history b
where a.fact_fillrate_snapshotid = b.fact_fillrate_snapshotid
and  a.dd_openqtycvrdbyplant <> b.dd_openqtycvrdbyplant;
drop table if exists fact_fillrate_snapshot_history;
drop table if exists tmp_diff;
drop table if exists tmp_distinct;
drop table if exists tmp_tbp;
drop table if exists tmp_upd_sales;



drop table if exists good_fillrate_date;
create table good_fillrate_date
as select distinct dd_salesdocno,dd_salesitemno,min(dd_scheduleno) dd_scheduleno,min(datevalue) mindate
from fact_fillrate_snapshot f,dim_date dd
where f.dim_dateidsnapshot=dd.dim_dateid
group by dd_salesdocno,dd_salesitemno;

update fact_fillrate_snapshot f
set dd_duplicate_flag='No'
from fact_fillrate_snapshot f,good_fillrate_date g,dim_date dd
where f.dd_salesdocno=g.dd_salesdocno and f.dd_salesitemno=g.dd_salesitemno and f.dd_scheduleno=g.dd_scheduleno
and f.dim_dateidsnapshot=dd.dim_dateid
and dd.datevalue=g.mindate;

update fact_fillrate_snapshot f
set dd_duplicate_flag='Yes'
from fact_fillrate_snapshot f
where concat(dd_salesdocno,dd_salesitemno,dd_scheduleno) not in
(select distinct concat(dd_salesdocno,dd_salesitemno,dd_scheduleno) from good_fillrate_date);

update fact_fillrate_snapshot
set ct_countsalesdocitem_NEW=ct_countsalesdocitem
from fact_fillrate_snapshot
where ct_countsalesdocitem_NEW<>ct_countsalesdocitem;

update fact_fillrate_snapshot f
set DD_OPENQTYCVRDBYPLANT=case when trim(DD_OPENQTYCVRDBYPLANT) = 'Not Set'
and (case when Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when
sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')
AND dd_ItemRelForDelv = 'X' then (case when (ct_ScheduleQtySalesUnit -
(ct_ShippedAgnstOrderQty - ct_CmlQtyReceived)) < 0 then 0.0000 else
(ct_ScheduleQtySalesUnit - (ct_ShippedAgnstOrderQty -ct_CmlQtyReceived)) end)
when (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) < 0 then 0.0000 else
 (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) end) =0 then 'X'
else DD_OPENQTYCVRDBYPLANT end
from  fact_fillrate_snapshot f, dim_salesdocumenttype sdt,dim_date d
where f.dim_salesdocumenttypeid=sdt.dim_salesdocumenttypeid
and f.dim_dateidsnapshot=d.dim_dateid
and d.datevalue= (select processing_date from tmp_dayofprocessing_var_fill)
and DD_OPENQTYCVRDBYPLANT<>case when trim(DD_OPENQTYCVRDBYPLANT) = 'Not Set'
and (case when Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when
sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')
AND dd_ItemRelForDelv = 'X' then (case when (ct_ScheduleQtySalesUnit -
(ct_ShippedAgnstOrderQty - ct_CmlQtyReceived)) < 0 then 0.0000 else
(ct_ScheduleQtySalesUnit - (ct_ShippedAgnstOrderQty -ct_CmlQtyReceived)) end)
when (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) < 0 then 0.0000 else
 (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) end) =0 then 'X'
else DD_OPENQTYCVRDBYPLANT end;


drop table if exists tmp_MerckLSFillRate;
create table tmp_MerckLSFillRate as
select
dd_salesdocno,
dd_salesitemno,
DIM_DATEIDSNAPSHOT,
case when (SUM(case when DD_OPENQTYCVRDBYPLANT = 'X' and upper(DD_DUPLICATE_FLAG)='NO' then ct_countsalesdocitem_new else 0 end) > COUNT( DISTINCT dd_salesdocno || dd_salesitemno)) then 100
else SUM(case when DD_OPENQTYCVRDBYPLANT = 'X' and upper(DD_DUPLICATE_FLAG)='NO' then ct_countsalesdocitem_new else 0 end) / COUNT( DISTINCT dd_salesdocno || dd_salesitemno) * 100
end as dd_flag
from fact_fillrate_snapshot
group by DD_SALESDOCNO,dd_salesitemno,DIM_DATEIDSNAPSHOT;

update fact_fillrate_snapshot f
set DD_OPENQTYCVRDBYPLANT = 'X'
from  fact_fillrate_snapshot f, dim_salesdocumenttype sdt, dim_date dd
where
DD_OPENQTYCVRDBYPLANT ='Not Set'
and (case when Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND dd_ItemRelForDelv = 'X' then (case when (ct_ScheduleQtySalesUnit - (ct_ShippedAgnstOrderQty - ct_CmlQtyReceived)) < 0 then 0.0000 else (ct_ScheduleQtySalesUnit - (ct_ShippedAgnstOrderQty -ct_CmlQtyReceived)) end) when (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) < 0 then 0.0000 else (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) end) =0
and f.dim_dateidsnapshot = dd.dim_dateid
and dd.datevalue= (select processing_date from tmp_dayofprocessing_var_fill)
and  f.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid;

update fact_fillrate_snapshot f
set  dd_MerckLSFillRateflag = case when dd_flag =100 then 'Yes' else 'No' end
from tmp_MerckLSFillRate tmp, fact_fillrate_snapshot f
  where f.dd_salesdocno = tmp.dd_salesdocno
    and f.dd_salesitemno = tmp.dd_salesitemno
    and f.DIM_DATEIDSNAPSHOT = tmp.DIM_DATEIDSNAPSHOT;

/*Disabled 2017-10-27 Roxana D - as requested
update fact_fillrate_snapshot   f
set f.dim_clusterid = dc.dim_clusterid
from fact_fillrate_snapshot   f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> dc.dim_clusterid

update emd586.fact_fillrate_snapshot f
set f.dim_clusterid = ff.dim_clusterid
from emd586.fact_fillrate_snapshot f,EMDPhoenixDAD.fact_fillrate_snapshot ff
where f.fact_fillrate_snapshotid = ff.fact_fillrate_snapshotid
	and f.dim_projectsourceid=6
	and f.dim_clusterid <> ff.dim_clusterid
*/

/*Add dd_primaryproductionlocation Roxana D 2017-11-01*/

update FACT_FILLRATE_SNAPSHOT f
set f.DD_PRIMARYPRODUCTIONLOCATION = mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
FROM  FACT_FILLRATE_SNAPSHOT f
inner join  dim_cluster dc on dc.dim_clusterid = f.dim_clusterid
inner join dim_bwproducthierarchy ph on f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
   and businesssector = 'BS-02'
inner join (select distinct primary_production_location,primary_production_location_name
from dim_mdg_part ) mdg on mdg.primary_production_location = dc.primary_manufacturing_site
where f.DD_PRIMARYPRODUCTIONLOCATION <> mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
and f.dim_dateidsnapshot = ifnull((select dim_dateid from dim_date where companycode = 'Not Set'   and datevalue = (select processing_date from tmp_dayofprocessing_var_fill)),1);
/*End*/

	update fact_fillrate_snapshot f_so
set dim_mercklsconforreqdateid = mlsrd.dim_dateid
from fact_fillrate_snapshot f_so,dim_date_factory_calendar mlsrd, dim_part prt, dim_bwproducthierarchy bw
where f_so.Dim_Partid = prt.Dim_Partid
and f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
and
CASE WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN f_so.dim_dateidexpectedship_emd ELSE
  CASE WHEN trim(prt.mtomts) = 'MTO' THEN f_so.dim_accordinggidatemto_emd
      ELSE f_so.dim_dateidexpectedship_emd END END=mlsrd.dim_dateid
and dim_mercklsconforreqdateid <> mlsrd.dim_dateid;

update fact_fillrate_snapshot f
set f.dd_CustomerPONo=ff.dd_CustomerPONo
from fact_fillrate_snapshot f,fact_salesorder ff
where f.dd_salesdocno=ff.dd_salesdocno and f.dd_salesitemno=ff.dd_salesitemno
and f.dd_scheduleno=ff.dd_scheduleno
and f.dd_CustomerPONo='Not Set'
and f.dd_CustomerPONo<>ff.dd_CustomerPONo;

update emd586.fact_fillrate_snapshot f
set  f.dd_CustomerPONo=ff.dd_CustomerPONo
from emd586.fact_fillrate_snapshot f,fact_fillrate_snapshot ff
where f.fact_fillrate_snapshotid=ff.fact_fillrate_snapshotid
and f.dd_salesdocno=ff.dd_salesdocno and to_char(f.dd_salesitemno)=to_char(ff.dd_salesitemno)
and f.dd_scheduleno=ff.dd_scheduleno and f.dim_dateidsnapshot=ff.dim_dateidsnapshot
and f.dd_CustomerPONo='Not Set'
and f.dd_CustomerPONo<>ff.dd_CustomerPONo;

/* 10.10.2017  Fix ShipToHeader Roxana D */

drop table if exists tmp_fillrate;
create table tmp_fillrate as
select max(dim_customer_shipto) as dim_customer_shipto ,dd_salesdocno , dim_projectsourceid
from fact_salesorder
group by dd_salesdocno, dim_projectsourceid;

update fact_fillrate_snapshot f
set f.dim_customer_shipto = s.dim_customer_shipto
from fact_fillrate_snapshot f, tmp_fillrate s
where f.dd_salesdocno = s.dd_salesdocno
and f.dim_projectsourceid = s.dim_projectsourceid
and f.dim_customer_shipto = 1;

update emd586.fact_fillrate_snapshot f
set f.dim_customer_shipto = s.dim_customer_shipto
from emd586.fact_fillrate_snapshot f, tmp_fillrate s
where
	f.dd_salesdocno = s.dd_salesdocno
	and f.dim_projectsourceid = s.dim_projectsourceid
	and f.dim_customer_shipto = 1
	and f.dim_customer_shipto <> s.dim_customer_shipto;

drop table if exists tmp_fillrate;

update fact_fillrate_snapshot f
set DD_OPENQTYCVRDBYPLANT=case when trim(DD_OPENQTYCVRDBYPLANT) = 'Not Set'
and (case when Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when
sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')
AND dd_ItemRelForDelv = 'X' then (case when (ct_ScheduleQtySalesUnit -
(ct_ShippedAgnstOrderQty - ct_CmlQtyReceived)) < 0 then 0.0000 else
(ct_ScheduleQtySalesUnit - (ct_ShippedAgnstOrderQty -ct_CmlQtyReceived)) end)
when (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) < 0 then 0.0000 else
 (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) end) =0 then 'X'
else DD_OPENQTYCVRDBYPLANT end
from  fact_fillrate_snapshot f, dim_salesdocumenttype sdt,dim_date d
where f.dim_salesdocumenttypeid=sdt.dim_salesdocumenttypeid
and f.dim_dateidsnapshot=d.dim_dateid
and d.datevalue= (select processing_date from tmp_dayofprocessing_var_fill)
and DD_OPENQTYCVRDBYPLANT<>case when trim(DD_OPENQTYCVRDBYPLANT) = 'Not Set'
and (case when Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when
sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')
AND dd_ItemRelForDelv = 'X' then (case when (ct_ScheduleQtySalesUnit -
(ct_ShippedAgnstOrderQty - ct_CmlQtyReceived)) < 0 then 0.0000 else
(ct_ScheduleQtySalesUnit - (ct_ShippedAgnstOrderQty -ct_CmlQtyReceived)) end)
when (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) < 0 then 0.0000 else
 (ct_ScheduleQtySalesUnit - ct_ShippedAgnstOrderQty) end) =0 then 'X'
else DD_OPENQTYCVRDBYPLANT end;

update emd586.fact_fillrate_snapshot f
set  f.DD_OPENQTYCVRDBYPLANT=ff.DD_OPENQTYCVRDBYPLANT
from emd586.fact_fillrate_snapshot f,fact_fillrate_snapshot ff,dim_date d
where f.fact_fillrate_snapshotid=ff.fact_fillrate_snapshotid
and f.dd_salesdocno=ff.dd_salesdocno and to_char(f.dd_salesitemno)=to_char(ff.dd_salesitemno)
and f.dd_scheduleno=ff.dd_scheduleno and f.dim_dateidsnapshot=ff.dim_dateidsnapshot
and f.DD_OPENQTYCVRDBYPLANT<>ff.DD_OPENQTYCVRDBYPLANT
and f.dim_dateidsnapshot=d.dim_dateid
and d.datevalue= (select processing_date from tmp_dayofprocessing_var_fill);
/* End ShipTo Header */

drop table if exists tmp_dayofprocessing_var_fill;
drop table if exists tmp_loaded_datevalues;


/*2017-12-13 Add dimension for BIFILTERS_PRE_PRD Roxana D*/


update fact_FILLRATE_snapshot f
set f.dim_BIFILTERS_PRE_PRDId = ifnull(d.DIM_BIFILTERS_PRE_PRDid, 1)
from fact_FILLRATE_snapshot f, DIM_BIFILTERS_PRE_PRD d
where f.dim_projectsourceid = d.dim_projectsourceid
and case when f.DD_BIFILTERS_PRE_PRD_NEW = 'X' then 'Yes' else 'No' end = TRIM(d.BIFILTERS_PRE_PRD_CODE)
AND f.dim_BIFILTERS_PRE_PRDId <> ifnull(d.DIM_BIFILTERS_PRE_PRDid, 1);

/**/