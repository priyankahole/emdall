/**********************************************************************************/
/*  Script : vw_bi_populate_inventoryaging_fact.sql                               */
/*  Description : This script is recreated to allocate total stock quantity into  */
/*                each material movement quantity to reflect actual stock aging.  */
/*  Created On : July 2, 2013                                                     */
/*  Create By : Hiten Suthar                                                      */
/*                                                                                */
/*  Change History                                                                */
/*  Date         CVSversion  By          Description                              */
/*  02 July 2013  1.0         Hiten      New Script                               */
/*  31 July 2013  1.42        Ashu       Optimized an Insert statement            */
/*  14 Aug 2013   1.47        Hiten      Only latest ex rate                      */
/*                                       and added MARC, STO and WIP stock        */
/*  27 Aug 2013   1.54        Hiten      Default for null column values           */
/*  27 Aug 2013   1.55        Shanthi    Checking in Profit Center dimension      */
/*  28 Aug 2013   1.56	      Shanthi    New Field WIP Qty from PRod Order	  */
/*  10 Sep 2013   1.57        Lokesh	 Currency changes			  */
/*  15 Oct 2013   1.81        Issam 	 Added material movement measures	  */
/*  01 Nov 2013   1.86        Hiten 	 On Hand Amount Adjustment       	  */
/*  20 Feb 2014	  1.87		  Nicoleta	 Added dim_DateidExpiryDate       */
/*  19 May 2014	  1.98		  Hiten	 special stock ind citeria change for MARD  */
/**********************************************************************************/

DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

DROP TABLE IF EXISTS fact_inventoryaging_tmp_populate;
CREATE TABLE fact_inventoryaging_tmp_populate
like  fact_inventoryaging  INCLUDING DEFAULTS INCLUDING IDENTITY;

ALTER TABLE fact_inventoryaging_tmp_populate ADD PRIMARY KEY (fact_inventoryagingid);


delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryaging_tmp_populate',ifnull(max(fact_inventoryagingid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
FROM fact_inventoryaging_tmp_populate;

DELETE FROM fact_inventoryaging_tmp_populate;

DELETE FROM MCHB WHERE MCHB_CLABS = 0 AND MCHB_CUMLM = 0 AND MCHB_CINSM = 0 AND MCHB_CEINM = 0 AND MCHB_CSPEM = 0;
DELETE FROM MARD WHERE MARD_LABST = 0 AND MARD_UMLME = 0 AND MARD_INSME = 0 AND MARD_EINME = 0 AND MARD_SPEME = 0
			AND MARD_KSPEM = 0 AND MARD_KINSM = 0 AND MARD_KEINM = 0 AND MARD_KLABS = 0;
DELETE FROM MSLB WHERE MSLB_LBLAB = 0 AND MSLB_LBINS = 0 AND MSLB_LBEIN = 0;
DELETE FROM MSKU WHERE MSKU_KULAB = 0 AND MSKU_KUINS = 0 AND MSKU_KUEIN = 0;
DELETE FROM MARC WHERE MARC_UMLMC = 0 AND MARC_TRAME = 0 AND MARC_GLGMG = 0 AND MARC_BWESB = 0;
DELETE FROM MARC WHERE MARC_LVORM = 'X';

delete from mard 
where exists (select 1 from mard b where mard.mard_matnr = b.mard_matnr and mard.mard_werks = b.mard_werks 
			and mard.mard_lgort <> b.mard_lgort and mard.mard_labst = -1 * b.mard_labst
			and b.mard_labst > 0);

UPDATE MCHB SET MCHB_CHARG = 'Not Set' WHERE MCHB_CHARG IS NULL;
UPDATE MSLB SET MSLB_CHARG = 'Not Set' WHERE MSLB_CHARG IS NULL;
UPDATE MSKU SET MSKU_CHARG = 'Not Set' WHERE MSKU_CHARG IS NULL;

UPDATE fact_materialmovement SET dd_BatchNumber = 'Not Set' WHERE dd_BatchNumber IS NULL;

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'dim_storagelocation';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'dim_storagelocation', IFNULL(MAX(dim_storagelocationid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM dim_storagelocation WHERE dim_storagelocationid <> 1;

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
				Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent) 
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_storagelocation') + row_number() over (order by ''),tty.*
FROM
   (SELECT DISTINCT MCHB_LGORT,
                   MCHB_LGORT,
                   'Not Set' Division,
                   'Not Set' FreezingBookInventory,
                   'Not Set' MRPExclude,
                   'Not Set' StorageResource,
                   'Not Set' PartnerStorageLocation,
                   'Not Set' StorageSalesOrg,
                   'Not Set' DistributionChannel,
                   'Not Set' ShipReceivePoint ,
                   MCHB_WERKS Plant ,
                   'Not Set' InTransit,
                   current_timestamp RowStartDate,
                   1 RowIsCurrent
     FROM MCHB m
    WHERE MCHB_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE LocationCode = MCHB_LGORT AND Plant = MCHB_WERKS)) tty;

 UPDATE NUMBER_FOUNTAIN 
 SET max_id = ifnull(( select max(dim_storagelocationid) from dim_storagelocation), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
 WHERE table_name = 'dim_storagelocation';

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
				Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent) 
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_storagelocation') + row_number() over (order by ''),tty.*
FROM
   (SELECT DISTINCT MARD_LGORT,
                   MARD_LGORT,
                   'Not Set' Division,
                   'Not Set' FreezingBookInventory ,
                   'Not Set' MRPExclude,
                   'Not Set' StorageResource,
                   'Not Set' PartnerStorageLocation,
                   'Not Set' StorageSalesOrg,
                   'Not Set' DistributionChannel,
                   'Not Set' ShipReceivePoint,
                   MARD_WERKS Plant ,
                   'Not Set' InTransit,
                   current_timestamp RowStartDate,
                   1 RowIsCurrent          
     FROM MARD m
    WHERE MARD_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE LocationCode = MARD_LGORT AND Plant = MARD_WERKS)) tty;

DELETE FROM fact_materialmovement_tmp_invagin;

INSERT INTO fact_materialmovement_tmp_invagin
  (Dim_Partid, Dim_Plantid, Dim_MovementTypeid, dd_BatchNumber, dd_MaterialDocYearItem)
  SELECT x.Dim_Partid, x.Dim_Plantid, x.Dim_MovementTypeid, ifnull(x.dd_BatchNumber,'Not Set') dd_BatchNumber,
      max(concat(x.dd_MaterialDocYear,LPAD(x.dd_MaterialDocNo,10,'0'),LPAD(x.dd_MaterialDocItemNo,5,'0')))
  FROM fact_materialmovement x
        INNER JOIN dim_movementtype dmt ON x.Dim_MovementTypeid = dmt.Dim_MovementTypeid
  WHERE dmt.MovementType in ('101','131','105','501','511','521')
  GROUP BY x.Dim_Partid, x.Dim_Plantid, x.Dim_MovementTypeid, ifnull(x.dd_BatchNumber,'Not Set');


DROP TABLE IF EXISTS tmp_MARD_TotStk_001;
CREATE TABLE tmp_MARD_TotStk_001 AS
SELECT MARD_MATNR PartNumber,
	MARD_WERKS PlantCode, 
	MARD_LGORT StorLocCode, 
	convert(varchar(10),'Not Set') BatchNumber,
	MARD_UMLME MARD_UMLME_1,
	MARD_LABST MARD_LABST_2, 
	MARD_SPEME MARD_SPEME_3, 
	MARD_INSME MARD_INSME_4, 
	MARD_EINME MARD_EINME_5,
       (MARD_LABST + MARD_EINME + MARD_INSME + MARD_SPEME + MARD_UMLME) TotalStock,
	MARD_KSPEM, 
	MARD_KINSM, 
	MARD_KEINM,
	MARD_KLABS, 
	MARD_RETME,
	MARD_DLINL,
	MARD_ERSDA
from MARD mrd
WHERE NOT EXISTS (SELECT 1 FROM MCHB mch
		   WHERE MARD_MATNR = MCHB_MATNR
			 AND MARD_WERKS = MCHB_WERKS
			 AND MARD_LGORT = MCHB_LGORT);




INSERT INTO tmp_MARD_TotStk_001
SELECT MCHB_MATNR PartNumber,
	MCHB_WERKS PlantCode, 
	MCHB_LGORT StorLocCode, 
	MCHB_CHARG BatchNumber,
	MCHB_CUMLM MARD_UMLME_1,
	MCHB_CLABS MARD_LABST_2, 
	MCHB_CSPEM MARD_SPEME_3, 
	MCHB_CINSM MARD_INSME_4, 
	MCHB_CEINM MARD_EINME_5,
       (MCHB_CLABS + MCHB_CEINM + MCHB_CINSM + MCHB_CSPEM + MCHB_CUMLM) TotalStock,
	0 MARD_KSPEM, 
	0 MARD_KINSM, 
	0 MARD_KEINM,
	0 MARD_KLABS, 
	MCHB_CRETM MARD_RETME,
	MCHB_ERSDA MARD_DLINL,
	MCHB_ERSDA MARD_ERSDA 
from MCHB;



DROP TABLE IF EXISTS tmp_MARD_TotStk_001_U;
CREATE TABLE tmp_MARD_TotStk_001_U AS
SELECT PartNumber,
	PlantCode,
	SUM(TotalStock) TotalStockQty
FROM tmp_MARD_TotStk_001
GROUP BY PartNumber,
	PlantCode;


DROP TABLE IF EXISTS tmp_MARD_TotStk_t01;
CREATE TABLE tmp_MARD_TotStk_t01 AS
SELECT PartNumber,
	PlantCode, 
	StorLocCode, 
	BatchNumber,
	TotalStock,
    ROW_NUMBER() OVER(PARTITION BY PartNumber, PlantCode 
    			ORDER BY MARD_UMLME_1 desc, 
				MARD_LABST_2 desc, 
				MARD_SPEME_3 desc, 
				MARD_INSME_4 desc, 
				MARD_EINME_5 desc, 
				StorLocCode, BatchNumber) M_RowSeqNo
FROM tmp_MARD_TotStk_001;

DROP TABLE IF EXISTS tmp_MARD_TotStk_t02;
CREATE TABLE tmp_MARD_TotStk_t02 AS
SELECT a.PartNumber,
	a.PlantCode, 
	a.StorLocCode, 
	a.BatchNumber,
	a.TotalStock,
        a.M_RowSeqNo,
	SUM(b.TotalStock) - a.TotalStock TotalStockCUM_A,
	SUM(b.TotalStock) TotalStockCUM_B
FROM tmp_MARD_TotStk_t01 a
	inner join tmp_MARD_TotStk_t01 b on a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode
WHERE a.M_RowSeqNo >= b.M_RowSeqNo
GROUP BY a.PartNumber,
	a.PlantCode, 
	a.StorLocCode, 
	a.BatchNumber,
	a.TotalStock,
        a.M_RowSeqNo;


DROP TABLE IF EXISTS materialmovement_tmp_001_pre;
CREATE TABLE materialmovement_tmp_001_pre AS
      SELECT cx.fact_materialmovementid,
          cx.ct_Quantity,
	      dp1.PartNumber, 
		  dpl.PlantCode,  
		  dt1.DateValue, 
		  cx.dd_MaterialDocNo, cx.dd_MaterialDocItemNo, cx.dd_DocumentScheduleNo
      FROM fact_materialmovement cx 
	    INNER JOIN dim_movementtype dmt ON cx.Dim_MovementTypeid = dmt.Dim_MovementTypeid AND cx.dd_debitcreditid = 'Debit'
				AND dmt.MovementType in ('101','105','131','222','301','305','309','351','453','501','503','505','511','521','523','525','541','561','602','621','631','643','645','647','644','653','655','657','673','701')
        INNER JOIN dim_Plant dpl ON cx.dim_Plantid = dpl.dim_Plantid
        INNER JOIN dim_Part dp1 ON cx.dim_Partid = dp1.dim_Partid
        INNER JOIN dim_date dt1 ON dt1.Dim_Dateid = cx.dim_DateIDPostingDate
	    INNER JOIN tmp_MARD_TotStk_001_U mrd ON dp1.PartNumber = mrd.PartNumber AND dpl.PlantCode = mrd.PlantCode
	    INNER JOIN dim_specialstock spst ON spst.dim_specialstockid = cx.dim_specialstockid
     WHERE cx.ct_Quantity > 0 
	   AND spst.specialstockindicator not in ('O','V','W','E','Q')
	   AND (dmt.MovementType not in ('351','641','643','645','647')
            OR (dmt.MovementType in ('351','641','643','645','647')
				AND NOT EXISTS (SELECT 1 
						FROM fact_materialmovement cx1 
							INNER JOIN dim_movementtype dmt1 ON cx1.Dim_MovementTypeid = dmt1.Dim_MovementTypeid
						WHERE cx1.dd_debitcreditid = 'Debit' AND dmt1.MovementType = '101'
						     AND cx1.dim_Partid = cx.dim_Partid AND cx1.dim_specialstockid = cx.dim_specialstockid
						     AND cx1.dd_DocumentNo = cx.dd_DocumentNo AND cx1.dd_DocumentItemNo = cx.dd_DocumentItemNo
						     AND cx1.ct_QtyEntryUOM = cx.ct_QtyEntryUOM)));


DROP TABLE IF EXISTS materialmovement_tmp_001;
CREATE TABLE materialmovement_tmp_001 AS
      SELECT cx.fact_materialmovementid,
          cx.ct_Quantity ct_Quantity,
	      cx.PartNumber PartNumber, 
		  cx.PlantCode PlantCode,  
		  cx.DateValue PostingDate,
		  ROW_NUMBER() OVER(PARTITION BY cx.PartNumber, cx.PlantCode
					ORDER BY cx.DateValue DESC, cx.dd_MaterialDocNo DESC, cx.dd_MaterialDocItemNo DESC, cx.dd_DocumentScheduleNo DESC) RowSeqNo
      FROM materialmovement_tmp_001_pre cx;

DROP TABLE IF EXISTS materialmovement_tmp_001_pre;

DROP TABLE IF EXISTS tmp_mm_lastrcptinfo_001;
CREATE TABLE tmp_mm_lastrcptinfo_001 AS
SELECT cx.PartNumber,
	cx.PlantCode,
	cx.ct_Quantity,
	cx.PostingDate
FROM  materialmovement_tmp_001 cx
WHERE cx.RowSeqNo = 1;

DROP TABLE IF EXISTS mm_tmp_maxseqno_001;
CREATE TABLE mm_tmp_maxseqno_001 AS
SELECT a.PartNumber, 
	  a.PlantCode, 
	  MAX(a.RowSeqNo) MaxRowSeqNo
FROM materialmovement_tmp_001 a
GROUP BY a.PartNumber, 
	  a.PlantCode;

DROP TABLE IF EXISTS materialmovement_tmp_002;
CREATE TABLE materialmovement_tmp_002 AS
SELECT a.fact_materialmovementid,
	  a.ct_Quantity,
	  a.PartNumber, 
	  a.PlantCode, 
	  a.PostingDate,
	  a.RowSeqNo,
	  SUM(b.ct_Quantity) - a.ct_Quantity QuantityCUM_A,
	  SUM(b.ct_Quantity) QuantityCUM_B,
	  c.MaxRowSeqNo
FROM materialmovement_tmp_001 a
	inner join materialmovement_tmp_001 b on a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode
	inner join mm_tmp_maxseqno_001 c on a.PartNumber = c.PartNumber and a.PlantCode = c.PlantCode
WHERE a.RowSeqNo >= b.RowSeqNo
GROUP BY a.fact_materialmovementid,
	  a.ct_Quantity,
	  a.PartNumber, 
	  a.PlantCode, 
	  a.PostingDate,
	  a.RowSeqNo,
	  c.MaxRowSeqNo;

DROP TABLE IF EXISTS materialmovement_tmp_new;
CREATE TABLE materialmovement_tmp_new 
AS
SELECT cx.fact_materialmovementid,
	cx.dd_MaterialDocNo dd_MaterialDocNo,
	cx.dd_MaterialDocItemNo dd_MaterialDocItemNo,
	cx.dd_MaterialDocYear dd_MaterialDocYear,
	ifnull(cx.dd_DocumentNo,'Not Set') dd_DocumentNo,
	cx.dd_DocumentItemNo dd_DocumentItemNo,
	cx.dd_DocumentScheduleNo,
	ifnull(cx.dd_SalesOrderNo,'Not Set') dd_SalesOrderNo,
	cx.dd_SalesOrderItemNo dd_SalesOrderItemNo,
	ifnull(cx.dd_SalesOrderDlvrNo,0) dd_SalesOrderDlvrNo,
	cx.dd_BatchNumber dd_BatchNumber,
	cx.dd_ValuationType dd_ValuationType,
	cx.dd_debitcreditid dd_debitcreditid,
	cx.amt_LocalCurrAmt amt_LocalCurrAmt,
	cx.amt_DeliveryCost amt_DeliveryCost,
	cx.amt_ExchangeRate_GBL amt_ExchangeRate_GBL,
	cx.amt_AltPriceControl amt_AltPriceControl,
	cx.amt_OnHand_ValStock amt_OnHand_ValStock,
	cx.ct_Quantity ct_Quantity,
	cx.ct_Quantity ct_QtyEntryUOM,
	cx.Dim_MovementTypeid Dim_MovementTypeid,
	cx.dim_Companyid dim_Companyid,
	cx.Dim_Partid Dim_Partid,
	cx.Dim_Plantid Dim_Plantid,
	cx.Dim_StorageLocationid Dim_StorageLocationid,
	cx.Dim_Vendorid Dim_Vendorid,
	cx.dim_DateIDPostingDate dim_DateIDPostingDate,
	cx.dim_MovementIndicatorid dim_MovementIndicatorid,
	cx.dim_CostCenterid dim_CostCenterid,
	cx.dim_specialstockid dim_specialstockid,
	cx.Dim_StockTypeid Dim_StockTypeid,
	cx.Dim_Currencyid Dim_Currencyid,
	cx.amt_ExchangeRate amt_ExchangeRate,	
	a.PartNumber,
	a.PlantCode,
	a.PostingDate,	
	a.QuantityCUM_A,
	a.QuantityCUM_B,
	b.ct_Quantity LatestReceiptQty,
	b.PostingDate LatestPostingDate,
	a.RowSeqNo,
	a.MaxRowSeqNo,
	cx.Dim_Currencyid_TRA Dim_Currencyid_TRA,
	cx.Dim_Currencyid_GBL Dim_Currencyid_GBL
FROM fact_materialmovement cx
	inner join materialmovement_tmp_002 a on a.fact_materialmovementid = cx.fact_materialmovementid
	inner join tmp_mm_lastrcptinfo_001 b on a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode
	inner join tmp_MARD_TotStk_001_U c on a.PartNumber = c.PartNumber and a.PlantCode = c.PlantCode and a.QuantityCUM_A < c.TotalStockQty;

DROP TABLE IF EXISTS mm_tmp_maxseqno_001;
CREATE TABLE mm_tmp_maxseqno_001 AS
SELECT a.PartNumber, 
	  a.PlantCode, 
	  MAX(a.RowSeqNo) MaxRowSeqNo
FROM materialmovement_tmp_new a
GROUP BY a.PartNumber, 
	  a.PlantCode;

update materialmovement_tmp_new a
set a.MaxRowSeqNo = b.MaxRowSeqNo
from materialmovement_tmp_new a,mm_tmp_maxseqno_001 b
where a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode;


DROP TABLE IF EXISTS tmp_c2;
CREATE TABLE tmp_c2 AS
SELECT PartNumber,PlantCode,max(QuantityCUM_B) as max_stk
FROM materialmovement_tmp_new c1
GROUP BY PartNumber,PlantCode;

DROP TABLE IF EXISTS tmp_c3;
CREATE TABLE tmp_c3 AS
SELECT c1.*
FROM materialmovement_tmp_new c1, tmp_c2 c2
WHERE c2.PartNumber = c1.PartNumber AND c2.PlantCode = c1.PlantCode
	AND c1.QuantityCUM_B = ifnull(c2.max_stk,0) ;


/*** MBEW NO BWTAR values ***/

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x 
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY)
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR AS
SELECT DISTINCT m.MATNR,
       m.BWKEY,
       m.LFGJA,
       m.LFMON,
       m.VPRSV,
       m.VERPR,
       m.STPRS,
       m.PEINH,
       m.SALK3,
       m.BWTAR,
       m.MBEW_LBKUM,
       ((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, 
       m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;	

ALTER TABLE tmp2_MBEW_NO_BWTAR ADD column multiplier decimal(18,5) default 0;

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = convert(decimal (18,5), (b.STPRS / b.PEINH))
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  convert(decimal (18,5),(b.VERPR / b.PEINH))
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

/* divide inventory stock into material movement qty */

DROP TABLE IF EXISTS tmp_fact_invaging_001;
CREATE TABLE tmp_fact_invaging_001 AS
SELECT b.fact_materialmovementid,
	a.PartNumber,
	a.PlantCode,
	a.StorLocCode,
	a.BatchNumber,
	case when a.MARD_UMLME_1 <= 0 then 0
		when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_UMLME_1
				when a.MARD_UMLME_1 <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A)
				else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
			end
		else 0
	end MARD_UMLME_1,
	case when a.MARD_LABST_2 <= 0 then 0
		when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_LABST_2
				when (a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then case when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - (a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when a.MARD_UMLME_1 <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - (a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end							
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_LABST_2,
	case when a.MARD_SPEME_3 <= 0 then 0
		when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_SPEME_3
				when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then case when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_SPEME_3, 
	case when a.MARD_INSME_4 <= 0 then 0
		when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_INSME_4
				when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										- ((a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_INSME_4, 
	case when a.MARD_EINME_5 <= 0 then 0
		when (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_EINME_5
				when (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo 
				then case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_EINME_5,			
	b.PostingDate,	
	b.dim_DateIDPostingDate,
	b.LatestReceiptQty,
	b.LatestPostingDate,
	a.BatchNumber dd_BatchNumber,
	b.dd_ValuationType,
	b.Dim_StorageLocationid, 
	b.dim_Companyid,
	b.dim_stocktypeid, 
	b.dim_specialstockid,
	b.Dim_MovementIndicatorid, 
	b.dim_CostCenterid,
	b.Dim_MovementTypeid,
	b.RowSeqNo,
	b.MaxRowSeqNo,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KSPEM ELSE 0 END MARD_KSPEM, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_KINSM ELSE 0 END MARD_KINSM, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_KEINM ELSE 0 END MARD_KEINM,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KLABS ELSE 0 END MARD_KLABS, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_RETME ELSE 0 END MARD_RETME,
	a.MARD_DLINL,
	a.MARD_ERSDA,
	ROW_NUMBER() OVER(PARTITION BY a.PartNumber, a.PlantCode
			  ORDER BY dt1.DateValue, b.dd_MaterialDocNo, b.dd_MaterialDocItemNo, b.dd_DocumentScheduleNo) MinRowSeqNo
FROM tmp_MARD_TotStk_001 a
	inner join materialmovement_tmp_new b on a.PartNumber = b.PartNumber
						and a.PlantCode = b.PlantCode
	inner join dim_date dt1 on b.dim_DateIDPostingDate = dt1.dim_dateid
	inner join tmp_MARD_TotStk_t02 c on c.PartNumber = a.PartNumber
						and c.PlantCode = a.PlantCode
						and c.StorLocCode = a.StorLocCode
						and c.BatchNumber = a.BatchNumber
WHERE ((b.QuantityCUM_A = 0 and c.TotalStockCUM_A = 0) 
	or (b.QuantityCUM_A < c.TotalStockCUM_B and b.QuantityCUM_B > c.TotalStockCUM_A));


/*Insert 1 */

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MARD';

DROP TABLE IF EXISTS TMP_INSERT_1_STOCK;
CREATE TABLE TMP_INSERT_1_STOCK
AS
SELECT MARD_LABST_2 ct_StockQty, 
	MARD_EINME_5 ct_TotalRestrictedStock, 
	MARD_INSME_4 ct_StockInQInsp, 
	MARD_SPEME_3 ct_BlockedStock, 
	MARD_UMLME_1 ct_StockInTransfer, 
	MARD_KSPEM ct_BlockedConsgnStock, 
	MARD_KINSM ct_ConsgnStockInQInsp, 
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock, 
	MARD_RETME ct_BlockedStockReturns, 
	IFNULL((CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END),0) amt_StockValueAmt,
	IFNULL((CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END),0) amt_StockValueAmt_GBL,
	IFNULL((CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END),0) amt_BlockedStockAmt,
	IFNULL((CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END),0)amt_BlockedStockAmt_GBL,
	IFNULL((CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END),0) amt_StockInQInspAmt,
	IFNULL((CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END),0) amt_StockInQInspAmt_GBL,
	IFNULL((CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END),0) amt_StockInTransferAmt,
	IFNULL((CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END),0) amt_StockInTransferAmt_GBL,
	IFNULL((CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_KLABS) END),0) amt_UnrestrictedConsgnStockAmt,
	IFNULL((CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_KLABS) END),0) amt_UnrestrictedConsgnStockAmt_GBL,
	IFNULL(b.multiplier,0) amt_StdUnitPrice,
	IFNULL(b.multiplier,0) amt_StdUnitPrice_GBL,
	IFNULL((CASE WHEN MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END
			  + CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END
			  + CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END
			  + CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END
			  + CASE WHEN MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_EINME_5) END)
	END),0) amt_OnHand,
	IFNULL((CASE WHEN MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END
			  + CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END
			  + CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END
			  + CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END
			  + CASE WHEN MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_EINME_5) END)
	END),0) amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) ct_LastReceivedQty, 
	c1.dd_BatchNumber dd_BatchNo, 
	ifnull(c1.dd_ValuationType,'Not Set') dd_ValuationType, 
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo, 
	dmt.MovementType dd_MovementType, 
	c1.dim_DateIDPostingDate dim_StorageLocEntryDateid, 
	convert(bigint,1) dim_LastReceivedDateid,
	dp.Dim_PartID, 
	p.dim_plantid, 
	sl.Dim_StorageLocationid, 
	c.dim_Companyid,
	convert(bigint,1) dim_vendorid, 
	dc.dim_currencyid, 
	c1.dim_stocktypeid, 
	convert(bigint,1) dim_specialstockid,
	convert(bigint,1) Dim_PurchaseOrgid,
	pg.Dim_PurchaseGroupid, 
	dph.dim_producthierarchyid, 
	uom.Dim_UnitOfMeasureid, 
	c1.Dim_MovementIndicatorid, 
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	convert(decimal (18,6),1) amt_ExchangeRate_GBL,
	convert(decimal (18,6),1) amt_ExchangeRate, 
	dc.Dim_Currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001)),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,c1.LatestPostingDate /* LastReceivedDate */
	,p.CompanyCode /* LastReceivedDate */
FROM tmp_fact_invaging_001 c1 
	INNER JOIN dim_plant p ON c1.PlantCode = p.PlantCode
	INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = c1.PartNumber AND b.BWKEY = p.ValuationArea
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_part dp ON dp.PartNumber = c1.PartNumber AND dp.Plant = c1.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = c1.StorLocCode AND sl.Plant = c1.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
    INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
    INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
WHERE MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 > 0;

MERGE INTO TMP_INSERT_1_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_1_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_UnrestrictedConsgnStockAmt_GBL = ST.amt_UnrestrictedConsgnStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockInTransferAmt_GBL = ST.amt_StockInTransferAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_1_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_1_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_1_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(lrd.dim_dateid,1) dim_dateid
	 FROM TMP_INSERT_1_STOCK t
		LEFT JOIN dim_date lrd ON t.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = t.CompanyCode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_LastReceivedDateid = SRC.dim_dateid;
	
INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL) 
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, 
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_1_STOCK;

update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
where table_name = 'fact_inventoryaging_tmp_populate';

/* Changes in above query - 
	a) Found out all columns where were not insert column list. Populate the default values ( according to earlier defn in MySQL )
	b) ifnull for c1.LatestReceiptQty,lrd.dim_dateid and c1.dd_ValuationType ( which were found after debugging errors )
	c) Fact row_number logic */
/* Above query works now */


/* INSERT 2 */

/* For now replaced getExchangeRate return value with 1 ( checked that tcurr does not have anything in fossil2 or the qa db on mysql 
   For dbs which have data in this table, the function would need to be converted to SQLs. 
   Already started that - refer getExchangeRate_func.sql */

drop table if exists tmp_tbl_StorageLocEntryDate;
CREATE table  tmp_tbl_StorageLocEntryDate AS 
SELECT  c2.PartNumber, c2.PlantCode, MAX(c2.LatestPostingDate) as LatestPostingDate
FROM	 materialmovement_tmp_new c2
GROUP BY c2.PartNumber, c2.PlantCode;

DROP TABLE IF EXISTS TMP_INSERT_2_STOCK;
CREATE TABLE TMP_INSERT_2_STOCK
AS
SELECT DISTINCT a.MARD_LABST_2 ct_StockQty, 
	a.MARD_EINME_5 ct_TotalRestrictedStock, 
	a.MARD_INSME_4 ct_StockInQInsp, 
	a.MARD_SPEME_3 ct_BlockedStock, 
	a.MARD_UMLME_1 ct_StockInTransfer, 
	a.MARD_KSPEM ct_BlockedConsgnStock, 
	a.MARD_KINSM ct_ConsgnStockInQInsp, 
	a.MARD_KEINM ct_RestrictedConsgnStock,
	a.MARD_KLABS ct_UnrestrictedConsgnStock, 
	a.MARD_RETME ct_BlockedStockReturns, 
	ifnull((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END),0) amt_StockValueAmt,
	ifnull((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END),0) amt_StockValueAmt_GBL,
	ifnull((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END),0) amt_BlockedStockAmt,
	ifnull((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END),0) amt_BlockedStockAmt_GBL,
	ifnull((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END),0) amt_StockInQInspAmt,
	ifnull((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END),0) amt_StockInQInspAmt_GBL,
	ifnull((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END),0) amt_StockInTransferAmt,
	ifnull((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END),0) amt_StockInTransferAmt_GBL,
	ifnull((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END),0) amt_UnrestrictedConsgnStockAmt,
	ifnull((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END ),0) amt_UnrestrictedConsgnStockAmt_GBL,
	ifnull(b.multiplier,0) amt_StdUnitPrice,
	ifnull(b.multiplier,0) amt_StdUnitPrice_GBL,
	IFNULL((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END)
	END),0) amt_OnHand,
	IFNULL((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END) 
		  END ),0) amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) ct_LastReceivedQty, 
	a.BatchNumber dd_BatchNo, 
	ifnull(c1.dd_ValuationType,'Not Set') dd_ValuationType,
	'Not Set' dd_DocumentNo, 
	0 dd_DocumentItemNo, 
	dmt.MovementType dd_MovementType, 
	c1.dim_DateIDPostingDate dim_StorageLocEntryDateid, 
	convert(bigint, 1) dim_LastReceivedDateid,
	Dim_Partid, p.dim_plantid, sl.Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	convert(bigint,1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid,
	c1.Dim_MovementIndicatorid, 
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid, 
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001)),1) dim_Currencyid_GBL	
	,dc.CurrencyCode /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,c1.LatestPostingDate /* LastReceivedDate */
	,p.CompanyCode /* LastReceivedDate */
FROM tmp_MARD_TotStk_001 a 
	INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
	INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.PartNumber AND b.BWKEY = p.ValuationArea
	INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN tmp_fact_invaging_001 c1 ON a.PlantCode = c1.PlantCode AND a.PartNumber = c1.PartNumber AND c1.MinRowSeqNo = 1
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode 
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid 
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and c2.dd_BatchNo = a.BatchNumber);
					
MERGE INTO TMP_INSERT_2_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_2_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_UnrestrictedConsgnStockAmt_GBL = ST.amt_UnrestrictedConsgnStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockInTransferAmt_GBL = ST.amt_StockInTransferAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_2_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_2_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_2_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(lrd.dim_dateid,1) dim_dateid
	 FROM TMP_INSERT_2_STOCK t
		LEFT JOIN dim_date lrd ON t.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = t.CompanyCode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_LastReceivedDateid = SRC.dim_dateid;
	
INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL) 
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, 
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_2_STOCK;

/* INSERT 3 - No MBEW */

update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
where table_name = 'fact_inventoryaging_tmp_populate';

DROP TABLE IF EXISTS TMP_INSERT_3_STOCK;
CREATE TABLE TMP_INSERT_3_STOCK
AS
SELECT DISTINCT a.MARD_LABST_2 ct_StockQty, 
	a.MARD_EINME_5 ct_TotalRestrictedStock, 
	a.MARD_INSME_4 ct_StockInQInsp, 
	a.MARD_SPEME_3 ct_BlockedStock, 
	a.MARD_UMLME_1 ct_StockInTransfer, 
	a.MARD_KSPEM ct_BlockedConsgnStock, 
	a.MARD_KINSM ct_ConsgnStockInQInsp, 
	a.MARD_KEINM ct_RestrictedConsgnStock,
	a.MARD_KLABS ct_UnrestrictedConsgnStock, 
	a.MARD_RETME ct_BlockedStockReturns, 
	ifnull((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END), 0) amt_StockValueAmt,
	ifnull((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END), 0) amt_StockValueAmt_GBL,
	ifnull((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END), 0) amt_BlockedStockAmt,
	ifnull((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END), 0) amt_BlockedStockAmt_GBL,
	ifnull((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END), 0) amt_StockInQInspAmt,
	ifnull((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END), 0) amt_StockInQInspAmt_GBL,
	ifnull((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END), 0) amt_StockInTransferAmt,
	ifnull((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END), 0) amt_StockInTransferAmt_GBL,
	ifnull((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END), 0) amt_UnrestrictedConsgnStockAmt,
	ifnull((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END), 0) amt_UnrestrictedConsgnStockAmt_GBL,
	ifnull((b.multiplier), 0) amt_StdUnitPrice,
	ifnull((b.multiplier), 0) amt_StdUnitPrice_GBL,
	ifnull((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END)
	END),0) amt_OnHand,
	ifnull((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END) 
		  END ), 0) amt_OnHand_GBL,
	0 ct_LastReceivedQty, 
	a.BatchNumber dd_BatchNo, 
	'Not Set' dd_ValuationType,
	'Not Set' dd_DocumentNo, 
	0 dd_DocumentItemNo, 
	'Not Set' dd_MovementType, 
	convert(bigint,1) dim_StorageLocEntryDateid,
	1 dim_LastReceivedDateid,
	Dim_Partid, p.dim_plantid, sl.Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	convert(bigint, 1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid,
	1 Dim_MovementIndicatorid, 
	1 dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid, 
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,p.CompanyCode /*StorageLocEntryDate*/
	,a.PartNumber /*StorageLocEntryDate*/
	,sled.Dim_Dateid /*StorageLocEntryDate*/
	,a.PlantCode /*StorageLocEntryDate*/
FROM tmp_MARD_TotStk_001 a 
	INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
	INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.PartNumber AND b.BWKEY = p.ValuationArea
	INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode 
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
    WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid 
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and c2.dd_BatchNo = a.BatchNumber);
					
MERGE INTO TMP_INSERT_3_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_3_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_UnrestrictedConsgnStockAmt_GBL = ST.amt_UnrestrictedConsgnStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockInTransferAmt_GBL = ST.amt_StockInTransferAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_3_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_3_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_3_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, lrd.Dim_Dateid
	FROM TMP_INSERT_3_STOCK t
		LEFT JOIN tmp_tbl_StorageLocEntryDate c2 ON c2.PartNumber = t.PartNumber AND c2.PlantCode = t.PlantCode
		LEFT JOIN dim_date lrd on lrd.DateValue = c2.LatestPostingDate and lrd.CompanyCode = t.CompanyCode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_StorageLocEntryDateid = ifnull(SRC.Dim_Dateid,ST.Dim_Dateid);
		
INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL) 
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, 
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_3_STOCK;

/* INSERT 4 - No MBEW */

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
 where table_name = 'fact_inventoryaging_tmp_populate';

	
DROP TABLE IF EXISTS TMP_INSERT_4_STOCK;
CREATE TABLE TMP_INSERT_4_STOCK
AS
SELECT MARD_LABST_2 ct_StockQty, 
	MARD_EINME_5 ct_TotalRestrictedStock, 
	MARD_INSME_4 ct_StockInQInsp, 
	MARD_SPEME_3 ct_BlockedStock, 
	MARD_UMLME_1 ct_StockInTransfer, 
	MARD_KSPEM ct_BlockedConsgnStock, 
	MARD_KINSM ct_ConsgnStockInQInsp, 
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock, 
	MARD_RETME ct_BlockedStockReturns, 
	0 amt_StockValueAmt,
	0 amt_StockValueAmt_GBL,
	0 amt_BlockedStockAmt,
	0 amt_BlockedStockAmt_GBL,
	0 amt_StockInQInspAmt,
	0 amt_StockInQInspAmt_GBL,
	0 amt_StockInTransferAmt,
	0 amt_StockInTransferAmt_GBL,
	0 amt_UnrestrictedConsgnStockAmt,
	0 amt_UnrestrictedConsgnStockAmt_GBL,
	0 amt_StdUnitPrice,
	0 amt_StdUnitPrice_GBL,
	0 amt_OnHand,
	0 amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) ct_LastReceivedQty, 
	c1.dd_BatchNumber dd_BatchNo, 
	ifnull(c1.dd_ValuationType,'Not Set') dd_ValuationType, 
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo, 
	dmt.MovementType dd_MovementType, 
	c1.dim_DateIDPostingDate dim_StorageLocEntryDateid, 
	convert(bigint, 1) dim_LastReceivedDateid,
	dp.Dim_PartID, 
	p.dim_plantid, 
	sl.Dim_StorageLocationid, 
	c.dim_Companyid,
	1 dim_vendorid, 
	dc.dim_currencyid, 
	c1.dim_stocktypeid, 
	1 dim_specialstockid,
	convert(bigint, 1) Dim_PurchaseOrgid,
	pg.Dim_PurchaseGroupid, 
	dph.dim_producthierarchyid, 
	uom.Dim_UnitOfMeasureid, 
	c1.Dim_MovementIndicatorid, 
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
	1 amt_ExchangeRate, 
	dc.Dim_Currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001)),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,c1.LatestPostingDate /* LastReceivedDate */
	,p.CompanyCode /* LastReceivedDate */
FROM tmp_fact_invaging_001 c1 
	INNER JOIN dim_plant p ON c1.PlantCode = p.PlantCode
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_part dp ON dp.PartNumber = c1.PartNumber AND dp.Plant = c1.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = c1.StorLocCode AND sl.Plant = c1.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
		WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid 
				and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
				and c2.dd_BatchNo = c1.dd_BatchNumber);
				
MERGE INTO TMP_INSERT_4_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_4_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_4_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_4_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_4_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(lrd.dim_dateid,1) dim_dateid
	 FROM TMP_INSERT_4_STOCK t
		LEFT JOIN dim_date lrd ON t.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = t.CompanyCode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_LastReceivedDateid = SRC.dim_dateid;
	
INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL) 
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, 
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_4_STOCK;

/* INSERT 5 - No MBEW - No Movement TYpe*/

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
 where table_name = 'fact_inventoryaging_tmp_populate';
	
DROP TABLE IF EXISTS TMP_INSERT_5_STOCK;
CREATE TABLE TMP_INSERT_5_STOCK
AS
SELECT DISTINCT MARD_LABST_2 ct_StockQty, 
	MARD_EINME_5 ct_TotalRestrictedStock, 
	MARD_INSME_4 ct_StockInQInsp, 
	MARD_SPEME_3 ct_BlockedStock, 
	MARD_UMLME_1 ct_StockInTransfer, 
	MARD_KSPEM ct_BlockedConsgnStock, 
	MARD_KINSM ct_ConsgnStockInQInsp, 
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock, 
	MARD_RETME ct_BlockedStockReturns, 
	0 amt_StockValueAmt,
	0 amt_StockValueAmt_GBL,
	0 amt_BlockedStockAmt,
	0 amt_BlockedStockAmt_GBL,
	0 amt_StockInQInspAmt,
	0 amt_StockInQInspAmt_GBL,
	0 amt_StockInTransferAmt,
	0 amt_StockInTransferAmt_GBL,
	0 amt_UnrestrictedConsgnStockAmt,
	0 amt_UnrestrictedConsgnStockAmt_GBL,
	0 amt_StdUnitPrice,
	0 amt_StdUnitPrice_GBL,
	0 amt_OnHand,
	0 amt_OnHand_GBL,
	0 ct_LastReceivedQty, 
	a.BatchNumber dd_BatchNo, 
	'Not Set' dd_ValuationType,
	'Not Set' dd_DocumentNo, 
	0 dd_DocumentItemNo, 
	'Not Set' dd_MovementType,
	convert(bigint,1) dim_StorageLocEntryDateid,
	1 dim_LastReceivedDateid,
	Dim_Partid, p.dim_plantid, Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	convert(bigint, 1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid, 1 Dim_MovementIndicatorid, 1 dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid, 
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL	
	,dc.CurrencyCode /* amt_ExchangeRate_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,p.CompanyCode /*StorageLocEntryDate*/
	,a.PartNumber /*StorageLocEntryDate*/
	,sled.Dim_Dateid /*StorageLocEntryDate*/
	,a.PlantCode /*StorageLocEntryDate*/
FROM tmp_MARD_TotStk_001 a 
	INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
	INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode 
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid 
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and c2.dd_BatchNo = a.BatchNumber);
					
MERGE INTO TMP_INSERT_5_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_5_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_5_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_5_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_5_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, lrd.Dim_Dateid
	FROM TMP_INSERT_5_STOCK t
		LEFT JOIN tmp_tbl_StorageLocEntryDate c2 ON c2.PartNumber = t.PartNumber AND c2.PlantCode = t.PlantCode
		LEFT JOIN dim_date lrd on lrd.DateValue = c2.LatestPostingDate and lrd.CompanyCode = t.CompanyCode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_StorageLocEntryDateid = ifnull(SRC.Dim_Dateid,ST.Dim_Dateid);
		
INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL) 
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, 
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_5_STOCK;

 UPDATE NUMBER_FOUNTAIN 
 SET max_id = ifnull((select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 WHERE table_name = 'fact_inventoryaging_tmp_populate';
 
/***********************LK: 26 Apr change : Insert queries 7/8/9/10/11 Starts ***********************/

/*Insert 7 */

/* Temporary tables to be used in inserts 7 to 11 */

/* LastReceivedQty */

/* Store mm and datevalue in a tmp table */

DROP TABLE IF EXISTS TMP_fact_mm_0;
CREATE TABLE TMP_fact_mm_0 
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'O';

/* Only retrive the min dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_1;
CREATE TABLE TMP_fact_mm_1 
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,min(mm.DateValue) as min_DateValue
from TMP_fact_mm_0 mm 
GROUP by mm.Dim_Partid,mm.Dim_Vendorid;

/* Now get the ct_QtyEntryUOM corresponding to the min datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_2;
CREATE TABLE TMP_fact_mm_2
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,mm.ct_Quantity ct_QtyEntryUOM 
FROM TMP_fact_mm_0 mm, TMP_fact_mm_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.min_DateValue;


/* StorageLocEntryDate/LastReceivedDate */

/* Simply use the posting date id for the min/max date */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0 
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where  mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'O';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm 
GROUP by mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT distinct mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate ,mm.dim_specialstockid , 
row_number() over (partition by mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate ,mm.dim_specialstockid order by '') rowno
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT distinct mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.min_DateValue;

DROP TABLE IF EXISTS fitp_tmp_p01;
CREATE TABLE fitp_tmp_p01 LIKE fact_inventoryaging_tmp_populate INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE fitp_tmp_p01 ADD PRIMARY KEY (fact_inventoryagingid);
ALTER TABLE fitp_tmp_p01 ADD COLUMN Dim_Partid_upd BIGINT DEFAULT 0;
ALTER TABLE fitp_tmp_p01 ADD COLUMN Dim_Vendorid_upd BIGINT DEFAULT 0;
ALTER TABLE fitp_tmp_p01 ADD COLUMN PurchOrg_upd varchar(20);
ALTER TABLE fitp_tmp_p01 ADD COLUMN CurrencyCode varchar(20) default 'Not Set';
ALTER TABLE fitp_tmp_p01 ADD COLUMN MSLB_SOBKZ varchar(20) default 'Not Set';
ALTER TABLE fitp_tmp_p01 ADD COLUMN pGlobalCurrency varchar(20) default 'Not Set';

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MSLB';

INSERT INTO fitp_tmp_p01(ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL,
				Dim_Partid_upd,
				Dim_Vendorid_upd,
				PurchOrg_upd,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,
				CurrencyCode,pGlobalCurrency,MSLB_SOBKZ)
								
 SELECT  a.MSLB_LBLAB ct_StockQty,
	a.MSLB_LBEIN ct_TotalRestrictedStock,
	a.MSLB_LBINS ct_StockInQInsp,
	ifnull((CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END), 0)  StockValueAmt,
	ifnull((CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END), 0)  StockValueAmt_GBL,
    ifnull((b.multiplier * a.MSLB_LBINS), 0) amt_StockInQInspAmt,
	ifnull((b.multiplier * a.MSLB_LBINS), 0)  amt_StockInQInspAmt_GBL,
    ifnull((b.multiplier), 0) amt_StdUnitPrice,
	ifnull((b.multiplier), 0) amt_StdUnitPrice_GBL,
	ifnull((CASE WHEN MSLB_LBLAB+MSLB_LBINS+MSLB_LBEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END 
			+ CASE WHEN MSLB_LBINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBINS) END
			+ CASE WHEN MSLB_LBEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBEIN) END
		  ) END),0) amt_OnHand,
	ifnull((CASE WHEN MSLB_LBLAB+MSLB_LBINS+MSLB_LBEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END 
			+ CASE WHEN MSLB_LBINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBINS) END
			+ CASE WHEN MSLB_LBEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBEIN) END
		  ) END), 0) amt_OnHand_GBL,
	1 LastReceivedQty,
	a.MSLB_CHARG BatchNo,
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo,
	1 StorageLocEntryDate,
	1 LastReceivedDate,
	Dim_Partid,
	p.dim_plantid,
	1 Dim_StorageLocationid,
	dim_companyid,
	v.dim_vendorid,
	dim_currencyid,
	1 dim_stocktypeid,
    convert(bigint,1) dim_specialstockid,
	convert(bigint,1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	1 Dim_MovementIndicatorid,
	1 dim_CostCenterid,
	stkc.dim_StockCategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (order by '') fact_inventoryagingid, /* table name is correct here do not change it to fitp_tmp_p01 */ 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
	dp.Dim_Partid Dim_Partid_upd,
	v.Dim_Vendorid Dim_Vendorid_upd,
	p.PurchOrg PurchOrg_upd,
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
	,a.MSLB_SOBKZ /* dim_specialstockid */
     FROM MSLB a
          INNER JOIN dim_plant p ON a.MSLB_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSLB_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSLB_MATNR AND dp.Plant = a.MSLB_WERKS
          INNER JOIN dim_vendor v ON v.VendorNumber = MSLB_LIFNR
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
          CROSS JOIN tmp_stkcategory_001  stkc;	
		  
MERGE INTO fitp_tmp_p01 ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM fitp_tmp_p01 t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_UnrestrictedConsgnStockAmt_GBL = ST.amt_UnrestrictedConsgnStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockInTransferAmt_GBL = ST.amt_StockInTransferAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;
		
MERGE INTO fitp_tmp_p01 FA
USING ( SELECT fact_inventoryagingid, IFNULL(mm.dim_specialstockid,1) dim_specialstockid
		FROM fitp_tmp_p01 T1
				LEFT JOIN dim_specialstock mm on mm.specialstockindicator = T1.MSLB_SOBKZ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_specialstockid = SRC.dim_specialstockid;
				
Update fitp_tmp_p01 fitp
Set dim_StorageLocEntryDateid = mm.dim_DateIDPostingDate
from 
fitp_tmp_p01 fitp,
TMP_fact_mm_postingdate_2_min mm /* This has min date */
WHERE 
mm.Dim_Partid = fitp.Dim_Partid_upd 
and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd ;

update fitp_tmp_p01 fitp
set dim_StorageLocEntryDateId = 1 
where dim_StorageLocEntryDateid is null;


Update fitp_tmp_p01 fitp
Set 
dim_LastReceivedDateid = mm.dim_DateIDPostingDate
from fitp_tmp_p01 fitp,
TMP_fact_mm_postingdate_2 mm     /* This has max date */
WHERE 
mm.Dim_Partid = fitp.Dim_Partid_upd 
and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd
and mm.rowno = 1;

Update fitp_tmp_p01 fitp
SET dim_LastReceivedDateid = 1
WHERE dim_LastReceivedDateid IS NULL;

DROP TABLE IF EXISTS TMP_UPDT_TMP_fact_mm_2;
CREATE TABLE TMP_UPDT_TMP_fact_mm_2
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,sum(mm.ct_QtyEntryUOM) ct_QtyEntryUOM
FROM TMP_fact_mm_2 mm
GROUP BY mm.Dim_Partid,mm.Dim_Vendorid;

Update fitp_tmp_p01 fitp
Set ct_LastReceivedQty =  mm.ct_QtyEntryUOM
from fitp_tmp_p01 fitp,TMP_UPDT_TMP_fact_mm_2 mm
where 
mm.Dim_Partid = fitp.Dim_Partid_upd 
and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd;

Update fitp_tmp_p01 fitp
Set Dim_PurchaseOrgid = po.Dim_PurchaseOrgid 
from fitp_tmp_p01 fitp,dim_purchaseorg po
where po.PurchaseOrgCode = fitp.PurchOrg_upd;

Update fitp_tmp_p01 fitp
Set Dim_PurchaseOrgid = 1
WHERE  Dim_PurchaseOrgid is null;

Insert into fact_inventoryaging_tmp_populate(ct_StockQty,
		ct_TotalRestrictedStock,
		ct_StockInQInsp,
		amt_StockValueAmt,
		amt_StockValueAmt_GBL,
		amt_StockInQInspAmt,
		amt_StockInQInspAmt_GBL,
		amt_StdUnitPrice,
		amt_StdUnitPrice_GBL,
		amt_OnHand,
		amt_OnHand_GBL,
		ct_LastReceivedQty,
		dd_BatchNo,
		dd_DocumentNo,
		dd_DocumentItemNo,
		dim_StorageLocEntryDateid,
		dim_LastReceivedDateid,
		dim_Partid,
		dim_Plantid,
		dim_StorageLocationid,
		dim_Companyid,
		dim_Vendorid,
		dim_Currencyid,
		dim_StockTypeid,
		dim_specialstockid,
		Dim_PurchaseOrgid,
		Dim_PurchaseGroupid,
		dim_producthierarchyid,
		Dim_UnitOfMeasureid,
		Dim_MovementIndicatorid,
		dim_CostCenterid,
		dim_stockcategoryid,
		fact_inventoryagingid,
		Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
		Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
		amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
		amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
		amt_ExchangeRate_GBL,
		amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
Select ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
from fitp_tmp_p01;

drop table if exists fitp_tmp_p01;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

 
/*Insert 8 */
/* -- Vendor Stocks with no valuation	*/
DROP TABLE IF EXISTS TMP_INSERT_8_STOCK;
CREATE TABLE TMP_INSERT_8_STOCK
AS
  SELECT  a.MSLB_LBLAB ct_StockQty,
          a.MSLB_LBEIN ct_TotalRestrictedStock,
          a.MSLB_LBINS ct_StockInQInsp,
          0 amt_StockValueAmt,
          0 amt_StockValueAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
		  0 amt_OnHand,
		  0 amt_OnHand_GBL,
         convert(decimal (18,4), 0) ct_LastReceivedQty,
          a.MSLB_CHARG dd_BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          convert(bigint, 1) dim_StorageLocEntryDateid,
          convert(bigint, 1) dim_LastReceivedDateid,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          v.dim_vendorid,
          dim_currencyid,
          1 dim_stocktypeid,
          convert(bigint, 1) dim_specialstockid,
          convert(bigint, 1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_StockCategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (order by '') fact_inventoryagingid, 
	  convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
		,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		,p.PurchOrg /* Dim_PurchaseOrgid */
		,a.MSLB_SOBKZ /* dim_specialstockid */
     FROM MSLB a
          INNER JOIN dim_plant p
             ON a.MSLB_WERKS = p.PlantCode
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSLB_MATNR AND dp.Plant = a.MSLB_WERKS
          INNER JOIN dim_vendor v
             ON v.VendorNumber = MSLB_LIFNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
          CROSS JOIN tmp_stkcategory_001  stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_inventoryaging_tmp_populate c2
           WHERE c2.Dim_Partid = dp.Dim_Partid
                AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_Vendorid = v.Dim_Vendorid);	
				
MERGE INTO TMP_INSERT_8_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_8_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_8_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_8_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_8_STOCK FA
USING ( SELECT fact_inventoryagingid, IFNULL(mm.dim_specialstockid,1) dim_specialstockid
		FROM TMP_INSERT_8_STOCK T1
				LEFT JOIN dim_specialstock mm on mm.specialstockindicator = T1.MSLB_SOBKZ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_specialstockid = SRC.dim_specialstockid;
				
UPDATE TMP_INSERT_8_STOCK fitp
SET dim_StorageLocEntryDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_8_STOCK fitp, TMP_fact_mm_postingdate_2_min mm /* This has min date */
WHERE mm.Dim_Partid = fitp.Dim_Partid AND mm.Dim_Vendorid = fitp.Dim_Vendorid ;

Update TMP_INSERT_8_STOCK fitp
SET dim_LastReceivedDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_8_STOCK fitp, TMP_fact_mm_postingdate_2 mm     /* This has max date */
WHERE mm.Dim_Partid = fitp.Dim_Partid AND mm.Dim_Vendorid = fitp.Dim_Vendorid;

UPDATE TMP_INSERT_8_STOCK fitp
SET ct_LastReceivedQty =  mm.ct_QtyEntryUOM
FROM TMP_INSERT_8_STOCK fitp,TMP_fact_mm_2 mm
WHERE mm.Dim_Partid = fitp.Dim_Partid AND mm.Dim_Vendorid = fitp.Dim_Vendorid;

INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid, 
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid, 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_8_STOCK;			
				
 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';
	
/*Insert 9 */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0 
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'W';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT distinct mm.Dim_Partid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm 
GROUP by mm.Dim_Partid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT distinct mm.Dim_Partid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT distinct mm.Dim_Partid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.min_DateValue;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MSKU';
				
DROP TABLE IF EXISTS TMP_INSERT_9_STOCK;
CREATE TABLE TMP_INSERT_9_STOCK
AS
  SELECT  a.MSKU_KULAB ct_StockQty,
          a.MSKU_KUEIN ct_TotalRestrictedStock,
          a.MSKU_KUINS ct_StockInQInsp,
          ifnull((CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3
               ELSE (b.multiplier * a.MSKU_KULAB)
          END), 0) amt_StockValueAmt,
          ifnull((CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3
               ELSE (b.multiplier * a.MSKU_KULAB)
          END), 0) amt_StockValueAmt_GBL,
          ifnull((b.multiplier * a.MSKU_KUINS), 0) amt_StockInQInspAmt,
          ifnull((b.multiplier * a.MSKU_KUINS), 0) amt_StockInQInspAmt_GBL,
          ifnull(b.multiplier, 0) amt_StdUnitPrice,
          ifnull(b.multiplier, 0) amt_StdUnitPrice_GBL,
	ifnull((CASE WHEN MSKU_KULAB+MSKU_KUINS+MSKU_KUEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KULAB) END 
			+ CASE WHEN MSKU_KUINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUINS) END
			+ CASE WHEN MSKU_KUEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUEIN) END
		  ) END ), 0) amt_OnHand,
	ifnull((CASE WHEN MSKU_KULAB+MSKU_KUINS+MSKU_KUEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KULAB) END 
			+ CASE WHEN MSKU_KUINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUINS) END
			+ CASE WHEN MSKU_KUEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUEIN) END
		  ) END ), 0) amt_OnHand_GBL,
          0.00 ct_LastReceivedQty,
          a.MSKU_CHARG dd_BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          convert(bigint, 1) dim_StorageLocEntryDateid,
          convert(bigint, 1) dim_LastReceivedDateid,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          cm.dim_customerid,
          dim_currencyid,
          1 dim_stocktypeid,
          convert(bigint, 1) dim_specialstockid,
          convert(bigint, 1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
		(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (order by '') fact_inventoryagingid, 
		convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
		,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		,p.PurchOrg /* Dim_PurchaseOrgid */
		,a.MSKU_SOBKZ /* dim_specialstockid */
     FROM MSKU a
          INNER JOIN dim_plant p
             ON a.MSKU_WERKS = p.PlantCode
         INNER JOIN tmp2_MBEW_NO_BWTAR b ON     b.MATNR = a.MSKU_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKU_MATNR AND dp.Plant = a.MSKU_WERKS
          INNER JOIN dim_customer cm
             ON cm.CustomerNumber = MSKU_KUNNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
	      CROSS JOIN tmp_stkcategory_001 stkc;
		  
MERGE INTO TMP_INSERT_9_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_9_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_StockInQInspAmt_GBL = ST.amt_StockInQInspAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_9_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_9_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_9_STOCK FA
USING ( SELECT fact_inventoryagingid, IFNULL(mm.dim_specialstockid,1) dim_specialstockid
		FROM TMP_INSERT_9_STOCK T1
				LEFT JOIN dim_specialstock mm on mm.specialstockindicator = T1.MSKU_SOBKZ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_specialstockid = SRC.dim_specialstockid;
				
UPDATE TMP_INSERT_9_STOCK fitp
SET dim_StorageLocEntryDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_9_STOCK fitp, TMP_fact_mm_postingdate_2_min mm /* This has min date */
WHERE mm.Dim_Partid = fitp.Dim_Partid;

Update TMP_INSERT_9_STOCK fitp
SET dim_LastReceivedDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_9_STOCK fitp, TMP_fact_mm_postingdate_2 mm     /* This has max date */
WHERE mm.Dim_Partid = fitp.Dim_Partid;

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Customerid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid, 
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Customerid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid, 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_9_STOCK;
				
 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

/*Insert 10 */
/* -- Customer stocks with no valuation	*/
DROP TABLE IF EXISTS TMP_INSERT_10_STOCK;
CREATE TABLE TMP_INSERT_10_STOCK
AS
  SELECT  a.MSKU_KULAB ct_StockQty,
          a.MSKU_KUEIN ct_TotalRestrictedStock,
          a.MSKU_KUINS ct_StockInQInsp,
          0 amt_StockValueAmt,
          0 amt_StockValueAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          0 amt_OnHand,
          0 amt_OnHand_GBL,
          0.00 ct_LastReceivedQty,
          a.MSKU_CHARG dd_BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          convert(bigint, 1) dim_StorageLocEntryDateid,
          convert(bigint, 1) dim_LastReceivedDateid,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          cm.dim_customerid,
          dim_currencyid,
          1 dim_stocktypeid,
          convert(bigint, 1)  dim_specialstockid,
          convert(bigint, 1)  Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
		(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (order by '') fact_inventoryagingid, 
		convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
		,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		,p.PurchOrg /* Dim_PurchaseOrgid */
		,a.MSKU_SOBKZ /* dim_specialstockid */
     FROM MSKU a
          INNER JOIN dim_plant p
             ON a.MSKU_WERKS = p.PlantCode
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKU_MATNR AND dp.Plant = a.MSKU_WERKS
          INNER JOIN dim_customer cm
             ON cm.CustomerNumber = MSKU_KUNNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
		  CROSS JOIN tmp_stkcategory_001 stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_inventoryaging_tmp_populate c2
           WHERE c2.Dim_Partid = dp.Dim_Partid
                AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_CustomerId = cm.Dim_CustomerId);
				
MERGE INTO TMP_INSERT_10_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_10_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_10_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_10_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_10_STOCK FA
USING ( SELECT fact_inventoryagingid, IFNULL(mm.dim_specialstockid,1) dim_specialstockid
		FROM TMP_INSERT_10_STOCK T1
				LEFT JOIN dim_specialstock mm on mm.specialstockindicator = T1.MSKU_SOBKZ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_specialstockid = SRC.dim_specialstockid;
				
UPDATE TMP_INSERT_10_STOCK fitp
SET dim_StorageLocEntryDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_10_STOCK fitp, TMP_fact_mm_postingdate_2_min mm /* This has min date */
WHERE mm.Dim_Partid = fitp.Dim_Partid;

Update TMP_INSERT_10_STOCK fitp
SET dim_LastReceivedDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_10_STOCK fitp, TMP_fact_mm_postingdate_2 mm     /* This has max date */
WHERE mm.Dim_Partid = fitp.Dim_Partid;

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Customerid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid, 
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Customerid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid, 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_10_STOCK;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
						where table_name = 'fact_inventoryaging_tmp_populate';

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.Dim_Partid, mm.Dim_StorageLocationid, mmdt.Dim_Dateid,
	row_number() over(partition by mm.Dim_Partid, mm.Dim_StorageLocationid order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate;

UPDATE 
fact_inventoryaging_tmp_populate f
SET f.dim_dateidtransaction = IFNULL(mmdt.Dim_Dateid,1)
FROM 
fact_inventoryaging_tmp_populate f
		LEFT JOIN tmp_custom_postproc_dim_date mmdt 
		ON mmdt.Dim_Partid = f.dim_Partid 
		and mmdt.Dim_StorageLocationid = f.dim_StorageLocationid
		and mmdt.RowSeqNo = 1
WHERE f.dim_stockcategoryid in (2,3);

/*Insert 11 */

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm 
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = '641';

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MARC';

	
	
/* LK: 12 Sep - Done till here. Do fact_purchase before coming back here */	
	
/* # INTRA stock analytics */

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (fp.ct_QtyIssued - (CASE WHEN fp.ct_ReceivedQty < 0 THEN -1*fp.ct_ReceivedQty ELSE fp.ct_ReceivedQty END))
		* (fp.PriceConversion_EqualTo / fp.PriceConversion_Denom) ct_StockInTransit,
          fp.dd_DocumentNo,
          fp.dd_DocumentItemNo,
          fp.Dim_Partid,
          fp.dim_currencyid,	/* Local currency */
		  fp.Dim_currencyid Dim_CurrencyId_TRA,		/* inventoryaging should have tran curr same as local curr */
		  fp.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,	
		  1 amt_ExchangeRate,	/* Tran->Local exch rate is 1 */
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          fp.Dim_PlantidSupplying, 
	  (fp.amt_ExchangeRate_GBL/case when fp.amt_ExchangeRate = 0 THEN 1 ELSE fp.amt_ExchangeRate END) amt_ExchangeRate_GBL,	
	  /*LK: amt_ExchangeRate_GBL : (Tran to Global) * (Local to Tran) = (Tran to Global)/(Tran to Local)  = Local to Global */
	  (fp.amt_UnitPrice * case when fp.amt_ExchangeRate = 0 THEN 1 ELSE fp.amt_ExchangeRate END) amt_UnitPrice	/* LK: fp in tran, ivntryaging in local */
     FROM fact_purchase fp
          INNER JOIN dim_purchasemisc pm ON fp.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
          INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = fp.Dim_Currencyid
     WHERE CASE WHEN fp.ct_ReceivedQty < 0 THEN -1*fp.ct_ReceivedQty ELSE fp.ct_ReceivedQty END < fp.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and fp.Dim_PlantidSupplying <> 1 and fp.Dim_Vendorid = 1;

DROP TABLE IF EXISTS TMP_INSERT_11_STOCK;
CREATE TABLE TMP_INSERT_11_STOCK AS 
SELECT distinct fp.ct_StockInTransit,
          (b.multiplier * fp.ct_StockInTransit) amt_StockInTransitAmt,
          (b.multiplier * fp.ct_StockInTransit) * fp.amt_ExchangeRate_GBL amt_StockInTransitAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          b.multiplier * fp.amt_ExchangeRate_GBL amt_StdUnitPrice_GBL,
          (b.multiplier * fp.ct_StockInTransit) amt_OnHand,
          0 amt_OnHand_GBL,
          0 ct_LastReceivedQty,
          'Not Set' dd_BatchNo,
          fp.dd_DocumentNo dd_DocumentNo,
          fp.dd_DocumentItemNo dd_DocumentItemNo,
          convert(bigint, 1) dim_StorageLocEntryDateid,
          1 dim_LastReceivedDateid,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          fp.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          fp.Dim_PlantidSupplying Dim_SupplyingPlantId, 
	
		convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,
		fp.dim_currencyid_TRA,
		fp.dim_currencyid_GBL
	  	,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
     FROM dim_plant p
          INNER JOIN MARC a ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN tmp_fact_fp_001 fp ON fp.Dim_Partid = dp.Dim_Partid
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency 
		  CROSS JOIN tmp_GlobalCurr_001 gbl
		  CROSS JOIN tmp_stkcategory_001 stkc;


DROP TABLE IF EXISTS TMP_INSERT_11STOCKB;
CREATE TABLE TMP_INSERT_11STOCKB
AS 
select 
(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
CT_STOCKINTRANSIT,
AMT_STOCKINTRANSITAMT,
AMT_STOCKINTRANSITAMT_GBL,
AMT_STDUNITPRICE,
AMT_STDUNITPRICE_GBL,
AMT_ONHAND,
AMT_ONHAND_GBL,
CT_LASTRECEIVEDQTY,
DD_BATCHNO,
DD_DOCUMENTNO,
DD_DOCUMENTITEMNO,
DIM_STORAGELOCENTRYDATEID,
DIM_LASTRECEIVEDDATEID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_COMPANYID,
DIM_VENDORID,
DIM_CURRENCYID,
DIM_STOCKTYPEID,
DIM_SPECIALSTOCKID,
DIM_PURCHASEORGID,
DIM_PURCHASEGROUPID,
DIM_PRODUCTHIERARCHYID,
DIM_UNITOFMEASUREID,
DIM_MOVEMENTINDICATORID,
DIM_COSTCENTERID,
DIM_STOCKCATEGORYID,
DIM_SUPPLYINGPLANTID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
CURRENCYCODE,
PGLOBALCURRENCY
from TMP_INSERT_11_STOCK;



		   
		   
		  
		  
MERGE INTO TMP_INSERT_11STOCKB ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_11STOCKB t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;
	
MERGE INTO TMP_INSERT_11STOCKB ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(mmdt.Dim_Dateid,1) Dim_Dateid
	 FROM TMP_INSERT_11STOCKB t
		LEFT JOIN tmp_custom_postproc_dim_date mmdt ON mmdt.dd_DocumentNo = t.dd_DocumentNo and mmdt.dd_DocumentItemNo = t.dd_DocumentItemNo and mmdt.RowSeqNo = 1
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.dim_StorageLocEntryDateid = SRC.Dim_Dateid;
	
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
	amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	Dim_SupplyingPlantId,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT ct_StockInTransit,
	amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	Dim_SupplyingPlantId,
	fact_inventoryagingid,
	1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_11STOCKB;

/********* Replaced with above ********
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                ct_StockInTransfer,
                                amt_StockInTransferAmt,
                                amt_StockInTransferAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
   SELECT ifnull(MARC_TRAME,0) ct_StockInTransit,
          ifnull((b.multiplier * MARC_TRAME),0) amt_StockInTransitAmt,
          ifnull(b.multiplier * MARC_TRAME
	   * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0)
            amt_StockInTransitAmt_GBL,
          ifnull(MARC_UMLMC, 0) ct_StockInTransfer,
          ifnull((b.multiplier * MARC_UMLMC),0) amt_StockInTransferAmt,
          ifnull(b.multiplier * MARC_UMLMC
           * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0) 
	    amt_StockInTransferAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          1  StorageLocEntryDate,
          1 LastReceivedDate,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          1,
          ifnull(pg.Dim_PurchaseGroupid,1),
          ifnull(dph.dim_producthierarchyid,1),
          1,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          1,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	  1,1,1,1,1,1, 1,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)
     FROM dim_plant p
          INNER JOIN MARC a ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
********************************************/

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

 
  /*# INTER stock analytics */

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm 
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = '643';

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'STO';

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (a.ct_QtyIssued - (CASE WHEN a.ct_ReceivedQty < 0 THEN -1*a.ct_ReceivedQty ELSE a.ct_ReceivedQty END))
		* (a.PriceConversion_EqualTo / a.PriceConversion_Denom) ct_StockInTransit,
          a.amt_StdUnitPrice,
	  a.amt_UnitPrice,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
          a.Dim_Partid,
          a.dim_plantidordering,
          a.dim_currencyid,
		  a.Dim_currencyid Dim_CurrencyId_TRA,			
		  a.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,	
		  1 amt_ExchangeRate,	 
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          a.Dim_PlantIdSupplying, 
	     (a.amt_ExchangeRate_GBL/case when a.amt_ExchangeRate = 0 THEN 1 ELSE a.amt_ExchangeRate END) amt_ExchangeRate_GBL
     FROM fact_purchase a
          INNER JOIN dim_purchasemisc pm ON a.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
     WHERE CASE WHEN a.ct_ReceivedQty < 0 THEN -1*a.ct_ReceivedQty ELSE a.ct_ReceivedQty END < a.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and a.Dim_PlantidSupplying <> 1
           and not exists (select 1 from fact_inventoryaging_tmp_populate fi inner join dim_stockcategory sc on fi.dim_stockcategoryid = sc.Dim_StockCategoryid
                          where fi.dim_Partid = a.dim_partid and fi.dd_DocumentNo = a.dd_DocumentNo and sc.CategoryCode = 'MARC');

DROP TABLE IF EXISTS fitp_ui0;
CREATE TABLE fitp_ui0 LIKE fact_inventoryaging_tmp_populate INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE fitp_ui0  ADD PRIMARY KEY (fact_inventoryagingid);
ALTER TABLE fitp_ui0 ADD COLUMN CurrencyCode varchar(20) default 'Not Set';
ALTER TABLE fitp_ui0 ADD COLUMN pGlobalCurrency varchar(20) default 'Not Set';

INSERT INTO fitp_ui0(ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,CurrencyCode,pGlobalCurrency)
   SELECT a.ct_StockInTransit,
          (b.multiplier * a.ct_StockInTransit) amt_StockInTransitAmt,
          (b.multiplier * a.ct_StockInTransit * a.amt_ExchangeRate_GBL) amt_StockInTransitAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          (b.multiplier * a.amt_ExchangeRate_GBL)  amt_StdUnitPrice_GBL,
          (b.multiplier * a.ct_StockInTransit) amt_OnHand,
          0 amt_OnHand_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
		  1 StorageLocEntryDate,
          1 LastReceivedDate,
          a.Dim_Partid,
          a.dim_plantidordering,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          a.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          a.Dim_PlantIdSupplying, 
	     (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
		  convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		  a.amt_ExchangeRate, 
		  a.dim_Currencyid_TRA,
		  a.dim_Currencyid_GBL
		 ,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		 ,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
     FROM tmp_fact_fp_001 a
          INNER JOIN dim_plant p ON a.dim_plantidordering = p.dim_plantid
          INNER JOIN dim_part dp ON dp.dim_partid = a.dim_partid
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = dp.PartNumber AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
		  CROSS JOIN tmp_stkcategory_001 stkc;
		  
MERGE INTO fitp_ui0 ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM fitp_ui0 t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;
	
MERGE INTO fitp_ui0 FA
USING ( SELECT fact_inventoryagingid, IFNULL(mmdt.Dim_Dateid,1) dim_StorageLocEntryDateid
		FROM fitp_ui0 T1
				LEFT JOIN tmp_custom_postproc_dim_date mmdt ON (mmdt.dd_DocumentNo = T1.dd_DocumentNo 
											and mmdt.dd_DocumentItemNo = T1.dd_DocumentItemNo
											and mmdt.RowSeqNo = 1) ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_StorageLocEntryDateid = SRC.dim_StorageLocEntryDateid;



Insert into fact_inventoryaging_tmp_populate(ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
                                fact_inventoryagingid,
                                amt_ExchangeRate_GBL,
                                amt_ExchangeRate, 
				dim_Currencyid_TRA,
				dim_Currencyid_GBL)
Select ct_StockInTransit,
	amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
        amt_OnHand,
        amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	Dim_SupplyingPlantId,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, 
	dim_Currencyid_TRA,
	dim_Currencyid_GBL
from fitp_ui0;

drop table if exists fitp_ui0;


update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';


/*********************** LK: 26 Apr change : Insert queries 7/8/9/10/11 Ends ***********************/

/* Update 1 */

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
and ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x 
			       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
				     AND ((x.VPRSV = 'S' AND x.STPRS > 0) OR (x.VPRSV = 'V' AND x.VERPR > 0)))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,
((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, 
m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  convert(decimal (18,5),(b.STPRS / b.PEINH))
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  convert(decimal (18,5),(b.VERPR / b.PEINH))
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

drop table if exists pl_prt_MBEW_NO_BWTAR;
create table pl_prt_MBEW_NO_BWTAR
as
select distinct ia.dim_partid,ia.dim_plantid,prt.PartNumber,pl.ValuationArea,pl.CompanyCode
from fact_inventoryaging_tmp_populate ia,dim_plant pl,dim_part prt
WHERE ia.dim_partid = prt.Dim_partid
 and ia.dim_plantid = pl.dim_plantid;

DROP TABLE IF EXISTS TMP_UPDT_INV;							   
CREATE TABLE TMP_UPDT_INV
AS
SELECT
	ia.FACT_INVENTORYAGINGID
	,MBEW_BWPRH
	,MBEW_LPLPR
	,MBEW_VMVER
	,MBEW_ZPLP1
	,MBEW_STPRV
	,MBEW_VPLPR
	,dt1.Dim_Dateid dt1_Dim_Dateid
	,dt2.Dim_Dateid dt2_Dim_Dateid
	,m.VPRSV
	,m.STPRS
	,m.PEINH
	,m.VERPR
FROM  
fact_inventoryaging_tmp_populate ia
		INNER JOIN pl_prt_MBEW_NO_BWTAR pl ON ia.dim_partid = pl.Dim_partid
    		                                   and ia.dim_plantid = pl.dim_plantid
		INNER JOIN tmp2_MBEW_NO_BWTAR m ON pl.PartNumber = m.MATNR
    						   AND pl.ValuationArea = m.BWKEY
		LEFT JOIN dim_Date dt1 ON dt1.DateValue = m.MBEW_LAEPR 
				       AND dt1.CompanyCode = pl.CompanyCode
		LEFT JOIN dim_Date dt2 ON dt2.DateValue = m.MBEW_ZPLD2 
		                       AND dt2.CompanyCode = pl.CompanyCode;
							   
UPDATE fact_inventoryaging_tmp_populate ia
  set amt_CommericalPrice1 = ifnull(t.MBEW_BWPRH,0),
      amt_CurPlannedPrice = ifnull(t.MBEW_LPLPR,0),
      amt_MovingAvgPrice = ifnull(t.MBEW_VMVER,0),
      amt_PlannedPrice1 = ifnull(t.MBEW_ZPLP1,0),
      amt_PreviousPrice = ifnull(t.MBEW_STPRV,0),
      amt_PrevPlannedPrice = ifnull(t.MBEW_VPLPR,0),
      Dim_DateIdLastChangedPrice = ifnull(t.dt1_Dim_Dateid,1),
      Dim_DateIdPlannedPrice2 = ifnull(t.dt2_Dim_Dateid,1),
      amt_MtlDlvrCost = ifnull((CASE t.VPRSV WHEN 'S' THEN t.STPRS / t.PEINH
                                WHEN 'V' THEN t.VERPR / t.PEINH END), 0)
FROM  fact_inventoryaging_tmp_populate ia, TMP_UPDT_INV t
WHERE ia.FACT_INVENTORYAGINGID = t.FACT_INVENTORYAGINGID;

DROP TABLE IF EXISTS TMP_UPDT_INV;


/* 2nd and 3rd updates	*/

DROP TABLE IF EXISTS TMP_UPDT_INV_PURCH;
CREATE TABLE TMP_UPDT_INV_PURCH
AS
SELECT fp.dd_DocumentNo,fp.dd_DocumentItemNo, fp.Dim_ConsumptionTypeid,fp.Dim_DocumentStatusid,fp.Dim_DocumentTypeid,fp.Dim_IncoTerm1id,fp.Dim_ItemCategoryid,fp.Dim_ItemStatusid, fp.Dim_Termid,fp.Dim_PurchaseMiscid,fp.Dim_PlantidSupplying,
	row_number() over(partition by fp.dd_DocumentNo,fp.dd_DocumentItemNo order by '') rono
FROM fact_purchase fp;

UPDATE fact_inventoryaging_tmp_populate ia
   SET ia.Dim_ConsumptionTypeid = fp.Dim_ConsumptionTypeid,
       ia.Dim_DocumentStatusid = fp.Dim_DocumentStatusid,
       ia.Dim_DocumentTypeid = fp.Dim_DocumentTypeid,
       ia.Dim_IncoTermid = fp.Dim_IncoTerm1id,
       ia.Dim_ItemCategoryid = fp.Dim_ItemCategoryid,
       ia.Dim_ItemStatusid = fp.Dim_ItemStatusid,
       ia.Dim_Termid = fp.Dim_Termid,
       ia.Dim_PurchaseMiscid = fp.Dim_PurchaseMiscid,
       ia.Dim_SupplyingPlantId = fp.Dim_PlantidSupplying
FROM fact_inventoryaging_tmp_populate ia,TMP_UPDT_INV_PURCH fp
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	   AND fp.rono = 1;

/* COMMENT - THIS UPDATE NEEDS TO BE CHECKED AGAIN.  */

UPDATE fact_purchase fp
SET fp.ct_UnrestrictedOnHandQty = ia.ct_StockQty
	,fp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_purchase fp,fact_inventoryaging_tmp_populate ia
WHERE ia.dim_Partid = fp.Dim_Partid
AND ia.dim_Plantid = fp.Dim_PlantidSupplying
AND ia.dd_documentno = fp.dd_documentno
AND ia.dd_documentitemno = fp.dd_documentitemno
AND fp.ct_UnrestrictedOnHandQty <> ia.ct_StockQty;

/* Update 4	*/
/* This update is using up a lot of memory on VW and does not complete */


drop table if exists tmp_keph;
CREATE TABLE tmp_keph 
AS 
SELECT KEPH_KALNR,KEPH_BWVAR,KEPH_KADKY,KEPH_KST001,k.KEPH_KST002,k.KEPH_KST003 + k.KEPH_KST004 + k.KEPH_KST005 + k.KEPH_KST006 + k.KEPH_KST007 + k.KEPH_KST008
                        + k.KEPH_KST009 + k.KEPH_KST010 + k.KEPH_KST011 + k.KEPH_KST012 + k.KEPH_KST013 + k.KEPH_KST014
                        + k.KEPH_KST015 + k.KEPH_KST016 + k.KEPH_KST017 + k.KEPH_KST018 + k.KEPH_KST019 + k.KEPH_KST020
                        + k.KEPH_KST021 + k.KEPH_KST022 + k.KEPH_KST023 + k.KEPH_KST024 + k.KEPH_KST025 + k.KEPH_KST026
                        + k.KEPH_KST027 as keph_othercosts, k.KEPH_KST001 + k.KEPH_KST002 as keph_allcosts
FROM keph k;

update tmp_keph
set keph_allcosts = keph_allcosts + keph_othercosts;

drop table if exists tmp_dim_date;
create table tmp_dim_date as
select DateValue,FinancialYear,FinancialMonthNumber,(FinancialYear * 100) + FinancialMonthNumber as yymm,CompanyCode from dim_date;

/*Use intermediate table ia1 as this was failing in fossil2 */

drop table if exists ia1;
create table ia1 as
select distinct ia.dim_Partid,ia.dim_plantid,pl.CompanyCode,pl.ValuationArea,p.PartNumber
from fact_inventoryaging_tmp_populate ia,dim_part p, dim_plant pl
where  ia.dim_Partid = p.Dim_Partid and ia.dim_plantid = pl.dim_plantid;

drop table if exists tmp3_MBEW_NO_BWTAR;
create table tmp3_MBEW_NO_BWTAR as
select distinct ia.dim_Partid,ia.dim_plantid,ia.CompanyCode, m.MATNR,m.BWKEY,m.MBEW_KALN1,m.MBEW_BWVA2,((m.MBEW_PDATL * 100) + m.MBEW_PPRDL) as m_yymm,m.multiplier
from ia1 ia, tmp2_MBEW_NO_BWTAR m
where ia.PartNumber = m.MATNR and ia.ValuationArea = m.BWKEY;

/*Create an intermediate table, which would decrease the number of rows read from keph */

drop table if exists tmp4_MBEW_NO_BWTAR;
create table tmp4_MBEW_NO_BWTAR as
select distinct m.*
from tmp3_MBEW_NO_BWTAR m,tmp_dim_date dt
where cast(dt.yymm as integer) = cast(m.m_yymm as integer);

drop table if exists tmp4_dim_date;
create table tmp4_dim_date as
select distinct dt.*
from tmp4_MBEW_NO_BWTAR m,tmp_dim_date dt
where dt.yymm = m.m_yymm;

drop table if exists tmp2_keph;
create table tmp2_keph as
select DISTINCT m.dim_Partid,m.dim_plantid,m.CompanyCode,m.m_yymm,k.KEPH_KST001,k.KEPH_KST002,k.keph_othercosts,k.keph_allcosts,k.KEPH_KADKY,k.KEPH_KALNR,k.KEPH_BWVAR
FROM   tmp4_MBEW_NO_BWTAR m, tmp_keph k
where k.KEPH_KALNR = m.MBEW_KALN1 and k.KEPH_BWVAR = m.MBEW_BWVA2 and k.keph_allcosts = m.multiplier;


MERGE INTO fact_inventoryaging_tmp_populate ia
USING 
( 
SELECT DISTINCT
ia.fact_inventoryagingid,
max(k.KEPH_KST001) KEPH_KST001,
max(k.KEPH_KST002) KEPH_KST002,
max(k.keph_othercosts) keph_othercosts
FROM
fact_inventoryaging_tmp_populate ia
		INNER JOIN tmp2_keph k ON ( ia.dim_Partid = k.Dim_Partid 
							   and  ia.dim_plantid = k.dim_plantid )
		INNER JOIN tmp4_dim_date dt ON ( k.KEPH_KADKY = dt.DateValue 
									and  k.CompanyCode = dt.CompanyCode
									and  dt.yymm = k.m_yymm )
GROUP BY ia.fact_inventoryagingid
) fb
ON fb.fact_inventoryagingid = ia.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
SET ia.amt_MtlDlvrCost = fb.KEPH_KST001,
       ia.amt_OverheadCost = fb.KEPH_KST002,
       ia.amt_OtherCost = fb.keph_othercosts,
	   ia.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
;

/* Issam change 24th Jun */
/* Update Purchase Order and Sales Order Open Qtys */

MERGE INTO fact_inventoryaging_tmp_populate ia
USING 
( 
SELECT fpr.Dim_partid,
SUM(case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (fpr.ct_DeliveryQty - fpr.ct_ReceivedQty) < 0 then 0.0000 else (fpr.ct_DeliveryQty - fpr.ct_ReceivedQty) end)  as ct_POOpenQty_sum
FROM 
fact_purchase fpr,
Dim_PurchaseMisc atrb
WHERE 
fpr.Dim_PurchaseMiscid = atrb.Dim_PurchaseMiscid
group by fpr.Dim_partid) fb
ON fb.dim_partid = ia.dim_partid
WHEN MATCHED THEN UPDATE
SET ia.ct_POOpenQty = ifnull(fb.ct_POOpenQty_sum,0);

MERGE INTO fact_inventoryaging_tmp_populate ia
USING 
( 
SELECT fso.Dim_partid,
SUM(fso.ct_AfsOpenQty) as ct_AfsOpenQty_sum
FROM 
fact_salesorder fso
group by fso.Dim_partid) fb
ON fb.dim_partid = ia.dim_partid
WHEN MATCHED THEN UPDATE
SET ia.ct_SOOpenQty = ifnull(fb.ct_AfsOpenQty_sum,0);

/* Update 5	*/

MERGE INTO dim_part
USING 
( 
SELECT f_inv.dim_Partid,
sum(f_inv.ct_StockQty+f_inv.ct_StockInQInsp+f_inv.ct_BlockedStock+f_inv.ct_StockInTransfer) as TotalPlantStock_sum
FROM 
fact_inventoryaging_tmp_populate f_inv
group by f_inv.dim_Partid) fb
ON fb.dim_partid = dim_part.dim_partid
WHEN MATCHED THEN UPDATE
SET dim_part.TotalPlantStock = ifnull(fb.TotalPlantStock_sum,0);

MERGE INTO dim_part
USING 
( 
SELECT f_inv.dim_Partid,
sum(f_inv.amt_StockValueAmt+f_inv.amt_StockInQInspAmt+f_inv.amt_BlockedStockAmt+f_inv.amt_StockInTransferAmt) as TotalPlantStockAmt_sum
FROM 
fact_inventoryaging_tmp_populate f_inv
group by f_inv.dim_Partid) fb
ON fb.dim_partid = dim_part.dim_partid
WHEN MATCHED THEN UPDATE
SET dim_part.TotalPlantStockAmt = ifnull(fb.TotalPlantStockAmt_sum,0);

/* # WIP stock analytics */

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'WIP';

INSERT INTO fact_inventoryaging_tmp_populate(ct_StockQty,
                               amt_StockValueAmt,
                               ct_LastReceivedQty,
                               dim_StorageLocEntryDateid,
                               dim_LastReceivedDateid,
                               dim_Partid,
                               dim_Plantid,
                               dim_StorageLocationid,
                               dim_Companyid,
                               dim_Vendorid,
                               dim_Currencyid,
				   dim_Currencyid_TRA,
				   dim_Currencyid_GBL,
                               dim_StockTypeid,
                               dim_specialstockid,
                               Dim_PurchaseOrgid,
                               Dim_PurchaseGroupid,
                               dim_producthierarchyid,
                               Dim_UnitOfMeasureid,
                               Dim_MovementIndicatorid,
                               Dim_ConsumptionTypeid,
                               dim_costcenterid,
                               Dim_DocumentStatusid,
                               Dim_DocumentTypeid,
                               Dim_IncoTermid,
                               Dim_ItemCategoryid,
                               Dim_ItemStatusid,
                               Dim_Termid,
                               Dim_PurchaseMiscid,
                               amt_StockValueAmt_GBL,
                               ct_TotalRestrictedStock,
                               ct_StockInQInsp,
                               ct_BlockedStock,
                               ct_StockInTransfer,
                               ct_UnrestrictedConsgnStock,
                               ct_RestrictedConsgnStock,
                               ct_BlockedConsgnStock,
                               ct_ConsgnStockInQInsp,
                               ct_BlockedStockReturns,
                               amt_BlockedStockAmt,
                               amt_BlockedStockAmt_GBL,
                               amt_StockInQInspAmt,
                               amt_StockInQInspAmt_GBL,
                               amt_StockInTransferAmt,
                               amt_StockInTransferAmt_GBL,
                               amt_UnrestrictedConsgnStockAmt,
                               amt_UnrestrictedConsgnStockAmt_GBL,
                               amt_StdUnitPrice,
                               amt_StdUnitPrice_GBL,
                               dim_stockcategoryid,
                               ct_StockInTransit,
                               amt_StockInTransitAmt,
                               amt_StockInTransitAmt_GBL,
                               Dim_SupplyingPlantId,
                               amt_MtlDlvrCost,
                               amt_OverheadCost,
                               amt_OtherCost,
                               amt_WIPBalance,
                               ct_WIPAging,
			       ct_WIPQty,
                               amt_MovingAvgPrice,
                               amt_PreviousPrice,
                               amt_CommericalPrice1,
                               amt_PlannedPrice1,
                               amt_PrevPlannedPrice,
                               amt_CurPlannedPrice,
                               Dim_DateIdPlannedPrice2,
                               Dim_DateIdLastChangedPrice,
                               Dim_DateidActualRelease,
                               Dim_DateidActualStart,
                               dd_ProdOrdernumber,
                               dd_ProdOrderitemno,
                               dd_Plannedorderno,
                               dim_productionorderstatusid,
                               Dim_productionordertypeid,
			       fact_inventoryagingid,
			       amt_ExchangeRate_GBL,amt_ExchangeRate)
  SELECT 0 ct_StockQty,
        0 amt_StockValueAmt,
        0 ct_LastReceivedQty,
        1 dim_StorageLocEntryDateid,
        1 dim_LastReceivedDateid,
        po.Dim_PartidItem dim_Partid,
        po.Dim_Plantid dim_Plantid,
        po.Dim_StorageLocationid dim_StorageLocationid,
        po.Dim_Companyid dim_Companyid,
        1 dim_Vendorid,
        po.Dim_Currencyid dim_Currencyid,
		po.Dim_Currencyid dim_Currencyid_TRA,
		po.Dim_Currencyid_GBL dim_Currencyid_GBL,
        po.Dim_StockTypeid dim_StockTypeid,
        po.dim_specialstockid dim_specialstockid,
        po.Dim_PurchaseOrgid Dim_PurchaseOrgid,
        1 Dim_PurchaseGroupid,
        1 dim_producthierarchyid,
        po.Dim_UnitOfMeasureid Dim_UnitOfMeasureid,
        1 Dim_MovementIndicatorid,
        po.Dim_ConsumptionTypeid Dim_ConsumptionTypeid,
        1 dim_costcenterid,
        1 Dim_DocumentStatusid,
        1 Dim_DocumentTypeid,
        1 Dim_IncoTermid,
        1 Dim_ItemCategoryid,
        1 Dim_ItemStatusid,
        1 Dim_Termid,
        1 Dim_PurchaseMiscid,
        0 amt_StockValueAmt_GBL,
        0 ct_TotalRestrictedStock,
        0 ct_StockInQInsp,
        0 ct_BlockedStock,
        0 ct_StockInTransfer,
        0 ct_UnrestrictedConsgnStock,
        0 ct_RestrictedConsgnStock,
        0 ct_BlockedConsgnStock,
        0 ct_ConsgnStockInQInsp,
        0 ct_BlockedStockReturns,
        0 amt_BlockedStockAmt,
        0 amt_BlockedStockAmt_GBL,
        0 amt_StockInQInspAmt,
        0 amt_StockInQInspAmt_GBL,
        0 amt_StockInTransferAmt,
        0 amt_StockInTransferAmt_GBL,
        0 amt_UnrestrictedConsgnStockAmt,
        0 amt_UnrestrictedConsgnStockAmt_GBL,
        0 amt_StdUnitPrice,
        0 amt_StdUnitPrice_GBL,
        stkc.dim_stockcategoryid,
        0 ct_StockInTransit,
        0 amt_StockInTransitAmt,
        0 amt_StockInTransitAmt_GBL,
        1 Dim_SupplyingPlantId,
        0 amt_MtlDlvrCost,
        0 amt_OverheadCost,
        0 amt_OtherCost,
        po.amt_WIPBalance * po.amt_ExchangeRate amt_WIPBalance,
		0 ct_WIPAging,
		ifnull(po.ct_WIPQty,0),
        0 amt_MovingAvgPrice,
        0 amt_PreviousPrice,
        0 amt_CommericalPrice1,
        0 amt_PlannedPrice1,
        0 amt_PrevPlannedPrice,
        0 amt_CurPlannedPrice,
        1 Dim_DateIdPlannedPrice2,
        1 Dim_DateIdLastChangedPrice,
        po.Dim_DateidActualRelease Dim_DateidActualRelease,
        po.Dim_DateidActualStart Dim_DateidActualStart,
        po.dd_ordernumber dd_ProdOrdernumber,
        po.dd_orderitemno dd_ProdOrderitemno,
        po.dd_plannedorderno dd_Plannedorderno,
        po.dim_productionorderstatusid dim_productionorderstatusid,
        po.Dim_ordertypeid Dim_productionordertypeid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	(po.amt_ExchangeRate_GBL/case when po.amt_ExchangeRate = 0 THEN 1 ELSE po.amt_ExchangeRate END) amt_ExchangeRate_GBL,	/* LK: GBL exchange rate for inventoryaging is local-->global */
	1 amt_ExchangeRate			
  FROM fact_productionorder po 
	INNER JOIN dim_productionorderstatus ost ON po.dim_productionorderstatusid = ost.dim_productionorderstatusid
	INNER JOIN dim_part p ON po.Dim_PartidItem = p.dim_partid
	INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = po.Dim_Currencyid
	CROSS JOIN tmp_stkcategory_001 stkc
  WHERE ost.Closed = 'Not Set' 
      and ost.Delivered = 'Not Set'
      and ost.Created = 'Not Set' 
      and ost.TechnicallyCompleted = 'Not Set'
      and po.ct_OrderItemQty - po.ct_GRQty > 0;
	  
MERGE INTO fact_inventoryaging_tmp_populate FA
USING ( SELECT fact_inventoryagingid, ifnull(days_between(current_date,pard.DateValue),0) ct_WIPAging
		FROM fact_inventoryaging_tmp_populate T1
				LEFT JOIN dim_date pard ON ( pard.Dim_Dateid = t1.Dim_DateidActualRelease 
										and t1.Dim_DateidActualRelease > 1 ) ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.ct_WIPAging = SRC.ct_WIPAging
WHERE FA.ct_WIPAging <> SRC.ct_WIPAging;

DELETE FROM fact_materialmovement_tmp_invagin;



/* Begin 15 Oct 2013 changes */

DROP TABLE IF EXISTS fact_materialmovement_tmp0;
CREATE TABLE fact_materialmovement_tmp0 as 
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity 
FROM fact_materialmovement fmm 
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE 
mt.movementtype IN ('101', '501') 
AND pd.DateValue >= current_date - INTERVAL '30' DAY
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GRQty_Late30 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp0 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp0;


DROP TABLE IF EXISTS fact_materialmovement_tmp1;
CREATE TABLE fact_materialmovement_tmp1 as 
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity 
FROM fact_materialmovement fmm 
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE 
mt.movementtype IN ('101', '501') 
AND pd.DateValue >= (current_date - INTERVAL '60' DAY) 
AND pd.DateValue <= (current_date - INTERVAL '31' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GRQty_31_60 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp1 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp1;


DROP TABLE IF EXISTS fact_materialmovement_tmp2;
CREATE TABLE fact_materialmovement_tmp2 as 
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity 
FROM fact_materialmovement fmm 
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE 
mt.movementtype IN ('101', '501') 
AND pd.DateValue >= (current_date - INTERVAL '90' DAY) 
AND pd.DateValue <= (current_date - INTERVAL '61' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GRQty_61_90 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp2 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp2;


DROP TABLE IF EXISTS fact_materialmovement_tmp3;
CREATE TABLE fact_materialmovement_tmp3 as 
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity 
FROM fact_materialmovement fmm 
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE 
mt.movementtype IN ('101', '501') 
AND pd.DateValue between (current_date - INTERVAL '180' DAY(3)) and (current_date - INTERVAL '91' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GRQty_91_180 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp3 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp3;
						
	
DROP TABLE IF EXISTS fact_materialmovement_tmp4;
CREATE TABLE fact_materialmovement_tmp4 as 
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity 
FROM fact_materialmovement fmm 
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE 
mt.movementtype IN ('201', '261', '301', '601', '641') 
AND pd.DateValue >= (current_date - INTERVAL '30' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GIQty_Late30 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp4 fm
ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp4;


DROP TABLE IF EXISTS fact_materialmovement_tmp5;
CREATE TABLE fact_materialmovement_tmp5 as 
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity 
FROM fact_materialmovement fmm 
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE 
mt.movementtype IN ('201', '261', '301', '601', '641') 
AND pd.DateValue >= (current_date - INTERVAL '60' DAY) AND pd.DateValue <= (current_date - INTERVAL '31' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GIQty_31_60 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp5 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp5;


DROP TABLE IF EXISTS fact_materialmovement_tmp6;
CREATE TABLE fact_materialmovement_tmp6 as 
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity 
FROM fact_materialmovement fmm 
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE 
mt.movementtype IN ('201', '261', '301', '601', '641') 
AND pd.DateValue >= (current_date - INTERVAL '90' DAY) AND pd.DateValue <= (current_date - INTERVAL '61' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GIQty_61_90 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp6 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp6;


DROP TABLE IF EXISTS fact_materialmovement_tmp7;
CREATE TABLE fact_materialmovement_tmp7 as 
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity 
FROM fact_materialmovement fmm 
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE 
mt.movementtype IN ('201', '261', '301', '601', '641') 
AND pd.DateValue >= (current_date - INTERVAL '180' DAY(3)) AND pd.DateValue <= (current_date - INTERVAL '91' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GIQty_91_180 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp7 fm
ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp7;							
/* End 15 Oct 2013 changes */

DROP TABLE IF EXISTS tmp_fact_purchase; 
CREATE TABLE tmp_fact_purchase AS
select  max(fp.dim_profitcenterid)dim_profitcenterid,fp.dd_documentno ,fp.dd_documentitemno
from fact_purchase fp
where   fp.dim_profitcenterid <> 1
group by fp.dd_documentno ,fp.dd_documentitemno;

update fact_inventoryaging_tmp_populate inv
set inv.dim_profitcenterid = fp.dim_profitcenterid
from 
fact_inventoryaging_tmp_populate inv,
tmp_fact_purchase fp
where 
fp.dd_documentno = inv.dd_documentno 
and fp.dd_documentitemno = inv.dd_documentitemno
and inv.dim_profitcenterid = 1;



/* Sales Order stock analytics */


DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MSKA';

update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';

DELETE FROM MSKA WHERE MSKA_KAEIN = 0 and MSKA_KAINS = 0 and MSKA_KALAB = 0 and MSKA_KASPE = 0;

   /* Intermediate tables -- replace MBEW_NO_BWTAR  */
drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = cast((b.STPRS / b.PEINH) as decimal (18,5))
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  cast((b.VERPR / b.PEINH) as decimal (18,5))
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

delete from NUMBER_FOUNTAIN where TABLE_NAME = 'dim_storagelocation';
insert into NUMBER_FOUNTAIN (MAX_ID,TABLE_NAME) 
select ifnull(( select max(dim_storagelocationid) from dim_storagelocation), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) a,'dim_storagelocation';

  INSERT INTO dim_storagelocation(Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent,dim_storagelocationid)
   SELECT DISTINCT MSKA_LGORT,
                   MSKA_LGORT,
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   MSKA_WERKS,
                   'Not Set',
                   current_timestamp,
                   1,
				   row_number() over (order by '')+(select MAX_ID from NUMBER_FOUNTAIN where TABLE_NAME = 'dim_storagelocation')	/*This should be changed later ( to max + 1 ). This should be standard - common for all such scenarios*/
     FROM MSKA m
    WHERE MSKA_LGORT IS NOT NULL
      AND not exists (SELECT 1
                            FROM dim_storagelocation d
                           WHERE d.LocationCode = m.MSKA_LGORT AND d.Plant = m.MSKA_WERKS);

		
/* StorageLocEntryDate/LastReceivedDate */

/* Simply use the posting date id for the min/max date */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0 
AS
SELECT mm.*,mmdt.DateValue,mt.MovementType
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'E';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm 
GROUP by mm.Dim_Partid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate,mm.Dim_MovementTypeid,mm.MovementType,
	mm.dim_specialstockid, mm.Dim_MovementIndicatorid,
	ROW_NUMBER() OVER(PARTITION BY mm.Dim_Partid ORDER BY mm.DateValue DESC) RowSeqNo
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate,mm.Dim_MovementTypeid,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.min_DateValue;

DROP TABLE IF EXISTS TMP_INSERT_12_STOCK;
CREATE TABLE TMP_INSERT_12_STOCK
AS
  SELECT a.MSKA_KALAB ct_StockQty,
          a.MSKA_KAEIN ct_TotalRestrictedStock,
          a.MSKA_KAINS ct_StockInQInsp,
          a.MSKA_KASPE ct_BlockedStock,
          0 ct_StockInTransfer,
          0 ct_BlockedConsgnStock,
          0 ct_ConsgnStockInQInsp,
          0 ct_RestrictedConsgnStock,
          0 ct_UnrestrictedConsgnStock,
          0 ct_BlockedStockReturns,
          ifnull((b.multiplier * a.MSKA_KALAB),0) amt_StockValueAmt,
          ifnull((b.multiplier * a.MSKA_KALAB),0) amt_StockValueAmt_GBL,
          ifnull((b.multiplier * a.MSKA_KASPE), 0) amt_BlockedStockAmt,
          ifnull((b.multiplier * a.MSKA_KASPE),0) amt_BlockedStockAmt_GBL,
          ifnull((b.multiplier * a.MSKA_KAINS), 0) amt_StockInQInspAmt,
          ifnull((b.multiplier * a.MSKA_KAINS),0) amt_StockInQInspAmt_GBL,
          0 amt_StockInTransferAmt,
          0 amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt,
          0 amt_UnrestrictedConsgnStockAmt_GBL,
          IFNULL(b.multiplier,0)  amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          (b.multiplier * (a.MSKA_KALAB+a.MSKA_KAEIN+a.MSKA_KAINS+a.MSKA_KASPE)) amt_OnHand,
          0 amt_OnHand_GBL,
          0 ct_LastReceivedQty,
          ifnull(a.MSKA_CHARG,'Not Set') dd_BatchNo,
          'Not Set' dd_ValuationType,
          a.MSKA_VBELN dd_DocumentNo,
          a.MSKA_POSNR dd_DocumentItemNo,
          'Not Set' dd_MovementType,
          convert(bigint,1) dim_StorageLocEntryDateid,
          convert(bigint,1) dim_LastReceivedDateid,
          dp.Dim_PartID,
          p.dim_plantid,
          sloc.Dim_StorageLocationid,
          cmp.dim_Companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
		  dc.dim_currencyid dim_currencyid_TRA,	
		  ifnull((SELECT Dim_Currencyid
                 FROM Dim_Currency dcr
                WHERE dcr.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ), 1) Dim_Currencyid_GBL,
          1 dim_stocktypeid,
          spstk.dim_specialstockid,
          convert(bigint,1) Dim_PurchaseOrgid,
          pg.Dim_PurchaseGroupid,
          dph.dim_producthierarchyid,
          uom.Dim_UnitOfMeasureid,
          convert(bigint ,1) Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid, 
		  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
		  convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
	      1 amt_ExchangeRate
		  ,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		  ,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		  ,p.PurchOrg /* Dim_PurchaseOrgid */
		  ,dt.dim_dateid /* dim_LastReceivedDateid, dim_StorageLocEntryDateid */
     FROM MSKA a
          INNER JOIN dim_plant p ON a.MSKA_WERKS = p.PlantCode
          INNER JOIN dim_Company cmp ON cmp.CompanyCode = p.CompanyCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSKA_MATNR AND dp.Plant = a.MSKA_WERKS
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_date dt ON a.MSKA_ERSDA = dt.DateValue and dt.CompanyCode = p.CompanyCode
          INNER JOIN dim_storagelocation sloc ON sloc.LocationCode = MSKA_LGORT AND sloc.Plant = MSKA_WERKS
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = cmp.Currency
          INNER JOIN dim_specialstock spstk ON spstk.specialstockindicator = a.MSKA_SOBKZ 
		  CROSS JOIN tmp_GlobalCurr_001 gbl
	      CROSS JOIN tmp_stkcategory_001 stkc;
		  
MERGE INTO TMP_INSERT_12_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_12_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_StockInQInspAmt_GBL = ST.amt_StockInQInspAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_12_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_12_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_12_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(mm.Dim_MovementIndicatorid, 1) Dim_MovementIndicatorid, ifnull(mm.dim_DateIDPostingDate, t.dim_dateid) dim_DateIDPostingDate, ifnull(mm.MovementType, 'Not Set') MovementType
	 FROM TMP_INSERT_12_STOCK t
		LEFT JOIN TMP_fact_mm_postingdate_2 mm ON mm.dim_partid = t.Dim_PartID and mm.RowSeqNo = 1
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_MovementIndicatorid = SRC.Dim_MovementIndicatorid,
		ST.dim_LastReceivedDateid = SRC.dim_DateIDPostingDate,
		ST.dim_StorageLocEntryDateid = SRC.dim_DateIDPostingDate,
		ST.dd_MovementType = SRC.MovementType;
		
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	ct_BlockedStock,
	ct_StockInTransfer,
	ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock,
	ct_UnrestrictedConsgnStock,
	ct_BlockedStockReturns,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StockInTransferAmt,
	amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_ValuationType,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dd_MovementType,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_StockCategoryid,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	ct_BlockedStock,
	ct_StockInTransfer,
	ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock,
	ct_UnrestrictedConsgnStock,
	ct_BlockedStockReturns,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StockInTransferAmt,
	amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_ValuationType,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dd_MovementType,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_StockCategoryid,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate
FROM TMP_INSERT_12_STOCK;


update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';
	
DROP TABLE IF EXISTS TMP_INSERT_13_STOCK;
CREATE TABLE TMP_INSERT_13_STOCK
AS
  SELECT a.MSKA_KALAB ct_StockQty,
          a.MSKA_KAEIN ct_TotalRestrictedStock,
          a.MSKA_KAINS ct_StockInQInsp,
          a.MSKA_KASPE ct_BlockedStock,
          0 ct_StockInTransfer,
          0 ct_BlockedConsgnStock,
          0 ct_ConsgnStockInQInsp,
          0 ct_RestrictedConsgnStock,
          0 ct_UnrestrictedConsgnStock,
          0 ct_BlockedStockReturns,
          0 amt_StockValueAmt,
          0 amt_StockValueAmt_GBL,
          0 amt_BlockedStockAmt,
          0 amt_BlockedStockAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StockInTransferAmt,
          0 amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt,
          0 amt_UnrestrictedConsgnStockAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          0 amt_OnHand,
          0 amt_OnHand_GBL,
          0 ct_LastReceivedQty,
          ifnull(a.MSKA_CHARG,'Not Set') dd_BatchNo,
          'Not Set' dd_ValuationType,
          a.MSKA_VBELN dd_DocumentNo,
          a.MSKA_POSNR dd_DocumentItemNo,
          'Not Set' dd_MovementType,
          convert(bigint,1) dim_StorageLocEntryDateid,
          convert(bigint,1) dim_LastReceivedDateid,
          dp.Dim_PartID,
          p.dim_plantid,
          sloc.Dim_StorageLocationid,
          cmp.dim_Companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
	  dc.dim_currencyid dim_currencyid_TRA,	
	  ifnull((SELECT Dim_Currencyid
                 FROM Dim_Currency dcr
                WHERE dcr.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ), 1) Dim_Currencyid_GBL,
          1 dim_stocktypeid,
          spstk.dim_specialstockid,
          convert(bigint,1) Dim_PurchaseOrgid,
          pg.Dim_PurchaseGroupid,
          dph.dim_producthierarchyid,
          uom.Dim_UnitOfMeasureid,
          convert(bigint ,1) Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid, 
	      (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	      convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
	      1 amt_ExchangeRate
	  	  ,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		  ,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		  ,p.PurchOrg /* Dim_PurchaseOrgid */
		  ,dt.dim_dateid /* dim_LastReceivedDateid, dim_StorageLocEntryDateid */
     FROM MSKA a
          INNER JOIN dim_plant p ON a.MSKA_WERKS = p.PlantCode
          INNER JOIN dim_Company cmp ON cmp.CompanyCode = p.CompanyCode
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSKA_MATNR AND dp.Plant = a.MSKA_WERKS
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_date dt ON a.MSKA_ERSDA = dt.DateValue and dt.CompanyCode = p.CompanyCode
          INNER JOIN dim_storagelocation sloc ON sloc.LocationCode = MSKA_LGORT AND sloc.Plant = MSKA_WERKS
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = cmp.Currency
          INNER JOIN dim_specialstock spstk ON spstk.specialstockindicator = a.MSKA_SOBKZ
	      CROSS JOIN tmp_GlobalCurr_001 gbl
	      CROSS JOIN tmp_stkcategory_001 stkc
 WHERE not exists (select 1 from tmp2_MBEW_NO_BWTAR b where b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea);

MERGE INTO TMP_INSERT_13_STOCK ST
USING 
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_13_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE 
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;
		
MERGE INTO TMP_INSERT_13_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_13_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;
	
MERGE INTO TMP_INSERT_13_STOCK ST
USING 
	(SElECT t.fact_inventoryagingid, ifnull(mm.Dim_MovementIndicatorid, 1) Dim_MovementIndicatorid, ifnull(mm.dim_DateIDPostingDate, t.dim_dateid) dim_DateIDPostingDate, ifnull(mm.MovementType, 'Not Set') MovementType
	 FROM TMP_INSERT_13_STOCK t
		LEFT JOIN TMP_fact_mm_postingdate_2 mm ON mm.dim_partid = t.Dim_PartID and mm.RowSeqNo = 1
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_MovementIndicatorid = SRC.Dim_MovementIndicatorid,
		ST.dim_LastReceivedDateid = SRC.dim_DateIDPostingDate,
		ST.dim_StorageLocEntryDateid = SRC.dim_DateIDPostingDate,
		ST.dd_MovementType = SRC.MovementType;
		
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	ct_BlockedStock,
	ct_StockInTransfer,
	ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock,
	ct_UnrestrictedConsgnStock,
	ct_BlockedStockReturns,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StockInTransferAmt,
	amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_ValuationType,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dd_MovementType,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_StockCategoryid,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	ct_BlockedStock,
	ct_StockInTransfer,
	ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock,
	ct_UnrestrictedConsgnStock,
	ct_BlockedStockReturns,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StockInTransferAmt,
	amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_ValuationType,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dd_MovementType,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_StockCategoryid,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate
FROM TMP_INSERT_13_STOCK;



UPDATE fact_inventoryaging_tmp_populate ig
SET ig.dim_profitcenterid = pc.dim_profitcenterid
FROM 
fact_inventoryaging_tmp_populate ig,
dim_part pt, 
Dim_ProfitCenter pc
WHERE ig.dim_PartId = pt.Dim_PartId
 AND pc.ProfitCenterCode = pt.ProfitCenterCode
 AND pc.validto >= current_date
 AND pc.RowIsCurrent = 1;
 
UPDATE fact_inventoryaging_tmp_populate ig
SET ig.dim_profitcenterid = 1
WHERE ig.dim_profitcenterid IS NULL;
 

UPDATE fact_inventoryaging_tmp_populate
SET Dim_DateidActualRelease = 1
WHERE Dim_DateidActualRelease is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_DateidActualStart = 1
WHERE Dim_DateidActualStart is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_ProdOrdernumber = 'Not Set'
WHERE dd_ProdOrdernumber is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_ProdOrderitemno = 0
WHERE dd_ProdOrderitemno is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_Plannedorderno = 'Not Set'
WHERE dd_Plannedorderno is null;

UPDATE fact_inventoryaging_tmp_populate
SET dim_productionorderstatusid = 1
WHERE dim_productionorderstatusid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_productionordertypeid = 1
WHERE Dim_productionordertypeid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_profitcenterid = 1
WHERE Dim_profitcenterid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_partsalesid = 1
WHERE Dim_partsalesid is null;

/* Drop original table and rename staging table to orig table name */
drop table if exists fact_inventoryaging;
rename table fact_inventoryaging_tmp_populate to fact_inventoryaging;

/* start changes 20 Feb for dim_DateidExpiryDate */

/*update fact_inventoryaging i
set dim_DateidExpiryDate = dt1.dim_dateid
from	
fact_inventoryaging i
		INNER JOIN mch1 m ON i.dd_BatchNo = m.mch1_charg
		INNER JOIN dim_company dc ON i.dim_companyid = dc.dim_companyid
		INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid AND dp1.partnumber =  m.mch1_matnr 
		INNER JOIN dim_date dt1 ON dt1.CompanyCode = dc.CompanyCode AND dt1.datevalue = m.mch1_vfdat*/

drop table if exists tmp_for_DateidExpiryDate;
create table tmp_for_DateidExpiryDate as
select distinct dt1.dim_dateid,i.fact_inventoryagingid, row_number() over (partition by fact_inventoryagingid order by fact_inventoryagingid) as row_num
from
fact_inventoryaging i
INNER JOIN mch1 m ON i.dd_BatchNo = m.mch1_charg
INNER JOIN dim_company dc ON i.dim_companyid = dc.dim_companyid
INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid AND dp1.partnumber = m.mch1_matnr
INNER JOIN dim_date dt1 ON dt1.CompanyCode = dc.CompanyCode AND dt1.datevalue = m.mch1_vfdat
where 1=1;


update fact_inventoryaging i
set dim_DateidExpiryDate = dt1.dim_dateid
FROM fact_inventoryaging i,tmp_for_DateidExpiryDate dt1
where i.fact_inventoryagingid=dt1.fact_inventoryagingid
and row_num=1
and dim_DateidExpiryDate <> dt1.dim_dateid;

drop table if exists tmp_for_DateidExpiryDate;


UPDATE fact_inventoryaging i
set i.Dim_vendorid = v.dim_vendorid
from	
fact_inventoryaging i
		INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid
		INNER JOIN dim_vendor v ON dp1.ManfacturerNumber =  v.vendornumber
where v.rowiscurrent = 1
AND   i.Dim_VendorId = 1;

update fact_inventoryaging i
set dim_DateidExpiryDate = 1 where dim_DateidExpiryDate is null;

update fact_inventoryaging i
set dim_DateofManufacture = dt1.dim_dateid
from	
fact_inventoryaging i
		INNER JOIN mch1 m ON i.dd_BatchNo = m.mch1_charg
		INNER JOIN dim_company dc ON i.dim_companyid = dc.dim_companyid
		INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid AND dp1.partnumber =  m.mch1_matnr 
		INNER JOIN dim_date dt1 ON dt1.CompanyCode = dc.CompanyCode AND dt1.datevalue = m.MCH1_HSDAT;
		
/*update fact_inventoryaging i
set dim_Dateoflastgoodsreceipt = dt1.dim_dateid
from	
fact_inventoryaging i
		INNER JOIN mch1 m ON i.dd_BatchNo = m.mch1_charg
		INNER JOIN dim_company dc ON i.dim_companyid = dc.dim_companyid
		INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid AND dp1.partnumber =  m.mch1_matnr 
		INNER JOIN dim_date dt1 ON dt1.CompanyCode = dc.CompanyCode AND dt1.datevalue = m.MCH1_LWEDT*/

drop table if exists tmp_for_Dateoflastgoodsreceipt;
create table tmp_for_Dateoflastgoodsreceipt as
select distinct dt1.dim_dateid,i.fact_inventoryagingid, row_number() over (partition by fact_inventoryagingid order by fact_inventoryagingid) as row_num
from
fact_inventoryaging i
                INNER JOIN mch1 m ON i.dd_BatchNo = m.mch1_charg
                INNER JOIN dim_company dc ON i.dim_companyid = dc.dim_companyid
                INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid AND dp1.partnumber =  m.mch1_matnr
                INNER JOIN dim_date dt1 ON dt1.CompanyCode = dc.CompanyCode AND dt1.datevalue = m.MCH1_LWEDT
WHERE 1=1;

 update fact_inventoryaging i
set dim_Dateoflastgoodsreceipt = dt1.dim_dateid
FROM fact_inventoryaging i,tmp_for_Dateoflastgoodsreceipt dt1
where i.fact_inventoryagingid=dt1.fact_inventoryagingid
and row_num=1
and dim_Dateoflastgoodsreceipt <> dt1.dim_dateid;
 drop table if exists tmp_for_Dateoflastgoodsreceipt;


 	   
   
/* Update BW Hierarchy */

update fact_inventoryaging f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_inventoryaging f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and current_date between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

update fact_inventoryaging f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_inventoryaging f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

/* COGS */

DROP TABLE IF EXISTS csv_cogs;
CREATE TABLE csv_cogs
AS
SELECT * FROM EMDTempoCC4.csv_cogs;

update fact_inventoryaging f
set f.amt_cogsactualrate_emd = ifnull(c.Z_GCACTF,0)
	,f.amt_cogsfixedrate_emd = ifnull(c.Z_GCFIXF,0)
	,f.amt_cogsfixedplanrate_emd = ifnull(c.Z_GCPLFF,0)
	,f.amt_cogsplanrate_emd = ifnull(c.Z_GCPLRF,0)
	,f.amt_cogsprevyearfixedrate_emd = ifnull(c.Z_GCPYFF,0)
	,f.amt_cogsprevyearrate_emd = ifnull(c.Z_GCPYRF,0)
	,f.amt_cogsprevyearto1_emd = ifnull(c.Z_GCPYTF1,0)
	,f.amt_cogsprevyearto2_emd = ifnull(c.Z_GCPYTF2,0)
	,f.amt_cogsprevyearto3_emd = ifnull(c.Z_GCPYTF3,0)
	,f.amt_cogsturnoverrate1_emd = ifnull(c.Z_GCTOF1,0)
	,f.amt_cogsturnoverrate2_emd = ifnull(c.Z_GCTOF2,0)
	,f.amt_cogsturnoverrate3_emd = ifnull(c.Z_GCTOF3,0)
from fact_inventoryaging f, dim_part dp, dim_company dc, csv_cogs c
where f.dim_partid = dp.dim_partid
	and f.dim_companyid = dc.dim_companyid
	and lpad(c.PRODUCT,18,'0') = lpad(dp.partnumber,18,'0')
	and right('000000' || c.Z_REPUNIT, 6) = dc.company;

/* MDG Part */
DROP TABLE IF EXISTS tmp_dim_mdg_partid;
CREATE TABLE tmp_dim_mdg_partid as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;


UPDATE fact_inventoryaging f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM tmp_dim_mdg_partid tmp, fact_inventoryaging f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);
	  
DROP TABLE IF EXISTS tmp_dim_mdg_partid;

/* add average inventory / inventory coverage - Marius 7 feb 2016 */

DROP TABLE IF EXISTS micstag_lkp_inv_location;
CREATE TABLE micstag_lkp_inv_location
AS
SELECT * FROM EMDTempoCC4.micstag_lkp_inv_location;

DROP TABLE IF EXISTS micstag_lkp_transaction_type;
CREATE TABLE micstag_lkp_transaction_type
AS
SELECT * FROM EMDTempoCC4.micstag_lkp_transaction_type;

DROP TABLE IF EXISTS STSC_DFUVIEW;
CREATE TABLE STSC_DFUVIEW
AS
SELECT * FROM EMDTempoCC4.STSC_DFUVIEW;

DROP TABLE IF EXISTS tmp_inv_cov_flag;
CREATE TABLE tmp_inv_cov_flag
AS
SELECT DISTINCT RIGHT('000000' || CONVERT(VARCHAR(10), a.micstag_lkp_inv_location_org_id),6) company
	,a.micstag_lkp_inv_location_sub_inv_code storagelocationcode
	,a.micstag_lkp_inv_location_add_flag 
FROM micstag_lkp_inv_location a 
WHERE UPPER(a.micstag_lkp_inv_location_add_flag) = 'ADD';

UPDATE fact_inventoryaging f
SET f.dd_invcovflag_emd = 'Not Set'
WHERE f.dd_invcovflag_emd <> 'Not Set';

UPDATE fact_inventoryaging f
SET f.dd_invcovflag_emd = 'X'
FROM fact_inventoryaging f, tmp_inv_cov_flag t, dim_company dc, dim_storagelocation sl
WHERE f.dim_companyid = dc.dim_companyid AND f.dim_storagelocationid = sl.dim_storagelocationid
	AND t.company = dc.company AND t.storagelocationcode = sl.locationcode
	AND f.dd_invcovflag_emd <> 'X';

DROP TABLE IF EXISTS tmp_avg_fcst;
CREATE TABLE tmp_avg_fcst
AS
SELECT stsc_dfuview_dmdunit
	,stsc_dfuview_loc
	,stsc_dfuview_udc_fcstlevel
	,IFNULL(stsc_dfuview_udc_avgfcst,0) stsc_dfuview_udc_avgfcst
FROM STSC_DFUVIEW WHERE stsc_dfuview_udc_fcstlevel = 111;

INSERT INTO tmp_avg_fcst(stsc_dfuview_dmdunit,stsc_dfuview_loc,stsc_dfuview_udc_fcstlevel,stsc_dfuview_udc_avgfcst)
SELECT 
	stsc_dfuview_udc_prodlocalid stsc_dfuview_dmdunit
	,stsc_dfuview_loc
	,stsc_dfuview_udc_fcstlevel
	,IFNULL(stsc_dfuview_udc_avgfcst,0) stsc_dfuview_udc_avgfcst
FROM STSC_DFUVIEW WHERE stsc_dfuview_udc_fcstlevel = 111 AND stsc_dfuview_dmdunit <> stsc_dfuview_udc_prodlocalid;

DROP TABLE IF EXISTS tmp_avg_fcst_company;
CREATE TABLE tmp_avg_fcst_company
AS
SELECT DISTINCT b.micstag_lkp_transaction_type_org_id
	,b.micstag_lkp_transaction_type_dp_code 
FROM micstag_lkp_transaction_type b;

DROP TABLE IF EXISTS tmp_avg_fcst_final;
CREATE TABLE tmp_avg_fcst_final
AS
SELECT RIGHT('000000' || CONVERT(VARCHAR(10), b.micstag_lkp_transaction_type_org_id), 6) micstag_lkp_transaction_type_org_id
	,a.stsc_dfuview_dmdunit
	,SUM(a.stsc_dfuview_udc_avgfcst) stsc_dfuview_udc_avgfcst
FROM tmp_avg_fcst a
	INNER JOIN tmp_avg_fcst_company b ON a.stsc_dfuview_loc = b.micstag_lkp_transaction_type_dp_code
GROUP BY RIGHT('000000' || CONVERT(VARCHAR(10), b.micstag_lkp_transaction_type_org_id), 6), a.stsc_dfuview_dmdunit;

DROP TABLE IF EXISTS tmp_avg_fcst_updt;
CREATE TABLE tmp_avg_fcst_updt
AS
SELECT dc.company
	,dp.partnumber
	,MAX(f.fact_inventoryagingid) fact_inventoryagingid
	,MAX(t.stsc_dfuview_udc_avgfcst) stsc_dfuview_udc_avgfcst
FROM fact_inventoryaging f
	INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	INNER JOIN dim_part dp ON f.dim_partid = dp.dim_partid
	INNER JOIN tmp_avg_fcst_final t ON dc.company = t.micstag_lkp_transaction_type_org_id AND dp.partnumber = t.stsc_dfuview_dmdunit
WHERE f.dd_invcovflag_emd = 'X'
GROUP BY dc.company, dp.partnumber;

UPDATE fact_inventoryaging
SET ct_avgfcst_emd = 0
WHERE ct_avgfcst_emd <> 0;

UPDATE fact_inventoryaging f
SET f.ct_avgfcst_emd = t.stsc_dfuview_udc_avgfcst
FROM fact_inventoryaging f, tmp_avg_fcst_updt t
WHERE f.fact_inventoryagingid = t.fact_inventoryagingid
	AND f.ct_avgfcst_emd <> t.stsc_dfuview_udc_avgfcst;
	
UPDATE fact_inventoryaging f
SET f.dim_dateidlastprocessed = (select dim_dateid from dim_date d where d.datevalue = current_date and d.companycode = 'Not Set');

/* OP Rate EMD - Marius */

UPDATE fact_inventoryaging f
SET f.amt_ExchangeRate_GBL = 1
WHERE ifnull(f.amt_ExchangeRate_GBL,-1) <> 1;

DROP TABLE IF EXISTS csv_oprate;
CREATE TABLE csv_oprate
AS
SELECT * FROM EMDTempoCC4.csv_oprate;

update  csv_oprate
set exrate = exrate /100
where fromc = 'TWD';

UPDATE fact_inventoryaging f
SET f.amt_ExchangeRate_GBL = ifnull(r.exrate,1)
FROM fact_inventoryaging f, dim_currency dc, csv_oprate r
WHERE f.dim_currencyid = dc.dim_currencyid
	AND dc.CurrencyCode = r.fromc
	AND r.toc = (SELECT ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD'))
	AND ifnull(f.amt_ExchangeRate_GBL,-1) <> ifnull(r.exrate,1);

/* OP Rate EMD - Marius */

/* Geo Hierarchy - Marius */

update fact_inventoryaging f
set f.dim_bwhierarchycountryid = dim_con.dim_bwhierarchycountryid
from fact_inventoryaging f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
and    dim_prd.business in ('DIV.32') and dim_con.internalhierarchyid = '548WP23VF6R8ICTMIER1FW7R0'
and    dp.dim_plantid = f.dim_plantid   
and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_inventoryaging f
set f.dim_bwhierarchycountryid = dim_con.dim_bwhierarchycountryid
from fact_inventoryaging f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
and    dim_prd.business  in ('DIV.31','DIV.35','DIV.34') and dim_con.internalhierarchyid = '548YXNWXHSJ2Y19OSUCHY2CL7'
and    dp.dim_plantid = f.dim_plantid   
and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_inventoryaging f
set f.dim_bwhierarchycountryid = dim_con.dim_bwhierarchycountryid
from fact_inventoryaging f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
and    dim_prd.business  in ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88') and dim_con.internalhierarchyid = '549HWG7F0J8VZ1P8YIYH1CPQ3'
and    dp.dim_plantid = f.dim_plantid   
and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_inventoryaging f
set f.dim_bwhierarchycountryid = dim_con.dim_bwhierarchycountryid
from fact_inventoryaging f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
and    dim_prd.business  in ('DIV-87') and dim_con.internalhierarchyid = '54G8L55S3E80Q2SXZYJAELHKR'
and    dp.dim_plantid = f.dim_plantid   
and    dp.country = dim_con.nodehierarchynamelvl7;

/* End Geo Hierarchy - Marius */

/* BW Country Hierarchy for LS Applied & Research / Process Solutions - Oana */
update fact_inventoryaging f
	set f.dim_countryhierpsid = dch.dim_countryhierpsid
from fact_inventoryaging f, dim_company dc, dim_countryhierps dch
where dch.country = dc.country
and f.dim_companyid = dc.dim_companyid
and f.dim_countryhierpsid <> dch.dim_countryhierpsid;

update fact_inventoryaging f
	set f.dim_countryhierarid = dch.dim_countryhierarid
from fact_inventoryaging f, dim_company dc, dim_countryhierar dch
where dch.country = dc.country
and f.dim_companyid = dc.dim_companyid
and f.dim_countryhierarid <> dch.dim_countryhierarid;
/* END BW Country Hierarchy for LS Applied & Research / Process Solutions - Oana */

/* LS Cluster 28 apr - Marius */

update fact_inventoryaging f
set f.dim_clusterid = dc.dim_clusterid
from fact_inventoryaging f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> dc.dim_clusterid;
	
/* end LS cluster */

UPDATE fact_inventoryaging f
SET AMT_EXCHANGERATE_CUSTOM = 0.1
FROM fact_inventoryaging f, dim_currency dc
where f.dim_currencyid_tra = dc.dim_currencyid  
and dc.CURRENCYCODE in ('TND');


UPDATE fact_inventoryaging f
SET AMT_EXCHANGERATE_CUSTOM = 100
FROM fact_inventoryaging f, dim_currency dc
where f.dim_currencyid_tra = dc.dim_currencyid  
and dc.CURRENCYCODE in ('CLP','COP','HUF','IDR','JPY','KRW','TWD','VND');


UPDATE fact_inventoryaging f
SET ct_baseuomratioKG = 1/1000
FROM fact_inventoryaging f,MARM uc, dim_part dp
WHERE f.dim_partid = dp.dim_partid 
and uc.marm_matnr = dp.partnumber 
and uc.marm_meinh = 'G';

UPDATE fact_inventoryaging f
SET ct_baseuomratioKG = MARM_UMREN/MARM_UMREZ
FROM fact_inventoryaging f,MARM uc, dim_part dp
WHERE f.dim_partid = dp.dim_partid 
and uc.marm_matnr = dp.partnumber 
and uc.marm_meinh = 'KG';




UPDATE fact_inventoryaging f
SET ct_baseuomratioPC = MARM_UMREN/MARM_UMREZ
FROM fact_inventoryaging f,MARM uc, dim_part dp
WHERE f.dim_partid = dp.dim_partid 
and uc.marm_matnr = dp.partnumber 
and uc.marm_meinh = 'PC';

UPDATE fact_inventoryaging f_invagng
SET ct_baseuomratioPC = 1
where ct_baseuomratioPC = 0;

/* Bring container   from JDA Product dimension */
DROP TABLE IF EXISTS tmp_dim_jda_product;
CREATE TABLE tmp_dim_jda_product
AS SELECT DISTINCT container , ITEMGLOBALCODE
FROM EMDTEMPOCC4.dim_jda_product;

UPDATE fact_inventoryaging f
SET  f.dd_jdacontainer =  container
FROM  fact_inventoryaging f, dim_mdg_part mdg, tmp_dim_jda_product prod
where f.dim_mdg_partid = mdg.dim_mdg_partid
and mdg.PARTNUMBER = prod.ITEMGLOBALCODE;


UPDATE fact_inventoryaging f 
SET dd_materialstatus = 
CASE 
         WHEN d1.datevalue < CURRENT_DATE AND d1.dim_dateid <> 1  THEN 'Expired' 
         ELSE 
           CASE 
             WHEN ct_blockedstock > 0 THEN 'Blocked' 
             ELSE 
               CASE 
                 WHEN ct_totalrestrictedstock > 0 THEN 'Restricted' 
                 ELSE 'Unrestricted' 
               END 
           END 
       END 
FROM   dim_date d1, 
       fact_inventoryaging f 
WHERE  f.dim_dateidexpirydate = d1.dim_dateid;

/* Commercial View*/
/*
update fact_inventoryaging f
	set f.dim_commercialviewid=ds.dim_commercialviewid
	from dim_commercialview ds, dim_plant a, dim_bwproducthierarchy b, fact_inventoryaging f
	where ds.SOLDTOCOUNTRY=a.COUNTRY
	and ds.UPPERPRODHIER=b.BUSINESS
	and f.DIM_BWPRODUCTHIERARCHYID=b.DIM_BWPRODUCTHIERARCHYID
	and f.dim_plantid = a.dim_plantid
	and f.dim_commercialviewid <> ds.dim_commercialviewid*/

	/*Added by CatalinM @22.08.2016*/

update fact_inventoryaging f
    set f.dim_countryhierpsid = dch.dim_countryhierpsid
from fact_inventoryaging f, dim_company dc, dim_countryhierps dch
where dch.country = dc.country
and f.dim_companyid = dc.dim_companyid
and f.dim_countryhierpsid <> dch.dim_countryhierpsid;

update fact_inventoryaging f
    set f.dim_countryhierarid = dch.dim_countryhierarid
from fact_inventoryaging f, dim_company dc, dim_countryhierar dch
where dch.country = dc.country
and f.dim_companyid = dc.dim_companyid
and f.dim_countryhierarid <> dch.dim_countryhierarid;
 
/* Marius 22 Aug 2016 Count of Materials with Safety Stock < Unrestricted Stock */

drop table if exists tmp_inv_part;
create table tmp_inv_part
as
select f_invagng.dim_partid
	,CASE WHEN SUM( ct_StockQty ) < MAX( (prt.SafetyStock) ) THEN 1 ELSE 0 END flag
	,max(fact_inventoryagingid) id
from fact_inventoryaging f_invagng
	inner join dim_part prt on f_invagng.dim_partid = prt.dim_partid
group by f_invagng.dim_partid;

update fact_inventoryaging f
set f.ct_countmaterialsafetystock = flag
from fact_inventoryaging f
	inner join tmp_inv_part  t on f.fact_inventoryagingid = t.id
where f.ct_countmaterialsafetystock <> t.flag;


/*@Catalin Add master data from dim_part with safetystock >0 */

delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging';	
INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_inventoryaging',IFNULL(MAX(fact_inventoryagingid),0)
FROM fact_inventoryaging;
insert into fact_inventoryaging
(FACT_INVENTORYAGINGID
,CT_STOCKQTY
,AMT_STOCKVALUEAMT
,CT_LASTRECEIVEDQTY
,DD_BATCHNO
,DD_VALUATIONTYPE
,DD_DOCUMENTNO
,DD_DOCUMENTITEMNO
,DD_MOVEMENTTYPE
,DIM_STORAGELOCENTRYDATEID
,DIM_LASTRECEIVEDDATEID
,DIM_PARTID
,DIM_PLANTID
,DIM_STORAGELOCATIONID
,DIM_COMPANYID
,DIM_VENDORID
,DIM_CURRENCYID
,DIM_STOCKTYPEID
,DIM_SPECIALSTOCKID
,DIM_PURCHASEORGID
,DIM_PURCHASEGROUPID
,DIM_PRODUCTHIERARCHYID
,DIM_UNITOFMEASUREID
,DIM_MOVEMENTINDICATORID
,DIM_CONSUMPTIONTYPEID
,DIM_COSTCENTERID
,DIM_DOCUMENTSTATUSID
,DIM_DOCUMENTTYPEID
,DIM_INCOTERMID
,DIM_ITEMCATEGORYID
,DIM_ITEMSTATUSID
,DIM_TERMID
,DIM_PURCHASEMISCID
,AMT_STOCKVALUEAMT_GBL
,CT_TOTALRESTRICTEDSTOCK
,CT_STOCKINQINSP
,CT_BLOCKEDSTOCK
,CT_STOCKINTRANSFER
,CT_UNRESTRICTEDCONSGNSTOCK
,CT_RESTRICTEDCONSGNSTOCK
,CT_BLOCKEDCONSGNSTOCK
,CT_CONSGNSTOCKINQINSP
,CT_BLOCKEDSTOCKRETURNS
,AMT_BLOCKEDSTOCKAMT
,AMT_BLOCKEDSTOCKAMT_GBL
,AMT_STOCKINQINSPAMT
,AMT_STOCKINQINSPAMT_GBL
,AMT_STOCKINTRANSFERAMT
,AMT_STOCKINTRANSFERAMT_GBL
,AMT_UNRESTRICTEDCONSGNSTOCKAMT
,AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL
,AMT_STDUNITPRICE
,AMT_STDUNITPRICE_GBL
,DIM_STOCKCATEGORYID
,CT_STOCKINTRANSIT
,AMT_STOCKINTRANSITAMT
,AMT_STOCKINTRANSITAMT_GBL
,DIM_SUPPLYINGPLANTID
,AMT_MTLDLVRCOST
,AMT_OVERHEADCOST
,AMT_OTHERCOST
,AMT_WIPBALANCE
,CT_WIPAGING
,AMT_MOVINGAVGPRICE
,AMT_PREVIOUSPRICE
,AMT_COMMERICALPRICE1
,AMT_PLANNEDPRICE1
,AMT_PREVPLANNEDPRICE
,AMT_CURPLANNEDPRICE
,DIM_DATEIDPLANNEDPRICE2
,DIM_DATEIDLASTCHANGEDPRICE
,DIM_CUSTOMERID
,AMT_ONHAND
,AMT_ONHAND_GBL
,CT_POOPENQTY
,CT_SOOPENQTY
,AMT_EXCHANGERATE_GBL
,AMT_EXCHANGERATE
,DIM_PROFITCENTERID
,CT_WIPQTY
,DIM_PARTSALESID
,DIM_DATEIDTRANSACTION
,DIM_DATEIDACTUALRELEASE
,DIM_DATEIDACTUALSTART
,DD_PRODORDERNUMBER
,DD_PRODORDERITEMNO
,DD_PLANNEDORDERNO
,DIM_PRODUCTIONORDERSTATUSID
,DIM_PRODUCTIONORDERTYPEID
,DIM_CURRENCYID_TRA
,DIM_CURRENCYID_GBL
,CT_GRQTY_LATE30
,CT_GRQTY_31_60
,CT_GRQTY_61_90
,CT_GRQTY_91_180
,CT_GIQTY_LATE30
,CT_GIQTY_31_60
,CT_GIQTY_61_90
,CT_GIQTY_91_180
,DIM_DATEIDEXPIRYDATE
,CT_GLOBALONHANDAMT_MERCK
,CT_GLOBALEXTTOTALCOST_MERCK
,CT_LOCALONHANDAMT_MERCK
,CT_LOCALEXTTOTALCOST_MERCK
,AMT_GBLSTDPRICE_MERCK
,AMT_STDPRICEPMRA_MERCK
,DW_INSERT_DATE
,DW_UPDATE_DATE
,DIM_PROJECTSOURCEID
,DIM_MDG_PARTID
,DIM_BWPARTID
,DIM_BWPRODUCTHIERARCHYID
,CT_TOTVALSTKQTY
,AMT_PRICEUNIT
,AMT_COGSACTUALRATE_EMD
,AMT_COGSFIXEDRATE_EMD
,AMT_COGSFIXEDPLANRATE_EMD
,AMT_COGSPLANRATE_EMD
,AMT_COGSPREVYEARFIXEDRATE_EMD
,AMT_COGSPREVYEARRATE_EMD
,AMT_COGSPREVYEARTO1_EMD
,AMT_COGSPREVYEARTO2_EMD
,AMT_COGSPREVYEARTO3_EMD
,AMT_COGSTURNOVERRATE1_EMD
,AMT_COGSTURNOVERRATE2_EMD
,AMT_COGSTURNOVERRATE3_EMD
,DD_INVCOVFLAG_EMD
,CT_AVGFCST_EMD
,DIM_DATEIDLASTPROCESSED
,DIM_BWHIERARCHYCOUNTRYID
,DIM_CLUSTERID
,AMT_EXCHANGERATE_CUSTOM
,DIM_DATEOFMANUFACTURE
,DIM_DATEOFLASTGOODSRECEIPT
,CT_BASEUOMRATIOKG
,DD_JDACONTAINER
,DD_MATERIALSTATUS
,CT_BASEUOMRATIOPC
,CT_BASEUOMRATIOPCCUSTOM
,DIM_COUNTRYHIERPSID
,DIM_COUNTRYHIERARID
,CT_COUNTMATERIALSAFETYSTOCK
)
select 
(SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_inventoryaging' ) + ROW_NUMBER() OVER(order by '') FACT_INVENTORYAGINGID,
0 CT_STOCKQTY,
0 AMT_STOCKVALUEAMT,
0 CT_LASTRECEIVEDQTY,
0 DD_BATCHNO,
'Not Set' DD_VALUATIONTYPE,
0 DD_DOCUMENTNO,
0 DD_DOCUMENTITEMNO,
'Not Set' DD_MOVEMENTTYPE,
1 DIM_STORAGELOCENTRYDATEID,
1 DIM_LASTRECEIVEDDATEID,
d.dim_partid DIM_PARTID,
1 DIM_PLANTID,
1 DIM_STORAGELOCATIONID,
1 DIM_COMPANYID,
1 DIM_VENDORID,
1 DIM_CURRENCYID,
1 DIM_STOCKTYPEID,
1 DIM_SPECIALSTOCKID,
1 DIM_PURCHASEORGID,
1 DIM_PURCHASEGROUPID,
1 DIM_PRODUCTHIERARCHYID,
1 DIM_UNITOFMEASUREID,
1 DIM_MOVEMENTINDICATORID,
1 DIM_CONSUMPTIONTYPEID,
1 DIM_COSTCENTERID,
1 DIM_DOCUMENTSTATUSID,
1 DIM_DOCUMENTTYPEID,
1 DIM_INCOTERMID,
1 DIM_ITEMCATEGORYID,
1 DIM_ITEMSTATUSID,
1 DIM_TERMID,
1 DIM_PURCHASEMISCID,
0 AMT_STOCKVALUEAMT_GBL,
0 CT_TOTALRESTRICTEDSTOCK,
0 CT_STOCKINQINSP,
0 CT_BLOCKEDSTOCK,
0 CT_STOCKINTRANSFER,
0 CT_UNRESTRICTEDCONSGNSTOCK,
0 CT_RESTRICTEDCONSGNSTOCK,
0 CT_BLOCKEDCONSGNSTOCK,
0 CT_CONSGNSTOCKINQINSP,
0 CT_BLOCKEDSTOCKRETURNS,
0 AMT_BLOCKEDSTOCKAMT,
0 AMT_BLOCKEDSTOCKAMT_GBL,
0 AMT_STOCKINQINSPAMT,
0 AMT_STOCKINQINSPAMT_GBL,
0 AMT_STOCKINTRANSFERAMT,
0 AMT_STOCKINTRANSFERAMT_GBL,
0 AMT_UNRESTRICTEDCONSGNSTOCKAMT,
0 AMT_UNRESTRICTEDCONSGNSTOCKAMT_GBL,
0 AMT_STDUNITPRICE,
0 AMT_STDUNITPRICE_GBL,
1 DIM_STOCKCATEGORYID,
0 CT_STOCKINTRANSIT,
0 AMT_STOCKINTRANSITAMT,
0 AMT_STOCKINTRANSITAMT_GBL,
1 DIM_SUPPLYINGPLANTID,
0 AMT_MTLDLVRCOST,
0 AMT_OVERHEADCOST,
0 AMT_OTHERCOST,
0 AMT_WIPBALANCE,
0 CT_WIPAGING,
0 AMT_MOVINGAVGPRICE,
0 AMT_PREVIOUSPRICE,
0 AMT_COMMERICALPRICE1,
0 AMT_PLANNEDPRICE1,
0 AMT_PREVPLANNEDPRICE,
0 AMT_CURPLANNEDPRICE,
1 DIM_DATEIDPLANNEDPRICE2,
1 DIM_DATEIDLASTCHANGEDPRICE,
1 DIM_CUSTOMERID,
0 AMT_ONHAND,
0 AMT_ONHAND_GBL,
0 CT_POOPENQTY,
0 CT_SOOPENQTY,
1 AMT_EXCHANGERATE_GBL,
1 AMT_EXCHANGERATE,
1 DIM_PROFITCENTERID,
0 CT_WIPQTY,
1 DIM_PARTSALESID,
1 DIM_DATEIDTRANSACTION,
1 DIM_DATEIDACTUALRELEASE,
1 DIM_DATEIDACTUALSTART,
0 DD_PRODORDERNUMBER,
0 DD_PRODORDERITEMNO,
0 DD_PLANNEDORDERNO,
1 DIM_PRODUCTIONORDERSTATUSID,
1 DIM_PRODUCTIONORDERTYPEID,
1 DIM_CURRENCYID_TRA,
1 DIM_CURRENCYID_GBL,
0 CT_GRQTY_LATE30,
0 CT_GRQTY_31_60,
0 CT_GRQTY_61_90,
0 CT_GRQTY_91_180,
0 CT_GIQTY_LATE30,
0 CT_GIQTY_31_60,
0 CT_GIQTY_61_90,
0 CT_GIQTY_91_180,
1 DIM_DATEIDEXPIRYDATE,
0 CT_GLOBALONHANDAMT_MERCK,
0 CT_GLOBALEXTTOTALCOST_MERCK,
0 CT_LOCALONHANDAMT_MERCK,
0 CT_LOCALEXTTOTALCOST_MERCK,
0 AMT_GBLSTDPRICE_MERCK,
0 AMT_STDPRICEPMRA_MERCK,
current_timestamp,
current_timestamp,
1 DIM_PROJECTSOURCEID,
1 DIM_MDG_PARTID,
1 DIM_BWPARTID,
1 DIM_BWPRODUCTHIERARCHYID,
0 CT_TOTVALSTKQTY,
0 AMT_PRICEUNIT,
0 AMT_COGSACTUALRATE_EMD,
0 AMT_COGSFIXEDRATE_EMD,
0 AMT_COGSFIXEDPLANRATE_EMD,
0 AMT_COGSPLANRATE_EMD,
0 AMT_COGSPREVYEARFIXEDRATE_EMD,
0 AMT_COGSPREVYEARRATE_EMD,
0 AMT_COGSPREVYEARTO1_EMD,
0 AMT_COGSPREVYEARTO2_EMD,
0 AMT_COGSPREVYEARTO3_EMD,
0 AMT_COGSTURNOVERRATE1_EMD,
0 AMT_COGSTURNOVERRATE2_EMD,
0 AMT_COGSTURNOVERRATE3_EMD,
'Not Set 'DD_INVCOVFLAG_EMD,
0 CT_AVGFCST_EMD,
1 DIM_DATEIDLASTPROCESSED,
1 DIM_BWHIERARCHYCOUNTRYID,
1 DIM_CLUSTERID,
1 AMT_EXCHANGERATE_CUSTOM,
1 DIM_DATEOFMANUFACTURE,
1 DIM_DATEOFLASTGOODSRECEIPT,
0 CT_BASEUOMRATIOKG,
'Not Set 'DD_JDACONTAINER,
'Not Set 'DD_MATERIALSTATUS,
0 CT_BASEUOMRATIOPC,
0 CT_BASEUOMRATIOPCCUSTOM,
1 DIM_COUNTRYHIERPSID,
1 DIM_COUNTRYHIERARID,
0 CT_COUNTMATERIALSAFETYSTOCK
from
dim_part d
WHERE d.dim_partid NOT IN (SELECT f.dim_partid FROM fact_inventoryaging f ) 
AND d.safetystock > 0;



/* Age Cluster */

UPDATE fact_inventoryaging
SET dd_agecluster = 
case 
when mdg_part.materialtype = 'ZSTA' and dtofm.DateValue between current_date- interval '6' month and current_date then '<6 months'
when mdg_part.materialtype = 'ZSTA' and dtofm.DateValue between current_date- interval '12' month and current_date- interval '6' month then '7-12 months'
when mdg_part.materialtype = 'ZSTA' and dtofm.DateValue between current_date- interval '24' month and current_date- interval '12' month then '13-24 months'
when mdg_part.materialtype = 'ZSTA' and dtofm.DateValue < current_date- interval '24' month then
case when dtofm.DateValue = '0001-01-01' and doflgr.DateValue = '0001-01-01' then 'Not Set' 
when dtofm.DateValue = '0001-01-01' and doflgr.DateValue between current_date- interval '6' month and current_date then '<6 months'
when dtofm.DateValue = '0001-01-01' and doflgr.DateValue between current_date- interval '12' month and current_date- interval '6' month then '7-12 months'
when dtofm.DateValue = '0001-01-01' and doflgr.DateValue between current_date- interval '24' month and current_date- interval '12' month then '13-24 months'
else '24+ months' 
end
when doflgr.DateValue between current_date- interval '6' month and current_date then '<6 months'
when doflgr.DateValue between current_date- interval '12' month and current_date- interval '6' month then '7-12 months'
when doflgr.DateValue between current_date- interval '24' month and current_date- interval '12' month then '13-24 months'
when doflgr.DateValue < current_date- interval '24' month then
case when doflgr.DateValue = '0001-01-01' then 'Not Set'
else '24+ months' end
end
from fact_inventoryaging f, dim_mdg_part mdg_part, dim_date dtofm, dim_date doflgr
where f.dim_dateofmanufacture = dtofm.dim_dateid
and   f.dim_mdg_partid = mdg_part.dim_mdg_partid
and   f.dim_dateoflastgoodsreceipt = doflgr.dim_dateid ;


/* Age Cluster Sorting */ 

UPDATE fact_inventoryaging
SET dd_ageclustersort = case when dd_agecluster = '<6 months' then 'a'
                             when dd_agecluster = '7-12 months' then 'b'
                             when dd_agecluster = '13-24 months' then 'c'
                             when dd_agecluster = '24+ months' then 'd'
                             when dd_agecluster = 'Not Set' then 'e'
end;


/*@catalin add Batch status qkz*/

drop table if exists tmp_mch1;
create table tmp_mch1 as
select mch1_zusch,mch1_matnr,mch1_charg, row_number() over(partition by mch1_matnr,mch1_charg order by '') rownumber
from mch1;

update fact_inventoryaging a
set a.dd_batch_status_qkz = ifnull(mch1_zusch,'Not Set')
from fact_inventoryaging a, tmp_mch1 b,dim_part c
where c.dim_Partid = a.dim_Partid
and c.partnumber = b.mch1_matnr
and a.dd_BatchNo = b.mch1_charg
and a.dd_batch_status_qkz <> ifnull(mch1_zusch,'Not Set');
 
  
/* Drop all the intermediate tables created */

drop table if exists materialmovement_tmp_new; 
drop table if exists a1;
drop table if exists a99;
drop table if exists b2;
drop table if exists b2;	
drop table if exists b3;
drop table if exists b4;
drop table if exists b;
drop table if exists ia1;
drop table if exists pl_prt_mbew_no_bwtar;
drop table if exists tmp2_keph;
drop table if exists tmp_MBEW_NO_BWTAR;
drop table if exists tmp_MBEWH_NO_BWTAR;
drop table if exists tmp2_mbew_no_bwtar;
drop table if exists tmp2_tcurf;
drop table if exists tmp2_tcurr;
drop table if exists tmp3_mbew_no_bwtar;
drop table if exists tmp3_tcurr;
drop table if exists tmp4_dim_date;
drop table if exists tmp4_mbew_no_bwtar;
drop table if exists tmp_all_x;
drop table if exists tmp_all_x_2;
drop table if exists tmp_c1;
drop table if exists tmp_c2;
drop table if exists tmp_c3;
drop table if exists tmp_dim_date;
drop table if exists tmp_fact_mm_0;
drop table if exists tmp_fact_mm_1;
drop table if exists tmp_fact_mm_2;
drop table if exists tmp_fact_mm_postingdate_0;
drop table if exists tmp_fact_mm_postingdate_1;
drop table if exists tmp_fact_mm_postingdate_2;
drop table if exists tmp_fact_mm_postingdate_2_min;
drop table if exists tmp_keph;
drop table if exists tmp_lrd;
drop table if exists tmp_mard;
drop table if exists tmp_mbew_no_bwtar;
drop table if exists tmp_tbl_storagelocentrydate;
drop table if exists tmp_tcurf;
drop table if exists tmp_tcurr;
DROP TABLE IF EXISTS tmp_GlobalCurr_001;
DROP TABLE IF EXISTS tmp_stkcategory_001;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins_c1;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_del;
DROP TABLE IF EXISTS TMP_INSERT_1_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_2_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_3_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_4_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_5_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_6_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_7_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_8_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_9_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_10_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_11_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_12_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_13_STOCK;
DROP TABLE IF EXISTS TMP_MCH1;