/* ################################################################################################################## */
/* */
/*   Script         : bi_populate_salesorderdelivery_fact */
/*   Author         : Ashu */
/*   Created On     : 18 Feb 2013 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_salesorderdelivery_fact from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*   8 may 2017      Cornelia                    add Ship To Address */
/*   3  Feb 2016     Cornelia                    Add loading end date vttk_dalen */
/*   27 Jan 2016     Cornelia                    Add  1 day tolerance */
/*   27 Jan 2016     Cornelia                    Change DD_INTERCOMPFLAG logic to use dim_customeridsoldto with merge */
/*   29 Jan 2015     CristianT 1.23				 Add amt_scheduletotal */
/*   15 Dec 2014     Alex D    1.22              Add dim_routeid */
/*   12 Dec 2014     Alex M    1.21              Add dd_Purchaseorderitem */
/*   2  May 2014     George    1.17              Added dd_BusinessCustomerPONo */
/*   14 Feb 2014     George    1.16              Added Dim_CustomerGroup4id      */
/*   12 Feb 2014     George    1.15              Added Dim_ScheduleDeliveryBlockid                                   */
/*	 26 Sep 2013     Issam     1.14              Added fields dd_SDCreateTime dd_DeliveryTime, dd_PickingTime,
												 dd_GITime, dd_SDLineCreateTime											*/
/*   08 Sep 2013     Lokesh    1.10              Currency and exchange rate changes */
/*   13 Aug 2013     Issam     1.9               Added Sales District */
/*   29 Apr 2013     Hiten     1.2               Revised population logic */
/*   24 Feb 2013     Lokesh    1.1		 Add part 2 + while loop logic  */
/*   18 Feb 2013     Ashu      1.0               Existing code migrated to Vectorwise */
/* #################################################################################################################### */

/*Refresh the required tables from corresponding mysql db first ( for testing ) */
/*cd /home/fusionops/ispring/db/schema_migration/bin */
/*./refresh_vw_from_mysql_sameserver.sh_lk albea albea dim_billingdocumenttype dim_controllingarea dim_customer dim_date dim_distributionchannel dim_part dim_plant dim_producthierarchy dim_salesorderheaderstatus dim_salesorderitemstatus dim_storagelocation fact_salesorder fact_salesorderdelivery likp_lips systemproperty */

Drop table if exists flag_holder_722;
Drop table if exists cursor_table1_722;


/*DROP TABLE IF EXISTS NUMBER_FOUNTAIN
CREATE TABLE NUMBER_FOUNTAIN
(
table_name      varchar(40) NOT NULL,
max_id          int     NOT NULL
)*/


DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'fact_salesorderdelivery';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_salesorderdelivery',ifnull(max(fact_salesorderdeliveryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_salesorderdelivery;

Create table cursor_table1_722(
v_iid		  Integer,
v_DlvrDocNo       VARCHAR(50) null,
v_plantcode       VARCHAR(50) null,
v_DlvrItemNo      INTEGER null,
v_SalesDocNo      VARCHAR(50) null,
v_SalesItemNo     INTEGER null,
v_DeliveryQty     DECIMAL(18,4) null,
v_AGI_Date        DATE null,
v_PGI_Date        DATE null,
v_GIDate          DATE null,
v_DlvrCost        DECIMAL(18,4) null,
v_SchedQty        DECIMAL(18,4) null,
v_SchedNo         INTEGER null,
v_ControllingArea VARCHAR(4) null,
v_ProfitCenter    VARCHAR(10) null,
v_BillingType     VARCHAR(4) null,
v_DistChannel     VARCHAR(2) null,
v_DlvrRowNo	   INTEGER null,
v_DlvrRowNoMax	   INTEGER null,
v_DeliveryQtyCUMM  DECIMAL(18,4) null);

Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select row_number() over(order by ''),
	LIKP_VBELN v_DlvrDocNo,
	LIPS_POSNR v_DlvrItemNo,
	LIPS_VGBEL v_SalesDocNo,
	LIPS_VGPOS v_SalesItemNo,
	LIPS_LFIMG v_DeliveryQty,
	ifnull(LIKP_WADAT_IST,LIKP_WADAT) v_AGI_Date,
	LIKP_WADAT v_PGI_Date,
	LIPS_WAVWR v_DlvrCost,
	LIPS_WERKS v_plantcode,
	LIPS_KOKRS v_ControllingArea,
	LIPS_PRCTR v_ProfitCenter,
	LIKP_FKARV v_BillingType,
	LIKP_VTWIV v_DistChannel,
	 ROW_NUMBER() OVER (PARTITION BY LIPS_VGBEL,LIPS_VGPOS ORDER BY LIKP_WADAT_IST,LIKP_WADAT,LIKP_VBELN,LIPS_POSNR) AS v_DlvrRowNo,
	 COUNT(1) OVER (PARTITION BY LIPS_VGBEL,LIPS_VGPOS) v_DlvrRowNoMax
from LIKP_LIPS
where exists (select 1 from fact_salesorder f1
              where f1.dd_SalesDocNo = LIPS_VGBEL and f1.dd_SalesItemNo = LIPS_VGPOS and f1.dd_ItemRelForDelv = 'X')
      and LIPS_LFIMG > 0
order by LIKP_VBELN, v_AGI_Date, LIPS_POSNR;

Create table flag_holder_722 as
         SELECT ifnull(property_value,'true') pDeltaChangesFlag
                   FROM systemproperty
                  WHERE property = 'process.delta.salesorderdelivery';

/*** Remove deleted lines ***/

/* CDPOS Deletes - BI-1041 - 7 Aug 2015 */
DELETE FROM fact_salesorderdelivery
WHERE exists (select 1 from CDPOS_LIKP a where a.CDPOS_OBJECTID = dd_SalesDlvrDocNo AND a.CDPOS_CHNGIND = 'D' AND a.CDPOS_TABNAME = 'LIKP');

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE exists (select 1 from LIKP_LIPS a where dd_SalesDlvrDocNo = LIKP_VBELN)
AND not exists ( SELECT 1 from LIKP_LIPS where dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrItemNo = LIPS_POSNR)
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.* FROM fact_salesorderdelivery,flag_holder_722
WHERE exists (select 1 from LIKP_LIPS a where dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrItemNo = LIPS_POSNR)
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE not exists (select 1 from fact_salesorder f
                      where fact_salesorderdelivery.dd_SalesDocNo = f.dd_SalesDocNo
                            and fact_salesorderdelivery.dd_SalesItemNo = f.dd_SalesItemNo
                            and fact_salesorderdelivery.dd_ScheduleNo = f.dd_ScheduleNo)
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE dd_SalesDlvrDocNo = 'Not Set'
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
where  pDeltaChangesFlag <> 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

/*** All shipment lines not in LIKP_LIPS - 10/16/2013 ***/

DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'cursor_table1_722';

INSERT INTO NUMBER_FOUNTAIN
select 'cursor_table1_722',ifnull(max(v_iid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM cursor_table1_722;

Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select (select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'cursor_table1_722' ) + row_number() over (order by ''),
	dd_SalesDlvrDocNo v_DlvrDocNo,
	dd_SalesDlvrItemNo v_DlvrItemNo,
	dd_SalesDocNo v_SalesDocNo,
	dd_SalesItemNo v_SalesItemNo,
	ct_QtyDelivered v_DeliveryQty,
	case when f.Dim_DateidActualGoodsIssue = 1 then agi.datevalue else pgi.datevalue end v_AGI_Date,
	pgi.datevalue v_PGI_Date,
	amt_Cost v_DlvrCost,
	pl.plantcode v_plantcode,
	ctr.ControllingAreaCode v_ControllingArea,
	pc.ProfitCenterCode v_ProfitCenter,
	btp.Type v_BillingType,
	dcn.DistributionChannelCode v_DistChannel,
	 1 AS v_DlvrRowNo,
	 1 v_DlvrRowNoMax
from fact_salesorderdelivery f
	inner join dim_date agi on f.Dim_DateidActualGoodsIssue = agi.dim_dateid
	inner join dim_date pgi on f.Dim_DateidPlannedGoodsIssue = pgi.dim_dateid
	inner join dim_plant pl on pl.dim_plantid = f.dim_plantid
	inner join Dim_ControllingArea ctr on ctr.Dim_ControllingAreaId = f.Dim_ControllingAreaId
	inner join Dim_ProfitCenter pc on pc.Dim_ProfitCenterid = f.Dim_ProfitCenterid
	inner join dim_date bdt on f.Dim_DateidBillingDate = bdt.dim_dateid
	inner join dim_billingdocumenttype btp on f.dim_billingdocumenttypeid = btp.dim_billingdocumenttypeid
	inner join dim_distributionchannel dcn on dcn.dim_distributionchannelid = f.dim_distributionchannelid
where exists (select 1 from LIKP_LIPS where f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS)
	and not exists (select 1 from LIKP_LIPS where f.dd_SalesDlvrDocNo = LIKP_VBELN and f.dd_SalesDlvrItemNo = LIPS_POSNR);

Drop table if exists cursor_table1_722_tmp;
Create table cursor_table1_722_tmp As
select v_iid,
	v_DlvrDocNo,
	v_DlvrItemNo,
	v_SalesDocNo,
	v_SalesItemNo,
	v_DeliveryQty,
	v_AGI_Date,
	v_PGI_Date,
	v_DlvrCost,
	v_plantcode,
	v_ControllingArea,
	v_ProfitCenter,
	v_BillingType,
	v_DistChannel,
	 ROW_NUMBER() OVER (PARTITION BY v_SalesDocNo,v_SalesItemNo ORDER BY v_AGI_Date,v_PGI_Date,v_DlvrDocNo,v_DlvrItemNo) AS v_DlvrRowNo,
	 COUNT(1) OVER (PARTITION BY v_SalesDocNo,v_SalesItemNo) v_DlvrRowNoMax
from cursor_table1_722;

delete from cursor_table1_722;
Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select v_iid,
	v_DlvrDocNo,
	v_DlvrItemNo,
	v_SalesDocNo,
	v_SalesItemNo,
	v_DeliveryQty,
	v_AGI_Date,
	v_PGI_Date,
	v_DlvrCost,
	v_plantcode,
	v_ControllingArea,
	v_ProfitCenter,
	v_BillingType,
	v_DistChannel,
	v_DlvrRowNo,
	v_DlvrRowNoMax
from cursor_table1_722_tmp;

Drop table if exists cursor_table1_722_tmp;

/******/

/*** Update cummulative delivery qty ***/

Drop table if exists cursor_table1_723;
Create table cursor_table1_723 as
select  a.v_iid,
	a.v_DlvrDocNo,
	a.v_plantcode,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_DlvrCost,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_DeliveryQty) v_DeliveryQtyCUMM
from cursor_table1_722 a inner join cursor_table1_722 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
group by a.v_iid,
	a.v_DlvrDocNo,
	a.v_plantcode,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_DlvrCost,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;

Drop table if exists salesorder_table_101;
Create table salesorder_table_101 as
select f.dd_SalesDocNo v_SalesDocNo,
	f.dd_SalesItemNo v_SalesItemNo,
	f.dd_ScheduleNo v_SchedNo,
	gi.DateValue v_GIDate,
	f.ct_ConfirmedQty v_SchedQty,
	ROW_NUMBER() OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo ORDER BY gi.DateValue,f.dd_ScheduleNo) AS v_DlvrRowNo,
	COUNT(1) OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo) v_DlvrRowNoMax
From fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
where f.dd_ItemRelForDelv = 'X' and f.ct_ConfirmedQty > 0
	and exists (select 1 from cursor_table1_722 where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo);

Drop table if exists cursor_table1_724;
Create table cursor_table1_724 as
select a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_SchedQty) v_SchedQtyCUMM
from salesorder_table_101 a inner join salesorder_table_101 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
group by a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;
Drop table if exists salesorder_table_101;

/*** Get final values with schedule link ***/

Drop table if exists cursor_table1_722;
Create table cursor_table1_722 as
select a.v_iid,
	a.v_DlvrDocNo,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_plantcode,
	a.v_AGI_Date,
	a.v_PGI_Date,
	b.v_GIDate,
	a.v_DlvrCost,
	b.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	a.v_DeliveryQtyCUMM,
	case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM
	    then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax and b.v_SchedQtyCUMM <= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then 0
	    		else
	    		case when b.v_SchedQty > a.v_DeliveryQty
			      	then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty then a.v_DeliveryQty
					  when b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty - (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM)
					  else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	else case when b.v_DlvrRowNo = b.v_DlvrRowNoMax
			      	  		then b.v_SchedQty
			      	  		else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  				when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
			      	  					then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 		else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  				when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  			  	else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  		end
			 end
		 end
	    else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty
		      	then case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then a.v_DeliveryQty + (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM) else a.v_DeliveryQty end
		      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then b.v_SchedQty
		      	  	else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM))
		      	   end
		 end
	end v_SchedQty,
      case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM
            then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax
            			then case when (b.v_SchedQtyCUMM - b.v_SchedQty) > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
            						then a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)
            				 	else a.v_DeliveryQty end
		      else
		    	case when b.v_SchedQty > a.v_DeliveryQty
	                      then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty or b.v_DlvrRowNo = b.v_DlvrRowNoMax
	                                then a.v_DeliveryQty
	                                else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty
	                                          else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
	                           end
	                      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty
	                      			else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  						when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
			      	  							then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 			else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
	                      					when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  		  				else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	   end
	                 end
	              end
            else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty then a.v_DeliveryQty
                      else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM))
                 end
      end v_DeliveryQty,
      ROW_NUMBER() OVER (PARTITION BY a.v_DlvrDocNo,a.v_DlvrItemNo ORDER BY b.v_DlvrRowNo) AS v_DlvrRowSeq
from cursor_table1_723 a
	inner join cursor_table1_724 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DeliveryQtyCUMM > (b.v_SchedQtyCUMM - b.v_SchedQty)
	and ((b.v_DlvrRowNo < b.v_DlvrRowNoMax and b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)) or b.v_DlvrRowNo = b.v_DlvrRowNoMax);

Drop table if exists cursor_table1_723;
Drop table if exists cursor_table1_724;


/*****/

/* loop table */
drop table if exists loop_tbl_722;
create table loop_tbl_722 as
select * from cursor_table1_722 where v_DlvrRowNoMax > 0 and v_DeliveryQty > 0;

/* This intermediate table is used to handle the order by asc that was used in the insert query ( in mysql proc ) */

DROP TABLE IF EXISTS tmp4a_fs_dimpc ;
CREATE TABLE  tmp4a_fs_dimpc
AS
select pc.ProfitCenterCode,pc.ControllingArea,v_AGI_Date,min(pc.ValidTo) as ValidTo
FROM loop_tbl_722, Dim_ProfitCenter pc
WHERE pc.ProfitCenterCode = v_ProfitCenter
AND pc.ControllingArea = v_ControllingArea
AND pc.ValidTo >= v_AGI_Date
AND pc.RowIsCurrent = 1
GROUP BY pc.ProfitCenterCode,pc.ControllingArea,v_AGI_Date;


DROP TABLE IF EXISTS tmp4_fs_dimpc ;
CREATE TABLE  tmp4_fs_dimpc
AS
select a.ProfitCenterCode,a.ControllingArea,v_AGI_Date,a.ValidTo,pc.dim_profitcenterid
FROM tmp4a_fs_dimpc a , Dim_ProfitCenter pc
WHERE pc.ProfitCenterCode = a.ProfitCenterCode
AND pc.ControllingArea = a.ControllingArea
AND pc.RowIsCurrent = 1
AND pc.ValidTo = a.ValidTo;


drop table if exists tmp_fsod_t001t;
create table tmp_fsod_t001t as
	SELECT LIPS_VGBEL dd_SalesDocNo,
              LIPS_VGPOS dd_SalesItemNo,
              f.dd_ScheduleNo,
              LIKP_VBELN dd_SalesDlvrDocNo, 	-- used for Dim_DeliveryHeaderStatusid
              LIPS_POSNR dd_SalesDlvrItemNo,	-- used for Dim_DeliveryItemStatusid
              ifnull(lips_bwart ,'Not Set') dd_MovementType,
              IFNULL(lt.v_DeliveryQty,0) ct_QtyDelivered,
              case when v_DlvrRowSeq = 1 then lt.v_DlvrCost else 0 end amt_Cost_DocCurr,
              case when v_DlvrRowSeq = 1 then lt.v_DlvrCost else 0 end amt_Cost,	/* LK: 8 Sep 2013 */
              /*case when v_DlvrRowSeq = 1 then (lt.v_DlvrCost * (case when f.amt_ExchangeRate < 0 then (1/(-1 * f.amt_ExchangeRate)) else f.amt_ExchangeRate end)) else 0 end amt_Cost,*/
              lips_vbeaf ct_FixedProcessDays,
              lips_vbeav ct_ShipProcessDays,
              lt.v_SchedQty ct_ScheduleQtySalesUnit,
              lt.v_SchedQty ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL,
              convert(bigint, 1) as Dim_DateidPlannedGoodsIssue, 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPlannedGoodsIssue,
              convert(bigint, 1) as Dim_DateidActualGoodsIssue, 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGoodsIssue,
              convert(bigint, 1) as Dim_DateidDeliveryDate, 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_lfdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDeliveryDate,
              convert(bigint, 1) as Dim_DateidLoadingDate, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoadingDate,
              convert(bigint, 1) as Dim_DateidPickingDate, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_kodat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPickingDate,
              convert(bigint, 1) as Dim_DateidDlvrDocCreated, 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDlvrDocCreated,
              convert(bigint, 1) as Dim_DateidMatlAvail, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = lips_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMatlAvail,
              convert(bigint, 1) as Dim_CustomeridSoldTo, 				-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = likp_kunag),f.Dim_CustomerID) Dim_CustomeridSoldTo,
              convert(bigint, 1) as Dim_CustomeridShipTo, 				-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = likp_kunnr),1) Dim_CustomeridShipTo,
              convert(bigint, 1) as Dim_Partid, 						-- ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber = lips_matnr AND dp.Plant = lips_werks),1) Dim_Partid,
              pl.Dim_Plantid,
              convert(bigint, 1) as Dim_StorageLocationid, 				-- ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode = lips_lgort and sl.plant = lips_werks),1) Dim_StorageLocationid,
              convert(bigint, 1) as Dim_ProductHierarchyid,				-- ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy = lips_prodh),1) Dim_ProductHierarchyid,
              convert(bigint, 1) as Dim_DeliveryHeaderStatusid, 		-- ifnull((select Dim_SalesOrderHeaderStatusid from Dim_SalesOrderHeaderStatus sohs where sohs.SalesDocumentNumber = LIKP_VBELN),1) Dim_DeliveryHeaderStatusid,
              convert(bigint, 1) as Dim_DeliveryItemStatusid, 			-- ifnull((select Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber = LIKP_VBELN and sois.SalesItemNumber = LIPS_POSNR),1) Dim_DeliveryItemStatusid,
              f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail as Dim_DateidMatlAvailOriginal,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId,
              convert(bigint, 1) as Dim_ProfitCenterid, 	-- ifnull((select pc.Dim_ProfitCenterid from tmp4_fs_dimpc pc where  pc.ProfitCenterCode = v_ProfitCenter AND pc.ControllingArea = v_ControllingArea AND pc.ValidTo >= v_AGI_Date ),1) Dim_ProfitCenterid,
              convert(bigint, 1) as Dim_ControllingAreaId, 	-- ifnull((select ca.Dim_ControllingAreaid from Dim_ControllingArea ca where  ca.ControllingAreaCode = v_ControllingArea),1) Dim_ControllingAreaId,
              f.Dim_BillingDateId Dim_DateIdBillingDate,
              convert(bigint, 1) as Dim_BillingDocumentTypeid, -- ifnull((SELECT dim_billingdocumenttypeid FROM dim_billingdocumenttype bdt WHERE bdt.Type = v_BillingType AND bdt.RowIsCurrent= 1), 1),
              convert(bigint, 1) as Dim_DistributionChannelId, -- ifnull((SELECT Dim_DistributionChannelId FROM dim_distributionchannel dc WHERE dc.DistributionChannelCode = v_DistChannel AND dc.RowIsCurrent = 1),f.Dim_DistributionChannelId),
              convert(bigint, 1) as Dim_DateidActualGI_Original, -- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGI_Original,
              ifnull(f.Dim_PurchaseOrderTypeId,1) Dim_PurchaseOrderTypeId,
		(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by '') rid,
		f.dim_Currencyid_TRA,
		f.dim_Currencyid_GBL,
		f.dim_currencyid_STAT,
		f.amt_exchangerate_STAT,
	/* changes 26 Sep 2013 */
		ifnull(LIKP_ERZET, '000000') as dd_SDCreateTime,
		ifnull(LIKP_LFUHR, '000000') as dd_DeliveryTime,
		ifnull(LIKP_KOUHR, '000000') as dd_PickingTime,
		ifnull(LIKP_WAUHR, '000000') as dd_GITime,
		ifnull(LIPS_ERZET, '000000') as dd_SDLineCreateTime,
		ifnull(likp_bolnr,'Not Set') as dd_BillofLading,
        likp_wadat, 			-- Dim_DateidPlannedGoodsIssue,
        likp_wadat_ist, 		-- Dim_DateidActualGoodsIssue,
        likp_lfdat, 			-- Dim_DateidDeliveryDate,
        likp_lddat,				-- Dim_DateidLoadingDate,
        likp_kodat,				-- Dim_DateidPickingDate,
        likp_erdat, 			-- Dim_DateidDlvrDocCreated,
        lips_mbdat, 			-- Dim_DateidMatlAvail,
        likp_kunag, 			-- Dim_CustomeridSoldTo,
        likp_kunnr, 			-- Dim_CustomeridShipTo,
        lips_matnr, lips_werks, -- Dim_Partid,
        lips_lgort, 			-- Dim_StorageLocationid,
        lips_prodh, 			-- Dim_ProductHierarchyid,
        v_ProfitCenter, v_ControllingArea, v_AGI_Date, -- Dim_ProfitCenterid, Dim_ControllingAreaId
        v_BillingType,			-- Dim_BillingDocumentTypeid,
        v_DistChannel, ifnull(f.Dim_DistributionChannelId,1) as Dim_DistributionChannelId_f, -- Dim_DistributionChannelId,
		pl.CompanyCode, f.Dim_CustomerID
	FROM	loop_tbl_722 lt
		inner join LIKP_LIPS on LIKP_VBELN = lt.v_DlvrDocNo and LIPS_POSNR = lt.v_DlvrItemNo
		inner join fact_salesorder f on f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS and f.dd_ScheduleNo = lt.v_SchedNo
		inner join Dim_Plant pl on pl.plantcode = LIPS_WERKS;

	/* Dim_DateidPlannedGoodsIssue */
	update tmp_fsod_t001t f
	set f.Dim_DateidPlannedGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_wadat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidPlannedGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidActualGoodsIssue */
	update tmp_fsod_t001t f
	set f.Dim_DateidActualGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidActualGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDeliveryDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidDeliveryDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_lfdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidDeliveryDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidLoadingDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidLoadingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_lddat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidLoadingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidPickingDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidPickingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_kodat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidPickingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDlvrDocCreated */
	update tmp_fsod_t001t f
	set f.Dim_DateidDlvrDocCreated = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_erdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidDlvrDocCreated <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidMatlAvail */
	update tmp_fsod_t001t f
	set f.Dim_DateidMatlAvail = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.lips_mbdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidMatlAvail <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_CustomeridSoldTo */
	update tmp_fsod_t001t f
	set f.Dim_CustomeridSoldTo = ifnull(cust.Dim_CustomerID, f.Dim_CustomerID)
	from tmp_fsod_t001t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunag
	where f.Dim_CustomeridSoldTo <> ifnull(cust.Dim_CustomerID, f.Dim_CustomerID);

	/* Dim_CustomeridShipTo */
	update tmp_fsod_t001t f
	set f.Dim_CustomeridShipTo = ifnull(cust.Dim_CustomerID, 1)
	from tmp_fsod_t001t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunnr
	where f.Dim_CustomeridShipTo <> ifnull(cust.Dim_CustomerID, 1);

	/* Dim_Partid */
	update tmp_fsod_t001t f
	set f.Dim_Partid = ifnull(dp.dim_partid, 1)
	from tmp_fsod_t001t f
			left join dim_part dp on dp.PartNumber = f.lips_matnr AND dp.Plant = f.lips_werks
	where f.Dim_Partid <> ifnull(dp.dim_partid, 1);

	/* Dim_StorageLocationid */
	update tmp_fsod_t001t f
	set f.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
	from tmp_fsod_t001t f
			left join Dim_StorageLocation sl on sl.LocationCode = f.lips_lgort and sl.plant = f.lips_werks
	where f.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

	/* Dim_ProductHierarchyid */
	update tmp_fsod_t001t f
	set f.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
	from tmp_fsod_t001t f
			left join Dim_ProductHierarchy ph on ph.ProductHierarchy = lips_prodh
	where f.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

	/* Dim_DeliveryHeaderStatusid */
	update tmp_fsod_t001t f
	set f.Dim_DeliveryHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
	from tmp_fsod_t001t f
			left join Dim_SalesOrderHeaderStatus sohs on sohs.SalesDocumentNumber = f.dd_SalesDlvrDocNo
	where f.Dim_DeliveryHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

	/* Dim_DeliveryItemStatusid */
	update tmp_fsod_t001t f
	set f.Dim_DeliveryItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
	from tmp_fsod_t001t f
			left join (select a.SalesDocumentNumber,a.SalesItemNumber,max(a.Dim_SalesOrderItemStatusid) Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus a group by a.SalesDocumentNumber,a.SalesItemNumber) sois
				on sois.SalesDocumentNumber = f.dd_SalesDlvrDocNo and sois.SalesItemNumber = f.dd_SalesDlvrItemNo
	where f.Dim_DeliveryItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

	/* Dim_ProfitCenterid */
	merge into tmp_fsod_t001t fa
	using (select t0.rid, ifnull(t1.Dim_ProfitCenterid, 1) Dim_ProfitCenterid
		   from tmp_fsod_t001t t0
					left join (select distinct f.rid, pc.Dim_ProfitCenterid
							   from tmp_fsod_t001t f
										inner join tmp4_fs_dimpc pc on    pc.ProfitCenterCode = f.v_ProfitCenter
										   						      AND pc.ControllingArea = f.v_ControllingArea
							   where pc.ValidTo >= f.v_AGI_Date) t1 on t0.rid = t1.rid
		  ) src
	on fa.rid = src.rid
	when matched then update set fa.Dim_ProfitCenterid = ifnull(src.Dim_ProfitCenterid, 1)
	where fa.Dim_ProfitCenterid <> ifnull(src.Dim_ProfitCenterid, 1);

	/* Dim_ControllingAreaId */
	update tmp_fsod_t001t f
	set f.Dim_ControllingAreaId = ifnull(ca.Dim_ControllingAreaid, 1)
	from tmp_fsod_t001t f
			left join Dim_ControllingArea ca on ca.ControllingAreaCode = f.v_ControllingArea
	where f.Dim_ControllingAreaId <> ifnull(ca.Dim_ControllingAreaid, 1);

	/* Dim_BillingDocumentTypeid */
	update tmp_fsod_t001t f
	set f.Dim_BillingDocumentTypeid = ifnull(bdt.dim_billingdocumenttypeid, 1)
	from tmp_fsod_t001t f
			left join dim_billingdocumenttype bdt on bdt.Type = v_BillingType AND bdt.RowIsCurrent= 1
	where f.Dim_BillingDocumentTypeid <> ifnull(bdt.dim_billingdocumenttypeid, 1);

	/* Dim_DistributionChannelId */
	update tmp_fsod_t001t f
	set f.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f)
	from tmp_fsod_t001t f
			left join dim_distributionchannel dc on dc.DistributionChannelCode = f.v_DistChannel AND dc.RowIsCurrent = 1
	where f.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f);

	/* Dim_DateidActualGI_Original */
	update tmp_fsod_t001t f
	set f.Dim_DateidActualGI_Original = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join Dim_Date dd on dd.DateValue = likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidActualGI_Original <> ifnull(dd.Dim_Dateid, 1);

   INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateidBillingDate,
              Dim_BillingDocumentTypeid,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,
	      Dim_PurchaseOrderTypeId,
	      fact_salesorderdeliveryid,
	      dim_Currencyid_TRA,
	      dim_Currencyid_GBL,
	      dim_currencyid_STAT,
	      amt_exchangerate_STAT,
		  dd_SDCreateTime,
		  dd_DeliveryTime,
		  dd_PickingTime,
		  dd_GITime,
		  dd_SDLineCreateTime,
		  dd_BillofLading
		)
select dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateidBillingDate,
              Dim_BillingDocumentTypeid,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,
	      Dim_PurchaseOrderTypeId,
	      rid,
	      dim_Currencyid_TRA,
	      dim_Currencyid_GBL,
	      dim_currencyid_STAT,
	      amt_exchangerate_STAT,
		  dd_SDCreateTime,
		  dd_DeliveryTime,
		  dd_PickingTime,
		  dd_GITime,
		  dd_SDLineCreateTime,
		  dd_BillofLading
from tmp_fsod_t001t;

drop table if exists tmp_fsod_t001t;


	update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_salesorderdeliveryid),0) from fact_salesorderdelivery)
	where table_name = 'fact_salesorderdelivery';

    DROP TABLE IF EXISTS tmp_fact_sodf_dimpc_1;
	CREATE TABLE tmp_fact_sodf_dimpc_1
	AS
	SELECT pc.ProfitCenterCode,pc.ControllingArea,ifnull(LIKP_WADAT_IST,LIKP_WADAT) LIKP_WADAT_IST,min(pc.ValidTo) as min_ValidTo
	FROM LIKP_LIPS , Dim_ProfitCenter pc
	WHERE pc.ProfitCenterCode = LIPS_PRCTR
 	AND pc.ControllingArea = LIPS_KOKRS
       AND pc.ValidTo >= ifnull(LIKP_WADAT_IST,LIKP_WADAT)
	AND pc.RowIsCurrent = 1
	GROUP BY pc.ProfitCenterCode,pc.ControllingArea,ifnull(LIKP_WADAT_IST,LIKP_WADAT);

	DROP TABLE IF EXISTS tmp_fact_sodf_dimpc_2;
	CREATE TABLE tmp_fact_sodf_dimpc_2
	AS
	SELECT t.*,pc.Dim_ProfitCenterid
	from Dim_ProfitCenter pc, tmp_fact_sodf_dimpc_1 t
	WHERE pc.ProfitCenterCode = t.ProfitCenterCode
	AND pc.ControllingArea = t.ControllingArea
	AND pc.ValidTo = t.min_ValidTo;

	DROP TABLE IF EXISTS tmp_fact_sodf_minsched;
	CREATE TABLE tmp_fact_sodf_minsched
	AS
	select f1.dd_SalesDocNo,f1.dd_SalesItemNo,min(f1.dd_ScheduleNo) min_dd_ScheduleNo
	from fact_salesorder f1
	GROUP BY f1.dd_SalesDocNo,f1.dd_SalesItemNo;


	DROP TABLE IF EXISTS tmp_fact_sodf_LIKP_LIPS;
	CREATE TABLE tmp_fact_sodf_LIKP_LIPS
	AS
	SELECT l.*,f.min_dd_ScheduleNo
	from LIKP_LIPS l inner join tmp_fact_sodf_minsched f on f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS
	where not exists (select 1 from fact_salesorder f1
                        where f1.dd_SalesDocNo = LIPS_VGBEL and f1.dd_SalesItemNo = LIPS_VGPOS and f1.dd_ItemRelForDelv = 'X')
		or not exists (select 1 from loop_tbl_722 lt where LIKP_VBELN = lt.v_DlvrDocNo and LIPS_POSNR = lt.v_DlvrItemNo);


drop table if exists tmp_fsod_t002t;
create table tmp_fsod_t002t as
      SELECT ll.LIPS_VGBEL dd_SalesDocNo,
              ll.LIPS_VGPOS dd_SalesItemNo,
              0 dd_ScheduleNo,
              ll.LIKP_VBELN dd_SalesDlvrDocNo,
              ll.LIPS_POSNR dd_SalesDlvrItemNo,
              ifnull(ll.lips_bwart ,'Not Set') dd_MovementType,
              f.dd_ReferenceDocumentNo as dd_ReferenceDocNo,
              ll.LIPS_LFIMG ct_QtyDelivered,
              ll.LIPS_WAVWR amt_Cost_DocCurr,
              ll.LIPS_WAVWR amt_Cost,
              /*(ll.LIPS_WAVWR * (case when f.amt_ExchangeRate < 0 then (1/(-1 * f.amt_ExchangeRate)) else f.amt_ExchangeRate end)) amt_Cost,*/
              ifnull(f.amt_SubTotal3,0.0000) as amt_SalesSubTotal3,
              ifnull(f.amt_SubTotal4,0.0000) as amt_SalesSubTotal4,
              ll.lips_vbeaf ct_FixedProcessDays,
              ll.lips_vbeav ct_ShipProcessDays,
              ll.LIPS_LFIMG ct_ScheduleQtySalesUnit,
              ll.LIPS_LFIMG ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL
              ,convert(bigint, 1) as  Dim_DateidPlannedGoodsIssue 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPlannedGoodsIssue,
              ,convert(bigint, 1) as  Dim_DateidActualGoodsIssue 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGoodsIssue,
              ,convert(bigint, 1) as  Dim_DateidDeliveryDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_lfdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDeliveryDate,
              ,convert(bigint, 1) as  Dim_DateidLoadingDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoadingDate,
              ,convert(bigint, 1) as  Dim_DateidPickingDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_kodat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPickingDate,
              ,convert(bigint, 1) as  Dim_DateidDlvrDocCreated 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDlvrDocCreated,
              ,convert(bigint, 1) as  Dim_DateidMatlAvail 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.lips_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMatlAvail,
              ,convert(bigint, 1) as  Dim_CustomeridSoldTo 			-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = ll.likp_kunag),f.Dim_CustomerID) Dim_CustomeridSoldTo,
              ,convert(bigint, 1) as  Dim_CustomeridShipTo 			-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = ll.likp_kunnr),1) Dim_CustomeridShipTo,
              ,convert(bigint, 1) as  Dim_Partid 					-- ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber = ll.lips_matnr AND dp.Plant = ll.lips_werks),1) Dim_Partid,
              ,pl.Dim_Plantid
              ,convert(bigint, 1) as  Dim_StorageLocationid 		-- ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode = ll.lips_lgort and sl.plant = ll.lips_werks),1) Dim_StorageLocationid,
              ,convert(bigint, 1) as  Dim_ProductHierarchyid 		-- ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy = ll.lips_prodh),1) Dim_ProductHierarchyid,
              ,convert(bigint, 1) as  Dim_DeliveryHeaderStatusid 	-- ifnull((select Dim_SalesOrderHeaderStatusid from Dim_SalesOrderHeaderStatus sohs where sohs.SalesDocumentNumber = ll.LIKP_VBELN),1) Dim_DeliveryHeaderStatusid,
              ,convert(bigint, 1) as  Dim_DeliveryItemStatusid 		-- ifnull((select Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber = ll.LIKP_VBELN and sois.SalesItemNumber = ll.LIPS_POSNR),1) Dim_DeliveryItemStatusid,
              ,f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail as Dim_DateidMatlAvailOriginal,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId
			  ,convert(bigint, 1) as  Dim_ProfitCenterid 			-- ifnull((select pc.Dim_ProfitCenterid from tmp_fact_sodf_dimpc_2 pc where  pc.ProfitCenterCode = LIPS_PRCTR AND pc.ControllingArea = LIPS_KOKRS AND pc.LIKP_WADAT_IST = ifnull(ll.LIKP_WADAT_IST,ll.LIKP_WADAT)),1) Dim_ProfitCenterid,
              ,convert(bigint, 1) as  Dim_ControllingAreaId 		-- ifnull((select ca.Dim_ControllingAreaid from Dim_ControllingArea ca where  ca.ControllingAreaCode = ll.LIPS_KOKRS),1) Dim_ControllingAreaId,
              ,f.Dim_BillingDateId Dim_DateIdBillingDate
              ,convert(bigint, 1) as  Dim_BillingDocumentTypeId 	-- ifnull((SELECT dim_billingdocumenttypeid FROM dim_billingdocumenttype bdt WHERE bdt.Type = ll.LIKP_FKARV AND bdt.RowIsCurrent= 1), 1) Dim_BillingDocumentTypeId,
              ,convert(bigint, 1) as  Dim_DistributionChannelId 	-- ifnull((SELECT Dim_DistributionChannelId FROM dim_distributionchannel dc WHERE dc.DistributionChannelCode = ll.LIKP_VTWIV AND dc.RowIsCurrent = 1),f.Dim_DistributionChannelId) Dim_DistributionChannelId,
              ,convert(bigint, 1) as  Dim_DateidActualGI_Original 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGI_Original,
			  ,ifnull(Dim_PurchaseOrderTypeId,1) Dim_PurchaseOrderTypeId,
			  (select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by '') fact_salesorderdeliveryid,
			  f.dim_Currencyid_TRA,
			  f.dim_Currencyid_GBL,
			  f.dim_currencyid_STAT,
			  f.amt_exchangerate_STAT,
			  ifnull(LIKP_ERZET, '000000') as dd_SDCreateTime,
			  ifnull(LIKP_LFUHR, '000000') as dd_DeliveryTime,
			  ifnull(LIKP_KOUHR, '000000') as dd_PickingTime,
			  ifnull(LIKP_WAUHR, '000000') as dd_GITime,
			  ifnull(LIPS_ERZET, '000000') as dd_SDLineCreateTime,
			  ifnull(likp_bolnr,'Not Set') as dd_BillofLading,
		likp_wadat, 			-- Dim_DateidPlannedGoodsIssue,
        likp_wadat_ist, 		-- Dim_DateidActualGoodsIssue,
        likp_lfdat, 			-- Dim_DateidDeliveryDate,
        likp_lddat,				-- Dim_DateidLoadingDate,
        likp_kodat,				-- Dim_DateidPickingDate,
        likp_erdat, 			-- Dim_DateidDlvrDocCreated,
        lips_mbdat, 			-- Dim_DateidMatlAvail,
        likp_kunag, 			-- Dim_CustomeridSoldTo,
        likp_kunnr, 			-- Dim_CustomeridShipTo,
        lips_matnr, lips_werks, -- Dim_Partid,
        lips_lgort, 			-- Dim_StorageLocationid,
        lips_prodh, 			-- Dim_ProductHierarchyid,
        LIPS_PRCTR, LIPS_KOKRS, -- Dim_ProfitCenterid, Dim_ControllingAreaId
        LIKP_FKARV,			-- Dim_BillingDocumentTypeid,
        LIKP_VTWIV, f.Dim_DistributionChannelId as Dim_DistributionChannelId_f, -- Dim_DistributionChannelId,
		pl.CompanyCode, f.Dim_CustomerID
      FROM tmp_fact_sodf_LIKP_LIPS ll
          inner join fact_salesorder f on f.dd_SalesDocNo = ll.LIPS_VGBEL and f.dd_SalesItemNo = ll.LIPS_VGPOS
                                          and f.dd_ScheduleNo = ll.min_dd_ScheduleNo
          inner join Dim_Plant pl on pl.plantcode = ll.LIPS_WERKS;

	/* Dim_DateidPlannedGoodsIssue */
	update tmp_fsod_t002t f
	set f.Dim_DateidPlannedGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_wadat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidPlannedGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidActualGoodsIssue */
	update tmp_fsod_t002t f
	set f.Dim_DateidActualGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidActualGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDeliveryDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidDeliveryDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_lfdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidDeliveryDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidLoadingDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidLoadingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_lddat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidLoadingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidPickingDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidPickingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_kodat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidPickingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDlvrDocCreated */
	update tmp_fsod_t002t f
	set f.Dim_DateidDlvrDocCreated = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_erdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidDlvrDocCreated <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidMatlAvail */
	update tmp_fsod_t002t f
	set f.Dim_DateidMatlAvail = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.lips_mbdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidMatlAvail <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_Partid */
	update tmp_fsod_t002t f
	set f.Dim_Partid = ifnull(dp.dim_partid, 1)
	from tmp_fsod_t002t f
			left join dim_part dp on dp.PartNumber = f.lips_matnr AND dp.Plant = f.lips_werks
	where f.Dim_Partid <> ifnull(dp.dim_partid, 1);

	/* Dim_StorageLocationid */
	update tmp_fsod_t002t f
	set f.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
	from tmp_fsod_t002t f
			left join Dim_StorageLocation sl on sl.LocationCode = f.lips_lgort and sl.plant = f.lips_werks
	where f.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

	/* Dim_ProductHierarchyid */
	update tmp_fsod_t002t f
	set f.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
	from tmp_fsod_t002t f
			left join Dim_ProductHierarchy ph on ph.ProductHierarchy = lips_prodh
	where f.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

	/* Dim_DeliveryHeaderStatusid */
	update tmp_fsod_t002t f
	set f.Dim_DeliveryHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
	from tmp_fsod_t002t f
			left join Dim_SalesOrderHeaderStatus sohs on sohs.SalesDocumentNumber = f.dd_SalesDlvrDocNo
	where f.Dim_DeliveryHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

	/* Dim_DeliveryItemStatusid */
	update tmp_fsod_t002t f
	set f.Dim_DeliveryItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
	from tmp_fsod_t002t f
			left join (select a.SalesDocumentNumber,a.SalesItemNumber,max(a.Dim_SalesOrderItemStatusid) Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus a group by a.SalesDocumentNumber,a.SalesItemNumber) sois on sois.SalesDocumentNumber = f.dd_SalesDlvrDocNo and sois.SalesItemNumber = f.dd_SalesDlvrItemNo
	where f.Dim_DeliveryItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

	/* Dim_ProfitCenterid */
	update tmp_fsod_t002t f
	set f.Dim_ProfitCenterid = ifnull(pc.Dim_ProfitCenterid, 1)
	from tmp_fsod_t002t f
			left join tmp_fact_sodf_dimpc_2 pc on   pc.ProfitCenterCode = f.LIPS_PRCTR
												AND pc.ControllingArea  = f.LIPS_KOKRS
												AND pc.LIKP_WADAT_IST   = ifnull(f.LIKP_WADAT_IST,f.LIKP_WADAT)
	where f.Dim_ProfitCenterid <> ifnull(pc.Dim_ProfitCenterid, 1);

	/* Dim_ControllingAreaId */
	update tmp_fsod_t002t f
	set f.Dim_ControllingAreaId = ifnull(ca.Dim_ControllingAreaid, 1)
	from tmp_fsod_t002t f
			left join Dim_ControllingArea ca on ca.ControllingAreaCode = f.LIPS_KOKRS
	where f.Dim_ControllingAreaId <> ifnull(ca.Dim_ControllingAreaid, 1);

	/* Dim_BillingDocumentTypeid */
	update tmp_fsod_t002t f
	set f.Dim_BillingDocumentTypeid = ifnull(bdt.dim_billingdocumenttypeid, 1)
	from tmp_fsod_t002t f
			left join dim_billingdocumenttype bdt on bdt.Type = LIKP_FKARV AND bdt.RowIsCurrent= 1
	where f.Dim_BillingDocumentTypeid <> ifnull(bdt.dim_billingdocumenttypeid, 1);

	/* Dim_DistributionChannelId */
	update tmp_fsod_t002t f
	set f.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f)
	from tmp_fsod_t002t f
			left join dim_distributionchannel dc on dc.DistributionChannelCode = f.LIKP_VTWIV AND dc.RowIsCurrent = 1
	where f.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f);

	/* Dim_DateidActualGI_Original */
	update tmp_fsod_t002t f
	set f.Dim_DateidActualGI_Original = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join Dim_Date dd on dd.DateValue = likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidActualGI_Original <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_CustomeridSoldTo */
	update tmp_fsod_t002t f
	set f.Dim_CustomeridSoldTo = ifnull(cust.Dim_CustomerID, f.Dim_CustomerID)
	from tmp_fsod_t002t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunag
	where f.Dim_CustomeridSoldTo <> ifnull(cust.Dim_CustomerID, f.Dim_CustomerID);

	/* Dim_CustomeridShipTo */
	update tmp_fsod_t002t f
	set f.Dim_CustomeridShipTo = ifnull(cust.Dim_CustomerID, 1)
	from tmp_fsod_t002t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunnr
	where f.Dim_CustomeridShipTo <> ifnull(cust.Dim_CustomerID, 1);

      INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,Dim_PurchaseOrderTypeId,fact_salesorderdeliveryid,
              dim_Currencyid_TRA,
              dim_Currencyid_GBL,
              dim_currencyid_STAT,
              amt_exchangerate_STAT,
			  dd_SDCreateTime,
			  dd_DeliveryTime,
			  dd_PickingTime,
			  dd_GITime,
			  dd_SDLineCreateTime,
			  dd_BillofLading)
select dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,Dim_PurchaseOrderTypeId,fact_salesorderdeliveryid,
              dim_Currencyid_TRA,
              dim_Currencyid_GBL,
              dim_currencyid_STAT,
              amt_exchangerate_STAT,
			  dd_SDCreateTime,
			  dd_DeliveryTime,
			  dd_PickingTime,
			  dd_GITime,
			  dd_SDLineCreateTime,
			  dd_BillofLading
from tmp_fsod_t002t;

drop table if exists tmp_fsod_t002t;

drop table if exists tmp_updt_VBFA;
create table tmp_updt_VBFA
as
select fact_salesorderdeliveryid, max(case when (sod.ct_QtyDelivered - v.vbfa_rfmng) < 0 then 0 else (sod.ct_QtyDelivered - v.vbfa_rfmng) end) ct_QtyDelivered
FROM VBFA v, fact_salesorderdelivery sod
  WHERE sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
      and v.vbfa_bwart = '602' and sod.ct_QtyDelivered > 0
      and not exists (select 1 from VBFA v1
                      where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                            and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat)
GROUP BY fact_salesorderdeliveryid;

UPDATE fact_salesorderdelivery sod
SET sod.ct_QtyDelivered = ifnull(v.ct_QtyDelivered,0)
FROM tmp_updt_VBFA v, fact_salesorderdelivery sod
WHERE sod.fact_salesorderdeliveryid = v.fact_salesorderdeliveryid
  AND ifnull(sod.ct_QtyDelivered,-1) = ifnull(v.ct_QtyDelivered,0);

drop table if exists tmp_updt_VBFA;

/* start CALL bi_populate_so_shipment() */

drop table if exists update_so_shipment_001;
create table update_so_shipment_001 as
select f.dd_SalesDocNo v_dd_SalesDocNo, f.dd_SalesItemNo v_dd_SalesItemNo, f.dd_ScheduleNo v_dd_ScheduleNo,
	ifnull(sum(f.ct_QtyDelivered), 0) v_ct_DeliveredQty,
	ifnull(max(f.Dim_DateidDeliveryDate), 1) v_Dim_DateidShipmentDelivery,
	ifnull(max(f.Dim_DateidActualGoodsIssue), 1) v_Dim_DateidActualGI,
	min(f.Dim_CustomeridShipto) v_Dim_CustomeridShipTo,
	ifnull(min(f.Dim_DateidDlvrDocCreated), 1) v_Dim_DateidDlvrDocCreated
from fact_salesorderdelivery f
	inner join dim_salesorderitemstatus sois on f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.GoodsMovementStatus <> 'Not yet processed'
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo;

/* LK: 12 Aug 2014 - Dim_CustomeridShipTo should not depend on sois.GoodsMovementStatus */
drop table if exists update_so_shipment_001_Dim_CustomeridShipTo;
CREATE TABLE update_so_shipment_001_Dim_CustomeridShipTo
AS
select f.dd_SalesDocNo v_dd_SalesDocNo, f.dd_SalesItemNo v_dd_SalesItemNo, f.dd_ScheduleNo v_dd_ScheduleNo,
min(f.Dim_CustomeridShipto) v_Dim_CustomeridShipTo
from fact_salesorderdelivery f
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo;

UPDATE fact_salesorder so
SET ct_DeliveredQty = 0
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE  ct_DeliveredQty <> 0;

/*UPDATE fact_salesorder so
   SET ct_DeliveredQty = v_ct_DeliveredQty
	,dw_update_date = current_timestamp
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo and ct_DeliveredQty <> v_ct_DeliveredQty

UPDATE fact_salesorder so
   SET Dim_DateidShipmentDelivery = v_Dim_DateidShipmentDelivery
	,dw_update_date = current_timestamp
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo

UPDATE fact_salesorder so
   SET Dim_DateidActualGI = v_Dim_DateidActualGI
	,dw_update_date = current_timestamp
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo
 */


/* Updates for Salesorder */

MERGE INTO fact_salesorder so
USING update_so_shipment_001 u ON so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET ct_DeliveredQty = v_ct_DeliveredQty,Dim_DateidShipmentDelivery = v_Dim_DateidShipmentDelivery,Dim_DateidActualGI = v_Dim_DateidActualGI,dw_update_date = current_timestamp;


/* Deviation GI Date vs Conf Del Date */

UPDATE fact_salesorder fso
SET ct_DevGIdtvsConfDeldt = ifnull(Case when cddemd.DateValue = '0001-01-01' then 0
                            else
							    case when agidt.DateValue = '0001-01-01' then (current_date - cddemd.DateValue)
							         else(agidt.DateValue - cddemd.DateValue)
								 end
						     end, 0)
FROM fact_salesorder fso, dim_date agidt, dim_date cddemd
WHERE fso.dim_actualgidatepm = agidt.dim_dateid
AND   fso.dim_dateidconfirmeddelivery_emd = cddemd.dim_dateid;



/*Deviation GI Date vs Req Date*/

UPDATE fact_salesorder fso
SET ct_DevGIdtvsReqdt = ifnull(Case when  rdemd.DateValue = '0001-01-01' then 0
                             else
								  case when agidt.DateValue = '0001-01-01' then (current_date - rdemd.DateValue)
                                       else(agidt.DateValue - rdemd.DateValue)
									   end
					    end, 0)
FROM fact_salesorder fso, dim_date agidt, dim_date rdemd
WHERE fso.dim_actualgidatepm = agidt.dim_dateid
AND   fso.dim_dateidrequested_emd = rdemd.dim_dateid;



/* Deviation GI Date vs Acc GI Conf Date	 */



UPDATE fact_salesorder fso
SET ct_DevGIdtvsAccGIConfdt = ifnull(Case when  acgidmto.DateValue = '0001-01-01' then 999999
								   else
								      case when agidt.DateValue = '0001-01-01' then (current_date - acgidmto.DateValue)
                                           else(agidt.DateValue - acgidmto.DateValue)
							          end
							  end, 0)
FROM fact_salesorder fso, dim_date agidt, dim_date_factory_calendar acgidmto
WHERE fso.dim_actualgidatepm = agidt.dim_dateid
AND   fso.dim_accordinggidatemto_emd = acgidmto.dim_dateid;


 /*Delivery Service according to Requested Date */

UPDATE fact_salesorder fso
SET dd_devGIdtvsAccGidt = ifnull(case when dd_DevGIdtvsConfDeldt in (' ','Not Set') then ' '
                               else
							       case when cast (dd_DevGIdtvsConfDeldt as int) <= 1 then '1'
								        else '0'
										end
								end, ' ');


 /*Delivery Service against confirmed del date */


UPDATE fact_salesorder fso
SET ct_DelSeragconfdeldt =  ifnull(case when rdemd.DateValue >  current_date then 999999
                               else case when dim_actualgidatepm = 1 then 0
                                    else case when ct_DEVGIDTVSREQDT  > 1 then 0
		                                 else 1
                                             end
                                    end
								end, 0)

FROM fact_salesorder fso,dim_date rdemd
WHERE  fso.dim_dateidrequested_emd = rdemd.dim_dateid;



 /* Delivery Service against requested del date */



UPDATE fact_salesorder fso
SET ct_DelSeragreqdeldt =  ifnull(case when cddemd.DateValue >  current_date then 999999
                               else case when dim_actualgidatepm = 1 then 0
                                     else case when ct_DEVGIDTVSCONFDELDT  > 1 then 0
		                            else 1
                                    end
                           end
						   end, 0)
FROM fact_salesorder fso,  dim_date cddemd
WHERE  fso.dim_dateidconfirmeddelivery_emd = cddemd.dim_dateid;




 /* Delivery Service against according GI-date (confirmed)*/

UPDATE fact_salesorder fso
SET    ct_delseragaccgidt = ifnull(CASE
                              WHEN acgidmto.datevalue = '0001-01-01' THEN 999999
                              ELSE
                                CASE
                                  WHEN acgidmto.datevalue > CURRENT_DATE
                                       AND dim_actualgidatepm = 1 THEN 999999
                                  ELSE
                                    CASE
                                      WHEN ( ct_devgidtvsaccgiconfdt >= 1
                                             AND ct_devgidtvsaccgiconfdt <
                                                 999999 )
                                    THEN 0
                                      ELSE 1
                                    END
                                END
                            END, 0)
FROM   fact_salesorder fso,
       dim_date_factory_calendar acgidmto
WHERE  fso.dim_accordinggidatemto_emd = acgidmto.dim_dateid;

UPDATE fact_salesorder
SET dd_ontimeconfirmed = CASE WHEN CT_DELSERAGACCGIDT = 0 THEN 'No' WHEN CT_DELSERAGACCGIDT=1 THEN 'Yes' ELSE 'Not Set' END
FROM fact_salesorder
WHERE dd_ontimeconfirmed <> CASE WHEN CT_DELSERAGACCGIDT = 0 THEN 'No' WHEN CT_DELSERAGACCGIDT=1 THEN 'Yes' ELSE 'Not Set' END ;


/*Merck LS OTIF -- get Actual GI Date on the first scheduleline */
drop table if exists tmp_actualdategi_merckls;
create table tmp_actualdategi_merckls
as select distinct f.dd_salesdocno,
f.DD_SALESITEMNO,
first_value(f.DIM_DATEIDACTUALGI) over (partition by f.dd_salesdocno, f.DD_SALESITEMNO order by dd.datevalue desc) as DD_DATEIDACTUALGI
from fact_salesorder f
	inner join dim_date dd on f.DIM_DATEIDACTUALGI = dd.dim_dateid;

update fact_salesorder f
set f.dim_dateidactualgimerckls = ifnull(t.DD_DATEIDACTUALGI, 1)
from fact_salesorder f, tmp_actualdategi_merckls t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_dateidactualgimerckls <> ifnull(t.DD_DATEIDACTUALGI, 1);
/* and f.DIM_DATEIDACTUALGI = 1 */

/* update fact_salesorder f
set f.dim_dateidactualgimerckls = f.DIM_DATEIDACTUALGI
where f.dim_dateidactualgimerckls = 1 */
/*END Merck LS OTIF -- get Actual GI Date on the first scheduleline */

UPDATE fact_salesorder so
   SET Dim_CustomeridShipTo = ifnull(v_Dim_CustomeridShipTo, Dim_CustomeridShipTo)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

/* LK: 12 Aug 2014 - Dim_CustomeridShipTo should not depend on sois.GoodsMovementStatus */
UPDATE fact_salesorder so
   SET Dim_CustomeridShipTo = ifnull(v_Dim_CustomeridShipTo, 1)
   ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001_Dim_CustomeridShipTo u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo
 AND v_Dim_CustomeridShipTo is NOT NULL and Dim_CustomeridShipTo = 1 AND v_Dim_CustomeridShipTo <> 1;

drop table if exists update_so_shipment_001_Dim_CustomeridShipTo;

UPDATE fact_salesorder so
   SET Dim_DateidDlvrDocCreated = ifnull(v_Dim_DateidDlvrDocCreated, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

/* APP-6863 - remove from logic this exception - Oana 19 July 2017 */
/*
  UPDATE fact_salesorder so
  SET ct_DeliveredQty = so.ct_ConfirmedQty
  fROM dim_salesorderitemstatus s, fact_salesorder so
  WHERE so.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      AND so.dd_ItemRelForDelv = 'X'
      AND s.OverallDeliveryStatus = 'Completely processed'
      AND so.ct_DeliveredQty < so.ct_ConfirmedQty
      AND not exists (select 1 from fact_salesorderdelivery sod inner join VBFA v on sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
                      where v.vbfa_bwart = '602' and sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo
                      and not exists (select 1 from VBFA v1 where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                                      and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat))
*/

/* end update so_shipment */

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_salesorderdeliveryid),0) from fact_salesorderdelivery)
where table_name = 'fact_salesorderdelivery';

  INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,fact_salesorderdeliveryid,
                dim_Currencyid_TRA,
                dim_Currencyid_GBL,
                dim_currencyid_STAT,
                amt_exchangerate_STAT	)

      SELECT f.dd_SalesDocNo,
              f.dd_SalesItemNo,
              f.dd_ScheduleNo,
              'Not Set' dd_SalesDlvrDocNo,
              0 dd_SalesDlvrItemNo,
              'Not Set' dd_MovementType,
              f.dd_ReferenceDocumentNo,
              0 ct_QtyDelivered,
              0 amt_Cost_DocCurr,
              0 amt_Cost,
              ifnull(f.amt_SubTotal3,0.0000) amt_SalesSubTotal3,
              ifnull(f.amt_SubTotal4,0.0000) amt_SalesSubTotal4,
              0 ct_FixedProcessDays,
              0 ct_ShipProcessDays,
              f.ct_ScheduleQtySalesUnit,
              f.ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL,
              f.Dim_DateidGoodsIssue,
              1 Dim_DateidActualGoodsIssue,
              f.Dim_DateidSchedDelivery,
              f.Dim_DateidLoading,
              1 Dim_DateidPickingDate,
              1 Dim_DateidDlvrDocCreated,
              f.Dim_DateidMtrlAvail,
              f.Dim_CustomerID Dim_CustomeridSoldTo,
              f.Dim_CustomerID Dim_CustomeridShipTo,
              f.Dim_Partid,
              f.Dim_Plantid,
              f.Dim_StorageLocationid,
              f.Dim_ProductHierarchyid,
              1 Dim_DeliveryHeaderStatusid,
              1 Dim_DeliveryItemStatusid,
              f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId,
              1 Dim_ProfitCenterid,
              1 Dim_ControllingAreaId,
              f.Dim_BillingDateId Dim_DateIdBillingDate,
              1 Dim_BillingDocumentTypeId,
              f.Dim_DistributionChannelId Dim_DistributionChannelId,
	(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by ''),
                f.dim_Currencyid_TRA,
                f.dim_Currencyid_GBL,
                f.dim_currencyid_STAT,
                f.amt_exchangerate_STAT

      FROM fact_salesorder f inner join dim_salesorderitemstatus s
                                    ON f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      where f.dd_ItemRelForDelv = 'X' and s.OverallDeliveryStatus <> 'Completely processed'
            and (ct_ConfirmedQty - ct_DeliveredQty) > 0
            and not exists (select 1 from fact_salesorderdelivery f1
                            where f1.dd_SalesDocNo = f.dd_SalesDocNo and f1.dd_SalesItemNo = f.dd_SalesItemNo and f1.dd_ScheduleNo = f.dd_ScheduleNo);

UPDATE fact_salesorderdelivery sod
SET sod.Dim_deliveryTypeId = ifnull(dt.Dim_deliveryTypeId, 1)
  FROM LIKP_LIPS, Dim_Deliverytype dt, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
  AND LIKP_LFART IS NOT NULL
  AND dt.DeliveryType = LIKP_LFART
  AND dt.RowIsCurrent = 1;

UPDATE facT_salesorderdelivery
SET Dim_deliveryTypeId = 1
WHERE Dim_deliveryTypeId IS NULL;

/* 10 Aug 2015 CristianT Start: Fix for ambiguous error */
/* Old update is here
UPDATE       fact_salesorderdelivery sod
FROM 		likp_lips_vbuk v, dim_overallstatusforcreditcheck oscc
SET sod.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckID
WHERE		 sod.dd_SalesDlvrDocNo = v.VBUK_VBELN
AND		 oscc.overallstatusforcreditcheck = ifnull(v.VBUK_CMGST, 'Not Set')
AND 		oscc.RowIsCurrent = 1 */
DROP TABLE IF EXISTS tmp_overallstatus;
CREATE TABLE tmp_overallstatus AS
select distinct sod.fact_salesorderdeliveryid, ifnull(v.VBUK_CMGST, 'Not Set') as VBUK_CMGST
FROM fact_salesorderdelivery sod
		inner join likp_lips_vbuk v on sod.dd_SalesDlvrDocNo = v.VBUK_VBELN;

merge into fact_salesorderdelivery sod
using ( select max(oscc.dim_overallstatusforcreditcheckID) dim_overallstatusforcreditcheckID, tt.fact_salesorderdeliveryid
		from tmp_overallstatus tt
				inner join dim_overallstatusforcreditcheck oscc on tt.VBUK_CMGST = oscc.overallstatusforcreditcheck
		where oscc.RowIsCurrent = 1
	    group by tt.fact_salesorderdeliveryid) src
on sod.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update SET sod.Dim_OverallStatusCreditCheckId = ifnull(src.dim_overallstatusforcreditcheckID, 1)
where sod.Dim_OverallStatusCreditCheckId <> ifnull(src.dim_overallstatusforcreditcheckID, 1);

DROP TABLE IF EXISTS tmp_overallstatus;
/* 10 Aug 2015 CristianT End */

merge into fact_salesorderdelivery fact
using (select fact_salesorderdeliveryid, min(fb.dd_billing_no) dd_billing_no
	   from fact_salesorderdelivery sod
		   		inner join fact_billing fb on    fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        								     AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
	   group by  fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.dd_billing_no = ifnull(src.dd_billing_no,'Not Set')
where fact.dd_billing_no <> ifnull(src.dd_billing_no,'Not Set');

merge into fact_salesorderdelivery fact
using (select fact_salesorderdeliveryid, max(so.Dim_SalesDistrictId) Dim_SalesDistrictId
	   from fact_salesorderdelivery sod
		   		inner join fact_salesorder so on    sod.dd_SalesDocNo = so.dd_SalesDocNo
												and sod.dd_SalesItemNo = so.dd_SalesItemNo
												and sod.dd_ScheduleNo = so.dd_ScheduleNo
	   group by  fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.Dim_SalesDistrictId = ifnull(src.Dim_SalesDistrictId,1)
where fact.Dim_SalesDistrictId <> ifnull(src.Dim_SalesDistrictId,1);

/* Start of the final update	*/

UPDATE fact_salesorderdelivery sd
  /*  SET sd.amt_Cost =
            Decimal(((CASE
                  WHEN f.amt_ExchangeRate < 0
                  THEN
                      (1 / (-1 * f.amt_ExchangeRate))
                  ELSE
                      f.amt_ExchangeRate
                END) * sd.amt_Cost_DocCurr),18,4) , */
	SET sd.amt_Cost = ifnull(sd.amt_Cost_DocCurr,0),			--Not multiplying by local exchg rate. Stored as it is in doc/tran curr
        sd.ct_PriceUnit = ifnull(f.ct_PriceUnit,1),
        sd.amt_UnitPrice = ifnull(f.amt_UnitPrice,0)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
/*
merge into fact_salesorderdelivery fact
using (select sd.fact_salesorderdeliveryid, min(f.amt_UnitPriceUoM) amt_UnitPriceUoM
	   From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
	   WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
			AND f.dd_SalesItemNo = v.VBAP_POSNR
			AND f.dd_ScheduleNo = v.VBEP_ETENR
			AND sd.dd_SalesDocNo = f.dd_SalesDocNo
			AND sd.dd_SalesItemNo = f.dd_SalesItemNo
			AND sd.dd_ScheduleNo = f.dd_ScheduleNo
	   group by sd.fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.amt_UnitPriceUoM = src.amt_UnitPriceUoM
where fact.amt_UnitPriceUoM <> src.amt_UnitPriceUoM   */

/* APP-7744 - Shipped Amt and Std Unit Price incorrect - Oana 23 Oct2017 */
merge into fact_salesorderdelivery fact
using (select sd.fact_salesorderdeliveryid, min(f.amt_UnitPriceUoM) amt_UnitPriceUoM
	   From fact_salesorder f,  fact_salesorderdelivery sd
	   WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
			AND sd.dd_SalesItemNo = f.dd_SalesItemNo
			AND sd.dd_ScheduleNo = f.dd_ScheduleNo
	   group by sd.fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.amt_UnitPriceUoM = IFNULL(src.amt_UnitPriceUoM,0)
where fact.amt_UnitPriceUoM <> IFNULL(src.amt_UnitPriceUoM,0);
/* END APP-7744 - Shipped Amt and Std Unit Price incorrect - Oana 23 Oct2017 */

UPDATE fact_salesorderdelivery sd
    SET sd.amt_ExchangeRate = IFNULL(f.amt_ExchangeRate,1),
        sd.amt_ExchangeRate_GBL = IFNULL(f.amt_ExchangeRate_GBL,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET
        sd.Dim_DateidSalesOrderCreated = IFNULL(f.Dim_DateidSalesOrderCreated,1),
        sd.Dim_DateidSchedDeliveryReq = IFNULL(f.Dim_DateidSchedDeliveryReq,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_DateidSchedDlvrReqPrev = IFNULL(f.Dim_DateidSchedDlvrReqPrev,1),
        sd.Dim_DateidMatlAvailOriginal = IFNULL(f.Dim_DateidMtrlAvail,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_DateidFirstDate = IFNULL(f.Dim_DateidFirstDate,1),
        sd.Dim_Currencyid = IFNULL(f.Dim_Currencyid,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_TRA = IFNULL(f.Dim_Currencyid_TRA,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_GBL = IFNULL(f.Dim_Currencyid_GBL,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_STAT = IFNULL(f.Dim_Currencyid_STAT,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.amt_ExchangeRate_STAT = IFNULL(f.amt_ExchangeRate_STAT,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Companyid = IFNULL(f.Dim_Companyid,1),
        sd.Dim_SalesDivisionid = f.Dim_SalesDivisionid
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_ShipReceivePointid = ifnull(f.Dim_ShipReceivePointid, 1),
        sd.Dim_DocumentCategoryid = ifnull(f.Dim_DocumentCategoryid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_SalesDocumentTypeid = ifnull(f.Dim_SalesDocumentTypeid, 1),
        sd.Dim_SalesOrgid = ifnull(f.Dim_SalesOrgid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_SalesGroupid = ifnull(f.Dim_SalesGroupid, 1),
        sd.Dim_CostCenterid = ifnull(f.Dim_CostCenterid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_BillingBlockid = ifnull(f.Dim_BillingBlockid, 1),
        sd.Dim_TransactionGroupid = ifnull(f.Dim_TransactionGroupid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sd
    SET sd.dim_purchaseordertypeid = ifnull(f.dim_purchaseordertypeid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_CustomeridSoldTo =
            ifnull(CASE sd.Dim_CustomeridSoldTo
              WHEN 1 THEN f.Dim_CustomerID
              ELSE sd.Dim_CustomeridSoldTo
            END, 1),
        sd.Dim_CustomerGroup1id = ifnull(f.Dim_CustomerGroup1id, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup4Id = ifnull(so.Dim_MaterialPriceGroup4Id, 1)
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup4Id,-1) <> ifnull(so.Dim_MaterialPriceGroup4Id, 1);

update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup5Id = ifnull(so.Dim_MaterialPriceGroup5Id, 1)
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup5Id,-1) <> ifnull(so.Dim_MaterialPriceGroup5Id, 1);

UPDATE fact_salesorderdelivery sd
    SET sd.Dim_CustomerGroup2id = ifnull(f.Dim_CustomerGroup2id, 1),
        sd.dim_salesorderitemcategoryid = ifnull(f.dim_salesorderitemcategoryid, 1),
        sd.Dim_ScheduleLineCategoryId = ifnull(f.Dim_ScheduleLineCategoryId, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
  WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

/* Start Changes 12 Feb 2014 */
UPDATE fact_salesorderdelivery sod
 set sod.dim_scheduledeliveryblockid = ifnull(so.dim_scheduledeliveryblockid, 1)
FROM  fact_salesorder so, fact_salesorderdelivery sod
 WHERE sod.dd_Salesdocno = so.dd_SalesDocNo
 and sod.dd_SalesItemNo = so.dd_SalesItemNo
 and sod.dd_ScheduleNo = so.dd_ScheduleNo
 and ifnull(sod.dim_scheduledeliveryblockid,-1) <> ifnull(so.dim_scheduledeliveryblockid,-2);

UPDATE fact_salesorderdelivery sod
SET sod.dim_scheduledeliveryblockid = 1
WHERE sod.dim_scheduledeliveryblockid is NULL;

UPDATE fact_salesorderdelivery sod
SET sod.Dim_CustomerGroup4id = ifnull(so.Dim_CustomerGroup4id,1)
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.Dim_CustomerGroup4id,-1) <> ifnull(so.Dim_CustomerGroup4id,-2);

update fact_salesorderdelivery
set Dim_CustomerGroup4id = 1
where Dim_CustomerGroup4id is NULL;

/* END Changes 14 Feb 2014 */
UPDATE fact_salesorderdelivery sod
set dd_trackingNo = dd_BillofLading
where ifnull(dd_trackingNo,'Not Set') <> dd_BillofLading;

UPDATE fact_salesorderdelivery sod
set dd_trackingNo = 'Not Set'
where dd_trackingNo is null;
/* Start Changes 02 May 2014 */

update fact_salesorderdelivery set dd_BusinessCustomerPONo = 'Not Set' where dd_BusinessCustomerPONo is NULL;

UPDATE fact_salesorderdelivery sod
SET sod.dd_BusinessCustomerPONo = ifnull(so.dd_BusinessCustomerPONo,'Not Set')
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.dd_BusinessCustomerPONo,'Not Set') <> ifnull(so.dd_BusinessCustomerPONo,'Not Set');

/* CristiT Update 13 June 2014 */
update fact_salesorderdelivery sod
set sod.dd_ShipmentNumber = IFNULL(v.VTTK_TKNUM,'Not Set')
FROM fact_salesorderdelivery sod
		inner join LIKP_LIPS l on    sod.dd_SalesDlvrDocNo = l.LIKP_VBELN
  								 AND sod.dd_SalesDlvrItemNo = l.LIPS_POSNR
	    inner join (select max(VTTK_TKNUM) VTTK_TKNUM, VTTP_VBELN from VTTK_VTTP group by VTTP_VBELN) v on l.LIKP_VBELN = v.VTTP_VBELN
where sod.dd_ShipmentNumber <> v.VTTK_TKNUM;

UPDATE fact_salesorderdelivery
SET dd_ShipmentNumber = 'Not Set'
WHERE dd_ShipmentNumber IS NULL;

UPDATE fact_salesorderdelivery sod
SET sod.Dim_ShippingConditionShipmentId = ifnull(sc.Dim_ShippingConditionId, 1)
FROM LIKP_LIPS l, VTTK_VTTP v, Dim_ShippingCondition sc, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = l.LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = l.LIPS_POSNR
  AND l.LIKP_VBELN = v.VTTP_VBELN
  AND sod.dd_ShipmentNumber = v.VTTK_TKNUM
  AND sc.ShippingConditionCode = ifnull(v.VTTK_VSBED, 'Not Set')
  AND sc.RowIsCurrent = 1;

UPDATE fact_salesorderdelivery
SET Dim_ShippingConditionShipmentId = 1
WHERE Dim_ShippingConditionShipmentId IS NULL;
/* CristiT End of Update 13 June 2014 */

/* CristianT 07 Aug 2015 Start: Added logic to populate the following base columns */
DROP TABLE IF EXISTS tmp_vttk_fields;
CREATE TABLE tmp_vttk_fields AS
SELECT DISTINCT ds.VTTK_TKNUM dd_ShipmentNumber,
       ds.VTTP_VBELN dd_SalesDlvrDocNo,
       ifnull(ddcsc.dim_dateid, 1) dim_currshipmentcompldateid,
       ifnull(ddpsc.dim_dateid, 1) dim_plannedshipmentcompldateid,
       ifnull(ddpse.dim_dateid, 1) dim_plannedshipmentenddateid,
       ifnull(ddsd.dim_dateid, 1) dim_shipmentdateid,
       ifnull(ds.VTTK_EXTI1, 'Not Set') dd_voyfltnum,
       ifnull(ds.VTTK_TDLNR, 'Not Set') dd_serviceagent
FROM (select v.VTTK_TKNUM, v.VTTP_VBELN, v.VTTK_DTABF, v.VTTK_DPABF, v.Vttk_DPTEN, v.VTTK_DATEN, v.VTTK_EXTI1, v.VTTK_TDLNR,
	         dc.companycode
	  from fact_salesorderdelivery sod
				inner join dim_company dc on sod.Dim_Companyid = dc.Dim_Companyid
				inner join LIKP_LIPS l on    sod.dd_SalesDlvrDocNo = l.LIKP_VBELN
										 AND sod.dd_SalesDlvrItemNo = l.LIPS_POSNR
				inner join vttk_vttp v on    v.VTTK_TKNUM = sod.dd_ShipmentNumber
										 and v.VTTP_VBELN = l.LIKP_VBELN) ds
			left join dim_date ddcsc on ddcsc.datevalue = ds.VTTK_DTABF and ddcsc.companycode = ds.companycode
			left join dim_date ddpsc on ddpsc.datevalue = ds.VTTK_DPABF and ddpsc.companycode = ds.companycode
			left join dim_date ddpse on ddpse.datevalue = ds.Vttk_DPTEN and ddpse.companycode = ds.companycode
			left join dim_date ddsd  on ddsd.datevalue = ds.VTTK_DATEN  and ddsd.companycode = ds.companycode;

UPDATE fact_salesorderdelivery sod
SET sod.dim_currshipmentcompldateid = ifnull(tmp.dim_currshipmentcompldateid, 1)
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dim_currshipmentcompldateid <> ifnull(tmp.dim_currshipmentcompldateid, 1);

UPDATE fact_salesorderdelivery sod
SET sod.dim_plannedshipmentcompldateid = ifnull(tmp.dim_plannedshipmentcompldateid, 1)
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dim_plannedshipmentcompldateid <> ifnull(tmp.dim_plannedshipmentcompldateid, 1);

UPDATE fact_salesorderdelivery sod
SET sod.dim_plannedshipmentenddateid = ifnull(tmp.dim_plannedshipmentenddateid, 1)
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dim_plannedshipmentenddateid <> ifnull(tmp.dim_plannedshipmentenddateid, 1);

UPDATE fact_salesorderdelivery sod
SET sod.dim_shipmentdateid = ifnull(tmp.dim_shipmentdateid, 1)
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dim_shipmentdateid <> ifnull(tmp.dim_shipmentdateid, 1);

UPDATE fact_salesorderdelivery sod
SET sod.dd_serviceagent = ifnull(tmp.dd_serviceagent, 'Not Set')
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dd_serviceagent <> ifnull(tmp.dd_serviceagent, 'Not Set');

UPDATE fact_salesorderdelivery sod
SET sod.dd_voyfltnum = ifnull(tmp.dd_voyfltnum, 'Not Set')
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dd_voyfltnum <> ifnull(tmp.dd_voyfltnum, 'Not Set');

DROP TABLE IF EXISTS tmp_vttk_fields;
/* CristianT 07 Aug 2015 End */
 /* Update sales order item from sales order done by Alex Manolache 12 12 2014*/

UPDATE fact_salesorderdelivery sod
SET sod.dd_Purchaseorderitem = ifnull(so.dd_Purchaseorderitem ,'Not Set')
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND sod.dd_Purchaseorderitem <> ifnull(so.dd_Purchaseorderitem ,'Not Set');

/* Update dim_routeid  from sales order done by Alex D 15 12 2014*/
merge into fact_salesorderdelivery fact
using ( select sod.fact_salesorderdeliveryid,
			   ifnull(min(so.dim_routeid), 1) dim_routeid,
			   ifnull(min(so.amt_scheduletotal), 0) amt_scheduletotal
		FROM fact_salesorder so, fact_salesorderdelivery sod
		WHERE   sod.dd_SalesDocNo = so.dd_SalesDocNo
			AND sod.dd_SalesItemNo = so.dd_SalesItemNo
			AND sod.dd_ScheduleNo = so.dd_ScheduleNo
		group by fact_salesorderdeliveryid
	   ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.dim_routeid = src.dim_routeid,
	                         fact.amt_scheduletotal = src.amt_scheduletotal;

/* Update BW Hierarchy */

update fact_salesorderdelivery f
set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_salesorderdelivery f, dim_part dp, dim_bwproducthierarchy bw, fact_salesorder fso
where f.dd_salesdocno = fso.dd_salesdocno
and f.dd_salesitemno = fso.dd_salesitemno
and f.dd_scheduleno = fso.dd_scheduleno
and fso.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);

update fact_salesorderdelivery f
set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_salesorderdelivery f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);



/* Merck LS OTIF and OTIF Pct */
drop table if exists tmp_salesorder_otif;
create table tmp_salesorder_otif as
select f.dd_salesdocno,
	f.DD_SALESITEMNO,
    min(f.DD_SCHEDULENO) as DD_SCHEDULENO,
	dt1.datevalue as MTSdate,
	dt1.nextworkingdate as MTSdatecasual,
	dt2.datevalue as MTOdate,
	dt2.nextworkingdate as MTOdatecasual,
	d.mtomts,
	max(dt.datevalue) as ActualGIdate,
	sum(f.ct_ScheduleQtySalesUnit)as ct_ScheduleQtySalesUnit,
	sum(CASE WHEN f.Dim_DateidActualGI <> 1 then f.ct_DeliveredQty else 0 end) as ct_DeliveredQty
from fact_salesorder f
	inner join dim_part d 	on f.dim_partid = d.dim_partid
	inner join dim_date dt  on f.DIM_DATEIDACTUALGI = dt.dim_dateid
	inner join dim_date_factory_calendar dt1 on f.dim_dateidexpectedship_emd = dt1.dim_dateid
	inner join dim_date_factory_calendar dt2 on f.dim_accordinggidatemto_emd = dt2.dim_dateid
group by f.dd_salesdocno, f.DD_SALESITEMNO, dt1.datevalue, dt2.datevalue, dt1.weekdayname, dt2.weekdayname, d.mtomts,dt1.nextworkingdate,dt2.nextworkingdate
HAVING sum(f.ct_ScheduleQtySalesUnit)<> 0;

update fact_salesorder f
set f.dd_deliveryontime = ifnull(
                  CASE
					  WHEN UPPER(b.businessdesc) LIKE '%RESEARCH SOLUTION%' AND t.MTSdate = t.ActualGIdate and t.ActualGIdate <> '0001-01-01' THEN 'X'
					  WHEN t.mtomts = 'MTS' AND UPPER(b.businessdesc) NOT LIKE '%RESEARCH SOLUTION%' AND t.MTSdate = t.ActualGIdate AND t.ActualGIdate <> '0001-01-01' THEN 'X'
					  WHEN t.mtomts = 'MTO' AND UPPER(b.businessdesc) NOT LIKE '%RESEARCH SOLUTION%' AND t.MTOdate = t.ActualGIdate AND t.ActualGIdate <> '0001-01-01' THEN 'X'
					  ELSE  'Not Set'
				  END, 'Not Set'),
f.dd_deliveryisfull = ifnull( case
							when t.ct_ScheduleQtySalesUnit = t.ct_DeliveredQty then 'X'
							else  'Not Set'
						end, 'Not Set' )
from fact_salesorder f, tmp_salesorder_otif t, dim_bwproducthierarchy b
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_bwproducthierarchyid = b.dim_bwproducthierarchyid;


/* 27 Jan 2016 Cornelia add  1 day tolerance */

update fact_salesorder f
set
f.dd_deliveryontimecasual = ifnull(
              CASE
	              WHEN UPPER(b.businessdesc) LIKE '%RESEARCH SOLUTION%' AND t.MTSdatecasual >= t.ActualGIdate and t.ActualGIdate <> '0001-01-01' THEN 'X'
				  WHEN t.mtomts = 'MTS' AND UPPER(b.businessdesc) NOT LIKE '%RESEARCH SOLUTION%' AND t.MTSdatecasual >= t.ActualGIdate AND t.ActualGIdate <> '0001-01-01' THEN 'X'
				  WHEN t.mtomts = 'MTO' AND UPPER(b.businessdesc) NOT LIKE '%RESEARCH SOLUTION%' AND t.MTOdatecasual >= t.ActualGIdate AND t.ActualGIdate <> '0001-01-01' THEN 'X'
				  ELSE  'Not Set'
			  END, 'Not Set')
from fact_salesorder f, tmp_salesorder_otif t, dim_bwproducthierarchy b
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_bwproducthierarchyid = b.dim_bwproducthierarchyid;

drop table if exists tmp_salesorder_otif_new;
create table tmp_salesorder_otif_new as
select f.dd_salesdocno,
	f.DD_SALESITEMNO,
    min(f.DD_SCHEDULENO) as DD_SCHEDULENO,
	dt1.datevalue as MTSdate,
	max(case when dt1.isapublicholiday=1 or dt1.isaweekendday=1 then dt1.nextworkingdate
    else dt1.datevalue end)  MTSdatecasual,
	dt2.datevalue as MTOdate,
	max(case when dt2.isapublicholiday=1 or dt2.isaweekendday=1 then dt2.nextworkingdate
    else dt2.datevalue end) as MTOdatecasual,
	d.mtomts,
	max(dt.datevalue) as ActualGIdate,
	sum(f.ct_ScheduleQtySalesUnit)as ct_ScheduleQtySalesUnit,
	sum(CASE WHEN f.Dim_DateidActualGI <> 1 then f.ct_DeliveredQty else 0 end) as ct_DeliveredQty
from fact_salesorder f
	inner join dim_part d 	on f.dim_partid = d.dim_partid
	inner join dim_date dt  on f.DIM_DATEIDACTUALGI = dt.dim_dateid
	inner join dim_date_factory_calendar dt1 on f.dim_dateidexpectedship_emd = dt1.dim_dateid
	inner join dim_date_factory_calendar dt2 on f.dim_accordinggidatemto_emd = dt2.dim_dateid
group by f.dd_salesdocno, f.DD_SALESITEMNO, dt1.datevalue, dt2.datevalue, dt1.weekdayname, dt2.weekdayname, d.mtomts,dt1.nextworkingdate,dt2.nextworkingdate
HAVING sum(f.ct_ScheduleQtySalesUnit)<> 0;

update fact_salesorder f
set
f.dd_deliveryontimecasual_new = ifnull(
              CASE
	              WHEN UPPER(b.businessdesc) LIKE '%RESEARCH SOLUTION%' AND t.MTSdatecasual >= t.ActualGIdate and t.ActualGIdate <> '0001-01-01' THEN 'X'
				  WHEN t.mtomts = 'MTS' AND UPPER(b.businessdesc) NOT LIKE '%RESEARCH SOLUTION%' AND t.MTSdatecasual >= t.ActualGIdate AND t.ActualGIdate <> '0001-01-01' THEN 'X'
				  WHEN t.mtomts = 'MTO' AND UPPER(b.businessdesc) NOT LIKE '%RESEARCH SOLUTION%' AND t.MTOdatecasual >= t.ActualGIdate AND t.ActualGIdate <> '0001-01-01' THEN 'X'
				  ELSE  'Not Set'
			  END, 'Not Set')
from fact_salesorder f, tmp_salesorder_otif_new t, dim_bwproducthierarchy b
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_bwproducthierarchyid = b.dim_bwproducthierarchyid;


/* END 27 Jan 2016 */

update fact_salesorder f
set f.ct_deliveryisfull = ifnull(case
							   when f.dd_deliveryisfull = 'X' and f.DD_SCHEDULENO = t.DD_SCHEDULENO then 1
						       else 0
						   end, 0),
f.ct_deliveryontime = ifnull(case
							when f.dd_deliveryontime = 'X' and f.DD_SCHEDULENO = t.DD_SCHEDULENO then 1
							else 0
						end, 0),
f.ct_deliveryontimecasual = ifnull(case
								when f.dd_deliveryontimecasual = 'X' and f.DD_SCHEDULENO = t.DD_SCHEDULENO then 1
								else 0
							 end, 0)

from fact_salesorder f, tmp_salesorder_otif t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO;



update fact_salesorder f
set f.ct_countsalesdocitem = 1
from fact_salesorder f, tmp_salesorder_otif t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.DD_SCHEDULENO = t.DD_SCHEDULENO;

/* end OTIF */


/*   PM Delivery document */

DROP TABLE IF EXISTS tmp_SalesDlvrDocNo;
CREATE TABLE tmp_SalesDlvrDocNo as
SELECT left (GROUP_CONCAT( distinct dd_SalesDlvrDocNo),54)  dd_SalesDlvrDocNo,f.dd_SalesDocNo,f.dd_SalesItemNo--,f.dd_ScheduleNo
FROM fact_salesorderdelivery f
GROUP BY f.dd_SalesDocNo,f.dd_SalesItemNo;--,f.dd_ScheduleNo - update delivery doc at item level


UPDATE fact_salesorder so
SET so.dd_SalesDlvrDocNo = f.dd_SalesDlvrDocNo
FROM tmp_SalesDlvrDocNo f , fact_salesorder so
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
AND  f.dd_SalesItemNo = so.dd_SalesItemNo;
--AND f.dd_ScheduleNo = so.dd_ScheduleNo

/* Add loading date - Cornelia */
drop table if exists tmp_update_loadingdate;
create table tmp_update_loadingdate as
select sod.dim_date_enddateloadingid
,sod.dd_SalesDlvrDocNo
,sod.dd_salesdlvrItemno
,sod.dd_SalesDocNo
,sod.dd_salesItemNo
,sod.dd_ScheduleNo
,dat.dim_Dateid
,v.vttk_dalen
,v.VTTK_SHTYP
,v.vttk_tknum
,row_number() over(partition by
sod.dd_SalesDlvrDocNo, sod.dd_salesdlvrItemno order by v.vttk_dalen) rownumber
FROM
FACT_SALESORDERDELIVERY SOD
inner join dim_company cp on sod.dim_companyid = cp.dim_companyid
inner join LIKP_LIPS l on sod.dd_SalesDlvrDocNo = l.LIKP_VBELN
                                   AND sod.dd_SalesDlvrItemNo = l.LIPS_POSNR
inner join VTTK_VTTP v ON sod.dd_SalesDlvrDocNo = v.VTTP_VBELN,
dim_date dat
WHERE
dat.datevalue = v.vttk_dalen
and dat.companycode = cp.companycode
and v.vttk_dalen is not null
and left(v.VTTK_SHTYP,2) <> 'YH' and  left(v.VTTK_SHTYP,4) <> 'YM99' AND left(v.VTTK_SHTYP,4) <> 'YMUM';

/*
update fact_salesorder so
set so.dim_date_enddateloadingid = ifnull(sod.dim_Dateid,1)
FROM
FACT_SALESORDER SO , tmp_update_loadingdate sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
and sod.rownumber = 1
and so.dim_date_enddateloadingid <> ifnull(sod.dim_Dateid,1) */

update fact_salesorderdelivery sod
set sod.dim_date_enddateloadingid = ifnull(tmp.dim_Dateid,1)
from  fact_salesorderdelivery sod, tmp_update_loadingdate tmp
where sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
and sod.dd_salesdlvrItemno = tmp.dd_salesdlvrItemno
and tmp.rownumber = 1
and sod.dim_date_enddateloadingid <> ifnull(tmp.dim_Dateid,1);

/* Update CommercialView*/

   update fact_salesorderdelivery f
	set f.dim_commercialviewid= ifnull(ds.dim_commercialviewid, 1)
	from dim_commercialview ds, dim_customer a, dim_bwproducthierarchy b,fact_salesorderdelivery f
	where ds.SOLDTOCOUNTRY=a.COUNTRY
	and ds.UPPERPRODHIER=b.BUSINESS
	and f.DIM_BWPRODUCTHIERARCHYID=b.DIM_BWPRODUCTHIERARCHYID
	and f.dim_customeridsoldto=a.dim_customerid
	and f.dim_commercialviewid <> ifnull(ds.dim_commercialviewid, 1);

   update fact_salesorderdelivery f
	set f.dim_commercialviewid= ifnull(ds.dim_commercialviewid, 1)
	from dim_commercialview ds, emdtempocc4.dim_customer a, dim_bwproducthierarchy b,fact_salesorderdelivery f
	where ds.SOLDTOCOUNTRY=a.COUNTRY
	and ds.UPPERPRODHIER=b.BUSINESS
	and f.DIM_BWPRODUCTHIERARCHYID=b.DIM_BWPRODUCTHIERARCHYID
	and f.dim_customeridsoldto=a.dim_customerid
	and f.dim_commercialviewid <> ifnull(ds.dim_commercialviewid, 1);

/* Intercompany Flag*/

update fact_salesorderdelivery fso
set DD_INTERCOMPFLAG = ifnull(filter_object, 'Not Set')
from fact_salesorderdelivery fso, ACE_3RD_PARTY_FILTERS ace, dim_customer dc, dim_projectsource prj
where  fso.Dim_CustomeridShipTo = dc.dim_customerid
    and ((left(ace.description,8) = left(prj.projectcode,8) and trim (leading '0' from dc.customernumber)=trim (leading '0' from (right(ace.intercompany,8))))
or (ace.filter_value='PCOMPANY' AND  trim (leading '0' from dc.TRADINGPARTNER)=ace.intercompany));

update fact_salesorderdelivery sod
set sod.DD_INTERCOMPFLAG = ifnull(so.DD_INTERCOMPFLAG, 'Not Set')
from fact_salesorder so, fact_salesorderdelivery sod
where sod.dd_SalesDocNo = so.dd_SalesDocNo
	and sod.dd_SalesItemNo = so.dd_SalesItemNo
    and sod.dd_ScheduleNo =  so.dd_ScheduleNo
    and sod.DD_INTERCOMPFLAG <> ifnull(so.DD_INTERCOMPFLAG, 'Not Set');

/* Alex D - cover intercomapny flag 'Not Set' using  dd_SalesDocNo */
drop table if exists tmp_INTERCOMPFLAG;
create table tmp_INTERCOMPFLAG as
select distinct f.dd_SalesDocNo,f.DD_INTERCOMPFLAG
from fact_salesorderdelivery f where f.DD_INTERCOMPFLAG<>'Not Set';

update fact_salesorderdelivery f
set  f.DD_INTERCOMPFLAG  = ifnull(tmp.DD_INTERCOMPFLAG, 'Not Set')
from fact_salesorderdelivery f ,tmp_INTERCOMPFLAG tmp
where f.dd_SalesDocNo = tmp.dd_SalesDocNo
and f.DD_INTERCOMPFLAG = 'Not Set';


/*MERGE INTO fact_salesorderdelivery fso
USING ( SELECT  distinct fact_salesorderdeliveryid,filter_object
FROM fact_salesorderdelivery fso, ACE_3RD_PARTY_FILTERS ace, dim_customer dc, dim_projectsource prj
WHERE fso.dim_customeridsoldto = dc.dim_customerid
    AND ((left(ace.description,8) = left(prj.projectcode,8) and trim (leading '0' from dc.customernumber)=trim (leading '0' from (right(ace.intercompany,8))))
    OR (ace.filter_value='PCOMPANY' AND  trim (leading '0' from dc.TRADINGPARTNER)=ace.intercompany ))) SRC
ON fso.fact_salesorderdeliveryid = SRC.fact_salesorderdeliveryid
WHEN MATCHED THEN UPDATE SET fso.DD_INTERCOMPFLAG = ifnull(SRC.filter_object, 'Not Set')
WHERE fso.DD_INTERCOMPFLAG <> SRC.filter_object */

update fact_salesorderdelivery f
set DD_INTERCOMPFLAG ='Intercompany'
where DD_INTERCOMPFLAG = 'Not Set';

/* Update Merck LS ACE Opne Orders*/

update fact_salesorderdelivery  fso
set dd_ace_openorders = ifnull('X', 'Not Set')
from fact_salesorderdelivery fso
    inner join fact_salesorder f on f.dd_salesdocno = fso.dd_salesdocno
    and f.dd_salesitemno = fso.dd_salesitemno
    and f.dd_scheduleno = fso.dd_scheduleno
	inner join dim_date socd on f.Dim_DateidSalesOrderCreated = socd.dim_dateid and socd.datevalue > '2012-12-31'
	inner join Dim_DocumentCategory dc on f.Dim_DocumentCategoryid = dc.Dim_DocumentCategoryid
	inner join dim_salesorderitemstatus sois on f.Dim_SalesOrderItemStatusid = sois.Dim_SalesOrderItemStatusid
	inner join Dim_SalesOrderItemCategory soic on f.Dim_salesorderitemcategoryid = soic.Dim_SalesOrderItemCategoryid
	inner join Dim_SalesDocumentType sdt on f.Dim_SalesDocumentTypeid = sdt.Dim_SalesDocumentTypeid
	inner join dim_company cmp on f.dim_companyid = cmp.dim_companyid
	inner join Dim_ScheduleLineCategory slct on f.Dim_ScheduleLineCategoryId = slct.Dim_ScheduleLineCategoryId;

	/* Merck LS Actual Goods Issue Date */

drop table if exists update_so_shipment_001;
create table update_so_shipment_001 as
select f.dd_SalesDocNo v_dd_SalesDocNo, f.dd_SalesItemNo v_dd_SalesItemNo, f.dd_ScheduleNo v_dd_ScheduleNo,
	ifnull(sum(f.ct_QtyDelivered), 0) v_ct_DeliveredQty,
	ifnull(max(f.Dim_DateidDeliveryDate), 1) v_Dim_DateidShipmentDelivery,
	ifnull(max(f.Dim_DateidActualGoodsIssue), 1) v_Dim_DateidActualGI,
	min(f.Dim_CustomeridShipto) v_Dim_CustomeridShipTo,
	ifnull(min(f.Dim_DateidDlvrDocCreated), 1) v_Dim_DateidDlvrDocCreated
from fact_salesorderdelivery f
	inner join dim_salesorderitemstatus sois on f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.GoodsMovementStatus <> 'Not yet processed'
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo;


MERGE INTO fact_salesorderdelivery so
USING update_so_shipment_001 u ON so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET Dim_DateidActualGI = ifnull(v_Dim_DateidActualGI, 1);

drop table if exists tmp_actualdategi_merckls;
create table tmp_actualdategi_merckls
as select distinct f.dd_salesdocno,
f.DD_SALESITEMNO,
first_value(f.DIM_DATEIDACTUALGI) over (partition by f.dd_salesdocno, f.DD_SALESITEMNO order by dd.datevalue desc) as DD_DATEIDACTUALGI
from fact_salesorderdelivery f
inner join dim_date dd on f.DIM_DATEIDACTUALGI = dd.dim_dateid;

update fact_salesorderdelivery f
set f.dim_dateidactualgimerckls = ifnull(t.DD_DATEIDACTUALGI, 1)
from fact_salesorderdelivery f, tmp_actualdategi_merckls t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_dateidactualgimerckls <> ifnull(t.DD_DATEIDACTUALGI, 1);


/* Update Merck LS Cluster */
  update fact_salesorderdelivery f
set f.dim_clusterid = ifnull(dc.dim_clusterid, 1)
from fact_salesorderdelivery f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> ifnull(dc.dim_clusterid, 1);

	/* dim_AccordingGIDateMTO_emd and dim_dateidexpectedship_emd */
	update fact_salesorderdelivery f
set dim_AccordingGIDateMTO_emd = ifnull(dim_dateid, 1)
from fact_salesorderdelivery f,fact_salesorder fso, dim_date_factory_calendar dd
where f.dd_salesdocno = fso.dd_salesdocno
    and f.dd_salesitemno = fso.dd_salesitemno
    and f.dd_scheduleno = fso.dd_scheduleno
    and fso.dim_AccordingGIDateMTO_emd=dd.dim_dateid;

   update fact_salesorderdelivery f
set dim_dateidexpectedship_emd = ifnull(dim_dateid, 1)
from fact_salesorderdelivery f,fact_salesorder fso, dim_date_factory_calendar dd
where f.dd_salesdocno = fso.dd_salesdocno
    and f.dd_salesitemno = fso.dd_salesitemno
    and f.dd_scheduleno = fso.dd_scheduleno
    and fso.dim_dateidexpectedship_emd =dd.dim_dateid;

	/* GSA Code Update */

	update fact_salesorderdelivery f_so set f_so.dim_gsamapimportid = ifnull(gsa.dim_gsamapimportid,1)
    FROM fact_salesorderdelivery f_so
        ,dim_customer dc
        ,dim_bwproducthierarchy dph
        ,dim_gsamapimport gsa
 WHERE f_so.dim_customeridsoldto = dc.dim_customerid
   AND f_so.dim_bwproducthierarchyid = dph.dim_bwproducthierarchyid
   AND TRIM(LEADING '0' FROM dc.customernumber) = CAST(gsa.customer_number AS VARCHAR(10))
   AND TRIM(UPPER(dph.businessdesc)) LIKE '%'||TRIM(UPPER(gsa.business_name))||'%'
   AND gsa.platform = 'PHOENIX'
   and f_so.dim_gsamapimportid <> ifnull(gsa.dim_gsamapimportid,1);

/*
   update fact_salesorderdelivery f_so set f_so.dim_gsamapimportid = ifnull(gsa.dim_gsamapimportid,1)
    FROM fact_salesorderdelivery f_so
        ,emdtempocc4.dim_customer dc
        ,dim_bwproducthierarchy dph
        ,dim_gsamapimport gsa
 WHERE f_so.dim_customeridsoldto = dc.dim_customerid
   AND f_so.dim_bwproducthierarchyid = dph.dim_bwproducthierarchyid
   AND TRIM(LEADING '0' FROM dc.customernumber) = CAST(gsa.customer_number AS VARCHAR(10))
   AND TRIM(UPPER(dph.businessdesc)) LIKE '%'||TRIM(UPPER(gsa.business_name))||'%'
   AND gsa.platform = 'TEMPO'
   and f_so.dim_gsamapimportid <> ifnull(gsa.dim_gsamapimportid,1)*/

MERGE INTO fact_salesorderdelivery f_so
 USING (SELECT f_so.dim_customeridsoldto as dim_customeridsoldto, f_so.dim_bwproducthierarchyid  dim_bwproducthierarchyid,
            MAX(gsa.dim_gsamapimportid) as dim_gsamapimportid
         FROM fact_salesorderdelivery f_so,emdtempocc4.dim_customer dc
        ,dim_bwproducthierarchy dph,dim_gsamapimport gsa
        WHERE f_so.dim_customeridsoldto = dc.dim_customerid
        AND f_so.dim_bwproducthierarchyid = dph.dim_bwproducthierarchyid
        AND TRIM(LEADING '0' FROM dc.customernumber) = CAST(gsa.customer_number AS VARCHAR(10))
        AND TRIM(UPPER(dph.businessdesc)) LIKE '%'||TRIM(UPPER(gsa.business_name))||'%'
        AND gsa.platform = 'TEMPO'
        and f_so.dim_gsamapimportid <> ifnull(gsa.dim_gsamapimportid,1)
        GROUP BY f_so.dim_customeridsoldto, f_so.dim_bwproducthierarchyid) x
ON (f_so.dim_customeridsoldto = x.dim_customeridsoldto
and f_so.dim_bwproducthierarchyid = x.dim_bwproducthierarchyid)
WHEN MATCHED THEN
UPDATE SET f_so.dim_gsamapimportid = ifnull(x.dim_gsamapimportid,1)
WHERE f_so.dim_gsamapimportid <> ifnull(x.dim_gsamapimportid,1);

  /* MDG Part */

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_SOD;
CREATE TABLE TMP_DIM_MDG_PARTID_SOD as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;


UPDATE fact_salesorderdelivery f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM fact_salesorderdelivery f, TMP_DIM_MDG_PARTID_SOD tmp
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

drop table if exists TMP_DIM_MDG_PARTID_SOD;


/*drop table if exists partnumber_businessSectorNotSet_salesorderdelivery
create table partnumber_businessSectorNotSet_salesorderdelivery
as
select distinct fact_salesorderdeliveryid
from fact_salesorderdelivery f,dim_bwproducthierarchy bw
where f.dim_bwproducthierarchyid=bw.dim_bwproducthierarchyid
and businesssector='Not Set'

update fact_salesorderdelivery f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_salesorderdelivery f, dim_part dp,dim_mdg_part mdg, dim_bwproducthierarchy bw,partnumber_businessSectorNotSet_salesorderdelivery pn
where f.dim_partid = dp.dim_partid
and f.dim_mdg_partid=mdg.dim_mdg_partid
and f.fact_salesorderdeliveryid=pn.fact_salesorderdeliveryid
and dp.producthierarchy = bw.lowerhierarchycode
and mdg.GlbProductGroup = bw.upperhierarchycode
and f.dim_bwproducthierarchyid <>bw.dim_bwproducthierarchyid
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate

  update fact_salesorderdelivery f
set f.dim_clusterid = dc.dim_clusterid
from fact_salesorderdelivery f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> dc.dim_clusterid*/

/*Deviation GI Date vs Req Date*/

UPDATE fact_salesorder fso
SET ct_Devexpect = ifnull(Case when  rdemd.DateValue = '0001-01-01' then 0
                             else
								  case when agidt.DateValue = '0001-01-01' then (current_date - rdemd.DateValue)
                                       else(agidt.DateValue - rdemd.DateValue)
									   end
					    end, 0)
FROM fact_salesorder fso, dim_date_factory_calendar rdemd, dim_date agidt
WHERE fso.dim_dateidexpectedship_emd = rdemd.dim_dateid
AND   fso.dim_dateidactualgi = agidt.dim_dateid;




 /*Delivery Service against confirmed del date */


UPDATE fact_salesorder fso
SET ct_DelSexpectdt =  ifnull(case when rdemd.DateValue >  (current_date+1 ) then 999999
                               else case when dim_dateidexpectedship_emd = 1 then 0
                                    else case when ct_Devexpect  > 1 then 0
		                                 else 1
                                             end
                                    end
								end, 0)

FROM fact_salesorder fso,dim_date rdemd
WHERE  fso.dim_dateidactualgi = rdemd.dim_dateid;

UPDATE fact_salesorder
SET dd_ontimerequested = CASE WHEN CT_DELSEXPECTDT = 0 THEN 'No' WHEN CT_DELSEXPECTDT = 1 THEN 'Yes' ELSE 'Not Set' END
FROM fact_salesorder
WHERE dd_ontimerequested <> CASE WHEN CT_DELSEXPECTDT = 0 THEN 'No' WHEN CT_DELSEXPECTDT = 1 THEN 'Yes' ELSE 'Not Set' END;



/* Add Incoterms to salesdelivery -  Alina 01.02.2017*/

update fact_salesorderdelivery f
set f.dd_inco1 = ifnull(ff.dd_inco1, 'Not Set'), f.dd_inco2 = ifnull(ff.dd_inco2, 'Not Set')
from  fact_salesorderdelivery f, fact_salesorder ff
where f.dd_SalesDocNo = ff.dd_SalesDocNo
        AND f.dd_SalesItemNo = ff.dd_SalesItemNo
        AND f.dd_ScheduleNo = ff.dd_ScheduleNo;

/* 3 Feb 2017  - Add  Loading End Date from Sales */
/* update fact_salesorderdelivery f
set f.dim_date_enddateloadingid = ff.dim_date_enddateloadingid
from  fact_salesorderdelivery f, fact_salesorder ff
where f.dd_SalesDocNo = ff.dd_SalesDocNo
        AND f.dd_SalesItemNo = ff.dd_SalesItemNo
        AND f.dd_ScheduleNo = ff.dd_ScheduleNo
and f.dim_date_enddateloadingid <> ff.dim_date_enddateloadingid */


DROP TABLE IF EXISTS tmp_SalesDlvrDocNo;
Drop table if exists flag_holder_722;
Drop table if exists cursor_table1_722;
Drop table if exists update_tbl_722;
Drop table if exists loop_tbl_722;
Drop table if exists update_so_shipment_001;
Drop table if exists tmp_fact_sodf_LIKP_LIPS;

/* Marius Shipped against Order Qty - Moved here from vw_bi_populate_so_shippedAgainstOrderQty.sql */


drop table if exists tmp_so_002;
create table tmp_so_002 as
select distinct
	f.dd_SalesDocNo,
	f.dd_SalesItemNo,
	f.dd_ScheduleNo,
	PGI.DateValue GI_Date,
	f.ct_ScheduleQtySalesUnit SchedQty,
	p.PlantCode
from fact_Salesorder f
inner join fact_SalesOrderDelivery sd on f.dd_SalesDocNo = sd.dd_SalesDocNo and f.dd_SalesItemNo = sd.dd_SalesItemNo
/* inner join likp_lips l on l.likp_vbeln = sd.dd_SalesDlvrDocNo and l.lips_posnr = sd.dd_Salesdlvritemno - TEMPORARY COMMENTED TO FIX VALUES */
inner join dim_salesorderitemstatus sois on sd.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
inner join dim_Date PGI on PGI.Dim_DateId = f.Dim_DateidGoodsIssue
inner join Dim_Plant p on p.Dim_Plantid = f.Dim_Plantid
where f.ct_ScheduleQtySalesUnit > 0
	and f.dd_ItemRelForDelv = 'X'
	and sois.GoodsMovementStatus <> 'Not yet processed';

drop table if exists tmp_so_001;
create table tmp_so_001 as
select f.dd_SalesDocNo,
	f.dd_SalesItemNo,
	f.dd_ScheduleNo,
	f.GI_Date,
	f.SchedQty,
	f.PlantCode,
	ROW_NUMBER() OVER(PARTITION BY f.dd_SalesDocNo, f.dd_SalesItemNo
			  ORDER BY f.GI_Date, f.dd_ScheduleNo) RowSeqNo
from tmp_so_002 f;


drop table if exists tmp_sodlvr_001;
create table tmp_sodlvr_001 as
select fd.dd_SalesDocNo,
	fd.dd_SalesItemNo,
	sum(fd.ct_QtyDelivered) TotQtyDelivered
from fact_salesorderdelivery fd
where exists (select 1 from tmp_so_001 a
		where a.dd_SalesDocNo = fd.dd_SalesDocNo
			and a.dd_SalesItemNo = fd.dd_SalesItemNo)
group by fd.dd_SalesDocNo, fd.dd_SalesItemNo;


drop table if exists tmp_socummqty_001;
create table tmp_socummqty_001 as
select VBLB_VBELN c_SalesDocNo,
	VBLB_POSNR c_SalesItemNo,
	max(VBLB_ABEFZ) c_TotalCmlRcvdQty
from VBLB
group by VBLB_VBELN, VBLB_POSNR;


drop table if exists tmp_so_maxseqno_001;
create table tmp_so_maxseqno_001 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	max(a.RowSeqNo) MaxRowSeqNo
from tmp_so_001 a
group by a.dd_SalesDocNo,
	a.dd_SalesItemNo;


drop table if exists tmp_so_002;
create table tmp_so_002 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo,
	sum(b.SchedQty) RunnSchedQty
from tmp_so_001 a
	inner join tmp_so_001 b
	on (a.dd_SalesDocNo = b.dd_SalesDocNo
		and a.dd_SalesItemNo = b.dd_SalesItemNo)
where a.RowSeqNo >= b.RowSeqNo
group by a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo;


drop table if exists tmp_so_001;
create table tmp_so_001 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo,
	a.RunnSchedQty,
	b.TotQtyDelivered,
	ifnull(c.c_TotalCmlRcvdQty,0) TotalCmlRcvdQty,
	m.MaxRowSeqNo
from tmp_so_002 a
	inner join tmp_so_maxseqno_001 m on a.dd_SalesDocNo = m.dd_SalesDocNo and a.dd_SalesItemNo = m.dd_SalesItemNo
	inner join tmp_sodlvr_001 b on a.dd_SalesDocNo = b.dd_SalesDocNo and a.dd_SalesItemNo = b.dd_SalesItemNo
	left join tmp_socummqty_001 c on a.dd_SalesDocNo = c.c_SalesDocNo and a.dd_SalesItemNo = c.c_SalesItemNo;


drop table if exists tmp_so_002;
create table tmp_so_002 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	case when a.TotQtyDelivered > (a.RunnSchedQty - a.SchedQty)
		then case when a.TotQtyDelivered >= a.RunnSchedQty and a.RowSeqNo < a.MaxRowSeqNo
			then a.SchedQty
			else a.TotQtyDelivered - (a.RunnSchedQty - a.SchedQty)
		     end
	     else 0
	end ShippedAgnstOrderQty,
	case when a.TotalCmlRcvdQty > (a.RunnSchedQty - a.SchedQty)
		then case when a.TotalCmlRcvdQty >= a.RunnSchedQty and a.RowSeqNo < a.MaxRowSeqNo
			then a.SchedQty
			else a.TotalCmlRcvdQty - (a.RunnSchedQty - a.SchedQty)
		     end
	     else 0
	end CmlQtyReceived
from tmp_so_001 a;


UPDATE fact_Salesorder f
      SET f.ct_ShippedAgnstOrderQty = ifnull(a.ShippedAgnstOrderQty, 0),
          f.ct_CmlQtyReceived = ifnull(a.CmlQtyReceived, 0)
FROM tmp_so_002 a, fact_Salesorder f
    WHERE     f.dd_SalesDocNo = a.dd_SalesDocNo
          AND f.dd_SalesItemNo = a.dd_SalesItemNo
          AND f.dd_ScheduleNo = a.dd_ScheduleNo;


drop table if exists tmp_so_001;
drop table if exists tmp_sodlvr_001;
drop table if exists tmp_socummqty_001;
drop table if exists tmp_so_maxseqno_001;
drop table if exists tmp_so_002;

/* END Marius Shipped against Order Qty - Moved here from vw_bi_populate_so_shippedAgainstOrderQty.sql */


/* EMD Merck LS Backorder Filter - Moved in Delivery from Sales Order - since it's using the field ct_CmlQtyReceived - calculated in Sales Delivery -  Roxana D 2017-10-23 */

update fact_salesorder f
set dd_mercklsbackorderfilter = 'Not Set'
where dd_mercklsbackorderfilter <> 'Not Set';

update fact_salesorder f
set dd_mercklsbackorderfilter = ifnull('X', 'Not Set')
from fact_salesorder f
	inner join dim_date socd on f.Dim_DateidSalesOrderCreated = socd.dim_dateid and socd.datevalue > '2012-12-31'
	inner join Dim_DocumentCategory dc on f.Dim_DocumentCategoryid = dc.Dim_DocumentCategoryid
	inner join dim_salesorderitemstatus sois on f.Dim_SalesOrderItemStatusid = sois.Dim_SalesOrderItemStatusid
	inner join Dim_SalesOrderItemCategory soic on f.Dim_salesorderitemcategoryid = soic.Dim_SalesOrderItemCategoryid
	inner join Dim_SalesDocumentType sdt on f.Dim_SalesDocumentTypeid = sdt.Dim_SalesDocumentTypeid
	inner join dim_company cmp on f.dim_companyid = cmp.dim_companyid
where dc.DocumentCategory in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBAK-VBTYP' and PLATFORMS like '%P16%')
	and sois.ItemRejectionStatus in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBUP-ABSTA' and PLATFORMS like '%P16%')
	and sois.OverallDeliveryStatus in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBUP-LFGSA' and PLATFORMS like '%P16%')
	and sois.itemdatafordeliv = 'Completely processed'
	and soic.SalesOrderItemCategory not in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBAP-PSTYV' and PLATFORMS like '%P16%')
	and sdt.DocumentType in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBAK-AUART' and PLATFORMS like '%P16%')
	and trim(leading '0' from cmp.company) in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'T001-RCOMP' and PLATFORMS like '%P16%')
	and
	case when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
		 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
				then (case when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
						   else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) end)
		 when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
		 else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
	end > 0 ;

/* EMD Merck LS Backorder Filter - end 2017-10-23 */


/* Moved to vw_bi_populate_salesorderdelivery_fact from Sales Order  since it's using the dd_mercklsbackorderfilter and ct_CmlQtyReceived  2017-10-23  Roxana D */
/* EMD Merck LS Backorder covered by Material */

drop table if exists tmp_sales_openqty_1;
create table tmp_sales_openqty_1
as
select
	f_so.dd_salesdocno
	,f_so.dd_salesitemno
	,dp.partnumber
	,d.deliverypriority
	,dd.datevalue dim_dateidrequested_emd
	,sum(case
	         when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
			 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X'
			     then (case
				           when (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 then 0.0000
						   else (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
					   end)
			 when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000
			 else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
	      end) openqty
from fact_salesorder f_so
	inner join dim_part dp on f_so.dim_partid = dp.dim_partid
	inner join dim_deliverypriority d on f_so.dim_deliverypriorityId = d.dim_deliverypriorityId
	inner join dim_date dd on f_so.dim_dateidrequested_emd = dd.dim_dateid
	inner join dim_date cddemd on f_so.dim_dateidconfirmeddelivery_emd = cddemd.dim_dateid
	inner join dim_salesdocumenttype sdt on f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
	inner join dim_salesorderheaderstatus sohs on f_so.dim_salesorderheaderstatusid = sohs.dim_salesorderheaderstatusid
	inner join dim_salesorderitemstatus sois on f_so.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
	inner join dim_date_factory_calendar esdemd on f_so.dim_dateidexpectedship_emd = esdemd.dim_dateid
	inner join dim_date_factory_calendar acgidmto on f_so.dim_accordinggidatemto_emd = acgidmto.dim_dateid
	inner join dim_bwproducthierarchy bw on f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
where dd_mercklsbackorderfilter = 'X'
	and CASE
	    WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%'
		    	THEN
							 CASE
							      WHEN DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue ) < current_date
								     AND sohs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
								     AND sois.OverallProcessingStatus   not in ('Completely processed' , 'Tratado completamente')
							      THEN 'Yes'
										ELSE 'No'
							 END
			ELSE
					case
							when DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue ) < current_date
								and dp.mtomts = 'MTS'
								and sohs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
								and sois.OverallProcessingStatus   not in ('Completely processed' , 'Tratado completamente')
							then 'Yes'
							when DECODE( acgidmto.DateValue , '0001-01-01', cddemd.DateValue , acgidmto.DateValue ) < current_date
								and dp.mtomts = 'MTO'
								and sohs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
								and sois.OverallProcessingStatus   not in ('Completely processed' , 'Tratado completamente')
							then  'Yes'
						else 'No'
						end
	END  = 'Yes'
	and bw.businesssector = 'BS-02'
group by dd_salesdocno, dd_salesitemno, dp.partnumber, d.deliverypriority, dd.datevalue
having sum(case
	         when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
			 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X'
			     then (case
				           when (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 then 0.0000
						   else (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
					   end)
			 when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000
			 else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
	      end) > 0;

drop table if exists tmp_sales_openqty;

create table tmp_sales_openqty
as
select
	row_number() over(order by deliverypriority desc, dim_dateidrequested_emd asc) proc_order
	,dd_salesdocno
	,dd_salesitemno
	,partnumber
	,openqty
from tmp_sales_openqty_1;

drop table if exists tmp_sales_openqty_cum;
create table tmp_sales_openqty_cum
as
select
	a.proc_order
	,a.dd_salesdocno
	,a.dd_salesitemno
	,a.partnumber
	,a.openqty
	,'Not Set' cvrd_by_mat
	,sum(a.openqty) over(partition by partnumber order by a.proc_order rows between unbounded preceding and current row) cum_openqty
from tmp_sales_openqty a
group by a.proc_order, a.dd_salesdocno, a.dd_salesitemno, a.partnumber, a.openqty;

drop table if exists tmp_uprestrictedstock;
create table tmp_uprestrictedstock
as
select
	dp.partnumber
	,sum(f.ct_StockQty) ct_StockQty
from fact_inventoryaging f
	inner join dim_part dp on dp.dim_partid = f.dim_partid
group by dp.partnumber;

update tmp_sales_openqty_cum a
set a.cvrd_by_mat = 'X'
from tmp_sales_openqty_cum a, tmp_uprestrictedstock b
where a.partnumber = b.partnumber and a.cum_openqty <= b.ct_StockQty;

/* check if the remaining stock can be allocated */

drop table if exists tmp_unalocatedstock;
create table tmp_unalocatedstock
as
select a.partnumber,max(b.ct_StockQty) - max(a.cum_openqty) remaining_stock
from tmp_sales_openqty_cum a
	inner join tmp_uprestrictedstock b on a.partnumber = b.partnumber
where a.cvrd_by_mat = 'X'
group by a.partnumber
having max(b.ct_StockQty) - max(a.cum_openqty) > 0;

/* add materials that did not manage to cover any order but have stock */

drop table if exists tmp_materialsnotcovered;
create table tmp_materialsnotcovered
as
select distinct a.partnumber from tmp_sales_openqty_cum a where not exists (select 1 from tmp_sales_openqty_cum b where a.partnumber = b.partnumber and b.cvrd_by_mat = 'X');

insert into tmp_unalocatedstock(partnumber, remaining_stock)
select partnumber, ct_StockQty from tmp_uprestrictedstock where partnumber in (select partnumber from tmp_materialsnotcovered) and ct_StockQty > 0;

drop table if exists tmp_sowithunalocatedstock;
create table tmp_sowithunalocatedstock
as
select a.*
from tmp_sales_openqty_cum a
	inner join tmp_unalocatedstock b on a.partnumber = b.partnumber
where a.openqty <= b.remaining_stock
	and a.cvrd_by_mat = 'Not Set';

execute script emd_covered_by_material('tmp_sowithunalocatedstock','tmp_unalocatedstock','tmp_sales_openqty_cum');

/* end check if the remaining stock can be allocated */

update fact_salesorder f
set dd_openqtycvrdbymat = 'Not Set'
where dd_openqtycvrdbymat <> 'Not Set';

update fact_salesorder f
set f.dd_openqtycvrdbymat = ifnull(trim(t.cvrd_by_mat), 'Not Set')
from fact_salesorder f, tmp_sales_openqty_cum t
where f.dd_salesdocno = t.dd_salesdocno and f.dd_salesitemno = t.dd_salesitemno
	and f.dd_openqtycvrdbymat <> ifnull(trim(t.cvrd_by_mat), 'Not Set');

drop table if exists tmp_sales_openqty_1;
drop table if exists tmp_sales_openqty;
drop table if exists tmp_sales_openqty_cum;
drop table if exists tmp_unalocatedstock;
drop table if exists tmp_sowithunalocatedstock;
drop table if exists tmp_materialsnotcovered;

/* END EMD Merck LS Backorder covered by Material - addded from vw_bi_populate_salesorder_fact - Roxana D 2017-10-23*/





/* EMD Merck LS Backorder covered by Plant */

drop table if exists tmp_sales_openqty_1;
create table tmp_sales_openqty_1
as
select
	f_so.dd_salesdocno
	,f_so.dd_salesitemno
	,dp.partnumber
	,pl.PLANTCODE
	,d.deliverypriority
	,CASE
	     WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue )
	     ELSE CASE WHEN dp.mtomts = 'MTS' THEN DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue )
		           ELSE DECODE( acgidmto.DateValue , '0001-01-01', cddemd.DateValue , acgidmto.DateValue )
		      END end dim_dateidrequested_emd
	,sum(case
	         when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
			 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X'
			     then (case
				           when (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 then 0.0000
						   else (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
					   end)
			 when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000
			 else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
	      end) openqty
from fact_salesorder f_so
	inner join dim_part dp on f_so.dim_partid = dp.dim_partid
	inner join dim_deliverypriority d on f_so.dim_deliverypriorityId = d.dim_deliverypriorityId
	inner join dim_date dd on f_so.dim_dateidrequested_emd = dd.dim_dateid
	inner join dim_date cddemd on f_so.dim_dateidconfirmeddelivery_emd = cddemd.dim_dateid
	inner join dim_salesdocumenttype sdt on f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
	inner join dim_salesorderheaderstatus sohs on f_so.dim_salesorderheaderstatusid = sohs.dim_salesorderheaderstatusid
	inner join dim_salesorderitemstatus sois on f_so.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
	inner join dim_date_factory_calendar esdemd on f_so.dim_dateidexpectedship_emd = esdemd.dim_dateid
	inner join dim_date_factory_calendar acgidmto on f_so.dim_accordinggidatemto_emd = acgidmto.dim_dateid
	inner join dim_bwproducthierarchy bw on f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
	inner join dim_plant pl on f_so.dim_plantid = pl.dim_plantid
where dd_mercklsbackorderfilter = 'X'
	and CASE
    WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%'
	    	THEN
						 CASE
						      WHEN DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue ) < current_date
							     AND sohs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
							     AND sois.OverallProcessingStatus   not in ('Completely processed' , 'Tratado completamente')
						      THEN 'Yes'
									ELSE 'No'
						 END
		ELSE
				case
						when DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue ) < current_date
							and dp.mtomts = 'MTS'
							and sohs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
							and sois.OverallProcessingStatus   not in ('Completely processed' , 'Tratado completamente')
						then 'Yes'
						when DECODE( acgidmto.DateValue , '0001-01-01', cddemd.DateValue , acgidmto.DateValue ) < current_date
							and dp.mtomts = 'MTO'
							and sohs.OverallProcessStatusItem not in ('Completely processed' , 'Tratado completamente')
							and sois.OverallProcessingStatus   not in ('Completely processed' , 'Tratado completamente')
						then  'Yes'
					else 'No'
					end
END  = 'Yes'
	and bw.businesssector = 'BS-02'
group by dd_salesdocno, dd_salesitemno, dp.partnumber, pl.PLANTCODE, d.deliverypriority, CASE
	     WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue )
	     ELSE CASE WHEN dp.mtomts = 'MTS' THEN DECODE( esdemd.DateValue ,'0001-01-01', dd.DateValue , esdemd.DateValue )
		           ELSE DECODE( acgidmto.DateValue , '0001-01-01', cddemd.DateValue , acgidmto.DateValue )
		      END
 END
having sum(case
	         when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
			 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X'
			     then (case
				           when (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 then 0.0000
						   else (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
					   end)
			 when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000
			 else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
	      end) > 0;

drop table if exists tmp_sales_openqty;
create table tmp_sales_openqty
as
select
	row_number() over(order by dim_dateidrequested_emd asc, dd_salesdocno asc) proc_order
	,dd_salesdocno
	,dd_salesitemno
	,partnumber
	,PLANTCODE
	,openqty
from tmp_sales_openqty_1;

drop table if exists tmp_sales_openqty_cum;
create table tmp_sales_openqty_cum
as
select
	a.proc_order
	,a.dd_salesdocno
	,a.dd_salesitemno
	,a.partnumber
	,a.PLANTCODE
	,a.openqty
	,'Not Set' cvrd_by_plant
	,sum(a.openqty) over(partition by partnumber,PLANTCODE order by a.proc_order rows between unbounded preceding and current row) cum_openqty
from tmp_sales_openqty a
group by a.proc_order, a.dd_salesdocno, a.dd_salesitemno, a.partnumber, a.PLANTCODE, a.openqty;

drop table if exists tmp_uprestrictedstock;
create table tmp_uprestrictedstock
as
select
	dp.partnumber
	,pl.PLANTCODE
	,sum(f.ct_StockQty) ct_StockQty
from fact_inventoryaging f
	inner join dim_part dp on dp.dim_partid = f.dim_partid
	inner join dim_plant pl on f.dim_plantid = pl.dim_plantid
group by dp.partnumber, pl.PLANTCODE;

update tmp_sales_openqty_cum a
set a.cvrd_by_plant = ifnull('X', 'Not Set')
from tmp_sales_openqty_cum a, tmp_uprestrictedstock b
where a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and a.cum_openqty <= b.ct_StockQty;

/* check if the remaining stock can be allocated */

drop table if exists tmp_unalocatedstock;
create table tmp_unalocatedstock
as
select a.partnumber,a.PLANTCODE,max(b.ct_StockQty) - max(a.cum_openqty) remaining_stock
from tmp_sales_openqty_cum a
	inner join tmp_uprestrictedstock b on a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE
where a.cvrd_by_plant = 'X'
group by a.partnumber,a.PLANTCODE
having max(b.ct_StockQty) - max(a.cum_openqty) > 0;

/* add materials that did not manage to cover any order but have stock */

drop table if exists tmp_materialsnotcovered;
create table tmp_materialsnotcovered
as
select distinct a.partnumber,a.PLANTCODE from tmp_sales_openqty_cum a where not exists (select 1 from tmp_sales_openqty_cum b where a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE and b.cvrd_by_plant = 'X');

insert into tmp_unalocatedstock(partnumber, PLANTCODE, remaining_stock)
select partnumber, PLANTCODE, ct_StockQty from tmp_uprestrictedstock where (partnumber,PLANTCODE) in (select partnumber,PLANTCODE from tmp_materialsnotcovered) and ct_StockQty > 0;

drop table if exists tmp_sowithunalocatedstock;
create table tmp_sowithunalocatedstock
as
select a.*
from tmp_sales_openqty_cum a
	inner join tmp_unalocatedstock b on a.partnumber = b.partnumber and a.PLANTCODE = b.PLANTCODE
where a.openqty <= b.remaining_stock
	and a.cvrd_by_plant = 'Not Set';

execute script emd_covered_by_plant('tmp_sowithunalocatedstock','tmp_unalocatedstock','tmp_sales_openqty_cum');

/* end check if the remaining stock can be allocated */

update fact_salesorder f
set dd_openqtycvrdbyplant = 'Not Set'
where dd_openqtycvrdbyplant <> 'Not Set';

update fact_salesorder f
set f.dd_openqtycvrdbyplant = ifnull(trim(t.cvrd_by_plant), 'Not Set')
from fact_salesorder f, tmp_sales_openqty_cum t
where f.dd_salesdocno = t.dd_salesdocno and f.dd_salesitemno = t.dd_salesitemno
	and f.dd_openqtycvrdbyplant <> ifnull(trim(t.cvrd_by_plant), 'Not Set');

update fact_salesorder f
	set f.dd_openqtycvrdbyplant = ifnull('X', 'Not Set')
from fact_salesorder f
	inner join dim_salesorderitemstatus sois on f.Dim_SalesOrderItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.OverallDeliveryStatus not in (select distinct FILTER_VALUE from csv_ace_backorder_filters where TECHNICAL = 'VBUP-LFGSA' and PLATFORMS like '%P16%');

/*Add dd_openqtycvrdbyplant as dimension - Roxana D - 2017-10-26*/

update fact_salesorder f
set F.DIM_BLOCKEDBACKORDERBYPLANTID =  IFNULL(D.DIM_BLOCKEDBACKORDERBYPLANTID,1)
from fact_salesorder f, DIM_BLOCKEDBACKORDERBYPLANT D
WHERE F.dd_openqtycvrdbyplant = D.BACKORDERVALUE
     AND F.DIM_BLOCKEDBACKORDERBYPLANTID <>  IFNULL(D.DIM_BLOCKEDBACKORDERBYPLANTID,1);

/*End 2017-10-26*/



/* Roxana - 10 Jan 2017 Merck PM - calculate coverage percent */

drop table if exists tmp_pm_sales_openqty_1;
create table tmp_pm_sales_openqty_1
as
select
	f_so.dd_salesdocno
	,f_so.dd_salesitemno
	,dp.partnumber
	,pl.PLANTCODE
	,dd.datevalue dim_dateidsocreated
	,sum(case
	         when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
			 when sdt.Description IN ('Cognmt. Fill-up 7102', 'Consignmnt FillupMDA', 'Consignment Fill-up', 'MERCK Standard Order',
				'Pigment Order 6401', 'Standard Order', 'Standard Order 6201', 'Standard Order 6401', 'Standard Order 7101',
				'Standard Order 7102', 'Standard Order MAT', 'Standard Order MKOR', 'Standard Order-MDM', 'Standard Order-MESC',
				'Standard Order-MHK', 'Standard Order_MDA', 'Bonded 7101', 'Envío a consigna', 'Pedido estándar')
			and ic.Description NOT IN ('Third Party Item', '3rd Party Item w/ GR', 'ThirdParty Serv WIP', '3rdParty2Consignment', 'Third Party Item-MES',
				'3rd FOC W/O Cost', '3rd FOC with Cost', '3rd Party Item 71', '3rd Party Item w/GR', '3rd Party w/ GR FOC', 'FOC- Third party-MHK',
				'Third Party Item FOC', 'Third Party Item-MHK' , 'Third Party Item_MDA')
			and c.TradingPartner = 'Not Set' then
					(case when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000
						else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) end)
		end) pm_openqty
from fact_salesorder f_so
	inner join dim_part dp on f_so.dim_partid = dp.dim_partid
	inner join dim_date dd on f_so.dim_dateidsocreated = dd.dim_dateid
	inner join dim_salesorderheaderstatus sohs on f_so.dim_salesorderheaderstatusid = sohs.dim_salesorderheaderstatusid
	inner join dim_salesorderitemstatus sois on f_so.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
	inner join dim_date_factory_calendar esdemd on f_so.dim_dateidexpectedship_emd = esdemd.dim_dateid
	inner join dim_bwproducthierarchy bw on f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
	inner join dim_plant pl on f_so.dim_plantid = pl.dim_plantid
	inner join dim_salesorderrejectreason sorr on f_so.dim_salesorderrejectreasonid = sorr.dim_salesorderrejectreasonid
	inner join dim_salesdocumenttype sdt on f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
	inner join dim_salesorderitemcategory ic on f_so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
	inner join dim_customer c on f_so.dim_customerid = c.dim_customerid
where
	(case when TO_DATE ( esdemd.DateValue ,'DD MON YYYY') = '0001-01-01' then 'No Requested Date'
when sois.OverallProcessingStatus <> 'Completely processed'
and sohs.OverallProcessStatusItem <> 'Completely processed'
and sorr.Description = 'Not Set'
and TO_DATE( esdemd.DateValue ,'DD MON YYYY') > current_date +29
then 'Future Order'
when sois.OverallProcessingStatus <> 'Completely processed'
and sohs.OverallProcessStatusItem <> 'Completely processed'
and sorr.Description = 'Not Set'
and TO_DATE( esdemd.DateValue,'DD MON YYYY') <= current_date +29
and TO_DATE( esdemd.DateValue,'DD MON YYYY') >= current_date
then 'Open Order'
when sois.OverallProcessingStatus <> 'Completely processed'
and sohs.OverallProcessStatusItem <> 'Completely processed'
and sorr.Description = 'Not Set'
and TO_DATE( esdemd.DateValue,'DD MON YYYY') < current_date
then 'Backorder'
when sois.OverallProcessingStatus = 'Completely processed'
or sohs.OverallProcessStatusItem = 'Tratado completamente'
or sois.OverallDeliveryStatus = 'Completely processed'
or sois.OverallDeliveryStatus = 'Tratado completamente'
then 'Closed Order'
else 'Open Rejected'
end) = 'Backorder'
group by dd_salesdocno, dd_salesitemno, dp.partnumber, pl.PLANTCODE, dd.datevalue
having sum(case
	         when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
			 when sdt.Description IN ('Cognmt. Fill-up 7102', 'Consignmnt FillupMDA', 'Consignment Fill-up', 'MERCK Standard Order',
				'Pigment Order 6401', 'Standard Order', 'Standard Order 6201', 'Standard Order 6401', 'Standard Order 7101',
				'Standard Order 7102', 'Standard Order MAT', 'Standard Order MKOR', 'Standard Order-MDM', 'Standard Order-MESC',
				'Standard Order-MHK', 'Standard Order_MDA', 'Bonded 7101', 'Envío a consigna', 'Pedido estándar')
				and ic.Description NOT IN ('Third Party Item', '3rd Party Item w/ GR', 'ThirdParty Serv WIP', '3rdParty2Consignment', 'Third Party Item-MES',
				'3rd FOC W/O Cost', '3rd FOC with Cost', '3rd Party Item 71', '3rd Party Item w/GR', '3rd Party w/ GR FOC', 'FOC- Third party-MHK',
				'Third Party Item FOC', 'Third Party Item-MHK' , 'Third Party Item_MDA') then
					(case when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000
						else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) end)
	      end) > 0;


drop table if exists tmp_qtycoveragepercent;
create table tmp_qtycoveragepercent as
select a.partnumber
	,a.PLANTCODE
	,sum(pm_openqty) as openqtysum
	,ct_stockqty
from tmp_pm_sales_openqty_1 a, tmp_uprestrictedstock b
where a.partnumber = b.partnumber
and a.PLANTCODE = b.PLANTCODE
group by a.partnumber,a.PLANTCODE,ct_stockqty;


UPDATE fact_salesorder
SET ct_coveragepercent = ifnull(CASE
	WHEN (openqtysum * ct_baseuomratiokg * ct_baseuomratio) = 0 OR (openqtysum * ct_baseuomratiokg) = 0 THEN 0
    WHEN unitofmeasure <> 'KG' THEN a.ct_stockqty * ct_baseuomratiokg * ct_baseuomratio /
      CASE
        WHEN uom <> 'KG' THEN (openqtysum * ct_baseuomratiokg * ct_baseuomratio)
        ELSE openqtysum * ct_baseuomratiokg
      END
    ELSE (a.ct_stockqty * ct_baseuomratiokg) /
      CASE
        WHEN uom <> 'KG' THEN (openqtysum * ct_baseuomratiokg * ct_baseuomratio)
        ELSE openqtysum * ct_baseuomratiokg
      END
  END * 100,0)
FROM fact_salesorder f, tmp_qtycoveragepercent a, dim_part p,dim_plant pl, dim_unitofmeasure uom
WHERE f.dim_partid = p.dim_partid
AND p.partnumber = a.partnumber
AND a.PLANTCODE = pl.PLANTCODE
AND f.dim_plantid = pl.dim_plantid
AND f.Dim_SalesUoMid = uom.dim_unitofmeasureid;

/* End of Calculate coverage percent for Merck PM */

drop table if exists tmp_sales_openqty_1;
drop table if exists tmp_sales_openqty;
drop table if exists tmp_sales_openqty_cum;
drop table if exists tmp_unalocatedstock;
drop table if exists tmp_sowithunalocatedstock;
drop table if exists tmp_materialsnotcovered;
drop table if exists tmp_pm_sales_openqty_1;
drop table if exists tmp_qtycoveragepercent;

/* END EMD Merck LS Backorder covered by Plant */

/* 6 Feb 2016 Roxana - 	BI-4683 - add new id for Sales Order in order to populate shipment type */

drop table if exists  tmp_itemstatus;
create table tmp_itemstatus as
select distinct dd_SalesDocNo, dd_SalesItemNo, max(dim_deliveryitemstatusid) dim_deliveryitemstatusid from fact_salesorderdelivery
group by dd_SalesDocNo,dd_SalesItemNo;

MERGE INTO fact_salesorder so
USING tmp_itemstatus sod ON so.dd_SalesDocNo = sod.dd_SalesDocNo and so.dd_SalesItemNo = sod.dd_SalesItemNo
WHEN MATCHED THEN UPDATE SET so.dim_deliveryitemstatusid = ifnull(sod.dim_deliveryitemstatusid, 1)
WHERE so.dim_deliveryitemstatusid <> ifnull(sod.dim_deliveryitemstatusid, 1);


/* 8 Feb 2016 Roxana - Add Total Weight field */

UPDATE fact_salesorderdelivery sod
SET ct_totalweight = ifnull(LIKP_BTGEW,0)
FROM fact_salesorderdelivery sod, LIKP_LIPS a
WHERE dd_SalesDlvrDocNo = LIKP_VBELN
	and dd_SalesDlvrItemNo = LIPS_POSNR
	and ct_totalweight <> ifnull(LIKP_BTGEW,0);


/* 10 Feb 2016 Roxana - Add Sales UOM */

drop table if exists tmp_salesuom;
create table tmp_salesuom as select distinct dd_SalesDocNo,dd_SalesItemNo,max(Dim_SalesUoMid) Dim_SalesUoMid
from fact_salesorder
group by dd_SalesDocNo,dd_SalesItemNo;

UPDATE fact_salesorderdelivery sod
SET sod.Dim_SalesUoMid  = ifnull(so.Dim_SalesUoMid, 1)
FROM tmp_salesuom so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
	AND sod.dd_SalesItemNo = so.dd_SalesItemNo;

/* Marius HC SAP Forecast */

update fact_salesorder f set ct_merckhc_sapforecast = 0 where ct_merckhc_sapforecast <> 0;

drop table if exists tmp_sapfcst;
create table tmp_sapfcst
as
select PBIM_MATNR,PBIM_WERKS,month(PBED_PDATU) PBED_PDATU,SUM(PBED_PLNMG+PBED_ENTMG) PBED_PLNMG
from PBED_PBIM
where PBIM_BEDAE = 'VSF' and PBIM_VERSB = '00' and year(PBED_PDATU) = year(current_date)
group by PBIM_MATNR,PBIM_WERKS,month(PBED_PDATU);

update fact_salesorder f
set f.ct_merckhc_sapforecast = ifnull(t.PBED_PLNMG, 0)
from fact_salesorder f
	inner join dim_part dp on f.dim_partid = dp.dim_partid
	inner join dim_plant pl on f.dim_plantid = pl.dim_plantid
	inner join dim_date agi on f.dim_dateidactualgi = agi.dim_dateid
	inner join tmp_sapfcst t on dp.partnumber = t.PBIM_MATNR and pl.plantcode = t.PBIM_WERKS and agi.calendarmonthnumber = t.PBED_PDATU
where agi.CalendarYear = year(current_date) and f.ct_merckhc_sapforecast <> ifnull(t.PBED_PLNMG, 0);

/*BI-5275 - Added by Victor 21 Jan 2017 Forecast Parts Logic for Sales Order*/

UPDATE fact_salesorder f_so SET f_so.dd_forecastorders_hc = 'Not Set';

-- Marking the Forecasted Orders
UPDATE fact_salesorder f_so
   SET f_so.dd_forecastorders_hc = ifnull('X', 'Not Set')
    FROM fact_salesorder f_so
        ,dim_salesdocumenttype dt
        ,dim_documentcategory dc
        ,dim_distributionchannel dch
        ,dim_mdg_part dmg
   WHERE f_so.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
     AND f_so.dim_documentcategoryid = dc.dim_documentcategoryid
     AND f_so.dim_distributionchannelid = dch.dim_distributionchannelid
     AND f_so.dim_mdg_partid = dmg.dim_mdg_partid
     AND dch.DistributionChannelCode IN (11,12,13,14,15,19,20)
     AND dc.DocumentCategory = 'C'
     AND dt.DocumentType IN ('YTA')
     AND dmg.mattypeforglbrep IN ('FIN','MER')
     AND f_so.dd_forecastorders_hc <> ifnull('X', 'Not Set');



-- Adding the AVGUnitPrice for forecasted parts
UPDATE fact_salesorder f_so SET f_so.ct_avgprice = 0 WHERE f_so.ct_avgprice <> 0;

/*
UPDATE fact_salesorder f_so
SET f_so.ct_avgprice = IFNULL(tmp.opunitprice_euro,0)
  FROM fact_salesorder f_so
      ,emdtempocc4.UPLOAD_CVS_AVGPRICEOP2017 tmp
      ,dim_part dp
      ,dim_plant dl
      ,dim_date acgi
 WHERE f_so.dim_partid = dp.dim_partid
   AND f_so.dim_plantid = dl.dim_plantid
   AND f_so.dim_dateidactualgi = acgi.dim_dateid
   AND tmp.part_number = dp.partnumber
   AND tmp.plant_code = dl.plantcode
   AND TO_CHAR(acgi.datevalue,'MMYYYY') = REPLACE(tmp.forecast_date,'-','')
   AND REPLACE(tmp.forecast_date,'-','') = TO_CHAR(CURRENT_DATE,'MMYYYY')
   AND f_so.dd_forecastorders_hc = 'X'
   AND f_so.ct_avgprice <> IFNULL(tmp.opunitprice_euro,0)*/

--- INSERT DUMMY ROWS FOR FORECASTED PARTS

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorder';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_salesorder',IFNULL(MAX(fact_salesorderid),0)
FROM fact_salesorder;

DELETE FROM fact_salesorder WHERE dd_dummyrow ='X';

	 INSERT INTO fact_salesorder
	     (
			FACT_SALESORDERID
			,DD_SALESDOCNO
			,DD_SALESITEMNO
			,DD_SCHEDULENO
			,CT_SCHEDULEQTYSALESUNIT
			,CT_CONFIRMEDQTY
			,CT_CORRECTEDQTY
			,CT_DELIVEREDQTY
			,CT_ONHANDCOVEREDQTY
			,AMT_UNITPRICE
			,CT_PRICEUNIT
			,AMT_SCHEDULETOTAL
			,AMT_STDCOST
			,AMT_TARGETVALUE
			,CT_TARGETQTY
			,AMT_EXCHANGERATE
			,CT_OVERDLVRTOLERANCE
			,CT_UNDERDLVRTOLERANCE
			,DIM_DATEIDSALESORDERCREATED
			,DIM_DATEIDSCHEDDELIVERYREQ
			,DIM_DATEIDSCHEDDLVRREQPREV
			,DIM_DATEIDSCHEDDELIVERY
			,DIM_DATEIDGOODSISSUE
			,DIM_DATEIDMTRLAVAIL
			,DIM_DATEIDLOADING
			,DIM_DATEIDTRANSPORT
			,DIM_CURRENCYID
			,DIM_PRODUCTHIERARCHYID
			,DIM_PLANTID
			,DIM_COMPANYID
			,DIM_STORAGELOCATIONID
			,DIM_SALESDIVISIONID
			,DIM_SHIPRECEIVEPOINTID
			,DIM_DOCUMENTCATEGORYID
			,DIM_SALESDOCUMENTTYPEID
			,DIM_SALESORGID
			,DIM_CUSTOMERID
			,DIM_DATEIDVALIDFROM
			,DIM_DATEIDVALIDTO
			,DIM_SALESGROUPID
			,DIM_COSTCENTERID
			,DIM_CONTROLLINGAREAID
			,DIM_BILLINGBLOCKID
			,DIM_TRANSACTIONGROUPID
			,DIM_SALESORDERREJECTREASONID
			,DIM_PARTID
			,DIM_PARTSALESID
			,DIM_SALESORDERHEADERSTATUSID
			,DIM_SALESORDERITEMSTATUSID
			,DIM_CUSTOMERGROUP1ID
			,DIM_CUSTOMERGROUP2ID
			,DIM_SALESORDERITEMCATEGORYID
			,AMT_EXCHANGERATE_GBL
			,CT_FILLQTY
			,CT_ONHANDQTY
			,DIM_SCHEDULELINECATEGORYID
			,DIM_SALESMISCID
			,DD_ITEMRELFORDELV
			,DIM_DATEIDSHIPMENTDELIVERY
			,DIM_DATEIDACTUALGI
			,DIM_DATEIDSHIPDLVRFILL
			,DIM_DATEIDACTUALGIFILL
			,DIM_PROFITCENTERID
			,DIM_CUSTOMERIDSHIPTO
			,DIM_UNITOFMEASUREID
			,DIM_DISTRIBUTIONCHANNELID
			,DD_BATCHNO
			,DD_CREATEDBY
			,DIM_DATEIDLOADINGFILL
			,DIM_PAYERPARTNERFUNCTIONID
			,DIM_BILLTOPARTYPARTNERFUNCTIONID
			,CT_SHIPPEDAGNSTORDERQTY
			,CT_CMLQTYRECEIVED
			,DIM_CUSTOMERGROUPID
			,DIM_SALESOFFICEID
			,CT_CUMORDERQTY
			,DIM_CREDITREPRESENTATIVEID
			,DD_CUSTOMERPONO
			,DIM_DELIVERYBLOCKID
			,DIM_MATERIALPRICEGROUP1ID
			,DIM_DATEIDDLVRDOCCREATED
			,AMT_CUSTOMEREXPECTEDPRICE
			,DIM_OVERALLSTATUSCREDITCHECKID
			,DIM_SALESDISTRICTID
			,DIM_ACCOUNTASSIGNMENTGROUPID
			,DIM_MATERIALGROUPID
			,DIM_SALESDOCORDERREASONID
			,AMT_SUBTOTAL3
			,AMT_SUBTOTAL4
			,CT_AFSOPENQTY
			,DIM_PURCHASEORDERTYPEID
			,DIM_DATEIDQUOTATIONVALIDFROM
			,DIM_DATEIDPURCHASEORDER
			,DIM_DATEIDQUOTATIONVALIDTO
			,DIM_DATEIDSOCREATED
			,DIM_DATEIDSODOCUMENT
			,DD_SUBSEQUENTDOCNO
			,DD_SUBSDOCITEMNO
			,DD_SUBSSCHEDULENO
			,DIM_SUBSDOCCATEGORYID
			,CT_AFSTOTALDRAWN
			,DD_REFERENCEDOCUMENTNO
			,DIM_DATEIDNEXTDATE
			,AMT_TAX
			,DD_BUSINESSCUSTOMERPONO
			,DIM_ROUTEID
			,DIM_BILLINGDATEID
			,DIM_CUSTOMERPAYMENTTERMSID
			,DIM_SALESRISKCATEGORYID
			,CT_FILLQTY_CRD
			,DIM_DATEIDSOITEMCHANGEDON
			,DD_CUSTOMERMATERIALNO
			,DIM_BASEUOMID
			,DIM_SALESUOMID
			,AMT_UNITPRICEUOM
			,DIM_MATERIALPRICEGROUP4ID
			,DIM_MATERIALPRICEGROUP5ID
			,AMT_SUBTOTAL3_ORDERQTY
			,AMT_SUBTOTAL3INCUSTCONFIG_BILLING
			,DIM_CUSTOMERMASTERSALESID
			,DD_SALESORDERBLOCKED
			,DD_SOCREATETIME
			,DD_REQDELIVERYTIME
			,DD_SOLINECREATETIME
			,DD_DELIVERYTIME
			,DD_PLANNEDGITIME
			,DIM_CURRENCYID_TRA
			,DIM_CURRENCYID_GBL
			,DIM_CURRENCYID_STAT
			,AMT_EXCHANGERATE_STAT
			,DIM_DELIVERYPRIORITYID
			,DD_DOCUMENTCONDITIONNO
			,AMT_SUBTOTAL1
			,AMT_SUBTOTAL2
			,AMT_SUBTOTAL5
			,AMT_SUBTOTAL6
			,DD_HIGHLEVELITEM
			,DD_PRODORDERNO
			,DD_PRODORDERITEMNO
			,DIM_CUSTOMERCONDITIONGROUPS1ID
			,DIM_CUSTOMERCONDITIONGROUPS2ID
			,DIM_CUSTOMERCONDITIONGROUPS3ID
			,DIM_SCHEDULEDELIVERYBLOCKID
			,DIM_CUSTOMERGROUP4ID
			,DD_OPENCONFIRMATIONSEXISTS
			,DD_CONDITIONNO
			,DD_CLEAREDBLOCKEDSTS
			,CT_AFSINRDDUNITS
			,DIM_AGREEMENTSID
			,DD_RO_MATERIALDESC
			,DIM_MATERIALPRICINGGROUPID
			,DD_DELIVERYINDICATOR
			,DIM_CUSTOMERCONDITIONGROUPS4ID
			,DIM_CUSTOMERCONDITIONGROUPS5ID
			,DD_PURCHASEORDERITEM
			,DIM_MATERIALPRICEGROUP2ID
			,DW_INSERT_DATE
			,DW_UPDATE_DATE
			,DIM_PROJECTSOURCEID
			,DD_BILLING_NO
			,DIM_MDG_PARTID
			,DIM_BWPRODUCTHIERARCHYID
			,CT_FIRSTNONZEROCONFIRMQTY
			,CT_VOLUME
			,CT_NOTCONFIRMEDQTY
			,DD_CUSTOMCUSTEXPPRICEINC
			,DD_DOCUMENTFLOWGROUP
			,DD_ORDERBY
			,DD_CUSTOMPRICEMISSINGINC
			,DIM_CUSTOMERENDUSERFORFTRADE
			,DIM_DATEIDREQUESTED_EMD
			,DIM_DATEIDEXPECTEDSHIP_EMD
			,DIM_DATEIDCONFIRMEDDELIVERY_EMD
			,DIM_ACCORDINGGIDATEMTO_EMD
			,CT_BASEUOMRATIO
			,DD_MERCKLSBACKORDERFILTER
			,DD_OPENQTYCVRDBYMAT
			,AMT_EXCHANGERATE_CUSTOM
			,DD_INTERCOMPFLAG
			,DD_LSINTERCOMPFLAG
			,DIM_COMMERCIALVIEWID
			,CT_BASEUOMRATIOKG
			,DIM_CLUSTERID
			,DD_OPENQTYCVRDBYPLANT
			,DD_BACKORDERREASON
			,DD_BACKORDER_RESPONSIBILITY
			,DD_ACE_OPENORDERS
			,DD_DELIVERYONTIME
			,DD_DELIVERYISFULL
			,CT_DELIVERYONTIME
			,CT_DELIVERYISFULL
			,DD_BACKORDERFLAG
			,DIM_DATEIDACTUALGIMERCKLS
			,CT_COUNTSALESDOCITEM
			,DD_DELIVERYONTIMECASUAL
			,CT_DELIVERYONTIMECASUAL
			,DD_ORA_ACTIVE_HOLD_INFO
			,DD_ORA_ATTRIBUTE11
			,DD_ORA_TRADSALES_FLAG
			,DD_ORA_LINE_ON_HOLD
			,DIM_BACKORDER_DATEID
			,DIM_PRODUCTIONSCHEDULERID
			,DIM_DATEIDACCORDINGMA_EMD
			,CT_SCHEDULEQTYBASEUOM
			,DD_MOVEMENTTYPE
			,DIM_CUSTOMER_SHIPTO
			,CT_NRDELIVEREDSALEORD
			,CT_NROPENNOTDELIVERED
			,CT_BCKORDERLINESREQ
			,CT_BCKORDERLINESCONF
			,CT_DELIVEREDLINESREQ
			,CT_DELIVEREDLINESCONF
			,CT_ACTUALQUANTITYDELIVERED
			,CT_ACTUALQTYDELIVEREDREQ
			,CT_ACTUALQTYDELIVEREDCONF
			,CT_COMPLETEDELIVEREDORDERS
			,CT_GROSSWEIGHTITEM
			,CT_NETWEIGHTITEM
			,DD_WEIGHTUNIT
			,DD_DEVGIDTVSCONFDELDT
			,DD_DEVGIDTVSREQDT
			,DD_DEVGIDTVSACCGICONFDT
			,DD_DEVGIDTVSACCGIDT
			,DD_DELSERAGCONFDELDT
			,DD_DELSERAGREQDELDT
			,DD_DELSERAGACCGIDT
			,DIM_COUNTRYHIERPSID
			,DIM_COUNTRYHIERARID
			,DD_SALESDLVRDOCNO
			,CT_DELSERAGACCGIDT
			,CT_DELSERAGREQDELDT
			,CT_DELSERAGCONFDELDT
			,CT_DEVGIDTVSACCGICONFDT
			,CT_DEVGIDTVSREQDT
			,CT_DEVGIDTVSCONFDELDT
			,DD_PMDELIVERYSERVICEFLAG
			,DD_MRKHC_SOATRISK
			,CT_ONHANDQTYINV
			,DIM_DATE_ENDDATELOADINGID
			,DD_GSA_CODE
			,DD_SHIP_TO_PARTY
			,DIM_GSAMAPIMPORTID
			,CT_TOTALRESTRICTEDSTOCK
			,CT_STOCKQTY
			,CT_STOCKINTRANSFER
			,CT_ONHANDWIPQTY
			,CT_ONHAND
			,CT_STOCKINTRANSIT
			,CT_WIPQTY
			,DD_FILLRATESNAPSHOT_FLAG
			,CT_DEVEXPECT
			,CT_DELSEXPECTDT
			,DIM_ORIGINAL_DATEIDEXPECTEDSHIP_EMD
			,DIM_ORIGINAL_ACCORDINGGIDATEMTO_EMD
			,CT_COUNT_ACCORDINGGIDATEMTO_EMD
			,CT_COUNT_DATEIDEXPECTEDSHIP_EMD
			,DIM_SHIPMENTID
			,DIM_ADDRESSID
			,CT_COVERAGEPERCENT
			,DIM_SALESDOCTYPEDROPSHIPID
			,DD_INCO1
			,DD_INCO2
			,CT_MERCKHC_SAPFORECAST
			,CT_AVGPRICE
			,DD_FORECASTORDERS_HC
			,DD_DUMMYROW
	     )
	     SELECT
	     (SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_salesorder' ) + ROW_NUMBER() OVER(order by '') AS FACT_SALESORDERID
	     ,CONCAT('DY-', ROW_NUMBER() OVER(order by '')) AS DD_SALESDOCNO
	     ,0 AS DD_SALESITEMNO
	     ,0 AS DD_SCHEDULENO
	     ,0 AS CT_SCHEDULEQTYSALESUNIT
	     ,0 AS CT_CONFIRMEDQTY
	     ,0 AS CT_CORRECTEDQTY
	     ,0 AS CT_DELIVEREDQTY
	     ,0 AS CT_ONHANDCOVEREDQTY
	     ,0 AS AMT_UNITPRICE
	     ,1 AS CT_PRICEUNIT
	     ,0 AS AMT_SCHEDULETOTAL
	     ,0 AS AMT_STDCOST
	     ,0 AS AMT_TARGETVALUE
	     ,0 AS CT_TARGETQTY
	     ,1 AS AMT_EXCHANGERATE
	     ,0 AS CT_OVERDLVRTOLERANCE
	     ,0 AS CT_UNDERDLVRTOLERANCE
	     ,1 AS DIM_DATEIDSALESORDERCREATED
	     ,1 AS DIM_DATEIDSCHEDDELIVERYREQ
	     ,1 AS DIM_DATEIDSCHEDDLVRREQPREV
	     ,1 AS DIM_DATEIDSCHEDDELIVERY
	     ,1 AS DIM_DATEIDGOODSISSUE
	     ,1 AS DIM_DATEIDMTRLAVAIL
	     ,1 AS DIM_DATEIDLOADING
	     ,1 AS DIM_DATEIDTRANSPORT
	     ,1 AS DIM_CURRENCYID
	     ,1 AS DIM_PRODUCTHIERARCHYID
	     ,IFNULL(dl.dim_plantid,1) DIM_PLANTID
	     ,1 AS DIM_COMPANYID
	     ,1 AS DIM_STORAGELOCATIONID
	     ,1 AS DIM_SALESDIVISIONID
	     ,1 AS DIM_SHIPRECEIVEPOINTID
	     ,1 AS DIM_DOCUMENTCATEGORYID
	     ,1 AS DIM_SALESDOCUMENTTYPEID
	     ,1 AS DIM_SALESORGID
	     ,1 AS DIM_CUSTOMERID
	     ,1 AS DIM_DATEIDVALIDFROM
	     ,1 AS DIM_DATEIDVALIDTO
	     ,0 AS DIM_SALESGROUPID
	     ,1 AS DIM_COSTCENTERID
	     ,0 AS DIM_CONTROLLINGAREAID
	     ,0 AS DIM_BILLINGBLOCKID
	     ,0 AS DIM_TRANSACTIONGROUPID
	     ,0 AS DIM_SALESORDERREJECTREASONID
	     ,IFNULL(dp.dim_partid,1) DIM_PARTID
	     ,1 AS DIM_PARTSALESID
	     ,0 AS DIM_SALESORDERHEADERSTATUSID
	     ,0 AS DIM_SALESORDERITEMSTATUSID
	     ,0 AS DIM_CUSTOMERGROUP1ID
	     ,0 AS DIM_CUSTOMERGROUP2ID
	     ,1 AS DIM_SALESORDERITEMCATEGORYID
	     ,1 AS AMT_EXCHANGERATE_GBL
	     ,0 AS CT_FILLQTY
	     ,0 AS CT_ONHANDQTY
	     ,1 AS DIM_SCHEDULELINECATEGORYID
	     ,1 AS DIM_SALESMISCID
	     ,'Not Set' AS DD_ITEMRELFORDELV
	     ,1 AS DIM_DATEIDSHIPMENTDELIVERY
	     ,IFNULL(acgi.dim_dateid,1) DIM_DATEIDACTUALGI
	     ,1 AS DIM_DATEIDSHIPDLVRFILL
	     ,1 AS DIM_DATEIDACTUALGIFILL
	     ,1 AS DIM_PROFITCENTERID
	     ,1 AS DIM_CUSTOMERIDSHIPTO
	     ,1 AS DIM_UNITOFMEASUREID
	     ,1 AS DIM_DISTRIBUTIONCHANNELID
	     ,'Not Set' AS DD_BATCHNO
	     ,'Not Set' AS DD_CREATEDBY
	     ,1 AS DIM_DATEIDLOADINGFILL
	     ,1 AS DIM_PAYERPARTNERFUNCTIONID
	     ,1 AS DIM_BILLTOPARTYPARTNERFUNCTIONID
	     ,0 AS CT_SHIPPEDAGNSTORDERQTY
	     ,0 AS CT_CMLQTYRECEIVED
	     ,1 AS DIM_CUSTOMERGROUPID
	     ,1 AS DIM_SALESOFFICEID
	     ,0 AS CT_CUMORDERQTY
	     ,1 AS DIM_CREDITREPRESENTATIVEID
	     ,'Not Set' AS DD_CUSTOMERPONO
	     ,1 AS DIM_DELIVERYBLOCKID
	     ,1 AS DIM_MATERIALPRICEGROUP1ID
	     ,1 AS DIM_DATEIDDLVRDOCCREATED
	     ,0 AS AMT_CUSTOMEREXPECTEDPRICE
	     ,1 AS DIM_OVERALLSTATUSCREDITCHECKID
	     ,1 AS DIM_SALESDISTRICTID
	     ,1 AS DIM_ACCOUNTASSIGNMENTGROUPID
	     ,1 AS DIM_MATERIALGROUPID
	     ,1 AS DIM_SALESDOCORDERREASONID
	     ,0 AS AMT_SUBTOTAL3
	     ,0 AS AMT_SUBTOTAL4
	     ,0 AS CT_AFSOPENQTY
	     ,1 AS DIM_PURCHASEORDERTYPEID
	     ,1 AS DIM_DATEIDQUOTATIONVALIDFROM
	     ,1 AS DIM_DATEIDPURCHASEORDER
	     ,1 AS DIM_DATEIDQUOTATIONVALIDTO
	     ,1 AS DIM_DATEIDSOCREATED
	     ,1 AS DIM_DATEIDSODOCUMENT
	     ,'Not Set' AS DD_SUBSEQUENTDOCNO
	     ,0 AS DD_SUBSDOCITEMNO
	     ,0 AS DD_SUBSSCHEDULENO
	     ,1 AS DIM_SUBSDOCCATEGORYID
	     ,0 AS CT_AFSTOTALDRAWN
	     ,'Not Set' AS DD_REFERENCEDOCUMENTNO
	     ,1 AS DIM_DATEIDNEXTDATE
	     ,0 AS AMT_TAX
	     ,'Not Set' AS DD_BUSINESSCUSTOMERPONO
	     ,1 AS DIM_ROUTEID
	     ,1 AS DIM_BILLINGDATEID
	     ,1 AS DIM_CUSTOMERPAYMENTTERMSID
	     ,1 AS DIM_SALESRISKCATEGORYID
	     ,0 AS CT_FILLQTY_CRD
	     ,1 AS DIM_DATEIDSOITEMCHANGEDON
	     ,'Not Set' AS DD_CUSTOMERMATERIALNO
	     ,1 AS DIM_BASEUOMID
	     ,1 AS DIM_SALESUOMID
	     ,0 AS AMT_UNITPRICEUOM
	     ,1 AS DIM_MATERIALPRICEGROUP4ID
	     ,1 AS DIM_MATERIALPRICEGROUP5ID
	     ,0 AS AMT_SUBTOTAL3_ORDERQTY
	     ,0 AS AMT_SUBTOTAL3INCUSTCONFIG_BILLING
	     ,1 AS DIM_CUSTOMERMASTERSALESID
	     ,'Not Set' AS DD_SALESORDERBLOCKED
	     ,'Not Set' AS DD_SOCREATETIME
	     ,'Not Set' AS DD_REQDELIVERYTIME
	     ,'Not Set' AS DD_SOLINECREATETIME
	     ,'Not Set' AS DD_DELIVERYTIME
	     ,'Not Set' AS DD_PLANNEDGITIME
	     ,1 AS DIM_CURRENCYID_TRA
	     ,1 AS DIM_CURRENCYID_GBL
	     ,1 AS DIM_CURRENCYID_STAT
	     ,1 AS AMT_EXCHANGERATE_STAT
	     ,1 AS DIM_DELIVERYPRIORITYID
	     ,'Not Set' AS DD_DOCUMENTCONDITIONNO
	     ,0 AS AMT_SUBTOTAL1
	     ,0 AS AMT_SUBTOTAL2
	     ,0 AS AMT_SUBTOTAL5
	     ,0 AS AMT_SUBTOTAL6
	     ,0 AS DD_HIGHLEVELITEM
	     ,'Not Set' AS DD_PRODORDERNO
	     ,0 AS DD_PRODORDERITEMNO
	     ,1 AS DIM_CUSTOMERCONDITIONGROUPS1ID
	     ,1 AS DIM_CUSTOMERCONDITIONGROUPS2ID
	     ,1 AS DIM_CUSTOMERCONDITIONGROUPS3ID
	     ,1 AS DIM_SCHEDULEDELIVERYBLOCKID
	     ,1 AS DIM_CUSTOMERGROUP4ID
	     ,'Not Set' AS DD_OPENCONFIRMATIONSEXISTS
	     ,'Not Set' AS DD_CONDITIONNO
	     ,'Not Set' AS DD_CLEAREDBLOCKEDSTS
	     ,0 AS CT_AFSINRDDUNITS
	     ,1 AS DIM_AGREEMENTSID
	     ,'Not Set' AS DD_RO_MATERIALDESC
	     ,1 AS DIM_MATERIALPRICINGGROUPID
	     ,'Not Set' AS DD_DELIVERYINDICATOR
	     ,1 AS DIM_CUSTOMERCONDITIONGROUPS4ID
	     ,1 AS DIM_CUSTOMERCONDITIONGROUPS5ID
	     ,'0' AS DD_PURCHASEORDERITEM
	     ,1 AS DIM_MATERIALPRICEGROUP2ID
	     ,CURRENT_TIMESTAMP AS DW_INSERT_DATE
	     ,CURRENT_TIMESTAMP AS DW_UPDATE_DATE
	     ,ps.DIM_PROJECTSOURCEID AS DIM_PROJECTSOURCEID
	     ,'Not Set' AS DD_BILLING_NO
	     ,1 AS DIM_MDG_PARTID
	     ,IFNULL(bw.dim_bwproducthierarchyid,1) AS DIM_BWPRODUCTHIERARCHYID
	     ,0 AS CT_FIRSTNONZEROCONFIRMQTY
	     ,0 AS CT_VOLUME
	     ,0 AS CT_NOTCONFIRMEDQTY
	     ,'Not Set' AS DD_CUSTOMCUSTEXPPRICEINC
	     ,'Not Set' AS DD_DOCUMENTFLOWGROUP
	     ,'Not Set' AS DD_ORDERBY
	     ,'Not Set' AS DD_CUSTOMPRICEMISSINGINC
	     ,1 AS DIM_CUSTOMERENDUSERFORFTRADE
	     ,1 AS DIM_DATEIDREQUESTED_EMD
	     ,1 AS DIM_DATEIDEXPECTEDSHIP_EMD
	     ,1 AS DIM_DATEIDCONFIRMEDDELIVERY_EMD
	     ,1 AS DIM_ACCORDINGGIDATEMTO_EMD
	     ,0 AS CT_BASEUOMRATIO
	     ,'Not Set' AS DD_MERCKLSBACKORDERFILTER
	     ,'Not Set' AS DD_OPENQTYCVRDBYMAT
	     ,1 AS AMT_EXCHANGERATE_CUSTOM
	     ,'Not Set' AS DD_INTERCOMPFLAG
	     ,'Not Set' AS DD_LSINTERCOMPFLAG
	     ,1 AS DIM_COMMERCIALVIEWID
	     ,0 AS CT_BASEUOMRATIOKG
	     ,1 AS DIM_CLUSTERID
	     ,'Not Set' AS DD_OPENQTYCVRDBYPLANT
	     ,'Not Set' AS DD_BACKORDERREASON
	     ,'Not Set' AS DD_BACKORDER_RESPONSIBILITY
	     ,'Not Set' AS DD_ACE_OPENORDERS
	     ,'Not Set' AS DD_DELIVERYONTIME
	     ,'Not Set' AS DD_DELIVERYISFULL
	     ,0 AS CT_DELIVERYONTIME
	     ,0 AS CT_DELIVERYISFULL
	     ,'Not Set' AS DD_BACKORDERFLAG
	     ,1 AS DIM_DATEIDACTUALGIMERCKLS
	     ,0 AS CT_COUNTSALESDOCITEM
	     ,'Not Set' AS DD_DELIVERYONTIMECASUAL
	     ,0 AS CT_DELIVERYONTIMECASUAL
	     ,0 AS DD_ORA_ACTIVE_HOLD_INFO
	     ,0 AS DD_ORA_ATTRIBUTE11
	     ,0 AS DD_ORA_TRADSALES_FLAG
	     ,0 AS DD_ORA_LINE_ON_HOLD
	     ,0 AS DIM_BACKORDER_DATEID
	     ,1 AS DIM_PRODUCTIONSCHEDULERID
	     ,1 AS DIM_DATEIDACCORDINGMA_EMD
	     ,0 AS CT_SCHEDULEQTYBASEUOM
	     ,'Not Set' AS DD_MOVEMENTTYPE
	     ,1 AS DIM_CUSTOMER_SHIPTO
	     ,0 AS CT_NRDELIVEREDSALEORD
	     ,0 AS CT_NROPENNOTDELIVERED
	     ,0 AS CT_BCKORDERLINESREQ
	     ,0 AS CT_BCKORDERLINESCONF
	     ,0 AS CT_DELIVEREDLINESREQ
	     ,0 AS CT_DELIVEREDLINESCONF
	     ,0 AS CT_ACTUALQUANTITYDELIVERED
	     ,0 AS CT_ACTUALQTYDELIVEREDREQ
	     ,0 AS CT_ACTUALQTYDELIVEREDCONF
	     ,0 AS CT_COMPLETEDELIVEREDORDERS
	     ,0 AS CT_GROSSWEIGHTITEM
	     ,0 AS CT_NETWEIGHTITEM
	     ,'Not Set' AS DD_WEIGHTUNIT
	     ,'Not Set' AS DD_DEVGIDTVSCONFDELDT
	     ,'Not Set' AS DD_DEVGIDTVSREQDT
	     ,'Not Set' AS DD_DEVGIDTVSACCGICONFDT
	     ,'Not Set' AS DD_DEVGIDTVSACCGIDT
	     ,'Not set' AS DD_DELSERAGCONFDELDT
	     ,'Not set' AS DD_DELSERAGREQDELDT
	     ,'Not set' AS DD_DELSERAGACCGIDT
	     ,1 AS DIM_COUNTRYHIERPSID
	     ,1 AS DIM_COUNTRYHIERARID
	     ,'Not Set' AS DD_SALESDLVRDOCNO
	     ,0 AS CT_DELSERAGACCGIDT
	     ,0 AS CT_DELSERAGREQDELDT
	     ,0 AS CT_DELSERAGCONFDELDT
	     ,0 AS CT_DEVGIDTVSACCGICONFDT
	     ,0 AS CT_DEVGIDTVSREQDT
	     ,0 AS CT_DEVGIDTVSCONFDELDT
	     ,'Not Set' AS DD_PMDELIVERYSERVICEFLAG
	     ,'Not Set' AS DD_MRKHC_SOATRISK
	     ,0 AS CT_ONHANDQTYINV
	     ,1 AS DIM_DATE_ENDDATELOADINGID
	     ,'Not Set' AS DD_GSA_CODE
	     ,'Not Set' AS DD_SHIP_TO_PARTY
	     ,1 AS DIM_GSAMAPIMPORTID
	     ,0 AS CT_TOTALRESTRICTEDSTOCK
	     ,0 AS CT_STOCKQTY
	     ,0 AS CT_STOCKINTRANSFER
	     ,0 AS CT_ONHANDWIPQTY
	     ,0 AS CT_ONHAND
	     ,0 AS CT_STOCKINTRANSIT
	     ,0 AS CT_WIPQTY
	     ,'Not Set' AS DD_FILLRATESNAPSHOT_FLAG
	     ,0 AS CT_DEVEXPECT
	     ,0 AS CT_DELSEXPECTDT
	     ,1 AS DIM_ORIGINAL_DATEIDEXPECTEDSHIP_EMD
	     ,1 AS DIM_ORIGINAL_ACCORDINGGIDATEMTO_EMD
	     ,1 AS CT_COUNT_ACCORDINGGIDATEMTO_EMD
	     ,1 AS CT_COUNT_DATEIDEXPECTEDSHIP_EMD
	     ,1 AS DIM_SHIPMENTID
	     ,1 AS DIM_ADDRESSID
	     ,0 AS CT_COVERAGEPERCENT
	     ,1 AS DIM_SALESDOCTYPEDROPSHIPID
	     ,'Not Set' AS DD_INCO1
	     ,'Not Set' AS DD_INCO2
	     ,tmp.PBED_PLNMG AS CT_MERCKHC_SAPFORECAST
	     ,0 AS CT_AVGPRICE
	     ,'X' AS DD_FORECASTORDERS_HC
	     ,'X' AS DD_DUMMYROW
	     FROM
	           tmp_sapfcst tmp
	          ,dim_part dp
	          ,dim_plant dl
	          ,dim_date acgi
	          ,dim_bwproducthierarchy bw
	          ,dim_projectsource ps
	     WHERE
			 			  dp.partnumber = tmp.PBIM_MATNR
					AND dp.plant = tmp.PBIM_WERKS
					AND tmp.PBIM_WERKS = dl.plantcode
					AND acgi.CalendarYear = YEAR(current_date)
					AND acgi.calendarmonthnumber = tmp.PBED_PDATU
					AND acgi.datevalue = TRUNC(current_date,'MM')
					AND acgi.companycode = 'Not Set'
					AND dp.producthierarchy = bw.lowerhierarchycode
					AND dp.productgroupsbu = bw.upperhierarchycode
					AND TO_DATE('2017-12-28') BETWEEN bw.upperhierstartdate AND bw.upperhierenddate
					AND tmp.PBED_PLNMG <> 0
					AND (dp.partnumber,dl.plantcode) NOT IN
					(
					SELECT DISTINCT dp.partnumber, dl.plantcode
					FROM fact_salesorder f_so
					,dim_part dp
					,dim_plant dl
					,dim_date acgi
					WHERE f_so.dim_partid = dp.dim_partid
					AND f_so.dim_plantid = dl.dim_plantid
					AND f_so.dim_dateidactualgi = acgi.dim_dateid
					AND TRUNC(acgi.datevalue,'MM') = TRUNC(CURRENT_DATE,'MM')
	        AND DD_FORECASTORDERS_HC = 'X'

		);
/*
UPDATE fact_salesorder f_so
SET f_so.ct_avgprice = IFNULL(tmp.opunitprice_euro,0)
  FROM fact_salesorder f_so
      ,emdtempocc4.UPLOAD_CVS_AVGPRICEOP2017 tmp
      ,dim_part dp
      ,dim_plant dl
      ,dim_date acgi
 WHERE f_so.dim_partid = dp.dim_partid
   AND f_so.dim_plantid = dl.dim_plantid
   AND f_so.dim_dateidactualgi = acgi.dim_dateid
   AND tmp.part_number = dp.partnumber
   AND tmp.plant_code = dl.plantcode
   AND TO_CHAR(acgi.datevalue,'MMYYYY') = REPLACE(tmp.forecast_date,'-','')
   AND REPLACE(tmp.forecast_date,'-','') = TO_CHAR(CURRENT_DATE,'MMYYYY')
   AND f_so.dd_forecastorders_hc = 'X'
   AND f_so.dd_dummyrow = 'X'
   AND f_so.ct_avgprice <> IFNULL(tmp.opunitprice_euro,0)*/

   DROP TABLE IF EXISTS tmp_inventorymeasures;
CREATE TABLE tmp_inventorymeasures
AS
SELECT f_invagng.dim_partid as dim_partid,
       f_invagng.dim_plantid as dim_plantid,
	   f_invagng.dim_projectsourceid as dim_projectsourceid,
       sum(f_invagng.ct_TotalRestrictedStock) as ct_totalrestrictedstock,
       sum(f_invagng.ct_StockQty) as ct_stockqty,
       sum(f_invagng.ct_StockInTransfer) as ct_stockintransfer,
       sum(f_invagng.CT_WIPQTY + f_invagng.CT_STOCKQTY + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) as ct_onhandwipqty,
       sum(f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) as ct_onhand,
       sum(f_invagng.ct_StockInTransit) as ct_stockintransit,
       sum(f_invagng.ct_orderitemqty - f_invagng.ct_grqty) as ct_wipqty,
			 sum(f_invagng.ct_StockInQInsp) as ct_StockInQInsp
FROM fact_inventoryaging f_invagng
GROUP BY f_invagng.dim_partid,
         f_invagng.dim_plantid,
		 f_invagng.dim_projectsourceid;

UPDATE fact_salesorder so
SET so.ct_totalrestrictedstock = ifnull(tmpinv.ct_totalrestrictedstock, 0),
  so.ct_stockqty = ifnull(tmpinv.ct_stockqty, 0),
  so.ct_stockintransfer = ifnull(tmpinv.ct_stockintransfer, 0),
  so.ct_onhandwipqty = ifnull(tmpinv.ct_onhandwipqty, 0),
  so.ct_onhand = ifnull(tmpinv.ct_onhand, 0),
  so.ct_stockintransit = ifnull(tmpinv.ct_stockintransit, 0),
	so.ct_wipqty = ifnull(tmpinv.ct_wipqty, 0),
	so.ct_StockInQInsp = ifnull(tmpinv.ct_StockInQInsp, 0)
FROM fact_salesorder so, tmp_inventorymeasures tmpinv
WHERE so.dim_partid = tmpinv.dim_partid
 AND so.dim_plantid = tmpinv.dim_plantid
 AND so.dd_dummyrow = 'X';



/* MDG Part FOR dummy_recordrs*/
DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_SOD;
CREATE TABLE TMP_DIM_MDG_PARTID_SOD as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;


UPDATE fact_salesorder f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM fact_salesorder f, TMP_DIM_MDG_PARTID_SOD tmp
WHERE f.dim_partid = tmp.dim_partid
 AND dd_dummyrow = 'X'
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

-- delete the dummy rows that are not finished goods or mer
DELETE FROM fact_salesorder f_so WHERE fact_salesorderid IN (
SELECT fact_salesorderid
   FROM fact_salesorder f_so
       ,dim_mdg_part dmg
  WHERE  f_so.dim_mdg_partid = dmg.dim_mdg_partid
    AND dmg.mattypeforglbrep NOT IN ('FIN','MER')
    AND f_so.dd_dummyrow = 'X');


DROP TABLE IF EXISTS tmp_forecast_plant;
CREATE TABLE tmp_forecast_plant AS
SELECT
     AVG(ct_avgprice) * AVG(ct_merckhc_sapforecast) forecast_eur
    ,AVG(ct_avgprice) * AVG(ct_stockqty) amt_inv_available
    ,AVG(ct_avgprice) ct_avgprice
    ,AVG(ct_merckhc_sapforecast) ct_merckhc_sapforecast
    ,AVG(ct_stockqty) ct_stockqty
    ,SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 THEN f_so.ct_DeliveredQty ELSE 0 END) ct_goods_issue
    ,SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)) amt_shipped
    ,CASE
        WHEN AVG(CT_MERCKHC_SAPFORECAST) > SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 THEN f_so.ct_DeliveredQty ELSE 0 END)
        THEN AVG(CT_MERCKHC_SAPFORECAST) - SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 THEN f_so.ct_DeliveredQty ELSE 0 END)
        ELSE 0
    END ct_remaining_forecast
    ,SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end) +
		LEAST(
		       CASE WHEN AVG(CT_MERCKHC_SAPFORECAST) > SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
		            THEN AVG(CT_MERCKHC_SAPFORECAST) - SUM (CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
		            ELSE 0
		       END
			, AVG(ct_StockQty)) ct_projected_sales

	,SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end) +
		LEAST(
				CASE WHEN AVG(CT_MERCKHC_SAPFORECAST) > SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
				THEN AVG(CT_MERCKHC_SAPFORECAST) - SUM (CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
				ELSE 0
				END
		, AVG(ct_StockQty)) - AVG(CT_MERCKHC_SAPFORECAST) ct_project_vs_forecast
	,CASE
	     WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
	     THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
		 ELSE 0
	 END amt_remaining_forecast
    ,SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)* amt_exchangerate_gbl) +
    LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
			 THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
			 ELSE 0
		END
		,
		AVG(ct_StockQty * CT_AVGPRICE)
		) amt_projected_sales
    ,SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)* amt_exchangerate_gbl) +
     LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
		     THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
		     ELSE 0
		END
		,
		AVG(ct_StockQty * CT_AVGPRICE)
		) - ( AVG( CT_MERCKHC_SAPFORECAST ) * AVG( CT_AVGPRICE ) ) amt_project_vs_forecast
    ,SUM ( (CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end) ) + AVG( ct_StockQty ) ct_potential_sales
    ,SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_ExchangeRate_GBL) + AVG(ct_StockQty * CT_AVGPRICE) amt_potential_sales
		,AVG(ct_totalrestrictedstock) * AVG(ct_avgprice) amt_forecast_totalrestricted
    ,AVG(ct_totalrestrictedstock) ct_totalrestrictedstock
		,AVG(ct_StockInQInsp) * AVG(ct_avgprice) amt_forecast_StockInQInsp
    ,AVG(ct_StockInQInsp) ct_StockInQInsp
    ,dp.partnumber
    ,dl.plantcode
    ,TRUNC(acgi.datevalue,'MM') gidate
	,
		Abs( (SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)* amt_exchangerate_gbl) + LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl ) THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
			ELSE 0
		END
		,
		AVG(ct_StockQty * CT_AVGPRICE)
		) - ( AVG( CT_MERCKHC_SAPFORECAST ) * AVG( CT_AVGPRICE ) )) ) as amt_forecastRiskEur
	,LEAST( (
		Abs( (SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)* amt_exchangerate_gbl) + LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl ) THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
			ELSE 0
		END
		,
		AVG(ct_StockQty * CT_AVGPRICE)
		) - ( AVG( CT_MERCKHC_SAPFORECAST ) * AVG( CT_AVGPRICE ) )) )) , (AVG(ct_StockInQInsp) * AVG(CT_AVGPRICE)) ) as amt_neededQAEur
	,(
			Abs( (SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)* amt_exchangerate_gbl) + LEAST(
				CASE WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl ) THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
					ELSE 0
				END
			,
			AVG(ct_StockQty * CT_AVGPRICE)
		) -
		( AVG( CT_MERCKHC_SAPFORECAST ) * AVG( CT_AVGPRICE ) )) ) ) - (case when (AVG(ct_StockInQInsp) * AVG(CT_AVGPRICE)) >= -1* (SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)* amt_exchangerate_gbl) + LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl ) THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
			ELSE 0
		END
		,
		AVG(ct_StockQty * CT_AVGPRICE)
		) - ( AVG( CT_MERCKHC_SAPFORECAST ) * AVG( CT_AVGPRICE ) )) then -1* (SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)* amt_exchangerate_gbl) + LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl ) THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
			ELSE 0
		END
		,
		AVG(ct_StockQty * CT_AVGPRICE)
		) - ( AVG( CT_MERCKHC_SAPFORECAST ) * AVG( CT_AVGPRICE ) )) else (AVG(ct_StockInQInsp) * AVG(CT_AVGPRICE))
		End) as amt_riskLevel2Eur
	,
		Abs( (SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end) +
		LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST) > SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
		THEN AVG(CT_MERCKHC_SAPFORECAST) - SUM (CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
			ELSE 0
		END
		, AVG(ct_StockQty)) - AVG(CT_MERCKHC_SAPFORECAST)) ) as ct_forecastRiskUnits
	,LEAST( (
		Abs( (SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end) +
		LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST) > SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
			THEN AVG(CT_MERCKHC_SAPFORECAST) - SUM (CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
			ELSE 0
		END
		, AVG(ct_StockQty)) - AVG(CT_MERCKHC_SAPFORECAST)) ) ) ,AVG( (ct_StockInQInsp) )) as ct_neededQAUnits
	,(
		Abs( (SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end) +
		LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST) > SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
			THEN AVG(CT_MERCKHC_SAPFORECAST) - SUM (CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
			ELSE 0
		END
		, AVG(ct_StockQty)) - AVG(CT_MERCKHC_SAPFORECAST)) ) ) - (LEAST( (
		Abs( (SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end) +
		LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST) > SUM(CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
			THEN AVG(CT_MERCKHC_SAPFORECAST) - SUM (CASE WHEN f_so.Dim_DateidActualGI <> 1 then f_so.ct_DeliveredQty else 0 end)
			ELSE 0
		END
		, AVG(ct_StockQty)) - AVG(CT_MERCKHC_SAPFORECAST)) )) , (AVG(ct_StockInQInsp) * AVG(CT_AVGPRICE)) )) as ct_riskLevel2Units
    FROM fact_salesorder f_so
        ,dim_part dp
        ,dim_plant dl
        ,dim_date acgi
        ,dim_mdg_part dmg
			  ,dim_bwproducthierarchy bw
   WHERE f_so.dim_partid = dp.dim_partid
     AND f_so.dim_plantid = dl.dim_plantid
     AND f_so.dim_dateidactualgi = acgi.dim_dateid
		 AND f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
     AND TO_CHAR(acgi.datevalue,'MMYYYY') = TO_CHAR(CURRENT_DATE,'MMYYYY')
     AND f_so.dim_mdg_partid = dmg.dim_mdg_partid
     AND f_so.dd_forecastorders_hc = 'X'
     AND dmg.mattypeforglbrep IN ('FIN','MER')
		 AND bw.businesssector = 'BS-01'
		 AND bw.businessdesc = 'Biopharma'
  GROUP BY
          dp.partnumber
        ,dl.plantcode
        ,TRUNC(acgi.datevalue,'MM')
 HAVING
	ROUND(SUM(f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)* amt_exchangerate_gbl) +
     LEAST(
		CASE WHEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) > SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
		     THEN AVG(CT_MERCKHC_SAPFORECAST * CT_AVGPRICE) - SUM( f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl )
		     ELSE 0
		END
		,
		AVG(ct_StockQty * CT_AVGPRICE)
		) - ( AVG( CT_MERCKHC_SAPFORECAST ) * AVG( CT_AVGPRICE ) ),2) < 0;


--1.1 Added the dd_fulfillment_risk amt_project_vs_forecast < 0

UPDATE fact_salesorder f_so SET dd_fulfillment_risk = 'N' WHERE f_so.dd_forecastorders_hc = 'X' AND dd_fulfillment_risk <> 'N' ;

UPDATE fact_salesorder f_so
SET dd_fulfillment_risk = ifnull('X', 'Not Set')
FROM fact_salesorder f_so
     ,dim_part dp
     ,tmp_forecast_plant t
WHERE f_so.dim_partid = dp.dim_partid
  AND dp.partnumber = t.partnumber
  AND dp.plant = t.plantcode
  AND f_so.dd_forecastorders_hc = 'X'
  AND ROUND(amt_project_vs_forecast,2) < 0
  AND dd_fulfillment_risk <> ifnull('X', 'Not Set');


DROP TABLE IF EXISTS tmp_forecast_plant_f;
CREATE TABLE tmp_forecast_plant_f AS
SELECT
       SUM(forecast_eur) forecast_eur
      ,SUM(amt_inv_available) amt_inv_available
      ,SUM(ct_avgprice) ct_avgprice
      ,SUM(ct_merckhc_sapforecast)ct_merckhc_sapforecast
      ,SUM(ct_stockqty) ct_stockqty
      ,SUM(ct_goods_issue) ct_goods_issue
      ,SUM(amt_shipped) amt_shipped
      ,SUM(ct_remaining_forecast) ct_remaining_forecast
      ,SUM(ct_projected_sales) ct_projected_sales
      ,SUM(ct_project_vs_forecast) ct_project_vs_forecast
      ,SUM(amt_remaining_forecast)amt_remaining_forecast
      ,SUM(amt_projected_sales) amt_projected_sales
      ,SUM(amt_project_vs_forecast) amt_project_vs_forecast
      ,SUM(ct_potential_sales) ct_potential_sales
      ,SUM(amt_potential_sales) amt_potential_sales
			,SUM(amt_forecast_totalrestricted) amt_forecast_totalrestricted
      ,SUM(ct_totalrestrictedstock) ct_totalrestrictedstock
			,SUM(amt_forecast_StockInQInsp) amt_forecast_StockInQInsp
      ,SUM(ct_StockInQInsp) ct_StockInQInsp
      ,plantcode
      ,gidate
      ,SUM(amt_forecastRiskEur) as amt_forecastRiskEur
      ,SUM(amt_neededQAEur) as amt_neededQAEur
      ,SUM(amt_riskLevel2Eur) as amt_riskLevel2Eur
      ,SUM(ct_forecastRiskUnits) as ct_forecastRiskUnits
      ,SUM(ct_neededQAUnits) as ct_neededQAUnits
      ,SUM(ct_riskLevel2Units) as ct_riskLevel2Units
  FROM tmp_forecast_plant
 WHERE ROUND(amt_project_vs_forecast,2) < 0
 GROUP BY plantcode,gidate;

UPDATE  fact_salesorder f
SET f.amt_forecast_eur_pl = 0
   ,f.amt_inv_available_pl = 0
   ,f.ct_merckhc_sapforecast_pl = 0
   ,f.ct_avgprice_pl = 0
   ,f.ct_stockqty_pl = 0
   ,f.ct_goods_issue_pl = 0
   ,f.amt_shipped_pl = 0
   ,f.ct_remaining_forecast_pl = 0
   ,f.ct_projected_sales_pl = 0
   ,f.ct_project_vs_forecast_pl = 0
   ,f.amt_remaining_forecast_pl = 0
   ,f.amt_projected_sales_pl = 0
   ,f.amt_project_vs_forecast_pl = 0
   ,f.ct_potential_sales_pl = 0
   ,f.amt_potential_sales_pl = 0
	 ,f.amt_forecast_totalrestricted_pl = 0
   ,f.ct_totalrestricted_pl = 0
	 ,f.amt_forecast_StockInQInsp_pl = 0
   ,f.ct_StockInQInsp_pl = 0
   ,f.amt_forecastRiskEur = 0
   ,f.amt_neededQAEur = 0
   ,f.amt_riskLevel2Eur = 0
   ,f.ct_forecastRiskUnits = 0
   ,f.ct_neededQAUnits = 0
   ,f.ct_riskLevel2Units = 0
FROM fact_salesorder f;

update fact_salesorder f
		set f.amt_forecast_eur_pl = ifnull(t.forecast_eur, 0)
		,f.amt_inv_available_pl = ifnull(t.amt_inv_available, 0)
		,f.ct_merckhc_sapforecast_pl = ifnull(t.ct_merckhc_sapforecast, 0)
		,f.ct_avgprice_pl = ifnull(t.ct_avgprice, 0)
		,f.ct_stockqty_pl = ifnull(t.ct_stockqty, 0)
		,f.ct_goods_issue_pl = ifnull(t.ct_goods_issue, 0)
		,f.amt_shipped_pl = ifnull(t.amt_shipped, 0)
		,f.ct_remaining_forecast_pl = ifnull(t.ct_remaining_forecast, 0)
		,f.ct_projected_sales_pl = ifnull(t.ct_projected_sales, 0)
		,f.ct_project_vs_forecast_pl = ifnull(t.ct_project_vs_forecast, 0)
		,f.amt_remaining_forecast_pl = ifnull(t.amt_remaining_forecast, 0)
		,f.amt_projected_sales_pl = ifnull(t.amt_projected_sales, 0)
		,f.amt_project_vs_forecast_pl = ifnull(t.amt_project_vs_forecast, 0)
		,f.ct_potential_sales_pl = ifnull(t.ct_potential_sales, 0)
		,f.amt_potential_sales_pl = ifnull(t.amt_potential_sales, 0)
		,f.amt_forecast_totalrestricted_pl = ifnull(t.amt_forecast_totalrestricted, 0)
	  ,f.ct_totalrestricted_pl = ifnull(t.ct_totalrestrictedstock, 0)
		,f.amt_forecast_StockInQInsp_pl = ifnull(t.amt_forecast_StockInQInsp, 0)
		,f.ct_StockInQInsp_pl = ifnull(t.ct_StockInQInsp, 0)
		,f.amt_forecastRiskEur = ifnull(t.amt_forecastRiskEur, 0)
        ,f.amt_neededQAEur = ifnull(t.amt_neededQAEur, 0)
        ,f.amt_riskLevel2Eur = ifnull(t.amt_riskLevel2Eur, 0)
        ,f.ct_forecastRiskUnits = ifnull(t.ct_forecastRiskUnits, 0)
        ,f.ct_neededQAUnits = ifnull(t.ct_neededQAUnits, 0)
        ,f.ct_riskLevel2Units = ifnull(t.ct_riskLevel2Units, 0)
from fact_salesorder f
inner join dim_plant pl on f.dim_plantid = pl.dim_plantid
inner join dim_date agi on f.dim_dateidactualgi = agi.dim_dateid
inner join tmp_forecast_plant_f t on pl.plantcode = t.plantcode AND TRUNC(agi.datevalue,'MM') = t.gidate;

DROP TABLE if EXISTS TMP_DIM_MDG_PARTID_SOD;
DROP TABLE if EXISTS tmp_sapfcst;
DROP TABLE IF EXISTS tmp_inventorymeasures;

/*END BI-5275 - Added by Victor 21 Jan 2017 Forecast Parts Logic*/


/* DOT Metric - 07032017 - Alina */

drop table if exists datediffcalculations;
create table datediffcalculations
as select
distinct
 f.dd_SalesDocNo
,f.dd_SalesItemNo
,f.dd_ScheduleNo
,f.dd_SalesDlvrDocNo
,f.dd_SalesDlvrItemNo
,case when d.datevalue<>'0001-01-01' and pgid.datevalue<>'0001-01-01' then d.datevalue-
(case when dfc.isapublicholiday=0 then pgid.datevalue
when dfc.isapublicholiday=1 then dfc.nextworkingdate
else pgid.datevalue end)
when d.datevalue='0001-01-01' and (dd.datevalue<>'0001-01-01' and pgid.datevalue<>'0001-01-01') then dd.datevalue-
(case when dfc.isapublicholiday=0 then pgid.datevalue
when dfc.isapublicholiday=1 then dfc.nextworkingdate
else pgid.datevalue end)
else 0 end DateDiff

from fact_salesorderdelivery f
inner join dim_part prt on f.dim_partid = prt.dim_partid
join dim_date d on f.dim_date_enddateloadingid=d.dim_dateid
inner join dim_date pgid on f.dim_dateidplannedgoodsissue=pgid.dim_dateid
inner join dim_date_factory_calendar dfc on pgid.datevalue=dfc.datevalue
inner join dim_plant dp on f.dim_plantid = dp.dim_plantid
and dfc.PLANTCODE_FACTORY = dp.plantcode
join dim_date dd on f.dim_dateidactualgoodsissue=dd.dim_dateid;



update fact_salesorderdelivery
set dd_diffInDays = ifnull(datediff, 0)
from fact_salesorderdelivery f,datediffcalculations df
where
f.dd_SalesDocNo=df.dd_SalesDocNo
and f.dd_SalesItemNo=df.dd_SalesItemNo
and f.dd_ScheduleNo=df.dd_ScheduleNo
and f.dd_SalesDlvrDocNo=df.dd_SalesDlvrDocNo
and f.dd_SalesDlvrItemNo=df.dd_SalesDlvrItemNo
and  f.dd_diffInDays <> ifnull(datediff, 0);

update fact_salesorderdelivery
set dd_dot_flag=case when dd_diffInDays>0 then 'Late' else 'OnTime' end
from fact_salesorderdelivery;

/* 13 april 2017 Veronica P - Delivery Group and Batch Split */

UPDATE fact_salesorderdelivery fsod
SET dd_batch_split = ifnull(fso.dd_batch_split,'Not Set')
FROM fact_salesorderdelivery fsod
INNER JOIN fact_salesorder fso on fso.dd_salesdocno = fsod.dd_salesdocno and fso.dd_salesitemno = fsod.dd_salesitemno and fso.dd_ScheduleNo=fsod.dd_ScheduleNo
WHERE fsod.dd_batch_split <> ifnull(fso.dd_batch_split,'Not Set');


UPDATE fact_salesorderdelivery fsod
SET dd_delivery_group = ifnull(fso.dd_delivery_group,'Not Set')
FROM fact_salesorderdelivery fsod
INNER JOIN fact_salesorder fso on fso.dd_salesdocno = fsod.dd_salesdocno and fso.dd_salesitemno = fsod.dd_salesitemno and fso.dd_ScheduleNo=fsod.dd_ScheduleNo
WHERE fsod.dd_delivery_group <> ifnull(fso.dd_delivery_group,'Not Set');

/* Update SO Create Date */
/* Removed the Schedule No from join condition - Roxana D - 2017-10-18

UPDATE fact_salesorderdelivery sd
	SET dim_dateidsocreated = f.dim_dateidsocreated
FROM fact_salesorder f, fact_salesorderdelivery sd
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
		AND sd.dim_dateidsocreated <> f.dim_dateidsocreated
*/

MERGE INTO fact_salesorderdelivery sd
USING (
		SELECT   dd_SalesDocNo, dd_SalesItemNo, max(dim_dateidsocreated) as dim_dateidsocreated
		FROM fact_salesorder f
		GROUP BY dd_SalesDocNo, dd_SalesItemNo
) f
on  sd.dd_SalesDocNo = f.dd_SalesDocNo
AND sd.dd_SalesItemNo = f.dd_SalesItemNo
when matched then update
set  sd.dim_dateidsocreated = ifnull(f.dim_dateidsocreated, 1)
where sd.dim_dateidsocreated <> ifnull(f.dim_dateidsocreated, 1);



/*@Catalin 08.01.2017 BI-4974 add batch to salesdelivery  - Moved after the insert of all lines Roxana D 19 Dec 2017*/

update fact_salesorderdelivery a
set a.dd_batch = ifnull(b.LIPS_CHARG,'Not Set'),
	a.dw_update_date = current_date
 from
fact_salesorderdelivery a,LIKP_LIPS b where a.dd_SalesDlvrDocNo = b.LIKP_VBELN
and a.dd_SalesDlvrItemNo = b.LIPS_POSNR
and a.dd_batch <>ifnull(b.LIPS_CHARG,'Not Set') ;

/* */

/* Update Guarantee Date */

UPDATE fact_salesorderdelivery sd
	SET dim_dateidguaranteedate = ifnull(f.dim_dateidguaranteedate, 1)
From fact_salesorder f, fact_salesorderdelivery sd
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
		AND sd.dim_dateidguaranteedate <> ifnull(f.dim_dateidguaranteedate, 1);


/* Update Promise Delivery Date */

UPDATE fact_salesorderdelivery sd
	SET dim_dateidscheddelivery = ifnull(f.dim_dateidscheddelivery, 1)
From fact_salesorder f, fact_salesorderdelivery sd
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
		AND sd.dim_dateidscheddelivery <> ifnull(f.dim_dateidscheddelivery, 1);

/* SO Document Date */

UPDATE fact_salesorderdelivery sd
	SET Dim_DateIdSODocument = ifnull(f.Dim_DateIdSODocument, 1)
From fact_salesorder f, fact_salesorderdelivery sd
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
		AND sd.Dim_DateIdSODocument <> ifnull(f.Dim_DateIdSODocument, 1);

/* SO Item Changed On */

UPDATE fact_salesorderdelivery sd
	SET dim_dateidsoitemchangedon = ifnull(f.dim_dateidsoitemchangedon, 1)
From fact_salesorder f, fact_salesorderdelivery sd
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
		AND sd.dim_dateidsoitemchangedon <> ifnull(f.dim_dateidsoitemchangedon, 1);

/* 8 may 2017 Cornelia add Ship To Address */
UPDATE fact_salesorderdelivery sd
	SET dim_addressid = ifnull(f.dim_addressid, 1)
From fact_salesorder f, fact_salesorderdelivery sd
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
	AND sd.dim_addressid <> ifnull(f.dim_addressid, 1);


/* 23 May 2017 - Roxana - add Difference in Days metric for PM */

drop table if exists datediffcalculationsPM;
create table datediffcalculationsPM
as select
distinct
 f.dd_SalesDocNo
,f.dd_SalesItemNo
,f.dd_ScheduleNo
,f.dd_SalesDlvrDocNo
,f.dd_SalesDlvrItemNo
,case when d.datevalue <> '0001-01-01' and pgid.datevalue <> '0001-01-01' then d.datevalue - pgid.datevalue
when d.datevalue = '0001-01-01' and (dd.datevalue <> '0001-01-01' and pgid.datevalue<>'0001-01-01') then dd.datevalue - pgid.datevalue
else 0 end DateDiffPM
from fact_salesorderdelivery f
join dim_date d on f.dim_date_enddateloadingid = d.dim_dateid
inner join dim_date pgid on f.dim_dateidplannedgoodsissue = pgid.dim_dateid
join dim_date dd on f.dim_dateidactualgoodsissue = dd.dim_dateid;


update fact_salesorderdelivery
set dd_diffInDaysPM = ifnull(datediffPM, 0)
from fact_salesorderdelivery f,datediffcalculationsPM df
where f.dd_SalesDocNo = df.dd_SalesDocNo
and f.dd_SalesItemNo = df.dd_SalesItemNo
and f.dd_ScheduleNo = df.dd_ScheduleNo
and f.dd_SalesDlvrDocNo = df.dd_SalesDlvrDocNo
and f.dd_SalesDlvrItemNo = df.dd_SalesDlvrItemNo
and f.dd_diffInDaysPM <> ifnull(datediffPM, 0);


/* Alex D update Order Creation Time */
/*Roxana D - copy the Creation time from Sales Order
UPDATE fact_salesorderdelivery sd
    SET dd_VBAKCreateTime = ifnull(VBAK_ERZET, 'Not Set')
From   VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE    sd.dd_SalesDocNo = v.VBAK_VBELN
        AND sd.dd_SalesItemNo =  v.VBAP_POSNR
        AND sd.dd_ScheduleNo = v.VBEP_ETENR
        AND dd_VBAKCreateTime <> ifnull(VBAK_ERZET, 'Not Set')
 */

MERGE INTO fact_salesorderdelivery sd
USING (
		SELECT dd_SalesDocNo, dd_SalesItemNo, max(dd_SOCreateTime) AS dd_SOCreateTime
		FROM fact_salesorder f
		GROUP BY dd_SalesDocNo, dd_SalesItemNo
) f
on  sd.dd_SalesDocNo = f.dd_SalesDocNo
AND sd.dd_SalesItemNo = f.dd_SalesItemNo
when matched then update
set  sd.dd_VBAKCreateTime = f.dd_SOCreateTime
where sd.dd_VBAKCreateTime <> f.dd_SOCreateTime;


/* 30 May 2017 - Roxana - Populate dim_unitofmeasureidtotalweight */

UPDATE fact_salesorderdelivery sod
SET sod.dim_unitofmeasureidtotalweight = ifnull(uom.dim_unitofmeasureid, 1)
FROM fact_salesorderdelivery sod, LIKP_LIPS a, dim_unitofmeasure uom
WHERE   dd_SalesDlvrDocNo = LIKP_VBELN
	and dd_SalesDlvrItemNo = LIPS_POSNR
	and uom.RowIsCurrent = 1
	and uom = ifnull(LIKP_GEWEI,'Not Set')
	and sod.dim_unitofmeasureidtotalweight <> ifnull(uom.dim_unitofmeasureid, 1);


/* 10 Jul 2017 Roxana - Add Planned Goods Issue Date in Sales Analysis */

drop table if exists tmp_plannedgoodsissuedate;
create table tmp_plannedgoodsissuedate as
select f.dd_SalesDocNo , f.dd_SalesDlvrDocNo,
	max(dim_dateidplannedgoodsissue) dim_dateidplannedgoodsissue
from fact_salesorderdelivery f
group by f.dd_SalesDocNo, f.dd_SalesDlvrDocNo;


 UPDATE fact_salesorder so
 SET so.dim_dateidplannedgoodsissue = ifnull(tmp.dim_dateidplannedgoodsissue, 1)
 FROM fact_salesorder so, tmp_plannedgoodsissuedate tmp
 WHERE so.dd_SalesDocNo = tmp.dd_SalesDocNo
 	AND so.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
 	AND so.dim_dateidplannedgoodsissue <> ifnull(tmp.dim_dateidplannedgoodsissue, 1);


drop table if exists tmp_plannedgoodsissuedate;

/* 18 Jul 2017 - Roxana - Add Shipment Number in Sales Analysis */

drop table if exists tmp_shipmentnumber;
create table tmp_shipmentnumber as select distinct dd_ShipmentNumber,dd_SalesDocNo,dd_SalesDlvrDocNo,
		dd_SalesItemNo from fact_salesorderdelivery where dd_ShipmentNumber <> 'Not Set';

UPDATE fact_salesorder so
SET so.dd_ShipmentNumber = ifnull(sod.dd_ShipmentNumber, 'Not Set')
FROM fact_salesorder so, tmp_shipmentnumber sod
	WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
 	AND so.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
	AND so.dd_SalesItemNo = sod.dd_SalesItemNo
	AND so.dd_ShipmentNumber <> ifnull(sod.dd_ShipmentNumber, 'Not Set');


drop table if exists tmp_shipmentnumber;


UPDATE fact_salesorderdelivery sd
	SET sd.dim_batchid = ifnull(f.dim_batchid, 0)
From fact_salesorder f, fact_salesorderdelivery sd
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
	AND sd.dim_batchid <> ifnull(f.dim_batchid, 0);


	update fact_salesorderdelivery f_so
set dim_mercklsconforreqdateid = ifnull(mlsrd.dim_dateid, 1)
from fact_salesorderdelivery f_so,dim_date_factory_calendar mlsrd, dim_part prt, dim_bwproducthierarchy bw
where f_so.Dim_Partid = prt.Dim_Partid
and f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
and
CASE WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN f_so.dim_dateidexpectedship_emd ELSE
  CASE WHEN trim(prt.mtomts) = 'MTO' THEN f_so.dim_accordinggidatemto_emd
      ELSE f_so.dim_dateidexpectedship_emd END END=mlsrd.dim_dateid
and dim_mercklsconforreqdateid <> ifnull(mlsrd.dim_dateid, 1);



 /* Roxana - 30 Aug 2017 -  Add Shipping Condition in Sales Analysis */


drop table if exists tmp_shippingcondition;
create table tmp_shippingcondition as
select distinct dd_salesdlvrdocno,dd_salesdocno,dd_salesitemno,dd_ShipmentNumber, dim_shippingconditionshipmentid
from fact_salesorderdelivery;

update fact_salesorder so
set so.dim_shippingconditionshipmentid = ifnull(tmp.dim_shippingconditionshipmentid, 1)
from fact_salesorder so,tmp_shippingcondition tmp
where so.dd_salesdlvrdocno = tmp.dd_salesdlvrdocno
and so.dd_salesdocno = tmp.dd_salesdocno
and so.dd_salesitemno = tmp.dd_salesitemno
and so.dd_ShipmentNumber = tmp.dd_ShipmentNumber
and so.dim_shippingconditionshipmentid <> ifnull(tmp.dim_shippingconditionshipmentid, 1);


drop table if exists tmp_shippingcondition;

update fact_salesorder f
set f.dim_clusterid = ifnull(dc.dim_clusterid, 1)
from fact_salesorder f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> ifnull(dc.dim_clusterid, 1);

	update emd586.fact_salesorder f
set f.dim_clusterid = ifnull(dc.dim_clusterid, 1)
from emd586.fact_salesorder f, emd586.dim_mdg_part mdg, emd586.dim_bwproducthierarchy ph, emd586.dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> ifnull(dc.dim_clusterid, 1)
and f.dim_projectsourceid=6
and mdg.projectsourceid=6
and ph.projectsourceid=6
and substring(dc.dim_clusterid,1,2)='60';

update fact_salesorderdelivery sod
SET sod.dim_customeridsoldto = ifnull(so.dim_customerid, 1)
 from fact_salesorder so, fact_salesorderdelivery sod, dim_bwproducthierarchy ph
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
and sod.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
and ph.businesssector = 'BS-02'
AND sod.dim_customeridsoldto <> ifnull(so.dim_customerid, 1);


UPDATE fact_salesorderdelivery sod
SET sod.dim_salesorderitemstatusid = ifnull(so.dim_salesorderitemstatusid, 1)
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND sod.dim_salesorderitemstatusid <> ifnull(so.dim_salesorderitemstatusid, 1);

/* APP-7497 Add BWCountry Hierarchy to a couple of facts - Oana 30Oct2017 */
update fact_salesorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_salesorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-32')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY04' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7
	and 	f.dd_forecastorders_hc = 'X'
   	and 	f.dd_dummyrow = 'X';

update fact_salesorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_salesorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-31','DIV-35','DIV-34')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY05' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7
	and 	f.dd_forecastorders_hc = 'X'
   	and 	f.dd_dummyrow = 'X';

update fact_salesorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_salesorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY07' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7
	and 	f.dd_forecastorders_hc = 'X'
   	and 	f.dd_dummyrow = 'X';

update fact_salesorder f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_salesorder f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-87')
	and 	dim_con.INTERNALHIERARCHYNAME = '0COUNTRY08' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7
	and 	f.dd_forecastorders_hc = 'X'
   	and 	f.dd_dummyrow = 'X';
/* END APP-7497 Add BWCountry Hierarchy to a couple of facts - Oana 30Oct2017 */

/*Populate Bill to Party - APP 8094 - Bogdan S - 21Nov2017 - Start*/
DROP TABLE IF EXISTS TMP_UPD_DELIVERY_SO;
CREATE TABLE TMP_UPD_DELIVERY_SO
AS
SELECT fso.dd_SalesDocNo,fso.dd_SalesItemNo,max(fso.Dim_BillToPartyPartnerFunctionId) Dim_BillToPartyPartnerFunctionId
FROM fact_salesorder fso
GROUP BY fso.dd_SalesDocNo, fso.dd_SalesItemNo;
   
UPDATE fact_salesorderdelivery fsd
SET fsd.Dim_BillToPartyPartnerFunctionId = ifnull(fso.Dim_BillToPartyPartnerFunctionId, 1)
FROM TMP_UPD_DELIVERY_SO fso, fact_salesorderdelivery fsd
WHERE fsd.dd_salesdocno = fso.dd_SalesDocNo
   AND fsd.dd_salesitemno = fso.dd_SalesItemNo
   AND fsd.Dim_BillToPartyPartnerFunctionId <> ifnull(fso.Dim_BillToPartyPartnerFunctionId, 1);
/*Populate Bill to Party - APP 8094 - Bogdan S - 21Nov2017 - End*/

/* APP-7957 New Demand Flags - Oana 21Nov2017 */
drop table if exists tmp_demandflags;
create table tmp_demandflags as
select distinct fso.dd_salesdocno,
	fso.dd_salesitemno,
	fso.dd_scheduleno,
	fso.dd_cancelleddemand_flag,
	fso.dd_mtddemand_flag
from fact_salesorder fso, fact_salesorderdelivery fsod
where fso.dd_salesdocno = fsod.dd_salesdocno
	and fso.dd_salesitemno = fsod.dd_salesitemno
	and fso.dd_scheduleno = fsod.dd_scheduleno;

update fact_salesorderdelivery fsod
	set fsod.dd_cancelleddemand_flag = ifnull(t.dd_cancelleddemand_flag,'Not Set'),
		fsod.dd_mtddemand_flag = ifnull(t.dd_mtddemand_flag,'Not Set')
from fact_salesorderdelivery fsod, tmp_demandflags t
where fsod.dd_salesdocno = t.dd_salesdocno
	and fsod.dd_salesitemno = t.dd_salesitemno
	and fsod.dd_scheduleno = t.dd_scheduleno;

drop table if exists tmp_demandflags;
/* END APP-7957 New Demand Flags - Oana 21Nov2017 */


/* Bogdan S - 28 Nov 2017 - add the calculation of Open Order Qty, Order Amt and Open Order Amt in backend - Start*/
update fact_salesorder f_so
set amt_openorderpm = ifnull(
CASE 	
    WHEN (sois.OverallProcessingStatus) in ('Completely processed' , 'Tratado completamente') THEN 0.0000 
    WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
    WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
        THEN 	(CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000 
        			  ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) 
          		 END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END)) 
    	ELSE 	(CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000 
          			  ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)  
          		 END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END)) 
END * amt_exchangerate, 0)
from fact_salesorder f_so, dim_salesdocumenttype sdt, dim_salesorderitemstatus sois
where f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
and f_so.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid;

update fact_salesorder f_so
set ct_openorder = ifnull(
CASE 
	WHEN f_so.dim_salesorderrejectreasonid <> 1 THEN 0.0000 
	WHEN sdt.documenttype IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') AND f_so.dd_itemrelfordelv = 'X' 
		THEN (CASE WHEN (f_so.ct_scheduleqtysalesunit - (f_so.ct_shippedagnstorderqty - f_so.ct_cmlqtyreceived)) < 0 THEN 0.0000 
				   ELSE (f_so.ct_scheduleqtysalesunit - (f_so.ct_shippedagnstorderqty - f_so.ct_cmlqtyreceived)) 
			  END) 
	WHEN (f_so.ct_scheduleqtysalesunit - f_so.ct_shippedagnstorderqty) < 0 THEN 0.0000 
	ELSE (f_so.ct_scheduleqtysalesunit - f_so.ct_shippedagnstorderqty) 
END, 0) 
from fact_salesorder f_so, dim_salesdocumenttype sdt
where f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid;

update fact_salesorder f_so
set amt_order = ifnull (CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) 
					 		ELSE f_so.amt_ScheduleTotal 
			    	   END, 0)
from fact_salesorder f_so, dim_salesmisc dsi
where f_so.dim_salesmiscid = dsi.dim_salesmiscid;

update fact_salesorder f_so
set amt_openorder = ifnull(
CASE 
	WHEN f_so.dim_salesorderrejectreasonid <> 1 THEN 0.0000 
	WHEN sdt.documenttype IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') AND f_so.dd_itemrelfordelv = 'X' 
		THEN (CASE WHEN (f_so.ct_scheduleqtysalesunit - (f_so.ct_shippedagnstorderqty - f_so.ct_cmlqtyreceived)) < 0 THEN 0.0000 
				   ELSE (f_so.ct_scheduleqtysalesunit - (f_so.ct_shippedagnstorderqty - f_so.ct_cmlqtyreceived)) 
			  END * f_so.amt_unitpriceuom / (CASE WHEN f_so.ct_priceunit <> 0 THEN f_so.ct_priceunit ELSE 1 END)) 
		ELSE (CASE WHEN (f_so.ct_scheduleqtysalesunit - f_so.ct_shippedagnstorderqty) < 0 THEN 0.0000 
				   ELSE (f_so.ct_scheduleqtysalesunit - f_so.ct_shippedagnstorderqty) 
			  END * f_so.amt_unitpriceuom/(CASE WHEN f_so.ct_priceunit <> 0 THEN f_so.ct_priceunit ELSE 1 END)) 
END, 0)
from fact_salesorder f_so, dim_salesdocumenttype sdt
where f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid;
/* Bogdan S - 28 Nov 2017 - add the calculation of Open Order Qty, Order Amt and Open Order Amt in backend - End*/



/* Roxana H - 18 Dec 2017 -  Add Delivered Qty in Sales in order to calculated the measures from below */


drop table if exists tmp_deliveredqty;
create table tmp_deliveredqty as
select sum(sod.ct_QtyDelivered) QtyDelivered,sod.dd_salesdocno, sod.dd_salesitemno,sod.dd_Scheduleno
from fact_Salesorder f, fact_salesorderdelivery sod
    WHERE     f.dd_SalesDocNo = sod.dd_SalesDocNo
          AND f.dd_SalesItemNo = sod.dd_SalesItemNo
          AND f.dd_ScheduleNo = sod.dd_ScheduleNo
group by sod.dd_salesdocno, sod.dd_salesitemno,sod.dd_Scheduleno;


UPDATE fact_Salesorder f
      SET f.ct_QtyDelivered = ifnull(tmp.QtyDelivered, 0)
FROM  fact_Salesorder f, tmp_deliveredqty tmp
    WHERE     f.dd_SalesDocNo = tmp.dd_SalesDocNo
          AND f.dd_SalesItemNo = tmp.dd_SalesItemNo
          AND f.dd_ScheduleNo = tmp.dd_ScheduleNo
and f.ct_QtyDelivered <> ifnull(tmp.QtyDelivered, 0);



/* 28 Nov 2017 - Roxana H - Calculate Open Order Measures at Order Line Level */

/* Qty */

drop table if exists tmp_QtyDelivered;
create table tmp_QtyDelivered as
select sum(so.ct_QtyDelivered) ct_QtyDelivered, so.dd_salesdocno, so.dd_salesitemno
from fact_salesorder so
group by so.dd_salesdocno, so.dd_salesitemno;


drop table if exists tmp_CumOrderQty;
create table tmp_CumOrderQty as
select avg(so.ct_CumOrderQty)  ct_CumOrderQty, so.dd_salesdocno, so.dd_salesitemno, so.dd_scheduleno
from fact_salesorder so
where dd_scheduleno = 1
group by so.dd_salesdocno, so.dd_salesitemno, so.dd_scheduleno;


update fact_salesorder so 
set so.ct_openorderpmorderline = 0
where so.ct_openorderpmorderline <> 0;


update fact_salesorder so 
set so.ct_openorderpmorderline = ifnull(tmp2.ct_CumOrderQty - tmp.ct_QtyDelivered,0)
from fact_salesorder so, tmp_QtyDelivered tmp ,tmp_CumOrderQty tmp2
where so.dd_salesdocno = tmp.dd_salesdocno
and so.dd_salesitemno = tmp.dd_salesitemno
and so.dd_salesdocno = tmp2.dd_salesdocno
and so.dd_salesitemno = tmp2.dd_salesitemno
and so.dd_scheduleno = tmp2.dd_scheduleno
and so.ct_openorderpmorderline <> ifnull((tmp2.ct_CumOrderQty - tmp.ct_QtyDelivered),0);


/* Amt */

update fact_salesorder so 
set so.amt_openorderpmorderline = ct_openorderpmorderline * amt_UnitPriceUoM / (case when ct_PriceUnit <> 0 then ct_PriceUnit else 1 end) 
where so.amt_openorderpmorderline <> ct_openorderpmorderline * amt_UnitPriceUoM / (case when ct_PriceUnit <> 0 then ct_PriceUnit else 1 end) ;


/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */


drop table if exists tmp_updatedate_deliv;
create table tmp_updatedate_deliv as
select max(dw_update_date) dd_updatedate from  fact_salesorderdelivery;


update fact_salesorderdelivery f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_salesorderdelivery f, tmp_updatedate_deliv t
where f.dd_updatedate <> t.dd_updatedate;
