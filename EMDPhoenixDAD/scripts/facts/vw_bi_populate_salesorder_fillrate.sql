/*   Script         : bi_populate_salesorder_fillrate */
/*   Author         : Ashu */
/*   Created On     : 13 Feb 2013 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_salesorder_fillrate migration from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*   13 Feb 2013     Ashu      1.0               Existing code migrated to Vectorwise */


drop table if exists fact_salesorder_720;

Create table fact_salesorder_720(
  v_Fact_SalesOrderid   integer null,
  v_dd_SalesDocNo       varchar(50) null,
  v_dd_SalesItemNo      integer null,
  v_dd_ScheduleNo       integer null,
  v_dim_doccategoryid   integer null,
  v_dim_part_id         integer null,
  v_goods_issue_date    date null, 
  v_ct_FillQty          decimal(18,4) null,
  v_ct_OnHandQty        decimal(18,4) null,
  v_TotalQty            decimal(18,4) null,
  iflag 		integer default 0,
  sr_no		int null);

Insert into fact_salesorder_720(
  v_Fact_SalesOrderid,
  v_dd_SalesDocNo ,
  v_dd_SalesItemNo,
  v_dd_ScheduleNo,
  v_dim_part_id,
  v_goods_issue_date,
  v_ct_FillQty,
  v_ct_OnHandQty,
  v_TotalQty
  )
  SELECT Fact_SalesOrderid v_Fact_SalesOrderid,
         dd_SalesDocNo v_dd_SalesDocNo ,
         dd_SalesItemNo v_dd_SalesItemNo,
         dd_ScheduleNo v_dd_ScheduleNo,
         dim_partid v_dim_part_id,
         dt.DateValue v_goods_issue_date,
         ct_FillQty v_ct_FillQty,
         ct_OnHandQty v_ct_OnHandQty,
         ct_ConfirmedQty v_TotalQty
  FROM fact_salesorder f
      INNER JOIN dim_date dt ON dt.dim_dateid = f.Dim_DateidGoodsIssue
      INNER JOIN dim_date adt ON adt.dim_dateid = f.Dim_DateidActualGI
      inner join dim_documentcategory dg on dg.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
      inner join Dim_ScheduleLineCategory slc on f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
  WHERE (   dt.DateValue = current_date
         OR adt.DateValue = current_date
         OR ((dt.DateValue < current_date OR adt.DateValue < current_date) AND ct_FillQty = 0 AND ct_OnHandQty = 0)
		)
        AND f.Dim_SalesOrderRejectReasonid = 1 
		AND dg.DocumentCategory <> 'H' 
        AND f.dd_ItemRelForDelv = 'X'
  ORDER BY dt.DateValue DESC, dd_SalesDocNo, dd_SalesItemNo,dd_ScheduleNo;

drop table if exists tmp1_exists_f_sod;
create table tmp1_exists_f_sod as
select f_sod.dd_SalesDocNo,f_sod.dd_SalesItemNo,f_sod.dd_ScheduleNo,d_gi.dim_dateid,d_gi.datevalue,d_pgi.datevalue pgi_datevalue,f_sod.ct_QtyDelivered
FROM fact_salesorderdelivery f_sod
                      INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                      INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                      inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.GoodsMovementStatus <> 'Not yet processed';

drop table if exists fact_salesorder_720_a;
create table fact_salesorder_720_a as
select v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_goods_issue_date,
	   sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END) ct_QtyDelivered_sum
from tmp1_exists_f_sod f_sod,fact_salesorder_720 
WHERE   f_sod.dd_SalesDocNo = v_dd_SalesDocNo
	AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
	AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
	AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue > v_goods_issue_date) or (f_sod.dim_dateid = 1 and f_sod.pgi_datevalue > v_goods_issue_date))
group by v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_goods_issue_date;


/* **
UPDATE fact_salesorder f
From dim_salesorderitemstatus s,fact_salesorder_720 x
        SET   ct_FillQty = f.ct_ConfirmedQty - ifnull((SELECT f_sod.ct_QtyDelivered_sum
                                            FROM    fact_salesorder_720_a f_sod 
                                            WHERE     f_sod.v_dd_SalesDocNo = x.v_dd_SalesDocNo
                                                  AND f_sod.v_dd_SalesItemNo = x.v_dd_SalesItemNo
                                                  AND f_sod.v_dd_ScheduleNo = x.v_dd_ScheduleNo
						AND f_sod.v_goods_issue_date = x.v_goods_issue_date),0)
where s.OverallDeliveryStatus = 'Completely processed'
and exists (SELECT 1 FROM tmp1_exists_f_sod f_sod
		 WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                      AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                      AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue <= v_goods_issue_date) or (f_sod.dim_dateid = 1 and f_sod.pgi_datevalue <= v_goods_issue_date)))
AND  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty = 0
** */

/* Split the above update into 2 queries */

DROP TABLE IF EXISTS tmp_fact_salesorder_ct_FillQty;
CREATE TABLE tmp_fact_salesorder_ct_FillQty AS
SELECT f.dd_SalesDocNo,f.dd_SalesItemNo,f.dd_ScheduleNo,f.ct_ConfirmedQty,
	   ifnull(f_sod.ct_QtyDelivered_sum, 0) ct_QtyDelivered_sum
FROM fact_salesorder f
		inner join dim_salesorderitemstatus s on f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
		inner join fact_salesorder_720 x on f.Fact_SalesOrderid = x.v_Fact_SalesOrderid
		left join fact_salesorder_720_a f_sod on  f_sod.v_dd_SalesDocNo = x.v_dd_SalesDocNo
											  AND f_sod.v_dd_SalesItemNo = x.v_dd_SalesItemNo
											  AND f_sod.v_dd_ScheduleNo = x.v_dd_ScheduleNo
											  AND f_sod.v_goods_issue_date = x.v_goods_issue_date
where   s.OverallDeliveryStatus = 'Completely processed'
	AND x.v_ct_FillQty = 0
	AND exists (SELECT 1 FROM tmp1_exists_f_sod f_sod
			    WHERE    f_sod.dd_SalesDocNo = x.v_dd_SalesDocNo
				     AND f_sod.dd_SalesItemNo = x.v_dd_SalesItemNo
				     AND f_sod.dd_ScheduleNo = x.v_dd_ScheduleNo
					 AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue <= x.v_goods_issue_date) or (f_sod.dim_dateid = 1 and f_sod.pgi_datevalue <= x.v_goods_issue_date)));


UPDATE fact_salesorder f
SET ct_FillQty = c.ct_ConfirmedQty - c.ct_QtyDelivered_sum
FROM tmp_fact_salesorder_ct_FillQty c, fact_salesorder f
WHERE f.dd_SalesDocNo = c.dd_SalesDocNo
AND f.dd_SalesItemNo = c.dd_SalesItemNo
AND f.dd_ScheduleNo = c.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_fact_salesorder_ct_FillQty;

drop table if exists tmp_fact_salesorder_t001_1;
create table tmp_fact_salesorder_t001_1 as
SELECT x.v_Fact_SalesOrderid, 
	   sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END) as ct_FillQty,
	   max(f_sod.Dim_DateidDeliveryDate) 		as Dim_DateidShipDlvrFill,
	   max(f_sod.Dim_DateidActualGoodsIssue) 	as Dim_DateidActualGIFill,
	   max(f_sod.Dim_DateidLoadingDate) 		as Dim_DateidLoadingFill
FROM fact_salesorderdelivery f_sod
		inner join fact_salesorder_720 x on    f_sod.dd_SalesDocNo = v_dd_SalesDocNo
										   AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
										   AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
		INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
		INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
		INNER JOIN dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
WHERE     sois.GoodsMovementStatus <> 'Not yet processed'
	 AND (   (d_gi.dim_dateid <> 1 and d_gi.datevalue <= x.v_goods_issue_date) 
		  or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= x.v_goods_issue_date))
group by x.v_Fact_SalesOrderid;

merge into fact_salesorder fact
using (select f.fact_salesorderid, ifnull(t1.ct_FillQty, 0) ct_FillQty
	   from fact_salesorder f
				inner join fact_salesorder_720 t0 on f.Fact_SalesOrderid = t0.v_Fact_SalesOrderid
								left join tmp_fact_salesorder_t001_1 t1 on t0.v_Fact_SalesOrderid = t1.v_Fact_SalesOrderid
				inner join dim_salesorderitemstatus s on f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
	   where     t0.v_ct_FillQty = 0
			 and (     s.OverallDeliveryStatus <> 'Completely processed'
					or not exists (SELECT 1 
								   FROM fact_salesorderdelivery f_sod1
											  INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod1.Dim_DateidActualGoodsIssue
											  INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod1.Dim_DateidPlannedGoodsIssue
											  inner join dim_salesorderitemstatus sois on f_sod1.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
								   WHERE    f_sod1.dd_SalesDocNo = t0.v_dd_SalesDocNo
									    AND f_sod1.dd_SalesItemNo = t0.v_dd_SalesItemNo
									    AND f_sod1.dd_ScheduleNo = t0.v_dd_ScheduleNo
										AND sois.GoodsMovementStatus <> 'Not yet processed'
										AND (   (d_gi.dim_dateid <> 1 and d_gi.datevalue <= t0.v_goods_issue_date)
											 or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= t0.v_goods_issue_date))))			 
	  ) src on fact.fact_salesorderid = src.fact_salesorderid
when matched then update set fact.ct_FillQty = src.ct_FillQty
where fact.ct_FillQty <> src.ct_FillQty;

			/* ORIGINAL SCRIPT
			UPDATE fact_salesorder f
			From dim_salesorderitemstatus s,fact_salesorder_720
					SET   ct_FillQty = ifnull((SELECT sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END)
									FROM    fact_salesorderdelivery f_sod
										  INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
										  INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
										  inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
									WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
										  AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
										  AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
										  and sois.GoodsMovementStatus <> 'Not yet processed'
										  AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))),0)
			where  ( s.OverallDeliveryStatus <> 'Completely processed'
			or not exists (SELECT 1 FROM fact_salesorderdelivery f_sod
								  INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
								  INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
								  inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
							WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
								  AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
								  AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
								  and sois.GoodsMovementStatus <> 'Not yet processed'
			AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))))
			AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
			AND Fact_SalesOrderid = v_Fact_SalesOrderid
			AND v_ct_FillQty=0 */


merge into fact_salesorder fact
using (select f.fact_salesorderid, ifnull(t1.Dim_DateidShipDlvrFill, 1) Dim_DateidShipDlvrFill
	   from fact_salesorder f
				inner join fact_salesorder_720 t0 on f.Fact_SalesOrderid = t0.v_Fact_SalesOrderid
								left join tmp_fact_salesorder_t001_1 t1 on t0.v_Fact_SalesOrderid = t1.v_Fact_SalesOrderid
	   where     t0.v_ct_FillQty = 0			 
	  ) src on fact.fact_salesorderid = src.fact_salesorderid
when matched then update set fact.Dim_DateidShipDlvrFill = src.Dim_DateidShipDlvrFill
where fact.Dim_DateidShipDlvrFill <> src.Dim_DateidShipDlvrFill;

			/* ORIGINAL SCRIPT
			UPDATE fact_salesorder f
			From dim_salesorderitemstatus s,fact_salesorder_720
			SET Dim_DateidShipDlvrFill =
						  ifnull((SELECT max(f_sod.Dim_DateidDeliveryDate)
								  FROM    fact_salesorderdelivery f_sod
										INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
										INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
										inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
								  WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
										AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
										AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
										and sois.GoodsMovementStatus <> 'Not yet processed'
										AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))),1)
			Where  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
			AND Fact_SalesOrderid = v_Fact_SalesOrderid
			AND v_ct_FillQty=0 */

merge into fact_salesorder fact
using (select f.fact_salesorderid, ifnull(t1.Dim_DateidActualGIFill, 1) Dim_DateidActualGIFill
	   from fact_salesorder f
				inner join fact_salesorder_720 t0 on f.Fact_SalesOrderid = t0.v_Fact_SalesOrderid
								left join tmp_fact_salesorder_t001_1 t1 on t0.v_Fact_SalesOrderid = t1.v_Fact_SalesOrderid
	   where     t0.v_ct_FillQty = 0			 
	  ) src on fact.fact_salesorderid = src.fact_salesorderid
when matched then update set fact.Dim_DateidActualGIFill = src.Dim_DateidActualGIFill
where fact.Dim_DateidActualGIFill <> src.Dim_DateidActualGIFill;

			/* ORIGINAL SCRIPT
			UPDATE fact_salesorder f
			From dim_salesorderitemstatus s,fact_salesorder_720
			SET Dim_DateidActualGIFill =
						  ifnull((SELECT max(f_sod.Dim_DateidActualGoodsIssue)
								  FROM    fact_salesorderdelivery f_sod
										INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
										INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
										inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
								  WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
										AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
										AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
										and sois.GoodsMovementStatus <> 'Not yet processed'
										AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))),1)
			Where  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
			AND Fact_SalesOrderid = v_Fact_SalesOrderid
			AND v_ct_FillQty=0 */

merge into fact_salesorder fact
using (select f.fact_salesorderid, ifnull(t1.Dim_DateidLoadingFill, 1) Dim_DateidLoadingFill
	   from fact_salesorder f
				inner join fact_salesorder_720 t0 on f.Fact_SalesOrderid = t0.v_Fact_SalesOrderid
								left join tmp_fact_salesorder_t001_1 t1 on t0.v_Fact_SalesOrderid = t1.v_Fact_SalesOrderid
	   where     t0.v_ct_FillQty = 0			 
	  ) src on fact.fact_salesorderid = src.fact_salesorderid
when matched then update set fact.Dim_DateidLoadingFill = src.Dim_DateidLoadingFill
where fact.Dim_DateidLoadingFill <> src.Dim_DateidLoadingFill;

			/* ORIGINAL SCRIPT
			UPDATE fact_salesorder f
			From dim_salesorderitemstatus s,fact_salesorder_720
			SET Dim_DateidLoadingFill = ifnull((SELECT max(f_sod.Dim_DateidLoadingDate)
								  FROM    fact_salesorderdelivery f_sod
										INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
										INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
										inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
								  WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
										AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
										AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
										and sois.GoodsMovementStatus <> 'Not yet processed'
										AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))),1)
			WHERE f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
					AND Fact_SalesOrderid = v_Fact_SalesOrderid
			AND v_ct_FillQty=0 */


UPDATE fact_salesorder
SET   ct_OnHandQty = ct_FillQty
From fact_salesorder_720, fact_salesorder
WHERE Fact_SalesOrderid = v_Fact_SalesOrderid
        AND v_goods_issue_date < current_date
        AND v_ct_OnHandQty = 0
AND ct_OnHandQty <> ct_FillQty;

DROP TABLE IF EXISTS tmp_fillrate1;
create table tmp_fillrate1
as 
select distinct f_so_sq_001.Dim_Partid, d_GoodsIssue_sq_001.DateValue, f_so_sq_001.dd_SalesDocNo, f_so_sq_001.dd_SalesItemNo,
				f_so_sq_001.dd_ScheduleNo, ( f_so_sq_001.ct_ConfirmedQty  - f_so_sq_001.ct_DeliveredQty ) diff_cnf_dlvr,
				f_so_sq_001.ct_ConfirmedQty sum_prev_diff_cnf_dlvr,
				row_number() over(ORDER BY f_so_sq_001.Dim_Partid, d_GoodsIssue_sq_001.DateValue, f_so_sq_001.dd_SalesDocNo, f_so_sq_001.dd_SalesItemNo,
										   f_so_sq_001.dd_ScheduleNo) sr_no
FROM fact_salesorder f_so_sq_001
		inner join dim_date d_GoodsIssue_sq_001	on f_so_sq_001.Dim_DateidGoodsIssue = d_GoodsIssue_sq_001.Dim_Dateid
where    f_so_sq_001.Dim_SalesOrderRejectReasonid = 1
	 and f_so_sq_001.dd_ItemRelForDelv = 'X'
	 and not exists ( select 1 from fact_salesorder_720 v where v.v_dim_doccategoryid  = f_so_sq_001.Dim_DocumentCategoryid )
ORDER BY f_so_sq_001.Dim_Partid,d_GoodsIssue_sq_001.DateValue,f_so_sq_001.dd_SalesDocNo , f_so_sq_001.dd_SalesItemNo,
		 f_so_sq_001.dd_ScheduleNo;

UPDATE tmp_fillrate1
set sum_prev_diff_cnf_dlvr = 0;

drop table if exists tmp_fillrate1b;
create table tmp_fillrate1b
as
select Dim_Partid,sr_no,diff_cnf_dlvr
from tmp_fillrate1
where diff_cnf_dlvr > 0;

drop table if exists tmp_fillrate1a;
create table tmp_fillrate1a
AS
SELECT b.Dim_Partid,a.sr_no,sum(b.diff_cnf_dlvr) as sum_diff_cnf_dlvr
from tmp_fillrate1 a, tmp_fillrate1b b
where a.Dim_Partid = b.Dim_Partid
and b.sr_no < a.sr_no
GROUP BY b.dim_partid,a.sr_no;

update tmp_fillrate1 a
set sum_prev_diff_cnf_dlvr = b.sum_diff_cnf_dlvr
from tmp_fillrate1a b, tmp_fillrate1 a
where a.Dim_Partid = b.Dim_Partid
and b.sr_no = a.sr_no
AND  sum_prev_diff_cnf_dlvr<>  b.sum_diff_cnf_dlvr;

drop table if exists tmp_fillrate1a;
drop table if exists tmp_fillrate1b;


DROP TABLE IF EXISTS tmp_fillrate2;
create table tmp_fillrate2
as
select f_inv_sq_001.dim_Partid,sum(f_inv_sq_001.ct_StockQty + f_inv_sq_001.ct_BlockedStock
                  + f_inv_sq_001.ct_StockInQInsp + f_inv_sq_001.ct_StockInTransfer
                  + f_inv_sq_001.ct_TotalRestrictedStock) sum_ivaging
FROM fact_inventoryaging f_inv_sq_001
GROUP BY f_inv_sq_001.dim_Partid;

merge into fact_salesorder_720 fact
using (select f.v_Fact_SalesOrderid, ifnull(f_inv_sq_001.sum_ivaging,0) - ifnull(sum_prev_diff_cnf_dlvr,0) as v_ct_OnHandQty
		from fact_salesorder_720 f
				left join tmp_fillrate2 f_inv_sq_001 on f_inv_sq_001.dim_Partid = f.v_dim_part_id
				left join tmp_fillrate1 f_so_sq_001 on    f_so_sq_001.Dim_Partid = f.v_dim_part_id
													  AND f_so_sq_001.DateValue = f.v_goods_issue_date
													  AND f_so_sq_001.dd_SalesDocNo = f.v_dd_SalesDocNo
													  AND f_so_sq_001.dd_SalesItemNo = f.v_dd_SalesItemNo
													  AND f_so_sq_001.dd_ScheduleNo = f.v_dd_ScheduleNo
		WHERE    v_ct_OnHandQty = 0 
			 AND v_goods_issue_date >= current_date) src on fact.v_Fact_SalesOrderid = src.v_Fact_SalesOrderid
when matched then update set fact.v_ct_OnHandQty = src.v_ct_OnHandQty
where fact.v_ct_OnHandQty <> src.v_ct_OnHandQty;

Update fact_salesorder_720
Set v_ct_OnHandQty = (case when v_ct_OnHandQty < 0 then 0 when v_ct_OnHandQty > v_TotalQty then v_TotalQty else v_ct_OnHandQty end)
Where iflag=1;

UPDATE fact_salesorder
SET   ct_OnHandQty = v_ct_OnHandQty
FRom fact_salesorder_720, fact_salesorder
WHERE Fact_SalesOrderid = v_Fact_SalesOrderid
AND iflag=1
AND ct_OnHandQty <> v_ct_OnHandQty;

drop table if exists fact_salesorder_720;

Create table fact_salesorder_720(
  v_Fact_SalesOrderid   integer null,
  v_dd_SalesDocNo       varchar(50) null,
  v_dd_SalesItemNo      integer null,
  v_dd_ScheduleNo       integer null,
  v_dim_doccategoryid   integer null,
  v_dim_part_id         integer null,
  v_cust_req_date    date null,
  v_ct_FillQty_CRD      decimal(18,4) null,
  v_TotalQty            decimal(18,4) null,
  iflag                 integer default 0,
  sr_no         int null);

Insert into fact_salesorder_720(
  v_Fact_SalesOrderid,
  v_dd_SalesDocNo ,
  v_dd_SalesItemNo,
  v_dd_ScheduleNo,
  v_dim_part_id,
  v_cust_req_date,
  v_ct_FillQty_CRD,
  v_TotalQty
  )
  SELECT Fact_SalesOrderid v_Fact_SalesOrderid,
         dd_SalesDocNo v_dd_SalesDocNo ,
         dd_SalesItemNo v_dd_SalesItemNo,
         dd_ScheduleNo v_dd_ScheduleNo,
         dim_partid v_dim_part_id,
         dt.DateValue v_cust_req_date,
         ct_FillQty_CRD v_ct_FillQty_CRD,
         ct_ConfirmedQty v_TotalQty
  FROM fact_salesorder f
      INNER JOIN dim_date dt ON dt.dim_dateid = f.Dim_DateidSchedDeliveryReq
      INNER JOIN dim_date adt ON adt.dim_dateid = f.Dim_DateidActualGI
      inner join dim_documentcategory dg on dg.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
      inner join Dim_ScheduleLineCategory slc on f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
  WHERE (dt.DateValue = current_date OR adt.DateValue = current_date
            OR ((dt.DateValue < current_date OR adt.DateValue < current_date) AND ct_FillQty_CRD = 0))
        AND f.Dim_SalesOrderRejectReasonid = 1 and dg.DocumentCategory <> 'H'
        AND f.dd_ItemRelForDelv = 'X'
  ORDER BY dt.DateValue DESC, dd_SalesDocNo, dd_SalesItemNo,dd_ScheduleNo;


Update fact_salesorder_720
SET v_dim_doccategoryid = dg.Dim_DocumentCategoryid
from dim_documentcategory dg, fact_salesorder_720
where dg.DocumentCategory = 'H'
AND v_dim_doccategoryid <> dg.Dim_DocumentCategoryid;

drop table if exists tmp1_exists_f_sod;
create table tmp1_exists_f_sod
as
select f_sod.dd_SalesDocNo,f_sod.dd_SalesItemNo,f_sod.dd_ScheduleNo,d_gi.dim_dateid,d_gi.datevalue,d_crd.datevalue crd_datevalue,f_sod.ct_QtyDelivered
FROM fact_salesorderdelivery f_sod
                      INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGI_Original
                      INNER JOIN dim_date d_crd ON d_crd.Dim_Dateid = f_sod.Dim_DateidSchedDeliveryReq
                      inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.GoodsMovementStatus <> 'Not yet processed';


drop table if exists fact_salesorder_720_a;
create table fact_salesorder_720_a
as
select v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_cust_req_date,sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END) ct_QtyDelivered_sum
from tmp1_exists_f_sod f_sod,fact_salesorder_720
WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue > v_cust_req_date) or (f_sod.dim_dateid = 1 and f_sod.crd_datevalue > v_cust_req_date))
group by v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_cust_req_date;


/*UPDATE fact_salesorder f
From dim_salesorderitemstatus s,fact_salesorder_720 x
        SET   ct_FillQty_CRD = f.ct_ConfirmedQty - ifnull((SELECT f_sod.ct_QtyDelivered_sum
                                            FROM    fact_salesorder_720_a f_sod
                                            WHERE     f_sod.v_dd_SalesDocNo = x.v_dd_SalesDocNo
                                                  AND f_sod.v_dd_SalesItemNo = x.v_dd_SalesItemNo
                                                  AND f_sod.v_dd_ScheduleNo = x.v_dd_ScheduleNo
                                                AND f_sod.v_cust_req_date = x.v_cust_req_date),0)
where s.OverallDeliveryStatus = 'Completely processed'
and exists (SELECT 1 FROM tmp1_exists_f_sod f_sod
                 WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                      AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                      AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue <= v_cust_req_date) or (f_sod.dim_dateid = 1 and f_sod.crd_datevalue <= v_cust_req_date)))
AND  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty_CRD =0*/

DROP TABLE IF EXISTS tmp_fact_salesorder_ct_FillQty_CRD;
CREATE TABLE tmp_fact_salesorder_ct_FillQty_CRD
AS
SELECT f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo, f.ct_ConfirmedQty,
	   ifnull(f_sod.ct_QtyDelivered_sum,0) ct_QtyDelivered_sum
FROM fact_salesorder f
		inner join fact_salesorder_720 x on f.Fact_SalesOrderid = x.v_Fact_SalesOrderid
						left join fact_salesorder_720_a f_sod on    f_sod.v_dd_SalesDocNo = x.v_dd_SalesDocNo
																AND f_sod.v_dd_SalesItemNo = x.v_dd_SalesItemNo
																AND f_sod.v_dd_ScheduleNo = x.v_dd_ScheduleNo
																AND f_sod.v_cust_req_date = x.v_cust_req_date
		inner join dim_salesorderitemstatus s on f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
where    s.OverallDeliveryStatus = 'Completely processed'
	 and x.v_ct_FillQty_CRD = 0
	 and exists (SELECT 1 FROM tmp1_exists_f_sod f_sod
                 WHERE     f_sod.dd_SalesDocNo = x.v_dd_SalesDocNo
                      AND f_sod.dd_SalesItemNo = x.v_dd_SalesItemNo
                      AND f_sod.dd_ScheduleNo = x.v_dd_ScheduleNo
					  AND (    (f_sod.dim_dateid <> 1 and f_sod.datevalue <= x.v_cust_req_date) 
							or (f_sod.dim_dateid = 1 and f_sod.crd_datevalue <= x.v_cust_req_date)));


UPDATE fact_salesorder f
SET ct_FillQty = c.ct_ConfirmedQty - c.ct_QtyDelivered_sum
FROM tmp_fact_salesorder_ct_FillQty_CRD c, fact_salesorder f
WHERE f.dd_SalesDocNo = c.dd_SalesDocNo
AND f.dd_SalesItemNo = c.dd_SalesItemNo
AND f.dd_ScheduleNo = c.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_fact_salesorder_ct_FillQty_CRD;

drop table if exists tmp_fact_salesorder_t001_1;
create table tmp_fact_salesorder_t001_1 as
SELECT x.v_Fact_SalesOrderid, 
	   sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END) as ct_FillQty_CRD
FROM fact_salesorderdelivery f_sod
		inner join fact_salesorder_720 x on    f_sod.dd_SalesDocNo = v_dd_SalesDocNo
										   AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
										   AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
		INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGI_Original
		INNER JOIN dim_date d_crd ON d_crd.Dim_Dateid = f_sod.Dim_DateidSchedDeliveryReq
		INNER JOIN dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
WHERE     sois.GoodsMovementStatus <> 'Not yet processed'
	 AND (   (d_gi.dim_dateid <> 1 and d_gi.datevalue <= x.v_cust_req_date) 
		  or (d_gi.dim_dateid = 1 and d_crd.datevalue <= x.v_cust_req_date))
group by x.v_Fact_SalesOrderid;

merge into fact_salesorder fact
using (select f.fact_salesorderid, ifnull(t1.ct_FillQty_CRD, 0) ct_FillQty_CRD
	   from fact_salesorder f
				inner join fact_salesorder_720 t0 on f.Fact_SalesOrderid = t0.v_Fact_SalesOrderid
								left join tmp_fact_salesorder_t001_1 t1 on t0.v_Fact_SalesOrderid = t1.v_Fact_SalesOrderid
				inner join dim_salesorderitemstatus s on f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
	   where     t0.v_ct_FillQty_CRD = 0
			 and (     s.OverallDeliveryStatus <> 'Completely processed'
					or not exists (SELECT 1 
								   FROM fact_salesorderdelivery f_sod1
											  INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod1.Dim_DateidActualGI_Original
											  INNER JOIN dim_date d_crd ON d_crd.Dim_Dateid = f_sod1.Dim_DateidSchedDeliveryReq
											  inner join dim_salesorderitemstatus sois on f_sod1.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
								   WHERE    f_sod1.dd_SalesDocNo = t0.v_dd_SalesDocNo
									    AND f_sod1.dd_SalesItemNo = t0.v_dd_SalesItemNo
									    AND f_sod1.dd_ScheduleNo = t0.v_dd_ScheduleNo
										AND sois.GoodsMovementStatus <> 'Not yet processed'
										AND (   (d_gi.dim_dateid <> 1 and d_gi.datevalue <= t0.v_cust_req_date)
											 or (d_gi.dim_dateid = 1 and d_crd.datevalue <= t0.v_cust_req_date))))			 
	  ) src on fact.fact_salesorderid = src.fact_salesorderid
when matched then update set fact.ct_FillQty_CRD = src.ct_FillQty_CRD
where fact.ct_FillQty_CRD <> src.ct_FillQty_CRD;

drop table if exists tmp_fact_salesorder_t001_1;

drop table if exists fact_salesorder_720;
drop table if exists tmp1_exists_f_sod;
drop table if exists fact_salesorder_720_a;
