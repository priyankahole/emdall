/* Run in this order : vw_bi_populate_shortage_fact.part1.sql, vw_getstdprice_custom.fact_shortage.sql,
	vw_getstdprice_std.part1.sql, vw_funct_fiscal_year.custom.getStdPrice.sql,vw_funct_fiscal_year.standard.sql, vw_getstdprice_std.part2.sql,
		vw_bi_populate_shortage_fact.part2.sql */

DROP TABLE IF EXISTS tmp_resb_for_shortagefact;
create table tmp_resb_for_shortagefact
as
select RESB_RSNUM,RESB_RSPOS,min(rb.RESB_BDTER) as min_RESB_BDTER
FROM RESB rb
	INNER JOIN fact_shortage_temp s ON  rb.RESB_RSNUM = s.dd_ReservationNo
					AND rb.RESB_RSPOS = s.dd_ReservationItemNo
WHERE s.dd_ReservationNo <> 0 
AND s.dd_ReservationItemNo <> 0
GROUP BY RESB_RSNUM,RESB_RSPOS;

/*START 08-12-2016 Cristian Cleciu: Optimized MERGE statement with temp tables*/
DROP TABLE IF EXISTS tmp_T0;
CREATE TABLE tmp_T0 AS  
(SELECT fs1.fact_shortageid,
                      pl.companycode,
                      pl.plantcode,
                      r.resb_matnr,
                      dt.financialyear,
                      dt.financialmonthnumber,
                      r.resb_umrez,
                      r.resb_umren,
                      IFNULL(( CASE
                                 WHEN IFNULL(r.resb_peinh, 0) = 0 THEN 1
                                 ELSE r.resb_peinh
                               END ), 0) AS unitprice,
                      pl.rowiscurrent
               FROM   fact_shortage_temp fs1
                      INNER JOIN resb r
                              ON ( fs1.dd_reservationno = r.resb_rsnum
                                   AND fs1.dd_reservationitemno = r.resb_rspos )
                      INNER JOIN dim_plant pl
                              ON ( pl.plantcode = r.resb_werks )
                      LEFT JOIN tmp_resb_for_shortagefact tmp
                             ON tmp.resb_rsnum = fs1.dd_reservationno
                                AND tmp.resb_rspos = fs1.dd_reservationitemno
                      INNER JOIN dim_date dt
                              ON dt.companycode = pl.companycode
                                 AND dt.datevalue = tmp.min_resb_bdter
               WHERE  pl.rowiscurrent = 1
                      AND fs1.dd_reservationno <> 0
                      AND fs1.dd_reservationitemno <> 0);
					  
DROP TABLE IF EXISTS tmp_src;					  
CREATE TABLE tmp_src AS 
(SELECT MAX(z.standardprice) StandardPrice,
              tmp_T0.fact_shortageid
       FROM    tmp_T0
              LEFT JOIN tmp_getstdprice z
                     ON z.pcompanycode = tmp_T0.companycode
                        AND z.pplant = tmp_T0.plantcode
                        AND z.pmaterialno = tmp_T0.resb_matnr
                        AND z.pfiyear = tmp_T0.financialyear
                        AND z.pperiod = tmp_T0.financialmonthnumber
                        AND z.vumrez = tmp_T0.resb_umrez
                        AND z.vumren = tmp_T0.resb_umren
                        AND z.punitprice = tmp_T0.unitprice
       WHERE  z.fact_script_name = 'bi_populate_shortage_fact'
              AND z.ponumber IS NULL
              AND tmp_T0.rowiscurrent = 1
       GROUP  BY tmp_T0.fact_shortageid);
	   
MERGE INTO fact_shortage_temp FA
USING  tmp_src
ON FA.fact_shortageid = tmp_src.fact_shortageid
WHEN matched THEN
UPDATE SET FA.amt_stdprice = IFNULL(tmp_src.standardprice, 0)
WHERE fa.amt_stdprice <> IFNULL(tmp_src.standardprice, 0);

DROP TABLE IF EXISTS tmp_T0;
DROP TABLE IF EXISTS tmp_src;
/*END 08-12-2016 Cristian Cleciu: Optimized MERGE statement with temp tables*/


DROP TABLE IF EXISTS tmp_resb_for_shortagefact;
/*DROP TABLE IF EXISTS tmp_resb_for_shortagefact_2 */

UPDATE 
fact_shortage_temp s 
SET s.amt_StdPrice = fp.amt_StdUnitPrice
from
fact_shortage_temp s ,
fact_purchase fp
WHERE      
s.dd_DocumentNo = fp.dd_DocumentNo
AND s.dd_DocumentItemNo = fp.dd_DocumentItemNo
AND s.dd_ScheduleNo = fp.dd_ScheduleNo
AND s.dim_ComponentId = fp.Dim_Partid
AND ifnull(s.amt_StdPrice,-1) <> ifnull(fp.amt_StdUnitPrice,-2) ;

	   
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1_pre;
CREATE TABLE TMP_FS_fact_inventoryaging1_pre
AS	
SELECT 	 
inv.dim_PartId,
inv.dim_Plantid,
inv.dim_stockcategoryid,
inv.ct_StockQty + inv.ct_StockInQInsp + inv.ct_BlockedStock + inv.ct_StockInTransfer as upd_dd_OnHandQty,
inv.ct_StockQty as upd_dd_UnrestrictedStockQty
FROM 
fact_inventoryaging inv
WHERE 
EXISTS ( SELECT 1 from fact_shortage_temp s
WHERE s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2);
	
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
CREATE TABLE TMP_FS_fact_inventoryaging1
AS
SELECT 
inv.dim_PartId,
inv.dim_Plantid,
inv.dim_stockcategoryid,
SUM(upd_dd_OnHandQty) as sum_upd_dd_OnHandQty,
SUM(inv.upd_dd_UnrestrictedStockQty) sum_upd_dd_UnrestrictedStockQty
FROM TMP_FS_fact_inventoryaging1_pre inv
GROUP BY  inv.dim_PartId,inv.dim_Plantid,inv.dim_stockcategoryid;

UPDATE fact_shortage_temp s
   SET s.dd_OnHandQty = 0
WHERE IFNULL(dd_OnHandQty,-1) <> IFNULL(dd_OnHandQty,-2)   ;

UPDATE fact_shortage_temp s
   SET s.dd_UnrestrictedStockQty = 0
WHERE IFNULL(dd_UnrestrictedStockQty,-1) <> IFNULL(dd_UnrestrictedStockQty,-2)   ;
   
UPDATE fact_shortage_temp s
SET s.dd_OnHandQty = sum_upd_dd_OnHandQty
FROM 
fact_shortage_temp s,
TMP_FS_fact_inventoryaging1 inv
WHERE     
s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2
AND s.dd_OnHandQty  <> sum_upd_dd_OnHandQty;


UPDATE 
fact_shortage_temp s
SET s.dd_UnrestrictedStockQty = sum_upd_dd_UnrestrictedStockQty
FROM 
fact_shortage_temp s,
TMP_FS_fact_inventoryaging1 inv
WHERE     
s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2
AND s.dd_UnrestrictedStockQty  <> sum_upd_dd_UnrestrictedStockQty;
					 
	
DROP TABLE IF EXISTS TMP_FS_MARD;
CREATE TABLE TMP_FS_MARD
AS	
SELECT 
m.MARD_MATNR,
m.MARD_WERKS,
sum(ifnull(m.MARD_LABST, 0)) as upd_dd_StockQty
FROM  
MARD m
WHERE 
EXISTS ( 
select 1 
from 	
fact_Shortage s, 
dim_part pt, 
dim_plant pl
WHERE 	
m.MARD_MATNR = pt.PartNumber
AND m.MARD_WERKS = pt.Plant				 
AND s.Dim_ComponentId = pt.Dim_Partid
AND pt.RowIsCurrent = 1
AND s.Dim_ComponentPlantid = pl.Dim_Plantid
AND pl.RowIsCurrent = 1
AND pt.Plant = pl.PlantCode 
)
GROUP BY m.MARD_MATNR,m.MARD_WERKS;
						 
UPDATE fact_shortage_temp s
   SET s.dd_StockQty = 0
WHERE IFNULL(dd_StockQty,-1) <> IFNULL(dd_StockQty,-2)   ;
             
UPDATE 
fact_shortage_temp s 
SET s.dd_StockQty =           upd_dd_StockQty
from 
fact_shortage_temp s ,
dim_part pt, 
dim_plant pl,
TMP_FS_MARD m
WHERE     s.Dim_ComponentId = pt.Dim_Partid
AND pt.RowIsCurrent = 1
AND s.Dim_ComponentPlantid = pl.Dim_Plantid
AND pl.RowIsCurrent = 1
AND pt.Plant = pl.PlantCode
AND m.MARD_MATNR = pt.PartNumber 
AND m.MARD_WERKS = pt.Plant;


DELETE FROM fact_shortage_temp
WHERE dd_ReservationNo <> 0 
AND dd_ReservationItemNo <> 0 
 AND EXISTS
          (SELECT 1
             FROM RESB r
            WHERE     r.RESB_RSNUM = dd_ReservationNo
                  AND r.RESB_RSPOS = dd_ReservationItemNo
                  AND r.RESB_XLOEK = 'X'); 
				  
drop table if exists fact_shortage;
rename table fact_shortage_temp to fact_shortage;				  

DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1_pre;
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
DROP TABLE IF EXISTS TMP_FS_MARD;
drop table if exists fact_shortage_temp;
DROP TABLE IF EXISTS fact_shortage_temp_1;
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
DROP TABLE IF EXISTS TMP_FS_MARD;