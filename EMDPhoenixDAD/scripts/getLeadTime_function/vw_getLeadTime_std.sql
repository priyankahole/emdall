/* #########################################################################################################################################	*/
/* */
/*   Script         : vw_getLeadTime_std.sql */
/*   Author         : Lokesh */
/*   Created On     : 22 Jul 2013 */
/*  */
/*  */
/*   Description    : getLeadTime function migrated from MySQL  */
/* */
/*   Change History */
/*   Date            By        Version            Desc 		
/*   29-Jul-2013     Lokesh      1.1              																									*/
/*   22-Jul-2013     Lokesh      1.0              Standard function for getLeadTime in VW ( Created from Ashu's migrated script vw_getLeadTime) */
/*                                                Run this after running the fact-specific script that populates tmp_getLeadTime 	*/
/* ##########################################################################################################################################					*/

UPDATE tmp_getLeadTime
set processed_flag = 'Y'
WHERE processed_flag IS NULL;       /* Only pick up the rows which are not processed already */ 
 
Update tmp_getLeadTime 
SET v_leadTime = 0,
    idone=0
WHERE processed_flag = 'Y'	;

Update tmp_getLeadTime
Set v_leadTime = 0,
    idone=1
where pPart IS NULL OR pPlant IS NULL
AND processed_flag = 'Y';

Update tmp_getLeadTime t1
Set v_leadTime = ifnull(ekt.EKPO_PLIFZ, 0) 
FROM tmp_getLeadTime t1
		LEFT JOIN (SELECT distinct t2.EKPO_PLIFZ, T2.EKPO_EBELN, T2.EKPO_EBELP
				   FROM EKKO_EKPO_EKET T2) ekt
				ON ekt.EKPO_EBELN = t1.DocumentNumber AND ekt.EKPO_EBELP = t1.DocumentLineNumber
Where     t1.DocumentNumber IS NOT NULL 
	  AND t1.DocumentLineNumber > 0
      AND t1.DocumentType = 'PO'
      AND t1.idone = 0
	  AND t1.processed_flag = 'Y';

Drop table if exists EINE_00e;
Create table EINE_00e as Select T.* 
from EINE T 
ORDER BY EINE_PRDAT DESC, APLFZ DESC;

Update tmp_getLeadTime T1
Set v_leadTime = ifnull(e.APLFZ, 0)
FROM tmp_getLeadTime T1
		LEFT JOIN (SELECT max(t2.APLFZ) APLFZ, T2.EBELN, T2.EBELP
			       FROM EINE_00e T2
				   WHERE ifnull(T2.EINE_ESOKZ, '0') = '0'
				   GROUP BY T2.EBELN, T2.EBELP) e 
				  ON e.EBELN = T1.DocumentNumber AND e.EBELP = T1.DocumentLineNumber
Where     T1.DocumentNumber IS NOT NULL 
	  AND T1.DocumentLineNumber > 0
      AND T1.DocumentType = 'PO'
      AND T1.idone = 0
      AND T1.v_leadTime = 0
	  AND T1.processed_flag = 'Y';


DROP TABLE IF EXISTS tmp_EINE_00e_EINA;
CREATE TABLE tmp_EINE_00e_EINA
AS
SELECT e.APLFZ,b.EKPO_EBELN,b.EKPO_EBELP,e.EBELN,e.EINE_ERDAT,ifnull(b.EKPO_AEDAT, b.EKKO_BEDAT) EKPO_AEDAT,
e.EINE_PRDAT, ifnull(e.EINE_ESOKZ, '0') EINE_ESOKZ
FROM EINE_00e e
  INNER JOIN EINA ea
		  ON e.INFNR = ea.INFNR
	   INNER JOIN EKKO_EKPO_EKET b
		  ON ea.MATNR = b.EKPO_MATNR
			 AND e.WERKS = b.EKPO_WERKS
			 AND ea.LIFNR = b.EKKO_LIFNR
ORDER BY EINE_PRDAT DESC, APLFZ DESC;

Update tmp_getLeadTime t1
Set v_leadTime =  IFNULL(t2.APLFZ, 0)
FROM tmp_getLeadTime t1
		left join (select AVG(e.APLFZ) AS APLFZ, e.EKPO_EBELN, e.EKPO_EBELP
				   from tmp_EINE_00e_EINA e
				   where     e.EINE_ESOKZ = '0'
					     and e.APLFZ <> 999
						 and e.EINE_PRDAT >= e.EKPO_AEDAT
						 and e.EINE_ERDAT <= e.EKPO_AEDAT
						 and e.EBELN IS NULL
                    group by e.EKPO_EBELN, e.EKPO_EBELP) t2
				  ON t2.EKPO_EBELN = t1.DocumentNumber AND t2.EKPO_EBELP = t1.DocumentLineNumber
Where     t1.DocumentNumber IS NOT NULL 
	  AND t1.DocumentLineNumber > 0
      AND t1.DocumentType = 'PO'
      AND t1.idone = 0
      AND t1.v_leadTime = 0 
	  AND t1.processed_flag = 'Y';
	
Update tmp_getLeadTime t1
Set v_leadTime = ifnull(pr.EBAN_PLIFZ, 0)
from tmp_getLeadTime t1
		left join eban pr on pr.EBAN_BANFN = t1.DocumentNumber AND pr.EBAN_BNFPO = t1.DocumentLineNumber
Where     t1.DocumentType = 'PR'
      AND t1.idone = 0
      AND t1.v_leadTime = 0
	  AND t1.processed_flag = 'Y';

Update tmp_getLeadTime t1
Set v_leadTime = ifnull(t2.aplfz, 0)
from tmp_getLeadTime t1
		left join (select e.aplfz, pr.EBAN_BANFN, pr.EBAN_BNFPO
			       from eine e inner join eban pr on e.INFNR = pr.EBAN_INFNR
				   where e.APLFZ <> 999) t2
				  on t2.EBAN_BANFN = t1.DocumentNumber AND t2.EBAN_BNFPO = t1.DocumentLineNumber
Where     t1.DocumentType = 'PR'
      AND t1.idone = 0
      AND t1.v_leadTime = 0
	  AND t1.processed_flag = 'Y';


UPDATE tmp_getLeadTime glt
SET v_leadTime = IFNULL(t2.APLFZ,0)
FROM tmp_getLeadTime glt
        LEFT JOIN (SELECT max(e.APLFZ) APLFZ, 
						  a.MATNR, e.WERKS,
						  max(pr.EBAN_LIFNR) EBAN_LIFNR, max(pr.EBAN_FLIEF) EBAN_FLIEF, 
						  a.LIFNR 
			       FROM eina a
							INNER JOIN eine e ON a.INFNR = e.INFNR
							INNER JOIN eban pr ON    pr.EBAN_MATNR = a.MATNR
												 AND pr.EBAN_WERKS = e.WERKS
				   WHERE e.APLFZ <> 999
						 AND e.EINE_PRDAT >= pr.EBAN_BADAT
						 AND e.EINE_ERDAT <= pr.EBAN_BADAT
						 AND IFNULL(e.EINE_ESOKZ, '0') = '0'
				   GROUP BY a.MATNR, e.WERKS,a.LIFNR) t2
				  ON     glt.pPart = t2.MATNR
					 AND glt.pPlant = t2.WERKS
					 AND glt.pVendor = t2.LIFNR
					 AND ((glt.pVendor = t2.EBAN_LIFNR AND t2.EBAN_LIFNR IS NOT NULL) OR 
					      (glt.pVendor = t2.EBAN_FLIEF AND t2.EBAN_FLIEF IS NOT NULL))
WHERE     glt.DocumentType = 'PR'
      AND glt.idone = 0
      AND glt.v_leadTime = 0
	  AND glt.processed_flag = 'Y'
	  AND glt.pVendor IS NOT NULL;


Update tmp_getLeadTime t1
Set v_leadTime = ifnull(t2.APLFZ, 0)
from tmp_getLeadTime t1
		left join (select e.APLFZ, a.MATNR, e.WERKS, a.LIFNR 
				   from eina a, eine e, plaf p
				   where     a.INFNR = e.INFNR
					     and p.PLAF_MATNR = a.MATNR
						 AND p.PLAF_PLWRK = e.WERKS
						 AND e.EINE_ERDAT <= p.PLAF_PERTR
						 AND e.EINE_PRDAT >= p.PLAF_PERTR
						 AND e.APLFZ <> 999
                         AND ifnull(e.EINE_ESOKZ, '0') = '0') t2
				  on    t2.MATNR = t1.pPart 
				    AND t2.WERKS = t1.pPlant 
					AND t2.LIFNR = t1.pVendor
Where DocumentType = 'PLAN_ORDER'
      AND idone = 0
      AND v_leadTime = 0
	  AND processed_flag = 'Y'
	  AND pVendor IS NOT NULL;


Update tmp_getLeadTime t1
Set v_leadTime = IFNULL(p.MARC_PLIFZ, 0)
from tmp_getLeadTime t1
		left join (select MARA_MATNR, MARC_WERKS, min(MARC_PLIFZ) MARC_PLIFZ
		           from MARA_MARC_MAKT
				   group by MARA_MATNR, MARC_WERKS) p on p.MARA_MATNR = t1.pPart AND p.MARC_WERKS = t1.pPlant
Where     t1.idone = 0
      AND t1.v_leadTime = 0
	  AND t1.processed_flag = 'Y'
	  AND v_leadTime <> IFNULL(p.MARC_PLIFZ, 0);

Update tmp_getLeadTime t1
Set t1.v_leadTime = IFNULL(t2.PLIFZ,0)
from tmp_getLeadTime t1
		left join (select m.PLIFZ, m.LIFNR, dp.PlantCode
				   from LFM1 m, dim_plant dp
				   where     m.EKORG = dp.PurchOrg
					     AND m.PLIFZ <> 999) t2
				   on t2.LIFNR = t1.pVendor and t2.PlantCode = t1.pPlant
Where     t1.idone = 0
      AND t1.v_leadTime = 0
	  AND t1.processed_flag = 'Y'
	  AND t1.pVendor IS NOT NULL;
	  

Drop table if exists EINE_00e;
DROP TABLE IF EXISTS tmp_EINE_00e_EINA;
