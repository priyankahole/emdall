

/**************************************************************************************************************/
/*   Script         : vw_getstdprice.fact_excessandshortage.sql	 */
/*   Author         : Lokesh */
/*   Created On     : 5 Aug 2013 */
/*   Description    : Function to populate tmp_getStdPrice for getstdprice  */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   5 Aug 2013      Lokesh    1.0               New script                          */
/******************************************************************************************************************/

/* Stdprice script for fact_excessandshortage starts here */

/* Populate tmp_getStdPrice  This will change for each proc where this function is required*/

DROP TABLE IF EXISTS tmp_getStdPrice_fact_ex_sh;
CREATE TABLE tmp_getStdPrice_fact_ex_sh 
as
SELECT * FROM tmp_getStdPrice
WHERE 1=2;


INSERT INTO tmp_getStdPrice_fact_ex_sh
(
 pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
SELECT 
pl.CompanyCode,
pl.PlantCode,
MDKP_MATNR,
dd.CalendarYear,
dd.FinancialMonthNumber,
PLAF_UMREZ,
PLAF_UMREN,
null,
0,
'fact_excessandshortage'
FROM  plaf po,
       mdkp k,
       mdtb t,
       dim_vendor v,
       dim_vendor fv,
       dim_consumptiontype ct,
       dim_part p,
       dim_plant pl,
       Dim_Date dd,
	   fact_excessandshortage m
WHERE     m.Dim_ActionStateid = 2
AND k.MDKP_DTNUM = t.MDTB_DTNUM
AND dd.Dim_Dateid <> 1
AND m.dd_DocumentNo = po.PLAF_PLNUM
AND m.dd_DocumentNo = t.MDTB_DELNR
AND p.Dim_Partid = m.Dim_Partid
AND pl.PlantCode = p.Plant
AND v.VendorNumber = ifnull(PLAF_EMLIF, 'Not Set')
AND v.RowIsCurrent = 1
AND fv.VendorNumber = ifnull(PLAF_FLIEF, 'Not Set')
AND fv.RowIsCurrent = 1
AND ct.ConsumptionCode = ifnull(po.PLAF_KZVBR, 'Not Set')
AND ct.RowIsCurrent = 1
AND dd.DateValue = ifnull(MDTB_UMDAT, MDTB_DAT01)
AND ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL
AND MDTB_DAT02 IS NOT NULL
AND ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL
AND dd.CompanyCode = pl.CompanyCode;		 
	
INSERT INTO tmp_getStdPrice
SELECT DISTINCT * FROM tmp_getStdPrice_fact_ex_sh;

DROP TABLE IF EXISTS tmp_getStdPrice_fact_ex_sh;					  


UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE fact_script_name = 'fact_excessandshortage';


