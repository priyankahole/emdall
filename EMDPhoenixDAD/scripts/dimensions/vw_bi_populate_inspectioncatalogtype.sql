UPDATE    dim_inspectioncatalogtype  
   SET inspectioncatalogtypename = TQ15T_katalogtxt,
			dw_update_date = current_timestamp
FROM TQ15T, dim_inspectioncatalogtype
   WHERE inspectioncatalogtypecode = tq15t_katalogart
        and inspectioncatalogtypekey = tq15t_schlagwort
        and RowIsCurrent = 1;

INSERT INTO dim_inspectioncatalogtype(Dim_inspectioncatalogtypeid,RowIsCurrent,rowstartdate)
SELECT 1,1,current_timestamp
FROM (SELECT 1) D
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectioncatalogtype
               WHERE Dim_inspectioncatalogtypeid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectioncatalogtype';
   
insert into number_fountain
select 	'dim_inspectioncatalogtype',
	ifnull(max(d.Dim_inspectioncatalogtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectioncatalogtype d
where d.Dim_inspectioncatalogtypeid <> 1; 

INSERT INTO dim_inspectioncatalogtype(Dim_inspectioncatalogtypeid,
                                          inspectioncatalogtypecode,
                                          inspectioncatalogtypename,
                                          inspectioncatalogtypekey,
                                          RowStartDate,
                                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectioncatalogtype')
          + row_number() over(order by '') ,
                        t.TQ15T_katalogart,
          t.TQ15T_katalogtxt,
                t.TQ15T_schlagwort,
          current_timestamp,
          1
     FROM TQ15T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectioncatalogtype c
               WHERE c.inspectioncatalogtypename = t.TQ15T_KATALOGTXT
                     AND c.RowIsCurrent = 1);

