truncate table distinct_TQ30T;
insert into distinct_TQ30T
 select t.TQ30T_ART, t.TQ30T_LTXA1, t.TQ30T_KURZTEXT, t.TQ30T_SPRACHE,
row_number() over (partition by TQ30T_ART order by TQ30T_SPRACHE desc) as rowno
from TQ30T_all t;

truncate table TQ30T;
insert into TQ30T 
   select TQ30T_ART, TQ30T_LTXA1, TQ30T_KURZTEXT from distinct_TQ30T where rowno = 1;

UPDATE    dim_inspectiontype it
     SET it.InspectionTypeName = t.TQ30T_LTXA1,
       it.InspectionTypeText = t.TQ30T_KURZTEXT,
			it.dw_update_date = current_timestamp
	 FROM
          dim_inspectiontype it JOIN TQ30T t
     ON it.InspectionTypeCode = t.TQ30T_ART
     AND it.RowIsCurrent = 1;
	 
INSERT INTO dim_inspectiontype(dim_inspectiontypeid, RowIsCurrent,inspectiontypecode,inspectiontypename,inspectiontypetext,rowstartdate)
SELECT 1, 1,'Not Set','Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectiontype
               WHERE dim_inspectiontypeid = 1);
delete from number_fountain m where m.table_name = 'dim_inspectiontype';
   
insert into number_fountain
select 	'dim_inspectiontype',
	ifnull(max(d.dim_InspectionTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectiontype d
where d.dim_InspectionTypeid <> 1; 

INSERT INTO dim_inspectiontype(dim_InspectionTypeid,
                               InspectionTypeCode,
                               InspectionTypeName,
                               InspectionTypeText,
                               RowStartDate,
                               RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectiontype')
          + row_number() over(order by '') ,
                        t.TQ30T_ART,
          t.TQ30T_LTXA1,
          t.TQ30T_KURZTEXT,
          current_timestamp,
          1
     FROM TQ30T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectiontype it
               WHERE it.InspectionTypeCode = t.TQ30T_ART
                     AND it.RowIsCurrent = 1);
