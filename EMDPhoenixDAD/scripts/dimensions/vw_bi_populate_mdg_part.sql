/**********************************************************************************/
/*  06 Nov 2015   1.00		  LiviuT     First version */
/*  10 Oct 2016   1.01        CristianT  Removed droping tables with source from EMDTEMPOCC4 schema. Added truncate and insert logic */
/**********************************************************************************/

/* 10 Oct 2016 CristianT Start: Removed droping tables with source from EMDTEMPOCC4 schema. Added truncate and insert logic */
TRUNCATE TABLE MDG_MARA;
INSERT INTO MDG_MARA(
MATNR,
YYD_GLMTY,
YYD_YSBU,
YYD_ABCCL,
BISMT,
MEINS,
MHDHB,
MHDRZ,
MSTAE,
MTART,
MTPOS_MARA,
PRDHA,
XCHPF,
YYD_APORL,
YYD_GLOPH,
YYD_SCPPI,
YYD_SCPST,
YYD_COGSI,
YYD_COMCO,
YYD_PPUNI,
MARA_NTGEW,
MARA_GEWEI,
YYD_YGART,
YYD_YKZLP,
MARA_YYD_AIDOS,
MARA_YYD_AIUNI,
MARA_YYD_RIMIB,
MARA_YYD_IMCOD
)
SELECT MATNR,
       YYD_GLMTY,
       YYD_YSBU,
       YYD_ABCCL,
       BISMT,
       MEINS,
       MHDHB,
       MHDRZ,
       MSTAE,
       MTART,
       MTPOS_MARA,
       PRDHA,
       XCHPF,
       YYD_APORL,
       YYD_GLOPH,
       YYD_SCPPI,
       YYD_SCPST,
       YYD_COGSI,
       YYD_COMCO,
       YYD_PPUNI,
       MARA_NTGEW,
       MARA_GEWEI,
	   YYD_YGART,
	   YYD_YKZLP,
	   MARA_YYD_AIDOS,
           MARA_YYD_AIUNI,
      MARA_YYD_RIMIB,
      MARA_YYD_IMCOD
FROM EMDTempoCC4.MDG_MARA;

TRUNCATE TABLE MDG_T9D241T;
INSERT INTO MDG_T9D241T(
YYD_GLMTY,
YYD_TEXT
)
SELECT YYD_GLMTY,
       YYD_TEXT
FROM EMDTempoCC4.MDG_T9D241T;

TRUNCATE TABLE MDG_T141T;
INSERT INTO MDG_T141T(
MMSTA,
MTSTB
)
SELECT MMSTA,
       MTSTB
FROM EMDTempoCC4.MDG_T141T;

TRUNCATE TABLE MDG_T134T;
INSERT INTO MDG_T134T(
T134T_MANDT,
T134T_SPRAS,
T134T_MTART,
T134T_MTBEZ
)
SELECT T134T_MANDT,
       T134T_SPRAS,
       T134T_MTART,
       T134T_MTBEZ
FROM EMDTempoCC4.MDG_T134T;

TRUNCATE TABLE MDG_TPTMT;
INSERT INTO MDG_TPTMT(
MTPOS,
BEZEI
)
SELECT MTPOS,
       BEZEI
FROM EMDTempoCC4.MDG_TPTMT;

TRUNCATE TABLE MDG_T179T;
INSERT INTO MDG_T179T(
MDG_T179T_PRODH,
MDG_T179T_SPRAS,
MDG_T179T_VTEXT
)
SELECT MDG_T179T_PRODH,
       MDG_T179T_SPRAS,
       MDG_T179T_VTEXT
FROM EMDTempoCC4.MDG_T179T;

TRUNCATE TABLE MDG_T9D520T;
INSERT INTO MDG_T9D520T(
YYD_APORL,
LTEXT
)
SELECT YYD_APORL,
       LTEXT
FROM EMDTempoCC4.MDG_T9D520T;

TRUNCATE TABLE MDG_T9D335T;
INSERT INTO MDG_T9D335T(
MDG_T9D335T_LTEXT,
MDG_T9D335T_SPRAS,
MDG_T9D335T_YYD_GLOPH
)
SELECT MDG_T9D335T_LTEXT,
       MDG_T9D335T_SPRAS,
       MDG_T9D335T_YYD_GLOPH
FROM EMDTempoCC4.MDG_T9D335T;

TRUNCATE TABLE MDG_YYD_V_MAPPING;
INSERT INTO MDG_YYD_V_MAPPING(
MANDT,
USMDKMMMATERIAL,
USMDTMMMATERIAL,
MD_MMMATERIAL
)
SELECT MANDT,
       USMDKMMMATERIAL,
       USMDTMMMATERIAL,
       MD_MMMATERIAL
FROM EMDTempoCC4.MDG_YYD_V_MAPPING;

TRUNCATE TABLE MDG_YYD_V_YORG;
INSERT INTO MDG_YYD_V_YORG(
MD_MMYYD_ORGA,
MD_MMYYD_YORGA,
MANDT,
USMDKMMMATERIAL,
USMD_ACTIVE,
USMD_O_YORG
)
SELECT MD_MMYYD_ORGA,
       MD_MMYYD_YORGA,
       MANDT,
       USMDKMMMATERIAL,
       USMD_ACTIVE,
       USMD_O_YORG
FROM EMDTempoCC4.MDG_YYD_V_YORG;

TRUNCATE TABLE MDG_T9D519T;
INSERT INTO MDG_T9D519T(
YYD_YORGA,
SPRAS,
LTEXT
)
SELECT YYD_YORGA,
       SPRAS,
       LTEXT
FROM EMDTempoCC4.MDG_T9D519T;

TRUNCATE TABLE MDG_MAKT;
INSERT INTO MDG_MAKT(
MAKT_MATNR,
MAKT_MAKTX
)
SELECT MAKT_MATNR,
       MAKT_MAKTX
FROM EMDTempoCC4.MDG_MAKT;

TRUNCATE TABLE MDG_T9DRIMIBT;
INSERT INTO MDG_T9DRIMIBT(
T9DRIMIBT_YYD_RIMIB ,
T9DRIMIBT_LTEXT
)
SELECT T9DRIMIBT_YYD_RIMIB,
       T9DRIMIBT_LTEXT
FROM EMDTempoCC4.MDG_T9DRIMIBT;

TRUNCATE TABLE MDG_T9D343T;
INSERT INTO MDG_T9D343T(
MDG_T9D343T_YYD_IMCOD ,
MDG_T9D343T_YYD_TEXT
)
SELECT MDG_T9D343T_YYD_IMCOD,
       MDG_T9D343T_YYD_TEXT
FROM EMDTempoCC4.MDG_T9D343T;

/* 10 Oct 2016 CristianT End */

INSERT INTO dim_mdg_part
(
  dim_mdg_partid,
  partnumber,
  product_hierarchy,
  global_operational_hierarchy,
  planned_product_hierarchy,
  product_hierarchy_phoenix,
  price_hierarchy,
  product_hierarchy_description,
  MatTypeforGlbRep,
  GlbProductGroup,
  GlbABCIndicator,
  Oldmatnumber,
  BaseUnitofMeasure,
  TotalShelfLife,
  MinRemainingShelfLife,
  CrossPlantMatStatus,
  MaterialType,
  Generalitemcatgroup,
  Producthierarchy,
  Batchmanreqind,
  APORelevance,
  MatTypeforGlbRepDescr,
  CrossPlantMatStatusDescr,
  MaterialTypeDescr,
  GeneralitemcatgroupDescr,
  ProducthierarchyDescr,
  APORelevanceDesc,
  COGSICMIndicator,
  UnitOfCompleteContentOfThePackage,
  CompleteContentOfThePackage,
  ppu,
  ppu_units,
  act_conv,
  unit2,
  gaussuom,
  gaussuomdesc,
  articlenumber
)
SELECT 1,
       'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   0,
	   0,
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   0,
	   0,
	   'Not Set',
	   0,
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set'
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 'x' FROM dim_mdg_part x WHERE x.dim_mdg_partid = 1);


	
/* Roxana H - 7 Dec 2017 - uncommented the update from below */

update dim_mdg_part
set MatTypeforGlbRep = ifnull(YYD_GLMTY,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(MatTypeforGlbRep,'a') <> ifnull(YYD_GLMTY,'Not Set');
	
update dim_mdg_part
set GlbProductGroup = ifnull(YYD_YSBU,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(GlbProductGroup,'a') <> ifnull(YYD_YSBU,'Not Set');

update dim_mdg_part
set GlbABCIndicator = ifnull(YYD_ABCCL,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(GlbABCIndicator,'a') <> ifnull(YYD_ABCCL,'Not Set');

update dim_mdg_part
set Oldmatnumber = ifnull(BISMT,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(Oldmatnumber,'a') <> ifnull(BISMT,'Not Set');

update dim_mdg_part
set BaseUnitofMeasure = ifnull(MEINS,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(BaseUnitofMeasure,'a') <> ifnull(MEINS,'Not Set');

update dim_mdg_part
set TotalShelfLife = ifnull(MHDHB,0)
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(TotalShelfLife,-1) <> ifnull(MHDHB,0);

update dim_mdg_part
set MinRemainingShelfLife = ifnull(MHDRZ,0)
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(MinRemainingShelfLife,-1) <> ifnull(MHDRZ,0);

update dim_mdg_part
set CrossPlantMatStatus = ifnull(MSTAE,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(CrossPlantMatStatus,'a') <> ifnull(MSTAE,'Not Set');

update dim_mdg_part
set MaterialType = ifnull(MTART,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(MaterialType,'a') <> ifnull(MTART,'Not Set');

update dim_mdg_part
set Generalitemcatgroup = ifnull(MTPOS_MARA,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(Generalitemcatgroup,'a') <> ifnull(MTPOS_MARA,'Not Set');

update dim_mdg_part
set Producthierarchy = ifnull(PRDHA,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(Producthierarchy,'a') <> ifnull(PRDHA,'Not Set');

update dim_mdg_part
set Batchmanreqind = ifnull(XCHPF,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(Batchmanreqind,'a') <> ifnull(XCHPF,'Not Set');

update dim_mdg_part
set APORelevance = ifnull(YYD_APORL,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(APORelevance,'a') <> ifnull(YYD_APORL,'Not Set');

UPDATE dim_mdg_part d
SET d.global_operational_hierarchy = ifnull(m.YYD_GLOPH, 'Not Set'),
    d.dw_update_date = current_timestamp
FROM dim_mdg_part d, MDG_MARA m
WHERE d.partnumber = m.MATNR
      AND d.global_operational_hierarchy <> ifnull(m.YYD_GLOPH, 'Not Set');

update dim_mdg_part
set MatTypeforGlbRepDescr = ifnull(yyd_text,'Not Set')
from dim_mdg_part, MDG_T9D241T
where MatTypeforGlbRep = yyd_glmty
	and ifnull(MatTypeforGlbRepDescr,'a') <> ifnull(yyd_text,'Not Set');

update dim_mdg_part
set CrossPlantMatStatusDescr = ifnull(mtstb,'Not Set')
from dim_mdg_part, MDG_T141T
where CrossPlantMatStatus = mmsta
	and ifnull(CrossPlantMatStatusDescr,'a') <> ifnull(mtstb,'Not Set');

update dim_mdg_part
set MaterialTypeDescr = ifnull(t134t_mtbez,'Not Set')
from dim_mdg_part, MDG_T134T
where MaterialType = t134t_mtart
	and ifnull(MaterialTypeDescr,'a') <> ifnull(t134t_mtbez,'Not Set');

update dim_mdg_part
set GeneralitemcatgroupDescr = ifnull(bezei,'Not Set')
from dim_mdg_part, MDG_TPTMT
where Generalitemcatgroup = mtpos
	and ifnull(GeneralitemcatgroupDescr,'a') <> ifnull(bezei,'Not Set');

update dim_mdg_part
set ProducthierarchyDescr = ifnull(mdg_t179t_vtext,'Not Set')
from dim_mdg_part, MDG_T179T
where Producthierarchy = mdg_t179t_prodh
	and ifnull(ProducthierarchyDescr,'a') <> ifnull(mdg_t179t_vtext,'Not Set');

update dim_mdg_part
set APORelevanceDesc = 'Not Set'
where APORelevanceDesc <> 'Not Set';


update dim_mdg_part
set APORelevanceDesc = ifnull(ltext,'Not Set')
from dim_mdg_part, MDG_T9D520T
where APORelevance = yyd_aporl
	and ifnull(APORelevanceDesc,'a') <> ifnull(ltext,'Not Set');

update dim_mdg_part d
set global_operational_hierarchy_description = ifnull(mdg_t9d335t_ltext,'Not Set'),
	d.dw_update_date = current_timestamp
from dim_mdg_part d, MDG_T9D335T
where mdg_t9d335t_yyd_gloph = global_operational_hierarchy and global_operational_hierarchy_description <> ifnull(mdg_t9d335t_ltext,'Not Set');

update dim_mdg_part
set COGSICMIndicator = ifnull(YYD_COGSI,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(COGSICMIndicator,'a') <> ifnull(YYD_COGSI,'Not Set');

update dim_mdg_part
set UnitOfCompleteContentOfThePackage = ifnull(YYD_PPUNI,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(UnitOfCompleteContentOfThePackage,'a') <> ifnull(YYD_PPUNI,'Not Set');

update dim_mdg_part
set CompleteContentOfThePackage = ifnull(YYD_COMCO,0)
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(CompleteContentOfThePackage,-1) <> ifnull(YYD_COMCO,0);

update dim_mdg_part
set articlenumber = ifnull(YYD_YGART,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(articlenumber,'a') <> ifnull(YYD_YGART,'Not Set');

update dim_mdg_part
set supplychainplaningppi = ifnull(YYD_SCPPI,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(supplychainplaningppi,'a') <> ifnull(YYD_SCPPI,'Not Set');


 /* Alex D 03-05-2017  */
update dim_mdg_part d
set Dosage = ifnull(MARA_YYD_AIDOS, 0)
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
     and ifnull(Dosage,-1) <> ifnull(MARA_YYD_AIDOS,0);

update dim_mdg_part d
set UnitofDosage = ifnull(MARA_YYD_AIUNI,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
     and ifnull(UnitofDosage,'x') <> ifnull(MARA_YYD_AIUNI,'Not Set');

/* APP-7108: Oana 04Aug2017 */
update dim_mdg_part d
set globalbrand = ifnull(MARA_YYD_RIMIB, 'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
     and globalbrand <> ifnull(MARA_YYD_RIMIB,'Not Set');

update dim_mdg_part d
  set IMCCode = ifnull(MARA_YYD_IMCOD, 'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
     and IMCCode <> ifnull(MARA_YYD_IMCOD,'Not Set');


/* INSERT NEW ROWS */
DELETE FROM number_fountain m
WHERE m.table_name = 'dim_mdg_part';

INSERT INTO number_fountain
SELECT 'dim_mdg_part',
       ifnull(MAX(d.dim_mdg_partid ), ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_mdg_part d
WHERE d.dim_mdg_partid <> 1;

INSERT INTO dim_mdg_part
(
  dim_mdg_partid,
  partnumber,
  MatTypeforGlbRep,
  GlbProductGroup,
  GlbABCIndicator,
  Oldmatnumber,
  BaseUnitofMeasure,
  TotalShelfLife,
  MinRemainingShelfLife,
  CrossPlantMatStatus,
  MaterialType,
  Generalitemcatgroup,
  Producthierarchy,
  Batchmanreqind,
  APORelevance,
  global_operational_hierarchy,
  MatTypeforGlbRepDescr,
  CrossPlantMatStatusDescr,
  MaterialTypeDescr,
  GeneralitemcatgroupDescr,
  ProducthierarchyDescr,
  APORelevanceDesc,
  global_operational_hierarchy_description,
  COGSICMIndicator,
  UnitOfCompleteContentOfThePackage,
  CompleteContentOfThePackage,
  ppu,
  ppu_units,
  act_conv,
  unit2,
  gaussuom,
  gaussuomdesc,
  articlenumber,
  supplychainplaningppi,
  Dosage,
  UnitofDosage,
  GlobalBrand,
  IMCCode
)
SELECT (SELECT ifnull(m.max_id, 1)
        FROM number_fountain m
        WHERE m.table_name = 'dim_mdg_part') + ROW_NUMBER() OVER(ORDER BY '') as dim_mdg_partid,
       m.MATNR as partnumber,
	   ifnull(YYD_GLMTY,'Not Set'),
	   ifnull(YYD_YSBU,'Not Set'),
	   ifnull(YYD_ABCCL,'Not Set'),
	   ifnull(BISMT,'Not Set'),
	   ifnull(MEINS,'Not Set'),
	   ifnull(MHDHB,0),
	   ifnull(MHDRZ,0),
	   ifnull(MSTAE,'Not Set'),
	   ifnull(MTART,'Not Set'),
	   ifnull(MTPOS_MARA,'Not Set'),
	   ifnull(PRDHA,'Not Set'),
	   ifnull(XCHPF,'Not Set'),
	   ifnull(YYD_APORL,'Not Set'),
	   ifnull(YYD_GLOPH, 'Not Set'),
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   ifnull(YYD_COGSI,'Not Set'),
	   ifnull(YYD_PPUNI,'Not Set'),
	   ifnull(YYD_COMCO,0),
	   0,
	   'Not Set',
	   0,
	   'Not Set',
	   'Not Set',
	   'Not Set',
	   ifnull(YYD_YGART, 'Not Set'),
	   ifnull(YYD_SCPPI,'Not Set'),
	   ifnull(MARA_YYD_AIDOS, 0 ) as Dosage,
           ifnull(MARA_YYD_AIUNI,'Not Set') as UnitofDosage,
     ifnull(MARA_YYD_RIMIB,'Not Set')  as GlobalBrand,
     ifnull(MARA_YYD_IMCOD, 'Not Set') as IMCCode
FROM MDG_MARA m
WHERE NOT EXISTS (SELECT 'x'
                  FROM dim_mdg_part dmp
                  WHERE dmp.partnumber = m.MATNR);

update dim_mdg_part
set MatTypeforGlbRepDescr = ifnull(yyd_text,'Not Set')
from dim_mdg_part, MDG_T9D241T
where MatTypeforGlbRep = yyd_glmty
	and ifnull(MatTypeforGlbRepDescr,'a') <> ifnull(yyd_text,'Not Set');

update dim_mdg_part
set CrossPlantMatStatusDescr = ifnull(mtstb,'Not Set')
from dim_mdg_part, MDG_T141T
where CrossPlantMatStatus = mmsta
	and ifnull(CrossPlantMatStatusDescr,'a') <> ifnull(mtstb,'Not Set');

update dim_mdg_part
set MaterialTypeDescr = ifnull(t134t_mtbez,'Not Set')
from dim_mdg_part, MDG_T134T
where MaterialType = t134t_mtart
	and ifnull(MaterialTypeDescr,'a') <> ifnull(t134t_mtbez,'Not Set');

update dim_mdg_part
set GeneralitemcatgroupDescr = ifnull(bezei,'Not Set')
from dim_mdg_part, MDG_TPTMT
where Generalitemcatgroup = mtpos
	and ifnull(GeneralitemcatgroupDescr,'a') <> ifnull(bezei,'Not Set');

update dim_mdg_part
set ProducthierarchyDescr = ifnull(mdg_t179t_vtext,'Not Set')
from dim_mdg_part, MDG_T179T
where Producthierarchy = mdg_t179t_prodh
	and ifnull(ProducthierarchyDescr,'a') <> ifnull(mdg_t179t_vtext,'Not Set');

update dim_mdg_part
set APORelevanceDesc = ifnull(ltext,'Not Set')
from dim_mdg_part, MDG_T9D520T
where APORelevance = yyd_aporl
	and ifnull(APORelevanceDesc,'a') <> ifnull(ltext,'Not Set');

update dim_mdg_part d
set global_operational_hierarchy_description = ifnull(mdg_t9d335t_ltext,'Not Set'),
	d.dw_update_date = current_timestamp
from dim_mdg_part d, MDG_T9D335T
where mdg_t9d335t_yyd_gloph = global_operational_hierarchy and global_operational_hierarchy_description <> ifnull(mdg_t9d335t_ltext,'Not Set');

/* 21 March 2017 Cornelia add PRM and PRP */

drop table if exists tmp_aditionam_mdm_atributes;
create table tmp_aditionam_mdm_atributes
as
select a.md_mmmaterial,a.usmdkmmmaterial,b.usmd_active,b.usmd_o_yorg,b.md_mmyyd_orga,b.md_mmyyd_yorga
from MDG_YYD_V_MAPPING a
	inner join MDG_YYD_V_YORG b on a.usmdkmmmaterial = b.usmdkmmmaterial and usmd_active = 1 and ifnull(usmd_o_yorg,'Y') <> 'X';


update dim_mdg_part d
set d.PRM_Product_manager_code = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PRM'
	and ifnull(d.PRM_Product_manager_code,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.PRM_Product_manager_desc = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.PRM_Product_manager_code = t.YYD_YORGA and d.PRM_Product_manager_desc <> ifnull(t.LTEXT,'Not Set');


update dim_mdg_part d
set d.PRP_Price_responsible_code = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PRP'
	and ifnull(d.PRP_Price_responsible_code,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.PRP_Price_responsible_desc = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.PRP_Price_responsible_code = t.YYD_YORGA and d.PRP_Price_responsible_desc <> ifnull(t.LTEXT,'Not Set');
/* END 21 March 2017 */

/* aditional atributes */

drop table if exists tmp_aditionam_mdm_atributes;
create table tmp_aditionam_mdm_atributes
as
select a.md_mmmaterial,a.usmdkmmmaterial,b.usmd_active,b.usmd_o_yorg,b.md_mmyyd_orga,b.md_mmyyd_yorga
from MDG_YYD_V_MAPPING a
	inner join MDG_YYD_V_YORG b on a.usmdkmmmaterial = b.usmdkmmmaterial and usmd_active = 1 and ifnull(usmd_o_yorg,'Y') <> 'X';

update dim_mdg_part d
set d.primary_production_location = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'PPL'
	and ifnull(d.primary_production_location,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.primary_production_location_name = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.primary_production_location = t.YYD_YORGA and d.primary_production_location_name <> ifnull(t.LTEXT,'Not Set');

update dim_mdg_part d
set d.global_demand_planner = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'GDP'
	and ifnull(d.global_demand_planner,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.global_supply_planner = ifnull(t.md_mmyyd_yorga,'Not Set')
from dim_mdg_part d, tmp_aditionam_mdm_atributes t
where d.partnumber = t.md_mmmaterial and t.md_mmyyd_orga = 'GSP'
	and ifnull(d.global_supply_planner,'aaa') <> ifnull(t.md_mmyyd_yorga,'Not Set');

update dim_mdg_part d
set d.global_demand_planner_desc = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.global_demand_planner = t.YYD_YORGA and d.global_demand_planner_desc <> ifnull(t.LTEXT,'Not Set');

update dim_mdg_part d
set d.global_supply_planner_desc = ifnull(t.LTEXT,'Not Set')
from dim_mdg_part d, MDG_T9D519T t
where d.global_supply_planner = t.YYD_YORGA and d.global_supply_planner_desc <> ifnull(t.LTEXT,'Not Set');

drop table if exists tmp_aditionam_mdm_atributes;

/* HC PPU */

/* update dim_mdg_part d
set d.ppu = ifnull(t.ppu,0)
from dim_mdg_part d, csv_hc_ppu t
where d.partnumber = t.item
	and ifnull(d.ppu,-1) <> ifnull(t.ppu,0) */

update dim_mdg_part d
set d.ppu_units = ifnull(t.units,'Not Set')
from dim_mdg_part d, csv_hc_ppu t
where d.partnumber = t.item
	and ifnull(d.ppu_units,'xx') <> ifnull(t.units,'Not Set');

/* update dim_mdg_part d
set d.act_conv = ifnull(t.act_conv,0)
from dim_mdg_part d, csv_hc_ppu t
where d.partnumber = t.item
	and ifnull(d.act_conv,-1) <> ifnull(t.act_conv,0) */

/* APP-7579 Update calculation for PPU and ACT_CONV - Oana 25Oct2017 */
update dim_mdg_part md
	set md.ppu = 1;

update dim_mdg_part md
	set md.ppu = case when MARM_UMREZ<>0  then MARM_UMREN / MARM_UMREZ else 1 end
from dim_mdg_part md, MARM
where right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || MARM_MATNR,18)
	and MARM_ATINN in ('123','124','125','126');

update dim_mdg_part md
set md.act_conv = case when partnumber = 'A19030N2' THEN 0.000001
			when partnumber in ('A5403AN0','A5403AN4','B54030N2','B54030N3','B54030N4','B54030N5') THEN 0.001000
			when partnumber in  ('B1903121','B1903122') THEN 1000.000000
		else 1
	end;
/* End APP-7579 Update calculation for PPU and ACT_CONV - Oana 25Oct2017 */



update dim_mdg_part d
set d.unit2 = ifnull(t.unit2,'Not Set')
from dim_mdg_part d, csv_hc_ppu t
where d.partnumber = t.item
	and ifnull(d.unit2,'xx') <> ifnull(t.unit2,'Not Set');

update dim_mdg_part d
set d.gaussuom = ifnull(t.gaussuom,'Not Set')
from dim_mdg_part d, csv_hc_ppu t
where d.partnumber = t.item
	and ifnull(d.gaussuom,'xx') <> ifnull(t.gaussuom,'Not Set');

update dim_mdg_part d
set d.gaussuomdesc = ifnull(t.gaussuomdesc,'Not Set')
from dim_mdg_part d, csv_hc_ppu t
where d.partnumber = t.item
	and ifnull(d.gaussuomdesc,'xx') <> ifnull(t.gaussuomdesc,'Not Set');

update dim_mdg_part d
  set d.partdescription = ifnull(t.MAKT_MAKTX,'Not Set')
  from dim_mdg_part d
    inner join MDG_MAKT t on d.partnumber = t.MAKT_MATNR
  where d.partdescription <> ifnull(t.MAKT_MAKTX,'Not Set');

UPDATE dim_mdg_part dp
   SET NetWeight = ifnull(MARA_NTGEW, 0),
                dp.dw_update_date = current_timestamp
from MDG_MARA mck, dim_mdg_part dp
 WHERE     dp.PartNumber = mck.MATNR
          and NetWeight <> ifnull(MARA_NTGEW, 0);

UPDATE dim_mdg_part dp
   SET WeightUnit = ifnull(MARA_GEWEI, 'Not Set'),
                dp.dw_update_date = current_timestamp
from MDG_MARA mck, dim_mdg_part dp
 WHERE     dp.PartNumber = mck.MATNR
          and WeightUnit <> ifnull(MARA_GEWEI, 'Not Set');

/*BI-4717 @Catalin add maingroup fields to MDG*/

update dim_mdg_part c
set maingroup = IFNULL(substring(c.producthierarchy,1,3),'Not Set')
	,uppergroup =IFNULL(substring(c.producthierarchy,1,6),'Not Set')
	,articlegroup = IFNULL(substring(c.producthierarchy,1,9),'Not Set')
	,internationalarticle = IFNULL(c.producthierarchy,'Not Set')
	,dw_update_date = CURRENT_DATE
where producthierarchy <>'Not Set';

update dim_mdg_part d
SET maingroupdesc = ifnull(mdg_t179t_vtext,'Not Set')
FROM dim_mdg_part d,MDG_T179T c
WHERE d.maingroup = c.mdg_t179t_prodh
AND maingroupdesc <> ifnull(mdg_t179t_vtext,'Not Set');

update dim_mdg_part d
SET uppergroupdesc = ifnull(mdg_t179t_vtext,'Not Set')
FROM dim_mdg_part d,MDG_T179T c
WHERE d.uppergroup = c.mdg_t179t_prodh
AND uppergroupdesc <> ifnull(mdg_t179t_vtext,'Not Set');

update dim_mdg_part d
SET articlegroupdesc = ifnull(mdg_t179t_vtext,'Not Set')
FROM dim_mdg_part d,MDG_T179T c
WHERE d.articlegroup = c.mdg_t179t_prodh
AND articlegroupdesc <> ifnull(mdg_t179t_vtext,'Not Set');

update dim_mdg_part d
SET internationalarticledesc = ifnull(mdg_t179t_vtext,'Not Set')
FROM dim_mdg_part d,MDG_T179T c
WHERE d.internationalarticle = c.mdg_t179t_prodh
AND internationalarticledesc <> ifnull(mdg_t179t_vtext,'Not Set');



/* Add Product Manager info */

drop table if exists tmp_productmanager;
create table tmp_productmanager as
select distinct GROUP_CONCAT( distinct b.MD_YDYYD_HNAME) MD_YDYYD_HNAME, a.MD_YDPRODH
from EMDTEMPOCC4.YYD_V_GPHM a, EMDTEMPOCC4.YYD_V_YYD_POGRP b
where a.USMDKYDPRODH = b.USMDKYDPRODH
and b.USMD_ACTIVE  = 1
and b.USMD_O_YYD_POGRP is null
group by a.MD_YDPRODH;


update dim_mdg_part d
set productmanager = ifnull(a.MD_YDYYD_HNAME, 'Not Set')
from dim_mdg_part d, tmp_productmanager a
where left(d.Producthierarchy,3) = ifnull(a.MD_YDPRODH,'Not Set');

update dim_mdg_part d
set productmanager = ifnull(a.MD_YDYYD_HNAME, 'Not Set')
from dim_mdg_part d, tmp_productmanager a
where left(d.Producthierarchy,6) = ifnull(a.MD_YDPRODH,'Not Set');

update dim_mdg_part d
set productmanager = ifnull(a.MD_YDYYD_HNAME, 'Not Set')
from dim_mdg_part d, tmp_productmanager a
where left(d.Producthierarchy,9) = ifnull(a.MD_YDPRODH,'Not Set');

update dim_mdg_part d
set productmanager = ifnull(a.MD_YDYYD_HNAME, 'Not Set')
from dim_mdg_part d, tmp_productmanager a
where d.Producthierarchy = ifnull(a.MD_YDPRODH,'Not Set');

/*Cornelia 10 march 2017 add IndicatorSalesCatalogue */
update dim_mdg_part
set IndicatorSalesCatalogue = ifnull(YYD_YKZLP,'Not Set')
from dim_mdg_part, MDG_MARA
where partnumber = MATNR
	and ifnull(IndicatorSalesCatalogue,'a') <> ifnull(YYD_YKZLP,'Not Set');

update dim_mdg_part dmp
set dmp.IndicatorSalesCatalogueDesc = ifnull(cd.description,'Not Set')
from dim_mdg_part dmp,  catalogue_desc cd
where dmp.IndicatorSalesCatalogue = cd.code
	and ifnull(IndicatorSalesCatalogueDesc,'a') <> ifnull(cd.description,'Not Set');

/* APP-7108 - Oana 21Aug2017 */
update dim_mdg_part d
set globalbranddescr = ifnull(T9DRIMIBT_LTEXT, 'Not Set')
from dim_mdg_part, MDG_T9DRIMIBT
where globalbrand = T9DRIMIBT_YYD_RIMIB
     and globalbranddescr <> ifnull(T9DRIMIBT_LTEXT,'Not Set');

update dim_mdg_part d
  set IMCCodeDescr = ifnull(MDG_T9D343T_YYD_TEXT, 'Not Set')
from dim_mdg_part, MDG_T9D343T
where IMCCode = MDG_T9D343T_YYD_IMCOD
     and IMCCodeDescr <> ifnull(MDG_T9D343T_YYD_TEXT,'Not Set');

update dim_mdg_part
  set IMCCODE_and_Descr = IMCCode || ' ' || IMCCodeDescr
where IMCCODE_and_Descr <> IMCCode || ' ' || IMCCodeDescr
  and IMCCODE <> 'Not Set' ;

/* Expression test  */

update dim_mdg_part
set PRODUCT_HIERARCHY_DESCRIPTION2 = decode(producthierarchy,'Not Set',producthierarchy,left(producthierarchy,3))
where PRODUCT_HIERARCHY_DESCRIPTION2 <> decode(producthierarchy,'Not Set',producthierarchy,left(producthierarchy,3));

update dim_mdg_part
set PrimaryProductionLocation = primary_production_location || ' - ' || primary_production_location_name
where PrimaryProductionLocation <> primary_production_location || ' - ' || primary_production_location_name;

/* 7 august 2017 - Cornelia add new attribute MaterialCategory */
update dim_mdg_part 
set materialcategory ='Finished goods'
where mattypeforglbrep in (
'MER',
'FERT',
'FIN',
'ZCSM',
'ZFRT',
'ZVFT',
'ZVAR',
'KMAT',
'LEER',
'NLAG',
'UNBW',
'ZLIT',
'ZRCH',
'ZCTO',
'Not Set'
)
and materialcategory <> 'Finished goods';  /* Roxana H - 7 Dec 2017 */


update dim_mdg_part 
set materialcategory ='Raw Material'
where mattypeforglbrep in (
'DIEN',
'DUM',
'HIBE',
'RAW',
'ROH',
'VERP',
'ZUNB',
'ZVRP',
'PROC',
'ZHLB',
'ZIMU',
'ABF'
)
and materialcategory <> 'Raw Material'; /* Roxana H - 7 Dec 2017 */

update dim_mdg_part 
set materialcategory ='Unfinished Goods'
where mattypeforglbrep in ('HALB','UFG')
and materialcategory <>'Unfinished Goods'; /* Roxana H - 7 Dec 2017 */

UPDATE dim_mdg_part dp 
SET  dp.MDGPartNumber_NoLeadZero = ifnull(case when length(dp.partnumber) = 18 and dp.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from dp.partnumber) else dp.partnumber end,'Not Set');
