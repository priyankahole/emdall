/* ##################################################################################################################
  
     Script         : bi_populate_so_headerstatus_dim 
     Author         : Ashu
     Created On     : 17 Jan 2013
  
  
     Description    : Stored Proc bi_populate_so_headerstatus_dim from MySQL to Vectorwise syntax
  
     Change History
     Date            By        Version           Desc
     17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
	 7 July 2014     George    1.1               Added GeneralIncompleteStatusItemCode,OverallCreditStatus,OverallCreditStatusCode,OverallBlkdStatusCode,
														OverallDlvrBlkStatusCode,TotalIncompleteStatusAllItemsCode,RejectionStatusCode
	 11 July 2013    George    1.2               Added TVBST source fields: GeneralIncompletionProcessingstatusOfHeader,OverallProcessingstatusofcreditchecks,
												 OverallBlockedProcessingStatus,DeliveryBlockProcessingStatus,TotalIncompletionProcessingStatusAllItems,OverallRejectionProcessingStatus
	  5 Feb 2015     Liviu Ionescu   1.3     Add combine for table dim_salesorderheaderstatus, VBUK
	 #################################################################################################################### */
insert into dim_salesorderheaderstatus
 (dim_salesorderheaderstatusid
           )
select 1
from (select 1) a
where not exists ( select 'x' from dim_salesorderheaderstatus where dim_salesorderheaderstatusid = 1);


DELETE FROM VBUK
  WHERE NOT EXISTS
            (SELECT 1
               FROM VBAK_VBAP_VBEP
              WHERE VBUK_VBELN = VBAK_VBELN) 
        AND NOT EXISTS
               (SELECT 1
                  FROM fact_salesorder
                 WHERE dd_SalesDocNo = VBUK_VBELN);

 
DELETE FROM VBUK
  WHERE NOT EXISTS
               (SELECT 1
                  FROM VBAK_VBAP
                 WHERE VBUK_VBELN = VBAP_VBELN)
        AND NOT EXISTS
               (SELECT 1
                  FROM fact_salesorder
                 WHERE dd_SalesDocNo = VBUK_VBELN);


merge into dim_salesorderheaderstatus dest
using(select dim_salesorderheaderstatusid, 
			ifnull(t1.DD07T_DDTEXT, 'Not Set') RefStatusAllItems,
			ifnull(t2.DD07T_DDTEXT, 'Not Set') ConfirmationStatus,
			ifnull(t3.DD07T_DDTEXT, 'Not Set') DeliveryStatus,
			ifnull(t4.DD07T_DDTEXT, 'Not Set') OverallDlvrStatusOfItem,
			ifnull(t5.DD07T_DDTEXT, 'Not Set') BillingStatus,
			ifnull(t6.DD07T_DDTEXT, 'Not Set') BillingStatusOfOrdRelBillDoc,
			ifnull(t7.DD07T_DDTEXT, 'Not Set') RejectionStatus,
			ifnull(t8.DD07T_DDTEXT, 'Not Set') OverallProcessStatusItem,
			ifnull(t9.DD07T_DDTEXT, 'Not Set') TotalIncompleteStatusAllItems,
			ifnull(t10.DD07T_DDTEXT, 'Not Set') GeneralIncompleteStatusItem,
			ifnull(t11.DD07T_DDTEXT, 'Not Set') OverallPickingStatus,
			ifnull(t12.DD07T_DDTEXT, 'Not Set') HeaderIncompletionStautusConcernDlvry,
            ifnull(t13.DD07T_DDTEXT, 'Not Set') OverallBlkdStatus,
			ifnull(t14.DD07T_DDTEXT, 'Not Set') OverallDlvrBlkStatus,
			ifnull(t15.DD07T_DDTEXT, 'Not Set') GeneralIncompleteStatusItemCode,
			ifnull(t16.DD07T_DDTEXT, 'Not Set') OverallCreditStatus,
			ifnull(t16.DD07T_DOMVALUE, 'Not Set') OverallCreditStatusCode,
			ifnull(t13.DD07T_DOMVALUE, 'Not Set') OverallBlkdStatusCode,
			ifnull(t14.DD07T_DOMVALUE, 'Not Set') OverallDlvrBlkStatusCode,
			ifnull(t9.DD07T_DOMVALUE, 'Not Set') TotalIncompleteStatusAllItemsCode,
			ifnull(t7.DD07T_DOMVALUE, 'Not Set') RejectionStatusCode
	  from dim_salesorderheaderstatus dim
		inner join VBUK ds on dim.SalesDocumentNumber = ds.VBUK_VBELN
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t1  on  t1.DD07T_DOMVALUE = ds.VBUK_RFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t2  on  t2.DD07T_DOMVALUE = ds.VBUK_BESTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t3  on  t3.DD07T_DOMVALUE = ds.VBUK_LFSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t4  on  t4.DD07T_DOMVALUE = ds.VBUK_LFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t5  on  t5.DD07T_DOMVALUE = ds.VBUK_FKSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t6  on  t6.DD07T_DOMVALUE = ds.VBUK_FKSAK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t7  on  t7.DD07T_DOMVALUE = ds.VBUK_ABSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t8  on  t8.DD07T_DOMVALUE = ds.VBUK_GBSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t9  on  t9.DD07T_DOMVALUE = ds.VBUK_UVALS
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t10 on t10.DD07T_DOMVALUE = ds.VBUK_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t11 on t11.DD07T_DOMVALUE = ds.VBUK_KOSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t12 on t12.DD07T_DOMVALUE = ds.VBUK_UVVLK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t13  on  t13.DD07T_DOMVALUE = ds.VBUK_SPSTG
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t14  on  t14.DD07T_DOMVALUE = ds.VBUK_LSSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t15  on  t15.DD07T_DOMVALUE = ds.VBUK_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t16  on  t16.DD07T_DOMVALUE = ds.VBUK_CMGST
) src
on dest.dim_salesorderheaderstatusid = src.dim_salesorderheaderstatusid
when matched then update set dest.dw_update_date = current_timestamp,
							 dest.RefStatusAllItems = src.RefStatusAllItems,
							 dest.ConfirmationStatus = src.ConfirmationStatus,
							 dest.DeliveryStatus = src.DeliveryStatus,
							 dest.OverallDlvrStatusOfItem = src.OverallDlvrStatusOfItem,
							 dest.BillingStatus = src.BillingStatus,
							 dest.BillingStatusOfOrdRelBillDoc = src.BillingStatusOfOrdRelBillDoc,
							 dest.RejectionStatus = src.RejectionStatus,
							 dest.OverallProcessStatusItem = src.OverallProcessStatusItem,
							 dest.TotalIncompleteStatusAllItems = src.TotalIncompleteStatusAllItems,
							 dest.GeneralIncompleteStatusItem = src.GeneralIncompleteStatusItem,
							 dest.OverallPickingStatus = src.OverallPickingStatus,
							 dest.HeaderIncompletionStautusConcernDlvry = src.HeaderIncompletionStautusConcernDlvry,
							 dest.OverallBlkdStatus = src.OverallBlkdStatus,
							 dest.OverallDlvrBlkStatus = src.OverallDlvrBlkStatus,
							 dest.GeneralIncompleteStatusItemCode = src.GeneralIncompleteStatusItemCode,
							 dest.OverallCreditStatus = src.OverallCreditStatus,
							 dest.OverallCreditStatusCode = src.OverallCreditStatusCode,
							 dest.OverallBlkdStatusCode = src.OverallBlkdStatusCode,
							 dest.OverallDlvrBlkStatusCode = src.OverallDlvrBlkStatusCode,
							 dest.TotalIncompleteStatusAllItemsCode = src.TotalIncompleteStatusAllItemsCode,
							 dest.RejectionStatusCode = src.RejectionStatusCode;


delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 							 
							 
  INSERT INTO dim_salesorderheaderstatus(
	      dim_salesorderheaderstatusid,
          SalesDocumentNumber,
          RefStatusAllItems,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDlvrStatusOfItem,
          BillingStatus,
          BillingStatusOfOrdRelBillDoc,
          RejectionStatus,
          OverallProcessStatusItem,
          TotalIncompleteStatusAllItems,
          GeneralIncompleteStatusItem,
          OverallPickingStatus,
	      HeaderIncompletionStautusConcernDlvry,
		  OverallBlkdStatus,
		  OverallDlvrBlkStatus,
		  GeneralIncompleteStatusItemCode,
		  OverallCreditStatus,
		  OverallCreditStatusCode,
		  OverallBlkdStatusCode,
		  OverallDlvrBlkStatusCode,
		  TotalIncompleteStatusAllItemsCode,
		  RejectionStatusCode
          )
 SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by '')),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull(t1.DD07T_DDTEXT, 'Not Set') RefStatusAllItems,
		ifnull(t2.DD07T_DDTEXT, 'Not Set') ConfirmationStatus,
		ifnull(t3.DD07T_DDTEXT, 'Not Set') DeliveryStatus,
		ifnull(t4.DD07T_DDTEXT, 'Not Set') OverallDlvrStatusOfItem,
		ifnull(t5.DD07T_DDTEXT, 'Not Set') BillingStatus,
		ifnull(t6.DD07T_DDTEXT, 'Not Set') BillingStatusOfOrdRelBillDoc,
		ifnull(t7.DD07T_DDTEXT, 'Not Set') RejectionStatus,
		ifnull(t8.DD07T_DDTEXT, 'Not Set') OverallProcessStatusItem,
		ifnull(t9.DD07T_DDTEXT, 'Not Set') TotalIncompleteStatusAllItems,
		ifnull(t10.DD07T_DDTEXT, 'Not Set') GeneralIncompleteStatusItem,
		ifnull(t11.DD07T_DDTEXT, 'Not Set') OverallPickingStatus,
		ifnull(t12.DD07T_DDTEXT, 'Not Set') HeaderIncompletionStautusConcernDlvry,
		ifnull(t13.DD07T_DDTEXT, 'Not Set') OverallBlkdStatus,
		ifnull(t14.DD07T_DDTEXT, 'Not Set') OverallDlvrBlkStatus,
		ifnull(t15.DD07T_DDTEXT, 'Not Set') GeneralIncompleteStatusItemCode,
		ifnull(t16.DD07T_DDTEXT, 'Not Set') OverallCreditStatus,
			ifnull(t16.DD07T_DOMVALUE, 'Not Set') OverallCreditStatusCode,
			ifnull(t13.DD07T_DOMVALUE, 'Not Set') OverallBlkdStatusCode,
			ifnull(t14.DD07T_DOMVALUE, 'Not Set') OverallDlvrBlkStatusCode,
			ifnull(t9.DD07T_DOMVALUE, 'Not Set') TotalIncompleteStatusAllItemsCode,
			ifnull(t7.DD07T_DOMVALUE, 'Not Set') RejectionStatusCode
FROM VBUK ds
		inner join VBAK_VBAP_VBEP t0 on t0.VBAK_VBELN = ds.VBUK_VBELN
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t1  on  t1.DD07T_DOMVALUE = ds.VBUK_RFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t2  on  t2.DD07T_DOMVALUE = ds.VBUK_BESTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t3  on  t3.DD07T_DOMVALUE = ds.VBUK_LFSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t4  on  t4.DD07T_DOMVALUE = ds.VBUK_LFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t5  on  t5.DD07T_DOMVALUE = ds.VBUK_FKSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t6  on  t6.DD07T_DOMVALUE = ds.VBUK_FKSAK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t7  on  t7.DD07T_DOMVALUE = ds.VBUK_ABSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t8  on  t8.DD07T_DOMVALUE = ds.VBUK_GBSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t9  on  t9.DD07T_DOMVALUE = ds.VBUK_UVALS
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t10 on t10.DD07T_DOMVALUE = ds.VBUK_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t11 on t11.DD07T_DOMVALUE = ds.VBUK_KOSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t12 on t12.DD07T_DOMVALUE = ds.VBUK_UVVLK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t13  on  t13.DD07T_DOMVALUE = ds.VBUK_SPSTG
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t14  on  t14.DD07T_DOMVALUE = ds.VBUK_LSSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t15  on  t15.DD07T_DOMVALUE = ds.VBUK_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t16  on  t16.DD07T_DOMVALUE = ds.VBUK_CMGST
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = ds.VBUK_VBELN)) dtbl1;

delete from number_fountain m where m.table_name = 'dim_salesorderheaderstatus';

insert into number_fountain
select 	'dim_salesorderheaderstatus',
	ifnull(max(d.dim_salesorderheaderstatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderheaderstatus d
where d.dim_salesorderheaderstatusid <> 1; 

  INSERT INTO dim_salesorderheaderstatus(
	      dim_salesorderheaderstatusid,
          SalesDocumentNumber,
          RefStatusAllItems,
          ConfirmationStatus,
          DeliveryStatus,
          OverallDlvrStatusOfItem,
          BillingStatus,
          BillingStatusOfOrdRelBillDoc,
          RejectionStatus,
          OverallProcessStatusItem,
          TotalIncompleteStatusAllItems,
          GeneralIncompleteStatusItem,
          OverallPickingStatus,
	      HeaderIncompletionStautusConcernDlvry,
		  OverallBlkdStatus,
		  OverallDlvrBlkStatus,
		  GeneralIncompleteStatusItemCode,
		  OverallCreditStatus,
		  OverallCreditStatusCode,
		  OverallBlkdStatusCode,
		  OverallDlvrBlkStatusCode,
		  TotalIncompleteStatusAllItemsCode,
		  RejectionStatusCode
          )
 SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderheaderstatus') + row_number() over (order by '')),dtbl1.*
 FROM  
( SELECT distinct VBUK_VBELN,
        ifnull(t1.DD07T_DDTEXT, 'Not Set') RefStatusAllItems,
		ifnull(t2.DD07T_DDTEXT, 'Not Set') ConfirmationStatus,
		ifnull(t3.DD07T_DDTEXT, 'Not Set') DeliveryStatus,
		ifnull(t4.DD07T_DDTEXT, 'Not Set') OverallDlvrStatusOfItem,
		ifnull(t5.DD07T_DDTEXT, 'Not Set') BillingStatus,
		ifnull(t6.DD07T_DDTEXT, 'Not Set') BillingStatusOfOrdRelBillDoc,
		ifnull(t7.DD07T_DDTEXT, 'Not Set') RejectionStatus,
		ifnull(t8.DD07T_DDTEXT, 'Not Set') OverallProcessStatusItem,
		ifnull(t9.DD07T_DDTEXT, 'Not Set') TotalIncompleteStatusAllItems,
		ifnull(t10.DD07T_DDTEXT, 'Not Set') GeneralIncompleteStatusItem,
		ifnull(t11.DD07T_DDTEXT, 'Not Set') OverallPickingStatus,
		ifnull(t12.DD07T_DDTEXT, 'Not Set') HeaderIncompletionStautusConcernDlvry,
		ifnull(t13.DD07T_DDTEXT, 'Not Set') OverallBlkdStatus,
		ifnull(t14.DD07T_DDTEXT, 'Not Set') OverallDlvrBlkStatus,
		ifnull(t15.DD07T_DDTEXT, 'Not Set') GeneralIncompleteStatusItemCode,
		ifnull(t16.DD07T_DDTEXT, 'Not Set') OverallCreditStatus,
			ifnull(t16.DD07T_DOMVALUE, 'Not Set') OverallCreditStatusCode,
			ifnull(t13.DD07T_DOMVALUE, 'Not Set') OverallBlkdStatusCode,
			ifnull(t14.DD07T_DOMVALUE, 'Not Set') OverallDlvrBlkStatusCode,
			ifnull(t9.DD07T_DOMVALUE, 'Not Set') TotalIncompleteStatusAllItemsCode,
			ifnull(t7.DD07T_DOMVALUE, 'Not Set') RejectionStatusCode
FROM VBUK ds
		inner join VBAK_VBAP t0 on t0.VBAP_VBELN = ds.VBUK_VBELN
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t1  on  t1.DD07T_DOMVALUE = ds.VBUK_RFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t2  on  t2.DD07T_DOMVALUE = ds.VBUK_BESTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t3  on  t3.DD07T_DOMVALUE = ds.VBUK_LFSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t4  on  t4.DD07T_DOMVALUE = ds.VBUK_LFGSK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t5  on  t5.DD07T_DOMVALUE = ds.VBUK_FKSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t6  on  t6.DD07T_DOMVALUE = ds.VBUK_FKSAK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t7  on  t7.DD07T_DOMVALUE = ds.VBUK_ABSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t8  on  t8.DD07T_DOMVALUE = ds.VBUK_GBSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t9  on  t9.DD07T_DOMVALUE = ds.VBUK_UVALS
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t10 on t10.DD07T_DOMVALUE = ds.VBUK_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t11 on t11.DD07T_DOMVALUE = ds.VBUK_KOSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t12 on t12.DD07T_DOMVALUE = ds.VBUK_UVVLK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t13  on  t13.DD07T_DOMVALUE = ds.VBUK_SPSTG
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t14  on  t14.DD07T_DOMVALUE = ds.VBUK_LSSTK
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t15  on  t15.DD07T_DOMVALUE = ds.VBUK_UVALL
		left join (select t.DD07T_DDTEXT, t.DD07T_DOMVALUE from dd07t t where t.DD07T_DOMNAME = 'STATV') t16  on  t16.DD07T_DOMVALUE = ds.VBUK_CMGST
WHERE not exists (select 1 from dim_salesorderheaderstatus s where s.SalesDocumentNumber = ds.VBUK_VBELN)) dtbl1;