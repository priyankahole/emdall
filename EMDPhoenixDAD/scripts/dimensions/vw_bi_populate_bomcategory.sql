
INSERT INTO dim_bomcategory(dim_bomcategoryid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_bomcategory
               WHERE dim_bomcategoryid = 1);

UPDATE  dim_bomcategory bc
SET 	bc.Description = t.DD07T_DDTEXT,
		bc.dw_update_date = current_timestamp
	FROM	dim_bomcategory bc, DD07T t
WHERE 	bc.RowIsCurrent = 1
		AND  t.DD07T_DOMNAME = 'STLTY'
        AND t.DD07T_DOMVALUE IS NOT NULL
        AND bc.category = t.DD07T_DOMVALUE;

delete from number_fountain m where m.table_name = 'dim_bomcategory';

insert into number_fountain
select 	'dim_bomcategory',
	ifnull(max(d.dim_bomcategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_bomcategory d
where d.dim_bomcategoryid <> 1;

INSERT INTO dim_bomcategory(Dim_BomCategoryid,
                            Category,
                            Description,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_bomcategory')
          + row_number() over(order by '') ,
          t.DD07T_DOMVALUE,
          t.DD07T_DDTEXT,
          current_timestamp,
          1
     FROM DD07T t
	 WHERE t.DD07T_DOMNAME = 'STLTY' AND t.DD07T_DOMVALUE IS NOT NULL
          AND NOT EXISTS
                     (SELECT 1
                      FROM dim_bomcategory s
                      WHERE     s.Category = t.DD07T_DOMVALUE
                             AND t.DD07T_DOMNAME = 'STLTY'
                             AND s.RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_bomcategory';
							 