UPDATE    dim_documenttype dt
SET dt.Description = t.T161T_BATXT,
			dt.dw_update_date = current_timestamp
       FROM
          t161t t, dim_documenttype dt
 WHERE dt.RowIsCurrent = 1
 AND dt.type = t.T161T_BSART AND dt.category = t.T161T_BSTYP;
 
INSERT INTO dim_documenttype(dim_documenttypeid, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_documenttype
               WHERE dim_documenttypeid = 1);

delete from number_fountain m where m.table_name = 'dim_documenttype';
   
insert into number_fountain
select 	'dim_documenttype',
	ifnull(max(d.Dim_DocumentTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_documenttype d
where d.Dim_DocumentTypeid <> 1; 
			   
INSERT INTO dim_documenttype(Dim_DocumentTypeid,
                                                        Description,
                             Category,
                             Type,
                             RowStartDate,
                             RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_documenttype')
          + row_number() over(order by ''),
                  T161T_BATXT,
          T161T_BSTYP,
          T161T_BSART,
          current_timestamp,
          1
     FROM t161t
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_documenttype
                       WHERE type = T161T_BSART AND category = T161T_BSTYP);

