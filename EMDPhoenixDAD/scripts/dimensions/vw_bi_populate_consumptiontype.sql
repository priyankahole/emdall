
INSERT INTO dim_consumptiontype(dim_consumptiontypeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_consumptiontype
               WHERE dim_consumptiontypeId = 1);
			   
UPDATE    dim_consumptiontype ct
   SET ct.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			ct.dw_update_date = current_timestamp
			FROM
          DD07T dt, dim_consumptiontype ct
 WHERE ct.RowIsCurrent = 1
 AND   DD07T_DOMNAME = 'KZVBR'
          AND DD07T_DOMVALUE IS NOT NULL
          AND ct.ConsumptionCode = dt.DD07T_DOMVALUE;
		  
delete from number_fountain m where m.table_name = 'dim_consumptiontype';

insert into number_fountain
select 	'dim_consumptiontype',
	ifnull(max(d.dim_consumptiontypeId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_consumptiontype d
where d.dim_consumptiontypeId <> 1;

INSERT INTO dim_consumptiontype(Dim_ConsumptionTypeid,
								Description,
                                ConsumptionCode,
                                RowStartDate,
                                RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_consumptiontype') 
          + row_number() over(order by '') ,
			ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'KZVBR' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_consumptiontype
                    WHERE ConsumptionCode = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'KZVBR')
   ORDER BY 2;

delete from number_fountain m where m.table_name = 'dim_consumptiontype';   