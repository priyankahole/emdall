
UPDATE 
dim_MaterialPriceGroup4 a
SET a.Description = ifnull(TVM4T_BEZEI, 'Not Set'),
    dw_update_date = current_timestamp
FROM 
dim_MaterialPriceGroup4 a,
TVM4T
WHERE a.MaterialPriceGroup4 = TVM4T_MVGR4 AND RowIsCurrent = 1;