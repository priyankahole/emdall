UPDATE    dim_objectcategory oc
       SET oc.ObjectCategoryName = t.TJ03T_TXT,
			oc.dw_update_date = current_timestamp
	   FROM
         dim_objectcategory oc, TJ03T t
 WHERE oc.RowIsCurrent = 1
      AND oc.ObjectCategoryCode = t.TJ03T_OBTYP
;

INSERT INTO dim_objectcategory(dim_objectcategoryId, RowIsCurrent,objectcategorycode,objectcategoryname,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_objectcategory
               WHERE dim_objectcategoryId = 1);

delete from number_fountain m where m.table_name = 'dim_objectcategory';
   
insert into number_fountain
select 	'dim_objectcategory',
	ifnull(max(d.dim_ObjectCategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_objectcategory d
where d.dim_ObjectCategoryid <> 1; 

INSERT INTO dim_objectcategory(dim_ObjectCategoryid,
			       ObjectCategoryCode,
                               ObjectCategoryName,
                               RowStartDate,
                               RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_objectcategory') 
          + row_number() over(order by '') ,
          t.TJ03T_OBTYP,
          t.TJ03T_TXT,
          current_timestamp,
          1
     FROM TJ03T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_objectcategory oc
               WHERE oc.ObjectCategoryCode = t.TJ03T_OBTYP
                     AND oc.RowIsCurrent = 1)
;
