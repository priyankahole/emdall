
INSERT INTO dim_catalogprofile(dim_catalogprofileId, RowIsCurrent,rowstartdate)
SELECT 1, 1,current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_catalogprofile
               WHERE dim_catalogprofileId = 1);
			   
UPDATE    dim_catalogprofile cp
  SET cp.CatalogProfileName = ifnull(t.T352B_T_RBNRX, 'Not Set'),
			cp.dw_update_date = current_timestamp
  FROM
          dim_catalogprofile cp, T352B_T t
 WHERE cp.RowIsCurrent = 1
 AND t.T352B_T_RBNR = cp.CatalogProfileCode;
 
delete from number_fountain m where m.table_name = 'dim_catalogprofile';

insert into number_fountain
select 	'dim_catalogprofile',
	ifnull(max(d.dim_catalogprofileId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_catalogprofile d
where d.dim_catalogprofileId <> 1;

INSERT INTO dim_catalogprofile(dim_catalogprofileid,
			       CatalogProfileCode,
                               CatalogProfileName,               
                               RowStartDate,
                               RowIsCurrent)
        SELECT  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_catalogprofile') 
          + row_number() over(order by ''),
           t.T352B_T_RBNR,
          t.T352B_T_RBNRX,
          current_timestamp,
          1
     FROM T352B_T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_catalogprofile s
               WHERE s.CatalogProfileCode = t.T352B_T_RBNR
                     AND s.RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_catalogprofile';
					 