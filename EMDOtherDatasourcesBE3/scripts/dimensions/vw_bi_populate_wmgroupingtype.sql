/* 	Server: QA
	Process Name: WM Grouping Type Transfer
	Interface No: 2
*/

INSERT INTO dim_wmgroupingtype
(dim_wmgroupingtypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmgroupingtype where dim_wmgroupingtypeid = 1);


UPDATE    
dim_wmgroupingtype wmgt
SET wmgt.WMGroupingTypeDescription = ifnull(dt.DD07T_DDTEXT, 'Not Set'),
    wmgt.dw_update_date = current_timestamp
FROM
dim_wmgroupingtype wmgt,
DD07T dt
WHERE wmgt.WMGroupingType = dt.DD07T_DOMVALUE 
AND wmgt.RowIsCurrent = 1
AND dt.DD07T_DOMNAME = 'KEORD' 
AND dt.DD07T_DOMNAME IS NOT NULL;
 
delete from number_fountain m where m.table_name = 'dim_wmgroupingtype';

insert into number_fountain				   
select 	'dim_wmgroupingtype',
	ifnull(max(d.dim_wmgroupingtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmgroupingtype d
where d.dim_wmgroupingtypeid <> 1;

INSERT INTO dim_wmgroupingtype(dim_wmgroupingtypeid,
								WMGroupingTypeDescription,
                               WMGroupingType,
                               RowStartDate,
                               RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmgroupingtype') 
          + row_number() over(order by '') ,
			ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'KEORD' 
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_wmgroupingtype
                    WHERE WMGroupingType = DD07T_DOMVALUE AND DD07T_DOMNAME = 'KEORD')
   ORDER BY 2;