
INSERT INTO dim_costcenter(dim_costcenterId, RowIsCurrent,ValidTo)
SELECT 1, 1, '0001-01-01'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_costcenter
               WHERE dim_costcenterId = 1);
			   
UPDATE dim_costcenter cc
   SET cc.Name = ifnull(CSKT_KTEXT, 'Not Set'),
			cc.dw_update_date = current_timestamp
FROM CSKT ck, dim_costcenter cc
 WHERE cc.RowIsCurrent = 1   
 AND  ck.CSKT_KOSTL = cc.Code
          AND cc.ControllingArea = ifnull(CSKT_KOKRS, 'Not Set')
          AND cc.ValidTo = CSKT_DATBI;
		  
delete from number_fountain m where m.table_name = 'dim_costcenter';

insert into number_fountain
select 	'dim_costcenter',
	ifnull(max(d.dim_costcenterId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_costcenter d
where d.dim_costcenterId <> 1;

INSERT INTO dim_costcenter(dim_costcenterid,
			Code,
			  Name,
			  ControllingArea,
			  ValidTo,
                          RowStartDate,
                          RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_costcenter') 
          + row_number() over(order by ''),
			CSKT_KOSTL,
	    ifnull(CSKT_KTEXT, 'Not Set'),
	    ifnull(CSKT_KOKRS, 'Not Set'),
	    CSKT_DATBI,
            current_timestamp,
            1
       FROM CSKT
      WHERE CSKT_KOSTL IS NOT NULL
	    AND NOT EXISTS
                  (SELECT 1
                     FROM dim_costcenter
                    WHERE Code = CSKT_KOSTL);
					
delete from number_fountain m where m.table_name = 'dim_costcenter';
