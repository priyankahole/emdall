drop table if exists tmp_maxdateto;
create table tmp_maxdateto as
select RSHIEDIR_HIENM ,max(RSHIEDIR_DATETO) RSHIEDIR_DATETO
from RSHIEDIR
where RSHIEDIR_HIENM in ('0COUNTRY01','0COUNTRY04','0COUNTRY05','0COUNTRY07','0COUNTRY08','0COUNTRY09','0COUNTRY10')
	and  RSHIEDIR_VERSION = 'A'
group by RSHIEDIR_HIENM;

drop table if exists tmp_countryhier_new;
create table tmp_countryhier_new as
select 	rs.RSHIEDIR_HIEID hierid
	,rs.RSHIEDIR_HIENM hiername
	,to_date(rs.RSHIEDIR_DATETO, 'YYYYMMDD') dateto
from RSHIEDIR rs, tmp_maxdateto tmp
where rs.RSHIEDIR_HIENM = tmp.RSHIEDIR_HIENM
	and  rs.RSHIEDIR_DATETO = tmp.RSHIEDIR_DATETO
	and  rs.RSHIEDIR_VERSION = 'A';

drop table if exists tmp_ch;
create table tmp_ch as
SELECT
    IFNULL(a.hieid,'Not Set') as internalhierarchyID,
    IFNULL(a.objevers,'Not Set') as objectversion,
		IFNULL(t.hiername,'Not Set') as internalhierarchyName,
	   IFNULL(a.nodeid, 0) as internalidno,
       IFNULL(a.iobjnm,'Not Set') as infoobject,
	ifnull(t.dateto,'0001-01-01') as validto,
	   	   CASE
         WHEN a.childid = 0 THEN a.nodename
         WHEN b.childid = 0 THEN b.nodename
         WHEN c.childid = 0 THEN c.nodename
         WHEN d.childid = 0 THEN d.nodename
         WHEN e.childid = 0 THEN e.nodename
		 WHEN f.childid = 0 THEN f.nodename
		 WHEN g.childid = 0 THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl7,
	   CASE
         WHEN a.tlevel = 6 and a.childid <> 0 THEN a.nodename
         WHEN b.tlevel = 6 and b.childid <> 0  THEN b.nodename
         WHEN c.tlevel = 6 and c.childid <> 0  THEN c.nodename
         WHEN d.tlevel = 6 and d.childid <> 0  THEN d.nodename
         WHEN e.tlevel = 6 and e.childid <> 0  THEN e.nodename
		 WHEN f.tlevel = 6 and f.childid <> 0  THEN f.nodename
		 WHEN g.tlevel = 6 and g.childid <> 0  THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl6,
       CASE
         WHEN a.tlevel = 5 and a.childid <> 0  THEN a.nodename
         WHEN b.tlevel = 5 and b.childid <> 0  THEN b.nodename
         WHEN c.tlevel = 5 and c.childid <> 0  THEN c.nodename
         WHEN d.tlevel = 5 and d.childid <> 0  THEN d.nodename
         WHEN e.tlevel = 5 and e.childid <> 0  THEN e.nodename
		 WHEN f.tlevel = 5 and f.childid <> 0  THEN f.nodename
		 WHEN g.tlevel = 5 and g.childid <> 0  THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl5,
       CASE
         WHEN a.tlevel = 4 and a.childid <> 0  THEN a.nodename
         WHEN b.tlevel = 4 and b.childid <> 0  THEN b.nodename
         WHEN c.tlevel = 4 and c.childid <> 0  THEN c.nodename
         WHEN d.tlevel = 4 and d.childid <> 0  THEN d.nodename
         WHEN e.tlevel = 4 and e.childid <> 0  THEN e.nodename
		 WHEN f.tlevel = 4 and f.childid <> 0  THEN f.nodename
		 WHEN g.tlevel = 4 and g.childid <> 0  THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl4,
       CASE
         WHEN a.tlevel = 3 and a.childid <> 0  THEN a.nodename
         WHEN b.tlevel = 3 and b.childid <> 0  THEN b.nodename
         WHEN c.tlevel = 3 and c.childid <> 0  THEN c.nodename
         WHEN d.tlevel = 3 and d.childid <> 0  THEN d.nodename
         WHEN e.tlevel = 3 and e.childid <> 0  THEN e.nodename
		 WHEN f.tlevel = 3 and f.childid <> 0  THEN f.nodename
		 WHEN g.tlevel = 3 and g.childid <> 0  THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl3,
       CASE
         WHEN a.tlevel = 2 and a.childid <> 0  THEN a.nodename
         WHEN b.tlevel = 2 and b.childid <> 0  THEN b.nodename
         WHEN c.tlevel = 2 and c.childid <> 0  THEN c.nodename
         WHEN d.tlevel = 2 and d.childid <> 0  THEN d.nodename
         WHEN e.tlevel = 2 and e.childid <> 0  THEN e.nodename
		 WHEN f.tlevel = 2 and f.childid <> 0  THEN f.nodename
         WHEN g.tlevel = 2 and g.childid <> 0  THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl2,
       CASE
         WHEN a.tlevel = 1 and a.childid <> 0  THEN a.nodename
         WHEN b.tlevel = 1 and b.childid <> 0  THEN b.nodename
         WHEN c.tlevel = 1 and c.childid <> 0  THEN c.nodename
         WHEN d.tlevel = 1 and d.childid <> 0  THEN d.nodename
         WHEN e.tlevel = 1 and e.childid <> 0  THEN e.nodename
		 WHEN f.tlevel = 1 and f.childid <> 0  THEN f.nodename
		 WHEN g.tlevel = 1 and g.childid <> 0  THEN g.nodename
         ELSE 'Not Set'
       END nodehierarchynamelvl1,
	   IFNULL(a.tlevel, 0) as nodehierarchylevel,
	   IFNULL(a.link,'Not Set') as nodehierarchylinkind,
	   IFNULL(a.parentid, 0) as nodehierarchyparentid,
	   IFNULL(a.childid, 0) as nodehierarchychildid,
	   IFNULL(a.nextid, 0) as nodehierarchynextid
FROM   HCOUNTRY a
       LEFT JOIN HCOUNTRY b
              ON a.hieid = b.hieid
                 AND a.parentid = b.nodeid
       LEFT JOIN HCOUNTRY c
              ON b.hieid = c.hieid
                 AND b.parentid = c.nodeid
       LEFT JOIN HCOUNTRY d
              ON c.hieid = d.hieid
                 AND c.parentid = d.nodeid
       LEFT JOIN HCOUNTRY e
              ON d.hieid = e.hieid
                 AND d.parentid = e.nodeid
		LEFT JOIN HCOUNTRY f
              ON a.hieid = f.hieid
                 AND a.parentid = f.nodeid
	    LEFT JOIN HCOUNTRY g
              ON a.hieid = g.hieid
                 AND a.parentid = g.nodeid
		INNER JOIN tmp_countryhier_new t
			ON a.hieid = t.hierid
where a.childid = 0;

merge into dim_bwhierarchycountry bw
	using tmp_ch t
	on bw.internalhierarchyid = t.internalhierarchyid
		and bw.internalidno = t.internalidno
 when matched then update set bw.nodehierarchynamelvl7 = t.nodehierarchynamelvl7,
			bw.nodehierarchynamelvl6 = t.nodehierarchynamelvl6,
			bw.nodehierarchynamelvl5 = t.nodehierarchynamelvl5,
			bw.nodehierarchynamelvl4 = t.nodehierarchynamelvl4,
			bw.nodehierarchynamelvl3 = t.nodehierarchynamelvl3,
			bw.nodehierarchynamelvl2 = t.nodehierarchynamelvl2,
			bw.nodehierarchynamelvl1 = t.nodehierarchynamelvl1,
			bw.dw_update_date = current_timestamp;

delete from number_fountain m where m.table_name = 'dim_bwhierarchycountry';
insert into number_fountain
select 'dim_bwhierarchycountry',
 ifnull(max(d.dim_bwhierarchycountryid ),
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_bwhierarchycountry d
where d.dim_bwhierarchycountryid <> 1;

merge into dim_bwhierarchycountry bw
	using ( select
			(select max_id from number_fountain where table_name = 'dim_bwhierarchycountry') + row_number() over(order by '') as dim_bwhierarchycountryid
			,internalhierarchyid
			,internalhierarchyname
			,internalidno
			,validto
			,nodehierarchynamelvl7
			,nodehierarchynamelvl6
			,nodehierarchynamelvl5
			,nodehierarchynamelvl4
			,nodehierarchynamelvl3
			,nodehierarchynamelvl2
			,nodehierarchynamelvl1
	 from tmp_ch tmp)t
	on bw.internalhierarchyid = t.internalhierarchyid
		and bw.internalidno = t.internalidno
 when not matched then insert values (t.dim_bwhierarchycountryid, t.internalhierarchyname, t.internalhierarchyid, t.internalidno,
			t.nodehierarchynamelvl7, t.nodehierarchynamelvl6, t.nodehierarchynamelvl5, t.nodehierarchynamelvl4, t.nodehierarchynamelvl3,
			t.nodehierarchynamelvl2, t.nodehierarchynamelvl1,'Not Set','Not Set','Not Set','Not Set','Not Set',
			'Not Set','Not Set', current_timestamp,current_timestamp,1,'Not Set',t.validto);

update dim_bwhierarchycountry bw
	set bw.internalhierarchydescr = ifnull(RSHIEDIRT_TXTSH,'Not Set')
from dim_bwhierarchycountry bw, RSHIEDIRT rs
where bw.internalhierarchyid = rs.RSHIEDIRT_HIEID
	and  bw.internalhierarchydescr <> ifnull(RSHIEDIRT_TXTSH,'Not Set');

update dim_bwhierarchycountry bw
	set bw.nodehierarchydesclvl1 = ifnull(tc.txtsh,'Not Set')
from dim_bwhierarchycountry bw, tcountry tc
where bw.nodehierarchynamelvl1 = tc.country
	and bw.nodehierarchydesclvl1 <> ifnull(tc.txtsh,'Not Set');

update dim_bwhierarchycountry bw
	set bw.nodehierarchydesclvl2 = ifnull(tc.txtsh,'Not Set')
from dim_bwhierarchycountry bw, tcountry tc
where bw.nodehierarchynamelvl2 = tc.country
	and bw.nodehierarchydesclvl2 <> ifnull(tc.txtsh,'Not Set');

update dim_bwhierarchycountry bw
	set bw.nodehierarchydesclvl3 = ifnull(tc.txtsh,'Not Set')
from dim_bwhierarchycountry bw, tcountry tc
where bw.nodehierarchynamelvl3 = tc.country
	and bw.nodehierarchydesclvl3 <> ifnull(tc.txtsh,'Not Set');

update dim_bwhierarchycountry bw
	set bw.nodehierarchydesclvl4 = ifnull(tc.txtsh,'Not Set')
from dim_bwhierarchycountry bw, tcountry tc
where bw.nodehierarchynamelvl4 = tc.country
	and bw.nodehierarchydesclvl4 <> ifnull(tc.txtsh,'Not Set');

update dim_bwhierarchycountry bw
	set bw.nodehierarchydesclvl5 = ifnull(tc.txtsh,'Not Set')
from dim_bwhierarchycountry bw, tcountry tc
where bw.nodehierarchynamelvl5 = tc.country
	and bw.nodehierarchydesclvl5 <> ifnull(tc.txtsh,'Not Set');

update dim_bwhierarchycountry bw
	set bw.nodehierarchydesclvl6 = ifnull(tc.txtsh,'Not Set')
from dim_bwhierarchycountry bw, tcountry tc
where bw.nodehierarchynamelvl6 = tc.country
	and bw.nodehierarchydesclvl6 <> ifnull(tc.txtsh,'Not Set');

update dim_bwhierarchycountry bw
	set bw.nodehierarchydesclvl7 = ifnull(tc.txtsh,'Not Set')
from dim_bwhierarchycountry bw, tcountry tc
where bw.nodehierarchynamelvl7 = tc.country
	and bw.nodehierarchydesclvl7 <> ifnull(tc.txtsh,'Not Set');

update dim_bwhierarchycountry bw
	set bw.validto = ifnull(to_date(rs.RSHIEDIR_DATETO, 'YYYYMMDD'),'0001-01-01')
from dim_bwhierarchycountry bw, RSHIEDIR rs
where bw.internalhierarchyid = rs.RSHIEDIR_HIEID
	and bw.validto <> ifnull(to_date(rs.RSHIEDIR_DATETO, 'YYYYMMDD'),'0001-01-01');
