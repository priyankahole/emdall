/******************************************************************************************************************/
/*   Script         : vw_bi_populate_twimpactedbatch_products_dim                                                 */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 06 Nov 2018                                                                                 */
/*   Description    : Populating script of dim_twimpactedbatch_products                                           */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   06 Nov 2016         Cristian T      1.0               Creating the script.                                   */
/*   14 May 2019         Cristian T      1.1               Changed the source of data to HDFS                     */
/******************************************************************************************************************/

INSERT INTO dim_twimpactedbatch_products(
dim_twimpactedbatch_productsid
)
SELECT 1 as dim_twimpactedbatch_productsid
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_twimpactedbatch_products WHERE dim_twimpactedbatch_productsid = 1);

DELETE FROM number_fountain m WHERE m.table_name = 'dim_twimpactedbatch_products';

INSERT INTO number_fountain
SELECT 	'dim_twimpactedbatch_products',
	    IFNULL(MAX(d.dim_twimpactedbatch_productsid),IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_twimpactedbatch_products d
WHERE d.dim_twimpactedbatch_productsid <> 1;

DELETE FROM dim_twimpactedbatch_products WHERE dim_twimpactedbatch_productsid <> 1;

INSERT INTO dim_twimpactedbatch_products(
dim_twimpactedbatch_productsid,
dw_insert_date,
dw_update_date,
rowiscurrent,
projectsourceid,
pr_id,
hc_product_name,
hc_item_code,
hc_batch_number,
hc_study_number,
hc_initial_risk_assessment
)
SELECT (SELECT IFNULL(m.max_id, 1) FROM number_fountain m WHERE m.table_name = 'dim_twimpactedbatch_products') + ROW_NUMBER() over(order by pr_id) AS dim_twimpactedbatch_productsid,
       t.*
FROM (
SELECT current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as rowiscurrent,
       1 as projectsourceid,
       ifnull(dev.pr_id, 0) as pr_id,
       ifnull(dev.hc_product_name, 'Not Set') as hc_product_name,
       ifnull(dev.hc_item_code, 'Not Set') as hc_item_code,
       ifnull(dev.hc_batch_number, 'Not Set') as hc_batch_number,
       ifnull(dev.hc_study_number, 'Not Set') as hc_study_number,
       ifnull(dev.hc_initial_risk_assessment, 'Not Set') as hc_initial_risk_assessment
FROM tw_rpt_hc_dev_grid_risk dev
UNION ALL
SELECT current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as rowiscurrent,
       1 as projectsourceid,
       ifnull(capa.pr_id, 0) as pr_id,
       ifnull(capa.hc_product_name, 'Not Set') as hc_product_name,
       ifnull(capa.hc_item_code, 'Not Set') as hc_item_code,
       ifnull(capa.hc_batch_number, 'Not Set') as hc_batch_number,
       ifnull(capa.hc_study_number, 'Not Set') as hc_study_number,
       'Not Set' as hc_initial_risk_assessment
FROM tw_rpt_capa_pi capa
     ) t;

UPDATE dim_twimpactedbatch_products dim
SET dim.projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     dim_twimpactedbatch_products dim;

DELETE FROM emd586.dim_twimpactedbatch_products;
INSERT INTO emd586.dim_twimpactedbatch_products(
dim_twimpactedbatch_productsid,
dw_insert_date,
dw_update_date,
rowiscurrent,
projectsourceid,
pr_id,
hc_product_name,
hc_item_code,
hc_batch_number,
hc_study_number,
hc_initial_risk_assessment
)
SELECT dim_twimpactedbatch_productsid,
       dw_insert_date,
       dw_update_date,
       rowiscurrent,
       projectsourceid,
       pr_id,
       hc_product_name,
       hc_item_code,
       hc_batch_number,
       hc_study_number,
       hc_initial_risk_assessment
FROM dim_twimpactedbatch_products;
