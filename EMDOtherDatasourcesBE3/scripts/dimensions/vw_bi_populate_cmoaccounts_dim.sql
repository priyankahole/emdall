/******************************************************************************************************************/
/*   Script         : vw_bi_populate_cmoaccounts_dim                                                              */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 06 Nov 2018                                                                                 */
/*   Description    : Populating script of dim_cmoaccounts                                                        */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   09 Nov 2016         Cristian T      1.0               Creating the script.                                   */
/******************************************************************************************************************/

INSERT INTO dim_cmoaccounts(
dim_cmoaccountsid
)
SELECT 1 as dim_cmoaccountsid
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_cmoaccounts WHERE dim_cmoaccountsid = 1);

DELETE FROM number_fountain m WHERE m.table_name = 'dim_cmoaccounts';

INSERT INTO number_fountain
SELECT 	'dim_cmoaccounts',
	    IFNULL(MAX(d.dim_cmoaccountsid),IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_cmoaccounts d
WHERE d.dim_cmoaccountsid <> 1;


INSERT INTO dim_cmoaccounts(
dim_cmoaccountsid,
dw_insert_date,
dw_update_date,
rowiscurrent,
projectsourceid,
cmo_id,
cmo_id_wo_last3_char,
cmo_name,
cmo_billing_country,
cmo_mb_regions,
cmo_status_mb,
cmo_type,
cmo_record_type_id,
cmo_merck_biopharma_flag,
cmo_merck_pharma_flag,
cmo_mb_level
)
SELECT (SELECT IFNULL(m.max_id, 1) FROM number_fountain m WHERE m.table_name = 'dim_cmoaccounts') + ROW_NUMBER() over(order by '') AS dim_cmoaccountsid,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as rowiscurrent,
       1 as projectsourceid,
       ifnull(cmo_id, 'Not Set') as cmo_id,
       ifnull(cmo_id_wo_last3_char, 'Not Set') as cmo_id_wo_last3_char,
       ifnull(cmo_name, 'Not Set') as cmo_name,
       ifnull(cmo_billing_country, 'Not Set') as cmo_billing_country,
       ifnull(cmo_mb_regions, 'Not Set') as cmo_mb_regions,
       ifnull(cmo_status_mb, 'Not Set') as cmo_status_mb,
       ifnull(cmo_type, 'Not Set') as cmo_type,
       ifnull(cmo_record_type_id, 'Not Set') as cmo_record_type_id,
       ifnull(cmo_merck_biopharma_flag, 'Not Set') as cmo_merck_biopharma_flag,
       ifnull(cmo_merck_pharma_flag, 'Not Set') as cmo_merck_pharma_flag,
       ifnull(cmo_mb_level, 'Not Set') as cmo_mb_level
FROM cmo_accounts dev
WHERE NOT EXISTS (SELECT 1
                  FROM dim_cmoaccounts dim
                  WHERE dim.cmo_id = dev.cmo_id);

UPDATE dim_cmoaccounts dim
SET dim.projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     dim_cmoaccounts dim;

UPDATE dim_cmoaccounts dim
SET cmo_name = ifnull(dev.cmo_name, 'Not Set'),
    cmo_billing_country = ifnull(dev.cmo_billing_country, 'Not Set'),
    cmo_mb_regions = ifnull(dev.cmo_mb_regions, 'Not Set'),
    cmo_status_mb = ifnull(dev.cmo_status_mb, 'Not Set'),
    cmo_type = ifnull(dev.cmo_type, 'Not Set'),
    cmo_record_type_id = ifnull(dev.cmo_record_type_id, 'Not Set'),
    cmo_merck_biopharma_flag = ifnull(dev.cmo_merck_biopharma_flag, 'Not Set'),
    cmo_merck_pharma_flag = ifnull(dev.cmo_merck_pharma_flag, 'Not Set'),
    cmo_mb_level = ifnull(dev.cmo_mb_level, 'Not Set'),
    dw_update_date = current_timestamp
FROM dim_cmoaccounts dim,
     cmo_accounts dev
WHERE dim.cmo_id = dev.cmo_id;
/*
DELETE FROM emd586.dim_cmoaccounts WHERE projectsourceid = 14 and dim_cmoaccountsid <> 1
INSERT INTO emd586.dim_cmoaccounts(
dim_cmoaccountsid,
dw_insert_date,
dw_update_date,
rowiscurrent,
projectsourceid,
cmo_id,
cmo_id_wo_last3_char,
cmo_name,
cmo_billing_country,
cmo_mb_regions,
cmo_status_mb,
cmo_type,
cmo_record_type_id,
cmo_merck_biopharma_flag,
cmo_merck_pharma_flag,
cmo_mb_level
)
SELECT dim_cmoaccountsid,
       dw_insert_date,
       dw_update_date,
       rowiscurrent,
       projectsourceid,
       cmo_id,
       cmo_id_wo_last3_char,
       cmo_name,
       cmo_billing_country,
       cmo_mb_regions,
       cmo_status_mb,
       cmo_type,
       cmo_record_type_id,
       cmo_merck_biopharma_flag,
       cmo_merck_pharma_flag,
       cmo_mb_level
FROM dim_cmoaccounts
WHERE dim_cmoaccountsid <> 1
*/