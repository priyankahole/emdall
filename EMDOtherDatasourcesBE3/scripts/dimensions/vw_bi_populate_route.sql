UPDATE    dim_route r
   SET r.RouteName = t.TVROT_BEZEI,
       r.TransitTime = t.TVRO_TRAZT,
       r.TransitTimeInCalendarDays = t.TVRO_TRAZTD,
       r.TransportLeadTimeInCalendarDays = t.TVRO_TDVZTD,
	   -- r.factorycalendar = ifnull(t.tvro_spfbk,'Not Set'),
	   -- r.modeoftransport = ifnull(t.tvro_expvz,'Not Set'),
	   --r.shippingtype = ifnull(t.tvro_vsart,'Not Set'),
			r.dw_update_date = current_timestamp
			FROM
          dim_route r,TVROT t
 WHERE r.RowIsCurrent = 1
 AND r.RouteCode = t.TVROT_ROUTE;

INSERT INTO dim_route(dim_routeId, RowIsCurrent, transportleadtimeincalendardays,routecode,routename,rowstartdate,transittimeincalendardays)
SELECT 1, 1, 0,'Not Set','Not Set',current_timestamp,0
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_route
               WHERE dim_routeId = 1);

delete from number_fountain m where m.table_name = 'dim_route';

insert into number_fountain
select 	'dim_route',
	ifnull(max(d.dim_routeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_route d
where d.dim_routeid <> 1; 

INSERT INTO dim_route(dim_routeid,
                                RouteCode,
                                RouteName,
			                          TransitTime,
                                TransitTimeInCalendarDays,
                                TransportLeadTimeInCalendarDays,
								-- factorycalendar,
								-- modeoftransport,
								-- shippingtype,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_route') 
          + row_number() over(order by ''),
			  t.TVROT_ROUTE,
          t.TVROT_BEZEI,
	  t.TVRO_TRAZT,
	  t.TVRO_TRAZTD,
    t.TVRO_TDVZTD,
	--ifnull(t.tvro_spfbk,'Not Set'),
	--ifnull(t.tvro_expvz,'Not Set'),
	--ifnull(t.tvro_vsart,'Not Set'),
          current_timestamp,
          1
     FROM TVROT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_route r
               WHERE r.RouteCode = t.TVROT_ROUTE AND r.RowIsCurrent = 1)
;

