/******************************************************************************************************************/
/*   Script         : bi_populate_gamedmachinegroup_dim                                                           */
/*   Author         : CristianT                                                                                   */
/*   Created On     : 25 Apr 2017                                                                                 */
/*   Description    : Populating script of dim_gamedmachinegroup                                                  */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   25 Apr 2016         CristianT      1.0               Creating the script.                                    */
/******************************************************************************************************************/

DELETE FROM dim_gamedmachinegroup
WHERE dim_gamedmachinegroupid <> 1;

INSERT INTO dim_gamedmachinegroup(
dim_gamedmachinegroupid
)
SELECT 1 as dim_gamedmachinegroupid
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_gamedmachinegroup WHERE dim_gamedmachinegroupid = 1);

DELETE FROM number_fountain m WHERE m.table_name = 'dim_gamedmachinegroup';

INSERT INTO number_fountain
SELECT 	'dim_gamedmachinegroup',
	    IFNULL(MAX(d.dim_gamedmachinegroupid),IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_gamedmachinegroup d
WHERE d.dim_gamedmachinegroupid <> 1;

INSERT INTO dim_gamedmachinegroup(
dim_gamedmachinegroupid,
site_name,
machine_grp_id,
machine_grp_name,
machine_id,
machine_name,
dw_insert_date,
dw_update_date,
projectsourceid,
rowiscurrent
)
SELECT (SELECT IFNULL(m.max_id, 1) FROM number_fountain m WHERE m.table_name = 'dim_gamedmachinegroup') + ROW_NUMBER() over(order by '') AS dim_gamedmachinegroupid,
       t.*
FROM (
       SELECT ifnull(grp.SITE_NAME, 'Not Set') as site_name,
              ifnull(grp.MACHINE_GRP_ID, 0) as machine_grp_id,
              ifnull(grp.MACHINE_GRP_NAME, 'Not Set') as machine_grp_name,
              ifnull(grp.MACHINE_ID, 0) as machine_id,
              ifnull(grp.MACHINE_NAME, 'Not Set') as machine_name,
              current_timestamp as dw_insert_date,
              current_timestamp as dw_update_date,
              1 as projectsourceid,
              1 as rowiscurrent
       FROM oee_machine_grp_extract grp
     ) t;

UPDATE dim_gamedmachinegroup dim
SET dim.projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     dim_gamedmachinegroup dim;

DELETE FROM emd586.dim_gamedmachinegroup;
INSERT INTO emd586.dim_gamedmachinegroup
SELECT *
FROM dim_gamedmachinegroup;