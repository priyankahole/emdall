/*Insert Not Set record */
insert into dim_customerpartnerfunctions 
(dim_customerpartnerfunctionsid, rowchangereason, rowenddate, rowiscurrent, rowstartdate, 
customernumber1, customername1, salesorgcode, salesorgdesc, distributionchannelcode, 
distributionchannelname, divisioncode, divisionname, partnerfunction, partnerfunctiondesc, 
partnercounter, customernumberbusinesspartner, customernamebusinesspartner, vendoraccnumber, 
vendorname, personalnumber, contactpersonnumber, customerdescriptionofpartner, defaultpartner
) 
SELECT 1,null,null,1,current_timestamp,
'Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set',
'Not Set',0,0,'Not Set','Not Set'
FROM (SELECT 1) a
where not exists(select 1 from dim_customerpartnerfunctions where dim_customerpartnerfunctionsid = 1);


UPDATE dim_customerpartnerfunctions cpf 
   SET    CustomerNumberBusinessPartner = ifnull(kp.KNVP_KUNN2,'Not Set'),
          VendorAccNumber= ifnull(kp.KNVP_LIFNR,'Not Set') ,
          PersonalNumber = ifnull(kp.KNVP_PERNR, 0),
          ContactPersonNumber = ifnull(kp.KNVP_PARNR, 0),
          CustomerDescriptionofPartner = ifnull(kp.KNVP_KNREF, 'Not Set'),
          DefaultPartner = ifnull(kp.KNVP_DEFPA, 'Not Set'),
		  dw_update_date = current_timestamp
FROM KNVP kp, dim_customerpartnerfunctions cpf
WHERE   cpf.CustomerNumber1 = kp.KNVP_KUNNR
	AND cpf.SalesOrgCode = kp.KNVP_VKORG
	AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
	AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
	AND cpf.PartnerFunction = kp.KNVP_PARVW
	AND cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
	AND cpf.RowIsCurrent = 1;
							
/* CustomerName1 */
UPDATE dim_customerpartnerfunctions cpf 
SET cpf.CustomerName1 = ifnull(k1.KNA1_NAME1, 'Not Set'),
    cpf.dw_update_date = current_timestamp
FROM dim_customerpartnerfunctions cpf
		inner join KNVP kp on cpf.CustomerNumber1 = kp.KNVP_KUNNR and 
							  cpf.SalesOrgCode    = kp.KNVP_VKORG and
							  cpf.DivisionCode    = ifnull(kp.KNVP_SPART, 'Not Set') and 
							  cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set') and
							  cpf.PartnerFunction = kp.KNVP_PARVW and 
							  cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
		left join KNA1 k1 on k1.KNA1_KUNNR = kp.KNVP_KUNNR
WHERE    cpf.RowIsCurrent = 1
     AND cpf.CustomerName1 <> ifnull(k1.KNA1_NAME1,'Not Set');
	 
/* DistributionChannelName */
UPDATE dim_customerpartnerfunctions cpf 
SET cpf.DistributionChannelName = ifnull(k1.TVTWT_VTEXT, 'Not Set'),
    cpf.dw_update_date = current_timestamp
FROM dim_customerpartnerfunctions cpf
		inner join KNVP kp on cpf.CustomerNumber1 = kp.KNVP_KUNNR and 
							  cpf.SalesOrgCode    = kp.KNVP_VKORG and
							  cpf.DivisionCode    = ifnull(kp.KNVP_SPART, 'Not Set') and 
							  cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set') and
							  cpf.PartnerFunction = kp.KNVP_PARVW and 
							  cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
		left join TVTWT k1 on k1.TVTWT_VTWEG = kp.KNVP_VTWEG
WHERE    cpf.RowIsCurrent = 1
     AND cpf.DistributionChannelName <> ifnull(k1.TVTWT_VTEXT,'Not Set');
	 
/* DivisionName */
UPDATE dim_customerpartnerfunctions cpf 
SET cpf.DivisionName = ifnull(k1.TSPAT_VTEXT, 'Not Set'),
    cpf.dw_update_date = current_timestamp
FROM dim_customerpartnerfunctions cpf
		inner join KNVP kp on cpf.CustomerNumber1 = kp.KNVP_KUNNR and 
							  cpf.SalesOrgCode    = kp.KNVP_VKORG and
							  cpf.DivisionCode    = ifnull(kp.KNVP_SPART, 'Not Set') and 
							  cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set') and
							  cpf.PartnerFunction = kp.KNVP_PARVW and 
							  cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
		left join TSPAT k1 on k1.TSPAT_SPART = kp.KNVP_SPART
WHERE    cpf.RowIsCurrent = 1
     AND cpf.DivisionName <> ifnull(k1.TSPAT_VTEXT,'Not Set');
	 
/* PartnerFunctionDesc */
UPDATE dim_customerpartnerfunctions cpf 
SET cpf.PartnerFunctionDesc = ifnull(k1.TPART_VTEXT, 'Not Set'),
    cpf.dw_update_date = current_timestamp
FROM dim_customerpartnerfunctions cpf
		inner join KNVP kp on cpf.CustomerNumber1 = kp.KNVP_KUNNR and 
							  cpf.SalesOrgCode    = kp.KNVP_VKORG and
							  cpf.DivisionCode    = ifnull(kp.KNVP_SPART, 'Not Set') and 
							  cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set') and
							  cpf.PartnerFunction = kp.KNVP_PARVW and 
							  cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
		left join TPART k1 on k1.TPART_PARVW = kp.KNVP_PARVW
WHERE    cpf.RowIsCurrent = 1
     AND cpf.PartnerFunctionDesc <> ifnull(k1.TPART_VTEXT,'Not Set');

/* SalesOrgDesc */
UPDATE dim_customerpartnerfunctions cpf 
SET cpf.SalesOrgDesc = ifnull(k1.TVKOT_VTEXT, 'Not Set'),
    cpf.dw_update_date = current_timestamp
FROM dim_customerpartnerfunctions cpf
		inner join KNVP kp on cpf.CustomerNumber1 = kp.KNVP_KUNNR and 
							  cpf.SalesOrgCode    = kp.KNVP_VKORG and
							  cpf.DivisionCode    = ifnull(kp.KNVP_SPART, 'Not Set') and 
							  cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set') and
							  cpf.PartnerFunction = kp.KNVP_PARVW and 
							  cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
		left join TVKOT k1 on k1.TVKOT_VKORG = kp.KNVP_VKORG
WHERE    cpf.RowIsCurrent = 1
     AND cpf.SalesOrgDesc <> ifnull(k1.TVKOT_VTEXT,'Not Set');
	 
/* CustomerNameBusinessPartner */
UPDATE dim_customerpartnerfunctions cpf 
SET cpf.CustomerNameBusinessPartner = ifnull(k1.KNA1_NAME1, 'Not Set'),
    cpf.dw_update_date = current_timestamp
FROM dim_customerpartnerfunctions cpf
		inner join KNVP kp on cpf.CustomerNumber1 = kp.KNVP_KUNNR and 
							  cpf.SalesOrgCode    = kp.KNVP_VKORG and
							  cpf.DivisionCode    = ifnull(kp.KNVP_SPART, 'Not Set') and 
							  cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set') and
							  cpf.PartnerFunction = kp.KNVP_PARVW and 
							  cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
		left join KNA1 k1 on k1.KNA1_KUNNR = kp.KNVP_KUNN2
WHERE    cpf.RowIsCurrent = 1
     AND cpf.CustomerNameBusinessPartner <> ifnull(k1.KNA1_NAME1,'Not Set');

/* VendorName */
UPDATE dim_customerpartnerfunctions cpf 
SET cpf.VendorName = ifnull(k1.NAME1, 'Not Set'),
    cpf.dw_update_date = current_timestamp
FROM dim_customerpartnerfunctions cpf
		inner join KNVP kp on cpf.CustomerNumber1 = kp.KNVP_KUNNR and 
							  cpf.SalesOrgCode    = kp.KNVP_VKORG and
							  cpf.DivisionCode    = ifnull(kp.KNVP_SPART, 'Not Set') and 
							  cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set') and
							  cpf.PartnerFunction = kp.KNVP_PARVW and 
							  cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
		left join LFA1 k1 on k1.LIFNR = kp.KNVP_LIFNR
WHERE    cpf.RowIsCurrent = 1
     AND cpf.VendorName <> ifnull(k1.NAME1,'Not Set');
	 
	 
/* Start of Populate Script */

delete from NUMBER_FOUNTAIN where table_name = 'dim_customerpartnerfunctions';
     
INSERT INTO NUMBER_FOUNTAIN
select 	'dim_customerpartnerfunctions',
	ifnull(max(d.dim_customerpartnerfunctionsid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customerpartnerfunctions d
where d.dim_customerpartnerfunctionsid <> 1;

     INSERT INTO dim_customerpartnerfunctions(dim_customerpartnerfunctionsid,
                                         CustomerNumber1,
                                         CustomerName1,
                                         DistributionChannelCode,
                                         DistributionChannelName,
                                         DivisionCode,
                                         DivisionName,
                                         PartnerFunction,
                                         PartnerFunctionDesc,
                                         SalesOrgCode,
                                         SalesOrgDesc,
                                         PartnerCounter,
                                         CustomerNumberBusinessPartner,
                                         CustomerNameBusinessPartner,
                                         VendorAccNumber,
                                         VendorName,
                                         PersonalNumber,
                                         ContactPersonNumber,
                                         CustomerDescriptionofPartner,
                                         DefaultPartner,
                                         RowStartDate,
                                         RowIsCurrent)
   SELECT (SELECT ifnull(m.max_id, 1) 
		   from NUMBER_FOUNTAIN m 
		   WHERE m.table_name = 'dim_customerpartnerfunctions') + row_number() over (order by '') dim_customerpartnerfunctionsid,
		   src.*
    from (select distinct
          kp.KNVP_KUNNR CustomerNumber1,
          ifnull(k1.KNA1_NAME1,'Not Set')
          CustomerName1,
          ifnull(tv1.TVTWT_VTWEG,'Not Set')
          DistChannelCode,
          ifnull(tv2.TVTWT_VTEXT,'Not Set')
          DistChannelName,
          ifnull(ts.TSPAT_SPART,'Not Set')
          DivisionCode,
          ifnull(ts1.TSPAT_VTEXT,'Not Set')
          DivisionName,
          ifnull(tp.TPART_PARVW,'Not Set') PartnerFunction,
          ifnull(tp.TPART_VTEXT,'Not Set') PartnerFunctionDesc,
          ifnull(tvk.TVKOT_VKORG,'Not Set')
          SalesOrgCode,
          ifnull(tvk1.TVKOT_VTEXT,'Not Set')
          SalesOrgDesc,
          ifnull(cast(kp.KNVP_PARZA as varchar(100)), 'Not Set') PartnerCounter,
          kp.KNVP_KUNN2 CustomerNumberBusinessPartner,
          ifnull(k2.KNA1_NAME1,'Not Set')
          CustomerNameBusinessPartner,
          kp.KNVP_LIFNR VendorAccNumber,
          ifnull(l1.NAME1,'Not Set')
          VendorName,
          ifnull(kp.KNVP_PERNR, 0) PersonalNumber,
          ifnull(kp.KNVP_PARNR, 0) ContactPersonNumber,
          ifnull(kp.KNVP_KNREF, 'Not Set') CustomerDescriptionofPartner,
          ifnull(kp.KNVP_DEFPA, 'Not Set') DefaultPartner,
          current_date,
          1
          FROM KNVP kp
          INNER JOIN tpart tp ON kp.KNVP_PARVW = tp.TPART_PARVW
		  LEFT JOIN KNA1 k1 ON k1.KNA1_KUNNR = kp.KNVP_KUNNR
		  LEFT JOIN TVTWT tv1 ON tv1.TVTWT_VTWEG = kp.KNVP_VTWEG
		  LEFT JOIN TVTWT tv2 ON tv2.TVTWT_VTWEG = kp.KNVP_VTWEG
		  LEFT JOIN TSPAT ts ON ts.TSPAT_SPART = kp.KNVP_SPART
		  LEFT JOIN TSPAT ts1 ON ts1.TSPAT_SPART = kp.KNVP_SPART
		  LEFT JOIN TVKOT tvk ON tvk.TVKOT_VKORG = kp.KNVP_VKORG
		  LEFT JOIN TVKOT tvk1 ON tvk1.TVKOT_VKORG = kp.KNVP_VKORG
		  LEFT JOIN KNA1 k2 ON k2.KNA1_KUNNR = kp.KNVP_KUNN2
		  LEFT JOIN LFA1 l1 ON l1.LIFNR = kp.KNVP_LIFNR
		  WHERE NOT EXISTS
                     (SELECT 1
                        FROM dim_customerpartnerfunctions cpf
                       WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
                            AND cpf.SalesOrgCode = kp.KNVP_VKORG
                            AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
                            AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
                            AND cpf.PartnerFunction = kp.KNVP_PARVW
                            AND cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set'))
		) src;

delete from NUMBER_FOUNTAIN where table_name = 'dim_customerpartnerfunctions';
