DELETE FROM dim_workcenter;
INSERT INTO dim_workcenter
(
dim_workcenterid,
objectid,
objecttype,
headerstartdate,
headerenddate,
headerchgdon_grnd,
headerusername_grnd,
headerchgdon_vora,
headerusername_vora,
headerchgdon_term,
headerusername_term,
headerchgdon_tech,
headerusername_tech,
workcenter,
plant,
workcentercategory,
headerstdtextkey,
headercontrolkey,
languagekey,
wctextchangedon,
wctextusername_vora,
ktext_description,
ktext_up_description,
application,
controlkeydescription,
activitytypeset,
costcenterenddate,
costcenterstartdate,
costcenternumber,
costcenterchangedon,
costcenterusername,
costcentercontrollingarea,
costcentername,
costcenteractivitytype,
costcenterrefindicator,
ccformulakeycosting,
ccbusinessprocess,
ccactivitydescriptiontype 
)
SELECT ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)
       + row_number() over (order by crhd_objid ) dim_workcenterid,
ifnull(crhd_objid,'Not Set'),
ifnull(crhd_objty,'Not Set'),
ifnull(crhd_begda,'1 Jan 1900'),
ifnull(crhd_endda,'1 Jan 1900'),
ifnull(crhd_aedat_grnd,'1 Jan 1900'),
ifnull(crhd_aenam_grnd,'Not Set'),
ifnull(crhd_aedat_vora,'1 Jan 1900'),
ifnull(crhd_aenam_vora,'Not Set'),
ifnull(crhd_aedat_term,'1 Jan 1900'),
ifnull(crhd_aenam_term,'Not Set'),
ifnull(crhd_aedat_tech,'1 Jan 1900'),
ifnull(crhd_aenam_tech,'Not Set'),
ifnull(crhd_arbpl,'Not Set'),
ifnull(crhd_werks,'Not Set'),
ifnull(crhd_verwe,'Not Set'),
ifnull(crhd_ktsch,'Not Set'),
ifnull(crhd_steus,'Not Set'),
ifnull(crtx_spras,'Not Set'),
ifnull(crtx_aedat_text,'1 Jan 1900'),
ifnull(crtx_aenam_text,'Not Set'),
ifnull(crtx_ktext,'Not Set'),
ifnull(crtx_ktext_up,'Not Set'),
ifnull(t430t_plnaw,'Not Set'),
ifnull(t430t_txt,'Not Set'),
ifnull(crco_LASET,'Not Set'),
ifnull(crco_ENDDA,'1 Jan 1900'),
ifnull(crco_BEGDA,'1 Jan 1900'),
ifnull(crco_LANUM,'Not Set'),
ifnull(crco_AEDAT_KOST,'1 Jan 1900'),
ifnull(crco_AENAM_KOST,'Not Set'),
ifnull(crco_KOKRS,'Not Set'),
ifnull(crco_KOSTL,'Not Set'),
ifnull(crco_LSTAR,'Not Set'),
ifnull(crco_LSTAR_REF,'Not Set'),
ifnull(crco_FORML,'Not Set'),
ifnull(crco_PRZ,'Not Set'),
ifnull(crco_ACTXY,'Not Set')

FROM CRHD LEFT OUTER JOIN CRTX ON crhd_objid = crtx_objid 
	LEFT OUTER JOIN CRCO on crhd_objid = crco_objid 
	LEFT OUTER JOIN T430T ON crhd_steus = t430t_steus AND crtx_spras = t430t_spras
ORDER BY crhd_objid;

UPDATE dim_workcenter
set dim_workcenterid = dim_workcenterid + 1;

INSERT INTO dim_workcenter
(dim_workcenterid)
SELECT 1;
