/******************************************************************************************************************/
/*   Script         : bi_populate_qualityusers_dim                                                                */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 22 Nov 2016                                                                                 */
/*   Description    : Populating script of dim_qualityusers                                                       */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   22 Nov 2016         Cristian T      1.0               Creating the script.                                   */
/*   05 Sept 2019        Cristian B      2.0               Adding logic for snapshots.                            */
/******************************************************************************************************************/

/* 24 Aug 2017 CristianT Start: Added specific number_fountain table for the autocommit property */

DROP TABLE IF EXISTS number_fountain_dim_qualityusers;
CREATE TABLE number_fountain_dim_qualityusers LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

/* 24 Aug 2017 CristianT End */

INSERT INTO dim_qualityusers(
dim_qualityusersid
)
SELECT 1 as dim_qualityusersid
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_qualityusers WHERE dim_qualityusersid = 1);

DELETE FROM number_fountain_dim_qualityusers m WHERE m.table_name = 'dim_qualityusers';

INSERT INTO number_fountain_dim_qualityusers
SELECT 	'dim_qualityusers',
	    IFNULL(MAX(d.dim_qualityusersid),IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_qualityusers d
WHERE d.dim_qualityusersid <> 1;

UPDATE dim_qualityusers
SET rowiscurrent = 0
WHERE dim_qualityusersid <> 1;

INSERT INTO dim_qualityusers(
dim_qualityusersid,
merck_uid,
first_name,
last_name,
fn_ln,
ln_fn,
mail,
cmgnumber,
costcenter,
department_short_name,
department_long_name,
department,
temporary_worker_flag,
department_number,
employee_manager,
contact_person,
dw_insert_date,
dw_update_date,
projectsourceid,
dd_service,
dd_subservice,
dd_site,
dd_country,
dd_departmenttype,
rowiscurrent,
userid
)
SELECT (SELECT IFNULL(m.max_id, 1) FROM number_fountain_dim_qualityusers m WHERE m.table_name = 'dim_qualityusers') + ROW_NUMBER() over(order by t.merck_uid) AS dim_qualityusersid,
       t.*
FROM (
SELECT DISTINCT ifnull(usr.merck_uid, 'Not Set') as merck_uid,
       ifnull(usr.first_name, 'Not Set') as first_name,
       ifnull(usr.last_name, 'Not Set') as last_name,
       ifnull(usr.first_name || ' ' || usr.last_name, 'Not Set') as fn_ln,
       ifnull(usr.last_name || ', ' || usr.first_name, 'Not Set') as ln_fn,
       ifnull(usr.mail, 'Not Set') as mail,
       ifnull(usr.cmgnumber, 'Not Set') as cmgnumber,
       ifnull(usr.costcenter, 'Not Set') as costcenter,
       ifnull(usr.department_short_name, 'Not Set') as department_short_name,
       ifnull(usr.department_long_name, 'Not Set') as department_long_name,
       ifnull(usr.department, 'Not Set') as department,
       ifnull(usr.temporary_worker_flag, 'Not Set') as temporary_worker_flag,
       ifnull(usr.department_number, 'Not Set') as department_number,
       ifnull(usr.employee_manager, 'Not Set') as employee_manager,
       ifnull(usr.contact_person, 'Not Set') as contact_person,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as projectsourceid,
       'Not Set' as dd_service,
       'Not Set' as dd_subservice,
       'Not Set' as dd_site,
       'Not Set' as dd_country,
       'Not Set' as dd_departmenttype,
       1 as rowiscurrent,
       ifnull(usr.userid, 'Not Set') as userid
FROM cdm_users usr) AS t;

UPDATE dim_qualityusers dim
SET dim.projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     dim_qualityusers dim;

/* Mask German Names */
UPDATE dim_qualityusers dim
SET dim.fn_ln = 'Anonymus'
WHERE dim.cmgnumber = '1500'
  AND rowiscurrent = 1;

UPDATE dim_qualityusers dim
SET dim.ln_fn = 'Anonymus'
WHERE dim.cmgnumber = '1500'
  AND rowiscurrent = 1;
/* Mask German Names */

/*29 Aug 2019 - AR - APP-12613 reverted to this update after elimination of duplicates on dptmap*/
UPDATE dim_qualityusers dim
SET dim.dd_service = ifnull(dpt.service, 'Not Set'),
    dim.dd_subservice = ifnull(dpt.sub_service, 'Not Set'),
    dim.dd_site = ifnull(dpt.site, 'Not Set'),
    dim.dd_country = ifnull(dpt.country, 'Not Set'),
    dim.dd_departmenttype = ifnull(dpt.department, 'Not Set')
FROM dptmap dpt,
     dim_qualityusers dim
WHERE dim.department_number = dpt.department_number
  AND dim.rowiscurrent = 1;

/* 26 Aug 2019 - AR - APP-12613
merge into dim_qualityusers dim
using
(select max(service) as service, department_number, max(sub_service) as sub_service, site, country, department
from dptmap
group by department_number, site, country, department) dpt
on dim.department_number = dpt.department_number
when matched then update
set dim.dd_service = ifnull(dpt.service, 'Not Set'),
dim.dd_subservice = ifnull(dpt.sub_service, 'Not Set'),
dim.dd_site = ifnull(dpt.site, 'Not Set'),
dim.dd_country = ifnull(dpt.country, 'Not Set'),
dim.dd_departmenttype = ifnull(dpt.department, 'Not Set')
*/

DROP TABLE IF EXISTS tmp_multipleIDs;
CREATE TABLE tmp_multipleIDs
AS
SELECT merck_uid as merck_uid,
       max(dim_qualityusersid) as dim_qualityusersid
FROM dim_qualityusers
WHERE rowiscurrent = 1
      AND fn_ln <> 'Anonymus'
GROUP BY merck_uid
HAVING count(*) > 1;

UPDATE dim_qualityusers dim
SET dim.rowiscurrent = 0
FROM dim_qualityusers dim,
     tmp_multipleIDs tmp
WHERE dim.merck_uid = tmp.merck_uid
      AND dim.dim_qualityusersid <> tmp.dim_qualityusersid
      AND dim.rowiscurrent = 1;

DROP TABLE IF EXISTS tmp_multipleIDs;

DELETE FROM emd586.dim_qualityusers;
INSERT INTO emd586.dim_qualityusers
SELECT *
FROM dim_qualityusers;


EXPORT EMD586.dim_qualityusers into local csv file '/home/fusionops/ispring/import/dim_qualityusers_exported.csv.gz' COLUMN DELIMITER='"' REPLACE REJECT LIMIT 0;