UPDATE 
dim_MaterialPriceGroup3 a
SET a.Description = ifnull(TVM3T_BEZEI, 'Not Set'),
    dw_update_date = current_timestamp 
FROM 
dim_MaterialPriceGroup3 a,
TVM3T
WHERE a.MaterialPriceGroup3 = TVM3T_MVGR3 AND RowIsCurrent = 1;