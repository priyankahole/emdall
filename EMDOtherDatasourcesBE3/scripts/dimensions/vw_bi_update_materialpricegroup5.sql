
UPDATE 
dim_MaterialPriceGroup5 a 
SET a.Description = ifnull(TVM5T_BEZEI, 'Not Set'),
    dw_update_date = current_timestamp
FROM 
dim_MaterialPriceGroup5 a,
TVM5T
WHERE a.MaterialPriceGroup5 = TVM5T_MVGR5 AND RowIsCurrent = 1;
