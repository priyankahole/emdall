
DROP TABLE IF EXISTS tmp_distinct_mara_marc_mbew;
CREATE TABLE tmp_distinct_mara_marc_mbew
AS
SELECT DISTINCT *
FROM MARA_MARC_MBEW;

DROP TABLE IF EXISTS MARA_MARC_MBEW_orig;
rename MARA_MARC_MBEW to MARA_MARC_MBEW_orig;
RENAME tmp_distinct_mara_marc_mbew to MARA_MARC_MBEW;


insert into dim_part (dim_partid,rowstartdate,rowiscurrent,partnumber_noleadzero)
select 1,current_timestamp,1,'Not Set' from (select 1) as src
where
not exists (select 1 from dim_part where dim_partid=1);

truncate table tmp_mara_marc_makt_spras_ins;
truncate table tmp_mara_marc_makt_spras_del;

insert into tmp_mara_marc_makt_spras_ins
select  a.MARA_MATNR,a.MARC_WERKS
FROM    mara_marc_makt_spras a;


insert into tmp_mara_marc_makt_spras_del
select b.MARA_MATNR,b.MARC_WERKS
from mara_marc_makt b;

merge into tmp_mara_marc_makt_spras_ins dst
using (select t1.rowid rid
	   from tmp_mara_marc_makt_spras_ins t1
				inner join tmp_mara_marc_makt_spras_del t2 on t1.MARA_MATNR = t2.MARA_MATNR and t1.MARC_WERKS = t2.MARC_WERKS) src
on dst.rowid = src.rid
when matched then delete;
		/* VW Original
			call vectorwise (combine 'tmp_mara_marc_makt_spras_ins-tmp_mara_marc_makt_spras_del') */

INSERT INTO mara_marc_makt(
   MARA_MATNR
  ,MARA_MTART
  ,MARA_MATKL
  ,MARA_PRDHA
  ,MARA_TRAGR
  ,MARA_SPART
  ,MARA_MTPOS
  ,MARA_KZREV
  ,MARA_MEINS
  ,MARA_MFRPN
  ,MARA_MSTAE
  ,MARA_LAEDA
  ,MARA_ERSDA
  ,MARC_WERKS
  ,MARC_MMSTA
  ,MARC_MAABC
  ,MARC_KZKRI
  ,MARC_EKGRP
  ,MARC_DISPO
  ,MARC_PLIFZ
  ,MARC_BESKZ
  ,MARC_MINBE
  ,MARC_EISBE
  ,MARC_MABST
  ,MARC_USEQU
  ,MARC_KAUTB
  ,MARC_STAWN
  ,MARC_LGPRO
  ,MARC_SOBSL
  ,MARC_INSMK
  ,MARC_XCHPF
  ,MARC_LVORM
  ,MARC_BSTRF
  ,MARC_DISMM
  ,MARC_DISLS
  ,MARC_BSTMI
  ,MARC_WEBAZ
  ,MARC_LGFSB
  ,MARC_HERKL
  ,MARC_BWSCL
  ,MARC_STRGR
  ,MARC_DISPR
  ,MARC_DISGR
  ,MARC_SCHGT
  ,MAKT_MAKTX
  ,MARC_DZEIT
  ,MAKT_SPRAS
  ,MARC_PRCTR
  ,MARA_EXTWG
  ,MARC_KZAUS
  ,MARC_BEARZ
  ,MARC_WZEIT
  ,MARA_J_3AGEND
  ,MARA_BISMT
  ,MARA_MSTAV
  ,MARA_VOLUM
  ,MARA_LABOR
  ,MARA_J_3APGNR
  ,MARA_J_3ACOL
  ,MARA_AFS_SCHNITT
  ,MARC_NCOST
  ,MARC_SFEPR
  ,MARC_FXHOR
  ,MARA_WRKST
  ,MARA_EAN11
  ,MARA_MFRNR
  ,MARA_GROES
  ,MARC_FHORI
  ,MARA_RAUBE
  ,MARA_STOFF
  ,MARC_RWPRO,MARC_SHFLG,MARC_SHZET,MARC_BSTMA,MARC_BSTFE,MARA_YYD_YSBU,MARC_FEVOR,
  MARA_GEWEI,MARA_NTGEW,MARC_YYD_YRESG,MARC_YYD_WVDAT,MARC_MATGR,MARA_YYD_LEADM
) SELECT a.MARA_MATNR, MARA_MTART, MARA_MATKL, MARA_PRDHA, MARA_TRAGR, MARA_SPART, MARA_MTPOS,
 MARA_KZREV, MARA_MEINS, MARA_MFRPN, MARA_MSTAE, MARA_LAEDA, MARA_ERSDA, a.MARC_WERKS, MARC_MMSTA,
 MARC_MAABC, MARC_KZKRI, MARC_EKGRP, MARC_DISPO, MARC_PLIFZ, MARC_BESKZ, MARC_MINBE, MARC_EISBE,
 MARC_MABST, MARC_USEQU, MARC_KAUTB, MARC_STAWN, MARC_LGPRO, MARC_SOBSL, MARC_INSMK, MARC_XCHPF,
 MARC_LVORM, MARC_BSTRF, MARC_DISMM, MARC_DISLS, MARC_BSTMI, MARC_WEBAZ, MARC_LGFSB, MARC_HERKL,
 MARC_BWSCL, MARC_STRGR, MARC_DISPR, MARC_DISGR, MARC_SCHGT, MAKT_MAKTX, MARC_DZEIT, MAKT_SPRAS,
 MARC_PRCTR, MARA_EXTWG, MARC_KZAUS, MARC_BEARZ, MARC_WZEIT, MARA_J_3AGEND, MARA_BISMT, MARA_MSTAV,
 MARA_VOLUM, MARA_LABOR, MARA_J_3APGNR, MARA_J_3ACOL, MARA_AFS_SCHNITT, MARC_NCOST, MARC_SFEPR, MARC_FXHOR, MARA_WRKST, MARA_EAN11, MARA_MFRNR,  
 MARA_GROES,MARC_FHORI,MARA_RAUBE,MARA_STOFF,MARC_RWPRO,MARC_SHFLG,MARC_SHZET,MARC_BSTMA,MARC_BSTFE,MARA_YYD_YSBU,MARC_FEVOR,
 MARA_GEWEI,MARA_NTGEW,MARC_YYD_YRESG,MARC_YYD_WVDAT,MARC_MATGR,MARA_YYD_LEADM
FROM mara_marc_makt_spras a,  tmp_mara_marc_makt_spras_ins b
WHERE a.MARA_MATNR = b.MARA_MATNR and a.MARC_WERKS = b.MARC_WERKS;

truncate table tmp_mara_marc_makt_spras_ins;
truncate table tmp_mara_marc_makt_spras_del;

/*

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
       Revision = ifnull(MARA_KZREV, 'Not Set'),
       UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),
       CommodityCode = ifnull(MARC_STAWN, 'Not Set'),
       PartType = ifnull(MARA_MTART, 'Not Set'),
       LeadTime = ifnull(MARC_PLIFZ, 0),
       StockLocation = ifnull(marc_lgpro, 'Not Set'),
       PurchaseGroupCode = ifnull(marc_ekgrp, 'Not Set'),
       PurchaseGroupDescription =
          ifnull((SELECT t.T024_EKNAM
                    FROM t024 t
                   WHERE t.T024_EKGRP = MARC_EKGRP),
                 'Not Set'),
       MRPController = ifnull(marc_dispo, 'Not Set'),
       MaterialGroup = ifnull(mara_matkl, 'Not Set'),
     --  Plant = ifnull(mck.marc_werks, 'Not Set'),
       ABCIndicator = ifnull(marc_maabc, 'Not Set'),
       ProcurementType = ifnull(marc_beskz, 'Not Set'),
       StorageLocation = ifnull(MARC_LGFSB, 'Not Set'),
       CriticalPart = ifnull(MARC_KZKRI, 'Not Set'),
       MRPType = ifnull(MARC_DISMM, 'Not Set'),
       SupplySource = ifnull(MARC_BWSCL, 'Not Set'),
       StrategyGroup = ifnull(MARC_STRGR, 'Not Set'),
       TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),
       Division = ifnull(MARA_SPART, 'Not Set'),
       DivisionDescription =
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),
                 'Not Set'),
       GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
       DeletionFlag = ifnull(MARC_LVORM, 'Not Set'),
       MaterialStatus = ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
       ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
       MPN = ifnull(MARA_MFRPN, 'Not Set'),
       BulkMaterial = ifnull(MARC_SCHGT, 'Not Set'),
       ProductHierarchyDescription =
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set'),
       MRPProfile = ifnull(mck.MARC_DISPR, 'Not Set'),
       MRPProfileDescription =
          ifnull((SELECT p.T401T_KTEXT
                    FROM t401t p
                   WHERE p.T401T_DISPR = mck.MARC_DISPR),
                 'Not Set'),
       PartTypeDescription =
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set'),
       MaterialGroupDescription =
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set'),
       MRPTypeDescription =
          ifnull((SELECT mt.T438T_DIBEZ
                    FROM T438T mt
                   WHERE mt.T438T_DISMM = mck.MARC_DISMM),
                 'Not Set'),
       ProcurementTypeDescription =
          ifnull(
             (SELECT dd.DD07T_DDTEXT
                FROM DD07T dd
               WHERE dd.DD07T_DOMNAME = 'BESKZ'
                     AND dd.DD07T_DOMVALUE = MARC_BESKZ),
             'Not Set'),
       MRPControllerDescription =
          ifnull(
             (SELECT mc.T024D_DSNAM
                FROM T024D mc
               WHERE mc.T024D_DISPO = mck.MARC_DISPO
                     AND mc.T024D_WERKS = mck.MARC_WERKS),
             'Not Set'),
       MRPGroup = ifnull(MARC_DISGR, 'Not Set'),
       MRPGroupDescription =
          ifnull(
             (SELECT mpg.T438X_TEXT40
                FROM T438X mpg
               WHERE mpg.T438X_DISGR = mck.MARC_DISGR
                     AND mpg.T438X_WERKS = mck.MARC_WERKS),
             'Not Set'),
       MRPLotSize = ifnull(mck.MARC_DISLS, 'Not Set'),
       MRPLotSizeDescription =
          ifnull((SELECT ml.T439T_LOSLT
                    FROM T439T ml
                   WHERE ml.T439T_DISLS = mck.MARC_DISLS),
                 'Not Set'),
       materialstatusdescription =
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set'),
       dp.SpecialProcurement = ifnull(mck.MARC_SOBSL, 'Not Set'),
       dp.SpecialProcurementDescription =
          ifnull(
             (SELECT sp.T460T_LTEXT
                FROM t460t sp
               WHERE sp.T460T_SOBSL = mck.marc_sobsl
                     AND sp.T460T_WERKS = mck.MARC_WERKS),
             'Not Set'),
       dp.InhouseProductionTime = mck.MARC_DZEIT,
       dp.ProfitCenterCode = ifnull(mck.MARC_PRCTR, 'Not Set'),
       dp.ProfitCenterName =
          ifnull(
             (SELECT c.CEPCT_KTEXT
                FROM cepct c
               WHERE c.CEPCT_PRCTR = mck.MARC_PRCTR
                     AND c.CEPCT_DATBI > current_date),
             'Not Set'),
       dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
       dp.ExternalMaterialGroupDescription =
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set'),
       dp.MaterialDiscontinuationFlag = ifnull(MARC_KZAUS, 'Not Set'),
       dp.MaterialDiscontinuationFlagDescription =
          ifnull(
             (SELECT DD07T_DDTEXT
                FROM DD07T
               WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS),
             'Not Set'),
       dp.ProcessingTime = MARC_BEARZ,
       dp.TotalReplenishmentLeadTime = MARC_WZEIT,
       dp.GRProcessingTime = MARC_WEBAZ,
       dp.OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
       dp.AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
       dp.AFSColorDescription =
          ifnull((SELECT J_3ACOLRT_TEXT
                    FROM J_3ACOLRT
                   WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL),
                 'Not Set'),
       dp.SDMaterialStatusDescription =
          ifnull((SELECT TVMST_VMSTB
                    FROM tvmst
                   WHERE TVMST_VMSTA = MARA_MSTAV),
                 'Not Set'),
       dp.Volume = MARA_VOLUM,
       dp.AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),
       dp.AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
       dp.DoNotCost = ifnull(MARC_NCOST, 'Not Set'),
       dp.PlantMaterialStatus = ifnull(mck.MARC_MMSTA, 'Not Set'),
       dp.PlantMaterialStatusDescription =
          ifnull((SELECT t141t_MTSTB
                    FROM t141t mst
                   WHERE mst.T141T_MMSTA = mck.MARC_MMSTA),
                 'Not Set'),
       dp.REMProfile = ifnull(MARC_SFEPR, 'Not Set'),
       dp.PlanningTimeFence = ifnull(MARC_FXHOR, 0),
       dp.SafetyStock = ifnull(MARC_EISBE, 0.0000),
       dp.RoundingValue = ifnull(MARC_BSTRF, 0.0000),
       dp.ReorderPoint = ifnull(MARC_MINBE, 0.0000),
       dp.MinimumLotSize = ifnull(MARC_BSTMI,0.0000),
       dp.ValidFrom = (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and  j1.J_3AMAD_WERKS = mck.MARC_WERKS),
       dp.MRPStatus = ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and  j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
       dp.CheckingGroupCode = ifnull(mck.MARC_MTVFP, 'Not Set'),
       dp.CheckingGroup = ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
       dp.UPCNumber=ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set'),
       dp.MaximumStockLevel = ifnull(mck.MARC_MABST, 0.0000),
       dp.ConsumptionMode = ifnull(mck.MARC_VRMOD, 'Not Set'),
       dp.ConsumptionModeDescription = ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
       dp.ConsumptionPeriodBackward = ifnull(mck.MARC_VINT1, 0),
       dp.ConsumptionPeriodForward = ifnull(mck.MARC_VINT2, 0),
       dp.PartLongDesc = ifnull(mck.MAKT_MAKTG,'Not Set'),
       dp.ManfacturerNumber = ifnull(mck.MARA_MFRNR,'Not Set'),
       dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
       dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
	   dp.SchedMarginKey = ifnull(mck.MARC_FHORI,'Not Set'),
	   dp.rangeofcoverage=ifnull(mck.MARC_RWPRO,'Not Set'),
       dp.MRPControllerTelephone = ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set'),
			dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   
	   */

UPDATE dim_part dp
   SET NetWeight = ifnull(MARA_NTGEW, 0),
                dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks AND p.languagekey = makt_spras
          and NetWeight <> ifnull(MARA_NTGEW, 0);
	   
UPDATE dim_part dp
   SET WeightUnit = ifnull(MARA_GEWEI, 'Not Set'),
                dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks AND p.languagekey = makt_spras
          and WeightUnit <> ifnull(MARA_GEWEI, 'Not Set');
		  
		  
		  



	   
UPDATE dim_part dp 
   SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks AND p.languagekey = makt_spras
	  and PartDescription <> ifnull(MAKT_MAKTX, 'Not Set');
	   
UPDATE dim_part dp
   SET ProdSupervisor = ifnull(MARC_FEVOR, 'Not Set'),
                dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks AND p.languagekey = makt_spras
          and ProdSupervisor <> ifnull(MARC_FEVOR, 'Not Set');


UPDATE dim_part dp 
   SET Revision = ifnull(MARA_KZREV, 'Not Set'),	
	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and Revision <> ifnull(MARA_KZREV, 'Not Set');
	   
UPDATE dim_part dp 
   SET UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),	
	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and UnitOfMeasure <> ifnull(MARA_MEINS, 'Not Set');
	   
UPDATE dim_part dp 
   SET CommodityCode = ifnull(MARC_STAWN, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp     
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and CommodityCode <> ifnull(MARC_STAWN, 'Not Set');
	   
UPDATE dim_part dp 
   SET PartType = ifnull(MARA_MTART, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p   , dim_part dp    
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and PartType <> ifnull(MARA_MTART, 'Not Set');
	   
UPDATE dim_part dp 
   SET LeadTime = ifnull(MARC_PLIFZ, 0),
	   	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p    , dim_part dp   
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and LeadTime <> ifnull(MARC_PLIFZ, 0);	   
	   
UPDATE dim_part dp 
   SET  StockLocation = ifnull(marc_lgpro, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p    , dim_part dp   
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and StockLocation <> ifnull(marc_lgpro, 'Not Set');	   

UPDATE dim_part dp 
   SET  PurchaseGroupCode = ifnull(marc_ekgrp, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p      , dim_part dp 
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and PurchaseGroupCode <> ifnull(marc_ekgrp, 'Not Set');	 

UPDATE dim_part dp 
   SET  PurchaseGroupDescription = ifnull(t.T024_EKNAM,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join t024 t on t.T024_EKGRP = mck.MARC_EKGRP
WHERE PurchaseGroupDescription <> ifnull(t.T024_EKNAM,'Not Set');	  
				 
UPDATE dim_part dp 
   SET  MRPController = ifnull(marc_dispo, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and MRPController <> ifnull(marc_dispo, 'Not Set');
	   
UPDATE dim_part dp 
   SET   MaterialGroup = ifnull(mara_matkl, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and MaterialGroup <> ifnull(mara_matkl, 'Not Set');
	   
UPDATE dim_part dp 
   SET  ABCIndicator = ifnull(marc_maabc, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and ABCIndicator <> ifnull(marc_maabc, 'Not Set');
	   
UPDATE dim_part dp 
   SET  ProcurementType = ifnull(marc_beskz, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and ProcurementType <> ifnull(marc_beskz, 'Not Set');
	   
UPDATE dim_part dp 
   SET  StorageLocation = ifnull(MARC_LGFSB, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and StorageLocation <> ifnull(MARC_LGFSB, 'Not Set');
	   
UPDATE dim_part dp 
   SET CriticalPart = ifnull(MARC_KZKRI, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and CriticalPart <> ifnull(MARC_KZKRI, 'Not Set');
	   
UPDATE dim_part dp 
   SET MRPType = ifnull(MARC_DISMM, 'Not Set'),
       dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and MRPType <> ifnull(MARC_DISMM, 'Not Set');
	   
UPDATE dim_part dp 
   SET  SupplySource = ifnull(MARC_BWSCL, 'Not Set'),
dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  SupplySource <> ifnull(MARC_BWSCL, 'Not Set');
 	   
UPDATE dim_part dp 
   SET   StrategyGroup = ifnull(MARC_STRGR, 'Not Set'),  
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  StrategyGroup <> ifnull(MARC_STRGR, 'Not Set');

UPDATE dim_part dp 
   SET  TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),	   
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and TransportationGroup <> ifnull(MARA_TRAGR, 'Not Set');
	
UPDATE dim_part dp 
   SET  Division = ifnull(MARA_SPART, 'Not Set'),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and Division <> ifnull(MARA_SPART, 'Not Set');
	   
UPDATE dim_part dp 
   SET  DivisionDescription = ifnull(t.TSPAT_VTEXT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join TSPAT t on t.TSPAT_SPART = mck.MARA_SPART
WHERE DivisionDescription <> ifnull(t.TSPAT_VTEXT,'Not Set');	
				 
UPDATE dim_part dp 
   SET  GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
   			   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  GeneralItemCategory <> ifnull(MARA_MTPOS, 'Not Set');
	   
UPDATE dim_part dp 
   SET DeletionFlag = ifnull(MARC_LVORM, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and DeletionFlag <> ifnull(MARC_LVORM, 'Not Set');
	   
UPDATE dim_part dp 
   SET  MaterialStatus = ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MaterialStatus <> ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set');
	   
UPDATE dim_part dp 
   SET ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ProductHierarchy <> ifnull(MARA_PRDHA, 'Not Set');
	   
UPDATE dim_part dp 
   SET MPN = ifnull(MARA_MFRPN, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MPN <> ifnull(MARA_MFRPN, 'Not Set');
	   
UPDATE dim_part dp 
   SET BulkMaterial = ifnull(MARC_SCHGT, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and BulkMaterial <> ifnull(MARC_SCHGT, 'Not Set');

UPDATE dim_part dp 
   SET  ProductHierarchyDescription = ifnull(t.VTEXT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join t179t t on t.PRODH = mck.MARA_PRDHA
WHERE ProductHierarchyDescription <> ifnull(t.VTEXT,'Not Set');	

UPDATE dim_part dp 
   SET  MRPProfile = ifnull(mck.MARC_DISPR, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPProfile <> ifnull(mck.MARC_DISPR, 'Not Set');

UPDATE dim_part dp 
   SET  MRPProfileDescription = ifnull(t.T401T_KTEXT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join t401t t on t.T401T_DISPR = mck.MARC_DISPR
WHERE MRPProfileDescription <> ifnull(t.T401T_KTEXT,'Not Set');	

UPDATE dim_part dp 
   SET  PartTypeDescription = ifnull(t.T134T_MTBEZ,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join T134T t on t.T134T_MTART = mck.MARA_MTART
WHERE PartTypeDescription <> ifnull(t.T134T_MTBEZ,'Not Set');	

UPDATE dim_part dp 
   SET  MaterialGroupDescription = ifnull(t.T023T_WGBEZ,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join T023T t on t.T023T_MATKL = mck.MARA_MATKL
WHERE MaterialGroupDescription <> ifnull(t.T023T_WGBEZ,'Not Set');

UPDATE dim_part dp 
   SET  MRPTypeDescription = ifnull(t.T438T_DIBEZ,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join T438T t on t.T438T_DISMM = mck.MARC_DISMM
WHERE MRPTypeDescription <> ifnull(t.T438T_DIBEZ,'Not Set');
				 
UPDATE dim_part dp 
   SET  MRPControllerDescription = ifnull(t.T024D_DSNAM,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join T024D t on t.T024D_DISPO = mck.MARC_DISPO AND t.T024D_WERKS = mck.MARC_WERKS
WHERE MRPControllerDescription <> ifnull(t.T024D_DSNAM,'Not Set');
			 
UPDATE dim_part dp 
   SET MRPGroup = ifnull(MARC_DISGR, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPGroup <> ifnull(MARC_DISGR, 'Not Set');

UPDATE dim_part dp 
   SET  MRPGroupDescription = ifnull(t.T438X_TEXT40,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join T438X t on t.T438X_DISGR = mck.MARC_DISGR AND t.T438X_WERKS = mck.MARC_WERKS
WHERE MRPGroupDescription <> ifnull(t.T438X_TEXT40,'Not Set');

UPDATE dim_part dp 
   SET MRPLotSize = ifnull(mck.MARC_DISLS, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks AND p.languagekey = makt_spras
	   and MRPLotSize <> ifnull(mck.MARC_DISLS, 'Not Set');

UPDATE dim_part dp 
   SET  MRPLotSizeDescription = ifnull(t.T439T_LOSLT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join T439T t on t.T439T_DISLS = mck.MARC_DISLS
WHERE MRPLotSizeDescription <> ifnull(t.T439T_LOSLT,'Not Set');
	   
UPDATE dim_part dp 
   SET  materialstatusdescription = ifnull(t.t141t_MTSTB,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join t141t t on t.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)
WHERE materialstatusdescription <> ifnull(t.t141t_MTSTB,'Not Set');

UPDATE dim_part dp 
   SET dp.SpecialProcurement = ifnull(mck.MARC_SOBSL, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SpecialProcurement <> ifnull(mck.MARC_SOBSL, 'Not Set');
	   
UPDATE dim_part dp 
   SET  SpecialProcurementDescription = ifnull(t.T460T_LTEXT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join t460t t on t.T460T_SOBSL = mck.marc_sobsl AND t.T460T_WERKS = mck.MARC_WERKS
WHERE SpecialProcurementDescription <> ifnull(t.T460T_LTEXT,'Not Set');

UPDATE dim_part dp 
   SET dp.InhouseProductionTime = mck.MARC_DZEIT,
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.InhouseProductionTime <> mck.MARC_DZEIT;
	   
UPDATE dim_part dp 
   SET dp.ProfitCenterCode = ifnull(mck.MARC_PRCTR, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ProfitCenterCode <> ifnull(mck.MARC_PRCTR, 'Not Set');
	   
UPDATE dim_part dp 
   SET  ProfitCenterName = ifnull(t.CEPCT_KTEXT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join cepct t on t.CEPCT_PRCTR = mck.MARC_PRCTR AND t.CEPCT_DATBI > current_date
WHERE ProfitCenterName <> ifnull(t.CEPCT_KTEXT,'Not Set');

UPDATE dim_part dp 
   SET dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ExternalMaterialGroupCode <> ifnull(MARA_EXTWG, 'Not Set');

UPDATE dim_part dp 
   SET  ExternalMaterialGroupDescription = ifnull(t.TWEWT_EWBEZ,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join twewt t on t.TWEWT_EXTWG = mck.MARA_EXTWG
WHERE ExternalMaterialGroupDescription <> ifnull(t.TWEWT_EWBEZ,'Not Set');
	   
UPDATE dim_part dp 
   SET dp.MaterialDiscontinuationFlag = ifnull(MARC_KZAUS, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MaterialDiscontinuationFlag <> ifnull(MARC_KZAUS, 'Not Set');

UPDATE dim_part dp 
   SET  MaterialDiscontinuationFlagDescription = ifnull(t.DD07T_DDTEXT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join DD07T t on t.DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = mck.MARC_KZAUS
WHERE MaterialDiscontinuationFlagDescription <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_part dp 
   SET dp.ProcessingTime = MARC_BEARZ,
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ifnull(dp.ProcessingTime,-1) <> ifnull(MARC_BEARZ,-2);

UPDATE dim_part dp 
   SET dp.TotalReplenishmentLeadTime = MARC_WZEIT,
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ifnull(dp.TotalReplenishmentLeadTime,-1) <> ifnull(MARC_WZEIT,-2);
	   
UPDATE dim_part dp 
   SET dp.GRProcessingTime = MARC_WEBAZ,
       dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.GRProcessingTime <> MARC_WEBAZ;
	   
UPDATE dim_part dp 
   SET dp.OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.OldPartNumber <> ifnull(MARA_BISMT, 'Not Set');
	   
   
UPDATE dim_part dp 
   SET dp.AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.AFSColor <> ifnull(MARA_J_3ACOL, 'Not Set');
	   
UPDATE dim_part dp 
   SET  AFSColorDescription = ifnull(t.J_3ACOLRT_TEXT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join J_3ACOLRT t on t.J_3ACOLRT_J_3ACOL = mck.MARA_J_3ACOL
WHERE AFSColorDescription <> ifnull(t.J_3ACOLRT_TEXT,'Not Set');

UPDATE dim_part dp 
   SET  SDMaterialStatusDescription = ifnull(t.TVMST_VMSTB,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join tvmst t on t.TVMST_VMSTA = MARA_MSTAV
WHERE SDMaterialStatusDescription <> ifnull(t.TVMST_VMSTB,'Not Set');

UPDATE dim_part dp 
   SET dp.Volume = MARA_VOLUM,	
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
and dp.Volume <> MARA_VOLUM;	 

UPDATE dim_part dp 
   SET dp.AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),  
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.AFSMasterGrid <> ifnull(MARA_J_3APGNR, 'Not Set');
	
UPDATE dim_part dp 
   SET dp.AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.AFSPattern <> ifnull(MARA_AFS_SCHNITT, 'Not Set');
	   
UPDATE dim_part dp 
   SET  dp.DoNotCost = ifnull(MARC_NCOST, 'Not Set'),
    	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.DoNotCost <> ifnull(MARC_NCOST, 'Not Set');
	   
UPDATE dim_part dp 
   SET dp.PlantMaterialStatus = ifnull(mck.MARC_MMSTA, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.PlantMaterialStatus <> ifnull(mck.MARC_MMSTA, 'Not Set');
	   
UPDATE dim_part dp 
   SET  PlantMaterialStatusDescription = ifnull(t.t141t_MTSTB,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join t141t t on t.T141T_MMSTA = mck.MARC_MMSTA
WHERE PlantMaterialStatusDescription <> ifnull(t.t141t_MTSTB,'Not Set');

UPDATE dim_part dp 
   SET dp.REMProfile = ifnull(MARC_SFEPR, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.REMProfile <> ifnull(MARC_SFEPR, 'Not Set');

UPDATE dim_part dp 
   SET dp.PlanningTimeFence = ifnull(MARC_FXHOR, 0),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.PlanningTimeFence <> ifnull(MARC_FXHOR, 0);
	   
UPDATE dim_part dp 
   SET dp.SafetyStock = ifnull(MARC_EISBE, 0.0000),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp  
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SafetyStock <> ifnull(MARC_EISBE, 0.0000);
	   
   
UPDATE dim_part dp 
   SET dp.RoundingValue = ifnull(MARC_BSTRF, 0.0000),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  dp.RoundingValue <> ifnull(MARC_BSTRF, 0.0000);
	   
UPDATE dim_part dp 
   SET dp.ReorderPoint = ifnull(MARC_MINBE, 0.0000),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ReorderPoint <> ifnull(MARC_MINBE, 0.0000);
	   
UPDATE dim_part dp 
   SET dp.MinimumLotSize = ifnull(MARC_BSTMI,0.0000),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MinimumLotSize <> ifnull(MARC_BSTMI,0.0000);
	   
UPDATE dim_part dp 
   SET  ValidFrom = ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join J_3AMAD j1 on mck.mara_matnr = j1.j_3amad_matnr and j1.J_3AMAD_WERKS = mck.MARC_WERKS
WHERE ValidFrom <> ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01');

UPDATE dim_part dp 
SET dp.MRPStatus = 'Not Set',
dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
AND dp.Plant = mck.MARC_WERKS
AND p.plantcode = marc_werks 
and dp.MRPStatus <> 'Not Set';


UPDATE dim_part dp 
SET dp.MRPStatus = IFNULL(j2.J_3AMAD_J_4ASTAT,'Not Set'),
dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p,J_3AMAD j2, dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
AND dp.Plant = mck.MARC_WERKS
AND p.plantcode = marc_werks 
AND mck.mara_matnr = j2.j_3amad_matnr and j2.J_3AMAD_WERKS = mck.MARC_WERKS
and dp.MRPStatus <> IFNULL(j2.J_3AMAD_J_4ASTAT,'Not Set');
   
UPDATE dim_part dp 
   SET dp.CheckingGroupCode = ifnull(mck.MARC_MTVFP, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks AND p.languagekey = makt_spras      
   and dp.CheckingGroupCode <> ifnull(mck.MARC_MTVFP, 'Not Set');

UPDATE dim_part dp 
   SET  CheckingGroup = ifnull(t.TMVFT_BEZEI,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join TMVFT t on mck.marc_mtvfp = t.tmvft_mtvfp
WHERE CheckingGroup <> ifnull(t.TMVFT_BEZEI,'Not Set');

/* Ambiguos repalce solved */
merge into dim_part dim
using(select dp.dim_partid, 
			 ifnull(max(t.MEAN_EAN11),'Not Set') as UPCNumber,
			 ifnull(max(t1.DD07T_DDTEXT),'Not Set') as ProcurementTypeDescription
	  from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join MEAN t on t.MEAN_MATNR = mck.mara_matnr
		left join DD07T t1 on t1.DD07T_DOMVALUE = mck.MARC_BESKZ
	  group by dp.dim_partid
	  ) src on dim.dim_Partid = src.dim_partid
when matched then update set dim.UPCNumber = src.UPCNumber,
							 dim.ProcurementTypeDescription = src.ProcurementTypeDescription
where (dim.UPCNumber <> src.UPCNumber or dim.ProcurementTypeDescription = src.ProcurementTypeDescription);
 
UPDATE dim_part dp 
   SET dp.MaximumStockLevel = ifnull(mck.MARC_MABST, 0.0000), 
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks   
	   and dp.MaximumStockLevel <> ifnull(mck.MARC_MABST, 0.0000);
	   
UPDATE dim_part dp 
   SET dp.ConsumptionMode = ifnull(mck.MARC_VRMOD, 'Not Set'),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp  
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks AND p.languagekey = makt_spras
	   and dp.ConsumptionMode <> ifnull(mck.MARC_VRMOD, 'Not Set');

UPDATE dim_part dp 
   SET  ConsumptionModeDescription = ifnull(t.DD07T_DDTEXT,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks AND p.languagekey = makt_spras
		left join DD07T t on t.DD07T_DOMNAME = 'VRMOD' AND t.DD07T_DOMVALUE = mck.MARC_VRMOD
WHERE ConsumptionModeDescription <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_part dp 
   SET dp.ConsumptionPeriodBackward = ifnull(mck.MARC_VINT1, 0),    	 
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks AND p.languagekey = makt_spras    
	   and dp.ConsumptionPeriodBackward <> ifnull(mck.MARC_VINT1, 0);
	   
UPDATE dim_part dp 
   SET dp.ConsumptionPeriodForward = ifnull(mck.MARC_VINT2, 0),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS 
       AND p.plantcode = marc_werks  AND p.languagekey = makt_spras
and dp.ConsumptionPeriodForward <> ifnull(mck.MARC_VINT2, 0);
   
UPDATE dim_part dp 
   SET dp.PartLongDesc = ifnull(mck.MAKT_MAKTG,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS AND p.languagekey = makt_spras
       AND p.plantcode = marc_werks  
	   and dp.PartLongDesc <> ifnull(mck.MAKT_MAKTG,'Not Set');
   
UPDATE dim_part dp 
   SET dp.ManfacturerNumber = ifnull(mck.MARA_MFRNR,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks  
	   and dp.ManfacturerNumber <> ifnull(mck.MARA_MFRNR,'Not Set');

	      
UPDATE dim_part dp 
   SET dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
      dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.UPCCode <> ifnull(mck.MARA_EAN11,'Not Set');

UPDATE dim_part dp 
   SET dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	  and dp.NDCCode <> ifnull(mck.MARA_GROES,'Not Set');
   
UPDATE dim_part dp 
   SET dp.SchedMarginKey = ifnull(mck.MARC_FHORI,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.SchedMarginKey <> ifnull(mck.MARC_FHORI,'Not Set');

UPDATE dim_part dp 
   SET  MRPControllerTelephone = ifnull(t.T024D_DSTEL,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join T024D t on t.T024D_DISPO = mck.MARC_DISPO AND t.T024D_WERKS = mck.MARC_WERKS
WHERE MRPControllerTelephone <> ifnull(t.T024D_DSTEL,'Not Set');
	   
 UPDATE dim_part dp 
   SET dp.storageconditioncode = ifnull(mck.MARA_RAUBE,'Not Set'),
	   dp.hazardousmaterialnumber = ifnull(mck.MARA_STOFF,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
       AND (dp.storageconditioncode <> ifnull(mck.MARA_RAUBE,'Not Set') OR
			dp.hazardousmaterialnumber <> ifnull(mck.MARA_STOFF,'Not Set')
			);
       

/*17 feb 2015 new MARC columns */	   

UPDATE dim_part dp 
   SET dp.rangeofcoverage=ifnull(mck.MARC_RWPRO,'Not Set')
from mara_marc_makt mck, dim_plant p , dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.rangeofcoverage<>ifnull(mck.MARC_RWPRO,'Not Set');
	   
 UPDATE dim_part dp 
   SET dp.safetytimeind = ifnull(mck.MARC_SHFLG,'Not Set')
   , dp.dw_update_date = current_timestamp
 from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.safetytimeind <> ifnull(mck.MARC_SHFLG,'Not Set');
 
  UPDATE dim_part dp 
   SET dp.safetytime = ifnull(mck.MARC_SHZET,0)
   , dp.dw_update_date = current_timestamp
  from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.safetytime <> ifnull(mck.MARC_SHZET,0);
 
  UPDATE dim_part dp 
   SET dp.maxlotsize  = ifnull(mck.MARC_BSTMA,0)
   , dp.dw_update_date = current_timestamp
  from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.maxlotsize  <> ifnull(mck.MARC_BSTMA,0);
 
  UPDATE dim_part dp 
   SET dp.fixedlotsize = ifnull(mck.MARC_BSTFE,0)
   , dp.dw_update_date = current_timestamp
  from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.fixedlotsize <> ifnull(mck.MARC_BSTFE,0);
 
 /* end 17 feb 2015 new MARC columns */
 
/* 25 Nov 2015 CristianT Start: Add Product Group(SBU) */
UPDATE dim_part dp 
SET dp.productgroupsbu = ifnull(mck.MARA_YYD_YSBU, 'Not Set'),
    dp.dw_update_date = current_timestamp
FROM dim_part dp, mara_marc_makt mck
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND dp.productgroupsbu <> ifnull(mck.MARA_YYD_YSBU,'Not Set');

/* 25 Nov 2015 CristianT End */

/* Reason and Date of Availability - Oana 03 Oct 2016 */
UPDATE dim_part dp 
SET dp.REASONOUTOFSTOCK = ifnull(mck.MARC_YYD_YRESG,'Not Set'),
    dp.dw_update_date = current_timestamp
FROM dim_part dp, mara_marc_makt mck
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND dp.REASONOUTOFSTOCK <> ifnull(mck.MARC_YYD_YRESG,'Not Set');
	    
UPDATE dim_part dp 
SET dp.dateofavailability = ifnull(mck.MARC_YYD_WVDAT,to_date('0001-01-01','YYYY-MM-DD')),
    dp.dw_update_date = current_timestamp
FROM dim_part dp, mara_marc_makt mck
 WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND dp.dateofavailability <> ifnull(mck.MARC_YYD_WVDAT,to_date('0001-01-01','YYYY-MM-DD'));
/* END Reason and Date of Availability - Oana 03 Oct 2016 */

truncate table tmp_dim_MARA_MARC_MAKT_ins;	
truncate table tmp_dim_MARA_MARC_MAKT_del;
	   
insert into tmp_dim_MARA_MARC_MAKT_ins
select mck.MARA_MATNR,mck.MARC_WERKS
FROM    MARA_MARC_MAKT mck;

insert into tmp_dim_MARA_MARC_MAKT_del
select p.PartNumber,p.Plant
from dim_part p;

merge into tmp_dim_MARA_MARC_MAKT_ins dst
using (select distinct t1.rowid rid
	   from tmp_dim_MARA_MARC_MAKT_ins t1
				inner join tmp_dim_MARA_MARC_MAKT_del t2 on t1.MARA_MATNR = t2.MARA_MATNR and t1.MARC_WERKS = t2.MARC_WERKS ) src
on dst.rowid = src.rid
when matched then delete;
	/* VW Original: call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_ins-tmp_dim_MARA_MARC_MAKT_del') */
   
drop table if exists tmp_dim_part_f_ins1;
create table tmp_dim_part_f_ins1 as
select    distinct
		  mck.MARA_MATNR PartNumber,
          ifnull(MAKT_MAKTX, 'Not Set') PartDescription,
          ifnull(MARA_KZREV, 'Not Set') Revision,
          ifnull(MARA_MEINS, 'Not Set') UnitOfMeasure,
          current_timestamp RowStartDate,
          1 RowIsCurrent,
          ifnull(MARC_STAWN, 'Not Set') CommodityCode,
          ifnull(MARA_MTART, 'Not Set') PartType,
          ifnull(MARC_PLIFZ, 0) LeadTime,
          ifnull(MARC_LGPRO, 'Not Set') StockLocation,
          ifnull(MARC_EKGRP, 'Not Set') PurchaseGroupCode,
          convert(varchar(200), 'Not Set') as PurchaseGroupDescription, -- ifnull((SELECT t.T024_EKNAM FROM t024 t WHERE t.T024_EKGRP = MARC_EKGRP), 'Not Set') PurchaseGroupDescription,
          ifnull(MARC_DISPO, 'Not Set') MRPController,
          ifnull(MARA_MATKL, 'Not Set') MaterialGroup,
          mck.MARC_WERKS Plant,
          ifnull(MARC_MAABC, 'Not Set') ABCIndicator, 
          ifnull(MARC_BESKZ, 'Not Set') ProcurementType,
          ifnull(MARC_LGFSB, 'Not Set') StorageLocation,
          ifnull(MARC_KZKRI, 'Not Set') CriticalPart,
          ifnull(MARC_DISMM, 'Not Set') MRPType,
          ifnull(MARC_BWSCL, 'Not Set') SupplySource,
          ifnull(MARC_STRGR, 'Not Set') StrategyGroup,
          ifnull(MARA_TRAGR, 'Not Set') TransportationGroup,
          ifnull(MARA_SPART, 'Not Set') Division,
		  convert(varchar(200), 'Not Set') as DivisionDescription, 			-- ifnull((SELECT TSPAT_VTEXT FROM TSPAT WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),'Not Set') DivisionDescription,
          ifnull(MARA_MTPOS, 'Not Set') GeneralItemCategory,
          ifnull(MARC_LVORM, 'Not Set') DeletionFlag,
          ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set') MaterialStatus,
          ifnull(MARA_PRDHA, 'Not Set') ProductHierarchy,
          ifnull(MARA_MFRPN, 'Not Set') MPN,
          convert(varchar(200), 'Not Set') as ProductHierarchyDescription,  -- ifnull((SELECT t.VTEXT FROM t179t t WHERE t.PRODH = mck.MARA_PRDHA), 'Not Set') ProductHierarchyDescription,
          ifnull(mck.MARC_DISPR, 'Not Set') MRPProfile,
          convert(varchar(200), 'Not Set') as MRPProfileDescription, 		-- ifnull((SELECT p.T401T_KTEXT FROM t401t p WHERE p.T401T_DISPR = mck.MARC_DISPR), 'Not Set') MRPProfileDescription,
          convert(varchar(200), 'Not Set') as PartTypeDescription, 			-- ifnull((SELECT pt.T134T_MTBEZ FROM T134T pt WHERE pt.T134T_MTART = mck.MARA_MTART), 'Not Set') PartTypeDescription, 
          convert(varchar(200), 'Not Set') as MaterialGroupDescription, 	-- ifnull((SELECT mg.T023T_WGBEZ FROM T023T mg WHERE mg.T023T_MATKL = mck.MARA_MATKL), 'Not Set') MaterialGroupDescription,
          convert(varchar(200), 'Not Set') as MRPTypeDescription, 			-- ifnull((SELECT mt.T438T_DIBEZ FROM T438T mt WHERE mt.T438T_DISMM = mck.MARC_DISMM), 'Not Set') MRPTypeDescription,
          convert(varchar(200), 'Not Set') as ProcurementTypeDescription, 	-- ifnull((SELECT dd.DD07T_DDTEXT FROM DD07T dd WHERE dd.DD07T_DOMNAME = 'BESKZ' AND dd.DD07T_DOMVALUE = MARC_BESKZ), 'Not Set') ProcurementTypeDescription,
          convert(varchar(200), 'Not Set') as MRPControllerDescription, 	-- ifnull((SELECT mc.T024D_DSNAM FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set') MRPControllerDescription,
          ifnull(MARC_DISGR, 'Not Set') MRPGroup,
          convert(varchar(200), 'Not Set') as MRPGroupDescription, 			-- ifnull((SELECT mpg.T438X_TEXT40 FROM T438X mpg WHERE mpg.T438X_DISGR = mck.MARC_DISGR AND mpg.T438X_WERKS = mck.MARC_WERKS), 'Not Set') MRPGroupDescription,
          ifnull(mck.MARC_BSTMI,0.0000) MinimumLotSize,
          ifnull(mck.MARC_DISLS, 'Not Set') MRPLotSize,
          convert(varchar(200), 'Not Set') as MRPLotSizeDescription, 		-- ifnull((SELECT ml.T439T_LOSLT FROM T439T ml WHERE ml.T439T_DISLS = mck.MARC_DISLS), 'Not Set') MRPLotSizeDescription,
          convert(varchar(200), 'Not Set') as materialstatusdescription, 	-- ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)), 'Not Set') materialstatusdescription, 
          ifnull(mck.MARC_SOBSL, 'Not Set') SpecialProcurement,
          convert(varchar(200), 'Not Set') as SpecialProcurementDescription, -- ifnull((SELECT sp.T460T_LTEXT FROM t460t sp WHERE sp.T460T_SOBSL = mck.MARC_SOBSL AND sp.T460T_WERKS = mck.MARC_WERKS), 'Not Set') SpecialProcurementDescription,
          ifnull(MARC_SCHGT, 'Not Set') BulkMaterial,
          mck.MARC_DZEIT InhouseProductionTime,
          ifnull(MARC_PRCTR, 'Not Set') ProfitCenterCode,
          convert(varchar(200), 'Not Set') as ProfitCenterName, 			-- ifnull((SELECT c.CEPCT_KTEXT FROM cepct c WHERE c.CEPCT_PRCTR = MARC_PRCTR AND c.CEPCT_DATBI > current_date), 'Not Set') ProfitCenterName,
          ifnull(MARA_EXTWG, 'Not Set') ExternalMaterialGroupCode,
          convert(varchar(200), 'Not Set') as ExternalMaterialGroupDescription, 	-- ifnull((SELECT t.TWEWT_EWBEZ FROM twewt t WHERE t.TWEWT_EXTWG = MARA_EXTWG), 'Not Set') ExternalMaterialGroupDescription,
          ifnull(MARC_KZAUS, 'Not Set') MaterialDiscontinuationFlag,
          convert(varchar(200), 'Not Set') as MaterialDiscontinuationFlagDescription, 	-- ifnull((SELECT DD07T_DDTEXT FROM DD07T WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS), 'Not Set') MaterialDiscontinuationFlagDescription,
          mck.MARC_BEARZ ProcessingTime,
          mck.MARC_WZEIT TotalReplenishmentLeadTime,
		  mck.MARC_WEBAZ GRProcessingTime,
		  ifnull(mck.MARA_BISMT, 'Not Set') OldPartNumber,
		  ifnull(mck.MARA_J_3ACOL, 'Not Set') AFSColor,
		  convert(varchar(200), 'Not Set') as AFSColorDescription, 			-- ifnull((SELECT J_3ACOLRT_TEXT FROM J_3ACOLRT WHERE J_3ACOLRT_J_3ACOL = mck.MARA_J_3ACOL), 'Not Set') AFSColorDescription,
		  convert(varchar(200), 'Not Set') as SDMaterialStatusDescription, 	-- ifnull((SELECT TVMST_VMSTB FROM tvmst WHERE TVMST_VMSTA = MARA_MSTAV), 'Not Set') SDMaterialStatusDescription,
		  MARA_VOLUM Volume, 
		  ifnull(MARA_J_3APGNR, 'Not Set') AFSMasterGrid,
		  ifnull(MARA_AFS_SCHNITT, 'Not Set') AFSPattern,
		  ifnull(MARA_LABOR, 'Not Set') Laboratory,
		  ifnull(MARC_NCOST,'Not Set') DoNotCost,
		  ifnull(mck.MARC_MMSTA,'Not Set') PlantMaterialStatus,
		  convert(varchar(200), 'Not Set') as PlantMaterialStatusDescription, 	-- ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = mck.MARC_MMSTA), 'Not Set') PlantMaterialStatusDescription,
		  ifnull(MARC_SFEPR,'Not Set') REMProfile,
		  ifnull(MARC_FXHOR,0) PlanningTimeFence,
		  ifnull(MARC_EISBE,0.0000) SafetyStock,
		  ifnull(MARC_BSTRF,0.0000) RoundingValue,
		  ifnull(MARC_MINBE,0.0000) ReorderPoint,
		  to_date('0001-01-01','YYYY-MM-DD') as ValidFrom, 					-- (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and  j1.J_3AMAD_WERKS = mck.MARC_WERKS) ValidFrom,
		  convert(varchar(200), 'Not Set') as MRPStatus, 					-- ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and  j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set') MRPStatus,
		  ifnull(mck.MARC_MTVFP, 'Not Set') CheckingGroupCode,
		  convert(varchar(200), 'Not Set') as CheckingGroup, 				-- ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set') CheckingGroup,
		  convert(varchar(200), 'Not Set') as UPCNumber, 					-- ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set') UPCNumber,
		  ifnull(mck.MARC_MABST, 0.0000) MaximumStockLevel,
		  ifnull(mck.MARC_VRMOD, 'Not Set') ConsumptionMode,
		  convert(varchar(200), 'Not Set') as ConsumptionModeDescription, 	-- ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set') ConsumptionModeDescription,
		  ifnull(mck.MARC_VINT1, 0) ConsumptionPeriodBackward,
		  ifnull(mck.MARC_VINT2, 0) ConsumptionPeriodForward,
		  ifnull(mck.MAKT_MAKTG,'Not Set') PartLongDesc, 	
		  ifnull(mck.MARA_MFRNR,'Not Set') ManfacturerNumber,
		  ifnull(mck.MARA_EAN11,'Not Set') UPCCode,
		  ifnull(mck.MARA_GROES,'Not Set') NDCCode,
		  ifnull(mck.MARC_FHORI,'Not Set') SchedMarginKey,
		  ifnull(mck.MARA_RAUBE,'Not Set') storageconditioncode,
		  ifnull(mck.MARA_STOFF,'Not Set') hazardousmaterialnumber,
		  ifnull(mck.MARC_RWPRO,'Not Set') rangeofcoverage,
		  ifnull(mck.MARC_SHFLG,'Not Set') safetytimeind,
		  ifnull(mck.MARC_SHZET,0) safetytime,
		  ifnull(mck.MARC_BSTMA,0) maxlotsize,
		  ifnull(mck.MARC_BSTFE,0) fixedlotsize,
		  convert(varchar(200), 'Not Set') as MRPControllerTelephone,		-- ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set') MRPControllerTelephone
		  ifnull(mck.MARA_YYD_YSBU, 'Not Set') productgroupsbu
		  ,MARC_EKGRP 				-- PurchaseGroupDescription
		  ,MARA_SPART				-- DivisionDescription
		  ,MARA_PRDHA				-- ProductHierarchyDescription
		  ,MARC_DISPR				-- MRPProfileDescription,		  
		  ,MARA_MTART				-- PartTypeDescription, 		  
		  ,MARA_MATKL				-- MaterialGroupDescription,		  
		  ,MARC_DISMM				-- MRPTypeDescription,		  
		  ,MARC_BESKZ				-- ProcurementTypeDescription,		  
		  ,MARC_DISPO				-- MRPControllerDescription,   		-- use Plant		  
		  ,MARC_DISGR				-- MRPGroupDescription,		  		-- use Plant
		  ,MARC_DISLS				-- MRPLotSizeDescription,		  
		  ,MARC_MMSTA, MARA_MSTAE	-- materialstatusdescription, 		  
		  ,MARC_SOBSL				-- SpecialProcurementDescription,	-- use Plant
		  ,MARC_PRCTR				-- ProfitCenterName,		  
		  ,MARA_EXTWG				-- ExternalMaterialGroupDescription,		  
		  ,MARC_KZAUS				-- MaterialDiscontinuationFlagDescription,	
		  ,MARA_J_3ACOL				-- AFSColorDescription,
		  ,MARA_MSTAV				-- SDMaterialStatusDescription,		  		  
		  ,marc_mtvfp				-- CheckingGroup,
		  ,MARC_VRMOD				-- ConsumptionModeDescription,
		  ,ifnull(MARC_FEVOR, 'Not Set') ProdSupervisor
		  ,ifnull(MARA_NTGEW, 0) NetWeight
		  ,ifnull(MARA_GEWEI, 'Not Set') WeightUnit
		  ,ifnull(MARC_YYD_YRESG, ' Not Set') as REASONOUTOFSTOCK
		  ,ifnull(MARC_YYD_WVDAT, to_date('0001-01-01','YYYY-MM-DD')) as DateOfAvailability
		  ,ifnull(MARC_MATGR,'Not Set') MaterialGrouping
		  ,ifnull(MARA_YYD_LEADM,'Not Set') LeadMatnr
FROM MARA_MARC_MAKT mck
        INNER JOIN dim_plant dp ON dp.plantcode = mck.marc_werks /* CristianB 23 feb 2017 Requested on BI-5575 AND dp.languagekey = makt_spras */
		INNER JOIN tmp_dim_MARA_MARC_MAKT_ins p ON    p.MARA_MATNR = mck.MARA_MATNR 
												  AND p.MARC_WERKS = mck.MARC_WERKS
WHERE ifnull(mck.MARC_WERKS,'NULL') <> 'NULL';

/* PurchaseGroupDescription */
update tmp_dim_part_f_ins1 t1
set t1.PurchaseGroupDescription = ifnull(t.T024_EKNAM, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t024 t on t.T024_EKGRP = MARC_EKGRP
where t1.PurchaseGroupDescription <> ifnull(t.T024_EKNAM, 'Not Set');

/* DivisionDescription */
update tmp_dim_part_f_ins1 t1
set t1.DivisionDescription = ifnull(t.TSPAT_VTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join TSPAT t on TSPAT_SPART = MARA_SPART
where t1.DivisionDescription <> ifnull(t.TSPAT_VTEXT, 'Not Set');

/* ProductHierarchyDescription */
update tmp_dim_part_f_ins1 t1
set t1.ProductHierarchyDescription = ifnull(t.VTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t179t t on t.PRODH = MARA_PRDHA
where t1.ProductHierarchyDescription <> ifnull(t.VTEXT, 'Not Set');

/* MRPProfileDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPProfileDescription = ifnull(t.T401T_KTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t401t t on t.T401T_DISPR = MARC_DISPR
where t1.MRPProfileDescription <> ifnull(t.T401T_KTEXT, 'Not Set');

/* PartTypeDescription */
update tmp_dim_part_f_ins1 t1
set t1.PartTypeDescription = ifnull(t.T134T_MTBEZ, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T134T t on t.T134T_MTART = MARA_MTART
where t1.PartTypeDescription <> ifnull(t.T134T_MTBEZ, 'Not Set');

/* MaterialGroupDescription */
update tmp_dim_part_f_ins1 t1
set t1.MaterialGroupDescription = ifnull(t.T023T_WGBEZ, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T023T t on t.T023T_MATKL = MARA_MATKL
where t1.MaterialGroupDescription <> ifnull(t.T023T_WGBEZ, 'Not Set');

/* MRPTypeDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPTypeDescription = ifnull(t.T438T_DIBEZ, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T438T t on t.T438T_DISMM = MARC_DISMM
where t1.MRPTypeDescription <> ifnull(t.T438T_DIBEZ, 'Not Set');

/* ProcurementTypeDescription */
update tmp_dim_part_f_ins1 t1
set t1.ProcurementTypeDescription = ifnull(t.DD07T_DDTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join DD07T t on t.DD07T_DOMNAME = 'BESKZ' AND t.DD07T_DOMVALUE = MARC_BESKZ
where t1.ProcurementTypeDescription <> ifnull(t.DD07T_DDTEXT, 'Not Set');

/* MRPControllerDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPControllerDescription = ifnull(t.T024D_DSNAM, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T024D t on t.T024D_DISPO = MARC_DISPO AND t.T024D_WERKS = Plant
where t1.MRPControllerDescription <> ifnull(t.T024D_DSNAM, 'Not Set');

/* MRPGroupDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPGroupDescription = ifnull(t.T438X_TEXT40, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T438X t on t.T438X_DISGR = MARC_DISGR AND t.T438X_WERKS = Plant
where t1.MRPGroupDescription <> ifnull(t.T438X_TEXT40, 'Not Set');

/* MRPLotSizeDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPLotSizeDescription = ifnull(t.T439T_LOSLT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T439T t on t.T439T_DISLS = MARC_DISLS
where t1.MRPLotSizeDescription <> ifnull(t.T439T_LOSLT, 'Not Set');

/* materialstatusdescription */
update tmp_dim_part_f_ins1 t1
set t1.materialstatusdescription = ifnull(t.t141t_MTSTB, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t141t t on t.T141T_MMSTA = ifnull(MARC_MMSTA, MARA_MSTAE)
where t1.materialstatusdescription <> ifnull(t.t141t_MTSTB, 'Not Set');

/* SpecialProcurementDescription */
update tmp_dim_part_f_ins1 t1
set t1.SpecialProcurementDescription = ifnull(t.T460T_LTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t460t t on t.T460T_SOBSL = MARC_SOBSL AND t.T460T_WERKS = Plant
where t1.SpecialProcurementDescription <> ifnull(t.T460T_LTEXT, 'Not Set');

/* ProfitCenterName */
update tmp_dim_part_f_ins1 t1
set t1.ProfitCenterName = ifnull(t.CEPCT_KTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join cepct t on t.CEPCT_PRCTR = MARC_PRCTR AND t.CEPCT_DATBI > current_date
where t1.ProfitCenterName <> ifnull(t.CEPCT_KTEXT, 'Not Set');

/* ExternalMaterialGroupDescription */
update tmp_dim_part_f_ins1 t1
set t1.ExternalMaterialGroupDescription = ifnull(t.TWEWT_EWBEZ, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join twewt t on t.TWEWT_EXTWG = MARA_EXTWG
where t1.ExternalMaterialGroupDescription <> ifnull(t.TWEWT_EWBEZ, 'Not Set');

/* MaterialDiscontinuationFlagDescription */
update tmp_dim_part_f_ins1 t1
set t1.MaterialDiscontinuationFlagDescription = ifnull(t.DD07T_DDTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join DD07T t on t.DD07T_DOMNAME = 'KZAUS' AND t.DD07T_DOMVALUE = MARC_KZAUS
where t1.MaterialDiscontinuationFlagDescription <> ifnull(t.DD07T_DDTEXT, 'Not Set');

/* AFSColorDescription */
update tmp_dim_part_f_ins1 t1
set t1.AFSColorDescription = ifnull(t.J_3ACOLRT_TEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join J_3ACOLRT t on t.J_3ACOLRT_J_3ACOL = MARA_J_3ACOL
where t1.AFSColorDescription <> ifnull(t.J_3ACOLRT_TEXT, 'Not Set');

/* SDMaterialStatusDescription */
update tmp_dim_part_f_ins1 t1
set t1.SDMaterialStatusDescription = ifnull(t.TVMST_VMSTB, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join tvmst t on t.TVMST_VMSTA = MARA_MSTAV
where t1.SDMaterialStatusDescription <> ifnull(t.TVMST_VMSTB, 'Not Set')
and t1.SDMaterialStatusDescription = 'Not Set'; /* BI-5298 additional filters for update*/

UPDATE dim_part dp 
   SET  SDMaterialStatusDescription = ifnull(t.TVMST_VMSTB,'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks 
		left join tvmst t on t.TVMST_VMSTA = MARA_MSTAV
WHERE SDMaterialStatusDescription <> ifnull(t.TVMST_VMSTB,'Not Set');

/* PlantMaterialStatusDescription */
update tmp_dim_part_f_ins1 t1
set t1.PlantMaterialStatusDescription = ifnull(t.t141t_MTSTB, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t141t t on t.T141T_MMSTA = MARC_MMSTA
where t1.PlantMaterialStatusDescription <> ifnull(t.t141t_MTSTB, 'Not Set');

/* ValidFrom */
update tmp_dim_part_f_ins1 t1
set t1.ValidFrom = ifnull(t.J_3AMAD_J_4ADTFR, to_date('0001-01-01','YYYY-MM-DD'))
from tmp_dim_part_f_ins1 t1
		left join J_3AMAD t on t.j_3amad_matnr = PartNumber and t.J_3AMAD_WERKS = Plant
where t1.ValidFrom <> ifnull(t.J_3AMAD_J_4ADTFR, to_date('0001-01-01','YYYY-MM-DD'));

/* MRPStatus */
update tmp_dim_part_f_ins1 t1
set t1.MRPStatus = ifnull(t.J_3AMAD_J_4ASTAT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join J_3AMAD t on t.j_3amad_matnr = PartNumber and t.J_3AMAD_WERKS = Plant
where t1.MRPStatus <> ifnull(t.J_3AMAD_J_4ASTAT, 'Not Set');

/* CheckingGroup */
update tmp_dim_part_f_ins1 t1
set t1.CheckingGroup = ifnull(t.TMVFT_BEZEI, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join TMVFT t on t.tmvft_mtvfp = marc_mtvfp
where t1.CheckingGroup <> ifnull(t.TMVFT_BEZEI, 'Not Set');

/* UPCNumber ambiguos replace */
merge into tmp_dim_part_f_ins1 dim
using(select dp.rowid rid, ifnull(max(t.MEAN_EAN11),'Not Set') as UPCNumber
	  from tmp_dim_part_f_ins1 dp
				left join MEAN t on t.MEAN_MATNR = dp.PartNumber
	  group by dp.rowid
	  ) src on dim.rowid = src.rid
when matched then update set dim.UPCNumber = src.UPCNumber
where dim.UPCNumber <> src.UPCNumber;

/* ConsumptionModeDescription */
update tmp_dim_part_f_ins1 t1
set t1.ConsumptionModeDescription = ifnull(t.DD07T_DDTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join DD07T t on t.DD07T_DOMNAME = 'VRMOD' AND t.DD07T_DOMVALUE = MARC_VRMOD
where t1.ConsumptionModeDescription <> ifnull(t.DD07T_DDTEXT, 'Not Set');

/* MRPControllerTelephone */
update tmp_dim_part_f_ins1 t1
set t1.MRPControllerTelephone = ifnull(t.T024D_DSTEL, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T024D t on t.T024D_DISPO = MARC_DISPO AND t.T024D_WERKS = Plant
where t1.MRPControllerTelephone <> ifnull(t.T024D_DSTEL, 'Not Set');

delete from number_fountain m where m.table_name = 'dim_part';
   
insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1; 

INSERT INTO dim_part(dim_partid,
                     PartNumber,
                     PartDescription,
                     Revision,
                     UnitOfMeasure,
                     RowStartDate,
                     RowIsCurrent,
                     CommodityCode,
                     PartType,
                     LeadTime,
                     StockLocation,
                     PurchaseGroupCode,
                     PurchaseGroupDescription,
                     MRPController,
                     MaterialGroup,
                     Plant,
                     ABCIndicator,
                     ProcurementType,
                     StorageLocation,
                     CriticalPart,
                     MRPType,
                     SupplySource,
                     StrategyGroup,
                     TransportationGroup,
                     Division,
                     DivisionDescription,
                     GeneralItemCategory,
                     DeletionFlag,
                     MaterialStatus,
                     ProductHierarchy,
                     MPN,
                     ProductHierarchyDescription,
                     MRPProfile,
                     MRPProfileDescription,
                     PartTypeDescription,
                     MaterialGroupDescription,
                     MRPTypeDescription,
                     ProcurementTypeDescription,
                     MRPControllerDescription,
                     MRPGroup,
                     MRPGroupDescription,
                     MinimumLotSize,
                     MRPLotSize,
                     MRPLotSizeDescription,
                     MaterialStatusDescription,
                     SpecialProcurement,
                     SpecialProcurementDescription,
                     BulkMaterial,
                     InhouseProductionTime,
                     ProfitCenterCode,
                     ProfitCenterName,
                     ExternalMaterialGroupCode,
                     ExternalMaterialGroupDescription,
                     MaterialDiscontinuationFlag,
                     MaterialDiscontinuationFlagDescription,
                     ProcessingTime,
                     TotalReplenishmentLeadTime,
                     GRProcessingTime,
                     OldPartNumber,
                     AFSColor,
                     AFSColorDescription,
                     SDMaterialStatusDescription,
                     Volume,
                     AFSMasterGrid,
                     AFSPattern,
                     Laboratory,
                     DoNotCost,
                     PlantMaterialStatus,
                     PlantMaterialStatusDescription,
                     REMProfile,
                     PlanningTimeFence,
                     SafetyStock,
                     RoundingValue,
                     ReorderPoint,
                     ValidFrom,
                     MRPStatus,
                     CheckingGroupCode,
                     CheckingGroup,
                        UPCNumber,
                        MaximumStockLevel,
                        ConsumptionMode,
                        ConsumptionModeDescription,
                        ConsumptionPeriodBackward,
                        ConsumptionPeriodForward,
                        PartLongDesc,
						ManfacturerNumber,
                        UPCCode,
                        NDCCode,
						SchedMarginKey,
						storageconditioncode,
						hazardousmaterialnumber,
						rangeofcoverage,
						safetytimeind,
						safetytime,
						maxlotsize,
						fixedlotsize,
						MRPControllerTelephone,
						productgroupsbu,ProdSupervisor,
						NetWeight,
						WeightUnit,
						REASONOUTOFSTOCK,
						DateOfAvailability,
						MaterialGrouping,
						LeadMatnr)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') + row_number() over(order by ''), t.* 
   from (SELECT DISTINCT PartNumber,
                     PartDescription,
                     Revision,
                     UnitOfMeasure,
                     RowStartDate,
                     RowIsCurrent,
                     CommodityCode,
                     PartType,
                     LeadTime,
                     StockLocation,
                     PurchaseGroupCode,
                     PurchaseGroupDescription,
                     MRPController,
                     MaterialGroup,
                     Plant,
                     ABCIndicator,
                     ProcurementType,
                     StorageLocation,
                     CriticalPart,
                     MRPType,
                     SupplySource,
                     StrategyGroup,
                     TransportationGroup,
                     Division,
                     DivisionDescription,
                     GeneralItemCategory,
                     DeletionFlag,
                     MaterialStatus,
                     ProductHierarchy,
                     MPN,
                     ProductHierarchyDescription,
                     MRPProfile,
                     MRPProfileDescription,
                     PartTypeDescription,
                     MaterialGroupDescription,
                     MRPTypeDescription,
                     ProcurementTypeDescription,
                     MRPControllerDescription,
                     MRPGroup,
                     MRPGroupDescription,
                     MinimumLotSize,
                     MRPLotSize,
                     MRPLotSizeDescription,
                     MaterialStatusDescription,
                     SpecialProcurement,
                     SpecialProcurementDescription,
                     BulkMaterial,
                     InhouseProductionTime,
                     ProfitCenterCode,
                     ProfitCenterName,
                     ExternalMaterialGroupCode,
                     ExternalMaterialGroupDescription,
                     MaterialDiscontinuationFlag,
                     MaterialDiscontinuationFlagDescription,
                     ProcessingTime,
                     TotalReplenishmentLeadTime,
                     GRProcessingTime,
                     OldPartNumber,
                     AFSColor,
                     AFSColorDescription,
                     SDMaterialStatusDescription,
                     Volume,
                     AFSMasterGrid,
                     AFSPattern,
                     Laboratory,
                     DoNotCost,
                     PlantMaterialStatus,
                     PlantMaterialStatusDescription,
                     REMProfile,
                     PlanningTimeFence,
                     SafetyStock,
                     RoundingValue,
                     ReorderPoint,
                     ValidFrom,
                     MRPStatus,
                     CheckingGroupCode,
                     CheckingGroup,
                     UPCNumber,
                     MaximumStockLevel,
                     ConsumptionMode,
                     ConsumptionModeDescription,
                     ConsumptionPeriodBackward,
                     ConsumptionPeriodForward,
                     PartLongDesc,
                     ManfacturerNumber,
                     UPCCode,
                     NDCCode,
                     SchedMarginKey,
                     storageconditioncode,
                     hazardousmaterialnumber,
                     rangeofcoverage,
                     safetytimeind,
                     safetytime,
                     maxlotsize,
                     fixedlotsize,
                     MRPControllerTelephone,
                     productgroupsbu,ProdSupervisor,
					 NetWeight,
					 WeightUnit,
					 REASONOUTOFSTOCK,
						DateOfAvailability,
						MaterialGrouping,
						LeadMatnr
		 from tmp_dim_part_f_ins1) t
		 where not exists (select 1 from dim_part tt where tt.PartNumber = t.PartNumber and tt.Plant = t.Plant);
		 
drop table if exists tmp_dim_part_f_ins1;

UPDATE dim_part dp 
SET  dp.PartNumber_NoLeadZero = ifnull(case when length(dp.partnumber) = 18 and dp.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from dp.partnumber) else dp.partnumber end,'Not Set'),
	 dp.dw_update_date = current_timestamp;	

update dim_part dp
set dp.loadinggroup = ifnull(ds.marc_ladgr,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt ds, dim_part dp
where     dp.partnumber = ds.mara_matnr
	  and dp.plant = marc_werks
	  and dp.loadinggroup <> ifnull(ds.marc_ladgr,'Not Set');

truncate table tmp_dim_MARA_MARC_MAKT_ins;	
truncate table tmp_dim_MARA_MARC_MAKT_del;
		/* VW Original
			call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_ins-tmp_dim_MARA_MARC_MAKT_ins')
			call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_del-tmp_dim_MARA_MARC_MAKT_del') */

DROP TABLE IF EXISTS tmp_MARA_MARC_MBEW;
CREATE TABLE tmp_MARA_MARC_MBEW
AS SELECT  MBEW_LAEPR,MBEW_STPRV,MBEW_VMVER,MBEW_PDATL,MBEW_PPRDL,MBEW_BWVA2,MBEW_ZPLD2,MBEW_ZPLD1,MBEW_LBKUM,MBEW_PEINH,MBEW_SALK3,MBEW_STPRS,MBEW_VERPR,MBEW_VMSTP,MBEW_VPRSV,MBEW_VJSTP,MARA_LAEDA,MBEW_VPLPR,MBEW_ZPLP1 ,MBEW_LPLPR,MBEW_BWPRH,
	MBEW_KALN1,MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,ifnull(mmm.MBEW_BWTAR,'Not Set') as MBEW_BWTAR,MBEW_LFMON,MBEW_KALNR,MBEW_ZPLPR,MBEW_VMPEI,MBEW_VJPEI,MBEW_LVORM,
row_number() over(partition by MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,ifnull(mmm.MBEW_BWTAR,'Not Set'),MBEW_LFMON order by '') as rowno FROM MARA_MARC_MBEW mmm;	   


UPDATE MBEW m 
   SET VERPR = MBEW_VERPR 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND rowno = 1
       AND VERPR <> MBEW_VERPR;

UPDATE MBEW m 
   SET STPRS = MBEW_STPRS
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON 
       AND rowno = 1
       AND STPRS <> MBEW_STPRS;

UPDATE MBEW m 
   SET SALK3 = MBEW_SALK3 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
 WHERE     m.MATNR = mmm.MBEW_MATNR
        AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND rowno = 1
       AND SALK3 <> MBEW_SALK3;

UPDATE MBEW m 
   SET PEINH = MBEW_PEINH 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND rowno = 1
       AND PEINH <> MBEW_PEINH;

UPDATE MBEW m 
   SET m.MBEW_LBKUM = mmm.MBEW_LBKUM 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND rowno = 1
       AND m.MBEW_LBKUM <> mmm.MBEW_LBKUM;

UPDATE MBEW m 
   SET LFMON = MBEW_LFMON 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
	   AND rowno = 1
       AND LFMON <> MBEW_LFMON;

	   
	   
	   
UPDATE MBEW m 
   SET m.MBEW_VMSTP = mmm.MBEW_VMSTP
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE m.MATNR = mmm.MBEW_MATNR
	AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND rowno = 1
       AND m.MBEW_VMSTP <> mmm.MBEW_VMSTP;
       

UPDATE MBEW m 
   SET VPRSV = MBEW_VPRSV 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND VPRSV <> MBEW_VPRSV 
       AND rowno = 1
       AND m.LFMON = mmm.MBEW_LFMON;

UPDATE MBEW m 
   SET m.MBEW_VJSTP = mmm.MBEW_VJSTP 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND rowno = 1
       AND m.MBEW_VJSTP <> mmm.MBEW_VJSTP;

UPDATE MBEW m 
   SET m.MBEW_VMPEI = mmm.MBEW_VMPEI 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
	   AND rowno = 1
       AND  m.MBEW_VMPEI <> mmm.MBEW_VMPEI;

UPDATE MBEW m 
   SET m.MBEW_VJPEI = mmm.MBEW_VJPEI 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
	   AND rowno = 1
       AND  m.MBEW_VJPEI <> mmm.MBEW_VJPEI;

UPDATE MBEW m 
   SET LVORM = MBEW_LVORM 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
	   AND rowno = 1
       AND LVORM <> MBEW_LVORM;

UPDATE MBEW m 
   SET        m.MBEW_ZPLD1 = mmm.MBEW_ZPLD1 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND rowno = 1
       AND m.MBEW_ZPLD1 <> mmm.MBEW_ZPLD1;







UPDATE MBEW m 
   SET        m.MBEW_KALN1 = mmm.MBEW_KALN1 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1;

UPDATE MBEW m 
   SET        m.MBEW_BWPRH = mmm.MBEW_BWPRH
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1
       AND m.MBEW_BWPRH <> mmm.MBEW_BWPRH;

UPDATE MBEW m 
   SET        m.MBEW_LPLPR = mmm.MBEW_LPLPR 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1;


UPDATE MBEW m 
   SET m.MBEW_VMVER = mmm.MBEW_VMVER 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_VMVER <> mmm.MBEW_VMVER
	   AND mmm.rowno = 1;

UPDATE MBEW m 
   SET m.MBEW_ZPLP1 = mmm.MBEW_ZPLP1 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1;
       
UPDATE MBEW m 
   SET m.MBEW_STPRV = mmm.MBEW_STPRV 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_STPRV <> mmm.MBEW_STPRV
	   AND mmm.rowno = 1;

UPDATE MBEW m 
   SET m.MBEW_VPLPR = mmm.MBEW_VPLPR 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1;

UPDATE MBEW m 
   SET m.MBEW_LAEPR = mmm.MBEW_LAEPR 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR 
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND  m.MBEW_LAEPR <> mmm.MBEW_LAEPR
	   AND mmm.rowno = 1;

UPDATE MBEW m 
   SET m.MBEW_ZPLD2 = mmm.MBEW_ZPLD2 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1
       AND m.MBEW_ZPLD2 <> mmm.MBEW_ZPLD2;

UPDATE MBEW m 
   SET m.MBEW_BWVA2 = mmm.MBEW_BWVA2
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1
       AND m.MBEW_BWVA2 <> mmm.MBEW_BWVA2;

UPDATE MBEW m 
   SET 	m.MBEW_KALNR = mmm.MBEW_KALNR 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_KALNR <> mmm.MBEW_KALNR
	   AND mmm.rowno = 1;

UPDATE MBEW m 
   SET 	m.MARA_LAEDA = mmm.MARA_LAEDA
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MARA_LAEDA <> mmm.MARA_LAEDA
       AND mmm.rowno = 1;

UPDATE MBEW m 
   SET 	m.MBEW_ZPLPR = mmm.MBEW_ZPLPR
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND 	m.MBEW_ZPLPR <> mmm.MBEW_ZPLPR
	   AND mmm.rowno = 1;

UPDATE MBEW m 
   SET 	m.MBEW_PDATL = mmm.MBEW_PDATL 
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1
       AND m.MBEW_PDATL <> mmm.MBEW_PDATL;

UPDATE MBEW m 
   SET 	m.MBEW_PPRDL = mmm.MBEW_PPRDL
FROM tmp_MARA_MARC_MBEW mmm, MBEW m
 WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND mmm.rowno = 1
       AND m.MBEW_PPRDL <> mmm.MBEW_PPRDL;


DROP TABLE IF EXISTS MBEW_000;

CREATE TABLE MBEW_000
AS
   SELECT MATNR,
          BWKEY,
          LFGJA,
          ifnull(mb.BWTAR, 'Not Set') BWTAR,
          LFMON
     FROM MBEW mb;

INSERT INTO MBEW(VERPR,
                 STPRS,
                 SALK3,
                 PEINH,
                 MBEW_LBKUM,
                 LFMON,
                 LFGJA,
                 MBEW_VMSTP,
                 VPRSV,
                 MBEW_VJSTP,
                 MBEW_VMPEI,
                 MBEW_VJPEI,
                 LVORM,
                 MATNR,
                 BWTAR,
                 BWKEY,
                 MBEW_ZPLD1,
                 MBEW_KALN1,
                 MBEW_BWPRH,
                 MBEW_LPLPR,
                 MBEW_VMVER,
                 MBEW_ZPLP1,
                 MBEW_STPRV,
                 MBEW_VPLPR,
                 MBEW_LAEPR,
                 MBEW_ZPLD2,
                 MBEW_BWVA2,
                 MBEW_KALNR,
                 MARA_LAEDA,
                 MBEW_ZPLPR,
                 MBEW_PDATL,
                 MBEW_PPRDL)
   SELECT DISTINCT MBEW_VERPR,
                   MBEW_STPRS,
                   MBEW_SALK3,
                   MBEW_PEINH,
                   MBEW_LBKUM,
                   MBEW_LFMON,
                   MBEW_LFGJA,
                   MBEW_VMSTP,
                   MBEW_VPRSV,
                   MBEW_VJSTP,
                   MBEW_VMPEI,
                   MBEW_VJPEI,
                   MBEW_LVORM,
                   MBEW_MATNR,
                   MBEW_BWTAR,
                   MBEW_BWKEY,
                   MBEW_ZPLD1,
                   MBEW_KALN1,
                   MBEW_BWPRH,
                   MBEW_LPLPR,
                   MBEW_VMVER,
                   MBEW_ZPLP1,
                   MBEW_STPRV,
                   MBEW_VPLPR,
                   MBEW_LAEPR,
                   MBEW_ZPLD2,
                   MBEW_BWVA2,
                   MBEW_KALNR,
                   MARA_LAEDA,
                   MBEW_ZPLPR,
                   MBEW_PDATL,
                   MBEW_PPRDL
     FROM MARA_MARC_MBEW mmm
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM MBEW_000 mb
                   WHERE     mb.MATNR = mmm.MBEW_MATNR
                         AND mb.BWKEY = mmm.MBEW_BWKEY
                         AND mb.LFGJA = mmm.MBEW_LFGJA
                         AND BWTAR = ifnull(mmm.MBEW_BWTAR, 'Not Set')
                         AND mb.LFMON = mmm.MBEW_LFMON);

DROP TABLE IF EXISTS MBEW_000;

truncate table mbew_no_bwtar;
		/* VW Original: CALL vectorwise(combine 'mbew_no_bwtar-mbew_no_bwtar') */

DROP TABLE IF EXISTS SALK3_000;

CREATE TABLE SALK3_000
AS
   SELECT a.MATNR,
          a.BWKEY,
          ((a.LFGJA * 100) + a.LFMON) LFGJA_LFMON,
          max(a.SALK3) MAXSALKS,
          max(a.MBEW_KALNR) MAXMBEW_KALNR
     FROM mbew a
    WHERE a.BWTAR IS NULL
   GROUP BY a.MATNR, a.BWKEY, ((a.LFGJA * 100) + a.LFMON);

INSERT INTO mbew_no_bwtar(BWKEY,
                          MATNR,
                          LVORM,
                          VPRSV,
                          VERPR,
                          STPRS,
                          LFGJA,
                          LFMON,
                          PEINH,
                          SALK3,
                          BWTAR,
                          MBEW_LBKUM,
                          MBEW_VMPEI,
                          MBEW_VMSTP,
                          MBEW_VJSTP,
                          MBEW_VJPEI,
                          MBEW_ZPLD1,
                          MBEW_KALN1,
                          MBEW_VMVER,
                          MBEW_STPRV,
                          MBEW_LAEPR,
                          MBEW_BWPRH,
                          MBEW_ZPLP1,
                          MBEW_ZPLD2,
                          MBEW_VPLPR,
                          MBEW_LPLPR,
                          MBEW_BWVA2,
                          MBEW_KALNR,
                          MARA_LAEDA,
                          MBEW_ZPLPR,
                          MBEW_PDATL,
                          MBEW_PPRDL)
   SELECT DISTINCT BWKEY,
                   MATNR,
                   LVORM,
                   VPRSV,
                   VERPR,
                   STPRS,
                   LFGJA,
                   LFMON,
                   PEINH,
                   SALK3,
                   BWTAR,
                   MBEW_LBKUM,
                   MBEW_VMPEI,
                   MBEW_VMSTP,
                   MBEW_VJSTP,
                   MBEW_VJPEI,
                   NULL MBEW_ZPLD1,
                   MBEW_KALN1,
                   MBEW_VMVER,
                   MBEW_STPRV,
                   NULL MBEW_LAEPR,
                   MBEW_BWPRH,
                   MBEW_ZPLP1,
                   NULL MBEW_ZPLD2,
                   MBEW_VPLPR,
                   MBEW_LPLPR,
                   MBEW_BWVA2,
                   MBEW_KALNR,
                   NULL MARA_LAEDA,
                   MBEW_ZPLPR,
                   MBEW_PDATL,
                   MBEW_PPRDL
     FROM mbew m
    WHERE BWTAR IS NULL
          AND SALK3 IN
                 (SELECT MAXSALKS
                    FROM SALK3_000 a
                   WHERE     a.MATNR = m.MATNR
                         AND a.BWKEY = m.BWKEY
                         AND LFGJA_LFMON = ((m.LFGJA * 100) + m.LFMON))
          AND MBEW_KALNR IN
                 (SELECT MAXMBEW_KALNR
                    FROM SALK3_000 a
                   WHERE     a.MATNR = m.MATNR
                         AND a.BWKEY = m.BWKEY
                         AND LFGJA_LFMON = ((m.LFGJA * 100) + m.LFMON));
/* temporaty fix for MBEW */	
drop table if exists tmp_MBEW_NO_BWTAR_distinct_123;
create table tmp_MBEW_NO_BWTAR_distinct_123
as
select x.*, row_number() over(partition by MATNR,BWKEY,LFGJA,LFMON order by '') rono from mbew_no_bwtar x;

delete from mbew_no_bwtar;

insert into mbew_no_bwtar(BWKEY,MATNR,LVORM,VPRSV,VERPR,STPRS,LFGJA,LFMON,PEINH,SALK3,BWTAR,MBEW_LBKUM,MBEW_VMPEI,MBEW_VMSTP,MBEW_VJSTP,MBEW_VJPEI,
	MBEW_ZPLD1,MBEW_KALN1,MBEW_VMVER,MBEW_STPRV,MBEW_LAEPR,MBEW_BWPRH,MBEW_ZPLP1,MBEW_ZPLD2,MBEW_VPLPR,MBEW_LPLPR,MBEW_BWVA2,MBEW_KALNR,MARA_LAEDA,
	MBEW_ZPLPR,MBEW_PDATL,MBEW_PPRDL,MBEW_BKLAS)
select BWKEY,MATNR,LVORM,VPRSV,VERPR,STPRS,LFGJA,LFMON,PEINH,SALK3,BWTAR,MBEW_LBKUM,MBEW_VMPEI,MBEW_VMSTP,MBEW_VJSTP,MBEW_VJPEI,
	MBEW_ZPLD1,MBEW_KALN1,MBEW_VMVER,MBEW_STPRV,MBEW_LAEPR,MBEW_BWPRH,MBEW_ZPLP1,MBEW_ZPLD2,MBEW_VPLPR,MBEW_LPLPR,MBEW_BWVA2,MBEW_KALNR,MARA_LAEDA,
	MBEW_ZPLPR,MBEW_PDATL,MBEW_PPRDL,MBEW_BKLAS
from tmp_MBEW_NO_BWTAR_distinct_123 where rono = 1;
/* end temporaty fix for MBEW 26.05.2016 */					 

DROP TABLE IF EXISTS SALK3_000;

UPDATE dim_part dpt 
   SET dpt.AFSTargetGroup = ifnull(MARA_J_3AGEND, 'Not Set'),
       dpt.AFSTargetGroupDescription = ifnull(atg.J_3AGENDRT_TEXT, 'Not Set'),
			dpt.dw_update_date = current_timestamp
from dim_part dpt
		inner join mara_marc_makt mck on    dpt.PartNumber = mck.MARA_MATNR
										AND dpt.Plant = mck.MARC_WERKS
		inner join dim_plant p on    p.plantcode = marc_werks
								 AND p.languagekey = makt_spras
		left join j_3agendrt atg on atg.J_3AGENDRT_J_3AGEND = mck.MARA_J_3AGEND;

UPDATE dim_part dpt 
 SET dpt.ExtManfacturerCode = dv.VendorMfgCode,
     dpt.ExtManfacturerDesc = dv.VendorName,
			dpt.dw_update_date = current_timestamp
from  dim_Vendor dv, dim_part dpt
WHERE dpt.ManfacturerNumber = dv.VendorNumber;     

update dim_part  
set 	ValidFrom ='0001-01-01',
	dw_update_date = current_timestamp 
where ValidFrom is null;

/* 17 feb 2015 */
update  dim_part pt 
set storageconditiondescription = ifnull(rbtxt,'Not Set'),
	dw_update_date = current_timestamp
from t142t t, dim_part pt
where storageconditioncode = raube
and storageconditioncode <> ifnull(rbtxt,'Not Set');

/* 19 feb 2015 */
update dim_part pt
set lotsizeindicator = ifnull(T439A.T439A_LOSKZ,'Not Set'),
	dw_update_date = current_timestamp
from T439A, dim_part pt
where T439A.T439A_DISLS = pt.MRPLotSize
and lotsizeindicator <> ifnull(T439A.T439A_LOSKZ,'Not Set');

update dim_part pt
set numberofperiods = ifnull(T439A.T439A_PERAZ,0),
	dw_update_date = current_timestamp
from T439A, dim_part pt
where T439A.T439A_DISLS = pt.MRPLotSize
and numberofperiods <> ifnull(T439A.T439A_PERAZ,0);

update dim_part pt
set periodindicator = ifnull(T438R.RWPER,'Not Set'),
	dw_update_date = current_timestamp
from T438R, dim_part pt
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage
and periodindicator <> ifnull(T438R.RWPER,'Not Set');


update dim_part pt
set targetcoverage1 = ifnull(T438R.RW1TG,0),
	dw_update_date = current_timestamp
from T438R, dim_part pt
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage
and targetcoverage1 <> ifnull(T438R.RW1TG,0);

update dim_part pt
set typeperiodlength = ifnull(T438R.RWART,'Not Set'),
	dw_update_date = current_timestamp
from T438R, dim_part pt
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage
and typeperiodlength <> ifnull(T438R.RWART,'Not Set');

/* end 19 feb 2015 */

INSERT INTO tmp_MARA_MARC_MBEWH_ins
select mmh.MBEWH_MATNR,mmh.MBEWH_BWKEY,mmh.MBEWH_LFGJA,mmh.MBEWH_LFMON,MBEWH_BWTAR
from MARA_MARC_MBEWH mmh;

insert into tmp_MARA_MARC_MBEWH_del
select mh.MATNR,mh.BWKEY,mh.LFGJA,mh.LFMON,BWTAR
from MBEWH mh;

merge into tmp_MARA_MARC_MBEWH_ins dst
using (select distinct t1.rowid rid
	   from tmp_MARA_MARC_MBEWH_ins t1
				inner join tmp_MARA_MARC_MBEWH_del t2 on    ifnull(t1.MBEWH_MATNR,'Not Set') = ifnull(t2.MBEWH_MATNR,'Not Set')
														and ifnull(t1.MBEWH_BWKEY,'Not Set') = ifnull(t2.MBEWH_BWKEY,'Not Set')
														and ifnull(t1.MBEWH_LFGJA, 0.00) = ifnull(t2.MBEWH_LFGJA, 0.00) 
														and ifnull(t1.MBEWH_LFMON, 0.00) = ifnull(t2.MBEWH_LFMON, 0.00)
														and ifnull(t1.MBEWH_BWTAR,'Not Set') = ifnull(t2.MBEWH_BWTAR,'Not Set')) src
on dst.rowid = src.rid
when matched then delete;
			/* VW Original:
				call vectorwise (combine 'tmp_MARA_MARC_MBEWH_ins-tmp_MARA_MARC_MBEWH_del') */

INSERT INTO MBEWH(VERPR,
                  STPRS,
                  SALK3,
                  PEINH,
                  MBEWH_LBKUM,
                  LFMON,
                  LFGJA,
                  VPRSV,
                  MATNR,
                  BWTAR,
                  BWKEY)
   SELECT distinct mmh.MBEWH_VERPR,
          mmh.MBEWH_STPRS,
          mmh.MBEWH_SALK3,
          mmh.MBEWH_PEINH,
          mmh.MBEWH_LBKUM,
          mmh.MBEWH_LFMON,
          mmh.MBEWH_LFGJA,
          mmh.MBEWH_VPRSV,
          mmh.MBEWH_MATNR,
          mmh.MBEWH_BWTAR,
          mmh.MBEWH_BWKEY
     FROM MARA_MARC_MBEWH mmh, tmp_MARA_MARC_MBEWH_ins mh
        WHERE mmh.MBEWH_MATNR = mh.MBEWH_MATNR
        AND mmh.MBEWH_BWKEY = mh.MBEWH_BWKEY
        AND mmh.MBEWH_LFGJA = mh.MBEWH_LFGJA
        AND mmh.MBEWH_LFMON = mh.MBEWH_LFMON
        AND ifnull(mmh.MBEWH_BWTAR, 'Not Set') = ifnull(mh.MBEWH_BWTAR, 'Not Set');

delete from mbewh_no_bwtar;

INSERT INTO mbewh_no_bwtar(MATNR,
                           BWKEY,
                           LFGJA,
                           LFMON,
                           VPRSV,
                           VERPR,
                           STPRS,
                           PEINH,
                           SALK3,
                           BWTAR,
                           MBEWH_LBKUM)
   SELECT distinct MATNR,
          BWKEY,
          LFGJA,
          LFMON,
          VPRSV,
          VERPR,
          STPRS,
          PEINH,
          SALK3,
          BWTAR,
          MBEWH_LBKUM
     FROM mbewh
     WHERE ifnull(BWTAR,'null') <> 'null';

truncate table tmp_MARA_MARC_MBEWH_ins;
truncate table tmp_MARA_MARC_MBEWH_del;
DROP TABLE IF EXISTS tmp_MARA_MARC_MBEW;
			
/* Marius 18 jan 2016 update Made to Order/ Made to Ship*/
update dim_part dp
set mtomts = 'Not Set'
from mara_marc_makt ma,dim_part dp
where dp.partnumber = mara_matnr and dp.plant = ma.marc_werks
and ifnull(mtomts,'a') <> 'Not Set';

update dim_part dp
set mtomts = 'MTO'
from mara_marc_makt ma,dim_part dp
where dp.partnumber = mara_matnr and dp.plant = ma.marc_werks
	and marc_disgr in (select filter_value from filter_criteria_mto where 
	source_system_name = 'Tempo Europe' 
	and filter_object= 'DISGR')
	and ifnull(mtomts,'a') <> 'MTO';

/* When an item is not Made to Order, it is Made to Ship */	
update dim_part dp
set mtomts = 'MTS'
from mara_marc_makt ma,dim_part dp
where dp.partnumber = mara_matnr and dp.plant = ma.marc_werks
	and ifnull(mtomts,'a') <> 'MTO'
	and ifnull(mtomts,'a') <> 'MTS';
/* end 18 Jan 2016 */	
update dim_part dt
set dt.abcgroup = ds.abcgroup,dt.abcsegments = ds.abcsegments
from dim_part dt, csv_abc ds
where dt.abcindicator = ds.abcindicator
and dt.abcgroup <> ds.abcgroup
and dt.abcsegments <> ds.abcsegments;


/* Alex D Manual Uploaded file*/
/*DELETE FROM dim_part dp
WHERE EXISTS( SELECT 1 FROM tmp_manual_inventory h Where dp.PLANT=h.Plant
AND dp.PartNumber = h.LOCAL_MATERIAL_NUMBER)*/

DROP TABLE IF EXISTS max_holder_t01;
CREATE TABLE max_holder_t01
AS
SELECT ifnull(max(dim_partid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) maxid FROM dim_part;

insert into dim_part
(
dim_partid,
partnumber,
PARTDESCRIPTION,
PLANT
)
select
x.maxid + row_number() over(order by '') as dim_partid,
partnumber,
LOCAL_MATERIAL_DESCRIPTION,
PLANT
from
    (select distinct max_holder_t01.maxid,
            LOCAL_MATERIAL_NUMBER as partnumber,
			LOCAL_MATERIAL_DESCRIPTION,
            PLANT as PLANT
      FROM tmp_manual_inventory
      CROSS JOIN max_holder_t01
      WHERE LOCAL_MATERIAL_NUMBER <> '#') x
where not exists (select 1 from dim_part ne where x.partnumber = ne.partnumber and x.PLANT = ne.PLANT);

/* Reason out of stock description - Oana 06Oct2016 */
UPDATE dim_part dp
SET dp.REASONOUTOFSTOCKDESC = ifnull(t.T9D20T_BEZEI,'Not Set'),
    dp.dw_update_date = current_timestamp
FROM dim_part dp, T9D20T t
 WHERE dp.REASONOUTOFSTOCK = t.T9D20T_YYD_YRESG
       AND dp.REASONOUTOFSTOCKDESC <> ifnull(t.T9D20T_BEZEI,'Not Set');
	   
update dim_part set REASONOUTOFSTOCKDESC = 'Not Set' where REASONOUTOFSTOCK ='Not Set';
/* END Reason out of stock description - Oana 06Oct2016 */	


/* Roxana - Add Material Grouping Field */

UPDATE dim_part dp
   SET MaterialGrouping = ifnull(MARC_MATGR, 'Not Set'),
                dp.dw_update_date = current_timestamp
FROM mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = mck.marc_werks 
	   AND p.languagekey = mck.makt_spras
       AND MaterialGrouping <> ifnull(MARC_MATGR, 'Not Set');
update dim_part dp
set dp.laboratory = ifnull(ds.MARA_LABOR,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt ds, dim_part dp
where     dp.partnumber = ds.mara_matnr
	  and dp.plant = marc_werks
	  and dp.laboratory <> ifnull(ds.MARA_LABOR,'Not Set');
/* Cornelia 7 march 2017 add ABCdesc */
UPDATE dim_part dp 
   SET  ABCdesc = ifnull(TMABCT_TMABC, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from  dim_part dp, TMABCT t
	   WHERE    dp.ABCIndicator = t.TMABCT_MAABC
	   and ABCdesc <> ifnull(TMABCT_TMABC, 'Not Set');	  


/* Roxana - Add Lead Material Number Field */

	   
UPDATE dim_part dp
   SET LeadMatnr = ifnull(MARA_YYD_LEADM, 'Not Set'),
                dp.dw_update_date = current_timestamp
FROM mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = mck.marc_werks 
	   AND p.languagekey = mck.makt_spras
       AND LeadMatnr <> ifnull(MARA_YYD_LEADM, 'Not Set');