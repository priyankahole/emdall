UPDATE 
dim_MaterialPriceGroup5 a
SET a.Description = ifnull(TVM5T_BEZEI, 'Not Set'),
	a.dw_update_date = current_timestamp
FROM 
dim_MaterialPriceGroup5 a,
TVM5T
WHERE a.MaterialPriceGroup5 = TVM5T_MVGR5 AND RowIsCurrent = 1;
 
INSERT INTO dim_MaterialPriceGroup5(dim_MaterialPriceGroup5Id, MaterialPriceGroup5,Description,RowIsCurrent, RowStartDate)
SELECT 1, 'Not Set', 'Not Set', 1, current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup5
               WHERE dim_MaterialPriceGroup5Id = 1);


delete from number_fountain m where m.table_name = 'dim_MaterialPriceGroup5';
   
insert into number_fountain
select 	'dim_MaterialPriceGroup5',
	ifnull(max(d.dim_materialpricegroup5Id), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MaterialPriceGroup5 d
where d.dim_materialpricegroup5Id <> 1; 

INSERT INTO dim_MaterialPriceGroup5(dim_materialpricegroup5Id,
                                    MaterialPriceGroup5,
                                    Description,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MaterialPriceGroup5')
          + row_number() over(order by '') ,
          t.TVM5T_MVGR5,
          ifnull(TVM5T_BEZEI, 'Not Set'),
          current_timestamp,
          1
     FROM TVM5T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup5 a
               WHERE a.MaterialPriceGroup5 = TVM5T_MVGR5);
