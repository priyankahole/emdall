/* Capacity Utilization - Manufacturing Sites */
insert into dim_jda_manufsite (dim_jda_manufsiteid,manufsite,res_udc_grp1,dw_insert_date,dw_update_date,projectsourceid,resource)
select 1, 'Not Set', 'Not Set', current_timestamp, current_timestamp, 1, 'Not Set'
from (select 1) where not exists (select 'x' from dim_jda_manufsite where dim_jda_manufsiteid = 1);

drop table if exists tmp_jda_manufsite;
create table tmp_jda_manufsite as
  select distinct stsc_loc_descr as descr
	,stsc_res_udc_grp1 as res_udc_grp1
	,stsc_res_res as resource
from stsc_res res, stsc_loc loc
where stsc_res_loc = stsc_loc_loc
and stsc_res_udc_grp1 = 'MANUFACTURING';

delete from number_fountain m where m.table_name = 'dim_jda_manufsite';
insert into number_fountain
select 'dim_jda_manufsite'
        ,ifnull(max(d.dim_jda_manufsiteid)
        ,ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_manufsite d
where dim_jda_manufsiteid <> 1;

insert into dim_jda_manufsite (DIM_JDA_MANUFSITEID,MANUFSITE,RES_UDC_GRP1, RESOURCE)
select (select m.max_id from number_fountain m where m.table_name = 'dim_jda_manufsite' )+ row_number() over(order by '') dim_jda_manufsiteid
	,ifnull(descr,'Not Set') as manufsite
	,ifnull(t.res_udc_grp1,'Not Set') as res_udc_grp1
	,t.resource as resource
from tmp_jda_manufsite t
where not exists (select 'x' from dim_jda_manufsite dm where dm.resource = ifnull(t.resource,'Not Set'));

update dim_jda_manufsite dm
  set manufsite = ifnull(descr,'Not Set')
from dim_jda_manufsite dm, tmp_jda_manufsite t
where dm.resource = ifnull(t.resource,'Not Set')
	and manufsite <> ifnull(descr,'Not Set');
