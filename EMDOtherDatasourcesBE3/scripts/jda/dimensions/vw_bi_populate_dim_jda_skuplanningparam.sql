/* dim_jda_skuplanningparam */
insert into dim_jda_skuplanningparam (dim_jda_skuplanningparamid,item,loc,drpcovdur,drprule,incdrpqty,mindrpqty,mpscovdur,mpsrule,incmpsqty,minmpsqty,dw_insert_date,dw_update_date)
	values(1,'Not Set','Not Set',10080,1,1,0,10080,1,1,0, current_timestamp,current_timestamp);

delete from number_fountain m where m.table_name = 'dim_jda_skuplanningparam';
insert into number_fountain
select 'dim_jda_skuplanningparam',
            ifnull(max(d.dim_jda_skuplanningparamid), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_skuplanningparam d
where d.dim_jda_skuplanningparamid <> 1;

drop table if exists tmp_skuplanningparam;
create table tmp_skuplanningparam as 
	select spp.item
		,spp.loc
		,ifnull(spp.drpcovdur,10080)/1440 	as drpcovdur
		,spp.drprule
		,spp.incdrpqty
		,spp.mindrpqty
		,ifnull(spp.mpscovdur,10080)/1440	as mpscovdur
		,spp.mpsrule
		,spp.incmpsqty
		,spp.minmpsqty
	from stsc_skuplanningparam spp;

update dim_jda_skuplanningparam dspp
	set dspp.drpcovdur = ifnull(tspp.drpcovdur,7),
		dw_update_date = current_timestamp
from dim_jda_skuplanningparam dspp , tmp_skuplanningparam tspp
where dspp.item = tspp.item and dspp.loc = tspp.loc;

update dim_jda_skuplanningparam dspp
	set dspp.drprule = ifnull(tspp.drprule,1),
		dw_update_date = current_timestamp
from dim_jda_skuplanningparam dspp , tmp_skuplanningparam tspp
where dspp.item = tspp.item and dspp.loc = tspp.loc;

update dim_jda_skuplanningparam dspp
	set dspp.incdrpqty = ifnull(tspp.incdrpqty,1),
		dw_update_date = current_timestamp
from dim_jda_skuplanningparam dspp , tmp_skuplanningparam tspp
where dspp.item = tspp.item and dspp.loc = tspp.loc;

update dim_jda_skuplanningparam dspp
	set dspp.mindrpqty = ifnull(tspp.mindrpqty,0),
		dw_update_date = current_timestamp
from dim_jda_skuplanningparam dspp , tmp_skuplanningparam tspp
where dspp.item = tspp.item and dspp.loc = tspp.loc;

update dim_jda_skuplanningparam dspp
	set dspp.mpscovdur = ifnull(tspp.mpscovdur,7),
		dw_update_date = current_timestamp
from dim_jda_skuplanningparam dspp , tmp_skuplanningparam tspp
where dspp.item = tspp.item and dspp.loc = tspp.loc;

update dim_jda_skuplanningparam dspp
	set dspp.mpsrule = ifnull(tspp.mpsrule,1),
		dw_update_date = current_timestamp
from dim_jda_skuplanningparam dspp , tmp_skuplanningparam tspp
where dspp.item = tspp.item and dspp.loc = tspp.loc;

update dim_jda_skuplanningparam dspp
	set dspp.incmpsqty = ifnull(tspp.incmpsqty,1),
		dw_update_date = current_timestamp
from dim_jda_skuplanningparam dspp , tmp_skuplanningparam tspp
where dspp.item = tspp.item and dspp.loc = tspp.loc;

update dim_jda_skuplanningparam dspp
	set dspp.minmpsqty = ifnull(tspp.minmpsqty,0),
		dw_update_date = current_timestamp
from dim_jda_skuplanningparam dspp , tmp_skuplanningparam tspp
where dspp.item = tspp.item and dspp.loc = tspp.loc;

insert into dim_jda_skuplanningparam (dim_jda_skuplanningparamid,item,loc,drpcovdur,drprule,incdrpqty,mindrpqty,mpscovdur,mpsrule,incmpsqty,minmpsqty,dw_insert_date,dw_update_date)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_skuplanningparam') + row_number()  over(order by '') as dim_jda_skuplanningparamid
		,spp.item
		,spp.loc
		,ifnull(spp.drpcovdur,7) 			as drpcovdur		--converted in days
		,ifnull(spp.drprule,1) 				as drprule
		,ifnull(spp.incdrpqty,1)			as incdrpqty
		,ifnull(spp.mindrpqty,0)			as mindrpqty
		,ifnull(spp.mpscovdur,7)			as mpscovdur		--converted in days
		,ifnull(spp.mpsrule,1)				as mpsrule
		,ifnull(spp.incmpsqty,1)			as incmpsqty
		,ifnull(spp.minmpsqty,0)			as minmpsqty
		,current_timestamp 					as dw_insert_date
		,current_timestamp					as dw_update_date
	from tmp_skuplanningparam spp
	where not exists (select 'x' from dim_jda_skuplanningparam dspp where dspp.item = spp.item and dspp.loc = spp.loc)
		and spp.item is not null;

drop table if exists tmp_skuplanningparam;