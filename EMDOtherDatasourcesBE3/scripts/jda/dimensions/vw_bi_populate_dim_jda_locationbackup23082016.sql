/* dim_jda_location */

insert into dim_jda_location (dim_jda_locationid,
                              location_id)
select 1, -99999999
from (select 1) a
where not exists ( select 'x' from dim_jda_location where dim_jda_locationid = 1); 

drop table if exists tmp_dw_location;
create table tmp_dw_location as
select   nl.nwmgr_location_location_id		as location_id	 /* taking this ID as base for further table joins */
	    ,nl.nwmgr_location_name				as location_name /* lowest level drill */
		,sl.stsc_loc_loc					as loc			 /* this field can contain Null values, so further joins are preferable to be performed on previous - nwmgr_location_name */
		,sl.stsc_loc_udc_country 			as country
		,sl.stsc_loc_descr					as locdescr
		,sl.stsc_loc_udc_market				as market
		,sl.stsc_loc_udc_region				as region
		,sl.stsc_loc_udc_tpregion			as tpregion
		,sl.stsc_loc_udc_lrpregion			as locgrouping
		,sl.stsc_loc_cust					as dpplanner
		,sl.stsc_loc_udc_scmanager			as scmanager
		,sl.stsc_loc_udc_respcmg			as respcmg
		,sl.stsc_loc_udc_regulflex			as regulflex
		,sl.stsc_loc_udc_reporting_country	as reporting_country
		,sl.stsc_loc_udc_reporting_region	as reporting_region
		,nl.nwmgr_location_uda_exchrate		as exchrate	
		,sl.stsc_loc_udc_gauss_rep_region	as udc_gauss_rep_region
from nwmgr_location nl
		left join stsc_loc sl on ifnull(nwmgr_location_name,'Not Set') = ifnull(stsc_loc_loc, 'Not Set');

update dim_jda_location djl
set djl.country = ifnull(t.country,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.country <> ifnull(t.country,'Not Set');

update dim_jda_location djl
set djl.locdescr = ifnull(t.locdescr,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.locdescr <> ifnull(t.locdescr,'Not Set');
	  
update dim_jda_location djl
set djl.market = ifnull(t.market,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.market <> ifnull(t.market,'Not Set');
	  
update dim_jda_location djl
set djl.region = ifnull(t.region,'Not Set'),
    dw_update_date = current_timestamp
from tmp_dw_location t, dim_jda_location djl
where     djl.location_id = t.location_id
	  and djl.region <> ifnull(t.region,'Not Set');

update dim_jda_location djl
set djl.tpregion = ifnull(t.tpregion,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.tpregion <> ifnull(t.tpregion,'Not Set');

update dim_jda_location djl
set djl.locgrouping = ifnull(t.locgrouping,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.locgrouping <> ifnull(t.locgrouping,'Not Set');

update dim_jda_location djl
set djl.dpplanner = ifnull(t.dpplanner,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.dpplanner <> ifnull(t.dpplanner,'Not Set');

update dim_jda_location djl
set djl.scmanager = ifnull(t.scmanager,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.scmanager <> ifnull(t.scmanager,'Not Set');

update dim_jda_location djl
set djl.respcmg = ifnull(t.respcmg,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.respcmg <> ifnull(t.respcmg,'Not Set');

update dim_jda_location djl
set djl.regulflex = ifnull(t.regulflex,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.regulflex <> ifnull(t.regulflex,'Not Set');

update dim_jda_location djl
set djl.reporting_country = ifnull(t.reporting_country,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.reporting_country <> ifnull(t.reporting_country,'Not Set');

update dim_jda_location djl
set djl.reporting_region = ifnull(t.reporting_region,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.reporting_region <> ifnull(t.reporting_region,'Not Set');
	  
update dim_jda_location djl
set djl.exchrate = ifnull(t.exchrate,0),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.exchrate <> ifnull(t.exchrate,0);
	  
update dim_jda_location djl
set djl.udc_gauss_rep_region = ifnull(t.stsc_loc_udc_gauss_rep_region,'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_location djl,tmp_dw_location t
where     djl.location_id = t.location_id
	  and djl.udc_gauss_rep_region <> ifnull(t.stsc_loc_udc_gauss_rep_region,'Not Set');	  
	  
delete from number_fountain m where m.table_name = 'dim_jda_location';
insert into number_fountain
select 'dim_jda_location',
            ifnull(max(d.dim_jda_locationid ), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_location d
where d.dim_jda_locationid <> 1;

insert into dim_jda_location(
					 dim_jda_locationid	
					,location_id		
					,location_name		
					,loc				
					,country			
					,locdescr			
					,market				
					,region				
					,tpregion			
					,locgrouping		
					,dpplanner			
					,scmanager			
					,respcmg			
					,regulflex			
					,reporting_country	
					,reporting_region	
					,plannig_item_flag)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_location') + row_number()  over(order by '') AS dim_jda_locationid
		,location_id		
		,ifnull(location_name,'Not Set') as location_name		
		,ifnull(loc,'Not Set') 			as loc
		,ifnull(country,'Not Set') 		as country
		,ifnull(locdescr,'Not Set') 	as locdescr
		,ifnull(market,'Not Set') 		as market
		,ifnull(region,'Not Set') 		as region
		,ifnull(tpregion,'Not Set') 	as tpregion
		,ifnull(locgrouping,'Not Set') 	as locgrouping
		,ifnull(dpplanner,'Not Set') 	as dpplanner
		,ifnull(scmanager,'Not Set') 	as scmanager
		,ifnull(respcmg,'Not Set') 		as respcmg
		,ifnull(regulflex,'Not Set')	as regulflex
		,ifnull(reporting_country,'Not Set') as reporting_country
		,ifnull(reporting_region,'Not Set')  as reporting_region
		,case when loc is null and location_name is not null 
				then 'Yes' 
			   else 'Not Set'
		 end as plannig_item_flag
from tmp_dw_location ds
where not exists (select 'x' from dim_jda_location dc where dc.location_id = ds.location_id);
