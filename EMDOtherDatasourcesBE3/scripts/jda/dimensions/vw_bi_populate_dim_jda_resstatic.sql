delete from number_fountain m where m.table_name = 'dim_jda_resstatic';
insert into number_fountain
select 'dim_jda_resstatic'
        ,ifnull(max(d.dim_jda_resstaticid)
        ,ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_resstatic d
where dim_jda_resstaticid <> 1;

drop table if exists tmp_jda_cu_resstatic;
create table tmp_jda_cu_resstatic as
select stsc_resprojstatic_loc loc,
	stsc_resprojstatic_res res,
	(case when month(stsc_resprojstatic_startdate) = month(weekstartdate) then weekstartdate else weekenddate end) as cudate,
	sum(stsc_resprojstatic_cpptotcap) capacity
from stsc_resprojstatic rs
	inner join stsc_res on stsc_res_res = stsc_resprojstatic_res
	inner join stsc_loc on stsc_resprojstatic_loc = stsc_loc_loc
	inner join dim_date dt on stsc_resprojstatic_startdate = datevalue
where stsc_res_udc_grp1 in ('MANUFACTURING','PACKING')
	and rs.stsc_resprojstatic_loc in ('252MF','450MF','354MF','220MF1')
group by stsc_resprojstatic_loc,
	stsc_resprojstatic_res,
	(case when month(stsc_resprojstatic_startdate) = month(weekstartdate) then weekstartdate else weekenddate end);

insert into dim_jda_resstatic (dim_jda_resstaticid,loc,res,datevalue,capacity,snapshotdate)
select (select max_id from number_fountain where table_name = 'dim_jda_manufsite') + row_number() over(order by '') dim_jda_resstaticid
, rs.loc
, rs.res
, rs.cudate as datevalue
, ifnull(rs.capacity,0) as capacity
,current_date as snapshotdate
from tmp_jda_cu_resstatic rs;
