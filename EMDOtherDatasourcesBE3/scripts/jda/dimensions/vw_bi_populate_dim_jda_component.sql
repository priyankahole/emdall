/* dim_jda_product */

insert into dim_jda_component (dim_jda_componentid,
                               component_id)
select 1, -99999999
from (select 1) a
where not exists ( select 'x' from dim_jda_component where dim_jda_componentid = 1); 

drop table if exists tmp_dw_component;
create table tmp_dw_component as
select nwmgr_component_component_id, nwmgr_component_enterprise_id, 
	   nwmgr_component_name, nwmgr_component_description
from NWMGR_COMPONENT np;

update dim_jda_component djp
set djp.component_name = ifnull(t.nwmgr_component_name, 'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_component djp,tmp_dw_component t
where     djp.component_id = t.nwmgr_component_component_id
	  and djp.component_name <> ifnull(t.nwmgr_component_name, 'Not Set');	

update dim_jda_component djp
set djp.component_description = ifnull(t.nwmgr_component_description, 'Not Set'),
    dw_update_date = current_timestamp
from dim_jda_component djp,tmp_dw_component t
where     djp.component_id = t.nwmgr_component_component_id
	  and djp.component_description <> ifnull(t.nwmgr_component_description, 'Not Set');	

delete from number_fountain m where m.table_name = 'dim_jda_component';
insert into number_fountain
select 'dim_jda_component',
            ifnull(max(d.dim_jda_componentid ), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_component d
where d.dim_jda_componentid <> 1;

insert into dim_jda_component(
					 dim_jda_componentid	
					,component_id								 
					,component_name							 
					,component_description)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_component') + row_number() over(order by '') AS dim_jda_componentid
		,nwmgr_component_component_id								 
		,ifnull(nwmgr_component_name,'Not Set')		    as component_name						 
		,ifnull(nwmgr_component_description,'Not Set')	as component_description	 
from tmp_dw_component ds
where not exists (select 'x' from dim_jda_component dc where dc.component_id = ds.nwmgr_component_component_id);

/* insert 2 component types for Forecast History: LAG0 and LAG1 */
insert into dim_jda_component(
					 dim_jda_componentid	
					,component_id								 
					,component_name							 
					,component_description
					,c_type
					,c_type2
					,c_type3)	
select dim_jda_componentid, component_id, component_name, component_description, c_type, c_type2, c_type3
from (select -99999999 as dim_jda_componentid, /* keep this ID fixed, and map FCST_LAG0 to it in fact processing */
		     -99999998 as component_id, 
		     'History-Lag0' as component_name, 
		     'History-Lag0' as component_description, 
		     'FCST_LAG0' as c_type, 
		     'Not Set'   as c_type2, 
		     'Not Set'   as c_type3) t
where not exists (select 'X' from dim_jda_component t1 where t1.dim_jda_componentid = t.dim_jda_componentid);

insert into dim_jda_component(
					 dim_jda_componentid	
					,component_id								 
					,component_name							 
					,component_description
					,c_type
					,c_type2
					,c_type3)	
select dim_jda_componentid, component_id, component_name, component_description, c_type, c_type2, c_type3
from (select -99999997 as dim_jda_componentid, /* keep this ID fixed, and map FCST_LAG0 to it in fact processing */
		     -99999996 as component_id, 
		     'History-Lag1' as component_name, 
		     'History-Lag1' as component_description, 
		     'FCST_LAG1' as c_type, 
		     'Not Set'   as c_type2, 
		     'Not Set'   as c_type3) t
where not exists (select 'X' from dim_jda_component t1 where t1.dim_jda_componentid = t.dim_jda_componentid);	

/* Build the types */
update dim_jda_component dst
set c_type = 'MKTFCST',
    dw_update_date = current_timestamp
where substr(dst.component_description, 1, 6) IN ('MKTFCS');

update dim_jda_component dst
set c_type = 'TENDER',
    dw_update_date = current_timestamp
where substr(dst.component_description, 1, 6) IN ('TENDER');
	/* it overwrites the TENDER
	update dim_jda_component dst
	set c_type = 'Tender Forecast'
	where dst.component_name = 'Tender Forecast' */

update dim_jda_component dst
set c_type = 'ADJHIST',
    dw_update_date = current_timestamp
where dst.component_name = 'Adj Hist';

update dim_jda_component dst
set c_type = 'LRP',
    dw_update_date = current_timestamp
where dst.component_name = 'LRP Demand';

update dim_jda_component dst
set c_type = 'OPERATING PLAN',
    dw_update_date = current_timestamp
where dst.component_description like 'OPQ%';

update dim_jda_component dst
set c_type = 'OPERATING PLAN EURO',
    dw_update_date = current_timestamp
where dst.component_description like 'OPVGC%';

update dim_jda_component dst
set c_type = 'SPP',
    dw_update_date = current_timestamp
where dst.component_description like 'SPPQ%';

update dim_jda_component dst
set c_type = 'STATFCST',
    dw_update_date = current_timestamp
where dst.component_name = 'Stat Forecast';

update dim_jda_component dst
set c_type = 'STAT231',
    dw_update_date = current_timestamp
where dst.component_description like 'STAT231%';

update dim_jda_component dst
set c_type = 'SOP FRAMING',
    dw_update_date = current_timestamp
where dst.component_description like 'SOP%';

update dim_jda_component dst
set c_type = 'OPERATING PLAN LC',
    c_type2 = 'OPERATING PLAN LC',
	c_type3 = 'OP LC',
    dw_update_date = current_timestamp
where dst.component_name = 'Operating Plan (in Local Currency)';


/* F.O.C Hist / F.O.C. Fcst */

update dim_jda_component dst
set c_type = 'F.O.C Hist / F.O.C. Fcst',
    dw_update_date = current_timestamp
where dst.component_name = 'F.O.C Hist / F.O.C. Fcst';


/* F1 */
	update dim_jda_component dst
	set c_type = 'F1 Euro',
		c_type2 = 'F1 Euro',
		c_type3 = 'F1 Euro',
    dw_update_date = current_timestamp
	where dst.component_name = 'F1 (in Euro)';

	update dim_jda_component dst
	set c_type = 'F1 LC',
		c_type2 = 'F1 LC',
		c_type3 = 'F1 LC',
    dw_update_date = current_timestamp
	where dst.component_name = 'F1 (in LC)';

	update dim_jda_component dst
	set c_type = 'F1 QTY',
		c_type2 = 'F1 QTY',
		c_type3 = 'F1 QTY',
    dw_update_date = current_timestamp
	where dst.component_name = 'F1 (in Qty)';

/* F2 */
	update dim_jda_component dst
	set c_type = 'F2 Euro',
		c_type2 = 'F2 Euro',
		c_type3 = 'F2 Euro',
    dw_update_date = current_timestamp
	where dst.component_name = 'F2 (in Euro)';

	update dim_jda_component dst
	set c_type = 'F2 LC',
		c_type2 = 'F2 LC',
		c_type3 = 'F2 LC',
    dw_update_date = current_timestamp
	where dst.component_name = 'F2 (in LC)';

	update dim_jda_component dst
	set c_type = 'F2 QTY',
		c_type2 = 'F2 QTY',
		c_type3 = 'F2 QTY',
    dw_update_date = current_timestamp
	where dst.component_name = 'F2 (in Qty)';	

/* F3 */
	update dim_jda_component dst
	set c_type = 'F3 Euro',
		c_type2 = 'F3 Euro',
		c_type3 = 'F3 Euro',
    dw_update_date = current_timestamp
	where dst.component_name = 'F3 (in Euro)';

	update dim_jda_component dst
	set c_type = 'F3 LC',
		c_type2 = 'F3 LC',
		c_type3 = 'F3 LC',
    dw_update_date = current_timestamp
	where dst.component_name = 'F3 (in LC)';

	update dim_jda_component dst
	set c_type = 'F3 QTY',
		c_type2 = 'F3 QTY',
		c_type3 = 'F3 QTY',
    dw_update_date = current_timestamp
	where dst.component_name = 'F3 (in Qty)';

/* Actual Values */
	update dim_jda_component dst
	set c_type = 'Actual Values Euro',
		c_type2 = 'Actual Values Euro',
		c_type3 = 'AV Euro',
    dw_update_date = current_timestamp
	where dst.component_name = 'Actual Values (in Euro)';

	update dim_jda_component dst
	set c_type = 'Actual Values LC',
		c_type2 = 'Actual Values LC',
		c_type3 = 'AV LC',
    dw_update_date = current_timestamp
	where dst.component_name = 'Actual Values (in LC)';

/* Price */	
	update dim_jda_component dst
	set c_type = 'Price LC',
		c_type2 = 'Price LC',
		c_type3 = 'Price LC',
    dw_update_date = current_timestamp
	where dst.component_name = 'Price (in LC)';

	update dim_jda_component dst
	set c_type = 'Price Tender LC',
		c_type2 = 'Price Tender LC',
		c_type3 = 'Price Tender LC',
    dw_update_date = current_timestamp
	where dst.component_name = 'Price Tender (in LC)';	
	
	update dim_jda_component dst
	set c_type = 'CurrRate',
		c_type2 = 'CurrRate',
		c_type3 = 'CurrRate',
    dw_update_date = current_timestamp
	where dst.component_name = 'Currency Rate (LC>Euro)';	
	
/* TYPE_2 */
update dim_jda_component t
set c_type2 = case when c_type = 'ADJHIST' OR c_type = 'MKTFCST' OR c_type = 'TENDER'
						then 'FCST'
					 else c_type end,
    dw_update_date = current_timestamp
where c_type2 <> case when c_type = 'ADJHIST' OR c_type = 'MKTFCST' OR c_type = 'TENDER'
						  then 'FCST'
						else c_type end;

/* TYPE_3 */
update dim_jda_component t
set c_type3 = case when c_type = 'ADJHIST' OR c_type = 'FCST_LAG0' OR c_type = 'FCST_LAG1'
						then 'FCST'
					 when c_type = 'OPERATING PLAN'
						then 'OP'
					 when c_type = 'OPERATING PLAN EURO'
						then 'OP EURO'
					 else c_type3 end,
    dw_update_date = current_timestamp
where c_type3 <> case when c_type = 'ADJHIST' OR c_type = 'FCST_LAG0' OR c_type = 'FCST_LAG1'
						then 'FCST'
					 when c_type = 'OPERATING PLAN'
						then 'OP'
					 when c_type = 'OPERATING PLAN EURO'
						then 'OP EURO'
					 else c_type3 end;


/* Oana V - 11 July 2016 - JDA Demand for IBP */
update dim_jda_component dst 
    set c_type = 'F.O.C. Hist' , 
    dw_update_date = current_timestamp 
where dst.component_name = 'F.O.C. Hist';

update dim_jda_component dst 
    set c_type = 'Actual Hist' , 
    dw_update_date = current_timestamp 
where dst.component_name = 'Actual Hist';

update dim_jda_component dst 
    set c_type = 'Demand Adj/Supply Risk' , 
    dw_update_date = current_timestamp 
where dst.component_name = 'Demand Adjustment / Supply Risk';

update dim_jda_component dst 
    set c_type = 'Demand Adj/Business Risk & Opportunities' , 
    dw_update_date = current_timestamp 
where dst.component_name = 'Demand Adjustment / Business Risk & Opportunities';

update dim_jda_component dst 
    set c_type = 'Tender Demand Adj/Supply Risk' , 
    dw_update_date = current_timestamp 
where dst.component_name = 'Tender Demand Adjustment / Supply Risk';

update dim_jda_component dst 
    set c_type = 'Tender Demand Adj/Business Risk & Opportunities' , 
    dw_update_date = current_timestamp 
where dst.component_name = 'Tender Demand Adjustment / Business Risk & Opportunities';

update dim_jda_component dst 
    set c_type = 'Consensus Forecast LC' , 
    dw_update_date = current_timestamp 
where dst.component_name = 'Consensus Forecast (in LC)';

update dim_jda_component dst 
    set c_type = 'Consensus Forecast_V QTY' , 
    dw_update_date = current_timestamp 
where dst.component_name = 'Consensus Forecast (in Qty)_V';
/* END Oana V - 11 July 2016 - JDA Demand for IBP */

update dim_jda_component dst
    set c_type = 'Late Sales Recognition (in LC)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'Late Sales Recognition (in LC)';

update dim_jda_component dst
    set c_type = 'Commission Income (in LC)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'Commission Income (in LC)';

update dim_jda_component dst
    set c_type = 'Business Risk Late Sales Reco (in LC)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'Business Risk Late Sales Reco (in LC)';

update dim_jda_component dst
    set c_type = 'Supply Risk Global from IBP (in LC)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'Supply Risk Global from IBP (in LC)';

update dim_jda_component dst
    set c_type = 'Supply Risk Global from IBP (in Qty)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'Supply Risk Global from IBP (in Qty)';

update dim_jda_component
  set component_name_tender_fcst = case when trim(COMPONENT_NAME) in ('Actual Hist / Market Fcst', 'Tender Forecast') then 'FCST + Tender' else COMPONENT_NAME end;

update dim_jda_component dst
    set c_type = 'F1 Consensus Forecast Frozen (in Qty)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'F1 Consensus Forecast Frozen (in Qty)';

update dim_jda_component dst
    set c_type = 'F1 Consensus Forecast Frozen (in LC)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'F1 Consensus Forecast Frozen (in LC)';

update dim_jda_component dst
    set c_type = 'F2 Consensus Forecast Frozen (in LC)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'F2 Consensus Forecast Frozen (in LC)';

update dim_jda_component dst
    set c_type = 'F3 Consensus Forecast Frozen (in LC)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'F3 Consensus Forecast Frozen (in LC)';

update dim_jda_component dst
    set c_type = 'F2 Consensus Forecast Frozen (in Qty)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'F2 Consensus Forecast Frozen (in Qty)';

update dim_jda_component dst
    set c_type = 'F3 Consensus Forecast Frozen (in Qty)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'F3 Consensus Forecast Frozen (in Qty)';

update dim_jda_component dst
    set c_type = 'OP Consensus Forecast Frozen (in Qty)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'OP Consensus Forecast Frozen (in Qty)';

update dim_jda_component dst
    set c_type = 'OP Consensus Forecast Frozen (in LC)' ,
    dw_update_date = current_timestamp
where dst.component_name = 'OP Consensus Forecast Frozen (in LC)';
