/* Capacity Utilization - Packaging Sites */
insert into dim_jda_packsite(dim_jda_packsiteid,packsite,res_udc_grp1,dw_insert_date,dw_update_date,projectsourceid)
select 1, 'Not Set', 'Not Set', current_timestamp, current_timestamp, 1
from (select 1) where not exists (select 'x' from dim_jda_packsite where dim_jda_packsiteid = 1);

drop table if exists tmp_jda_packsite;
create table tmp_jda_packsite as
  select distinct stsc_loc_descr as descr
	,stsc_res_udc_grp1 as res_udc_grp1
  ,stsc_res_res as resource
from stsc_res res, stsc_loc loc
where stsc_res_loc = stsc_loc_loc
and stsc_res_udc_grp1 = 'PACKING';

delete from number_fountain m where m.table_name = 'dim_jda_packsite';
insert into number_fountain
select 'dim_jda_packsite'
        ,ifnull(max(d.dim_jda_packsiteid)
        ,ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_packsite d
where dim_jda_packsiteid <> 1;

insert into dim_jda_packsite (dim_jda_packsiteID,packsite,res_udc_grp1,resource)
select (select m.max_id from number_fountain m where m.table_name = 'dim_jda_packsite')+ row_number() over(order by '') dim_jda_packsiteid
	,ifnull(descr,'Not Set') as packsite
	,ifnull(t.res_udc_grp1,'Not Set') as res_udc_grp1
  ,t.resource
from tmp_jda_packsite t
where not exists (select 'x' from dim_jda_packsite dp where dp.resource = t.resource);

update dim_jda_packsite dp
  set dp.packsite = ifnull(descr,'Not Set')
from dim_jda_packsite dp, tmp_jda_packsite t
where dp.resource = t.resource
  and dp.packsite <> ifnull(descr,'Not Set');
