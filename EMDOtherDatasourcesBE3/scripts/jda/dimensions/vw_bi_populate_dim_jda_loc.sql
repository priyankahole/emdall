/* dim_jda_loc */
insert into dim_jda_loc(DIM_JDA_LOCID,LOC,LOCDESCR,COUNTRY,MARKET,REGION,GAUSS_COUNTRY,LOC_TYPE,CMG_ID,DW_INSERT_DATE,DW_UPDATE_DATE)
	select 1,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',0,'Not Set',current_timestamp,current_timestamp
	from (select 1) a
	where not exists(select 'x' from dim_jda_loc where dim_jda_locid = 1);

delete from number_fountain m where m.table_name = 'dim_jda_loc';
insert into number_fountain
select 'dim_jda_loc',
            ifnull(max(d.dim_jda_locid),
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_loc d
where d.dim_jda_locid <> 1;

drop table if exists tmp_jda_loc;
create table tmp_jda_loc as
	select sl.stsc_loc_loc					 as loc
		,sl.stsc_loc_descr					 as locdescr
		,sl.stsc_loc_udc_country 			 as country
		,sl.stsc_loc_udc_market				 as market
		,sl.stsc_loc_udc_region				 as region
		,sl.stsc_loc_udc_gauss_rep_region	 as gauss_country
		,sl.stsc_loc_loc_type				 as loc_type
		,sl.stsc_loc_udc_cmg_id				 as cmg_id
		,sl.stsc_loc_udc_isocode	as isocode
	from stsc_loc sl;

update dim_jda_loc d
	set d.locdescr = ifnull(t.locdescr,'Not Set'),
		d.DW_UPDATE_DATE = current_timestamp
from dim_jda_loc d, tmp_jda_loc t
where d.loc = t.loc
	and d.locdescr <> ifnull(t.locdescr,'Not Set');

update dim_jda_loc d
	set d.country = ifnull(t.country,'Not Set'),
		d.DW_UPDATE_DATE = current_timestamp
from dim_jda_loc d, tmp_jda_loc t
where d.loc = t.loc
	and d.country <> ifnull(t.country,'Not Set');

update dim_jda_loc d
	set d.market = ifnull(t.market,'Not Set'),
		d.DW_UPDATE_DATE = current_timestamp
from dim_jda_loc d, tmp_jda_loc t
where d.loc = t.loc
	and d.market <> ifnull(t.market,'Not Set');

update dim_jda_loc d
	set d.region = ifnull(t.region,'Not Set'),
		d.DW_UPDATE_DATE = current_timestamp
from dim_jda_loc d, tmp_jda_loc t
where d.loc = t.loc
	and d.region <> ifnull(t.region,'Not Set');

update dim_jda_loc d
	set d.gauss_country = ifnull(t.gauss_country,'Not Set'),
		d.DW_UPDATE_DATE = current_timestamp
from dim_jda_loc d, tmp_jda_loc t
where d.loc = t.loc
	and d.gauss_country <> ifnull(t.gauss_country,'Not Set');

update dim_jda_loc d
	set d.cmg_id = ifnull(t.cmg_id,'Not Set'),
		d.DW_UPDATE_DATE = current_timestamp
from dim_jda_loc d, tmp_jda_loc t
where d.loc = t.loc
	and d.cmg_id <> ifnull(t.cmg_id,'Not Set');

update dim_jda_loc d
	set d.isocode = ifnull(t.isocode,'Not Set'),
		d.DW_UPDATE_DATE = current_timestamp
from dim_jda_loc d, tmp_jda_loc t
where d.loc = t.loc
	and d.isocode <> ifnull(t.isocode,'Not Set');

insert into dim_jda_loc(DIM_JDA_LOCID,LOC,LOCDESCR,COUNTRY,MARKET,REGION,GAUSS_COUNTRY,LOC_TYPE,CMG_ID,ISOCODE)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_loc') + row_number()  over(order by '') AS dim_jda_locid
		,t.loc 							as loc
		,ifnull(t.locdescr,'Not Set') 	as locdescr
		,ifnull(t.country,'Not Set')	as country
		,ifnull(t.market,'Not Set')		as market
		,ifnull(t.region,'Not Set')		as region
		,ifnull(t.locdescr,'Not Set')	as gauss_country
		,ifnull(t.loc_type,0)			as loc_type
		,ifnull(t.cmg_id,'Not Set')		as cmg_id
		,ifnull(t.isocode,'Not Set') as isocode
	from tmp_jda_loc t
		where not exists (select 'x' from dim_jda_loc d where d.loc = t.loc)
		and t.loc is not null;

drop table if exists tmp_jda_loc;

update dim_jda_loc
set locsort = case when region = 'Manuf Site' then 'A' else region end;
