/* dim_jda_skusafetystockparam */
delete from number_fountain m where m.table_name = 'dim_jda_skusafetystockparam';
insert into number_fountain
select 'dim_jda_skusafetystockparam',
            ifnull(max(d.dim_jda_skusafetystockparamid), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_skusafetystockparam d
where d.dim_jda_skusafetystockparamid <> 1;

drop table if exists tmp_skusafetystockparam;
create table tmp_skusafetystockparam as
	select sssp.item
		,sssp.loc
		,sssp.maxss
		,sssp.minss
		,ifnull(sssp.sscov,0) / 1440  as sscov
		,sssp.ssrule
		,sssp.udc_ss_optimum 		as ss_optimum
		,sssp.udc_ss_optimum_qty	as ss_optimum_qty
		,sssp.udc_ss_strategy		as ss_strategy
	from stsc_skusafetystockparam sssp;

update dim_jda_skusafetystockparam dsssp
	set dsssp.MAXSS = ifnull(tsssp.MAXSS,999999999),
		dsssp.dw_update_date = current_timestamp
from dim_jda_skusafetystockparam dsssp,tmp_skusafetystockparam tsssp
where dsssp.item = tsssp.item
	and dsssp.loc = tsssp.loc;

update dim_jda_skusafetystockparam dsssp
	set dsssp.MINSS = ifnull(tsssp.MINSS,0),
		dsssp.dw_update_date = current_timestamp
from dim_jda_skusafetystockparam dsssp,tmp_skusafetystockparam tsssp
where dsssp.item = tsssp.item
	and dsssp.loc = tsssp.loc;

update dim_jda_skusafetystockparam dsssp
	set dsssp.SSCOV = ifnull(tsssp.SSCOV,0),
		dsssp.dw_update_date = current_timestamp
from dim_jda_skusafetystockparam dsssp,tmp_skusafetystockparam tsssp
where dsssp.item = tsssp.item
	and dsssp.loc = tsssp.loc;

update dim_jda_skusafetystockparam dsssp
	set dsssp.SSRULE = ifnull(tsssp.SSRULE,1),
		dsssp.dw_update_date = current_timestamp
from dim_jda_skusafetystockparam dsssp,tmp_skusafetystockparam tsssp
where dsssp.item = tsssp.item
	and dsssp.loc = tsssp.loc;

update dim_jda_skusafetystockparam dsssp
	set dsssp.SS_OPTIMUM = ifnull(tsssp.SS_OPTIMUM,0),
		dsssp.dw_update_date = current_timestamp
from dim_jda_skusafetystockparam dsssp,tmp_skusafetystockparam tsssp
where dsssp.item = tsssp.item
	and dsssp.loc = tsssp.loc;

update dim_jda_skusafetystockparam dsssp
	set dsssp.SS_OPTIMUM_QTY = ifnull(tsssp.SS_OPTIMUM_QTY,0),
		dsssp.dw_update_date = current_timestamp
from dim_jda_skusafetystockparam dsssp,tmp_skusafetystockparam tsssp
where dsssp.item = tsssp.item
	and dsssp.loc = tsssp.loc;

update dim_jda_skusafetystockparam dsssp
	set dsssp.SS_STRATEGY = ifnull(tsssp.SS_STRATEGY,'Not Set'),
		dsssp.dw_update_date = current_timestamp
from dim_jda_skusafetystockparam dsssp,tmp_skusafetystockparam tsssp
where dsssp.item = tsssp.item
	and dsssp.loc = tsssp.loc;

insert into dim_jda_skusafetystockparam (DIM_JDA_SKUSAFETYSTOCKPARAMID,ITEM,LOC,MAXSS,MINSS,SSCOV,SSRULE,SS_OPTIMUM,SS_OPTIMUM_QTY,SS_STRATEGY,DW_INSERT_DATE,DW_UPDATE_DATE)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_jda_skusafetystockparam') + row_number()  over(order by '') as dim_jda_skusafetystockparamid
		,sssp.item
		,sssp.loc
		,ifnull(sssp.maxss,999999999) 	as maxss
		,ifnull(sssp.minss,0)			as minss
		,ifnull(sssp.sscov,0)			as sscov
		,ifnull(sssp.ssrule,1)			as ssrule
		,ifnull(sssp.ss_optimum,0)		as ss_optimum
		,ifnull(sssp.ss_optimum_qty,0)	as ss_optimum_qty
		,ifnull(sssp.ss_strategy,'Not Set')		as ss_strategy
		,current_timestamp				as dw_insert_date
		,current_timestamp				as dw_update_date
	from tmp_skusafetystockparam sssp
	where not exists (select 'x' from dim_jda_skusafetystockparam dsssp where dsssp.item = sssp.item and dsssp.loc = sssp.loc)
		and sssp.item is not null;

drop table if exists tmp_skusafetystockparam;


