/* JDA Resource */
insert into dim_jda_resource(DIM_JDA_RESOURCEID,RESOURCE,RESOURCE_DESCR,LOC,UDC_GRP1,dw_insert_date, dw_update_Date, projectsourceid)
select 1,'Not Set','Not Set','Not Set','Not Set',current_timestamp, current_timestamp,1
from (select 1) where not exists (select 'x' from dim_jda_resource  where dim_jda_resourceid = 1);

drop table if exists tmp_jda_resource;
create table tmp_jda_resource as
  select STSC_RES_RES res
    ,STSC_RES_DESCR res_descr
    ,STSC_RES_LOC loc
    ,STSC_RES_UDC_GRP1 udc_grp1
  from stsc_res;

delete from number_fountain m where m.table_name = 'dim_jda_resource';
insert into number_fountain
select 'dim_jda_resource'
        ,ifnull(max(d.dim_jda_resourceid)
        ,ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_jda_resource d
where d.dim_jda_resourceid <> 1;

insert into dim_jda_resource(DIM_JDA_RESOURCEID,RESOURCE,RESOURCE_DESCR,LOC,UDC_GRP1)
  select (select m.max_id from number_fountain m where m.table_name = 'dim_jda_resource') + row_number() over(order by '') as dim_jda_resourceid
    ,ifnull(res,'Not Set') as resource
    ,ifnull(res_descr,'Not Set') as resource_descr
    ,ifnull(loc,'Not Set') as loc
    ,ifnull(udc_grp1,'Not Set') as udc_grp1
  from tmp_jda_resource t
  where not exists (select 'x' from dim_jda_resource djr where djr.resource = t.res);

update  dim_jda_resource djr
  set djr.resource_descr = ifnull(t.res_descr,'Not Set')
from dim_jda_resource djr , tmp_jda_resource t
where djr.resource = t.res
  and djr.resource_descr <> ifnull(t.res_descr,'Not Set');

update  dim_jda_resource djr
  set djr.loc = ifnull(t.loc,'Not Set')
from dim_jda_resource djr , tmp_jda_resource t
where djr.resource = t.res
  and djr.loc <> ifnull(t.loc,'Not Set');

update  dim_jda_resource djr
  set djr.udc_grp1 = ifnull(t.udc_grp1,'Not Set')
from dim_jda_resource djr , tmp_jda_resource t
where djr.resource = t.res
  and djr.udc_grp1 <> ifnull(t.udc_grp1,'Not Set');
