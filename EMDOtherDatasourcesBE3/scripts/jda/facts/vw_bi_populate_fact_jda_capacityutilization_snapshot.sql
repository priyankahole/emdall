delete from number_fountain m where m.table_name = 'fact_jda_capacityutilization_snapshot';
insert into number_fountain
select 'fact_jda_capacityutilization_snapshot', ifnull(max(f.fact_jda_capacityutilization_snapshotid ),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from FACT_JDA_CAPACITYUTILIZATION_SNAPSHOT f;


insert into fact_jda_capacityutilization_snapshot(FACT_JDA_CAPACITYUTILIZATION_SNAPSHOTID,DIM_JDA_ITEMID,DIM_JDA_LOCATIONID,DIM_JDA_RESOURCEID
	,DIM_JDA_PACKSITEID,DIM_JDA_MANUFSITEID,DIM_DATEID,
	CT_CAPACITY,CT_TOTALLOAD,CT_LOAD,DW_INSERT_DATE,DW_UPDATE_DATE,DIM_PROJECTSOURCEID,DD_GROUPSITE,CT_COUNT_ROW,
	CT_COUNT_KEY,DIM_JDA_RESSTATICID,CT_SUPPLYQTY,DD_SUPPLYSEQNUM,DIM_SNAPSHOTDATEID)
select (select max_id from number_fountain where table_name = 'fact_jda_capacityutilization_snapshot') + row_number() over(order by'') as FACT_JDA_CAPACITYUTILIZATION_SNAPSHOTID
	,ifnull(f.DIM_JDA_ITEMID,1) as DIM_JDA_ITEMID
	,ifnull(f.DIM_JDA_LOCATIONID,1) as DIM_JDA_LOCATIONID
	,ifnull(f.DIM_JDA_RESOURCEID,1) as DIM_JDA_RESOURCEID
	,ifnull(f.DIM_JDA_PACKSITEID,1) as DIM_JDA_PACKSITEID
	,ifnull(f.DIM_JDA_MANUFSITEID,1) as DIM_JDA_MANUFSITEID
	,ifnull(f.DIM_DATEID,1) as DIM_DATEID
	,ifnull(f.CT_CAPACITY,0) as CT_CAPACITY
	,ifnull(f.CT_TOTALLOAD,0) as CT_TOTALLOAD
	,ifnull(f.CT_LOAD,0) as CT_LOAD
	,ifnull(f.DW_INSERT_DATE,current_timestamp) as DW_INSERT_DATE
	,ifnull(f.DW_UPDATE_DATE,current_timestamp) as DW_UPDATE_DATE
	,ifnull(f.DIM_PROJECTSOURCEID,1) as DIM_PROJECTSOURCEID
	,ifnull(f.DD_GROUPSITE,'Not Set') as DD_GROUPSITE
	,ifnull(f.CT_COUNT_ROW,0) as CT_COUNT_ROW
	,ifnull(f.CT_COUNT_KEY,0) as CT_COUNT_KEY
	,ifnull(f.DIM_JDA_RESSTATICID,1) as DIM_JDA_RESSTATICID
	,ifnull(f.CT_SUPPLYQTY,0) as CT_SUPPLYQTY
	,ifnull(f.DD_SUPPLYSEQNUM,0) as DD_SUPPLYSEQNUM
	,ifnull((select dim_dateid from dim_date where datevalue = current_date),1) as DIM_SNAPSHOTDATEID
from fact_jda_capacityutilization f;
