
DELETE FROM number_fountain m 
WHERE  m.table_name = 'fact_jda_demandforecastsnapshot'; 

INSERT INTO number_fountain 
SELECT 'fact_jda_demandforecastsnapshot', 
       IFNULL(Max(f.fact_jda_demandforecastsnapshotid), IFNULL( 
       (SELECT s.dim_projectsourceid * 
               s.multiplier 
        FROM   dim_projectsource s), 1) 
       ) 
FROM  fact_jda_demandforecastsnapshot f; 

INSERT INTO FACT_JDA_DEMANDFORECASTSNAPSHOT 
(
	FACT_JDA_DEMANDFORECASTSNAPSHOTID,
	DIM_DATEIDSTARTDATE,
	DD_PLANNING_ITEM_ID,
	DIM_JDA_LOCATIONID,
	DIM_JDA_PRODUCTID,
	DD_TYPE,
	DD_ABC_CLASS,
	DD_LOCALBRAND,
	DD_ITEMLOCALCODE,
	DD_MODEL,
	DD_FCSTLEVEL,
	DD_PLANNIG_ITEM_FLAG,
	CT_PACKS,
	CT_UNITS,
	CT_EQ_UNIT,
	CT_MCG,
	CT_IU,
	DD_TYPE_2,
	DD_TYPE_3,
	CT_PACKS_COR,
	CT_UNITS_COR,
	CT_LAG0_UNITS_COR,
	CT_LAG0_PACKS_COR,
	CT_LAG1_PACKS_COR,
	CT_LAG1_UNITS_COR,
	CT_LAG0_EQ_UNIT,
	CT_LAG1_EQ_UNIT,
	DIM_PROJECTSOURCEID,
	DW_INSERT_DATE,
	DW_UPDATE_DATE,
	DD_DFULIFECYCLE,
	DD_GAUSSITEMCODE,
	CT_AVGSALESPMONTH,
	CT_AVGFCST,
	CT_OP_EURO,
	DD_DFUCLASS,
	DIM_JDA_COMPONENTID,
	DIM_JDA_DATEHOLDERID,
	CT_PRICE,
	CT_CURRRATE,
	CT_AVG3M_BIAS1M,
	CT_AVG6M_BIAS1M,
	CT_AVG_MKTSFA_12M,
	CT_AVG_STATSFA_12M,
	CT_STATSFA,
	CT_MKTSFA,
	DD_SYSTBIAS3M,
	DD_SYSTBIAS6M,
	CT_VOLATILITY,
	DD_SEGMENTATION,
	DD_LASTSATURDAYCPM,
	DD_NDLASTSATURDAYCPM,
	DD_CURRENTDATE,
	DIM_DATEHOLDERID,
	DIM_LAG0_MONTHID,
	DIM_LAG1_MONTHID,
	DIM_NDLASTSATURDAYCPMID,
	DIM_LASTSATURDAYCPMID,
	CT_PRODLOCATION,
	CT_UDA_EXCHRATE,
	CT_PRICE_EURO_AVG_QUARTER,
	CT_PRICE_EURO_AVG_YEAR,
	AMT_PVE_PRICE_EURO_Q_PREV_YEAR,
	AMT_PVE_PRICE_EURO_YTD_PREV_YEAR,
	AMT_COGSFIXEDPLANRATE_EMD,
	DD_CMG_ID,
	AMT_JDACOGS,
	DD_COGSSOURCE,
	AMT_JDACOGS_OP,
	DD_COGSSOURCE_OP,
	DD_WCOGSPCTG,
	CT_UNITS_COR_YESTERDAY,
	DIM_DATEIDSNAPSHOT,
	DIM_MDG_PARTID
)
SELECT
	(select max_id from number_fountain where table_name = 'fact_jda_demandforecastsnapshot') + row_number() over(order by '') as FACT_JDA_DEMANDFORECASTSNAPSHOTID,
	ifnull(f.DIM_DATEIDSTARTDATE,1) as DIM_DATEIDSTARTDATE,
	ifnull(f.DD_PLANNING_ITEM_ID,0) DD_PLANNING_ITEM_ID,
	ifnull(f.DIM_JDA_LOCATIONID,1) DIM_JDA_LOCATIONID,
	ifnull(f.DIM_JDA_PRODUCTID,1) DIM_JDA_PRODUCTID,
	ifnull(f.DD_TYPE,'Not Set') DD_TYPE,
	ifnull(f.DD_ABC_CLASS,'Not Set') DD_ABC_CLASS,
	ifnull(f.DD_LOCALBRAND,'Not Set') DD_LOCALBRAND,
	ifnull(f.DD_ITEMLOCALCODE,'Not Set') DD_ITEMLOCALCODE,
	ifnull(f.DD_MODEL,'Not Set') DD_MODEL,
	ifnull(f.DD_FCSTLEVEL,'Not Set') DD_FCSTLEVEL,
	ifnull(f.DD_PLANNIG_ITEM_FLAG,'Not Set') DD_PLANNIG_ITEM_FLAG,
	ifnull(f.CT_PACKS,0) CT_PACKS,
	ifnull(f.CT_UNITS,0) CT_UNITS,
	ifnull(f.CT_EQ_UNIT,0) CT_EQ_UNIT,
	ifnull(f.CT_MCG,0) CT_MCG,
	ifnull(f.CT_IU,0) CT_IU,
	ifnull(f.DD_TYPE_2,'Not Set') DD_TYPE_2,
	ifnull(f.DD_TYPE_3,'Not Set') DD_TYPE_3,
	ifnull(f.CT_PACKS_COR,0) CT_PACKS_COR,
	ifnull(f.CT_UNITS_COR,0) CT_UNITS_COR,
	ifnull(f.CT_LAG0_UNITS_COR,0) CT_LAG0_UNITS_COR,
	ifnull(f.CT_LAG0_PACKS_COR,0) CT_LAG0_PACKS_COR,
	ifnull(f.CT_LAG1_PACKS_COR,0) CT_LAG1_PACKS_COR,
	ifnull(f.CT_LAG1_UNITS_COR,0) CT_LAG1_UNITS_COR,
	ifnull(f.CT_LAG0_EQ_UNIT,0) CT_LAG0_EQ_UNIT,
	ifnull(f.CT_LAG1_EQ_UNIT,0) CT_LAG1_EQ_UNIT,
	ifnull(f.DIM_PROJECTSOURCEID,1) DIM_PROJECTSOURCEID,
	ifnull(f.DW_INSERT_DATE,current_timestamp) DW_INSERT_DATE,
	ifnull(f.DW_UPDATE_DATE,current_timestamp) DW_UPDATE_DATE,
	ifnull(f.DD_DFULIFECYCLE,'Not Set') DD_DFULIFECYCLE,
	ifnull(f.DD_GAUSSITEMCODE,'Not Set') DD_GAUSSITEMCODE,
	ifnull(f.CT_AVGSALESPMONTH,0) CT_AVGSALESPMONTH,
	ifnull(f.CT_AVGFCST,0) CT_AVGFCST,
	ifnull(f.CT_OP_EURO,0) CT_OP_EURO,
	ifnull(f.DD_DFUCLASS,'Not Set') DD_DFUCLASS,
	ifnull(f.DIM_JDA_COMPONENTID,1) DIM_JDA_COMPONENTID,
	ifnull(f.DIM_JDA_DATEHOLDERID,1) DIM_JDA_DATEHOLDERID,
	ifnull(f.CT_PRICE,0) CT_PRICE,
	ifnull(f.CT_CURRRATE,0) CT_CURRRATE,
	ifnull(f.CT_AVG3M_BIAS1M,0) CT_AVG3M_BIAS1M,
	ifnull(f.CT_AVG6M_BIAS1M,0) CT_AVG6M_BIAS1M,
	ifnull(f.CT_AVG_MKTSFA_12M,0) CT_AVG_MKTSFA_12M,
	ifnull(f.CT_AVG_STATSFA_12M,0) CT_AVG_STATSFA_12M,
	ifnull(f.CT_STATSFA,0) CT_STATSFA,
	ifnull(f.CT_MKTSFA,0) CT_MKTSFA,
	ifnull(f.DD_SYSTBIAS3M,'Not Set') DD_SYSTBIAS3M,
	ifnull(f.DD_SYSTBIAS6M,'Not Set') DD_SYSTBIAS6M,
	ifnull(f.CT_VOLATILITY,0) CT_VOLATILITY,
	ifnull(f.DD_SEGMENTATION,'Not Set') DD_SEGMENTATION,
	ifnull(f.DD_LASTSATURDAYCPM,current_date) DD_LASTSATURDAYCPM,
	ifnull(f.DD_NDLASTSATURDAYCPM,current_date) DD_NDLASTSATURDAYCPM,
	ifnull(f.DD_CURRENTDATE,current_date) DD_CURRENTDATE,
	ifnull(f.DIM_DATEHOLDERID,1) DIM_DATEHOLDERID,
	ifnull(f.DIM_LAG0_MONTHID,1) DIM_LAG0_MONTHID,
	ifnull(f.DIM_LAG1_MONTHID,1) DIM_LAG1_MONTHID,
	ifnull(f.DIM_NDLASTSATURDAYCPMID,1) DIM_NDLASTSATURDAYCPMID,
	ifnull(f.DIM_LASTSATURDAYCPMID,1) DIM_LASTSATURDAYCPMID,
	ifnull(f.CT_PRODLOCATION,0) CT_PRODLOCATION,
	ifnull(f.CT_UDA_EXCHRATE,0) CT_UDA_EXCHRATE,
	ifnull(f.CT_PRICE_EURO_AVG_QUARTER,0) CT_PRICE_EURO_AVG_QUARTER,
	ifnull(f.CT_PRICE_EURO_AVG_YEAR,0) CT_PRICE_EURO_AVG_YEAR,
	ifnull(f.AMT_PVE_PRICE_EURO_Q_PREV_YEAR,0) AMT_PVE_PRICE_EURO_Q_PREV_YEAR,
	ifnull(f.AMT_PVE_PRICE_EURO_YTD_PREV_YEAR,0) AMT_PVE_PRICE_EURO_YTD_PREV_YEAR,
	ifnull(f.AMT_COGSFIXEDPLANRATE_EMD,0) AMT_COGSFIXEDPLANRATE_EMD,
	ifnull(f.DD_CMG_ID,0) DD_CMG_ID,
	ifnull(f.AMT_JDACOGS,0) AMT_JDACOGS,
	ifnull(f.DD_COGSSOURCE,'Not Set') DD_COGSSOURCE,
	ifnull(f.AMT_JDACOGS_OP,0) AMT_JDACOGS_OP,
	ifnull(f.DD_COGSSOURCE_OP,'Not Set') DD_COGSSOURCE_OP,
	ifnull(f.DD_WCOGSPCTG,'Not Set') DD_WCOGSPCTG,
	ifnull(f.CT_UNITS_COR_YESTERDAY,0) CT_UNITS_COR_YESTERDAY,
	ifnull(f.DIM_DATEIDSNAPSHOT,1) DIM_DATEIDSNAPSHOT,
	ifnull(f.DIM_MDG_PARTID,1) DIM_MDG_PARTID
FROM
	FACT_JDA_DEMANDFORECAST f inner join DIM_JDA_COMPONENT dc on dc.DIM_JDA_COMPONENTID = f.DIM_JDA_COMPONENTID
    inner join dim_date dt on dt.dim_dateid = f.dim_dateidstartdate
WHERE date_trunc('YEAR',datevalue) = date_trunc('YEAR', current_date)
	AND (f.dd_type in ('Actual / Mkt & Tender Fcst Value (in Qty)' , 'Demand Risks & Business Opportunities (in Qty)' ,
  	'Tender Demand Adj/Supply Risk','Supply Risk (in Qty)',
  	'Actual / Mkt & Tender Fcst Value (in Qty) Cum','Demand Risks & Business Opportunities (in Qty) Cum','Supply Risk (in Qty) Cum',
  	'Actual/Mkt & Tender Fcst Value EURO','Demand Risks & Business Opportunities Euro','Demand Adj Value Tender/Business Risk & Opp EURO',
  	'Supply Risk EURO', 'Demand Adj Value Tender/Supply Risk EURO',
  	'Actual / Mkt & Tender Fcst Value (in Euro) Cum','Demand Risks & Business Opportunities Euro Cum','Supply Risk (in Euro) Cum',
  	'Actual/Mkt & Tender Fcst Value LC','Demand Risks & Business Opportunities LC','Tender Demand Adj/Business Risk & Opportunities',
  	'Supply Risk LC' ,'Tender Demand Adj/Supply Risk',
  	'Actual / Mkt & Tender Fcst Value (in LC) Cum','Demand Risks & Business Opportunities LC Cum', 'Supply Risk (in LC) Cum', 'Consensus Forecast QTY',
  	'Breathing Space (in Qty)', 'Consensus Forecast EURO', 'Consensus Forecast LC', 'Breathing Space (in Euro)', 'Breathing Space (in LC)',
  	'F1 (in Euro) @ OP rate', 'F2 (in Euro) @ OP rate', 'F3 (in Euro) @ OP rate', 'F2 (in LC) Cum', 'F1 (in LC) Cum', 'F3 (in LC) Cum',
  	'F1 (in Euro) Cum',  'F2 (in Euro) Cum',  'F3 (in Euro) Cum', 'Consensus Forecast (in Qty) Cum', 'Consensus Forecast (in Euro) Cum',
  	'Consensus Forecast (in LC) Cum', 'Breathing Space (in LC)', 'Breathing Space (in Euro)','Actual Hist / Mkt Fcst /Tender')
    OR	dc.c_type in ('Price LC','Price Tender LC','Actual Values LC')
  OR	dc.c_type3 in ('F1 QTY' , 'F2 QTY', 'F3 QTY', 'OP', 'OP EURO', 'OP LC', 'F1 LC', 'F2 LC', 'F3 LC'));

update FACT_JDA_DEMANDFORECASTSNAPSHOT f
	set f.dim_dateidsnapshot  = (select dim_dateid from dim_date where datevalue = current_Date and companycode = 'Not Set')
	where f.dim_dateidsnapshot = 1;	

/* don't delete last 7 days --- don't delete if it's a Saturday --- don't delete if it's month end date */
DELETE FROM FACT_JDA_DEMANDFORECASTSNAPSHOT
WHERE dim_dateidsnapshot NOT IN 
(
	SELECT dim_dateid FROM DIM_DATE
		WHERE ((datevalue > current_date - 7 and datevalue <= current_date)
		OR weekdaynumber = 7 OR datevalue in (SELECT DISTINCT monthenddate from dim_date where companycode = 'Not Set' AND calendaryear = year(current_Date)))
		AND companycode = 'Not Set'
		AND calendaryear = year(current_Date)
);

/* copying above delete query for the harmonization part - avoid implementation of accumulating fact*/

DELETE FROM EMD586.FACT_JDA_DEMANDFORECASTSNAPSHOT
WHERE dim_dateidsnapshot NOT IN 
(
	SELECT dim_dateid FROM DIM_DATE
		WHERE ((datevalue > current_date - 7 and datevalue <= current_date)
		OR weekdaynumber = 7 OR datevalue in (SELECT DISTINCT monthenddate from dim_date where companycode = 'Not Set' AND calendaryear = year(current_Date)))
		AND companycode = 'Not Set'
		AND calendaryear = year(current_Date)
);
