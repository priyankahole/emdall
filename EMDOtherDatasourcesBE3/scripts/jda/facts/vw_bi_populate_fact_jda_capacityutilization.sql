drop table if exists tmp_jda_cu_static;
create table tmp_jda_cu_static as
select stsc_resprojstatic_loc loc,
	stsc_resprojstatic_res res,
	STSC_LOC_DESCR descr,
	stsc_res_udc_grp1 udc_grp1,
	(case when month(stsc_resprojstatic_startdate) = month(weekstartdate) then weekstartdate else weekenddate end) as cudate,
	year(stsc_resprojstatic_startdate) as year_start,
	month(stsc_resprojstatic_startdate) as month_start,
	calendarweek as week_start,
	sum(stsc_resprojstatic_cpptotcap) capacity,
	sum(stsc_resprojstatic_mptotload) totalload
from stsc_resprojstatic rs
	inner join stsc_res on stsc_res_res = stsc_resprojstatic_res
	inner join stsc_loc on stsc_resprojstatic_loc = stsc_loc_loc
	inner join dim_date dt on stsc_resprojstatic_startdate = datevalue
where stsc_res_udc_grp1 in ('MANUFACTURING','PACKING')
	and rs.stsc_resprojstatic_loc in ('252MF','450MF','354MF','220MF1')
group by stsc_resprojstatic_loc,
	stsc_resprojstatic_res,
	STSC_LOC_DESCR,
	stsc_res_udc_grp1,
	(case when month(stsc_resprojstatic_startdate) = month(weekstartdate) then weekstartdate else weekenddate end),
	year(stsc_resprojstatic_startdate) ,
	month(stsc_resprojstatic_startdate) ,
	calendarweek;

drop table if exists tmp_jda_cu_detail;
create table tmp_jda_cu_detail as
select stsc_resloaddetail_item item,
	stsc_resloaddetail_loc loc,
	stsc_resloaddetail_res res,
	STSC_RESLOADDETAIL_SUPPLYSEQNUM supplyseqnum,
	--stsc_resloaddetail_whenloaded whenloaded,
	(case when month(stsc_resloaddetail_whenloaded) = month(weekstartdate) then weekstartdate else weekenddate end) as cudate,
	year(stsc_resloaddetail_whenloaded) year_load,
	month(stsc_resloaddetail_whenloaded) month_load,
	calendarweek week_load,
	sum(stsc_resloaddetail_loadqty) loadqty,
	sum(STSC_RESLOADDETAIL_SUPPLYQTY_SINGLE) supplyqty
from stsc_resloaddetail
	inner join stsc_item on stsc_item_item = stsc_resloaddetail_item
	inner join dim_date dt on stsc_resloaddetail_whenloaded = datevalue
	inner join stsc_res on stsc_res_res = stsc_resloaddetail_res
where stsc_res_udc_grp1 in ('MANUFACTURING','PACKING')
	and stsc_resloaddetail_loc  in ('252MF','450MF','354MF','220MF1')
group by stsc_resloaddetail_item,
	stsc_resloaddetail_loc,
	stsc_resloaddetail_res,
	STSC_RESLOADDETAIL_SUPPLYSEQNUM,
	(case when month(stsc_resloaddetail_whenloaded) = month(weekstartdate) then weekstartdate else weekenddate end) ,
	year(stsc_resloaddetail_whenloaded) ,
	month(stsc_resloaddetail_whenloaded),
	calendarweek;

drop table if exists tmp_jda_capacityutil;
create table tmp_jda_capacityutil as
select 	d.item
	,s.loc
	,s.res
	,d.supplyseqnum
	,s.descr
	,s.udc_grp1
	--,s.startdate
	,s.year_start
	,s.month_start
	,s.week_start
	,s.cudate
	,s.capacity
	,s.totalload
	,sum(d.loadqty) loadqty
	,sum(d.supplyqty) supplyqty
from tmp_jda_cu_static s, tmp_jda_cu_detail d --, dim_date dt
where s.loc = d.loc
	and s.res = d.res
	and s.month_start = d.month_load
	and s.year_start = d.year_load
	and s.week_start = d.week_load
	and s.cudate = d.cudate
and s.udc_grp1 in ('MANUFACTURING','PACKING')
	and s.loc in ('252MF','450MF','354MF','220MF1')
group by d.item
	,s.loc
	,s.res
	,d.supplyseqnum
	,s.descr
	,s.udc_grp1
	--,s.startdate
	,s.year_start
	,s.month_start
	,s.week_start
	,s.cudate
	,s.capacity
	,s.totalload;

drop table if exists tmp_jda_location;
create table tmp_jda_location as
select dl.dim_jda_locationid
	,dl.location_name
	,count(nl.nwmgr_location_name) over(partition by nl.nwmgr_location_name order by nl.nwmgr_location_enterprise_id) as rowno
from  dim_jda_location dl, nwmgr_location nl
where dl.location_id = nl.nwmgr_location_location_id;

delete from fact_jda_capacityutilization;
insert into fact_jda_capacityutilization(fact_jda_capacityutilizationid,dim_jda_itemid,dim_jda_locationid,dim_jda_resourceid,dim_jda_packsiteid,dim_jda_manufsiteid,dim_dateid,ct_capacity,ct_totalload,ct_load,dd_groupsite,ct_supplyqty,dd_supplyseqnum,dim_jda_resstaticid)
select (select dim_projectsourceid * multiplier from dim_projectsource) + row_number() over(order by'') fact_jda_capacityutilizationid
	,ifnull(di.dim_jda_itemid,1) as dim_jda_itemid
	,ifnull(tl.dim_jda_locationid,1) as dim_jda_locationid
	,ifnull(dr.dim_jda_resourceid,1) as dim_jda_resourceid
	,ifnull(dp.dim_jda_packsiteid,1) dim_jda_packsiteid
	,ifnull(dm.dim_jda_manufsiteid,1) dim_jda_manufsiteid
	,ifnull(dt.dim_dateid,1) as dim_dateid
	,ifnull(t.capacity,0) as ct_capacity
	,ifnull(t.totalload,0) as ct_totalload
	,ifnull(t.loadqty,0) as ct_load
	,ifnull(t.udc_grp1,'Not Set') as dd_groupsite
	,ifnull(t.supplyqty,0) as ct_supplyqty
	,ifnull(t.supplyseqnum,0) as dd_supplyseqnum
	,ifnull(rs.dim_jda_resstaticid,1) as dim_jda_resstaticid
from tmp_jda_capacityutil t
	inner join dim_jda_item di on t.item = di.item
	inner join tmp_jda_location tl on t.loc = tl.location_name and tl.rowno = 1
	inner join dim_jda_resource dr on t.res = dr.resource
	left join dim_jda_packsite dp on t.res = dp.resource
	left join dim_jda_manufsite dm on t.res = dm.resource
	inner join dim_date dt on  t.cudate = dt.datevalue
	inner join dim_jda_resstatic rs on t.loc = rs.loc and t.res = rs.res
		and t.cudate = rs.datevalue and rs.snapshotdate = current_date;

/*
delete from dim_jda_resstatic
insert into dim_jda_resstatic (dim_jda_resstaticid,loc,res,datevalue,capacity)
select (select dim_projectsourceid * multiplier from dim_projectsource) + row_number() over(order by '') dim_jda_resstaticid
, rs.loc
, rs.res
, rs.datevalue
, rs.capacity
from (select distinct loc, res, cudate as datevalue, capacity
	from tmp_jda_cu_static s
	where s.udc_grp1 in ('MANUFACTURING','PACKING')
		and s.loc in ('252MF','450MF','354MF','220MF1')) rs

update fact_jda_capacityutilization f
	set f.dim_jda_resstaticid = rs.dim_jda_resstaticid
from fact_jda_capacityutilization f
	inner join dim_jda_location dl on dl.dim_jda_locationid = f.dim_jda_locationid
	inner join dim_jda_resource dr on dr.dim_jda_resourceid = f.dim_jda_resourceid
	inner join dim_Date dt on dt.dim_dateid = f.dim_dateid
	inner join dim_jda_resstatic rs on rs.loc = dl.location_name and rs.datevalue = dt.datevalue
		and dr.resource = rs.res and rs.capacity = f.ct_capacity
*/
