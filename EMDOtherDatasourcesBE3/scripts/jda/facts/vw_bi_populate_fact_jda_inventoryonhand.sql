drop table if exists tmp_jda_inventoryoh;
create table tmp_jda_inventoryoh as
select 
	dim_jda_itemid
	,dim_jda_locid
	,dim_jda_skuid
	,dim_dateid as dim_dateidohpost
	,cast(0 as decimal(18,4)) as amt_cogsfixedplanrate
	,sum(stsc_inventory_qty) as CT_INVENTORYMF1
	,0 as CT_INVENTORYMF2
	,0 as CT_INVENTORYMF3
	,0 as CT_INTRANSIT
	,0 as CT_INVENTORYS1
	,0 as CT_INVENTORYS2
	,'Not Set' as mdg_flag
	,cast('Not Set' as varchar(50)) as cogssource
from stsc_inventory i
	inner join dim_jda_item di  on i.stsc_inventory_item = di.item
	inner join dim_jda_sku ds on i.stsc_inventory_item = ds.sku_item and i.stsc_inventory_loc = ds.sku_loc
	inner join dim_jda_loc dl on i.stsc_inventory_loc = dl.loc
	inner join dim_date dt on dt.datevalue = ds.ohpost and dt.companycode = 'Not Set'
	--left join EMDTempoCC4.csv_cogs csvg on di.item = trim(leading 0 from csvg.PRODUCT)  and dl.cmg_id = trim(leading 0 from csvg.Z_REPUNIT)
where i.stsc_inventory_availdate >= ds.ohpost
	and dl.loc_type in ('1','2')
group by dim_jda_itemid	,dim_jda_locid,dim_jda_skuid,dim_dateid
union
select 
	dim_jda_itemid
	,dim_jda_locid
	,dim_jda_skuid
	,dim_dateid as dim_dateidohpost
	,cast(0 as decimal(18,4)) as amt_cogsfixedplanrate
	,0 as CT_INVENTORYMF1
	,sum(stsc_inventory_qty) as CT_INVENTORYMF2
	,0 as CT_INVENTORYMF3
	,0 as CT_INTRANSIT
	,0 as CT_INVENTORYS1
	,0 as CT_INVENTORYS2
	,'Not Set' as mdg_flag
	,cast('Not Set' as varchar(50)) as cogssource
from stsc_inventory i
	inner join dim_jda_item di  on i.stsc_inventory_item = di.item
	inner join dim_jda_sku ds on i.stsc_inventory_item = ds.sku_item and i.stsc_inventory_loc = ds.sku_loc
	inner join dim_jda_loc dl on i.stsc_inventory_loc = dl.loc
	inner join dim_date dt on dt.datevalue = ds.ohpost and dt.companycode = 'Not Set'
	--left join EMDTempoCC4.csv_cogs csvg on di.item = trim(leading 0 from csvg.PRODUCT)  and dl.cmg_id = trim(leading 0 from csvg.Z_REPUNIT)
where i.stsc_inventory_availdate < ds.ohpost
	and dl.loc_type in ('1','2')
group by dim_jda_itemid	,dim_jda_locid,dim_jda_skuid,dim_dateid
union
select 
	dim_jda_itemid
	,dim_jda_locid
	,dss.dim_jda_skuid
	,dim_dateid as dim_dateidohpost
	,cast(0 as decimal(18,4)) as amt_cogsfixedplanrate
	,0 as CT_INVENTORYMF1
	,0 as CT_INVENTORYMF2
	,sum(vll.stsc_vehicleloadline_qty) as CT_INVENTORYMF3
	,0 as CT_INTRANSIT
	,0 as CT_INVENTORYS1
	,0 as CT_INVENTORYS2
	,'Not Set' as mdg_flag
	,cast('Not Set' as varchar(50)) as cogssource
from  stsc_vehicleloadline vll
	inner join stsc_vehicleload vl on vll.stsc_vehicleloadline_loadid = vl.stsc_vehicleload_loadid
	inner join dim_jda_sku dsd on vll.stsc_vehicleloadline_item = dsd.sku_item and vll.stsc_vehicleloadline_dest = dsd.sku_loc
	inner join dim_jda_sku dss on vll.stsc_vehicleloadline_item = dss.sku_item and vll.stsc_vehicleloadline_source = dss.sku_loc
	inner join dim_jda_loc dl on dss.sku_loc = dl.loc
	inner join dim_jda_item di on vll.stsc_vehicleloadline_item = di.item
	inner join dim_date dt on dt.datevalue = dss.ohpost and dt.companycode = 'Not Set'
	--left join EMDTempoCC4.csv_cogs csvg on di.item = trim(leading 0 from csvg.PRODUCT)  and dl.cmg_id = trim(leading 0 from csvg.Z_REPUNIT)
where vl.stsc_vehicleload_sourcestatus = 1
	and vll.STSC_VEHICLELOADLINE_SCHEDSHIPDATE <= dss.ohpost
	and dsd.CUSTORDERCOPYSW <> 1
group by dim_jda_itemid,dim_jda_locid,dss.dim_jda_skuid,dim_dateid
union
select 
	dim_jda_itemid
	,dim_jda_locid
	,dss.dim_jda_skuid
	,dim_dateid as dim_dateidohpost
	,cast(0 as decimal(18,4)) as amt_cogsfixedplanrate
	,0 as CT_INVENTORYMF1
	,0 as CT_INVENTORYMF2
	,0 as CT_INVENTORYMF3
	,sum(vll.stsc_vehicleloadline_qty) as CT_INTRANSIT
	,0 as CT_INVENTORYS1
	,0 as CT_INVENTORYS2
	,'Not Set' as mdg_flag
	,cast('Not Set' as varchar(50)) as cogssource
from  stsc_vehicleloadline vll
	inner join stsc_vehicleload vl on vll.stsc_vehicleloadline_loadid = vl.stsc_vehicleload_loadid
	inner join dim_jda_sku dsd on vll.stsc_vehicleloadline_item = dsd.sku_item and vll.stsc_vehicleloadline_dest = dsd.sku_loc
	inner join dim_jda_sku dss on vll.stsc_vehicleloadline_item = dss.sku_item and vll.stsc_vehicleloadline_source = dss.sku_loc
	inner join dim_jda_loc dl on dsd.sku_loc = dl.loc
	inner join dim_jda_item di on vll.stsc_vehicleloadline_item = di.item
	inner join dim_date dt on dt.datevalue = dss.ohpost and dt.companycode = 'Not Set'
	--left join EMDTempoCC4.csv_cogs csvg on di.item = trim(leading 0 from csvg.PRODUCT)  and dl.cmg_id = trim(leading 0 from csvg.Z_REPUNIT)
where vl.stsc_vehicleload_sourcestatus = 3
	and dsd.CUSTORDERCOPYSW <> 1
group by dim_jda_itemid,dim_jda_locid,dss.dim_jda_skuid,dim_dateid
union
select 
	dim_jda_itemid
	,dim_jda_locid
	,dim_jda_skuid
	,dim_dateid as dim_dateidohpost
	,cast(0 as decimal(18,4)) as amt_cogsfixedplanrate
	,0 as CT_INVENTORYMF1
	,0 as CT_INVENTORYMF2
	,0 as CT_INVENTORYMF3
	,0 as CT_INTRANSIT
	,sum(stsc_inventory_qty) as CT_INVENTORYS1
	,0 as CT_INVENTORYS2
	,'Not Set' as mdg_flag
	,cast('Not Set' as varchar(50)) as cogssource
from stsc_inventory i
	inner join dim_jda_item di  on i.stsc_inventory_item = di.item
	inner join dim_jda_sku ds on i.stsc_inventory_item = ds.sku_item and i.stsc_inventory_loc = ds.sku_loc
	inner join dim_jda_loc dl on i.stsc_inventory_loc = dl.loc
	inner join dim_date dt on dt.datevalue = ds.ohpost and dt.companycode = 'Not Set'
	--left join EMDTempoCC4.csv_cogs csvg on di.item = trim(leading 0 from csvg.PRODUCT)  and dl.cmg_id = trim(leading 0 from csvg.Z_REPUNIT)
where i.stsc_inventory_availdate >= ds.ohpost
	and dl.loc_type in ('3','5')
	and ds.CUSTORDERCOPYSW <> 1
group by dim_jda_itemid	,dim_jda_locid,dim_jda_skuid,dim_dateid
union
select 
	dim_jda_itemid
	,dim_jda_locid
	,dim_jda_skuid
	,dim_dateid as dim_dateidohpost
	,cast(0 as decimal(18,4)) as amt_cogsfixedplanrate
	,0 as CT_INVENTORYMF1
	,0 as CT_INVENTORYMF2
	,0 as CT_INVENTORYMF3
	,0 as CT_INTRANSIT
	,0 as CT_INVENTORYS1
	,sum(stsc_inventory_qty) as CT_INVENTORYS2
	,'Not Set' as mdg_flag
	,cast('Not Set' as varchar(50)) as cogssource
from stsc_inventory i
	inner join dim_jda_item di  on i.stsc_inventory_item = di.item
	inner join dim_jda_sku ds on i.stsc_inventory_item = ds.sku_item and i.stsc_inventory_loc = ds.sku_loc
	inner join dim_jda_loc dl on i.stsc_inventory_loc = dl.loc
	inner join dim_date dt on dt.datevalue = ds.ohpost and dt.companycode = 'Not Set'
	--left join EMDTempoCC4.csv_cogs csvg on di.item = trim(leading 0 from csvg.PRODUCT)  and dl.cmg_id = trim(leading 0 from csvg.Z_REPUNIT)
where i.stsc_inventory_availdate < ds.ohpost
	and dl.loc_type in ('3','5')
	and ds.CUSTORDERCOPYSW <> 1
group by dim_jda_itemid	,dim_jda_locid,dim_jda_skuid,dim_dateid;

/* get COGS */
/* get SAP Std Price */
drop table if exists tmp_inventory_stdprice;
create table tmp_inventory_stdprice
as
select dp.partnumber, dc.company, max(f.amt_StdUnitPrice * f.amt_exchangerate_gbl) stdprice
from emd586.fact_inventoryhistory f
	inner join emd586.dim_part dp on f.dim_partid = dp.dim_partid
	inner join emd586.dim_company dc on f.dim_companyid = dc.dim_companyid
	inner join emd586.dim_date s on f.dim_dateidsnapshot = s.dim_dateid
where s.datevalue >= current_date - interval '3' month
group by dp.partnumber, dc.company;

/* get Business Line */
drop table if exists tmp_upperhier;
create table tmp_upperhier as 
	select distinct partnumber,businessline, max(dim_bwproducthierarchyid) as dim_bwproducthierarchyid , row_number() over(partition by partnumber order by businessline ) as rowno
		from EMD586.dim_part dp, EMD586.dim_bwproducthierarchy bw
	 	where dp.producthierarchy = bw.lowerhierarchycode
			and dp.productgroupsbu = bw.upperhierarchycode
			and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
			and businessline <> 'Not Set'
		group by partnumber,businessline;

drop table if exists tmp_getstcost;
create table tmp_getstcost as
select dim_jda_skuid
	,SKU_ITEM as item
	,cmg_id as company
	,cast(0 as decimal(18,4)) amt_cogsfixedplanrate
	,mdg.COGSICMINDICATOR as mdgflag
	,cast('Not Set' as varchar(50)) as cogssource
 from dim_jda_sku djs
	inner join  dim_jda_loc djl on djs.SKU_LOC = djl.loc
	left outer join dim_mdg_part mdg on SKU_ITEM = mdg.partnumber;

update tmp_getstcost t
	set t.amt_cogsfixedplanrate = t1.stdprice,
		t.cogssource = 'Local Std. Cost (SAP)'
from tmp_getstcost t, tmp_inventory_stdprice t1
where t.item = t1.partnumber 
	and t.company = trim(leading 0 from t1.company)
	and mdgflag <> 'X';

update tmp_getstcost t
	set t.amt_cogsfixedplanrate = csvg.Z_GCPLFF,
		t.cogssource = 'World Std. Cost'
from tmp_getstcost t,  EMDTempoCC4.csv_cogs csvg
where t.company = trim(leading 0 from csvg.Z_REPUNIT)
	and t.item = trim(leading 0 from csvg.PRODUCT)
	and mdgflag = 'X';

update tmp_getstcost t
	set t.amt_cogsfixedplanrate = PE_APPLYED*t1.stdprice,
		t.cogssource = 'Group Reporting PE'
from tmp_getstcost t 
	inner join tmp_upperhier tup on t.item = tup.partnumber
	inner join csv_pe_pctg csvpe on (t.company = csvpe.company and csvpe.BWPRODHIER_BL = tup.businessline)
	inner join tmp_inventory_stdprice t1 on  t.item = t1.partnumber and t.company = trim(leading 0 from t1.company)
where amt_cogsfixedplanrate = 0
	and mdgflag = 'X'
	and rowno = 1;
	
/* get Std. Cost whithout considering the company - only those cases without yet a cost */
drop table if exists tmp_sap_avgstdprice;
create table tmp_sap_avgstdprice as
select t.item, avg(stdprice) as stdprice
from tmp_getstcost t, tmp_inventory_stdprice t1
where t.item = t1.partnumber 
group by t.item;

drop table if exists tmp_cogs_avgprice;
create table tmp_cogs_avgprice as 
select item, avg(csvg.Z_GCPLFF) as Z_GCPLFF
from tmp_getstcost t,  EMDTempoCC4.csv_cogs csvg
where t.item = trim(leading 0 from csvg.PRODUCT)
group by item;

update tmp_getstcost t
	set t.amt_cogsfixedplanrate = t1.stdprice,
		t.cogssource = 'Local Std. Cost (SAP) Avg.'
from tmp_getstcost t, tmp_sap_avgstdprice t1
where t.item = t1.item 
	and t.cogssource = 'Not Set'
	and mdgflag <> 'X';

update tmp_getstcost t
	set t.amt_cogsfixedplanrate = csvg.Z_GCPLFF,
		t.cogssource = 'World Std. Cost Avg.'
from tmp_getstcost t,  tmp_cogs_avgprice csvg
where t.item = csvg.item
	and t.cogssource = 'Not Set'
	and mdgflag = 'X';

update tmp_getstcost t
	set t.amt_cogsfixedplanrate = PE_APPLYED*t1.stdprice,
		t.cogssource = 'Group Reporting PE Avg.'
from tmp_getstcost t 
	inner join tmp_upperhier tup on t.item = tup.partnumber
	inner join csv_pe_pctg csvpe on (t.company = csvpe.company and csvpe.BWPRODHIER_BL = tup.businessline)
	inner join tmp_sap_avgstdprice t1 on  t.item = t1.item
where amt_cogsfixedplanrate = 0
	and mdgflag = 'X'
	and t.cogssource = 'Not Set'
	and rowno = 1;	
	
update tmp_getstcost t
	set t.amt_cogsfixedplanrate = t1.stdprice,
		t.cogssource = 'Group Reporting PE Avg.'
from tmp_getstcost t ,tmp_sap_avgstdprice t1
where t.item = t1.item
	and mdgflag = 'X'
	and t.cogssource = 'Not Set';		

update tmp_jda_inventoryoh t
	set t.amt_cogsfixedplanrate = t1.amt_cogsfixedplanrate,
		t.mdg_flag = ifnull(t1.mdgflag,'Not Set'),
		t.cogssource = t1.cogssource
from tmp_jda_inventoryoh t,tmp_getstcost t1
	where t.dim_jda_skuid = t1.dim_jda_skuid;
/* END get COGS */

truncate table fact_jda_inventoryonhand;
delete from number_fountain m where m.table_name = 'fact_jda_inventoryonhand';
insert into number_fountain
select 'fact_jda_inventoryonhand',
            ifnull(max(d.fact_jda_inventoryonhandid), 
            ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_jda_inventoryonhand d
where d.fact_jda_inventoryonhandid <> 1;

insert into fact_jda_inventoryonhand(fact_jda_inventoryonhandid,dim_jda_itemid,dim_jda_locid,dim_dateidohpost,ct_inventorymf1,
	ct_inventorymf2,ct_inventorymf3,ct_intransit,ct_inventorys1,ct_inventorys2,dw_insert_date,dw_update_date,dim_jda_skuid,amt_cogsfixedplanrate,dd_mdg_flag,dd_cogssource)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_jda_inventoryonhand') + row_number()  over(order by '') AS fact_jda_inventoryonhandid
	,ifnull(dim_jda_itemid,1) as dim_jda_itemid
	,ifnull(dim_jda_locid,1) as dim_jda_locid
	,ifnull(dim_dateidohpost,1) as dim_dateidohpost
	,ifnull(ct_inventorymf1,0) as ct_inventorymf1
	,ifnull(ct_inventorymf2,0) as ct_inventorymf2
	,ifnull(ct_inventorymf3,0) as ct_inventorymf3
	,ifnull(ct_intransit,0) as ct_intransit
	,ifnull(ct_inventorys1,0) as ct_inventorys1
	,ifnull(ct_inventorys2,0) as ct_inventorys2
	,current_timestamp as dw_insert_date
	,current_timestamp as dw_update_date
	,ifnull(dim_jda_skuid,1) as dim_jda_skuid
	,ifnull(amt_cogsfixedplanrate,0) as amt_cogsfixedplanrate
	,ifnull(mdg_flag,'Not Set') as dd_mdg_flag
	,ifnull(cogssource,'Not Set') as dd_cogssource
from tmp_jda_inventoryoh;

update fact_jda_inventoryonhand f
	set f.dim_bwproducthierarchyid = ifnull(t.dim_bwproducthierarchyid,1)
from fact_jda_inventoryonhand f, dim_jda_sku djs , tmp_upperhier t
where f.dim_jda_skuid = djs.dim_jda_skuid
	and djs.sku_item = t.partnumber
	and rowno = 1;

drop table if exists tmp_jda_inventoryoh;
drop table if exists tmp_inventory_stdprice;
drop table if exists tmp_upperhier;