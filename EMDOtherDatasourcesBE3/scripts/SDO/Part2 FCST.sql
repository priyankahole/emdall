delete from tmp_SDO_M_HIST_ORDERS;
insert into tmp_SDO_M_HIST_ORDERS (DMDUNIT,LOC,STARTDATE,QTY ) select DMDUNIT,LOC,STARTDATE,sum(QTY) QTY from SDO_M_HIST_ORDERS   
where concat(year(startdate),month(startdate)) = concat(year(sysdate),month(sysdate)) group by  DMDUNIT,LOC,STARTDATE;

--create mapping between dfu and sku
drop table if exists dfu_mapping;
create table dfu_mapping as 
select 
fcst.DMDUNIT, 
fcst.LOC,
fcst.QTY,
STSC_DFUTOSKU_SKULOC as SKU_LOC, 
STSC_ITEM as SKU_ITEM  from DSO_FCST fcst
   left join STSC_DFUTOSKU dfu on dfu.STSC_DFUTOSKU_DMDUNIT = fcst.DMDUNIT    
                                AND dfu.STSC_DFUTOSKU_DFULOC = fcst.LOC
where  concat(year(fcst.startdate),month(fcst.startdate)) = concat(year(sysdate),month(sysdate));


-- get only sku with 28 weeks of value
drop table if exists tmp_fullmoonsku;
create table tmp_fullmoonsku as 
select ITEM, LOC 
from SDO_SKUPROJSTATIC stat
where  concat(year(stat.STARTDATE),month(stat.STARTDATE)) = concat(year(sysdate),month(sysdate))
--and ALLOCTOTFCST >0
group by  ITEM, LOC
having count(*)=28 ;

--get the sum/percentage of ALLOCTOTFCST at dfu level 
drop table if exists tmp_SKUPROJSTATIC;
create table tmp_SKUPROJSTATIC  as 
select stat.ITEM, 
       stat.LOC,
       xmap.DMDUNIT as dfu_unit,
       xmap.LOC as dfu_loc,
       stat.STARTDATE,
       stat.ALLOCTOTFCST,
       sum (stat.ALLOCTOTFCST) over (partition by stat.ITEM, stat.LOC) toatal_sku,
       (stat.ALLOCTOTFCST/ case when sum (stat.ALLOCTOTFCST) over (partition by xmap.DMDUNIT, xmap.LOC)= 0 then 1 else sum (stat.ALLOCTOTFCST) over (partition by xmap.DMDUNIT, xmap.LOC)end)*100 percentage
from SDO_SKUPROJSTATIC stat,
     tmp_fullmoonsku tmp,
     dfu_mapping xmap
where stat.ITEM = tmp.ITEM and stat.LOC =tmp.LOC 
  and stat.ITEM = xmap.SKU_ITEM and stat.LOC  = xmap.SKU_LOC
  and concat(year(stat.startdate),month(stat.startdate)) = concat(year(sysdate),month(sysdate))
order by xmap.DMDUNIT, xmap.LOC,stat.startdate;



 -- get the value from fcst split by percentage from SDO_SKUPROJSTATIC 

drop table if exists tmp_finalfcst;
create table tmp_finalfcst as 
select fcst.DMDUNIT,
       fcst.LOC,
       stat.STARTDATE,
       QTY * percentage /100 ct_fcstqty 
from DSO_FCST fcst,
     tmp_SKUPROJSTATIC stat
where fcst.DMDUNIT = stat.dfu_unit
  and fcst.LOC  = stat.dfu_loc
  and concat(year(stat.startdate),month(stat.startdate)) = concat(year(fcst.startdate),month(fcst.startdate))
order by stat.startdate;


/* Populate fact */

delete from  fact_sdopilot;

DROP TABLE IF EXISTS max_holder_pilot;
CREATE TABLE max_holder_pilot
AS
SELECT ifnull(max(fact_sdopilotid),ifnull((select s.dim_projectsourceid * s.multiplier from  dim_projectsource s),0)) maxid
FROM fact_sdopilot;





insert into fact_sdopilot
(
fact_sdopilotid,
dim_startdateid ,
dd_dfuitem, 
dd_dfuloc,
ct_fcstqty,
ct_salesqty,
ct_anticipation,
ct_trend ,
ct_oneoff,
ct_replacement,
amt_exchangerate,
amt_exchangerate_GBL,
dd_source,
dd_date
)
select 
max_holder_pilot.maxid + row_number() over(order by '')   as fact_sdopilotid,
ifnull(dim_dateid, 1)as dim_dateid,
ifnull(fcst.DMDUNIT,'Not Set') as dd_dfuitem , 
ifnull(fcst.LOC,'Not Set') as dd_dfuloc,
SUM(ct_fcstqty) OVER(partition by fcst.DMDUNIT,fcst.LOC ORDER BY fcst.startdate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as ct_fcstqty, 
0 as ct_salesqty,
0 as ct_anticipation,
0 as ct_trend ,
0 as ct_oneoff,
0 as ct_replacement,
1 as amt_exchangerate,
1 as amt_exchangerate_GBL,
'Fcst' as dd_source,
fcst.STARTDATE as dd_date
from tmp_finalfcst fcst
  left join dim_date dd on dd.datevalue = fcst.STARTDATE   and companycode = 'Not Set'  
  cross join max_holder_pilot;


DROP TABLE IF EXISTS max_holder_pilot;
CREATE TABLE max_holder_pilot
AS
SELECT ifnull(max(fact_sdopilotid),ifnull((select s.dim_projectsourceid * s.multiplier from  dim_projectsource s),0)) maxid
FROM fact_sdopilot;



insert into fact_sdopilot
(
fact_sdopilotid,
dim_startdateid ,
dd_dfuitem, 
dd_dfuloc,
ct_fcstqty,
ct_salesqty,
ct_anticipation,
ct_trend ,
ct_oneoff,
ct_replacement,
amt_exchangerate,
amt_exchangerate_GBL,
dd_source,
dd_date
)
select 
max_holder_pilot.maxid + row_number() over(order by '')   as fact_sdopilotid,
ifnull(dim_dateid, 1)as dim_dateid,
ifnull(sale.DMDUNIT,'Not Set') as dd_dfuitem , 
ifnull(sale.LOC,'Not Set') as dd_dfuloc,
0  as ct_fcstqty, 
SUM(sale.qty) OVER(partition by sale.DMDUNIT,sale.LOC ORDER BY sale.startdate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as ct_salesqty, 
0 as ct_anticipation,
0 as ct_trend ,
0 as ct_oneoff,
0 as ct_replacement,
1 as amt_exchangerate,
1 as amt_exchangerate_GBL,
'Actual' as dd_source,
sale.STARTDATE as dd_date
from tmp_SDO_M_HIST_ORDERS sale
  left join dim_date dd on dd.datevalue = sale.STARTDATE   and companycode = 'Not Set'  
  cross join max_holder_pilot
where concat(year(sale.startdate),month(sale.startdate)) = concat(year(sysdate),month(sysdate)-1);

/* Anticipation */

drop table if exists  tmp_actual;
create table tmp_actual as
select f1.dd_dfuitem,f1.dd_dfuloc,f1.dd_date,f1.dim_startdateid,f1.ct_salesqty ,f2.days
from fact_sdopilot f1
inner join 
(select dd_dfuitem,dd_dfuloc,dd_source,max(dd_date) dd_date,count(*) days
from fact_sdopilot f
where   dd_source = 'Actual'
   group by dd_dfuitem,dd_dfuloc,dd_source) f2
on f1.dd_dfuitem =f2.dd_dfuitem
  and f1.dd_dfuloc = f2.dd_dfuloc
  and f1.dd_date = f2.dd_date
  and f1.dd_source = f2.dd_source;

drop table if exists  tmp_fcst;
create table tmp_fcst as
select f1.dd_dfuitem,f1.dd_dfuloc,f1.dd_date,f1.dim_startdateid,f1.ct_fcstqty ,f2.days
from fact_sdopilot f1
inner join 
(select dd_dfuitem,dd_dfuloc,dd_source,max(dd_date) dd_date,count(*) days
from fact_sdopilot f
where   dd_source = 'Fcst'
   group by dd_dfuitem,dd_dfuloc,dd_source) f2
on f1.dd_dfuitem =f2.dd_dfuitem
  and f1.dd_dfuloc = f2.dd_dfuloc
  and f1.dd_date = f2.dd_date
  and f1.dd_source =f2.dd_source;


drop table if exists  tmp_Anticipation ;
create table tmp_Anticipation as
select 
act.dd_dfuitem, act.dd_dfuloc,act.dd_date actdate, fcst.dd_date fcstdate,fcst.ct_fcstqty,act.ct_salesqty,(fcst.ct_fcstqty - act.ct_salesqty) /(fcst.days- act.days) as ct_anticipationinterval
from tmp_actual act,tmp_fcst fcst
where   act.dd_dfuitem = fcst.dd_dfuitem
  and act.dd_dfuloc =  fcst.dd_dfuloc
  and act.dd_dfuitem ='3000320003'
and  act.dd_dfuloc = '411DE';


drop table if exists tmp_Anticipation1;
create table tmp_Anticipation1 as 
select  
tmp.dd_dfuitem, tmp.dd_dfuloc,f.dd_date,dim_startdateid   ,ct_anticipationinterval
from fact_sdopilot f, tmp_Anticipation tmp  
where tmp.dd_dfuitem = f.dd_dfuitem
and   tmp.dd_dfuloc = f.dd_dfuloc
and actdate <= f.dd_date 
and fcstdate >= f.dd_date
and  dd_source = 'Fcst';
  
update tmp_Anticipation1 f1
set f1.ct_anticipationinterval = f.ct_salesqty
from tmp_Anticipation1 f1, tmp_Anticipation f
where f.ACTDATE = f1.dd_date
  and f.dd_dfuitem = f1.dd_dfuitem
  and f.dd_dfuloc = f1.dd_dfuloc;


DROP TABLE IF EXISTS max_holder_pilot;
CREATE TABLE max_holder_pilot
AS
SELECT ifnull(max(fact_sdopilotid),ifnull((select s.dim_projectsourceid * s.multiplier from  dim_projectsource s),0)) maxid
FROM fact_sdopilot;

 insert into fact_sdopilot
(
 fact_sdopilotid,dd_dfuitem, dd_dfuloc, dd_date, dim_startdateid,ct_anticipation  
)
select  
max_holder_pilot.maxid + row_number() over(order by '')   as fact_sdopilotid,
dd_dfuitem, dd_dfuloc, dd_date, dim_startdateid,
SUM(ct_anticipationinterval) OVER(partition by dd_dfuitem, dd_dfuloc ORDER BY dd_date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as ct_anticipation  
from tmp_Anticipation1
 cross join max_holder_pilot;
 
/* Trend */
-- Calculate avg of increase by day for each sku (max value - min value / count of days)
drop table if exists  tmp_actualtrend;
create table tmp_actualtrend as
select f1.dd_dfuitem,f1.dd_dfuloc,  f3.dd_date as maxdate,(sum(f3.CT_SALESQTY) -sum(f1.ct_salesqty))/f3.days   as ct_avgqty
from fact_sdopilot f1
inner join 
(select dd_dfuitem,dd_dfuloc,dd_source,min(dd_date) dd_date  from fact_sdopilot f where   dd_source = 'Actual' group by dd_dfuitem,dd_dfuloc,dd_source) f2
  on f1.dd_dfuitem =f2.dd_dfuitem
  and f1.dd_dfuloc = f2.dd_dfuloc
  and f1.dd_date = f2.dd_date
  and f1.dd_source =f2.dd_source
inner join tmp_actual f3 
           on f1.dd_dfuitem =f3.dd_dfuitem
           and f1.dd_dfuloc = f3.dd_dfuloc
group by f1.dd_dfuitem,f1.dd_dfuloc,f3.dd_date,f3.days;

 
   
-- assign the previous calculated value to the next days (from last actual value until the last day of the month)
drop table if exists tmp_trend;
create table tmp_trend as 
select  
tmp.dd_dfuitem, tmp.dd_dfuloc,f.dd_date,dim_startdateid   ,ct_avgqty
from fact_sdopilot f, tmp_actualtrend tmp  
where tmp.dd_dfuitem = f.dd_dfuitem
and   tmp.dd_dfuloc = f.dd_dfuloc
and maxdate <= f.dd_date   
and  dd_source = 'Fcst';


update tmp_trend f
set f.ct_avgqty = f1.ct_salesqty
from tmp_trend f,  tmp_actual f1
where f.dd_date = f1.dd_date
  and f.dd_dfuitem = f1.dd_dfuitem
  and f.dd_dfuloc = f1.dd_dfuloc;

--insert values into fact
DROP TABLE IF EXISTS max_holder_pilot;
CREATE TABLE max_holder_pilot
AS
SELECT ifnull(max(fact_sdopilotid),ifnull((select s.dim_projectsourceid * s.multiplier from  dim_projectsource s),0)) maxid
FROM fact_sdopilot;

 insert into fact_sdopilot
(
 fact_sdopilotid,dd_dfuitem, dd_dfuloc, dd_date, dim_startdateid,ct_trend
)
select  
max_holder_pilot.maxid + row_number() over(order by '')   as fact_sdopilotid,
dd_dfuitem, dd_dfuloc, dd_date, dim_startdateid,
SUM(ct_avgqty) OVER(partition by dd_dfuitem, dd_dfuloc ORDER BY dd_date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as ct_trend  
from tmp_trend
 cross join max_holder_pilot;

update fact_sdopilot
set ct_trend  = null
where ct_trend  = 0;

update fact_sdopilot
set ct_anticipation   = null
where ct_anticipation   = 0;

update fact_sdopilot
set ct_salesqty   = null
where ct_salesqty   = 0;


 

/* One off */
drop table if exists tmp_oneoff;
create table tmp_oneoff as 
select   f1.dd_dfuitem,f1.dd_dfuloc,f1.ct_salesqty,f1.dd_date  from fact_sdopilot f1
 inner join  (select  max(dd_date) dd_date, dd_dfuitem,dd_dfuloc  from fact_sdopilot where  ct_salesqty is not null  group by dd_dfuitem,dd_dfuloc) f2
     on f1.dd_dfuitem = f2.dd_dfuitem and f1.dd_dfuloc = f2.dd_dfuloc and f1.dd_date = f2.dd_date 
where f1.dd_source ='Actual'
union all select DMDUNIT,LOC,CT_FCSTQTY,STARTDATE
 from tmp_finalfcst tmp
   inner join  (select  max(dd_date) dd_date, dd_dfuitem,dd_dfuloc  from fact_sdopilot where  ct_salesqty is not null  group by dd_dfuitem,dd_dfuloc) f2
      on tmp.dmdunit = f2.dd_dfuitem and tmp.loc = f2.dd_dfuloc and tmp.startdate > f2.dd_date ;
 


DROP TABLE IF EXISTS max_holder_pilot;
CREATE TABLE max_holder_pilot
AS
SELECT ifnull(max(fact_sdopilotid),ifnull((select s.dim_projectsourceid * s.multiplier from  dim_projectsource s),0)) maxid
FROM fact_sdopilot;



insert into fact_sdopilot
(
fact_sdopilotid,
dim_startdateid ,
dd_dfuitem, 
dd_dfuloc,
ct_fcstqty,
ct_salesqty,
ct_anticipation,
ct_trend ,
ct_oneoff,
ct_replacement,
amt_exchangerate,
amt_exchangerate_GBL,
dd_source,
dd_date
)
select 
max_holder_pilot.maxid + row_number() over(order by '')   as fact_sdopilotid,
ifnull(dim_dateid, 1)as dim_dateid,
ifnull(t.dd_dfuitem,'Not Set') as dd_dfuitem , 
ifnull(t.dd_dfuloc,'Not Set') as dd_dfuloc,
null  as ct_fcstqty, 
null as ct_salesqty,
null as ct_anticipation,
null as ct_trend ,
SUM(t.ct_salesqty) OVER(partition by t.dd_dfuitem,t.dd_dfuloc ORDER BY t.dd_date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as ct_oneoff, 
0 as ct_replacement,
1 as amt_exchangerate,
1 as amt_exchangerate_GBL,
'OneOff' as dd_source,
t.dd_date as dd_date
from tmp_oneoff  t
  left join dim_date dd on dd.datevalue = t.dd_date   and companycode = 'Not Set'  
  cross join max_holder_pilot;
  
  
update fact_sdopilot
set ct_oneoff  = null
where ct_oneoff   = 0;

/* Manual */

drop table if exists  tmp_Manual ;
create table tmp_manual as select * from tmp_Anticipation;

update tmp_manual
set CT_FCSTQTY = 10008.8000 --  'var'
where DD_DFUITEM = '3000320003'  --get value
and DD_DFULOC ='411DE'; -- get value
  

drop table if exists tmp_manual1;
create table tmp_manual1 as 
select  
tmp.dd_dfuitem, tmp.dd_dfuloc,f.dd_date,dim_startdateid   ,ct_anticipationinterval/intervaldays as ct_manualinterval
from fact_sdopilot f, tmp_Manual tmp  
where tmp.dd_dfuitem = f.dd_dfuitem
and   tmp.dd_dfuloc = f.dd_dfuloc
and actdate <= f.dd_date 
and fcstdate >= f.dd_date
and  dd_source = 'Fcst';
  
update tmp_manual1 f1
set f1.ct_manualinterval = f.ct_salesqty
from tmp_manual1 f1, tmp_manual f
where f.ACTDATE = f1.dd_date
  and f.dd_dfuitem = f1.dd_dfuitem
  and f.dd_dfuloc = f1.dd_dfuloc;


DROP TABLE IF EXISTS max_holder_pilot;
CREATE TABLE max_holder_pilot
AS
SELECT ifnull(max(fact_sdopilotid),ifnull((select s.dim_projectsourceid * s.multiplier from  dim_projectsource s),0)) maxid
FROM fact_sdopilot;

 insert into fact_sdopilot
(
 fact_sdopilotid,dd_dfuitem, dd_dfuloc, dd_date, dim_startdateid,ct_manual
)
select  
max_holder_pilot.maxid + row_number() over(order by '')   as fact_sdopilotid,
dd_dfuitem, dd_dfuloc, dd_date, dim_startdateid,
SUM(ct_manualinterval) OVER(partition by dd_dfuitem, dd_dfuloc ORDER BY dd_date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as ct_manual
from tmp_manual1
 cross join max_holder_pilot;

delete from EMD586.fact_sdopilot;
insert into EMD586.fact_sdopilot
(
fact_sdopilotid,
dim_startdateid ,
dd_dfuitem, 
dd_dfuloc,
ct_fcstqty,
ct_salesqty,
ct_anticipation,
ct_trend ,
ct_oneoff,
ct_replacement,
amt_exchangerate,
amt_exchangerate_GBL,
dd_source,
dd_date
)
select 
fact_sdopilotid,
dim_startdateid ,
dd_dfuitem, 
dd_dfuloc,
ct_fcstqty,
ct_salesqty,
ct_anticipation,
ct_trend ,
ct_oneoff,
ct_replacement,
amt_exchangerate,
amt_exchangerate_GBL,
dd_source,
dd_date
from EMDOTHERDATASOURCESBE3.fact_sdopilot;