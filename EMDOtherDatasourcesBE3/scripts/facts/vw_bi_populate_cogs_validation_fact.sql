truncate table fact_cogs_validation;
delete from number_fountain m where m.table_name = 'fact_cogs_validation';
	insert into number_fountain
	select 'fact_cogs_validation', ifnull(max(f.fact_cogs_validationid), 
									 ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from fact_cogs_validation f;

insert into fact_cogs_validation (
	fact_cogs_validationid
	,dd_company
	,dd_partnumber
	,dd_version
	,dd_calmonth
	,amt_fixplan_rate
	,amt_ExchangeRate
	,amt_ExchangeRate_GBL
	,dw_insert_date
	,dw_update_date
	,dim_projectsourceid)
select
	(select max_id from number_fountain where table_name = 'fact_cogs_validation') + row_number() over(order by '') as fact_cogs_validationid
	,z_repunit dd_compnay
	,product dd_partnumber
	,z_version
	,calmonth
	,Z_GCPLFF amt_fixplanrate
	,1
	,1
	,current_timestamp dw_insert_date
	,current_timestamp dw_update_date
	,1 dim_projectsourceid
from YBZICMICOGA;

truncate table csv_cogs;
insert into csv_cogs (Z_REPUNIT,PRODUCT,Z_VERSION,Z_RU_PRE,CALMONTH,CALMONTH2,CALYEAR,CURKEY_GC,ZICMZCOGS,Z_GCACTF,Z_GCFIXF,Z_GCPLFF,Z_GCPLRF,Z_GCPYFF,Z_GCPYRF,Z_GCPYTF1,Z_GCPYTF2,Z_GCPYTF3,Z_GCTOF1,Z_GCTOF2,Z_GCTOF3)
	select (t.Z_REPUNIT,t.PRODUCT,t.Z_VERSION,t.Z_RU_PRE,t.CALMONTH,t.CALMONTH2,t.CALYEAR,t.CURKEY_GC,t.ZICMZCOGS,t.Z_GCACTF,t.Z_GCFIXF,t.Z_GCPLFF,
		t.Z_GCPLRF,t.Z_GCPYFF,t.Z_GCPYRF,t.Z_GCPYTF1,t.Z_GCPYTF2,t.Z_GCPYTF3,t.Z_GCTOF1,t.Z_GCTOF2,t.Z_GCTOF3)
	from YBZICMICOGA t;
