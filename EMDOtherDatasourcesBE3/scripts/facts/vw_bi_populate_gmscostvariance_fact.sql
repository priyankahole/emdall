/******************************************************************************************************************/
/*   Script         : bi_populate_gmscostvariance_fact                                                            */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 12 Nov 2017                                                                                 */
/*   Description    : Populating script of fact_gmscostvariance                                                   */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   12 Nov 2017         CristianT       1.0               Creating the script.                                   */
/*   26 Mar 2018         CristianT       1.1               Adding dd_siteorder                                    */
/*   12 Jun 2018         CristianT       1.2               Adding ct_cost_variance_vs_op_cumulated                */
/*   21 JUn 2018         CristianB       1.3               Added ct_cp_ms_completed,ct_cp_ms_to_be_confirmed      */
/*                                                          ct_cp_ms_missed,ct_cp_ms_on_hold,ct_cp_ms_at_risk     */
/******************************************************************************************************************/

DROP TABLE IF EXISTS number_fountain_fact_gmscostvariance;
CREATE TABLE number_fountain_fact_gmscostvariance LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_gmscostvariance;
CREATE TABLE tmp_fact_gmscostvariance
AS
SELECT *
FROM fact_gmscostvariance
WHERE 1 = 0;

DELETE FROM number_fountain_fact_gmscostvariance WHERE table_name = 'fact_gmscostvariance';

INSERT INTO number_fountain_fact_gmscostvariance
SELECT 'fact_gmscostvariance', ifnull(max(fact_gmscostvarianceid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_gmscostvariance;

DROP TABLE IF EXISTS tmp_gms_datevalues;
CREATE TABLE tmp_gms_datevalues
AS
SELECT dim_dateid,
       datevalue,
       monthname,
       calendaryear,
       ROW_NUMBER() over(partition by monthname, calendaryear order by datevalue) as ro_no
FROM dim_date
WHERE companycode = 'Not Set';

DELETE FROM tmp_gms_datevalues
WHERE ro_no > 1;

INSERT INTO tmp_fact_gmscostvariance(
fact_gmscostvarianceid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_site,
dim_dateid,
ct_qi,
ct_cost_variance,
ct_checkpointachieved,
ct_ehsir_12monthrolling,
ct_ehs_process_index,
dd_organization,
ct_dsiday,
ct_pa,
ct_otif,
ct_devplanpotential,
ct_devplanrole3plus,
ct_devplan1plus2,
ct_cost_variance_vs_op,
ct_csl,
ct_cost_variance_vs_op_cumulated,
ct_cp_ms_completed,
ct_cp_ms_at_risk,
ct_cp_ms_missed,
ct_cp_ms_on_hold,
ct_cp_ms_to_be_confirmed,
ct_capa_backlog,
ct_dev_human_error,
ct_imprv_oper_savings,
ct_inv_value,
ct_balance_male_manager,
ct_balance_female_manager,
ct_gms_potential_upward_carrer,
ct_gms_potential_lateral_Career,
ct_avg_development_plans,
ct_pov_deviationvsop,
ct_pov_deviationvsop_accumulated,
ct_ccv_pov_accumulated,
ct_logistics_costs
)
SELECT (SELECT max_id from number_fountain_fact_gmscostvariance WHERE table_name = 'fact_gmscostvariance') + ROW_NUMBER() over(order by '') AS fact_gmscostvarianceid,
       t.*
FROM (
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(qi.plant, 'Not Set') as dd_site,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       ifnull(qi.qi, 0) as ct_qi,
       ifnull(qi.cost_variance, 0) as ct_cost_variance,
       ifnull(qi.checkpointachieved , 0) as ct_checkpointachieved,
       ifnull(qi.ehsir_12monthrolling, 0) as ct_ehsir_12monthrolling,
       ifnull(qi.ehs_process_index, 0) as ct_ehs_process_index,
       ifnull(qi.organization, 'Not Set') as dd_organization,
       ifnull(qi.dsiday, 0) as ct_dsiday,
       ifnull(qi.pa, 0) as ct_pa,
       ifnull(qi.otif, 0) as ct_otif,
       ifnull(qi.devplanpotential, 0) as ct_devplanpotential,
       ifnull(qi.devplanrole3plus, 0) as ct_devplanrole3plus,
       ifnull(qi.devplan1plus2, 0) as ct_devplan1plus2,
       ifnull(qi.cost_variance_vs_op, 0) as ct_cost_variance_vs_op,
       ifnull(qi.csl, 0) as ct_csl,
       ifnull(qi.cost_variance_vs_op_cumulated, 0) as ct_cost_variance_vs_op_cumulated,
       ifnull(qi.cp_ms_completed, 0) as ct_cp_ms_completed,
       ifnull(qi.cp_ms_at_risk, 0) as ct_cp_ms_at_risk,
       ifnull(qi.cp_ms_missed, 0) as ct_cp_ms_missed,
       ifnull(qi.cp_ms_on_hold, 0) as ct_cp_ms_on_hold,
       ifnull(qi.cp_ms_to_be_confirmed, 0) as ct_cp_ms_to_be_confirmed,
       ifnull(qi.capa_backlog, 0) as ct_capa_backlog,
       ifnull(qi.dev_human_error, 0) as ct_dev_human_error,
       ifnull(qi.imprv_oper_savings, 0) as ct_imprv_oper_savings,
       ifnull(qi.inv_value, 0) as ct_inv_value,
       ifnull(qi.balance_male_manager, 0) as ct_balance_male_manager,
       ifnull(qi.balance_female_manager, 0) as ct_balance_female_manager,
       ifnull(qi.gms_potential_upward_carrer, 0) as ct_gms_potential_upward_carrer,
       ifnull(qi.gms_potential_lateral_Career, 0) as ct_gms_potential_lateral_Career,
       ifnull(qi.avg_development_plans, 0) as ct_avg_development_plans,
       ifnull(qi.pov_deviationvsop, 0) as ct_pov_deviationvsop,
       ifnull(qi.pov_deviationvsop_accumulated, 0) as ct_pov_deviationvsop_accumulated,
       ifnull(qi.ccv_pov_accumulated, 0) as ct_ccv_pov_accumulated,
       ifnull(qi.logistics_costs, 0) as ct_logistics_costs
FROM qi_cost_variance qi
     INNER JOIN tmp_gms_datevalues dt ON trim(dt.monthname) = trim(qi.qi_month) AND dt.calendaryear = trim(qi.qi_year)
     ) t;

DROP TABLE IF EXISTS tmp_gms_datevalues;

UPDATE tmp_fact_gmscostvariance tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_gmscostvariance tmp;

DELETE FROM fact_gmscostvariance;

INSERT INTO fact_gmscostvariance (
fact_gmscostvarianceid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_site,
dim_dateid,
ct_qi,
ct_cost_variance,
ct_checkpointachieved,
ct_ehsir_12monthrolling,
ct_ehs_process_index,
dd_organization,
ct_dsiday,
ct_pa,
ct_otif,
ct_devplanpotential,
ct_devplanrole3plus,
ct_devplan1plus2,
ct_cost_variance_vs_op,
ct_csl,
ct_cost_variance_vs_op_cumulated,
ct_cp_ms_completed,
ct_cp_ms_at_risk,
ct_cp_ms_missed,
ct_cp_ms_on_hold,
ct_cp_ms_to_be_confirmed,
ct_capa_backlog,
ct_dev_human_error,
ct_imprv_oper_savings,
ct_inv_value,
ct_balance_male_manager,
ct_balance_female_manager,
ct_gms_potential_upward_carrer,
ct_gms_potential_lateral_Career,
ct_avg_development_plans,
ct_pov_deviationvsop,
ct_pov_deviationvsop_accumulated,
ct_ccv_pov_accumulated,
ct_logistics_costs
)
SELECT fact_gmscostvarianceid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_site,
       dim_dateid,
       ct_qi,
       ct_cost_variance,
       ct_checkpointachieved,
       ct_ehsir_12monthrolling,
       ct_ehs_process_index,
       dd_organization,
       ct_dsiday,
       ct_pa,
       ct_otif,
       ct_devplanpotential,
       ct_devplanrole3plus,
       ct_devplan1plus2,
       ct_cost_variance_vs_op,
       ct_csl,
       ct_cost_variance_vs_op_cumulated,
       ct_cp_ms_completed,
       ct_cp_ms_at_risk,
       ct_cp_ms_missed,
       ct_cp_ms_on_hold,
       ct_cp_ms_to_be_confirmed,
       ct_capa_backlog,
       ct_dev_human_error,
       ct_imprv_oper_savings,
       ct_inv_value,
       ct_balance_male_manager,
       ct_balance_female_manager,
       ct_gms_potential_upward_carrer,
       ct_gms_potential_lateral_Career,
       ct_avg_development_plans,
       ct_pov_deviationvsop,
       ct_pov_deviationvsop_accumulated,
       ct_ccv_pov_accumulated,
       ct_logistics_costs
FROM tmp_fact_gmscostvariance;

DROP TABLE IF EXISTS tmp_fact_gmscostvariance;

/* 26 Mar 2018 CristianT Start: Adding dd_siteorder */
DROP TABLE IF EXISTS tmp_dd_siteorder;
CREATE TABLE tmp_dd_siteorder
AS
SELECT dd_site,
       CASE
         WHEN dd_site = 'GMS' THEN 1
         WHEN dd_site = 'Dev & Launch' THEN 2
         WHEN dd_site = 'Biotech' THEN 3
         WHEN dd_site = 'Pharma' THEN 4
         WHEN dd_site = 'SNO' THEN 5
         WHEN dd_site = 'EHS' THEN 6
         WHEN dd_site = 'Engineering' THEN 7
         WHEN dd_site = 'SBO' THEN 8
         WHEN dd_site like 'Target %' THEN 100 + ROW_NUMBER() over(order by dd_site)
         WHEN dd_site like '%CCV%' THEN 200 + ROW_NUMBER() over(order by dd_site)
         ELSE 8 + ROW_NUMBER() over(order by dd_site)
       END as dd_siteorder
FROM fact_gmscostvariance
GROUP BY dd_site;


UPDATE fact_gmscostvariance
SET dd_siteorder = tmp.dd_siteorder
FROM fact_gmscostvariance fct,
     tmp_dd_siteorder tmp
WHERE fct.dd_site = tmp.dd_site;

DROP TABLE IF EXISTS tmp_dd_siteorder;

/* 26 Mar 2018 CristianT End */

DELETE FROM emd586.fact_gmscostvariance;
INSERT INTO emd586.fact_gmscostvariance
SELECT *
FROM fact_gmscostvariance;

