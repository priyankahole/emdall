/******************************************************************************************************************/
/*   Script         : bi_populate_fact_ortems                                                                     */
/*   Author         : Cristian Cleciu                                                                             */
/*   Created On     : 06 Sep 2017                                                                                 */
/*   Description    : Populating script of fact_ortems                                                            */
/*********************************************Change History*******************************************************/
/*   Date                By             		Version      Desc                                                 */
/*   06 Sep 2017         Cristian Cleciu        1.0          Creating the script.                                 */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_ortems;
CREATE TABLE tmp_fact_ortems
AS
SELECT *
FROM fact_ortems
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_ortems';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_ortems',IFNULL(MAX(fact_ortemsID),0)
FROM tmp_fact_ortems;

INSERT INTO tmp_fact_ortems( 
fact_ortemsid            
,dw_update_date           
,dw_insert_date           
,dd_site_name             
,dd_machine_name          
,dd_format_name           
,dd_format_description    
,dd_part_name             
,dd_part_description      
,dd_order_number          
,dd_order_description     
,dd_order_start_ts        
,dd_order_end_ts          
,dim_date_order_start_id  
,dim_date_order_end_id    
,ct_ort_plan_ord_dur_sec  
,dd_ort_plan_ord_dur_hr   
,ct_ort_plan_quantity     
,ct_oee_ord_dur_sec       
,dd_oee_ord_dur_hr        
,ct_oee_ord_quantity      
,ct_delta_dur_sec         
,dd_delta_dur_hr          
,ct_delta_quantity        
,dim_projectsourceid
,dd_max_end_date
,dd_min_start_date 
,ct_ort_plan_quantity_orig_value
,ct_ort_part_mult_factor			   
)
SELECT 
(select ifnull(max_id,1) 
 from number_fountain 
 where table_name = 'tmp_fact_ortems') + row_number() over(order by '') AS fact_ortemsid
,current_timestamp AS dw_update_date           
,current_timestamp AS dw_insert_date           
,ifnull(st.site_name,'Not Set') AS dd_site_name             
,ifnull(st.machine_name,'Not Set') AS dd_machine_name          
,ifnull(st.format_name,'Not Set') AS dd_format_name           
,ifnull(st.format_description,'Not Set') AS dd_format_description    
,ifnull(st.part_name,'Not Set') AS dd_part_name             
,ifnull(st.part_description,'Not Set') AS dd_part_description      
,ifnull(st.order_number,'Not Set') AS dd_order_number          
,ifnull(st.order_description,'Not Set') AS dd_order_description     
,ifnull(st.order_start_ts,'0001-01-01') AS dd_order_start_ts        
,ifnull(st.order_end_ts,'0001-01-01') AS dd_order_end_ts          
,1 AS dim_date_order_start_id  
,1 AS dim_date_order_end_id    
,ifnull(st.ort_plan_ord_dur_sec,0) AS ct_ort_plan_ord_dur_sec  
,ifnull(st.ort_plan_ord_dur_hr,'Not Set') AS dd_ort_plan_ord_dur_hr   
,ifnull(st.ort_plan_quantity,0) AS ct_ort_plan_quantity     
,ifnull(st.oee_ord_dur_sec,0) AS ct_oee_ord_dur_sec       
,ifnull(st.oee_ord_dur_hr,'Not Set') AS dd_oee_ord_dur_hr        
,ifnull(st.oee_ord_quantity,0) AS ct_oee_ord_quantity      
,ifnull(st.delta_dur_sec,0) AS ct_delta_dur_sec         
,ifnull(st.delta_dur_hr,'Not Set') AS dd_delta_dur_hr          
,ifnull(st.delta_quantity,0) AS ct_delta_quantity        
,1 AS dim_projectsourceid 
,0 AS dd_max_end_date
,0 AS dd_min_start_date 
,ifnull(st.ort_plan_quantity_orig_value,0) AS ct_ort_plan_quantity_orig_value
,ifnull(st.ort_part_mult_factor,0) AS ct_ort_part_mult_factor			
FROM ord_ortplan_vs_gmdact st;

-- updates for Date dimensions 

UPDATE tmp_fact_ortems F
SET F.dim_date_order_start_id = D.DIM_DATEID
FROM  tmp_fact_ortems F, DIM_DATE D 
WHERE to_date(f.dd_order_start_ts) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_date_order_start_id  <> D.DIM_DATEID;
      
UPDATE tmp_fact_ortems F
SET F.dim_date_order_end_id = D.DIM_DATEID
FROM  tmp_fact_ortems F, DIM_DATE D 
WHERE to_date(f.dd_order_end_ts) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_date_order_end_id  <> D.DIM_DATEID;

DELETE FROM fact_ortems;

INSERT INTO fact_ortems(
fact_ortemsid            
,dw_update_date           
,dw_insert_date           
,dd_site_name             
,dd_machine_name          
,dd_format_name           
,dd_format_description    
,dd_part_name             
,dd_part_description      
,dd_order_number          
,dd_order_description     
,dd_order_start_ts        
,dd_order_end_ts          
,dim_date_order_start_id  
,dim_date_order_end_id    
,ct_ort_plan_ord_dur_sec  
,dd_ort_plan_ord_dur_hr   
,ct_ort_plan_quantity     
,ct_oee_ord_dur_sec       
,dd_oee_ord_dur_hr        
,ct_oee_ord_quantity      
,ct_delta_dur_sec         
,dd_delta_dur_hr          
,ct_delta_quantity        
,dim_projectsourceid
,dd_max_end_date
,dd_min_start_date 
,ct_ort_plan_quantity_orig_value
,ct_ort_part_mult_factor 
)
SELECT 
fact_ortemsid            
,dw_update_date           
,dw_insert_date           
,dd_site_name             
,dd_machine_name          
,dd_format_name           
,dd_format_description    
,dd_part_name             
,dd_part_description      
,dd_order_number          
,dd_order_description     
,dd_order_start_ts        
,dd_order_end_ts          
,dim_date_order_start_id  
,dim_date_order_end_id    
,ct_ort_plan_ord_dur_sec  
,dd_ort_plan_ord_dur_hr   
,ct_ort_plan_quantity     
,ct_oee_ord_dur_sec       
,dd_oee_ord_dur_hr        
,ct_oee_ord_quantity      
,ct_delta_dur_sec         
,dd_delta_dur_hr          
,ct_delta_quantity        
,dim_projectsourceid
,dd_max_end_date
,dd_min_start_date  
,ct_ort_plan_quantity_orig_value
,ct_ort_part_mult_factor
FROM tmp_fact_ortems;

DROP TABLE IF EXISTS tmp_fact_ortems;

/*created flag for min start date and max end date*/

drop table if exists tmp_max_order_end_date;

create table tmp_max_order_end_date as 
select dd_machine_name, 
max(dd_order_end_ts) as dd_order_end_ts,
1 as max_end_date
   from fact_ortems f
group by  dd_machine_name
order by dd_machine_name asc;

update fact_ortems f
set f.dd_max_end_date = t.max_end_date 
from fact_ortems f, tmp_max_order_end_date t
where f.dd_machine_name = t.dd_machine_name
and f.dd_order_end_ts = t.dd_order_end_ts;

drop table if exists tmp_min_order_start_date;

create table tmp_min_order_start_date as 
select dd_machine_name, 
min(dd_order_start_ts) as dd_order_start_ts,
1 as min_start_date
   from fact_ortems f
group by  dd_machine_name
order by dd_machine_name asc;

update fact_ortems f
set f.dd_min_start_date = t.min_start_date 
from fact_ortems f, tmp_min_order_start_date t
where f.dd_machine_name = t.dd_machine_name
and f.dd_order_start_ts = t.dd_order_start_ts;

drop table if exists tmp_max_order_end_date;
drop table if exists tmp_min_order_start_date;
